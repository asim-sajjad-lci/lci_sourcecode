/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the PayScheduleSetupDepartment database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40935",indexes = {
        @Index(columnList = "PYSCHDINDXD")
})
@NamedQuery(name = "PayScheduleSetupDepartment.findAll", query = "SELECT p FROM PayScheduleSetupDepartment p")
public class PayScheduleSetupDepartment extends HcmBaseEntity implements Serializable {
	
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PYSCHDINDXD")
	private int payScheduleDepartmentIndexId;
	
	@ManyToOne
	@JoinColumn(name="PYSCHDINDX")
	private PayScheduleSetup payScheduleSetup;
	
	@ManyToOne
	@JoinColumn(name="DEPINDX")
	private Department department;
	
	@Column(name = "DEPDSCR",columnDefinition="char(31)")
	private String departmentDescription;
	
	
	@Column(name = "DEPDSCRA",columnDefinition="char(61)")
	private String arabicDepartmentDescription;
	
	
	@Column(name = "PYSCHDACT")
	private Boolean payScheduleStatus;


	public int getPayScheduleDepartmentIndexId() {
		return payScheduleDepartmentIndexId;
	}


	public void setPayScheduleDepartmentIndexId(int payScheduleDepartmentIndexId) {
		this.payScheduleDepartmentIndexId = payScheduleDepartmentIndexId;
	}


	public PayScheduleSetup getPayScheduleSetup() {
		return payScheduleSetup;
	}


	public void setPayScheduleSetup(PayScheduleSetup payScheduleSetup) {
		this.payScheduleSetup = payScheduleSetup;
	}


	public Department getDepartment() {
		return department;
	}


	public void setDepartment(Department department) {
		this.department = department;
	}


	public String getDepartmentDescription() {
		return departmentDescription;
	}


	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}


	public String getArabicDepartmentDescription() {
		return arabicDepartmentDescription;
	}


	public void setArabicDepartmentDescription(String arabicDepartmentDescription) {
		this.arabicDepartmentDescription = arabicDepartmentDescription;
	}


	public Boolean getPayScheduleStatus() {
		return payScheduleStatus;
	}


	public void setPayScheduleStatus(Boolean payScheduleStatus) {
		this.payScheduleStatus = payScheduleStatus;
	}
	
	

}
