package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.EmployeeContacts;

public class DtoEmployeeContacts extends DtoBase{

	
	
	private Integer id;
	private DtoEmployeeMaster employeeMaster;
	private Integer employeeDepedentSequence;
	private Integer employeeContactSequence;
	private String employeeContactName;
	private String employeeContactNameArabic;
	private String employeeContactNameRelationship;
	private String employeeContactHomePhone;
	private String employeeContactMobilePhone;
	private String employeeContactWorkPhone;
	private String employeeContactAddress;
	private String employeeContactWorkPhoneExt;
	private List<DtoEmployeeContacts> listContact;
	private boolean inActive;
	private List<DtoEmployeeContacts> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}
	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	public Integer getEmployeeDepedentSequence() {
		return employeeDepedentSequence;
	}
	public void setEmployeeDepedentSequence(Integer employeeDepedentSequence) {
		this.employeeDepedentSequence = employeeDepedentSequence;
	}
	public Integer getEmployeeContactSequence() {
		return employeeContactSequence;
	}
	public void setEmployeeContactSequence(Integer employeeContactSequence) {
		this.employeeContactSequence = employeeContactSequence;
	}
	public String getEmployeeContactName() {
		return employeeContactName;
	}
	public void setEmployeeContactName(String employeeContactName) {
		this.employeeContactName = employeeContactName;
	}
	public String getEmployeeContactNameArabic() {
		return employeeContactNameArabic;
	}
	public void setEmployeeContactNameArabic(String employeeContactNameArabic) {
		this.employeeContactNameArabic = employeeContactNameArabic;
	}
	public String getEmployeeContactNameRelationship() {
		return employeeContactNameRelationship;
	}
	public void setEmployeeContactNameRelationship(String employeeContactNameRelationship) {
		this.employeeContactNameRelationship = employeeContactNameRelationship;
	}
	public String getEmployeeContactHomePhone() {
		return employeeContactHomePhone;
	}
	public void setEmployeeContactHomePhone(String employeeContactHomePhone) {
		this.employeeContactHomePhone = employeeContactHomePhone;
	}
	public String getEmployeeContactMobilePhone() {
		return employeeContactMobilePhone;
	}
	public void setEmployeeContactMobilePhone(String employeeContactMobilePhone) {
		this.employeeContactMobilePhone = employeeContactMobilePhone;
	}
	public String getEmployeeContactWorkPhone() {
		return employeeContactWorkPhone;
	}
	public void setEmployeeContactWorkPhone(String employeeContactWorkPhone) {
		this.employeeContactWorkPhone = employeeContactWorkPhone;
	}
	public String getEmployeeContactAddress() {
		return employeeContactAddress;
	}
	public void setEmployeeContactAddress(String employeeContactAddress) {
		this.employeeContactAddress = employeeContactAddress;
	}
	public String getEmployeeContactWorkPhoneExt() {
		return employeeContactWorkPhoneExt;
	}
	public void setEmployeeContactWorkPhoneExt(String employeeContactWorkPhoneExt) {
		this.employeeContactWorkPhoneExt = employeeContactWorkPhoneExt;
	}
	public boolean isInActive() {
		return inActive;
	}
	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}
	public List<DtoEmployeeContacts> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoEmployeeContacts> delete) {
		this.delete = delete;
	}
	
	
	
	public List<DtoEmployeeContacts> getListContact() {
		return listContact;
	}
	public void setListContact(List<DtoEmployeeContacts> listContact) {
		this.listContact = listContact;
	}
	public DtoEmployeeContacts() {
	}
	
	public DtoEmployeeContacts(EmployeeContacts employeeContacts ) {
	}
	

}
