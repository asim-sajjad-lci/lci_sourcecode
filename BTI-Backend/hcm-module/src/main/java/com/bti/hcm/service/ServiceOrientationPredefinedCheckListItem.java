/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.OrientationCheckListSetup;
import com.bti.hcm.model.OrientationPredefinedCheckListItem;
import com.bti.hcm.model.dto.DtoOrientationCheckListSetup;
import com.bti.hcm.model.dto.DtoOrientationPredefinedCheckListItem;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryOrientationCheckListSetup;
import com.bti.hcm.repository.RepositoryOrientationPredefinedCheckListItem;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service OrientationPredefinedCheckListItem
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("serviceOrientationPredefinedCheckListItem")
public class ServiceOrientationPredefinedCheckListItem {
	
	
	/**
	 * @Description LOGGER use for put a logger in OrientationPredefinedCheckListItem Service
	 */
	static Logger log = Logger.getLogger(ServiceOrientationPredefinedCheckListItem.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in OrientationPredefinedCheckListItem service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in OrientationPredefinedCheckListItem service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in OrientationPredefinedCheckListItem service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryOrientationPredefinedCheckListItem Autowired here using annotation of spring for access of repositoryOrientationPredefinedCheckListItem method in OrientationPredefinedCheckListItem service
	 * 				In short Access OrientationPredefinedCheckListItem Query from Database using repositoryOrientationPredefinedCheckListItem.
	 */
	@Autowired
	RepositoryOrientationPredefinedCheckListItem repositoryOrientationPredefinedCheckListItem;
	
	@Autowired
	RepositoryOrientationCheckListSetup repositoryOrientationCheckListSetup;
	
	/**
	 * @Description: save and update OrientationPredefinedCheckListItem data
	 * @param dtoOrientationPredefinedCheckListItem
	 * @return
	 * @throws ParseException
	 */
	public DtoOrientationPredefinedCheckListItem saveOrUpdateOrientationPredefinedCheckListItem(DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem) throws ParseException {
		log.info("saveOrUpdateOrientationPredefinedCheckListItem Method");
		OrientationPredefinedCheckListItem orientationPredefinedCheckListItem = null;
		OrientationCheckListSetup orientationCheckListSetup =null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			
			if (dtoOrientationPredefinedCheckListItem.getId()>0) {
				orientationPredefinedCheckListItem = repositoryOrientationPredefinedCheckListItem.findByIdAndIsDeleted(dtoOrientationPredefinedCheckListItem.getId(), false);
				//orientationCheckListSetup = repositoryOrientationCheckListSetup.findByIdAndIsDeleted(dtoOrientationPredefinedCheckListItem.getOrientationPredefinedCheckListItemId(), false);
				
				orientationPredefinedCheckListItem.setUpdatedBy(loggedInUserId);
				orientationPredefinedCheckListItem.setUpdatedDate(new Date());
				Integer rowId = repositoryOrientationPredefinedCheckListItem.getCountOfTotaOrientationPredefinedCheckListItem();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				orientationPredefinedCheckListItem.setRowId(increment);
			} else {
				orientationPredefinedCheckListItem = new OrientationPredefinedCheckListItem();
				orientationPredefinedCheckListItem.setCreatedDate(new Date());
			}
			
			
			
			orientationPredefinedCheckListItem.setPreDefineCheckListItemDescription(dtoOrientationPredefinedCheckListItem.getPreDefineCheckListItemDescription());
			orientationPredefinedCheckListItem.setPreDefineCheckListItemDescriptionArabic(dtoOrientationPredefinedCheckListItem.getPreDefineCheckListItemDescriptionArabic());
			orientationPredefinedCheckListItem.setPreDefineCheckListType(dtoOrientationPredefinedCheckListItem.getPreDefineCheckListType());
			orientationPredefinedCheckListItem.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			
			orientationPredefinedCheckListItem = repositoryOrientationPredefinedCheckListItem.saveAndFlush(orientationPredefinedCheckListItem);
			
			if(orientationPredefinedCheckListItem.getListOrientationCheckListSetup()==null) {
				for (DtoOrientationCheckListSetup dtoOrientationCheckListSetup :dtoOrientationPredefinedCheckListItem.getSubItems()) {
					
					orientationCheckListSetup = new OrientationCheckListSetup();
					orientationCheckListSetup.setOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
					orientationCheckListSetup.setOrientationChecklistItemDescription(dtoOrientationCheckListSetup.getItemDesc());
					orientationCheckListSetup.setOrientationChecklistItemDescriptionArabic(dtoOrientationCheckListSetup.getItemDescArabic());
					orientationCheckListSetup.setCreatedDate(new Date());
					repositoryOrientationCheckListSetup.saveAndFlush(orientationCheckListSetup);
				}
			}else{
				
				/*List<OrientationCheckListSetup> checkListSetup=	repositoryOrientationCheckListSetup.findAllIds(orientationPredefinedCheckListItem.getId());*/
				List<Integer> ids = new ArrayList<>();
				for (DtoOrientationCheckListSetup dtoOrientationCheckListSetup : dtoOrientationPredefinedCheckListItem.getSubItems()) {
					
					if(dtoOrientationCheckListSetup.getId()!=null && dtoOrientationCheckListSetup.getId()>0) {
						ids.add(dtoOrientationCheckListSetup.getId());
					}
						if(dtoOrientationCheckListSetup.getId()!=null) {
							OrientationCheckListSetup orientationCheckListSetupUpdate = repositoryOrientationCheckListSetup.findOne(dtoOrientationCheckListSetup.getId());
							orientationCheckListSetupUpdate.setOrientationChecklistItemDescription(dtoOrientationCheckListSetup.getItemDesc());
							orientationCheckListSetupUpdate.setOrientationChecklistItemDescriptionArabic(dtoOrientationCheckListSetup.getItemDescArabic());
							orientationCheckListSetupUpdate.setUpdatedDate(new Date());
							orientationCheckListSetupUpdate.setUpdatedBy(loggedInUserId);
							orientationCheckListSetupUpdate.setIsDeleted(dtoOrientationCheckListSetup.isIsdeletedItems());
							orientationCheckListSetupUpdate.setOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
							repositoryOrientationCheckListSetup.saveAndFlush(orientationCheckListSetupUpdate);
						
						}else {
							OrientationCheckListSetup orientationCheckListSetupUpdate = new OrientationCheckListSetup();
							orientationCheckListSetupUpdate.setOrientationChecklistItemDescription(dtoOrientationCheckListSetup.getItemDesc());
							orientationCheckListSetupUpdate.setOrientationChecklistItemDescriptionArabic(dtoOrientationCheckListSetup.getItemDescArabic());
							orientationCheckListSetupUpdate.setUpdatedDate(new Date());
							orientationCheckListSetupUpdate.setUpdatedBy(loggedInUserId);
							orientationCheckListSetupUpdate.setCreatedDate(new Date());
							orientationCheckListSetupUpdate.setIsDeleted(dtoOrientationCheckListSetup.isIsdeletedItems());
							orientationCheckListSetupUpdate.setOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
							repositoryOrientationCheckListSetup.saveAndFlush(orientationCheckListSetupUpdate);
						}
						
				}
				/*if(!ids.isEmpty()) {
					repositoryOrientationCheckListSetup.findAllIdForDelete(orientationPredefinedCheckListItem.getId(),ids);
					List<Integer>  forDelete = repositoryOrientationCheckListSetup.findAllIdForDelete(orientationPredefinedCheckListItem.getId(),ids);
					repositoryOrientationCheckListSetup.deleteforAllId(forDelete);
				}*/
				
				
			}
			
			log.debug("OrientationPredefinedCheckListItem is:"+dtoOrientationPredefinedCheckListItem.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoOrientationPredefinedCheckListItem;

	}
	/**
	 * @Description: get All OrientationPredefinedCheckListItem
	 * @param dtoOrientationPredefinedCheckListItem
	 * @return
	 */
	public DtoSearch getAllOrientationPredefinedCheckListItem(DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem) {
		log.info("getAllOrientationPredefinedCheckListItem Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoOrientationPredefinedCheckListItem.getPageNumber());
			dtoSearch.setPageSize(dtoOrientationPredefinedCheckListItem.getPageSize());
			dtoSearch.setTotalCount(repositoryOrientationPredefinedCheckListItem.getCountOfTotalOrientationPredefinedCheckListItem());
			List<OrientationPredefinedCheckListItem> orientationPredefinedCheckListItemList = null;
			if (dtoOrientationPredefinedCheckListItem.getPageNumber() != null && dtoOrientationPredefinedCheckListItem.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoOrientationPredefinedCheckListItem.getPageNumber(), dtoOrientationPredefinedCheckListItem.getPageSize(), Direction.DESC, "createdDate");
				orientationPredefinedCheckListItemList = repositoryOrientationPredefinedCheckListItem.findByIsDeleted(false, pageable);
			} else {
				orientationPredefinedCheckListItemList = repositoryOrientationPredefinedCheckListItem.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			List<DtoOrientationPredefinedCheckListItem> dtoOrientationPredefinedCheckListItemList=new ArrayList<>();
			if(orientationPredefinedCheckListItemList!=null && orientationPredefinedCheckListItemList.isEmpty())
			{
				for (OrientationPredefinedCheckListItem orientationPredefinedCheckListItem : orientationPredefinedCheckListItemList) 
				{
					dtoOrientationPredefinedCheckListItem=new DtoOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
					dtoOrientationPredefinedCheckListItem.setPreDefineCheckListItemDescription(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescription());
					dtoOrientationPredefinedCheckListItem.setPreDefineCheckListItemDescriptionArabic(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescriptionArabic());
					dtoOrientationPredefinedCheckListItem.setPreDefineCheckListType(orientationPredefinedCheckListItem.getPreDefineCheckListType());
					dtoOrientationPredefinedCheckListItem.setId(orientationPredefinedCheckListItem.getId());
					dtoOrientationPredefinedCheckListItemList.add(dtoOrientationPredefinedCheckListItem);
				}
				dtoSearch.setRecords(dtoOrientationPredefinedCheckListItemList);
			}
			log.debug("All OrientationPredefinedCheckListItem List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	/**
	 * @Description: Search OrientationPredefinedCheckListItem data
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchOrientationPredefinedCheckListItem(DtoSearch dtoSearch) {
		try {
			log.info("searchOrientationPredefinedCheckListItem Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				String condition="";

				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "preDefineCheckListType" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "preDefineCheckListItemDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "preDefineCheckListItemDescriptionArabic" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
					
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositoryOrientationPredefinedCheckListItem.predictiveOrientationPredefinedCheckListItemSearchTotalCount("%"+searchWord+"%"));
				List<OrientationPredefinedCheckListItem> orientationPredefinedCheckListItemList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						orientationPredefinedCheckListItemList = this.repositoryOrientationPredefinedCheckListItem.predictiveOrientationPredefinedCheckListItemSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						orientationPredefinedCheckListItemList = this.repositoryOrientationPredefinedCheckListItem.predictiveOrientationPredefinedCheckListItemSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						orientationPredefinedCheckListItemList = this.repositoryOrientationPredefinedCheckListItem.predictiveOrientationPredefinedCheckListItemSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}
			
				if(orientationPredefinedCheckListItemList != null && !orientationPredefinedCheckListItemList.isEmpty()){
					List<DtoOrientationPredefinedCheckListItem> dtoOrientationPredefinedCheckListItemList = new ArrayList<>();
					DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem=null;
					for (OrientationPredefinedCheckListItem orientationPredefinedCheckListItem : orientationPredefinedCheckListItemList) {
						dtoOrientationPredefinedCheckListItem = new DtoOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
						
						List<OrientationCheckListSetup> list=orientationPredefinedCheckListItem.getListOrientationCheckListSetup();
						

						dtoOrientationPredefinedCheckListItem.setPreDefineCheckListItemDescription(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescription());
						dtoOrientationPredefinedCheckListItem.setPreDefineCheckListItemDescriptionArabic(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescriptionArabic());
						dtoOrientationPredefinedCheckListItem.setPreDefineCheckListType(orientationPredefinedCheckListItem.getPreDefineCheckListType());
						dtoOrientationPredefinedCheckListItem.setId(orientationPredefinedCheckListItem.getId());
						
						if(dtoOrientationPredefinedCheckListItem.getSubItems()==null) {
							List<DtoOrientationCheckListSetup> listOrientationCheckListSetup = new ArrayList<>();
							dtoOrientationPredefinedCheckListItem.setSubItems(listOrientationCheckListSetup);
						}
						for(OrientationCheckListSetup orientationCheckListSetup2:list) {
							if(orientationCheckListSetup2.getIsDeleted()==false) {
								DtoOrientationCheckListSetup dtoOrientationCheckListSetup = new DtoOrientationCheckListSetup();
								
								dtoOrientationCheckListSetup.setItemDesc(orientationCheckListSetup2.getOrientationChecklistItemDescriptionArabic());
								dtoOrientationCheckListSetup.setItemDescArabic(orientationCheckListSetup2.getOrientationChecklistItemDescriptionArabic());
								dtoOrientationCheckListSetup.setOrientationPredefinedCheckListItemId(orientationCheckListSetup2.getId());
								dtoOrientationCheckListSetup.setId(orientationCheckListSetup2.getId());
								dtoOrientationPredefinedCheckListItem.getSubItems().add(dtoOrientationCheckListSetup);
								
							}
							
						}
						dtoOrientationPredefinedCheckListItemList.add(dtoOrientationPredefinedCheckListItem);
						/*dtoOrientationPredefinedCheckListItemList.add(dtoOrientationPredefinedCheckListItem);*/
					}
					dtoSearch.setRecords(dtoOrientationPredefinedCheckListItemList);
				}
			}
			log.debug("Search OrientationPredefinedCheckListItem Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description: get OrientationPredefinedCheckListItem data by id
	 * @param id
	 * @return
	 */
	public DtoOrientationPredefinedCheckListItem getDtoOrientationPredefinedCheckListItemById(int id) {
		log.info("getDtoOrientationPredefinedCheckListItemById Method");
		DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem = new DtoOrientationPredefinedCheckListItem();
		try {
			List<DtoOrientationCheckListSetup> listOrientationCheckListSetup = new ArrayList<>();
			if (id > 0) {
				OrientationPredefinedCheckListItem orientationPredefinedCheckListItem = repositoryOrientationPredefinedCheckListItem.findByIdAndIsDeleted(id, false);
				if (orientationPredefinedCheckListItem != null) {
					dtoOrientationPredefinedCheckListItem = new DtoOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
					List<OrientationCheckListSetup> orientationCheckListSetupList = orientationPredefinedCheckListItem.getListOrientationCheckListSetup();
					if(!orientationCheckListSetupList.isEmpty()) {
						for(OrientationCheckListSetup orientationCheckListSetup2:orientationCheckListSetupList) {
							DtoOrientationCheckListSetup dtoOrientationCheckListSetup = new DtoOrientationCheckListSetup();
							
							dtoOrientationCheckListSetup.setItemDesc(orientationCheckListSetup2.getOrientationChecklistItemDescriptionArabic());
							dtoOrientationCheckListSetup.setItemDescArabic(orientationCheckListSetup2.getOrientationChecklistItemDescriptionArabic());
							dtoOrientationCheckListSetup.setOrientationPredefinedCheckListItemId(orientationCheckListSetup2.getId());
							dtoOrientationCheckListSetup.setId(orientationCheckListSetup2.getId());
							listOrientationCheckListSetup.add(dtoOrientationCheckListSetup);
						}
					}
					
					dtoOrientationPredefinedCheckListItem.setSubItems(listOrientationCheckListSetup);
				} else {
					dtoOrientationPredefinedCheckListItem.setMessageType("ORIENTATION_PREDEFINED_CHECKLIST_ITEM_NOT_GETTING");

				}
			} else {
				dtoOrientationPredefinedCheckListItem.setMessageType("INVALID_ORIENTATION_PREDEFINED_CHECKLIST_ITEM_ID");

			}
			log.debug("OrientationPredefinedCheckListItem By Id is:"+dtoOrientationPredefinedCheckListItem.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoOrientationPredefinedCheckListItem;
	}
	/**
	 * @Description: delete OrientationPredefinedCheckListItem
	 * @param ids
	 * @return
	 */
	public DtoOrientationPredefinedCheckListItem deleteOrientationPredefinedCheckListItem(List<Integer> ids) {
		log.info("deleteOrientationPredefinedCheckListItem Method");
		DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem = new DtoOrientationPredefinedCheckListItem();
		dtoOrientationPredefinedCheckListItem
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ORIENTATION_PREDEFINED_CHECKLIST_ITEM_DELETED", false));
		List<DtoOrientationPredefinedCheckListItem> deleteOrientationPredefinedCheckListItemList = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer id : ids) {
				OrientationPredefinedCheckListItem orientationPredefinedCheckListItem = repositoryOrientationPredefinedCheckListItem.findOne(id);
					DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem2 = new DtoOrientationPredefinedCheckListItem();
					if(orientationPredefinedCheckListItem.getListOrientationCheckListSetup()!=null) {
						for(OrientationCheckListSetup checkListSetup : orientationPredefinedCheckListItem.getListOrientationCheckListSetup()) {
							dtoOrientationPredefinedCheckListItem2.setPreDefineCheckListItemDescription(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescription());
							dtoOrientationPredefinedCheckListItem2.setPreDefineCheckListItemDescriptionArabic(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescriptionArabic());
							dtoOrientationPredefinedCheckListItem2.setPreDefineCheckListType(orientationPredefinedCheckListItem.getPreDefineCheckListType());
							dtoOrientationPredefinedCheckListItem2.setId(orientationPredefinedCheckListItem.getId());
							repositoryOrientationCheckListSetup.deleteSingleOrientationCheckListSetup(true, loggedInUserId, checkListSetup.getId());
							repositoryOrientationPredefinedCheckListItem.deleteSingleOrientationPredefinedCheckListItem(true, loggedInUserId, id);
						}
					}
					
					deleteOrientationPredefinedCheckListItemList.add(dtoOrientationPredefinedCheckListItem2);

					
			}
			dtoOrientationPredefinedCheckListItem.setDeleteOrientationPredefinedCheckListItem(deleteOrientationPredefinedCheckListItemList);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete OrientationPredefinedCheckListItem :"+dtoOrientationPredefinedCheckListItem.getId());
		return dtoOrientationPredefinedCheckListItem;
	}
	/**
	 * @Description: check Repeat id
	 * @param id
	 * @return
	 */
	public DtoOrientationPredefinedCheckListItem repeatById(int id) {
		log.info("repeatByOrientationPredefinedCheckListItemId Method");
		DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem = new DtoOrientationPredefinedCheckListItem();
		try {
			List<OrientationPredefinedCheckListItem> orientationPredefinedCheckListItem=repositoryOrientationPredefinedCheckListItem.findById(id);
			if(orientationPredefinedCheckListItem!=null && !orientationPredefinedCheckListItem.isEmpty()) {
				dtoOrientationPredefinedCheckListItem.setIsRepeat(true);
			}else {
				dtoOrientationPredefinedCheckListItem.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoOrientationPredefinedCheckListItem;
	}
	
	public List<DtoOrientationPredefinedCheckListItem> getAllForDroupDown() {
		log.info("repeatByOrientationPredefinedCheckListItemId Method");
		DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem = new DtoOrientationPredefinedCheckListItem();
		List<DtoOrientationPredefinedCheckListItem> dtoOrientationPredefinedCheckListItemList =new ArrayList<>();
		try {
			List<OrientationPredefinedCheckListItem> orientationPredefinedCheckListItem=repositoryOrientationPredefinedCheckListItem.findByIsDeleted(false);
			if(orientationPredefinedCheckListItem!=null && !orientationPredefinedCheckListItem.isEmpty()) {
				for (OrientationPredefinedCheckListItem orientationPredefinedCheckListItem2 : orientationPredefinedCheckListItem) {
					dtoOrientationPredefinedCheckListItem.setPreDefineCheckListItemDescription(orientationPredefinedCheckListItem2.getPreDefineCheckListItemDescription());
					dtoOrientationPredefinedCheckListItem.setPreDefineCheckListItemDescriptionArabic(orientationPredefinedCheckListItem2.getPreDefineCheckListItemDescriptionArabic());
					dtoOrientationPredefinedCheckListItem.setPreDefineCheckListType(orientationPredefinedCheckListItem2.getPreDefineCheckListType());
					dtoOrientationPredefinedCheckListItem.setId(orientationPredefinedCheckListItem2.getId());
					dtoOrientationPredefinedCheckListItemList.add(dtoOrientationPredefinedCheckListItem);
				}
				
			}
		} catch (Exception e) {
			
			log.error(e);
		}
		return dtoOrientationPredefinedCheckListItemList;
	}
	public DtoSearch getAllList(DtoSearch dtoSearch) {
		try {
			log.info("searchOrientationPredefinedCheckListItem Method");
				List<OrientationPredefinedCheckListItem> orientationPredefinedCheckListItemList =null;
					short termination = 1;
						orientationPredefinedCheckListItemList = this.repositoryOrientationPredefinedCheckListItem.getAllOriation(termination);
				if(orientationPredefinedCheckListItemList != null && !orientationPredefinedCheckListItemList.isEmpty()){
					List<DtoOrientationPredefinedCheckListItem> dtoOrientationPredefinedCheckListItemList = new ArrayList<>();
					DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem=null;
					for (OrientationPredefinedCheckListItem orientationPredefinedCheckListItem : orientationPredefinedCheckListItemList) {
						if(orientationPredefinedCheckListItem.getIsDeleted() != true) {
							dtoOrientationPredefinedCheckListItem = new DtoOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
							dtoOrientationPredefinedCheckListItem.setPreDefineCheckListItemDescription(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescription());
							dtoOrientationPredefinedCheckListItem.setPreDefineCheckListItemDescriptionArabic(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescriptionArabic());
							dtoOrientationPredefinedCheckListItem.setPreDefineCheckListType(orientationPredefinedCheckListItem.getPreDefineCheckListType());
							dtoOrientationPredefinedCheckListItem.setId(orientationPredefinedCheckListItem.getId());
							dtoOrientationPredefinedCheckListItemList.add(dtoOrientationPredefinedCheckListItem);
						}
					dtoSearch.setRecords(dtoOrientationPredefinedCheckListItemList);
				}
			}
			log.debug("Search OrientationPredefinedCheckListItem Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;

	}

}
