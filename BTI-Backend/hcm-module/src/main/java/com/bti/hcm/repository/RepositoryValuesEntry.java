package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Miscellaneous;
import com.bti.hcm.model.Values;

@Repository("repositoryValuesEntry")
public interface RepositoryValuesEntry extends JpaRepository<Values,Integer> {

	/**
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Values d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleValuesEntry(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Values d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteAllValuesEntries(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	
	/**
	 * @param deleted
	 * @return
	 */
	public List<Values> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	
	@Query("select count(*) from Values d ")
	public Integer getCountOfTotalValuesEntries();
	
	
	@Query("select p from Values p where (p.id =:id) and p.isDeleted=false")
	public List<Values> findByValuesId(@Param("id")String miscId);
	
	
	
	@Query("select d from Values d where (d.id like :searchKeyWord) and d.isDeleted=false")
	public List<Miscellaneous> predictiveValuesAllIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	
}
