package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.RetirementFundSetup;

@Repository("repositoryRetirementFundSetup")
public interface RepositoryRetirementFundSetup  extends JpaRepository<RetirementFundSetup, Integer>{

	RetirementFundSetup findByIdAndIsDeleted(Integer id, boolean b);

	RetirementFundSetup saveAndFlush(RetirementFundSetup retirementFundSetup);

	RetirementFundSetup findOne(Integer planId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update RetirementFundSetup r set r.isDeleted =:deleted ,r.updatedBy =:updateById where r.id =:id ")
	public void deleteSingleRetirementFundSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from RetirementFundSetup r where (r.fundId LIKE :searchKeyWord or r.planDescription LIKE :searchKeyWord) and r.isDeleted=false")
	Integer predictiveRetirementFundSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select r from RetirementFundSetup r where (r.fundId LIKE :searchKeyWord or r.planDescription LIKE :searchKeyWord) and r.isDeleted=false")
	List<RetirementFundSetup> predictiveRetirementFundSetupSearchWithPagination(@Param("searchKeyWord")String searchKeyWord, Pageable pageable);
	
	@Query("select r from RetirementFundSetup r where (r.planId =:planId) and r.isDeleted=false")
	List<RetirementFundSetup> findByRetirementPlanId(@Param("planId") Integer planId);

	@Query("select count(*) from RetirementFundSetup r ")
	public Integer getCountOfTotaRetirementFundSetup();

}
