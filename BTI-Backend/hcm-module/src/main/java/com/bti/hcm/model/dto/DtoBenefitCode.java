/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO BenefitCode class having getter and setter for fields (POJO)
 * Name Name of Project: BTI Created on: Feg 15, 2018
 * 
 * @author seasia Version:
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoBenefitCode extends DtoBase {

	private Integer id;
	private String benefitId;
	private String desc;
	private String arbicDesc;
	private Date startDate;
	private Date endDate;
	private Boolean transction;
	private Short method;
	private BigDecimal amount;
	private BigDecimal percent;
	private BigDecimal perPeriod;
	private BigDecimal perYear;
	private BigDecimal lifeTime;
	private Short frequency;
	private Boolean inActive;
	private List<DtoBenefitCode> deleteBenefitCode;
	private short codeType;
	private List<DtoPayCode> dtoPayCode;
	private BigDecimal payFactor;
	private boolean isCustomDate;
	private Integer noOfDays;
	private Integer endDateDays;

	private Integer roundOf; // Me
	private Integer benefitTypeId; // Me

	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getRoundOf() {
		return roundOf;
	}

	public void setRoundOf(Integer roundOf) {
		this.roundOf = roundOf;
	}

	public Integer getBenefitTypeId() {
		return benefitTypeId;
	}

	public void setBenefitTypeId(Integer benefitTypeId) {
		this.benefitTypeId = benefitTypeId;
	}

	public String getBenefitId() {
		return benefitId;
	}

	public void setBenefitId(String benefitId) {
		this.benefitId = benefitId;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getTransction() {
		return transction;
	}

	public void setTransction(Boolean transction) {
		this.transction = transction;
	}

	public Short getMethod() {
		return method;
	}

	public void setMethod(Short method) {
		this.method = method;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	public BigDecimal getPerPeriod() {
		return perPeriod;
	}

	public void setPerPeriod(BigDecimal perPeriod) {
		this.perPeriod = perPeriod;
	}

	public BigDecimal getPerYear() {
		return perYear;
	}

	public void setPerYear(BigDecimal perYear) {
		this.perYear = perYear;
	}

	public BigDecimal getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(BigDecimal lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Short getFrequency() {
		return frequency;
	}

	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}

	public Boolean getInActive() {
		return inActive;
	}

	public void setInActive(Boolean inActive) {
		this.inActive = inActive;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DtoBenefitCode() {

	}

	public DtoBenefitCode(BenefitCode benefitCode) {
		this.benefitId = benefitCode.getBenefitId();

		if (UtilRandomKey.isNotBlank(benefitCode.getDesc())) {
			this.desc = benefitCode.getDesc();
		} else {
			this.desc = "";
		}

	}

	public List<DtoBenefitCode> getDeleteBenefitCode() {
		return deleteBenefitCode;
	}

	public void setDeleteBenefitCode(List<DtoBenefitCode> deleteBenefitCode) {
		this.deleteBenefitCode = deleteBenefitCode;
	}

	public List<DtoPayCode> getDtoPayCode() {
		return dtoPayCode;
	}

	public void setDtoPayCode(List<DtoPayCode> dtoPayCode) {
		this.dtoPayCode = dtoPayCode;
	}

	public short getCodeType() {
		return codeType;
	}

	public void setCodeType(short codeType) {
		this.codeType = codeType;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getEndDateDays() {
		return endDateDays;
	}

	public void setEndDateDays(Integer endDateDays) {
		this.endDateDays = endDateDays;
	}

	public boolean isCustomDate() {
		return isCustomDate;
	}

	public void setCustomDate(boolean isCustomDate) {
		this.isCustomDate = isCustomDate;
	}

}
