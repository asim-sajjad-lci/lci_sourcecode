package com.bti.hcm.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.PayrollTransectionHistoryByEmployee;
import com.bti.hcm.model.dto.DtoPayrollTransectionHistoryByEmployee;
import com.bti.hcm.repository.RepositoryPayrollTransectionHistoryByEmployee;

@Service("/payrollTransectionHistoryByEmployee")
public class ServicePayrollTransectionHistoryByEmployee {

	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServicePayrollTransectionHistoryByEmployee.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryPayrollTransectionHistoryByEmployee repositoryPayrollTransectionHistoryByEmployee;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	
	public DtoPayrollTransectionHistoryByEmployee saveOrUpdate(DtoPayrollTransectionHistoryByEmployee dtoPayrollTransectionHistoryByEmployee) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		PayrollTransectionHistoryByEmployee payrollTransectionHistoryByEmployee=null;
		if(dtoPayrollTransectionHistoryByEmployee.getId()!=null && dtoPayrollTransectionHistoryByEmployee.getId()>0) {
			payrollTransectionHistoryByEmployee=repositoryPayrollTransectionHistoryByEmployee.findByIdAndIsDeleted(dtoPayrollTransectionHistoryByEmployee.getId(),false);
			payrollTransectionHistoryByEmployee.setUpdatedBy(loggedInUserId);
			payrollTransectionHistoryByEmployee.setUpdatedDate(new Date());
		}else {
			payrollTransectionHistoryByEmployee = new PayrollTransectionHistoryByEmployee();
			payrollTransectionHistoryByEmployee.setCreatedDate(new Date());
			Integer rowId = repositoryPayrollTransectionHistoryByEmployee.findAll().size();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			payrollTransectionHistoryByEmployee.setRowId(increment);
		}
		payrollTransectionHistoryByEmployee.setCodeIndexId(dtoPayrollTransectionHistoryByEmployee.getCodeIndexId());
		payrollTransectionHistoryByEmployee.setYear(dtoPayrollTransectionHistoryByEmployee.getYear());
		payrollTransectionHistoryByEmployee.setCodeType(dtoPayrollTransectionHistoryByEmployee.getCodeType());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod1(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod1());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod2(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod2());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod3(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod3());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod4(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod4());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod5(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod5());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod6(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod6());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod7(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod7());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod8(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod8());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod9(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod9());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod10(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod11());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod11(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod12());
		payrollTransectionHistoryByEmployee.setTotalAmountPeriod12(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod1());
		repositoryPayrollTransectionHistoryByEmployee.saveAndFlush(payrollTransectionHistoryByEmployee);
		return dtoPayrollTransectionHistoryByEmployee;
	}

}
