package com.bti.hcm.model.dto;

import java.util.Date;

public class DtoEmployeeInfo {

	private String employeeCode;
	private String employeeName;
	private String employeeNameArabic;
	private String site;
	private String siteArabic;
	private String position;
	private String positionArabic;
	private Date joiningDate;
	private String nationality;
	private boolean citizen;
	private String department;
	private short employeeMaritalStatus;
	private String employeeId;
	
	
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Date getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}
	public String getEmployeeNameArabic() {
		return employeeNameArabic;
	}
	public void setEmployeeNameArabic(String employeeNameArabic) {
		this.employeeNameArabic = employeeNameArabic;
	}
	public String getSiteArabic() {
		return siteArabic;
	}
	public void setSiteArabic(String siteArabic) {
		this.siteArabic = siteArabic;
	}
	public String getPositionArabic() {
		return positionArabic;
	}
	public void setPositionArabic(String positionArabic) {
		this.positionArabic = positionArabic;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationalities) {
		this.nationality = nationalities;
	}
	public boolean isCitizen() {
		return citizen;
	}
	public void setCitizen(boolean citizen) {
		this.citizen = citizen;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public short getEmployeeMaritalStatus() {
		return employeeMaritalStatus;
	}
	public void setEmployeeMaritalStatus(short employeeMaritalStatus) {
		this.employeeMaritalStatus = employeeMaritalStatus;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	
	
}
