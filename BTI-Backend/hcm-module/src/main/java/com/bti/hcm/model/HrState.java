/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the state_master database table.
 * Name of Project: BTI
 * Created on: June 20, 2017
 * Modified on: June 20, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */ 
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "state_master",indexes = {
        @Index(columnList = "state_id")
})
@NamedQuery(name = "HrState.findAll", query = "SELECT s FROM HrState s")
public class HrState extends HcmBaseEntity2 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "state_id")
	private int stateId;

	@Column(name = "state_name")
	private String stateName;
	
	@Column(name = "state_code")
	private String stateCode;

	/*// bi-directional many-to-one association to CityMaster
	@OneToMany(mappedBy = "stateMaster")
	private List<HrCity> cityMasters;*/


	// bi-directional many-to-one association to CountryMaster
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "country_id")
	private HrCountry countryMaster;


	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="lang_id")
	private Language language;

	public HrState() {
		// TODO Auto-generated constructor stub
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public HrCountry getCountryMaster() {
		return countryMaster;
	}

	public void setCountryMaster(HrCountry countryMaster) {
		this.countryMaster = countryMaster;
	}
	
	

}