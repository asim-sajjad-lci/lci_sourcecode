/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.HijriYear;
import com.bti.hcm.model.dto.DtoHijriYear;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryHijriYear;

/**
 * Description: Interface for Hijri Year 
 * Name of Project:Hcm 
 * Version: 0.0.1
 * Created on: February 12, 2018
 * 
 * @author Bansi
 *
 */
@Service("serviceHijriYear")
public class ServiceHijriYear {

	/**
	 * @Description LOGGER use for put a logger in HijriYear Service
	 */
	static Logger logger = Logger.getLogger(ServiceHijriMonth.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in HijriYear service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in HijriYear service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description Autowired spring annotation to access repositoryHijriDay method
	 *              in HijriDay service In short, access HijriYear Query from
	 *              Database using repositoryHijriDay.
	 */
	@Autowired
	RepositoryHijriYear repositoryHijriYear;

	public DtoSearch getAllHijriYear() {
		logger.info("getAllHijriMonth Method called!!");
		DtoHijriYear dtoHijriYear = null;
		DtoSearch dtoSearch = new DtoSearch();
		List<HijriYear> hijriYearList = repositoryHijriYear.findAll();
		List<DtoHijriYear> dtoHijriYearList = new ArrayList<>();
		if (hijriYearList != null && hijriYearList.size() > 0) {
			for (HijriYear hijriYear : hijriYearList) {
				dtoHijriYear = new DtoHijriYear();
				dtoHijriYear.setYear(hijriYear.getYear());
				dtoHijriYear.setId(hijriYear.getId());
				dtoHijriYearList.add(dtoHijriYear);
			}
			dtoSearch.setRecords(dtoHijriYearList);
		}
		return dtoSearch;
	}

}
