/*package com.bti.hcm.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/workflow")
public class ControllerWorkFlow extends BaseConroller{

	private static final Logger logger = Logger.getLogger(ControllerWorkFlow.class);
	
	
	/*
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
    
    @Autowired
	ServiceWorkFlow serviceWorkFlow;
    
    
    @Autowired
	RepositoryWorkFlow repositoryWorkFlow;
    
	
	@RequestMapping(value = "/create", method = RequestMethod.POST,consumes = {"multipart/form-data"})
	public ResponseMessage createEmployeeMaster(HttpServletRequest request,@RequestParam ("DtoWorkFlow")String   dtoWorkFlow, @RequestParam(name = "file",required = false) MultipartFile file) throws Exception{
		logger.info("Workflow file upload Info");
		UserSession session = sessionManager.validateUserSessionId(request);
		ObjectMapper objectMapper = new ObjectMapper();
		DtoWorkFlow dtWorkFlowObject = objectMapper.readValue(dtoWorkFlow, DtoWorkFlow.class);
		ResponseMessage responseMessage = null;
		
		dtWorkFlowObject = serviceWorkFlow.saveOrUpdate(dtWorkFlowObject,file);
			//responseMessage=displayMessage(dtWorkFlowObject, "EMPLOYEE_CREATED", "EMPLOYEE_NOT_CREATED", serviceResponse);
		
		logger.debug("Workflow file upload successfully Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
    @RequestMapping(value = "/tasks", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Task> getTasks(@RequestBody DtoWorkFlow data) {
		return taskService.createTaskQuery().taskAssignee(data.getFileDesc()).list();
	}
    
    
    
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/create", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public void addNewBorn(@RequestBody DtoWorkFlow data) {

    	WorkFlowUploadFile workFlowUploadFile = new WorkFlowUploadFile(data.getWorFlowId(),data.getWorkFlowFile());
    	repositoryWorkFlow.save(workFlowUploadFile);
        Map<String, Object> vars = Collections.<String, Object>singletonMap("applicant", workFlowUploadFile);
        runtimeService.startProcessInstanceByKey("myProcess", vars);
    }
    
    
   
	@RequestMapping(value = "/create", method = RequestMethod.POST,consumes = {"multipart/form-data"})
	public ResponseMessage createEmployeeMaster(HttpServletRequest request,@RequestParam ("DtoWorkFlow")String   dtoWorkFlow, @RequestParam(name = "file",required = false) MultipartFile file) throws Exception{
		logger.info("Create Workflow file upload successfully Method");
		ObjectMapper objectMapper = new ObjectMapper();
		DtoWorkFlow dtWorkFlowObject = objectMapper.readValue(dtoWorkFlow, DtoWorkFlow.class);
		ResponseMessage responseMessage = null;
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtWorkFlowObject = serviceWorkFlow.saveOrUpdate(dtWorkFlowObject,file);
				responseMessage = displayMessage(dtWorkFlowObject,"EMPLOYEE_CREATED","EMPLOYEE_NOT_CREATED",serviceResponse);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			logger.debug("Create Workflow file upload successfully Method:"+responseMessage.getMessage());
		    return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		logger.info("Search Workflow file upload Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceWorkFlow.getAll(dtoSearch);
			responseMessage=displayMessage(dtoSearch,"WORK_FLOW__GETTING","WORK_FLOW_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoWorkFlow> dtoWorkFlowList = serviceWorkFlow.getAllDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("WORK_FLOW__GETTING", false), dtoWorkFlowList);
			}else {
				responseMessage =unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			logger.error(e);
		}
		return responseMessage;
	}
	
}
*/