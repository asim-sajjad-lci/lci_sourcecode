/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Bank;

/**
 * Description: Interface for Bank 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryBank")

public interface RepositoryBank extends JpaRepository<Bank, Integer>{
	
	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	public Bank findByIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * @param deleted
	 * @return
	 */
	public List<Bank> findByIsDeleted(Boolean deleted);

	/**
	 * @return
	 */
	@Query("select count(*) from Bank b where b.isDeleted=false")
	public Integer getCountOfTotalBank();
	
	
	/**
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<Bank> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Bank b set b.isDeleted =:deleted, b.updatedBy=:updateById where b.id IN (:idList)")
	public void deleteBanks(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Bank b set b.isDeleted =:deleted ,b.updatedBy =:updateById where b.id =:id ")
	public void deleteSingleBank(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select b from Bank b where (b.bankName like :searchKeyWord  or b.bankDescription like :searchKeyWord or b.bankDescriptionArabic like :searchKeyWord) and b.isDeleted=false")
	public List<Bank> predictiveBankSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<Bank> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from Bank b where (b.bankName like :searchKeyWord  or b.bankDescription like :searchKeyWord  or b.bankDescriptionArabic like :searchKeyWord) and b.isDeleted=false")
	public Integer predictiveBankSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select b from Bank b where (b.bankName like :searchKeyWord  or b.bankDescription like  :searchKeyWord  or b.bankDescriptionArabic like :searchKeyWord) and b.isDeleted=false")
	public List<Bank> predictiveBankSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select b from Bank b where (b.id =:id) and b.isDeleted=false")
	public List<Bank> findByBankId(@Param("id")String id);
	
	
	@Query("select b.id from Bank b where (b.bankName like :searchKeyWord) and b.isDeleted=false order by b.id desc")
	public List<String> predictiveBankIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select b from Bank b where (b.bankName like :searchKeyWord) and b.isDeleted=false")
	public List<Bank> predictiveBankAllIdsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select count(*) from Bank b ")
	public Integer getCountOfTotalBanks();
	

	@Query("select count(*) from Bank d")
	public Integer getCountOfTotalBankWithOutDelete();
	
	 //@Cacheable("bank")
	@Query("select b from Bank b where id in(:list) and b.isDeleted=false")
	public List<Bank> findAllBankListId(@Param("list") List<Integer> ls);
	
	@Query("select b from Bank b where b.id =:id and b.isDeleted=false")
	public Bank findByBankIdOne(@Param("id") String ls);
}
