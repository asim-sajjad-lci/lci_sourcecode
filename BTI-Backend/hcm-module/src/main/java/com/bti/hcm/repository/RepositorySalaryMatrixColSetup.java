package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.SalaryMatrixColSetup;

@Repository("repositorySalaryMatrixColSetup")
public interface RepositorySalaryMatrixColSetup extends JpaRepository<SalaryMatrixColSetup, Integer>{



	/**
	 * 
	 * @param id
	 * @param b
	 * @return
	 */
	SalaryMatrixColSetup findByColSalaryMatrixIndexIdAndIsDeleted(Integer id, boolean b);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from SalaryMatrixColSetup s where s.isDeleted=false")
	Integer getCountOfTotalSalaryMatrixColSetup();

	/**
	 * 
	 * @param b
	 * @param pageable
	 * @return
	 */
	List<SalaryMatrixColSetup> findByIsDeleted(boolean b, Pageable pageable);

	/**
	 * 
	 * @param b
	 * @return
	 */
	List<SalaryMatrixColSetup> findByIsDeletedOrderByCreatedDateDesc(boolean b);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from SalaryMatrixColSetup d where (d.salaryMatrixSetup.salaryMatrixId like :searchKeyWord  or d.colSalaryMatrixDescription like :searchKeyWord or d.arabicColSalaryMatrixDescription like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveSalaryMatrixColSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select d from SalaryMatrixColSetup d where (d.salaryMatrixSetup.salaryMatrixId like :searchKeyWord  or d.colSalaryMatrixDescription like :searchKeyWord or d.arabicColSalaryMatrixDescription like :searchKeyWord) and d.isDeleted=false")
	List<SalaryMatrixColSetup> predictiveSalaryMatrixColSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select d from SalaryMatrixColSetup d where (d.salaryMatrixSetup.salaryMatrixId like :searchKeyWord  or d.colSalaryMatrixDescription like :searchKeyWord or d.arabicColSalaryMatrixDescription like :searchKeyWord) and d.isDeleted=false")
	List<SalaryMatrixColSetup> predictiveSalaryMatrixColSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update SalaryMatrixColSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleSalaryMatrixColSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	/**
	 * 
	 * @param colSalaryMatrixIndexId
	 * @return
	 */
	@Query("select s from SalaryMatrixColSetup s where (s.colSalaryMatrixIndexId =:colSalaryMatrixIndexId) and s.isDeleted=false")
	List<SalaryMatrixColSetup> findBySalaryMatrixColSetupColSalaryMatrixIndexId(@Param("colSalaryMatrixIndexId")Integer colSalaryMatrixIndexId);


	@Query("select s from SalaryMatrixColSetup s where (s.salaryMatrixSetup.id =:id)")
	public List<SalaryMatrixColSetup> findBySalaryMatrixId(@Param("id")Integer id);

	@Query("select count(*) from SalaryMatrixColSetup r ")
	public Integer getCountOfTotaSalaryMatrixColSetup();

	@Query("select count(*) from SalaryMatrixColSetup r ")
	public Integer getCountOfTotaSalaryMatrixColSetups();





















}
