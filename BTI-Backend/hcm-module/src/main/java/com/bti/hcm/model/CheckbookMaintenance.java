/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Description: The persistent class for the PriceGroupSetup database table.
 * Name of Project: Ims 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "GL00200_h",indexes = {
        @Index(columnList = "CHEKBOKINX")
})
public class CheckbookMaintenance extends HcmBaseEntity implements Serializable {
	
	/**
	 * Price Group Setup
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CHEKBOKINX")
	private Integer id;
	
	@Column(name = "CHEKBOKID",columnDefinition = "char(15)")
	private String checkbookId;

	@Column(name = "DSCRIPTN",columnDefinition = "char(31)")
	private String checkbookDescription;
	 
	@Column(name = "DSCRIPTNA",columnDefinition = "char(61)")
	private String checkbookArabicDescription;

	@Column(name = "BANKID",columnDefinition = "char(15)")
	private String bankIdAddress;
	
	@Column(name = "ACTROWID",columnDefinition = "char(15)")
	private long accountTableRowIndex;
	
	@Column(name = "NXTCHKNUM")
	private Integer nextCheckNumber;
	
	@Column(name = "NXTDEPNUM")
	private Integer nextDepositNumber;
	
	@Column(name = "INACTIVE")
	private Boolean inActive;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getCheckbookDescription() {
		return checkbookDescription;
	}

	public void setCheckbookDescription(String checkbookDescription) {
		this.checkbookDescription = checkbookDescription;
	}

	public String getCheckbookArabicDescription() {
		return checkbookArabicDescription;
	}

	public void setCheckbookArabicDescription(String checkbookArabicDescription) {
		this.checkbookArabicDescription = checkbookArabicDescription;
	}

	public String getBankIdAddress() {
		return bankIdAddress;
	}

	public void setBankIdAddress(String bankIdAddress) {
		this.bankIdAddress = bankIdAddress;
	}

	public long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Integer getNextCheckNumber() {
		return nextCheckNumber;
	}

	public void setNextCheckNumber(Integer nextCheckNumber) {
		this.nextCheckNumber = nextCheckNumber;
	}

	public Integer getNextDepositNumber() {
		return nextDepositNumber;
	}

	public void setNextDepositNumber(Integer nextDepositNumber) {
		this.nextDepositNumber = nextDepositNumber;
	}

	public Boolean getInActive() {
		return inActive;
	}

	public void setInActive(Boolean inActive) {
		this.inActive = inActive;
	}

	

		
}
