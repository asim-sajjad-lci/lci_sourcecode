package com.bti.hcm.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoPayRoleReport {

	private List<DtoDepartment> departmentList;
}
