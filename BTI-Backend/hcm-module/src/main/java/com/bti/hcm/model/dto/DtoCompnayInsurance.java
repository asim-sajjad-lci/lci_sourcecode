package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.CompnayInsurance;
import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.util.UtilRandomKey;

public class DtoCompnayInsurance extends DtoBase{
	private Integer id;
	private String insCompanyId;
	private String desc;
	private String arbicDesc;
	private String address;
	private HrCity city;
	private HrState state;
	private HrCountry country;
	
	private String phoneNo;
	private String contactPerson;
	private List<DtoCompnayInsurance> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getInsCompanyId() {
		return insCompanyId;
	}
	public void setInsCompanyId(String insCompanyId) {
		this.insCompanyId = insCompanyId;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getArbicDesc() {
		return arbicDesc;
	}
	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public HrCity getCity() {
		return city;
	}
	public void setCity(HrCity city) {
		this.city = city;
	}
	public HrState getState() {
		return state;
	}
	public void setState(HrState state) {
		this.state = state;
	}
	public HrCountry getCountry() {
		return country;
	}
	public void setCountry(HrCountry country) {
		this.country = country;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public List<DtoCompnayInsurance> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoCompnayInsurance> delete) {
		this.delete = delete;
	}
	public DtoCompnayInsurance() {
	
	}
	
	public DtoCompnayInsurance(CompnayInsurance helthInsurance) {
		this.insCompanyId = helthInsurance.getInsCompanyId();
		
		if (UtilRandomKey.isNotBlank(helthInsurance.getDesc())) {
			this.desc = helthInsurance.getDesc();
		} else {
			this.desc = "";
		}
		
		if (UtilRandomKey.isNotBlank(helthInsurance.getArbicDesc())) {
			this.arbicDesc = helthInsurance.getArbicDesc();
		} else {
			this.arbicDesc = "";
		}
		
		if (UtilRandomKey.isNotBlank(helthInsurance.getAddress())) {
			this.address = helthInsurance.getAddress();
		} else {
			this.address = "";
		}
		
		if (UtilRandomKey.isNotBlank(helthInsurance.getPhoneNo())) {
			this.phoneNo = helthInsurance.getPhoneNo();
		} else {
			this.phoneNo = "";
		}
		if (UtilRandomKey.isNotBlank(helthInsurance.getContactPerson())) {
			this.contactPerson = helthInsurance.getContactPerson();
		} else {
			this.contactPerson = "";
		}
	}
}
