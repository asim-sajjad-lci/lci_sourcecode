package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.EmployeeAddressMaster;

public class DtoEmployeeAddressMaster extends DtoBase{

	private Integer employeeAddressIndexId;
	private String addressId;
	private String address1;
	private String address2;
	private String personalEmail;
	private String businessEmail;
	private String addressArabic1;
	private String addressArabic2;
	private String pOBox;
	private int postCode;
	private String personalPhone;
	private String businessPhone;
	private String otherPhone;
	private String ext;
	private String locationLinkGoogleMap;

	private Integer cityId;
	private Integer stateId;
	private Integer countryId;

	private String cityName;
	private String stateName;
	private String countryName;
	
	private DtoHrCountry dtoHrCountry;
	private DtoCity dtoCity;
	private DtoHrState dtoHrState;

	private List<DtoEmployeeAddressMaster> delete;

	public Integer getEmployeeAddressIndexId() {
		return employeeAddressIndexId;
	}

	public void setEmployeeAddressIndexId(Integer employeeAddressIndexId) {
		this.employeeAddressIndexId = employeeAddressIndexId;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getBusinessEmail() {
		return businessEmail;
	}

	public void setBusinessEmail(String businessEmail) {
		this.businessEmail = businessEmail;
	}

	public String getAddressArabic1() {
		return addressArabic1;
	}

	public void setAddressArabic1(String addressArabic1) {
		this.addressArabic1 = addressArabic1;
	}

	public String getAddressArabic2() {
		return addressArabic2;
	}

	public void setAddressArabic2(String addressArabic2) {
		this.addressArabic2 = addressArabic2;
	}

	public String getpOBox() {
		return pOBox;
	}

	public void setpOBox(String pOBox) {
		this.pOBox = pOBox;
	}

	public int getPostCode() {
		return postCode;
	}

	public void setPostCode(int postCode) {
		this.postCode = postCode;
	}

	public String getPersonalPhone() {
		return personalPhone;
	}

	public void setPersonalPhone(String personalPhone) {
		this.personalPhone = personalPhone;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getLocationLinkGoogleMap() {
		return locationLinkGoogleMap;
	}

	public void setLocationLinkGoogleMap(String locationLinkGoogleMap) {
		this.locationLinkGoogleMap = locationLinkGoogleMap;
	}


	public List<DtoEmployeeAddressMaster> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeeAddressMaster> delete) {
		this.delete = delete;
	}


	public DtoEmployeeAddressMaster() {
	}

	public DtoEmployeeAddressMaster(EmployeeAddressMaster employeeAddressMaster) {
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public DtoHrCountry getDtoHrCountry() {
		return dtoHrCountry;
	}

	public void setDtoHrCountry(DtoHrCountry dtoHrCountry) {
		this.dtoHrCountry = dtoHrCountry;
	}

	public DtoCity getDtoCity() {
		return dtoCity;
	}

	public void setDtoCity(DtoCity dtoCity) {
		this.dtoCity = dtoCity;
	}

	public DtoHrState getDtoHrState() {
		return dtoHrState;
	}

	public void setDtoHrState(DtoHrState dtoHrState) {
		this.dtoHrState = dtoHrState;
	}

}
