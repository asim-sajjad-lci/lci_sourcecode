package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeAddressMaster;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeAddressMaster;
import com.bti.hcm.service.ServiceEmployeeNationalities;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeAddressMaster")	
public class ControllerEmployeeAddressMaster extends BaseController{
	
	

	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeAddressMaster.class);
	
	@Autowired
	ServiceEmployeeNationalities serviceEmployeeNationalities;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	@Autowired
	ServiceEmployeeAddressMaster serviceEmployeeAddressMaster;
	
	@RequestMapping(value = "/getAllEmployeeAddressMasterDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllEmployeeAddressMasterDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeeAddressMaster> dtoEmployeeAddressMasterList = serviceEmployeeAddressMaster.getAllEmployeeAddressMasterDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_ADDRESS_MASTER_GET_ALL", false), dtoEmployeeAddressMasterList);
			} else {
				responseMessage =unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
		
			LOGGER.error(e);
		}

		return responseMessage;
	}
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoEmployeeAddressMaster dtoEmployeeAddressMaster) throws Exception{
		LOGGER.info("Create TimeCode Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeAddressMaster = serviceEmployeeAddressMaster.saveOrUpdate(dtoEmployeeAddressMaster);
			responseMessage=displayMessage(dtoEmployeeAddressMaster, "EMPLOYEE_ADDRESS_MASTER_GET_CREATED", "EMPLOYEE_ADDRESS_MASTER_GET_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create TimeCode Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeeAddressMaster dtoEmployeeAddressMaster) throws Exception {
		LOGGER.info("Update TimeCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeAddressMaster = serviceEmployeeAddressMaster.saveOrUpdate(dtoEmployeeAddressMaster);
			responseMessage=displayMessage(dtoEmployeeAddressMaster, "EMPLOYEE_ADDRESS_MASTER_GET_UPDATED_SUCCESS", "EMPLOYEE_ADDRESS_MASTER_GET_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update TimeCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoEmployeeAddressMaster dtoEmployeeAddressMaster) throws Exception {
		LOGGER.info("Delete EmployeeAddressMaster Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeAddressMaster.getIds() != null && !dtoEmployeeAddressMaster.getIds().isEmpty()) {
				DtoEmployeeAddressMaster dtoEmployeeAddressMaster1 = serviceEmployeeAddressMaster.delete(dtoEmployeeAddressMaster.getIds());
				if(dtoEmployeeAddressMaster1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_ADDRESS_MASTER_GET_DELETED", false), dtoEmployeeAddressMaster1);

				}else {				
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_ADDRESS_MASTER_GET_NOT_DELETE_ID_MESSAGE", false), dtoEmployeeAddressMaster1);

				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeeAddressMaster Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoEmployeeAddressMaster dtoEmployeeAddressMaster) throws Exception {
		LOGGER.info("Get EmployeeAddressMaster ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeAddressMaster dtoEmployeeAddressMasterObj = serviceEmployeeAddressMaster.getById(dtoEmployeeAddressMaster.getEmployeeAddressIndexId());
			responseMessage=displayMessage(dtoEmployeeAddressMasterObj, "EMPLOYEE_ADDRESS_MASTER_GET_GET_DETAIL", "EMPLOYEE_ADDRESS_MASTER_GET_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get EmployeeAddressMaster by Id Method:"+dtoEmployeeAddressMaster.getEmployeeAddressIndexId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/addressIdcheck", method = RequestMethod.POST)
	public ResponseMessage payCodeIdcheck(HttpServletRequest request, @RequestBody DtoEmployeeAddressMaster dtoEmployeeAddressMaster) throws Exception {
		LOGGER.info("EmployeeAddressMasterIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeAddressMaster dtoEmployeeAddressMasterObj = serviceEmployeeAddressMaster.repeatByAddressId(dtoEmployeeAddressMaster.getAddressId());
		    responseMessage=displayMessage(dtoEmployeeAddressMasterObj, "EMPLOYEE_ADDRESS_MASTER_GET_REPEAT_FOUND", "EMPLOYEE_ADDRESS_MASTER_GET_REPEAT_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	

	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search search Time code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeAddressMaster.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_ADDRESS_MASTER_GET_GET_DETAIL", "EMPLOYEE_ADDRESS_MASTER_GET_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

}

