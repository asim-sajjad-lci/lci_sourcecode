/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the PayScheduleSetupPosition database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40925",indexes = {
        @Index(columnList = "PYSCHDINDXP")
})
@NamedQuery(name = "PayScheduleSetupPosition.findAll", query = "SELECT p FROM PayScheduleSetupPosition p")
public class PayScheduleSetupPosition extends HcmBaseEntity implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PYSCHDINDXP")
	private int paySchedulePositionIndexId;
	
	
	@Column(name = "POTDSCR",columnDefinition="char(31)")
	private String positionDescription;
	

	@Column(name = "POTDSCRA",columnDefinition="char(61)")
	private String positionDescriptionArabic;


	@Column(name = "PYSCHDACT")
	private Boolean payScheduleStatus;
	
	
	@ManyToOne
	@JoinColumn(name="PYSCHDINDX")
	private PayScheduleSetup payScheduleSetup;
	
	@ManyToOne
	@JoinColumn(name="POTINDX")
	private Position position;


	public int getPaySchedulePositionIndexId() {
		return paySchedulePositionIndexId;
	}


	public void setPaySchedulePositionIndexId(int paySchedulePositionIndexId) {
		this.paySchedulePositionIndexId = paySchedulePositionIndexId;
	}


	public String getPositionDescription() {
		return positionDescription;
	}


	public void setPositionDescription(String positionDescription) {
		this.positionDescription = positionDescription;
	}


	public String getPositionDescriptionArabic() {
		return positionDescriptionArabic;
	}


	public void setPositionDescriptionArabic(String positionDescriptionArabic) {
		this.positionDescriptionArabic = positionDescriptionArabic;
	}


	public Boolean getPayScheduleStatus() {
		return payScheduleStatus;
	}


	public void setPayScheduleStatus(Boolean payScheduleStatus) {
		this.payScheduleStatus = payScheduleStatus;
	}


	public PayScheduleSetup getPayScheduleSetup() {
		return payScheduleSetup;
	}


	public void setPayScheduleSetup(PayScheduleSetup payScheduleSetup) {
		this.payScheduleSetup = payScheduleSetup;
	}


	public Position getPosition() {
		return position;
	}


	public void setPosition(Position position) {
		this.position = position;
	}
	
	
	
}


