/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.PositionClass;
import com.bti.hcm.model.dto.DtoPositionClass;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPositionClass;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service PositionClass
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("servicePositon")
public class ServicePositionClass {

	/**
	 * @Description LOGGER use for put a logger in servicePositon Service
	 */
	static Logger log = Logger.getLogger(ServicePositionClass.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in servicePositon service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in servicePositon service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in servicePositon service
	 */
	@Autowired
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPosition Autowired here using annotation of spring for access of repositoryPosition method in servicePositon service
	 * 				In short Access Position Query from Database using repositoryPosition.
	 */
	@Autowired
	RepositoryPositionClass repositoryPositionClass;





	/**
	 * @Description: save and update Postion data
	 * @param dtoPositionClass
	 * @return
	 * @throws ParseException
	 */
	public DtoPositionClass saveOrUpdatePosition(DtoPositionClass dtoPositionClass){
		try {
			log.info("saveOrUpdatePosition Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			PositionClass positionClass = null;
			if (dtoPositionClass.getId() != null && dtoPositionClass.getId() > 0) {
				positionClass = repositoryPositionClass.findByIdAndIsDeleted(dtoPositionClass.getId(), false);
				positionClass.setUpdatedBy(loggedInUserId);
				positionClass.setUpdatedDate(new Date());
			} else {
				positionClass = new PositionClass();
				positionClass.setCreatedDate(new Date());
				
				Integer rowId = repositoryPositionClass.getCountOfTotaPositionClass();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				positionClass.setRowId(increment);
			}
			positionClass.setDescription(dtoPositionClass.getPositionClassDescription());
			positionClass.setPositionClassId(dtoPositionClass.getPositionClassId());
			positionClass.setArabicDescription(dtoPositionClass.getArabicPositionClassDescription());
			positionClass.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryPositionClass.saveAndFlush(positionClass);
			log.debug("Position is:"+dtoPositionClass.getId());
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPositionClass;

	}

	/**
	 * @Description: get All PosiitonClass
	 * @param dtoPositionClass
	 * @return
	 */
	public DtoSearch getAllPosition() {
		log.info("getAllPosition Method");
		DtoPositionClass dtoPositionClass = new DtoPositionClass();
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setTotalCount(repositoryPositionClass.getCountOfTotalPostionClass());
			List<PositionClass> positionList = null;
			
				positionList = repositoryPositionClass.findByIsDeleted(false);
			
			
			List<DtoPositionClass> dtoPositionList=new ArrayList<>();
			if(positionList!=null && !positionList.isEmpty())
			{
				for (PositionClass positionClass : positionList) 
				{
					dtoPositionClass=new DtoPositionClass(positionClass);
					dtoPositionClass.setPositionClassId(positionClass.getPositionClassId());
					dtoPositionClass.setId(positionClass.getId());
					dtoPositionList.add(dtoPositionClass);
				}
				dtoSearch.setRecords(dtoPositionList);
			}
			log.debug("All Position List Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description: Search PositonClass data
	 * @param dtoSearch
	 * @return
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPosition(DtoSearch dtoSearch) {
		log.info("searchPosition Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				
				if(dtoSearch.getSortOn().equals("positionClassId") || dtoSearch.getSortOn().equals("description") || dtoSearch.getSortOn().equals("arabicDescription")) {
					condition=dtoSearch.getSortOn();
				}else {
					condition="id";
				}
				
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}			
			dtoSearch.setTotalCount(this.repositoryPositionClass.predictivePositionClassSearchTotalCount("%"+searchWord+"%"));
			List<PositionClass> positionList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					positionList = this.repositoryPositionClass.predictivePositionClassSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					positionList =  this.repositoryPositionClass.predictivePositionClassSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					positionList =  this.repositoryPositionClass.predictivePositionClassSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			
		
			if(positionList != null && !positionList.isEmpty()){
				List<DtoPositionClass> dtoPositionList = new ArrayList<>();
				DtoPositionClass dtoPositionClass=null;
				for (PositionClass positionClass : positionList) {
					dtoPositionClass = new DtoPositionClass(positionClass);
					dtoPositionClass.setArabicPositionClassDescription(positionClass.getArabicDescription());
					dtoPositionClass.setId(positionClass.getId());
					dtoPositionClass.setPositionClassDescription(positionClass.getDescription());
					dtoPositionList.add(dtoPositionClass);
				}
				dtoSearch.setRecords(dtoPositionList);
			}
		}
		return dtoSearch;
	}

	/**
	 * @Description: get PositonClass data by id
	 * @param id
	 * @return
	 */
	public DtoPositionClass getPositionByPositionId(int id) {
		log.info("getPositionByPositionId Method");
		DtoPositionClass dtoPositionClass = new DtoPositionClass();
		if (id > 0) {
			PositionClass positionClass = repositoryPositionClass.findByIdAndIsDeleted(id, false);
			if (positionClass != null) {
				dtoPositionClass = new DtoPositionClass(positionClass);
				dtoPositionClass.setArabicPositionClassDescription(positionClass.getArabicDescription());
				dtoPositionClass.setId(positionClass.getId());
				dtoPositionClass.setPositionClassDescription(positionClass.getDescription());
				dtoPositionClass.setArabicPositionClassDescription(positionClass.getArabicDescription());
			} else {
				dtoPositionClass.setMessageType("POSITION_CLASS_NOT_GETTING");

			}
		} else {
			dtoPositionClass.setMessageType("INVALID_POSITION_CLASS_ID");

		}
		log.debug("position By Id is:"+dtoPositionClass.getId());
		return dtoPositionClass;
	}


	/**
	 * @Description: delete Position Class
	 * @param ids
	 * @return
	 */
	
	 public DtoPositionClass deletePositionClass(List<Integer> ids) {
		log.info("deletePositionClass Method");
		DtoPositionClass dtoPositionClass = new DtoPositionClass();
		dtoPositionClass
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("POSITION_CLASS_DELETED", false));
		dtoPositionClass.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("POSITION_CLASS_ASSOCIATED", false));
		List<DtoPositionClass> deletePositionClass = new ArrayList<>();
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("POSITION_CLASS_NOT_DELETE_ID_MESSAGE", false).getMessage());
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer positionClassId : ids) {
				PositionClass positionClass = repositoryPositionClass.findOne(positionClassId);
				if(positionClass.getListPositionSetup().isEmpty() && positionClass.getPosition().isEmpty()){
					
					DtoPositionClass dtoPositionClass2 = new DtoPositionClass();
					dtoPositionClass2.setId(positionClassId);
					dtoPositionClass2.setPositionClassDescription(positionClass.getDescription());
					repositoryPositionClass.deleteSinglePositionClasss(true, loggedInUserId, positionClassId);
					deletePositionClass.add(dtoPositionClass2);
				}else{
					inValidDelete = true;
					//invlidDeleteMessage.append(positionClass.getPositionClassId()+","); 
				}
				

			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoPositionClass.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoPositionClass.setMessageType("");
				
			}
			dtoPositionClass.setDeletePosition(deletePositionClass);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Position Class :"+dtoPositionClass.getId());
		return dtoPositionClass;
	}
	/**
	 * @Description: check Repeat PositionClassId
	 * @param positionClassId
	 * @return
	 */
	
	public DtoPositionClass repeatByPositinClassId(String positionClassId) {
		log.info("repeatByPositinClassId Method");
		DtoPositionClass dtoPositionClass = new DtoPositionClass();
		try {
			List<PositionClass> positionClass=repositoryPositionClass.findByPositionClassId(positionClassId.trim());
			if(positionClass!=null && !positionClass.isEmpty()) {
				dtoPositionClass.setIsRepeat(true);
			}else {
				dtoPositionClass.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPositionClass;
	}

	public DtoSearch searchPositionClassId(DtoSearch dtoSearch) {
		log.info("searchPositionClassId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			
			List<String> positionClassIdList = this.repositoryPositionClass.predictivePositionClassIdSearchWithPagination("%"+searchWord+"%");
				if(!positionClassIdList.isEmpty()) {
					dtoSearch.setTotalCount(positionClassIdList.size());
					dtoSearch.setRecords(positionClassIdList.size());
				}
			dtoSearch.setIds(positionClassIdList);
		}
		
		return dtoSearch;
	}

	
	
	
	
	
	public DtoSearch searchAllPositionClassIds(DtoSearch dtoSearch) {
		log.info("searchAllPositionClassIds Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			List<DtoPositionClass> dtoPositionClass = new ArrayList<>();
			List<PositionClass> positionClassList = this.repositoryPositionClass.predictivesearchAllPositionClassIdsWithPagination("%"+searchWord+"%");
				if(!positionClassList.isEmpty()) {
						for (PositionClass positionClass : positionClassList) {
							DtoPositionClass dtoPositionClass1=new DtoPositionClass(positionClass);
							dtoPositionClass1.setId(positionClass.getId());
							dtoPositionClass1.setPositionClassId(positionClass.getPositionClassId());
							dtoPositionClass1.setPositionClassDescription(positionClass.getPositionClassId()+" | "+positionClass.getDescription());
							dtoPositionClass1.setArabicPositionClassDescription(positionClass.getArabicDescription());
							dtoPositionClass.add(dtoPositionClass1);
						}
						dtoSearch.setRecords(dtoPositionClass);
					dtoSearch.setTotalCount(positionClassList.size());
				}
			
		}
		
		return dtoSearch;
	}


}
