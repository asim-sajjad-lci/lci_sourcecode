package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTimeCode;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTimeCode;

@RestController
@RequestMapping("/timeCode")
public class ControllerTimeCode {
	@Autowired
	ServiceTimeCode serviceTimeCode;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerSkillSteup.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @param request{
		  "timeCodeId":"1",
		  "desc":"1",
		  "arbicDesc":"1",
		  "accrualPeriod":1,"timeType":1
		}
		@param response
		{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"timeCodeId": "1",
		"inActive": false,
		"desc": "1",
		"arbicDesc": "1",
		"accrualPeriod": 1,
		"timeType": 1,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Time Code created successfully.",
		"messageShort": "TIME_CODE_CREATED"
		}
		}
	 * @param dtoTimeCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoTimeCode dtoTimeCode) throws Exception{
		LOGGER.info("Create TimeCode Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTimeCode = serviceTimeCode.saveOrUpdate(dtoTimeCode);
			if (dtoTimeCode != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("TIME_CODE_CREATED", false), dtoTimeCode);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_CREATED", false), dtoTimeCode);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSIONS_EXPIRED", false));
		}
		LOGGER.debug("Create TimeCode Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	/**
	 * @param request{
		  "id":1,
		  "timeCodeId":"1",
		  "desc":"1",
		  "arbicDesc":"1",
		  "accrualPeriod":1,"timeType":1
		}
		@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": 1,
		"timeCodeId": "1",
		"inActive": false,
		"desc": "1",
		"arbicDesc": "1",
		"accrualPeriod": 1,
		"timeType": 1,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Time Code  updated successfully.",
		"messageShort": "TIME_CODE_UPDATED_SUCCESS"
		}
		}
	 * @param dtoTimeCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoTimeCode dtoTimeCode) throws Exception {
		LOGGER.info("Update TimeCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTimeCode = serviceTimeCode.saveOrUpdate(dtoTimeCode);
			if (dtoTimeCode != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("TIME_CODE_UPDATED_SUCCESS", false), dtoTimeCode);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_UPDATED", false), dtoTimeCode);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Update TimeCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
		  
				 "ids":[1]
		}
		
		@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"timeCodeId": null,
		"inActive": false,
		"desc": null,
		"arbicDesc": null,
		"accrualPeriod": null,
		"timeType": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": "Position deleted successfully.",
		"associateMessage": "Position has been assigned.",
		"delete": [
		  {
		"id": 1,
		"timeCodeId": null,
		"inActive": false,
		"desc": null,
		"arbicDesc": null,
		"accrualPeriod": null,
		"timeType": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		}
		],
		"isRepeat": null
		},
		"btiMessage": {
		"message": "N/A",
		"messageShort": "N/A"
		}
		}
	 * @param dtoTimeCode
	 * @return
	 * @throws Exception
	 */
	
	/*
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoTimeCode dtoTimeCode) throws Exception {
		LOGGER.info("Delete TimeCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTimeCode.getIds() != null && dtoTimeCode.getIds().isEmpty()) {
				DtoTimeCode dtoPosition1 = serviceTimeCode.delete(dtoTimeCode.getIds());
				if(dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("TIME_CODE_DELETED", false), dtoPosition1);

				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoPosition1.getMessageType());
					btiMessageHcm.setMessageShort("TIME_CODE_NOT_DELETE_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoPosition1);

				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Delete TimeCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	*/
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoTimeCode dtoTimeCode) throws Exception {
		LOGGER.info("Delete DeductionCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTimeCode.getIds() != null && !dtoTimeCode.getIds().isEmpty()) {
				DtoTimeCode dtoTimeCode2 = serviceTimeCode.delete(dtoTimeCode.getIds());
				
				if(dtoTimeCode2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("TIME_CODE_DELETED", false), dtoTimeCode2);	
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false), dtoTimeCode2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete DeductionCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	/**
	 * @param request{
	  "id" : 1
	}
	@param response{
				
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"timeCodeId": "1",
		"inActive": false,
		"desc": null,
		"arbicDesc": null,
		"accrualPeriod": null,
		"timeType": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Position fetched successfully.",
		"messageShort": "TIME_CODE_GET_DETAIL"
		
		}
		}
	 * @param dtoTimeCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoTimeCode dtoTimeCode) throws Exception {
		LOGGER.info("Get TimeCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTimeCode dtoPayCodeObj = serviceTimeCode.getById(dtoTimeCode.getId());
			if (dtoPayCodeObj != null) {
				if (dtoPayCodeObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("TIME_CODE_GET_DETAIL", false), dtoPayCodeObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted(dtoPayCodeObj.getMessageType(), false),
							dtoPayCodeObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_GETTING", false), dtoPayCodeObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Get TimeCode by Id Method:"+dtoTimeCode.getId());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  "payCodeId" : "1"
	}
	@param request{
	"code": 302,
	"status": "FOUND",
	"result": {
	"id": null,
	"payCodeId": null,
	"description": null,
	"arbicDescription": null,
	"payType": null,
	"baseOnPayCode": null,
	"baseOnPayCodeAmount": null,
	"payFactor": null,
	"payRate": null,
	"unitofPay": null,
	"payperiod": null,
	"inActive": false,
	"pageNumber": null,
	"pageSize": null,
	"ids": null,
	"isActive": null,
	"messageType": null,
	"message": null,
	"deleteMessage": null,
	"associateMessage": null,
	"delete": null,
	"isRepeat": true
	},
	"btiMessage": {
	"message": "time Code Id is already created.",
	"messageShort": "TIME_CODE_DETAIL_FATECHED"
	}
	}
	 * @param dtoTimeCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeCodeIdcheck", method = RequestMethod.POST)
	public ResponseMessage payCodeIdcheck(HttpServletRequest request, @RequestBody DtoTimeCode dtoTimeCode) throws Exception {
		LOGGER.info("timeCodeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTimeCode dtoPayCodeObj = serviceTimeCode.repeatByTimeCodeId(dtoTimeCode.getTimeCodeId());
			if (dtoPayCodeObj != null) {
				if (dtoPayCodeObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("TIME_CODE_REPEAT_FOUND", false), dtoPayCodeObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted(dtoPayCodeObj.getMessageType(), false),
							dtoPayCodeObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("TIME_CODE_REPEAT_DEPARTMENTID_NOT_FOUND", false), dtoPayCodeObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{
		  "sortOn" : "timeCodeId",
		  "sortBy" : "DESC",
		 "searchKeyword" : "",
		"pageNumber":"0",
		"pageSize":"10"
		}
		 @param response{
			"code": 200,
			"status": "OK",
			"result": {
			"searchKeyword": "",
			"pageNumber": 0,
			"pageSize": 10,
			"sortOn": "timeCodeId",
			"sortBy": "DESC",
			"totalCount": 1,
			"records": [
			  {
			"id": 1,
			"timeCodeId": "1",
			"inActive": false,
			"desc": "1",
			"arbicDesc": "1",
			"accrualPeriod": 1,
			"timeType": 1,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"delete": null,
			"isRepeat": null
			}
			],
			},
			"btiMessage": {
			"message": "time code list fetched successfully.",
			"messageShort": "Time_CODE_GET_ALL"
			}
			}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search search Time code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTimeCode.search(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.response.getMessageByShortAndIsDeleted("TIME_CODE_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						response.getMessageByShortAndIsDeleted("TIME_CODE_LIST_NOT_GETTING", false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Times code Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{
			 "searchKeyword" : ""
	}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchTimeCodeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchTimeCodeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Time Code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTimeCode.searchTimeCodeId(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.response.getMessageByShortAndIsDeleted("TIME_CODE_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						response.getMessageByShortAndIsDeleted("TIME_CODE_LIST_NOT_GETTING", false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Time code Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllTimeCodeInActive", method = RequestMethod.GET)
	public ResponseMessage getAllTimeCodeInActive(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag == true) {
				List<DtoTimeCode> timeCodeList = serviceTimeCode.getAllTimeCodeInActiveList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("POSITION_PLAN_GET_ALL", false), timeCodeList);
			}else {
				responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
						response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
			}
			
		} catch (Exception e) {
			
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/searchAllTimeCodeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAllTimeCodeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("searchAllTimeCodeId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag == true) {
			dtoSearch = this.serviceTimeCode.searchAllTimeCodeId(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.response.getMessageByShortAndIsDeleted("TIME_CODE_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						response.getMessageByShortAndIsDeleted("TIME_CODE_LIST_NOT_GETTING", false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Time code Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	
}
