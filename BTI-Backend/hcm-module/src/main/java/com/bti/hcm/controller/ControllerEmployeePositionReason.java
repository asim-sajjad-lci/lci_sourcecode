package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeePositionReason;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/controllerEmployeePositionReason")
public class ControllerEmployeePositionReason extends BaseController{

	@Autowired
	ServiceEmployeePositionReason serviceEmployeePositionReason;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeContacts.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeContacts Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePositionReason.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_CONTACTS_GET_DETAIL", "EMPLOYEE_CONTACTS_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllForDroupDown", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeContacts Method");
		DtoSearch dtoSearch = new DtoSearch();
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePositionReason.getAllForDroupDown(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_CONTACTS_GET_DETAIL", "EMPLOYEE_CONTACTS_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PositionReason Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllPositionReason", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllPositionReason(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search PositionReason Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePositionReason.getAllPositionReason(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_POSITION_RESITION_GET_ALL", "EMPLOYEE_POSITION_RESITION_LIST_NOT_GETTING", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PositionReason Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	
}
