package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00204",indexes = {
        @Index(columnList = "EMPBINDX")
})
public class EmployeeBenefitMaintenance extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPBINDX")
	private Integer id;

	@Column(name = "BENCSTDT")
	private Date startDate;

	@Column(name = "BENCEDDT")
	private Date endDate;

	@Column(name = "BENCTRXRQ")
	private boolean transactionRequired;
	
	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private EmployeeMaster employeeMaster;

	@ManyToOne
	@JoinColumn(name = "BENCINDX")
	@Where(clause = "BENCINCTV = false and is_deleted = false")
	private BenefitCode benefitCode;

	
	@Column(name = "BENCMETHD")
	private Short benefitMethod;

	
	@Column(name = "BENCAMT", precision = 10, scale = 3)
	private BigDecimal benefitAmount;

	@Column(name = "DEDCPERT", precision = 10, scale = 5)
	private BigDecimal benefitPercent;

	@Column(name = "BENCMXPERD", precision = 10, scale = 3)
	private BigDecimal perPeriord;

	@Column(name = "BENCMXYERL", precision = 10, scale = 3)
	private BigDecimal perYear;

	@Column(name = "BENCMXLIFE", precision = 10, scale = 3)
	private BigDecimal lifeTime;

	@Column(name = "BENCFREQN")
	private Short frequency;

	@Column(name = "BENCINCTV")
	private Boolean inactive;
	
	@Column(name = "PYCDFACTR", precision = 15, scale = 6)
	private BigDecimal payFactor;

	@Column(name="BENNOOFDAYS")
	private Integer noOfDays;
	
	@Column(name="BENENDDAYS")
	private Integer endDateDays;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isTransactionRequired() {
		return transactionRequired;
	}

	public void setTransactionRequired(boolean transactionRequired) {
		this.transactionRequired = transactionRequired;
	}

	public Short getBenefitMethod() {
		return benefitMethod;
	}

	public void setBenefitMethod(Short benefitMethod) {
		this.benefitMethod = benefitMethod;
	}

	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public BigDecimal getBenefitPercent() {
		return benefitPercent;
	}

	public void setBenefitPercent(BigDecimal benefitPercent) {
		this.benefitPercent = benefitPercent;
	}

	public BigDecimal getPerPeriord() {
		return perPeriord;
	}

	public void setPerPeriord(BigDecimal perPeriord) {
		this.perPeriord = perPeriord;
	}

	public BigDecimal getPerYear() {
		return perYear;
	}

	public void setPerYear(BigDecimal perYear) {
		this.perYear = perYear;
	}

	public BigDecimal getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(BigDecimal lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Short getFrequency() {
		return frequency;
	}

	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}

	public Boolean getInactive() {
		return inactive;
	}

	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}

	

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public BenefitCode getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(BenefitCode benefitCode) {
		this.benefitCode = benefitCode;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getEndDateDays() {
		return endDateDays;
	}

	public void setEndDateDays(Integer endDateDays) {
		this.endDateDays = endDateDays;
	}
	
	

}
