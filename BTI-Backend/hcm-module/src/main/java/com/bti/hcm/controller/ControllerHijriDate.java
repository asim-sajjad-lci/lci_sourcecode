package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoGetMonthById;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceHijriMonth;
import com.bti.hcm.service.ServiceHijriYear;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerHijriDate
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 * 
 * @author Bansi
 *
 */
@RestController
@RequestMapping("/hijriDate")
public class ControllerHijriDate {
	
	/**
	 * @Description LOGGER use for put a logger in Hijri date Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerHijriDate.class);
	
	/**
	 * @Description Autowired spring annotation to access ServiceHijriMonth method in this controller.
	 */
	@Autowired
	ServiceHijriMonth serviceHijriMonth;
	
	/**
	 * @Description Autowired spring annotation to access ServiceHijriYear method in this controller.
	 */
	@Autowired
	ServiceHijriYear serviceHijriYear;
	
	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @description : Get Hijri Years
	 * @param request
	 * @return
	 * @throws Exception 
	 * @response {
     *	"code": 201,
    	"status": "CREATED",
    	"result": {
        	"records": [
            	{
                	"id": 1,
                	"year": 1445
            	}
        	]
    	},
    	"btiMessage": {
        	"message": "Hijri years fetched successfully",
        	"messageShort": "HIJRI_YEARS_GET_ALL"
    	}
	}
	*/
	@RequestMapping(value = "/getAllYear", method = RequestMethod.GET)
	public ResponseMessage getAllHijriYear(HttpServletRequest request) throws Exception {
		LOGGER.info("Get All Hijri Year Method called");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceHijriYear.getAllHijriYear();
			if (dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("HIJRI_YEAR_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("HIJRI_YEAR_LIST_NOT_GETTING", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Get All Hijri Year Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getMonthById", method = RequestMethod.POST)
	public ResponseMessage getMonthById(HttpServletRequest request,@RequestBody DtoGetMonthById dtoGetMonthById) throws Exception {
		LOGGER.info("get Month By Id");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoGetMonthById dtoSearch = serviceHijriMonth.getMonthById(dtoGetMonthById.getId());
			if (dtoSearch != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("HIJRI_YEAR_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("HIJRI_YEAR_LIST_NOT_GETTING", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Get All Hijri Year Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
}
