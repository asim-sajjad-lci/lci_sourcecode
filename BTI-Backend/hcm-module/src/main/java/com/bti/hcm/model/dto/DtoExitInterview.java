package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.ExitInterview;
import com.bti.hcm.util.UtilRandomKey;

public class DtoExitInterview extends DtoBase {
	private Integer id;
	private String exitInterviewId;
	private Integer sequence;
	private String desc;
	private String arbicDesc;
	private boolean inActive;
	private List<DtoExitInterview> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExitInterviewId() {
		return exitInterviewId;
	}

	public void setExitInterviewId(String exitInterviewId) {
		this.exitInterviewId = exitInterviewId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public List<DtoExitInterview> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoExitInterview> delete) {
		this.delete = delete;
	}

	public DtoExitInterview() {
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public DtoExitInterview(ExitInterview exitInterview) {
		this.exitInterviewId = exitInterview.getExitInterviewId();

		if (UtilRandomKey.isNotBlank(exitInterview.getDesc())) {
			this.desc = exitInterview.getDesc();
		} else {
			this.desc = "";
		}

		if (UtilRandomKey.isNotBlank(exitInterview.getArbicDesc())) {
			this.arbicDesc = exitInterview.getArbicDesc();
		} else {
			this.arbicDesc = "";
		}
	}
}
