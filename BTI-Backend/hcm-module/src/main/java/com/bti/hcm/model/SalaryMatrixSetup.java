package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40904",indexes = {
        @Index(columnList = "SMTXINDX")
})
@NamedQuery(name = "SalaryMatrixSetup.findAll", query = "SELECT s FROM SalaryMatrixSetup s")
public class SalaryMatrixSetup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SMTXINDX")
	private Integer id;

	@Column(name = "SMTXID", columnDefinition = "char(15)")
	private String salaryMatrixId;

	@Column(name = "SMTXDSCR", columnDefinition = "char(31)")
	private String description;

	@Column(name = "SMTXDSCRA", columnDefinition = "char(61)")
	private String arabicSalaryMatrixDescription;

	@Column(name = "SMTXPYUNT")
	private Short payUnit;

	@Column(name = "SMTXROW")
	private int totalRow;

	@Column(name = "SMTXCOL")
	private int totalColumns;

	@Column(name = "SMTXTROW")
	private int totalRowValues;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "salaryMatrixSetup")
	private List<SalaryMatrixColSetup> salaryMatrixColSetup;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "salaryMatrixSetup")
	private List<SalaryMatrixRow> salaryMatrixRow;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSalaryMatrixId() {
		return salaryMatrixId;
	}

	public void setSalaryMatrixId(String salaryMatrixId) {
		this.salaryMatrixId = salaryMatrixId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicSalaryMatrixDescription() {
		return arabicSalaryMatrixDescription;
	}

	public void setArabicSalaryMatrixDescription(String arabicSalaryMatrixDescription) {
		this.arabicSalaryMatrixDescription = arabicSalaryMatrixDescription;
	}

	public Short getPayUnit() {
		return payUnit;
	}

	public void setPayUnit(Short payUnit) {
		this.payUnit = payUnit;
	}

	public int getTotalRow() {
		return totalRow;
	}

	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}

	public int getTotalColumns() {
		return totalColumns;
	}

	public void setTotalColumns(int totalColumns) {
		this.totalColumns = totalColumns;
	}

	public int getTotalRowValues() {
		return totalRowValues;
	}

	public void setTotalRowValues(int totalRowValues) {
		this.totalRowValues = totalRowValues;
	}

	public List<SalaryMatrixColSetup> getSalaryMatrixColSetup() {
		return salaryMatrixColSetup;
	}

	public void setSalaryMatrixColSetup(List<SalaryMatrixColSetup> salaryMatrixColSetup) {
		this.salaryMatrixColSetup = salaryMatrixColSetup;
	}

	public List<SalaryMatrixRow> getSalaryMatrixRow() {
		return salaryMatrixRow;
	}

	public void setSalaryMatrixRow(List<SalaryMatrixRow> salaryMatrixRow) {
		this.salaryMatrixRow = salaryMatrixRow;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
