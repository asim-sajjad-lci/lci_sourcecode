package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.model.TransactionEntry;

@Repository("repositoryTransactionEntry")
public interface RepositoryTransactionEntry extends JpaRepository<TransactionEntry, Integer>{

	TransactionEntry findByIdAndIsDeleted(Integer id, boolean b);
	
	TransactionEntry findByIdAndIsDeletedAndFromBuild(Integer id, boolean b,boolean c);
	
	
	List<TransactionEntry> findByIsDeleted(boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TransactionEntry t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	public void deleteSingleTransactionEntry(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	TraningCourse findByEntryNumberAndIsDeleted(Integer entryNumber,boolean b);
	
	@Query("select t from TransactionEntry t where (t.description like :searchKeyWord or t.arabicDescription LIKE :searchKeyWord) and t.isDeleted=false")
	List<TransactionEntry> predictivegetAllTransactionEntrySearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	
	@Query("select count(*) from TransactionEntry t where (t.description like :searchKeyWord or t.arabicDescription LIKE :searchKeyWord) and t.isDeleted=false")
	Integer predictivegetAllTransactionEntrySearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	//@Cacheable("transactionEntry")
	@Query("select t from TransactionEntry t where t.batches.id =:id and t.isDeleted=false")
	List<TransactionEntry> findByBatches(@Param("id")Integer id);
	
	@Query("select t from TransactionEntry t where t.batches.id =:id and t.isDeleted=false and t.fromBuild=false")
	List<TransactionEntry> findByBatches1(@Param("id")Integer id);
	
	@Query("select t from TransactionEntry t where t.batches.id =:id and t.isDeleted=false")
	List<TransactionEntry> findByBatchesAndFromBuild(@Param("id")Integer id);
	
	//@Cacheable("transactionEntry")
	TransactionEntry findTopByOrderByIdDesc();
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TransactionEntry t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	void deleteSingleTranscationEntryRow(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("DELETE FROM TransactionEntry t  where t.id =:id ")
	void deleteSingleTranscationEntryRow(@Param("id") Integer id);
	
	@Query("select d from TransactionEntry d where d.batches.id in(:list) and d.isDeleted=false")	
	public List<TransactionEntry> getIncludeBatches(@Param("list") List<Integer> ids);
	
}
