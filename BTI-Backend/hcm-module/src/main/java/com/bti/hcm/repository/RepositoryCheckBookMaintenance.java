/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.CheckbookMaintenance;



/**
 * Description: Interface for Price Group Setup 
 * Name of Project: Ims
 * Version: 0.0.1
 */
@Repository("repositoryCheckbookMaintenance")
public interface RepositoryCheckBookMaintenance extends JpaRepository<CheckbookMaintenance, Integer>{

	/**
	 * 
	 * @param id
	 * @param b
	 * @return
	 */
	CheckbookMaintenance findByIdAndIsDeleted(Integer id, boolean b);
	
	
	/**
	 * 
	 * @param b
	 * @param loggedInUserId
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update CheckbookMaintenance d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleCheckbookMaintenance(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer id);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from CheckbookMaintenance d where (d.checkbookId like :searchKeyWord or d.bankIdAddress like :searchKeyWord or d.checkbookDescription like :searchKeyWord or d.checkbookArabicDescription like :searchKeyWord) and d.isDeleted = false")
	Integer predictiveCheckbookMaintenanceSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageRequest
	 * @return
	 */
	@Query("select d from CheckbookMaintenance d where (d.checkbookId like :searchKeyWord or d.bankIdAddress like :searchKeyWord or d.checkbookDescription like :searchKeyWord or d.checkbookArabicDescription like :searchKeyWord)and d.isDeleted = false")
	List<CheckbookMaintenance> predictiveCheckbookMaintenanceSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select d from CheckbookMaintenance d where (d.id =:id)and d.isDeleted = false")
	CheckbookMaintenance findById(@Param("id")Integer id);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select d from CheckbookMaintenance d where (d.checkbookId =:id)and d.isDeleted = false")
	List<CheckbookMaintenance> uniqueCheck(@Param("id") String id);


	@Query("select d from CheckbookMaintenance d where d.isDeleted = false")
	List<CheckbookMaintenance> getAllData();


	CheckbookMaintenance findByCheckbookIdAndIsDeleted(String checkbookId, boolean b);

	@Query("select d from CheckbookMaintenance d where (d.checkbookId like :searchKeyWord)and d.isDeleted = false")
	List<CheckbookMaintenance> getAllDroupdown(@Param("searchKeyWord")String string);
	

}


