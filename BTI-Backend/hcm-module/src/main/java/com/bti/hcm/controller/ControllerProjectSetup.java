package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoProjectSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceProjectSetup;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/projectSetup")
public class ControllerProjectSetup extends BaseController{
	
	private static final Logger LOGGER = Logger.getLogger(ControllerProjectSetup.class);
	
	@Autowired(required = true)
	ServiceProjectSetup serviceProjectSetup;
	
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createProjectSetuo(HttpServletRequest request, @RequestBody DtoProjectSetup dtoProjectSetup)
			throws Exception {
		LOGGER.info("Create ProjectSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoProjectSetup = serviceProjectSetup.saveOrUpdateProjectSetup(dtoProjectSetup);
			responseMessage = displayMessage(dtoProjectSetup, "PROJECT_CREATED", "PROJECT_NOT_CREATED",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create ProjectSetup Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateProjectSetuo(HttpServletRequest request, @RequestBody DtoProjectSetup dtoProjectSetup)
			throws Exception {
		LOGGER.info("Update ProjectSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoProjectSetup = serviceProjectSetup.saveOrUpdateProjectSetup(dtoProjectSetup);
			responseMessage = displayMessage(dtoProjectSetup, "PROJECT_CREATED", "PROJECT_NOT_CREATED",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update ProjectSetup Method:" + responseMessage.getMessage());
		return responseMessage;
	}
	
	@PostMapping("/delete")
	
	public ResponseMessage deleteDivision(HttpServletRequest request, @RequestBody DtoProjectSetup dtoProjectSetup)
			throws Exception {
		LOGGER.info("Delete ProjectSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoProjectSetup.getIds() != null && !dtoProjectSetup.getIds().isEmpty()) {
				DtoProjectSetup dtoProjectSetup2 = serviceProjectSetup.deleteDivision(dtoProjectSetup.getIds());
				if (dtoProjectSetup2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("PROJECT_SETUP_DELETED", false), dtoProjectSetup2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("PROJECT_SETUP_NOT_DELETE_ID_MESSAGE", false),
							dtoProjectSetup2);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete ProjectSetup Method:" + responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getProjectSetupById", method = RequestMethod.POST)
	public ResponseMessage getProjectSetupById(HttpServletRequest request, @RequestBody DtoProjectSetup dtoProjectSetup)
			throws Exception {
		LOGGER.info("Get ProjectSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoProjectSetup dtoProjectSetupObj = serviceProjectSetup
					.getProjectSetuoByProjectSetupId(dtoProjectSetup.getId());
			responseMessage = displayMessage(dtoProjectSetupObj, "PROJECT_GET_DETAIL", "PROEJCT_NOT_GETTING",
					serviceResponse);
			
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ProjectSetup ById Method:" + dtoProjectSetup.getId());
		return responseMessage;
	}
	
	@RequestMapping(value = "/projectIdcheck", method = RequestMethod.POST)
	public ResponseMessage projectIdcheck(HttpServletRequest request, @RequestBody DtoProjectSetup dtoProjectSetup)
			throws Exception {
		LOGGER.info("projectIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoProjectSetup dtoProjectSetupObj = serviceProjectSetup
					.repeatByProjectSetupId(dtoProjectSetup.getProjectId());
			
			 if (dtoProjectSetupObj !=null && dtoProjectSetupObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                         serviceResponse.getMessageByShortAndIsDeleted("PROJECT_RESULT", false), dtoProjectSetupObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                         serviceResponse.getMessageByShortAndIsDeleted("PROJECT_REPEAT_PROJECTID_NOT_FOUND", false),
                         dtoProjectSetupObj);
             }
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllProjectId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllProjectId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("Search Project Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceProjectSetup.getAllProjectId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, MessageConstant.PROJECT_GET_ALL,
					MessageConstant.PROJECT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllProjectDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllProjectDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoProjectSetup> dtoProjectSetupList = serviceProjectSetup.getAllProjectSetupDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("PROJECT_GET_ALL", false), dtoProjectSetupList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchProject(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceProjectSetup.searchProject(dtoSearch);
			responseMessage = displayMessage(dtoSearch, MessageConstant.DEPARTMENT_GET_ALL,
					MessageConstant.DEPARTMENT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getProjectById", method = RequestMethod.POST)
	public ResponseMessage getProjectById(HttpServletRequest request, @RequestBody DtoDepartment dtoDepartment)
			throws Exception {
		LOGGER.info("Get Project ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoProjectSetup dtoProjectSetupObj = serviceProjectSetup.getProjectByProjectId(dtoDepartment.getId());
			responseMessage = displayMessage(dtoProjectSetupObj, "PROJECT_GET_DETAIL", "PROJECT_NOT_GETTING",
					serviceResponse);
			
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Project ById Method:" + dtoDepartment.getId());
		return responseMessage;
	}

}
