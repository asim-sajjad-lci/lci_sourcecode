/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPayScheduleSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayScheduleSetup;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: Controller PayScheduleSetup
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/PayScheduleSetup")
public class ControllerPayScheduleSetup extends BaseController{
	
	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetup Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerPayScheduleSetup.class);
	
	/**
	 * @Description servicePayScheduleSetup Autowired here using annotation of spring for use of servicePayScheduleSetup method in this controller
	 */
	@Autowired(required=true)
	ServicePayScheduleSetup servicePayScheduleSetup;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @description : Create PayScheduleSetup
	 * @param request
	 * @param dtoPayScheduleSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "payScheduleId":"A101",
		  "payScheduleDescription":"Testing",
		  "payScheduleDescriptionArabic":"Testing arabic",
		  "paySchedulePayPeriod":1,
		  "payScheduleBeggingDate":15151512311,
		  "payScheduleYear":1
		  
		}
		@response
			{
				"code": 201,
				"status": "CREATED",
				"result": {
				"payScheduleIndexId": 0,
				"payScheduleId": "A101",
				"payScheduleDescription": "Testing",
				"payScheduleDescriptionArabic": "Testing arabic",
				"paySchedulePayPeriod": 1,
				"payScheduleBeggingDate": 15151512311,
				"payScheduleYear": 1
			},
			"btiMessage": {
				"message": "PayScheduleSetup created successfully.",
				"messageShort": "PAY_SCHEDULE_SETUP_CREATED"
			}
		}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createPayScheduleSetup(HttpServletRequest request, @RequestBody DtoPayScheduleSetup dtoPayScheduleSetup) throws Exception {
		LOGGER.info("Create PayScheduleSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetup = servicePayScheduleSetup.saveOrUpdatePayScheduleSetup(dtoPayScheduleSetup);
			responseMessage=displayMessage(dtoPayScheduleSetup, "PAY_SCHEDULE_SETUP_CREATED", "PAY_SCHEDULE_SETUP_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PayScheduleSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	/**
	 * @description update PayScheduleSetup
	 * @param request
	 * @param dtoPayScheduleSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "payScheduleIndexId":1,
		  "payScheduleId":"A101",
		  "payScheduleDescription":"Testingsss",
		  "payScheduleDescriptionArabic":"Testing arabicsss",
		  "paySchedulePayPeriod":1,
		  "payScheduleBeggingDate":15151512311,
		  "payScheduleYear":1
		  
		}
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"payScheduleIndexId": 1,
			"payScheduleId": "A101",
			"payScheduleDescription": "Testingsss",
			"payScheduleDescriptionArabic": "Testing arabicsss",
			"paySchedulePayPeriod": 1,
			"payScheduleBeggingDate": 15151512311,
			"payScheduleYear": 1
			},
			"btiMessage": {
				"message": "PayScheduleSetup updated successfully.",
				"messageShort": "PAY_SCHEDULE_SETUP_UPDATED_SUCCESS"
			}
		}
		
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePayScheduleSetup(HttpServletRequest request, @RequestBody DtoPayScheduleSetup dtoPayScheduleSetup) throws Exception {
		LOGGER.info("Update PayScheduleSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetup = servicePayScheduleSetup.saveOrUpdatePayScheduleSetup(dtoPayScheduleSetup);
			responseMessage=displayMessage(dtoPayScheduleSetup, "PAY_SCHEDULE_SETUP_UPDATED_SUCCESS", MessageConstant.PAY_SCHEDULE_SETUP_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update PayScheduleSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	/**
	 * @description delete PayScheduleSetup
	 * @param request
	 * @param dtoPayScheduleSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
			  "ids":[1]
		}
		@response
		{
					"code": 201,
					"status": "CREATED",
					"result": {
					"payScheduleIndexId": 0,
					"paySchedulePayPeriod": 0,
					"payScheduleYear": 0,
					"deleteMessage": "N/A",
					"associateMessage": "N/A",
					"deletePayScheduleSetup": [
					  {
					"payScheduleIndexId": 1,
					"paySchedulePayPeriod": 0,
					"payScheduleYear": 0
				}
			],
		},
			"btiMessage": {
					"message": "PayScheduleSetup deleted successfully.",
					"messageShort": "PAY_SCHEDULE_SETUP_DELETED"
				}
			}
	 *//*
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePayScheduleSetup(HttpServletRequest request, @RequestBody DtoPayScheduleSetup dtoPayScheduleSetup) throws Exception {
		LOGGER.info("Delete PayScheduleSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayScheduleSetup.getIds() != null && dtoPayScheduleSetup.getIds().isEmpty()) {
				DtoPayScheduleSetup dtoPayScheduleSetup2 = servicePayScheduleSetup.deletePaySCheduleSetup(dtoPayScheduleSetup.getIds());
				
				
				if(dtoPayScheduleSetup2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_DELETED", false), dtoPayScheduleSetup2);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHDULE_SETUP_DELETED_ID_MESSAGE", false), dtoPayScheduleSetup2);


				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete PayScheduleSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	*/
	
	
	
	

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePayScheduleSetup(HttpServletRequest request, @RequestBody DtoPayScheduleSetup dtoPayScheduleSetup) throws Exception {
		LOGGER.info("Delete PayScheduleSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayScheduleSetup.getIds() != null && !dtoPayScheduleSetup.getIds().isEmpty()) {
				DtoPayScheduleSetup dtoPayScheduleSetup2 = servicePayScheduleSetup.deletePaySCheduleSetup(dtoPayScheduleSetup.getIds());
				
				if(dtoPayScheduleSetup2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_DELETED", false), dtoPayScheduleSetup2);	
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHDULE_SETUP_DELETED_ID_MESSAGE", false), dtoPayScheduleSetup2);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete PayScheduleSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	
	
	/**
	 * @description search PayScheduleSetup
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "searchKeyword":"a",
		  "sortOn":"payScheduleId",
		  "sortBy":"DESC",
		 "pageNumber":"0",
		  "pageSize":"10"
		}
		
		@response
		{
				"code": 200,
				"status": "OK",
				"result": {
							"searchKeyword": "a",
							"pageNumber": 0,
							"pageSize": 10,
							"sortOn": "payScheduleId",
							"sortBy": "DESC",
							"totalCount": 1,
				"records": [
				  {
						"payScheduleIndexId": 0,
						"payScheduleId": "A101",
						"payScheduleDescription": "Testings",
						"payScheduleDescriptionArabic": "Testing arabics",
						"paySchedulePayPeriod": 1,
						"payScheduleBeggingDate": 15151512000,
						"payScheduleYear": 1
						}
					],
				}, 
				"btiMessage": {
						"message": "PayScheduleSetup list fetched successfully.",
						"messageShort": "PAY_SCHEDULE_SETUP_GET_ALL"
						}
				}
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPayScheduleSetup(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search PayScheduleSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayScheduleSetup.searchPayScheduleSetup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.PAY_SCHEDULE_SETUP_GET_ALL, "PAY_SCHEDULE_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PayScheduleSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * @description ScheduleSetupById is exiest or not
	 * @param request
	 * @param dtoPayScheduleSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "payScheduleIndexId":2
		}
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"payScheduleIndexId": 0,
			"payScheduleId": "A101",
			"payScheduleDescription": "Testings",
			"payScheduleDescriptionArabic": "Testing arabics",
			"paySchedulePayPeriod": 1,
			"payScheduleBeggingDate": 15151512000,
			"payScheduleYear": 1
			},
			"btiMessage": {
				"message": "PayScheduleSetup Detail fetched successfully.",
				"messageShort": "PAY_SCHEDULE_SETUP_GET_DETAIL"
				}
			}
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPayScheduleSetupById(HttpServletRequest request, @RequestBody DtoPayScheduleSetup dtoPayScheduleSetup) throws Exception {
		LOGGER.info("Get PayScheduleSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayScheduleSetup dtoPayScheduleSetupObj = servicePayScheduleSetup.getPayScheduleSetupByPayScheduleId(dtoPayScheduleSetup.getPayScheduleIndexId());
			responseMessage=displayMessage(dtoPayScheduleSetupObj, "PAY_SCHEDULE_SETUP_GET_DETAIL", "PAY_SCHEDULE_SETUP_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get PayScheduleSetup ById Method:"+dtoPayScheduleSetup.getPayScheduleIndexId());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetup
	 * @return
	 * @throws Exception
	 * @request{
		  "payScheduleIndexId":2
		 
		}
		@response
		{
			"code": 302,
			"status": "FOUND",
			"result": {
					"payScheduleIndexId": 0,
					"paySchedulePayPeriod": 0,
					"payScheduleYear": 0,
					"isRepeat": true
					},
			"btiMessage": {
				"message": "PAY_SCHEDULE_SETUP_RESULT",
				"messageShort": "N/A"
				}
			}
	 *//*
	@RequestMapping(value = "/paycodeIdcheck", method = RequestMethod.POST)
	public ResponseMessage paycodeIdcheck(HttpServletRequest request, @RequestBody DtoPayScheduleSetup dtoPayScheduleSetup) throws Exception {
		LOGGER.info("paycodeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayScheduleSetup dtoPayScheduleSetupObj = servicePayScheduleSetup.repeatByPayScheduleSetuptId(dtoPayScheduleSetup.getPayScheduleId());
			responseMessage=displayMessage(dtoPayScheduleSetupObj, "PAY_SCHEDULE_SETUP_RESULT", "PAY_SCHEDULE_SETUP_REPEAT_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}*/
	
	
	
	
	@RequestMapping(value = "/paycodeIdcheck", method = RequestMethod.POST)
	public ResponseMessage paycodeIdcheck(HttpServletRequest request, @RequestBody DtoPayScheduleSetup dtoPayScheduleSetup)
			throws Exception {
		LOGGER.info("paycodeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayScheduleSetup dtoPayScheduleSetupObj = servicePayScheduleSetup.repeatByPayScheduleSetuptId(dtoPayScheduleSetup.getPayScheduleId());
			
			 if (dtoPayScheduleSetupObj !=null && dtoPayScheduleSetupObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                         serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_RESULT", false), dtoPayScheduleSetupObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                         serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_REPEAT_NOT_FOUND", false),
                         dtoPayScheduleSetupObj);
             }
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	
	
	@RequestMapping(value = "/getAllPayScheduleSetup", method = RequestMethod.GET)
	public ResponseMessage paycodeIdcheck(HttpServletRequest request) throws Exception {
		LOGGER.info("paycodeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch =  servicePayScheduleSetup.getAllPayScheduleSetup();
			responseMessage=displayMessage(dtoSearch, "PAY_SCHEDULE_SETUP_GET_ALL", "PAY_SCHEDULE_SETUP_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/searchPayScheduleId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPayScheduleId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("searchPayScheduleId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayScheduleSetup.searchPayScheduleId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_SCHEDULE_SETUP_GET_ALL", "PAY_SCHEDULE_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PayScheduleSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	
	
	
}
