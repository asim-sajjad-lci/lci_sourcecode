/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.LifeInsuranceSetup;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO LifeInsuranceSetup class having getter and setter for fields
 * (POJO) Name Name of Project: Hcm Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoLifeInsuranceSetup extends DtoBase {

	private Integer id;
	private String lifeInsuraceId;
	private String lifeInsuraceDescription;
	private String lifeInsuraceDescriptionArabic;
	private String healthInsuranceGroupNumber;
	private Short lifeInsuranceFrequency;
	private BigDecimal amount;
	private BigDecimal spouseAmount;
	private BigDecimal childAmount;
	private BigDecimal coverageTotalAmount;
	private BigDecimal employeePay;
	private Date startDate;
	private Date endDate;
	private short insuranceType;
	private Integer healthInsuranceId;
	private List<DtoLifeInsuranceSetup> deleteLifeInsuranceSetup;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLifeInsuraceId() {
		return lifeInsuraceId;
	}

	public void setLifeInsuraceId(String lifeInsuraceId) {
		this.lifeInsuraceId = lifeInsuraceId;
	}

	public String getLifeInsuraceDescription() {
		return lifeInsuraceDescription;
	}

	public void setLifeInsuraceDescription(String lifeInsuraceDescription) {
		this.lifeInsuraceDescription = lifeInsuraceDescription;
	}

	public String getLifeInsuraceDescriptionArabic() {
		return lifeInsuraceDescriptionArabic;
	}

	public void setLifeInsuraceDescriptionArabic(String lifeInsuraceDescriptionArabic) {
		this.lifeInsuraceDescriptionArabic = lifeInsuraceDescriptionArabic;
	}

	public String getHealthInsuranceGroupNumber() {
		return healthInsuranceGroupNumber;
	}

	public void setHealthInsuranceGroupNumber(String healthInsuranceGroupNumber) {
		this.healthInsuranceGroupNumber = healthInsuranceGroupNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getSpouseAmount() {
		return spouseAmount;
	}

	public void setSpouseAmount(BigDecimal spouseAmount) {
		this.spouseAmount = spouseAmount;
	}

	public BigDecimal getChildAmount() {
		return childAmount;
	}

	public void setChildAmount(BigDecimal childAmount) {
		this.childAmount = childAmount;
	}

	public BigDecimal getCoverageTotalAmount() {
		return coverageTotalAmount;
	}

	public void setCoverageTotalAmount(BigDecimal coverageTotalAmount) {
		this.coverageTotalAmount = coverageTotalAmount;
	}

	public BigDecimal getEmployeePay() {
		return employeePay;
	}

	public void setEmployeePay(BigDecimal employeePay) {
		this.employeePay = employeePay;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public short getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(short insuranceType) {
		this.insuranceType = insuranceType;
	}

	public Integer getHealthInsuranceId() {
		return healthInsuranceId;
	}

	public void setHealthInsuranceId(Integer healthInsuranceId) {
		this.healthInsuranceId = healthInsuranceId;
	}

	public Short getLifeInsuranceFrequency() {
		return lifeInsuranceFrequency;
	}

	public void setLifeInsuranceFrequency(Short lifeInsuranceFrequency) {
		this.lifeInsuranceFrequency = lifeInsuranceFrequency;
	}

	public DtoLifeInsuranceSetup() {

	}

	public DtoLifeInsuranceSetup(LifeInsuranceSetup lifeInsuranceSetup) {

	}

	public List<DtoLifeInsuranceSetup> getDeleteLifeInsuranceSetup() {
		return deleteLifeInsuranceSetup;
	}

	public void setDeleteLifeInsuranceSetup(List<DtoLifeInsuranceSetup> deleteLifeInsuranceSetup) {
		this.deleteLifeInsuranceSetup = deleteLifeInsuranceSetup;
	}

}
