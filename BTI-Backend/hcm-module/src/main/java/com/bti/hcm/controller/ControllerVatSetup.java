package com.bti.hcm.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoVatSetup;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceVatSetup;


@RestController
@RequestMapping("/vatSetup")
public class ControllerVatSetup extends BaseController{
	
	@Autowired
	ServiceResponse response;
	
	@Autowired
	ServiceVatSetup serviceVatSetup;
	
	@Autowired
	ServiceHcmHome serviceHome;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerVatSetup.class);
	
	/**
	 * 
	 * @param request
	 * @param dtoVatSetup
	 * @return
	 * @param dtoVatSetup {
	"vatScheduleId" : "vat1",
	"vatDescription" : "aaaa",
	"vatDescriptionArabic" : "bbbbb",
	"vatSeriesType": 121,
	"vatIdNumber" : 122,
	"accountTableRowIndex" : 25,
	"vatBasedOn" : 22,
	"percentageBasedOn" : 12,
	"minimumVatAmt" : 15,
	"maximumVatAmt" : 18,
	"ytdTotalSales" : 200,
	"ytdTotalTaxableSales" : 300,
	"ytdTotalSalesTaxes" : 1500,
	"lastYearTotalSales" : 158,
	"lastYearTotalTaxableSales" : 140,
	"lastYearTotalSalesTaxes" : 199
	}
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createItemCategorySetup(HttpServletRequest request,@RequestBody DtoVatSetup dtoVatSetup) throws Exception{
		LOGGER.info("Create Vat Setup Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoVatSetup = serviceVatSetup.saveOrUpdate(dtoVatSetup);
			
			responseMessage = displayMessage(dtoVatSetup,"VAT_SETUP_CREATED","VAT_SETUP_NOT_CREATED",response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create Vat Setup Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return 
	 * @param {"searchKeyword":"",
	 * "sortOn":"",
	 * "sortBy":"",
	 * "pageNumber":"0",
	 * "pageSize":"10"}
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Vat Setup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceVatSetup.search(dtoSearch);
			responseMessage = displayMessage(dtoSearch,MessageConstant.VAT_SETUP_GET_ALL,MessageConstant.VAT_SETUP_GET_ALL,response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Vat Setup Method Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllData", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllData(HttpServletRequest request) throws Exception {
		ResponseMessage responseMessage = null;
		List<DtoVatSetup> dtoSearch = null;
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceVatSetup.getAllData();
		
			responseMessage = displayMessage(dtoSearch,MessageConstant.VAT_SETUP_GET_ALL,MessageConstant.VAT_SETUP_GET_ALL,response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllDroupDown", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllDroupDown(HttpServletRequest request,@RequestBody DtoSearch search) throws Exception {
		ResponseMessage responseMessage = null;
		List<DtoVatSetup> dtoSearch = null;
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceVatSetup.getAllDroupDown(search);
			responseMessage = displayMessage(dtoSearch,MessageConstant.VAT_SETUP_GET_ALL,MessageConstant.VAT_SETUP_GET_ALL,response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/purchaseId", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage purchaseId(HttpServletRequest request) throws Exception {
		LOGGER.info("getAllData Vat Setup Method");
		ResponseMessage responseMessage = null;
		List<DtoVatSetup> dtoSearch = new ArrayList<>();
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceVatSetup.purchaseId();
			responseMessage = displayMessage(dtoSearch,MessageConstant.VAT_SETUP_GET_ALL,MessageConstant.VAT_SETUP_GET_ALL,response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("getAllData Vat Setup Method : "+dtoSearch);
		return responseMessage;
	}
	
	@RequestMapping(value = "/salesId", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage salesId(HttpServletRequest request) throws Exception {
		LOGGER.info("getAllData Vat Setup Method");
		ResponseMessage responseMessage = null;
		List<DtoVatSetup> dtoSearch = new ArrayList<>();
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceVatSetup.salesId();
			responseMessage = displayMessage(dtoSearch,MessageConstant.VAT_SETUP_GET_ALL,MessageConstant.VAT_SETUP_GET_ALL,response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("getAllData Vat Setup Method : "+dtoSearch);
		return responseMessage;
	}



}
