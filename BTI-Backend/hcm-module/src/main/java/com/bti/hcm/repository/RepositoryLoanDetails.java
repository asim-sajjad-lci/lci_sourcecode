package com.bti.hcm.repository;
/**
 * 	@author HAMID
 * 
 */
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.LoanDetails;

@Repository("repositoryLoanDetails")
public interface RepositoryLoanDetails extends JpaRepository<LoanDetails, Integer> {

	// @Query("select count(*) from ManualChecksDistribution m ")
	// public Integer getCountOfTotaLocation();

	
	LoanDetails findByIdAndIsDeleted(Integer id, Boolean b);	//LoanDetailsId
	
	//	Integer countByIdAndIsDeleted(Integer id, Boolean b);	//LoanDetailsId
	
	@Query("select ld from LoanDetails ld where ld.loan.id =:id and ld.isDeleted=false")
	List<LoanDetails> findByLoanIdAndIsDeleted(@Param("id") Integer id);	//Loan.Id    //deprecative y below query
	
	
	@Query("select ld from LoanDetails ld where ld.isDeleted=false and ld.isPaid=false and ld.isPostpone=false and ld.loan.id IN "
			+ "(select l.id from Loan l where l.employeeMaster.employeeIndexId =:id ) ")
	List<LoanDetails> findByEmployeeIdAndIsDeleted(@Param("id") Integer id, Pageable pageable);	//Loan.Id    ///
	
	
	@Query("select count(ld) from LoanDetails ld where ld.isDeleted=false and ld.isPaid=false and ld.isPostpone=false and ld.loan.id IN "
			+ "(select l.id from Loan l where l.employeeMaster.employeeIndexId =:id ) ")
	Integer countByEmployeeIdAndIsDeleted(@Param("id") Integer id);	//Loan.Id    ///
	
	
	//	Integer countByLoanIdAndIsDeleted(Integer id, Boolean isDeleted);	//Loan.Id
	
	LoanDetails findByIdAndIsDeletedAndLoanId(Integer id, boolean b, Integer loanId);
	
	List<LoanDetails> findByIsDeletedAndLoanId(boolean b, Integer loanId);
	
	
	@Query("select sum(ld.amt) from LoanDetails ld where ld.isPaid = true and ld.isDeleted = false and ld.loan.id IN "
			+ "( select l.id from Loan l where l.employeeMaster.employeeIndexId =:id and l.isDeleted = false ) ")	//For TotalDeduction
	List<Double> sumOfAllDeductionsForThisEmployee(@Param ("id") Integer id);
	
	
	@Query("select sum(ld.amt) from LoanDetails ld where ld.isPaid = false and ld.isDeleted = false and ld.loan.id IN "
			+ "( select l.id from Loan l where l.employeeMaster.employeeIndexId =:id and l.isDeleted = false ) ")	//For TotalRemainings
	List<Double> sumOfAllRemainingsForThisEmployee(@Param ("id") Integer id);	
	
	
}
