package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.HelthCoverageType;
import com.bti.hcm.model.dto.DtoHelthCoverageType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryHelthCoverageType;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceHelthCoverageType")
public class ServiceHelthCoverageType {

	/**
	 * @Description LOGGER use for put a logger in HelthCoverageType Service
	 */
	static Logger log = Logger.getLogger(ServiceHelthCoverageType.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in HelthCoverageType service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in HelthCoverageType service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in HelthCoverageType service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryAccrual Autowired here using annotation of spring for access of repositoryAccrual method in Accrual service
	 * 				In short Access Accrual Query from Database using repositoryAccrual.
	 */
	@Autowired
	RepositoryHelthCoverageType repositoryHelthCoverageType;
	
	public DtoHelthCoverageType saveOrUpdate(DtoHelthCoverageType dtoHelthCoverageType) {
		try {

			log.info("saveOrUpdate Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
			HelthCoverageType helthCoverageType = null;
			if (dtoHelthCoverageType.getId() != null && dtoHelthCoverageType.getId() > 0) {
				helthCoverageType = repositoryHelthCoverageType.findByIdAndIsDeleted(dtoHelthCoverageType.getId(), false);
				helthCoverageType.setUpdatedBy(loggedInUserId);
				helthCoverageType.setUpdatedDate(new Date());
			} else {
				helthCoverageType = new HelthCoverageType();
				helthCoverageType.setCreatedDate(new Date());
				Integer rowId = repositoryHelthCoverageType.getCountOfTotalHelthCoverageType();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				helthCoverageType.setRowId(increment);
			}
			
			helthCoverageType.setDesc(dtoHelthCoverageType.getDesc());
			helthCoverageType.setArbicDesc(dtoHelthCoverageType.getArbicDesc());
			helthCoverageType.setHelthCoverageId(dtoHelthCoverageType.getHelthCoverageId());
			helthCoverageType.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			//helthCoverageType.setRowId(loggedInUserId);
			helthCoverageType = repositoryHelthCoverageType.saveAndFlush(helthCoverageType);
			log.debug("helthCoverageType is:"+dtoHelthCoverageType.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoHelthCoverageType;
	}
	
	public DtoHelthCoverageType delete(List<Integer> ids) {
		log.info("delete Method");
		DtoHelthCoverageType dtoHelthCoverageType = new DtoHelthCoverageType();
		dtoHelthCoverageType.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("HELTH_COVERAGE_DELETED", false));
		dtoHelthCoverageType.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("HEALTH_COVERAG_TYPE_ASSOCIATED", false));
		boolean inValidDelete = false;
		List<DtoHelthCoverageType> deleteHelthCovergae = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse
				.getMessageByShortAndIsDeleted("HELTH_COVERAGE_NOT_DELETED", false).getMessage());
		try {
			for (Integer helthCoverageTypeId : ids) {
				HelthCoverageType helthCoverageType = repositoryHelthCoverageType.findOne(helthCoverageTypeId);
				if(helthCoverageType!=null && helthCoverageType.getHelthInsurance().isEmpty()) {
					DtoHelthCoverageType dtohelthCoverageType2 = new DtoHelthCoverageType();
					dtohelthCoverageType2.setId(helthCoverageTypeId);
					repositoryHelthCoverageType.deleteSingleHelyhCovergareType(true, loggedInUserId, helthCoverageTypeId);
					deleteHelthCovergae.add(dtohelthCoverageType2);
				}else {
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoHelthCoverageType.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoHelthCoverageType.setMessageType("");

			}
			dtoHelthCoverageType.setDelete(deleteHelthCovergae);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete dtohelthCoverageType :"+dtoHelthCoverageType.getId());
		return dtoHelthCoverageType;
	}

	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			

			log.info("search dtohelthCoverageType Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
	             String condition="";
				
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("helthCoverageId") || dtoSearch.getSortOn().equals("desc") || dtoSearch.getSortOn().equals("arbicDesc")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	
				
				
				
				
				dtoSearch.setTotalCount(this.repositoryHelthCoverageType.predictiveHelthCoverageTypeSearchTotalCount("%"+searchWord+"%"));
				List<HelthCoverageType> helthCoverageType2List =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						helthCoverageType2List =this.repositoryHelthCoverageType.predictiveHelthCoverageTypeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						helthCoverageType2List = this.repositoryHelthCoverageType.predictiveHelthCoverageTypeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						helthCoverageType2List = this.repositoryHelthCoverageType.predictiveHelthCoverageTypeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
			
				if(helthCoverageType2List != null && !helthCoverageType2List.isEmpty()){
					List<DtoHelthCoverageType> dtohelthCoverageTypeList = new ArrayList<>();
					DtoHelthCoverageType dtohelthCoverageType=null;
					for (HelthCoverageType accrual : helthCoverageType2List) {
						dtohelthCoverageType = new DtoHelthCoverageType(accrual);
						dtohelthCoverageType.setId(accrual.getId());
						dtohelthCoverageType.setDesc(accrual.getDesc());
						dtohelthCoverageType.setArbicDesc(accrual.getArbicDesc());
						dtohelthCoverageType.setHelthCoverageId(accrual.getHelthCoverageId());
						dtohelthCoverageTypeList.add(dtohelthCoverageType);
					}
					dtoSearch.setRecords(dtohelthCoverageTypeList);
				}
			}
			log.debug("Search helthCoverageTypeList Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	public DtoHelthCoverageType getByHelthCoverageTypeId(Integer id) {
		log.info("getByHelthCoverageTypeId Method");
		DtoHelthCoverageType dtohelthCoverageTypeList = new DtoHelthCoverageType();
		try {
			if (id > 0) {
				HelthCoverageType helthCoverageType = repositoryHelthCoverageType.findByIdAndIsDeleted(id, false);
				if (helthCoverageType != null) {
					dtohelthCoverageTypeList = new DtoHelthCoverageType(helthCoverageType);
					dtohelthCoverageTypeList.setId(helthCoverageType.getId());
					dtohelthCoverageTypeList.setDesc(helthCoverageType.getDesc());
					dtohelthCoverageTypeList.setArbicDesc(helthCoverageType.getArbicDesc());
					dtohelthCoverageTypeList.setHelthCoverageId(helthCoverageType.getHelthCoverageId());
					
				} else {
					dtohelthCoverageTypeList.setMessageType("SKILL_STEUP_NOT_GETTING");

				}
			} else {
				dtohelthCoverageTypeList.setMessageType("INVALID_HEALTH_COVERAG_TYPE_ID");

			}
			log.debug("helthCoverageType By Id is:"+dtohelthCoverageTypeList.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtohelthCoverageTypeList;
	}
	
	public DtoSearch getAll(DtoHelthCoverageType dtohelthCoverageType) {
		log.info("getAll HelthCoverageType Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtohelthCoverageType.getPageNumber());
			dtoSearch.setPageSize(dtohelthCoverageType.getPageSize());
			dtoSearch.setTotalCount(repositoryHelthCoverageType.getCountOfTotalAccrual());
			List<HelthCoverageType> skillAccrualList = null;
			if (dtohelthCoverageType.getPageNumber() != null && dtohelthCoverageType.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtohelthCoverageType.getPageNumber(), dtohelthCoverageType.getPageSize(), Direction.DESC, "createdDate");
				skillAccrualList = repositoryHelthCoverageType.findByIsDeleted(false, pageable);
			} else {
				skillAccrualList = repositoryHelthCoverageType.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoHelthCoverageType> dtoPOsitionList=new ArrayList<>();
			if(skillAccrualList!=null && !skillAccrualList.isEmpty())
			{
				for (HelthCoverageType accrual : skillAccrualList) 
				{
					dtohelthCoverageType=new DtoHelthCoverageType(accrual);
					dtohelthCoverageType.setId(accrual.getId());
					dtohelthCoverageType.setDesc(accrual.getDesc());
					dtohelthCoverageType.setArbicDesc(accrual.getArbicDesc());
					dtohelthCoverageType.setHelthCoverageId(accrual.getHelthCoverageId());
					
					dtoPOsitionList.add(dtohelthCoverageType);
				}
				dtoSearch.setRecords(dtoPOsitionList);	
			}
			log.debug("All HelthCoverageType List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoHelthCoverageType repeatByHelthCoverageTypeId(String helthCoverageId) {
		log.info("repeatByHelthCoverageTypeId Method");
		DtoHelthCoverageType dtoHelthCoverageType = new DtoHelthCoverageType();
		try {
			List<HelthCoverageType> helthCoverageType=repositoryHelthCoverageType.findByHelthCoverageTypeId(helthCoverageId.trim());
			if(helthCoverageType!=null && !helthCoverageType.isEmpty()) {
				dtoHelthCoverageType.setIsRepeat(true);
			}else {
				dtoHelthCoverageType.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoHelthCoverageType;
	}

	public DtoSearch searchHelthCoverageId(DtoSearch dtoSearch) {
		try {

			log.info("searchHelthCoverageId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				List<String> helthCoverageIdList =new ArrayList<>();
				
					helthCoverageIdList = this.repositoryHelthCoverageType.predictiveHelthCoverageIdSearchWithPagination("%"+searchWord+"%");
					if(!helthCoverageIdList.isEmpty()) {
						dtoSearch.setTotalCount(helthCoverageIdList.size());
						dtoSearch.setRecords(helthCoverageIdList.size());
					}
				dtoSearch.setIds(helthCoverageIdList);
			}
			
			
		} catch (Exception e) {
		log.error(e);
		}
		return dtoSearch;
	}
	
	
	public DtoSearch getAllCoverageType() {
		DtoHelthCoverageType dtoHelthCoverageType;
		DtoSearch dtoSearch = new DtoSearch();
		try {
			List<HelthCoverageType> helthCoverageTypeList = null;
			helthCoverageTypeList = repositoryHelthCoverageType.findByIsDeleted(false);
			List<DtoHelthCoverageType> dtoHelthCoverageTypeList=new ArrayList<>();
			if(helthCoverageTypeList!=null && !helthCoverageTypeList.isEmpty())
			{
				for (HelthCoverageType helthCoverageType : helthCoverageTypeList) 
				{
					dtoHelthCoverageType=new DtoHelthCoverageType(helthCoverageType);
					dtoHelthCoverageType.setId(helthCoverageType.getId());
					dtoHelthCoverageType.setHelthCoverageId(helthCoverageType.getHelthCoverageId());
					dtoHelthCoverageType.setDesc(helthCoverageType.getDesc());
					dtoHelthCoverageType.setArbicDesc(helthCoverageType.getArbicDesc());
					dtoHelthCoverageTypeList.add(dtoHelthCoverageType);
				}
				dtoSearch.setRecords(dtoHelthCoverageTypeList);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
}
