package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.EmployeeEducation;

public class DtoEmployeeEducation extends DtoBase{
	private Integer id;
	private String schoolName;
	private String major;
	private Integer endYear;
	private Integer startYear;
	private String degree;
	private BigDecimal gpa;
	private String comments;
	private Integer educationSequence;

	private List<DtoEmployeeEducation> listEmployeeEducation;
	private DtoEmployeeMaster employeeMaster;
	private List<DtoEmployeeEducation> delete;

	public DtoEmployeeEducation(EmployeeEducation employeeEducation) {
		
	}

	public DtoEmployeeEducation() {
		
	}

	public DtoEmployeeEducation(List<EmployeeEducation> employeeEducation) {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Integer getEndYear() {
		return endYear;
	}

	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}

	public Integer getStartYear() {
		return startYear;
	}

	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}
	
	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public BigDecimal getGpa() {
		return gpa;
	}

	public void setGpa(BigDecimal gpa) {
		this.gpa = gpa;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getEducationSequence() {
		return educationSequence;
	}

	public void setEducationSequence(Integer educationSequence) {
		this.educationSequence = educationSequence;
	}


	public List<DtoEmployeeEducation> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeeEducation> delete) {
		this.delete = delete;
	}


	public List<DtoEmployeeEducation> getListEmployeeEducation() {
		return listEmployeeEducation;
	}

	public void setListEmployeeEducation(List<DtoEmployeeEducation> listEmployeeEducation) {
		this.listEmployeeEducation = listEmployeeEducation;
	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

}
