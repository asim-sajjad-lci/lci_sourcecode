package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AtteandaceOption;

@Repository("repositoryAtteandaceOption")
public interface RepositoryAtteandaceOption extends JpaRepository<AtteandaceOption, Integer>{

	AtteandaceOption findByIdAndIsDeleted(Integer id, boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update AtteandaceOption d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleAtteandaceOption(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select d from AtteandaceOption d where ( d.reasonDesc like :searchKeyWord  or d.atteandanceTypeDesc like :searchKeyWord) and d.isDeleted=false")
	List<AtteandaceOption> predictiveAtteandaceOptionSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select count(*) from AtteandaceOption d where ( d.reasonDesc like :searchKeyWord  or d.atteandanceTypeDesc like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveAtteandaceSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	List<AtteandaceOption> findByIsDeleted(boolean b, Pageable pageable);

	List<AtteandaceOption> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select count(*) from AtteandaceOption s where s.isDeleted=false")
	Integer getCountOfTotalAtteandace();

	@Query("select d from AtteandaceOption d where (d.atteandanceTypeDesc like :searchKeyWord) and d.isDeleted=false")
	List<AtteandaceOption> getAllAccrualTypeId(@Param("searchKeyWord") String searchKeyWord);

	@Query("select count(*) from AtteandaceOption a ")
	Integer getCountOfTotalAtteandaces();

}
