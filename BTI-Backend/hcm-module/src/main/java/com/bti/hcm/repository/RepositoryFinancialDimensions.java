package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.FinancialDimensions;

@Repository("/repositoryFinancialDimensions")
public interface RepositoryFinancialDimensions extends JpaRepository<FinancialDimensions, Integer> {

	public List<FinancialDimensions> findByIsDeleted(boolean b);

	@Query("select h from FinancialDimensions h where (h.dimensionDescription like :searchKeyWord)and h.isDeleted=false")
	public List<FinancialDimensions> getAllDroupdown(@Param("searchKeyWord")String string);
}
