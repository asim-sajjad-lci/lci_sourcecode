package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeeMonthlyInstallment;

public class DtoEmployeeLoanEntry {

	private Integer id;
	private Integer workflowRequestId;
	private Double loanAmount;
	private Integer numberOfMonths;
	private Date startDate;
	private Date endDate;
	private Double remainingAmount;
	private Boolean isComplete;
	private String employeeCode;
	private List<DtoEmployeeMonthlyInstallment> dtoEmployeeMonthlyInstallmentList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWorkflowRequestId() {
		return workflowRequestId;
	}

	public void setWorkflowRequestId(Integer workflowRequestId) {
		this.workflowRequestId = workflowRequestId;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Integer getNumberOfMonths() {
		return numberOfMonths;
	}

	public void setNumberOfMonths(Integer numberOfMonths) {
		this.numberOfMonths = numberOfMonths;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public Boolean getIsComplete() {
		return isComplete;
	}

	public void setIsComplete(Boolean isComplete) {
		this.isComplete = isComplete;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public List<DtoEmployeeMonthlyInstallment> getDtoEmployeeMonthlyInstallmentList() {
		return dtoEmployeeMonthlyInstallmentList;
	}

	public void setDtoEmployeeMonthlyInstallmentList(List<DtoEmployeeMonthlyInstallment> dtoEmployeeMonthlyInstallmentList) {
		this.dtoEmployeeMonthlyInstallmentList = dtoEmployeeMonthlyInstallmentList;
	}
	
}
