package com.bti.hcm.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.BuildChecks;
import com.bti.hcm.model.Default;
import com.bti.hcm.model.Distribution;
import com.bti.hcm.model.dto.DtoDefault;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryDefault;
import com.bti.hcm.repository.RepositoryDistribution;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeMaster;

@Service("/serviceDefault")
public class ServiceDefault {
	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceDefault.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryBuildChecks repositoryBuildChecks;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired(required=false)
	RepositoryDefault repositoryDefault;
	
	@Autowired(required=false)
	RepositoryDistribution repositoryDistribution;
	
	
	
	@Async
	public DtoDefault saveOrUpdate(DtoDefault dtoDefault) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Default defaults=null;
			if(dtoDefault.getId()!=null && dtoDefault.getId()>0) {
				defaults = repositoryDefault.findByIdAndIsDeleted(dtoDefault.getId(), false);
				defaults.setUpdatedBy(loggedInUserId);
				defaults.setUpdatedDate(new Date());
			} else {
				defaults = new Default();
				defaults.setCreatedDate(new Date());
				Integer rowId = repositoryDefault.findAll().size();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				defaults.setRowId(increment);
			}
			defaults.setDefaultID(dtoDefault.getDefaultID());
			defaults.setDesc(dtoDefault.getDesc());
			defaults.setArabicDesc(dtoDefault.getArabicDesc());
			repositoryDefault.saveAndFlush(defaults);
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDefault;
	}
	
	public DtoDefault delete(List<Integer> ids) {
		log.info("delete Default Method");
		DtoDefault dtoDefault = new DtoDefault();
		try {
			dtoDefault.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DEFAULT_DELETED", false));
			dtoDefault.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DEFAULT_ASSOCIATED", false));
			List<DtoDefault> deleteDtoDefault = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_NOT_DELETE_ID_MESSAGE", false).getMessage());
			try {
				for (Integer planId : ids) {
					Default defaults = repositoryDefault.findOne(planId);
					if(defaults!=null && defaults.getListDistribution().isEmpty()) {
						DtoDefault dtoDefault2=new DtoDefault();
						if(defaults.getListBuildChecks().isEmpty()) {
							dtoDefault.setId(defaults.getId());
							repositoryDefault.deleteSingleDefault(true, loggedInUserId, planId);
							deleteDtoDefault.add(dtoDefault2);	
						}else {
							inValidDelete = true;
						}
		}else {
			inValidDelete = true;
		}
					}

				if(inValidDelete){
					dtoDefault.setMessageType(invlidDeleteMessage.toString());
				}
				if(!inValidDelete){
					dtoDefault.setMessageType("");
				}
				dtoDefault.setDelete(deleteDtoDefault);
			} 
			catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete Default"
					+ " :"+dtoDefault.getId());
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoDefault;
	}

	
	
	public DtoDefault getById(int id) {
		DtoDefault dtoDefault  = new DtoDefault();
		try {
			if (id > 0) {
				Default default1 = repositoryDefault.findByIdAndIsDeleted(id, false);
				if (default1 != null) {
					dtoDefault = new DtoDefault(default1);
				
					dtoDefault.setId(default1.getId());
					dtoDefault.setDefaultID(default1.getDefaultID());
					dtoDefault.setDesc(default1.getDesc());
					dtoDefault.setArabicDesc(default1.getArabicDesc());
					
					
					
				} 
			} else {
				dtoDefault.setMessageType("INVALID_ID");

			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoDefault;
	}	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		
		try {

			log.info("Default Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("defaultID")|| dtoSearch.getSortOn().equals("desc")|| dtoSearch.getSortOn().equals("arabicDesc")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				 
				dtoSearch.setTotalCount(this.repositoryDefault.predictiveDefaultSearchTotalCount("%"+searchWord+"%"));
				List<Default> defaultList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						defaultList = this.repositoryDefault.predictiveDefaultSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						defaultList = this.repositoryDefault.predictiveDefaultSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						defaultList = this.repositoryDefault.predictiveDefaultSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(defaultList != null && !defaultList.isEmpty()){
					List<DtoDefault> dtoDefaultList = new ArrayList<>();
					DtoDefault dtoDefault=null;
					for (Default defaults : defaultList) {
						dtoDefault = new DtoDefault(defaults);
						dtoDefault.setId(defaults.getId());
						dtoDefault.setDesc(defaults.getDesc());
						dtoDefault.setDefaultID(defaults.getDefaultID());
						dtoDefault.setArabicDesc(defaults.getArabicDesc());
						dtoDefaultList.add(dtoDefault);
						}
					dtoSearch.setRecords(dtoDefaultList);
						}
			log.debug("Search default Size is:"+dtoSearch.getTotalCount());
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	public DtoSearch searchDefaultIdForCalculateChecks(DtoSearch dtoSearch) {

		try {
			log.info("search  defaultId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
								
				List<Default> defaultList = this.repositoryDefault.predictiveSearchDefaultIdWithPagination("%"+searchWord+"%");
					if(!defaultList.isEmpty()) {
						if(!defaultList.isEmpty()){
							List<DtoDefault> dtoDefault = new ArrayList<>();
							DtoDefault dtoDefault1=null;
							for (Default default1 : defaultList) {
								
								if(default1.getListDistribution().isEmpty()) {
									List<BuildChecks> buildChecks = this.repositoryBuildChecks.findByDefauiltId(default1.getId());
									if(!buildChecks.isEmpty()) {
									dtoDefault1 = new DtoDefault(default1);
									dtoDefault1.setId(default1.getId());
									dtoDefault1.setDefaultID(default1.getDefaultID());
									dtoDefault1.setDesc(default1.getDesc());
									dtoDefault1.setCreatedBy(default1.getUpdatedBy());
									
										Date date = buildChecks.get(0).getCreatedDate();
										 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
									        String time = localDateFormat.format(date);
									        dtoDefault1.setBuildDate(date);
									        dtoDefault1.setBuildtime(time);
										
									
									dtoDefault1.setUserId(default1.getUpdatedBy());
									dtoDefault.add(dtoDefault1);
									}
								}
								
							}
							dtoSearch.setRecords(dtoDefault);
						}

						dtoSearch.setTotalCount(defaultList.size());
					}
				
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public DtoSearch searchDefaultId(DtoSearch dtoSearch) {

		try {
			log.info("search  defaultId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
								
				List<Default> defaultList = this.repositoryDefault.predictiveSearchDefaultIdWithPagination("%"+searchWord+"%");
					if(!defaultList.isEmpty()) {
						if(!defaultList.isEmpty()){
							List<DtoDefault> dtoDefault = new ArrayList<>();
							DtoDefault dtoDefault1=null;
							for (Default default1 : defaultList) {
								List<BuildChecks> buildChecks = this.repositoryBuildChecks.findByDefauiltId(default1.getId());
								dtoDefault1 = new DtoDefault(default1);
								dtoDefault1.setId(default1.getId());
								dtoDefault1.setDefaultID(default1.getDefaultID());
								dtoDefault1.setDesc(default1.getDesc());
								dtoDefault1.setCreatedBy(default1.getUpdatedBy());
								if(!buildChecks.isEmpty()) {
									Date date = buildChecks.get(0).getCreatedDate();
									 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
								        String time = localDateFormat.format(date);
								        dtoDefault1.setBuildDate(date);
								        dtoDefault1.setBuildtime(time);
									
								}
								dtoDefault1.setUserId(default1.getUpdatedBy());
								dtoDefault.add(dtoDefault1);
							}
							dtoSearch.setRecords(dtoDefault);
						}

						dtoSearch.setTotalCount(defaultList.size());
					}
				
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	@Transactional
	@Async
	public List<DtoDefault> getAllDefaultDropDown() {
		log.info("getAllDefaultDropDown  Method");
		List<DtoDefault> dtoDefaultList = new ArrayList<>();
		try {
			List<Default> list = repositoryDefault.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (Default defaults : list) {
					
					//List<Distribution> listDefaut = repositoryDistribution.getAllByDefaultId(defaults.getId());
					
					/*if(listDefaut.isEmpty()) {*/
						DtoDefault dtoDefault = new DtoDefault();
						dtoDefault.setId(defaults.getId());
						dtoDefault.setDefaultID(defaults.getDefaultID());
						dtoDefault.setDesc(defaults.getDesc());
						dtoDefaultList.add(dtoDefault);	
					/*}*/
				
				}
			}
			log.debug("DefaultId is:"+dtoDefaultList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDefaultList;
	}

	
	public DtoDefault repeatByDefaultId(String defaultID) {
		log.info("repeatByDefaultId Method");
		DtoDefault dtoDefault = new DtoDefault();
		try {
			List<Default> defaults=repositoryDefault.findByDefaultId(defaultID.trim());
			if(defaults!=null && !defaults.isEmpty()) {
				dtoDefault.setIsRepeat(true);
			}else {
				dtoDefault.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoDefault;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllForCalculateCheck(DtoSearch dtoSearch) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {

			log.info("Default Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equalsIgnoreCase("defaultID") || dtoSearch.getSortOn().equalsIgnoreCase("desc") || dtoSearch.getSortOn().equalsIgnoreCase("arabicDesc")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				
				dtoSearch.setTotalCount(this.repositoryDefault.predictiveDefaultSearchTotalCount("%"+searchWord+"%"));
				List<Default> defaultList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						defaultList = this.repositoryDefault.predictiveDefaultSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equalsIgnoreCase("ASC")){
						defaultList = this.repositoryDefault.predictiveDefaultSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equalsIgnoreCase("DESC")){
						defaultList = this.repositoryDefault.predictiveDefaultSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
					
					//defaultList = repositoryDefault.findByIsDeleted(false);
					dtoSearch.setTotalCount(repositoryDefault.predictiveDefaultSearchWithPaginationCount("%"+searchWord+"%"));
				}
				if(defaultList != null && !defaultList.isEmpty()){
					List<DtoDefault> dtoDefaultList = new ArrayList<>();
					DtoDefault dtoDefault=null;
					for (Default defaults : defaultList) {
						
						
							dtoDefault = new DtoDefault(defaults);
							dtoDefault.setId(defaults.getId());
							dtoDefault.setDesc(defaults.getDesc());
							dtoDefault.setDefaultID(defaults.getDefaultID());
							dtoDefault.setUserId(defaults.getUpdatedBy());
							dtoDefault.setUserId(loggedInUserId);
							if(!defaults.getListBuildChecks().isEmpty()) {
								Date date = defaults.getListBuildChecks().get(0).getCreatedDate();
								 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
							        String time = localDateFormat.format(date);
							        dtoDefault.setBuildDate(date);
							        dtoDefault.setBuildtime(time);
//							        if(defaults.getListDistribution() != null && !defaults.getListDistribution().isEmpty()) {
//										for (Distribution distribution : defaults.getListDistribution()) {
//											dtoDefault.setPostingDate(distribution.getPostingDate());
//										}
//									}
							}
							dtoDefault.setArabicDesc(defaults.getArabicDesc());
							dtoDefaultList.add(dtoDefault);
								
						
						
						}
					dtoSearch.setRecords(dtoDefaultList);
						}
			log.debug("Search default Size is:"+dtoSearch.getTotalCount());
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
}
