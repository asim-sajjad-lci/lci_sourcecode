package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.ShiftCode;

public class DtoShiftCode extends DtoBase{

	private Integer id;
	private String shiftCodeId;
	private String desc;
	private String arabicDesc;
	private boolean inActive;
	private short shitPremium;
	private BigDecimal amount;
	private BigDecimal percent;
	private List<DtoShiftCode> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getShiftCodeId() {
		return shiftCodeId;
	}
	public void setShiftCodeId(String shiftCodeId) {
		this.shiftCodeId = shiftCodeId;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getArabicDesc() {
		return arabicDesc;
	}
	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}
	public boolean isInActive() {
		return inActive;
	}
	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}
	public short getShitPremium() {
		return shitPremium;
	}
	public void setShitPremium(short shitPremium) {
		this.shitPremium = shitPremium;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getPercent() {
		return percent;
	}
	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}
	public List<DtoShiftCode> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoShiftCode> delete) {
		this.delete = delete;
	}
	
	public DtoShiftCode() {
	}

	public DtoShiftCode(ShiftCode shiftCode) {
		
		this.shiftCodeId = shiftCode.getShiftCodeId();
	}
}
