package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.EmployeePayCodeMaintenance;

public class DtoEmployeePayCodeMaintenance extends DtoBase{
	private Integer id;
	private String baseOnPayCode;
	private BigDecimal amount;
	private BigDecimal payFactory;
	private BigDecimal payRate;
	private Short unitOfPay;
	private Short payPeriod;
	private Boolean inactive;
	private Integer payCodeIdPrimary;
	private String payCodeId;
	private String description;
	private DtoEmployeeMaster employeeMaster;
	private DtoEmployeeDependent employeeDependent;
	private DtoPayCode payCode;
	private DtoPayCodeType payCodeType;
	private Integer baseOnPayCodeId;
	private BigDecimal baseOnPayCodeAmount;
	private String employeeLastName;
	private String employeeFirstName;

	private List<DtoEmployeePayCodeMaintenance> delete;
	
	private DtoBasedOnPayCode dtoBasedOnPayCode;

	public DtoEmployeePayCodeMaintenance(EmployeePayCodeMaintenance employeePayCodeMaintenance) {

	}

	public DtoEmployeePayCodeMaintenance() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBaseOnPayCode() {
		return baseOnPayCode;
	}

	public void setBaseOnPayCode(String baseOnPayCode) {
		this.baseOnPayCode = baseOnPayCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPayFactory() {
		return payFactory;
	}

	public void setPayFactory(BigDecimal payFactory) {
		this.payFactory = payFactory;
	}

	public BigDecimal getPayRate() {
		return payRate;
	}

	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}

	public Short getUnitOfPay() {
		return unitOfPay;
	}

	public void setUnitOfPay(Short unitOfPay) {
		this.unitOfPay = unitOfPay;
	}

	public Short getPayPeriod() {
		return payPeriod;
	}

	public void setPayPeriod(Short payPeriod) {
		this.payPeriod = payPeriod;
	}

	public Boolean getInactive() {
		return inactive;
	}

	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public List<DtoEmployeePayCodeMaintenance> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeePayCodeMaintenance> delete) {
		this.delete = delete;
	}


	

	public DtoEmployeeDependent getEmployeeDependent() {
		return employeeDependent;
	}

	public void setEmployeeDependent(DtoEmployeeDependent employeeDependent) {
		this.employeeDependent = employeeDependent;
	}

	public DtoPayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(DtoPayCode payCode) {
		this.payCode = payCode;
	}

	public DtoPayCodeType getPayCodeType() {
		return payCodeType;
	}

	public void setPayCodeType(DtoPayCodeType payCodeType) {
		this.payCodeType = payCodeType;
	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public String getEmployeeLastName() {
		return employeeLastName;
	}

	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public Integer getPayCodeIdPrimary() {
		return payCodeIdPrimary;
	}

	public void setPayCodeIdPrimary(Integer payCodeIdPrimary) {
		this.payCodeIdPrimary = payCodeIdPrimary;
	}

	public String getPayCodeId() {
		return payCodeId;
	}

	public void setPayCodeId(String payCodeId) {
		this.payCodeId = payCodeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DtoBasedOnPayCode getDtoBasedOnPayCode() {
		return dtoBasedOnPayCode;
	}

	public void setDtoBasedOnPayCode(DtoBasedOnPayCode dtoBasedOnPayCode) {
		this.dtoBasedOnPayCode = dtoBasedOnPayCode;
	}

	public Integer getBaseOnPayCodeId() {
		return baseOnPayCodeId;
	}

	public void setBaseOnPayCodeId(Integer baseOnPayCodeId) {
		this.baseOnPayCodeId = baseOnPayCodeId;
	}

	public BigDecimal getBaseOnPayCodeAmount() {
		return baseOnPayCodeAmount;
	}

	public void setBaseOnPayCodeAmount(BigDecimal baseOnPayCodeAmount) {
		this.baseOnPayCodeAmount = baseOnPayCodeAmount;
	}

	
	

}
