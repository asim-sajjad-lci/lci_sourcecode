package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBasedOnPayCode;
import com.bti.hcm.model.dto.DtoEmployeePayCodeMaintenance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeePayCodeMaintenance;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeePayCodeMaintenance")
public class ControllerEmployeePayCodeMaintenance extends BaseController {

	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeePayCodeMaintenance.class);

	@Autowired
	ServiceEmployeePayCodeMaintenance serviceEmployeePayCodeMaintenance;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,
			@RequestBody DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance) throws Exception {
		LOGGER.info("Create EmployeePayCodeMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeePayCodeMaintenance = serviceEmployeePayCodeMaintenance
					.saveOrUpdate(dtoEmployeePayCodeMaintenance);
			responseMessage = displayMessage(dtoEmployeePayCodeMaintenance, "EMPLOYEE_PAY_CODET_MAINTENENCE_CREATED",
					"EMPLOYEE_PAY_CODET_MAINTENENCE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeePayCodeMaintenance Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request,
			@RequestBody DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance) throws Exception {
		LOGGER.info("Update EmployeePayCodeMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeePayCodeMaintenance = serviceEmployeePayCodeMaintenance
					.saveOrUpdate(dtoEmployeePayCodeMaintenance);
			responseMessage = displayMessage(dtoEmployeePayCodeMaintenance, "EMPLOYEE_PAY_CODET_MAINTENENCE_UPDATED",
					"EMPLOYEE_PAY_CODET_MAINTENENCE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeePayCodeMaintenance Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request,
			@RequestBody DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance) throws Exception {
		LOGGER.info("Delete EmployeePayCodeMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeePayCodeMaintenance.getIds() != null && !dtoEmployeePayCodeMaintenance.getIds().isEmpty()) {
				DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance2 = serviceEmployeePayCodeMaintenance
						.delete(dtoEmployeePayCodeMaintenance.getIds());
				if (dtoEmployeePayCodeMaintenance2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(
							HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
									.getMessageByShortAndIsDeleted("EMPLOYEE_PAY_CODET_MAINTENENCE_DELETED", false),
							dtoEmployeePayCodeMaintenance2);
				} else {
					responseMessage = new ResponseMessage(
							HttpStatus.FOUND.value(), HttpStatus.FOUND, serviceResponse
									.getMessageByShortAndIsDeleted("EMPLOYEE_PAY_CODET_MAINTENENCE_NOT_DELETED", false),
							dtoEmployeePayCodeMaintenance2);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_PAY_CODET_MAINTENENCE_LIST_IS_EMPTY",
								false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeePayCodeMaintenance Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request,
			@RequestBody DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenanceObj = serviceEmployeePayCodeMaintenance
					.getById(dtoEmployeePayCodeMaintenance.getId());
			responseMessage = displayMessage(dtoEmployeePayCodeMaintenanceObj,
					"EMPLOYEE_PAY_CODET_MAINTENENCE_LIST_GET_DETAIL", "EMPLOYEE_PAY_CODET_MAINTENENCE_NOT_GETTING",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:" + dtoEmployeePayCodeMaintenance.getId());
		return responseMessage;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeBenefitMaintenance(@RequestBody DtoSearch dtoSearch,
			HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeePayCodeMaintenances Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePayCodeMaintenance.search(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "EMPLOYEE_PAY_CODET_MAINTENENCE_GET_ALL",
					"EMPLOYEE_PAY_CODET_MAINTENENCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if (dtoSearch != null) {
			LOGGER.debug("Search EmployeePayCodeMaintenancess Method:" + dtoSearch.getTotalCount());
		}

		return responseMessage;
	}

	@RequestMapping(value = "/getAllEmployeePaycodeMainteneceDropDownList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllEmployeePaycodeMainteneceDropDownList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeePayCodeMaintenance> employeePayCodeMaintenanceList = serviceEmployeePayCodeMaintenance
						.getAllEmployeePaycodeMainteneceDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_GET_ALL", false),
						employeePayCodeMaintenanceList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return responseMessage;
	}

	@RequestMapping(value = "/checkDuplicate", method = RequestMethod.POST)
	public ResponseMessage payCodeIdCheck(HttpServletRequest request,
			@RequestBody DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance) throws Exception {
		LOGGER.info("PayCodeIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeePayCodeMaintenance dtoPaycode = serviceEmployeePayCodeMaintenance
					.repeatByEmployeeId(dtoEmployeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId());
			responseMessage = displayMessage(dtoPaycode, "PAYCODE_MAINTAIN_EXIST",
					"PAYCODE_MAINTAIN_REPEAT_PAYCODE_MAINTAIN_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/findCodeByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage findCodeByEmployeeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("Search EmployeePayCodeMaintenancess Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePayCodeMaintenance.findCodeByEmployeeId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "EMPLOYEE_PAY_CODET_MAINTENENCE_GET_ALL",
					"EMPLOYEE_PAY_CODET_MAINTENENCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if (dtoSearch != null) {
			LOGGER.debug("Search EmployeePayCodeMaintenancess Method:" + dtoSearch.getTotalCount());
		}

		return responseMessage;
	}

	@RequestMapping(value = "/findBasedOnCodeByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage findBasedOnCodeByEmployeeId(@RequestBody DtoBasedOnPayCode dtoBasedOnPayCode,
			HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeePayCodeMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBasedOnPayCode = this.serviceEmployeePayCodeMaintenance.findBasedOnCodeByEmployeeId(dtoBasedOnPayCode);
			responseMessage = displayMessage(dtoBasedOnPayCode, "EMPLOYEE_PAY_CODET_MAINTENENCE_GET_ALL",
					"EMPLOYEE_PAY_CODET_MAINTENENCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if (dtoBasedOnPayCode != null) {
			LOGGER.debug("Search EmployeePayCodeMaintenance Method:" + dtoBasedOnPayCode.getTotalCount());
		}

		return responseMessage;
	}

	@RequestMapping(value = "/findBasedOnCodeByEmployeeIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage findBasedOnCodeByEmployeeId1(@RequestBody DtoBasedOnPayCode dtoBasedOnPayCode,
			HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeePayCodeMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBasedOnPayCode = this.serviceEmployeePayCodeMaintenance.findBasedOnCodeByEmployeeId1(dtoBasedOnPayCode);
			responseMessage = displayMessage(dtoBasedOnPayCode, "EMPLOYEE_PAY_CODET_MAINTENENCE_GET_ALL",
					"EMPLOYEE_PAY_CODET_MAINTENENCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if (dtoBasedOnPayCode != null) {
			LOGGER.debug("Search EmployeePayCodeMaintenance Method:" + dtoBasedOnPayCode.getTotalCount());
		}

		return responseMessage;
	}

	@RequestMapping(value = "/CheckDuplicateByEmployeeIdAndPayCodeId", method = RequestMethod.POST)
	public ResponseMessage CheckDuplicateByEmployeeIdAndPayCodeId(HttpServletRequest request,
			@RequestBody DtoBasedOnPayCode dtoBasedOnPayCode) throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBasedOnPayCode = serviceEmployeePayCodeMaintenance
					.checkDuplicateByEmployeeIdAndPayCodeId(dtoBasedOnPayCode);
			if (dtoBasedOnPayCode != null && dtoBasedOnPayCode.getIsRepeat()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("PAYCODE_MAINTAIN_EXIST", false),
						dtoBasedOnPayCode);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(
								"PAYCODE_MAINTAIN_REPEAT_PAYCODE_MAINTAIN_NOT_FOUND", false),
						dtoBasedOnPayCode);
			}
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

}
