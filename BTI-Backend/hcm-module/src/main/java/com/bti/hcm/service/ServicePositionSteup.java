package com.bti.hcm.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bti.hcm.model.Position;
import com.bti.hcm.model.PositionSetup;
import com.bti.hcm.model.dto.DtoPositionSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositoryPositionSteup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("servicePositionSteup")
public class ServicePositionSteup {

	/**
	 * @Description LOGGER use for put a logger in ServicePositionSteup Service
	 */
	static Logger log = Logger.getLogger(ServicePositionSteup.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in ServicePositionSteup service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServicePositionSteup service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in ServicePositionSteup service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPositionSteup Autowired here using annotation of spring for access of repositoryPositionSteup method in PositionSteup service
	 * 				In short Access PositionSteup Query from Database using repositoryPositionSteup.
	 */
	
	@Autowired(required=false)
	RepositoryPositionSteup repositoryPositionSteup;
	
	/**
	 * @Description repositoryPosition Autowired here using annotation of spring for access of repositoryPosition method in Position service
	 * 				In short Access Position Query from Database using repositoryPosition.
	 */
	@Autowired(required=false)
	RepositoryPosition repositoryPosition;

	/**
	 * @param dtoPositionSetup
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public DtoPositionSetup saveOrUpdatePositionSetup(DtoPositionSetup dtoPositionSetup,MultipartFile file) throws IOException {
		log.info("saveOrUpdatePosition Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		PositionSetup positionSetup = null;
		if (dtoPositionSetup.getId() != null && dtoPositionSetup.getId() > 0) {
			
			positionSetup = repositoryPositionSteup.findByIdAndIsDeleted(dtoPositionSetup.getId(), false);
			positionSetup.setUpdatedBy(loggedInUserId);
			positionSetup.setUpdatedDate(new Date());
		} else {
			positionSetup = new PositionSetup();
			positionSetup.setCreatedDate(new Date());
			
			Integer rowId = repositoryPositionSteup.getCountOfTotaPositionSteup();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			positionSetup.setRowId(increment);
		}
		Position position =  repositoryPosition.findOne(Integer.valueOf(dtoPositionSetup.getPositionIds()));
		positionSetup.setAttachmentType(dtoPositionSetup.getAttachmentType());
		positionSetup.setAttachmentTime(new Date());
		positionSetup.setAttachmentDate(new Date());
		if(file!=null){
			positionSetup.setAttachment(file.getBytes());
			positionSetup.setDocumentAttachmentName(file.getOriginalFilename());
			positionSetup.setFileType(FilenameUtils.getExtension(file.getOriginalFilename()));
		}else {
			positionSetup.setAttachment(null);
		}
		positionSetup.setUpdatedBy(loggedInUserId);
		positionSetup.setPosition(position);
		positionSetup.setDocumentAttachmenDesc(dtoPositionSetup.getDocumentAttachmenDesc());
		positionSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		positionSetup.setRowId(loggedInUserId);
		positionSetup = repositoryPositionSteup.saveAndFlush(positionSetup);
		log.debug("Position is:"+positionSetup.getId());
		return dtoPositionSetup;
	}
	/**
	 * @param dtoPositionSetup
	 * @return
	 */
	public DtoSearch getAllPostionSteup(DtoSearch dtoSearch) {
		log.info("getAllPostionSteup Method");
		
		
		
		
		
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				if(dtoSearch.getSortOn().equals("attachmentDate") || dtoSearch.getSortOn().equals("attachmentTime") || dtoSearch.getSortOn().equals("attachmentType") || dtoSearch.getSortOn().equals("documentAttachmenDesc")
						|| dtoSearch.getSortOn().equals("positionIds")|| dtoSearch.getSortOn().equals("userId")
						) {
					
					if(dtoSearch.getSortOn().equals("positionIds")) {
						dtoSearch.setSortOn("position.positionId");
					}
					if(dtoSearch.getSortOn().equals("userId")) {
						dtoSearch.setSortOn("updatedBy");
					}
					condition = dtoSearch.getSortOn();
				
			}else {
				condition = "id";
			}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
			
		}
		
		
			
			dtoSearch.setTotalCount(this.repositoryPositionSteup.findAttachmentByPositionIdCount("%"+searchWord+"%",dtoSearch.getId()));
			List<PositionSetup> skillPositionList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					skillPositionList = this.repositoryPositionSteup.findAttachmentByPositionId("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					skillPositionList = this.repositoryPositionSteup.findAttachmentByPositionId("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					skillPositionList = this.repositoryPositionSteup.findAttachmentByPositionId("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					
				}
				
			}
		
		
		
		
		List<DtoPositionSetup> dtoPOsitionList=new ArrayList<>();
		if(skillPositionList!=null && !skillPositionList.isEmpty())
		{
			for (PositionSetup position : skillPositionList) 
			{
				DtoPositionSetup	dtoPositionSetup=new DtoPositionSetup(position);
				
				dtoPositionSetup.setAttachmentType(position.getAttachmentType());
				if(position.getPosition()!=null){
					dtoPositionSetup.setPositionId(position.getPosition().getId());	
					dtoPositionSetup.setPositionIds(String.valueOf(position.getPosition().getId()));
				}
				dtoPositionSetup.setUserId(position.getUpdatedBy());
				dtoPositionSetup.setAttachmentType(position.getAttachmentType());
				dtoPositionSetup.setAttachmentDate(position.getAttachmentDate());
				dtoPositionSetup.setAttachmentTime(position.getAttachmentTime());
				dtoPositionSetup.setDocumentAttachmenDesc(position.getDocumentAttachmenDesc());
				dtoPositionSetup.setDocumentAttachmentName(position.getDocumentAttachmentName());
				dtoPositionSetup.setId(position.getId());
				dtoPositionSetup.setUserId(position.getUpdatedBy());
				dtoPOsitionList.add(dtoPositionSetup);
			}
			dtoSearch.setRecords(dtoPOsitionList);
		}
		log.debug("All Postion Steup List Size is:"+dtoSearch.getTotalCount());
		
		}
		return dtoSearch;
	}

	/**
	 * @param ids
	 * @return
	 */
	public DtoPositionSetup deletePositionSteup(List<Integer> ids) {
		log.info("deletePosition Method");
		DtoPositionSetup dtoPosition = new DtoPositionSetup();
		dtoPosition
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("POSITION_DELETED", false));
		dtoPosition.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("POSTION_ASSOCIATED", false));
		List<DtoPositionSetup> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer positionId : ids) {
				PositionSetup position = repositoryPositionSteup.findOne(positionId);
				DtoPositionSetup dtoskillSetup2 = new DtoPositionSetup();
				dtoskillSetup2.setId(positionId);
				dtoskillSetup2.setDocumentAttachmenDesc(position.getDocumentAttachmenDesc());
				dtoskillSetup2.setDocumentAttachmentName(position.getDocumentAttachmentName());
				repositoryPositionSteup.deleteSinglePosition(true, loggedInUserId, positionId);
				deletePosition.add(dtoskillSetup2);

			}
			dtoPosition.setDeletePositionSetup(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete position Setup :"+dtoPosition.getId());
		return dtoPosition;
	}

	/**
	 * @param id
	 * @return
	 */
	public DtoPositionSetup getByPositionSetupId(Integer id) {
		log.info("getByPositionId Method");
		DtoPositionSetup dtoPosition = new DtoPositionSetup();
		if (id > 0) {
			PositionSetup position = repositoryPositionSteup.findByIdAndIsDeleted(id, false);
			if (position != null) {
				dtoPosition = new DtoPositionSetup(position);
			} else {
				dtoPosition.setMessageType("POSITION_LIST_NOT_GETTING");

			}
		} else {
			dtoPosition.setMessageType("POSITION_LIST_NOT_GETTING");

		}
		log.debug("position By Id is:"+dtoPosition.getId());
		return dtoPosition;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchPositionSetup(DtoSearch dtoSearch) {
		log.info("searchPositionSetup Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			String condition = "";

			if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

				if (dtoSearch.getSortOn().equals("locationId") || dtoSearch.getSortOn().equals("description")
						|| dtoSearch.getSortOn().equals("arabicDescription") || dtoSearch.getSortOn().equals("contactName")
						|| dtoSearch.getSortOn().equals("locationAddress") || dtoSearch.getSortOn().equals("phone")
						|| dtoSearch.getSortOn().equals("fax")|| dtoSearch.getSortOn().equals("city")) {
					
					if(dtoSearch.getSortOn().equals("cityName")) {
						dtoSearch.setSortOn("city");
					}
					condition = dtoSearch.getSortOn();
				} else {
					condition = "id";
				}

			} else {
				condition += "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");

			}
			
			
			dtoSearch.setTotalCount(this.repositoryPositionSteup.predictivePositionSetupSearchTotalCount("%"+searchWord+"%"));
			List<PositionSetup> positionSetupList =null;
			/*if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				positionSetupList = this.repositoryPositionSteup.predictivePositionSearchSetupWithPagination("%"+searchWord+"%");
			}else {*/
				positionSetupList = this.repositoryPositionSteup.predictivePositionSearchSetupWithPagination("%"+searchWord+"%");
			/*}*/
		
			if(positionSetupList != null && !positionSetupList.isEmpty()){
				List<DtoPositionSetup> dtoPositionSetupList = new ArrayList<>();
				DtoPositionSetup dtoPositionSetup=null;
				for (PositionSetup positionSetup : positionSetupList) {
					dtoPositionSetup = new DtoPositionSetup(positionSetup);
					
					
					if(positionSetup.getPosition()!=null){
						dtoPositionSetup.setPositionId(positionSetup.getPosition().getId());	
					}
					dtoPositionSetup.setUserId(positionSetup.getUpdatedBy());
					dtoPositionSetup.setAttachmentType(positionSetup.getAttachmentType());
					dtoPositionSetup.setAttachmentDate(positionSetup.getAttachmentDate());
					dtoPositionSetup.setAttachmentTime(positionSetup.getAttachmentTime());
					dtoPositionSetup.setDocumentAttachmenDesc(positionSetup.getDocumentAttachmenDesc());
					dtoPositionSetup.setDocumentAttachmentName(positionSetup.getDocumentAttachmentName());
					dtoPositionSetup.setId(positionSetup.getId());
					
					dtoPositionSetup.setDocumentAttachmenDesc(positionSetup.getDocumentAttachmenDesc());
					dtoPositionSetup.setDocumentAttachmentName(positionSetup.getDocumentAttachmentName());
					dtoPositionSetupList.add(dtoPositionSetup);
				}
				dtoSearch.setRecords(dtoPositionSetupList);
			}
		}
		log.debug("Search searchPositionSetup Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	/**
	 * @param id
	 * @return
	 */
	public PositionSetup getAttachment(Integer id) {
		
		PositionSetup position = repositoryPositionSteup.findByIdAndIsDeleted(id, false);
		
		if(position!=null){
			return position;
		}
			PositionSetup position1 = new PositionSetup();
			return position1;
		
		
		
	}
}
