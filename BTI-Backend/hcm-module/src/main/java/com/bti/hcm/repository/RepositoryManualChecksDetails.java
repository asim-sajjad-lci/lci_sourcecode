package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecksDetails;

@Repository("/repositoryManualChecksDetails")
public interface RepositoryManualChecksDetails extends JpaRepository<ManualChecksDetails, Integer>{

	ManualChecksDetails findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from ManualChecksDetails m ")
	Integer getCountOfTotaLocation();

}
