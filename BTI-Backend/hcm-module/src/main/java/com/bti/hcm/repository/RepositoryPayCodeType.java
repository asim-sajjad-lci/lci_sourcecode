package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayCodeType;

@Repository("repositoryPayCodeType")
public interface RepositoryPayCodeType extends JpaRepository<PayCodeType, Integer>{

	public List<PayCodeType> findAll();

	public List<PayCodeType> findByIsDeleted(Boolean deleted);
}
