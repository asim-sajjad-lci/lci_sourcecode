package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.Supervisor;
import com.bti.hcm.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoSupervisor extends DtoBase{

	private Integer id;
	String superVisionCode;
	String description;
	String arabicDescription;
	String employeeName;
	private Integer employeeId;
	private String employeeIds;
	private String employeeMiddleName;
	private List<DtoSupervisor> deletesupervisor;
	private List<DtoEmployeeMaster> listDtoEmployeeMaster;
	private DtoEmployeeMaster employeeMaster;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSuperVisionCode() {
		return superVisionCode;
	}

	public void setSuperVisionCode(String superVisionCode) {
		this.superVisionCode = superVisionCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	public List<DtoSupervisor> getDeletesupervisor() {
		return deletesupervisor;
	}

	public void setDeletesupervisor(List<DtoSupervisor> deletesupervisor) {
		this.deletesupervisor = deletesupervisor;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public DtoSupervisor() {

	}

	public DtoSupervisor(Supervisor supervisor) {

		if (UtilRandomKey.isNotBlank(supervisor.getArabicDescription())) {
			this.arabicDescription = supervisor.getArabicDescription();
		} else {
			this.arabicDescription = "";
		}

		if (UtilRandomKey.isNotBlank(supervisor.getDescription())) {
			this.description = supervisor.getDescription();
		} else {
			this.description = "";
		}

		if (UtilRandomKey.isNotBlank(supervisor.getSuperVisionCode())) {
			this.superVisionCode = supervisor.getSuperVisionCode();
		} else {
			this.superVisionCode = "";
		}
		if (UtilRandomKey.isNotBlank(supervisor.getEmployeeName())) {
			this.employeeName = supervisor.getEmployeeName();
		} else {
			this.employeeName = "";
		}

	}

	public List<DtoEmployeeMaster> getListDtoEmployeeMaster() {
		return listDtoEmployeeMaster;
	}

	public void setListDtoEmployeeMaster(List<DtoEmployeeMaster> listDtoEmployeeMaster) {
		this.listDtoEmployeeMaster = listDtoEmployeeMaster;
	}

	public String getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(String employeeIds) {
		this.employeeIds = employeeIds;
	}

	public String getEmployeeMiddleName() {
		return employeeMiddleName;
	}

	public void setEmployeeMiddleName(String employeeMiddleName) {
		this.employeeMiddleName = employeeMiddleName;
	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

}
