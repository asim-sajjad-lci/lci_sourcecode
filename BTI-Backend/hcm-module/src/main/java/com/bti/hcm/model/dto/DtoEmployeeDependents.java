package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.EmployeeDependents;

public class DtoEmployeeDependents extends DtoBase{
	private Integer id;
	private String empRelationshipId;
	private String desc;
	private String arabicDesc;
	private List<DtoEmployeeDependents> delete;

	public DtoEmployeeDependents(EmployeeDependents employeeDependents) {
	}

	public DtoEmployeeDependents() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmpRelationshipId() {
		return empRelationshipId;
	}

	public void setEmpRelationshipId(String empRelationshipId) {
		this.empRelationshipId = empRelationshipId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}


	public List<DtoEmployeeDependents> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeeDependents> delete) {
		this.delete = delete;
	}


}
