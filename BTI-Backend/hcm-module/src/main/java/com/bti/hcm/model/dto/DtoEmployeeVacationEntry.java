package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeeVacationDependant;
import com.bti.hcm.model.EmployeeVactionTicket;

public class DtoEmployeeVacationEntry {

	private Integer idx;
	private Integer workflowRequestId;
	private String applyingfor;
	private Boolean reserveForEmployee;
	private String finalDestination;
	private Boolean withTickets;
	private String typeOfVacation;
	private Boolean vacationOutsideCountry;
	private Date fromDate;
	private Date toDate;
	private String replacementEmployeeName;
	private Boolean needVisas;
	private String visaType;
	private Double numberOfDays;
	private EmployeeMaster employeeMaster;
	private EmployeeVactionTicket employeeVactionTicket;
	private List<EmployeeVacationDependant> employeeVacationDependantsList;

	private String employeeCode;
	
	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}


	public Integer getWorkflowRequestId() {
		return workflowRequestId;
	}


	public void setWorkflowRequestId(Integer workflowRequestId) {
		this.workflowRequestId = workflowRequestId;
	}


	public String getApplyingfor() {
		return applyingfor;
	}


	public void setApplyingfor(String applyingfor) {
		this.applyingfor = applyingfor;
	}


	public Boolean getReserveForEmployee() {
		return reserveForEmployee;
	}


	public void setReserveForEmployee(Boolean reserveForEmployee) {
		this.reserveForEmployee = reserveForEmployee;
	}


	public String getFinalDestination() {
		return finalDestination;
	}


	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}


	public Boolean getWithTickets() {
		return withTickets;
	}


	public void setWithTickets(Boolean withTickets) {
		this.withTickets = withTickets;
	}


	public String getTypeOfVacation() {
		return typeOfVacation;
	}


	public void setTypeOfVacation(String typeOfVacation) {
		this.typeOfVacation = typeOfVacation;
	}


	public Boolean getVacationOutsideCountry() {
		return vacationOutsideCountry;
	}


	public void setVacationOutsideCountry(Boolean vacationOutsideCountry) {
		this.vacationOutsideCountry = vacationOutsideCountry;
	}


	public Date getFromDate() {
		return fromDate;
	}


	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public Date getToDate() {
		return toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	public String getReplacementEmployeeName() {
		return replacementEmployeeName;
	}


	public void setReplacementEmployeeName(String replacementEmployeeName) {
		this.replacementEmployeeName = replacementEmployeeName;
	}


	public Boolean getNeedVisas() {
		return needVisas;
	}


	public void setNeedVisas(Boolean needVisas) {
		this.needVisas = needVisas;
	}


	public String getVisaType() {
		return visaType;
	}


	public void setVisaType(String visaType) {
		this.visaType = visaType;
	}


	public Double getNumberOfDays() {
		return numberOfDays;
	}


	public void setNumberOfDays(Double numberOfDays) {
		this.numberOfDays = numberOfDays;
	}


	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}


	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}


	public List<EmployeeVacationDependant> getEmployeeVacationDependant() {
		return employeeVacationDependantsList;
	}


	public void setEmployeeVacationDependant(List<EmployeeVacationDependant> employeeVacationDependant) {
		this.employeeVacationDependantsList = employeeVacationDependant;
	}
	
	public EmployeeVactionTicket getEmployeeVactionTicket() {
		return employeeVactionTicket;
	}


	public void setEmployeeVactionTicket(EmployeeVactionTicket employeeVactionTicket) {
		this.employeeVactionTicket = employeeVactionTicket;
	}
	
	
	
}
