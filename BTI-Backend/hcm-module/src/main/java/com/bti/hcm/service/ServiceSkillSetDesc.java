package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.SkillSetDesc;
import com.bti.hcm.model.SkillSetSetup;
import com.bti.hcm.model.SkillsSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSkillSetDesc;
import com.bti.hcm.model.dto.DtoSkillSteup;
import com.bti.hcm.repository.RepositorySkillSetDesc;
import com.bti.hcm.repository.RepositorySkillSetSetup;
import com.bti.hcm.repository.RepositorySkillSteps;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceSkillSetDesc")
public class ServiceSkillSetDesc {
	/**
	 * @Description LOGGER use for put a logger in SkillSetDesc Service
	 */
	static Logger log = Logger.getLogger(ServiceSkillSetDesc.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in SkillSetDesc service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in SkillSetDesc service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in SkillSetDesc service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description RepositorySkillSetSetup Autowired here using annotation of
	 *              spring for access of RepositorySkillSetSetup method in
	 *              SkillSetSetup service In short Access SkillSetSetup Query from
	 *              Database using RepositorySkillSetSetup.
	 */
	@Autowired
	RepositorySkillSetDesc repositorySkillSetDesc;

	@Autowired
	RepositorySkillSetSetup repositorySkillSetSetup;

	@Autowired
	RepositorySkillSteps repositorySkillSteps;

	/**
	 * @param dtoSkillSetDesc
	 * @return
	 */
	public DtoSkillSetDesc saveOrUpdate(DtoSkillSetDesc dtoSkillSetDesc) {
		log.info("saveOrUpdateSkill Desc Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		SkillSetDesc skillSetDesc = null;
		if (dtoSkillSetDesc.getId() != null && dtoSkillSetDesc.getId() > 0) {
			skillSetDesc = repositorySkillSetDesc.findByIdAndIsDeleted(dtoSkillSetDesc.getId(), false);
			skillSetDesc.setUpdatedBy(loggedInUserId);
			skillSetDesc.setUpdatedDate(new Date());
		} else {
			skillSetDesc = new SkillSetDesc();
			skillSetDesc.setCreatedDate(new Date());

			Integer rowId = repositorySkillSetDesc.getCountOfTotalSkillSetDesc();
			Integer increment = 0;
			if (rowId != 0) {
				increment = rowId + 1;
			} else {
				increment = 1;
			}
			skillSetDesc.setRowId(increment);
		}

		SkillSetSetup skillSetSetup = new SkillSetSetup();
		SkillsSetup skillsSetup = new SkillsSetup();
		if (dtoSkillSetDesc.getSkillsSetId() != null && dtoSkillSetDesc.getSkillId() != null) {
			skillSetSetup = repositorySkillSetSetup.findOne(dtoSkillSetDesc.getSkillsSetId());
			skillsSetup = repositorySkillSteps.findOne(dtoSkillSetDesc.getSkillId());
		}

		skillSetDesc.setComment(dtoSkillSetDesc.getComment());
		skillSetDesc.setSkillsId(skillsSetup);
		skillSetDesc.setSkillSetSetup(skillSetSetup);
		skillSetDesc.setSkillRequired(dtoSkillSetDesc.isSkillRequired());
		skillSetDesc.setSkillSetSeqn(dtoSkillSetDesc.getSkillSetSeqn());
		skillSetDesc.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		// skillSetDesc.setRowId(loggedInUserId);
		skillSetDesc = repositorySkillSetDesc.saveAndFlush(skillSetDesc);
		log.debug("skillSetSetup Desc is:" + skillSetDesc.getId());
		return dtoSkillSetDesc;
	}

	/**
	 * @param dtoSkillSetDesc
	 * @return
	 */
	public DtoSearch getAll(DtoSkillSetDesc dtoSkillSetDesc) {
		log.info("getAllSkillSteup Desc Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSkillSetDesc.getPageNumber());
		dtoSearch.setPageSize(dtoSkillSetDesc.getPageSize());
		dtoSearch.setTotalCount(repositorySkillSetDesc.getCountOfTotalSkillSetSetup());
		List<SkillSetDesc> skillSteupList = null;
		if (dtoSkillSetDesc.getPageNumber() != null && dtoSkillSetDesc.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSkillSetDesc.getPageNumber(), dtoSkillSetDesc.getPageSize(),
					Direction.DESC, "createdDate");
			skillSteupList = repositorySkillSetDesc.findByIsDeleted(false, pageable);
		} else {
			skillSteupList = repositorySkillSetDesc.findByIsDeletedOrderByCreatedDateDesc(false);
		}

		List<DtoSkillSetDesc> dtoSkillSteupList = new ArrayList<>();
		if (skillSteupList != null && !skillSteupList.isEmpty()) {
			for (SkillSetDesc skillsSetup : skillSteupList) {
				dtoSkillSetDesc = new DtoSkillSetDesc(skillsSetup);

				if (skillsSetup.getSkillsId() != null) {
					DtoSkillSteup dtoSkillSteup = new DtoSkillSteup();

					dtoSkillSteup.setId(skillsSetup.getSkillsId().getId());
					dtoSkillSteup.setSkillId(skillsSetup.getSkillsId().getSkillId());
					dtoSkillSteup.setSkillDesc(skillsSetup.getSkillsId().getSkillDesc());
					dtoSkillSteup.setFrequency(skillsSetup.getSkillsId().getFrequency());
					dtoSkillSteup.setCompensation(skillsSetup.getSkillsId().getCompensation());
					dtoSkillSetDesc.setSkillsId(dtoSkillSteup);
				}

				dtoSkillSetDesc.setSkillSetSetup(skillsSetup.getSkillSetSetup());
				dtoSkillSetDesc.setSkillRequired(skillsSetup.isSkillRequired());
				dtoSkillSetDesc.setSkillSetSeqn(skillsSetup.getSkillSetSeqn());
				dtoSkillSteupList.add(dtoSkillSetDesc);
			}
			dtoSearch.setRecords(dtoSkillSteupList);
		}
		log.debug("All Skill Set Desc List Size is:" + dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoSkillSetDesc delete(List<Integer> ids) {
		log.info("delete  SkillSetDesc Method");
		DtoSkillSetDesc dtoSkillSetDesc = new DtoSkillSetDesc();
		dtoSkillSetDesc
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SKILL_DETAIL_DELETED", false));
		dtoSkillSetDesc.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SKILL_DETAIL_DELETED", false));
		List<DtoSkillSetDesc> delete = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer skillStepId : ids) {
				SkillSetDesc skillsteup = repositorySkillSetDesc.findOne(skillStepId);
				if (skillsteup.getSkillsSetup().isEmpty()) {
					DtoSkillSetDesc dtoskillSetup2 = new DtoSkillSetDesc();
					dtoskillSetup2.setId(skillsteup.getId());
					repositorySkillSetDesc.deleteSingleSkillDesc(true, loggedInUserId, skillStepId);
					delete.add(dtoskillSetup2);
				}
			}
			dtoSkillSetDesc.setDeleteSkillSet(delete);
		} catch (NumberFormatException e) {

			log.error("Error in deleteSkillSteup Desc", e);
		}
		log.debug("Delete SkillSteup Desc :" + dtoSkillSetDesc.getId());
		return dtoSkillSetDesc;
	}

	/**
	 * @param id
	 * @return
	 */
	public DtoSkillSetDesc getById(Integer id) {
		log.info("getBySkillSteupId Method");
		DtoSkillSetDesc dtoSkillSteup = new DtoSkillSetDesc();
		if (id > 0) {
			SkillSetDesc skillSetDesc = repositorySkillSetDesc.findByIdAndIsDeleted(id, false);
			if (skillSetDesc != null) {
				dtoSkillSteup = new DtoSkillSetDesc(skillSetDesc);

				if (skillSetDesc.getSkillSetSetup() != null
						&& skillSetDesc.getSkillSetSetup().getSkillSetId() != null) {
					dtoSkillSteup.setSkillsSetPrimaryId(skillSetDesc.getSkillSetSetup().getId());
					// dtoSkillSteup.setSkillsSetId(Integer.parseInt(skillSetDesc.getSkillSetSetup().getSkillSetId()));
					dtoSkillSteup.setSkillSetId(skillSetDesc.getSkillSetSetup().getSkillSetId());
					dtoSkillSteup.setSkillSetDesc(skillSetDesc.getSkillSetSetup().getSkillSetDiscription());
					dtoSkillSteup.setSkillSetArbicDesc(skillSetDesc.getSkillSetSetup().getArabicDiscription());
				}

				if (skillSetDesc.getSkillsSetup() != null && !skillSetDesc.getSkillsSetup().isEmpty()) {
					List<Integer> listId = new ArrayList<>();
					for (SkillsSetup skillsSetup : skillSetDesc.getSkillsSetup()) {
						listId.add(skillsSetup.getId());
					}
					List<SkillsSetup> skillsSetup = repositorySkillSteps.findAllPayCodeListId(listId);
					if (!skillsSetup.isEmpty()) {
						List<DtoSkillSteup> dtoSkillSteupList = new ArrayList<>();
						for (SkillsSetup skillsSetup1 : skillsSetup) {
							DtoSkillSteup dtoSkillSteup2 = new DtoSkillSteup();
							dtoSkillSteup2.setId(skillsSetup1.getId());
							dtoSkillSteup2.setSkillId(skillsSetup1.getSkillId());
							dtoSkillSteup2.setSkillDesc(skillsSetup1.getSkillDesc());
							dtoSkillSteup2.setArabicDesc(skillsSetup1.getArabicDesc());
							dtoSkillSteup2.setCompensation(skillsSetup1.getCompensation());
							dtoSkillSteup2.setFrequency(skillsSetup1.getFrequency());
							dtoSkillSteupList.add(dtoSkillSteup2);
						}
						dtoSkillSteup.setDtoSkillSteupList(dtoSkillSteupList);
					}

					/*
					 * DtoSkillSteup dtoSkillSteup2 =new DtoSkillSteup();
					 * if(skillSetDesc.getSkillsId()!=null) {
					 * dtoSkillSteup2.setId(skillSetDesc.getSkillsId().getId());
					 * dtoSkillSteup2.setSkillId(skillSetDesc.getSkillsId().getSkillId());
					 * dtoSkillSteup2.setSkillDesc(skillSetDesc.getSkillsId().getSkillDesc());
					 * dtoSkillSteup2.setArabicDesc(skillSetDesc.getSkillsId().getArabicDesc());
					 * dtoSkillSteup2.setCompensation(skillSetDesc.getSkillsId().getCompensation());
					 * dtoSkillSteup2.setFrequency(skillSetDesc.getSkillsId().getFrequency()); }
					 */
					// dtoSkillSetSteup.setSkillsId(skillSetSetup.getSkillsId());
					// dtoSkillSetSteup.setSkillSetSetup(skillSetSetup.getSkillSetSetup());
					dtoSkillSteup.setSkillRequired(skillSetDesc.isSkillRequired());
					dtoSkillSteup.setSkillSetSeqn(skillSetDesc.getSkillSetSeqn());
					dtoSkillSteup.setId(skillSetDesc.getId());
					dtoSkillSteup.setComment(skillSetDesc.getComment());
					// dtoSkillSteup.setDtoSkillSteup(dtoSkillSteup);
				} else {
					dtoSkillSteup.setMessageType("SKILL_STEUP_NOT_GETTING");

				}
			} else {
				dtoSkillSteup.setMessageType("SKILL_STEUP_NOT_GETTING");

			}
			log.debug("SkillSetSteup By Id is:" + dtoSkillSteup.getId());
		}
		return dtoSkillSteup;
	}

	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search SkillSteup Desc Method");
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();

			String condition = "";

			if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
				if (dtoSearch.getSortOn().equals("comment")) {
					condition = dtoSearch.getSortOn();

				} else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			} else {
				condition += "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");

			}
			dtoSearch.setTotalCount(
					this.repositorySkillSetDesc.predictiveSkillSteupDescSearchTotalCount("%" + searchWord + "%"));
			List<SkillSetDesc> skillSteupList = null;
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

				if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
					skillSteupList = this.repositorySkillSetDesc.predictiveSkillsSetupDescSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, "id"));
				}
				if (dtoSearch.getSortBy().equals("ASC")) {
					skillSteupList = this.repositorySkillSetDesc.predictiveSkillsSetupDescSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.ASC, condition));
				} else if (dtoSearch.getSortBy().equals("DESC")) {
					skillSteupList = this.repositorySkillSetDesc.predictiveSkillsSetupDescSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, condition));
				}

			}

			/*
			 * dtoSearch.setTotalCount(this.repositorySkillSetDesc.
			 * predictiveSkillSteupDescSearchTotalCount("%"+searchWord+"%"));
			 * List<SkillSetDesc> skillSteupList =null; if(dtoSearch.getPageNumber()!=null
			 * && dtoSearch.getPageSize()!=null){ skillSteupList =
			 * this.repositorySkillSetDesc.predictiveSkillsSetupDescSearchWithPagination("%"
			 * +searchWord+"%"); }else { skillSteupList =
			 * this.repositorySkillSetDesc.predictiveSkillsSetupDescSearchWithPagination("%"
			 * +searchWord+"%"); }
			 */

			if (skillSteupList != null && !skillSteupList.isEmpty()) {
				List<DtoSkillSetDesc> dtoSkillSteupList = new ArrayList<>();
				DtoSkillSetDesc dtoSkillSetSteup = null;
				for (SkillSetDesc skillSetSetup : skillSteupList) {
					dtoSkillSetSteup = new DtoSkillSetDesc(skillSetSetup);
					if (skillSetSetup.getSkillSetSetup() != null
							&& skillSetSetup.getSkillSetSetup().getSkillSetId() != null) {
						dtoSkillSetSteup.setSkillsSetPrimaryId(skillSetSetup.getSkillSetSetup().getId());
						dtoSkillSetSteup
								.setSkillsSetId(Integer.parseInt(skillSetSetup.getSkillSetSetup().getSkillSetId()));
						dtoSkillSetSteup.setSkillSetDesc(skillSetSetup.getSkillSetSetup().getSkillSetDiscription());
						dtoSkillSetSteup.setSkillSetArbicDesc(skillSetSetup.getSkillSetSetup().getArabicDiscription());
					}

					// dtoSkillSetSteup.setSkillsId(skillSetSetup.getSkillsId());
					// dtoSkillSetSteup.setSkillSetSetup(skillSetSetup.getSkillSetSetup());
					dtoSkillSetSteup.setSkillRequired(skillSetSetup.isSkillRequired());
					dtoSkillSetSteup.setSkillSetSeqn(skillSetSetup.getSkillSetSeqn());
					dtoSkillSteupList.add(dtoSkillSetSteup);
				}
				dtoSearch.setRecords(dtoSkillSteupList);
			}
		}
		if (dtoSearch != null) {
			log.debug("Search SkillSetSteup Desc Size is:" + dtoSearch.getTotalCount());
		}

		return dtoSearch;
	}
}
