package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Where;
@BatchSize(size = 20)
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10300",indexes = {
        @Index(columnList = "HCMBULDINX")
})
public class BuildChecks extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMBULDINX")
	private Integer id;

	@Column(name = "HCMDSCR", columnDefinition = "char(140)")
	private String description;

	@Column(name = "HCMBULDDT")
	private Date checkDate;

	@Column(name = "HCMBULUSR", columnDefinition = "char(15)")
	private String checkByUserId;

	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
	        name = "BUILD_DEPARTMENT", 
	        joinColumns = { @JoinColumn(name = "buildId") }, 
	        inverseJoinColumns = { @JoinColumn(name = "departmentId") }
	    )
	private List<Department> listDepartment;
	
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
	        name = "BUILD_EMPLOYEE", 
	        joinColumns = { @JoinColumn(name = "buildId") }, 
	        inverseJoinColumns = { @JoinColumn(name = "employeeId") }
	    )
	private List<EmployeeMaster> listEmployee;
	
	
	
	/*@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
	        name = "Build_TransactionEntry", 
	        joinColumns = { @JoinColumn(name = "HCMBULDINX") }, 
	        inverseJoinColumns = { @JoinColumn(name = "HCMTRXINXD") }
	    )
	private List<TransactionEntryDetail> transactionEntryDetailList;*/
	
	
	@ManyToOne
	@JoinColumn(name = "HCMDEFINX")
	private Default default1;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "buildChecks")
	@Where(clause = "is_deleted = false")
	private List<BuildPayrollCheckByPayCodes> listBuildPayrollCheckByPayCodes;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "buildChecks")
	@Where(clause = "is_deleted = false")
	private List<BuildPayrollCheckByDeductions> listBuildPayrollCheckByDeductions;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "buildChecks")
	@Where(clause = "is_deleted = false")
	private List<BuildPayrollCheckByBenefits> listBuildPayrollCheckByBenefits;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "buildChecks")
	@Where(clause = "is_deleted = false")
	private List<BuildPayrollCheckByBatches> listBuildPayrollCheckByBatches;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "buildChecks")
	@Where(clause = "is_deleted = false")
	private List<Distribution> listDistribution;
	
	
	@Column(name = "HCMBULTYP")
	private Short typeofPayRun;

	@Column(name = "HCMPYFDT")
	private Date dateFrom;

	@Column(name = "HCMPYTDT")
	private Date dateTo;

	@Column(name = "HCMPWEK")
	private Boolean weekly;

	@Column(name = "HCMPBWEK")
	private Boolean biweekly;

	@Column(name = "HCMPSMON")
	private Boolean semimonthly;

	@Column(name = "HCMPMON")
	private Boolean monthly;

	@Column(name = "HCMPQUR")
	private Boolean quarterly;

	@Column(name = "HCMPSYR")
	private Boolean semiannually;

	@Column(name = "HCMPYR")
	private Boolean annually;

	@Column(name = "HCMPDMS")
	private Boolean dailyMisc;

	@Column(name = "HCMALLEM")
	private Boolean allEmployees;

	@Column(name = "HCMFEMINX")
	private Integer fromEmployeeId;

	@Column(name = "HCMTEMINX")
	private Integer toEmployeeId;

	@Column(name = "HCMALLDEP")
	private Boolean allDepertment;

	@Column(name = "HCMFDEPINX")
	private Integer fromDepartmentId;

	@Column(name = "HCMTDEPINX")
	private Integer toDepartmentId;
	
	@ManyToOne
	@JoinColumn(name = "HCMBATINDX")
	private Batches batches;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "buildChecks")
//	@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<TransactionEntryDetail> transactionEntryDetails;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public String getCheckByUserId() {
		return checkByUserId;
	}

	public void setCheckByUserId(String checkByUserId) {
		this.checkByUserId = checkByUserId;
	}

	public Short getTypeofPayRun() {
		return typeofPayRun;
	}

	public void setTypeofPayRun(Short typeofPayRun) {
		this.typeofPayRun = typeofPayRun;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Boolean getWeekly() {
		return weekly;
	}

	public void setWeekly(Boolean weekly) {
		this.weekly = weekly;
	}

	public Boolean getBiweekly() {
		return biweekly;
	}

	public void setBiweekly(Boolean biweekly) {
		this.biweekly = biweekly;
	}

	public Boolean getSemimonthly() {
		return semimonthly;
	}

	public void setSemimonthly(Boolean semimonthly) {
		this.semimonthly = semimonthly;
	}

	public Boolean getMonthly() {
		return monthly;
	}

	public void setMonthly(Boolean monthly) {
		this.monthly = monthly;
	}

	public Boolean getQuarterly() {
		return quarterly;
	}

	public void setQuarterly(Boolean quarterly) {
		this.quarterly = quarterly;
	}

	public Boolean getSemiannually() {
		return semiannually;
	}

	public void setSemiannually(Boolean semiannually) {
		this.semiannually = semiannually;
	}

	public Boolean getAnnually() {
		return annually;
	}

	public void setAnnually(Boolean annually) {
		this.annually = annually;
	}

	public Boolean getDailyMisc() {
		return dailyMisc;
	}

	public void setDailyMisc(Boolean dailyMisc) {
		this.dailyMisc = dailyMisc;
	}

	public Boolean getAllEmployees() {
		return allEmployees;
	}

	public void setAllEmployees(Boolean allEmployees) {
		this.allEmployees = allEmployees;
	}

	public Integer getFromEmployeeId() {
		return fromEmployeeId;
	}

	public void setFromEmployeeId(Integer fromEmployeeId) {
		this.fromEmployeeId = fromEmployeeId;
	}

	public Integer getToEmployeeId() {
		return toEmployeeId;
	}

	public void setToEmployeeId(Integer toEmployeeId) {
		this.toEmployeeId = toEmployeeId;
	}

	public Boolean getAllDepertment() {
		return allDepertment;
	}

	public void setAllDepertment(Boolean allDepertment) {
		this.allDepertment = allDepertment;
	}

	public Integer getFromDepartmentId() {
		return fromDepartmentId;
	}

	public void setFromDepartmentId(Integer fromDepartmentId) {
		this.fromDepartmentId = fromDepartmentId;
	}

	public Integer getToDepartmentId() {
		return toDepartmentId;
	}

	public void setToDepartmentId(Integer toDepartmentId) {
		this.toDepartmentId = toDepartmentId;
	}

	public List<BuildPayrollCheckByPayCodes> getListBuildPayrollCheckByPayCodes() {
		return listBuildPayrollCheckByPayCodes;
	}

	public void setListBuildPayrollCheckByPayCodes(List<BuildPayrollCheckByPayCodes> listBuildPayrollCheckByPayCodes) {
		this.listBuildPayrollCheckByPayCodes = listBuildPayrollCheckByPayCodes;
	}

	public List<BuildPayrollCheckByDeductions> getListBuildPayrollCheckByDeductions() {
		return listBuildPayrollCheckByDeductions;
	}

	public void setListBuildPayrollCheckByDeductions(
			List<BuildPayrollCheckByDeductions> listBuildPayrollCheckByDeductions) {
		this.listBuildPayrollCheckByDeductions = listBuildPayrollCheckByDeductions;
	}

	public List<BuildPayrollCheckByBenefits> getListBuildPayrollCheckByBenefits() {
		return listBuildPayrollCheckByBenefits;
	}

	public void setListBuildPayrollCheckByBenefits(List<BuildPayrollCheckByBenefits> listBuildPayrollCheckByBenefits) {
		this.listBuildPayrollCheckByBenefits = listBuildPayrollCheckByBenefits;
	}

	public List<BuildPayrollCheckByBatches> getListBuildPayrollCheckByBatches() {
		return listBuildPayrollCheckByBatches;
	}

	public void setListBuildPayrollCheckByBatches(List<BuildPayrollCheckByBatches> listBuildPayrollCheckByBatches) {
		this.listBuildPayrollCheckByBatches = listBuildPayrollCheckByBatches;
	}

	public Default getDefault1() {
		return default1;
	}

	public void setDefault1(Default default1) {
		this.default1 = default1;
	}

	public List<Department> getListDepartment() {
		return listDepartment;
	}

	public void setListDepartment(List<Department> listDepartment) {
		this.listDepartment = listDepartment;
	}

	public List<EmployeeMaster> getListEmployee() {
		return listEmployee;
	}

	public void setListEmployee(List<EmployeeMaster> listEmployee) {
		this.listEmployee = listEmployee;
	}

	public List<Distribution> getListDistribution() {
		return listDistribution;
	}

	public void setListDistribution(List<Distribution> listDistribution) {
		this.listDistribution = listDistribution;
	}

	public Batches getBatches() {
		return batches;
	}

	public void setBatches(Batches batches) {
		this.batches = batches;
	}

	public List<TransactionEntryDetail> getTransactionEntryDetails() {
		return transactionEntryDetails;
	}

	public void setTransactionEntryDetails(List<TransactionEntryDetail> transactionEntryDetails) {
		this.transactionEntryDetails = transactionEntryDetails;
	}
	
	
	
}
