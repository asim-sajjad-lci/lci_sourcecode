package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40801",indexes = {
        @Index(columnList = "TRNCINDXC")
})
public class TrainingCourseDetail extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRNCINDXC")
	private Integer id;

	@Column(name = "TRNCCLSID", columnDefinition = "char(15)")
	private String classId;

	@Column(name = "TRNCCLSNME", columnDefinition = "char(30)")
	private String className;

	@ManyToOne
	@JoinColumn(name = "TRNCINDX")
	private TraningCourse trainingCourse;
	@Column(name = "TRNCCLSSDT")
	private Date startDate;

	@Column(name = "TRNCCLSEDT")
	private Date endDate;

	@Column(name = "TRNCCLSSTM", columnDefinition = "char(30)")
	private String startTime;

	@Column(name = "TRNCCLSETM", columnDefinition = "char(30)")
	private String endTime;

	@Column(name = "TRNCCLSINTNME", columnDefinition = "char(90)")
	private String instructorName;

	@Column(name = "TRNCCLSENRL")
	private Integer enrolled;

	@Column(name = "TRNCCLSENMX")
	private Integer maximum;

	@Column(name = "TRNCCLSLOC", columnDefinition = "char(60)")
	private String classLocation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getInstructorName() {
		return instructorName;
	}

	public void setInstructorName(String instructorName) {
		this.instructorName = instructorName;
	}

	public Integer getEnrolled() {
		return enrolled;
	}

	public void setEnrolled(Integer enrolled) {
		this.enrolled = enrolled;
	}

	public Integer getMaximum() {
		return maximum;
	}

	public void setMaximum(Integer maximum) {
		this.maximum = maximum;
	}

	public String getClassLocation() {
		return classLocation;
	}

	public void setClassLocation(String classLocation) {
		this.classLocation = classLocation;
	}

	public TraningCourse getTrainingCourse() {
		return trainingCourse;
	}

	public void setTrainingCourse(TraningCourse trainingCourse) {
		this.trainingCourse = trainingCourse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((classId == null) ? 0 : classId.hashCode());
		result = prime * result + ((classLocation == null) ? 0 : classLocation.hashCode());
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((enrolled == null) ? 0 : enrolled.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((instructorName == null) ? 0 : instructorName.hashCode());
		result = prime * result + ((maximum == null) ? 0 : maximum.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		result = prime * result + ((trainingCourse == null) ? 0 : trainingCourse.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
