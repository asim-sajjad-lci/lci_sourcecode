/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Description: The persistent class for the InterviewTypeSetup database table.
 * Name of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40400",indexes = {
        @Index(columnList = "INTRVINDX")
})
@NamedQuery(name = "InterviewTypeSetup.findAll", query = "SELECT i FROM InterviewTypeSetup i")
public class InterviewTypeSetup extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INTRVINDX")
	private int id;

	@Column(name = "INTRVID", columnDefinition = "char(15)")
	private String interviewTypeId;

	@Column(name = "INTRVDSCR", columnDefinition = "char(31)")
	private String interviewTypeDescription;

	@Column(name = "INTRVDSCRA", columnDefinition = "char(61)")
	private String interviewTypeDescriptionArabic;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "interviewTypeSetup")
	private List<InterviewTypeSetupDetail> interviewTypeSetupDetail;

	@Column(name = "INTRVRNG")
	private int interviewRange;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInterviewTypeId() {
		return interviewTypeId;
	}

	public void setInterviewTypeId(String interviewTypeId) {
		this.interviewTypeId = interviewTypeId;
	}

	public String getInterviewTypeDescription() {
		return interviewTypeDescription;
	}

	public void setInterviewTypeDescription(String interviewTypeDescription) {
		this.interviewTypeDescription = interviewTypeDescription;
	}

	public String getInterviewTypeDescriptionArabic() {
		return interviewTypeDescriptionArabic;
	}

	public void setInterviewTypeDescriptionArabic(String interviewTypeDescriptionArabic) {
		this.interviewTypeDescriptionArabic = interviewTypeDescriptionArabic;
	}

	public int getInterviewRange() {
		return interviewRange;
	}

	public void setInterviewRange(int interviewRange) {
		this.interviewRange = interviewRange;
	}

	public List<InterviewTypeSetupDetail> getInterviewTypeSetupDetail() {
		return interviewTypeSetupDetail;
	}

	public void setInterviewTypeSetupDetail(List<InterviewTypeSetupDetail> interviewTypeSetupDetail) {
		this.interviewTypeSetupDetail = interviewTypeSetupDetail;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
