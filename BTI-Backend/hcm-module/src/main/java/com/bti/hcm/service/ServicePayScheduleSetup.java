/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Department;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.Location;
import com.bti.hcm.model.PayScheduleSetup;
import com.bti.hcm.model.PayScheduleSetupDepartment;
import com.bti.hcm.model.PayScheduleSetupEmployee;
import com.bti.hcm.model.PayScheduleSetupLocation;
import com.bti.hcm.model.PayScheduleSetupPosition;
import com.bti.hcm.model.PayScheduleSetupSchedulePeriods;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.dto.DtoPayScheduleSetup;
import com.bti.hcm.model.dto.DtoPayScheduleSetupDepartment;
import com.bti.hcm.model.dto.DtoPayScheduleSetupEmployee;
import com.bti.hcm.model.dto.DtoPayScheduleSetupLocation;
import com.bti.hcm.model.dto.DtoPayScheduleSetupPosition;
import com.bti.hcm.model.dto.DtoPayScheduleSetupSchedulePeriods;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryLocation;
import com.bti.hcm.repository.RepositoryPayScheduleSetup;
import com.bti.hcm.repository.RepositoryPayScheduleSetupDepartment;
import com.bti.hcm.repository.RepositoryPayScheduleSetupEmployee;
import com.bti.hcm.repository.RepositoryPayScheduleSetupLocation;
import com.bti.hcm.repository.RepositoryPayScheduleSetupPosition;
import com.bti.hcm.repository.RepositoryPayScheduleSetupSchedulePeriods;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service PayScheduleSetup
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("servicePayScheduleSetup")
public class ServicePayScheduleSetup {

	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetup Service
	 */
	static Logger log = Logger.getLogger(ServicePayScheduleSetup.class.getName());


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PayScheduleSetup service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PayScheduleSetup service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPayScheduleSetup Autowired here using annotation of spring for access of repositoryPayScheduleSetup method in PayScheduleSetup service
	 * 				In short Access PayScheduleSetup Query from Database using repositoryPayScheduleSetup.
	 */
	@Autowired
	RepositoryPayScheduleSetup repositoryPayScheduleSetup;
	
	
	@Autowired
	RepositoryPayScheduleSetupSchedulePeriods  repositoryPayScheduleSetupSchedulePeriods;
	
	@Autowired
	RepositoryPayScheduleSetupLocation repositoryPayScheduleSetupLocation;
	
	
	@Autowired
	RepositoryPayScheduleSetupPosition repositoryPayScheduleSetupPosition;
	
	@Autowired
	RepositoryPosition repositoryPosition;
	
	@Autowired
	RepositoryPayScheduleSetupDepartment repositoryPayScheduleSetupDepartment;
	
	@Autowired
	RepositoryDepartment repositoryDepartment;
	
	@Autowired
	RepositoryPayScheduleSetupEmployee repositoryPayScheduleSetupEmployee;
	
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired
	RepositoryLocation repositoryLocation;
	
	
	/**
	 * @Description: save and update PayScheduleSetup data
	 * @param dtoPayScheduleSetup
	 * @return
	 * @throws ParseException
	 */
	public DtoPayScheduleSetup saveOrUpdatePayScheduleSetup(DtoPayScheduleSetup dtoPayScheduleSetup) throws ParseException {
		log.info("saveOrUpdatePayScheduleSetup Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		PayScheduleSetup payScheduleSetup = null;
		if (dtoPayScheduleSetup.getPayScheduleIndexId() > 0) {
			payScheduleSetup = repositoryPayScheduleSetup.findByPayScheduleIndexIdAndIsDeleted(dtoPayScheduleSetup.getPayScheduleIndexId(), false);
			payScheduleSetup.setUpdatedBy(loggedInUserId);
			payScheduleSetup.setUpdatedDate(new Date());
		} else {
			payScheduleSetup = new PayScheduleSetup();
			payScheduleSetup.setCreatedDate(new Date());
			Integer rowId = repositoryPayScheduleSetup.getCountOfTotaPayScheduleSetup();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			payScheduleSetup.setRowId(increment);
		}
		payScheduleSetup.setPayScheduleId(dtoPayScheduleSetup.getPayScheduleId());
		payScheduleSetup.setPayScheduleDescription(dtoPayScheduleSetup.getPayScheduleDescription());
		payScheduleSetup.setPayScheduleDescriptionArabic(dtoPayScheduleSetup.getPayScheduleDescriptionArabic());
		payScheduleSetup.setPaySchedulePayPeriod(dtoPayScheduleSetup.getPaySchedulePayPeriod());
		payScheduleSetup.setPayScheduleBeggingDate(dtoPayScheduleSetup.getPayScheduleBeggingDate());
		payScheduleSetup.setPayScheduleYear(dtoPayScheduleSetup.getPayScheduleYear());
		payScheduleSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		payScheduleSetup = repositoryPayScheduleSetup.saveAndFlush(payScheduleSetup);
		if(dtoPayScheduleSetup.getSubItems()!=null && !dtoPayScheduleSetup.getSubItems().isEmpty()) {
			List<PayScheduleSetupSchedulePeriods> list = repositoryPayScheduleSetupSchedulePeriods.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
				for(PayScheduleSetupSchedulePeriods payScheduleSetupSchedulePeriods : list) {
					repositoryPayScheduleSetupSchedulePeriods.delete(payScheduleSetupSchedulePeriods.getPaySchedulePeriodIndexId());	
				}
				
				for(DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods :dtoPayScheduleSetup.getSubItems() ) {
					
					PayScheduleSetupSchedulePeriods payScheduleSetupSchedulePeriods=new PayScheduleSetupSchedulePeriods();
				
					payScheduleSetupSchedulePeriods.setCreatedDate(new Date());
					payScheduleSetupSchedulePeriods.setPeriodId(dtoPayScheduleSetupSchedulePeriods.getPeriodId());
					payScheduleSetupSchedulePeriods.setYear(dtoPayScheduleSetupSchedulePeriods.getYear());
					payScheduleSetupSchedulePeriods.setPeriodName(dtoPayScheduleSetupSchedulePeriods.getPeriodName());
					payScheduleSetupSchedulePeriods.setPeriodStartDate(dtoPayScheduleSetupSchedulePeriods.getPeriodStartDate());
					payScheduleSetupSchedulePeriods.setPeriodEndDate(dtoPayScheduleSetupSchedulePeriods.getPeriodEndDate());
					payScheduleSetupSchedulePeriods.setPayScheduleSetup(payScheduleSetup);
					repositoryPayScheduleSetupSchedulePeriods.saveAndFlush(payScheduleSetupSchedulePeriods);
				}
			
			
		}
		
		if(dtoPayScheduleSetup.getPayScheduleLocation()!=null && !dtoPayScheduleSetup.getPayScheduleLocation().isEmpty()) {
			List<PayScheduleSetupLocation> list = repositoryPayScheduleSetupLocation.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
			for(PayScheduleSetupLocation payScheduleSetupLocation : list) {
				repositoryPayScheduleSetupLocation.delete(payScheduleSetupLocation.getPayScheduleSetupLocationId());	
			}
			for(DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation :dtoPayScheduleSetup.getPayScheduleLocation()) {
				PayScheduleSetupLocation payScheduleSetupLocation=new  PayScheduleSetupLocation();
				Location location = repositoryLocation.findByIdAndIsDeleted(dtoPayScheduleSetupLocation.getLocationId(), false);
				if(location!=null) {
					payScheduleSetupLocation.setLocationName(dtoPayScheduleSetupLocation.getLocationName());
					payScheduleSetupLocation.setPayScheduleStatus(dtoPayScheduleSetupLocation.getPayScheduleStatus());
					payScheduleSetupLocation.setPayScheduleSetup(payScheduleSetup);
					payScheduleSetupLocation.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
					payScheduleSetupLocation.setLocation(location);
					payScheduleSetupLocation = repositoryPayScheduleSetupLocation.saveAndFlush(payScheduleSetupLocation);
				}
				
			}
		}
		
		if(dtoPayScheduleSetup.getPayScheduleSetupPositions()!=null && !dtoPayScheduleSetup.getPayScheduleSetupPositions().isEmpty()) {
			List<PayScheduleSetupPosition> list = repositoryPayScheduleSetupPosition.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
			for(PayScheduleSetupPosition payScheduleSetupPosition : list) {
				repositoryPayScheduleSetupPosition.delete(payScheduleSetupPosition.getPaySchedulePositionIndexId());	
			}
			for(DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition:dtoPayScheduleSetup.getPayScheduleSetupPositions()) {
				PayScheduleSetupPosition payScheduleSetupPosition =new PayScheduleSetupPosition();;
				Position position = repositoryPosition.findOne(dtoPayScheduleSetupPosition.getPositionId());
				if(position!=null) {
					payScheduleSetupPosition.setPositionDescription(dtoPayScheduleSetupPosition.getPositionDescription());
					payScheduleSetupPosition.setPositionDescriptionArabic(dtoPayScheduleSetupPosition.getPositionDescriptionArabic());
					payScheduleSetupPosition.setPayScheduleStatus(dtoPayScheduleSetupPosition.getPayScheduleStatus());
					payScheduleSetupPosition.setPayScheduleSetup(payScheduleSetup);
					payScheduleSetupPosition.setPosition(position);
					payScheduleSetupPosition.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
					payScheduleSetupPosition = repositoryPayScheduleSetupPosition.saveAndFlush(payScheduleSetupPosition);
				}
				
			}
		}
		if(dtoPayScheduleSetup.getPayScheduleSetupDepartments()!=null && !dtoPayScheduleSetup.getPayScheduleSetupDepartments().isEmpty()) {
			List<PayScheduleSetupDepartment> list = repositoryPayScheduleSetupDepartment.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
			for(PayScheduleSetupDepartment payScheduleSetupDepartment : list) {
				repositoryPayScheduleSetupDepartment.delete(payScheduleSetupDepartment.getPayScheduleDepartmentIndexId());	
			}
			for(DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment: dtoPayScheduleSetup.getPayScheduleSetupDepartments()) {
				PayScheduleSetupDepartment payScheduleSetupDepartment = new PayScheduleSetupDepartment();
				Department department = repositoryDepartment.findOne(dtoPayScheduleSetupDepartment.getDepartmentId());
				if(department!=null) {
					payScheduleSetupDepartment.setDepartmentDescription(dtoPayScheduleSetupDepartment.getDepartmentDescription());
					payScheduleSetupDepartment.setArabicDepartmentDescription(dtoPayScheduleSetupDepartment.getArabicDepartmentDescription());
					payScheduleSetupDepartment.setPayScheduleStatus(dtoPayScheduleSetupDepartment.getPayScheduleStatus());
					payScheduleSetupDepartment.setDepartment(department);
					payScheduleSetupDepartment.setPayScheduleSetup(payScheduleSetup);
					payScheduleSetupDepartment = repositoryPayScheduleSetupDepartment.saveAndFlush(payScheduleSetupDepartment);
				}
			}
		}
		
		if(dtoPayScheduleSetup.getPayScheduleSetupEmployees()!=null && !dtoPayScheduleSetup.getPayScheduleSetupEmployees().isEmpty()) {
			List<PayScheduleSetupEmployee> list = repositoryPayScheduleSetupEmployee.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
			for(PayScheduleSetupEmployee payScheduleSetupEmployee : list) {
				repositoryPayScheduleSetupEmployee.delete(payScheduleSetupEmployee.getPaySchedulEmployeeIndexId());	
			}
			for(DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee :dtoPayScheduleSetup.getPayScheduleSetupEmployees()) {
				PayScheduleSetupEmployee payScheduleSetupEmployee =new PayScheduleSetupEmployee();
				EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOne(dtoPayScheduleSetupEmployee.getEmployeeIndexId());
				if(employeeMaster!=null) {
						
							payScheduleSetupEmployee.setPayScheduleStatus(dtoPayScheduleSetupEmployee.getPayScheduleStatus());
							payScheduleSetupEmployee.setPayScheduleSetup(payScheduleSetup);
							payScheduleSetupEmployee.setCreatedDate(new Date());
							payScheduleSetupEmployee.setEmployeeFullName(dtoPayScheduleSetupEmployee.getEmployeeFullName());
							payScheduleSetupEmployee.setEmployeeMaster(employeeMaster);
							payScheduleSetupEmployee =repositoryPayScheduleSetupEmployee.saveAndFlush(payScheduleSetupEmployee);
							
						
					}else {
						payScheduleSetupEmployee = new PayScheduleSetupEmployee();
						payScheduleSetupEmployee.setPayScheduleStatus(dtoPayScheduleSetupEmployee.getPayScheduleStatus());
						payScheduleSetupEmployee.setPayScheduleSetup(payScheduleSetup);
						payScheduleSetupEmployee.setCreatedDate(new Date());
						payScheduleSetupEmployee.setEmployeeFullName(dtoPayScheduleSetupEmployee.getEmployeeFullName());
						payScheduleSetupEmployee.setEmployeeMaster(employeeMaster);
						payScheduleSetupEmployee =repositoryPayScheduleSetupEmployee.saveAndFlush(payScheduleSetupEmployee);
					}
					
				
				
			}
		}
		log.debug("payScheduleSetup is:"+dtoPayScheduleSetup.getPayScheduleIndexId());
		return dtoPayScheduleSetup;

	}
	
	/**
	 * @Description: Search PayScheduleSetup data
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPayScheduleSetup(DtoSearch dtoSearch) {
		log.info("searchPayScheduleSetup Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			switch(dtoSearch.getSortOn()){
			case "payScheduleId" : 
				condition+=dtoSearch.getSortOn();
				break;
			case "payScheduleDescription" : 
				condition+=dtoSearch.getSortOn();
				break;
			case "payScheduleDescriptionArabic" : 
				condition+=dtoSearch.getSortOn();
				break;
			default:
				condition+="payScheduleIndexId";
				
			}
		}else{
			condition+="paySchedulePeriodIndexId";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
		}
			dtoSearch.setTotalCount(this.repositoryPayScheduleSetup.predictivePayScheduleSetupSearchTotalCount("%"+searchWord+"%"));
			List<PayScheduleSetup> payScheduleSetupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					payScheduleSetupList = this.repositoryPayScheduleSetup.predictivePayScheduleSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					payScheduleSetupList = this.repositoryPayScheduleSetup.predictivePayScheduleSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					payScheduleSetupList = this.repositoryPayScheduleSetup.predictivePayScheduleSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
			}
			if(payScheduleSetupList != null && !payScheduleSetupList.isEmpty()){
				List<DtoPayScheduleSetup> dtoPayScheduleSetupList = new ArrayList<DtoPayScheduleSetup>();
				DtoPayScheduleSetup dtoPayScheduleSetup=null;
				for (PayScheduleSetup payScheduleSetup : payScheduleSetupList) {
					
						List<DtoPayScheduleSetupDepartment> dtoPayScheduleSetupDepartmentList = new ArrayList<DtoPayScheduleSetupDepartment>();
						DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment=null;
						List<PayScheduleSetupDepartment> department = repositoryPayScheduleSetupDepartment.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
						if(department!=null && !department.isEmpty()) {
						for(PayScheduleSetupDepartment payScheduleSetupDepartment : department) {
							if(payScheduleSetupDepartment.getIsDeleted()==false) {
								dtoPayScheduleSetupDepartment = new DtoPayScheduleSetupDepartment(payScheduleSetupDepartment);
								dtoPayScheduleSetupDepartment.setPayScheduleDepartmentIndexId(payScheduleSetupDepartment.getPayScheduleDepartmentIndexId());
								dtoPayScheduleSetupDepartment.setPayScheduleSetupId(payScheduleSetup.getPayScheduleIndexId());
								dtoPayScheduleSetupDepartment.setDepartmentId(payScheduleSetupDepartment.getDepartment().getId());
								dtoPayScheduleSetupDepartmentList.add(dtoPayScheduleSetupDepartment);
							}
						
						}
					}
					List<DtoPayScheduleSetupLocation> dtoPayScheduleSetupLocationList = new ArrayList<DtoPayScheduleSetupLocation>();
					List<PayScheduleSetupLocation> locations = repositoryPayScheduleSetupLocation.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
					if(locations!=null && !locations.isEmpty()) {
						DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation=null;
						for(PayScheduleSetupLocation payScheduleSetupLocation : locations) {
							if(payScheduleSetupLocation.getIsDeleted()==false) {
								dtoPayScheduleSetupLocation = new DtoPayScheduleSetupLocation(payScheduleSetupLocation);
								dtoPayScheduleSetupLocation.setPayScheduleSetupLocationId(payScheduleSetupLocation.getPayScheduleSetupLocationId());
								dtoPayScheduleSetupLocation.setLocationId(payScheduleSetupLocation.getLocation().getId());
								dtoPayScheduleSetupLocation.setPayScheduleSetupId(payScheduleSetup.getPayScheduleIndexId());
								dtoPayScheduleSetupLocationList.add(dtoPayScheduleSetupLocation);
							}
							
						}
					}
					List<DtoPayScheduleSetupPosition> dtoPayScheduleSetupPositionList = new ArrayList<DtoPayScheduleSetupPosition>();
					List<PayScheduleSetupPosition> positions = repositoryPayScheduleSetupPosition.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
					if(positions!=null && !positions.isEmpty()) {
						DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition=null;
						for(PayScheduleSetupPosition payScheduleSetupPosition :positions) {
							if(payScheduleSetupPosition.getIsDeleted()==false) {
								dtoPayScheduleSetupPosition = new DtoPayScheduleSetupPosition(payScheduleSetupPosition);
								dtoPayScheduleSetupPosition.setPaySchedulePositionIndexId(payScheduleSetupPosition.getPaySchedulePositionIndexId());
								dtoPayScheduleSetupPosition.setPositionId(payScheduleSetupPosition.getPosition().getId());
								dtoPayScheduleSetupPosition.setPayScheduleSetupId(payScheduleSetup.getPayScheduleIndexId());
								dtoPayScheduleSetupPositionList.add(dtoPayScheduleSetupPosition);
							}
						}
					}
					List<DtoPayScheduleSetupEmployee> dtoPayScheduleSetupEmployeeList = new ArrayList<DtoPayScheduleSetupEmployee>();
					List<PayScheduleSetupEmployee> employees = repositoryPayScheduleSetupEmployee.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
					if(employees!=null && !employees.isEmpty()) {
						DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee=null;
						for(PayScheduleSetupEmployee payScheduleSetupEmployee : employees) {
							if(payScheduleSetupEmployee.getIsDeleted()==false) {
								dtoPayScheduleSetupEmployee = new DtoPayScheduleSetupEmployee(payScheduleSetupEmployee);
								dtoPayScheduleSetupEmployee.setEmployeeIndexId(payScheduleSetupEmployee.getEmployeeMaster().getEmployeeIndexId());
								dtoPayScheduleSetupEmployee.setPaySchedulEmployeeIndexId(payScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
								dtoPayScheduleSetupEmployee.setEmployeeId(payScheduleSetupEmployee.getEmployeeMaster().getEmployeeIndexId());
								dtoPayScheduleSetupEmployee.setPayScheduleSetupId(payScheduleSetup.getPayScheduleIndexId());
								dtoPayScheduleSetupEmployee.setPaySchedulEmployeeIndexId(payScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
								dtoPayScheduleSetupEmployeeList.add(dtoPayScheduleSetupEmployee);
							}
							
						}
					}
					
					List<DtoPayScheduleSetupSchedulePeriods> dtoPayScheduleSetupSchedulePeriodsList = new ArrayList<DtoPayScheduleSetupSchedulePeriods>();
					List<PayScheduleSetupSchedulePeriods> period = repositoryPayScheduleSetupSchedulePeriods.findByPayScheduleSetup(payScheduleSetup.getPayScheduleIndexId());
					if(period!=null  && !period.isEmpty()) {
						DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods = null;
						for(PayScheduleSetupSchedulePeriods payScheduleSetupSchedulePeriods: period) {
							if(payScheduleSetupSchedulePeriods.getIsDeleted()==false) {
								dtoPayScheduleSetupSchedulePeriods = new DtoPayScheduleSetupSchedulePeriods(payScheduleSetupSchedulePeriods);
								dtoPayScheduleSetupSchedulePeriods.setPayScheduleIndexId(payScheduleSetupSchedulePeriods.getPaySchedulePeriodIndexId());
								dtoPayScheduleSetupSchedulePeriods.setPeriodId(payScheduleSetupSchedulePeriods.getPeriodId());
								dtoPayScheduleSetupSchedulePeriods.setYear(payScheduleSetupSchedulePeriods.getYear());
								dtoPayScheduleSetupSchedulePeriods.setPeriodStartDate(payScheduleSetupSchedulePeriods.getPeriodStartDate());
								dtoPayScheduleSetupSchedulePeriods.setPeriodEndDate(payScheduleSetupSchedulePeriods.getPeriodEndDate());
								dtoPayScheduleSetupSchedulePeriods.setPeriodName(payScheduleSetupSchedulePeriods.getPeriodName());
								dtoPayScheduleSetupSchedulePeriods.setPayScheduleSetupId(payScheduleSetup.getPayScheduleIndexId());
								int id = payScheduleSetupSchedulePeriods.getPeriodId();
								switch (id) {
									case 1 :
										dtoPayScheduleSetupSchedulePeriods.setPeriodNameId("Weekly");
										break;
									case 2 :
										dtoPayScheduleSetupSchedulePeriods.setPeriodNameId("Biweekly");
										break;
									case 3 :
										dtoPayScheduleSetupSchedulePeriods.setPeriodNameId("Semimonthly");
										break;
									case 4 :
										dtoPayScheduleSetupSchedulePeriods.setPeriodNameId("Monthly");
										break;
									case 5 :
										dtoPayScheduleSetupSchedulePeriods.setPeriodNameId("Quarterly");
										break;
									case 6 :
										dtoPayScheduleSetupSchedulePeriods.setPeriodNameId("Semiannually");
										break;
									case 7 :
										dtoPayScheduleSetupSchedulePeriods.setPeriodNameId("Annually");
										break;
									default :
										dtoPayScheduleSetupSchedulePeriods.setPeriodNameId("");
								}
								dtoPayScheduleSetupSchedulePeriodsList.add(dtoPayScheduleSetupSchedulePeriods);
							}
						}
					}
					dtoPayScheduleSetup = new DtoPayScheduleSetup(payScheduleSetup);
					dtoPayScheduleSetup.setPayScheduleId(payScheduleSetup.getPayScheduleId());
					dtoPayScheduleSetup.setPayScheduleIndexId(payScheduleSetup.getPayScheduleIndexId());
					dtoPayScheduleSetup.setPayScheduleDescription(payScheduleSetup.getPayScheduleDescription());
					dtoPayScheduleSetup.setPayScheduleDescriptionArabic(payScheduleSetup.getPayScheduleDescriptionArabic());
					dtoPayScheduleSetup.setPaySchedulePayPeriod(payScheduleSetup.getPaySchedulePayPeriod());
					dtoPayScheduleSetup.setPayScheduleBeggingDate(payScheduleSetup.getPayScheduleBeggingDate());
					dtoPayScheduleSetup.setPayScheduleYear(payScheduleSetup.getPayScheduleYear());
					dtoPayScheduleSetup.setPayScheduleSetupDepartments(dtoPayScheduleSetupDepartmentList);
					dtoPayScheduleSetup.setPayScheduleLocation(dtoPayScheduleSetupLocationList);
					dtoPayScheduleSetup.setPayScheduleSetupPositions(dtoPayScheduleSetupPositionList);
					dtoPayScheduleSetup.setPayScheduleSetupEmployees(dtoPayScheduleSetupEmployeeList);
					dtoPayScheduleSetup.setSubItems(dtoPayScheduleSetupSchedulePeriodsList);
					dtoPayScheduleSetupList.add(dtoPayScheduleSetup);
					
				}
				dtoSearch.setRecords(dtoPayScheduleSetupList);
			}
		}
		log.debug("Search PayScheduleSetup Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	/**
	 *  @Description: get PayScheduleSetup data by id
	 * @param id
	 * @return
	 */
	public DtoPayScheduleSetup getPayScheduleSetupByPayScheduleId(int id) {
		log.info("getPayScheduleSetupByPayScheduleId Method");
		DtoPayScheduleSetup dtoPayScheduleSetup = new DtoPayScheduleSetup();
		if (id > 0) {
			PayScheduleSetup payScheduleSetup = repositoryPayScheduleSetup.findByPayScheduleIndexIdAndIsDeleted(id, false);
			if (payScheduleSetup != null) {
				dtoPayScheduleSetup = new DtoPayScheduleSetup(payScheduleSetup);
				dtoPayScheduleSetup.setPayScheduleId(payScheduleSetup.getPayScheduleId());
				dtoPayScheduleSetup.setPayScheduleDescription(payScheduleSetup.getPayScheduleDescription());
				dtoPayScheduleSetup.setPayScheduleDescriptionArabic(payScheduleSetup.getPayScheduleDescriptionArabic());
				dtoPayScheduleSetup.setPaySchedulePayPeriod(payScheduleSetup.getPaySchedulePayPeriod());
				dtoPayScheduleSetup.setPayScheduleBeggingDate(payScheduleSetup.getPayScheduleBeggingDate());
				dtoPayScheduleSetup.setPayScheduleIndexId(payScheduleSetup.getPayScheduleIndexId());
				dtoPayScheduleSetup.setPayScheduleYear(payScheduleSetup.getPayScheduleYear());
			} else {
				dtoPayScheduleSetup.setMessageType("PAYSCHEDULE_NOT_GETTING");

			}
		} else {
			dtoPayScheduleSetup.setMessageType("INVALID_PAYSCHEDULE_INDEX_ID");

		}
		log.debug("PayScheduleSetup By Id is:"+dtoPayScheduleSetup.getPayScheduleIndexId());
		return dtoPayScheduleSetup;
	}
	/**
	 * @Description: delete PayScheduleSetup
	 * @param ids
	 * @return
	 */
	public DtoPayScheduleSetup deletePaySCheduleSetup(List<Integer> ids) {
		log.info("deletePaySCheduleSetup Method");
		DtoPayScheduleSetup dtoPayScheduleSetup = new DtoPayScheduleSetup();
		dtoPayScheduleSetup.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAYSCHEDULE_DELETED", false));
		dtoPayScheduleSetup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("PAYSCHEDULE_ASSOCIATED", false));
		List<DtoPayScheduleSetup> deletePayScheduleSetup = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHDULE_SETUP_DELETED_ID_MESSAGE", false).getMessage());
		try {
			for (Integer payScheduleIndexId : ids) {
				PayScheduleSetup payScheduleSetup = repositoryPayScheduleSetup.findOne(payScheduleIndexId);
				if(payScheduleSetup.getPayScheduleSetupLocation().isEmpty() && payScheduleSetup.getPayScheduleSetupPosition().isEmpty()
						&& payScheduleSetup.getPayScheduleSetupDepartment().isEmpty() && payScheduleSetup.getPayScheduleSetupEmployee().isEmpty()
						&& payScheduleSetup.getPayScheduleSetupSchedulePeriods().isEmpty()) {
					DtoPayScheduleSetup dtoPayScheduleSetup2 = new DtoPayScheduleSetup();
					dtoPayScheduleSetup2.setPayScheduleIndexId(payScheduleIndexId);
					repositoryPayScheduleSetup.deleteSinglePayScheduleSetup(true, loggedInUserId, payScheduleIndexId);
					deletePayScheduleSetup.add(dtoPayScheduleSetup2);
				}else {
					inValidDelete = true;
				}
				

			}
			if(inValidDelete){
				dtoPayScheduleSetup.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoPayScheduleSetup.setMessageType("");
				
			}
			dtoPayScheduleSetup.setDeletePayScheduleSetup(deletePayScheduleSetup);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete PayScheduleSetup :"+dtoPayScheduleSetup.getPayScheduleIndexId());
		return dtoPayScheduleSetup;
	}
	
	/**
	 * @Description: check Repeat payScheduleIndexId
	 * @param payScheduleIndexId
	 * @return
	 */
	public DtoPayScheduleSetup repeatByPayScheduleSetuptId(String payScheduleId) {
		log.info("repeatByPayScheduleSetuptId Method");
		DtoPayScheduleSetup dtoPayScheduleSetup = new DtoPayScheduleSetup();
		try {
			List<PayScheduleSetup> payScheduleSetup=repositoryPayScheduleSetup.findByPayScheduleSetup(payScheduleId.trim());
			if(payScheduleSetup!=null && !payScheduleSetup.isEmpty()) {
				dtoPayScheduleSetup.setIsRepeat(true);
			}else {
				dtoPayScheduleSetup.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPayScheduleSetup;
	}
	
	
	public DtoSearch getAllPayScheduleSetup() {
		log.info("getAllPayScheduleSetup Method");
		DtoPayScheduleSetup dtoPayScheduleSetup = new DtoPayScheduleSetup();
		DtoSearch dtoSearch = new DtoSearch();
		List<DtoPayScheduleSetup> dtoPayScheduleSetupList = new ArrayList<>();
		try {
			List<PayScheduleSetup> payScheduleSetupList=repositoryPayScheduleSetup.findByIsDeleted(false);
			if(payScheduleSetupList!=null && !payScheduleSetupList.isEmpty()) {
				for (PayScheduleSetup payScheduleSetup2 : payScheduleSetupList) {
					dtoPayScheduleSetup.setPayScheduleIndexId(payScheduleSetup2.getPayScheduleIndexId());
					dtoPayScheduleSetup.setPayScheduleDescription(payScheduleSetup2.getPayScheduleDescription());
					dtoPayScheduleSetupList.add(dtoPayScheduleSetup);
				}
				dtoSearch.setRecords(dtoPayScheduleSetupList);
			}
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}

	public DtoSearch searchPayScheduleId(DtoSearch dtoSearch) {
		log.info("searchPayScheduleId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			List<PayScheduleSetup> payScheduleSetupList =new ArrayList<>();
			
				payScheduleSetupList = this.repositoryPayScheduleSetup.predictiveSearchPayScheduleIdWithPagination("%"+searchWord+"%");
				if(!payScheduleSetupList.isEmpty()) {

					if(payScheduleSetupList != null && !payScheduleSetupList.isEmpty()){
						List<DtoPayScheduleSetup> dtoPayScheduleSetup = new ArrayList<>();
						DtoPayScheduleSetup dtoPayScheduleSetup1=null;
						for (PayScheduleSetup payScheduleSetup : payScheduleSetupList) {
							
							dtoPayScheduleSetup1=new DtoPayScheduleSetup(payScheduleSetup);
							dtoPayScheduleSetup1.setPayScheduleIndexId(payScheduleSetup.getPayScheduleIndexId());
							dtoPayScheduleSetup1.setPayScheduleId(payScheduleSetup.getPayScheduleId());
							dtoPayScheduleSetup1.setPayScheduleDescription(payScheduleSetup.getPayScheduleId()+" | "+payScheduleSetup.getPayScheduleDescription());
							
							dtoPayScheduleSetup.add(dtoPayScheduleSetup1);
						}
						dtoSearch.setRecords(dtoPayScheduleSetup);
					}

					dtoSearch.setTotalCount(payScheduleSetupList.size());
				}
			
		}
		
		return dtoSearch;
	}


	
	
	
	
	
	
	
	
	
}
