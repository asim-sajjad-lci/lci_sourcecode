/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the PayScheduleSetupEmployee database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40945",indexes = {
        @Index(columnList = "PYSCHDINDXE")
})
@NamedQuery(name = "PayScheduleSetupEmployee.findAll", query = "SELECT p FROM PayScheduleSetupEmployee p")
public class PayScheduleSetupEmployee extends HcmBaseEntity implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PYSCHDINDXE")
	private int paySchedulEmployeeIndexId;
	
	
	@ManyToOne
	@JoinColumn(name="PYSCHDINDX")
	private PayScheduleSetup payScheduleSetup;
	
	@Column(name = "EMPLYNMN",columnDefinition="char(150)")
	private String employeeFullName;
	
	
	@Column(name = "PYSCHDACT")
	private Boolean payScheduleStatus;
	
	@ManyToOne
	@JoinColumn(name="EMPLYINDX")
	private EmployeeMaster employeeMaster;

	public int getPaySchedulEmployeeIndexId() {
		return paySchedulEmployeeIndexId;
	}

	public void setPaySchedulEmployeeIndexId(int paySchedulEmployeeIndexId) {
		this.paySchedulEmployeeIndexId = paySchedulEmployeeIndexId;
	}

	public PayScheduleSetup getPayScheduleSetup() {
		return payScheduleSetup;
	}

	public void setPayScheduleSetup(PayScheduleSetup payScheduleSetup) {
		this.payScheduleSetup = payScheduleSetup;
	}

	public String getEmployeeFullName() {
		return employeeFullName;
	}

	public void setEmployeeFullName(String employeeFullName) {
		this.employeeFullName = employeeFullName;
	}

	public Boolean getPayScheduleStatus() {
		return payScheduleStatus;
	}

	public void setPayScheduleStatus(Boolean payScheduleStatus) {
		this.payScheduleStatus = payScheduleStatus;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	
	
	


}
