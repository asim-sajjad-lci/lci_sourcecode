package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.HealthInsuranceEnrollment;

public class DtoHealthInsuranceEnrollment extends DtoBase {
	private Integer id;
	private Short status;
	private Date eligibilityDate;
	private Date benefitStartDate;
	private Date benefitEndDate;
	private Boolean overrideCost;
	private BigDecimal insuredAmount;
	private BigDecimal costToEmployee;
	private BigDecimal costToEmployeer;
	private BigDecimal additionalCost;
	private String policyNumber;
	private Integer employeeId;
	private Integer helthInsuranceId;
	private DtoEmployeeMaster employeeMaster;
	private DtoHelthInsurance helthInsurance;
	private List<DtoHealthInsuranceEnrollment> delete;
	
	private DtoEmployeeMaster dtoEmployeeMaster;
	private DtoHelthInsurance dtoHelthInsurance;

	public DtoHealthInsuranceEnrollment(HealthInsuranceEnrollment healthInsuranceEnrollment) {

	}

	public DtoHealthInsuranceEnrollment() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getEligibilityDate() {
		return eligibilityDate;
	}

	public void setEligibilityDate(Date eligibilityDate) {
		this.eligibilityDate = eligibilityDate;
	}

	public Date getBenefitStartDate() {
		return benefitStartDate;
	}

	public void setBenefitStartDate(Date benefitStartDate) {
		this.benefitStartDate = benefitStartDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}

	public Boolean getOverrideCost() {
		return overrideCost;
	}

	public void setOverrideCost(Boolean overrideCost) {
		this.overrideCost = overrideCost;
	}

	public BigDecimal getInsuredAmount() {
		return insuredAmount;
	}

	public void setInsuredAmount(BigDecimal insuredAmount) {
		this.insuredAmount = insuredAmount;
	}

	public BigDecimal getCostToEmployee() {
		return costToEmployee;
	}

	public void setCostToEmployee(BigDecimal costToEmployee) {
		this.costToEmployee = costToEmployee;
	}

	public BigDecimal getCostToEmployeer() {
		return costToEmployeer;
	}

	public void setCostToEmployeer(BigDecimal costToEmployeer) {
		this.costToEmployeer = costToEmployeer;
	}

	public BigDecimal getAdditionalCost() {
		return additionalCost;
	}

	public void setAdditionalCost(BigDecimal additionalCost) {
		this.additionalCost = additionalCost;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public List<DtoHealthInsuranceEnrollment> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoHealthInsuranceEnrollment> delete) {
		this.delete = delete;
	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public DtoHelthInsurance getHelthInsurance() {
		return helthInsurance;
	}

	public void setHelthInsurance(DtoHelthInsurance helthInsurance) {
		this.helthInsurance = helthInsurance;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getHelthInsuranceId() {
		return helthInsuranceId;
	}

	public void setHelthInsuranceId(Integer helthInsuranceId) {
		this.helthInsuranceId = helthInsuranceId;
	}

	public DtoEmployeeMaster getDtoEmployeeMaster() {
		return dtoEmployeeMaster;
	}

	public void setDtoEmployeeMaster(DtoEmployeeMaster dtoEmployeeMaster) {
		this.dtoEmployeeMaster = dtoEmployeeMaster;
	}

	public DtoHelthInsurance getDtoHelthInsurance() {
		return dtoHelthInsurance;
	}

	public void setDtoHelthInsurance(DtoHelthInsurance dtoHelthInsurance) {
		this.dtoHelthInsurance = dtoHelthInsurance;
	}

}
