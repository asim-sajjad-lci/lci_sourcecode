package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPositionPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePositionPayCode;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/positionPlanPayCode")
public class ControllerPositionPayCode extends BaseController{

	@Autowired
	ServicePositionPayCode servicePositionPayCode;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerPositionPayCode.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @param request{ 	
  	"positionPaycodeSeqn":1,
  	"positionCode" :  1,
	  	"ratePay": 1
  	
	} @param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"positionPalnSetup": null,
		"positionPaycodeSeqn": 1,
		"positionCode": 1,
		"payCode": null,
		"deductionCode": null,
		"benefitCode": null,
		"ratePay": 1,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePosition": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "position pay code created successfully",
		
		}
		}
	 * @param dtoPositionPayCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/createPositionPayCode", method = RequestMethod.POST)
	public ResponseMessage createPositionPayCode(HttpServletRequest request,@RequestBody DtoPositionPayCode dtoPositionPayCode) throws Exception{
		LOGGER.info("Create Postion PayCode setup Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionPayCode = servicePositionPayCode.saveOrUpdatePositionPalnSetup(dtoPositionPayCode);
			responseMessage=displayMessage(dtoPositionPayCode, "PAY_CODE_CREATED1", "PAY_CODE_NOT_CREATED1", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create Postion PayCode setup Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	/**
	 * @param request{ 	
  	"pageNumber": "0",
  	"pageSize": "10"
  	
	}
	@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"pageNumber": 0,
		"pageSize": 10,
		"totalCount": 2,
		"records": [
		  {
		"id": 2,
		"positionPalnSetup": null,
		"positionPaycodeSeqn": null,
		"positionCode": null,
		"payCode": null,
		"deductionCode": null,
		"benefitCode": null,
		"ratePay": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePosition": null,
		"isRepeat": null
		},
		  {
		"id": 1,
		"positionPalnSetup": null,
		"positionPaycodeSeqn": null,
		"positionCode": null,
		"payCode": null,
		"deductionCode": null,
		"benefitCode": null,
		"ratePay": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePosition": null,
		"isRepeat": null
		}
		],
		},
		"btiMessage": {
		"message": "position pay code list fetched successfully",
		}
		}
	 * @param dtoPositionPayCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getPositionPlanPayCode(HttpServletRequest request, @RequestBody DtoPositionPayCode dtoPositionPayCode) throws Exception {
		LOGGER.info("Get All getPositionPlanPayCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePositionPayCode.getAllPostionPayCode(dtoPositionPayCode);
			responseMessage=displayMessage(dtoSearch, "PAY_CODE_GET_ALL1", "PAY_CODE_LIST_NOT_GETTING1", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get All getPositionPlanPayCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{ 	
  	"id":1,
  	"positionPaycodeSeqn":3,
  		"positionCode" :  3,
	  	"ratePay": 3
  	
	}@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": 1,
		"positionPalnSetup": null,
		"positionPaycodeSeqn": 3,
		"positionCode": 3,
		"payCode": null,
		"deductionCode": null,
		"benefitCode": null,
		"ratePay": 3,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePosition": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Skill Steup updated successfully",
		"messageShort": "PAY_CODE_UPDATED_SUCCESS"
		}
		}
	 * @param dtoPositionPayCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePositionPayCode(HttpServletRequest request, @RequestBody DtoPositionPayCode dtoPositionPayCode) throws Exception {
		LOGGER.info("Update Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionPayCode = servicePositionPayCode.saveOrUpdatePositionPalnSetup(dtoPositionPayCode);
			responseMessage=displayMessage(dtoPositionPayCode, "PAY_CODE_UPDATED_SUCCESS1", "PAY_CODE_NOT_UPDATED1", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update Position Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{ 	
  	"ids":[1]
  
  	
		}@param response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": null,
			"positionPalnSetup": null,
			"positionPaycodeSeqn": null,
			"positionCode": null,
			"payCode": null,
			"deductionCode": null,
			"benefitCode": null,
			"ratePay": null,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"associateMessage": "N/A",
			"deletePosition": [
			  {
			"id": 1,
			"positionPalnSetup": null,
			"positionPaycodeSeqn": null,
			"positionCode": null,
			"payCode": null,
			"deductionCode": null,
			"benefitCode": null,
			"ratePay": null,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePosition": null,
			"isRepeat": null
			}
			],
			"isRepeat": null
			},
			"btiMessage": {
			}
			}
	 * @param dtoPositionPayCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePositionPayCode(HttpServletRequest request, @RequestBody DtoPositionPayCode dtoPositionPayCode) throws Exception {
		LOGGER.info("Delete Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPositionPayCode.getIds() != null && !dtoPositionPayCode.getIds().isEmpty()) {
				DtoPositionPayCode dtoPosition1 = servicePositionPayCode.deletePositionPayCode(dtoPositionPayCode.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("PAY_CODE_DELETED1", false), dtoPosition1);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Position plan Setup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{ 	
  		"id":1
  
  		
		} @param response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": null,
			"positionPalnSetup": null,
			"positionPaycodeSeqn": null,
			"positionCode": null,
			"payCode": null,
			"deductionCode": null,
			"benefitCode": null,
			"ratePay": null,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePosition": null,
			"isRepeat": null
			},
			"btiMessage": {
			"message": "position pay code details fetched successfully",
			"messageShort": "PAY_CODE_GET_DETAIL"
			}
			}
	 * @param dtoPositionPayCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPositionPayCodeId", method = RequestMethod.POST)
	public ResponseMessage getPositionPayCodeId(HttpServletRequest request, @RequestBody DtoPositionPayCode dtoPositionPayCode) throws Exception {
		LOGGER.info("Get Position Steup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPositionPayCode dtoPositionObj = servicePositionPayCode.getByPositionPaycodeId(dtoPositionPayCode.getId());
			responseMessage=displayMessage(dtoPositionObj, MessageConstant.PAY_CODE_GET_DETAIL1, MessageConstant.PAY_CODE_NOT_GETTING1, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get Position plan Setup by Id Method:"+dtoPositionPayCode.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPositionPayCode(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search searchPositionPlan Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionPayCode.searchPositionPayCode(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_CODE_GET_DETAIL1", "PAY_CODE_NOT_GETTING1", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search searchPositionPlan Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	 @RequestMapping(value = "/searchPositionPlanId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
		public ResponseMessage searchPositionPayCodeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
			LOGGER.info("searchPositionPayCodeId Method");
			ResponseMessage responseMessage = null;
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtoSearch = this.servicePositionPayCode.searchPositionPayCodeId(dtoSearch);
				responseMessage=displayMessage(dtoSearch, "PAY_CODE_GET_DETAIL1", "PAY_CODE_NOT_GETTING1", response);
			} else {
				responseMessage = unauthorizedMsg(response);
			}
			if(dtoSearch!=null) {
				LOGGER.debug("Search searchPositionPlan Method:"+dtoSearch.getTotalCount());
			}
			return responseMessage;
		}
	
	
}
