package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR41201",indexes = {
        @Index(columnList = "ROWID")
})
public class SettingFamilyLeave extends HcmBaseEntity implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ROWID")
	private Integer id;
	
	@Column(name = "SECTMEDHT")
	private short method;
	
	@Column(name = "ISPERDI")
	private short isThisPeriod;
	
	@Column(name = "WNEPESTR")
	private Date periodStart;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public short getMethod() {
		return method;
	}

	public void setMethod(short method) {
		this.method = method;
	}

	public short getIsThisPeriod() {
		return isThisPeriod;
	}

	public void setIsThisPeriod(short isThisPeriod) {
		this.isThisPeriod = isThisPeriod;
	}

	public Date getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}
	
	
}
