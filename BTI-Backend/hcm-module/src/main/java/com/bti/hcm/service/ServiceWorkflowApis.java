package com.bti.hcm.service;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;

import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.EmployeeLoanEntry;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeeMonthlyInstallment;
import com.bti.hcm.model.dto.DtoEmployeeInfo;
import com.bti.hcm.model.dto.DtoEmployeeLoanEntry;
import com.bti.hcm.model.dto.DtoEmployeeLoanInfo;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeeLoanEntry;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.util.Constant;

@Service("serviceWorkflowApis")
public class ServiceWorkflowApis {

	
	static Logger log = Logger.getLogger(ServiceWorkflowApis.class.getName());
	
	@Autowired
	ServiceEmployeeMaster serviceEmployeeMaster; 
	
	@Autowired
	RepositoryEmployeeLoanEntry repositoryEmployeeLoanEntry;

	@Autowired
	private RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	private ModelMapper modelMapper;
	
	public DtoEmployeeLoanInfo getEmployeeLoanInfo(String employeeCode) {

		DtoEmployeeLoanInfo dtoEmployeeLoanInfo = null;
		DtoEmployeeMaster dtoEmployeeMaster = serviceEmployeeMaster.getEmployeeByCode(employeeCode);

		if (dtoEmployeeMaster != null) {
			dtoEmployeeLoanInfo = new DtoEmployeeLoanInfo();
			
			dtoEmployeeLoanInfo.setEmployeeCode(employeeCode);
			dtoEmployeeLoanInfo.setEmployeeName(dtoEmployeeMaster.getEmployeeFullName());
			dtoEmployeeLoanInfo.setEmployeeNameArabic(dtoEmployeeMaster.getEmployeeFullNameArabic());
			
			if (dtoEmployeeMaster.getDtoPosition() != null) {
				dtoEmployeeLoanInfo.setPosition(dtoEmployeeMaster.getDtoPosition().getDescription());
				dtoEmployeeLoanInfo.setPositionArabic(dtoEmployeeMaster.getDtoPosition().getArabicDescription());
			}
			
			if (dtoEmployeeMaster.getDtoLocation() != null) {
				dtoEmployeeLoanInfo.setSite(dtoEmployeeMaster.getDtoLocation().getDescription());
				dtoEmployeeLoanInfo.setSiteArabic(dtoEmployeeMaster.getDtoLocation().getArabicDescription());
			}
			
			dtoEmployeeLoanInfo.setJoiningDate(dtoEmployeeMaster.getEmployeeHireDate()); // TODO it may be replaced with Adjusted Hire Date Later
			
			dtoEmployeeLoanInfo.setNetMonthlySalary(8000);
			dtoEmployeeLoanInfo.setTotalLoan(20000);
			dtoEmployeeLoanInfo.setPreviousLoan(8000);
			dtoEmployeeLoanInfo.setTotalDeducted(2000);
			dtoEmployeeLoanInfo.setMonthlyInstallment(1000);
			dtoEmployeeLoanInfo.setRemaining(6000);
			dtoEmployeeLoanInfo.setLoanThreshold(20000);
			
		}
		
		return dtoEmployeeLoanInfo;
	}
	
	public String postEmployeeLoanEntry(DtoEmployeeLoanEntry dtoEmployeeLoanEntry) {
		String result = MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_FAILURE;
		try {
			
			EmployeeLoanEntry employeeLoanEntry = convertToModel(dtoEmployeeLoanEntry);
			
			String employeeCode = dtoEmployeeLoanEntry.getEmployeeCode();
			
			EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOneByEmployeeId(employeeCode);
			
			if (employeeMaster == null) {
				return MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_EMPLOYEE_NOT_FOUND;
			}
			
			employeeLoanEntry.setEmployeeMaster(employeeMaster);
			
			for (EmployeeMonthlyInstallment emi: employeeLoanEntry.getEmployeeMonthlyInstallmentList()) {
				emi.setEmployeeLoanEntry(employeeLoanEntry);
			}
			
			employeeLoanEntry = repositoryEmployeeLoanEntry.save(employeeLoanEntry);
			
			if (employeeLoanEntry.getId() > 0) {
				result = MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_SUCCESS;
			} 

		} catch (DataIntegrityViolationException divEx) {
			result = MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_DUPLICATE_REQUEST;
			log.error(divEx.getMessage());
			log.info(divEx.getStackTrace());
			divEx.printStackTrace();
		} catch (Exception ex) {
			result = MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_FAILURE;
			log.error(ex.getMessage());
			log.info(ex.getStackTrace());
			ex.printStackTrace();
		}
		return result;
	}
	
	
	
	private DtoEmployeeLoanEntry convertToDto(EmployeeLoanEntry employeeLoanEntry) {
		DtoEmployeeLoanEntry dtoEmployeeLoanEntry = modelMapper.map(employeeLoanEntry, DtoEmployeeLoanEntry.class);
	    return dtoEmployeeLoanEntry;
	} 
	
	private EmployeeLoanEntry convertToModel(DtoEmployeeLoanEntry dtoEmployeeLoanEntry) throws ParseException {
		EmployeeLoanEntry employeeLoanEntry = modelMapper.map(dtoEmployeeLoanEntry, EmployeeLoanEntry.class);
	    return employeeLoanEntry;
	}
	
	public DtoEmployeeInfo getEmployeeInfo(String employeeCode) {
		DtoEmployeeInfo dtoEmployeeInfo = null;
		DtoEmployeeMaster dtoEmployeeMaster = serviceEmployeeMaster.getEmployeeByCode(employeeCode);

		if (dtoEmployeeMaster != null) {
			dtoEmployeeInfo = new DtoEmployeeLoanInfo();
			
			dtoEmployeeInfo.setEmployeeCode(employeeCode);
			dtoEmployeeInfo.setEmployeeName(dtoEmployeeMaster.getEmployeeFullName());
			dtoEmployeeInfo.setEmployeeNameArabic(dtoEmployeeMaster.getEmployeeFullNameArabic());
			
			if (dtoEmployeeMaster.getDtoPosition() != null) {
				dtoEmployeeInfo.setPosition(dtoEmployeeMaster.getDtoPosition().getDescription());
				dtoEmployeeInfo.setPositionArabic(dtoEmployeeMaster.getDtoPosition().getArabicDescription());
			}
			
			if (dtoEmployeeMaster.getDtoLocation() != null) {
				dtoEmployeeInfo.setSite(dtoEmployeeMaster.getDtoLocation().getDescription());
				dtoEmployeeInfo.setSiteArabic(dtoEmployeeMaster.getDtoLocation().getArabicDescription());
			}
			
			dtoEmployeeInfo.setJoiningDate(dtoEmployeeMaster.getEmployeeHireDate()); 
			// TODO it may be replaced with Adjusted Hire Date Later
			
			dtoEmployeeInfo.setNationality(dtoEmployeeMaster.getDtoEmployeeNationalities().getEmployeeNationalityDescription());
			
			dtoEmployeeInfo.setCitizen(dtoEmployeeMaster.isEmployeeCitizen());
			
			dtoEmployeeInfo.setDepartment(dtoEmployeeMaster.getDtoDepartment().getDepartmentDescription());
			
			dtoEmployeeInfo.setEmployeeMaritalStatus(dtoEmployeeMaster.getEmployeeMaritalStatus());
			
			dtoEmployeeInfo.setEmployeeId(dtoEmployeeMaster.getEmployeeId());
			
		}
		
		return dtoEmployeeInfo;

	}
}
