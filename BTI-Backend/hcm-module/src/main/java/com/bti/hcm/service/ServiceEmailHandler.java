/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.hcm.service;
import java.io.File;
import java.util.Arrays;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;


/**
 * Description: Service Email Handler
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 11:11:38 AM
 * @author seasia
 * Version: 
 */
//@EnableAsync
@Service("serviceEmailHandler")
public class ServiceEmailHandler {

	private static final Log LOG = LogFactory.getLog(ServiceEmailHandler.class);

	@Value("${smtp.sender}")
	private String sender;

	@Value("${smtp.host}")
	private String host;

	@Value("${smtp.port}")
	private int port;

	@Value("${smtp.username}")
	private String username;

	@Value("${smtp.password}")
	private String password;

//	@Autowired
//	RepositoryEmailTemplates repositoryEmailTemplates;

	/**
	 * @return
	 */
	public Session getMailSession() {
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.socketFactory.port", port); //"465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");

		return Session.getDefaultInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		 

	}

	/**
	 * @param email
	 * @param emailText
	 * @param subject
	 */
	public void sendEmail(String email, String emailText, String subject) throws Exception {
		    
			Session session = getMailSession();
			try 
			{
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(this.username, this.sender));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
				message.setSubject(subject);
				BodyPart body = new MimeBodyPart();
				body.setContent(emailText, "text/html; charset=ISO-8859-1");
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(body);
				message.setContent(multipart, "text/html; charset=ISO-8859-1");
				Transport.send(message);
				 
			} 
			catch (Exception e) 
			{
				 LOG.info(Arrays.toString(e.getStackTrace()));
				 throw e;
			}
	}
	

	/**
	 * @param emailAddress
	 * @param emailText
	 * @param subject
	 */
	public void sendEmailWithAttachment(String fileName, String attachmentFullPath, String emailAddress, String employeName, String payPeriod, String fromPeriod, String toPeriod) throws Exception {
		    
		
		
			Session session = getMailSession();
			try 
			{
				
				String subject = generatePaySlipEmailSubject(payPeriod);
				String emailText = generatePaySlipEmailText(employeName, fromPeriod, toPeriod);
				
				
				
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(this.username, this.sender));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddress));
				message.setSubject(subject);
				
				BodyPart body = new MimeBodyPart();
				body.setContent(emailText, "text/html; charset=UTF-8");
				
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(body);
				message.setContent(multipart, "application/pdf; charset=UTF-8");
				
				
				MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_RELATED, "UTF-8");
			    helper.setTo(emailAddress);
			    helper.setSubject(subject);
			    helper.setText(emailText);
			         
			    FileSystemResource file 
			      = new FileSystemResource(new File(attachmentFullPath));
			    
			    helper.addAttachment(fileName, file, "application/pdf; charset=UTF-8");
			    
			    
			    Transport.send(message);
			} 
			catch (Exception e) 
			{
				 LOG.info(Arrays.toString(e.getStackTrace()));
				 throw e;
			}
	}
	
	
	private String generatePaySlipEmailSubject(String payPeriod) {
		String subject = "Payslip for " + payPeriod;
		return subject;
	}

	private String generatePaySlipEmailText(String employeName, String fromPeriod, String toPeriod) {
		String email= "Dear " + employeName + "\r\n" + 
				"\r\n" + 
				"Your payroll has been processed for the period from " + fromPeriod+ " to " + toPeriod + " . Please find attached your payslip for your reference\r\n" + 
				"\r\n" + 
				"Regards,\r\n" + 
				"LCI Team";
		return email;
	}

		

}
