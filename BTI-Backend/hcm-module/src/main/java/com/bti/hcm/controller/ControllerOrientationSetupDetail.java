package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoOrientationSetupDetail;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceOrientationSetupDetail;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/orientationSetupDetail")
public class ControllerOrientationSetupDetail extends BaseController {
	private static final Logger LOGGER = Logger.getLogger(ControllerOrientationSetupDetail.class);
	
	@Autowired
	ServiceOrientationSetupDetail serviceOrientationSetupDetail;

	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteOrientationSetupDetail(HttpServletRequest request, @RequestBody DtoOrientationSetupDetail dtoOrientationSetupDetail) throws Exception {
		LOGGER.info("Delete OrientationSetupDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoOrientationSetupDetail.getIds() != null && !dtoOrientationSetupDetail.getIds().isEmpty()) {
				DtoOrientationSetupDetail dtoOrientationSetupDetail2 =serviceOrientationSetupDetail.deleteOrientationSetupDetail(dtoOrientationSetupDetail.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ACCRUALSCHEDULE_DELETED", false), dtoOrientationSetupDetail2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete OrientationSetupDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
}
