package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Hijri Day class having getter and setter for fields (POJO) Name 
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 * 
 * @author Bansi
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoHijriDay extends DtoHijri{

	private Integer day;


	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}


}
