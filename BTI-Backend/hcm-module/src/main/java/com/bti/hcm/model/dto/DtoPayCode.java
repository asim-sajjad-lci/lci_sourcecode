package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.PayCodeType;

public class DtoPayCode extends DtoBase{

	private Integer id;
	private Integer payCodeTypeId;
	private String payCodeTypeDesc;
	private String payCodeTypeArabicDesc;
	private String payCodeId;
	private String description;
	private String arbicDescription;
	private PayCodeType payType;
	private String baseOnPayCode;
	private BigDecimal baseOnPayCodeAmount;
	private BigDecimal payFactor;
	private BigDecimal payRate;
	private Short unitofPay;
	private Short payperiod;
	private Boolean inActive;
	private BigDecimal amount;
	private List<DtoPayCode> delete;
	private List<DtoTimeCode> listTimeCode;
	private DtoPayCodeType dtoPayCodeType; 
	private Integer deductionId;
	private Integer benefitId;
	private List<Integer> listPayCodeId;
	private Integer baseOnPayCodeId;
	private BigDecimal benefitAmount;
	private String benefitIds;
	private Short method;
	private Integer employeeId;
	private String employeeFirstName;
	private Integer employeeIndexId;
	private String employeeIdInString;
	
	private Integer payTypes;
	
	//Me
	private Integer roundOf;
	private Integer payTypeId;
	
	
	public Integer getRoundOf() {
		return roundOf;
	}
	
	public void setRoundOf(Integer roundOf) {
		this.roundOf = roundOf;
	}

	public Integer getPayTypeId() {
		return payTypeId;
	}
	
	public void setPayTypeId(Integer payTypeId) {
		this.payTypeId = payTypeId;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPayCodeId() {
		return payCodeId;
	}

	public void setPayCodeId(String payCodeId) {
		this.payCodeId = payCodeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArbicDescription() {
		return arbicDescription;
	}

	public void setArbicDescription(String arbicDescription) {
		this.arbicDescription = arbicDescription;
	}

	public PayCodeType getPayType() {
		return payType;
	}

	public void setPayType(PayCodeType payType) {
		this.payType = payType;
	}

	public String getBaseOnPayCode() {
		return baseOnPayCode;
	}

	public void setBaseOnPayCode(String baseOnPayCode) {
		this.baseOnPayCode = baseOnPayCode;
	}

	public BigDecimal getBaseOnPayCodeAmount() {
		return baseOnPayCodeAmount;
	}

	public void setBaseOnPayCodeAmount(BigDecimal baseOnPayCodeAmount) {
		this.baseOnPayCodeAmount = baseOnPayCodeAmount;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

	public BigDecimal getPayRate() {
		return payRate;
	}

	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}

	public Short getUnitofPay() {
		return unitofPay;
	}

	public void setUnitofPay(Short unitofPay) {
		this.unitofPay = unitofPay;
	}

	public Short getPayperiod() {
		return payperiod;
	}

	public void setPayperiod(Short payperiod) {
		this.payperiod = payperiod;
	}

	
	public Boolean getInActive() {
		return inActive;
	}

	public void setInActive(Boolean inActive) {
		this.inActive = inActive;
	}

	public List<DtoPayCode> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoPayCode> delete) {
		this.delete = delete;
	}
	public DtoPayCode() {
	}

	public DtoPayCode(PayCode payCode) {
		this.payCodeId = payCode.getPayCodeId();

	}

	public DtoPayCode(List<PayCode> payCode) {

	}

	public Integer getPayCodeTypeId() {
		return payCodeTypeId;
	}

	public void setPayCodeTypeId(Integer payCodeTypeId) {
		this.payCodeTypeId = payCodeTypeId;
	}

	public String getPayCodeTypeDesc() {
		return payCodeTypeDesc;
	}

	public void setPayCodeTypeDesc(String payCodeTypeDesc) {
		this.payCodeTypeDesc = payCodeTypeDesc;
	}

	public String getPayCodeTypeArabicDesc() {
		return payCodeTypeArabicDesc;
	}

	public void setPayCodeTypeArabicDesc(String payCodeTypeArabicDesc) {
		this.payCodeTypeArabicDesc = payCodeTypeArabicDesc;
	}

	public List<DtoTimeCode> getListTimeCode() {
		return listTimeCode;
	}

	public void setListTimeCode(List<DtoTimeCode> listTimeCode) {
		this.listTimeCode = listTimeCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public DtoPayCodeType getDtoPayCodeType() {
		return dtoPayCodeType;
	}

	public void setDtoPayCodeType(DtoPayCodeType dtoPayCodeType) {
		this.dtoPayCodeType = dtoPayCodeType;
	}

	public List<Integer> getListPayCodeId() {
		return listPayCodeId;
	}

	public void setListPayCodeId(List<Integer> listPayCodeId) {
		this.listPayCodeId = listPayCodeId;
	}

	public Integer getDeductionId() {
		return deductionId;
	}

	public void setDeductionId(Integer deductionId) {
		this.deductionId = deductionId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getBenefitId() {
		return benefitId;
	}

	public void setBenefitId(Integer benefitId) {
		this.benefitId = benefitId;
	}

	public Integer getBaseOnPayCodeId() {
		return baseOnPayCodeId;
	}

	public void setBaseOnPayCodeId(Integer baseOnPayCodeId) {
		this.baseOnPayCodeId = baseOnPayCodeId;
	}

	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public String getBenefitIds() {
		return benefitIds;
	}

	public void setBenefitIds(String benefitIds) {
		this.benefitIds = benefitIds;
	}

	public Integer getPayTypes() {
		return payTypes;
	}

	public void setPayTypes(Integer payTypes) {
		this.payTypes = payTypes;
	}

	public Short getMethod() {
		return method;
	}

	public void setMethod(Short method) {
		this.method = method;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public Integer getEmployeeIndexId() {
		return employeeIndexId;
	}

	public void setEmployeeIndexId(Integer employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}

	public String getEmployeeIdInString() {
		return employeeIdInString;
	}

	public void setEmployeeIdInString(String employeeIdInString) {
		this.employeeIdInString = employeeIdInString;
	}

	
	
}
