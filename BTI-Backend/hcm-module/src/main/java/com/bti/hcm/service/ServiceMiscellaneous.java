package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Dimensions;
import com.bti.hcm.model.Miscellaneous;
import com.bti.hcm.model.Values;
import com.bti.hcm.model.dto.DtoMiscellaneous;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.model.dto.DtoValues;
import com.bti.hcm.repository.RepositoryDimensions;
import com.bti.hcm.repository.RepositoryMiscellaneous;
import com.bti.hcm.repository.RepositoryValues;
import com.bti.hcm.util.CustomIdGenerator;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceMiscellaneous")
public class ServiceMiscellaneous {

	static Logger log = Logger.getLogger(ServiceMiscellaneous.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryMiscellaneous repositoryMiscellaneous;

	@Autowired
	RepositoryValues repositoryValues;

	@Autowired
	RepositoryDimensions repositoryDimensions;

	@Autowired
	CustomIdGenerator customGenerator;

	/**
	 * @Description: save and update Miscellaneous data
	 * @param dtoMiscellaneous
	 * @return
	 *
	 */
//	public DtoMiscellaneous saveOrUpdate(DtoMiscellaneous dtoMiscellaneous) {
//
//		log.info("Miscellaneous save method");
//		try {
//
//			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
//			Miscellaneous miscellaneous = null;
//			if (dtoMiscellaneous.getId() != 0 && dtoMiscellaneous.getId() > 0) {
//				miscellaneous = repositoryMiscellaneous.findByIdAndIsDeleted(dtoMiscellaneous.getId(), false);
//				miscellaneous.setUpdatedBy(loggedInUserId);
//				miscellaneous.setUpdatedDate(new Date());
//			} else {
//				miscellaneous = new Miscellaneous();
//				miscellaneous.setCreatedDate(new Date());
//				Integer rowId = repositoryMiscellaneous.getCountOfTotalMiscellaneousEntries();
//				Integer increment = 0;
//				if (rowId != 0) {
//					increment = rowId + 1;
//				} else {
//					increment = 1;
//				}
//				miscellaneous.setRowId(increment);
//			}
//
//			miscellaneous.setCodeId(dtoMiscellaneous.getCodeId());
//			miscellaneous.setCodeArabicName(dtoMiscellaneous.getCodeArabicName());
//			miscellaneous.setCodeName(dtoMiscellaneous.getCodeName());
//			miscellaneous.setDimension(dtoMiscellaneous.isDimension());
//			miscellaneous.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
//			repositoryMiscellaneous.saveAndFlush(miscellaneous);
//			dtoMiscellaneous.setId(miscellaneous.getId());
//		} catch (Exception e) {
//			log.error(e);
//		}
//		return dtoMiscellaneous;
//	}

	public DtoMiscellaneous saveOrUpdate(DtoMiscellaneous dtoMiscellaneous) {

		log.info("Miscellaneous save method");
		try {

			Dimensions dimensions = new Dimensions();

			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Miscellaneous miscellaneous = null;
			if (dtoMiscellaneous.getId() != 0 && dtoMiscellaneous.getId() > 0) {
				miscellaneous = repositoryMiscellaneous.findByIdAndIsDeleted(dtoMiscellaneous.getId(), false);
				miscellaneous.setUpdatedBy(loggedInUserId);
				miscellaneous.setUpdatedDate(new Date());
			} else {
				miscellaneous = new Miscellaneous();
				miscellaneous.setCreatedDate(new Date());
				Integer rowId = repositoryMiscellaneous.getCountOfTotalMiscellaneousEntries();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				miscellaneous.setRowId(increment);
			}

			miscellaneous.setCodeId(dtoMiscellaneous.getCodeId());
			miscellaneous.setCodeArabicName(dtoMiscellaneous.getCodeArabicName());
			miscellaneous.setCodeName(dtoMiscellaneous.getCodeName());
			miscellaneous.setDimension(dtoMiscellaneous.isDimension());
			miscellaneous.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryMiscellaneous.saveAndFlush(miscellaneous);
			dtoMiscellaneous.setId(miscellaneous.getId());
			if (miscellaneous.isDimension()) {
				Integer rowId = repositoryDimensions.getCountOfTotalDimensionsEntries();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				dimensions.setId(customGenerator.generateCustomId(dimensions.getId()));
				dimensions.setRowId(increment);
				dimensions.setDimensionDesc(miscellaneous.getCodeName());
				dimensions.setTableName("hr40111");
				dimensions.setDimensionArabicDesc(miscellaneous.getCodeArabicName());
				dimensions.setTableIndx(miscellaneous.getId());
				dimensions.setCreatedDate(new Date());
				dimensions.setModuleCode("M-1011");
				dimensions.setColName("valueindx");
				dimensions.setEnglishDescription("valuedescr");
				dimensions.setArabicDescription("valueardescr");
				dimensions.setCode("valueid");
				repositoryDimensions.saveAndFlush(dimensions);
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneous;
	}

	/**
	 * @Description:get All Miscellaneous data
	 * @param dtoSearchActivity
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearchActivity searchMiscellaneousEntry(DtoSearchActivity dtoSearchActivity) {
		log.info("searchMiscellaneousEntry Method");
		try {
			if (dtoSearchActivity != null) {

				String searchWord = dtoSearchActivity.getSearchKeyword();
				dtoSearchActivity = searchByMiscellaneuos(dtoSearchActivity);
				dtoSearchActivity.setTotalCount(this.repositoryMiscellaneous
						.predictiveMiscellaneousCodeSearchTotalCount("%" + searchWord + "%"));

				List<Miscellaneous> miscellaneousList = searchByPageSizeAndNumber(dtoSearchActivity);

				if (miscellaneousList != null && !miscellaneousList.isEmpty()) {
					List<DtoMiscellaneous> dtoMiscellaneousList = new ArrayList<>();
					DtoMiscellaneous dtoMiscellaneous = null;
					for (Miscellaneous miscellaneous : miscellaneousList) {
						dtoMiscellaneous = new DtoMiscellaneous();
						dtoMiscellaneous.setId(miscellaneous.getId());
						dtoMiscellaneous.setCodeArabicName(miscellaneous.getCodeArabicName());
						dtoMiscellaneous.setCodeId(miscellaneous.getCodeId());
						dtoMiscellaneous.setCodeName(miscellaneous.getCodeName());
						dtoMiscellaneous.setDimension(miscellaneous.isDimension());
						dtoMiscellaneousList.add(dtoMiscellaneous);
					}
					dtoSearchActivity.setRecords(dtoMiscellaneousList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearchActivity;
	}

	/**
	 * 
	 * @param dtoSearchActivity
	 * @return
	 */
	public DtoSearchActivity searchByMiscellaneuos(DtoSearchActivity dtoSearchActivity) {

		String condition = "";

		if (dtoSearchActivity.getSortOn() != null || dtoSearchActivity.getSortBy() != null) {
			switch (dtoSearchActivity.getSortOn()) {
			case "codeId":
				condition += dtoSearchActivity.getSortOn();
				break;
			case "codeName":
				condition += dtoSearchActivity.getSortOn();
				break;
			case "codeArabicName":
				condition += dtoSearchActivity.getSortOn();
				break;
			default:
				condition += "id";
			}
		} else {
			condition += "id";
			dtoSearchActivity.setSortOn("");
			dtoSearchActivity.setSortBy("");
		}
		dtoSearchActivity.setCondition(condition);
		return dtoSearchActivity;

	}

	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public List<Miscellaneous> searchByPageSizeAndNumber(DtoSearch dtoSearch) {
		List<Miscellaneous> miscellaneousList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

			if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
				miscellaneousList = this.repositoryMiscellaneous.predictiveMiscellaneousAllIdSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%",
						new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if (dtoSearch.getSortBy().equals("ASC")) {
				miscellaneousList = this.repositoryMiscellaneous.predictiveMiscellaneousAllIdSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
			} else if (dtoSearch.getSortBy().equals("DESC")) {
				miscellaneousList = this.repositoryMiscellaneous.predictiveMiscellaneousAllIdSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
			}

		}

		return miscellaneousList;
	}

	/**
	 * @Description:delete Miscellaneous data
	 * @param ids
	 * @return
	 */
	public DtoMiscellaneous deleteMiscellaneousEntry(List<Integer> ids) {
		log.info("deleteMiscellaneousEntry Method");
		DtoMiscellaneous dtoMiscellaneous = new DtoMiscellaneous();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			dtoMiscellaneous.setDeleteMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("MISCELLANEOUS_ENTRY_DELETED", false));
			dtoMiscellaneous.setAssociateMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("MISCELLANEOUS_ENTRY_ASSOCIATED", false));
			boolean inValidDelete = false;
			StringBuilder invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse
					.getMessageByShortAndIsDeleted("MISCELLANEOUS_ENTRY_NOT_DELETE_ID_MESSAGE", false).getMessage());
			List<DtoMiscellaneous> deleteMiscellaneous = new ArrayList<>();

			for (Integer miscellaneousId : ids) {
				Miscellaneous miscellaneous = repositoryMiscellaneous.findOne(miscellaneousId);
				if (miscellaneous.getEmployeeMaster().isEmpty())

				/*
				 * && miscellaneous.getListEmployeePositionHistory().isEmpty() &&
				 * miscellaneous.getRequisitions().isEmpty()&&miscellaneous.
				 * getPayScheduleSetupDepartment().isEmpty()
				 */

				{
					DtoMiscellaneous dtoMiscellaneous2 = new DtoMiscellaneous();
					dtoMiscellaneous2.setId(miscellaneousId);
					dtoMiscellaneous2.setCodeArabicName(miscellaneous.getCodeArabicName());
					repositoryMiscellaneous.deleteSingleMiscellaneousEntry(true, loggedInUserId, miscellaneousId);
					//ME New
					Dimensions dimension = repositoryDimensions.findByTableIdxAnd5kRange(miscellaneous.getId());
					if(dimension != null && !dimension.equals(null)) {
						repositoryDimensions.deleteDimensionByTableIndx(true, loggedInUserId, miscellaneous.getId());	//ME
						log.info("Deleted from system table too the misc.id:" + miscellaneous.getId());
					}
					deleteMiscellaneous.add(dtoMiscellaneous2);
				} else {
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoMiscellaneous.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoMiscellaneous.setMessageType("");

			}
			dtoMiscellaneous.setDeleteMiscellaneous(deleteMiscellaneous);

			log.debug("Delete Miscellaneous :" + dtoMiscellaneous.getId());
		} catch (Exception e) {
			log.error(e);

		}
		return dtoMiscellaneous;
	}

	/**
	 * @Description:get Miscellaneous data by id
	 * @param id
	 * @return
	 */
	public DtoMiscellaneous getById(int id) {
		log.info("getMiscellaneousByMiscellaneousId Method");
		DtoMiscellaneous dtoMiscellaneous = new DtoMiscellaneous();
		try {
			if (id > 0) {
				Miscellaneous miscellaneous = repositoryMiscellaneous.findByIdAndIsDeleted(id, false);
				if (miscellaneous != null) {
					dtoMiscellaneous = new DtoMiscellaneous();
					dtoMiscellaneous.setId(miscellaneous.getId());
					dtoMiscellaneous.setCodeArabicName(miscellaneous.getCodeArabicName());
					dtoMiscellaneous.setCodeId(miscellaneous.getCodeId());
					dtoMiscellaneous.setCodeName(miscellaneous.getCodeName());
					dtoMiscellaneous.setDimension(miscellaneous.isDimension());
				} else {
					dtoMiscellaneous.setMessageType("MISCELLANEOUS_ENTRY_NOT_FOUND");

				}
			} else {
				dtoMiscellaneous.setMessageType("INVALID_MISCELLANEOUS_ID");

			}
			log.debug("Miscellaneous By Id is:" + dtoMiscellaneous.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneous;
	}

	/*
	 * MiscellaneousId repeat check
	 * 
	 * @param miscellaneousId
	 * 
	 */
	public DtoMiscellaneous repeatByMiscellaneousId(String miscellaneousId) {
		log.info("repeatByMiscellaneousId Method");
		DtoMiscellaneous dtoMiscellaneous = new DtoMiscellaneous();
		try {
			List<Miscellaneous> miscellaneous = repositoryMiscellaneous.findByMiscellaneousId(miscellaneousId.trim());
			if (miscellaneous != null && !miscellaneous.isEmpty()) {
				dtoMiscellaneous.setIsRepeat(true);
			} else {
				dtoMiscellaneous.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneous;
	}

	@Transactional
	@Async
	public List<DtoMiscellaneous> getAllMiscellaneousDropDownList() {
		log.info("getAllMiscellaneousList  Method");
		List<DtoMiscellaneous> dtoMiscellaneousList = new ArrayList<>();
		try {
			List<Miscellaneous> list = repositoryMiscellaneous.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (Miscellaneous miscellaneous : list) {
					DtoMiscellaneous dtoMiscellaneous = new DtoMiscellaneous();
					dtoMiscellaneous.setId(miscellaneous.getId());
					dtoMiscellaneous.setCodeArabicName(miscellaneous.getCodeArabicName());
					dtoMiscellaneous.setCodeId(miscellaneous.getCodeId());
					dtoMiscellaneous.setCodeName(miscellaneous.getCodeName());
					dtoMiscellaneous.setDimension(miscellaneous.isDimension());
					dtoMiscellaneousList.add(dtoMiscellaneous);
				}
			}
			log.debug("Miscellaneous is:" + dtoMiscellaneousList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneousList;
	}

	public DtoSearchActivity searchMiscellaneousEntryWithValues(DtoSearchActivity dtoSearchActivity) {
		log.info("searchMiscellaneousWithValues Method");
		try {
			if (dtoSearchActivity != null) {
				String searchWord = dtoSearchActivity.getSearchKeyword();
				dtoSearchActivity = searchByMiscellaneuos(dtoSearchActivity);
				dtoSearchActivity.setTotalCount(this.repositoryMiscellaneous
						.predictiveMiscellaneousCodeSearchTotalCount("%" + searchWord + "%"));

				List<Miscellaneous> miscellaneousList = searchByPageSizeAndNumber(dtoSearchActivity);
				if (miscellaneousList != null && !miscellaneousList.isEmpty()) {
					List<DtoMiscellaneous> dtoMiscellaneousList = new ArrayList<>();
					DtoMiscellaneous dtoMiscellaneous = null;
					for (Miscellaneous miscellaneous : miscellaneousList) {
						dtoMiscellaneous = new DtoMiscellaneous();
						dtoMiscellaneous.setId(miscellaneous.getId());
						dtoMiscellaneous.setCodeArabicName(miscellaneous.getCodeArabicName());
						dtoMiscellaneous.setCodeId(miscellaneous.getCodeId());
						dtoMiscellaneous.setCodeName(miscellaneous.getCodeName());
						dtoMiscellaneous.setDimension(miscellaneous.isDimension());

						List<Values> valuesList = repositoryValues.findAllMiscId(miscellaneous.getId());
						if (valuesList != null && !valuesList.isEmpty()) {
							List<DtoValues> dtoValuesList = new ArrayList<>();
							for (Values values : valuesList) {
								DtoValues dtoValues = new DtoValues();
								dtoValues.setId(values.getId());
								dtoValues.setValueId(values.getValueId());
								dtoValues.setMiscId(values.getMiscellaneous().getId());
								dtoValuesList.add(dtoValues);
							}
							dtoMiscellaneous.setDtoValuesList(dtoValuesList);

						}

						dtoMiscellaneousList.add(dtoMiscellaneous);

					}
					dtoSearchActivity.setRecords(dtoMiscellaneousList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearchActivity;
	}

}
