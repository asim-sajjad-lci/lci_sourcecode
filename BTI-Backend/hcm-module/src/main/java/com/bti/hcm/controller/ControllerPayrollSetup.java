package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPayrollSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayrollSetup;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/payrollSetup")
public class ControllerPayrollSetup extends BaseController{

	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetup Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerPayScheduleSetup.class);
	
	/**
	 * @Description servicePayrollSetup Autowired here using annotation of spring for use of servicePayrollSetup method in this controller
	 */
	@Autowired(required=true)
	ServicePayrollSetup servicePayrollSetup;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoPayrollSetup dtoPayrollSetup) throws Exception {
		LOGGER.info("Create PayrollSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayrollSetup = servicePayrollSetup.saveOrUpdate(dtoPayrollSetup);
			responseMessage=displayMessage(dtoPayrollSetup, "PAYROLL_SETUP_CREATED", "PAYROLL_SETUP_NOT_CREATED", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PayrollSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoPayrollSetup dtoPayrollSetup) throws Exception {
		LOGGER.info("Update PayrollSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayrollSetup = servicePayrollSetup.saveOrUpdate(dtoPayrollSetup);
			responseMessage=displayMessage(dtoPayrollSetup, "PAYROLL_SETUP_UPDATED_SUCCESS", "PAYROLL_SETUP_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update PayrollSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePayScheduleSetup(HttpServletRequest request, @RequestBody DtoPayrollSetup dtoPayrollSetup) throws Exception {
		LOGGER.info("Delete PayrollSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayrollSetup.getIds() != null && !dtoPayrollSetup.getIds().isEmpty()) {
				DtoPayrollSetup dtoPayrollSetupObj = servicePayrollSetup.delete(dtoPayrollSetup.getIds());
				
				
				if(dtoPayrollSetupObj.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("PAYROLL_SETUP_DELETED", false), dtoPayrollSetupObj);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("PAYROLL_SETUP_DELETED_ID_MESSAGE", false), dtoPayrollSetupObj);


				}
				
				
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete PayrollSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPayScheduleSetup(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search PayrollSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayrollSetup.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.PAYROLL_SETUP_GET_ALL, MessageConstant.PAYROLL_SETUP_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPayScheduleSetupById(HttpServletRequest request, @RequestBody DtoPayrollSetup dtoPayrollSetup) throws Exception {
		LOGGER.info("Get PayrollSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayrollSetup dtoPayrollSetupObj = servicePayrollSetup.getById(dtoPayrollSetup.getId());
			responseMessage=displayMessage(dtoPayrollSetupObj, "PAYROLL_SETUP_GET_ALL", "PAYROLL_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get PayrollSetup ById Method:"+dtoPayrollSetup.getId());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllDroupdownDimension", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllDroupdown(HttpServletRequest request,@RequestBody DtoSearch search) throws Exception {
		LOGGER.info("Search MaterialsSetupBill Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			search = this.servicePayrollSetup.getAllDroupdownDimension(search);
			responseMessage=displayMessage(search, "PAYROLL_SETUP_GET_ALL", "PAYROLL_SETUP_LIST_NOT_GETTING", serviceResponse);
			
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Search MaterialsSetupBill Method:"+search);
		return responseMessage;
	}
	
}
