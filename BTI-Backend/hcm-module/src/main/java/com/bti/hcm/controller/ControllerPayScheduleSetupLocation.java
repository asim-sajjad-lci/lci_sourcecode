/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPayScheduleSetupLocation;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayScheduleSetupLocation;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: Controller PayScheduleSetupLocation
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/payScheduleSetupLocation")
public class ControllerPayScheduleSetupLocation extends BaseController{
	
	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupLocation Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerPayScheduleSetupLocation.class);
	
	/**
	 * @Description servicePayScheduleSetupLocation Autowired here using annotation of spring for use of servicePayScheduleSetupLocation method in this controller
	 */
	@Autowired(required=true)
	ServicePayScheduleSetupLocation servicePayScheduleSetupLocation;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupLocation
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation) throws Exception {
		LOGGER.info("Create PayScheduleSetupLocation Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupLocation = servicePayScheduleSetupLocation.saveOrUpdatePayScheduleSetupLocation(dtoPayScheduleSetupLocation);
		    responseMessage=displayMessage(dtoPayScheduleSetupLocation, "PAY_SCHEDULE_SETUP_LOCATION_CREATED", "PAY_SCHEDULE_SETUP_LOCATION_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PayScheduleSetupLocation Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupLocation
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePayScheduleSetupPosition(HttpServletRequest request, @RequestBody DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation) throws Exception {
		LOGGER.info("Update PayScheduleSetupLocation Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupLocation = servicePayScheduleSetupLocation.saveOrUpdatePayScheduleSetupLocation(dtoPayScheduleSetupLocation);
			responseMessage=displayMessage(dtoPayScheduleSetupLocation, "PAY_SCHEDULE_SETUP_LOCATION_UPDATED_SUCCESS", "PAY_SCHEDULE_SETUP_LOCATION_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update PayScheduleSetupLocation Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupLocation
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation) throws Exception {
		LOGGER.info("Delete PayScheduleSetupLocation Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayScheduleSetupLocation.getIds() != null && !dtoPayScheduleSetupLocation.getIds().isEmpty()) {
				DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation2 = servicePayScheduleSetupLocation.deletePayScheduleSetupLocation(dtoPayScheduleSetupLocation.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_LOCATION_LIST_DELETED", false), dtoPayScheduleSetupLocation2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete PayScheduleSetupLocation Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search PayScheduleSetupLocation Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayScheduleSetupLocation.searchPayScheduleSetupLocation(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_SCHEDULE_SETUP_LOCATION_GET_ALL", "PAY_SCHEDULE_SETUP_LOCATION_LIST_NOT_GETTING", serviceResponse);
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PayScheduleSetupLocation Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
}
