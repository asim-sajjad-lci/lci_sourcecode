/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.InterviewTypeSetup;
import com.bti.hcm.util.UtilRandomKey;

/**
 * Description: DTO InterviewTypeSetup  class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
public class DtoInterviewTypeSetup extends DtoBase{
	
	private Integer id;
	private String interviewTypeId;
	private String interviewTypeDescription;
	private String interviewTypeDescriptionArabic;
	private Integer interviewTypeDetailId;
	private Integer interviewTypeSetupDetailCategoryRowSequance;
	private String interviewTypeSetupDetailDescription;
	private Integer interviewTypeSetupDetailCategorySequance;
	private Integer interviewTypeSetupDetailCategoryWeight;
	private Integer interviewTypeSetupId;
	private List<DtoInterviewTypeSetup> deleteInterviewTypeSetup;
	private Integer interviewRange;
	private List<DtoInterviewTypeSetupDetail> dtoInterviewTypeSetupDetail;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getInterviewTypeId() {
		return interviewTypeId;
	}
	public void setInterviewTypeId(String interviewTypeId) {
		this.interviewTypeId = interviewTypeId;
	}
	public String getInterviewTypeDescription() {
		return interviewTypeDescription;
	}
	public void setInterviewTypeDescription(String interviewTypeDescription) {
		this.interviewTypeDescription = interviewTypeDescription;
	}
	public String getInterviewTypeDescriptionArabic() {
		return interviewTypeDescriptionArabic;
	}
	public void setInterviewTypeDescriptionArabic(String interviewTypeDescriptionArabic) {
		this.interviewTypeDescriptionArabic = interviewTypeDescriptionArabic;
	}
	public List<DtoInterviewTypeSetup> getDeleteInterviewTypeSetup() {
		return deleteInterviewTypeSetup;
	}
	public void setDeleteInterviewTypeSetup(List<DtoInterviewTypeSetup> deleteInterviewTypeSetup) {
		this.deleteInterviewTypeSetup = deleteInterviewTypeSetup;
	}
	public DtoInterviewTypeSetup() {
		
	}
	public Integer getInterviewRange() {
		return interviewRange;
	}
	public void setInterviewRange(Integer interviewRange) {
		this.interviewRange = interviewRange;
	}
	public Integer getInterviewTypeSetupDetailCategoryRowSequance() {
		return interviewTypeSetupDetailCategoryRowSequance;
	}
	public void setInterviewTypeSetupDetailCategoryRowSequance(Integer interviewTypeSetupDetailCategoryRowSequance) {
		this.interviewTypeSetupDetailCategoryRowSequance = interviewTypeSetupDetailCategoryRowSequance;
	}
	public String getInterviewTypeSetupDetailDescription() {
		return interviewTypeSetupDetailDescription;
	}
	public void setInterviewTypeSetupDetailDescription(String interviewTypeSetupDetailDescription) {
		this.interviewTypeSetupDetailDescription = interviewTypeSetupDetailDescription;
	}
	public Integer getInterviewTypeSetupDetailCategorySequance() {
		return interviewTypeSetupDetailCategorySequance;
	}
	public void setInterviewTypeSetupDetailCategorySequance(Integer interviewTypeSetupDetailCategorySequance) {
		this.interviewTypeSetupDetailCategorySequance = interviewTypeSetupDetailCategorySequance;
	}
	public Integer getInterviewTypeSetupDetailCategoryWeight() {
		return interviewTypeSetupDetailCategoryWeight;
	}
	public void setInterviewTypeSetupDetailCategoryWeight(Integer interviewTypeSetupDetailCategoryWeight) {
		this.interviewTypeSetupDetailCategoryWeight = interviewTypeSetupDetailCategoryWeight;
	}
	public Integer getInterviewTypeSetupId() {
		return interviewTypeSetupId;
	}
	public void setInterviewTypeSetupId(Integer interviewTypeSetupId) {
		this.interviewTypeSetupId = interviewTypeSetupId;
	}
	public DtoInterviewTypeSetup(InterviewTypeSetup interviewTypeSetup) {
		if (UtilRandomKey.isNotBlank(interviewTypeSetup.getInterviewTypeId())) {
			this.interviewTypeId = interviewTypeSetup.getInterviewTypeId();
		} else {
			this.interviewTypeId = "";
		}
		
		if (UtilRandomKey.isNotBlank(interviewTypeSetup.getInterviewTypeDescription())) {
			this.interviewTypeDescription = interviewTypeSetup.getInterviewTypeDescription();
		} else {
			this.interviewTypeDescription = "";
		}
		
		
		if (UtilRandomKey.isNotBlank(interviewTypeSetup.getInterviewTypeDescriptionArabic())) {
			this.interviewTypeDescriptionArabic = interviewTypeSetup.getInterviewTypeDescriptionArabic();
		} else {
			this.interviewTypeDescriptionArabic = "";
		}
	}
	public Integer getInterviewTypeDetailId() {
		return interviewTypeDetailId;
	}
	public void setInterviewTypeDetailId(Integer interviewTypeDetailId) {
		this.interviewTypeDetailId = interviewTypeDetailId;
	}
	public List<DtoInterviewTypeSetupDetail> getDtoInterviewTypeSetupDetail() {
		return dtoInterviewTypeSetupDetail;
	}
	public void setDtoInterviewTypeSetupDetail(List<DtoInterviewTypeSetupDetail> dtoInterviewTypeSetupDetail) {
		this.dtoInterviewTypeSetupDetail = dtoInterviewTypeSetupDetail;
	}
	
	

}
