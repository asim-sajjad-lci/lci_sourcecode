package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.SettingFamilyLeave;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSettingFamilyLeave;
import com.bti.hcm.repository.RepositorySettingFamilyLeave;

@Service("serviceSettingFamilyLeave")
public class ServiceSettingFamilyLeave {
	
	/**
	 * @Description LOGGER use for put a logger in SettingFamilyLeave Service
	 */
	static Logger log = Logger.getLogger(ServiceSettingFamilyLeave.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in SettingFamilyLeave service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in SettingFamilyLeave service
	 */
	@Autowired
	ServiceResponse response;
	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in SettingFamilyLeave service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	
	/**
	 * @Description repositorySettingFamilyLeave Autowired here using annotation of spring for access of repositorySettingFamilyLeave method in SettingFamilyLeave service
	 */
	@Autowired(required=false)
	RepositorySettingFamilyLeave repositorySettingFamilyLeave;
	
	
	
	/**
	 * @param dtoSettingFamilyLeave
	 * @return
	 */
	
	public DtoSettingFamilyLeave saveOrUpdate(DtoSettingFamilyLeave dtoSettingFamilyLeave) {
		log.info("saveOrUpdate settingFamilyLeave Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		SettingFamilyLeave settingFamilyLeave = null;
		if (dtoSettingFamilyLeave.getId() != null && dtoSettingFamilyLeave.getId() > 0) {
			
			settingFamilyLeave = repositorySettingFamilyLeave.findByIdAndIsDeleted(dtoSettingFamilyLeave.getId(), false);
			settingFamilyLeave.setUpdatedBy(loggedInUserId);
			settingFamilyLeave.setUpdatedDate(new Date());
		} else {
			settingFamilyLeave = new SettingFamilyLeave();
			settingFamilyLeave.setCreatedDate(new Date());
			
			Integer rowId = repositorySettingFamilyLeave.getCountOfTotaSettingFamilyLeave();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			settingFamilyLeave.setRowId(increment);
		}
		settingFamilyLeave.setMethod(dtoSettingFamilyLeave.getMethod());
		settingFamilyLeave.setIsThisPeriod(dtoSettingFamilyLeave.getIsThisPeriod());
		settingFamilyLeave.setPeriodStart(dtoSettingFamilyLeave.getPeriodStart());
		settingFamilyLeave = repositorySettingFamilyLeave.saveAndFlush(settingFamilyLeave);
		log.debug("SettingFamilyLeave is:"+settingFamilyLeave.getId());
		return dtoSettingFamilyLeave;
	
	}
	
	/**
	 * @param ids
	 * @return
	 */
	public DtoSettingFamilyLeave delete(List<Integer> ids) {
		log.info("delete SettingFamilyLeave Method");
		DtoSettingFamilyLeave dtoSettingFamilyLeave = new DtoSettingFamilyLeave();
		dtoSettingFamilyLeave
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SHIFT_CODE_DELETED", false));
		dtoSettingFamilyLeave.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SHIFT_CODE_DELETED", false));
		List<DtoSettingFamilyLeave> deleteSettingFamilyLeave = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer id : ids) {
				SettingFamilyLeave settingFamilyLeave = repositorySettingFamilyLeave.findOne(id);
				if(settingFamilyLeave!=null) {
					DtoSettingFamilyLeave dtoSettingFamilyLeave1 = new DtoSettingFamilyLeave();
					dtoSettingFamilyLeave1.setId(settingFamilyLeave.getId());
					repositorySettingFamilyLeave.deleteSingleSettingFamilyLeave(true, loggedInUserId, id);
					deleteSettingFamilyLeave.add(dtoSettingFamilyLeave1);
				}else {
					inValidDelete = true;
				}
				

			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoSettingFamilyLeave.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoSettingFamilyLeave.setMessageType("");
				
			}
			
			dtoSettingFamilyLeave.setDelete(deleteSettingFamilyLeave);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete SettingFamilyLeave :"+dtoSettingFamilyLeave.getId());
		return dtoSettingFamilyLeave;
	}
	
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search SettingFamilyLeave Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				if(dtoSearch.getSortOn().equals("method") || dtoSearch.getSortOn().equals("isThisPeriod") || dtoSearch.getSortOn().equals("periodStart")) {
					condition = dtoSearch.getSortOn();
				
			}else {
				condition = "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
			
		}
			dtoSearch.setTotalCount(this.repositorySettingFamilyLeave.predictiveSettingFamilyLeaveSearchTotalCount());
			List<SettingFamilyLeave> payCodeList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					payCodeList = this.repositorySettingFamilyLeave.predictiveSettingFamilyLeaveSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					payCodeList = this.repositorySettingFamilyLeave.predictiveSettingFamilyLeaveSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					payCodeList = this.repositorySettingFamilyLeave.predictiveSettingFamilyLeaveSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
		
			if(payCodeList != null && !payCodeList.isEmpty()){
				List<DtoSettingFamilyLeave> dtoTimeCodeList = new ArrayList<>();
				DtoSettingFamilyLeave dtoSettingFamilyLeave=null;
				for (SettingFamilyLeave settingFamilyLeave : payCodeList) {
					dtoSettingFamilyLeave = new DtoSettingFamilyLeave(settingFamilyLeave);
					dtoSettingFamilyLeave.setId(settingFamilyLeave.getId());
					dtoSettingFamilyLeave.setMethod(settingFamilyLeave.getMethod());
					dtoSettingFamilyLeave.setIsThisPeriod(settingFamilyLeave.getIsThisPeriod());
					dtoSettingFamilyLeave.setPeriodStart(settingFamilyLeave.getPeriodStart());
					dtoSettingFamilyLeave.setId(settingFamilyLeave.getId());
					dtoTimeCodeList.add(dtoSettingFamilyLeave);
				}
				dtoSearch.setRecords(dtoTimeCodeList);
			}
		}
		return dtoSearch;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	public DtoSettingFamilyLeave getById(int id) {
		log.info("getById Method");
		DtoSettingFamilyLeave dtoSettingFamilyLeave = new DtoSettingFamilyLeave();
		if (id > 0) {
			SettingFamilyLeave payCode = repositorySettingFamilyLeave.findByIdAndIsDeleted(id, false);
			if (payCode != null) {
				dtoSettingFamilyLeave = new DtoSettingFamilyLeave(payCode);
				
				dtoSettingFamilyLeave.setMethod(payCode.getMethod());
				dtoSettingFamilyLeave.setIsThisPeriod(payCode.getIsThisPeriod());
				dtoSettingFamilyLeave.setPeriodStart(payCode.getPeriodStart());
				dtoSettingFamilyLeave.setId(payCode.getId());
				
			} else {
				dtoSettingFamilyLeave.setMessageType("SHIFT_CODE_NOT_GETTING");

			}
		} else {
			dtoSettingFamilyLeave.setMessageType("SHIFT_CODE_NOT_GETTING");

		}
		log.debug("SettingFamilyLeave By Id is:"+dtoSettingFamilyLeave.getId());
		return dtoSettingFamilyLeave;
	}
	
	

	
	
	public DtoSearch getIds(DtoSearch dtoSearch) {
		log.info("searchShiftCodeId Method");
		if(dtoSearch != null){
			
			List<SettingFamilyLeave> shiftCodeIdList =null;
			List<DtoSettingFamilyLeave> listDtoSettingFamilyLeave =new ArrayList<>();
			
				shiftCodeIdList = this.repositorySettingFamilyLeave.findAll();
				if(!shiftCodeIdList.isEmpty()) {
					
					for (SettingFamilyLeave dtoSettingFamilyLeave : shiftCodeIdList) {
						DtoSettingFamilyLeave dtoSettingFamilyLeave2 = new DtoSettingFamilyLeave();
						dtoSettingFamilyLeave2.setMethod(dtoSettingFamilyLeave.getMethod());
						dtoSettingFamilyLeave2.setIsThisPeriod(dtoSettingFamilyLeave.getIsThisPeriod());
						dtoSettingFamilyLeave2.setPeriodStart(dtoSettingFamilyLeave.getPeriodStart());
						dtoSettingFamilyLeave2.setId(dtoSettingFamilyLeave.getId());
						listDtoSettingFamilyLeave.add(dtoSettingFamilyLeave2);
					}
					
					
				}
			dtoSearch.setRecords(listDtoSettingFamilyLeave);
			dtoSearch.setTotalCount(listDtoSettingFamilyLeave.size());
		}
		
		return dtoSearch;
	}
}
