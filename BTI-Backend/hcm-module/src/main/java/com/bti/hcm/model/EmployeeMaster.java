/**

 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the EmployeeMaster database table. Name
 * of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00101",indexes = {
        @Index(columnList = "EMPLOYINDX")
})
@NamedQuery(name = "EmployeeMaster.findAll", query = "SELECT e FROM EmployeeMaster e")
public class EmployeeMaster extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPLOYINDX")
	private int employeeIndexId;

	@Column(name = "EMPLOYID", columnDefinition = "char(10)")
	private String employeeId;

	@Column(name = "EMPTITL", columnDefinition = "char(5)")
	private String employeeTitle;

	@Column(name = "EMPTITLA", columnDefinition = "char(31)")
	private String employeeTitleArabic;

	@Column(name = "EMPLSTNM", columnDefinition = "char(31)")
	private String employeeLastName;

	@Column(name = "EMPFRTNM", columnDefinition = "char(31)")
	private String employeeFirstName;

	@Column(name = "EMPMDLNM", columnDefinition = "char(31)")
	private String employeeMiddleName;

	@Column(name = "EMPLSTNMA", columnDefinition = "char(31)")
	private String employeeLastNameArabic;

	@Column(name = "EMPFRTNMA", columnDefinition = "char(31)")
	private String employeeFirstNameArabic;

	@Column(name = "EMPMDLNMA", columnDefinition = "char(31)")
	private String employeeMiddleNameArabic;

	@Column(name = "EMPHIRDT")
	private Date employeeHireDate;

	@Column(name = "EMPAHIRDT")
	@Temporal(TemporalType.DATE)
	private Date employeeAdjustHireDate;

	@Column(name = "EMPLSTDT")
	private Date employeeLastWorkDate;

	@Column(name = "EMPDCDT")
	private Date employeeInactiveDate;

	@Column(name = "EMPRESN", columnDefinition = "char(31)")
	private String employeeInactiveReason;

	@Column(name = "EMPLTYP")
	private short employeeType;

	@Column(name = "EMPGEND")
	private short employeeGender;

	@Column(name = "EMPMAST")
	private short employeeMaritalStatus;

	@Column(name = "EMPBOR")
	private Date employeeBirthDate;
	
	@Column(name = "EMPBORH")
	private String employeeBirthDateHijri;

	@Column(name = "EMPUSERID")
	private int employeeUserIdInSystem;

	@Column(name = "EMPWRKHR")
	private int employeeWorkHourYearly;

	@Column(name = "EMPCITZN")
	private boolean employeeCitizen;

	@Column(name = "EMPACTIV")
	private Boolean employeeInactive;

	@Column(name = "EMPIMAGR")
	private boolean employeeImmigration;

	@Column(name = "EMPSMOK")
	private boolean employeeSmoker;
	
	@Column(name = "EMPIDNMBR", columnDefinition = "char(31)")
	private String idNumber;
	
	@Column(name = "EMPIDEXPR")
	private Date expire;
	
	@Column(name = "EMPIDEXPRH")
	private String expireHijri;
	
	@Column(name = "EMPPASNMR",columnDefinition = "char(31)")
	private String passportNumber;
	
	@Column(name = "EMPPASEXPR")
	private Date expires;
	
	@Column(name = "EMPPASEXPRH")
	private String expiresHijri;
	
	@Column(name = "empnts")
	private String employeeNotes;

	@Column(name = "empemail")
	private String employeeEmail;

	@Column(name = "bnkactnmbr")
	private String employeeBankAccountNumber;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "BNKID")
	private Bank bank;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DIVINDX")
	private Division division;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DEPINDX")
	private Department department;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "LOCINDX")
	private Location location;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "SUPRINDX")
	private Supervisor supervisor;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "POTINDX")
	private Position position;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "EMPNAINDX")
	private EmployeeNationalities employeeNationalities;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "EMPADDINDX")
	private EmployeeAddressMaster employeeAddressMaster;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<EmployeeDirectDeposit> listEmployeeDirectDeposit;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
//	@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeeDeductionMaintenance> listEmployeeDeductionMaintenance;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeeBenefitMaintenance> listEmployeeBenefitMaintenance;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeePayCodeMaintenance> listEmployeePayCodeMaintenance;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<HealthInsuranceEnrollment> listHealthInsuranceEnrollment;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeeEducation> listEmployeeEducation;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<MiscellaneousBenefirtEnrollment> listMiscellaneousBenefirtEnrollment;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<ActivateEmployeePostDatePayRate> listActivateEmployeePostDatePayRate;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeePostDatedPayRates> listEmployeePostDatedPayRates;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeeDependent> listEmployeeDependent;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<ManualChecks> listManualChecks;
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	@Where(clause = "is_deleted = false")
	private List<Distribution> listDistribution;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "employeeMaster")
	@Where(clause = "is_deleted = false")
	private List<PayrollTransactionOpenYearDetails> listPayrollTransactionOpenYearDetails;


	@OneToMany
	@JoinColumn(name="employindx")
	private List<MiscellaneousEmployee> miscellaneousEmployee;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employeeMaster")
	@Where(clause = "is_deleted = false")
	private List<ApplicantHire> applicantHire;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "PROINDEX")
	private ProjectSetup projectSetup;
	
	
	@Lob
	@Column(name = "EMPIMG")
	private byte[] employeeImage;

	public int getEmployeeIndexId() {
		return employeeIndexId;
	}

	public void setEmployeeIndexId(int employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeTitle() {
		return employeeTitle;
	}

	public void setEmployeeTitle(String employeeTitle) {
		this.employeeTitle = employeeTitle;
	}

	public String getEmployeeTitleArabic() {
		return employeeTitleArabic;
	}

	public void setEmployeeTitleArabic(String employeeTitleArabic) {
		this.employeeTitleArabic = employeeTitleArabic;
	}

	public String getEmployeeLastName() {
		return employeeLastName;
	}

	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public String getEmployeeMiddleName() {
		return employeeMiddleName;
	}

	public void setEmployeeMiddleName(String employeeMiddleName) {
		this.employeeMiddleName = employeeMiddleName;
	}

	public String getEmployeeLastNameArabic() {
		return employeeLastNameArabic;
	}

	public void setEmployeeLastNameArabic(String employeeLastNameArabic) {
		this.employeeLastNameArabic = employeeLastNameArabic;
	}

	public String getEmployeeFirstNameArabic() {
		return employeeFirstNameArabic;
	}

	public void setEmployeeFirstNameArabic(String employeeFirstNameArabic) {
		this.employeeFirstNameArabic = employeeFirstNameArabic;
	}

	public String getEmployeeMiddleNameArabic() {
		return employeeMiddleNameArabic;
	}

	public void setEmployeeMiddleNameArabic(String employeeMiddleNameArabic) {
		this.employeeMiddleNameArabic = employeeMiddleNameArabic;
	}

	public Date getEmployeeHireDate() {
		return employeeHireDate;
	}

	public void setEmployeeHireDate(Date employeeHireDate) {
		this.employeeHireDate = employeeHireDate;
	}

	public Date getEmployeeAdjustHireDate() {
		return employeeAdjustHireDate;
	}

	public void setEmployeeAdjustHireDate(Date employeeAdjustHireDate) {
		this.employeeAdjustHireDate = employeeAdjustHireDate;
	}

	public Date getEmployeeLastWorkDate() {
		return employeeLastWorkDate;
	}

	public void setEmployeeLastWorkDate(Date employeeLastWorkDate) {
		this.employeeLastWorkDate = employeeLastWorkDate;
	}

	public Date getEmployeeInactiveDate() {
		return employeeInactiveDate;
	}

	public void setEmployeeInactiveDate(Date employeeInactiveDate) {
		this.employeeInactiveDate = employeeInactiveDate;
	}

	public Boolean getEmployeeInactive() {
		return employeeInactive;
	}

	public void setEmployeeInactive(Boolean employeeInactive) {
		this.employeeInactive = employeeInactive;
	}

	public String getEmployeeInactiveReason() {
		return employeeInactiveReason;
	}

	public boolean isEmployeeSmoker() {
		return employeeSmoker;
	}

	public void setEmployeeSmoker(boolean employeeSmoker) {
		this.employeeSmoker = employeeSmoker;
	}

	public void setEmployeeInactiveReason(String employeeInactiveReason) {
		this.employeeInactiveReason = employeeInactiveReason;
	}

	public short getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(short employeeType) {
		this.employeeType = employeeType;
	}

	public short getEmployeeGender() {
		return employeeGender;
	}

	public void setEmployeeGender(short employeeGender) {
		this.employeeGender = employeeGender;
	}

	public short getEmployeeMaritalStatus() {
		return employeeMaritalStatus;
	}

	public void setEmployeeMaritalStatus(short employeeMaritalStatus) {
		this.employeeMaritalStatus = employeeMaritalStatus;
	}

	public Date getEmployeeBirthDate() {
		return employeeBirthDate;
	}

	public void setEmployeeBirthDate(Date employeeBirthDate) {
		this.employeeBirthDate = employeeBirthDate;
	}

	public String getEmployeeBirthDateHijri() {
		return employeeBirthDateHijri;
	}

	public void setEmployeeBirthDateHijri(String employeeBirthDateHijri) {
		this.employeeBirthDateHijri = employeeBirthDateHijri;
	}

	public int getEmployeeUserIdInSystem() {
		return employeeUserIdInSystem;
	}

	public void setEmployeeUserIdInSystem(int employeeUserIdInSystem) {
		this.employeeUserIdInSystem = employeeUserIdInSystem;
	}

	public int getEmployeeWorkHourYearly() {
		return employeeWorkHourYearly;
	}

	public void setEmployeeWorkHourYearly(int employeeWorkHourYearly) {
		this.employeeWorkHourYearly = employeeWorkHourYearly;
	}

	public boolean isEmployeeCitizen() {
		return employeeCitizen;
	}

	public void setEmployeeCitizen(boolean employeeCitizen) {
		this.employeeCitizen = employeeCitizen;
	}

	public boolean isEmployeeInactive() {
		return employeeInactive;
	}

	public void setEmployeeInactive(boolean employeeInactive) {
		this.employeeInactive = employeeInactive;
	}

	public boolean isEmployeeImmigration() {
		return employeeImmigration;
	}

	public void setEmployeeImmigration(boolean employeeImmigration) {
		this.employeeImmigration = employeeImmigration;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public EmployeeAddressMaster getEmployeeAddressMaster() {
		return employeeAddressMaster;
	}

	public void setEmployeeAddressMaster(EmployeeAddressMaster employeeAddressMaster) {
		this.employeeAddressMaster = employeeAddressMaster;
	}

	public EmployeeNationalities getEmployeeNationalities() {
		return employeeNationalities;
	}

	public void setEmployeeNationalities(EmployeeNationalities employeeNationalities) {
		this.employeeNationalities = employeeNationalities;
	}

	public byte[] getEmployeeImage() {
		return employeeImage;
	}

	public void setEmployeeImage(byte[] employeeImage) {
		this.employeeImage = employeeImage;
	}

	public List<ApplicantHire> getApplicantHire() {
		return applicantHire;
	}

	public void setApplicantHire(List<ApplicantHire> applicantHire) {
		this.applicantHire = applicantHire;
	}

	public List<EmployeeDirectDeposit> getListEmployeeDirectDeposit() {
		return listEmployeeDirectDeposit;
	}

	public void setListEmployeeDirectDeposit(List<EmployeeDirectDeposit> listEmployeeDirectDeposit) {
		this.listEmployeeDirectDeposit = listEmployeeDirectDeposit;
	}

	public List<EmployeeBenefitMaintenance> getListEmployeeBenefitMaintenance() {
		return listEmployeeBenefitMaintenance;
	}

	public void setListEmployeeBenefitMaintenance(List<EmployeeBenefitMaintenance> listEmployeeBenefitMaintenance) {
		this.listEmployeeBenefitMaintenance = listEmployeeBenefitMaintenance;
	}

	public List<EmployeeDeductionMaintenance> getListEmployeeDeductionMaintenance() {
		return listEmployeeDeductionMaintenance;
	}

	public void setListEmployeeDeductionMaintenance(
			List<EmployeeDeductionMaintenance> listEmployeeDeductionMaintenance) {
		this.listEmployeeDeductionMaintenance = listEmployeeDeductionMaintenance;
	}

	public List<EmployeePayCodeMaintenance> getListEmployeePayCodeMaintenance() {
		return listEmployeePayCodeMaintenance;
	}

	public void setListEmployeePayCodeMaintenance(List<EmployeePayCodeMaintenance> listEmployeePayCodeMaintenance) {
		this.listEmployeePayCodeMaintenance = listEmployeePayCodeMaintenance;
	}

	public List<HealthInsuranceEnrollment> getListHealthInsuranceEnrollment() {
		return listHealthInsuranceEnrollment;
	}

	public void setListHealthInsuranceEnrollment(List<HealthInsuranceEnrollment> listHealthInsuranceEnrollment) {
		this.listHealthInsuranceEnrollment = listHealthInsuranceEnrollment;
	}

	public List<EmployeeEducation> getListEmployeeEducation() {
		return listEmployeeEducation;
	}

	public void setListEmployeeEducation(List<EmployeeEducation> listEmployeeEducation) {
		this.listEmployeeEducation = listEmployeeEducation;
	}

	public List<MiscellaneousBenefirtEnrollment> getListMiscellaneousBenefirtEnrollment() {
		return listMiscellaneousBenefirtEnrollment;
	}

	public void setListMiscellaneousBenefirtEnrollment(
			List<MiscellaneousBenefirtEnrollment> listMiscellaneousBenefirtEnrollment) {
		this.listMiscellaneousBenefirtEnrollment = listMiscellaneousBenefirtEnrollment;
	}

	public List<EmployeeDependent> getListEmployeeDependent() {
		return listEmployeeDependent;
	}

	public void setListEmployeeDependent(List<EmployeeDependent> listEmployeeDependent) {
		this.listEmployeeDependent = listEmployeeDependent;
	}

	public List<ActivateEmployeePostDatePayRate> getListActivateEmployeePostDatePayRate() {
		return listActivateEmployeePostDatePayRate;
	}

	public void setListActivateEmployeePostDatePayRate(
			List<ActivateEmployeePostDatePayRate> listActivateEmployeePostDatePayRate) {
		this.listActivateEmployeePostDatePayRate = listActivateEmployeePostDatePayRate;
	}

	public List<EmployeePostDatedPayRates> getListEmployeePostDatedPayRates() {
		return listEmployeePostDatedPayRates;
	}

	public void setListEmployeePostDatedPayRates(List<EmployeePostDatedPayRates> listEmployeePostDatedPayRates) {
		this.listEmployeePostDatedPayRates = listEmployeePostDatedPayRates;
	}

	public List<ManualChecks> getListManualChecks() {
		return listManualChecks;
	}

	public void setListManualChecks(List<ManualChecks> listManualChecks) {
		this.listManualChecks = listManualChecks;
	}

	public List<Distribution> getListDistribution() {
		return listDistribution;
	}

	public void setListDistribution(List<Distribution> listDistribution) {
		this.listDistribution = listDistribution;
	}

	public List<PayrollTransactionOpenYearDetails> getListPayrollTransactionOpenYearDetails() {
		return listPayrollTransactionOpenYearDetails;
	}

	public void setListPayrollTransactionOpenYearDetails(
			List<PayrollTransactionOpenYearDetails> listPayrollTransactionOpenYearDetails) {
		this.listPayrollTransactionOpenYearDetails = listPayrollTransactionOpenYearDetails;
	}

	
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public Date getExpire() {
		return expire;
	}

	public void setExpire(Date expire) {
		this.expire = expire;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Date getExpires() {
		return expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

	public String getExpireHijri() {
		return expireHijri;
	}

	public void setExpireHijri(String expireHijri) {
		this.expireHijri = expireHijri;
	}

	public String getExpiresHijri() {
		return expiresHijri;
	}

	public void setExpiresHijri(String expiresHijri) {
		this.expiresHijri = expiresHijri;
	}

	public String getEmployeeNotes() {
		return employeeNotes;
	}

	public void setEmployeeNotes(String employeeNotes) {
		this.employeeNotes = employeeNotes;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public String getEmployeeBankAccountNumber() {
		return employeeBankAccountNumber;
	}

	public void setEmployeeBankAccountNumber(String employeeBankAccountNumber) {
		this.employeeBankAccountNumber = employeeBankAccountNumber;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public ProjectSetup getProjectSetup() {
		return projectSetup;
	}

	public void setProjectSetup(ProjectSetup projectSetup) {
		this.projectSetup = projectSetup;
	}

	public List<MiscellaneousEmployee> getMiscellaneousEmployee() {
		return miscellaneousEmployee;
	}

	public void setMiscellaneousEmployee(List<MiscellaneousEmployee> miscellaneousEmployee) {
		this.miscellaneousEmployee = miscellaneousEmployee;
	}

	
	
	
}
