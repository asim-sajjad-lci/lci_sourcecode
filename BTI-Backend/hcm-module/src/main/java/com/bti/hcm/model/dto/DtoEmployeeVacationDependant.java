package com.bti.hcm.model.dto;

import com.bti.hcm.model.EmployeeVactionTicket;

public class DtoEmployeeVacationDependant {
	
	private Integer idx;
	private Integer employeeVacationRequestId;
	private String name;
	private String relation;
	private Integer age;
	private EmployeeVactionTicket employeeVactionTicket;

	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}

	public Integer getEmployeeVacationRequestId() {
		return employeeVacationRequestId;
	}

	public void setEmployeeVacationRequestId(Integer employeeVacationRequestId) {
		this.employeeVacationRequestId = employeeVacationRequestId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public EmployeeVactionTicket getEmployeeVactionTicket() {
		return employeeVactionTicket;
	}

	public void setEmployeeVactionTicket(EmployeeVactionTicket employeeVactionTicket) {
		this.employeeVactionTicket = employeeVactionTicket;
	}

}
