package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.ShiftCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoShiftCode;
import com.bti.hcm.repository.RepositoryShiftCode;

@Service("serviceShiftCode")
public class ServiceShiftCode {

	/**
	 * @Description LOGGER use for put a logger in ShiftCode Service
	 */
	static Logger log = Logger.getLogger(ServiceShiftCode.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */
	@Autowired
	ServiceResponse response;
	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in TimeCode service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	
	/**
	 * @Description repositoryShiftCode Autowired here using annotation of spring for access of repositoryShiftCode method in ShiftCode service
	 */
	@Autowired(required=false)
	RepositoryShiftCode repositoryShiftCode;
	
	
	
	/**
	 * @param dtoShiftCode
	 * @return
	 */
	
	public DtoShiftCode saveOrUpdate(DtoShiftCode dtoShiftCode) {
		log.info("saveOrUpdate ShiftCode Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		ShiftCode shiftCode = null;
		if (dtoShiftCode.getId() != null && dtoShiftCode.getId() > 0) {
			
			shiftCode = repositoryShiftCode.findByIdAndIsDeleted(dtoShiftCode.getId(), false);
			shiftCode.setUpdatedBy(loggedInUserId);
			shiftCode.setUpdatedDate(new Date());
		} else {
			shiftCode = new ShiftCode();
			shiftCode.setCreatedDate(new Date());
			
			Integer rowId = repositoryShiftCode.getCountOfTotalShiftCode();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			shiftCode.setRowId(increment);
		}
		shiftCode.setShiftCodeId(dtoShiftCode.getShiftCodeId());
		shiftCode.setDesc(dtoShiftCode.getDesc());
		shiftCode.setArabicDesc(dtoShiftCode.getArabicDesc());
		shiftCode.setInActive(dtoShiftCode.isInActive());
		shiftCode.setShitPremium(dtoShiftCode.getShitPremium());
		shiftCode.setAmount(dtoShiftCode.getAmount());
		shiftCode.setPercent(dtoShiftCode.getPercent());
		shiftCode = repositoryShiftCode.saveAndFlush(shiftCode);
		log.debug("ShiftCode is:"+shiftCode.getId());
		return dtoShiftCode;
	
	}
	
	/**
	 * @param ids
	 * @return
	 */
	public DtoShiftCode delete(List<Integer> ids) {
		log.info("delete ShiftCode Method");
		DtoShiftCode dtoShiftCode = new DtoShiftCode();
		dtoShiftCode
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SHIFT_CODE_DELETED", false));
		dtoShiftCode.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SHIFT_CODE_DELETED", false));
		List<DtoShiftCode> deleteShiftCode = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer id : ids) {
				ShiftCode payCode = repositoryShiftCode.findOne(id);
				if(payCode!=null) {
					DtoShiftCode dtoShiftCode1 = new DtoShiftCode();
					dtoShiftCode1.setId(payCode.getId());
					repositoryShiftCode.deleteSingleShiftCode(true, loggedInUserId, id);
					deleteShiftCode.add(dtoShiftCode1);
				}else {
					inValidDelete = true;
				}
				

			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoShiftCode.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoShiftCode.setMessageType("");
				
			}
			
			dtoShiftCode.setDelete(deleteShiftCode);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete ShiftCode :"+dtoShiftCode.getId());
		return dtoShiftCode;
	}
	
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search ShiftCode Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				if(dtoSearch.getSortOn().equals("shiftCodeId") || dtoSearch.getSortOn().equals("desc") || dtoSearch.getSortOn().equals("arabicDesc") || dtoSearch.getSortOn().equals("amount")
					|| dtoSearch.getSortOn().equals("percent")	) {
					condition = dtoSearch.getSortOn();
				
			}else {
				condition = "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
			
		}
			dtoSearch.setTotalCount(this.repositoryShiftCode.predictiveShiftCodeSearchTotalCount("%"+searchWord+"%"));
			List<ShiftCode> payCodeList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					payCodeList = this.repositoryShiftCode.predictiveShiftCodeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					payCodeList = this.repositoryShiftCode.predictiveShiftCodeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					payCodeList = this.repositoryShiftCode.predictiveShiftCodeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
		
			if(payCodeList != null && !payCodeList.isEmpty()){
				List<DtoShiftCode> DtoTimeCodeList = new ArrayList<>();
				DtoShiftCode dtoTimeCode=null;
				for (ShiftCode payCode : payCodeList) {
					dtoTimeCode = new DtoShiftCode(payCode);
					dtoTimeCode.setId(payCode.getId());
					dtoTimeCode.setShiftCodeId(payCode.getShiftCodeId());
					dtoTimeCode.setDesc(payCode.getDesc());
					dtoTimeCode.setArabicDesc(payCode.getArabicDesc());
					dtoTimeCode.setInActive(payCode.isInActive());
					dtoTimeCode.setShitPremium(payCode.getShitPremium());
					dtoTimeCode.setAmount(payCode.getAmount());
					dtoTimeCode.setPercent(payCode.getPercent());
					DtoTimeCodeList.add(dtoTimeCode);
				}
				dtoSearch.setRecords(DtoTimeCodeList);
			}
		}
		log.debug("Search ShiftCode Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	public DtoShiftCode getById(int id) {
		log.info("getById Method");
		DtoShiftCode dtoTimeCode = new DtoShiftCode();
		if (id > 0) {
			ShiftCode payCode = repositoryShiftCode.findByIdAndIsDeleted(id, false);
			if (payCode != null) {
				dtoTimeCode = new DtoShiftCode(payCode);
				
				dtoTimeCode.setId(payCode.getId());
				dtoTimeCode.setShiftCodeId(payCode.getShiftCodeId());
				dtoTimeCode.setDesc(payCode.getDesc());
				dtoTimeCode.setArabicDesc(payCode.getArabicDesc());
				dtoTimeCode.setInActive(payCode.isInActive());
				dtoTimeCode.setShitPremium(payCode.getShitPremium());
				dtoTimeCode.setAmount(payCode.getAmount());
				dtoTimeCode.setPercent(payCode.getPercent());
				dtoTimeCode.setId(payCode.getId());
			} else {
				dtoTimeCode.setMessageType("SHIFT_CODE_NOT_GETTING");

			}
		} else {
			dtoTimeCode.setMessageType("SHIFT_CODE_NOT_GETTING");

		}
		log.debug("ShiftCode By Id is:"+dtoTimeCode.getId());
		return dtoTimeCode;
	}
	
	/**
	 * @param timeCodeId
	 * @return
	 */
	public DtoShiftCode repeatByShiftCodeId(String timeCodeId) {
		log.info("repeatByShiftCodeId Method");
		DtoShiftCode dtoPayCode = new DtoShiftCode();
		try {
			List<ShiftCode> shiftCode=repositoryShiftCode.findByShiftCodeIdAndIsDeleted(timeCodeId,false);
			if(shiftCode!=null && !shiftCode.isEmpty()) {
				dtoPayCode.setIsRepeat(true);
			}else {
				dtoPayCode.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPayCode;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchShiftCodeId(DtoSearch dtoSearch) {
		log.info("searchShiftCodeId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			List<ShiftCode> timeCodeIdList =null;
			
				timeCodeIdList = this.repositoryShiftCode.predictiveShiftCodeSearchWithPagination("%"+searchWord+"%");
				if(!timeCodeIdList.isEmpty()) {
					
					
					
					
					if(!timeCodeIdList.isEmpty()){
						List<DtoShiftCode> dtoShiftCode = new ArrayList<>();
						DtoShiftCode dtoTimeCode=null;
						for (ShiftCode payCode : timeCodeIdList) {
							dtoTimeCode = new DtoShiftCode(payCode);
							dtoTimeCode.setId(payCode.getId());
							
							dtoTimeCode.setDesc(payCode.getShiftCodeId() + " | " + payCode.getDesc());
							
							dtoShiftCode.add(dtoTimeCode);
						}
						dtoSearch.setRecords(dtoShiftCode);
					}

					dtoSearch.setTotalCount(timeCodeIdList.size());
				}
			
		}
		
		return dtoSearch;
	}
	
	public DtoSearch getIds(DtoSearch dtoSearch) {
		log.info("searchShiftCodeId Method");
		if(dtoSearch != null){
			
			List<ShiftCode> shiftCodeIdList =null;
			
				shiftCodeIdList = this.repositoryShiftCode.findAll();
				if(!shiftCodeIdList.isEmpty()) {
					dtoSearch.setTotalCount(shiftCodeIdList.size());
					dtoSearch.setRecords(shiftCodeIdList.size());
				}
			dtoSearch.setRecords(shiftCodeIdList);
			dtoSearch.setTotalCount(shiftCodeIdList.size());
		}
		
		return dtoSearch;
	}
	
	public List<DtoShiftCode> getAllShiftCodeInActiveList() {
		log.info("getAllShiftCodeInActiveList  Method");
		List<DtoShiftCode> dtoShiftCodeList = new ArrayList<>();
		try {
			List<ShiftCode> list = repositoryShiftCode.findByIsDeletedAndInActive(false, false);
			if (list != null && !list.isEmpty()) {
				for (ShiftCode shiftCode : list) {
					DtoShiftCode dtoShiftCode = new DtoShiftCode();
					dtoShiftCode.setId(shiftCode.getId());
					dtoShiftCode.setDesc(shiftCode.getDesc());
					dtoShiftCode.setArabicDesc(shiftCode.getArabicDesc());
					dtoShiftCode.setAmount(shiftCode.getAmount());
					dtoShiftCode.setInActive(shiftCode.isInActive());
					dtoShiftCode.setShiftCodeId(shiftCode.getShiftCodeId());
					dtoShiftCodeList.add(dtoShiftCode);
				}
			}
			log.debug("DeducaitonCode is:"+dtoShiftCodeList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoShiftCodeList;
	}
}
