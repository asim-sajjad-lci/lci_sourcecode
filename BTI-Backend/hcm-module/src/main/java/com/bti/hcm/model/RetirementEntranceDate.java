package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40351",indexes = {
        @Index(columnList = "RETRPLNINDX")
})
public class RetirementEntranceDate extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RETRPLNINDX")
	private Integer id;

	@Column(name = "RETRPLNSEQ")
	private Integer dateSequence;

	@Column(name = "RETRINDX")
	private Integer planId;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "retirementEntranceDate")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<RetirementPlanSetup> listRetirementPlanSetup;

	@Column(name = "RETRDATEN", columnDefinition = "char(31)")
	private String entranceDescription;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDateSequence() {
		return dateSequence;
	}

	public void setDateSequence(Integer dateSequence) {
		this.dateSequence = dateSequence;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getEntranceDescription() {
		return entranceDescription;
	}

	public void setEntranceDescription(String entranceDescription) {
		this.entranceDescription = entranceDescription;
	}

	public List<RetirementPlanSetup> getListRetirementPlanSetup() {
		return listRetirementPlanSetup;
	}

	public void setListRetirementPlanSetup(List<RetirementPlanSetup> listRetirementPlanSetup) {
		this.listRetirementPlanSetup = listRetirementPlanSetup;
	}

}
