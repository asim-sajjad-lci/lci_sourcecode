package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00103",indexes = {
        @Index(columnList = "EMPCINDX")
})
public class EmployeeContacts extends HcmBaseEntity implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPCINDX")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;
	
	@Column(name = "EMPDEPSEQN")
	private Integer employeeDepedentSequence;
	
	@Column(name = "EMPCSEQC")
	private Integer employeeContactSequence;
	
	@Column(name = "EMPCNMA",columnDefinition = "char(150)")
	private String employeeContactName;
	
	@Column(name = "EMPCNMAA",columnDefinition = "char(150)")
	private String employeeContactNameArabic;
	
	@Column(name = "EMPCRESHP",columnDefinition = "char(61)")
	private String employeeContactNameRelationship;
	
	@Column(name = "EMPCHOME",columnDefinition = "char(21)")
	private String employeeContactHomePhone;
	
	@Column(name = "EMPCMOBIL",columnDefinition = "char(21)")
	private String employeeContactMobilePhone;
	
	@Column(name = "EMPCWRK",columnDefinition = "char(21)")
	private String employeeContactWorkPhone;
	
	@Column(name = "EMPCADDR",columnDefinition = "char(150)")
	private String employeeContactAddress;
	
	@Column(name = "EMPCEXT",columnDefinition = "char(10)")
	private String employeeContactWorkPhoneExt;

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public Integer getEmployeeDepedentSequence() {
		return employeeDepedentSequence;
	}

	public void setEmployeeDepedentSequence(Integer employeeDepedentSequence) {
		this.employeeDepedentSequence = employeeDepedentSequence;
	}

	public Integer getEmployeeContactSequence() {
		return employeeContactSequence;
	}

	public void setEmployeeContactSequence(Integer employeeContactSequence) {
		this.employeeContactSequence = employeeContactSequence;
	}

	public String getEmployeeContactName() {
		return employeeContactName;
	}

	public void setEmployeeContactName(String employeeContactName) {
		this.employeeContactName = employeeContactName;
	}

	public String getEmployeeContactNameArabic() {
		return employeeContactNameArabic;
	}

	public void setEmployeeContactNameArabic(String employeeContactNameArabic) {
		this.employeeContactNameArabic = employeeContactNameArabic;
	}

	public String getEmployeeContactNameRelationship() {
		return employeeContactNameRelationship;
	}

	public void setEmployeeContactNameRelationship(String employeeContactNameRelationship) {
		this.employeeContactNameRelationship = employeeContactNameRelationship;
	}

	public String getEmployeeContactHomePhone() {
		return employeeContactHomePhone;
	}

	public void setEmployeeContactHomePhone(String employeeContactHomePhone) {
		this.employeeContactHomePhone = employeeContactHomePhone;
	}

	public String getEmployeeContactMobilePhone() {
		return employeeContactMobilePhone;
	}

	public void setEmployeeContactMobilePhone(String employeeContactMobilePhone) {
		this.employeeContactMobilePhone = employeeContactMobilePhone;
	}

	public String getEmployeeContactWorkPhone() {
		return employeeContactWorkPhone;
	}

	public void setEmployeeContactWorkPhone(String employeeContactWorkPhone) {
		this.employeeContactWorkPhone = employeeContactWorkPhone;
	}

	public String getEmployeeContactAddress() {
		return employeeContactAddress;
	}

	public void setEmployeeContactAddress(String employeeContactAddress) {
		this.employeeContactAddress = employeeContactAddress;
	}

	public String getEmployeeContactWorkPhoneExt() {
		return employeeContactWorkPhoneExt;
	}

	public void setEmployeeContactWorkPhoneExt(String employeeContactWorkPhoneExt) {
		this.employeeContactWorkPhoneExt = employeeContactWorkPhoneExt;
	}
	
	
	
}
