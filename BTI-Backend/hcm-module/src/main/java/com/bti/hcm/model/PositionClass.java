package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "hr40113",indexes = {
        @Index(columnList = "POTCLSINDX")
})
@NamedQuery(name = "PositionClass.findAll", query = "SELECT p FROM PositionClass p")
public class PositionClass extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POTCLSINDX")
	private int id;

	@Column(name = "POTCLSID", columnDefinition = "char(15)")
	private String positionClassId;

	@Column(name = "POTCLSDSCR", columnDefinition = "char(15)")
	private String description;

	@Column(name = "POTCLSDSCRA", columnDefinition = "char(31)")
	private String arabicDescription;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "position")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<PositionSetup> listPositionSetup;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "positionClass")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<Position> position;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPositionClassId() {
		return positionClassId;
	}

	public void setPositionClassId(String positionClassId) {
		this.positionClassId = positionClassId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<PositionSetup> getListPositionSetup() {
		return listPositionSetup;
	}

	public void setListPositionSetup(List<PositionSetup> listPositionSetup) {
		this.listPositionSetup = listPositionSetup;
	}

	public List<Position> getPosition() {
		return position;
	}

	public void setPosition(List<Position> position) {
		this.position = position;
	}

	

	

}
