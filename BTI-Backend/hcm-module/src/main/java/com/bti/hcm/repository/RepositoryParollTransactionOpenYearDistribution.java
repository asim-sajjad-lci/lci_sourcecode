package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ParollTransactionOpenYearDistribution;

@Repository("repositoryParollTransactionOpenYearDistribution")
public interface RepositoryParollTransactionOpenYearDistribution extends JpaRepository<ParollTransactionOpenYearDistribution, Integer>{

	@Query("SELECT pto FROM ParollTransactionOpenYearDistribution pto WHERE YEAR(pto.createdDate) =:year and pto.isDeleted = false")
	List<ParollTransactionOpenYearDistribution> findByYear(@Param("year") Integer year);
	
}
