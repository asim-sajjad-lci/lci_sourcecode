package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.BenefitPreferences;

@Repository("repositoryBenefitPreferences")
public interface RepositoryBenefitPreferences extends JpaRepository<BenefitPreferences, Integer>{

	BenefitPreferences findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BenefitPreferences d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteBenefitPreferences(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from BenefitPreferences d where  d.isDeleted=false")
	Integer predictiveBenefitPreferencesTotalCount();

	@Query("select d from BenefitPreferences d where  d.isDeleted=false")
	List<BenefitPreferences> predictiveBenefitPreferencesTotalCount(Pageable pageRequest);

	
	public List<BenefitPreferences> findAll();

	@Query("select count(*) from BenefitPreferences b ")
	public Integer getCountOfTotalBenefitPreferences();
}
