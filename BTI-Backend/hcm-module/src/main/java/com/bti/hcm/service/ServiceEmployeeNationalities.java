package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeNationalities;
import com.bti.hcm.model.dto.DtoEmployeeNationalities;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryEmployeeNationalities;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceEmployeeNationalities")
public class ServiceEmployeeNationalities {

	/**
	 * @Description LOGGER use for put a logger in EmployeeNationalities Service
	 */
	static Logger log = Logger.getLogger(ServiceEmployeeNationalities.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in EmployeeNationalities service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in EmployeeNationalities service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in EmployeeNationalities service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryEmployeeNationalities Autowired here using annotation of spring for access of repositoryEmployeeNationalities method in EmployeeNationalities service
	 *              In short Access EmployeeNationalities Query from Database using repositoryEmployeeNationalities.
	 */
	@Autowired
	RepositoryEmployeeNationalities repositoryEmployeeNationalities;

	/**
	 * @Description: save and update EmployeeNationalities data
	 * @param dtoEmployeeNationalities
	 * @return
	 * @throws ParseException
	 */
	public DtoEmployeeNationalities saveOrUpdateEmployeeNationalities(DtoEmployeeNationalities dtoEmployeeNationalities)
			throws ParseException {
		log.info("saveOrUpdate EmployeeNationalities Service Called");
		EmployeeNationalities employeeNationalities = null;
		String oldValue = "";
		String newValue = "";
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		try {
			if (dtoEmployeeNationalities.getEmployeeNationalityIndexId() != null && dtoEmployeeNationalities.getEmployeeNationalityIndexId() > 0) {
				employeeNationalities = repositoryEmployeeNationalities.findByEmployeeNationalityIndexIdAndIsDeleted(dtoEmployeeNationalities.getEmployeeNationalityIndexId(), false);
				employeeNationalities.setUpdatedBy(loggedInUserId);
				employeeNationalities.setUpdatedDate(new Date());
				if (!employeeNationalities.getEmployeeNationalityDescription().equals(dtoEmployeeNationalities.getEmployeeNationalityDescription())) {
					oldValue += "Description old value: " + employeeNationalities.getEmployeeNationalityDescription() + ",    ";
					newValue += "Description new value:" + dtoEmployeeNationalities.getEmployeeNationalityDescription();
				}
				if (!employeeNationalities.getEmployeeNationalityDescriptionArabic().equals(dtoEmployeeNationalities.getEmployeeNationalityDescriptionArabic())) {
					oldValue += " Arabic Description old value: " + employeeNationalities.getEmployeeNationalityDescriptionArabic() + ",    ";
					newValue += " Arabic Description new value:" + dtoEmployeeNationalities.getEmployeeNationalityDescriptionArabic();
				}
			} else {
				employeeNationalities = new EmployeeNationalities();
				employeeNationalities.setCreatedDate(new Date());
				
				Integer rowId = repositoryEmployeeNationalities.getCountOfTotalEmployeeNationalities();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				employeeNationalities.setRowId(increment);
			}
			employeeNationalities.setEmployeeNationalityDescription(dtoEmployeeNationalities.getEmployeeNationalityDescription());
			employeeNationalities.setEmployeeNationalityId(dtoEmployeeNationalities.getEmployeeNationalityId());
			employeeNationalities.setEmployeeNationalityDescriptionArabic(dtoEmployeeNationalities.getEmployeeNationalityDescriptionArabic());
			employeeNationalities.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			employeeNationalities = repositoryEmployeeNationalities.saveAndFlush(employeeNationalities);
			log.debug("EmployeeNationalities is:" + dtoEmployeeNationalities.getEmployeeNationalityIndexId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeNationalities;
	}

	/**
	 * @Description: Search EmployeeNationalities data
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchEmployeeNationalities(DtoSearch dtoSearch) {
		log.info("Search EmployeeNationalities Service Called");
		try {
			if (dtoSearch != null) {dtoSearch = getDtoSearchWithRecord(dtoSearch);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	/**
	 * @Description: delete EmployeeNationalities
	 * @param ids
	 * @return
	 */
	public DtoEmployeeNationalities deleteEmployeeNationalities(List<Integer> ids) {
		log.info("Delete EmployeeNationalities Service Called");
		DtoEmployeeNationalities dtoEmployeeNationalities = new DtoEmployeeNationalities();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			dtoEmployeeNationalities.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_NATIONALITIES_DELETED", false));
			dtoEmployeeNationalities.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_NATIONALITIES_ASSOCIATED", false));
			boolean inValidDelete = false;
			StringBuilder invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_NATIONALITIES_NOT_DELETE_ID_MESSAGE", false).getMessage());
			List<DtoEmployeeNationalities> deleteEmployeeNationalities = new ArrayList<>();
			try {
				for (Integer employeeNationalityId : ids) {
					EmployeeNationalities employeeNationalities = repositoryEmployeeNationalities.findOne(employeeNationalityId);
					if(employeeNationalities.getListEmployeeMaster().isEmpty()) {
						DtoEmployeeNationalities dtoEmployeeNationalities2 = new DtoEmployeeNationalities();
						dtoEmployeeNationalities2.setEmployeeNationalityIndexId(employeeNationalityId);
						repositoryEmployeeNationalities.deleteSingleEmployeeNationalities(true, loggedInUserId, employeeNationalityId);
						deleteEmployeeNationalities.add(dtoEmployeeNationalities2);	
					}else {
						inValidDelete = true;
					}
					
				}
				if (inValidDelete) {
					dtoEmployeeNationalities.setMessageType(invlidDeleteMessage.toString());
				}
				if (!inValidDelete) {
					dtoEmployeeNationalities.setMessageType("");

				}
				dtoEmployeeNationalities.setDeleteEmployeeNationalities(deleteEmployeeNationalities);
			} catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete EmployeeNationalities :" + dtoEmployeeNationalities.getEmployeeNationalityIndexId());
		} catch (Exception e) {
			log.error(e);

		}
		return dtoEmployeeNationalities;
	}
	
	/**
	 * @Description: get EmployeeNationalitie data by id
	 * @param employeeNationalityIndexId
	 * @return
	 */
	public DtoEmployeeNationalities getEmployeeNationalitiesById(int employeeNationalityIndexId) {
		log.info("get EmployeeNationalitie ById Service Called");
		DtoEmployeeNationalities dtoEmployeeNationalities = new DtoEmployeeNationalities();
		try {
			if (employeeNationalityIndexId > 0) {
				EmployeeNationalities employeeNationalities = repositoryEmployeeNationalities.findByEmployeeNationalityIndexIdAndIsDeleted(employeeNationalityIndexId, false);
				if (employeeNationalities != null) {
					dtoEmployeeNationalities = new DtoEmployeeNationalities(employeeNationalities);
					dtoEmployeeNationalities.setEmployeeNationalityIndexId(employeeNationalities.getEmployeeNationalityIndexId());
					dtoEmployeeNationalities.setEmployeeNationalityDescription(employeeNationalities.getEmployeeNationalityDescription());
					dtoEmployeeNationalities.setEmployeeNationalityDescriptionArabic(employeeNationalities.getEmployeeNationalityDescriptionArabic());
					dtoEmployeeNationalities.setEmployeeNationalityId(employeeNationalities.getEmployeeNationalityId());
				} else {
					dtoEmployeeNationalities.setMessageType("EMPLOYEE_NATIONALITIES_NOT_GETTING");

				}
			} else {
				dtoEmployeeNationalities.setMessageType("INVALID_EMPLOYEE_NATIONALITIES_ID");

			}
			log.debug("Employee Nationalities By Id is:" + dtoEmployeeNationalities.getEmployeeNationalityIndexId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeNationalities;
	}
	
	/**
	 * @Description: check Repeat employeeNationalityId
	 * @param employeeNationalityId
	 * @return
	 */
	public DtoEmployeeNationalities repeatByEmployeeNationalitiesId(String employeeNationalityId) {
		log.info("repeatByEmployeeNationalitiesId Service Called");
		DtoEmployeeNationalities dtoEmployeeNationalities = new DtoEmployeeNationalities();
		try {
			List<EmployeeNationalities> employeeNationalities = repositoryEmployeeNationalities.findByEmployeeNationalityId(employeeNationalityId.trim());
			if (employeeNationalities != null && !employeeNationalities.isEmpty()) {
				dtoEmployeeNationalities.setIsRepeat(true);
			} else {
				dtoEmployeeNationalities.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
			
		}
		return dtoEmployeeNationalities;
	}
	
	/**
	 * @Description: Search by employeeNationalityId
	 * @param employeeNationalityId
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchEmployeeNationalityId(DtoSearch dtoSearch) {
		log.info("Search EmployeeNationalityId Service Called");
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();
			List<String> employeeNationalityIdList = this.repositoryEmployeeNationalities.predictiveEmployeeNationalityIdSearchWithPagination("%" + searchWord + "%");
			if (!employeeNationalityIdList.isEmpty()) {
				dtoSearch.setRecords(employeeNationalityIdList.size());
			}
			dtoSearch.setIds(employeeNationalityIdList);
		}
		return dtoSearch;
	}
	public List<DtoEmployeeNationalities> getAllEmployeeNationalitiesDropDown() {
		log.info("getAll EmployeeNationalitiesDropDown  Service Called");
		List<DtoEmployeeNationalities> dtoEmployeeNationalitiesList = new ArrayList<>();
		try {
			List<EmployeeNationalities> list = repositoryEmployeeNationalities.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (EmployeeNationalities employeeNationalities : list) {
					
					DtoEmployeeNationalities dtoEmployeeNationalities = new DtoEmployeeNationalities();
					dtoEmployeeNationalities.setEmployeeNationalityIndexId(employeeNationalities.getEmployeeNationalityIndexId());
					dtoEmployeeNationalities.setEmployeeNationalityDescription(employeeNationalities.getEmployeeNationalityDescription());
					dtoEmployeeNationalities.setEmployeeNationalityDescriptionArabic(employeeNationalities.getEmployeeNationalityDescriptionArabic());
					dtoEmployeeNationalities.setEmployeeNationalityId(employeeNationalities.getEmployeeNationalityId());
					dtoEmployeeNationalitiesList.add(dtoEmployeeNationalities);
				}
			}
			log.debug("EmployeeNationalities is:" + dtoEmployeeNationalitiesList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeNationalitiesList;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllEmployeeNationalityId(DtoSearch dtoSearch) {
		log.info("GetAll EmployeeNationalityId Service called");
		try {
			if (dtoSearch != null) {
				dtoSearch = getDtoSearchWithRecord(dtoSearch);
				}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	public DtoSearch getDtoSearchWithRecord(DtoSearch dtoSearch) {

		String searchWord = dtoSearch.getSearchKeyword();
		String condition = "";

		if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
			switch (dtoSearch.getSortOn()) {
			case "employeeNationalityId":
				condition += dtoSearch.getSortOn();
				break;
			case "employeeNationalityDescription":
				condition += dtoSearch.getSortOn();
				break;
			case "employeeNationalityDescriptionArabic":
				condition += dtoSearch.getSortOn();
				break;
			case "gregorianDate":
				condition += dtoSearch.getSortOn();
				break;
			case "hijriDate":
				condition += dtoSearch.getSortOn();
				break;
			default:
				condition += "employeeNationalityIndexId";
			}
		} else {
			condition += "employeeNationalityIndexId";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
		}
		dtoSearch.setTotalCount(this.repositoryEmployeeNationalities.predictiveEmployeeNationalitiesSearchTotalCount("%" + searchWord + "%"));
		List<EmployeeNationalities> employeeNationalitiesList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

			if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
				employeeNationalitiesList = this.repositoryEmployeeNationalities.predictiveEmployeeNationalitiesSearchWithPagination(
						"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.DESC, "employeeNationalityIndexId"));
			}
			if (dtoSearch.getSortBy().equals("ASC")) {
				employeeNationalitiesList = this.repositoryEmployeeNationalities.predictiveEmployeeNationalitiesSearchWithPagination(
						"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
			} else if (dtoSearch.getSortBy().equals("DESC")) {
				employeeNationalitiesList = this.repositoryEmployeeNationalities.predictiveEmployeeNationalitiesSearchWithPagination(
						"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
			}
		}
		if (employeeNationalitiesList != null && !employeeNationalitiesList.isEmpty()) {
			List<DtoEmployeeNationalities> dtoEmployeeNationalitiesList = new ArrayList<>();
			DtoEmployeeNationalities dtoEmployeeNationalities = null;
			for (EmployeeNationalities employeeNationalities : employeeNationalitiesList) {
				dtoEmployeeNationalities = new DtoEmployeeNationalities(employeeNationalities);
				dtoEmployeeNationalities.setEmployeeNationalityIndexId(employeeNationalities.getEmployeeNationalityIndexId());
				dtoEmployeeNationalities.setEmployeeNationalityDescriptionArabic(employeeNationalities.getEmployeeNationalityDescriptionArabic());
				dtoEmployeeNationalitiesList.add(dtoEmployeeNationalities);
			}
			dtoSearch.setRecords(dtoEmployeeNationalitiesList);
		}
		return dtoSearch;
	
	}
}
