package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.MiscellaneousBenefirtEnrollment;

public class DtoMiscellaneousBenefirtEnrollment extends DtoBase {
	private Integer id;
	private Short status;
	private Integer frequency;
	private Date startDate;
	private Date endDate;
	private Boolean inActive;
	private Short method;
	private BigDecimal dudctionAmount;
	private BigDecimal dudctionPercent;
	private BigDecimal monthlyAmount;
	private BigDecimal yearlyAmount;
	private BigDecimal lifetimeAmount;
	private Boolean empluyeeerinactive;
	private Short empluyeeermethod;
	private BigDecimal benefitAmount;
	private BigDecimal benefitPercent;
	private BigDecimal employermonthlyAmount;
	private BigDecimal employeryearlyAmount;
	private BigDecimal employerlifetimeAmount;
	private DtoEmployeeMaster employeeMaster;
	private DtoMiscellaneousBenefits dtoMiscellaneousBenefits;
	private List<DtoMiscellaneousBenefirtEnrollment> delete;

	public DtoMiscellaneousBenefirtEnrollment(MiscellaneousBenefirtEnrollment miscellaneousBenefirtEnrollment) {

	}

	public DtoMiscellaneousBenefirtEnrollment() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getInActive() {
		return inActive;
	}

	public void setInActive(Boolean inActive) {
		this.inActive = inActive;
	}

	public Short getMethod() {
		return method;
	}

	public void setMethod(Short method) {
		this.method = method;
	}

	public BigDecimal getDudctionAmount() {
		return dudctionAmount;
	}

	public void setDudctionAmount(BigDecimal dudctionAmount) {
		this.dudctionAmount = dudctionAmount;
	}

	public BigDecimal getDudctionPercent() {
		return dudctionPercent;
	}

	public void setDudctionPercent(BigDecimal dudctionPercent) {
		this.dudctionPercent = dudctionPercent;
	}

	public BigDecimal getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(BigDecimal monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public BigDecimal getYearlyAmount() {
		return yearlyAmount;
	}

	public void setYearlyAmount(BigDecimal yearlyAmount) {
		this.yearlyAmount = yearlyAmount;
	}

	public BigDecimal getLifetimeAmount() {
		return lifetimeAmount;
	}

	public void setLifetimeAmount(BigDecimal lifetimeAmount) {
		this.lifetimeAmount = lifetimeAmount;
	}

	public Boolean getEmpluyeeerinactive() {
		return empluyeeerinactive;
	}

	public void setEmpluyeeerinactive(Boolean empluyeeerinactive) {
		this.empluyeeerinactive = empluyeeerinactive;
	}

	public Short getEmpluyeeermethod() {
		return empluyeeermethod;
	}

	public void setEmpluyeeermethod(Short empluyeeermethod) {
		this.empluyeeermethod = empluyeeermethod;
	}

	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public BigDecimal getBenefitPercent() {
		return benefitPercent;
	}

	public void setBenefitPercent(BigDecimal benefitPercent) {
		this.benefitPercent = benefitPercent;
	}

	public BigDecimal getEmployermonthlyAmount() {
		return employermonthlyAmount;
	}

	public void setEmployermonthlyAmount(BigDecimal employermonthlyAmount) {
		this.employermonthlyAmount = employermonthlyAmount;
	}

	public BigDecimal getEmployeryearlyAmount() {
		return employeryearlyAmount;
	}

	public void setEmployeryearlyAmount(BigDecimal employeryearlyAmount) {
		this.employeryearlyAmount = employeryearlyAmount;
	}

	public BigDecimal getEmployerlifetimeAmount() {
		return employerlifetimeAmount;
	}

	public void setEmployerlifetimeAmount(BigDecimal employerlifetimeAmount) {
		this.employerlifetimeAmount = employerlifetimeAmount;
	}

	public List<DtoMiscellaneousBenefirtEnrollment> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoMiscellaneousBenefirtEnrollment> delete) {
		this.delete = delete;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public DtoMiscellaneousBenefits getDtoMiscellaneousBenefits() {
		return dtoMiscellaneousBenefits;
	}

	public void setDtoMiscellaneousBenefits(DtoMiscellaneousBenefits dtoMiscellaneousBenefits) {
		this.dtoMiscellaneousBenefits = dtoMiscellaneousBenefits;
	}

}
