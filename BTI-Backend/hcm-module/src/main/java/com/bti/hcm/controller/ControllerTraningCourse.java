package com.bti.hcm.controller;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchByTrainingId;
import com.bti.hcm.model.dto.DtoTraningCourse;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTraningCourse;

@RestController
@RequestMapping("/traningCourse")
public class ControllerTraningCourse extends BaseController{
	private static final Logger LOGGER = Logger.getLogger(ControllerTraningCourse.class);
	
	@Autowired(required=true)
	ServiceTraningCourse serviceTraningCourse;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @param request{ 
  "traningId" : "Tr1",
"desc" : "course 1",
 "arbicDesc" : "arabic 1",
"location":"Ahmedabad",
"prerequisiteId":"pId",
  "courseInCompany":true,
  "employeeCost":10,
  "employerCost":20,
  "supplierCost":30,
  "instructorCost":40,
  "subItems" : [{
  	"classId" : "class 1",
    "className" : "class Name",
    "instructorName" : "instructor Name",
    "classLocation" : "Ahmedabad",
    "maximum" : 10,
    "enrolled" : 20,
     "startDate" : "2018-04-06",
     "endDate" : "2018-04-06",
    "startTime" : "2018-04-06",
     "endTime" : "2018-04-06"
  }]
}
	 * @param dtoTraningCourse
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoTraningCourse dtoTraningCourse) throws Exception {
		LOGGER.info("Create TraningCourse Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTraningCourse = serviceTraningCourse.saveOrUpdateTraningCourse(dtoTraningCourse);
			responseMessage=displayMessage(dtoTraningCourse, "TRAININGCOURSE_CREATED", "TRAININGCOURSE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create TraningCourse Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoTraningCourse dtoTraningCourse) throws Exception {
		LOGGER.info("Update TraningCourse Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTraningCourse = serviceTraningCourse.saveOrUpdateTraningCourse(dtoTraningCourse);
			responseMessage=displayMessage(dtoTraningCourse, "TRAININGCOURSE_UPDATED_SUCCESS", "TRAININGCOURSE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update TraningCourse Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoTraningCourse dtoTraningCourse) throws Exception {
		LOGGER.info("Delete TraningCourse Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTraningCourse.getIds() != null && !dtoTraningCourse.getIds().isEmpty()) {
				DtoTraningCourse dtoTraningCourse2 = serviceTraningCourse.deleteTraningCourse(dtoTraningCourse.getIds());
				
				if(dtoTraningCourse2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("TRAININGCOURSE_DELETED", false), dtoTraningCourse2);
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoTraningCourse2.getMessageType());
					btiMessageHcm.setMessageShort("TRAININGCOURSE_NOT_DELETE_ID_MESSAGE");
					
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							btiMessageHcm, dtoTraningCourse2);
				}
				

				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete TraningCourse Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search TraningCourse Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTraningCourse.searchTraningCourse(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.TRAININGCOURSE_GET_ALL, MessageConstant.TRAININGCOURSE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TraningCourses Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getTraningCourseById1", method = RequestMethod.POST)
	public ResponseMessage getTraningCourseById1(HttpServletRequest request, @RequestBody DtoTraningCourse dtoTraningCourse) throws Exception {
		LOGGER.info("Get TraningCourse ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTraningCourse dtoTraningCourseObj = serviceTraningCourse.getTraningCourseById1(dtoTraningCourse.getId());
			responseMessage=displayMessage(dtoTraningCourseObj, "TRAININGCOURSE_GET_ALL", "TRAININGCOURSE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get TraningCourse by Id Method:"+dtoTraningCourse.getId());
		return responseMessage;
	}

	
	@RequestMapping(value = "/getTraningCourseById", method = RequestMethod.POST)
	public ResponseMessage getTraningCourseById(HttpServletRequest request, @RequestBody DtoTraningCourse dtoTraningCourse) throws Exception {
		LOGGER.info("Get TraningCourse ById Method");
		ResponseMessage responseMessage = null;
		DtoSearch dtoSearch = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = serviceTraningCourse.getTraningCourseById(dtoTraningCourse.getId());
			responseMessage=displayMessage(dtoSearch, "TRAININGCOURSE_GET_ALL", "TRAININGCOURSE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get TraningCourse by Id Method:"+dtoTraningCourse.getId());
		return responseMessage;
	}

	
	
	
	
	@RequestMapping(value = "/traningIdcheck", method = RequestMethod.POST)
	public ResponseMessage traningIdcheck(HttpServletRequest request, @RequestBody DtoTraningCourse dtoTraningCourse) throws Exception {
		LOGGER.info("traningIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTraningCourse dtoTraningCourseObj = serviceTraningCourse.repeatByTraningId(dtoTraningCourse.getTraningId());
			if (dtoTraningCourseObj != null) {
				if (dtoTraningCourseObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("TRANING_COURSE_RESULT", false), dtoTraningCourseObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoTraningCourseObj.getMessageType(), false),
							dtoTraningCourseObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("TRANING_COURSE_REPEAT_TRANINGID_NOT_FOUND", false), dtoTraningCourseObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getAllTraningCourseId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllDivisionId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search getAllTraningCourseId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTraningCourse.getAllTraningCourseId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "TRANING_COURSE_GET_ALL", "TRANING_COURSE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TraningCourse Methods:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchTraningId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchOrientationId(@RequestBody DtoSearchByTrainingId dtoSearchByTrainingId, HttpServletRequest request) throws Exception {
		LOGGER.info("searchTraningId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchByTrainingId = this.serviceTraningCourse.searchTraningId(dtoSearchByTrainingId);
			responseMessage=displayMessage(dtoSearchByTrainingId, "TRAINING_COURSE_GET_ALL", "TRAINING_COURSE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearchByTrainingId!=null) {
			LOGGER.debug("Search TraningCourse Method:"+dtoSearchByTrainingId.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/searchAllTraningId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAllTraningId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("searchAllTraningId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTraningCourse.searchAllTraningId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "TRAINING_COURSE_GET_ALL", "TRAINING_COURSE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TraningCourse Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
}
