package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoAtteandaceOption;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceAtteandaceOption;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/atteandaceOption")
public class ControllerAtteandaceOption extends BaseController{
	/**
	 * @Description LOGGER use for put a logger in AtteandaceOption  Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerAtteandaceOption.class);
	
	/**
	 * @Description serviceAtteandace Autowired here using annotation of spring for use of serviceAtteandace method in this controller
	 */
	@Autowired(required=true)
	ServiceAtteandaceOption serviceAtteandace;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @param request{
	  		"seqn": 1,
	  "reasonIndx": 1,
	  "atteandanceTypeSeqn": 1,
	  "atteandanceTypeIndx": 1,
	  "reasonDesc": "test",
	  "atteandanceTypeDesc": "test"
	}
	 * @param dtoAtteandace
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoAtteandaceOption dtoAtteandace) throws Exception {
		LOGGER.info("Create Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAtteandace = serviceAtteandace.saveOrUpdate(dtoAtteandace);
			responseMessage=displayMessage(dtoAtteandace, "ATTEANDACE_OPTION_CREATED", "ATTEANDACE_OPTION_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoAtteandaceOption dtoAtteandace) throws Exception {
		LOGGER.info("Update Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAtteandace = serviceAtteandace.saveOrUpdate(dtoAtteandace);
			responseMessage=displayMessage(dtoAtteandace, "ATTEANDACE_OPTION_UPDATED_SUCCESS", "ATTEANDACE_OPTION__NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteAttedanceOption(HttpServletRequest request, @RequestBody DtoAtteandaceOption dtoAtteandace) throws Exception {
		LOGGER.info("Delete AttedanceOption Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoAtteandace.getIds() != null && !dtoAtteandace.getIds().isEmpty()) {
				DtoAtteandaceOption dtoAtteandaceOption2 = serviceAtteandace.delete(dtoAtteandace.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ATTEANDACE_OPTION_DELETED", false), dtoAtteandaceOption2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete AttedanceOption Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAttedanceOption(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAtteandace.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.ATTEANDACE_OPTION_GET_ALL, MessageConstant.ATTEANDACE_OPTION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody  DtoAtteandaceOption dtoAtteandace) throws Exception {
		LOGGER.info("Get All PostionBudget Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceAtteandace.getAll(dtoAtteandace);
			responseMessage=displayMessage(dtoSearch, MessageConstant.ATTEANDACE_OPTION_GET_ALL, MessageConstant.ATTEANDACE_OPTION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All PostionBudget Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAtteandaceId", method = RequestMethod.POST)
	public ResponseMessage getPositionBudgetId(HttpServletRequest request, @RequestBody DtoAtteandaceOption dtoAtteandace) throws Exception {
		LOGGER.info("Get Position ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAtteandaceOption dtoPositionObj = serviceAtteandace.getByAtteandaceId(dtoAtteandace.getId());
			responseMessage=displayMessage(dtoPositionObj, "ATTEANDACE_OPTION_GET_DETAIL", "ATTEANDACE_OPTION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Position by Id Method:"+dtoAtteandace.getId());
		return responseMessage;
	}
	
	
	

	@RequestMapping(value = "/getAllAccrualTypeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllAccrualTypeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("getAllAccrualTypeId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAtteandace.getAllAccrualTypeId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ATTEANDACE_OPTION_GET_ALL", "ATTEANDACE_OPTION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
