package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Batches;

@Repository("repositoryBatches")
public interface RepositoryBatches extends JpaRepository<Batches, Integer>{

	public Batches findByIdAndIsDeleted(int id, boolean deleted);
	
	public List<Batches> findByIsDeleted(Boolean deleted);
	
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Batches d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleBatches(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,@Param("id") Integer id);
	
	@Query("select count(*) from Batches d where (d.batchId LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord or d.description LIKE :searchKeyWord) and d.listTransactionEntry IS NOT EMPTY and d.isDeleted=false and d.fromtranscation=true")
	public Integer predictiveBatchesSearchWithPaginationCount(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select count(*) from Batches d where (d.batchId LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord or d.description LIKE :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveBatchesSearchWithPaginationCountes(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select d from Batches d where (d.batchId LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord or d.description LIKE :searchKeyWord) and d.listTransactionEntry IS NOT EMPTY and d.isDeleted=false")
	public List<Batches> predictiveBatchesSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	@Query("select d from Batches d where (d.batchId LIKE :searchKeyWord  or d.description LIKE :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<Batches> predictiveBatchesSearch(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select d from Batches d where (d.batchId LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord or d.status LIKE :searchKeyWord  or d.description LIKE :searchKeyWord  or d.userID LIKE :searchKeyWord or d.postingDate LIKE :searchKeyWord) and d.isDeleted=false")
	public List<Batches> predictiveBatchesSearchWithPaginationGetAll(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/*@Query("select count(*) from Batches d where (d.batchId LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord or d.status LIKE :searchKeyWord or d.description LIKE :searchKeyWord  or d.userID LIKE :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveBatchesSearchWithPaginationCount(@Param("searchKeyWord") String searchKeyWord);
	*/
	//public List<Batches> findByBatchId(String batchId);
	
	@Query("select b from Batches b where (b.batchId =:batchId) and b.isDeleted=false")
	public List<Batches> findByBatchId(@Param("batchId")String batchId);
	
	@Query("select d from Batches d where (d.batchId LIKE :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<Batches> searchByBatchId(@Param("searchKeyWord") String searchKeyWord);

	@Query("select count(*) from Batches b ")
	public Integer getCountOfTotalAtteandaces();
	
	
	
	//@Query("select b From Batches b INNER JOIN TransactionEntry t ON b.id=t.batches.id")
	@Query("select b From Batches b where b.id in ( select t.batches.id from TransactionEntry t where (t.fromDate >=:fromDate  AND t.toDate <=:toDate) and t.isDeleted=false)")
	public List<Batches> getByFromAndToDate(@Param("fromDate")Date fromDate, @Param("toDate")Date toDate);

	@Query("select d from Batches d where d.id not in(:list) and d.isDeleted=false")	
	public List<Batches> getExculdeBatches(@Param("list") List<Integer> ids);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Batches b set b.status=3  where b.id in(:list) and b.isDeleted=false")	
	public void updateExculdeBatches(@Param("list") List<Integer> ids);
	
	
	@Query("select d from Batches d where d.id in(:list) and d.isDeleted=false")	
	public List<Batches> getIncludeBatches(@Param("list") List<Integer> ids);
	
	
	@Query("select d from Batches d where d.id =:id and d.isDeleted=false")	
	public List<Batches> getIncludeBatches1(@Param("id") Integer id);
	
	@Query("select d from Batches d where d.id =:id and d.isDeleted=false and d.fromtranscation=true")	
	public List<Batches> getIncludeBatches12(@Param("id") Integer id);

}
