/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.model.Location;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Location class having getter and setter for fields (POJO)
 * Name Name of Project: Hcm Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoLocation extends DtoBase {
	private Integer id;
	private String locationId;
	private String description;
	private String arabicDescription;
	private String contactName;
	private String locationAddress;
	private HrCity city;
	private HrState state;
	private HrCountry country;

	private Integer cityId;
	private Integer stateId;
	private Integer countryId;

	private String cityName;
	private String stateName;
	private String countryName;
	private String phone;
	private String fax;
	private List<DtoLocation> deleteLocation;
	private int valueId;
	private String valueDescription;

	private int valId;

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getLocationAddress() {
		return locationAddress;
	}

	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}

	public HrCity getCity() {
		return city;
	}

	public void setCity(HrCity city) {
		this.city = city;
	}

	public HrState getState() {
		return state;
	}

	public void setState(HrState state) {
		this.state = state;
	}

	public HrCountry getCountry() {
		return country;
	}

	public void setCountry(HrCountry country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public List<DtoLocation> getDeleteLocation() {
		return deleteLocation;
	}

	public void setDeleteLocation(List<DtoLocation> deleteLocation) {
		this.deleteLocation = deleteLocation;
	}

	public DtoLocation(Location location) {
		this.locationId = location.getLocationId();
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	
	
	public int getValueId() {
		return valueId;
	}

	public void setValueId(int valueId) {
		this.valueId = valueId;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	public DtoLocation() {

	}

	public int getValId() {
		return valId;
	}

	public void setValId(int valId) {
		this.valId = valId;
	}
	
	
}
