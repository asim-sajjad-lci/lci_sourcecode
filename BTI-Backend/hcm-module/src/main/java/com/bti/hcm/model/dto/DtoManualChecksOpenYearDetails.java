package com.bti.hcm.model.dto;

import java.util.Date;

import com.bti.hcm.model.ManualChecksOpenYearHeader;
import com.ibm.icu.math.BigDecimal;

public class DtoManualChecksOpenYearDetails extends DtoBase {

	private Integer id;
	private Integer paymentSequence;
	private Short paymentCodeType;
	private Integer codeId;
	private Date fromPeriodDate;
	private Date toPeriodDate;
	private ManualChecksOpenYearHeader manualChecksOpenYearHeader;
	private BigDecimal totalAmount;
	private Integer totalHours;
	private Integer weaksWorked;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPaymentSequence() {
		return paymentSequence;
	}

	public void setPaymentSequence(Integer paymentSequence) {
		this.paymentSequence = paymentSequence;
	}

	public Short getPaymentCodeType() {
		return paymentCodeType;
	}

	public void setPaymentCodeType(Short paymentCodeType) {
		this.paymentCodeType = paymentCodeType;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public Date getFromPeriodDate() {
		return fromPeriodDate;
	}

	public void setFromPeriodDate(Date fromPeriodDate) {
		this.fromPeriodDate = fromPeriodDate;
	}

	public Date getToPeriodDate() {
		return toPeriodDate;
	}

	public void setToPeriodDate(Date toPeriodDate) {
		this.toPeriodDate = toPeriodDate;
	}

	public ManualChecksOpenYearHeader getManualChecksOpenYearHeader() {
		return manualChecksOpenYearHeader;
	}

	public void setManualChecksOpenYearHeader(ManualChecksOpenYearHeader manualChecksOpenYearHeader) {
		this.manualChecksOpenYearHeader = manualChecksOpenYearHeader;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public Integer getWeaksWorked() {
		return weaksWorked;
	}

	public void setWeaksWorked(Integer weaksWorked) {
		this.weaksWorked = weaksWorked;
	}

}
