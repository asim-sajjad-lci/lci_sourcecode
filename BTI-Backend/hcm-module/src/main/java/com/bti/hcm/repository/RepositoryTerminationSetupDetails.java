package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TerminationSetupDetails;

@Repository("repositoryTerminationSetupDetails")
public interface RepositoryTerminationSetupDetails extends JpaRepository<TerminationSetupDetails, Integer>{

	TerminationSetupDetails findByIdAndIsDeleted(Integer id, boolean b);

	TerminationSetupDetails saveAndFlush(TerminationSetupDetails terminationSetupDetails);

	TerminationSetupDetails findOne(Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TerminationSetupDetails t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	public void deleteSingleTeminationSetupDetails(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from TerminationSetupDetails t where (t.terminationDesc LIKE :searchKeyWord or t.terminationArbicDesc LIKE :searchKeyWord or t.terminationPersonName LIKE :searchKeyWord) and t.isDeleted=false")
	Integer predictiveTerminationSetupDetailsSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);

	
	@Query("select d from TerminationSetupDetails d where ( d.terminationDesc like :searchKeyWord or d.terminationArbicDesc like :searchKeyWord or d.terminationPersonName like :searchKeyWord) and d.isDeleted=false")
	List<TerminationSetupDetails> predictiveTerminationSetupDetailsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select count(*) from TerminationSetupDetails t ")
	public Integer getCountOfTotalTerminationSetupDetails();



}
