/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;
import java.io.Serializable;

import javax.persistence.Column;
/**
 * Description: The persistent class for the SalaryMatrixColSetup database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40934",indexes = {
        @Index(columnList = "SMTXCINDX")
})
@NamedQuery(name = "SalaryMatrixColSetup.findAll", query = "SELECT s FROM SalaryMatrixColSetup s")
public class SalaryMatrixColSetup  extends HcmBaseEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SMTXCINDX")
	private int colSalaryMatrixIndexId;
	
	@ManyToOne
	@JoinColumn(name="SMTXINDX")
	private SalaryMatrixSetup salaryMatrixSetup;
	
	
	@Column(name = "SMTXCDSCR",columnDefinition="char(15)")
	private String colSalaryMatrixDescription;

	@Column(name = "SMTXCDSCRA",columnDefinition="char(61)")
	private String arabicColSalaryMatrixDescription;

	
	

	public int getColSalaryMatrixIndexId() {
		return colSalaryMatrixIndexId;
	}

	public void setColSalaryMatrixIndexId(int colSalaryMatrixIndexId) {
		this.colSalaryMatrixIndexId = colSalaryMatrixIndexId;
	}

	public SalaryMatrixSetup getSalaryMatrixSetup() {
		return salaryMatrixSetup;
	}

	public void setSalaryMatrixSetup(SalaryMatrixSetup salaryMatrixSetup) {
		this.salaryMatrixSetup = salaryMatrixSetup;
	}

	public String getColSalaryMatrixDescription() {
		return colSalaryMatrixDescription;
	}

	public void setColSalaryMatrixDescription(String colSalaryMatrixDescription) {
		this.colSalaryMatrixDescription = colSalaryMatrixDescription;
	}

	public String getArabicColSalaryMatrixDescription() {
		return arabicColSalaryMatrixDescription;
	}

	public void setArabicColSalaryMatrixDescription(String arabicColSalaryMatrixDescription) {
		this.arabicColSalaryMatrixDescription = arabicColSalaryMatrixDescription;
	}
	
	


	
}
