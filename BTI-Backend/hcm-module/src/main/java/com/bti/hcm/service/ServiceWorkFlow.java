/*package com.bti.hcm.service;

import org.springframework.stereotype.Service;


@Service("serviceWorkFlow")
public class ServiceWorkFlow {
	/*
	static Logger log = Logger.getLogger(ServiceDefault.class.getName());

	
	RepositoryWorkFlow repositoryWorkFlow;
	
	
	HttpServletRequest httpServletRequest;
	

    @Autowired
    private RuntimeService runtimeService;

	
	public DtoWorkFlow saveOrUpdate(DtoWorkFlow dtoWorkFlow,MultipartFile file) throws IOException {
		
		WorkFlowUploadFile workFlowUploadFile =null;
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if(dtoWorkFlow.getWorFlowId()!=null && dtoWorkFlow.getWorFlowId()>0) {
				workFlowUploadFile = repositoryWorkFlow.findByworkFlowIdAndIsDeleted(dtoWorkFlow.getWorFlowId(),false);
				workFlowUploadFile.setUpdatedBy(loggedInUserId);
				workFlowUploadFile.setUpdatedDate(new Date());
			}else {
				workFlowUploadFile = new WorkFlowUploadFile();
				workFlowUploadFile.setCreatedDate(new Date());
			}
			if(file!=null) {
				workFlowUploadFile.setWorkFlowFile(file.getBytes());
			}else
			{
				workFlowUploadFile.setWorkFlowFile(null);
				
			}
			workFlowUploadFile.setFileDesc(dtoWorkFlow.getFileDesc());
			Map<String, Object> vars = Collections.<String, Object>singletonMap("workFlowUploadFile", workFlowUploadFile);
	        runtimeService.startProcessInstanceByKey("myProcess", vars).getActivityId();
	        System.out.println(""+Var.class.getName());
	        File files = 
				      new File("/home/manoj/Downloads/diagrams.bpmn"); 
				    Scanner sc = new Scanner(files); 
				  
				    while (sc.hasNextLine()) {
				      System.out.println(sc.nextLine()); 
				  } 
			repositoryWorkFlow.saveAndFlush(workFlowUploadFile);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dtoWorkFlow;
	}

	
	@Transactional
	public DtoSearch getAll(DtoSearch dtoSearch) {
		try {
			if(dtoSearch!=null) {
				log.info("Default Method");
				String searchKeyword=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						if(dtoSearch.getSortOn().equals("defaultID")|| dtoSearch.getSortOn().equals("desc")|| dtoSearch.getSortOn().equals("arabicDesc")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
				 	dtoSearch.setTotalCount(this.repositoryWorkFlow.predictiveSearchTotalCount("%"+searchKeyword+"%"));
					List<WorkFlowUploadFile> workFlowUploadFileList =null;
					if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
						if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
							workFlowUploadFileList = this.repositoryWorkFlow.predictiveSearchWithPaginations("%"+searchKeyword+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
						}
						if(dtoSearch.getSortBy().equals("ASC")){
							workFlowUploadFileList = this.repositoryWorkFlow.predictiveSearchWithPaginations("%"+searchKeyword+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
						}else if(dtoSearch.getSortBy().equals("DESC")){
							workFlowUploadFileList = this.repositoryWorkFlow.predictiveSearchWithPaginations("%"+searchKeyword+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
						}
					}
					if(workFlowUploadFileList != null && !workFlowUploadFileList.isEmpty()){
						List<DtoWorkFlow> dtoWorkFlowList = new ArrayList<>();
						DtoWorkFlow dtoWorkFlow=null;
						for (WorkFlowUploadFile workFlowUploadFile : workFlowUploadFileList) {
							dtoWorkFlow = new DtoWorkFlow();
							dtoWorkFlow.setWorFlowId(workFlowUploadFile.getWorkFlowId());
							dtoWorkFlow.setWorkFlowFile(workFlowUploadFile.getWorkFlowFile());
							dtoWorkFlowList.add(dtoWorkFlow);
							}
						dtoSearch.setRecords(dtoWorkFlowList);
							}
				log.debug("Search default Size is:"+dtoSearch.getTotalCount());
				}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}


	public List<DtoWorkFlow> getAllDropDown() {
		List<DtoWorkFlow>dtoWorkFlowsList=new ArrayList<>();
		try {
			List<WorkFlowUploadFile>workFlowUploadFiles=repositoryWorkFlow.findByIsDeleted(false);
			if(!workFlowUploadFiles.isEmpty()) {
				for (WorkFlowUploadFile workFlowUploadFile : workFlowUploadFiles) {
					DtoWorkFlow dtoWorkFlow=new DtoWorkFlow();
					dtoWorkFlow.setWorFlowId(workFlowUploadFile.getWorkFlowId());
					dtoWorkFlow.setWorkFlowFile(workFlowUploadFile.getWorkFlowFile());
					
					 File file = 
						      new File("/home/manoj/Downloads/diagrams.bpmn"); 
						    Scanner sc = new Scanner(file); 
						  
						    while (sc.hasNextLine()) {
						      System.out.println(sc.nextLine()); 
						  } 
					
					dtoWorkFlowsList.add(dtoWorkFlow);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoWorkFlowsList;
	}
}
*/