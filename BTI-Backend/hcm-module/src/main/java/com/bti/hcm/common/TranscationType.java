package com.bti.hcm.common;

public interface TranscationType {
	
	public static final String SETUP="SETUP";
	public static final String MASTER="MASTER";
	public static final String TRANSCATION="TRANSCATION";

}
