package com.bti.hcm.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceJasper;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/controllerJasper")
public class ControllerJasper extends BaseController{

	private Logger log = Logger.getLogger(ControllerAccrual.class);
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	ServiceJasper serviceJasper;
	
	@RequestMapping(value = "/pdfFromXmlFile", method = RequestMethod.POST)
	public ResponseMessage createAccrual(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) throws Exception{
		log.info("Create Accrual Method");
		ResponseMessage responseMessage = null;
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtoSearch  = serviceJasper.pdfFromXmlFile(dtoSearch,request);
				responseMessage = displayMessage(dtoSearch,"ACCRUAL_CREATED","ACCRUALSETUP_NOT_CREATED",serviceResponse);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			log.debug("Create Accrual Method:"+responseMessage.getMessage());
		    return responseMessage;
	}
	
	
	
	@RequestMapping(value =  {"/attachment" }, method = RequestMethod.POST)
	public HttpEntity<byte[]> profileId(@RequestBody DtoSearch dtoSearch,HttpServletResponse response)

			throws  IOException {
		
		String location = System.getProperty("user.dir")+File.separator+"data-source"+File.separator+"jasper"+File.separator+"reports"+File.separator+dtoSearch.getFileLocation()+".pdf";
		 byte[] bFile = readBytesFromFile(location);

		ByteArrayOutputStream bao = null;
		HttpHeaders headers = new HttpHeaders();
		 headers.setContentDispositionFormData("attachment; filename=", "");
		 headers.setContentType(MediaType.APPLICATION_PDF);	 	    
		if(bFile!=null){
			byte[] bytes = bFile;

			bao = new ByteArrayOutputStream();
			bao.write(bytes);
			
		    return new HttpEntity<byte[]>(bao.toByteArray(), headers);
		}	
		

		 return new HttpEntity<byte[]>(null, headers);
	}
	
	
	private static byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                }
            }

        }

        return bytesArray;

    }
	
}
