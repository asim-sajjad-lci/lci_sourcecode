package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.OrientationCheckListSetup;
import com.bti.hcm.model.OrientationPredefinedCheckListItem;
import com.bti.hcm.model.OrientationSetup;
import com.bti.hcm.model.PredefinecheckList;
import com.bti.hcm.util.UtilRandomKey;

public class DtoOrientationSetup extends DtoBase{

	private Integer id;
	private String orientationId;
	private String orientationDesc;
	private String orientationArbicDesc;

	private List<DtoOrientationSetupDetail> subItems;
	
	
	private Integer predefinecheckListId;
	private PredefinecheckList predefinecheckList;

	private Integer orientationCheckListSetupId;
	private OrientationCheckListSetup orientationCheckListSetup;

	private Integer orientationPredefinedCheckListItemId;
	private OrientationPredefinedCheckListItem orientationPredefinedCheckListItem;
	private List<DtoOrientationSetup> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrientationId() {
		return orientationId;
	}

	public void setOrientationId(String orientationId) {
		this.orientationId = orientationId;
	}

	public String getOrientationDesc() {
		return orientationDesc;
	}

	public void setOrientationDesc(String orientationDesc) {
		this.orientationDesc = orientationDesc;
	}

	public String getOrientationArbicDesc() {
		return orientationArbicDesc;
	}

	public void setOrientationArbicDesc(String orientationArbicDesc) {
		this.orientationArbicDesc = orientationArbicDesc;
	}
	public List<DtoOrientationSetup> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoOrientationSetup> delete) {
		this.delete = delete;
	}
	public Integer getPredefinecheckListId() {
		return predefinecheckListId;
	}

	public void setPredefinecheckListId(Integer predefinecheckListId) {
		this.predefinecheckListId = predefinecheckListId;
	}

	public PredefinecheckList getPredefinecheckList() {
		return predefinecheckList;
	}

	public void setPredefinecheckList(PredefinecheckList predefinecheckList) {
		this.predefinecheckList = predefinecheckList;
	}

	public Integer getOrientationCheckListSetupId() {
		return orientationCheckListSetupId;
	}

	public void setOrientationCheckListSetupId(Integer orientationCheckListSetupId) {
		this.orientationCheckListSetupId = orientationCheckListSetupId;
	}

	public OrientationCheckListSetup getOrientationCheckListSetup() {
		return orientationCheckListSetup;
	}

	public void setOrientationCheckListSetup(OrientationCheckListSetup orientationCheckListSetup) {
		this.orientationCheckListSetup = orientationCheckListSetup;
	}

	public Integer getOrientationPredefinedCheckListItemId() {
		return orientationPredefinedCheckListItemId;
	}

	public void setOrientationPredefinedCheckListItemId(Integer orientationPredefinedCheckListItemId) {
		this.orientationPredefinedCheckListItemId = orientationPredefinedCheckListItemId;
	}

	public OrientationPredefinedCheckListItem getOrientationPredefinedCheckListItem() {
		return orientationPredefinedCheckListItem;
	}

	public void setOrientationPredefinedCheckListItem(
			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem) {
		this.orientationPredefinedCheckListItem = orientationPredefinedCheckListItem;
	}


	

	public List<DtoOrientationSetupDetail> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<DtoOrientationSetupDetail> subItems) {
		this.subItems = subItems;
	}

	public DtoOrientationSetup(OrientationSetup orientationSetup) {
		this.id = orientationSetup.getId();

		if (UtilRandomKey.isNotBlank(orientationSetup.getOrientationId())) {
			this.orientationId = orientationSetup.getOrientationId();
		} else {
			this.orientationId = "";
		}
		if (UtilRandomKey.isNotBlank(orientationSetup.getOrientationDesc())) {
			this.orientationDesc = orientationSetup.getOrientationDesc();
		} else {
			this.orientationDesc = "";
		}

		if (UtilRandomKey.isNotBlank(orientationSetup.getOrientationArbicDesc())) {
			this.orientationArbicDesc = orientationSetup.getOrientationArbicDesc();
		} else {
			this.orientationArbicDesc = "";
		}

	}

	public DtoOrientationSetup() {

	}

}
