package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40361",indexes = {
        @Index(columnList = "RETRFNDINDX")
})
public class RetirementFundSetup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RETRFNDINDX")
	private Integer id;

	@Column(name = "RETRINDX")
	private Integer planId;

	@Column(name = "RETRFNDID", columnDefinition = "char(15)")
	private String fundId;

	@Column(name = "RETRSEQNC")
	private Integer fundSequence;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "retirementFundSetup")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<RetirementPlanSetup> listRetirementPlanSetup;

	@Column(name = "RETRFNDDSCR", columnDefinition = "char(31)")
	private String planDescription;

	@Column(name = "RETRFNDACT")
	private BigInteger fundActive;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getFundId() {
		return fundId;
	}

	public void setFundId(String fundId) {
		this.fundId = fundId;
	}

	public Integer getFundSequence() {
		return fundSequence;
	}

	public void setFundSequence(Integer fundSequence) {
		this.fundSequence = fundSequence;
	}

	public String getPlanDescription() {
		return planDescription;
	}

	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}

	public BigInteger getFundActive() {
		return fundActive;
	}

	public void setFundActive(BigInteger fundActive) {
		this.fundActive = fundActive;
	}

	public List<RetirementPlanSetup> getListRetirementPlanSetup() {
		return listRetirementPlanSetup;
	}

	public void setListRetirementPlanSetup(List<RetirementPlanSetup> listRetirementPlanSetup) {
		this.listRetirementPlanSetup = listRetirementPlanSetup;
	}

}
