package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.SettingFamilyLeave;

@Repository("repositorySettingFamilyLeave")
public interface RepositorySettingFamilyLeave extends JpaRepository<SettingFamilyLeave, Integer>{
	
	SettingFamilyLeave findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update SettingFamilyLeave d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleSettingFamilyLeave(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from SettingFamilyLeave d where  d.isDeleted=false")
	Integer predictiveSettingFamilyLeaveSearchTotalCount();

	@Query("select d from SettingFamilyLeave d where  d.isDeleted=false")
	List<SettingFamilyLeave> predictiveSettingFamilyLeaveSearchWithPagination(Pageable pageRequest);
	
	@Query("select d from SettingFamilyLeave d where  d.isDeleted=false")
	Integer getAll();

	@Query("select count(*) from SettingFamilyLeave s ")
	public Integer getCountOfTotaSettingFamilyLeave();
}
