package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author HAMID
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10601")
public class LoanDetails extends HcmBaseEntity2 implements Serializable {

	private static final long serialVersionUID = 1L;
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idx")
	private int id;

	@Column(name = "mnthyr")
	private Date monthYear;

	@Column(name = "amt")
	private Float amt;	//deductionAmountPerThisMonth

	@Column(name = "pstpn")
	private Boolean isPostpone;

	@Column(name = "ispaid")
	private Boolean isPaid;

	// FK_hr10601 FK ( reqidx ) REFERENCES hr10600 (Loan) ( idx )
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "reqidx")
	private Loan loan; // private Integer reqIdx;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(Date monthYear) {
		this.monthYear = monthYear;
	}

	public Float getAmt() {
		return amt;
	}

	public void setAmt(Float amt) {
		this.amt = amt;
	}

	public Boolean getIsPostpone() {
		return isPostpone;
	}

	public void setIsPostpone(Boolean isPostpone) {
		this.isPostpone = isPostpone;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	/*
	// CONSTRAINT FK_hr10601_hr10200 FOREIGN KEY ( hcmtrxinxd ) REFERENCES hr10200 (
	// hcmtrxinxd )
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "hcmtrxinxd")
	private Object abc;// Tobe changed to
*/



}
