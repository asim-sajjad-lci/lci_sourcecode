/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PositionClass;

/**
 * Description: Interface for Position 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryPosition")
public interface RepositoryPositionClass extends JpaRepository<PositionClass, Integer> {
	
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public PositionClass findByIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PositionClass> findByIsDeleted(Boolean deleted);
	
	/**
	 * @return
	 */
	@Query("select count(*) from PositionClass p where p.isDeleted=false")
	public Integer getCountOfTotalPostionClass();
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<PositionClass> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PositionClass p set p.isDeleted =:deleted, p.updatedBy=:updateById where p.id IN (:idList)")
	public void deletePositionClass(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PositionClass p set p.isDeleted =:deleted ,p.updatedBy =:updateById where p.id =:id ")
	public void deleteSinglePositionClasss(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public PositionClass findTop1ByOrderByPositionClassIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select p from PositionClass p where (p.positionClassId like :searchKeyWord or p.description like :searchKeyWord or p.arabicDescription like :searchKeyWord) and p.isDeleted=false")
	public List<PositionClass> predictivePositionClassSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PositionClass> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PositionClass p where (p.positionClassId like :searchKeyWord or p.description like :searchKeyWord  or p.arabicDescription like :searchKeyWord) and p.isDeleted=false")
	public Integer predictivePositionClassSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select p from PositionClass p where (p.positionClassId like :searchKeyWord or p.description like  :searchKeyWord  or p.arabicDescription like :searchKeyWord) and p.isDeleted=false")
	public List<PositionClass> predictivePositionClassSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select p from PositionClass p where (p.positionClassId =:positionClassId) and p.isDeleted=false")
	public List<PositionClass> findByPositionClassId(@Param("positionClassId")String positionClassId);

	@Query("select d.positionClassId from PositionClass d where (d.positionClassId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<String> predictivePositionClassIdSearchWithPagination(@Param("searchKeyWord") String string);

	@Query("select d from PositionClass d where d.positionClassId like :searchKeyWord and d.isDeleted=false order by d.id desc")
	public List<PositionClass> predictivesearchAllPositionClassIdsWithPagination(@Param("searchKeyWord")String searchKeyWord);

	@Query("select count(*) from PositionClass p ")
	public Integer getCountOfTotaPositionClass();
}

