package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class DtoBuildPayrollCheckByBenefits extends DtoBase{

	private Integer id;
	private DtoBuildChecks buildChecks;
	private List<DtoBenefitCode> benefitCode;
	private DtoBenefitCode benefitCodess;
	private String benefitId;
	 private String desc;
	 private BigDecimal amount;

	private List<DtoBuildPayrollCheckByBenefits> listBuildChecks;

	private List<DtoBuildPayrollCheckByBenefits> delete;
	
	private Date startDate;
	private Date endDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DtoBuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(DtoBuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}

	public List<DtoBenefitCode> getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(List<DtoBenefitCode> benefitCode) {
		this.benefitCode = benefitCode;
	}


	public List<DtoBuildPayrollCheckByBenefits> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoBuildPayrollCheckByBenefits> delete) {
		this.delete = delete;
	}


	public List<DtoBuildPayrollCheckByBenefits> getListBuildChecks() {
		return listBuildChecks;
	}

	public void setListBuildChecks(List<DtoBuildPayrollCheckByBenefits> listBuildChecks) {
		this.listBuildChecks = listBuildChecks;
	}

	public String getBenefitId() {
		return benefitId;
	}

	public void setBenefitId(String benefitId) {
		this.benefitId = benefitId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public DtoBenefitCode getBenefitCodess() {
		return benefitCodess;
	}

	public void setBenefitCodess(DtoBenefitCode benefitCodess) {
		this.benefitCodess = benefitCodess;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	

}
