package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollTransactionHistoryYearDetail;

@Repository("repositoryPayrollTransactionHistoryYearDetail")
public interface RepositoryPayrollTransactionHistoryYearDetail extends JpaRepository<PayrollTransactionHistoryYearDetail, Integer>{

}
