/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoRequisitions;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceRequisitions;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerRequisitions
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/requisition")
public class ControllerRequisitions extends BaseController{
	
	
	private static final Logger LOGGER = Logger.getLogger(ControllerRequisitions.class);
	
	@Autowired
	ServiceRequisitions servicehcmrequisitions;
	
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;


	
	/**
	 * 
	 * @param request
	 * 
	 * @request
	 
	 				{
					     "stats": 1,
					     "internalPostDate":"12345415112",
					     "internalCloseDate":"12345415112",
					     "openingDate":"12345415112",
					  	"recruiterName":"Gaurav",
					     "companyId": 1,
					     "managerName": "TEST",
					     "jobPosting":1,
					     "advertisingField1":"TEST",
					     "advertisingField2":"TEST",
					     "advertisingField3": "TEST",
					     "advertisingField4": "TEST",
					     "positionsAvailable":1,
					     "positionsFilled":1,
					     "applicantsApplied":1,
					     "applicantsInterviewed": 1,
					     "costadvirs": 10.0,
					  	"costrecri":20.30,
					  	"departmentId":1,
					  	"divisionId":1,
					  	"supervisorId":1,
					     "positionId":99
					
					
					 } 
	 
	   @response
	   
	   {
							"code": 201,
							"status": "CREATED",
							"result": {
										"stats": 1,
										"internalPostDate": 12345415112,
										"internalCloseDate": 12345415112,
										"openingDate": 12345415112,
										"recruiterName": "Gaurav",
										"companyId": 1,
										"managerName": "TEST",
										"jobPosting": 1,
										"advertisingField1": "TEST",
										"advertisingField2": "TEST",
										"advertisingField3": "TEST",
										"advertisingField4": "TEST",
										"positionsAvailable": 1,
										"positionsFilled": 1,
										"applicantsApplied": 1,
										"applicantsInterviewed": 1,
										"costadvirs": 10,
										"costrecri": 20.3,
										"departmentId": 1,
										"divisionId": 1,
										"supervisorId": 1,
										"positionId": 99
							},
							"btiMessage": {
							"message": " Requistion created Successfully!",
							"messageShort": HCM_REQUISITION_CREATED"
							}
			}
	 * @param dtoHcmRequisitions
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoRequisitions dtoHcmRequisitions) throws Exception{
	
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHcmRequisitions = servicehcmrequisitions.saveOrUpdate(dtoHcmRequisitions);
			responseMessage=displayMessage(dtoHcmRequisitions, "HCM_REQUISITION_CREATED", "HCM_REQUISITION_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		
		return responseMessage;
		
		
	}

	/**
	 * 
	 * @param request
	 	@request
	 	
	 								{
									  	"id":1,
									     "stats": 1,
									     "internalPostDate":"12345415112",
									     "internalCloseDate":"12345415112",
									     "openingDate":"12345415112",
									  	"recruiterName":"Gaurav",
									     "companyId": 1,
									     "managerName": "TEST",
									     "jobPosting":1,
									     "advertisingField1":"TEST",
									     "advertisingField2":"TEST",
									     "advertisingField3": "TEST",
									     "advertisingField4": "TEST",
									     "positionsAvailable":1,
									     "positionsFilled":1,
									     "applicantsApplied":1,
									     "applicantsInterviewed": 1,
									     "costadvirs": 10.0,
									  	"costrecri":20.30,
									  	"departmentId":1,
									  	"divisionId":1,
									  	"supervisorId":1,
									     "positionId":99
									
									
									 } 
	 	
	   @response
	   
	   										{
													"code": 201,
													"status": "CREATED",
													"result": {
													"id": 1,
													"stats": 1,
													"internalPostDate": 12345415112,
													"internalCloseDate": 12345415112,
													"openingDate": 12345415112,
													"recruiterName": "Gaurav",
													"companyId": 1,
													"managerName": "TEST",
													"jobPosting": 1,
													"advertisingField1": "TEST",
													"advertisingField2": "TEST",
													"advertisingField3": "TEST",
													"advertisingField4": "TEST",
													"positionsAvailable": 1,
													"positionsFilled": 1,
													"applicantsApplied": 1,
													"applicantsInterviewed": 1,
													"costadvirs": 10,
													"costrecri": 20.3,
													"departmentId": 1,
													"divisionId": 1,
													"supervisorId": 1,
													"positionId": 99
													},
														"btiMessage": {
														"message": "Requisition updated successfully.",
														"messageShort": "HCM_REQUISITION_UPDATE_SUCCESS"
														}
													}
	 * @param dtoHcmRequisitions
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoRequisitions dtoHcmRequisitions) throws Exception {
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHcmRequisitions = servicehcmrequisitions.saveOrUpdate(dtoHcmRequisitions);
			responseMessage=displayMessage(dtoHcmRequisitions, "HCM_REQUISITION_UPDATE_SUCCESS", "HCM_REQUISITION_LIST_NOT_GETTING",serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoRequisitions
	 	@request
	 	
	 		{
				  "ids":[1]
			
			}
			
		@response
		
					{
							"code": 201,
							"status": "CREATED",
							"result": {
							"stats": 0,
							"companyId": 0,
							"jobPosting": 0,
							"positionsAvailable": 0,
							"positionsFilled": 0,
							"applicantsApplied": 0,
							"applicantsInterviewed": 0,
							"deleteMessage": "Department deleted successfully.",
							"associateMessage": "N/A"
						},
						"btiMessage": {
							"message": "Requisition deleted successfully.",
							"messageShort": "REQUSITION_DELETED"
							}
						}
			 	
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteDepartment(HttpServletRequest request, @RequestBody DtoRequisitions dtoRequisitions) throws Exception {
		LOGGER.info("Delete Requistion Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoRequisitions.getIds() != null && !dtoRequisitions.getIds().isEmpty()) {
				DtoRequisitions dtoRequisitions2 = servicehcmrequisitions.deleteRequisition(dtoRequisitions.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("REQUSITION_DELETED", false), dtoRequisitions2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Requistion Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	/**
	 * 
	 * @param dtoSearch
	 * @param request
		@request
									{
										"searchKeyword":"",
												  "sortOn":"",
												  "sortBy":"DESC",
												 "pageNumber":"0",
												  "pageSize":"5"
			  						}
		
		@response
		
									{
										"code": 200,
										"status": "OK",
										"result": {
														"searchKeyword": "",
														"pageNumber": 0,
														"pageSize": 5,
														"sortOn": "",
														"sortBy": "DESC",
														"totalCount": 1,
														"records": [
														  {
														"id": 1,
														"stats": 1,
														"internalPostDate": 12345415000,
														"internalCloseDate": 12345415000,
														"openingDate": 12345415000,
														"recruiterName": "Gaurav",
														"companyId": 1,
														"managerName": "TEST",
														"jobPosting": 1,
														"advertisingField1": "TEST",
														"advertisingField2": "TEST",
														"advertisingField3": "TEST",
														"advertisingField4": "TEST",
														"positionsAvailable": 1,
														"positionsFilled": 1,
														"applicantsApplied": 1,
														"applicantsInterviewed": 1,
														"costadvirs": 10,
														"costrecri": 20.3,
														"departmentId": 1,
														"divisionId": 1,
														"supervisorId": 1,
														"positionId": 99
														}
											],
										},
										"btiMessage": {
												"message": "Requisition details fetched successfully.",
												"messageShort": "REQUISITION_GET_ALL"
										}
								}
			
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchRequistion(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Requistion Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicehcmrequisitions.searchRequistion(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "REQUISITION_GET_ALL", "REQUISITION_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Requsition Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/requisitionsidcheck", method = RequestMethod.POST)
	public ResponseMessage requisitionsidcheck(HttpServletRequest request, @RequestBody DtoRequisitions dtoRequisitions) throws Exception {
		LOGGER.info("requisitionsidcheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoRequisitions dtoRequisitionsObj = servicehcmrequisitions.repeatById(dtoRequisitions.getId());
			if (dtoRequisitionsObj != null) {
				if (dtoRequisitionsObj.getIsRepeat()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("REQUISITION_LIST_REPEAT", false), dtoRequisitionsObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted("REQUISITION_LIST_ID_NOT_REPEAT", false),
							dtoRequisitionsObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("REQUISITION_REPEAT_REQUISITION_NOT_FOUND", false), dtoRequisitionsObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	
	
}
