package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10200",indexes = {
        @Index(columnList = "HCMTRXINXD")
})
public class TransactionEntryDetail extends HcmBaseEntity implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMTRXINXD")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name ="HCMTRXINX")
	private TransactionEntry transactionEntry;
	
	@Column(name = "HCMTRXID")
	private Integer entryNumber;
	
	@Column(name = "HCMTRXDSEQ")
	private Integer detailsSequence;
	
	@ManyToOne
	@JoinColumn(name ="EMPLOYINDX")
	private EmployeeMaster employeeMaster;
	
	
	@Column(name = "HCMTRXTYP")
	private short transactionType;
	
	@ManyToOne
	@JoinColumn(name ="DEPINDX")
	private Department  department;
	
	@ManyToOne
	@JoinColumn(name ="PYCDINDX")
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	private PayCode code;
	
	@ManyToOne
	@JoinColumn(name ="BENCINDX")
	@Where(clause = "BENCINCTV = false and is_deleted = false")
	private BenefitCode benefitCode;
	
	@ManyToOne
	@JoinColumn(name ="DEDCINDX")
	@Where(clause = "DEDCINCTV = false and is_deleted = false")
	private DeductionCode deductionCode;
	
	@ManyToOne
	@JoinColumn(name ="DIMINXVALUE")
	private FinancialDimensions dimensions;
	
	@Column(name = "HCMAMNT")
	private BigDecimal amount;
	
	@Column(name = "HCMPARTE")
	private BigDecimal payRate;
	
	@Column(name = "HCMTRXFDT")
	private Date fromDate;
	
	@Column(name = "HCMTRXTDT")
	private Date toDate;
	
	@Column(name = "HCMTRXFROMBUILD")
	private Boolean fromBuild;
	
	@ManyToOne
	@JoinColumn(name = "HCMBULDINX")
	private BuildChecks buildChecks;
	
	@Column(name = "HCMFROMTRX")
	private Boolean fromTranscationEntry;
	
	@ManyToOne
	@JoinColumn(name = "HCMBATINDX")
	private Batches batches;
	
/*	@Column(name = "HCMTRXFROMPAYCODEMAN")
	private Boolean fromPaycodeMantenance;*/
	
	
	/*@ManyToMany(mappedBy="transactionEntryDetailList")
	private List<BuildChecks> BuildChecksList;*/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TransactionEntry getTransactionEntry() {
		return transactionEntry;
	}

	public void setTransactionEntry(TransactionEntry transactionEntry) {
		this.transactionEntry = transactionEntry;
	}

	public Integer getEntryNumber() {
		return entryNumber;
	}

	public void setEntryNumber(Integer entryNumber) {
		this.entryNumber = entryNumber;
	}

	public Integer getDetailsSequence() {
		return detailsSequence;
	}

	public void setDetailsSequence(Integer detailsSequence) {
		this.detailsSequence = detailsSequence;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public short getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(short transactionType) {
		this.transactionType = transactionType;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public PayCode getCode() {
		return code;
	}

	public void setCode(PayCode code) {
		this.code = code;
	}

	public BenefitCode getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(BenefitCode benefitCode) {
		this.benefitCode = benefitCode;
	}

	public DeductionCode getDeductionCode() {
		return deductionCode;
	}

	public void setDeductionCode(DeductionCode deductionCode) {
		this.deductionCode = deductionCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPayRate() {
		return payRate;
	}

	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	

	public FinancialDimensions getDimensions() {
		return dimensions;
	}

	public void setDimensions(FinancialDimensions dimensions) {
		this.dimensions = dimensions;
	}

	public Boolean getFromBuild() {
		return fromBuild;
	}

	public void setFromBuild(Boolean fromBuild) {
		this.fromBuild = fromBuild;
	}

	public BuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(BuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}

	public Boolean getFromTranscationEntry() {
		return fromTranscationEntry;
	}

	public void setFromTranscationEntry(Boolean fromTranscationEntry) {
		this.fromTranscationEntry = fromTranscationEntry;
	}


	public Batches getBatches() {
		return batches;
	}

	public void setBatches(Batches batches) {
		this.batches = batches;
	}

	

	  
	

	
	
}
