package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SY40003")
public class Dimensions extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1266446008568844175L;

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DIMIDX")
	private Integer id;

	@Column(name = "DIMDSC")
	private String dimensionDesc;

	@Column(name = "DIMDSCA")
	private String dimensionArabicDesc;

	@Column(name = "MODULE_CODE")
	private String moduleCode;

	@Column(name = "TABLENAME")
	private String tableName;

	@Column(name = "TABLEIDX")
	private Integer tableIndx;

	@Column(name = "STATIC")
	private boolean isStatic;

	@Column(name = "Has_Update", columnDefinition = "tinyint(4) default 0")
	private boolean hasUpdate;

	@Column(name = "COLNAME")
	private String colName;

	@Column(name = "CODE")
	private String code;

	@Column(name = "LDESC")
	private String englishDescription;

	@Column(name = "ADESC")
	private String arabicDescription;

	@Column(name = "CONDITIONCOL")
	private String colCondition;
	
	public Dimensions() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDimensionDesc() {
		return dimensionDesc;
	}

	public void setDimensionDesc(String dimensionDesc) {
		this.dimensionDesc = dimensionDesc;
	}

	public String getDimensionArabicDesc() {
		return dimensionArabicDesc;
	}

	public void setDimensionArabicDesc(String dimensionArabicDesc) {
		this.dimensionArabicDesc = dimensionArabicDesc;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Integer getTableIndx() {
		return tableIndx;
	}

	public void setTableIndx(Integer tableIndx) {
		this.tableIndx = tableIndx;
	}

	public boolean isStatic() {
		return isStatic;
	}

	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}

	public boolean isHasUpdate() {
		return hasUpdate;
	}

	public void setHasUpdate(boolean hasUpdate) {
		this.hasUpdate = hasUpdate;
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEnglishDescription() {
		return englishDescription;
	}

	public void setEnglishDescription(String englishDescription) {
		this.englishDescription = englishDescription;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}
public String getColCondition() {
		return colCondition;
	}

	public void setColCondition(String colCondition) {
		this.colCondition = colCondition;
	}

}
