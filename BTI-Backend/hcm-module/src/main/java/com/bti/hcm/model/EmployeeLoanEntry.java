package com.bti.hcm.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "hr10600")
public class EmployeeLoanEntry extends HcmBaseEntity2 {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDX")
	private Integer id;
	
	@Column(name = "wrkflwreqid")
	private Integer workflowRequestId;
	
	@Column(name = "loanamt")
	private Double loanAmount;
	
	@Column(name = "numofmths")
	private Integer numberOfMonths;
	
	@Column(name = "strtdt")
	private Date startDate;
	
	
	@Column(name = "enddt")
	private Date endDate;
	
	@Column(name = "remamt")
	private Double remainingAmount;

	@Column(name = "iscmplt")
	private Boolean isComplete;
	
	@ManyToOne
	@JoinColumn(name = "empid")
	private EmployeeMaster  employeeMaster;

	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "id")
	private List<EmployeeMonthlyInstallment> employeeMonthlyInstallmentList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWorkflowRequestId() {
		return workflowRequestId;
	}

	public void setWorkflowRequestId(Integer workflowRequestId) {
		this.workflowRequestId = workflowRequestId;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Integer getNumberOfMonths() {
		return numberOfMonths;
	}

	public void setNumberOfMonths(Integer numberOfMonths) {
		this.numberOfMonths = numberOfMonths;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public Boolean getIsComplete() {
		return isComplete;
	}

	public void setIsComplete(Boolean isComplete) {
		this.isComplete = isComplete;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public List<EmployeeMonthlyInstallment> getEmployeeMonthlyInstallmentList() {
		return employeeMonthlyInstallmentList;
	}

	public void setEmployeeMonthlyInstallmentList(List<EmployeeMonthlyInstallment> employeeMonthlyInstallmentList) {
		this.employeeMonthlyInstallmentList = employeeMonthlyInstallmentList;
	}
	
}
