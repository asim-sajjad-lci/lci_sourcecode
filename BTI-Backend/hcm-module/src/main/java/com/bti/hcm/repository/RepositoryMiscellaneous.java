package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Miscellaneous;

@Repository("repositoryMiscellaneous")
public interface RepositoryMiscellaneous extends JpaRepository<Miscellaneous, Integer> {

	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	public Miscellaneous findByIdAndIsDeleted(int id, boolean deleted);

	/**
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Miscellaneous d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleMiscellaneousEntry(@Param("deleted") Boolean deleted,
			@Param("updateById") Integer updateById, @Param("id") Integer id);

	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Miscellaneous d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteAllMiscellaneousEntries(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);

	/**
	 * @param deleted
	 * @return
	 */
	public List<Miscellaneous> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	@Query("select count(*) from Miscellaneous d where d.isDeleted=false")
	public Integer getCountOfTotalMiscellaneousEntries();

	@Query("select p from Miscellaneous p where (p.codeId =:id) and p.isDeleted=false")
	public List<Miscellaneous> findByMiscellaneousId(@Param("id") String miscId);

	@Query("select d from Miscellaneous d where (d.codeId like :searchKeyword  or d.codeName like :searchKeyword  or d.codeArabicName like :searchKeyword) and d.isDeleted=false")
	public List<Miscellaneous> predictiveMiscellaneousAllIdSearchWithPagination(
			@Param("searchKeyword") String searchKeyWord, Pageable pageable);

	public List<Miscellaneous> findByIsDeleted(boolean b, Pageable pageable);

	@Query("select count(*) from Miscellaneous d where (d.codeId like :searchKeyword  or d.codeName like :searchKeyword  or d.codeArabicName like :searchKeyword) and d.isDeleted=false")
	public Integer predictiveMiscellaneousCodeSearchTotalCount(@Param("searchKeyword") String searchKeyWord);

	public List<Miscellaneous> findByIsDeleted(boolean b);
	
	@Query("select d from Miscellaneous d where d.isDeleted=false")
	public List<Miscellaneous> predictiveMiscellaneousAllWithPagination(Pageable pageable);

	
	//ME
	@Query("select d from Miscellaneous d where d.isDeleted=false")
	public List<Miscellaneous> predictiveMiscellaneousAll();
	
	
	//ME
	@Query("select count(*) from Miscellaneous d where d.isDeleted=false")
	public Integer countPredictiveMiscellaneousAll();
	

}
