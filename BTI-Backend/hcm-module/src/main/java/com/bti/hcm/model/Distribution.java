package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10350")
public class Distribution extends HcmBaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMBULDINXD")
	private Integer id;

	@Column(name = "CHEKNMBR")
	private String checkNumber;

	@ManyToOne
	@JoinColumn(name = "HCMDEFINX")
	private Default default1;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "distribution")
	@Where(clause = "is_deleted = false")
	private List<AuditTrialCodes> listAuditTrialCodes;
	
	@ManyToOne
	@JoinColumn(name = "HCMBULDINX")
	private BuildChecks  buildChecks;
	
	@ManyToOne
	@JoinColumn(name = "HCMTRXINX")
	private TransactionEntry transactionEntry;
	
	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster  employeeMaster;
	
	@Column(name="HCMCALPOSTDT")
	private Date postingDate;

	
//	@ManyToOne
//	@JoinColumn(name = "ACTROWID")
//	private AccountsTableAccumulation  accountsTableAccumulation;
	
	/*@ManyToOne
	@JoinColumn(name="HCMDEDINX")
	private BuildPayrollCheckByDeductions buildPayrollCheckByDeductions;
	
	@ManyToOne 
	@JoinColumn(name="HCMBENINX")
	private BuildPayrollCheckByBenefits buildPayrollCheckByBenefits;*/
	
	
	@Column(name = "HCMTRXPYRL")
	private String auditTransactionNo;

	@Column(name = "HCMCDINDX")
	private Integer codeId;

	@Column(name = "DITPOTTYP")
	private Short postingType;

	@Column(name = "DEBTAMT")
	private BigDecimal debit;

	@Column(name = "CRDTAMT")
	private BigDecimal credit;

	@Column(name = "ACCTSEQN")
	private Integer accountNumberSeq;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getAuditTransactionNo() {
		return auditTransactionNo;
	}

	public void setAuditTransactionNo(String auditTransactionNo) {
		this.auditTransactionNo = auditTransactionNo;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public Short getPostingType() {
		return postingType;
	}

	public void setPostingType(Short postingType) {
		this.postingType = postingType;
	}

	public BigDecimal getDebit() {
		return debit;
	}

	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}

	public BigDecimal getCredit() {
		return credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	public Integer getAccountNumberSeq() {
		return accountNumberSeq;
	}

	public void setAccountNumberSeq(Integer accountNumberSeq) {
		this.accountNumberSeq = accountNumberSeq;
	}
	public Default getDefault1() {
		return default1;
	}

	public void setDefault1(Default default1) {
		this.default1 = default1;
	}

	public BuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(BuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public AccountsTableAccumulation getAccountsTableAccumulation() {
//		return accountsTableAccumulation;
		return null;
	}

	public void setAccountsTableAccumulation(AccountsTableAccumulation accountsTableAccumulation) {
//		this.accountsTableAccumulation = accountsTableAccumulation;
	}
	

	public List<AuditTrialCodes> getListAuditTrialCodes() {
		return listAuditTrialCodes;
	}

	public void setListAuditTrialCodes(List<AuditTrialCodes> listAuditTrialCodes) {
		this.listAuditTrialCodes = listAuditTrialCodes;
	}

	public TransactionEntry getTransactionEntry() {
		return transactionEntry;
	}

	public void setTransactionEntry(TransactionEntry transactionEntry) {
		this.transactionEntry = transactionEntry;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accountNumberSeq == null) ? 0 : accountNumberSeq.hashCode());
		result = prime * result + ((auditTransactionNo == null) ? 0 : auditTransactionNo.hashCode());
		result = prime * result + ((checkNumber == null) ? 0 : checkNumber.hashCode());
		result = prime * result + ((codeId == null) ? 0 : codeId.hashCode());
		result = prime * result + ((credit == null) ? 0 : credit.hashCode());
		result = prime * result + ((debit == null) ? 0 : debit.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((postingType == null) ? 0 : postingType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Distribution other = (Distribution) obj;
		if (accountNumberSeq == null) {
			if (other.accountNumberSeq != null)
				return false;
		} else if (!accountNumberSeq.equals(other.accountNumberSeq))
			return false;
		if (auditTransactionNo == null) {
			if (other.auditTransactionNo != null)
				return false;
		} else if (!auditTransactionNo.equals(other.auditTransactionNo))
			return false;
		if (checkNumber == null) {
			if (other.checkNumber != null)
				return false;
		} else if (!checkNumber.equals(other.checkNumber))
			return false;
		if (codeId == null) {
			if (other.codeId != null)
				return false;
		} else if (!codeId.equals(other.codeId))
			return false;
		if (credit == null) {
			if (other.credit != null)
				return false;
		} else if (!credit.equals(other.credit))
			return false;
		if (debit == null) {
			if (other.debit != null)
				return false;
		} else if (!debit.equals(other.debit))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (postingType == null) {
			if (other.postingType != null)
				return false;
		} else if (!postingType.equals(other.postingType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Distribution [id=" + id + ", checkNumber=" + checkNumber + ", auditTransactionNo=" + auditTransactionNo
				+ ", codeId=" + codeId + ", postingType=" + postingType + ", debit=" + debit + ", credit=" + credit
				+ ", accountNumberSeq=" + accountNumberSeq + "]";
	}

	public void setAuditTrialCodes(AuditTrialCodes auditTrialCodes) {
		
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public void setPostingDate(String string) {
		// TODO Auto-generated method stub
		
	}

}
