package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00914",indexes = {
        @Index(columnList = "HETHINRINDX")
})
public class HealthInsuranceEnrollment extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HETHINRINDX")
	private Integer id;
	
	@Column(name = "ACCSTATS")
	private Short status;
	
	
	@Column(name = "ELIGBDT")
	private Date eligibilityDate;
	
	@Column(name = "HETHSTRDT")
	private Date benefitStartDate;
	
	@Column(name = "HETHENDDT")
	private Date benefitEndDate;
	
	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;
	

	@ManyToOne
	@JoinColumn(name = "HLINRINDX")
	private HelthInsurance helthInsurance;

	
	@Column(name = "OVERCST")
	private Boolean overrideCost;
	
	
	@Column(name = "HLINRMAXBF", precision = 10, scale = 3)
	private BigDecimal insuredAmount;
	
	
	@Column(name = "HLINREMAMT", precision = 10, scale = 3)
	private BigDecimal costToEmployee;
	
	
	@Column(name = "HLINRCOAMT", precision = 10, scale = 3)
	private BigDecimal costToEmployeer;
	
	@Column(name = "HLINREMEXP", precision = 10, scale = 3)
	private BigDecimal additionalCost;
	
	@Column(name = "HLPOLYID", columnDefinition = "char(31)")
	private String policyNumber;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getEligibilityDate() {
		return eligibilityDate;
	}

	public void setEligibilityDate(Date eligibilityDate) {
		this.eligibilityDate = eligibilityDate;
	}

	public Date getBenefitStartDate() {
		return benefitStartDate;
	}

	public void setBenefitStartDate(Date benefitStartDate) {
		this.benefitStartDate = benefitStartDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}

	public Boolean getOverrideCost() {
		return overrideCost;
	}

	public void setOverrideCost(Boolean overrideCost) {
		this.overrideCost = overrideCost;
	}

	public BigDecimal getInsuredAmount() {
		return insuredAmount;
	}

	public void setInsuredAmount(BigDecimal insuredAmount) {
		this.insuredAmount = insuredAmount;
	}

	public BigDecimal getCostToEmployee() {
		return costToEmployee;
	}

	public void setCostToEmployee(BigDecimal costToEmployee) {
		this.costToEmployee = costToEmployee;
	}

	public BigDecimal getCostToEmployeer() {
		return costToEmployeer;
	}

	public void setCostToEmployeer(BigDecimal costToEmployeer) {
		this.costToEmployeer = costToEmployeer;
	}

	public BigDecimal getAdditionalCost() {
		return additionalCost;
	}

	public void setAdditionalCost(BigDecimal additionalCost) {
		this.additionalCost = additionalCost;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public HelthInsurance getHelthInsurance() {
		return helthInsurance;
	}

	public void setHelthInsurance(HelthInsurance helthInsurance) {
		this.helthInsurance = helthInsurance;
	}
	
}

