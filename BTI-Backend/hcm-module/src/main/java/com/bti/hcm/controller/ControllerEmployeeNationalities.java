package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeNationalities;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeNationalities;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeNationalities")
public class ControllerEmployeeNationalities extends BaseController{

	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeNationalities.class);

	@Autowired
	ServiceEmployeeNationalities serviceEmployeeNationalities;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @description : Create EmployeeNationalities
	 * @param request
	 * @param dtoEmployeeNationalities
	 * @return
	 * @throws Exception
	 * @request 
	 * {
  	 *		"employeeNationalityIndexId": 0,
  	 *		"employeeNationalityId": "E001",
  	 *		"employeeNationalityDescription": "Employee Nationality Desc",
  	 *		"employeeNationalityDescriptionArabic": "Employee Nationality Arabic Desc"
	 *	}
	 * @response
	 * {
     *		"code": 201,
     *		"status": "CREATED",
     *		"result": {
     *   		"employeeNationalityIndexId": 0,
     *   		"employeeNationalityId": "E001",
     *   		"employeeNationalityDescriptionArabic": "Employee Nationality Arabic Desc",
     *   		"employeeNationalityDescription": "Employee Nationality Desc",
     *   		"pageNumber": null,
     *   		"pageSize": null,
     *   		"ids": null,
     *   		"isActive": null,
     *     		"messageType": null,
     *   		"message": null,
     *   		"deleteMessage": null,
     *   		"associateMessage": null,
     *   		"delete": null,
     *   		"isRepeat": null
     *		},
     *		"btiMessage": {
     *   		"message": "Employee Nationalities Created Successfully",
     *   		"messageShort": "EMPLOYEE_NATIONALITIES_CREATED"
     *		}
	 *	}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createEmployeeNationalities(HttpServletRequest request, @RequestBody DtoEmployeeNationalities dtoEmployeeNationalities)throws Exception {
		LOGGER.info("Create EmployeeNationalities URL called");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeNationalities = serviceEmployeeNationalities.saveOrUpdateEmployeeNationalities(dtoEmployeeNationalities);
			responseMessage=displayMessage(dtoEmployeeNationalities, "EMPLOYEE_NATIONALITIES_CREATED", "EMPLOYEE_NATIONALITIES_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeeNationalities Method:" + responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description update EmployeeNationalities
	 * @param request
	 * @param dtoEmployeeNationalities
	 * @return
	 * @throws Exception
	 * @request 
	 * {
  	 *		"employeeNationalityIndexId": 1,
  	 *		"employeeNationalityId": "E1",
  	 *		"employeeNationalityDescription": "Description",
  	 *		"employeeNationalityDescriptionArabic": "Arabic Description"
	 *	}
	 * @response
	 * {
     *		"code": 201,
     *		"status": "CREATED",
     *		"result": {
     *   		"employeeNationalityIndexId": 1,
     *   		"employeeNationalityId": "E1",
     *   		"employeeNationalityDescriptionArabic": "Arabic Description",
     *   		"employeeNationalityDescription": "Description",
     *   		"pageNumber": null,
     *   		"pageSize": null,
     *   		"ids": null,
     *   		"isActive": null,
     *     		"messageType": null,
     *   		"message": null,
     *   		"deleteMessage": null,
     *   		"associateMessage": null,
     *   		"delete": null,
     *   		"isRepeat": null
     *		},
     *		"btiMessage": {
     *   		"message": "Employee Nationalities Updated Successfully",
     *   		"messageShort": "EMPLOYEE_NATIONALITIES_UPDATED"
     *		}
	 *	}
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateEmployeeNationalities(HttpServletRequest request, @RequestBody DtoEmployeeNationalities dtoEmployeeNationalities)
			throws Exception {
		LOGGER.info("Update EmployeeNationalities URL called");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeNationalities = serviceEmployeeNationalities.saveOrUpdateEmployeeNationalities(dtoEmployeeNationalities);
			responseMessage=displayMessage(dtoEmployeeNationalities, MessageConstant.EMPLOYEE_NATIONALITIES_UPDATED, MessageConstant.EMPLOYEE_NATIONALITIES_GET_ALL, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeeNationalities Method:" + responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description delete EmployeeNationalities
	 * @param request
	 * @param dtoEmployeeNationalities
	 * @return
	 * @throws Exception
	 * @request 
	 * { 
	 * 		"ids":[1] 
	 * } 
	 * @response
	 * {
     *		"code": 201,
     *		"status": "CREATED",
     *		"result": {
     *   		"employeeNationalityIndexId": null,
     *   		"employeeNationalityId": null,
     *   		"employeeNationalityDescriptionArabic": null,
     *   		"employeeNationalityDescription": null,
     *   		"pageNumber": null,
     *   		"pageSize": null,
     *   		"ids": null,
     *   		"isActive": null,
     *   		"messageType": "",
     *   		"message": null,
     *   		"deleteMessage": "N/A",
     *   		"associateMessage": "N/A",
     *   		"deleteEmployeeNationalities": [
     *       		{
     *           		"employeeNationalityIndexId": 1,
     *           		"employeeNationalityId": null,
     *           		"employeeNationalityDescriptionArabic": "Arabic Description",
     *           		"employeeNationalityDescription": "Description",
     *           		"pageNumber": null,
     *           		"pageSize": null,
     *           		"ids": null,
     *           		"isActive": null,
     *           		"messageType": null,
     *           		"message": null,
     *           		"deleteMessage": null,
     *           		"associateMessage": null,
     *           		"deleteEmployeeNationalities": null,
     *           		"isRepeat": null
     *       		}
     *   		],
     *   		"isRepeat": null
     *		},
     *		"btiMessage": {
     *   		"message": "Employee Nationalities deleted successfully",
     *   		"messageShort": "EMPLOYEE_NATIONALITIES_DELETED"
     *		}
	 *	}
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteEmployeeNationalities(HttpServletRequest request, @RequestBody DtoEmployeeNationalities dtoEmployeeNationalities)
			throws Exception {
		LOGGER.info("Delete EmployeeNationalities URL called");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeNationalities.getIds() != null && !dtoEmployeeNationalities.getIds().isEmpty()) {
				DtoEmployeeNationalities dtoEmployeeNationalities2 = serviceEmployeeNationalities.deleteEmployeeNationalities(dtoEmployeeNationalities.getIds());
				if (dtoEmployeeNationalities2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_NATIONALITIES_DELETED", false), dtoEmployeeNationalities2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_NATIONALITIES_NOT_DELETE_ID_MESSAGE", false), dtoEmployeeNationalities2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeeNationalities Method:" + responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description search EmployeeNationalities
	 * @param dtoSearch
	 * @param request
	 * @return 
	 * @request
	 * { 
	 * 		"searchKeyword":"", 
	 * 		"pageNumber" : 0, 
	 * 		"pageSize" :2
	 * } 
	 * @response
	 * {
     *		"code": 200,
     *		"status": "OK",
     *		"result": {
     *   		"searchKeyword": "",
     *   		"pageNumber": 0,
     *   		"pageSize": 2,
     *   		"sortOn": "",
     *   		"sortBy": "",
     *   		"totalCount": 2,
     *   		"records": [
     *       		{
     *           		"employeeNationalityIndexId": 2,
     *           		"employeeNationalityId": "E001",
     *           		"employeeNationalityDescriptionArabic": "Employee Nationality Arabic Desc",
     *           		"employeeNationalityDescription": "Employee Nationality Desc",
     *           		"pageNumber": null,
     *           		"pageSize": null,
     *           		"ids": null,
     *           		"isActive": null,
     *           		"messageType": null,
     *           		"message": null,
     *           		"deleteMessage": null,
     *           		"associateMessage": null,
     *           		"delete": null,
     *           		"isRepeat": null
     *       		}
     *   		]
     *		},
     *		"btiMessage": {
     *   		"message": "Employee Nationalities list fetched successfully",
     *   		"messageShort": "EMPLOYEE_NATIONALITIES_GET_ALL"
     *		}
	 *	}
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeNationalities(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeNationalities URL called");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeNationalities.searchEmployeeNationalities(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.EMPLOYEE_NATIONALITIES_GET_ALL, MessageConstant.EMPLOYEE_NATIONALITIES_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search EmployeeNationalities Method:" + dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	/**
	 * @description get EmployeeNationalities By Id
	 * @param request
	 * @param dtoEmployeeNationalities
	 * @return
	 * @throws Exception
	 * @request 
	 * { 
	 * 		"employeeNationalityIndexId": 2 
	 * } 
	 * @response
	 * {
     *		"code": 201,
     *		"status": "CREATED",
     *		"result": {
     *   		"employeeNationalityIndexId": null,
     *   		"employeeNationalityId": "E001",
     *   		"employeeNationalityDescriptionArabic": null,
     *   		"employeeNationalityDescription": "Employee Nationality Desc",
     *   		"pageNumber": null,
     *   		"pageSize": null,
     *   		"ids": null,
     *   		"isActive": null,
     *   		"messageType": null,
     *   		"message": null,
     *   		"deleteMessage": null,
     *   		"associateMessage": null,
     *   		"deleteEmployeeNationalities": null,
     *   		"isRepeat": null
     *		},
     *		"btiMessage": {
     *   		"message": "Employee Nationalities details fetched successfully",
     *   		"messageShort": "EMPLOYEE_NATIONALITIES_GET_DETAIL"
     *		}
	 *	}
	 */
	@RequestMapping(value = "/getEmployeeNationalitiesById", method = RequestMethod.POST)
	public ResponseMessage getEmployeeNationalitiesById(HttpServletRequest request, @RequestBody DtoEmployeeNationalities dtoEmployeeNationalities) throws Exception {
		LOGGER.info("Get EmployeeNationalities ById URL called");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeNationalities = serviceEmployeeNationalities.getEmployeeNationalitiesById(dtoEmployeeNationalities.getEmployeeNationalityIndexId());
			responseMessage=displayMessage(dtoEmployeeNationalities, "EMPLOYEE_NATIONALITIES_GET_DETAIL", "EMPLOYEE_NATIONALITIES_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoEmployeeNationalities!=null) {
			LOGGER.debug("Get EmployeeNationalities ById Method:" + dtoEmployeeNationalities.getEmployeeNationalityIndexId());
		}
		
		return responseMessage;
	}
	
	/**
	 * @description EmployeeNationalitiesId is exist or not
	 * @param request
	 * @param dtoEmployeeNationalities
	 * @return
	 * @throws Exception
	 * @request 
	 * { 
	 * 		"employeeNationalityId":"E001"
	 * } 
	 * @response
	 * {
     *		"code": 302,
     *		"status": "FOUND",
     * 		"result": {
     *   		"employeeNationalityIndexId": null,
     *   		"employeeNationalityId": null,
     *   		"employeeNationalityDescriptionArabic": null,
     *   		"employeeNationalityDescription": null,
     *   		"pageNumber": null,
     *   		"pageSize": null,
     *   		"ids": null,
     *   		"isActive": null,
     *   		"messageType": null,
     *   		"message": null,
     *   		"deleteMessage": null,
     *   		"associateMessage": null,
     *   		"deleteEmployeeNationalities": null,
     *   		"isRepeat": true
     *		},
     *		"btiMessage": {
     *   		"message": "Employee Nationalities ID already exists.",
     *   		"messageShort": "EMPLOYEE_NATIONALITIES_RESULT"
     *		}
	 *	}
	 */
	/*@RequestMapping(value = "/employeeNationalitiesIdcheck", method = RequestMethod.POST)
	public ResponseMessage employeeNationalitiesIdcheck(HttpServletRequest request, @RequestBody DtoEmployeeNationalities dtoEmployeeNationalities)
			throws Exception {
		LOGGER.info("EmployeeNationalitiesId Check ById URL Called");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeNationalities = serviceEmployeeNationalities.repeatByEmployeeNationalitiesId(dtoEmployeeNationalities.getEmployeeNationalityId());
			responseMessage=displayMessage(dtoEmployeeNationalities, "EMPLOYEE_NATIONALITIES_RESULT", "EMPLOYEE_NATIONALITIES_REPEAT_EMPLOYEENATIONALITYID_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}*/
	
	
	
	@RequestMapping(value = "/employeeNationalitiesIdcheck", method = RequestMethod.POST)
	public ResponseMessage employeeNationalitiesIdcheck(HttpServletRequest request, @RequestBody DtoEmployeeNationalities dtoEmployeeNationalities)
			throws Exception {
		LOGGER.info("EmployeeNationalitiesId ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeNationalities dtoEmployeeNationalitiesObj = serviceEmployeeNationalities.repeatByEmployeeNationalitiesId(dtoEmployeeNationalities.getEmployeeNationalityId());
			
			 if (dtoEmployeeNationalitiesObj !=null && dtoEmployeeNationalitiesObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                         serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_NATIONALITIES_RESULT", false), dtoEmployeeNationalitiesObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                         serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_NATIONALITIES_REPEAT_EMPLOYEENATIONALITYID_NOT_FOUND", false),
                         dtoEmployeeNationalitiesObj);
             }
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	/**
	 * @description search EmployeeNationalities
	 * @param dtoSearch
	 * @param request
	 * @return 
	 * @request
	 * { 
	 * 		"searchKeyword":"E001", 
	 * 		"pageNumber" : 0, 
	 * 		"pageSize" :2
	 * } 
	 * @response
	 * {
    		"code": 200,
    		"status": "OK",
    		"result": {
        		"searchKeyword": "E001",
        		"pageNumber": 0,
        		"pageSize": 2,
        		"ids": [
            		"E001"
        		],
        		"records": 1
    		},
    		"btiMessage": {
        		"message": "Employee Nationalities list fetched successfully.",
        		"messageShort": "EMPLOYEE_NATIONALITIES_GET_ALL"
    		}
		}
	 */
	@RequestMapping(value = "/searchEmployeeNationalityId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeNationalityId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeNationalities by ID URL called");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeNationalities.searchEmployeeNationalityId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.EMPLOYEE_NATIONALITIES_GET_ALL, MessageConstant.EMPLOYEE_NATIONALITIES_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search EmployeeNationalities Method:" + dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllEmployeeNationalitiesDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllEmployeeNationalitiesDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeeNationalities> employeeNationalitiesList = serviceEmployeeNationalities.getAllEmployeeNationalitiesDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_NATIONALITIES_GET_ALL", false), employeeNationalitiesList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return responseMessage;
	}
	
	/**
	 * @description getAll EmployeeNationalityId
	 * @param dtoSearch
	 * @param request
	 * @return 
	 * @request
	 * { 
	 * 		"searchKeyword":"E001", 
	 * 		"pageNumber" : 0, 
	 * 		"pageSize" :2
	 * } 
	 * @response
	 * {
     *		"code": 200,
     *		"status": "OK",
     *		"result": {
     *   		"searchKeyword": "E001",
     *   		"pageNumber": 0,
     *   		"pageSize": 2,
     *   		"sortOn": "",
     *   		"sortBy": "",
     *   		"totalCount": 2,
     *   		"records": [
     *       		{
     *           		"employeeNationalityIndexId": 2,
     *           		"employeeNationalityId": "E001",
     *           		"employeeNationalityDescriptionArabic": "Employee Nationality Arabic Desc",
     *           		"employeeNationalityDescription": "Employee Nationality Desc",
     *           		"pageNumber": null,
     *           		"pageSize": null,
     *           		"ids": null,
     *           		"isActive": null,
     *           		"messageType": null,
     *           		"message": null,
     *           		"deleteMessage": null,
     *           		"associateMessage": null,
     *           		"delete": null,
     *           		"isRepeat": null
     *       		}
     *   		]
     *		},
     *		"btiMessage": {
     *   		"message": "Employee Nationalities list fetched successfully",
     *   		"messageShort": "EMPLOYEE_NATIONALITIES_GET_ALL"
     *		}
	 *	}
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllEmployeeNationalityId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllEmployeeNationalityId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("GetAll EmployeeNationalityId URL called");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeNationalities.getAllEmployeeNationalityId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_NATIONALITIES_GET_ALL", "EMPLOYEE_NATIONALITIES_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("GetAll EmployeeNationalityId Method:" + dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}

}
