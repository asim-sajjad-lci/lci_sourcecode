package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "work_flow_upload")
@NamedQuery(name = "WorkFlowUploadFile.findAll", query = "SELECT l FROM WorkFlowUploadFile l")
public class WorkFlowUploadFile extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "work_flow_id")
	private int workFlowId;

	@Column(name="DESCFILE")
	private String fileDesc;
	
	@Lob
	@Column(name = "work_flow_file")
	private byte[] workFlowFile;
	
	

	@OneToMany(mappedBy = "workFlowUploadFile")
	private List<WorkFlowAssign> workFlowAssignList;

	public WorkFlowUploadFile(Integer integer, byte[] workFlowFile2) {

	}

	public WorkFlowUploadFile() {

	}

	public int getWorkFlowId() {
		return workFlowId;
	}

	public void setWorkFlowId(int workFlowId) {
		this.workFlowId = workFlowId;
	}

	public String getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}

	public byte[] getWorkFlowFile() {
		return workFlowFile;
	}

	public void setWorkFlowFile(byte[] workFlowFile) {
		this.workFlowFile = workFlowFile;
	}

	public List<WorkFlowAssign> getWorkFlowAssignList() {
		return workFlowAssignList;
	}

	public void setWorkFlowAssignList(List<WorkFlowAssign> workFlowAssignList) {
		this.workFlowAssignList = workFlowAssignList;
	}

}
