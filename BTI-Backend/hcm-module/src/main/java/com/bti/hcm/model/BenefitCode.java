package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40903", indexes = { @Index(columnList = "BENCINDX") })
public class BenefitCode extends HcmBaseEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BENCINDX")
    private Integer id;

    @Column(name = "BENCID",columnDefinition="char(15)")
    private String benefitId;

    @Column(name = "BENCDSCR",columnDefinition="char(31)")
    private String desc;

    @Column(name = "BENCDSCRA",columnDefinition="char(61)")
    private String arbicDesc;

    @Column(name = "BENCSTDT")
    private Date startDate;

    @Column(name = "BENCEDDT")
    private Date endDate;

    @Column(name = "BENCTRXRQ")
    private boolean transction;

    @Column(name = "BENCMETHD")
    private Short method;
    
    @Column(name = "PYCDFACTR", precision = 15, scale = 6)
	private BigDecimal payFactor;
    
    @Column(name="DEDCCUSTDATE")
	private boolean isCustomDate;
    
    @Column(name="BENCNOOFDAYS")
	private Integer noOfDays;
	
	@Column(name="BENCENDDAYS")
	private Integer endDateDays;
    
	/*
	 * @ManyToOne(targetEntity = EmployeeBenefitMaintenance.class)
	 * 
	 * @JoinColumn(name ="BENCAMT") private BigDecimal benefitAmount;
	 */

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "benefitCode")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<EmployeeBenefitMaintenance> listEmployeeBenefitMaintenance;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "benefitCode")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<BuildPayrollCheckByBenefits> listBuildPayrollCheckByBenefits;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "benefitCode")
	private List<TransactionEntryDetail> transactionEntryDetail;
	
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="PYCDINDX") private PayCode payCode;
	 */
	
	@ManyToMany(cascade= {CascadeType.ALL})
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	@JoinTable(name = "Benefit_PayCode", joinColumns = { @JoinColumn(name = "BENCINDX") }, inverseJoinColumns = {
			@JoinColumn(name = "PYCDINDX") })
	private List<PayCode>payCodeList;
	
	@Column(name = "ALLPAYCODE")
	private boolean allPaycode;
	
    @Column(name = "BENCAMT",precision = 13,scale = 3)
    private BigDecimal amount;

    @Column(name = "BENCPERT",precision = 13,scale = 3)
    private BigDecimal percent;

    @Column(name = "BENCMXPERD",precision = 13,scale = 3)
    private BigDecimal perPeriod;

    @Column(name = "BENCMXYERL",precision = 13,scale = 3)
    private BigDecimal perYear;

    @Column(name = "BENCMXLIFE",precision = 13,scale = 3)
    private BigDecimal lifeTime;

    @Column(name = "BENCFREQN")
    private Short frequency;

    @Column(name = "BENCINCTV")
    private boolean inActive;

    
	@Column(name = "type_field")	// ME dtd 04Sep
	private Integer typeField;		//for new "Type" field binding 

	@Column(name = "round_of")		// ME dtd 04Sep
	private Integer roundOf;		
	
	
	public Integer getTypeField() {
		return typeField;
	}

	public void setTypeField(Integer typeField) {
		this.typeField = typeField;
	}

	public Integer getRoundOf() {
		return roundOf;
	}

	public void setRoundOf(Integer roundOf) {
		this.roundOf = roundOf;
	}
	
    public boolean isCustomDate() {
		return isCustomDate;
	}

	public void setCustomDate(boolean isCustomDate) {
		this.isCustomDate = isCustomDate;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBenefitId() {
        return benefitId;
    }

    public void setBenefitId(String benefitId) {
        this.benefitId = benefitId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getArbicDesc() {
        return arbicDesc;
    }

    public void setArbicDesc(String arbicDesc) {
        this.arbicDesc = arbicDesc;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isTransction() {
        return transction;
    }

    public void setTransction(boolean transction) {
        this.transction = transction;
    }

    public Short getMethod() {
        return method;
    }

    public void setMethod(Short method) {
        this.method = method;
    }

    public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	public BigDecimal getPerPeriod() {
		return perPeriod;
	}

	public void setPerPeriod(BigDecimal perPeriod) {
		this.perPeriod = perPeriod;
	}

	public BigDecimal getPerYear() {
		return perYear;
	}

	public void setPerYear(BigDecimal perYear) {
		this.perYear = perYear;
	}

	public BigDecimal getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(BigDecimal lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Short getFrequency() {
        return frequency;
    }

    public void setFrequency(Short frequency) {
        this.frequency = frequency;
    }

    public boolean isInActive() {
        return inActive;
    }

    public void setInActive(boolean inActive) {
        this.inActive = inActive;
    }

	public List<EmployeeBenefitMaintenance> getListEmployeeBenefitMaintenance() {
		return listEmployeeBenefitMaintenance;
	}

	public void setListEmployeeBenefitMaintenance(List<EmployeeBenefitMaintenance> listEmployeeBenefitMaintenance) {
		this.listEmployeeBenefitMaintenance = listEmployeeBenefitMaintenance;
	}

	public List<BuildPayrollCheckByBenefits> getListBuildPayrollCheckByBenefits() {
		return listBuildPayrollCheckByBenefits;
	}

	public void setListBuildPayrollCheckByBenefits(List<BuildPayrollCheckByBenefits> listBuildPayrollCheckByBenefits) {
		this.listBuildPayrollCheckByBenefits = listBuildPayrollCheckByBenefits;
	}
	/*
	 * public PayCode getPayCode() { return payCode; }
	 * 
	 * public void setPayCode(PayCode payCode) { this.payCode = payCode; }
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((arbicDesc == null) ? 0 : arbicDesc.hashCode());
		result = prime * result + ((benefitId == null) ? 0 : benefitId.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (inActive ? 1231 : 1237);
		result = prime * result + ((lifeTime == null) ? 0 : lifeTime.hashCode());
		result = prime * result
				+ ((listBuildPayrollCheckByBenefits == null) ? 0 : listBuildPayrollCheckByBenefits.hashCode());
		result = prime * result
				+ ((listEmployeeBenefitMaintenance == null) ? 0 : listEmployeeBenefitMaintenance.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((perPeriod == null) ? 0 : perPeriod.hashCode());
		result = prime * result + ((perYear == null) ? 0 : perYear.hashCode());
		result = prime * result + ((percent == null) ? 0 : percent.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + (transction ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

	public List<PayCode> getPayCodeList() {
		return payCodeList;
	}

	public void setPayCodeList(List<PayCode> payCodeList) {
		this.payCodeList = payCodeList;
	}

	public boolean isAllPaycode() {
		return allPaycode;
	}

	public void setAllPaycode(boolean allPaycode) {
		this.allPaycode = allPaycode;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getEndDateDays() {
		return endDateDays;
	}

	public void setEndDateDays(Integer endDateDays) {
		this.endDateDays = endDateDays;
	} 
	
	public List<TransactionEntryDetail> getTransactionEntryDetail() {
		return transactionEntryDetail;
	}

	public void setTransactionEntryDetail(List<TransactionEntryDetail> transactionEntryDetail) {
		this.transactionEntryDetail = transactionEntryDetail;
	} 
	
	/*
	 * public EmployeeBenefitMaintenance getEmployeeBenefitMaintenance() { return
	 * employeeBenefitMaintenance; }
	 * 
	 * public void setEmployeeBenefitMaintenance(EmployeeBenefitMaintenance
	 * employeeBenefitMaintenance) { this.employeeBenefitMaintenance =
	 * employeeBenefitMaintenance; }
	 */

	/*
	 * public BigDecimal getBenefitAmount() { return benefitAmount; }
	 * 
	 * public void setBenefitAmount(BigDecimal benefitAmount) { this.benefitAmount =
	 * benefitAmount; }
	 * 
	 */
	
}
