package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoRetirementFundSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceRetirementFundSetup;

@RestController
@RequestMapping("/retirementFundSetup")
public class ControllerRetirementFundSetup extends BaseController{

	
private static final Logger LOGGER = Logger.getLogger(ControllerRetirementFundSetup.class);
	
	
	@Autowired(required=true)
	ServiceRetirementFundSetup serviceRetirementFundSetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoRetirementFundSetup dtoRetirementFundSetup) throws Exception {
		LOGGER.info("Create RetirementFundSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoRetirementFundSetup = serviceRetirementFundSetup.saveOrUpdateRetirementFundSetup(dtoRetirementFundSetup);
			responseMessage=displayMessage(dtoRetirementFundSetup, "RETIREMENTFUNDSETUP_CREATED", "RETIREMENTFUNDSETUP_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create RetirementFundSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoRetirementFundSetup dtoRetirementFundSetup) throws Exception {
		LOGGER.info("Update RetirementFundSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoRetirementFundSetup = serviceRetirementFundSetup.saveOrUpdateRetirementFundSetup(dtoRetirementFundSetup);
			responseMessage=displayMessage(dtoRetirementFundSetup, "RETIREMENTFUNDSETUP_CREATED_UPDATED_SUCCESS", "RETIREMENTFUNDSETUP_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update RetirementFundSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoRetirementFundSetup dtoRetirementFundSetup) throws Exception {
		LOGGER.info("Delete RetirementFundSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoRetirementFundSetup.getIds() != null && !dtoRetirementFundSetup.getIds().isEmpty()) {
				DtoRetirementFundSetup dtoRetirementFundSetup2 = serviceRetirementFundSetup.deleteRetirementFundSetup(dtoRetirementFundSetup.getIds());
				
				if(dtoRetirementFundSetup2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("RETIREMENTFUNDSETUP_DELETED", false), dtoRetirementFundSetup2);
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoRetirementFundSetup2.getMessageType());
					btiMessageHcm.setMessageShort("RETIREMENT_FUND_SETUP_NOT_DELETE_ID_MESSAGE");
					
					
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoRetirementFundSetup2);
				}
				

				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete RetirementFundSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search RetirementFundSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceRetirementFundSetup.searchRetirementFundSetup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "RETIREMENTFUNDSETUP_GET_ALL", "RETIREMENTFUNDSETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search RetirementFundSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getRetirementPlanId", method = RequestMethod.POST)
	public ResponseMessage getRetirementPlanId(HttpServletRequest request, @RequestBody DtoRetirementFundSetup dtoRetirementFundSetup) throws Exception {
		LOGGER.info("Get RetirementFundSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoRetirementFundSetup dtoRetirementFundSetupObj = serviceRetirementFundSetup.getRetirementFundSetupById(dtoRetirementFundSetup.getId());
			responseMessage=displayMessage(dtoRetirementFundSetupObj, "RETIREMENTFUNDSETUP_LIST_GET_DETAIL", "RETIREMENTFUNDSETUP_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get RetirementPlanSetup by Id Method:"+dtoRetirementFundSetup.getId());
		return responseMessage;
	}
	
	
}
