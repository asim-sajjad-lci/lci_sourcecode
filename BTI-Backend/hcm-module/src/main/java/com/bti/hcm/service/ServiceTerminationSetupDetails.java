package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.TerminationSetup;
import com.bti.hcm.model.TerminationSetupDetails;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTerminationSetupDetails;
import com.bti.hcm.repository.RepositoryTerminationSetup;
import com.bti.hcm.repository.RepositoryTerminationSetupDetails;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceTerminationSetupDetails")
public class ServiceTerminationSetupDetails {
	
static Logger log = Logger.getLogger(ServiceTerminationSetupDetails.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryTerminationSetupDetails repositoryTerminationSetupDetails;
	
	
	@Autowired
	RepositoryTerminationSetup repositoryTerminationSetup;
	
	
	
	public DtoTerminationSetupDetails saveOrUpdateTerminationSetupDetails(DtoTerminationSetupDetails dtoTerminationSetupDetails) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		TerminationSetupDetails terminationSetupDetails=null;
		if (dtoTerminationSetupDetails.getId() != null && dtoTerminationSetupDetails.getId() > 0) {
			terminationSetupDetails = repositoryTerminationSetupDetails.findByIdAndIsDeleted(dtoTerminationSetupDetails.getId(), false);
			terminationSetupDetails.setUpdatedBy(loggedInUserId);
			terminationSetupDetails.setUpdatedDate(new Date());
		} else {
			terminationSetupDetails = new TerminationSetupDetails();
			terminationSetupDetails.setCreatedDate(new Date());
			
			Integer rowId = repositoryTerminationSetupDetails.getCountOfTotalTerminationSetupDetails();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			terminationSetupDetails.setRowId(increment);
		}
		
		TerminationSetup terminationSetup =null;
		if(dtoTerminationSetupDetails.getTerminationSetupId()!=null &&dtoTerminationSetupDetails.getTerminationSetupId()>0) {
			terminationSetup=repositoryTerminationSetup.findOne(dtoTerminationSetupDetails.getTerminationSetupId());
		}
		
		terminationSetupDetails.setTerminationSetup(terminationSetup);
		terminationSetupDetails.setTerminationSequence(dtoTerminationSetupDetails.getTerminationSequence());
		terminationSetupDetails.setTerminationDesc(dtoTerminationSetupDetails.getTerminationDesc());
		terminationSetupDetails.setTerminationArbicDesc(dtoTerminationSetupDetails.getTerminationArbicDesc());
		terminationSetupDetails.setTerminationDate(dtoTerminationSetupDetails.getTerminationDate());
		terminationSetupDetails.setTerminationPersonName(dtoTerminationSetupDetails.getTerminationPersonName());
		terminationSetupDetails.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryTerminationSetupDetails.saveAndFlush(terminationSetupDetails);
		return dtoTerminationSetupDetails;
	}
	
	public DtoTerminationSetupDetails deleteTerminationSetupDetails(List<Integer> ids) {
		log.info("delete TerminationSetupDetails Method");
		DtoTerminationSetupDetails dtoTerminationSetupDetails = new DtoTerminationSetupDetails();
		dtoTerminationSetupDetails.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TERMINATIONSETUPDETAILS_DELETED", false));
		dtoTerminationSetupDetails.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TERMINATIONSETUPDETAILS_ASSOCIATED", false));
		List<DtoTerminationSetupDetails> deleteTerminationSetupDetails = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		List<DtoTerminationSetupDetails> actualList = new ArrayList<>();
		try {
			for (Integer id : ids) {
				TerminationSetupDetails terminationSetupDetails = repositoryTerminationSetupDetails.findOne(id);
				if(!terminationSetupDetails.getTerminationSetup().getListTerminationSetupDetails().isEmpty() && terminationSetupDetails.getTerminationSetup().getListTerminationSetupDetails()!=null) {
					for(TerminationSetupDetails details :terminationSetupDetails.getTerminationSetup().getListTerminationSetupDetails()) {
						if(details.getIsDeleted()==false) {
							if(id!=details.getId()) {
								DtoTerminationSetupDetails dtoTerminationSetupDetails3 = new DtoTerminationSetupDetails();
								dtoTerminationSetupDetails3.setId(id);
								dtoTerminationSetupDetails3.setTerminationSequence(details.getTerminationSequence());
								dtoTerminationSetupDetails3.setTerminationDesc(details.getTerminationDesc());
								dtoTerminationSetupDetails3.setTerminationArbicDesc(details.getTerminationArbicDesc());
								dtoTerminationSetupDetails3.setTerminationDate(details.getTerminationDate());
								dtoTerminationSetupDetails3.setTerminationPersonName(details.getTerminationPersonName());
								actualList.add(dtoTerminationSetupDetails3);
							}
						}
					}
				}
				
				DtoTerminationSetupDetails dtoTerminationSetupDetails2 = new DtoTerminationSetupDetails();
				dtoTerminationSetupDetails2.setId(id);
				dtoTerminationSetupDetails2.setTerminationSequence(terminationSetupDetails.getTerminationSequence());
                dtoTerminationSetupDetails2.setTerminationDesc(terminationSetupDetails.getTerminationDesc());
                dtoTerminationSetupDetails2.setTerminationArbicDesc(terminationSetupDetails.getTerminationArbicDesc());
                dtoTerminationSetupDetails2.setTerminationDate(terminationSetupDetails.getTerminationDate());
                dtoTerminationSetupDetails2.setTerminationPersonName(terminationSetupDetails.getTerminationPersonName());
                dtoTerminationSetupDetails2.setList(actualList);

				
				repositoryTerminationSetupDetails.deleteSingleTeminationSetupDetails(true, loggedInUserId,id);
				deleteTerminationSetupDetails.add(dtoTerminationSetupDetails2);

			}
			dtoTerminationSetupDetails.setDelete(deleteTerminationSetupDetails);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Location :"+dtoTerminationSetupDetails.getId());
		return dtoTerminationSetupDetails;
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchTerminationSetupDetails(DtoSearch dtoSearch) {
		log.info("search TerminationSetupDetails Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("terminationSequence") || dtoSearch.getSortOn().equals("terminationDesc")|| dtoSearch.getSortOn().equals("terminationArbicDesc") || dtoSearch.getSortOn().equals("terminationDate") || dtoSearch.getSortOn().equals("terminationPersonName")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
			
			dtoSearch.setTotalCount(this.repositoryTerminationSetupDetails.predictiveTerminationSetupDetailsSearchTotalCount("%"+searchWord+"%"));
			List<TerminationSetupDetails> terminationSetupDetailsList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					terminationSetupDetailsList = this.repositoryTerminationSetupDetails.predictiveTerminationSetupDetailsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					terminationSetupDetailsList = this.repositoryTerminationSetupDetails.predictiveTerminationSetupDetailsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					terminationSetupDetailsList = this.repositoryTerminationSetupDetails.predictiveTerminationSetupDetailsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			if(terminationSetupDetailsList != null && !terminationSetupDetailsList.isEmpty()){
				List<DtoTerminationSetupDetails> dtoTerminationSetupDetailsList = new ArrayList<>();
				DtoTerminationSetupDetails dtoTerminationSetupDetails=null;
				for (TerminationSetupDetails terminationSetupDetails : terminationSetupDetailsList) {
					dtoTerminationSetupDetails = new DtoTerminationSetupDetails(terminationSetupDetails);
					
					
					dtoTerminationSetupDetails.setId(terminationSetupDetails.getId());
					dtoTerminationSetupDetails.setTerminationSequence(terminationSetupDetails.getTerminationSequence());
                    dtoTerminationSetupDetails.setTerminationDesc(terminationSetupDetails.getTerminationDesc());
                    dtoTerminationSetupDetails.setTerminationArbicDesc(terminationSetupDetails.getTerminationArbicDesc());
                    dtoTerminationSetupDetails.setTerminationPersonName(terminationSetupDetails.getTerminationPersonName());
                    dtoTerminationSetupDetails.setTerminationDate(terminationSetupDetails.getTerminationDate());
                    dtoTerminationSetupDetailsList.add(dtoTerminationSetupDetails);

					}
				dtoSearch.setRecords(dtoTerminationSetupDetailsList);
				}
				
			}
		
		log.debug("Search searchPosition Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
		}
	
	
	
	public DtoTerminationSetupDetails getRetirementPlanSetupDetailsById(int id) {
		DtoTerminationSetupDetails dtoTerminationSetupDetails  = new DtoTerminationSetupDetails();
		if (id > 0) {
			TerminationSetupDetails terminationSetupDetails = repositoryTerminationSetupDetails.findByIdAndIsDeleted(id, false);
			if (terminationSetupDetails != null) {
				dtoTerminationSetupDetails = new DtoTerminationSetupDetails(terminationSetupDetails);
			} else {
				dtoTerminationSetupDetails.setMessageType("TERMINATIONSETUPDETAILS_NOT_GETTING");

			}
		} else {
			dtoTerminationSetupDetails.setMessageType("INVALID_TERMINATIONDETAILS_ID");

		}
		return dtoTerminationSetupDetails;
	}


	
	
	
	

}
