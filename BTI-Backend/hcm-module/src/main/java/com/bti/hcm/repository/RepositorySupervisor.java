package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.Supervisor;

@Repository("repositorySupervisor")
public interface RepositorySupervisor extends JpaRepository<Supervisor, Integer>{

	public Supervisor findByIdAndIsDeleted(int id, boolean deleted);
	
	public List<Supervisor> findByIsDeleted(Boolean deleted);
	
	@Query("select count(*) from Supervisor d where d.isDeleted=false")
	public Integer getCountOfTotalSupervisor();
	
	public List<Supervisor> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Supervisor d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteSupervisor(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Supervisor d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleSupervisor(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	@Query("select d from Supervisor d where (d.description LIKE :searchKeyWord or d.superVisionCode LIKE :searchKeyWord or d.employeeName LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord) and d.isDeleted=false")
	public List<Supervisor> predictiveSupervisorSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	public List<Supervisor> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	@Query("select count(*) from Supervisor d where (d.description LIKE :searchKeyWord or d.superVisionCode LIKE :searchKeyWord or d.employeeName LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveSupervisorSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select p from Supervisor p where (p.superVisionCode =:superVisionCode) and p.isDeleted=false")
	public List<Supervisor> findBySuperVisionCode(@Param("superVisionCode")String superVisionCode);

	@Query("select d from Supervisor d where (d.superVisionCode LIKE :superVisorCode) and d.isDeleted=false")
	public Supervisor findSuperVisorByCode(@Param("superVisorCode")String superVisorCode);

	@Query("select d from EmployeeMaster d where (d.employeeIndexId=:searchKeyWord) and d.isDeleted=false")
	public EmployeeMaster predictiveEmployeeIdSearch(@Param("searchKeyWord") Integer searchKeyWord);
	
	@Query("select count(*) from Employee d where (d.id=:searchKeyWord) and d.isDeleted=false")
	public Integer predictiveEmployeeCount(@Param("searchKeyWord") Integer searchKeyWord);

	@Query("select d.superVisionCode from Supervisor d where (d.superVisionCode like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<String> predictiveSupervisorIdSearchWithPagination(@Param("searchKeyWord") String string);

	
	@Query("select s from Supervisor s where (s.superVisionCode like :searchKeyWord) and s.isDeleted=false")
	public List<Supervisor> predictivegetAllSuperVisorIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select count(*) from Supervisor s ")
	public Integer getCountOfTotalSupervisors();
	
}
