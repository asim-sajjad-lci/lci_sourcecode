package com.bti.hcm.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoBuildDefaultCheck extends DtoBase{

	private List<Integer> batchId;

	public List<Integer> getBatchId() {
		return batchId;
	}

	public void setBatchId(List<Integer> batchId) {
		this.batchId = batchId;
	}
	
	
	
	
	
	
}
