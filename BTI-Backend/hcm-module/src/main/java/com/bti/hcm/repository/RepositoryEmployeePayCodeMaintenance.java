package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeePayCodeMaintenance;

@Repository("/repositoryEmployeePayCodeMaintenance")
public interface RepositoryEmployeePayCodeMaintenance extends JpaRepository<EmployeePayCodeMaintenance, Integer> {

	// EmployeePayCodeMaintenance findByIdAndIsDeleted(Integer id);

	EmployeePayCodeMaintenance findByIdAndIsDeleted(Integer id, boolean b);

	EmployeePayCodeMaintenance findOne(Integer planId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeePayCodeMaintenance e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.id =:id ")
	public void deleteSingleEmployeePayCodeMaintenance(@Param("deleted") Boolean deleted,
			@Param("updateById") Integer updateById, @Param("id") Integer id);

	@Query("select count(*) from EmployeePayCodeMaintenance e where ((e.baseOnPayCode LIKE :searchKeyWord or e.employeeMaster.employeeId LIKE :searchKeyWord or e.payCode.payCodeId LIKE :searchKeyWord) and e.isDeleted=false)")
	Integer predictiveEmployeePayCodeMaintenanceSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	// HF
	@Query("select count(*) from EmployeePayCodeMaintenance e where e.isDeleted=false and (e.baseOnPayCode LIKE :searchKeyWord or e.employeeMaster.employeeId LIKE :searchKeyWord or e.payCode.payCodeId LIKE :searchKeyWord) ")
	Integer predictiveEmployeePayCodeMaintenanceSearchTotalCount2(@Param("searchKeyWord") String searchKeyWord);

	@Query("select e from EmployeePayCodeMaintenance e where ((e.baseOnPayCode LIKE :searchKeyWord or e.employeeMaster.employeeId LIKE :searchKeyWord or e.payCode.payCodeId LIKE :searchKeyWord) and e.isDeleted=false)")
	List<EmployeePayCodeMaintenance> predictiveEmployeePayCodeMaintenanceSearchWithPagination(
			@Param("searchKeyWord") String searchKeyWord, Pageable pageable);

	List<EmployeePayCodeMaintenance> findByIsDeleted(Boolean deleted);

	// @Cacheable("EmployeePayCodeMaintenance")
	// @Query("select m from EmployeePayCodeMaintenance m where
	// m.employeeMaster.employeeIndexId =:id and m.isDeleted=false and m. I :ids ")
	// List<EmployeePayCodeMaintenance> findByEmployeeIdAndPayCodeIds(@Param("id")
	// int id, @Param("ids") List<Integer> ids);

	// @Cacheable("EmployeePayCodeMaintenance")
	@Query("select m from EmployeePayCodeMaintenance m where m.employeeMaster.employeeIndexId =:id and m.isDeleted=false")
	List<EmployeePayCodeMaintenance> findByEmployeeId(@Param("id") int id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeePayCodeMaintenance e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.employeeMaster.employeeIndexId =:id ")
	void deleteByEmployeeId(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from EmployeePayCodeMaintenance e ")
	public Integer getCountOfTotalEmployeePayCodeMaintenance();

	@Query("select m from EmployeePayCodeMaintenance m where (m.employeeMaster.employeeIndexId =:id and m.payCodeType.id =:payCodeId) and m.isDeleted=false")
	public List<EmployeePayCodeMaintenance> findByEmployeeIdAndCodeId(@Param("id") int id,
			@Param("payCodeId") int payCodeId);

	public List<EmployeePayCodeMaintenance> findByIsDeletedAndInactive(Boolean deleted, Boolean inactive);

	@Query("select m from EmployeePayCodeMaintenance m where m.employeeMaster.employeeIndexId  =:employeeId and m.isDeleted=false")
	List<EmployeePayCodeMaintenance> findByEmployeeIds(@Param("employeeId") Integer employeeId);

	@Query("select m from EmployeePayCodeMaintenance m where (m.employeeMaster.employeeIndexId =:id and m.payCode.id =:payCodeId) and m.isDeleted=false")
	public List<EmployeePayCodeMaintenance> findByEmployeeIdAndCodeIdS(@Param("id") int id,
			@Param("payCodeId") int payCodeId);

	@Query("select m from EmployeePayCodeMaintenance m where m.employeeMaster.employeeIndexId =:id and m.payCode.id =:payCodeId and m.isDeleted=false")
	public List<EmployeePayCodeMaintenance> findByEmployeeIdAndCodeIdes(@Param("id") int id,
			@Param("payCodeId") int payCodeId);

	@Query("select m from EmployeePayCodeMaintenance m where m.payCode.id  =:paycodeId and m.isDeleted=false")
	public List<EmployeePayCodeMaintenance> findBypayCode(@Param("paycodeId") Integer paycodeId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeePayCodeMaintenance e set e.inactive=:inactive where e.id =:id ")
	public void deleteSingleEmployeePayCodeMaintenance(@Param("inactive") boolean inactive, @Param("id") Integer id);

	// ME Created
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDeductionMaintenance e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.employeeMaster.employeeIndexId =:id")
	public void deleteByEmployeeIdAndCodeId(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer empId);

	// ME
	public EmployeePayCodeMaintenance findByPayCodeIdAndIsDeletedAndEmployeeMasterEmployeeIndexIdAndEmployeeMasterIsDeleted(
			int payCodeId, Boolean deleted, int empIndexId, Boolean deleted2);

	// HF Test
	Integer countByIsDeleted(boolean b);

	
	//for TotalPkg&ESB Calc in Loan API.getEmpDetails 
	@Query("select m from EmployeePayCodeMaintenance m where m.employeeMaster.employeeIndexId =:id and m.isDeleted=false and m.inactive=false and m.payCode.id IN :payCodeIds ")
	List<EmployeePayCodeMaintenance> findByEmpIdAndIsDeletedAndIsInactiveAndPayCodeIds(@Param("id") int id,
			@Param("payCodeIds") List<Integer> payCodeIds);
	
	

}
