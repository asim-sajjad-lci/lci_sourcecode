package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Description: The persistent class for hijri day 
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 * 
 *
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR_HIJRI_DAY",indexes = {
        @Index(columnList = "HIJRIDAYINDX")
})
public class HijriDay implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HIJRIDAYINDX")
	private Integer Id;

	@Column(name = "HIJRIDAY", columnDefinition = "int(2)")
	private Integer day;

	@Column(name = "is_deleted", columnDefinition = "tinyint(0) default 0")
	protected Boolean isDeleted;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
