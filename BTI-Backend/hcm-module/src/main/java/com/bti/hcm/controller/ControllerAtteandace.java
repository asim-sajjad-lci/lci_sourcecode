package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoAtteandace;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceAtteandace;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/attedance")
public class ControllerAtteandace extends BaseController{

	/**
	 * @Description LOGGER use for put a logger in Atteandace Controller
	 */
	private Logger log = Logger.getLogger(ControllerAtteandace.class);
	/**
	 * @Description serviceAtteandace Autowired here using annotation of spring for use of serviceAtteandace method in this controller
	 */
	@Autowired(required=true)
	ServiceAtteandace serviceAtteandace;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @param request{
 
	  	"accrueType": 2,
	  	"attendance": true,
	  	"currentyear": 2,
  "numberOfDayInWeek": 2,
  "numberOfHourInDay": 2,
  "nextTranscationNum": 2,
  "pageNumber": "10",
  
  
	  	"pageSize": "10"
	  	
}
	 * @param dtoAtteandace
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoAtteandace dtoAtteandace) throws Exception {
		log.info("Create Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAtteandace = serviceAtteandace.saveOrUpdateAtteandace(dtoAtteandace);
			responseMessage=displayMessage(dtoAtteandace, "ATTEADANCE_CREATED", "ATTEADANCE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
 
 	 	"id":1,
	  	"accrueType": 2,
	  	"attendance": true,
	  	"currentyear": 2,
  "numberOfDayInWeek": 2,
  "numberOfHourInDay": 2,
  "nextTranscationNum": 2,
  "pageNumber": "10",
  
  
	  	"pageSize": "10"
	  	
}
	 * @param dtoAtteandace
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoAtteandace dtoAtteandace) throws Exception {
		log.info("Update Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAtteandace = serviceAtteandace.saveOrUpdateAtteandace(dtoAtteandace);
			responseMessage=displayMessage(dtoAtteandace, "ATTEADANCE_UPDATED_SUCCESS", "ATTEADANCE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  			"ids": [1]
		}
	 * @param dtoAtteandace
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletAttendace(HttpServletRequest request, @RequestBody DtoAtteandace dtoAtteandace) throws Exception {
		log.info("Delete Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoAtteandace.getIds() != null && !dtoAtteandace.getIds().isEmpty()) {
				DtoAtteandace dtpAttedance2 = serviceAtteandace.deleteAtteandace(dtoAtteandace.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ATTEADANCE_DELETED", false), dtpAttedance2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody  DtoAtteandace dtoAtteandace) throws Exception {
		log.info("Get All PostionBudget Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceAtteandace.getAll(dtoAtteandace);
			responseMessage=displayMessage(dtoSearch, "ATTEADANCE_GET_ALL", "ATTEADANCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get All PostionBudget Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
 
 	 	"id":1
	}
	 * @param dtoAtteandace
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAtteandaceId", method = RequestMethod.POST)
	public ResponseMessage getPositionBudgetId(HttpServletRequest request, @RequestBody DtoAtteandace dtoAtteandace) throws Exception {
		log.info("Get Position ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAtteandace dtoPositionObj = serviceAtteandace.getByAtteandaceId(dtoAtteandace.getId());
			responseMessage=displayMessage(dtoPositionObj, "ATTEADANCE_LIST_GET_DETAIL", "ATTEADANCE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get Position by Id Method:"+dtoAtteandace.getId());
		return responseMessage;
	}
}
