package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoPositionPlanSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePositionPalnSetup;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/positionPlanSetup")
public class ControllerPositionPlanSetup extends BaseController{
	
	@Autowired
	ServicePositionPalnSetup servicePositionPalnSetup;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger log = Logger.getLogger(ControllerSkillSteup.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @param request{
  	"positionPlanId":1,
  	"inactive" :  false,
 	 	"position":{
       						"id":1
     					},
	  	"positionPlanDesc": "test1",
	  	"positionPlanArbicDesc": "test2",
	  	"pageNumber": "0",
	  	"pageSize": "10"
  	
		}
		@param response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": null,
			"position": {
			"createdDate": null,
			"updatedDate": 1517486004400,
			"updatedBy": 0,
			"isDeleted": false,
			"updatedRow": null,
			"rowId": 0,
			"id": 1,
			"skillSetSetup": null,
			"positionId": null,
			"description": null,
			"arabicDescription": null,
			"positionClass": null,
			"reportToPostion": null,
			"postionLongDesc": null
			},
			"positionPlanId": 1,
			"positionPlanDesc": "test1",
			"positionPlanArbicDesc": "test2",
			"positionPlanStart": null,
			"positionPlanEnd": null,
			"inactive": false,
			"pageNumber": 0,
			"pageSize": 10,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePositionSetup": null,
			"isRepeat": null
			},
			"btiMessage": {
			"message": "Position plan created successfully",
			"messageShort": "POSITION_PLAN_CREATED"
			}
			}
	 * @param dtoPositionPlanSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/createPositionPlanSetup", method = RequestMethod.POST)
	public ResponseMessage createPositionPlanSetup(HttpServletRequest request,@RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception{
		log.info("Create Postion paln setup Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionPlanSetup = servicePositionPalnSetup.saveOrUpdatePositionPalnSetup(dtoPositionPlanSetup);
			responseMessage=displayMessage(dtoPositionPlanSetup, "POSITION_PLAN_CREATED", "POSITION_PLAN_NOT_CREATED", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		log.debug("Create Postion paln setup Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	/**
	 * @param request{
  	"pageNumber": "0",
  	"pageSize": "10"
  	
	}
	 @param request{
			"code": 201,
			"status": "CREATED",
			"result": {
			"pageNumber": 0,
			"pageSize": 10,
			"totalCount": 1,
			"records": [
			  {
			"id": 1,
			"position": null,
			"positionPlanId": null,
			"positionPlanDesc": "test1",
			"positionPlanArbicDesc": "test2",
			"positionPlanStart": null,
			"positionPlanEnd": null,
			"inactive": false,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePositionSetup": null,
			"isRepeat": null
			}
			],
			},
			"btiMessage": {
			"message": "Position plan list fetched successfully",
			"messageShort": "POSITION_PLAN_GET_ALL"
			}
			}
	 * @param dtoPositionPlanSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getPositionPlanSetup(HttpServletRequest request, @RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception {
		log.info("Get All Postion Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePositionPalnSetup.getAllPostionPlanSteup(dtoPositionPlanSetup);
			responseMessage=displayMessage(dtoSearch, MessageConstant.POSITION_PLAN_GET_ALL, MessageConstant.POSITION_PLAN_LIST_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Get All Postion Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllPostionId", method = RequestMethod.PUT)
	public ResponseMessage getPositionPlanSetupBaseOnPositionId(HttpServletRequest request, @RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception {
		log.info("Get All Postion Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePositionPalnSetup.findByAllPosionPlanPositionId(dtoPositionPlanSetup);
			responseMessage=displayMessage(dtoSearch, MessageConstant.POSITION_PLAN_GET_ALL, MessageConstant.POSITION_PLAN_LIST_NOT_GETTING, response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		log.debug("Get All Postion Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  		"id" : 1,
  		"positionPlanId":1,
  		"inactive" :  false,
 	 	"position":{
       				id":1},
	  	"positionPlanDesc": "test1",
	  	"positionPlanArbicDesc": "test55555555555",
	  	"pageNumber": "0",
	  	"pageSize": "10"
	}
	@param response
	{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": 1,
		"position": {
		"createdDate": null,
		"updatedDate": 1517486483642,
		"updatedBy": 0,
		"isDeleted": false,
		"updatedRow": null,
		"rowId": 0,
		"id": 1,
		"skillSetSetup": null,
		"positionId": null,
		"description": null,
		"arabicDescription": null,
		"positionClass": null,
		"reportToPostion": null,
		"postionLongDesc": null
		},
		"positionPlanId": 1,
		"positionPlanDesc": "test1",
		"positionPlanArbicDesc": "test55555555555",
		"positionPlanStart": null,
		"positionPlanEnd": null,
		"inactive": false,
		"pageNumber": 0,
		"pageSize": 10,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePositionSetup": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Position plan updated successfully",
		"messageShort": "POSITION_PLAN_UPDATED_SUCCESS"
		}
		}
	 * @param dtoPositionPlanSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePositionPlanSetup(HttpServletRequest request, @RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception {
		log.info("Update Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionPlanSetup = servicePositionPalnSetup.saveOrUpdatePositionPalnSetup(dtoPositionPlanSetup);
			responseMessage=displayMessage(dtoPositionPlanSetup, "POSITION_PLAN_UPDATED_SUCCESS", "POSITION_PLAN_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Update Position Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	public ResponseMessage updateStatus(HttpServletRequest request, @RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception {
		log.info("Update Status Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionPlanSetup = servicePositionPalnSetup.updateStatus(dtoPositionPlanSetup);
			responseMessage=displayMessage(dtoPositionPlanSetup, "POSITION_PLAN_UPDATED_SUCCESS", "POSITION_PLAN_NOT_UPDATED", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		log.debug("Update Status Position Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePositionPlanSetup(HttpServletRequest request, @RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception {
		log.info("Delete Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPositionPlanSetup.getIds() != null && !dtoPositionPlanSetup.getIds().isEmpty()) {
				DtoPositionPlanSetup dtoPosition1 = servicePositionPalnSetup.deletePositionPlanSteup(dtoPositionPlanSetup.getIds());
				if(dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("POSITION_PLAN_DELETED", false), dtoPosition1);
				}else {
					
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoPosition1.getMessageType());
					btiMessageHcm.setMessageShort("POSITION_PLAN_NOT_DELETE_ID_MESSAGE");
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoPosition1);
				}
				
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete Position plan Setup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  	"id" : 1
  	
	}
	 @param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"position": null,
		"positionPlanId": null,
		"positionPlanDesc": "test1",
		"positionPlanArbicDesc": "test55555555555",
		"positionPlanStart": null,
		"positionPlanEnd": null,
		"inactive": false,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePositionSetup": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Skill Steup details fetched successfully",
		"messageShort": "POSITION_PLAN_GET_DETAIL"
		}
		}
	 * @param dtoPositionPlanSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPositionPlanSetupId", method = RequestMethod.POST)
	public ResponseMessage getPositionPlanSetupId(HttpServletRequest request, @RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception {
		log.info("Get Position Steup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPositionPlanSetup dtoPositionObj = servicePositionPalnSetup.getByPositionPlanSetupId(dtoPositionPlanSetup.getId());
			responseMessage=displayMessage(dtoPositionObj, "POSITION_PLAN_GET_DETAIL", "POSITION_PLAN_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Get Position plan Setup by Id Method:"+dtoPositionPlanSetup.getId());
		return responseMessage;
	}
	
	/**
	 * @param request{
  	"positionPlanId":1
  	
	}
	@param response{
			"code": 302,
			"status": "FOUND",
			"result": {
			"id": null,
			"position": null,
			"positionPlanId": null,
			"positionPlanDesc": null,
			"positionPlanArbicDesc": null,
			"positionPlanStart": null,
			"positionPlanEnd": null,
			"inactive": false,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePositionSetup": null,
			"isRepeat": null
			},
			"btiMessage": {
			"message": "position plan already exists",
			"messageShort": "POSITION_PLAN_RESULT"
			}
			}

	 * @param dtoPositionPlanSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/positionPlanIdcheck", method = RequestMethod.POST)
	public ResponseMessage positionPlanIdcheck(HttpServletRequest request, @RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception {
		log.info("positionPlanIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPositionPlanSetup dtoPositionPlanSetupObj = servicePositionPalnSetup.repeatByPositionPlanId(dtoPositionPlanSetup.getPositionPlanId());
			if (dtoPositionPlanSetupObj != null) {
				if (dtoPositionPlanSetupObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("POSITION_PLAN_EXIST", false), dtoPositionPlanSetupObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted(dtoPositionPlanSetupObj.getMessageType(), false),
							dtoPositionPlanSetupObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("POSITION_PLAN_REPEAT_ID_NOT_FOUND", false), dtoPositionPlanSetupObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{
  		"searchKeyword":"t",
		  "pageNumber" : 0,
		  "pageSize" :2
  	
	}@param response{
			"code": 200,
			"status": "OK",
			"result": {
			"searchKeyword": "t",
			"pageNumber": 0,
			"pageSize": 2,
			"totalCount": 2,
			"records": [
			  {
			"id": null,
			"position": null,
			"positionPlanId": null,
			"positionPlanDesc": "test1",
			"positionPlanArbicDesc": "test55555555555",
			"positionPlanStart": null,
			"positionPlanEnd": null,
			"inactive": false,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePositionSetup": null,
			"isRepeat": null
			},
			  {
			"id": null,
			"position": null,
			"positionPlanId": 1,
			"positionPlanDesc": "test1",
			"positionPlanArbicDesc": "test2",
			"positionPlanStart": null,
			"positionPlanEnd": null,
			"inactive": false,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePositionSetup": null,
			"isRepeat": null
			}
			],
			},
			"btiMessage": {
			"message": "PositionPlan list fetched successfully",
			"messageShort": "POSITION_PLAN_GET_ALL"
			}
			}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPositionPlan(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search searchPositionPlan Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionPalnSetup.searchPositionPlan(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.POSITION_PLAN_GET_ALL, MessageConstant.POSITION_PLAN_LIST_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search searchPositionPlans Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	  @RequestMapping(value = "/searchPositionPlan1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	    public ResponseMessage searchPosition1(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
	        log.info("Search searchPosition Method");
	        ResponseMessage responseMessage = null;
	        boolean flag =serviceHcmHome.checkValidCompanyAccess();
	        if (flag) {
	                dtoSearch = this.servicePositionPalnSetup.searchPositionPlan1(dtoSearch);
	                responseMessage=displayMessage(dtoSearch, "POSITION_GET_ALL", "POSITION_LIST_NOT_GETTING", response);
	        } else {
	            responseMessage = unauthorizedMsg(response);
	        }
	        if(dtoSearch!=null) {
				log.debug("Search searchPositionPlan Method:"+dtoSearch.getTotalCount());
			}
	        return responseMessage;
	    } 
	  

		@RequestMapping(value = "/changeStatus", method = RequestMethod.POST)
	    public ResponseMessage changeStatus(HttpServletRequest request,@RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception{
	        log.info("Create Postion paln setup Info");
	        ResponseMessage responseMessage = null;
	        boolean flag =serviceHcmHome.checkValidCompanyAccess();
	        if (flag) {
	            dtoPositionPlanSetup = servicePositionPalnSetup.changeStatus(dtoPositionPlanSetup);
	            if (dtoPositionPlanSetup != null) {
	                responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
	response.getMessageByShortAndIsDeleted("STATUS CHANGE SUECSSFULLY", false), dtoPositionPlanSetup);
	            } else {
	                responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
	response.getMessageByShortAndIsDeleted("STATUS NOT UPDATED", false), dtoPositionPlanSetup);
	            }
	        } else {
	            responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
	response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
	        }
	        log.debug("Create Postion paln setup Method:"+responseMessage.getMessage());
	        return responseMessage;

	    } 
		
		@RequestMapping(value = "/searchPositionPlanId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
		public ResponseMessage searchPositionPlanId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
			log.info("Search Position Method");
			ResponseMessage responseMessage = null;
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtoSearch = this.servicePositionPalnSetup.searchPositionPlanId(dtoSearch);
				responseMessage=displayMessage(dtoSearch, MessageConstant.POSITION_PLAN_GET_ALL, MessageConstant.POSITION_PLAN_LIST_NOT_GETTING, response);
			} else {
				responseMessage = unauthorizedMsg(response);
			}
			if(dtoSearch!=null) {
				log.debug("Search searchPositionPlan Method:"+dtoSearch.getTotalCount());
			}
			return responseMessage;
		}

		
		@RequestMapping(value = "/getAllPositionPlantDropDown", method = RequestMethod.GET)
		@ResponseBody
		public ResponseMessage getAllPositionPlantDropDown(HttpServletRequest request) {
			ResponseMessage responseMessage = null;
			try {
				boolean flag =serviceHcmHome.checkValidCompanyAccess();
				if (flag) {
					List<DtoPositionPlanSetup> positionPlanSetupList = servicePositionPalnSetup.getAllPositionPlanSetupInActiveList();
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("POSITION_PLAN_GET_ALL", false), positionPlanSetupList);
				}else {
					responseMessage = unauthorizedMsg(response);
				}
				
			} catch (Exception e) {
				log.error(e);
			}
			
			return responseMessage;
		}
		
		
		@RequestMapping(value = "/getPostionByPositionPlanSetupId", method = RequestMethod.POST)
	    public ResponseMessage getPostionByPositionPlanSetupId(HttpServletRequest request,@RequestBody DtoPositionPlanSetup dtoPositionPlanSetup) throws Exception{
	        log.info("getPostionByPositionPlanSetupId");
	        ResponseMessage responseMessage = null;
	        boolean flag =serviceHcmHome.checkValidCompanyAccess();
	        if (flag) {
	            dtoPositionPlanSetup = servicePositionPalnSetup.getPostionFromPostionPlanSetupId(dtoPositionPlanSetup.getId());
	            responseMessage=displayMessage(dtoPositionPlanSetup, "POSITION_PLAN_GET_ALL", "POSITION_PLAN_LIST_NOT_GETTING", response);
	        } else {
	            responseMessage = unauthorizedMsg(response);
	        }
	        log.debug("getPostionByPositionPlanSetupId Method:"+responseMessage.getMessage());
	        return responseMessage;

	    } 
		
		
}
