package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.ProjectSetup;
import com.bti.hcm.model.dto.DtoProjectSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryProjectSetup;

@Service("serviceProjectSetup")
public class ServiceProjectSetup {
	
	static Logger log = Logger.getLogger(ServiceProjectSetup.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryProjectSetup repositoryProjectSetup;
	
	
	
	public DtoProjectSetup saveOrUpdateProjectSetup(DtoProjectSetup dtoProjectSetup){
		log.info("saveOrUpdateProjectSetup Method");
		ProjectSetup projectSetup = null;
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if (dtoProjectSetup.getId() != null && dtoProjectSetup.getId() > 0) {
				projectSetup = repositoryProjectSetup.findByIdAndIsDeleted(dtoProjectSetup.getId(), false);
				projectSetup.setUpdatedBy(loggedInUserId);
				projectSetup.setUpdatedDate(new Date());
			} else {
				projectSetup = new ProjectSetup();
				projectSetup.setCreatedDate(new Date());
				Integer rowId = repositoryProjectSetup.getCountOfTotalProjectSetup();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				projectSetup.setRowId(increment);
			}
			projectSetup.setProjectId(dtoProjectSetup.getProjectId());
			projectSetup.setProjectName(dtoProjectSetup.getProjectName());
			projectSetup.setProjectArabicName(dtoProjectSetup.getProjectArabicName());
			projectSetup.setProjectDescription(dtoProjectSetup.getProjectDescription());
			projectSetup.setProjectArabicDescription(dtoProjectSetup.getProjectArabicDescription());
			projectSetup.setStartDate(dtoProjectSetup.getStartDate());
			projectSetup.setEndDate(dtoProjectSetup.getEndDate());
			repositoryProjectSetup.saveAndFlush(projectSetup);
			log.debug("ProjectSetup is:" + dtoProjectSetup.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoProjectSetup;

	}
	
	
	public DtoProjectSetup getProjectSetuoByProjectSetupId(int id) {
		log.info("getProjectSetuoByProjectSetupId Method");
		DtoProjectSetup dtoProjectSetup = new DtoProjectSetup();
		try {
			if (id > 0) {
				ProjectSetup projectSetup = repositoryProjectSetup.findByIdAndIsDeleted(id, false);
				if (projectSetup != null) {
					dtoProjectSetup = new DtoProjectSetup(projectSetup);
					dtoProjectSetup.setStartDate(projectSetup.getStartDate());
					dtoProjectSetup.setEndDate(projectSetup.getEndDate());
				} else {
					dtoProjectSetup.setMessageType("PROJECT_NOT_GETTING");

				}
			} else {
				dtoProjectSetup.setMessageType("INVALID_PROJECT_ID");

			}
			log.debug("ProjectSetup By Id is:" + dtoProjectSetup.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoProjectSetup;
	}

	
	public DtoProjectSetup repeatByProjectSetupId(String projectId) {
		log.info("repeatByProjectSetupId Method");
		DtoProjectSetup dtoProjectSetup = new DtoProjectSetup();
		try {
			List<ProjectSetup> projectSetups = repositoryProjectSetup.findByProjectId(projectId.trim());
			if (projectSetups != null && !projectSetups.isEmpty()) {
				dtoProjectSetup.setIsRepeat(true);
			} else {
				dtoProjectSetup.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoProjectSetup;
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllProjectId(DtoSearch dtoSearch) {
		log.info("getAllProjectId Method");
		try {
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				dtoSearch=searchByProject(dtoSearch);
				dtoSearch.setTotalCount(
						this.repositoryProjectSetup.predictiveProjectSetupSearchTotalCount("%" + searchWord + "%"));
				
				List<ProjectSetup> projectList =  searchByPageSizeAndNumber(dtoSearch);
				
					if (projectList != null && !projectList.isEmpty()) {
					List<DtoProjectSetup> dtoProjectSetupList = new ArrayList<>();
					DtoProjectSetup dtoProjectSetup = null;
					for (ProjectSetup projectSetup : projectList) {
						dtoProjectSetup = new DtoProjectSetup(projectSetup);
						dtoProjectSetup.setProjectId(projectSetup.getProjectId());
						dtoProjectSetup.setProjectName(projectSetup.getProjectName());
						dtoProjectSetup.setProjectArabicName(projectSetup.getProjectArabicName());
						dtoProjectSetup.setProjectDescription(projectSetup.getProjectDescription());
						dtoProjectSetup.setProjectArabicDescription(projectSetup.getProjectArabicDescription());
						dtoProjectSetup.setStartDate(projectSetup.getStartDate());
						dtoProjectSetup.setEndDate(projectSetup.getEndDate());
						dtoProjectSetupList.add(dtoProjectSetup);
					}
					dtoSearch.setRecords(dtoProjectSetupList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		if(dtoSearch!=null) {
			log.debug("Search ProjectSetup Size is:" + dtoSearch.getTotalCount());
		}
		
		return dtoSearch;
	}
	
	
	
	
	
	
	
	public DtoSearch searchByProject(DtoSearch dtoSearch) {
		
		
		String condition = "";

		if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
			switch (dtoSearch.getSortOn()) {
			case "projectId":
				condition += dtoSearch.getSortOn();
				break;
			case "projectName":
				condition += dtoSearch.getSortOn();
				break;
			case "projectArabicName":
				condition += dtoSearch.getSortOn();
				break;
			case "projectDescription":
				condition += dtoSearch.getSortOn();
				break;
			case "projectArabicDescription":
				condition += dtoSearch.getSortOn();
				break;
			case "startDate":
				condition += dtoSearch.getSortOn();
				break;
			case "endDate":
				condition += dtoSearch.getSortOn();
				break;
			default:
				condition += "id";
			}
		} else {
			condition += "id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
		}
		dtoSearch.setCondition(condition);
		return dtoSearch;
		
	}
	
	public List<ProjectSetup> searchByPageSizeAndNumber(DtoSearch dtoSearch) {
		List<ProjectSetup> projectList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

			if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
				projectList = this.repositoryProjectSetup.predictiveProjectSetupSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if (dtoSearch.getSortBy().equals("ASC")) {
				projectList = this.repositoryProjectSetup.predictiveProjectSetupSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
			} else if (dtoSearch.getSortBy().equals("DESC")) {
				projectList = this.repositoryProjectSetup.predictiveProjectSetupSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
			}

		}
		
		return projectList;
	}
	
	
	
	@Transactional
	@Async
	public List<DtoProjectSetup> getAllProjectSetupDropDownList() {
		log.info("getAllProjectSetupDropDownList  Method");
		List<DtoProjectSetup> dtoProjectSetupList = new ArrayList<>();
		try {
			List<ProjectSetup> list = repositoryProjectSetup.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (ProjectSetup projectSetup : list) {
					DtoProjectSetup dtoProjectSetup = new DtoProjectSetup();
					dtoProjectSetup.setId(projectSetup.getId());
					dtoProjectSetup.setProjectId(projectSetup.getProjectId());
					dtoProjectSetup.setProjectName(projectSetup.getProjectName());
					dtoProjectSetup.setProjectArabicName(projectSetup.getProjectArabicName());
					dtoProjectSetup.setProjectDescription(projectSetup.getProjectDescription());
					dtoProjectSetup.setProjectArabicDescription(projectSetup.getProjectArabicDescription());
					dtoProjectSetup.setStartDate(projectSetup.getStartDate());
					dtoProjectSetup.setEndDate(projectSetup.getEndDate());
					dtoProjectSetupList.add(dtoProjectSetup);
				}
			}
			log.debug("Project is:" + dtoProjectSetupList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoProjectSetupList;
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchProject(DtoSearch dtoSearch) {
		log.info("searchProject Method");
		try {
			if (dtoSearch != null) {
				
				String searchWord = dtoSearch.getSearchKeyword();
				dtoSearch=searchByProject(dtoSearch);
				dtoSearch.setTotalCount(
						this.repositoryProjectSetup.predictiveProjectSetupSearchTotalCount("%" + searchWord + "%"));
				
				List<ProjectSetup> projectList =  searchByPageSizeAndNumber(dtoSearch);
				
				if (projectList != null && !projectList.isEmpty()) {
					List<DtoProjectSetup> dtoProjectSetupsList = new ArrayList<>();
					DtoProjectSetup dtoProjectSetup = null;
					for (ProjectSetup projectSetup : projectList) {
						dtoProjectSetup = new DtoProjectSetup(projectSetup);
						dtoProjectSetup.setId(projectSetup.getId());
						dtoProjectSetup.setProjectId(projectSetup.getProjectId());
						dtoProjectSetup.setProjectName(projectSetup.getProjectName());
						dtoProjectSetup.setProjectArabicName(projectSetup.getProjectArabicName());
						dtoProjectSetup.setProjectDescription(projectSetup.getProjectDescription());
						dtoProjectSetup.setProjectArabicDescription(projectSetup.getProjectArabicDescription());
						dtoProjectSetup.setStartDate(projectSetup.getStartDate());
						dtoProjectSetup.setEndDate(projectSetup.getEndDate());
						dtoProjectSetupsList.add(dtoProjectSetup);
					}
					dtoSearch.setRecords(dtoProjectSetupsList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	
	public DtoProjectSetup getProjectByProjectId(int id) {
		log.info("getProjectByProjectId Method");
		DtoProjectSetup dtoProjectSetup = new DtoProjectSetup();
		try {
			if (id > 0) {
				ProjectSetup projectSetup = repositoryProjectSetup.findByIdAndIsDeleted(id, false);
				if (projectSetup != null) {
					dtoProjectSetup = new DtoProjectSetup(projectSetup);
					
				} else {
					dtoProjectSetup.setMessageType("PROJECT_NOT_GETTING");

				}
			} else {
				dtoProjectSetup.setMessageType("INVALID_DEPARTMENT_ID");

			}
			log.debug("Department By Id is:" + dtoProjectSetup.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoProjectSetup;
	}


	public DtoProjectSetup deleteDivision(List<Integer> ids) {
		log.info("deleteProjectSetup Method");
		DtoProjectSetup dtoProjectSetup = new DtoProjectSetup();
		try {
			
			dtoProjectSetup.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PROJECT_SETUP_DELETED", false));
			dtoProjectSetup.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PROJECT_ASSOCIATED_ASSOCIATED", false));
			boolean inValidDelete = false;
			StringBuilder invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse
					.getMessageByShortAndIsDeleted("PROJECT_SETUP_NOT_DELETE_ID_MESSAGE", false).getMessage());
			List<DtoProjectSetup> deleteProjectSetup = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			try {
				for (Integer projectId : ids) {
					ProjectSetup projectSetup = repositoryProjectSetup.findOne(projectId);
					if (projectSetup.getEmployeeMaster().isEmpty()) {
//						&& projectSetup.getListEmployeePositionHistory().isEmpty()&&projectSetup.getRequisitions().isEmpty(){
						DtoProjectSetup dtoProjectSetup2 = new DtoProjectSetup();
						dtoProjectSetup2.setId(projectId);
						dtoProjectSetup2.setProjectArabicDescription(projectSetup.getProjectArabicDescription());
						dtoProjectSetup2.setProjectArabicName(projectSetup.getProjectArabicName());
						dtoProjectSetup2.setProjectId(projectSetup.getProjectId());
						dtoProjectSetup2.setProjectName(projectSetup.getProjectName());
						dtoProjectSetup2.setProjectDescription(projectSetup.getProjectDescription());
						repositoryProjectSetup.deleteSingleProjectSetup(true, loggedInUserId, projectId);
						deleteProjectSetup.add(dtoProjectSetup2);
					} else {
						inValidDelete = true;
					}
				}
				if (inValidDelete) {
					dtoProjectSetup.setMessageType(invlidDeleteMessage.toString());
				}
				if (!inValidDelete) {
					dtoProjectSetup.setMessageType("");
				}
				dtoProjectSetup.setDeleteProjectSetup(deleteProjectSetup);
			} catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete Project Setup :"+dtoProjectSetup.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoProjectSetup;
	}
}
