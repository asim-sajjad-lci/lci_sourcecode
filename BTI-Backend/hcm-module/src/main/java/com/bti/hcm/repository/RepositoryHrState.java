package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bti.hcm.model.HrState;

public interface RepositoryHrState extends JpaRepository<HrState, Integer>{
	@Query("select c from HrState c where (c.stateName like %:searchKeyWord% ) and c.isDeleted=false")
	public List<HrState> predictiveCitySearch(@Param("searchKeyWord") String searchKeyWord);
	
	
	List<HrState> findByIsDeletedAndLanguageLanguageId(boolean deleted, int langId);
	
	@Query("select p from HrState p where (p.countryMaster.countryId =:countryMaster) and p.isDeleted=false")
	List<HrState> findByCountryId(@Param("countryMaster") Integer countryId);

	@Query("select count(*) from HrState d where d.isDeleted=false")
	public Integer getCountOfTotalState();
	
	@Query("select p from HrState p where (p.countryMaster.countryId =:countryMaster) and p.language.id =:langId and p.isDeleted=false")
	List<HrState> findByStateIdByCountryLangId(@Param("countryMaster") Integer countryId,@Param("langId")int langId);
}
