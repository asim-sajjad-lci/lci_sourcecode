package com.bti.hcm.model.dto;


import java.util.Date;
import java.util.List;

import com.bti.hcm.model.PositionSetup;
import com.bti.hcm.util.UtilRandomKey;

public class DtoPositionSetup extends DtoBase{

	private Integer id;
	private String positionIds;
	private Integer positionId;
	private Integer ducumentAttachmentSecq;
	private String fileType;
	private String documentAttachmentName;
	private com.bti.hcm.model.Attachment postAttachmentId;
	private byte[] attachment;
	private String documentAttachmenDesc;
	private short attachmentType;
	private Date attachmentDate;
	private Date attachmentTime;
	private String fileName;
	protected Integer userId;
	private List<DtoPositionSetup> deletePositionSetup;
	public Integer getId() {
		return id;
	}
	
	

	public Integer getPositionId() {
		return positionId;
	}



	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}



	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getDucumentAttachmentSecq() {
		return ducumentAttachmentSecq;
	}
	public void setDucumentAttachmentSecq(Integer ducumentAttachmentSecq) {
		this.ducumentAttachmentSecq = ducumentAttachmentSecq;
	}
	public String getDocumentAttachmentName() {
		return documentAttachmentName;
	}
	public void setDocumentAttachmentName(String documentAttachmentName) {
		this.documentAttachmentName = documentAttachmentName;
	}
	
	public String getDocumentAttachmenDesc() {
		return documentAttachmenDesc;
	}
	public void setDocumentAttachmenDesc(String documentAttachmenDesc) {
		this.documentAttachmenDesc = documentAttachmenDesc;
	}
	public short getAttachmentType() {
		return attachmentType;
	}
	public void setAttachmentType(short s) {
		this.attachmentType = s;
	}
	public Date getAttachmentDate() {
		return attachmentDate;
	}
	public void setAttachmentDate(Date attachmentDate) {
		this.attachmentDate = attachmentDate;
	}
	public Date getAttachmentTime() {
		return attachmentTime;
	}
	public void setAttachmentTime(java.util.Date date) {
		this.attachmentTime = date;
	}
	
	public List<DtoPositionSetup> getDeletePositionSetup() {
		return deletePositionSetup;
	}
	public void setDeletePositionSetup(List<DtoPositionSetup> deletePositionSetup) {
		this.deletePositionSetup = deletePositionSetup;
	}
	
	public DtoPositionSetup() {
	
	}
	
	public DtoPositionSetup(PositionSetup positionSetup) {
		this.id = positionSetup.getId();
		
		if (UtilRandomKey.isNotBlank(positionSetup.getDocumentAttachmenDesc())) {
			this.documentAttachmenDesc = positionSetup.getDocumentAttachmenDesc();
		} else {
			this.documentAttachmenDesc = "";
		}
		if (UtilRandomKey.isNotBlank(positionSetup.getDocumentAttachmentName())) {
			this.documentAttachmentName = positionSetup.getDocumentAttachmentName();
		} else {
			this.documentAttachmentName = "";
		}
	}

	
	
	public com.bti.hcm.model.Attachment getPostAttachmentId() {
		return postAttachmentId;
	}

	public void setPostAttachmentId(com.bti.hcm.model.Attachment postAttachmentId) {
		this.postAttachmentId = postAttachmentId;
	}

	public byte[] getAttachment() {
		return attachment;
	}

	public void setAttachment(byte[] attachment) {
		this.attachment = attachment;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPositionIds() {
		return positionIds;
	}

	public void setPositionIds(String positionIds) {
		this.positionIds = positionIds;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	
	
}
