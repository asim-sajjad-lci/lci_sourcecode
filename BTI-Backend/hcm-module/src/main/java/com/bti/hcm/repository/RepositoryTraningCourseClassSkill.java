package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TraningCourseClassSkill;

@Repository("repositoryTraningCourseClassSkill")
public interface RepositoryTraningCourseClassSkill extends JpaRepository<TraningCourseClassSkill, Integer>{

	TraningCourseClassSkill findByIdAndIsDeleted(Integer id, boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TraningCourseClassSkill t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	public void deleteSingleTraningCourseClassSkill(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	
	
	@Query("select count(*) from TraningCourseClassSkill t where (t.skillDesc LIKE :searchKeyWord or t.skillArabicDesc LIKE :searchKeyWord or "
			+ " t.trainingCourse.traningId Like :searchKeyWord or t.trainingCourse.desc LIKE :searchKeyWord or t.trainingCourse.arbicDesc LIKE :searchKeyWord or t.trainingClass.classId LIKE :searchKeyWord or t.trainingClass.className LIKE :searchKeyWord) and t.isDeleted=false")
	Integer predictiveTraningCourseClassSkillSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);

	
	@Query("select t from TraningCourseClassSkill t where (t.skillDesc LIKE :searchKeyWord or t.skillArabicDesc LIKE :searchKeyWord or "
			+ " t.trainingCourse.traningId Like :searchKeyWord or t.trainingCourse.desc LIKE :searchKeyWord or t.trainingCourse.arbicDesc LIKE :searchKeyWord or t.trainingClass.classId LIKE :searchKeyWord or t.trainingClass.className LIKE :searchKeyWord) and t.isDeleted=false")
	List<TraningCourseClassSkill> predictiveTraningCourseClassSkillSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select t from TraningCourse t where (t.traningId =:traningId) and t.isDeleted=false")
	List<TraningCourseClassSkill> findByTraningCourseClassSkillId(@Param("traningId")String traningId);

	@Query("select count(*) from TraningCourseClassSkill t ")
	public Integer getCountOfTotalTraningCourseClassSkill();
}
