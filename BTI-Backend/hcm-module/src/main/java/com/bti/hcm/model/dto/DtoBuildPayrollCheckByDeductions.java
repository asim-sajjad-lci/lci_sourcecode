package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class DtoBuildPayrollCheckByDeductions extends DtoBase{

	private Integer id;
	private DtoBuildChecks buildChecks;
	private List<DtoDeductionCode> deductionCode;
	private DtoDeductionCode deductionCodess;
	private BigDecimal amount;
	
	private String diductionId;
	private String discription;

	private List<DtoBuildPayrollCheckByDeductions> listBuildChecks;

	private List<DtoBuildPayrollCheckByDeductions> delete;

	private Date startDate;
	private Date endDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DtoBuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(DtoBuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}

	public List<DtoDeductionCode> getDeductionCode() {
		return deductionCode;
	}

	public void setDeductionCode(List<DtoDeductionCode> deductionCode) {
		this.deductionCode = deductionCode;
	}


	public List<DtoBuildPayrollCheckByDeductions> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoBuildPayrollCheckByDeductions> delete) {
		this.delete = delete;
	}


	public List<DtoBuildPayrollCheckByDeductions> getListBuildChecks() {
		return listBuildChecks;
	}

	public void setListBuildChecks(List<DtoBuildPayrollCheckByDeductions> listBuildChecks) {
		this.listBuildChecks = listBuildChecks;
	}

	public String getDiductionId() {
		return diductionId;
	}

	public void setDiductionId(String diductionId) {
		this.diductionId = diductionId;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public DtoDeductionCode getDeductionCodess() {
		return deductionCodess;
	}

	public void setDeductionCodess(DtoDeductionCode deductionCodess) {
		this.deductionCodess = deductionCodess;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	

}
