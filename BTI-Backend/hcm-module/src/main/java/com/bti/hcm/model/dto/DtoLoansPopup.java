package com.bti.hcm.model.dto;

import java.util.Date;

/**
 * @author HAMID
 */
public class DtoLoansPopup extends DtoBase { /* hybrid of Loan n LoanDetails */

	
	private Integer loanId; // 1 //unused for Displaying purpose
	private String refId; // LN1001 //Loan

	private Integer loanDetailsId; // unused for Displaying purpose
	private Date monthYear; // LoanDeatils
	private Float deductionAmount; // LoanDetails

	private Boolean isPostpone;
	private Boolean isPaid;

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public Integer getLoanDetailsId() {
		return loanDetailsId;
	}

	public void setLoanDetailsId(Integer loanDetailsId) {
		this.loanDetailsId = loanDetailsId;
	}

	public Date getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(Date monthYear) {
		this.monthYear = monthYear;
	}

	public Float getDeductionAmount() {
		return deductionAmount;
	}

	public void setDeductionAmount(Float deductionAmount) {
		this.deductionAmount = deductionAmount;
	}

	public Boolean getIsPostpone() {
		return isPostpone;
	}

	public void setIsPostpone(Boolean isPostpone) {
		this.isPostpone = isPostpone;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

}
