package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40500",indexes = {
        @Index(columnList = "ORNTINDX")
})
public class OrientationSetup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORNTINDX")
	private Integer id;

	@Column(name = "ORNTID", columnDefinition = "char(15)")
	private String orientationId;

	@Column(name = "ORNTDSCR", columnDefinition = "char(31)")
	private String orientationDesc;

	@Column(name = "ORNTDSCRA", columnDefinition = "char(61)")
	private String orientationArbicDesc;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "orientationSetups")
	@Where(clause = "is_deleted = false")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<OrientationSetupDetail> listOrientationSetupDetail;
	
	/*@ManyToOne
	@JoinColumn(name = "ORNTINDXD")
	private OrientationSetupDetail orientationSetupDetail;*/
	
	@ManyToOne
	@JoinColumn(name = "ORNTCHKINDX")
	private OrientationCheckListSetup orientationCheckListSetup;

	@ManyToOne
	@JoinColumn(name = "PRCHKINDX")
	private OrientationPredefinedCheckListItem orientationPredefinedCheckListItem;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrientationId() {
		return orientationId;
	}

	public void setOrientationId(String orientationId) {
		this.orientationId = orientationId;
	}

	public String getOrientationDesc() {
		return orientationDesc;
	}

	public void setOrientationDesc(String orientationDesc) {
		this.orientationDesc = orientationDesc;
	}

	public String getOrientationArbicDesc() {
		return orientationArbicDesc;
	}

	public void setOrientationArbicDesc(String orientationArbicDesc) {
		this.orientationArbicDesc = orientationArbicDesc;
	}

	public OrientationPredefinedCheckListItem getOrientationPredefinedCheckListItem() {
		return orientationPredefinedCheckListItem;
	}

	public void setOrientationPredefinedCheckListItem(
			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem) {
		this.orientationPredefinedCheckListItem = orientationPredefinedCheckListItem;
	}

	public OrientationCheckListSetup getOrientationCheckListSetup() {
		return orientationCheckListSetup;
	}

	public void setOrientationCheckListSetup(OrientationCheckListSetup orientationCheckListSetup) {
		this.orientationCheckListSetup = orientationCheckListSetup;
	}

	public List<OrientationSetupDetail> getListOrientationSetupDetail() {
		return listOrientationSetupDetail;
	}

	public void setListOrientationSetupDetail(List<OrientationSetupDetail> listOrientationSetupDetail) {
		this.listOrientationSetupDetail = listOrientationSetupDetail;
	}

	/*public OrientationSetupDetail getOrientationSetupDetail() {
		return orientationSetupDetail;
	}

	public void setOrientationSetupDetail(OrientationSetupDetail orientationSetupDetail) {
		this.orientationSetupDetail = orientationSetupDetail;
	}*/

}
