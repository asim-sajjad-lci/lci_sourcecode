package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40233",indexes = {
        @Index(columnList = "ACCSCHDINX")
})
public class AccrualScheduleDetail extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACCSCHDINX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "ACCSCHINDX")
	private AccrualSchedule accrualSchedule;

	@ManyToOne
	@JoinColumn(name = "ACCRINDXID")
	private Accrual accrual;

	@ManyToOne
	@JoinColumn(name = "ACCBYINDX")
	private AccrualType accrualType;

	@Column(name = "ACCSCHSEQN")
	private Integer scheduleSequence;

	@Column(name = "ACCSCHSENT")
	private Integer scheduleSeniority;

	@Column(name = "ACCRDSCR", columnDefinition = "char(31)")
	private String description;

	@Column(name = "NUMOFACCR")
	private Integer hours;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getScheduleSequence() {
		return scheduleSequence;
	}

	public void setScheduleSequence(Integer scheduleSequence) {
		this.scheduleSequence = scheduleSequence;
	}

	public Integer getScheduleSeniority() {
		return scheduleSeniority;
	}

	public void setScheduleSeniority(Integer scheduleSeniority) {
		this.scheduleSeniority = scheduleSeniority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public AccrualSchedule getAccrualSchedule() {
		return accrualSchedule;
	}

	public void setAccrualSchedule(AccrualSchedule accrualSchedule) {
		this.accrualSchedule = accrualSchedule;
	}

	public Accrual getAccrual() {
		return accrual;
	}

	public void setAccrual(Accrual accrual) {
		this.accrual = accrual;
	}

	public AccrualType getAccrualType() {
		return accrualType;
	}

	public void setAccrualType(AccrualType accrualType) {
		this.accrualType = accrualType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accrual == null) ? 0 : accrual.hashCode());
		result = prime * result + ((accrualSchedule == null) ? 0 : accrualSchedule.hashCode());
		result = prime * result + ((accrualType == null) ? 0 : accrualType.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((hours == null) ? 0 : hours.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((scheduleSeniority == null) ? 0 : scheduleSeniority.hashCode());
		result = prime * result + ((scheduleSequence == null) ? 0 : scheduleSequence.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}