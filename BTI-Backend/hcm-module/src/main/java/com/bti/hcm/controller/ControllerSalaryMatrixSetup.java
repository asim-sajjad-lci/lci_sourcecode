package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSalaryMatrixSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceSalaryMatrixSetup;

@RestController
@RequestMapping("/salaryMatrixSetup")
public class ControllerSalaryMatrixSetup extends BaseController{

	/**
	 * @Description LOGGER use for put a logger in SalaryMatrixSetup Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerSalaryMatrixSetup.class);
	
	/**
	 * @Description serviceSalaryMatrixSetup Autowired here using annotation of spring for use of serviceSalaryMatrixSetup method in this controller
	 */
	@Autowired(required=true)
	ServiceSalaryMatrixSetup serviceSalaryMatrixSetup;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @param request
	 * {
			 "salaryMatrixId":"83",
			 "description":"This is sample Description",
			 "arabicSalaryMatrixDescription":"This is arabic Salary Descriotion",
			 "payUnit":123,
			 "totalRow":2,
			 "totalColumns":2,
			 "totalRowValues":0,
			 "listSalaryMatrixRow":[{
			    "desc":"This is Salary Matrix Row 1",
			    "arbic":"This is arabic Salary Matrix 1",
			   "listSalaryMatrixRowValue":[{
			    "sequence":1,
			    "amount":10.00
			
			 },
			 {
			    "sequence":2,
			    "amount":20.00
			
			 }]
			
			 },
			 {
			    "desc":"This is Salary Matrix Row 2",
			    "arbic":"This is arabic Salary Matrix 2",
			   "listSalaryMatrixRowValue":[{
			    "sequence":1,
			    "amount":10.00
			
			 },
			 {
			    "sequence":2,
			    "amount":20.00
			
			 }]
			
			 }],
			
			 "listSalaryMatrixColSetup":[{
			   "colSalaryMatrixDescription":"colSalary 1",
			   "arabicColSalaryMatrixDescription":"arabicC1"
			 },
			 {
			   "colSalaryMatrixDescription":"colSalary 2",
			   "arabicColSalaryMatrixDescription":"arabicColSa2"
			 }]
			
			}
	 * @param dtoSalaryMatrixSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoSalaryMatrixSetup dtoSalaryMatrixSetup) throws Exception {
		LOGGER.info("Create SalaryMatrix Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSalaryMatrixSetup = serviceSalaryMatrixSetup.saveOrUpdate(dtoSalaryMatrixSetup);
			responseMessage=displayMessage(dtoSalaryMatrixSetup, "SALARY_MATRIX_CREATED", "SALARY_MATRIX_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create SalaryMatrix Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	* @param request{
		  "id" : 1,
		 "salaryMatrixId":"1",
		  "description":"1",
		  "arabicSalaryMatrixDescription":"1",
		  "totalRow":1,
		  "totalColumns":1,
		  "totalRowValues":1
	}
	 * @param dtoSalaryMatrixSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoSalaryMatrixSetup dtoSalaryMatrixSetup) throws Exception {
		LOGGER.info("Update SalaryMatrix Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSalaryMatrixSetup = serviceSalaryMatrixSetup.saveOrUpdate(dtoSalaryMatrixSetup);
			responseMessage=displayMessage(dtoSalaryMatrixSetup, "SALARY_MATRIX_UPDATED_SUCCESS", MessageConstant.SALARY_MATRIX_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update SalaryMatrix Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param request{  
		 	"ids": [1]
	}
	 * @param dtoSalaryMatrixSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody  DtoSalaryMatrixSetup dtoSalaryMatrixSetup) throws Exception {
		LOGGER.info("Delete SalaryMatrixSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSalaryMatrixSetup.getIds() != null && !dtoSalaryMatrixSetup.getIds().isEmpty()) {
				DtoSalaryMatrixSetup dtoSalaryMatrixSetup2 = serviceSalaryMatrixSetup.delete(dtoSalaryMatrixSetup.getIds());
				if(dtoSalaryMatrixSetup2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("SALARY_MATRIX_DELETED", false), dtoSalaryMatrixSetup2);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("SALARY_MATRIX_NOT_DELETED", false), dtoSalaryMatrixSetup2);

				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete SalaryMatrixSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param dtoSearch
	 * @param request{  
		 	"searchKeyword":"test"
	}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search SalaryMatrixSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSalaryMatrixSetup.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SALARY_MATRIX_GET_ALL", "SALARY_MATRIX_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search SalaryMatrixSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
		/**
		 * 
		 * @param request
		 * @param dtoSearch
		 * @return
		 * @throws Exception
		 * @request
		 	{
			  "searchKeyword":"t",
			  "sortOn":"",
			  "sortBy":"DESC",
			 "pageNumber":"0",
			  "pageSize":"10"
			}
		 */
		@RequestMapping(value = "/getAll", method = RequestMethod.POST)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) throws Exception {
		LOGGER.info("Get All SalaryMatrixSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			 dtoSearch = serviceSalaryMatrixSetup.search(dtoSearch);
			 responseMessage=displayMessage(dtoSearch, "SALARY_MATRIX_GET_ALL", "SALARY_MATRIX_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All SalaryMatrixSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	/**
	 * @param request{  
		 "id" :1
		}
	 * @param dtoSalaryMatrixSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getSalaryMatrixSetupId", method = RequestMethod.POST)
	public ResponseMessage getSalaryMatrixSetupId(HttpServletRequest request, @RequestBody DtoSalaryMatrixSetup dtoSalaryMatrixSetup) throws Exception {
		LOGGER.info("Get SalaryMatrixSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSalaryMatrixSetup dtoAccrualObj = serviceSalaryMatrixSetup.getById(dtoSalaryMatrixSetup.getId());
			responseMessage=displayMessage(dtoAccrualObj, "SALARY_MATRIX_LIST_GET_DETAIL", "SALARY_MATRIX_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get SalaryMatrixSetup by Id Method:"+dtoSalaryMatrixSetup.getId());
		return responseMessage;
	}
	
	/**
	 * @param request{  
		 "salaryMatrixId":"1"
		}
	 * @param dtoSalaryMatrixSetup1
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/salaryMatrixSetupIdcheck", method = RequestMethod.POST)
	public ResponseMessage salaryMatrixSetupIdcheck(HttpServletRequest request, @RequestBody DtoSalaryMatrixSetup dtoSalaryMatrixSetup1) throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSalaryMatrixSetup dtoSalaryMatrixSetup = serviceSalaryMatrixSetup.repeatBySalaryMatrixSetupId(dtoSalaryMatrixSetup1.getSalaryMatrixId());
			if (dtoSalaryMatrixSetup != null) {
				if (dtoSalaryMatrixSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("SALARY_MATRIX_RESULT", false), dtoSalaryMatrixSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoSalaryMatrixSetup.getMessageType(), false),
							dtoSalaryMatrixSetup);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("SALARY_MATRIX_REPEAT_SALARY_MATRIX_ID_NOT_FOUND", false), dtoSalaryMatrixSetup);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/searchSalaryMatrixId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchSalaryMatrixId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("searchSalaryMatrixId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSalaryMatrixSetup.searchSalaryMatrixId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SALARY_MATRIX_SETUP_GET_ALL", "SALARY_MATRIX_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search SalaryMatrixSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
}
