package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.HrState;
import com.bti.hcm.model.dto.DtoHrState;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryHrState;

@Service("serviceHrState")
public class ServiceHrState {

	static Logger log = Logger.getLogger(ServiceHrState.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryHrState repositoryHrCountry;
	
	
	public DtoSearch searchState(DtoSearch dtoSearch){
		log.info("searchState  Method");
		if(dtoSearch != null){
			List<HrState> stateList = this.repositoryHrCountry.predictiveCitySearch(dtoSearch.getSearchKeyword());
			if(stateList != null && !stateList.isEmpty()){
				List<DtoHrState> dtoCityList = new ArrayList<>();
				for (HrState hrCity : stateList) {
					DtoHrState dtoCity = new DtoHrState(hrCity);
					dtoCityList.add(dtoCity);
				}
				dtoSearch.setRecords(dtoCityList);
			}
		}
		return dtoSearch;
	}
	
	/**
	 * @description get all State list in dropdown
	 * @return
	 */
	public List<DtoHrState> getStateListWithLanguage() {
		log.info("getStateListWithLanguage  Method");
		String langId = httpServletRequest.getHeader("langid");
		List<DtoHrState> stateList = new ArrayList<>();
//		List<HrState> list = repositoryHrCountry.findByIsDeletedAndLanguageLanguageId(false, Integer.parseInt(langId));
		List<HrState> list = repositoryHrCountry.findByIsDeletedAndLanguageLanguageId(false, 1);
		if(list.isEmpty()){
			list = repositoryHrCountry.findByIsDeletedAndLanguageLanguageId(false, 1);
		}
		if (list != null && !list.isEmpty()) {
			for (HrState hrState : list) {
				DtoHrState dtoState = new DtoHrState();
				dtoState.setStateId(hrState.getStateId());
				dtoState.setStateCode(hrState.getStateCode());
				dtoState.setStateName(hrState.getStateName());
				
				stateList.add(dtoState);
			}
		}
		return stateList;
	}
	
	public List<DtoHrState> getStateListBaseOnCountry(DtoHrState dtoHrState) {
		log.info("getStateListWithLanguage  Method");
		String langId = httpServletRequest.getHeader("langid");
		List<DtoHrState> stateList = new ArrayList<>();
//		List<HrState> list = repositoryHrCountry.findByStateIdByCountryLangId(dtoHrState.getCountryId(),Integer.valueOf(langId));
		List<HrState> list = repositoryHrCountry.findByStateIdByCountryLangId(dtoHrState.getCountryId(), 1);
		if (list != null && !list.isEmpty()) {
			for (HrState hrState : list) {
				DtoHrState dtoState = new DtoHrState();
				dtoState.setStateId(hrState.getStateId());
				dtoState.setStateCode(hrState.getStateCode());
				dtoState.setStateName(hrState.getStateName());
				hrState.getCountryMaster().getCountryName();
				stateList.add(dtoState);
			}
		}
		log.debug("City is:"+stateList.size());
		return stateList;
	}
	
	public List<DtoHrState> getStateByCountryByLangId(DtoHrState dtoHrState) {
		log.info("getStateListWithLanguage  Method");
		List<DtoHrState> stateList = new ArrayList<>();
		List<HrState> list = repositoryHrCountry.findByCountryId(dtoHrState.getCountryId());
		if(list.isEmpty()){
			list = repositoryHrCountry.findByIsDeletedAndLanguageLanguageId(false, 1);
		}
		if (list != null && !list.isEmpty()) {
			for (HrState hrState : list) {
				DtoHrState dtoState = new DtoHrState();
				dtoState.setStateId(hrState.getStateId());
				dtoState.setStateCode(hrState.getStateCode());
				dtoState.setStateName(hrState.getStateName());
				
				stateList.add(dtoState);
			}
		}
		log.debug("City is:"+stateList.size());
		return stateList;
	}
}
