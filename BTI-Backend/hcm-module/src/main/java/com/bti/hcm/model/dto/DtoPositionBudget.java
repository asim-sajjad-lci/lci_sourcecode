package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.bti.hcm.model.PositionBudget;
import com.bti.hcm.model.PositionPalnSetup;
import com.bti.hcm.util.UtilRandomKey;

public class DtoPositionBudget extends DtoBase{
	
	private Integer id;
	private PositionPalnSetup positionPalnSetup;
	private Integer budgetScheduleSeqn;
	private String positionPlanId;
	private Integer  positionPlanPrimaryId;
	private Date budgetScheduleStart;
	private Date budgetScheduleEnd;
	private String budgetArbicDesc;
	private String budgetDesc;
	private String description;
	private BigDecimal budgetAmount;
	private String positionPlanDesc;
	private List<DtoPositionBudget> deletePosition;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public PositionPalnSetup getPositionPalnSetup() {
		return positionPalnSetup;
	}
	public void setPositionPalnSetup(PositionPalnSetup positionPalnSetup) {
		this.positionPalnSetup = positionPalnSetup;
	}
	public Integer getBudgetScheduleSeqn() {
		return budgetScheduleSeqn;
	}
	public void setBudgetScheduleSeqn(Integer budgetScheduleSeqn) {
		this.budgetScheduleSeqn = budgetScheduleSeqn;
	}
	public Date getBudgetScheduleStart() {
		return budgetScheduleStart;
	}
	public void setBudgetScheduleStart(Date budgetScheduleStart) {
		this.budgetScheduleStart = budgetScheduleStart;
	}
	public Date getBudgetScheduleEnd() {
		return budgetScheduleEnd;
	}
	public void setBudgetScheduleEnd(Date budgetScheduleEnd) {
		this.budgetScheduleEnd = budgetScheduleEnd;
	}
	public String getBudgetDesc() {
		return budgetDesc;
	}
	public void setBudgetDesc(String budgetDesc) {
		this.budgetDesc = budgetDesc;
	}
	
	
	public BigDecimal getBudgetAmount() {
		return budgetAmount;
	}
	public void setBudgetAmount(BigDecimal budgetAmount) {
		this.budgetAmount = budgetAmount;
	}
	
	public List<DtoPositionBudget> getDeletePosition() {
		return deletePosition;
	}
	public void setDeletePosition(List<DtoPositionBudget> deletePosition) {
		this.deletePosition = deletePosition;
	}
	
	public DtoPositionBudget() {
	
	}
	
	public String getPositionPlanDesc() {
		return positionPlanDesc;
	}
	public void setPositionPlanDesc(String positionPlanDesc) {
		this.positionPlanDesc = positionPlanDesc;
	}
	
	
	public String getPositionPlanId() {
		return positionPlanId;
	}
	public void setPositionPlanId(String positionPlanId) {
		this.positionPlanId = positionPlanId;
	}
	public DtoPositionBudget(PositionBudget positionBudget) {
		
		
		if (UtilRandomKey.isNotBlank(positionBudget.getBudgetDesc())) {
			this.budgetDesc = positionBudget.getBudgetDesc();
		} else {
			this.budgetDesc = "";
		}
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBudgetArbicDesc() {
		return budgetArbicDesc;
	}
	public void setBudgetArbicDesc(String budgetArbicDesc) {
		this.budgetArbicDesc = budgetArbicDesc;
	}
	public Integer getPositionPlanPrimaryId() {
		return positionPlanPrimaryId;
	}
	public void setPositionPlanPrimaryId(Integer positionPlanPrimaryId) {
		this.positionPlanPrimaryId = positionPlanPrimaryId;
	}
	
	
	
}
