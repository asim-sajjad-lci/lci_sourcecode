/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Location;
/**
 * Description: Interface for Location 
 * Name of Project: Hcm
 * Version: 0.0.1
 */

@Repository("repositoryLocation")
public interface RepositoryLocation extends JpaRepository<Location, Integer> {
	
	public Location findByIdAndIsDeleted(int id, boolean deleted);
	
	public List<Location> findByIsDeleted(Boolean deleted);
	
	@Query("select count(*) from Location d where d.isDeleted=false")
	public Integer getCountOfTotalLocation();
	
	public List<Location> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Location d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteLocation(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Location d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleLocation(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	@Query("select d from Location d where (d.locationId LIKE :searchKeyWord or d.description LIKE :searchKeyWord or d.contactName LIKE :searchKeyWord or d.city.cityName LIKE :searchKeyWord or d.country.countryName LIKE :searchKeyWord or d.state.stateName LIKE :searchKeyWord or d.phone LIKE :searchKeyWord or  d.locationAddress LIKE :searchKeyWord or  d.country LIKE :searchKeyWord or  d.fax LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord) and d.isDeleted=false")
	public List<Location> predictiveLocationSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	public List<Location> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	@Query("select count(*) from Location d where (d.locationId LIKE :searchKeyWord or d.description LIKE :searchKeyWord or d.contactName LIKE :searchKeyWord or d.city.cityName LIKE :searchKeyWord or d.country.countryName LIKE :searchKeyWord or d.state.stateName LIKE :searchKeyWord or d.phone LIKE :searchKeyWord or  d.locationAddress LIKE :searchKeyWord or  d.country LIKE :searchKeyWord or  d.fax LIKE :searchKeyWord or d.arabicDescription LIKE :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveLocationSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select p from Location p where (p.locationId =:locationId) and p.isDeleted=false")
	public List<Location> findByLocationId(@Param("locationId")String locationId);

	@Query("select d.locationId from Location d where (d.locationId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<String> predictiveLocationIdSearchWithPagination(@Param("searchKeyWord") String string);

	 @Query("select d from Location d where (d.locationId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<Location> predictiveSearchAllLocationIdWithPagination(@Param("searchKeyWord")String locationId);

	 @Query("select count(*) from Location l ")
	public Integer getCountOfTotaLocation();

}
