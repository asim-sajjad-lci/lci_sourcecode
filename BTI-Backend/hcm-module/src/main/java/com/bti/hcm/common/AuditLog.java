package com.bti.hcm.common;

public interface AuditLog {
	
	public static final String CREATE_OR_UPDATE="CREATE_OR_UPDATE";
	
	
	public static final String DELETE="DELETE";
	public static final String SEARCH_GETALL="SEARCH_GETALL";
	public static final String GET_BY_SPECIFIC_ID="GET_BY_SPECIFIC_ID";
	public static final String DUPLICATE_ID_CHECK="DUPLICATE_ID_CHECK";
	public static final String GET_BY_ID="GET_BY_ID";
	public static final String SET_VALUE_IN_DROP_DOWN="SET_VALUE_IN_DROP_DOWN";
	
	
	public static final String EXCEPTION_CREATE="EXCEPTION_CREATE";
	public static final String EXCEPTION_SEARCH_GET_ALL="EXCEPTION_SEARCH_GET_ALL";
	public static final String EXCEPTION_GET_BY_SPECIFIC_ID="EXCEPTION_GET_BY_SPECIFIC_ID";
	public static final String EXCEPTION_IN_DELETE="EXCEPTION_IN_DELETE";
	public static final String EXCEPTION_DUPLICATE_ID_CHECK="EXCEPTION_DUPLICATE_ID_CHECK";
	public static final String EXCEPTION_SET_VALUE_IN_DROP_DOWN="EXCEPTION_SET_VALUE_IN_DROP_DOWN";
}
