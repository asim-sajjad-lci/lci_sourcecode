package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Accrual;
import com.bti.hcm.model.AccrualSchedule;
import com.bti.hcm.model.AccrualScheduleDetail;
import com.bti.hcm.model.AccrualType;
import com.bti.hcm.model.TimeCode;
import com.bti.hcm.model.dto.DtoAccrualSchedule;
import com.bti.hcm.model.dto.DtoAccrualScheduleDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTimeCode;
import com.bti.hcm.repository.RepositoryAccrual;
import com.bti.hcm.repository.RepositoryAccrualSchedule;
import com.bti.hcm.repository.RepositoryAccrualScheduleDetail;
import com.bti.hcm.repository.RepositoryAccrualType;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service AccrualSchedule Project: Hcm Version: 0.0.1
 */

@Service("serviceAccrualSchedule")
public class ServiceAccrualSchedule {
	static Logger log = Logger.getLogger(ServiceAccrualSchedule.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	ServiceAccrual serviceAccrual;

	@Autowired
	RepositoryAccrualSchedule repositoryAccrualSchedule;

	@Autowired
	RepositoryAccrualScheduleDetail repositoryAccrualScheduleDetail;

	@Autowired
	RepositoryAccrual repositoryAccrual;

	@Autowired
	RepositoryAccrualType repositoryAccrualType;

	/**
	 * @param dtoAccrualSchedule
	 * @return
	 */
	public DtoAccrualSchedule saveOrUpdateAccrualSchedule(DtoAccrualSchedule dtoAccrualSchedule) {
                  try {
              		log.info("enter into save or update Accrual Schdule");
            		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
            		AccrualSchedule accrualSchedule = null;
            		if (dtoAccrualSchedule.getId() != null && dtoAccrualSchedule.getId() > 0) {
            			accrualSchedule = repositoryAccrualSchedule.findByIdAndIsDeleted(dtoAccrualSchedule.getId(), false);
            			accrualSchedule.setUpdatedBy(loggedInUserId);
            			accrualSchedule.setUpdatedDate(new Date());

            		} else {
            			accrualSchedule = new AccrualSchedule();
            			accrualSchedule.setCreatedDate(new Date());
            			Integer rowId = repositoryAccrualSchedule.getCountOfTotalAccrualSchedules();
        				Integer increment=0;
        				if(rowId!=0) {
        					increment= rowId+1;
        				}else {
        					increment=1;
        				}
        				accrualSchedule.setRowId(increment);

            		}
            		accrualSchedule.setDesc(dtoAccrualSchedule.getDesc());
            		accrualSchedule.setScheduleId(dtoAccrualSchedule.getScheduleId());
            		accrualSchedule.setArbicDesc(dtoAccrualSchedule.getArbicDesc());
            		accrualSchedule.setStartDate(dtoAccrualSchedule.getStartDate());
            		accrualSchedule.setEndDate(dtoAccrualSchedule.getEndDate());
            		accrualSchedule.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
            		repositoryAccrualSchedule.saveAndFlush(accrualSchedule);

            		if (dtoAccrualSchedule.getSubItems() != null && !dtoAccrualSchedule.getSubItems().isEmpty()) {
            			for (DtoAccrualScheduleDetail dtoAccrualScheduleDetail : dtoAccrualSchedule.getSubItems()) {
            				Accrual accrual = repositoryAccrual.findOne(dtoAccrualScheduleDetail.getAccrualSetupId());
            				AccrualType accrualType = repositoryAccrualType.findOne(dtoAccrualScheduleDetail.getAccrualTypeId());
            				if (dtoAccrualScheduleDetail.getId() != null && dtoAccrualScheduleDetail.getId() > 0) {
            					AccrualScheduleDetail accrualScheduleDetailUpdate = repositoryAccrualScheduleDetail
            							.findOne(dtoAccrualScheduleDetail.getId());
            					accrualScheduleDetailUpdate.setScheduleSeniority(dtoAccrualScheduleDetail.getScheduleSeniority());
            					accrualScheduleDetailUpdate.setDescription(dtoAccrualScheduleDetail.getDescription());
            					accrualScheduleDetailUpdate.setHours(dtoAccrualScheduleDetail.getHours());
            					repositoryAccrualScheduleDetail.saveAndFlush(accrualScheduleDetailUpdate);
            				} else {
            					AccrualScheduleDetail accrualScheduleDetail = new AccrualScheduleDetail();
            					accrualScheduleDetail.setDescription(dtoAccrualScheduleDetail.getDescription());
            					accrualScheduleDetail.setScheduleSeniority(dtoAccrualScheduleDetail.getScheduleSeniority());
            					accrualScheduleDetail.setHours(dtoAccrualScheduleDetail.getHours());
            					accrualScheduleDetail.setAccrual(accrual);
            					accrualScheduleDetail.setAccrualType(accrualType);
            					accrualScheduleDetail.setAccrualSchedule(accrualSchedule);
            					repositoryAccrualScheduleDetail.saveAndFlush(accrualScheduleDetail);
            				}
            			}
            		}
            		log.debug("AccrualSchedule is:" + dtoAccrualSchedule.getId());
            		
				} catch (Exception e) {
					log.error(e);
				}

		return dtoAccrualSchedule;
	}

	public DtoSearch getAllAccrualSchedule() {
		DtoAccrualSchedule dtoAccrualSchedule = new DtoAccrualSchedule();

		DtoSearch dtoSearch = new DtoSearch();

		try {
			dtoSearch.setPageNumber(dtoAccrualSchedule.getPageNumber());
			dtoSearch.setPageSize(dtoAccrualSchedule.getPageSize());
			dtoSearch.setTotalCount(repositoryAccrualSchedule.getCountOfTotalAccrualSchedule());
			List<AccrualSchedule> accrualScheduleList = null;

			accrualScheduleList = repositoryAccrualSchedule.findByIsDeletedOrderByCreatedDateDesc(false);

			List<AccrualSchedule> accrualScheduleListAdd = new ArrayList<>();
			if (!accrualScheduleListAdd.isEmpty() && accrualScheduleList!=null) {
				for (AccrualSchedule accrualSchedule : accrualScheduleList) {
					dtoAccrualSchedule = new DtoAccrualSchedule(accrualSchedule);
					dtoAccrualSchedule.setScheduleId(accrualSchedule.getScheduleId());
					dtoAccrualSchedule.setId(accrualSchedule.getId());
					accrualScheduleListAdd.add(accrualSchedule);
				}
				dtoSearch.setRecords(accrualScheduleListAdd);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoAccrualSchedule deleteAccrualSchedule(List<Integer> ids) {
		log.info("deleteAccrualSchedule Method");
		DtoAccrualSchedule dtoAccrualSchedule = new DtoAccrualSchedule();
		dtoAccrualSchedule.setDeleteMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUALSCHEDULE_DELETED", false));
		dtoAccrualSchedule.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUALSCHEDULE_ASSOCIATED", false));
		List<DtoAccrualSchedule> deleteAccrualSchedule = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage
				.append(serviceResponse.getMessageByShortAndIsDeleted("ACCRUAL_SCHEDULE_NOT_DELETE_ID_MESSAGE", false));

		try {
			for (Integer scheduleId : ids) {
				AccrualSchedule accrualSchedule = repositoryAccrualSchedule.findOne(scheduleId);
				if (accrualSchedule.getListAccrualScheduleDetail().isEmpty()
						&& accrualSchedule.getListTimeCode().isEmpty()) {
					DtoAccrualSchedule dtoAccrualSchedule2 = new DtoAccrualSchedule();
					dtoAccrualSchedule2.setId(scheduleId);
					dtoAccrualSchedule2.setDesc(accrualSchedule.getDesc());
					dtoAccrualSchedule2.setArbicDesc(accrualSchedule.getArbicDesc());
					dtoAccrualSchedule2.setStartDate(accrualSchedule.getStartDate());
					dtoAccrualSchedule2.setEndDate(accrualSchedule.getEndDate());
					repositoryAccrualSchedule.deleteSingleAccrualSchedule(true, loggedInUserId, scheduleId);
					deleteAccrualSchedule.add(dtoAccrualSchedule2);
				}else {
					inValidDelete = true;
				}

			}

			if (inValidDelete) {
				invlidDeleteMessage.replace(invlidDeleteMessage.length() - 1, invlidDeleteMessage.length(), "");
				dtoAccrualSchedule.setMessageType(invlidDeleteMessage.toString());

			}
			if (!inValidDelete) {
				dtoAccrualSchedule.setMessageType("Accrual Schedule Deleted Sucessfully");

			}

			dtoAccrualSchedule.setDelete(deleteAccrualSchedule);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete ACCRUALSCHEDULE :" + dtoAccrualSchedule.getId());
		return dtoAccrualSchedule;
	}

	public DtoAccrualSchedule getscheduleById(int id) {
		DtoAccrualSchedule dtoAccrualSchedule = new DtoAccrualSchedule();
		try {
			if (id > 0) {
				AccrualSchedule accrualSchedule = repositoryAccrualSchedule.findByIdAndIsDeleted(id, false);
				if (accrualSchedule != null) {
					dtoAccrualSchedule = new DtoAccrualSchedule(accrualSchedule);
				} else {
					dtoAccrualSchedule.setMessageType("SCHEDULE_NOT_GETTING");

				}
			} else {
				dtoAccrualSchedule.setMessageType("INVALID_SCHEDULE_ID");

			}
		} catch (Exception e) {
			log.error(e);
		}

		return dtoAccrualSchedule;
	}


	public DtoAccrualSchedule repeatByScheduleId(String scheduleId) {
		log.info("repeatByScheduleId Method");
		DtoAccrualSchedule dtoAccrualSchedule = new DtoAccrualSchedule();
		try {
			List<AccrualSchedule> accrualSchedule = repositoryAccrualSchedule.findByScheduleId(scheduleId);
			if (accrualSchedule != null && !accrualSchedule.isEmpty()) {
				dtoAccrualSchedule.setIsRepeat(true);
				dtoAccrualSchedule.setScheduleId(scheduleId);
			} else {
				dtoAccrualSchedule.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrualSchedule;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchAccrualShedule(DtoSearch dtoSearch) {

		try {
			log.info("search searchAccrualShedule Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";
				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

					if (dtoSearch.getSortOn().equals("scheduleId") || dtoSearch.getSortOn().equals("desc")
							|| dtoSearch.getSortOn().equals("arbicDesc")) {
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
					}

				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}

				dtoSearch.setTotalCount(
						this.repositoryAccrualSchedule.predictiveAccrualSheduleSearchTotalCount("%" + searchWord + "%"));
				List<AccrualSchedule> accrualScheduleList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						accrualScheduleList = this.repositoryAccrualSchedule.predictiveAccrualSheduleSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						accrualScheduleList = this.repositoryAccrualSchedule.predictiveAccrualSheduleSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						accrualScheduleList = this.repositoryAccrualSchedule.predictiveAccrualSheduleSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.DESC, condition));
					}

				}
				if (accrualScheduleList != null && !accrualScheduleList.isEmpty()) {
					List<DtoAccrualSchedule> dtoAccrualScheduleList = new ArrayList<>();
					DtoAccrualSchedule dtoAccrualSchedule = null;
					for (AccrualSchedule accrualSchedule : accrualScheduleList) {
						dtoAccrualSchedule = new DtoAccrualSchedule(accrualSchedule);

						List<AccrualScheduleDetail> list = accrualSchedule.getListAccrualScheduleDetail();
						List<TimeCode> timeCodeList = accrualSchedule.getListTimeCode();

						dtoAccrualSchedule.setId(accrualSchedule.getId());
						dtoAccrualSchedule.setScheduleId(accrualSchedule.getScheduleId());
						dtoAccrualSchedule.setDesc(accrualSchedule.getDesc());
						dtoAccrualSchedule.setArbicDesc(accrualSchedule.getArbicDesc());
						dtoAccrualSchedule.setStartDate(accrualSchedule.getStartDate());
						dtoAccrualSchedule.setEndDate(accrualSchedule.getEndDate());

						if (dtoAccrualSchedule.getSubItems() == null) {
							List<DtoAccrualScheduleDetail> listAccrualScheduleDetail = new ArrayList<>();
							dtoAccrualSchedule.setSubItems(listAccrualScheduleDetail);
						}

						if (dtoAccrualSchedule.getTimeCodeList() == null) {
							List<DtoTimeCode> listDtoTimeCode = new ArrayList<>();
							dtoAccrualSchedule.setTimeCodeList(listDtoTimeCode);
						}

						for (AccrualScheduleDetail accrualScheduleDetail : list) {
							if (!accrualScheduleDetail.getIsDeleted()) {
								DtoAccrualScheduleDetail dtoAccrualScheduleDetail = new DtoAccrualScheduleDetail();
								dtoAccrualScheduleDetail.setScheduleSeniority(accrualScheduleDetail.getScheduleSeniority());
								dtoAccrualScheduleDetail.setDescription(accrualScheduleDetail.getDescription());
								dtoAccrualScheduleDetail.setHours(accrualScheduleDetail.getHours());
								dtoAccrualScheduleDetail.setAccrualTypeId(accrualScheduleDetail.getAccrualType().getId());
								dtoAccrualScheduleDetail
										.setAccrualSetupId(accrualScheduleDetail.getAccrualSchedule().getId());
								dtoAccrualScheduleDetail.setId(accrualScheduleDetail.getId());
								dtoAccrualSchedule.getSubItems().add(dtoAccrualScheduleDetail);

							}
						}

						for (TimeCode timeCode : timeCodeList) {
							DtoTimeCode dtoTimeCode = new DtoTimeCode();
							dtoTimeCode.setId(timeCode.getId());
							dtoTimeCode.setDesc(timeCode.getDesc());
							dtoTimeCode.setArbicDesc(timeCode.getArbicDesc());
							dtoTimeCode.setAccrualPeriod(timeCode.getAccrualPeriod());
							dtoTimeCode.setInActive(timeCode.isInActive());
							dtoAccrualSchedule.getTimeCodeList().add(dtoTimeCode);
						}
						dtoAccrualScheduleList.add(dtoAccrualSchedule);
					}

					dtoSearch.setRecords(dtoAccrualScheduleList);
				}

			}

			log.debug("Search searchPosition Size is:" + dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchAccrualSheduleIds(DtoSearch dtoSearch) {

		try {
			log.info("searchPositionPayCodeId Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();

				List<DtoAccrualSchedule> positionPayCod = new ArrayList<>();

				List<AccrualSchedule> positionPayCodeIdList = this.repositoryAccrualSchedule.predictiveAccrualScheduleIdSearchWithPagination("%" + searchWord + "%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				if (!positionPayCodeIdList.isEmpty() && positionPayCodeIdList!=null) {

					for (AccrualSchedule payCode : positionPayCodeIdList) {
						DtoAccrualSchedule dtoPayCode = new DtoAccrualSchedule();
						dtoPayCode.setId(payCode.getId());
						dtoPayCode.setScheduleId(payCode.getScheduleId());
						dtoPayCode.setDesc(payCode.getDesc());
						dtoPayCode.setArbicDesc(payCode.getArbicDesc());
						dtoPayCode.setStartDate(payCode.getStartDate());
						dtoPayCode.setEndDate(payCode.getEndDate());
						positionPayCod.add(dtoPayCode);
					}

					dtoSearch.setTotalCount(positionPayCodeIdList.size());
				}
				dtoSearch.setRecords(positionPayCod);
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;

	}

}
