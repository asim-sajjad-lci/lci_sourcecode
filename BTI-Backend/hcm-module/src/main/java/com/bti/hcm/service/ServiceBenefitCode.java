/**

 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.dto.DtoBenefitCode;
import com.bti.hcm.model.dto.DtoEmployeePayCodeMaintenance;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service BenefitCode Project: Hcm Version: 0.0.1
 */
@Service("serviceBenefitCode")
public class ServiceBenefitCode {

	/**
	 * @Description LOGGER use for put a logger in BenefitCode Service
	 */
	static Logger log = Logger.getLogger(ServiceBenefitCode.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in BenefitCode service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in BenefitCode service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in BenefitCode service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryBenefitCode Autowired here using annotation of spring
	 *              for access of repositoryBenefitCode method in BenefitCode
	 *              service In short Access BenefitCode Query from Database using
	 *              repositoryBenefitCode.
	 */
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;

	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;

	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	/**
	 * @Description: get All BenefitCode
	 * @param dtoBenefitCode
	 * @return
	 */
	public DtoSearch getAllBenefitCode(DtoBenefitCode dtoBenefitCode) {
		log.info("getAllBenefitCode Method");
		DtoSearch dtoSearch = new DtoSearch();

		try {

			dtoSearch.setPageNumber(dtoBenefitCode.getPageNumber());
			dtoSearch.setPageSize(dtoBenefitCode.getPageSize());
			dtoSearch.setTotalCount(repositoryBenefitCode.getCountOfTotalBenefitCode());
			List<BenefitCode> benefitCodeList = null;
			if (dtoBenefitCode.getPageNumber() != null && dtoBenefitCode.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoBenefitCode.getPageNumber(), dtoBenefitCode.getPageSize(),
						Direction.DESC, "createdDate");
				benefitCodeList = repositoryBenefitCode.findByIsDeleted(false, pageable);
			} else {
				benefitCodeList = repositoryBenefitCode.findByIsDeletedOrderByCreatedDateDesc(false);
			}

			List<DtoBenefitCode> dtoBenefitCodeList = new ArrayList<>();
			if (benefitCodeList != null && !benefitCodeList.isEmpty()) {
				for (BenefitCode benefitCode : benefitCodeList) {
					dtoBenefitCode = new DtoBenefitCode(benefitCode);
					dtoBenefitCode.setId(benefitCode.getId());
					dtoBenefitCode.setBenefitId(benefitCode.getBenefitId());
					dtoBenefitCode.setDesc(benefitCode.getDesc());
					dtoBenefitCode.setArbicDesc(benefitCode.getArbicDesc());
					dtoBenefitCode.setStartDate(benefitCode.getStartDate());
					dtoBenefitCode.setEndDate(benefitCode.getEndDate());
					dtoBenefitCode.setTransction(benefitCode.isTransction());
					dtoBenefitCode.setMethod(benefitCode.getMethod());
					dtoBenefitCode.setAmount(benefitCode.getAmount());
					dtoBenefitCode.setPercent(benefitCode.getPercent());
					dtoBenefitCode.setPerPeriod(benefitCode.getPerPeriod());
					dtoBenefitCode.setPerYear(benefitCode.getPerYear());
					dtoBenefitCode.setLifeTime(benefitCode.getLifeTime());
					dtoBenefitCode.setFrequency(benefitCode.getFrequency());
					dtoBenefitCode.setPayFactor(benefitCode.getPayFactor());
					dtoBenefitCode.setInActive(benefitCode.isInActive());
					dtoBenefitCodeList.add(dtoBenefitCode);
				}
				dtoSearch.setRecords(dtoBenefitCodeList);
			}
			log.debug("All BenefitCode List Size is:" + dtoSearch.getTotalCount());

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description: Search BenefitCode data
	 * @param dtoSearch
	 * @return
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchBenefitCode(DtoSearch dtoSearch) {

		try {
			log.info("searchBenefitCode Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();

				String condition = "";

				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

					if (dtoSearch.getSortOn().equals("benefitId") || dtoSearch.getSortOn().equals("desc")
							|| dtoSearch.getSortOn().equals("arbicDesc") || dtoSearch.getSortOn().equals("startDate")
							|| dtoSearch.getSortOn().equals("endDate") || dtoSearch.getSortOn().equals("amount")
							|| dtoSearch.getSortOn().equals("frequency") || dtoSearch.getSortOn().equals("perYear")
							|| dtoSearch.getSortOn().equals("method")) {
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
					}

				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
				dtoSearch.setTotalCount(
						this.repositoryBenefitCode.predictiveBenefitCodeSearchTotalCount("%" + searchWord + "%"));
				List<BenefitCode> benefitCodeList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						benefitCodeList = this.repositoryBenefitCode.predictiveBenefitCodeSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						benefitCodeList = this.repositoryBenefitCode.predictiveBenefitCodeSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						benefitCodeList = this.repositoryBenefitCode.predictiveBenefitCodeSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}

				if (benefitCodeList != null && !benefitCodeList.isEmpty()) {
					List<DtoBenefitCode> dtoBenefitCodeList = new ArrayList<>();
					DtoBenefitCode dtoBenefitCode = null;
					for (BenefitCode benefitCode : benefitCodeList) {
						dtoBenefitCode = new DtoBenefitCode(benefitCode);
						dtoBenefitCode.setId(benefitCode.getId());
						dtoBenefitCode.setBenefitId(benefitCode.getBenefitId());
						dtoBenefitCode.setDesc(benefitCode.getDesc());
						dtoBenefitCode.setArbicDesc(benefitCode.getArbicDesc());
						dtoBenefitCode.setStartDate(benefitCode.getStartDate());
						dtoBenefitCode.setEndDate(benefitCode.getEndDate());
						dtoBenefitCode.setTransction(benefitCode.isTransction());
						dtoBenefitCode.setMethod(benefitCode.getMethod());
						dtoBenefitCode.setAmount(benefitCode.getAmount());
						dtoBenefitCode.setEndDateDays(benefitCode.getEndDateDays());
						dtoBenefitCode.setNoOfDays(benefitCode.getNoOfDays());
						dtoBenefitCode.setCustomDate(benefitCode.isCustomDate());
						dtoBenefitCode.setPercent(benefitCode.getPercent());
						dtoBenefitCode.setPerPeriod(benefitCode.getPerPeriod());
						dtoBenefitCode.setPerYear(benefitCode.getPerYear());
						dtoBenefitCode.setPayFactor(benefitCode.getPayFactor());
						dtoBenefitCode.setLifeTime(benefitCode.getLifeTime());
						dtoBenefitCode.setFrequency(benefitCode.getFrequency());
						dtoBenefitCode.setInActive(benefitCode.isInActive());
						dtoBenefitCode.setRoundOf(benefitCode.getRoundOf());			//ME
						dtoBenefitCode.setBenefitTypeId(benefitCode.getTypeField());	//ME
						
						dtoBenefitCodeList.add(dtoBenefitCode);
					}
					dtoSearch.setRecords(dtoBenefitCodeList);
				}
			}
			log.debug("Search BenefitCode Size is:" + dtoSearch.getTotalCount());

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description: get BenefitCode data by id
	 * @param id
	 * @return
	 */
	public DtoBenefitCode getBenefitCodeByBenefitCodeId(int id) {
		log.info("getBenefitCodeByBenefitCodeId Method");
		DtoBenefitCode dtoBenefitCode = new DtoBenefitCode();

		try {

			if (id > 0) {
				BenefitCode benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(id, false);
				if (benefitCode != null) {
					dtoBenefitCode = new DtoBenefitCode(benefitCode);
					dtoBenefitCode.setId(benefitCode.getId());

					if (benefitCode.getPayCodeList() != null && !benefitCode.getPayCodeList().isEmpty()) {
						List<Integer> listId = new ArrayList<>();
						for (PayCode payCode : benefitCode.getPayCodeList()) {
							listId.add(payCode.getId());
						}
						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCode2.setDescription(payCode1.getDescription());
								payCode2.setAmount(payCode1.getBaseOnPayCodeAmount());
								payCode2.setPayFactor(payCode1.getPayFactor());
								payCode2.setPayRate(payCode1.getPayRate());
								payCodeList.add(payCode2);
							}
							dtoBenefitCode.setDtoPayCode(payCodeList);
						}

					}
					dtoBenefitCode.setPayFactor(benefitCode.getPayFactor());
					dtoBenefitCode.setAmount(benefitCode.getAmount());
					dtoBenefitCode.setArbicDesc(benefitCode.getArbicDesc());
					dtoBenefitCode.setEndDate(benefitCode.getEndDate());
					dtoBenefitCode.setStartDate(benefitCode.getStartDate());
					dtoBenefitCode.setFrequency(benefitCode.getFrequency());
					dtoBenefitCode.setMethod(benefitCode.getMethod());
					dtoBenefitCode.setAmount(benefitCode.getAmount());
					dtoBenefitCode.setPayFactor(benefitCode.getPayFactor());
					dtoBenefitCode.setPayFactor(benefitCode.getPayFactor());
					dtoBenefitCode.setLifeTime(benefitCode.getLifeTime());
					dtoBenefitCode.setPercent(benefitCode.getPercent());
					dtoBenefitCode.setPerPeriod(benefitCode.getPerPeriod());
					dtoBenefitCode.setEndDateDays(benefitCode.getEndDateDays());
					dtoBenefitCode.setNoOfDays(benefitCode.getNoOfDays());
					dtoBenefitCode.setCustomDate(benefitCode.isCustomDate());
					dtoBenefitCode.setPerYear(benefitCode.getPerYear());
					dtoBenefitCode.setTransction(benefitCode.isTransction());
					dtoBenefitCode.setInActive(benefitCode.isInActive());
					
					dtoBenefitCode.setBenefitTypeId(benefitCode.getTypeField());
					dtoBenefitCode.setRoundOf(benefitCode.getRoundOf());
					
				} else {
					dtoBenefitCode.setMessageType("BENEFIT_NOT_GETTING");

				}
			} else {
				dtoBenefitCode.setMessageType("INVALID_BENEFIT_ID");

			}
			log.debug("BenefitCode By Id is:" + dtoBenefitCode.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBenefitCode;
	}

	/**
	 * @Description: delete benefitCode
	 * @param ids
	 * @return
	 */
	public DtoBenefitCode deleteBenefitCode(List<Integer> ids) {
		log.info("deleteBenefitCode Method");
		DtoBenefitCode dtoBenefitCode = new DtoBenefitCode();
		dtoBenefitCode
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("BENEFITED_CODE_DELETED", false));
		dtoBenefitCode
				.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("BENEFIT_ASSOCIATED", false));
		List<DtoBenefitCode> deleteBenefitCode = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse
				.getMessageByShortAndIsDeleted("BENEFITED_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer benefitCodeId : ids) {
				BenefitCode benefitCode = repositoryBenefitCode.findOne(benefitCodeId);
				if (benefitCode.getListEmployeeBenefitMaintenance().isEmpty()
						&& benefitCode.getListBuildPayrollCheckByBenefits().isEmpty()) {
					DtoBenefitCode dtoBenefitCode2 = new DtoBenefitCode();
					dtoBenefitCode2.setId(benefitCodeId);
					repositoryBenefitCode.deleteSingleBenefitCode(true, loggedInUserId, benefitCodeId);
					deleteBenefitCode.add(dtoBenefitCode2);
				} else {
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoBenefitCode.setMessageType(invlidDeleteMessage.toString());

			}
			if (!inValidDelete) {
				dtoBenefitCode.setMessageType("");
			}
			dtoBenefitCode.setDeleteBenefitCode(deleteBenefitCode);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete BenefitCode :" + dtoBenefitCode.getId());
		return dtoBenefitCode;
	}

	public DtoBenefitCode repeatByBenefitCodeId(String benefitCodeId) {
		log.info("repeatBybBenefitCodeId Method");
		DtoBenefitCode dtoBenefitCode = new DtoBenefitCode();
		try {
			List<BenefitCode> benefitCode = repositoryBenefitCode.findByBenefitId(benefitCodeId);
			if (benefitCode != null && !benefitCode.isEmpty()) {
				dtoBenefitCode.setIsRepeat(true);
			} else {
				dtoBenefitCode.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBenefitCode;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchBenefitId(DtoSearch dtoSearch) {

		try {
			log.info("searchBenefitId Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				List<String> benefitIdList = this.repositoryBenefitCode
						.predictiveBenefitIdSearchWithPagination("%" + searchWord + "%");
				if (!benefitIdList.isEmpty()) {
					dtoSearch.setTotalCount(benefitIdList.size());
					dtoSearch.setRecords(benefitIdList.size());
				}
				dtoSearch.setIds(benefitIdList);
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public List<DtoBenefitCode> getAllBenefitCodeDropDownList() {
		log.info("getAllBenefitCodeDropDownList  Method");
		List<DtoBenefitCode> dtoBenefitCodeList = new ArrayList<>();
		try {
			List<BenefitCode> list = repositoryBenefitCode.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (BenefitCode benefitCode : list) {
					DtoBenefitCode dtoBenefitCode = new DtoBenefitCode();
					dtoBenefitCode.setId(benefitCode.getId());
					dtoBenefitCode.setBenefitId(benefitCode.getBenefitId());
					dtoBenefitCode.setDesc(benefitCode.getDesc());
					dtoBenefitCodeList.add(dtoBenefitCode);
				}
			}
			log.debug("BenefitCode is:" + dtoBenefitCodeList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBenefitCodeList;
	}

	public List<DtoBenefitCode> getAllBenefitCodeByInActiveList() {
		log.info("getAllBenefitCodeByInActiveList  Method");
		List<DtoBenefitCode> dtoBenefitCodeList = new ArrayList<>();
		try {
			List<BenefitCode> list = repositoryBenefitCode.findByIsDeletedAndInActive(false, false);
			if (list != null && !list.isEmpty()) {
				for (BenefitCode benefitCode : list) {
					DtoBenefitCode dtoBenefitCode = new DtoBenefitCode();

					if (benefitCode.getPayCodeList() != null && !benefitCode.getPayCodeList().isEmpty()) {
						List<Integer> listId = new ArrayList<>();
						for (PayCode payCode : benefitCode.getPayCodeList()) {
							listId.add(payCode.getId());
						}
						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCodeList.add(payCode2);
							}
							dtoBenefitCode.setDtoPayCode(payCodeList);
						}

					}

					dtoBenefitCode.setId(benefitCode.getId());
					dtoBenefitCode.setBenefitId(benefitCode.getBenefitId());
					dtoBenefitCode.setDesc(benefitCode.getBenefitId() + " | " + benefitCode.getDesc());
					dtoBenefitCode.setArbicDesc(benefitCode.getArbicDesc());
					dtoBenefitCode.setStartDate(benefitCode.getStartDate());
					dtoBenefitCode.setEndDate(benefitCode.getEndDate());
					dtoBenefitCode.setTransction(benefitCode.isTransction());
					dtoBenefitCode.setMethod(benefitCode.getMethod());
					dtoBenefitCode.setPayFactor(benefitCode.getPayFactor());
					dtoBenefitCode.setAmount(benefitCode.getAmount());
					dtoBenefitCode.setPercent(benefitCode.getPercent());
					dtoBenefitCode.setPerYear(benefitCode.getPerYear());
					dtoBenefitCode.setEndDateDays(benefitCode.getEndDateDays());
					dtoBenefitCode.setNoOfDays(benefitCode.getNoOfDays());
					dtoBenefitCode.setCustomDate(benefitCode.isCustomDate());
					dtoBenefitCode.setPerPeriod(benefitCode.getPerPeriod());
					dtoBenefitCode.setLifeTime(benefitCode.getLifeTime());
					dtoBenefitCode.setFrequency(benefitCode.getFrequency());
					dtoBenefitCode.setInActive(benefitCode.isInActive());
					
					dtoBenefitCode.setBenefitTypeId(benefitCode.getTypeField());	//ME
					dtoBenefitCode.setRoundOf(benefitCode.getRoundOf()); 			//ME

					dtoBenefitCodeList.add(dtoBenefitCode);
				}
			}
			log.debug("BenefitCode is:" + dtoBenefitCodeList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBenefitCodeList;
	}

	public DtoSearch searchAllBenefitId(DtoSearch dtoSearch) {

		try {
			log.info("searchAllBenefitId Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				List<BenefitCode> benefitCodeList = this.repositoryBenefitCode
						.predictivesearchBenefitIdWithPagination("%" + searchWord + "%");

				if (!benefitCodeList.isEmpty()) {
					List<DtoBenefitCode> dtoBenefitCode = new ArrayList<>();
					DtoBenefitCode dtoBenefitCode1 = null;
					for (BenefitCode benefitCode : benefitCodeList) {
						dtoBenefitCode1 = new DtoBenefitCode(benefitCode);
						dtoBenefitCode1.setId(benefitCode.getId());
						dtoBenefitCode1.setBenefitId(benefitCode.getBenefitId());
						dtoBenefitCode1.setAmount(benefitCode.getAmount());
						dtoBenefitCode1.setDesc(benefitCode.getBenefitId() + " | " + benefitCode.getDesc());
						dtoBenefitCode.add(dtoBenefitCode1);
					}
					dtoSearch.setRecords(dtoBenefitCode);
				}
				dtoSearch.setTotalCount(benefitCodeList.size());
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoBenefitCode saveOrUpdateBenefitCode(DtoBenefitCode dtoBenefitCode) {
		log.info("saveOrUpdateDtoBenefitCode Method");
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			BenefitCode benefitCode = null;
			if (dtoBenefitCode.getId() != null && dtoBenefitCode.getId() > 0) {
				benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(dtoBenefitCode.getId(), false);
				benefitCode.setUpdatedBy(loggedInUserId);
				benefitCode.setUpdatedDate(new Date());

				if (dtoBenefitCode.getStartDate() != null && dtoBenefitCode.getEndDate() != null) {
					List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance
							.findByBenefitId(dtoBenefitCode.getId());
					if (employeeBenefitMaintenanceList != null) {
						for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
							repositoryEmployeeBenefitMaintenance.deleteSingleEmployeeBenefitMaintenance1(
									dtoBenefitCode.getStartDate(), dtoBenefitCode.getEndDate(),
									dtoBenefitCode.getInActive(), employeeBenefitMaintenance.getId());
						}
					}
				}
				if (dtoBenefitCode.getInActive() != null) {

					List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance
							.findByBenefitId(dtoBenefitCode.getId());
					if (employeeBenefitMaintenanceList != null) {
						for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
							repositoryEmployeeBenefitMaintenance.deleteSingleEmployeeBenefitMaintenance2(
									dtoBenefitCode.getInActive(), employeeBenefitMaintenance.getId());
						}
					}

				}

				if (dtoBenefitCode.getNoOfDays() != null) {

					List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance
							.findByBenefitId(dtoBenefitCode.getId());
					if (employeeBenefitMaintenanceList != null) {
						for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
							repositoryEmployeeBenefitMaintenance.deleteSingleEmployeeBenefitMaintenance3(
									dtoBenefitCode.getNoOfDays(), employeeBenefitMaintenance.getId());
						}
					}

				}
				if (dtoBenefitCode.getEndDateDays() != null) {

					List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance
							.findByBenefitId(dtoBenefitCode.getId());
					if (employeeBenefitMaintenanceList != null) {
						for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
							repositoryEmployeeBenefitMaintenance.deleteSingleEmployeeBenefitMaintenance4(
									dtoBenefitCode.getEndDateDays(), employeeBenefitMaintenance.getId());
						}
					}

				}
				if (dtoBenefitCode.getTransction() != null) {
					List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance
							.findByBenefitId(dtoBenefitCode.getId());
					if (employeeBenefitMaintenanceList != null) {
						for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
							repositoryEmployeeBenefitMaintenance.deleteSingleEmployeeBenefitMaintenance5(
									dtoBenefitCode.getTransction(), employeeBenefitMaintenance.getId());
						}
					}
				}

			} else {

				benefitCode = new BenefitCode();
				benefitCode.setCreatedDate(new Date());
				Integer rowId = repositoryBenefitCode.getCountOfTotalAtteandaces();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				benefitCode.setRowId(increment);
			}
			if (dtoBenefitCode.getDtoPayCode() != null && !dtoBenefitCode.getDtoPayCode().isEmpty()) {
				List<PayCode> payCodeList = new ArrayList<>();
				for (DtoPayCode dtoPayCode : dtoBenefitCode.getDtoPayCode()) {
					PayCode payCode = repositoryPayCode.findByIdAndIsDeleted(dtoPayCode.getId(), false);
					payCodeList.add(payCode);
				}
				benefitCode.setPayCodeList(payCodeList);
			}
			benefitCode.setAllPaycode(true);
			benefitCode.setBenefitId(dtoBenefitCode.getBenefitId());
			benefitCode.setDesc(dtoBenefitCode.getDesc());
			benefitCode.setArbicDesc(dtoBenefitCode.getArbicDesc());

			benefitCode.setStartDate(dtoBenefitCode.getStartDate());
			benefitCode.setEndDate(dtoBenefitCode.getEndDate());

			benefitCode.setTransction(dtoBenefitCode.getTransction());
			benefitCode.setMethod(dtoBenefitCode.getMethod());
			benefitCode.setAmount(dtoBenefitCode.getAmount());
			benefitCode.setPayFactor(dtoBenefitCode.getPayFactor());
			benefitCode.setPercent(dtoBenefitCode.getPercent());
			benefitCode.setPerPeriod(dtoBenefitCode.getPerPeriod());
			benefitCode.setPerYear(dtoBenefitCode.getPerYear());
			benefitCode.setEndDateDays(dtoBenefitCode.getEndDateDays());
			benefitCode.setNoOfDays(dtoBenefitCode.getNoOfDays());
			benefitCode.setCustomDate(dtoBenefitCode.isCustomDate());
			benefitCode.setLifeTime(dtoBenefitCode.getLifeTime());
			benefitCode.setFrequency(dtoBenefitCode.getFrequency());
			benefitCode.setInActive(dtoBenefitCode.getInActive());
			benefitCode.setUpdatedBy(loggedInUserId);
			benefitCode.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			
			benefitCode.setTypeField(dtoBenefitCode.getBenefitTypeId());
			benefitCode.setRoundOf(dtoBenefitCode.getRoundOf());
			
			repositoryBenefitCode.saveAndFlush(benefitCode);
			log.debug("BenefitCodeId is:" + dtoBenefitCode.getId());

		} catch (Exception e) {
			log.error(e);
		}
		return dtoBenefitCode;
	}

	public DtoPayCode deducationCodeByPaycode(DtoPayCode dtoPayCode) {
		List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenanceList = new ArrayList<>();
		try {

			List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance
					.findByEmployeeIds(dtoPayCode.getEmployeeId());
			for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
				if (employeePayCodeMaintenance != null) {
					BenefitCode benefitCode = repositoryBenefitCode.findByBenefitId(dtoPayCode.getBenefitId());
					if (benefitCode.getPayCodeList() != null && benefitCode != null) {
						for (PayCode payCode : benefitCode.getPayCodeList()) {
							if (payCode.getId() == employeePayCodeMaintenance.getPayCode().getId()) {
								DtoPayCode dtoPayCodes = new DtoPayCode();
								DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance();
								dtoPayCodes.setId(payCode.getId());
								dtoPayCodes.setPayCodeId(payCode.getPayCodeId());
								dtoPayCodes.setPayFactor(payCode.getPayFactor());
								if (payCode.getBaseOnPayCodeAmount() == null) {
									dtoPayCodes.setAmount(payCode.getBaseOnPayCodeAmount());
								} else {
									dtoPayCodes.setAmount(benefitCode.getAmount());
									dtoPayCodes.setBaseOnPayCodeAmount(benefitCode.getAmount());
								}
								dtoPayCodes.setBenefitIds(benefitCode.getBenefitId());
								dtoPayCodes.setBenefitId(benefitCode.getId());
								dtoPayCodes.setPayRate(payCode.getPayRate());
								dtoEmployeePayCodeMaintenance.setPayCode(dtoPayCodes);
								dtoEmployeePayCodeMaintenance.setId(employeePayCodeMaintenance.getId());
								dtoEmployeePayCodeMaintenance.setPayRate(employeePayCodeMaintenance.getPayRate());
								dtoEmployeePayCodeMaintenanceList.add(dtoEmployeePayCodeMaintenance);
								dtoPayCode.setRecords(dtoEmployeePayCodeMaintenanceList);
							}

						}
					}
				}

			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayCode;
	}

//code wise data with method type:
	public DtoPayCode deducationCodeByPaycode123(DtoPayCode dtoPayCode) {
		List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenanceList = new ArrayList<>();
		try {

			if (dtoPayCode.getMethod() == 5) {
				List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance
						.findByEmployeeIds(dtoPayCode.getEmployeeId());
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
					if (employeePayCodeMaintenance != null) {
						BenefitCode benefitCode = repositoryBenefitCode.findByBenefitId(dtoPayCode.getBenefitId());
						if (benefitCode.getPayCodeList() != null && benefitCode != null) {
							for (PayCode payCode : benefitCode.getPayCodeList()) {
								if (payCode.getId() == employeePayCodeMaintenance.getPayCode().getId()) {
									DtoPayCode dtoPayCodes = new DtoPayCode();
									DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance();
									dtoPayCodes.setId(payCode.getId());
									dtoPayCodes.setPayCodeId(payCode.getPayCodeId());
									dtoPayCodes.setPayFactor(payCode.getPayFactor());
									if (payCode.getBaseOnPayCodeAmount() == null) {
										dtoPayCodes.setAmount(payCode.getBaseOnPayCodeAmount());
									} else {
										dtoPayCodes.setAmount(benefitCode.getAmount());
										dtoPayCodes.setBaseOnPayCodeAmount(benefitCode.getAmount());
									}
									dtoPayCodes.setBenefitIds(benefitCode.getBenefitId());
									dtoPayCodes.setBenefitId(benefitCode.getId());
									dtoPayCodes.setPayRate(payCode.getPayRate());
									dtoEmployeePayCodeMaintenance.setPayCode(dtoPayCodes);
									dtoEmployeePayCodeMaintenance.setId(employeePayCodeMaintenance.getId());
									dtoEmployeePayCodeMaintenance.setPayRate(employeePayCodeMaintenance.getPayRate());
									dtoEmployeePayCodeMaintenanceList.add(dtoEmployeePayCodeMaintenance);
									dtoPayCode.setRecords(dtoEmployeePayCodeMaintenanceList);
								}

							}
						}
					}

				}
			}
			if (dtoPayCode.getMethod() == 1 || dtoPayCode.getMethod() == 2 || dtoPayCode.getMethod() == 3
					|| dtoPayCode.getMethod() == 4) {
				BenefitCode benefitCode = repositoryBenefitCode.findByBenefitId(dtoPayCode.getBenefitId());
				if (benefitCode != null) {
					DtoBenefitCode dtoBenefitCode = new DtoBenefitCode();
					dtoBenefitCode.setId(benefitCode.getId());
					dtoBenefitCode.setBenefitId(benefitCode.getBenefitId());
					dtoBenefitCode.setDesc(benefitCode.getBenefitId() + " | " + benefitCode.getDesc());
					dtoBenefitCode.setArbicDesc(benefitCode.getArbicDesc());
					dtoBenefitCode.setStartDate(benefitCode.getStartDate());
					dtoBenefitCode.setEndDate(benefitCode.getEndDate());
					dtoBenefitCode.setTransction(benefitCode.isTransction());
					dtoBenefitCode.setMethod(benefitCode.getMethod());
					dtoBenefitCode.setPayFactor(benefitCode.getPayFactor());
					dtoBenefitCode.setAmount(benefitCode.getAmount());
					dtoBenefitCode.setPercent(benefitCode.getPercent());
					dtoBenefitCode.setPerYear(benefitCode.getPerYear());
					dtoBenefitCode.setEndDateDays(benefitCode.getEndDateDays());
					dtoBenefitCode.setNoOfDays(benefitCode.getNoOfDays());
					dtoBenefitCode.setCustomDate(benefitCode.isCustomDate());
					dtoBenefitCode.setPerPeriod(benefitCode.getPerPeriod());
					dtoBenefitCode.setLifeTime(benefitCode.getLifeTime());
					dtoBenefitCode.setFrequency(benefitCode.getFrequency());
					dtoBenefitCode.setInActive(benefitCode.isInActive());
				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayCode;
	}

}
