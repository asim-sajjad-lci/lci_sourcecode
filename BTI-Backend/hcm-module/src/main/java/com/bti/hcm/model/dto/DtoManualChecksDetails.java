package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.sql.Date;

public class DtoManualChecksDetails extends DtoBase {

	private Integer id;
	private Integer transaction;
	private Short transactiotype;
	private Integer codeId;
	private Date dateFrom;
	private Date dateTo;
	private BigDecimal totalAmount;
	private BigDecimal totalPercent;
	private Integer hours;
	private Integer daysWorks;
	private Integer weeksWorks;
	
	
	private DtoFinancialDimensionsValues dtoFinancialDimensionsValues;
	private DtoManualChecks manualChecks;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTransaction() {
		return transaction;
	}

	public void setTransaction(Integer transaction) {
		this.transaction = transaction;
	}

	public Short getTransactiotype() {
		return transactiotype;
	}

	public void setTransactiotype(Short transactiotype) {
		this.transactiotype = transactiotype;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalPercent() {
		return totalPercent;
	}

	public void setTotalPercent(BigDecimal totalPercent) {
		this.totalPercent = totalPercent;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public Integer getDaysWorks() {
		return daysWorks;
	}

	public void setDaysWorks(Integer daysWorks) {
		this.daysWorks = daysWorks;
	}

	public Integer getWeeksWorks() {
		return weeksWorks;
	}

	public void setWeeksWorks(Integer weeksWorks) {
		this.weeksWorks = weeksWorks;
	}

	public DtoFinancialDimensionsValues getDtoFinancialDimensionsValues() {
		return dtoFinancialDimensionsValues;
	}

	public void setDtoFinancialDimensionsValues(DtoFinancialDimensionsValues dtoFinancialDimensionsValues) {
		this.dtoFinancialDimensionsValues = dtoFinancialDimensionsValues;
	}

	public DtoManualChecks getManualChecks() {
		return manualChecks;
	}

	public void setManualChecks(DtoManualChecks manualChecks) {
		this.manualChecks = manualChecks;
	}

}
