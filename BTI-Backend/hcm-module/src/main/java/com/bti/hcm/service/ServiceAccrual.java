package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Accrual;
import com.bti.hcm.model.AccrualType;
import com.bti.hcm.model.dto.DtoAccrual;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAccrual;
import com.bti.hcm.repository.RepositoryAccrualType;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * @author Gaurav
 *
 */
@Service("serviceAccrual")
public class ServiceAccrual {
	/**
	 * @Description LOGGER use for put a logger in Accrual Service
	 */
	static Logger log = Logger.getLogger(ServiceAccrual.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in ServiceAccrual service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in Department service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in Department service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryAccrual Autowired here using annotation of spring for
	 *              access of repositoryAccrual method in Accrual service In short
	 *              Access Accrual Query from Database using repositoryAccrual.
	 */
	@Autowired
	RepositoryAccrual repositoryAccrual;

	@Autowired
	RepositoryAccrualType repositoryAccrualType;

	/**
	 * @param dtoAccrual
	 * @return
	 * @throws ParseException
	 */
	public DtoAccrual saveOrUpdate(DtoAccrual dtoAccrual) {
		try {
			log.info("saveOrUpdate Accrual Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Accrual accrual = null;
			if (dtoAccrual.getId() != null && dtoAccrual.getId() > 0) {
				accrual = repositoryAccrual.findByIdAndIsDeleted(dtoAccrual.getId(), false);
				accrual.setUpdatedBy(loggedInUserId);
				accrual.setUpdatedDate(new Date());
			} else {
				accrual = new Accrual();
				accrual.setCreatedDate(new Date());
				Integer rowId = repositoryAccrual.getCountOfTotalAccruals();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				accrual.setRowId(increment);
			}

			AccrualType accrualType = null;
			if (dtoAccrual.getAccrualTyepId() != null && dtoAccrual.getAccrualTyepId() > 0) {
				accrualType = repositoryAccrualType.findOne(dtoAccrual.getAccrualTyepId());

			}

			accrual.setAccrualType(accrualType);
			accrual.setAccuralId(dtoAccrual.getAccuralId());
			accrual.setDesc(dtoAccrual.getDesc());
			accrual.setArbic(dtoAccrual.getArbic());
			accrual.setNumHour(dtoAccrual.getNumHour());
			accrual.setType(dtoAccrual.getType());
			accrual.setReasons(dtoAccrual.getReasons());
			accrual.setNumDay(dtoAccrual.getNumDay());
			accrual.setMaxAccrualHour(dtoAccrual.getMaxAccrualHour());
			accrual.setMaxHour(dtoAccrual.getMaxHour());
			accrual.setWorkHour(dtoAccrual.getWorkHour());
			accrual.setPeriod(dtoAccrual.getPeriod());

			accrual.setUpdatedRow(UtilDateAndTimeHr.localToUTC());

			repositoryAccrual.saveAndFlush(accrual);
			log.debug("Accrual is:" + dtoAccrual.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrual;

	}

	/**
	 * @Description: get All DeductionCode
	 * @param DtoDeductionCode
	 * @return
	 */

	/**
	 * @param dtoSearch
	 * @return
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searAccrual(DtoSearch dtoSearch) {

		try {
			log.info("search Accrual Method");

			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";

				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
					if (dtoSearch.getSortOn().equals("accuralId") || dtoSearch.getSortOn().equals("desc")
							|| dtoSearch.getSortOn().equals("arbic") || dtoSearch.getSortOn().equals("numHour")) {
						condition = dtoSearch.getSortOn();

					} else {
						condition = "id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}
				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");

				}

				dtoSearch.setTotalCount(
						this.repositoryAccrual.predictiveAccrualSearchTotalCount("%" + searchWord + "%"));
				List<Accrual> accrualList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						accrualList = this.repositoryAccrual.predictiveAccrualSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						accrualList = this.repositoryAccrual.predictiveAccrualSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						accrualList = this.repositoryAccrual.predictiveAccrualSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}

				}

				if (accrualList != null && !accrualList.isEmpty()) {
					List<DtoAccrual> dtoTimeCodeList = new ArrayList<>();
					DtoAccrual dtoAccrual = null;
					for (Accrual accrual : accrualList) {
						dtoAccrual = getDtoAccrual(accrual);
						dtoTimeCodeList.add(dtoAccrual);
					}
					dtoSearch.setRecords(dtoTimeCodeList);
				}

			}

			log.debug("Search searchPosition Size is:" + dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}

		return dtoSearch;
	}

	/**
	 * @param id
	 * @return
	 */
	public DtoAccrual getaccrualById(int id) {
		log.info("getAccrualById Method");
		DtoAccrual dtoAccrual = new DtoAccrual();
		try {
			if (id > 0) {
				Accrual accrual = repositoryAccrual.findByIdAndIsDeleted(id, false);
				if (accrual == null) {
					dtoAccrual = new DtoAccrual(accrual);
					dtoAccrual.setMessageType("ACCRUAL_NOT_GETTING");
				} else {

					dtoAccrual = getDtoAccrual(accrual);

				}
			} else {
				dtoAccrual.setMessageType("ACCRUAL_NOT_GETTING");

			}
			log.debug("ACCRUAL By Id is:" + dtoAccrual.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrual;
	}

	public DtoAccrual getDtoAccrual(Accrual accrual) {
		DtoAccrual dtoAccrual = new DtoAccrual();
		dtoAccrual.setId(accrual.getId());
		dtoAccrual.setAccuralId(accrual.getAccuralId());
		if (accrual.getAccrualType() != null) {
			dtoAccrual.setAccrualTyepId(accrual.getAccrualType().getId());
		}
		dtoAccrual.setDesc(accrual.getDesc());
		dtoAccrual.setArbic(accrual.getArbic());
		dtoAccrual.setNumHour(accrual.getNumHour());
		dtoAccrual.setNumDay(accrual.getNumDay());
		dtoAccrual.setType(accrual.getType());
		dtoAccrual.setReasons(accrual.getReasons());
		dtoAccrual.setMaxAccrualHour(accrual.getMaxAccrualHour());
		dtoAccrual.setMaxHour(accrual.getMaxHour());
		dtoAccrual.setWorkHour(accrual.getWorkHour());
		dtoAccrual.setPeriod(accrual.getPeriod());
		return dtoAccrual;
	}

	/**
	 * @param ids
	 * @return
	 */
	public DtoAccrual deleteAccrual(List<Integer> ids) {
		log.info("delete Method");
		DtoAccrual dtoAccrual = new DtoAccrual();
		dtoAccrual.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUAL_DELETED", false));
		dtoAccrual
				.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUAL_ASSOCIATED", false));
		List<DtoAccrual> deleteAccrual = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));

		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append("AccrualSheduleDetail already in use");
		try {
			for (Integer accuralId : ids) {
				Accrual accrual = repositoryAccrual.findOne(accuralId);

				if (accrual.getListAccrualScheduleDetail().isEmpty()) {

					DtoAccrual dtoAccrual2 = new DtoAccrual();
					dtoAccrual2.setId(accrual.getId());
					repositoryAccrual.deleteSingleAccrualCode(true, loggedInUserId, accuralId);
					deleteAccrual.add(dtoAccrual2);
				}

				else {
					inValidDelete = true;
				}

			}

			if (!inValidDelete) {
				dtoAccrual.setMessageType("RetirementFundSetup is Deleted Sucessfully");

			}

			dtoAccrual.setDelete(deleteAccrual);
		} catch (NumberFormatException e) {

			log.error("Error in Deleting Accrual", e);
		}
		log.debug("Delete Accrual :" + dtoAccrual.getId());
		return dtoAccrual;
	}

	/**
	 * @param accrualId
	 * @return
	 */
	public DtoAccrual repeatByAccrualtId(String accrualId) {
		log.info("repeatByAccrualtId Method");
		DtoAccrual dtoAccrual = new DtoAccrual();
		try {
			List<Accrual> department = repositoryAccrual.findByAccruaCodeId(accrualId);
			if (department != null && !department.isEmpty()) {
				dtoAccrual.setIsRepeat(true);
			} else {
				dtoAccrual.setIsRepeat(false);
			}

		} catch (Exception e) {
			log.error("Error in Repeat Id", e);
		}

		return dtoAccrual;
	}

	public DtoAccrual repeatByAccrualId(String accuralId) {
		log.info("repeatByAccrualId Method");
		DtoAccrual dtoAccrual = new DtoAccrual();
		try {
			List<Accrual> accrual = repositoryAccrual.findByAccruaId(accuralId);

			for (Accrual accrual2 : accrual) {
				dtoAccrual = getDtoAccrual(accrual2);
			}
			if (accrual != null && !accrual.isEmpty()) {
				dtoAccrual.setIsRepeat(true);
			} else {
				dtoAccrual.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrual;
	}

	public DtoSearch getAllAccrualSetupId(DtoSearch dtoSearch) {
		log.info("getAllAccrualSetupId Method");
		try {
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				List<Accrual> accrualSetupIdList = this.repositoryAccrual.getAllAccrualSetupId("%" + searchWord + "%");
				if (accrualSetupIdList != null && !accrualSetupIdList.isEmpty()) {
					List<DtoAccrual> dtoAccrualList = new ArrayList<>();
					DtoAccrual dtoAccrual = null;
					for (Accrual accrual : accrualSetupIdList) {
						String desc = "";
						dtoAccrual = new DtoAccrual(accrual);
						dtoAccrual.setAccuralId(accrual.getAccuralId());
						if (accrual.getType() == 1) {
							desc = "One Time";
						} else if (accrual.getType() == 2) {
							desc = "Hourly";
						} else if (accrual.getType() == 3) {
							desc = "Period";
						} else if (accrual.getType() == 4) {
							desc = "Interval";
						}
						dtoAccrual.setDesc(accrual.getDesc() + " - " + desc);
						dtoAccrual.setArbic(accrual.getArbic());
						dtoAccrual.setNumHour(accrual.getNumHour());
						dtoAccrual.setNumDay(accrual.getNumDay());
						dtoAccrual.setType(accrual.getType());
						dtoAccrual.setReasons(accrual.getReasons());
						dtoAccrual.setMaxAccrualHour(accrual.getMaxAccrualHour());
						dtoAccrual.setMaxHour(accrual.getMaxHour());
						dtoAccrual.setWorkHour(accrual.getWorkHour());
						dtoAccrual.setPeriod(accrual.getPeriod());
						dtoAccrualList.add(dtoAccrual);
					}
					dtoSearch.setRecords(dtoAccrualList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		log.debug("getAllAccrualTypeId Size is:" + dtoSearch.getTotalCount());
		return dtoSearch;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchAccrualId(DtoSearch dtoSearch) {
		log.info("search Accrual Method");
		try {
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";

				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
					if (dtoSearch.getSortOn().equals("accuralId") || dtoSearch.getSortOn().equals("desc")
							|| dtoSearch.getSortOn().equals("arbic") || dtoSearch.getSortOn().equals("numHour")) {
						condition = dtoSearch.getSortOn();

					} else {
						condition = "id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}
				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");

				}
				List<Accrual> accrualList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						accrualList = this.repositoryAccrual.searchAccrualId("%" + searchWord + "%", new PageRequest(
								dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						accrualList = this.repositoryAccrual.searchAccrualId("%" + searchWord + "%", new PageRequest(
								dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						accrualList = this.repositoryAccrual.searchAccrualId("%" + searchWord + "%", new PageRequest(
								dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}

				}

				if (accrualList != null && !accrualList.isEmpty()) {
					List<DtoAccrual> dtoTimeCodeList = new ArrayList<>();
					DtoAccrual dtoAccrual = null;
					for (Accrual accrual : accrualList) {
						dtoAccrual = getDtoAccrual(accrual);
						dtoTimeCodeList.add(dtoAccrual);
					}
					dtoSearch.setRecords(dtoTimeCodeList);
				}

			}

			log.debug("Search searchPosition Size is:" + dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

}
