package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PositionCourseDetail;
import com.bti.hcm.model.PositionCourseToLink;
import com.bti.hcm.model.TraningCourse;

public class DtoPositionCourseDetail extends DtoBase{
	
	private Integer id;
	private Integer sequence;
	private Integer positionCourseId;
	private Integer traningCourseId;
	private PositionCourseToLink positionCourse;
	private TraningCourse traningCourse;
	private List<DtoPositionCourseDetail> deletePosition;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public Integer getPositionCourseId() {
		return positionCourseId;
	}
	public void setPositionCourseId(Integer positionCourseId) {
		this.positionCourseId = positionCourseId;
	}
	public Integer getTraningCourseId() {
		return traningCourseId;
	}
	public void setTraningCourseId(Integer traningCourseId) {
		this.traningCourseId = traningCourseId;
	}
	public PositionCourseToLink getPositionCourse() {
		return positionCourse;
	}
	public void setPositionCourse(PositionCourseToLink positionCourse) {
		this.positionCourse = positionCourse;
	}
	public TraningCourse getTraningCourse() {
		return traningCourse;
	}
	public void setTraningCourse(TraningCourse traningCourse) {
		this.traningCourse = traningCourse;
	}
	
	public List<DtoPositionCourseDetail> getDeletePosition() {
		return deletePosition;
	}
	public void setDeletePosition(List<DtoPositionCourseDetail> deletePosition) {
		this.deletePosition = deletePosition;
	}
	 public DtoPositionCourseDetail() {
	}
	 
	 public DtoPositionCourseDetail(PositionCourseDetail positionCourseDetail) {
	 }
	
}
