package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceRoutinesUtilities;

@RestController
@RequestMapping("/routineUtilities")
public class ControllerRoutinesUtilities extends BaseController {

	private static final Logger log = Logger.getLogger(ControllerRoutinesUtilities.class);

	/**
	 * @Description serviceTransactionEntry Autowired here using annotation of
	 *              spring for use of TransactionEntry method in this controller
	 */
	@Autowired(required = true)
	ServiceRoutinesUtilities serviceRoutinesUtilities;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@GetMapping("/yearEndProcess/{year}")
	public ResponseMessage process(HttpServletRequest request, @PathVariable Integer year) throws Exception {
		log.info("Payroll Year End Process");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			this.serviceRoutinesUtilities.getAndSave(year);

			responseMessage = displayMessage("", "YEAR_END_CLOSING_SUCCESSFUL", "YEAR_END_CLOSING_FALIURE",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if (responseMessage != null) {
			log.debug("Payroll Year End Process:" + responseMessage.getMessage());
		}

		return responseMessage;
	}

}
