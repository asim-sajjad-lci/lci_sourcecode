package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.BuildChecks;

@Repository("/repositoryBuildChecks")
public interface RepositoryBuildChecks extends JpaRepository<BuildChecks, Integer>{

	BuildChecks findByIdAndIsDeleted(Integer id, boolean b);

	BuildChecks findOne(Integer planId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BuildChecks b set b.isDeleted =:deleted ,b.updatedBy =:updateById where b.id =:id ")
	public void deleteSingleBuildChecks(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	
	@Query("select count(*) from BuildChecks b where (b.description LIKE :searchKeyWord or b.checkByUserId LIKE :searchKeyWord) and b.isDeleted=false")
	Integer predictiveBuildCheckSearchTotalCount(@Param("searchKeyWord")  String searchKeyWord);

	@Query("select b from BuildChecks b where (b.description LIKE :searchKeyWord or b.checkByUserId LIKE :searchKeyWord) and b.isDeleted=false")
	List<BuildChecks> predictiveBuildcheckSearchWithPagination(@Param("searchKeyWord")String searchKeyWord,Pageable pageable);

	@Query("select count(*) from BuildChecks b ")
	public Integer getCountOfTotalBuildChecks();

	@Query("select b from BuildChecks b where b.default1.id =:id and b.isDeleted=false")
	List<BuildChecks> getByDefaultId(@Param("id") Integer id);

	@Query("select b from BuildChecks b where b.default1.id =:id and b.isDeleted=false")
	List<BuildChecks> findByDefauiltId(@Param("id") Integer id);
	
	@Query("select b from BuildChecks b where b.default1.id =:id and b.isDeleted=false")
	BuildChecks findByDefauiltIds(@Param("id") Integer id);
	
	@Query("select b from BuildChecks b where b.default1.id =:id and b.isDeleted=false")
	List<BuildChecks> findByDefauiltIds1(@Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BuildChecks b set b.isDeleted =:deleted ,b.updatedBy =:updateById where b.id =:id ")
	public void deleteSingleBuildCheckss(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	

	
	

}
