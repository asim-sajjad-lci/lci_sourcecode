package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.VATSetup;

@Repository("repositoryVatSetup")
public interface RepositoryVatSetup extends JpaRepository<VATSetup, Integer>{
	/**
	 * 
	 * @param id
	 * @param b
	 * @return
	 */
	
	VATSetup findByIdAndIsDeleted(Integer id, boolean b);
	
	/**
	 * 
	 * @param b
	 * @param loggedInUserId
	 * @param id
	 */
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update VATSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id")
	void deleteSingleVatSetup(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer id);

	/**
	 * 
	 * @param b
	 * @param loggedInUserId
	 * @param id
	 */
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update VATSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id")
	void deleteSingleVatSetupids(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer id);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	
	@Query("select count(*) from VATSetup d where (d.vatScheduleId like :searchKeyWord or d.vatDescription like :searchKeyWord or d.vatDescriptionArabic like :searchKeyWord) and d.isDeleted = false")
	Integer predictiveVatSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * 
	 * @param searchKeyWord
	 * @param pageRequest
	 * @return
	 */
	@Query("select d from VATSetup d where (d.vatScheduleId like :searchKeyWord or d.vatDescription like :searchKeyWord or d.vatDescriptionArabic like :searchKeyWord) and d.isDeleted = false")
	List<VATSetup> predictiveVatSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select d from VATSetup d where (d.id =:id)and d.isDeleted = false")
	VATSetup findById(@Param("id")Integer id);
	
	
	/**
	 * 
	 * @return
	 */
	@Query("select d from VATSetup d where d.isDeleted = false")
	List<VATSetup> getAllData();

	
	@Query("select d from VATSetup d where (d.vatScheduleId like :searchKeyWord) and d.isDeleted = false")
	List<VATSetup> getAllDroupDown(@Param("searchKeyWord")String string);
	
	

}
