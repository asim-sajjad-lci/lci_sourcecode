package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollTransactionOpenYearDetails;

@Repository("repositoryPayrollTransactionOpenYearDetails")
public interface RepositoryPayrollTransactionOpenYearDetails
		extends JpaRepository<PayrollTransactionOpenYearDetails, Integer> {

	@Query("SELECT pto FROM PayrollTransactionOpenYearDetails pto WHERE YEAR(pto.createdDate) =:year and pto.isDeleted = false")
	List<PayrollTransactionOpenYearDetails> findByYear(@Param("year") Integer year);

}
