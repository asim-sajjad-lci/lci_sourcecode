package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40611",indexes = {
        @Index(columnList = "SKLSTINDXD")
})
public class SkillSetDesc extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SKLSTINDXD")
	private int id;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "SKLSTINDX")
	private SkillSetSetup skillSetSetup; 
	
	@Column(name = "SKLSTSEQN")
	private int skillSetSeqn;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "SKLINDX")
	private SkillsSetup skillsId;
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
	        name = "Skill_Desc_Set", 
	        joinColumns = { @JoinColumn(name = "descId") }, 
	        inverseJoinColumns = { @JoinColumn(name = "setupId") }
	    )
	private List<SkillsSetup> skillsSetup;
	
	
	@Column(name = "SKLSTREQ")
	private boolean  skillRequired;
	
	@Column(name = "SKLSTCOMM")
	private String  comment;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SkillSetSetup getSkillSetSetup() {
		return skillSetSetup;
	}

	public void setSkillSetSetup(SkillSetSetup skillSetSetup) {
		this.skillSetSetup = skillSetSetup;
	}

	public int getSkillSetSeqn() {
		return skillSetSeqn;
	}

	public void setSkillSetSeqn(int skillSetSeqn) {
		this.skillSetSeqn = skillSetSeqn;
	}

	public SkillsSetup getSkillsId() {
		return skillsId;
	}

	public void setSkillsId(SkillsSetup skillsId) {
		this.skillsId = skillsId;
	}

	public boolean isSkillRequired() {
		return skillRequired;
	}

	public void setSkillRequired(boolean skillRequired) {
		this.skillRequired = skillRequired;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<SkillsSetup> getSkillsSetup() {
		return skillsSetup;
	}

	public void setSkillsSetup(List<SkillsSetup> skillsSetup) {
		this.skillsSetup = skillsSetup;
	}

	

	
	
}
