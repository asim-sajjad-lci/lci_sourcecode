package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40205",indexes = {
        @Index(columnList = "SHFTINDXID")
})
public class ShiftCode extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/*@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SHFTINDXID")
	private int id;*/
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SHFTINDXID")
	private int id;
	
	
	@Column(name = "SHFTID")
	private String shiftCodeId;
	
	@Column(name = "SHFTDSCR")
	private String desc;
	
	@Column(name = "SHFTDSCRA")
	private String arabicDesc;
	

	@Column(name = "SHFTINATV")
	private boolean inActive;
	
	@Column(name = "SHFTTYPE")
	private short shitPremium;
	
	@Column(name = "SHFTAMNT",precision = 13, scale = 3)
	private BigDecimal amount;
	
	@Column(name = "SHFTPERCT",precision = 15, scale = 5)
	private BigDecimal percent;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getShiftCodeId() {
		return shiftCodeId;
	}

	public void setShiftCodeId(String shiftCodeId) {
		this.shiftCodeId = shiftCodeId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public short getShitPremium() {
		return shitPremium;
	}

	public void setShitPremium(short shitPremium) {
		this.shitPremium = shitPremium;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}
	
	
}
