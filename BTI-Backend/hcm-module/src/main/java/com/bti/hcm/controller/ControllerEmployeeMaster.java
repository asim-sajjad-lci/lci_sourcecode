/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeMasterHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeMaster;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * Description: ControllerEmployeeMaster
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/employeeMaster")
public class ControllerEmployeeMaster extends BaseController{
	
	
	/**
	 * @Description LOGGER use for put a logger in EmployeeMaster Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeMaster.class);
	
	/**
	 * @Description serviceEmployeeMaster Autowired here using annotation of spring for use of serviceEmployeeMaster method in this controller
	 */
	@Autowired(required=true)
	ServiceEmployeeMaster serviceEmployeeMaster;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	/**
	 * 
	 * @param request{
  "listContact" : [{
  "employeeDepedentSequence": 1,
  "employeeContactName": "bansi Merried",
  "employeeContactNameArabic": "bansi Single",
  "employeeContactSequence": 222222,
  "employeeContactNameRelationship": "merried",
  "employeeContactHomePhone":"989898989898",
  "employeeContactMobilePhone":"898888898978",
  "employeeContactWorkPhone":"696969898978",
  "employeeContactAddress":"sapath 11",
  "employeeContactWorkPhoneExt":"+91",
  "employeeMaster": {
    "employeeIndexId":1
  }
  },{
  "employeeDepedentSequence": 1,
  "employeeContactName": "manoj Merried",
  "employeeContactNameArabic": "manoj Single",
  "employeeContactSequence": 222222,
  "employeeContactNameRelationship": "merried",
  "employeeContactHomePhone":"989898989898",
  "employeeContactMobilePhone":"898888898978",
  "employeeContactWorkPhone":"696969898978",
  "employeeContactAddress":"sapath 10",
  "employeeContactWorkPhoneExt":"+91",
  "employeeMaster": {
    "employeeIndexId":1
  }
  }]
   
}
	 * @param dtoEmployeeMaster
	 * @param file
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST,consumes = {"multipart/form-data"})
	public ResponseMessage createEmployeeMaster(HttpServletRequest request,@RequestParam ("DtoEmployeeMaster")String   dtoEmployeeMaster, @RequestParam(name = "file",required = false) MultipartFile file) throws Exception{
		LOGGER.info("Create EmployeeMaster Info");
		
		ObjectMapper objectMapper = new ObjectMapper();
		DtoEmployeeMaster dtoEmployeeMasterObject = objectMapper.readValue(dtoEmployeeMaster, DtoEmployeeMaster.class);
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeMasterObject = serviceEmployeeMaster.saveOrUpdate(dtoEmployeeMasterObject,file);
			responseMessage=displayMessage(dtoEmployeeMasterObject, "EMPLOYEE_CREATED", "EMPLOYEE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Employee Master Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoEmployeeMaster
	 * @param file
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST,consumes = {"multipart/form-data"})
	public ResponseMessage updateEmployeeMaster(HttpServletRequest request,@RequestParam ("DtoEmployeeMaster")String   dtoEmployeeMaster, @RequestParam(name = "file",required=false) MultipartFile file) throws Exception{
		LOGGER.info("Update EmployeeMaster Info");
		
		ObjectMapper objectMapper = new ObjectMapper();
		DtoEmployeeMaster dtoEmployeeMasterObject = objectMapper.readValue(dtoEmployeeMaster, DtoEmployeeMaster.class);
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeMasterObject = serviceEmployeeMaster.saveOrUpdate(dtoEmployeeMasterObject,file);
			responseMessage=displayMessage(dtoEmployeeMasterObject, "EMPLOYEE_UPDATED_SUCCESS", "EMPLOYEE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Employee Master Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoEmployeeMaster
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoEmployeeMaster dtoEmployeeMaster) throws Exception {
		LOGGER.info("Delete Employee Master Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeMaster.getIds() != null && !dtoEmployeeMaster.getIds().isEmpty()) {
				DtoEmployeeMaster dtoEmployeeMaster2 = serviceEmployeeMaster.deleteEmployeeMaster(dtoEmployeeMaster.getIds());
				
				
				if(dtoEmployeeMaster2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DELETED", false), dtoEmployeeMaster2);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_MASTER_NOT_DELETE_ID_MESSAGE", false), dtoEmployeeMaster2);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Employee Master Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllEmployeeMasterDropDownList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllEmployeeMasterDropDownList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeeMaster> positionList = serviceEmployeeMaster.getAllEmployeeMasterInActiveList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.EMPLOYEE_GET_ALL, false), positionList);
			}else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/employeeMasterDropDownList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage employeeMasterDropDownList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeeMasterHcm> list = serviceEmployeeMaster.employeeMasterInActiveList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_GET_ALL", false), list);
			}else {
				responseMessage =unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoEmployeeMaster
	 * @return
	 * @throws Exception
	 * @Request
	 * {
		  "employeeId": "10"
		}
	 */
	@RequestMapping(value = "/employeeIdcheck", method = RequestMethod.POST)
	public ResponseMessage employeeIdCheck(HttpServletRequest request, @RequestBody DtoEmployeeMaster dtoEmployeeMaster)
			throws Exception {
		LOGGER.info("employeeIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeMaster dtoEmployeeMasterObj = serviceEmployeeMaster.repeatByEmployeeId(dtoEmployeeMaster.getEmployeeId());
			responseMessage=displayMessage(dtoEmployeeMasterObj, "EMPLOYEE_RESULT", "EMPLOYEE_REPEAT_EMPLOYEEID_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeMaster(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("Search EmployeeMaster Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeMaster.searchEmployee(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_GET_ALL", "EMPLOYEE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getAllEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllEmployeeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Employees Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeMaster.getAllEmployeeId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_MASTER_GET_ALL", "EMPLOYEE_MASTER_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}																			
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllEmployeeName", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllEmployeeName(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Employee Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeMaster.getAllEmployeeName(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_MASTER_GET_ALL", "EMPLOYEE_MASTER_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	

	@RequestMapping(value = "/getAllByEmployeeForDepartmentList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByEmployeeForDepartmentList(@RequestBody DtoDepartment dtoDepartment, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Employee Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		 
		if (flag) {
			List<DtoEmployeeMasterHcm> listEmployee = this.serviceEmployeeMaster.getAllByEmployeeForDepartmentList(dtoDepartment);
			responseMessage=displayMessage(listEmployee, "EMPLOYEE_MASTER_GET_ALL", "EMPLOYEE_MASTER_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getEmployeeByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getEmployeeByEmployeeId(@RequestBody DtoEmployeeMaster dtoEmployeeMaster, HttpServletRequest request) throws Exception {
		LOGGER.info("Get Employee By Id Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		 
		if (flag) {
			DtoEmployeeMaster employeeMaster = this.serviceEmployeeMaster.getEmployeeById(dtoEmployeeMaster);
			responseMessage=displayMessage(employeeMaster, "EMPLOYEE_MASTER_GET_ALL", "EMPLOYEE_MASTER_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
}
