/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Department;

/**
 * Description: Interface for Department 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryDepartment")

public interface RepositoryDepartment extends JpaRepository<Department, Integer>{
	
	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	public Department findByIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * @param deleted
	 * @return
	 */
	public List<Department> findByIsDeleted(Boolean deleted);

	/**
	 * @return
	 */
	@Query("select count(*) from Department d where d.isDeleted=false")
	public Integer getCountOfTotalDepartment();
	
	
	/**
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<Department> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Department d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteDepartments(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Department d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleDepartment(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * @return
	 */
	public Department findTop1ByOrderByDepartmentIdDesc();
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select d from Department d where (d.departmentId like :searchKeyWord  or d.departmentDescription like :searchKeyWord or d.arabicDepartmentDescription like :searchKeyWord) and d.isDeleted=false")
	public List<Department> predictiveDepartmentSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<Department> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from Department d where (d.departmentId like :searchKeyWord  or d.departmentDescription like :searchKeyWord  or d.arabicDepartmentDescription like :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveDepartmentSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select d from Department d where (d.departmentId like :searchKeyWord  or d.departmentDescription like  :searchKeyWord  or d.arabicDepartmentDescription like :searchKeyWord) and d.isDeleted=false")
	public List<Department> predictiveDepartmentSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param departmentId
	 * @return
	 */
	@Query("select p from Department p where (p.departmentId =:departmentId) and p.isDeleted=false")
	public List<Department> findByDepartmentId(@Param("departmentId")String departmentId);
	
	
	@Query("select d.departmentId from Department d where (d.departmentId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<String> predictiveDepartmentIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select d from Department d where (d.departmentId like :searchKeyWord) and d.isDeleted=false")
	public List<Department> predictiveDepartmentAllIdsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select count(*) from Department d ")
	public Integer getCountOfTotalDepartments();
	

	@Query("select count(*) from Department d")
	public Integer getCountOfTotalDepartmentWithOutDelete();
	
	 //@Cacheable("department")
	@Query("select d from Department d where id in(:list) and d.isDeleted=false")
	public List<Department> findAllDepartmentListId(@Param("list") List<Integer> ls);
	
	@Query("select d from Department d where d.departmentId =:DepID and d.isDeleted=false")
	public Department findByDepartmentIdOne(@Param("DepID") String ls);
}
