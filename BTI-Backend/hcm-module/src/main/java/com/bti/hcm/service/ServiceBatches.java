package com.bti.hcm.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Batches;
import com.bti.hcm.model.BuildPayrollCheckByBatches;
import com.bti.hcm.model.Distribution;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoBatches;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBatches;
import com.bti.hcm.repository.RepositoryDistribution;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceBatches")
public class ServiceBatches {

	/**
	 * @Description LOGGER use for put a logger in Batches Service
	 * updated on: 2019-04-26
	 * updated by: hamid
	 * */
	static Logger log = Logger.getLogger(ServiceBatches.class.getName());


	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required=false)	
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryBatches repositoryBatches;
	
	@Autowired
	RepositoryDistribution repositoryDistribution;
	
	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;
	
	
	public DtoBatches saveOrUpdate(DtoBatches dtoBatches){

		try {
			log.info("save  Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Batches batches = null;
			if (dtoBatches.getId() != null && dtoBatches.getId() > 0) {
				batches = repositoryBatches.findByIdAndIsDeleted(dtoBatches.getId(), false);
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedDate(new Date());
			} else {
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				batches.setRowId(increment);
			 }
			if(dtoBatches.getPostingDate()!=null) {
				batches.setPostingDate(dtoBatches.getPostingDate());
				batches.setStatus((byte)3);
			}else if(dtoBatches.getStatus() == 2){
				batches.setStatus(dtoBatches.getStatus());
			}else if(dtoBatches.getPostingDate() ==null){
				batches.setStatus((byte)0);
			}else if(!batches.getListBuildPayrollCheckByBatches().isEmpty()) {
				for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
					if(buildPayrollCheckByBatches.getBuildChecks()!=null) {
						batches.setStatus((byte)1);
					}
				}
			}
			
			
			
			batches.setArabicDescription(dtoBatches.getArabicDescription());
			batches.setBatchId(dtoBatches.getBatchId());
			batches.setDescription(dtoBatches.getDescription());
			batches.setArabicDescription(dtoBatches.getArabicDescription());
			batches.setTransactionType(dtoBatches.getTransactionType());
			batches.setTotalTransactions(dtoBatches.getTotalTransactions());
			batches.setApproved(dtoBatches.isApproved());
			batches.setFromtranscation(true);
			if(dtoBatches.isApproved()) {
				batches.setApprovedDate(new Date());
			}
			
			batches.setUserID(String.valueOf(loggedInUserId));
			batches.setUpdatedBy(loggedInUserId);
			batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			batches.setFromBuild(true);
			repositoryBatches.saveAndFlush(batches);
			log.debug("Batches is:"+dtoBatches.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBatches;

	}
	
	/**
	 * @Description: get All BenefitCode
	 * @param dtobatches
	 * @return
	 */
	public DtoSearch getAll(DtoSearch dtoSearch) {
		String condition="";
		String searchWord=dtoSearch.getSearchKeyword();
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			if(dtoSearch.getSortOn().equals("batchId") || dtoSearch.getSortOn().equals("description") ||
					dtoSearch.getSortOn().equals("transactionType") || dtoSearch.getSortOn().equals("quantityTotal")||
					dtoSearch.getSortOn().equals("totalTransactions") || dtoSearch.getSortOn().equals("approved")||
					dtoSearch.getSortOn().equals("approvedDate") || dtoSearch.getSortOn().equals("status")|| dtoSearch.getSortOn().equals("userID") ||dtoSearch.getSortOn().equals("postingDate")
					) {
				condition = dtoSearch.getSortOn();
			
		}else {
			condition = "id";
		}
	}else{
		condition+="id";
		dtoSearch.setSortOn("");
		dtoSearch.setSortBy("");
		
	}
		
		try {
			
			dtoSearch.setTotalCount(this.repositoryBatches.predictiveBatchesSearchWithPaginationCountes("%"+searchWord+"%"));
			List<Batches> batchesList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					batchesList =this.repositoryBatches.predictiveBatchesSearchWithPaginationGetAll("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					batchesList = this.repositoryBatches.predictiveBatchesSearchWithPaginationGetAll("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					batchesList = this.repositoryBatches.predictiveBatchesSearchWithPaginationGetAll("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			List<DtoBatches> dtoBatches=new ArrayList<>();
			if(batchesList!=null && !batchesList.isEmpty())
			{
				for (Batches batches : batchesList) 
				{
					DtoBatches dtobatches=new DtoBatches(batches);
					dtobatches.setId(batches.getId());
					dtobatches.setBatchId(batches.getBatchId());
					dtobatches.setDescription(batches.getDescription());
					dtobatches.setTransactionType(batches.getTransactionType());
					
					BigDecimal credit = BigDecimal.ZERO;
					BigDecimal debit = BigDecimal.ZERO;
					
					List<TransactionEntryDetail> detail=	repositoryTransactionEntryDetail.searchByBatchId(batches.getId());
					for (TransactionEntryDetail transactionEntryDetail : detail) {
						if(transactionEntryDetail.getBenefitCode()!=null && transactionEntryDetail.getAmount() != null) {
							credit = credit.add(BigDecimal.valueOf(transactionEntryDetail.getAmount().doubleValue()));
						}else if(transactionEntryDetail.getDeductionCode()!=null){
							
							debit = debit.add(BigDecimal.valueOf(transactionEntryDetail.getAmount().doubleValue()));
						}
					}
					
					int total = 	credit.intValue()-debit.intValue();
					
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
						if(buildPayrollCheckByBatches.getBuildChecks()!=null) {
							dtobatches.setBuildId(buildPayrollCheckByBatches.getBuildChecks().getId());
							
							if(buildPayrollCheckByBatches.getBuildChecks().getMonthly()!=null) {
								dtobatches.setStatusCheck(buildPayrollCheckByBatches.getBuildChecks().getMonthly());
							}
							
						}
					}
					
					dtobatches.setPostingDate(batches.getPostingDate());
					dtobatches.setQuantityTotal(BigDecimal.valueOf(total));
					dtobatches.setTotalTransactions((Double.valueOf(detail.size())));
					dtobatches.setApproved(batches.isApproved());
					if(batches.isApproved()) {
						dtobatches.setApprovedDate(batches.getApprovedDate());
						dtobatches.setUserID(Integer.parseInt(batches.getUserID()));
					}
					dtobatches.setStatus(batches.getStatus());
					dtobatches.setArabicDescription(batches.getArabicDescription());
					dtoBatches.add(dtobatches);
				}
				dtoSearch.setRecords(dtoBatches);
			}
			log.debug("All Batchess List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	
	
	public DtoSearch getAllBuildCheck(DtoSearch dtoSearch) {
		log.info("getAll Batcheses Method");
		String condition="";
		String searchWord=dtoSearch.getSearchKeyword();
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			if(dtoSearch.getSortOn().equals("batchId") || dtoSearch.getSortOn().equals("description") ||
					dtoSearch.getSortOn().equals("transactionType") || dtoSearch.getSortOn().equals("quantityTotal")||
					dtoSearch.getSortOn().equals("totalTransactions") || dtoSearch.getSortOn().equals("approved")||
					dtoSearch.getSortOn().equals("approvedDate") || dtoSearch.getSortOn().equals("status")|| dtoSearch.getSortOn().equals("userID")
					) {
				condition = dtoSearch.getSortOn();
			
		}else {
			condition = "id";
		}
	}else{
		condition+="id";
		dtoSearch.setSortOn("");
		dtoSearch.setSortBy("");
		
	}
		
		try {
			
			dtoSearch.setTotalCount(this.repositoryBatches.predictiveBatchesSearchWithPaginationCount("%"+searchWord+"%"));
			List<Batches> batchesList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					batchesList =this.repositoryBatches.predictiveBatchesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					batchesList = this.repositoryBatches.predictiveBatchesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					batchesList = this.repositoryBatches.predictiveBatchesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			List<DtoBatches> dtoBatches=new ArrayList<>();
			if(batchesList!=null && !batchesList.isEmpty())
			{
				for (Batches batches : batchesList) 
				{
					if(batches.getStatus() == 3) {
						BigDecimal totalamount = BigDecimal.ZERO; 
						DtoBatches dtobatches=new DtoBatches(batches);
						dtobatches.setId(batches.getId());
						dtobatches.setBatchId(batches.getBatchId());
						dtobatches.setDescription(batches.getDescription());
						dtobatches.setArabicDescription(batches.getArabicDescription());
						dtobatches.setTransactionType(batches.getTransactionType());
						
						List<TransactionEntryDetail> detail=	repositoryTransactionEntryDetail.searchByBatchId(batches.getId());
						for (TransactionEntryDetail transactionEntryDetail : detail) {
							if(transactionEntryDetail.getBenefitCode()!=null) {
								totalamount = totalamount.add(BigDecimal.valueOf(transactionEntryDetail.getBenefitCode().getAmount().doubleValue()));
							}else if(transactionEntryDetail.getDeductionCode()!=null){
								totalamount = totalamount.subtract(transactionEntryDetail.getDeductionCode().getAmount());
							}
						}
						
						dtobatches.setPostingDate(batches.getPostingDate());
						dtobatches.setQuantityTotal(totalamount);
						dtobatches.setTotalTransactions((Double.valueOf(detail.size())));
						dtobatches.setApproved(batches.isApproved());
						if(batches.isApproved()) {
							dtobatches.setApprovedDate(batches.getApprovedDate());
							dtobatches.setUserID(Integer.parseInt(batches.getUserID()));
						}
						dtobatches.setStatus(batches.getStatus());
						
						dtobatches.setArabicDescription(batches.getArabicDescription());
						dtoBatches.add(dtobatches);	
					}
					
				}
				dtoSearch.setRecords(dtoBatches);
			}
			log.debug("All Batches List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	/**
	 * @Description: delete benefitCode
	 * @param ids
	 * @return
	 */
	public DtoBatches deleteBatches(List<Integer> ids) {
		log.info("deleteBatches Method");
		DtoBatches dtoBatches = new DtoBatches();
		dtoBatches.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("BENEFIT_DELETED", false));
		dtoBatches.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("BENEFIT_ASSOCIATED", false));
		List<DtoBatches> deleteDtoBatches = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer benefitCodeId : ids) {
				Batches batches = repositoryBatches.findOne(benefitCodeId);
				if(batches!=null) {
					DtoBatches dtoBatches2 = new DtoBatches();
				if(batches.getListTransactionEntry().isEmpty() &&  batches.getListBuildPayrollCheckByBatches().isEmpty()) {
					dtoBatches2.setId(benefitCodeId);
					dtoBatches2.setBatchId(batches.getBatchId());
					dtoBatches2.setId(batches.getId());
					repositoryBatches.deleteSingleBatches(true, loggedInUserId, benefitCodeId);
					deleteDtoBatches.add(dtoBatches2);
			}else {
				inValidDelete = true;
			}
				
				}else {
					inValidDelete = true;
				}
		}

	if(inValidDelete){
		dtoBatches.setMessageType(invlidDeleteMessage.toString());
		
	}
	if(!inValidDelete){
		dtoBatches.setMessageType("");
		
	}
			dtoBatches.setDelete(deleteDtoBatches);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Batches :"+dtoBatches.getId());
		return dtoBatches;
	}
	
	
	public DtoBatches repeatByBatchesId(String benefitCodeId) {
		log.info("repeatByBatchesId Method");
		DtoBatches dtoBatches = new DtoBatches();
		try {
			List<Batches> benefitCode=repositoryBatches.findByBatchId(benefitCodeId);
			if(benefitCode!=null && !benefitCode.isEmpty()) {
				dtoBatches.setIsRepeat(true);
			}else {
				dtoBatches.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBatches;
	}

	
	
	public List<DtoBatches> getAllBatchesCodeDropDownList(DtoSearch dtoSearch) {
		log.info("getAllBatchesCodeDropDownList  Method");
		List<DtoBatches> dtoBatchesList = new ArrayList<>();
		try {
			String searchWord =	dtoSearch.getSearchKeyword();
			List<Batches> list = repositoryBatches.searchByBatchId("%"+searchWord+"%");
			if (list != null && !list.isEmpty()) {
				for (Batches batches : list) {
					List<Distribution> listDefaut = repositoryDistribution.getAllByDefaultId(batches.getId());
					if(listDefaut.isEmpty()) {
						if(batches.getStatus()!=0) {
							DtoBatches dtoBenefitCode = new DtoBatches();
							dtoBenefitCode.setId(batches.getId());
							dtoBenefitCode.setBatchId(batches.getBatchId());
							dtoBenefitCode.setDescription(batches.getDescription());
							dtoBenefitCode.setArabicDescription(batches.getArabicDescription());
							
							List<TransactionEntryDetail> detail = repositoryTransactionEntryDetail
									.searchByBatchId(batches.getId()); //ME Added
							
							dtoBenefitCode.setTotalTransactions(Double.valueOf(detail.size()));  //ME Added
							
							for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
								if(buildPayrollCheckByBatches.getBuildChecks()!=null) {
									dtoBenefitCode.setPayPeriodFrom(buildPayrollCheckByBatches.getBuildChecks().getDateFrom());
									dtoBenefitCode.setPayPeriodTo(buildPayrollCheckByBatches.getBuildChecks().getDateTo());
									dtoBenefitCode.setBuildId(buildPayrollCheckByBatches.getBuildChecks().getId());
									dtoBenefitCode.setBuildDate(buildPayrollCheckByBatches.getBuildChecks().getCreatedDate());
								}
								
							}
							dtoBatchesList.add(dtoBenefitCode);	
						}
					}
					
					
					
				}
			}
			log.debug("Batches is:"+dtoBatchesList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBatchesList;
	}

	public DtoSearch getById(Integer id) {
		
		DtoSearch dtoSearch = new DtoSearch();
		
		try {
			Batches batches=	repositoryBatches.findByIdAndIsDeleted(id, false);
			if(batches!=null) {
				BigDecimal totalamount = new BigDecimal("0.0");
				DtoBatches dtobatches=new DtoBatches(batches);
				dtobatches.setId(batches.getId());
				dtobatches.setBatchId(batches.getBatchId());
				dtobatches.setDescription(batches.getDescription());
				dtobatches.setArabicDescription(batches.getArabicDescription());
				dtobatches.setTransactionType(batches.getTransactionType());
				List<TransactionEntryDetail> detail=	repositoryTransactionEntryDetail.searchByBatchId(batches.getId());
				for (TransactionEntryDetail transactionEntryDetail : detail) {
					if(transactionEntryDetail.getBenefitCode()!=null) {
						totalamount = totalamount.add(transactionEntryDetail.getBenefitCode().getAmount());
					}else if(transactionEntryDetail.getDeductionCode()!=null){
						totalamount = totalamount.subtract(transactionEntryDetail.getDeductionCode().getAmount());
					}
				}
				dtobatches.setPostingDate(batches.getPostingDate());
				dtobatches.setQuantityTotal(totalamount);
				dtobatches.setTotalTransactions((Double.valueOf(detail.size())));
				dtobatches.setApproved(batches.isApproved());
				dtobatches.setApprovedDate(batches.getApprovedDate());
				dtobatches.setStatus(batches.getStatus());
				dtobatches.setUserID(Integer.parseInt(batches.getUserID()));
				dtoSearch.setRecords(dtobatches);
			}
				
		}catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	public DtoSearch getAllForTransectionEntry(DtoSearch dtoSearch) {
		log.info("getAll Batches Method");
		String condition="";
		String searchWord=dtoSearch.getSearchKeyword();
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			if(dtoSearch.getSortOn().equals("batchId") || dtoSearch.getSortOn().equals("description") ||
					dtoSearch.getSortOn().equals("transactionType") || dtoSearch.getSortOn().equals("quantityTotal")||
					dtoSearch.getSortOn().equals("approved")||
					dtoSearch.getSortOn().equals("approvedDate") || dtoSearch.getSortOn().equals("status")|| dtoSearch.getSortOn().equals("userID")
					) {
				condition = dtoSearch.getSortOn();
			
		}else {
			condition = "id";
		}
	}else{
		condition+="id";
		dtoSearch.setSortOn("");
		dtoSearch.setSortBy("");
		
	}
		
		try {
			
			
			List<Batches> batchesList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					batchesList =this.repositoryBatches.predictiveBatchesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					batchesList = this.repositoryBatches.predictiveBatchesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					batchesList = this.repositoryBatches.predictiveBatchesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			dtoSearch.setTotalCount(repositoryBatches.predictiveBatchesSearchWithPaginationCount("%"+searchWord+"%"));
			
			List<DtoBatches> dtoBatches=new ArrayList<>();
			if(batchesList!=null && !batchesList.isEmpty())
			{
				for (Batches batches : batchesList) 
				{
					if(!batches.getListTransactionEntry().isEmpty()) {
						BigDecimal totalamount = BigDecimal.ZERO; 
						DtoBatches dtobatches=new DtoBatches(batches);
						dtobatches.setId(batches.getId());
						dtobatches.setBatchId(batches.getBatchId());
						dtobatches.setPayPeriodFrom(batches.getListTransactionEntry().get(0).getFromDate());
						dtobatches.setPayPeriodTo(batches.getListTransactionEntry().get(0).getToDate());
						dtobatches.setDescription(batches.getDescription());
						dtobatches.setArabicDescription(batches.getArabicDescription());
						dtobatches.setTransactionType(batches.getTransactionType());
						
						List<TransactionEntryDetail> detail=	repositoryTransactionEntryDetail.searchByBatchId(batches.getId());
						for (TransactionEntryDetail transactionEntryDetail : detail) {
							if(transactionEntryDetail.getBenefitCode()!=null) {
								if(transactionEntryDetail.getBenefitCode().getAmount()!=null) {
									totalamount = totalamount.add(BigDecimal.valueOf(transactionEntryDetail.getBenefitCode().getAmount().doubleValue()));
								}
								
							}else if(transactionEntryDetail.getDeductionCode()!=null){
								if(transactionEntryDetail.getDeductionCode().getAmount()!=null) {
								totalamount = totalamount.subtract(transactionEntryDetail.getDeductionCode().getAmount());
							}
								
							}
						}
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
							if(buildPayrollCheckByBatches.getBuildChecks()!=null) {
								dtobatches.setBuildId(buildPayrollCheckByBatches.getBuildChecks().getId());
								dtobatches.setPayPeriodFrom(buildPayrollCheckByBatches.getBuildChecks().getDateFrom());
								dtobatches.setPayPeriodTo(buildPayrollCheckByBatches.getBuildChecks().getDateTo());
							}
						}
						
						dtobatches.setPostingDate(batches.getPostingDate());
						dtobatches.setQuantityTotal(totalamount);
						dtobatches.setTotalTransactions((Double.valueOf(detail.size())));
						dtobatches.setApproved(batches.isApproved());
						if(batches.isApproved()) {
							dtobatches.setApprovedDate(batches.getApprovedDate());
							dtobatches.setUserID(Integer.parseInt(batches.getUserID()));
						}
						dtobatches.setStatus(batches.getStatus());
						dtobatches.setArabicDescription(batches.getArabicDescription());
						dtoBatches.add(dtobatches);
					}
					
					
				}
				if(dtoSearch.getSortOn().equals("totalTransactions") && dtoSearch.getSortBy().equalsIgnoreCase("ASC")) {
					DtoBatches batches = new DtoBatches();
					DtoBatches.SortByTotalTransactionAsc asc = batches.new SortByTotalTransactionAsc();
					Collections.sort(dtoBatches,asc);
				}
				if(dtoSearch.getSortOn().equals("totalTransactions") && dtoSearch.getSortBy().equalsIgnoreCase("DESC")) {
					DtoBatches batches = new DtoBatches();
					DtoBatches.SortByTotalTransactionDesc desc = batches.new SortByTotalTransactionDesc();
					Collections.sort(dtoBatches,desc);
				}
				dtoSearch.setRecords(dtoBatches);
			}
			log.debug("All Batches List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
}
