package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeDependents;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeDependents;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeDependents")
public class ControllerEmployeeDependents extends BaseController{
	
private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeDependents.class);
	
	@Autowired
	ServiceEmployeeDependents serviceEmployeeDependents;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createEmployeeDependent(HttpServletRequest request, @RequestBody DtoEmployeeDependents dtoEmployeeDependents) throws Exception {
		LOGGER.info("Create EmployeeDependents Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeDependents  = serviceEmployeeDependents.saveOrUpdate(dtoEmployeeDependents);
			responseMessage=displayMessage(dtoEmployeeDependents, "EMPLOYEE_DEPENDENTS_CREATED", "EMPLOYEE_DEPENDENTS_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeeDependents Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeeDependents dtoEmployeeDependents) throws Exception {
		LOGGER.info("Update EmployeeDependents Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeDependents = serviceEmployeeDependents.saveOrUpdate(dtoEmployeeDependents);
			responseMessage=displayMessage(dtoEmployeeDependents, "EMPLOYEE_DEPENDENTS_UPDATED", "EMPLOYEE_DEPENDENTS_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeeDependents Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoEmployeeDependents dtoEmployeeDependents) throws Exception {
		LOGGER.info("Delete EmployeeDependents Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeDependents.getIds() != null && !dtoEmployeeDependents.getIds().isEmpty()) {
				DtoEmployeeDependents dtoEmployeeDependents2 = serviceEmployeeDependents.delete(dtoEmployeeDependents.getIds());
				if(dtoEmployeeDependents2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENTS_DELETED", false), dtoEmployeeDependents2);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_NOT_DELETED", false), dtoEmployeeDependents2);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENTS_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeeDependents Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoEmployeeDependents dtoEmployeeDependents) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeDependents dtoEmployeeDependentsObj = serviceEmployeeDependents.getById(dtoEmployeeDependents.getId());
			responseMessage=displayMessage(dtoEmployeeDependentsObj, "EMPLOYEE_DEPENDENTS_LIST_GET_DETAIL", "EMPLOYEE_DEPENDENTS_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoEmployeeDependents.getId());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeDependents(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeDependents Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeDependents.searchEmployeeDependents(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_DEPENDENTS_GET_ALL", "EMPLOYEE_DEPENDENTS_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllDroupDown", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllDroupDown(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("getAllDroupDown Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeDependents.getAllDroupDown(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_DEPENDENTS_GET_ALL", "EMPLOYEE_DEPENDENTS_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getAllemployeeDependentsDropDownList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllemployeeDependentsDropDownList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeeDependents> list = serviceEmployeeDependents.getAllemployeeDependentsDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_GET_ALL", false), list);
			}else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
		} catch (Exception e) {
			
			LOGGER.error(e);
		}
		return responseMessage;
	}
}
