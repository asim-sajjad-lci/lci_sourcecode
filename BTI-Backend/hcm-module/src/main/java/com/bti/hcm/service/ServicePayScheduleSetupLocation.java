/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Location;
import com.bti.hcm.model.PayScheduleSetup;
import com.bti.hcm.model.PayScheduleSetupLocation;
import com.bti.hcm.model.dto.DtoPayScheduleSetupLocation;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryLocation;
import com.bti.hcm.repository.RepositoryPayScheduleSetup;
import com.bti.hcm.repository.RepositoryPayScheduleSetupLocation;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service PayScheduleSetupLocation
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("servicePayScheduleSetupLocation")
public class ServicePayScheduleSetupLocation {
	
	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupLocation Service
	 */
	static Logger log = Logger.getLogger(ServicePayScheduleSetupLocation.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in PayScheduleSetupLocation service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PayScheduleSetupLocation service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PayScheduleSetupLocation service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPayScheduleSetupLocation Autowired here using annotation of spring for access of repositoryPayScheduleSetupLocation method in PayScheduleSetupLocation service
	 * 				In short Access PayScheduleSetupLocation Query from Database using repositoryPayScheduleSetupLocation.
	 */
	@Autowired
	RepositoryPayScheduleSetupLocation repositoryPayScheduleSetupLocation;
	
	@Autowired
	RepositoryPayScheduleSetup repositoryPayScheduleSetup;
	
	@Autowired
	RepositoryLocation repositoryLocation;
	
	/**
	 * @param dtoPayScheduleSetupLocation
	 * @return
	 * @throws ParseException
	 */
	public DtoPayScheduleSetupLocation saveOrUpdatePayScheduleSetupLocation (DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation) throws ParseException {
		log.info("saveOrUpdatePayScheduleSetupPosition Method");
		PayScheduleSetupLocation payScheduleSetupLocation = null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
			PayScheduleSetup payScheduleSetup = repositoryPayScheduleSetup.findOne(dtoPayScheduleSetupLocation.getPayScheduleSetupId());
			Location location = repositoryLocation.findOne(dtoPayScheduleSetupLocation.getLocationId());
			if (dtoPayScheduleSetupLocation.getPayScheduleSetupLocationId() != null && dtoPayScheduleSetupLocation.getPayScheduleSetupLocationId() > 0) {
				payScheduleSetupLocation = repositoryPayScheduleSetupLocation.findByPayScheduleSetupLocationIdAndIsDeleted(dtoPayScheduleSetupLocation.getPayScheduleSetupLocationId(), false);
				payScheduleSetupLocation.setUpdatedBy(loggedInUserId);
				payScheduleSetupLocation.setUpdatedDate(new Date());
			} else {
				payScheduleSetupLocation = new PayScheduleSetupLocation();
				payScheduleSetupLocation.setCreatedDate(new Date());
				
				Integer rowId = repositoryPayScheduleSetupLocation.getCountOfTotaPayScheduleSetupLocation();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				payScheduleSetupLocation.setRowId(increment);
			}
			payScheduleSetupLocation.setLocation(location);
			payScheduleSetupLocation.setLocationName(dtoPayScheduleSetupLocation.getLocationName());
			payScheduleSetupLocation.setPayScheduleStatus(dtoPayScheduleSetupLocation.getPayScheduleStatus());
			payScheduleSetupLocation.setPayScheduleSetup(payScheduleSetup);
			payScheduleSetupLocation.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			payScheduleSetupLocation = repositoryPayScheduleSetupLocation.saveAndFlush(payScheduleSetupLocation);
			log.debug("PayScheduleSetupLocation is:"+dtoPayScheduleSetupLocation.getPayScheduleSetupLocationId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayScheduleSetupLocation;
	}
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPayScheduleSetupLocation(DtoSearch dtoSearch) {
		log.info("searchPayScheduleSetupLocation Method");
		
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "locationName" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "payScheduleStatus" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "location" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "payScheduleSetup" : 
					condition+=dtoSearch.getSortOn();
					break;	
				default:
					condition+="paySchedulePositionIndexId";
				}
			}else{
				condition+="payScheduleSetupLocationId";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositoryPayScheduleSetupLocation.predictivePayScheduleSetupLocationSearchTotalCount("%"+searchWord+"%"));
				List<PayScheduleSetupLocation> payScheduleSetupLocationList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						payScheduleSetupLocationList = this.repositoryPayScheduleSetupLocation.predictivePayScheduleSetupLocationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						payScheduleSetupLocationList = this.repositoryPayScheduleSetupLocation.predictivePayScheduleSetupLocationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						payScheduleSetupLocationList = this.repositoryPayScheduleSetupLocation.predictivePayScheduleSetupLocationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(payScheduleSetupLocationList != null && !payScheduleSetupLocationList.isEmpty()){
					List<DtoPayScheduleSetupLocation> dtoPayScheduleSetupLocationList = new ArrayList<DtoPayScheduleSetupLocation>();
					DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation=null;
					for (PayScheduleSetupLocation payScheduleSetupLocation : payScheduleSetupLocationList) {
						dtoPayScheduleSetupLocation = new DtoPayScheduleSetupLocation(payScheduleSetupLocation);
						dtoPayScheduleSetupLocation.setLocationName(payScheduleSetupLocation.getLocationName());
						dtoPayScheduleSetupLocation.setPayScheduleStatus(payScheduleSetupLocation.getPayScheduleStatus());
						dtoPayScheduleSetupLocationList.add(dtoPayScheduleSetupLocation);
					}
					dtoSearch.setRecords(dtoPayScheduleSetupLocationList);
				}
			}
			log.debug("Search PayScheduleSetupLocation Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoPayScheduleSetupLocation deletePayScheduleSetupLocation(List<Integer> ids) {
		log.info("deletePayScheduleSetupLocation Method");
		DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation = new DtoPayScheduleSetupLocation();
		try {
			dtoPayScheduleSetupLocation
					.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_LOCATION_DELETED", false));
			dtoPayScheduleSetupLocation.setAssociateMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_LOCATION_ASSOCIATED", false));
			List<DtoPayScheduleSetupLocation> deletePayScheduleSetupLocation = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
			try {
				for (Integer payScheduleSetupLocationId : ids) {
					PayScheduleSetupLocation payScheduleSetupLocation = repositoryPayScheduleSetupLocation.findOne(payScheduleSetupLocationId);
					DtoPayScheduleSetupLocation dtoPayScheduleSetupLocation2 = new DtoPayScheduleSetupLocation();
					dtoPayScheduleSetupLocation2.setLocationName(payScheduleSetupLocation.getLocationName());
					dtoPayScheduleSetupLocation2.setPayScheduleStatus(payScheduleSetupLocation.getPayScheduleStatus());
					dtoPayScheduleSetupLocation2.setPayScheduleSetupLocationId(payScheduleSetupLocationId);
					repositoryPayScheduleSetupLocation.deleteSinglPayScheduleSetupLocation(true, loggedInUserId, payScheduleSetupLocationId);
					deletePayScheduleSetupLocation.add(dtoPayScheduleSetupLocation2);

				}
				dtoPayScheduleSetupLocation.setDeleteDtoPayScheduleSetupLocation(deletePayScheduleSetupLocation);
			} catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete deletePayScheduleSetupLocation :"+dtoPayScheduleSetupLocation.getPayScheduleSetupLocationId());
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
		}
		
		return dtoPayScheduleSetupLocation;
	}

}
