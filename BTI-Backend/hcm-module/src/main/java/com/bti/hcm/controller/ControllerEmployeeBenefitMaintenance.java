package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeBenefitMaintenance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeBenefitMaintenance;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeBenefitMaintenance")
public class ControllerEmployeeBenefitMaintenance extends BaseController {

	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeBenefitMaintenance.class);

	@Autowired
	ServiceEmployeeBenefitMaintenance serviceEmployeeBenefitMaintenance;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createEmployeeDependent(HttpServletRequest request,
			@RequestBody DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance) throws Exception {
		LOGGER.info("Create EmployeeBenefitMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeBenefitMaintenance = serviceEmployeeBenefitMaintenance
					.saveOrUpdate(dtoEmployeeBenefitMaintenance);
			responseMessage = displayMessage(dtoEmployeeBenefitMaintenance, "EMPLOYEE_BENEFIT_MAINTENENCE_CREATED",
					"EMPLOYEE_BENEFIT_MAINTENENCE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeeBenefitMaintenance Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request,
			@RequestBody DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance) throws Exception {
		LOGGER.info("Update EmployeeBenefitMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeBenefitMaintenance = serviceEmployeeBenefitMaintenance
					.saveOrUpdate(dtoEmployeeBenefitMaintenance);
			responseMessage = displayMessage(dtoEmployeeBenefitMaintenance, "EMPLOYEE_BENEFIT_MAINTENENCE_UPDATED",
					"EMPLOYEE_BENEFIT_MAINTENENCE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeeBenefitMaintenance Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request,
			@RequestBody DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance) throws Exception {
		LOGGER.info("Delete EmployeeBenefitMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeBenefitMaintenance.getIds() != null && !dtoEmployeeBenefitMaintenance.getIds().isEmpty()) {
				DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance2 = serviceEmployeeBenefitMaintenance
						.delete(dtoEmployeeBenefitMaintenance.getIds());
				if (dtoEmployeeBenefitMaintenance2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(
							HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
									.getMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTENENCE_DELETED", false),
							dtoEmployeeBenefitMaintenance2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, serviceResponse
							.getMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTENENCE_NOT_DELETED", false),
							dtoEmployeeBenefitMaintenance2);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTENENCE_LIST_IS_EMPTY",
								false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeeBenefitMaintenance Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request,
			@RequestBody DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenanceObj = serviceEmployeeBenefitMaintenance
					.getById(dtoEmployeeBenefitMaintenance.getId());
			responseMessage = displayMessage(dtoEmployeeBenefitMaintenanceObj,
					"EMPLOYEE_BENEFIT_MAINTENENCE_LIST_GET_DETAIL", "EMPLOYEE_BENEFIT_MAINTENENCE_NOT_GETTING",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:" + dtoEmployeeBenefitMaintenance.getId());
		return responseMessage;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeBenefitMaintenance(@RequestBody DtoSearch dtoSearch,
			HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeBenefitMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeBenefitMaintenance.searchEmployeeBenefitMaintenance(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "EMPLOYEE_BENEFIT_MAINTENENCE_GET_ALL",
					"EMPLOYEE_BENEFIT_MAINTENENCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllData(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeBenefitMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeBenefitMaintenance.getAllData(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "EMPLOYEE_BENEFIT_MAINTENENCE_GET_ALL",
					"EMPLOYEE_BENEFIT_MAINTENENCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@PostMapping("/validate")
	public ResponseMessage validate(@RequestBody DtoEmployeeBenefitMaintenance dto, HttpServletRequest request)
			throws Exception {
		LOGGER.info("benefit code with employee id Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance = serviceEmployeeBenefitMaintenance
					.validate(dto.getEmployeeMasters().getEmployeeIndexId(), dto.getBenefitCode().getId());
			responseMessage = displayMessage(dtoEmployeeBenefitMaintenance, "EMPLOYEE_BENEFIT_MAINTENANCE_RESULT",
					"NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
