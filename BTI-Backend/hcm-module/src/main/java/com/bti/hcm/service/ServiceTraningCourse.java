 package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.TrainingBatchSignup;
import com.bti.hcm.model.TrainingCourseDetail;
import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchByTrainingId;
import com.bti.hcm.model.dto.DtoTrainingCourseDetail;
import com.bti.hcm.model.dto.DtoTraningCourse;
import com.bti.hcm.repository.RepositoryTrainingCourseDetail;
import com.bti.hcm.repository.RepositoryTraningCourse;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceTraningCourse")
public class ServiceTraningCourse {

	static Logger log = Logger.getLogger(ServiceRetirementFundSetup.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryTraningCourse repositoryTraningCourse;
	
	@Autowired
	RepositoryTrainingCourseDetail repositoryTrainingCourseDetail;
	
	
	
	public DtoTraningCourse saveOrUpdateTraningCourse(DtoTraningCourse dtoTraningCourse) {
		log.info("enter into save or update TraningCourse Schdule");
		
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			TraningCourse traningCourse=null;
			if (dtoTraningCourse.getId() != null && dtoTraningCourse.getId() > 0) {
				traningCourse = repositoryTraningCourse.findByIdAndIsDeleted(dtoTraningCourse.getId(), false);
				traningCourse.setUpdatedBy(loggedInUserId);
				traningCourse.setUpdatedDate(new Date());
			} else {
				traningCourse = new TraningCourse();
				traningCourse.setCreatedDate(new Date());
				
				Integer rowId = repositoryTraningCourse.getCountOfTotalTraningCourse();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				traningCourse.setRowId(increment);
			}
			
			traningCourse.setTraningId(dtoTraningCourse.getTraningId());
			traningCourse.setDesc(dtoTraningCourse.getDesc());
			traningCourse.setArbicDesc(dtoTraningCourse.getArbicDesc());
			traningCourse.setLocation(dtoTraningCourse.getLocation());
	        traningCourse.setPrerequisiteId(dtoTraningCourse.getPrerequisiteId());
	        traningCourse.setEmployeeCost(dtoTraningCourse.getEmployeeCost());
	        traningCourse.setEmployerCost(dtoTraningCourse.getEmployerCost());
	        traningCourse.setSupplierCost(dtoTraningCourse.getSupplierCost());
	        traningCourse.setInstructorCost(dtoTraningCourse.getInstructorCost());
	        traningCourse.setCourseInCompany(dtoTraningCourse.isCourseInCompany());
		
	        traningCourse.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryTraningCourse.saveAndFlush(traningCourse);
			
			List<TrainingCourseDetail> trainingCourseDetailList = repositoryTrainingCourseDetail.findByTrainingCourseAndIsDeleted(traningCourse.getId());
			for(TrainingCourseDetail trainingCourseDetail : trainingCourseDetailList) {
				repositoryTrainingCourseDetail.deleteSingleTraningCourseDetail(true, loggedInUserId,trainingCourseDetail.getId());	
			}
				for (DtoTrainingCourseDetail dtoTrainingCourseDetail : dtoTraningCourse.getSubItems()) {
					
					TrainingCourseDetail trainingCourseDetail = new TrainingCourseDetail(); 
					
					trainingCourseDetail.setTrainingCourse(traningCourse);
					trainingCourseDetail.setClassId(dtoTrainingCourseDetail.getClassId());
					trainingCourseDetail.setClassName(dtoTrainingCourseDetail.getClassName());
					trainingCourseDetail.setStartDate(dtoTrainingCourseDetail.getStartDate());
					trainingCourseDetail.setEndDate(dtoTrainingCourseDetail.getEndDate());
					trainingCourseDetail.setStartTime(dtoTrainingCourseDetail.getStartTime());
					trainingCourseDetail.setEndTime(dtoTrainingCourseDetail.getEndTime());
					trainingCourseDetail.setInstructorName(dtoTrainingCourseDetail.getInstructorName());
					trainingCourseDetail.setEnrolled(dtoTrainingCourseDetail.getEnrolled());
					trainingCourseDetail.setMaximum(dtoTrainingCourseDetail.getMaximum());
					trainingCourseDetail.setClassLocation(dtoTrainingCourseDetail.getClassLocation());
					
					repositoryTrainingCourseDetail.saveAndFlush(trainingCourseDetail);
				}
			
		}catch (Exception e) {
			log.error(e);
		}
		
			return dtoTraningCourse;
	}
	
	
	
	
	
	public DtoTraningCourse deleteTraningCourse(List<Integer> ids) {
		log.info("delete TraningCourse Method");
		DtoTraningCourse dtoTraningCourse = new DtoTraningCourse();
		
		try {
			dtoTraningCourse.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRAININGCOURSE_DELETED", false));
			dtoTraningCourse.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRAININGCOURSE_DELETED", false));
			List<DtoTraningCourse> deleteTraningCourse = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TRAININGCOURSE_NOT_DELETE_ID_MESSAGE", false).getMessage());
			try {
				for (Integer id : ids) {
					TraningCourse traningCourse = repositoryTraningCourse.findOne(id);
					if(traningCourse!=null && traningCourse.getTrainingBatchSignupList().isEmpty()) {
						DtoTraningCourse dtoTraningCourse2 = new DtoTraningCourse();
						dtoTraningCourse2.setId(traningCourse.getId());
						
						dtoTraningCourse2.setTraningId(traningCourse.getTraningId());
						dtoTraningCourse2.setDesc(traningCourse.getDesc());
						dtoTraningCourse2.setArbicDesc(traningCourse.getArbicDesc());
						dtoTraningCourse2.setLocation(traningCourse.getLocation());
						dtoTraningCourse2.setPrerequisiteId(traningCourse.getPrerequisiteId());
						dtoTraningCourse2.setEmployeeCost(traningCourse.getEmployeeCost());
						dtoTraningCourse2.setEmployerCost(traningCourse.getEmployerCost());
						dtoTraningCourse2.setSupplierCost(traningCourse.getSupplierCost());
						dtoTraningCourse2.setInstructorCost(traningCourse.getInstructorCost());
						repositoryTraningCourse.deleteSingleTraningCourse(true, loggedInUserId, id);
						deleteTraningCourse.add(dtoTraningCourse2);
					}else {
						inValidDelete = true;
					}
				

				}
				dtoTraningCourse.setDelete(deleteTraningCourse);
				if(inValidDelete){
					invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
					dtoTraningCourse.setMessageType(invlidDeleteMessage.toString());
					
				}
				if(!inValidDelete){
					dtoTraningCourse.setMessageType("");
					
				}
				dtoTraningCourse.setDelete(deleteTraningCourse);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
			} catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete TraningCourse :"+dtoTraningCourse.getId());
	
		}catch (Exception e) {
			log.error(e);
		}
		
				return dtoTraningCourse;
	}	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchTraningCourse(DtoSearch dtoSearch) {
		log.info("search TraningCourse Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("traningId") || dtoSearch.getSortOn().equals("desc")|| dtoSearch.getSortOn().equals("arbicDesc")|| dtoSearch.getSortOn().equals("location")|| dtoSearch.getSortOn().equals("prerequisiteId")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				
				dtoSearch.setTotalCount(this.repositoryTraningCourse.predictiveTraningCourseSearchTotalCount("%"+searchWord+"%"));
				List<TraningCourse> traningCourseList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						traningCourseList = this.repositoryTraningCourse.predictiveTraningCourseSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						traningCourseList = this.repositoryTraningCourse.predictiveTraningCourseSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						traningCourseList = this.repositoryTraningCourse.predictiveTraningCourseSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(traningCourseList != null && !traningCourseList.isEmpty()){
					List<DtoTraningCourse> dtoTraningCourseList = new ArrayList<>();
					DtoTraningCourse dtoTraningCourse=null;
					for (TraningCourse traningCourse : traningCourseList) {
						dtoTraningCourse = new DtoTraningCourse(traningCourse);
						
						
						
						dtoTraningCourse =  dtoTraningCourse(dtoTraningCourse,traningCourse);	
						List<DtoTrainingCourseDetail> listDtoTrainingCourseDetail = new ArrayList<>();
						
						for (TrainingCourseDetail trainingCourseDetail : traningCourse.getListTrainingCourseDetail()) {
							DtoTrainingCourseDetail dtoTrainingCourseDetail = new DtoTrainingCourseDetail();
							dtoTrainingCourseDetail = getDtoTrainingCourseDetail(dtoTrainingCourseDetail,trainingCourseDetail);
							listDtoTrainingCourseDetail.add(dtoTrainingCourseDetail);
						}
						dtoTraningCourse.setSubItems(listDtoTrainingCourseDetail);
						
						dtoTraningCourseList.add(dtoTraningCourse);
						}		
					dtoSearch.setRecords(dtoTraningCourseList);
					}
					
				}
			
			log.debug("Search TraningCourse Size is:"+dtoSearch.getTotalCount());
			
		}catch (Exception e) {
			log.error(e);
		}
		
			return dtoSearch;
		}
	
	public DtoTraningCourse dtoTraningCourse(DtoTraningCourse dtoTraningCourse,TraningCourse traningCourse) {
		try {
			dtoTraningCourse.setId(traningCourse.getId());
			dtoTraningCourse.setTraningId(traningCourse.getTraningId());
			dtoTraningCourse.setDesc(traningCourse.getDesc());
			dtoTraningCourse.setArbicDesc(traningCourse.getArbicDesc());
			dtoTraningCourse.setLocation(traningCourse.getLocation());
			dtoTraningCourse.setPrerequisiteId(traningCourse.getPrerequisiteId());
			dtoTraningCourse.setEmployeeCost(traningCourse.getEmployeeCost());
			dtoTraningCourse.setEmployerCost(traningCourse.getEmployerCost());
			dtoTraningCourse.setSupplierCost(traningCourse.getSupplierCost());
			dtoTraningCourse.setInstructorCost(traningCourse.getInstructorCost());
			dtoTraningCourse.setCourseInCompany(traningCourse.isCourseInCompany());	
			
		}catch (Exception e) {
			log.error(e);
		}
			return dtoTraningCourse;
	}
	
	
	
	
	public DtoTraningCourse getTraningCourseById1(int id) {
		DtoTraningCourse dtoTraningCourse  = new DtoTraningCourse();
		try {
			if (id > 0) {
				TraningCourse traningCourse = repositoryTraningCourse.findByIdAndIsDeleted(id, false);
				if (traningCourse != null) {
					dtoTraningCourse = new DtoTraningCourse(traningCourse);
					
					dtoTraningCourse =  dtoTraningCourse(dtoTraningCourse,traningCourse);
				
					List<DtoTrainingCourseDetail> listDtoTrainingCourseDetail = new ArrayList<>();
					
					for (TrainingCourseDetail trainingCourseDetail : traningCourse.getListTrainingCourseDetail()) {
						DtoTrainingCourseDetail dtoTrainingCourseDetail = new DtoTrainingCourseDetail();
						
						
						dtoTrainingCourseDetail = getDtoTrainingCourseDetail(dtoTrainingCourseDetail,trainingCourseDetail);
						dtoTrainingCourseDetail.setStartDate(trainingCourseDetail.getStartDate());
						dtoTrainingCourseDetail.setId(trainingCourseDetail.getId());
						dtoTrainingCourseDetail.setClassId(trainingCourseDetail.getClassId());
						dtoTrainingCourseDetail.setClassName(trainingCourseDetail.getClassName());
						dtoTrainingCourseDetail.setStartDate(trainingCourseDetail.getStartDate());
						dtoTrainingCourseDetail.setEndDate(trainingCourseDetail.getEndDate());
						dtoTrainingCourseDetail.setStartTime(trainingCourseDetail.getStartTime());
						dtoTrainingCourseDetail.setEndTime(trainingCourseDetail.getEndTime());
						dtoTrainingCourseDetail.setInstructorName(trainingCourseDetail.getInstructorName());
						dtoTrainingCourseDetail.setEnrolled(trainingCourseDetail.getEnrolled());
						dtoTrainingCourseDetail.setMaximum(trainingCourseDetail.getMaximum());
						dtoTrainingCourseDetail.setClassLocation(trainingCourseDetail.getClassLocation());
						listDtoTrainingCourseDetail.add(dtoTrainingCourseDetail);
					}
					dtoTraningCourse.setSubItems(listDtoTrainingCourseDetail);
				} else {
					dtoTraningCourse.setMessageType("TRAININGCOURSE_LIST_NOT_GETTING");

				}
			} else {
				dtoTraningCourse.setMessageType("TRAININGCOURSE_LIST_NOT_GETTING");

			}
	
		}catch (Exception e) {
			log.error(e);
		}
				return dtoTraningCourse;
	}
	
	
	public DtoSearch getTraningCourseById(int id) {
		
		DtoSearch dtoSearch= new DtoSearch();
		
		try {
			DtoTraningCourse dtoTraningCourse  = new DtoTraningCourse();
			if (id > 0) {
				TraningCourse traningCourse = repositoryTraningCourse.findByIdAndIsDeleted(id, false);
				if (traningCourse != null) {
					dtoTraningCourse = new DtoTraningCourse(traningCourse);
					
					dtoTraningCourse =  dtoTraningCourse(dtoTraningCourse,traningCourse);
					
					
					List<DtoTrainingCourseDetail> listDtoTrainingCourseDetail = new ArrayList<>();
					
					for (TrainingCourseDetail trainingCourseDetail : traningCourse.getListTrainingCourseDetail()) {
						DtoTrainingCourseDetail dtoTrainingCourseDetail = new DtoTrainingCourseDetail();
						dtoTrainingCourseDetail = getDtoTrainingCourseDetail(dtoTrainingCourseDetail,trainingCourseDetail);
						Set<EmployeeMaster> employeeMasters = new HashSet<>(); 
						String employeeId = "";
						String employeeName = "";
						
						for (TrainingBatchSignup trainingBatchSignup : traningCourse.getTrainingBatchSignupList()) {
							
							for (EmployeeMaster dtoEmployeeMaster : trainingBatchSignup.getEmployeeMaster()) {
								
								employeeMasters.add(dtoEmployeeMaster);
							}
						}
						
						for (EmployeeMaster employeeMaster : employeeMasters) {
							employeeId += employeeMaster.getEmployeeId() + " ";
							employeeName += employeeMaster.getEmployeeFirstName() + " ";
						}
						
						dtoTrainingCourseDetail.setEmployeeId(employeeId);
						dtoTrainingCourseDetail.setEmployeeName(employeeName);
						
						
						listDtoTrainingCourseDetail.add(dtoTrainingCourseDetail);
					}
					
					
					dtoTraningCourse.setSubItems(listDtoTrainingCourseDetail);
					dtoSearch.setRecords(dtoTraningCourse);
				} else {
					dtoTraningCourse.setMessageType("TRAININGCOURSE_LIST_NOT_GETTING");

				}
			} else {
				dtoTraningCourse.setMessageType("TRAININGCOURSE_LIST_NOT_GETTING");

			}
	
		}catch (Exception e) {
			log.error(e);
		}
		
				return dtoSearch;
	}

	
	
	
	public DtoTraningCourse repeatByTraningId(String traningId) {
		log.info("repeatByTraningId Method");
		DtoTraningCourse dtoTraningCourse = new DtoTraningCourse();
		try {
			List<TraningCourse> traningCourse=repositoryTraningCourse.findByTrainingCourseTraningId(traningId);
			if(traningCourse!=null && !traningCourse.isEmpty()) {
				dtoTraningCourse.setIsRepeat(true);
				dtoTraningCourse.setTraningId(traningId);
			}else {
				dtoTraningCourse.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoTraningCourse;
	}
	

	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllTraningCourseId(DtoSearch dtoSearch) {
		try {
			if(dtoSearch != null){

				List<TraningCourse> traningCourseList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
						traningCourseList = this.repositoryTraningCourse.findByIsDeleted(false);
				}
				
				
				if(traningCourseList != null && !traningCourseList.isEmpty()){
					List<DtoTraningCourse> dtoTraningCourseList = new ArrayList<>();
					DtoTraningCourse dtoTraningCourse=null;
					for (TraningCourse traningCourse : traningCourseList) {
						dtoTraningCourse = new DtoTraningCourse(traningCourse);

						dtoTraningCourse =  dtoTraningCourse(dtoTraningCourse,traningCourse);
						dtoTraningCourseList.add(dtoTraningCourse);
						}		
					dtoSearch.setRecords(dtoTraningCourseList);
					}
					
				}
			
	
		}catch (Exception e) {
			log.error(e);
		}
				return dtoSearch;
		}


	public DtoSearchByTrainingId searchTraningId(DtoSearchByTrainingId dtoSearchByTrainingId) {
		log.info("searchTraningId Method");
		try {
			String trainingId= dtoSearchByTrainingId.getTrainingId();
			if(trainingId != null){
				
				
				List<String> orientationIdList = this.repositoryTraningCourse.predictiveSearchTraningIdWithPagination(dtoSearchByTrainingId.getTrainingId());
					if(!orientationIdList.isEmpty()) {
						dtoSearchByTrainingId.setTotalCount(orientationIdList.size());
						dtoSearchByTrainingId.setRecords(orientationIdList.size());
					}
					dtoSearchByTrainingId.setIds(orientationIdList);
			}
		} catch (Exception e) {
			log.error(e);
		}
		
		
		log.debug("searchTraningId Size is:"+dtoSearchByTrainingId.getTotalCount());
		return dtoSearchByTrainingId;
	}


	public DtoSearch searchAllTraningId(DtoSearch dtoSearch) {
		log.info("searchAllTraningId Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				
				 List<TraningCourse> traningCourseList = this.repositoryTraningCourse.predictiveSearchAllTraningIdWithPagination("%"+searchWord+"%");
					if(!traningCourseList.isEmpty()) {
						
						
						
						
						if(!traningCourseList.isEmpty()){
							List<DtoTraningCourse> dtoTraningCourse = new ArrayList<>();
							DtoTraningCourse dtoTraningCourse1=null;
							for (TraningCourse traningCourse : traningCourseList) {
								dtoTraningCourse1 = new DtoTraningCourse(traningCourse);
								
								dtoTraningCourse1.setId(traningCourse.getId());
								dtoTraningCourse1.setTraningId(traningCourse.getTraningId());
								dtoTraningCourse1.setDesc(traningCourse.getTraningId()+" | "+traningCourse.getDesc());
								
								dtoTraningCourse.add(dtoTraningCourse1);
							}
							dtoSearch.setRecords(dtoTraningCourse);
						}

						dtoSearch.setTotalCount(traningCourseList.size());
					}
				
			}
			
	
		}catch (Exception e) {
			log.error(e);
		}
		
				return dtoSearch;
	}
	
	public DtoTrainingCourseDetail getDtoTrainingCourseDetail(DtoTrainingCourseDetail dtoTrainingCourseDetail,TrainingCourseDetail trainingCourseDetail){
		try {
			dtoTrainingCourseDetail.setId(trainingCourseDetail.getId());
			dtoTrainingCourseDetail.setClassId(trainingCourseDetail.getClassId());
			dtoTrainingCourseDetail.setClassName(trainingCourseDetail.getClassName());
			dtoTrainingCourseDetail.setStartDate(trainingCourseDetail.getStartDate());
			dtoTrainingCourseDetail.setEndDate(trainingCourseDetail.getEndDate());
			dtoTrainingCourseDetail.setStartTime(trainingCourseDetail.getStartTime());
			dtoTrainingCourseDetail.setEndTime(trainingCourseDetail.getEndTime());
			dtoTrainingCourseDetail.setInstructorName(trainingCourseDetail.getInstructorName());
			dtoTrainingCourseDetail.setEnrolled(trainingCourseDetail.getEnrolled());
			dtoTrainingCourseDetail.setMaximum(trainingCourseDetail.getMaximum());
			dtoTrainingCourseDetail.setClassLocation(trainingCourseDetail.getClassLocation());
			
		}catch (Exception e) {
			log.error(e);
		}
			return dtoTrainingCourseDetail;
	}
	
	
	
	}
	
	
	


