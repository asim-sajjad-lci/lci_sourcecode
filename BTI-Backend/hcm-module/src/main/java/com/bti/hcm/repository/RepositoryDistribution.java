package com.bti.hcm.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Distribution;

@Repository("/repositoryDistribution")
public interface RepositoryDistribution extends JpaRepository<Distribution, Integer>{

	@Query("select d from Distribution d where (d.employeeMaster.employeeIndexId =:id) and d.isDeleted=false")
	public List<Distribution> getAllList(@Param("id") Integer searchKeyWord);
	
	@Query("select d from Distribution d where d.isDeleted=false  ORDER BY d.id desc")
	public List<Distribution> getAll();
	
	
	@Query("select d from Distribution d where (d.default1.id =:id) and d.isDeleted=false")
	public List<Distribution> getAllByDefaultId(@Param("id") Integer defaultId);
	
	@Query("select d from Distribution d where (d.buildChecks.id =:id) and d.isDeleted=false")
	public List<Distribution> getAllByBuildId(@Param("id") Integer buildId);

	@Query("select d from Distribution d where (d.employeeMaster.employeeIndexId =:id) and d.isDeleted=false")
	public List<Distribution> getAllByEmployeeId(@Param("id") Integer buildId);
	
	@Query("select d from Distribution d where (d.employeeMaster.employeeIndexId =:id) AND MONTH(d.updatedDate) = :month AND YEAR(d.updatedDate) = :year and d.isDeleted=false")
	public List<Distribution> getAllByEmployeeIdAndMonthAndYear(@Param("id") Integer buildId,@Param("month") Integer month, @Param("year") Integer year);
	
	
	@Query("select d from Distribution d where d.transactionEntry.id in(:list) AND d.isDeleted=false")
	public Set<Distribution> findByDistributionByTransactionId(@Param("list") Set<Integer> listIds);
}
