package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PayrollSetup;

public class DtoPayrollSetup extends DtoBase{

	private Integer id;
	private DtoFinancialDimensions departmentDimensions;
	private DtoFinancialDimensions employeeDimensions;
	private DtoFinancialDimensions projectDimensions;
	private DtoCheckbookMaintenance payrollCheckbook;
	private DtoCheckbookMaintenance manualCheckCheckbook;
	private DtoVatSetup vatSetup;
	private Integer nextPaymentNumber;
	private Integer checkNumber;
	private boolean multiPayrollProcesses;
	private boolean jvPerEmployee;
	private boolean displayPayRate;
	private boolean codeswithYTDAmounts;
	private List<DtoPayrollSetup> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public DtoFinancialDimensions getDepartmentDimensions() {
		return departmentDimensions;
	}
	public void setDepartmentDimensions(DtoFinancialDimensions departmentDimensions) {
		this.departmentDimensions = departmentDimensions;
	}
	public DtoFinancialDimensions getEmployeeDimensions() {
		return employeeDimensions;
	}
	public void setEmployeeDimensions(DtoFinancialDimensions employeeDimensions) {
		this.employeeDimensions = employeeDimensions;
	}
	public DtoFinancialDimensions getProjectDimensions() {
		return projectDimensions;
	}
	public void setProjectDimensions(DtoFinancialDimensions projectDimensions) {
		this.projectDimensions = projectDimensions;
	}
	public Integer getNextPaymentNumber() {
		return nextPaymentNumber;
	}
	public void setNextPaymentNumber(Integer nextPaymentNumber) {
		this.nextPaymentNumber = nextPaymentNumber;
	}
	public Integer getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}
	public boolean isMultiPayrollProcesses() {
		return multiPayrollProcesses;
	}
	public void setMultiPayrollProcesses(boolean multiPayrollProcesses) {
		this.multiPayrollProcesses = multiPayrollProcesses;
	}
	public boolean isJvPerEmployee() {
		return jvPerEmployee;
	}
	public void setJvPerEmployee(boolean jvPerEmployee) {
		this.jvPerEmployee = jvPerEmployee;
	}
	public boolean isDisplayPayRate() {
		return displayPayRate;
	}
	public void setDisplayPayRate(boolean displayPayRate) {
		this.displayPayRate = displayPayRate;
	}
	public boolean isCodeswithYTDAmounts() {
		return codeswithYTDAmounts;
	}
	public void setCodeswithYTDAmounts(boolean codeswithYTDAmounts) {
		this.codeswithYTDAmounts = codeswithYTDAmounts;
	}
	
	public DtoCheckbookMaintenance getPayrollCheckbook() {
		return payrollCheckbook;
	}
	public void setPayrollCheckbook(DtoCheckbookMaintenance payrollCheckbook) {
		this.payrollCheckbook = payrollCheckbook;
	}
	public DtoCheckbookMaintenance getManualCheckCheckbook() {
		return manualCheckCheckbook;
	}
	public void setManualCheckCheckbook(DtoCheckbookMaintenance manualCheckCheckbook) {
		this.manualCheckCheckbook = manualCheckCheckbook;
	}
	public DtoVatSetup getVatSetup() {
		return vatSetup;
	}
	public void setVatSetup(DtoVatSetup vatSetup) {
		this.vatSetup = vatSetup;
	}
	
	public List<DtoPayrollSetup> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoPayrollSetup> delete) {
		this.delete = delete;
	}
	public DtoPayrollSetup() {
	}
	
	public DtoPayrollSetup(PayrollSetup payrollSetup) {
	}
}
