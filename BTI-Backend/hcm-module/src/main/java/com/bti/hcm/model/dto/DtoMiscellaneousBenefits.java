package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.bti.hcm.model.MiscellaneousBenefits;
import com.bti.hcm.util.UtilRandomKey;

public class DtoMiscellaneousBenefits extends DtoBase{

	private Integer id;
	private String benefitsId;
	private String desc;
	private String arbicDesc;
	private short frequency;
	private Date startDate;
	private Date endDate;
	private boolean inactive;
	private short method;
	private BigDecimal dudctionAmount;	
	private BigDecimal dudctionPercent;	
	private BigDecimal monthlyAmount;
	private BigDecimal yearlyAmount;
	private BigDecimal lifetimeAmount;
	private boolean empluyeeerinactive;
	private short empluyeeermethod;
	private BigDecimal benefitAmount;	
	private BigDecimal benefitPercent;	
	private BigDecimal employermonthlyAmount;
	private BigDecimal employeryearlyAmount;
	private BigDecimal employerlifetimeAmount;
	private List<DtoMiscellaneousBenefits> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getBenefitsId() {
		return benefitsId;
	}
	public void setBenefitsId(String benefitsId) {
		this.benefitsId = benefitsId;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getArbicDesc() {
		return arbicDesc;
	}
	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public boolean isInactive() {
		return inactive;
	}
	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}
	
	public BigDecimal getDudctionAmount() {
		return dudctionAmount;
	}
	public void setDudctionAmount(BigDecimal dudctionAmount) {
		this.dudctionAmount = dudctionAmount;
	}
	public BigDecimal getDudctionPercent() {
		return dudctionPercent;
	}
	public void setDudctionPercent(BigDecimal dudctionPercent) {
		this.dudctionPercent = dudctionPercent;
	}
	public BigDecimal getMonthlyAmount() {
		return monthlyAmount;
	}
	public void setMonthlyAmount(BigDecimal monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}
	public BigDecimal getYearlyAmount() {
		return yearlyAmount;
	}
	public void setYearlyAmount(BigDecimal yearlyAmount) {
		this.yearlyAmount = yearlyAmount;
	}
	public BigDecimal getLifetimeAmount() {
		return lifetimeAmount;
	}
	public void setLifetimeAmount(BigDecimal lifetimeAmount) {
		this.lifetimeAmount = lifetimeAmount;
	}
	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}
	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}
	public BigDecimal getBenefitPercent() {
		return benefitPercent;
	}
	public void setBenefitPercent(BigDecimal benefitPercent) {
		this.benefitPercent = benefitPercent;
	}
	public BigDecimal getEmployermonthlyAmount() {
		return employermonthlyAmount;
	}
	public void setEmployermonthlyAmount(BigDecimal employermonthlyAmount) {
		this.employermonthlyAmount = employermonthlyAmount;
	}
	public BigDecimal getEmployeryearlyAmount() {
		return employeryearlyAmount;
	}
	public void setEmployeryearlyAmount(BigDecimal employeryearlyAmount) {
		this.employeryearlyAmount = employeryearlyAmount;
	}
	public BigDecimal getEmployerlifetimeAmount() {
		return employerlifetimeAmount;
	}
	public void setEmployerlifetimeAmount(BigDecimal employerlifetimeAmount) {
		this.employerlifetimeAmount = employerlifetimeAmount;
	}
	public boolean isEmpluyeeerinactive() {
		return empluyeeerinactive;
	}
	public List<DtoMiscellaneousBenefits> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoMiscellaneousBenefits> delete) {
		this.delete = delete;
	}
	public DtoMiscellaneousBenefits() {
	}
	
	public short getFrequency() {
		return frequency;
	}
	public void setFrequency(short frequency) {
		this.frequency = frequency;
	}
	public short getMethod() {
		return method;
	}
	public void setMethod(short method) {
		this.method = method;
	}
	public short getEmpluyeeermethod() {
		return empluyeeermethod;
	}
	public void setEmpluyeeermethod(short empluyeeermethod) {
		this.empluyeeermethod = empluyeeermethod;
	}
	public void setEmpluyeeerinactive(boolean empluyeeerinactive) {
		this.empluyeeerinactive = empluyeeerinactive;
	}
	public DtoMiscellaneousBenefits(MiscellaneousBenefits miscellaneousBenefits) {
		this.benefitsId = miscellaneousBenefits.getBenefitsId();
		if (UtilRandomKey.isNotBlank(miscellaneousBenefits.getDesc())) {
			this.desc = miscellaneousBenefits.getDesc();
		} else {
			this.desc = "";
		}
		
		if (UtilRandomKey.isNotBlank(miscellaneousBenefits.getArbicDesc())) {
			this.arbicDesc = miscellaneousBenefits.getArbicDesc();
		} else {
			this.arbicDesc = "";
		}
		
		
	}

	
}
