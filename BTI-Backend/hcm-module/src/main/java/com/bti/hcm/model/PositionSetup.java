package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40123",indexes = {
        @Index(columnList = "DCATCHINDX")
})
public class PositionSetup extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DCATCHINDX")
	private Integer id;
	
	
	@Column(name = "DCATCHSEQN")
	private Integer ducumentAttachmentSecq;
	
	@Column(name = "DCATCHREFR",columnDefinition="char(61)")
	private String documentAttachmentName;
	
	
	/*@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="postAttachment_id")
	private Attachment postAttachmentId;*/
	
	@ManyToOne
	@JoinColumn(name = "POTINDX")
	private Position position;
	
	@Lob @Column(name="DCATCHFLE")
	private byte[] attachment; 
	
	@Column(name = "DCATCHDSCR",columnDefinition="char(61)")
	private String documentAttachmenDesc;
	
	@Column(name = "DCATCHDTYPE",columnDefinition="char(61)")
	private String fileType;
	
	@Column(name = "DCATCHTYP")
	private short attachmentType;
	
	@Column(name = "DCATCHDT")
	private Date attachmentDate;
	
	@Column(name = "DCATCHTM")
	private Date attachmentTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDucumentAttachmentSecq() {
		return ducumentAttachmentSecq;
	}

	public void setDucumentAttachmentSecq(Integer ducumentAttachmentSecq) {
		this.ducumentAttachmentSecq = ducumentAttachmentSecq;
	}

	public String getDocumentAttachmentName() {
		return documentAttachmentName;
	}

	public void setDocumentAttachmentName(String documentAttachmentName) {
		this.documentAttachmentName = documentAttachmentName;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String getDocumentAttachmenDesc() {
		return documentAttachmenDesc;
	}

	public void setDocumentAttachmenDesc(String documentAttachmenDesc) {
		this.documentAttachmenDesc = documentAttachmenDesc;
	}

	

	public short getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(short attachmentType) {
		this.attachmentType = attachmentType;
	}

	public Date getAttachmentDate() {
		return attachmentDate;
	}

	public void setAttachmentDate(Date attachmentDate) {
		this.attachmentDate = attachmentDate;
	}

	public Date getAttachmentTime() {
		return attachmentTime;
	}

	public void setAttachmentTime(java.util.Date date) {
		this.attachmentTime = date;
	}

	public byte[] getAttachment() {
		return attachment;
	}

	public void setAttachment(byte[] attachment) {
		this.attachment = attachment;
	}

	/*public Attachment getPostAttachmentId() {
		return postAttachmentId;
	}

	public void setPostAttachmentId(Attachment postAttachmentId) {
		this.postAttachmentId = postAttachmentId;
	}*/

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	

	

}
