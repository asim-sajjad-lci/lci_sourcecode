/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.InterviewTypeSetupDetail;

/**
 * Description: Interface for InterviewTypeSetupDetail 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryInterviewTypeSetupDetail")
public interface RepositoryInterviewTypeSetupDetail extends JpaRepository<InterviewTypeSetupDetail, Integer>{
	
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public InterviewTypeSetupDetail findByIdAndIsDeleted(int id, boolean deleted);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<InterviewTypeSetupDetail> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from InterviewTypeSetupDetail i where i.isDeleted=false")
	public Integer getCountOfTotalInterviewTypeSetupDetail();

	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<InterviewTypeSetupDetail> findByIsDeleted(Boolean deleted, Pageable pageable);
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update InterviewTypeSetupDetail d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteInterviewTypeSetupDetail(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update InterviewTypeSetupDetail d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleInterviewTypeSetupDetail(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
		/**
		 * 
		 * @return
		 */
		public InterviewTypeSetupDetail findTop1ByOrderByIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select i from InterviewTypeSetupDetail i where (i.interviewTypeSetupDetailDescription like :searchKeyWord  or i.interviewTypeSetup.interviewTypeId like :searchKeyWord) and i.isDeleted=false")
	public List<InterviewTypeSetupDetail> predictiveInterviewTypeSetupDetailSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<InterviewTypeSetupDetail> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from InterviewTypeSetupDetail i where (i.interviewTypeSetupDetailDescription like :searchKeyWord  or i.interviewTypeSetup.interviewTypeId like :searchKeyWord) and i.isDeleted=false")
	public Integer predictiveInterviewTypeSetupDetailSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select i from InterviewTypeSetupDetail i where (i.interviewTypeSetupDetailDescription like :searchKeyWord  or i.interviewTypeSetup.interviewTypeId like :searchKeyWord) and i.isDeleted=false")
	public List<InterviewTypeSetupDetail> predictiveInterviewTypeSetupDetailSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select i from InterviewTypeSetupDetail i where (i.id =:id) and i.isDeleted=false")
	public List<InterviewTypeSetupDetail> findByInterviewTypeSetupDetailId(@Param("id")Integer id);
	
	@Query("select i from InterviewTypeSetupDetail i where (i.interviewTypeSetup.id =:id) and i.isDeleted=false")
	public List<InterviewTypeSetupDetail> findByInterViewTypeSetupId(@Param("id")Integer id);
	
	@Query("select count(*) from InterviewTypeSetupDetail i ")
	public Integer getCountOfTotalInterviewTypeSetupDetails();
}
