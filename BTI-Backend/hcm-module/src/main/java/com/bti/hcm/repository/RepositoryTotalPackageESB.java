package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TotalPackageESB;

/**
 * 
 * @author HAMID
 *
 */
@Repository("repositoryTotalPackageESB")
public interface RepositoryTotalPackageESB extends JpaRepository<TotalPackageESB, Integer> {

	@Query("select t from TotalPackageESB t where t.typeId =:typeId and t.isDeleted = false ")
	List<TotalPackageESB> findByTypeIdAndIsDeleted(@Param("typeId") int typeId); // For Simple Get

	Integer countByTypeIdAndIsDeleted(Integer typeId, boolean b); // For Simple Get

	TotalPackageESB findByTypeIdAndTypeFieldForCodesIndxidAndIsDeleted(int typeId, int typeIndxId, Boolean b); // Save

	Integer countByTypeIdAndTypeFieldForCodesIndxidAndIsDeleted(int typeId, int typeIndxId, Boolean b); // Save

	TotalPackageESB findByIdxAndIsDeleted(Integer id, Boolean b); // Save

}
