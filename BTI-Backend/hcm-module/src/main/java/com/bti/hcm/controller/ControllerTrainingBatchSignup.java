package com.bti.hcm.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchTranningById;
import com.bti.hcm.model.dto.DtoTrainingBatchSignup;
import com.bti.hcm.model.dto.DtoTrainingCourseDetail;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTrainingBatchSignup;

@RestController
@RequestMapping("/trainingBatchSignup")
public class ControllerTrainingBatchSignup extends BaseController{
	
private static final Logger LOGGER = Logger.getLogger(ControllerTrainingBatchSignup.class);
	
	@Autowired(required=true)
	ServiceTrainingBatchSignup serviceTrainingBatchSignup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoTrainingBatchSignup dtoTrainingBatchSignup) throws Exception {
		LOGGER.info("Create TrainingBatchSignup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTrainingBatchSignup = serviceTrainingBatchSignup.saveOrUpdateTrainingBatchSignup(dtoTrainingBatchSignup);
			responseMessage=displayMessage(dtoTrainingBatchSignup, "TRAININGCOURSE_BATCH_SIGNUP_CREATED", "TRAININGCOURSE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create TrainingBatchSignup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoTrainingBatchSignup dtoTrainingBatchSignup) throws Exception {
		LOGGER.info("Update TrainingBatchSignup Method");
		List<DtoTrainingCourseDetail> list = new ArrayList<>();
		for (DtoTrainingCourseDetail iterable_element : dtoTrainingBatchSignup.getTrainingClassList()) {
			DtoTrainingCourseDetail courseDetail = new DtoTrainingCourseDetail();
			courseDetail.setId(iterable_element.getId());
			list.add(courseDetail);
		}
		dtoTrainingBatchSignup.setDtoTrainingCourseDetail(list);
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTrainingBatchSignup = serviceTrainingBatchSignup.saveOrUpdateTrainingBatchSignup(dtoTrainingBatchSignup);
			responseMessage=displayMessage(dtoTrainingBatchSignup, "TRAININGCOURSE_UPDATED_SUCCESS", "TRAININGCOURSE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update TrainingBatchSignup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoTrainingBatchSignup dtoTrainingBatchSignup) throws Exception {
		LOGGER.info("Delete TrainingBatchSignup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTrainingBatchSignup.getIds() != null && !dtoTrainingBatchSignup.getIds().isEmpty()) {
				DtoTrainingBatchSignup dtoTrainingBatchSignup2 = serviceTrainingBatchSignup.deleteTrainingBatchSignup(dtoTrainingBatchSignup.getIds());
				
				if(dtoTrainingBatchSignup2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("TRAINING_BATCH_SIGHNUP_DELETED", false), dtoTrainingBatchSignup2);
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoTrainingBatchSignup2.getMessageType());
					btiMessageHcm.setMessageShort("TRAINING_BATCH_SIGHNUP_NOT_DELETE_ID_MESSAGE");
					
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							btiMessageHcm, dtoTrainingBatchSignup2);
				}
				

				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete TrainingBatchSignup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search TrainingBatchSignup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTrainingBatchSignup.searchTrainingBatchSignup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "TRAINING_BATCH_SIGHNUP_GET_ALL", "TRAINING_BATCH_SIGHNUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TrainingBatchSignup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getTraningBatchSighUpByClassId", method = RequestMethod.POST)
	public ResponseMessage getTraningCourseById(HttpServletRequest request, @RequestBody DtoTrainingBatchSignup dtoTrainingBatchSignup) throws Exception {
		LOGGER.info("Get TraningCourseBatchSighnUp ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTrainingBatchSignup dtoTrainingBatchSignupObj = serviceTrainingBatchSignup.getTraningBatchSighUpById(dtoTrainingBatchSignup.getId());
			responseMessage=displayMessage(dtoTrainingBatchSignupObj, "TRAINING_BATCH_SIGHNUP_GET_ALL", "TRAINING_BATCH_SIGHNUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get TraningCourseBatchSighnUp by Id Method:"+dtoTrainingBatchSignup.getId());
		return responseMessage;
	}
	
	
	
	
	
	@RequestMapping(value = "/getBatchSighnUpFromTrainingId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchTrainingId(@RequestBody DtoTrainingBatchSignup dtoTrainingBatchSignup, HttpServletRequest request) throws Exception {
		LOGGER.info(" searchTrainingId Methods");
		ResponseMessage responseMessage = null;
		DtoSearch dtoSearch = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTrainingBatchSignup.getTraningBatchSighUpByIdTraningId(dtoTrainingBatchSignup.getId());
			responseMessage=displayMessage(dtoSearch, "TRAINING_ID_GET_ALL", MessageConstant.TRAINING_ID_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Search searchTrainingId Method:"+dtoTrainingBatchSignup.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getSighnUpFromTrainingId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getSighnUpFromTrainingId(@RequestBody DtoTrainingBatchSignup dtoTrainingBatchSignup, HttpServletRequest request) throws Exception {
		LOGGER.info(" searchTrainingId Method");
		ResponseMessage responseMessage = null;
		DtoSearch dtoSearch = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTrainingBatchSignup.getSighnUpFromTrainingId(dtoTrainingBatchSignup.getId());
			responseMessage=displayMessage(dtoSearch, "TRAINING_ID_GET_ALL", "TRAINING_ID_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Search searchTrainingId Method:"+dtoTrainingBatchSignup.getId());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getTraningIdByStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getTraningIdByStatus(@RequestBody DtoSearchTranningById dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info(" searchTrainingId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTrainingBatchSignup.getTraningIdByStatus(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "TRAINING_BATCHUP_STATUS_GET_ALL", "TRAINING_ID_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	/**
	 * @param request{
		  "id": 55,
		  "trainingClassStatus": 0
		}
	 * @param dtoTrainingBatchSignup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/changeStatus", method = RequestMethod.POST)
	public ResponseMessage changeStatus(HttpServletRequest request, @RequestBody DtoTrainingBatchSignup dtoTrainingBatchSignup) throws Exception {
		LOGGER.info("Update TrainingBatchSignup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTrainingBatchSignup = serviceTrainingBatchSignup.changeStatus(dtoTrainingBatchSignup);
			responseMessage=displayMessage(dtoTrainingBatchSignup, "TRAININGCOURSE_UPDATED_SUCCESS", "TRAININGCOURSE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update TrainingBatchSignup Method:"+responseMessage.getMessage());
		return responseMessage;
	}

}
