package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoDateConvertReq;
import com.bti.hcm.service.ServiceCommon;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/common")
public class ControllerCommon extends BaseController{
	
	private static final Logger LOGGER = Logger.getLogger(ControllerCommon.class);
	
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	@Autowired
	ServiceCommon serviceCommon;
	
	
	@RequestMapping(value = "/convertDate", method = RequestMethod.POST)
	public ResponseMessage converGregorianDateToHijriDate(HttpServletRequest request, @RequestBody DtoDateConvertReq dtoDateConvertReq) throws Exception {
		LOGGER.info("convertDate called!!");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDateConvertReq = serviceCommon.dateConverter(dtoDateConvertReq);
			responseMessage=displayMessage(dtoDateConvertReq, "DEPARTMENT_CREATED", "DEPARTMENT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
					
		}
		return responseMessage;
	}
}
