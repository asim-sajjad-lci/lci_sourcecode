package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.SalaryMatrixRowValue;

public class DtoSalaryMatrixRowValue extends DtoBase{

	private Integer id;
	private Integer salaryMatrixRowId;
	private Integer sequence;
	private BigDecimal amount;
	private String desc;

	private boolean isDeleted;
	private List<DtoSalaryMatrixRowValue> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public List<DtoSalaryMatrixRowValue> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoSalaryMatrixRowValue> delete) {
		this.delete = delete;
	}

	public DtoSalaryMatrixRowValue() {
	}

	public DtoSalaryMatrixRowValue(SalaryMatrixRowValue salaryMatrixRowValue) {

	}

	public Integer getSalaryMatrixRowId() {
		return salaryMatrixRowId;
	}

	public void setSalaryMatrixRowId(Integer salaryMatrixRowId) {
		this.salaryMatrixRowId = salaryMatrixRowId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
