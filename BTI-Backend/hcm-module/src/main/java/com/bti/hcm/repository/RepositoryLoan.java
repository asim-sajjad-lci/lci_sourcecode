package com.bti.hcm.repository;

/**
 * 	@author HAMID
 * 
 */
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Loan;

@Repository("repositoryLoan")
public interface RepositoryLoan extends JpaRepository<Loan, Integer> {

	Loan findByIdAndIsDeleted(Integer id, Boolean b);
	
	
	@Query("select l from Loan l where l.isDeleted = false and (l.refId like :searchKeyword or l.employeeMaster.employeeId like :searchKeyword ) ")
	public List<Loan> viewLoan(@Param("searchKeyword") String searchKeyword, Pageable pageable); 
	
	
	@Query("select count(*) from Loan l where l.isDeleted = false and (l.refId like :searchKeyword or l.employeeMaster.employeeId like :searchKeyword ) ")
	Integer countByIsDeleted(@Param("searchKeyword") String searchKeyword);
	

	@Query("select sum(l.loanAmt) from Loan l where l.isDeleted = false and l.loanAmt > 0 and l.employeeMaster.employeeIndexId =:id ")
	Double sumAllLoansAmtForThisEmployee(@Param("id") Integer id);	//for PreviousLoan Amount			


	List<Loan> findByEmployeeMasterEmployeeIndexId(Integer empId);
	
	
	//Reverse get API ME
	
	//	@Query("select l from Loan l where isDeleted=false and l.id = (select l.id from loan l where )")
	//	Loan getLoanByLoanDetails(@Param("id") Integer id);	

	//////////////// for GetAll API's (in advance wrote) ///////////////////////

	// List<Loan> findByEmployeeMasterEmployeeIndexId();

	// Integer countByEmployeeMasterEmployeeIndexId();

}
