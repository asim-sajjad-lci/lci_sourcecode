package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Default;
@Repository("/repositoryDefault")
public interface RepositoryDefault  extends JpaRepository<Default, Integer> {

	Default findByIdAndIsDeleted(Integer id, boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Default d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleDefault(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from Default d where (d.defaultID LIKE :searchKeyWord or d.arabicDesc LIKE :searchKeyWord or d.desc LIKE :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveDefaultSearchTotalCount(@Param("searchKeyWord")  String searchKeyWord);

	@Query("select d from Default d where (d.defaultID LIKE :searchKeyWord or d.arabicDesc LIKE :searchKeyWord or d.desc LIKE :searchKeyWord) and d.listDistribution IS NOT EMPTY and d.isDeleted=false")
	List<Default> predictiveDefaultSearchWithPagination(@Param("searchKeyWord")String searchKeyWord,Pageable pageable);
	
	@Query("select d from Default d where (d.defaultID LIKE :searchKeyWord or d.arabicDesc LIKE :searchKeyWord or d.desc LIKE :searchKeyWord) and d.isDeleted=false")
	List<Default> predictiveDefaultSearchWithPaginations(@Param("searchKeyWord")String searchKeyWord,Pageable pageable);
	
	@Query("select count(*) from Default d where (d.defaultID LIKE :searchKeyWord or d.arabicDesc LIKE :searchKeyWord or d.desc LIKE :searchKeyWord) and d.listDistribution IS NOT EMPTY and d.isDeleted=false")
	Integer predictiveDefaultSearchWithPaginationCount(@Param("searchKeyWord")String searchKeyWord);

	@Query("select d from Default d where (d.defaultID like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<Default> predictiveSearchDefaultIdWithPagination(@Param("searchKeyWord")String defaultID);

	List<Default> findByIsDeleted(Boolean deleted);

	@Query("select d from Default d where (d.defaultID =:defaultID) and d.isDeleted=false")
	List<Default> findByDefaultId(@Param("defaultID")String defaultID);

}
