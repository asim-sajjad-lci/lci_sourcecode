package com.bti.hcm.model.dto;

import java.util.ArrayList;
import java.util.List;

public class DtoEmployeeVacation extends DtoEmployeeInfo {

	public class DtoEmployeeDependent {
		private String id;
		private String name;
		private int age;
		private String relationship;
		private boolean hasTicket;

		public DtoEmployeeDependent(String id, String name, int age, String relationship, boolean hasTicket) {
			super();
			this.id = id;
			this.name = name;
			this.age = age;
			this.relationship = relationship;
			this.hasTicket = hasTicket;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public String getRelationship() {
			return relationship;
		}

		public void setRelationship(String relationship) {
			this.relationship = relationship;
		}

		public boolean isHasTicket() {
			return hasTicket;
		}

		public void setHasTicket(boolean hasTicket) {
			this.hasTicket = hasTicket;
		}

	}

	public class VacationType {
		private String id;
		private String description;

		public VacationType(String id, String description) {
			super();
			this.id = id;
			this.description = description;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

	}

	private int leaveBalance;
	private List<DtoEmployeeDependent> dependents = new ArrayList<DtoEmployeeVacation.DtoEmployeeDependent>();
	private List<VacationType> vacationTypes = new ArrayList<DtoEmployeeVacation.VacationType>();
	private boolean hasTickets;

	public DtoEmployeeVacation() {
		super();

		this.leaveBalance = 22;

		this.dependents.add(new DtoEmployeeDependent("ED-001", "Samira Ahmed", 32, "Wife", true));
		this.dependents.add(new DtoEmployeeDependent("ED-002", "Ali Ahmed", 6, "Son", false));
		this.dependents.add(new DtoEmployeeDependent("ED-003", "Ayesha Ahmed", 3, "Daughter", true));

		this.vacationTypes.add(new VacationType("VT-001", "Annual Vacation"));
		this.vacationTypes.add(new VacationType("VT-002", "Emergency Vacation"));
		
		this.setHasTickets(true);

	}

	public int getLeaveBalance() {
		return leaveBalance;
	}

	public void setLeaveBalance(int leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

	public List<DtoEmployeeDependent> getDependents() {
		return dependents;
	}

	public void setDependents(List<DtoEmployeeDependent> dependents) {
		this.dependents = dependents;
	}

	public List<VacationType> getVacationTypes() {
		return vacationTypes;
	}

	public void setVacationTypes(List<VacationType> vacationTypes) {
		this.vacationTypes = vacationTypes;
	}

	public boolean isHasTickets() {
		return hasTickets;
	}

	public void setHasTickets(boolean hasTickets) {
		this.hasTickets = hasTickets;
	}

}
