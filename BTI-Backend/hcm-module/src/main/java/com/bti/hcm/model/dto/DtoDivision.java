/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.Division;
import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Division class having getter and setter for fields (POJO)
 * Name of Project: Hrm Created on: December 08, 2017
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoDivision extends DtoBase{

	private Integer id;
	private String divisionId;
	private String divisionDescription;
	private HrCity city;
	private HrState state;
	private HrCountry country;
	private Integer cityId;
	private Integer stateId;
	private Integer countryId;
	private String cityName;
	private String stateName;
	private String countryName;
	private List<DtoDivision> deleteDivision;
	private String arabicDivisionDescription;
	private String divisionAddress;
	private String phoneNumber;
	private String fax;
	private String email;
	private int valueId;
	private String valueDescription;

	private int valId;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}


	public String getDivisionDescription() {
		return divisionDescription;
	}

	public void setDivisionDescription(String divisionDescription) {
		this.divisionDescription = divisionDescription;
	}


	public List<DtoDivision> getDeleteDivision() {
		return deleteDivision;
	}

	public void setDeleteDivision(List<DtoDivision> deleteDivision) {
		this.deleteDivision = deleteDivision;
	}
	
	
	
	
	public String getArabicDivisionDescription() {
		return arabicDivisionDescription;
	}

	public void setArabicDivisionDescription(String arabicDivisionDescription) {
		this.arabicDivisionDescription = arabicDivisionDescription;
	}

	
	

	public String getDivisionAddress() {
		return divisionAddress;
	}

	public void setDivisionAddress(String divisionAddress) {
		this.divisionAddress = divisionAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	

	
	
	public HrCity getCity() {
		return city;
	}

	public void setCity(HrCity city) {
		this.city = city;
	}

	public HrState getState() {
		return state;
	}

	public void setState(HrState state) {
		this.state = state;
	}

	public HrCountry getCountry() {
		return country;
	}

	public void setCountry(HrCountry country) {
		this.country = country;
	}



	public int getValueId() {
		return valueId;
	}

	public void setValueId(int string) {
		this.valueId = string;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	public DtoDivision() {
		
	}
	public DtoDivision(Division division) {
		this.divisionId = division.getDivisionId();

		if (UtilRandomKey.isNotBlank(division.getArabicDivisionDescription())) {
			this.arabicDivisionDescription = division.getArabicDivisionDescription();
		} else {
			this.arabicDivisionDescription = "";
		}
		
		
		if (UtilRandomKey.isNotBlank(division.getPhoneNumber())) {
			this.phoneNumber = division.getPhoneNumber();
		} else {
			this.phoneNumber = "";
		}

		if (UtilRandomKey.isNotBlank(division.getFax())) {
			this.fax = division.getPhoneNumber();
		} else {
			this.fax = "";
		}
		
		
		if (UtilRandomKey.isNotBlank(division.getEmail())) {
			this.email = division.getEmail();
		} else {
			this.email = "";
		}

		if (UtilRandomKey.isNotBlank(division.getDivisionDescription())) {
			this.divisionDescription = division.getDivisionDescription();
		} else {
			this.divisionDescription = "";
		}
	}

	public int getValId() {
		return valId;
	}

	public void setValId(int valId) {
		this.valId = valId;
	}

	
}
