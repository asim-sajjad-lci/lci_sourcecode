package com.bti.hcm.model;



import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="hr40174",indexes = {
		@Index(columnList = "miscmindx")
})
public class MiscellaneousEmployee extends HcmBaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="miscmindx")
	private int miscellaneousIndex;
	
	@ManyToOne
	@JoinColumn(name="valueindx")
	private MiscellaneousDetails miscellaneousDetails;
	
	
	@ManyToOne
	@JoinColumn(name="employindx")
	private EmployeeMaster employeeMaster;
	
	@ManyToOne
	@JoinColumn(name="miscindx")
	private MiscellaneousHeader miscellaneousHeader;


	public int getMiscellaneousIndex() {
		return miscellaneousIndex;
	}


	public void setMiscellaneousIndex(int miscellaneousIndex) {
		this.miscellaneousIndex = miscellaneousIndex;
	}


	

	public MiscellaneousDetails getMiscellaneousDetails() {
		return miscellaneousDetails;
	}


	public void setMiscellaneousDetails(MiscellaneousDetails miscellaneousDetails) {
		this.miscellaneousDetails = miscellaneousDetails;
	}


	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}


	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}


	public MiscellaneousHeader getMiscellaneousHeader() {
		return miscellaneousHeader;
	}


	public void setMiscellaneousHeader(MiscellaneousHeader miscellaneousHeader) {
		this.miscellaneousHeader = miscellaneousHeader;
	}
	
	
	
}
