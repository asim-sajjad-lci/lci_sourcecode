package com.bti.hcm.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import groovy.json.internal.LazyMap;
import net.minidev.json.JSONObject;

import com.bti.hcm.model.dto.DtoCompanyInfo;
import com.bti.hcm.util.Constant;
import com.jayway.restassured.*;

@Service("serviceHome") 
@Transactional
public class ServiceHcmHome {
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Value("${script.accesspath}")
	private String accesspath;
	
	public boolean checkValidCompanyAccess() throws Exception 
    {
        try{  
      	  
      	  String langId = httpServletRequest.getHeader("langId");
      		String session=httpServletRequest.getHeader("session");
      		String userId=httpServletRequest.getHeader("userid");
      		String companyTenantId=httpServletRequest.getHeader("tenantid");
      	  
      		System.out.println("Before companyTenantId is :------>"+companyTenantId);
      		if(companyTenantId == null || companyTenantId == " ") {
      			companyTenantId="bti_hr";
      		}
      		System.out.println("After companyTenantId is :------>"+companyTenantId);
             JSONObject json = new JSONObject();
             json.put("userId", userId);
             json.put("session", session);
             json.put("companyTenantId", companyTenantId);
             String jsonString = json.toJSONString();
             Response response = RestAssured.given().header("Content-Type", "application/json")                          
                    .header("userid", userId)
                    .header("session", session)
                    .header("companyTenantId",companyTenantId)
                    .header("langid",langId)
                        .body(jsonString).when()
                       .post(accesspath+"/login/checkCompanyAccessForOtherModule").andReturn();
           JsonPath jsonpath =response.getBody().jsonPath();
           String code = jsonpath.getString("code");
           if(code.equalsIgnoreCase("200")){
                return true;
           }
           else{
                return false;
           }
        }
        catch(Exception e){
             return false;
        }
//		return true;
  }
	
	public boolean validateClient() throws Exception 
    {
        try{  
      	  
      		String clientId = httpServletRequest.getHeader(Constant.WORKFLOW_CLIENT_ID);
      		String token = httpServletRequest.getHeader(Constant.WORKFLOW_CLIENT_TOKEN);

             JSONObject json = new JSONObject();
             json.put(Constant.WORKFLOW_CLIENT_ID, clientId);
             json.put(Constant.WORKFLOW_CLIENT_TOKEN, token);
             String jsonString = json.toJSONString();
             Response response = RestAssured.given().header("Content-Type", "application/json")                          
                    .header(Constant.WORKFLOW_CLIENT_ID, clientId)
                    .header(Constant.WORKFLOW_CLIENT_TOKEN, token)
                        .body(jsonString).when()
                       .post(accesspath+"/login/validateClient").andReturn();
           JsonPath jsonpath =response.getBody().jsonPath();
           String code = jsonpath.getString("code");
           if(code.equalsIgnoreCase("200")){
                return true;
           }
           else{
                return false;
           }
        }
        catch(Exception e){
             return false;
        }
//		return true;
  }

	public DtoCompanyInfo getDtoCompanyInfo() 
    {
		
		DtoCompanyInfo dtoCompanyInfo = null;
        try{  
      	  
      	  	String langId = httpServletRequest.getHeader("langId");
      		String session=httpServletRequest.getHeader("session");
      		String userId=httpServletRequest.getHeader("userid");
      		String companyTenantId=httpServletRequest.getHeader("tenantid");
      	  
      		System.out.println("Before companyTenantId is :------>"+companyTenantId);
      		if(companyTenantId == null || companyTenantId == " ") {
      			companyTenantId="bti_hr";
      		}
      		System.out.println("After companyTenantId is :------>"+companyTenantId);
             JSONObject json = new JSONObject();
//             json.put("userId", userId);
//             json.put("session", session);
//             json.put("companyTenantId", companyTenantId);
             json.put("tenantId", companyTenantId);
             String jsonString = json.toJSONString();
             Response response = RestAssured.given().header("Content-Type", "application/json; charset=utf-8")                          
                    .header("userid", userId)
                    .header("session", session)
                    .header("tenantid",companyTenantId)
                    .header("langid",langId)
                    .header("Accept-Charset", "utf-8")
                        .body(jsonString).when()
                       .post(accesspath+"/company/getCompanyByTenantId").andReturn();
           JsonPath jsonpath =response.getBody().jsonPath();
           String code = jsonpath.getString("code");
           if(code.equalsIgnoreCase("302")){
        	   	Map<String, String>  result = jsonpath.getMap("result");
                String name = (String) result.get("name");
                String nameArabic = (String) result.get("nameArabic");
                
                System.out.println("name: " + name);
                System.out.println("nameArabic: " + nameArabic);
                
                dtoCompanyInfo = new DtoCompanyInfo();
                dtoCompanyInfo.setName(name);
                dtoCompanyInfo.setNameArabic(nameArabic);
                
           }
        }
        catch(Exception e){
        	e.printStackTrace();
        }
        return dtoCompanyInfo;
//		return true;
  }
	
}
