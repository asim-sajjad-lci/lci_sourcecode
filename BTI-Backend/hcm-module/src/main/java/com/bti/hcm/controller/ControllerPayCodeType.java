package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoPayCodeType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayCodeType;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/payCodeType")
public class ControllerPayCodeType extends BaseController{

	@Autowired
	ServicePayCodeType servicePayCodeType;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger log = Logger.getLogger(ControllerPayCodeType.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	@RequestMapping(value = "/searchIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPosition(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search search Pay Code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayCodeType.searchPayCodeTypeIds(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_CODE_GET_ALL", "PAY_CODE_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search Pay Code Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getAllPayCodeTypeDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllPayCodeTypeDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoPayCodeType> dtoPayCodeTypeList = servicePayCodeType.getAllPayCodeTypeDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("PAY_CODE_LIST_GET_ALL", false), dtoPayCodeTypeList);
			} else {
				responseMessage = unauthorizedMsg(response);
						}

		} catch (Exception e) {
			log.error(e);
		}

		return responseMessage;
	}
	
	
	
	
}
