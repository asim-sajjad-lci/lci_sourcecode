package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoHelthCoverageType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceHelthCoverageType;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/helthCoverageType")
public class ControllerHelthCoverageType extends BaseController{

	
	/**
	 * @Description LOGGER use for put a logger in HelthCoverageType Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerHelthCoverageType.class);
	
	/**
	 * @Description serviceHelthCoverageType Autowired here using annotation of spring for use of serviceHelthCoverageType method in this controller
	 */
	@Autowired(required=true)
	ServiceHelthCoverageType serviceHelthCoverageType;

	BaseController baseConroller= new BaseController();

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @param request{
   "helthCoverageId": 1,
  "desc": "test",
  "arbicDesc": "test"
		
 
}
	 * @param dtoHelthCoverageType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoHelthCoverageType dtoHelthCoverageType) throws Exception {
		LOGGER.info("Create Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHelthCoverageType = serviceHelthCoverageType.saveOrUpdate(dtoHelthCoverageType);
			responseMessage=displayMessage(dtoHelthCoverageType, "HELTH_COVERAGE_CREATED", "HELTH_COVERAGE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param request{
  "id":1,
  "helthCoverageId": 1,
  "desc": "test",
  "arbicDesc": "tssaassest"
		
 
}
	 * @param dtoHelthCoverageType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoHelthCoverageType dtoHelthCoverageType) throws Exception {
		LOGGER.info("Update Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHelthCoverageType = serviceHelthCoverageType.saveOrUpdate(dtoHelthCoverageType);
			responseMessage=displayMessage(dtoHelthCoverageType, "HELTH_COVERAGE_UPDATED_SUCCESS", "HELTH_COVERAGE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  			"ids": [1]
		}
	 * @param dtoHelthCoverageType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoHelthCoverageType dtoHelthCoverageType) throws Exception {
		LOGGER.info("Delete Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoHelthCoverageType.getIds() != null && !dtoHelthCoverageType.getIds().isEmpty()) {
				DtoHelthCoverageType dtoHealthCoverageType2 = serviceHelthCoverageType.delete(dtoHelthCoverageType.getIds());
				if(dtoHealthCoverageType2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("HELTH_COVERAGE_DELETED", false), dtoHealthCoverageType2);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("HELTH_COVERAGE_NOT_DELETED", false), dtoHealthCoverageType2);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = baseConroller.unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Delete Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Attendace Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceHelthCoverageType.search(dtoSearch);
	        responseMessage=displayMessage(dtoSearch, MessageConstant.HELTH_COVERAGE_GET_ALL, MessageConstant.HELTH_COVERAGE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Attendace Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody  DtoHelthCoverageType dtoHelthCoverageType) throws Exception {
		LOGGER.info("Get All PostionBudget Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceHelthCoverageType.getAll(dtoHelthCoverageType);
			responseMessage=displayMessage(dtoSearch, MessageConstant.HELTH_COVERAGE_GET_ALL, MessageConstant.HELTH_COVERAGE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All PostionBudget Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
 
 	 	"id":1
	}
	 * @param DtoHelthCoverageType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getHelthCoverageTypeId", method = RequestMethod.POST)
	public ResponseMessage getPositionBudgetId(HttpServletRequest request, @RequestBody DtoHelthCoverageType dtoAtteandace) throws Exception {
		LOGGER.info("Get Position ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoHelthCoverageType dtoPositionObj = serviceHelthCoverageType.getByHelthCoverageTypeId(dtoAtteandace.getId());
			responseMessage=displayMessage(dtoPositionObj, "HELTH_COVERAGE_LIST_GET_DETAIL", "HELTH_COVERAGE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Position by Id Method:"+dtoAtteandace.getId());
		return responseMessage;
	}
	/**
	 * @description healthConverageTypeId is exiest or not
	 * @param request
	 * @param dtoDeductionCode
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "helthCoverageId":"DEP003"
		 
		}
	 *@response{
				"code": 302,
				"status": "FOUND",
				"result": {
					"isRepeat": true
				},
				"btiMessage": {
					"message": "HealthCoverage ID already exists",
					"messageShort": "HELTH_COVERAGE_RESULT"
				}
		}
	 */
	/*@RequestMapping(value = "/HelthCoverageTypeIdcheck", method = RequestMethod.POST)
	public ResponseMessage deductionIdcheck(HttpServletRequest request, @RequestBody DtoHelthCoverageType dtoDeductionCode) throws Exception {
		LOGGER.info("DeductionCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoHelthCoverageType dtoDepartmentObj = serviceHelthCoverageType.repeatByHelthCoverageTypeId(dtoDeductionCode.getHelthCoverageId());
		    responseMessage=displayMessage(dtoDepartmentObj, "HELTH_COVERAGE_RESULT", "HELTH_COVERAGE_REPEAT_HELTH_COVERAGE_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	*/
	
	
	@RequestMapping(value = "/HelthCoverageTypeIdcheck", method = RequestMethod.POST)
	public ResponseMessage deductionIdcheck(HttpServletRequest request, @RequestBody DtoHelthCoverageType dtoHelthCoverageType)
			throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoHelthCoverageType dtoHelthCoverageTypeObj = serviceHelthCoverageType.repeatByHelthCoverageTypeId(dtoHelthCoverageType.getHelthCoverageId());
			
			 if (dtoHelthCoverageTypeObj !=null && dtoHelthCoverageTypeObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                		 serviceResponse.getMessageByShortAndIsDeleted("HELTH_COVERAGE_RESULT", false), dtoHelthCoverageTypeObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                		 serviceResponse.getMessageByShortAndIsDeleted("HELTH_COVERAGE_REPEAT_HELTH_COVERAGE_NOT_FOUND", false),
                         dtoHelthCoverageTypeObj);
             }
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	
	
	
	
	@RequestMapping(value = "/searchHelthCoverageId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchHelthCoverageId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("searchHelthCoverageId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceHelthCoverageType.searchHelthCoverageId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.HELTH_COVERAGE_GET_ALL, MessageConstant.HELTH_COVERAGE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Attendace Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllHealthCoverageType", method = RequestMethod.GET)
	public ResponseMessage getAllHealthCoverageType(HttpServletRequest request) throws Exception {
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceHelthCoverageType.getAllCoverageType();
			responseMessage=displayMessage(dtoSearch, "HELTH_COVERAGE_TYPE_GET_ALL", "HELTH_COVERAGE_TYPE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
