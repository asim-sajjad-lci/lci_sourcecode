package com.bti.hcm.model.dto;

import java.util.List;

public class DtoEmployeeQuickAssigments {

	private Integer id;
	private Integer employeeId;
	private Integer codeType;
	private boolean allCompanyCode;
	private Integer from;
	private Integer toIndex;
	private List<DtoEmployeeCodeType> listCodeTypes;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getCodeType() {
		return codeType;
	}
	public void setCodeType(Integer codeType) {
		this.codeType = codeType;
	}
	
	public boolean isAllCompanyCode() {
		return allCompanyCode;
	}
	public void setAllCompanyCode(boolean allCompanyCode) {
		this.allCompanyCode = allCompanyCode;
	}
	public List<DtoEmployeeCodeType> getListCodeTypes() {
		return listCodeTypes;
	}
	public void setListCodeTypes(List<DtoEmployeeCodeType> listCodeTypes) {
		this.listCodeTypes = listCodeTypes;
	}
	public Integer getFrom() {
		return from;
	}
	public void setFrom(Integer from) {
		this.from = from;
	}
	public Integer getToIndex() {
		return toIndex;
	}
	public void setToIndex(Integer toIndex) {
		this.toIndex = toIndex;
	}
	
	
}
