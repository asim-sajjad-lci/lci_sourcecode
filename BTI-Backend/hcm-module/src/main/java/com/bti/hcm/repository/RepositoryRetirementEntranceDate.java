package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.RetirementEntranceDate;

@Repository("repositoryRetirementEntranceDate")
public interface RepositoryRetirementEntranceDate extends JpaRepository<RetirementEntranceDate, Integer>{

	RetirementEntranceDate findByIdAndIsDeleted(Integer id, boolean b);

	RetirementEntranceDate saveAndFlush(RetirementEntranceDate retirementEntranceDate);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update RetirementEntranceDate r set r.isDeleted =:deleted ,r.updatedBy =:updateById where r.id =:id ")
	public void deleteSingleRetirementEntranceDate(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from RetirementEntranceDate r where (r.entranceDescription LIKE :searchKeyWord) and r.isDeleted=false")
	Integer predictiveRetirementEntranceDateSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	
	@Query("select r from RetirementEntranceDate r where (r.entranceDescription LIKE :searchKeyWord) and r.isDeleted=false")
	List<RetirementEntranceDate> predictiveRetirementEntranceDateSearchWithPagination(@Param("searchKeyWord")String searchKeyWord,Pageable pageable);
	
	@Query("select r from RetirementEntranceDate r where (r.planId =:planId) and r.isDeleted=false")
	List<RetirementEntranceDate> findByRetirementPlanId(@Param("planId")Integer planId);

	@Query("select count(*) from RetirementEntranceDate r ")
	public Integer getCountOfTotaRetirementEntranceDate();

}
