package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;

public class DtoTransactionEntryDisplay implements Comparator{

	private Integer transcationPrimaryId;
	private Integer transcationSubListId;
	private Integer transcationNumber;
	private Integer employeePrimaryId;
	private String employeeId;
	private String employeeName;
	private short trxType;
	private String departmentId;
	private Integer projectPrimaryId;
	private String projectId;
	private String codeId;
	private Integer codePrimaryId;
	private BigDecimal amount;
	private BigDecimal payRate;
	private BigDecimal payFactor;
	private boolean status;
	
	private DtoDeductionCode dtoDeductionCode;
	
	private Date buildDate;
	private String buildtime;
	
	public Integer getTranscationNumber() {
		return transcationNumber;
	}
	public void setTranscationNumber(Integer transcationNumber) {
		this.transcationNumber = transcationNumber;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public short getTrxType() {
		return trxType;
	}
	public void setTrxType(short trxType) {
		this.trxType = trxType;
	}
	
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getCodeId() {
		return codeId;
	}
	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getPayRate() {
		return payRate;
	}
	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}
	public Integer getEmployeePrimaryId() {
		return employeePrimaryId;
	}
	public void setEmployeePrimaryId(Integer employeePrimaryId) {
		this.employeePrimaryId = employeePrimaryId;
	}
	public Integer getProjectPrimaryId() {
		return projectPrimaryId;
	}
	public void setProjectPrimaryId(Integer projectPrimaryId) {
		this.projectPrimaryId = projectPrimaryId;
	}
	public Integer getCodePrimaryId() {
		return codePrimaryId;
	}
	public void setCodePrimaryId(Integer codePrimaryId) {
		this.codePrimaryId = codePrimaryId;
	}
	public Integer getTranscationPrimaryId() {
		return transcationPrimaryId;
	}
	public void setTranscationPrimaryId(Integer transcationPrimaryId) {
		this.transcationPrimaryId = transcationPrimaryId;
	}
	public Integer getTranscationSubListId() {
		return transcationSubListId;
	}
	public void setTranscationSubListId(Integer transcationSubListId) {
		this.transcationSubListId = transcationSubListId;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	public Date getBuildDate() {
		return buildDate;
	}
	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}
	public String getBuildtime() {
		return buildtime;
	}
	public void setBuildtime(String buildtime) {
		this.buildtime = buildtime;
	}
	public BigDecimal getPayFactor() {
		return payFactor;
	}
	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}
	@Override
	public int compare(Object o1, Object o2) {
		DtoTransactionEntryDisplay obj1=	(DtoTransactionEntryDisplay)o1;
		DtoTransactionEntryDisplay obj2=	(DtoTransactionEntryDisplay)o2;
		
		return obj1.codeId.compareTo(obj2.getCodeId());
	}
	
	
	
	public class SortByEmployee implements Comparator<DtoTransactionEntryDisplay>{
		@Override
		public int compare(DtoTransactionEntryDisplay o1, DtoTransactionEntryDisplay o2) {
			return o1.getEmployeeId().compareTo(o2.getEmployeeId());
		}
		
	}
	
	public class SortByDepartment implements Comparator<DtoTransactionEntryDisplay>{
		@Override
		public int compare(DtoTransactionEntryDisplay o1, DtoTransactionEntryDisplay o2) {
			return o1.getDepartmentId().compareTo(o2.getDepartmentId());
		}
		
	}
	
	public class SortByCodeId implements Comparator<DtoTransactionEntryDisplay>{
		@Override
		public int compare(DtoTransactionEntryDisplay o1, DtoTransactionEntryDisplay o2) {
			return o1.getCodeId().compareTo(o2.getCodeId());
		}
		
	}
	
	public class SortByProjectId implements Comparator<DtoTransactionEntryDisplay>{
		@Override
		public int compare(DtoTransactionEntryDisplay o1, DtoTransactionEntryDisplay o2) {
			return o1.getProjectId().compareTo(o2.getProjectId());
		}
		
	}

	public class SortByTransactionId implements Comparator<DtoTransactionEntryDisplay>{
		@Override
		public int compare(DtoTransactionEntryDisplay o1, DtoTransactionEntryDisplay o2) {
			return o1.getTranscationPrimaryId().compareTo(o2.getTranscationPrimaryId());
		}
		
	}

	public DtoDeductionCode getDtoDeductionCode() {
		return dtoDeductionCode;
	}
	public void setDtoDeductionCode(DtoDeductionCode dtoDeductionCode) {
		this.dtoDeductionCode = dtoDeductionCode;
	}
	
	
}
