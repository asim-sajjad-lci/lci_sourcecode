package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.SkillSetSetup;

/**
 * Description: Interface for SkillSetSetup 
 * Name of Project: Hcm
 * Version: 0.0.1
 * @author Gaurav
 *
 */
@Repository("RepositorySkillSetSetup")
public interface RepositorySkillSetSetup extends JpaRepository<SkillSetSetup, Integer>{

	SkillSetSetup findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from SkillSetSetup s where s.isDeleted=false")
	Integer getCountOfTotalSkillSetSetup();

	List<SkillSetSetup> findByIsDeleted(boolean b, Pageable pageable);

	List<SkillSetSetup> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update SkillSetSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleSkillSetSteup(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer skillStepId);

	@Query("select p from SkillSetSetup p where (p.skillSetId =:skillId) and p.isDeleted=false")
	List<SkillSetSetup> findByskillSetId(@Param("skillId")String skillId);

	@Query("select count(*) from SkillSetSetup d where (d.skillSetId like :searchKeyWord  or d.skillSetDiscription like  :searchKeyWord  or d.arabicDiscription like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveSkillSetSteupSearchTotalCount(@Param("searchKeyWord")String string);

	@Query("select d from SkillSetSetup d where (d.skillSetId like :searchKeyWord  or d.skillSetDiscription like  :searchKeyWord  or d.arabicDiscription like :searchKeyWord) and d.isDeleted=false")
	List<SkillSetSetup> predictiveSkillsSetSetupSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Query("select d from SkillSetSetup d where (d.skillSetId like :searchKeyWord ) and d.isDeleted=false")
	List<SkillSetSetup> predictiveSkillSetIdSearchWithPagination(@Param("searchKeyWord")String string);
	
	@Query("select d from SkillSetSetup d where d.isDeleted=false order by id desc")
	List<SkillSetSetup> findAll();

	
	@Query("select d from SkillSetSetup d where (d.skillSetId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<SkillSetSetup> predictivesearchSkillSetSetupIdWithPagination(@Param("searchKeyWord")String skillSetId);


}
