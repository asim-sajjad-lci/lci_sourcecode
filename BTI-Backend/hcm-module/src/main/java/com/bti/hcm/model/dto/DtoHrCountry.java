package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.HrCountry;

public class DtoHrCountry extends DtoBase{

	private String countryCode;
	private String shortName;
	private Integer defaultCountry;
	private List<DtoHrCountry> delete;
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public List<DtoHrCountry> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoHrCountry> delete) {
		this.delete = delete;
	}
	public DtoHrCountry() {
	}
	
	public Integer getDefaultCountry() {
		return defaultCountry;
	}
	public void setDefaultCountry(Integer defaultCountry) {
		this.defaultCountry = defaultCountry;
	}
	public DtoHrCountry(HrCountry hrCountry) {
	}

}
