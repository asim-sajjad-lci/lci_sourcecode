package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSalaryMatrixColSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceSalaryMatrixColSetup;

@RestController
@RequestMapping("/salaryMatrixColSetup")
public class ControllerSalaryMatrixColSetup extends BaseController{
	
	/**
	 * @Description LOGGER use for put a logger in SalaryMatrixColSetup Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerDepartment.class);
	
	/**
	 * @Description serviceSalaryMatrixColSetup Autowired here using annotation of spring for use of serviceSalaryMatrixColSetup method in this controller
	 */
	@Autowired(required=true)
	ServiceSalaryMatrixColSetup serviceSalaryMatrixColSetup;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * 
	 * @param request
	 * @param dtoSalaryMatrixColSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createt(HttpServletRequest request, @RequestBody DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup) throws Exception {
		LOGGER.info("Create SalaryMatrixColSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSalaryMatrixColSetup = serviceSalaryMatrixColSetup.saveOrUpdateSalaryMatrixColSetup(dtoSalaryMatrixColSetup);
			responseMessage=displayMessage(dtoSalaryMatrixColSetup, "SALARY_MATRIX_COL_SETUP_CREATED", "SALARY_MATRIX_COL_SETUP_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create SalaryMatrixColSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoSalaryMatrixColSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup) throws Exception {
		LOGGER.info("Update SalaryMatrixColSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSalaryMatrixColSetup = serviceSalaryMatrixColSetup.saveOrUpdateSalaryMatrixColSetup(dtoSalaryMatrixColSetup);
			responseMessage=displayMessage(dtoSalaryMatrixColSetup, "SALARY_MATRIX_COL_SETUP_UPDATED_SUCCESS", "SALARY_MATRIX_COL_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update SalaryMatrixColSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoSalaryMatrixColSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup) throws Exception {
		LOGGER.info("Delete SalaryMatrixColSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSalaryMatrixColSetup.getIds() != null && !dtoSalaryMatrixColSetup.getIds().isEmpty()) {
				DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup2 = serviceSalaryMatrixColSetup.deleteSalaryMatrixColSetup(dtoSalaryMatrixColSetup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("SALARY_MATRIX_COL_SETUP_DELETED", false), dtoSalaryMatrixColSetup2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete SalaryMatrixColSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search SalaryMatrixColSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSalaryMatrixColSetup.searchSalaryMatrixColSetup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SALARY_MATRIX_COL_SETUP_GET_ALL", "SALARY_MATRIX_COL_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search SalaryMatrixColSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
}
