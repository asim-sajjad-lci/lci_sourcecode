package com.bti.hcm.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.service.ServiceResponse;

@RestController
public class BaseController {
	
	protected ResponseMessage unauthorizedMsg(ServiceResponse serviceResponse) {
		return new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
				serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
	}
	
	protected <T> ResponseMessage displayMessage( T obj,String successMsg,String failMsg,ServiceResponse response) {
		if (obj != null) {
			return new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					response.getMessageByShortAndIsDeleted(successMsg, false), obj);
		} else {
			return new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					response.getMessageByShortAndIsDeleted(failMsg, false), obj);
		}
		
	}

	protected ResponseMessage displayCode() {
		return new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "No records present");
	}
}
