package com.bti.hcm.model.dto;

public class DtoBatchesDisplay {
	private Integer batchPrimaryId;
	private String batchId;
	private String description;
	private short status;
	private String userID;
	private Integer buildChecksId;
	private Boolean statusCheck;

	public Integer getBatchPrimaryId() {
		return batchPrimaryId;
	}

	public void setBatchPrimaryId(Integer batchPrimaryId) {
		this.batchPrimaryId = batchPrimaryId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public Integer getBuildChecksId() {
		return buildChecksId;
	}

	public void setBuildChecksId(Integer buildChecksId) {
		this.buildChecksId = buildChecksId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Boolean getStatusCheck() {
		return statusCheck;
	}

	public void setStatusCheck(Boolean statusCheck) {
		this.statusCheck = statusCheck;
	}

}
