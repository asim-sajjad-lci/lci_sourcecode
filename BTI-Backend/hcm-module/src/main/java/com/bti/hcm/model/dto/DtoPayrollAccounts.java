package com.bti.hcm.model.dto;

import java.util.List;

public class DtoPayrollAccounts extends DtoBase{
	private Integer id;
	private Short accountType;
	private Integer payrollCode;

	private DtoDepartment department;
	private DtoLocation location;
	private DtoPosition position;

	private List<DtoPosition> deletePosition;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getAccountType() {
		return accountType;
	}

	public void setAccountType(Short accountType) {
		this.accountType = accountType;
	}

	public Integer getPayrollCode() {
		return payrollCode;
	}

	public void setPayrollCode(Integer payrollCode) {
		this.payrollCode = payrollCode;
	}
	public List<DtoPosition> getDeletePosition() {
		return deletePosition;
	}

	public void setDeletePosition(List<DtoPosition> deletePosition) {
		this.deletePosition = deletePosition;
	}
	

	public DtoDepartment getDepartment() {
		return department;
	}

	public void setDepartment(DtoDepartment department) {
		this.department = department;
	}

	public DtoLocation getLocation() {
		return location;
	}

	public void setLocation(DtoLocation location) {
		this.location = location;
	}

	public DtoPosition getPosition() {
		return position;
	}

	public void setPosition(DtoPosition position) {
		this.position = position;
	}

}
