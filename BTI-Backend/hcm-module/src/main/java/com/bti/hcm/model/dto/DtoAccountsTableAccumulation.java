package com.bti.hcm.model.dto;

public class DtoAccountsTableAccumulation extends DtoBase {

	private Integer id;
	private Integer accountSegmant;
	private Integer transactionType;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAccountSegmant() {
		return accountSegmant;
	}
	public void setAccountSegmant(Integer accountSegmant) {
		this.accountSegmant = accountSegmant;
	}
	public Integer getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}

}
