package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40301",indexes = {
        @Index(columnList = "HLINRINDX")
})
public class HelthInsurance extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HLINRINDX")
	private Integer id;

	@Column(name = "HLINRID",columnDefinition = "char(15)")
	private String helthInsuranceId;
	
	@ManyToOne
	@JoinColumn(name="HLCOVRINDX")
	private HelthCoverageType helthCoverageType;
	
	@Column(name = "HLINRDSCR",columnDefinition = "char(31)")
	private String desc;
	
	@Column(name = "HLINRDSCRA",columnDefinition = "char(61)")
	private String arbicDesc;
	
	@Column(name = "HLINRFREQ")
	private short frequency;
	
	@ManyToOne
	@JoinColumn(name="INSCOMINDX")
	private CompnayInsurance compnayInsurance;
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "helthInsurance")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<HealthInsuranceEnrollment> listHealthInsuranceEnrollment;
	
	
	@Column(name = "HLINRGRNO",columnDefinition = "char(31)")
	private String groupNumber;
	
	@Column(name = "HLINRMAXAG")
	private Integer maxAge;
	
	@Column(name = "HLINRDEPNO")
	private Integer minAge;
	
	@Column(name = "HLINREMAMT",precision = 13,scale =3)
	private BigDecimal employeePay;
	
	@Column(name = "HLINRCOAMT",precision = 13,scale =3)
	private BigDecimal employerPay;
	
	@Column(name = "HLINREMEXP",precision = 13,scale =3)
	private BigDecimal expenses;
	
	@Column(name = "HLINRMAXBF",precision = 13,scale =3)
	private BigDecimal maxCoverage;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHelthInsuranceId() {
		return helthInsuranceId;
	}

	public void setHelthInsuranceId(String helthInsuranceId) {
		this.helthInsuranceId = helthInsuranceId;
	}

	public HelthCoverageType getHelthCoverageType() {
		return helthCoverageType;
	}

	public void setHelthCoverageType(HelthCoverageType helthCoverageType) {
		this.helthCoverageType = helthCoverageType;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public short getFrequency() {
		return frequency;
	}

	public void setFrequency(short frequency) {
		this.frequency = frequency;
	}

	public CompnayInsurance getCompnayInsurance() {
		return compnayInsurance;
	}

	public void setCompnayInsurance(CompnayInsurance compnayInsurance) {
		this.compnayInsurance = compnayInsurance;
	}

	public String getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}

	public Integer getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	public Integer getMinAge() {
		return minAge;
	}

	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}

	public BigDecimal getEmployeePay() {
		return employeePay;
	}

	public void setEmployeePay(BigDecimal employeePay) {
		this.employeePay = employeePay;
	}

	public BigDecimal getEmployerPay() {
		return employerPay;
	}

	public void setEmployerPay(BigDecimal employerPay) {
		this.employerPay = employerPay;
	}

	public BigDecimal getExpenses() {
		return expenses;
	}

	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}

	public BigDecimal getMaxCoverage() {
		return maxCoverage;
	}

	public void setMaxCoverage(BigDecimal maxCoverage) {
		this.maxCoverage = maxCoverage;
	}

	public List<HealthInsuranceEnrollment> getListHealthInsuranceEnrollment() {
		return listHealthInsuranceEnrollment;
	}

	public void setListHealthInsuranceEnrollment(List<HealthInsuranceEnrollment> listHealthInsuranceEnrollment) {
		this.listHealthInsuranceEnrollment = listHealthInsuranceEnrollment;
	}
	
}
