package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10301",indexes = {
        @Index(columnList = "HCMDEFINX")
})
public class Default extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMDEFINX")
	private Integer id;

	@Column(name = "HCMDEFID", columnDefinition = "char(15)")
	private String defaultID;

	@Column(name = "HCMDEFDSC", columnDefinition = "char(90)")
	private String desc;

	@Column(name = "HCMDEFDSCA", columnDefinition = "char(140)")
	private String arabicDesc;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "default1")
	@Where(clause = "is_deleted = false")
	private List<BuildChecks> listBuildChecks;
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "defaults")
	@Where(clause = "is_deleted = false")
	private List<PayrollTransactionOpenYearHeader> listPayrollTransactionOpenYearHeader;
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "default1")
	@Where(clause = "is_deleted = false")
	private List<Distribution> listDistribution;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDefaultID() {
		return defaultID;
	}

	public void setDefaultID(String defaultID) {
		this.defaultID = defaultID;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public List<BuildChecks> getListBuildChecks() {
		return listBuildChecks;
	}

	public void setListBuildChecks(List<BuildChecks> listBuildChecks) {
		this.listBuildChecks = listBuildChecks;
	}

	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

	public List<Distribution> getListDistribution() {
		return listDistribution;
	}

	public void setListDistribution(List<Distribution> listDistribution) {
		this.listDistribution = listDistribution;
	}

	public List<PayrollTransactionOpenYearHeader> getListPayrollTransactionOpenYearHeader() {
		return listPayrollTransactionOpenYearHeader;
	}

	public void setListPayrollTransactionOpenYearHeader(
			List<PayrollTransactionOpenYearHeader> listPayrollTransactionOpenYearHeader) {
		this.listPayrollTransactionOpenYearHeader = listPayrollTransactionOpenYearHeader;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((arabicDesc == null) ? 0 : arabicDesc.hashCode());
		result = prime * result + ((defaultID == null) ? 0 : defaultID.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listBuildChecks == null) ? 0 : listBuildChecks.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
