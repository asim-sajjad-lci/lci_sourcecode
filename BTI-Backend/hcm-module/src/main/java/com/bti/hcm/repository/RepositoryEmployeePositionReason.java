package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeePositionReason;

@Repository("repositoryEmployeePositionReason")
public interface RepositoryEmployeePositionReason extends JpaRepository<EmployeePositionReason, Integer>{

	@Query("select e from EmployeePositionReason e where (e.positionReason like :searchKeyWord) and e.isDeleted=false")
	public List<EmployeePositionReason> predictiveEmployeePositionReasonSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	public List<EmployeePositionReason> findByIsDeleted(boolean isDeleted);


	@Query("select count(*) from EmployeePositionReason e where (e.positionReason LIKE :searchKeyWord) and e.isDeleted=false")
	public Integer predictiveGetAllPositionReasonSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select e from EmployeePositionReason e where (e.positionReason like :searchKeyWord) and e.isDeleted=false")
	public List<EmployeePositionReason> predictiveEmployeeGetAllPositionReasonSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
}
