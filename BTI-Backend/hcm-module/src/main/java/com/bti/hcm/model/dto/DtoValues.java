package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.Miscellaneous;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoValues extends DtoBase{

	private int id;
	private String valueId;
	private String valueDescription;
	private String valueArabicDescription;
	private Miscellaneous miscellaneous;
	private int miscId;
	private List<DtoValues> deleteMiscellaneousValues;
	private DtoMiscellaneous dtoMiscellaneous;
	private String codeName;
	private int valId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getValueId() {
		return valueId;
	}
	public void setValueId(String valueId) {
		this.valueId = valueId;
	}
	public String getValueDescription() {
		return valueDescription;
	}
	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}
	public String getValueArabicDescription() {
		return valueArabicDescription;
	}
	public void setValueArabicDescription(String valueArabicDescription) {
		this.valueArabicDescription = valueArabicDescription;
	}
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}
	public List<DtoValues> getDeleteMiscellaneousValues() {
		return deleteMiscellaneousValues;
	}
	public void setDeleteMiscellaneousValues(List<DtoValues> deleteMiscellaneousValues) {
		this.deleteMiscellaneousValues = deleteMiscellaneousValues;
	}
	public DtoMiscellaneous getDtoMiscellaneous() {
		return dtoMiscellaneous;
	}
	public void setDtoMiscellaneous(DtoMiscellaneous dtoMiscellaneous) {
		this.dtoMiscellaneous = dtoMiscellaneous;
	}
	public int getMiscId() {
		return miscId;
	}
	public void setMiscId(int miscId) {
		this.miscId = miscId;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public int getValId() {
		return valId;
	}
	public void setValId(int valId) {
		this.valId = valId;
	}

	
	
}
