/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.dto.DtoCity;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryHrCity;


/**
 * Description: Service HrCity 
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("ServiceHrcity")
public class ServiceHrCity {
	
	/**
	 * @Description LOGGER use for put a logger in HrCity Service
	 */
	static Logger log = Logger.getLogger(ServiceHrCity.class.getName());
	
	/**
	 * @Description repositoryHrCity Autowired here using annotation of spring for access of repositoryHrCity method in HrCity service
	 * 				In short Access HrCity Query from Database using repositoryHrCity.
	 */
	@Autowired
	RepositoryHrCity repositoryHrCity;
	
	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in HrCity service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @description Searching a City for HrModule
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchCity(DtoSearch dtoSearch){
		log.info("searchCity  Method");
		if(dtoSearch != null){
			List<HrCity> cityList = this.repositoryHrCity.predictiveCitySearch(dtoSearch.getSearchKeyword());
			if(cityList != null && !cityList.isEmpty()){
				List<DtoCity> dtoCityList = new ArrayList<DtoCity>();
				for (HrCity hrCity : cityList) {
					DtoCity dtoCity = new DtoCity(hrCity);
					dtoCityList.add(dtoCity);
				}
				dtoSearch.setRecords(dtoCityList);
			}
		}
		return dtoSearch;
	}
	
	/**
	 * @description get all city list in dropdown
	 * @return
	 */
	public List<DtoCity> getCityListWithLanguage() {
		log.info("getCityListWithLanguage  Method");
		String langId = httpServletRequest.getHeader("langid");
		List<DtoCity> cityList = new ArrayList<>();
//		List<HrCity> list = repositoryHrCity.findByIsDeletedAndLanguageLanguageId(false, Integer.parseInt(langId));
		List<HrCity> list = repositoryHrCity.findByIsDeletedAndLanguageLanguageId(false, 1);
		if(list.size()==0){
//			list = repositoryHrCity.findByIsDeletedAndLanguageLanguageId(false, 1);
			list = repositoryHrCity.findByIsDeletedAndLanguageLanguageId(false, 1);
		}
		if (list != null && list.size() > 0) {
			for (HrCity hrCity : list) {
				DtoCity dtoCity = new DtoCity();
				dtoCity.setCityId(hrCity.getCityId());
				dtoCity.setCityName(hrCity.getCityName());
				
				cityList.add(dtoCity);
			}
		}
		log.debug("City is:"+cityList.size());
		return cityList;
	}

	public List<DtoCity> getCityListBaseOnState(DtoCity dtoHrState) {
		List<DtoCity> cityList = new ArrayList<>();
		List<HrCity> list = repositoryHrCity.findByStateId(dtoHrState.getStateId());
		if (list != null && !list.isEmpty()) {
			for (HrCity hrState : list) {
				DtoCity dtoCity = new DtoCity();
				dtoCity.setCityName(hrState.getCityName());
				dtoCity.setCityId(hrState.getCityId());
				cityList.add(dtoCity);
			}
		}
		log.debug("City is:"+cityList.size());
		return cityList;
	}
	
	public List<DtoCity> getCityByStateAndLangId(DtoCity dtoHrState) {
		String langId = httpServletRequest.getHeader("langid");
		List<DtoCity> cityList = new ArrayList<>();
//		List<HrCity> list = repositoryHrCity.getByStateIdAndLangId(dtoHrState.getStateId(),Integer.valueOf(langId));
		List<HrCity> list = repositoryHrCity.findByStateId(dtoHrState.getStateId());
		if (list != null && !list.isEmpty()) {
			for (HrCity hrState : list) {
				DtoCity dtoCity = new DtoCity();
				dtoCity.setCityName(hrState.getCityName());
				dtoCity.setCityId(hrState.getCityId());
				cityList.add(dtoCity);
			}
		}
		log.debug("City is:"+cityList.size());
		return cityList;
	}
	

}
