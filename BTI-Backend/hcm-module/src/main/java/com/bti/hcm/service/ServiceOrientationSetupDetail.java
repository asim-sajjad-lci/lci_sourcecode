package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.OrientationSetupDetail;
import com.bti.hcm.model.dto.DtoOrientationSetupDetail;
import com.bti.hcm.repository.RepositoryOrientationCheckListSetup;
import com.bti.hcm.repository.RepositoryOrientationPredefinedCheckListItem;
import com.bti.hcm.repository.RepositoryOrientationSetup;
import com.bti.hcm.repository.RepositoryOrientationSetupDetail;

@Service("serviceOrientationSetupDetail")
public class ServiceOrientationSetupDetail {

	
	static Logger log = Logger.getLogger(ServiceOrientationSetupDetail.class.getName());
	
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryOrientationSetup repositoryOrientationSetup;
	
	@Autowired
	RepositoryOrientationPredefinedCheckListItem repositoryOrientationPredefinedCheckListItem;
	
	
	@Autowired
	RepositoryOrientationCheckListSetup repositoryOrientationCheckListSetup;
	
	@Autowired
	RepositoryOrientationSetupDetail repositoryOrientationSetupDetail;
	
	
	
	
	public DtoOrientationSetupDetail deleteOrientationSetupDetail(List<Integer> ids) {
		log.info("delete OrientationSetupDetail Method");
		DtoOrientationSetupDetail dtoOrientationSetupDetail = new DtoOrientationSetupDetail();
		dtoOrientationSetupDetail.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ORIENTATIONSETUP_DETAIL_DELETED", false));
		dtoOrientationSetupDetail.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ORIENTATIONSETUP_DETAIL_ASSOCIATED", false));
		List<DtoOrientationSetupDetail> deleteDtoOrientationSetupDetail = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		try {
			for (Integer id : ids) {
				OrientationSetupDetail orientationSetupDetail = repositoryOrientationSetupDetail.findOne(id);
				DtoOrientationSetupDetail dtoOrientationSetupDetail2 = new DtoOrientationSetupDetail();
				dtoOrientationSetupDetail2.setId(orientationSetupDetail.getId());
				dtoOrientationSetupDetail2.setSequence(orientationSetupDetail.getSequence());
				dtoOrientationSetupDetail2.setChecklistitem(orientationSetupDetail.getChecklistitem());
				dtoOrientationSetupDetail2.setArabicDescription(orientationSetupDetail.getArabicDescription());
				repositoryOrientationSetupDetail.deleteSingleOrientationSetupDetail(true, loggedInUserId,id);
				deleteDtoOrientationSetupDetail.add(dtoOrientationSetupDetail2);
			}
			dtoOrientationSetupDetail.setDelete(deleteDtoOrientationSetupDetail);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete OrientationSetupDetail :"+dtoOrientationSetupDetail.getId());
		return dtoOrientationSetupDetail;
	}
}
