package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00931",indexes = {
        @Index(columnList = "EMPRELSHINDX")
})
@NamedQuery(name = "Applicant.findAll", query = "SELECT a FROM Applicant a")

public class EmployeeDependents extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPRELSHINDX")
	private int id;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "employeeDependents")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<EmployeeDependent> listEmployeeDependent;

	@Column(name = "EMPRELSHID", columnDefinition = "char(10)")
	private String empRelationshipId;

	@Column(name = "EMPRELDSCR", columnDefinition = "char(61)")
	private String desc;

	@Column(name = "EMPRELDSCRA", columnDefinition = "char(120)")
	private String arabicDesc;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmpRelationshipId() {
		return empRelationshipId;
	}

	public void setEmpRelationshipId(String empRelationshipId) {
		this.empRelationshipId = empRelationshipId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

	public List<EmployeeDependent> getListEmployeeDependent() {
		return listEmployeeDependent;
	}

	public void setListEmployeeDependent(List<EmployeeDependent> listEmployeeDependent) {
		this.listEmployeeDependent = listEmployeeDependent;
	}

}
