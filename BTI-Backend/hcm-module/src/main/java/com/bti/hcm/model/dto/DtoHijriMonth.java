package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Hijri month class having getter and setter for fields (POJO) Name
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 * 
 * @author Bansi
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoHijriMonth extends DtoHijri {

	private String month;


	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}


}
