package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "GL40002_h")
public class AuditTrialCodes extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "SERIES")
	private Integer serialId;

	@Column(name = "SEQNUMBR")
	private Integer sequenceNumber;

	@Column(name = "TRXSOURC")
	private String transectionSourceDesc;

	@Column(name = "TRXSRCPX")
	private String transectionSourceCode;

	@Column(name = "NXTTRXNUM")
	private Integer nextTransectionSourceNumber;
	

	@ManyToOne
	@JoinColumn(name = "HCMBULDINXD")
	private Distribution  distribution;
	
	@ManyToOne
	@JoinColumn(name = "HCMDEFINX")
	private Default defaultId;
	
	@Column(name = "SOURCDOC")
	private String sourceDocment;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSerialId() {
		return serialId;
	}

	public void setSerialId(Integer serialId) {
		this.serialId = serialId;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getTransectionSourceDesc() {
		return transectionSourceDesc;
	}

	public void setTransectionSourceDesc(String transectionSourceDesc) {
		this.transectionSourceDesc = transectionSourceDesc;
	}

	public String getTransectionSourceCode() {
		return transectionSourceCode;
	}

	public void setTransectionSourceCode(String transectionSourceCode) {
		this.transectionSourceCode = transectionSourceCode;
	}

	public Integer getNextTransectionSourceNumber() {
		return nextTransectionSourceNumber;
	}

	public void setNextTransectionSourceNumber(Integer nextTransectionSourceNumber) {
		this.nextTransectionSourceNumber = nextTransectionSourceNumber;
	}

	public String getSourceDocment() {
		return sourceDocment;
	}

	public void setSourceDocment(String sourceDocment) {
		this.sourceDocment = sourceDocment;
	}

	public Distribution getDistribution() {
		return distribution;
	}

	public void setDistribution(Distribution distribution) {
		this.distribution = distribution;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nextTransectionSourceNumber == null) ? 0 : nextTransectionSourceNumber.hashCode());
		result = prime * result + ((sequenceNumber == null) ? 0 : sequenceNumber.hashCode());
		result = prime * result + ((serialId == null) ? 0 : serialId.hashCode());
		result = prime * result + ((sourceDocment == null) ? 0 : sourceDocment.hashCode());
		result = prime * result + ((transectionSourceCode == null) ? 0 : transectionSourceCode.hashCode());
		result = prime * result + ((transectionSourceDesc == null) ? 0 : transectionSourceDesc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditTrialCodes other = (AuditTrialCodes) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nextTransectionSourceNumber == null) {
			if (other.nextTransectionSourceNumber != null)
				return false;
		} else if (!nextTransectionSourceNumber.equals(other.nextTransectionSourceNumber))
			return false;
		if (sequenceNumber == null) {
			if (other.sequenceNumber != null)
				return false;
		} else if (!sequenceNumber.equals(other.sequenceNumber))
			return false;
		if (serialId == null) {
			if (other.serialId != null)
				return false;
		} else if (!serialId.equals(other.serialId))
			return false;
		if (sourceDocment == null) {
			if (other.sourceDocment != null)
				return false;
		} else if (!sourceDocment.equals(other.sourceDocment))
			return false;
		if (transectionSourceCode == null) {
			if (other.transectionSourceCode != null)
				return false;
		} else if (!transectionSourceCode.equals(other.transectionSourceCode))
			return false;
		if (transectionSourceDesc == null) {
			if (other.transectionSourceDesc != null)
				return false;
		} else if (!transectionSourceDesc.equals(other.transectionSourceDesc))
			return false;
		return true;
	}

	
	
	public Default getDefaultId() {
		return defaultId;
	}

	public void setDefaultId(Default defaultId) {
		this.defaultId = defaultId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AuditTrialCodes [id=" + id + ", serialId=" + serialId + ", sequenceNumber=" + sequenceNumber
				+ ", transectionSourceDesc=" + transectionSourceDesc + ", transectionSourceCode="
				+ transectionSourceCode + ", nextTransectionSourceNumber=" + nextTransectionSourceNumber
				+ ", sourceDocment=" + sourceDocment + "]";
	}

}
