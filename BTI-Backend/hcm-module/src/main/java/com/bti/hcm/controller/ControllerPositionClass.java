package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoPositionClass;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePositionClass;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerPositionClass
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/positionClass")
public class ControllerPositionClass extends BaseController{
	
	
	
	/**
	 * @Description LOGGER use for put a logger in PositionClass Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerPositionClass.class);
	
	/**
	 * @Description servicePositionClass Autowired here using annotation of spring for use of servicePositionClass method in this controller
	 */
	@Autowired(required=true)
	ServicePositionClass servicePositionClass;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @description : Create Position Class
	 * @param request
	 * @param dtoPositionClass
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createPositionClass(HttpServletRequest request, @RequestBody DtoPositionClass dtoPositionClass) throws Exception {
		LOGGER.info("Create Position Class Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionClass = servicePositionClass.saveOrUpdatePosition(dtoPositionClass);
			responseMessage=displayMessage(dtoPositionClass, "POSITION_CLASS_CREATED", "POSITION_CLASS_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PositionClass Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description : Update Position Class	
	 * @param request
	 * @param dtoPositionClass
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePositionClass(HttpServletRequest request, @RequestBody DtoPositionClass dtoPositionClass) throws Exception {
		LOGGER.info("Update Position Class Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionClass = servicePositionClass.saveOrUpdatePosition(dtoPositionClass);
			responseMessage=displayMessage(dtoPositionClass, "POSITION_CLASS_UPDATED_SUCCESS", MessageConstant.POSITION_CLASS_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Position Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description : Delete Position Class
	 * @param request
	 * @param dtoPositionClass
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePositionClass(HttpServletRequest request, @RequestBody DtoPositionClass dtoPositionClass) throws Exception {
		LOGGER.info("Delete Position Class Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPositionClass.getIds() != null && !dtoPositionClass.getIds().isEmpty()) {
				DtoPositionClass dtoPositionClass2 = servicePositionClass.deletePositionClass(dtoPositionClass.getIds());
				
				if(dtoPositionClass2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("POSITION_CLASS_DELETED", false), dtoPositionClass2);
				}
				else {
					
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoPositionClass2.getMessageType());
					btiMessageHcm.setMessageShort("POSITION_CLASS_NOT_DELETE_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							btiMessageHcm, dtoPositionClass2);
				}
				
				
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Position Class Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description get All Position Class
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Position Class Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionClass.searchPosition(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.POSITION_CLASS_GET_ALL, MessageConstant.POSITION_CLASS_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Position Class Methods:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * @description PostionClassId is exiest or not
	 * @param request
	 * @param dtoPositionClass
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPositionClassById", method = RequestMethod.POST)
	public ResponseMessage getPositionClassById(HttpServletRequest request, @RequestBody DtoPositionClass dtoPositionClass) throws Exception {
		LOGGER.info("Get Position Class ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPositionClass dtoPositionClassObj = servicePositionClass.getPositionByPositionId(dtoPositionClass.getId());
			responseMessage=displayMessage(dtoPositionClassObj, "POSITION_GET_ALL", "POSITION_CLASS_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Positon Class ById Method:"+dtoPositionClass.getId());
		return responseMessage;
	}
    @RequestMapping(value = "/positionClassIdcheck", method = RequestMethod.POST)
    public ResponseMessage positionClassIdCheck(HttpServletRequest request, @RequestBody DtoPositionClass dtoPositionClass) throws Exception {
        LOGGER.info("positionClassIdCheck ById Method");
        ResponseMessage responseMessage = null;
        boolean flag =serviceHcmHome.checkValidCompanyAccess();
        if (flag) {
            DtoPositionClass dtoPositionClassObj = servicePositionClass.repeatByPositinClassId(dtoPositionClass.getPositionClassId());
            if (dtoPositionClassObj != null) {
                if (dtoPositionClassObj.getMessageType() == null) {
                    responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                            serviceResponse.getMessageByShortAndIsDeleted("POSITION_CLASS_RESULT", false), dtoPositionClassObj);
                } else {
                    responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                            serviceResponse.getMessageByShortAndIsDeleted(dtoPositionClassObj.getMessageType(), false),
                            dtoPositionClassObj);
                }

            } else {
                responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                        serviceResponse.getMessageByShortAndIsDeleted("POSITION_CLASS_REPEAT_NOT_FOUND", false), dtoPositionClassObj);
            }
        } else {
            responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
                    serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
        }
        return responseMessage;
    }
    @RequestMapping(value = "/getAllPostionClassId", method = RequestMethod.GET)
	public ResponseMessage getAllPostionClass(HttpServletRequest request) throws Exception {
		LOGGER.info("Get All Postion Class Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePositionClass.getAllPosition();
			responseMessage=displayMessage(dtoSearch, "POSITON_CLASS_GET_ALL", "POSITON_CLASS_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All Postion Class Method:"+responseMessage.getMessage());
		return responseMessage;
	}
    
    @RequestMapping(value = "/searchPositionClassId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPositionClassId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Position Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionClass.searchPositionClassId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "POSITION_CLASS_GET_ALL", "POSITION_CLASS_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Position Class Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
    
    
    
	@RequestMapping(value = "/searchAllPositionClassIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAllPositionClassIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("searchAllPositionClassIds Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionClass.searchAllPositionClassIds(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "POSITION_CLASS_GET_ALL", "POSITION_CLASS_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Position Class Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

    
    
}
