package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTypeFieldCodes;

@RestController
@RequestMapping("/typeFieldForCodes")
public class ControllerTypeFieldForCodes extends BaseController {

	private Logger log = Logger.getLogger(ControllerTypeFieldForCodes.class);

	@Autowired
	ServiceTypeFieldCodes serviceTypeFieldCodes;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@RequestMapping(value = "/getAllIds", method = RequestMethod.POST)
		//, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchIndexBasedOnCodeTypeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		log.info("Search searchIndexBasedOnCodeTypeId Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTypeFieldCodes.getIdsListByCodeTypeId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "CODES_GET_ALL_SUCCESS", "CODES_GET_ALL_FAILURE",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

}
