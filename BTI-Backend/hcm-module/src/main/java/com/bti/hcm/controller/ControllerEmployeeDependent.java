package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeDependent;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeDependent;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeDependent")
public class ControllerEmployeeDependent extends BaseController{
	
	
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeDependent.class);
	
	@Autowired
	ServiceEmployeeDependent serviceEmployeeDependent;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	/**
	 * 
	 * @param request
	 * @param dtoEmployeeDependent
	 * @return
	 * @throws Exception
	 * @request{
		  "sequence":1,
		  "empFistName":"aman1234",
		  "empMidleName":"raj",
		  "empLastName":"rajusdd",
		  "comments":"hones",
		  "dateOfBirth":1212121,
		  "phoneNumber":"7845532864",
		  "workNumber":"7845532864",
		  "address":"weeee",
		  "city":"sasas",
		  "employeeDependents":{
		  	"id":2
		  	
		  }
		}
		
	 * @response {
    "code": 201,
    "status": "CREATED",
    "result": {
        "id": null,
        "sequence": 1,
        "empFistName": "aman1234",
        "empMidleName": "raj",
        "empLastName": "rajusdd",
        "comments": "hones",
        "gender": 0,
        "dateOfBirth": 1212121,
        "phoneNumber": "7845532864",
        "workNumber": "7845532864",
        "address": "weeee",
        "city": "sasas",
        "employeeDependents": {
            "id": 2,
            "empRelationshipId": null,
            "desc": null,
            "arabicDesc": null,
            "pageNumber": null,
            "pageSize": null,
            "ids": null,
            "isActive": null,
            "messageType": null,
            "message": null,
            "deleteMessage": null,
            "associateMessage": null,
            "delete": null,
            "isRepeat": null
        },
        "pageNumber": null,
        "pageSize": null,
        "ids": null,
        "isActive": null,
        "messageType": null,
        "message": null,
        "deleteMessage": null,
        "associateMessage": null,
        "delete": null,
        "isRepeat": null
    },
    "btiMessage": {
        "message": "N/A",
        "messageShort": "N/A"
    }
}
	 */

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createEmployeeDependent(HttpServletRequest request, @RequestBody DtoEmployeeDependent dtoEmployeeDependent) throws Exception {
		LOGGER.info("Create EmployeeDependent Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeDependent  = serviceEmployeeDependent.saveOrUpdate(dtoEmployeeDependent);
			responseMessage=displayMessage(dtoEmployeeDependent, "EMPLOYEE_DEPENDENT_CREATED", "EMPLOYEE_DEPENDENT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeeDependent Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	/**
	 * 
	 * @param request
	 * @param dtoEmployeeDependent
	 * @return
	 * @throws Exception
	 * @request{
		"id":9,
			  "sequence":1,
			  "empFistName":"manoj",
			  "empMidleName":"raj",
			  "empLastName":"goa",
			  "comments":"hones",
			  "dateOfBirth":1212121,
			  "phoneNumber":"7845532864",
			  "workNumber":"7845532864",
			  "address":"weeee",
			  "city":"sasas",
			  "employeeDependents":{
			  	"id":2
			  	
			  }
	}


	
	
	response{
	    "code": 201,
	    "status": "CREATED",
	    "result": {
	        "id": 9,
	        "sequence": 1,
	        "empFistName": "aman1234",
	        "empMidleName": "raj",
	        "empLastName": "rajusdd",
	        "comments": "hones",
	        "gender": 0,
	        "dateOfBirth": 1212121,
	        "phoneNumber": "7845532864",
	        "workNumber": "7845532864",
	        "address": "weeee",
	        "city": "sasas",
	        "employeeDependents": {
	            "id": 2,
	            "empRelationshipId": null,
	            "desc": null,
	            "arabicDesc": null,
	            "pageNumber": null,
	            "pageSize": null,
	            "ids": null,
	            "isActive": null,
	            "messageType": null,
	            "message": null,
	            "deleteMessage": null,
	            "associateMessage": null,
	            "delete": null,
	            "isRepeat": null
	        },
	        "pageNumber": null,
	        "pageSize": null,
	        "ids": null,
	        "isActive": null,
	        "messageType": null,
	        "message": null,
	        "deleteMessage": null,
	        "associateMessage": null,
	        "delete": null,
	        "isRepeat": null
	    },
	    "btiMessage": {
	        "message": "N/A",
	        "messageShort": "N/A"
	    }
	}
*/
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeeDependent dtoEmployeeDependent) throws Exception {
		LOGGER.info("Update EmployeeDependent Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeDependent = serviceEmployeeDependent.update(dtoEmployeeDependent);
			responseMessage=displayMessage(dtoEmployeeDependent, "EMPLOYEE_DEPENDENT_UPDATED", "EMPLOYEE_DEPENDENT_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeeDependent Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoEmployeeDependent
	 * @return
	 * @throws Exception
	 * @request{
		"ids":[3]
		}
		
response{
    "code": 201,
    "status": "CREATED",
    "result": {
        "id": 3,
        "sequence": 0,
        "empFistName": "aman1234",
        "empMidleName": null,
        "empLastName": null,
        "comments": null,
        "gender": 0,
        "dateOfBirth": null,
        "phoneNumber": null,
        "workNumber": null,
        "address": null,
        "city": null,
        "employeeDependents": null,
        "pageNumber": null,
        "pageSize": null,
        "ids": null,
        "isActive": null,
        "messageType": "",
        "message": null,
        "deleteMessage": "N/A",
        "associateMessage": "N/A",
        "delete": [
            {
                "id": null,
                "sequence": 0,
                "empFistName": null,
                "empMidleName": null,
                "empLastName": null,
                "comments": null,
                "gender": 0,
                "dateOfBirth": null,
                "phoneNumber": null,
                "workNumber": null,
                "address": null,
                "city": null,
                "employeeDependents": null,
                "pageNumber": null,
                "pageSize": null,
                "ids": null,
                "isActive": null,
                "messageType": null,
                "message": null,
                "deleteMessage": null,
                "associateMessage": null,
                "delete": null,
                "isRepeat": null
            }
        ],
        "isRepeat": null
    },
    "btiMessage": {
        "message": "N/A",
        "messageShort": "N/A"
    }
}

*/

	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoEmployeeDependent dtoEmployeeDependent) throws Exception {
		LOGGER.info("Delete EmployeeDependent Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeDependent.getIds() != null && !dtoEmployeeDependent.getIds().isEmpty()) {
				DtoEmployeeDependent dtoEmployeeDependent2 = serviceEmployeeDependent.deleteEmployeeDependent(dtoEmployeeDependent.getIds());
				if(dtoEmployeeDependent2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_DELETED", false), dtoEmployeeDependent2);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_NOT_DELETED", false), dtoEmployeeDependent2);
				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_LIST_IS_EMPTY", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeeDependent Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoEmployeeDependent
	 * @return
	 * @throws Exception
	 * @request{
		"id":"9"
		}
	
	

response{
    "code": 201,
    "status": "CREATED",
    "result": {
        "id": 9,
        "sequence": 1,
        "empFistName": "manoj",
        "empMidleName": "raj",
        "empLastName": "goa",
        "comments": "hones",
        "gender": 0,
        "dateOfBirth": 1212000,
        "phoneNumber": "7845532864",
        "workNumber": "7845532864",
        "address": "weeee",
        "city": "sasas",
        "employeeDependents": null,
        "pageNumber": null,
        "pageSize": null,
        "ids": null,
        "isActive": null,
        "messageType": null,
        "message": null,
        "deleteMessage": null,
        "associateMessage": null,
        "delete": null,
        "isRepeat": null
    },
    "btiMessage": {
        "message": "N/A",
        "messageShort": "N/A"
    }
}



	*/
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoEmployeeDependent dtoEmployeeDependent) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeDependent dtoEmployeeDependentObj = serviceEmployeeDependent.getById(dtoEmployeeDependent.getId());
			responseMessage=displayMessage(dtoEmployeeDependentObj, "EMPLOYEE_DEPENDENT_LIST_GET_DETAIL", "EMPLOYEE_DEPENDENT_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoEmployeeDependent.getId());
		return responseMessage;
	}
	/**
	 * 
	 * @param request
	 * @param dtoEmployeeDependent
	 * @return
	 * @throws Exception
	 * @request{
		 "searchKeyword" : "",
		 "sortOn" : "",
		  "sortBy" : "ASC",
		 "pageNumber":"0",
		 "pageSize":"20"
		 }


	 {
		    "code": 200,
		    "status": "OK",
		    "result": {
		        "searchKeyword": "",
		        "pageNumber": 0,
		        "pageSize": 20,
		        "sortOn": "",
		        "sortBy": "ASC",
		        "totalCount": 7,
		        "records": [
		            {
		                "id": 1,
		                "sequence": 1,
		                "empFistName": "aman",
		                "empMidleName": "raj",
		                "empLastName": "raju",
		                "comments": "hones",
		                "gender": 0,
		                "dateOfBirth": 1212000,
		                "phoneNumber": "7845532864",
		                "workNumber": "7845532864",
		                "address": "weeee",
		                "city": "sasas",
		                "employeeDependents": {
		                    "id": 1,
		                    "empRelationshipId": "wel1223",
		                    "desc": "hi",
		                    "arabicDesc": "welcome",
		                    "pageNumber": null,
		                    "pageSize": null,
		                    "ids": null,
		                    "isActive": null,
		                    "messageType": null,
		                    "message": null,
		                    "deleteMessage": null,
		                    "associateMessage": null,
		                    "delete": null,
		                    "isRepeat": null
		                },
		                "pageNumber": null,
		                "pageSize": null,
		                "ids": null,
		                "isActive": null,
		                "messageType": null,
		                "message": null,
		                "deleteMessage": null,
		                "associateMessage": null,
		                "delete": null,
		                "isRepeat": null
		            },
		            {
		                "id": 2,
		                "sequence": 1,
		                "empFistName": "aman1234",
		                "empMidleName": "raj",
		                "empLastName": "rajusdd",
		                "comments": "hones",
		                "gender": 0,
		                "dateOfBirth": 1212000,
		                "phoneNumber": "7845532864",
		                "workNumber": "7845532864",
		                "address": "weeee",
		                "city": "sasas",
		                "employeeDependents": {
		                    "id": 1,
		                    "empRelationshipId": "wel1223",
		                    "desc": "hi",
		                    "arabicDesc": "welcome",
		                    "pageNumber": null,
		                    "pageSize": null,
		                    "ids": null,
		                    "isActive": null,
		                    "messageType": null,
		                    "message": null,
		                    "deleteMessage": null,
		                    "associateMessage": null,
		                    "delete": null,
		                    "isRepeat": null
		                },
		                "pageNumber": null,
		                "pageSize": null,
		                "ids": null,
		                "isActive": null,
		                "messageType": null,
		                "message": null,
		                "deleteMessage": null,
		                "associateMessage": null,
		                "delete": null,
		                "isRepeat": null
		            },
		            {
		                "id": 4,
		                "sequence": 1,
		                "empFistName": "aman1234",
		                "empMidleName": "raj",
		                "empLastName": "rajusdd",
		                "comments": "hones",
		                "gender": 0,
		                "dateOfBirth": 1212000,
		                "phoneNumber": "7845532864",
		                "workNumber": "7845532864",
		                "address": "weeee",
		                "city": "sasas",
		                "employeeDependents": {
		                    "id": 1,
		                    "empRelationshipId": "wel1223",
		                    "desc": "hi",
		                    "arabicDesc": "welcome",
		                    "pageNumber": null,
		                    "pageSize": null,
		                    "ids": null,
		                    "isActive": null,
		                    "messageType": null,
		                    "message": null,
		                    "deleteMessage": null,
		                    "associateMessage": null,
		                    "delete": null,
		                    "isRepeat": null
		                },
		                "pageNumber": null,
		                "pageSize": null,
		                "ids": null,
		                "isActive": null,
		                "messageType": null,
		                "message": null,
		                "deleteMessage": null,
		                "associateMessage": null,
		                "delete": null,
		                "isRepeat": null
		            },
		            {
		                "id": 6,
		                "sequence": 1,
		                "empFistName": "aman1234",
		                "empMidleName": "raj",
		                "empLastName": "rajusdd",
		                "comments": "hones",
		                "gender": 0,
		                "dateOfBirth": 1212000,
		                "phoneNumber": "7845532864",
		                "workNumber": "7845532864",
		                "address": "weeee",
		                "city": "sasas",
		                "employeeDependents": {
		                    "id": 1,
		                    "empRelationshipId": "wel1223",
		                    "desc": "hi",
		                    "arabicDesc": "welcome",
		                    "pageNumber": null,
		                    "pageSize": null,
		                    "ids": null,
		                    "isActive": null,
		                    "messageType": null,
		                    "message": null,
		                    "deleteMessage": null,
		                    "associateMessage": null,
		                    "delete": null,
		                    "isRepeat": null
		                },
		                "pageNumber": null,
		                "pageSize": null,
		                "ids": null,
		                "isActive": null,
		                "messageType": null,
		                "message": null,
		                "deleteMessage": null,
		                "associateMessage": null,
		                "delete": null,
		                "isRepeat": null
		            },
		            {
		                "id": 7,
		                "sequence": 1,
		                "empFistName": "aman1234",
		                "empMidleName": "raj",
		                "empLastName": "rajusdd",
		                "comments": "hones",
		                "gender": 0,
		                "dateOfBirth": 1212000,
		                "phoneNumber": "7845532864",
		                "workNumber": "7845532864",
		                "address": "weeee",
		                "city": "sasas",
		                "employeeDependents": {
		                    "id": 1,
		                    "empRelationshipId": "wel1223",
		                    "desc": "hi",
		                    "arabicDesc": "welcome",
		                    "pageNumber": null,
		                    "pageSize": null,
		                    "ids": null,
		                    "isActive": null,
		                    "messageType": null,
		                    "message": null,
		                    "deleteMessage": null,
		                    "associateMessage": null,
		                    "delete": null,
		                    "isRepeat": null
		                },
		                "pageNumber": null,
		                "pageSize": null,
		                "ids": null,
		                "isActive": null,
		                "messageType": null,
		                "message": null,
		                "deleteMessage": null,
		                "associateMessage": null,
		                "delete": null,
		                "isRepeat": null
		            },
		            {
		                "id": 8,
		                "sequence": 1,
		                "empFistName": "aman1234",
		                "empMidleName": "raj",
		                "empLastName": "rajusdd",
		                "comments": "hones",
		                "gender": 0,
		                "dateOfBirth": 1212000,
		                "phoneNumber": "7845532864",
		                "workNumber": "7845532864",
		                "address": "weeee",
		                "city": "sasas",
		                "employeeDependents": {
		                    "id": 2,
		                    "empRelationshipId": "we45454re",
		                    "desc": "hdsdi",
		                    "arabicDesc": "welcomsdsde",
		                    "pageNumber": null,
		                    "pageSize": null,
		                    "ids": null,
		                    "isActive": null,
		                    "messageType": null,
		                    "message": null,
		                    "deleteMessage": null,
		                    "associateMessage": null,
		                    "delete": null,
		                    "isRepeat": null
		                },
		                "pageNumber": null,
		                "pageSize": null,
		                "ids": null,
		                "isActive": null,
		                "messageType": null,
		                "message": null,
		                "deleteMessage": null,
		                "associateMessage": null,
		                "delete": null,
		                "isRepeat": null
		            },
		            {
		                "id": 9,
		                "sequence": 1,
		                "empFistName": "manoj",
		                "empMidleName": "raj",
		                "empLastName": "goa",
		                "comments": "hones",
		                "gender": 0,
		                "dateOfBirth": 1212000,
		                "phoneNumber": "7845532864",
		                "workNumber": "7845532864",
		                "address": "weeee",
		                "city": "sasas",
		                "employeeDependents": {
		                    "id": 2,
		                    "empRelationshipId": "we45454re",
		                    "desc": "hdsdi",
		                    "arabicDesc": "welcomsdsde",
		                    "pageNumber": null,
		                    "pageSize": null,
		                    "ids": null,
		                    "isActive": null,
		                    "messageType": null,
		                    "message": null,
		                    "deleteMessage": null,
		                    "associateMessage": null,
		                    "delete": null,
		                    "isRepeat": null
		                },
		                "pageNumber": null,
		                "pageSize": null,
		                "ids": null,
		                "isActive": null,
		                "messageType": null,
		                "message": null,
		                "deleteMessage": null,
		                "associateMessage": null,
		                "delete": null,
		                "isRepeat": null
		            }
		        ]
		    },
		    "btiMessage": {
		        "message": "N/A",
		        "messageShort": "N/A"
		    }
		}*/
	 
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeDependent(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeDependent Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeDependent.searchEmployeeDependent(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_DEPENDENT_GET_ALL", "EMPLOYEE_DEPENDENT_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/employeeDependentsDropDownList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage employeeDependentsDropDownList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeeDependent> list = serviceEmployeeDependent.employeeDependentsDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_GET_ALL", false), list);
			}else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByEmployeeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeContacts Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeDependent.getDependentByEmployeeId(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
			responseMessage=new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					this.serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_GET_DETAIL", false),dtoSearch);
				
			}else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse
				.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_NOT_GETTING", false),dtoSearch);
				}
		}
		else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
