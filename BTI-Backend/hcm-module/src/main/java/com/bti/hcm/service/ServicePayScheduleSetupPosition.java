/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.PayScheduleSetup;
import com.bti.hcm.model.PayScheduleSetupPosition;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.dto.DtoPayScheduleSetupPosition;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPayScheduleSetup;
import com.bti.hcm.repository.RepositoryPayScheduleSetupPosition;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service PayScheduleSetupPosition
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("servicePayScheduleSetupPosition")
public class ServicePayScheduleSetupPosition {
	
	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupPosition Service
	 */
	static Logger log = Logger.getLogger(ServicePayScheduleSetupPosition.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in PayScheduleSetupPosition service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PayScheduleSetupPosition service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PayScheduleSetupPosition service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPayScheduleSetupPosition Autowired here using annotation of spring for access of repositoryPayScheduleSetupPosition method in PayScheduleSetupPosition service
	 * 				In short Access PayScheduleSetupPosition Query from Database using repositoryPayScheduleSetupPosition.
	 */
	@Autowired
	RepositoryPayScheduleSetupPosition repositoryPayScheduleSetupPosition;
	
	
	@Autowired
	RepositoryPayScheduleSetup repositoryPayScheduleSetup;
	
	@Autowired
	RepositoryPosition repositoryPosition;
	
	/**
	 * @param dtoPayScheduleSetupPosition
	 * @return
	 * @throws ParseException
	 */
	public DtoPayScheduleSetupPosition saveOrUpdatePayScheduleSetupPosition(DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition) throws ParseException {
		log.info("saveOrUpdatePayScheduleSetupPosition Method");
		PayScheduleSetupPosition payScheduleSetupPosition = null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
			PayScheduleSetup payScheduleSetup = repositoryPayScheduleSetup.findOne(dtoPayScheduleSetupPosition.getPayScheduleSetupId());
			Position position = repositoryPosition.findOne(dtoPayScheduleSetupPosition.getPositionId());
			if (dtoPayScheduleSetupPosition.getPaySchedulePositionIndexId() != null && dtoPayScheduleSetupPosition.getPaySchedulePositionIndexId() > 0) {
				payScheduleSetupPosition = repositoryPayScheduleSetupPosition.findByPaySchedulePositionIndexIdAndIsDeleted(dtoPayScheduleSetupPosition.getPaySchedulePositionIndexId(), false);
				payScheduleSetupPosition.setUpdatedBy(loggedInUserId);
				payScheduleSetupPosition.setUpdatedDate(new Date());
			} else {
				payScheduleSetupPosition = new PayScheduleSetupPosition();
				payScheduleSetupPosition.setCreatedDate(new Date());
				
				Integer rowId = repositoryPayScheduleSetupPosition.getCountOfTotaPayScheduleSetupPosition();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				payScheduleSetupPosition.setRowId(increment);
			}
			payScheduleSetupPosition.setPositionDescription(dtoPayScheduleSetupPosition.getPositionDescription());
			payScheduleSetupPosition.setPositionDescriptionArabic(dtoPayScheduleSetupPosition.getPositionDescriptionArabic());
			payScheduleSetupPosition.setPayScheduleStatus(dtoPayScheduleSetupPosition.getPayScheduleStatus());
			payScheduleSetupPosition.setPayScheduleSetup(payScheduleSetup);
			payScheduleSetupPosition.setPosition(position);
			payScheduleSetupPosition.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			payScheduleSetupPosition = repositoryPayScheduleSetupPosition.saveAndFlush(payScheduleSetupPosition);
			log.debug("PayScheduleSetupPosition is:"+dtoPayScheduleSetupPosition.getPaySchedulePositionIndexId());
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPayScheduleSetupPosition;

	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPayScheduleSetupPosition(DtoSearch dtoSearch) {
		log.info("searchPayScheduleSetupPosition Method");
		
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "positionDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "positionDescriptionArabic" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "payScheduleSetup" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "position" : 
					condition+=dtoSearch.getSortOn();
					break;	
				default:
					condition+="paySchedulePositionIndexId";
				}
			}else{
				condition+="paySchedulePositionIndexId";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositoryPayScheduleSetupPosition.predictivePayScheduleSetupPositionSearchTotalCount("%"+searchWord+"%"));
				List<PayScheduleSetupPosition> payScheduleSetupPositionList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						payScheduleSetupPositionList = this.repositoryPayScheduleSetupPosition.predictivePayScheduleSetupPositionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						payScheduleSetupPositionList = this.repositoryPayScheduleSetupPosition.predictivePayScheduleSetupPositionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						payScheduleSetupPositionList = this.repositoryPayScheduleSetupPosition.predictivePayScheduleSetupPositionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(payScheduleSetupPositionList != null && !payScheduleSetupPositionList.isEmpty()){
					List<DtoPayScheduleSetupPosition> dtoPayScheduleSetupPositionList = new ArrayList<DtoPayScheduleSetupPosition>();
					DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition=null;
					for (PayScheduleSetupPosition payScheduleSetupPosition : payScheduleSetupPositionList) {
						dtoPayScheduleSetupPosition = new DtoPayScheduleSetupPosition(payScheduleSetupPosition);
						dtoPayScheduleSetupPosition.setPaySchedulePositionIndexId(payScheduleSetupPosition.getPaySchedulePositionIndexId());
						dtoPayScheduleSetupPosition.setPayScheduleSetupId(payScheduleSetupPosition.getPosition().getId());
						dtoPayScheduleSetupPosition.setPayScheduleStatus(payScheduleSetupPosition.getPayScheduleStatus());
						dtoPayScheduleSetupPosition.setPositionDescription(payScheduleSetupPosition.getPositionDescription());
						dtoPayScheduleSetupPosition.setPositionDescriptionArabic(payScheduleSetupPosition.getPositionDescriptionArabic());
						dtoPayScheduleSetupPosition.setPayScheduleSetupId(payScheduleSetupPosition.getPayScheduleSetup().getPayScheduleIndexId());
						dtoPayScheduleSetupPositionList.add(dtoPayScheduleSetupPosition);
					}
					dtoSearch.setRecords(dtoPayScheduleSetupPositionList);
				}
			}
			log.debug("Search PayScheduleSetupPosition Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoPayScheduleSetupPosition deletePayScheduleSetupPosition(List<Integer> ids) {
		log.info("deletePayScheduleSetupPosition Method");
		DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition = new DtoPayScheduleSetupPosition();
		try {
			dtoPayScheduleSetupPosition
					.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_DELETED", false));
			dtoPayScheduleSetupPosition.setAssociateMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_ASSOCIATED", false));
			List<DtoPayScheduleSetupPosition> deletePayScheduleSetupPosition = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
			try {
				for (Integer paySchedulePositionIndexId : ids) {
					PayScheduleSetupPosition payScheduleSetupPosition = repositoryPayScheduleSetupPosition.findOne(paySchedulePositionIndexId);
					DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition2 = new DtoPayScheduleSetupPosition();
					dtoPayScheduleSetupPosition2.setPaySchedulePositionIndexId(paySchedulePositionIndexId);
					dtoPayScheduleSetupPosition2.setPositionDescription(payScheduleSetupPosition.getPositionDescription());
					repositoryPayScheduleSetupPosition.deleteSinglPayScheduleSetupPosition(true, loggedInUserId, paySchedulePositionIndexId);
					deletePayScheduleSetupPosition.add(dtoPayScheduleSetupPosition2);

				}
				dtoPayScheduleSetupPosition.setDeletePayScheduleSetupPosition(deletePayScheduleSetupPosition);
			} catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete deletePayScheduleSetupPosition :"+dtoPayScheduleSetupPosition.getPaySchedulePositionIndexId());
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
		}
		
		return dtoPayScheduleSetupPosition;
	}
}
