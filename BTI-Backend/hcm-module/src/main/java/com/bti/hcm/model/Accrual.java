package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40203",indexes = {
        @Index(columnList = "ACCRINDXID")
})
public class Accrual extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACCRINDXID")
	private Integer id;

	@Column(name = "ACCRID")
	private String accuralId;

	@Column(name = "ACCRDSCR")
	private String desc;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accrual")
//	@LazyCollection(LazyCollectionOption.FALSE)
	private List<AccrualScheduleDetail> listAccrualScheduleDetail;

	@Column(name = "ACCRDSCRA")
	private String arbic;

	@ManyToOne
	@JoinColumn(name = "ACCBYINDX")
	private AccrualType accrualType;

	@Column(name = "NUMOFACCR")
	private Integer numHour;

	@Column(name = "NUMOFACCD")
	private Integer numDay;

	@Column(name = "MXACCHRS")
	private Integer maxAccrualHour;

	@Column(name = "MXHRSAVL")
	private Integer maxHour;

	@Column(name = "WRKHRSYR")
	private Integer workHour;

	@Column(name = "ACCRPERD")
	
	private short reasons;
	private short type;
	
	private short period;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAccuralId() {
		return accuralId;
	}

	public void setAccuralId(String accuralId) {
		this.accuralId = accuralId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbic() {
		return arbic;
	}

	public void setArbic(String arbic) {
		this.arbic = arbic;
	}

	public AccrualType getAccrualType() {
		return accrualType;
	}

	public void setAccrualType(AccrualType accrualType) {
		this.accrualType = accrualType;
	}

	public Integer getNumHour() {
		return numHour;
	}

	public void setNumHour(Integer numHour) {
		this.numHour = numHour;
	}

	public Integer getNumDay() {
		return numDay;
	}

	public void setNumDay(Integer numDay) {
		this.numDay = numDay;
	}

	public Integer getMaxAccrualHour() {
		return maxAccrualHour;
	}

	public void setMaxAccrualHour(Integer maxAccrualHour) {
		this.maxAccrualHour = maxAccrualHour;
	}

	public Integer getMaxHour() {
		return maxHour;
	}

	public void setMaxHour(Integer maxHour) {
		this.maxHour = maxHour;
	}

	public Integer getWorkHour() {
		return workHour;
	}

	public void setWorkHour(Integer workHour) {
		this.workHour = workHour;
	}

	public short getPeriod() {
		return period;
	}

	public void setPeriod(short period) {
		this.period = period;
	}

	public List<AccrualScheduleDetail> getListAccrualScheduleDetail() {
		return listAccrualScheduleDetail;
	}

	public void setListAccrualScheduleDetail(List<AccrualScheduleDetail> listAccrualScheduleDetail) {
		this.listAccrualScheduleDetail = listAccrualScheduleDetail;
	}

	public short getReasons() {
		return reasons;
	}

	public void setReasons(short reasons) {
		this.reasons = reasons;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accrualType == null) ? 0 : accrualType.hashCode());
		result = prime * result + ((accuralId == null) ? 0 : accuralId.hashCode());
		result = prime * result + ((arbic == null) ? 0 : arbic.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listAccrualScheduleDetail == null) ? 0 : listAccrualScheduleDetail.hashCode());
		result = prime * result + ((maxAccrualHour == null) ? 0 : maxAccrualHour.hashCode());
		result = prime * result + ((maxHour == null) ? 0 : maxHour.hashCode());
		result = prime * result + ((numDay == null) ? 0 : numDay.hashCode());
		result = prime * result + ((numHour == null) ? 0 : numHour.hashCode());
		result = prime * result + period;
		result = prime * result + reasons;
		result = prime * result + type;
		result = prime * result + ((workHour == null) ? 0 : workHour.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}


}
