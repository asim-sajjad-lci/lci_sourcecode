package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Position;

/**
 * @author Gaurav
 *
 */
@Repository("repositoryPositions")
public interface RepositoryPosition extends JpaRepository<Position, Integer>{

	Position findByIdAndIsDeleted(Integer id, boolean b);

	
	@Query("select count(*) from Position s where s.isDeleted=false")
	Integer getCountOfTotalPosition();
	
	public List<Position> findByIsDeleted(Boolean deleted);

	List<Position> findByIsDeleted(boolean b, Pageable pageable);

	List<Position> findByIsDeletedOrderByCreatedDateDesc(boolean b);


	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Position d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSinglePosition(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select p from Position p where (p.positionId =:positionId) and p.isDeleted=false")
	List<Position> findBypositionId(@Param("positionId")String positionId);;


	@Query("select count(*) from Position d where (d.reportToPostion like :searchKeyWord or d.positionClass.positionClassId like :searchKeyWord or d.reportToPostion like :searchKeyWord or d.positionId like :searchKeyWord or d.description like :searchKeyWord  or d.arabicDescription like :searchKeyWord) and d.isDeleted=false")
	Integer predictivePositionSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from Position d where (d.reportToPostion like :searchKeyWord or d.positionClass.positionClassId like :searchKeyWord or d.reportToPostion like :searchKeyWord or d.positionId like :searchKeyWord or d.description like :searchKeyWord  or d.arabicDescription like :searchKeyWord) and d.isDeleted=false")
	List<Position> predictivePositionSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);
	
	@Query("select count(*) from Position d where (d.id=:searchKeyWord) and d.isDeleted=false")
	Integer predictivePositionSearchTotalCount1(@Param("searchKeyWord") Integer searchKeyWord);

	@Query("select d from Position d where (d.id=:searchKeyWord) and d.isDeleted=false")
	List<Position> predictivePositionSearchWithPagination1(@Param("searchKeyWord") Integer searchKeyWord);

	@Query("select d.positionId from Position d where (d.positionId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<String> predictivePositionIdSearchWithPagination(@Param("searchKeyWord")String string);

	@Query("select d from Position d where (d.positionId like :searchKeyWord) and d.isDeleted=false")
	List<Position> predictiveGetAllPositionIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	 @Query("select d from Position d where (d.positionId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<Position> predictiveSearchAllPostionIdsWithPagination(@Param("searchKeyWord")String positionId);

	@Query("select count(*) from Position p ")
	public Integer getCountOfTotaPosition();
	 
}
