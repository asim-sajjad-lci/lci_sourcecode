package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoCompnayInsurance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceCompanyInsurance;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/helthInsurance")
public class ControllerCompanyInsurance extends BaseController{
	/**
	 * @Description LOGGER use for put a logger in HelthInsurance Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerCompanyInsurance.class);
	
	/**
	 * @Description serviceDepartment Autowired here using annotation of spring for use of ServiceHelthInsurance method in this controller
	 */
	@Autowired(required=true)
	ServiceCompanyInsurance serviceHelthInsurance;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createDepartment(HttpServletRequest request, @RequestBody DtoCompnayInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("Create Department Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHelthInsurance = serviceHelthInsurance.saveOrUpdate(dtoHelthInsurance);
			responseMessage=displayMessage(dtoHelthInsurance, "HELTH_INSURANCE_COMPANY_CREATED", "HELTH_INSURANCE_COMPANY_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Department Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateDepartment(HttpServletRequest request, @RequestBody DtoCompnayInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("Update Department Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHelthInsurance = serviceHelthInsurance.saveOrUpdate(dtoHelthInsurance);
			responseMessage=displayMessage(dtoHelthInsurance, "HELTH_INSURANCE_COMPANY_UPDATED_SUCCESS", "HELTH_INSURANCE_COMPANY_NOT_UPDATED_SUCCESS", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Department Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	/*
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteDepartment(HttpServletRequest request, @RequestBody DtoCompnayInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("Delete Department Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoHelthInsurance.getIds() != null && !dtoHelthInsurance.getIds().isEmpty()) {
				DtoCompnayInsurance dtoDepartment2 = serviceHelthInsurance.deleteHelthInsurance(dtoHelthInsurance.getIds());
				if(dtoDepartment2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("HELTH_INSURANCE_COMPANY_DELETED", false), dtoDepartment2);	
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoDepartment2.getMessageType());
					btiMessageHcm.setMessageShort("HELTH_INSURANCE__COMPANY_NOT_DELETE_ID_MESSAGE");
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoDepartment2);
					 
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Department Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	*/
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteDepartment(HttpServletRequest request, @RequestBody DtoCompnayInsurance dtoCompnayInsurance) throws Exception {
		LOGGER.info("Delete CompnayInsurance  Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoCompnayInsurance.getIds() != null && !dtoCompnayInsurance.getIds().isEmpty()) {
				DtoCompnayInsurance dtoCompnayInsurance2 = serviceHelthInsurance.deleteHelthInsurance(dtoCompnayInsurance.getIds());
				
				if(dtoCompnayInsurance2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("HELTH_INSURANCE_COMPANY_DELETED", false), dtoCompnayInsurance2);	
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("HELTH_INSURANCE__COMPANY_NOT_DELETE_ID_MESSAGE", false), dtoCompnayInsurance2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete CompnayInsurance  Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchDepartment(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search CompnayInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceHelthInsurance.searchHelthInsurance(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.HELTH_INSURANCE_COMPANY_GET_ALL, MessageConstant.HELTH_INSURANCE_COMAPNY_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search CompnayInsurance Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAllDepartment(HttpServletRequest request, @RequestBody DtoCompnayInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("Get All CompnayInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceHelthInsurance.getAll(dtoHelthInsurance);
			responseMessage=displayMessage(dtoSearch, MessageConstant.HELTH_INSURANCE_COMPANY_GET_ALL, MessageConstant.HELTH_INSURANCE_COMAPNY_LIST_NOT_GETTING, serviceResponse);		
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All CompnayInsurance Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getDepartmentById(HttpServletRequest request, @RequestBody DtoCompnayInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("Get CompnayInsurance ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoCompnayInsurance dtoDepartmentObj = serviceHelthInsurance.getById(dtoHelthInsurance.getId());
			responseMessage=displayMessage(dtoDepartmentObj, MessageConstant.HELTH_INSURANCE_COMPANY_GET_ALL, MessageConstant.HELTH_INSURANCE_COMAPNY_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get CompnayInsurance ById Method:"+dtoHelthInsurance.getId());
		return responseMessage;
	}
	
	
	/*@RequestMapping(value = "/helthInsuranceIdcheck", method = RequestMethod.POST)
	public ResponseMessage departmetnIdCheck(HttpServletRequest request, @RequestBody DtoCompnayInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoCompnayInsurance dtoDepartmentObj = serviceHelthInsurance.repeatByCompanyId(dtoHelthInsurance.getInsCompanyId());
			responseMessage=displayMessage(dtoDepartmentObj, "HELTH_INSURANCE_COMAPNY_RESULT", "HELTH_INSURANCE__COMPANY_REPEAT_ID_NOT_FOUND", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}*/
	
	
	
	

	@RequestMapping(value = "/helthInsuranceIdcheck", method = RequestMethod.POST)
	public ResponseMessage departmetnIdCheck(HttpServletRequest request, @RequestBody DtoCompnayInsurance dtoHelthInsurance)
			throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoCompnayInsurance dtoDepartmentObj = serviceHelthInsurance.repeatByCompanyId(dtoHelthInsurance.getInsCompanyId());
			
			 if (dtoDepartmentObj !=null && dtoDepartmentObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                		 serviceResponse.getMessageByShortAndIsDeleted("HELTH_INSURANCE_COMAPNY_RESULT", false), dtoDepartmentObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                		 serviceResponse.getMessageByShortAndIsDeleted("HELTH_INSURANCE__COMPANY_REPEAT_ID_NOT_FOUND", false),
                         dtoDepartmentObj);
             }
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/searchCompanyId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchCompanyId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search CompanyId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceHelthInsurance.searchCompanyId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "HELTH_INSURANCE_COMPANY_GET_ALL", "HELTH_INSURANCE_COMAPNY_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllCompanyInsurance", method = RequestMethod.GET)
	public ResponseMessage getAllCompanyInsurance(HttpServletRequest request) throws Exception {
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceHelthInsurance.getAllCompanyInsurance();
			responseMessage=displayMessage(dtoSearch, MessageConstant.HELTH_INSURANCE_COMPANY_GET_ALL, MessageConstant.HELTH_INSURANCE_COMAPNY_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
