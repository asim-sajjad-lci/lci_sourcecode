package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90310")
public class PayrollTransactionHistoryYearHeader extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RWPRLINDX")
	private Integer id;

	@Column(name = "YEAR1")
	private Integer year;

	@Column(name = "HCMBULDDT")
	private Date buildCheck;

	@Column(name = "HCMBULUSR")
	private String buildCheckByUserId;

	@Column(name = "HCMPYFDT")
	private Date payPeriodfromDate;

	@Column(name = "HCMPYTDT")
	private Date payPeriodToDate;

	@Column(name = "HCMPSTDT")
	private Date payrollPostDate;

	@Column(name = "HCMPWEK")
	private Boolean includePayPeriodsWeekly;

	@Column(name = "HCMPBWEK")
	private Boolean includePayPeriodsByWeekly;

	@Column(name = "HCMPSMON")
	private Boolean includePayPeriodsSemimonthly;

	@Column(name = "HCMPMON")
	private Boolean includePayPeriodsMonthly;

	@Column(name = "HCMPQUR")
	private Boolean includePayPeriodsQuarterly;

	@Column(name = "HCMPSYR")
	private Boolean includePayPeriodsSemiannually;

	@Column(name = "HCMPYR")
	private Boolean includePayPeriodsAnnually;

	@Column(name = "HCMPDMS")
	private Boolean includePayPeriodsDailyMisc;

	@Column(name = "HCMALLEM")
	private Boolean allEmployees;

	@Column(name = "HCMFEMINX")
	private Integer fromEmployeeIndexId;

	@Column(name = "HCMTEMINX")
	private Integer toEmployeeIndexID;

	@Column(name = "HCMALLDEP")
	private Boolean allDepartment;

	@Column(name = "HCMFDEPINX")
	private Integer fromDepartmentIndexId;

	@Column(name = "HCMTDEPINX")
	private Integer toDepartmentIndexId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Date getBuildCheck() {
		return buildCheck;
	}

	public void setBuildCheck(Date buildCheck) {
		this.buildCheck = buildCheck;
	}

	public String getBuildCheckByUserId() {
		return buildCheckByUserId;
	}

	public void setBuildCheckByUserId(String buildCheckByUserId) {
		this.buildCheckByUserId = buildCheckByUserId;
	}

	public Date getPayPeriodfromDate() {
		return payPeriodfromDate;
	}

	public void setPayPeriodfromDate(Date payPeriodfromDate) {
		this.payPeriodfromDate = payPeriodfromDate;
	}

	public Date getPayPeriodToDate() {
		return payPeriodToDate;
	}

	public void setPayPeriodToDate(Date payPeriodToDate) {
		this.payPeriodToDate = payPeriodToDate;
	}

	public Date getPayrollPostDate() {
		return payrollPostDate;
	}

	public void setPayrollPostDate(Date payrollPostDate) {
		this.payrollPostDate = payrollPostDate;
	}

	public Boolean getIncludePayPeriodsWeekly() {
		return includePayPeriodsWeekly;
	}

	public void setIncludePayPeriodsWeekly(Boolean includePayPeriodsWeekly) {
		this.includePayPeriodsWeekly = includePayPeriodsWeekly;
	}

	public Boolean getIncludePayPeriodsByWeekly() {
		return includePayPeriodsByWeekly;
	}

	public void setIncludePayPeriodsByWeekly(Boolean includePayPeriodsByWeekly) {
		this.includePayPeriodsByWeekly = includePayPeriodsByWeekly;
	}

	public Boolean getIncludePayPeriodsSemimonthly() {
		return includePayPeriodsSemimonthly;
	}

	public void setIncludePayPeriodsSemimonthly(Boolean includePayPeriodsSemimonthly) {
		this.includePayPeriodsSemimonthly = includePayPeriodsSemimonthly;
	}

	public Boolean getIncludePayPeriodsMonthly() {
		return includePayPeriodsMonthly;
	}

	public void setIncludePayPeriodsMonthly(Boolean includePayPeriodsMonthly) {
		this.includePayPeriodsMonthly = includePayPeriodsMonthly;
	}

	public Boolean getIncludePayPeriodsQuarterly() {
		return includePayPeriodsQuarterly;
	}

	public void setIncludePayPeriodsQuarterly(Boolean includePayPeriodsQuarterly) {
		this.includePayPeriodsQuarterly = includePayPeriodsQuarterly;
	}

	public Boolean getIncludePayPeriodsSemiannually() {
		return includePayPeriodsSemiannually;
	}

	public void setIncludePayPeriodsSemiannually(Boolean includePayPeriodsSemiannually) {
		this.includePayPeriodsSemiannually = includePayPeriodsSemiannually;
	}

	public Boolean getIncludePayPeriodsAnnually() {
		return includePayPeriodsAnnually;
	}

	public void setIncludePayPeriodsAnnually(Boolean includePayPeriodsAnnually) {
		this.includePayPeriodsAnnually = includePayPeriodsAnnually;
	}

	public Boolean getIncludePayPeriodsDailyMisc() {
		return includePayPeriodsDailyMisc;
	}

	public void setIncludePayPeriodsDailyMisc(Boolean includePayPeriodsDailyMisc) {
		this.includePayPeriodsDailyMisc = includePayPeriodsDailyMisc;
	}

	public Boolean getAllEmployees() {
		return allEmployees;
	}

	public void setAllEmployees(Boolean allEmployees) {
		this.allEmployees = allEmployees;
	}

	public Integer getFromEmployeeIndexId() {
		return fromEmployeeIndexId;
	}

	public void setFromEmployeeIndexId(Integer fromEmployeeIndexId) {
		this.fromEmployeeIndexId = fromEmployeeIndexId;
	}

	public Integer getToEmployeeIndexID() {
		return toEmployeeIndexID;
	}

	public void setToEmployeeIndexID(Integer toEmployeeIndexID) {
		this.toEmployeeIndexID = toEmployeeIndexID;
	}

	public Boolean getAllDepartment() {
		return allDepartment;
	}

	public void setAllDepartment(Boolean allDepartment) {
		this.allDepartment = allDepartment;
	}

	public Integer getFromDepartmentIndexId() {
		return fromDepartmentIndexId;
	}

	public void setFromDepartmentIndexId(Integer fromDepartmentIndexId) {
		this.fromDepartmentIndexId = fromDepartmentIndexId;
	}

	public Integer getToDepartmentIndexId() {
		return toDepartmentIndexId;
	}

	public void setToDepartmentIndexId(Integer toDepartmentIndexId) {
		this.toDepartmentIndexId = toDepartmentIndexId;
	}

}
