package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bti.hcm.model.CalculateChecks;

public interface RepositoryCalculateChecks extends JpaRepository<CalculateChecks, Integer>{

	CalculateChecks findByIdAndIsDeleted(Integer id, boolean b);
	
	List<CalculateChecks> findByIsDeleted(boolean b, Pageable pageable);
	
	List<CalculateChecks> findByIsDeleted(boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update CalculateChecks d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleCalculateChecks(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	
	@Query("select count(*) from CalculateChecks d where d.auditTransNum like :searchKeyWord and  d.isDeleted=false")
	Integer predictiveCalculateChecksSearchTotalCount(@Param("searchKeyWord")String string);

	
	@Query("select d from CalculateChecks d where d.auditTransNum like :searchKeyWord and d.isDeleted=false")
	List<CalculateChecks> predictiveCalculateChecksSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update CalculateChecks d set d.isDeleted =:deleted ,d.updatedBy =:updateById")
	void deleteAllRecord(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById);

}
