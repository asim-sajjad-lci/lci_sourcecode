package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90300")
public class PayrollTransactionHistoryYearDetail extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RWPRLINDX")
	private Integer id;

	@Column(name = "YEAR1")
	private Integer year;

	@Column(name = "HCMTRXPYRL")
	private String auditTransactionNumber;

	@Column(name = "HCMEMPSEQN")
	private Integer employeeCodeSequence;

	@Column(name = "HCMCDTYP")
	private Short codeType;

	@Column(name = "HCMCDINDX")
	private Integer codeIndexId;

	@Column(name = "HCMCDAMT", precision = 10, scale = 3)
	private BigDecimal totalAmount;

	@Column(name = "HCMCDAAMT", precision = 10, scale = 3)
	private BigDecimal totalActualAmount;

	@Column(name = "HCMCDPRNT", precision = 10, scale = 3)
	private BigDecimal totalPercentCode;

	@Column(name = "HCMCDHRS")
	private Integer totalHours;

	@Column(name = "HCMBSDRTE", precision = 10, scale = 3)
	private BigDecimal rateofBaseOnCode;

	@Column(name = "HCMCDBSD", precision = 10, scale = 3)
	private Integer codeBaseOnCodeIndex;

	@Column(name = "HCMCDFDT")
	private Date codeFromPeriodDate;

	@Column(name = "HCMCDTDT")
	private Date codeToPeriodDate;

	@Column(name = "PRLPOSTDT")
	private Date payrollPostDate;

	@Column(name = "PRLPOSTURS")
	private String payrollPostByUserId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getAuditTransactionNumber() {
		return auditTransactionNumber;
	}

	public void setAuditTransactionNumber(String auditTransactionNumber) {
		this.auditTransactionNumber = auditTransactionNumber;
	}

	public Integer getEmployeeCodeSequence() {
		return employeeCodeSequence;
	}

	public void setEmployeeCodeSequence(Integer employeeCodeSequence) {
		this.employeeCodeSequence = employeeCodeSequence;
	}

	public Short getCodeType() {
		return codeType;
	}

	public void setCodeType(Short codeType) {
		this.codeType = codeType;
	}

	public Integer getCodeIndexId() {
		return codeIndexId;
	}

	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}

	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}

	public BigDecimal getTotalPercentCode() {
		return totalPercentCode;
	}

	public void setTotalPercentCode(BigDecimal totalPercentCode) {
		this.totalPercentCode = totalPercentCode;
	}

	public Integer getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public BigDecimal getRateofBaseOnCode() {
		return rateofBaseOnCode;
	}

	public void setRateofBaseOnCode(BigDecimal rateofBaseOnCode) {
		this.rateofBaseOnCode = rateofBaseOnCode;
	}

	public Integer getCodeBaseOnCodeIndex() {
		return codeBaseOnCodeIndex;
	}

	public void setCodeBaseOnCodeIndex(Integer codeBaseOnCodeIndex) {
		this.codeBaseOnCodeIndex = codeBaseOnCodeIndex;
	}

	public Date getCodeFromPeriodDate() {
		return codeFromPeriodDate;
	}

	public void setCodeFromPeriodDate(Date codeFromPeriodDate) {
		this.codeFromPeriodDate = codeFromPeriodDate;
	}

	public Date getCodeToPeriodDate() {
		return codeToPeriodDate;
	}

	public void setCodeToPeriodDate(Date codeToPeriodDate) {
		this.codeToPeriodDate = codeToPeriodDate;
	}

	public Date getPayrollPostDate() {
		return payrollPostDate;
	}

	public void setPayrollPostDate(Date payrollPostDate) {
		this.payrollPostDate = payrollPostDate;
	}

	public String getPayrollPostByUserId() {
		return payrollPostByUserId;
	}

	public void setPayrollPostByUserId(String payrollPostByUserId) {
		this.payrollPostByUserId = payrollPostByUserId;
	}

}
