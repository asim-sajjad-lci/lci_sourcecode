package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeContacts;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeContacts;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeContacts")
public class ControllerEmployeeContacts extends BaseController{
	
	@Autowired
	ServiceEmployeeContacts serviceEmployeeContacts;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeContacts.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoEmployeeContacts dtoEmployeeContacts) throws Exception{
		LOGGER.info("Create TimeCode Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeContacts = serviceEmployeeContacts.saveOrUpdate(dtoEmployeeContacts);
			responseMessage=displayMessage(dtoEmployeeContacts, "EMPLOYEE_CONTACTS_CREATED", "EMPLOYEE_CONTACTS_NOT_CREATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create TimeCode Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeeContacts dtoEmployeeContacts) throws Exception {
		LOGGER.info("Update TimeCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeContacts = serviceEmployeeContacts.update(dtoEmployeeContacts);
			responseMessage=displayMessage(dtoEmployeeContacts, "EMPLOYEE_CONTACTS_UPDATED_SUCCESS", "EMPLOYEE_CONTACTS_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update TimeCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoEmployeeContacts dtoEmployeeContacts) throws Exception {
		LOGGER.info("Delete EmployeeContactss Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeContacts.getIds() != null && !dtoEmployeeContacts.getIds().isEmpty()) {
				DtoEmployeeContacts dtoEmployeeContacts1 = serviceEmployeeContacts.delete(dtoEmployeeContacts.getIds());
				if(dtoEmployeeContacts1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("EMPLOYEE_CONTACTS_DELETED", false), dtoEmployeeContacts1);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("EMPLOYEE_CONTACTS_NOT_DELETE_ID_MESSAGE", false), dtoEmployeeContacts1);

				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeeContactss Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoEmployeeContacts dtoEmployeeContacts) throws Exception {
		LOGGER.info("Get EmployeeContacts ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeContacts dtoEmployeeContacts1 = serviceEmployeeContacts.getById(dtoEmployeeContacts.getId());
			responseMessage=displayMessage(dtoEmployeeContacts1, MessageConstant.EMPLOYEE_CONTACTS_GET_DETAIL, MessageConstant.EMPLOYEE_CONTACTS_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get EmployeeContacts by Id Method:"+dtoEmployeeContacts.getId());
		return responseMessage;
	}
	
	
	
		@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
		public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeContacts Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeContacts.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.EMPLOYEE_CONTACTS_GET_DETAIL, MessageConstant.EMPLOYEE_CONTACTS_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}
		
		
		@RequestMapping(value = "/getAllByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
		public ResponseMessage getAllByEmployeeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
			LOGGER.info("Search EmployeeContacts Method");
			ResponseMessage responseMessage = null;
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtoSearch = this.serviceEmployeeContacts.getContectByEmployeeId(dtoSearch);
				responseMessage=displayMessage(dtoSearch, MessageConstant.EMPLOYEE_CONTACTS_GET_DETAIL, MessageConstant.EMPLOYEE_CONTACTS_NOT_GETTING, response);
			} else {
				responseMessage = unauthorizedMsg(response);
			}
			return responseMessage;
		}
	
		@RequestMapping(value = "/getAllIds", method = RequestMethod.GET)
	public ResponseMessage getAllTimeCodeInActive(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeeContacts> timeCodeList = serviceEmployeeContacts.getAllList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted(MessageConstant.EMPLOYEE_CONTACTS_GET_DETAIL, false), timeCodeList);
			}else {
				responseMessage = unauthorizedMsg(response);
			}
			
		} catch (Exception e) {
			
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
	
	
}
