package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.CodeForDeductionBenefitPayCode;
import com.bti.hcm.model.dto.DtoCodeForDeductionBenefitPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryCodeForDeductionBenefitPayCode;

@Service("serviceCodeForDeductionBenefitPayCode")
public class ServiceCodeForDeductionBenefitPayCode {
	
	
	static Logger log = Logger.getLogger(ServiceCodeForDeductionBenefitPayCode.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryCodeForDeductionBenefitPayCode repositoryCodeForDeductionBenefitPayCode;

		
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		
		try {

			log.info("CodeForDeductionBenefitPayCode Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("defaultID")|| dtoSearch.getSortOn().equals("desc")|| dtoSearch.getSortOn().equals("arabicDesc")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
				dtoSearch.setTotalCount(this.repositoryCodeForDeductionBenefitPayCode.predictiveSearchTotalCount("%"+searchWord+"%"));
				List<CodeForDeductionBenefitPayCode> codeForDeductionBenefitPayCodeList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						codeForDeductionBenefitPayCodeList = this.repositoryCodeForDeductionBenefitPayCode.predictiveCodeForDeductionBenefitPayCodeSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						codeForDeductionBenefitPayCodeList = this.repositoryCodeForDeductionBenefitPayCode.predictiveCodeForDeductionBenefitPayCodeSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						codeForDeductionBenefitPayCodeList = this.repositoryCodeForDeductionBenefitPayCode.predictiveCodeForDeductionBenefitPayCodeSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}
				if(codeForDeductionBenefitPayCodeList != null && !codeForDeductionBenefitPayCodeList.isEmpty()){
					List<DtoCodeForDeductionBenefitPayCode> dtoCodeForDeductionBenefitPayCodeList = new ArrayList<>();
					DtoCodeForDeductionBenefitPayCode dtoCodeForDeductionBenefitPayCode=null;
					for (CodeForDeductionBenefitPayCode codeForDeductionBenefitPayCode : codeForDeductionBenefitPayCodeList) {
						dtoCodeForDeductionBenefitPayCode = new DtoCodeForDeductionBenefitPayCode(codeForDeductionBenefitPayCode);
						dtoCodeForDeductionBenefitPayCode.setId(codeForDeductionBenefitPayCode.getId());
						dtoCodeForDeductionBenefitPayCode.setCodeID(codeForDeductionBenefitPayCode.getCodeID());
						dtoCodeForDeductionBenefitPayCode.setDesc(codeForDeductionBenefitPayCode.getDesc());
						dtoCodeForDeductionBenefitPayCodeList.add(dtoCodeForDeductionBenefitPayCode);
						}
					dtoSearch.setRecords(dtoCodeForDeductionBenefitPayCodeList);
						}
			log.debug("Search CodeForDeductionBenefitPayCode Size is:"+dtoSearch.getTotalCount());
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}


	public List<DtoCodeForDeductionBenefitPayCode> getAllDropDown() {
		List<DtoCodeForDeductionBenefitPayCode>dtoCodeForDeductionBenefitPayCodesList=new ArrayList<>();
		List<CodeForDeductionBenefitPayCode>codeForDeductionBenefitPayCodesList=repositoryCodeForDeductionBenefitPayCode.findByIsDeleted(false);
		if(!codeForDeductionBenefitPayCodesList.isEmpty()) {
			for (CodeForDeductionBenefitPayCode codeForDeductionBenefitPayCode : codeForDeductionBenefitPayCodesList) {
				DtoCodeForDeductionBenefitPayCode dtoCodeForDeductionBenefitPayCode2=new DtoCodeForDeductionBenefitPayCode(codeForDeductionBenefitPayCode);
				dtoCodeForDeductionBenefitPayCode2.setId(codeForDeductionBenefitPayCode.getId());
				dtoCodeForDeductionBenefitPayCode2.setCodeID(codeForDeductionBenefitPayCode.getCodeID());
				dtoCodeForDeductionBenefitPayCode2.setDesc(codeForDeductionBenefitPayCode.getDesc());
				dtoCodeForDeductionBenefitPayCodesList.add(dtoCodeForDeductionBenefitPayCode2);
			}
		}
		return dtoCodeForDeductionBenefitPayCodesList;
	}
	
}
