package com.bti.hcm.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.groovy.control.io.InputStreamReaderSource;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.DtoProcess;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoCalculateChecks;
import com.bti.hcm.model.dto.DtoCalculateChecksDisplay;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoEmployeeCalculateChecckReport;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceCalculateChecks;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.util.CommonUtils;
import com.bti.hcm.util.PreparePrintingCSV;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;

@RestController
@RequestMapping("/calculateChecks")
public class ControllerCalculateChecks extends BaseController{

	private Logger log = Logger.getLogger(ControllerCalculateChecks.class);
	/**
	 * @Description serviceCalculateChecks Autowired here using annotation of spring for use of serviceCalculateChecks method in this controller
	 */
	@Autowired(required=true)
	ServiceCalculateChecks serviceCalculateChecks;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	PreparePrintingCSV preparePrintingCSV;
	
	@Autowired
	ServletContext context;
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoCalculateChecks dtoCalculateChecks) throws Exception {
		log.info("Create Department Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoCalculateChecks = serviceCalculateChecks.saveOrUpdate(dtoCalculateChecks);
			responseMessage=displayMessage(dtoCalculateChecks, "CALCULATE_CHECK_CREATED", "CALCULATE_CHECK_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create Department Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoCalculateChecks dtoCalculateChecks) throws Exception {
		log.info("Create CalculateChecks Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoCalculateChecks = serviceCalculateChecks.saveOrUpdate(dtoCalculateChecks);
			responseMessage=displayMessage(dtoCalculateChecks, "CALCULATE_CHECK_UPDATED", "CALCULATE_CHECK_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create CalculateChecks Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoCalculateChecks dtoCalculateChecksDisplay) throws Exception {
		log.info("Delete CalculateChecks Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoCalculateChecksDisplay.getIds() != null && !dtoCalculateChecksDisplay.getIds().isEmpty()) {
				DtoCalculateChecks dtoCalculateChecksObj = serviceCalculateChecks.delete(dtoCalculateChecksDisplay.getIds());
				if(dtoCalculateChecksObj.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("CALCULATE_CHECK_DELETED", false), dtoCalculateChecksObj);	
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoCalculateChecksObj.getMessageType());
					btiMessageHcm.setMessageShort("CALCULATE_CHECK_NOT_DELETE_ID_MESSAGE");
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoCalculateChecksObj);
					 
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete CalculateChecks Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search CalculateChecksss Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceCalculateChecks.getAll(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.CALCULATE_CHECK_GET_ALL, MessageConstant.CALCULATE_CHECK_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllByDefaultId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByDefaultId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search CalculateCheckses Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceCalculateChecks.getAllByDefautId1(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.CALCULATE_CHECK_GET_ALL, MessageConstant.CALCULATE_CHECK_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getDistribrution", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getDistribrution(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search CalculateChecks Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceCalculateChecks.getDistribrution(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.CALCULATE_CHECK_GET_ALL, MessageConstant.CALCULATE_CHECK_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoCalculateChecksDisplay dtoCalculateChecks) throws Exception {
		log.info("Get CalculateChecks ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoCalculateChecksDisplay dtoCalculateChecksObj = serviceCalculateChecks.getById(dtoCalculateChecks.getId());
			responseMessage=displayMessage(dtoCalculateChecksObj, MessageConstant.CALCULATE_CHECK_GET_ALL, MessageConstant.CALCULATE_CHECK_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get CalculateChecks ById Method:"+dtoCalculateChecks.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/process", method = RequestMethod.POST)
	public ResponseMessage process(HttpServletRequest request, @RequestBody DtoProcess dtoProcess) throws Exception {
		log.info("Create calculate checkess Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoProcess = serviceCalculateChecks.process(dtoProcess);
			responseMessage=displayMessage(dtoProcess, "CALCULATE_CHECK_CREATED_FOR_PROCESS", "CALCULATE_CHECK_NOT_CREATED_FOR_PROCESS", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Createss calculatees check Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllDisctrubution", method = RequestMethod.GET)
	public ResponseMessage getAllDisctrubution(HttpServletRequest request) throws Exception {
		log.info("Creates calculate check Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceCalculateChecks.getAllDisctrubutionNew();
			responseMessage=displayMessage(dtoSearch, "CALCULATE_CHECK_CREATED_FOR_PROCESS", "CALCULATE_CHECK_NOT_CREATED_FOR_PROCESS", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("GetAll calculate check Methodess:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllDisctrubutionByDefaultId", method = RequestMethod.POST)
	public ResponseMessage getAllDisctrubutionByDefaultId(HttpServletRequest request,@RequestBody DtoSearch dtoSearch) throws Exception {
		log.info("Create calculate check Methodes");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			 dtoSearch = serviceCalculateChecks.getAllDisctrubutionByDefaultId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "CALCULATE_CHECK_CREATED_FOR_PROCESS", "CALCULATE_CHECK_NOT_CREATED_FOR_PROCESS", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("GeAlls calculate check Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	

	@RequestMapping(value = "/getAllPaySlipForReport", method = RequestMethod.GET)
	public ResponseMessage getAllPaySlipForReport(HttpServletRequest request) throws Exception {
		log.info("Create calculate check Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceCalculateChecks.getAllPaySlipForReport();
			responseMessage=displayMessage(dtoSearch, "CALCULATE_CHECK_CREATED_FOR_PROCESS_AND_REPOT", "CALCULATE_CHECK_NOT_CREATED_FOR_PROCESS_AND_REPORT", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create calculate check Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllForPayRollStatement", method = RequestMethod.GET)
	public ResponseMessage getAllForPayRollStatement(HttpServletRequest request) throws Exception {
		log.info("Create calculate check Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceCalculateChecks.getAllForPayRollStatement();
			responseMessage=displayMessage(dtoSearch, "CALCULATE_CHECK_CREATED_FOR_PROCESS_AND_REPOT_REPOSTSATATEMENT", "CALCULATE_CHECK_NOT_CREATED_FOR_PROCESS_AND_REPOSTSATATEMENT", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create calculate check Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllByDefaultIdForProcess", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByDefaultIdForProcess(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search CalculateChecks Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			
			dtoSearch = this.serviceCalculateChecks.getAllByDefautId1(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.CALCULATE_CHECK_GET_ALL, MessageConstant.CALCULATE_CHECK_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getCounts", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getCounts(@RequestBody DtoCalculateChecks dtoCalculateChecks, HttpServletRequest request) throws Exception {
		log.info("Get Counts Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoCalculateChecks dtoCalculateChecksObj = this.serviceCalculateChecks.getTotalNumbers(dtoCalculateChecks);
			responseMessage=displayMessage(dtoCalculateChecksObj, MessageConstant.CALCULATE_CHECK_GET_ALL, MessageConstant.CALCULATE_CHECK_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	/*
	@RequestMapping(value = "/processForBuildChecks", method = RequestMethod.POST)
	public ResponseMessage processForBuildChecks(HttpServletRequest request, @RequestBody DtoProcess dtoProcess) throws Exception {
		log.info("Create calculate checkess Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoProcess = serviceCalculateChecks.processForBuildChecks(dtoProcess);
			responseMessage=displayMessage(dtoProcess, "CALCULATE_CHECK_CREATED_FOR_PROCESS", "CALCULATE_CHECK_NOT_CREATED_FOR_PROCESS", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Createss calculatees check Method:"+responseMessage.getMessage());
		return responseMessage;
	}*/

	
	@RequestMapping(value = "/generateAndEmailPayslip", method = RequestMethod.POST)
	public ResponseMessage generateAndEmailPayslip(@RequestBody DtoSearch dtoSearch, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseMessage responseMessage = new ResponseMessage();
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			int loggedInUserId = Integer.parseInt(request.getHeader("userid"));
//			String id = dtoSearch.getId().toString();
//			String id = request.getParameter("id");
			String paySlipsGenerated = null;
			if (serviceCalculateChecks.generateAndEmailPaySlip(dtoSearch, loggedInUserId)) {
				paySlipsGenerated = "Success";
			}
			responseMessage=displayMessage(paySlipsGenerated, MessageConstant.PAYSLIPS_GENERATED_SUCCESSFULLY, MessageConstant.ERROR_IN_GENERATING_PAYSLIPS, serviceResponse);
		} else {
			responseMessage=unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/generateCSVFile", method = RequestMethod.GET)
	public ResponseMessage generateCSVFile(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseMessage responseMessage = new ResponseMessage();
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			int loggedInUserId = Integer.parseInt(request.getHeader("userid"));
			String id = request.getParameter("id");
			String postingDate = request.getParameter("postingDate");
			System.out.println("postingDate: " + postingDate);
			JSONObject jsonPostingDate = null;
			String formattedDate = "";
			if (postingDate != null && !postingDate.isEmpty()) {
				try {
					jsonPostingDate = new JSONObject(postingDate);
					JSONObject jsonDate = jsonPostingDate.getJSONObject("date");
					formattedDate = jsonDate.optString("year") + CommonUtils.padLeftZeros(jsonDate.optString("month"), 2) + CommonUtils.padLeftZeros(jsonDate.optString("day"), 2);
					System.out.println("formattedDate: " + formattedDate);
				} catch (Exception ex) {
					log.error(ex);
					ex.printStackTrace();
				}
			}
			

			String csvData[] = serviceCalculateChecks.generateCSVFile(id, loggedInUserId, formattedDate);
			writeDataToCsvUsingStringArray(response.getWriter(), csvData);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	public void writeDataToCsvUsingStringArray(PrintWriter writer,String[] csvData) {
		String[] CSV_HEADER = { csvData[0] };
		try (
			CSVWriter csvWriter = new CSVWriter(writer,
		                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
		                CSVWriter.NO_QUOTE_CHARACTER,
		                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
		                CSVWriter.DEFAULT_LINE_END);
		){
			csvWriter.writeNext(CSV_HEADER);
			for (int i=1; i < csvData.length; i++) {
				String[] data = {csvData[i]};
				csvWriter.writeNext(data);
			}
			
			System.out.println("Write CSV using CSVWriter successfully!");
		}catch (Exception e) {
			System.out.println("Writing CSV error!");
			e.printStackTrace();
		}
	}
	
	private ByteArrayOutputStream convertexcelToByteArrayOutputStream(String fileName) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (InputStream inputStream = new FileInputStream(fileName)){
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();

			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}

		} catch (Exception e) {
//			LOGGER.info(Arrays.toString(e.getStackTrace()));
		}   
		return baos;
	}


	
	///generateCSVFileBeforePosting
	@RequestMapping(value = "/generateCSVFileBeforePosting", method = RequestMethod.GET)
		public void generateCSVFileBeforePosting(HttpServletRequest request, HttpServletResponse response) throws Exception {
			ResponseMessage responseMessage = new ResponseMessage();
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			String reportID = httpServletRequest.getHeader("defaultId");
			DtoSearch dtoSearch = new DtoSearch();
			dtoSearch.setId(CommonUtils.parseInteger(reportID));
			dtoSearch.setAll(true);
			List<DtoDepartment> departments = (ArrayList<DtoDepartment>) serviceCalculateChecks.prepareDateForExcel(dtoSearch).getRecords();
			if (flag) {
				if(departments != null && !departments.isEmpty()) {
					serviceCalculateChecks.generateExcelFileBeforeProcessing(departments, request, response);
				}
				
			}

		}
	
	public static ByteArrayInputStream customersToExcel(List<DtoDepartment> dtoDepartments ) throws IOException {
		String[] COLUMNs = {"Id", "Name", "Address", "Age"};
		try{
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();
	 
			Sheet sheet = workbook.createSheet("Customers");
	 
			Font headerFont = workbook.createFont();
			//headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());
	 
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
	 
			// Row for Header
			Row headerRow = sheet.createRow(0);
	 
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
	 
			// CellStyle for Age
			//CellStyle ageCellStyle = workbook.createCellStyle();
			//ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
	 
			int rowIdx = 1;
			for (DtoDepartment dtoDepartment : dtoDepartments) {
				Row row = sheet.createRow(rowIdx++);
				row.createCell(0).setCellValue(dtoDepartment.getId());
				row.createCell(1).setCellValue(dtoDepartment.getDepartmentName());	 
			}
	 
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@RequestMapping(value = "/getAllByDefaultIdWithoutPagination", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByDefaultIdWithoutPagination(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search CalculateChecks Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			
			dtoSearch = this.serviceCalculateChecks.getAllByDefautIdWithoutPagination(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.CALCULATE_CHECK_GET_ALL, MessageConstant.CALCULATE_CHECK_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
}



