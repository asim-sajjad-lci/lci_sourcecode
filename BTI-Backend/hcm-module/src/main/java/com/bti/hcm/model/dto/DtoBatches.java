package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.Batches;

public class DtoBatches extends DtoBase{

	private Integer id;
	private String batchId;
	private String description;
	private short transactionType;
	private String arabicDescription;
	private BigDecimal quantityTotal;
	private Double totalTransactions;
	private Integer buildId;
	private Date buildDate;
	private Date payPeriodFrom;
	private Date payPeriodTo;
	private boolean approved;
	private Date postingDate;
	private Date approvedDate;
	private short status;
	private Integer userID;
	private boolean inActive;
	private boolean statusCheck;
	
	
    private List<DtoBatches> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public short getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(short transactionType) {
		this.transactionType = transactionType;
	}
	
	public BigDecimal getQuantityTotal() {
		return quantityTotal;
	}
	public void setQuantityTotal(BigDecimal quantityTotal) {
		this.quantityTotal = quantityTotal;
	}
	
	public Double getTotalTransactions() {
		return totalTransactions;
	}
	public void setTotalTransactions(Double totalTransactions) {
		this.totalTransactions = totalTransactions;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	public Date getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}
	
	public short getStatus() {
		return status;
	}
	public void setStatus(short status) {
		this.status = status;
	}
	
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public boolean isInActive() {
		return inActive;
	}
	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}
	
	public List<DtoBatches> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoBatches> delete) {
		this.delete = delete;
	}
    
    public String getArabicDescription() {
		return arabicDescription;
	}
	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}
	
	
	public Date getBuildDate() {
		return buildDate;
	}
	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}
	public Date getPayPeriodFrom() {
		return payPeriodFrom;
	}
	public void setPayPeriodFrom(Date payPeriodFrom) {
		this.payPeriodFrom = payPeriodFrom;
	}
	public Date getPayPeriodTo() {
		return payPeriodTo;
	}
	public void setPayPeriodTo(Date payPeriodTo) {
		this.payPeriodTo = payPeriodTo;
	}
	public Integer getBuildId() {
		return buildId;
	}
	public void setBuildId(Integer buildId) {
		this.buildId = buildId;
	}
	public Date getPostingDate() {
		return postingDate;
	}
	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}
	public DtoBatches() {
	}
    
    public DtoBatches(Batches batches) {
	}
	public boolean isStatusCheck() {
		return statusCheck;
	}
	public void setStatusCheck(boolean statusCheck) {
		this.statusCheck = statusCheck;
	}
	
	public class SortByTotalTransactionAsc implements Comparator<DtoBatches>{
		@Override
		public int compare(DtoBatches o1, DtoBatches o2) {
			return o1.getTotalTransactions().compareTo(o2.getTotalTransactions());
		}
	    
	}
	
	public class SortByTotalTransactionDesc implements Comparator<DtoBatches>{
		@Override
		public int compare(DtoBatches o1, DtoBatches o2) {
			return o2.getTotalTransactions().compareTo(o1.getTotalTransactions());
		}
	    
	}
	
    
}
