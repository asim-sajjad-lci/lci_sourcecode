package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.EmployeePostDatedPayRates;

public class DtoEmployeePostDatedPayRates extends DtoBase {

	private Integer id;
	private DtoEmployeeMaster employeeMaster;

	private DtoEmployeeMasterHcm employeeMasterHcm;
	private DtoPayCode payCode;
	private BigDecimal currentPayRate;
	private BigDecimal newPayRate;
	private Date effectiveDate;
	private String reasonforChange;
	private List<DtoEmployeePostDatedPayRates> delete;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public DtoPayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(DtoPayCode payCode) {
		this.payCode = payCode;
	}

	public BigDecimal getCurrentPayRate() {
		return currentPayRate;
	}

	public void setCurrentPayRate(BigDecimal currentPayRate) {
		this.currentPayRate = currentPayRate;
	}

	public BigDecimal getNewPayRate() {
		return newPayRate;
	}

	public void setNewPayRate(BigDecimal newPayRate) {
		this.newPayRate = newPayRate;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getReasonforChange() {
		return reasonforChange;
	}

	public void setReasonforChange(String reasonforChange) {
		this.reasonforChange = reasonforChange;
	}


	public List<DtoEmployeePostDatedPayRates> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeePostDatedPayRates> delete) {
		this.delete = delete;
	}
	public DtoEmployeePostDatedPayRates() {

	}

	public DtoEmployeePostDatedPayRates(EmployeePostDatedPayRates employeePostDatedPayRates) {

	}

	public DtoEmployeeMasterHcm getEmployeeMasterHcm() {
		return employeeMasterHcm;
	}

	public void setEmployeeMasterHcm(DtoEmployeeMasterHcm employeeMasterHcm) {
		this.employeeMasterHcm = employeeMasterHcm;
	}

}
