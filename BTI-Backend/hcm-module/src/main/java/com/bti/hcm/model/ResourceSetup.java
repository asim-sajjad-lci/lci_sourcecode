package com.bti.hcm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="resourcesetup")
public class ResourceSetup {

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="res_name_arabic")
	private String resourceArabicName;
	
	@Column(name="res_name")
	private String resourceName;
	
	@Column(name="resmod")
	private int resourceMod;
	
	@Column(name="tabref")
	private String tableReference;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getResourceArabicName() {
		return resourceArabicName;
	}

	public void setResourceArabicName(String resourceArabicName) {
		this.resourceArabicName = resourceArabicName;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public int getResourceMod() {
		return resourceMod;
	}

	public void setResourceMod(int resourceMod) {
		this.resourceMod = resourceMod;
	}

	public String getTableReference() {
		return tableReference;
	}

	public void setTableReference(String tableReference) {
		this.tableReference = tableReference;
	}
	
	
}
