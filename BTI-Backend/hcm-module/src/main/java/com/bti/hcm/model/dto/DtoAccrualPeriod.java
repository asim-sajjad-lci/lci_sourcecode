package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.AccrualPeriod;

public class DtoAccrualPeriod extends DtoBase{

	private Integer id;
	private Integer year;
	private Integer yearActive;
	private Integer rowDateIndexing;
	private Integer rowIdIndexing;
	private short periodType;
	private List<DtoAccrualPeriodSetting> subitems;
	private List<DtoAccrualPeriod> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getYearActive() {
		return yearActive;
	}
	public void setYearActive(Integer yearActive) {
		this.yearActive = yearActive;
	}
	public Integer getRowDateIndexing() {
		return rowDateIndexing;
	}
	public void setRowDateIndexing(Integer rowDateIndexing) {
		this.rowDateIndexing = rowDateIndexing;
	}
	public Integer getRowIdIndexing() {
		return rowIdIndexing;
	}
	public void setRowIdIndexing(Integer rowIdIndexing) {
		this.rowIdIndexing = rowIdIndexing;
	}
	
	public List<DtoAccrualPeriod> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoAccrualPeriod> delete) {
		this.delete = delete;
	}
	
	public List<DtoAccrualPeriodSetting> getSubitems() {
		return subitems;
	}
	public void setSubitems(List<DtoAccrualPeriodSetting> subitems) {
		this.subitems = subitems;
	}
	public short getPeriodType() {
		return periodType;
	}
	public void setPeriodType(short periodType) {
		this.periodType = periodType;
	}
	public DtoAccrualPeriod() {
	}
	
	public DtoAccrualPeriod(AccrualPeriod accrualPeriod) {
	}
}
