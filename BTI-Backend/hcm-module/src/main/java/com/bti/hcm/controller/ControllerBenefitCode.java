/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBenefitCode;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceBenefitCode;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerBenefitCode Name of Project: Hcm Version: 0.0.1
 */
@RestController
@RequestMapping("/benefitCode")
public class ControllerBenefitCode extends BaseController {

	/**
	 * @Description LOGGER use for put a logger in BenefitCode Controller
	 */
	private Logger log = Logger.getLogger(ControllerBenefitCode.class);

	/**
	 * @Description serviceBenefitCode Autowired here using annotation of spring for
	 *              use of serviceBenefitCode method in this controller
	 */
	@Autowired(required = true)
	ServiceBenefitCode serviceBenefitCode;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @description Create BeneditCode
	 * @param request
	 * @param dtoBenefitCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createBenefitCode(HttpServletRequest request, @RequestBody DtoBenefitCode dtoBenefitCode)
			throws Exception {
		log.info("Create BenefitCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBenefitCode = serviceBenefitCode.saveOrUpdateBenefitCode(dtoBenefitCode);
			responseMessage = displayMessage(dtoBenefitCode, "BENEFITED_CODE_CREATED", "BENEFITED_CODE_NOT_CREATED",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create BenefitCode Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description update BenefitCode
	 * @param request
	 * @param dtoBenefitCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateBenefitCodet(HttpServletRequest request, @RequestBody DtoBenefitCode dtoBenefitCode)
			throws Exception {
		log.info("Update BenefitCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBenefitCode = serviceBenefitCode.saveOrUpdateBenefitCode(dtoBenefitCode);
			responseMessage = displayMessage(dtoBenefitCode, "BENEFITED_CODE_UPDATED_SUCCESS",
					"BENEFITED_CODE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update BenefitCode Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description delete BenefitCode
	 * @param request
	 * @param dtoBenefitCode
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteBenefitCode(HttpServletRequest request, @RequestBody DtoBenefitCode dtoBenefitCode)
			throws Exception {
		log.info("Delete BenefitCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoBenefitCode.getIds() != null && !dtoBenefitCode.getIds().isEmpty()) {
				DtoBenefitCode dtoBenefitCode2 = serviceBenefitCode.deleteBenefitCode(dtoBenefitCode.getIds());

				if (dtoBenefitCode2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("BENEFITED_CODE_DELETED", false),
							dtoBenefitCode2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, serviceResponse
							.getMessageByShortAndIsDeleted("BENEFITED_CODE_NOT_DELETE_ID_MESSAGE", false),
							dtoBenefitCode2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete BenefitCode Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/*
	 * @RequestMapping(value = "/delete", method = RequestMethod.PUT) public
	 * ResponseMessage deleteBenefitCode11(HttpServletRequest request, @RequestBody
	 * DtoBenefitCode dtoBenefitCode)throws Exception {
	 * log.info("Delete BenefitCode Method:"); ResponseMessage responseMessage =
	 * null; boolean flag = serviceHcmHome.checkValidCompanyAccess(); if (flag) { if
	 * (dtoBenefitCode.getIds() != null && !dtoBenefitCode.getIds().isEmpty()) {
	 * DtoBenefitCode dtoBenefitCode2 =
	 * serviceBenefitCode.deleteBenefitCode(dtoBenefitCode.getIds()); if
	 * (dtoBenefitCode2.getMessageType() == null) { responseMessage = new
	 * ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
	 * serviceResponse.getMessageByShortAndIsDeleted("BENEFITED_CODE_DELETED",
	 * false), dtoBenefitCode2); } else { responseMessage = new
	 * ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
	 * serviceResponse.getMessageByShortAndIsDeleted(
	 * "BENEFITED_CODE_NOT_DELETE_ID_MESSAGE", false), dtoBenefitCode2); } } else {
	 * responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(),
	 * HttpStatus.BAD_REQUEST,
	 * serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false)); }
	 * 
	 * } else { responseMessage = new
	 * ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
	 * serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.
	 * SESSION_EXPIRED, false)); } log.debug("Delete Department Method:" +
	 * responseMessage.getMessage()); return responseMessage; }
	 */

	/**
	 * @description search BenefitCode
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchBenefitCode(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		log.info("Search BenefitCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBenefitCode.searchBenefitCode(dtoSearch);
			responseMessage = displayMessage(dtoSearch, MessageConstant.BENEFIT_CODE_GET_ALL,
					MessageConstant.BENEFIT_CODE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	/**
	 * @description getAll BenefitCode
	 * @param request
	 * @param dtoBenefitCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAllBenefitCode(HttpServletRequest request, @RequestBody DtoBenefitCode dtoBenefitCode)
			throws Exception {
		log.info("Get All BenefitCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceBenefitCode.getAllBenefitCode(dtoBenefitCode);
			responseMessage = displayMessage(dtoSearch, MessageConstant.BENEFIT_CODE_GET_ALL,
					MessageConstant.BENEFIT_CODE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get All BenefitCode Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description get BenefitCode by Id
	 * @param request
	 * @param dtoBenefitCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getBenefitCodeById", method = RequestMethod.POST)
	public ResponseMessage getBenefitCodeById(HttpServletRequest request, @RequestBody DtoBenefitCode dtoBenefitCode)
			throws Exception {
		log.info("Get BenefitCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoBenefitCode dtoBenefitCodeObj = serviceBenefitCode.getBenefitCodeByBenefitCodeId(dtoBenefitCode.getId());
			responseMessage = displayMessage(dtoBenefitCodeObj, MessageConstant.BENEFIT_CODE_GET_ALL,
					MessageConstant.BENEFIT_CODE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get BenefitCode ById Method:" + dtoBenefitCode.getId());
		return responseMessage;
	}

	/**
	 * @description get BenefitCode Id check
	 * @param request
	 * @param dtoBenefitCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/benefitCodeIdcheck", method = RequestMethod.POST)
	public ResponseMessage benefitCodeIdCheck(HttpServletRequest request, @RequestBody DtoBenefitCode dtoBenefitCode)
			throws Exception {
		log.info("benefitCodeIdCheck  Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoBenefitCode dtoBenefitCodeObject = serviceBenefitCode
					.repeatByBenefitCodeId(dtoBenefitCode.getBenefitId());
			responseMessage = displayMessage(dtoBenefitCodeObject, "BENEFIT_CODE_RESULT",
					"BENEFIT_CODE_REPEAT_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/searchBenefitId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchBenefitId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		log.info("Search BenefitId Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBenefitCode.searchBenefitId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "BENEFIT_CODE_GET_ALL", "BENEFIT_CODE_LIST_NOT_GETTING",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllBenefitDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllBenefitDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoBenefitCode> dtoBenefitCodeList = serviceBenefitCode.getAllBenefitCodeByInActiveList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("BENEFIT_CODE_GET_ALL", false),
						dtoBenefitCodeList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
			log.error(e);
		}

		return responseMessage;
	}

	@RequestMapping(value = "/searchAllBenefitId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAllBenefitId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		log.info("searchAllBenefitId Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBenefitCode.searchAllBenefitId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, MessageConstant.BENEFIT_CODE_GET_ALL,
					MessageConstant.BENEFIT_CODE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/findCodeByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage findCodeByEmployeeId(@RequestBody DtoPayCode dtoPayCode, HttpServletRequest request)
			throws Exception {
		log.info("Search EmployeePayCodeMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayCode = this.serviceBenefitCode.deducationCodeByPaycode(dtoPayCode);
			responseMessage = displayMessage(dtoPayCode, "EMPLOYEE_PAY_CODET_MAINTENENCE_GET_ALL",
					"EMPLOYEE_PAY_CODET_MAINTENENCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if (dtoPayCode != null) {
			log.debug("Search EmployeePayCodeMaintenance Method:" + dtoPayCode.getTotalCount());
		}

		return responseMessage;
	}

}
