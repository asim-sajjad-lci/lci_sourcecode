package com.bti.hcm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "hr10712")
public class EmployeeVactionTicket  extends HcmBaseEntity2 {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDX")
	private Integer idx;

	@Column(name = "emp_vac_req_id")
	private Integer employeeVacationRequestId;
	
	@Column(name = "id_dep")
	Boolean isDependant;

	@Column(name = "dprt_from")
	private String departFrom; 
	
	@Column(name = "ariv_to")
	private String arrivedTo;

	@Column(name = "dprt_dt")
	private Date departureDate;
	
	@Column(name = "tkt_cls")
	private String ticketClass;
	
	@Column(name = "nts")
	private String notes;

	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}

	public Integer getEmployeeVacationRequestId() {
		return employeeVacationRequestId;
	}

	public void setEmployeeVacationRequestId(Integer employeeVacationRequestId) {
		this.employeeVacationRequestId = employeeVacationRequestId;
	}

	public Boolean getIsDependant() {
		return isDependant;
	}

	public void setIsDependant(Boolean isDependant) {
		this.isDependant = isDependant;
	}

	public String getDepartFrom() {
		return departFrom;
	}

	public void setDepartFrom(String departFrom) {
		this.departFrom = departFrom;
	}

	public String getArrivedTo() {
		return arrivedTo;
	}

	public void setArrivedTo(String arrivedTo) {
		this.arrivedTo = arrivedTo;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getTicketClass() {
		return ticketClass;
	}

	public void setTicketClass(String ticketClass) {
		this.ticketClass = ticketClass;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	} 

}
