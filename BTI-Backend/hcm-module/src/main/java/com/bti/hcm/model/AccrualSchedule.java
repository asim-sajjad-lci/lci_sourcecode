package com.bti.hcm.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40223",indexes = {
        @Index(columnList = "ACCSCHINDX")
})
public class AccrualSchedule extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACCSCHINDX")
	private Integer id;

	@Column(name = "ACCSCHID", columnDefinition = "char(15)")
	private String scheduleId;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accrualSchedule")
//	@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "TMECDINCT = false and is_deleted = false")
	private List<TimeCode> listTimeCode;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accrualSchedule")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<AccrualScheduleDetail> listAccrualScheduleDetail;

	@Column(name = "ACCSCHDSCR", columnDefinition = "char(31)")
	private String desc;

	@Column(name = "ACCSCHDSCRA", columnDefinition = "char(61)")
	private String arbicDesc;

	@Column(name = "STRTDT")
	private Date startDate;

	@Column(name = "ENDDT")
	private Date endDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<AccrualScheduleDetail> getListAccrualScheduleDetail() {
		return listAccrualScheduleDetail;
	}

	public void setListAccrualScheduleDetail(List<AccrualScheduleDetail> listAccrualScheduleDetail) {
		this.listAccrualScheduleDetail = listAccrualScheduleDetail;
	}

	public List<TimeCode> getListTimeCode() {
		return listTimeCode;
	}

	public void setListTimeCode(List<TimeCode> listTimeCode) {
		this.listTimeCode = listTimeCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((arbicDesc == null) ? 0 : arbicDesc.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listAccrualScheduleDetail == null) ? 0 : listAccrualScheduleDetail.hashCode());
		result = prime * result + ((listTimeCode == null) ? 0 : listTimeCode.hashCode());
		result = prime * result + ((scheduleId == null) ? 0 : scheduleId.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
