/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the EmployeeDirectDeposit database
 * table. Name of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00205",indexes = {
        @Index(columnList = "EMPDPINDX")
})
@NamedQuery(name = "EmployeeDirectDeposit.findAll", query = "SELECT d FROM EmployeeDirectDeposit d")
public class EmployeeDirectDeposit extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPDPINDX")
	private Integer id;

	@Column(name = "BNKNME", columnDefinition = "char(21)")
	private String bankName;

	@Column(name = "ACCNMBR", columnDefinition = "char(40)")
	private String accountNumber;

	@Column(name = "ACCAMNT", precision = 13, scale = 3)
	private BigDecimal amount;

	@Column(name = "ACCPRNT", precision = 13, scale = 5)
	private BigDecimal percent;

	@Column(name = "ACCSTATS")
	private short status;

	@Column(name = "ACCSEQN")
	private int accountSequence;

	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}


	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public int getAccountSequence() {
		return accountSequence;
	}

	public void setAccountSequence(int accountSequence) {
		this.accountSequence = accountSequence;
	}

}
