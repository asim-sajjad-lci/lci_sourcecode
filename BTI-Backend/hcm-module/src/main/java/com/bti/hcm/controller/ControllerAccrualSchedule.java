package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoAccrualSchedule;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceAccrualSchedule;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/accrualSchedule")
public class ControllerAccrualSchedule extends BaseController{
	/**
	 * @Description LOGGER use for put a logger in AccrualSchedule Controller
	 */
	private  Logger log = Logger.getLogger(ControllerAccrualSchedule.class);


/**
 * @Description serviceAccrualSchedule Autowired here using annotation of spring for use of serviceAccrualSchedule method in this controller
 */
	@Autowired
	ServiceAccrualSchedule serviceAccrualSchedule;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @param request{
		"scheduleId" : "1",
		"desc" : "test",
		"arbicDesc" : "test"
		}
	 * @param dtoAccrualSchedule
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createAccrualSchedule(HttpServletRequest request, @RequestBody DtoAccrualSchedule dtoAccrualSchedule) throws Exception {
		log.info("Create AccrualSchedule Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrualSchedule  = serviceAccrualSchedule.saveOrUpdateAccrualSchedule(dtoAccrualSchedule);
			responseMessage=displayMessage(dtoAccrualSchedule, "ACCRUALSCHEDULE_CREATED", "ACCRUALSCHEDULE NOT_CREATED", serviceResponse);
		} else {
			responseMessage=unauthorizedMsg(serviceResponse);
		}
		log.debug("Create Location Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoAccrualSchedule
	 * @return
	 * @request{
	 * "pageNumber":"0",
 		"pageSize":"10"
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"pageNumber":0,
	 * "pageSize":10,
	 * "totalCount":1,
	 * "records":[
	 * {"locationId":1,
	 * "description":"Test dtoAccrualSchedule",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}
	 * ]
	 * },
	 * 
	 * 		"btiMessage":{"message":"All AccrualSchedule fetched successfully",
	 * 		"messageShort":"ACCRUALSCHEDULE_GET_ALL"}
	 * }
	 * @throws Exception
	 */

	@RequestMapping(value = "/getAllIds", method = RequestMethod.PUT)
	public ResponseMessage getAllAccrualSchedule(HttpServletRequest request) throws Exception {
		log.info("Get All DtoAccrualSchedule Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			com.bti.hcm.model.dto.DtoSearch dtoSearch = serviceAccrualSchedule.getAllAccrualSchedule();
			responseMessage=displayMessage(dtoSearch, "ACCRUALSCHEDULE_GET_ALL", "ACCRUALSCHEDULE LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		log.debug("Get All AccrualSchedule Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	/**
	 * 
	 * @param {
        "scheduleId" : "1",
		"desc" : "test",
		"arbicDesc" : "test"
         }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"id":2,
	 * "locationId":2,
	 * "description":"Testing Location",
	 * "arabicDescription":"موقع الاختبار",
	 * "contactName":"Gaurav",
	 * "locationAddress":"Surat",
	 * "city":"Surat",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * "btiMessage":{
	 * 					"message":"AccrualSchedule updated successfully",
	 * 					"messageShort":"AccrualScheduleUPDATE_SUCCESS"}
	 * }
	 * @throws Exception
	 */
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateAccrualSchedule(HttpServletRequest request, @RequestBody DtoAccrualSchedule dtoAccrualSchedule) throws Exception {
	log.info("Update AccrualSchedule Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			dtoAccrualSchedule = serviceAccrualSchedule.saveOrUpdateAccrualSchedule(dtoAccrualSchedule);
			responseMessage=displayMessage(dtoAccrualSchedule, "ACCRUALSCHEDULE_UPDATE_SUCCESS", "ACCRUALSCHEDULE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update Location Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoAccrualSchedule
	 * @return
	 * @request{
	 * "ids" : [2]
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"decId":0,
	 * "deleteMessage":"AccrualSchedule deleted successfully",
	 * "associateMessage":"N/A",
	 * "deleteAccrualSchedule":[
	 * {"id":2,
	 * "locationId":2,
	 * "description":"Testing AccrualSchedule",
	 * "contactName":"Gaurav",
	 * "locationAddress":"Surat",
	 * "city":"Surat",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}]},
	 * "btiMessage":{
	 * 					"message":"AccrualSchedule deleted successfully",
	 * 					"messageShort":"ACCRUALSCHEDULE_DELETED"}
	 * }
	 * @throws Exception
	 */
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteAccrualSchedule(HttpServletRequest request, @RequestBody DtoAccrualSchedule dtoAccrualSchedule) throws Exception {
		log.info("Delete AccrualSchedule Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoAccrualSchedule.getIds() != null && !dtoAccrualSchedule.getIds().isEmpty()) {
				DtoAccrualSchedule dtoAccrualSchedule2 =serviceAccrualSchedule.deleteAccrualSchedule(dtoAccrualSchedule.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ACCRUALSCHEDULE_DELETED", false), dtoAccrualSchedule2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete AccrualSchedule Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param AccrualSchedule
	 * @return
	 * @request{
	 * 		"id" : 1,
			"pageNumber" : 0,
			"pageSize" : 10

	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":1,
	 * "description":"Test AccrualSchedule",
	 * "arabicDescription":"",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * "btiMessage":{
	 * 							"message":"AccrualSchedule detail fetched successfully",
	 * 							"messageShort":"ACCRUALSCHEDULE_GET_DETAIL"}
	 * }
	 * @throws Exception
	 */
	

	@RequestMapping(value = "/getAccrualScheduleById", method = RequestMethod.POST)
	public ResponseMessage getAccrualScheduleById(HttpServletRequest request, @RequestBody DtoAccrualSchedule dtoAccrualSchedule) throws Exception {
		log.info("Get AccrualSchedule ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAccrualSchedule dtoAccrualScheduleObj = serviceAccrualSchedule.getscheduleById(dtoAccrualSchedule.getId());
			responseMessage=displayMessage(dtoAccrualScheduleObj, "LOCATION_GET_DETAIL", "ACCRUALSCHEDULE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		log.debug("Get AccrualSchedule ById Method:"+dtoAccrualSchedule.getId());
		return responseMessage;
	}
		
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @request{
	 * 	"searchKeyword" : "AHD",
		"pageNumber":"0",
		"pageSize":"10"
	 * }
	 * @response{
	 * "code":200,
	 * "status":"OK",
	 * "result":{
	 * "searchKeyword":"AHD",
	 * "pageNumber":0,
	 * "pageSize":10,
	 * "totalCount":1,
	 * "records":[
	 * {"locationId":1,
	 * "description":"Test Location",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}
	 * ]
	 * },
	 * "btiMessage":{
	 * 					"message":"All locations fetched successfully",
	 * 					"messageShort":"LOCATION_GET_ALL"}
	 * }
	 * @throws Exception
	 */
	
	
	


	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search RetirementFundSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccrualSchedule.searchAccrualShedule(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ACCRUALSHEDULE_GET_ALL", "ACCRUALSHEDULE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	
	
	
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "locationId" : 1
	 * }
	 * @response{
	 * "code":302,
	 * "status":"FOUND",
	 * "result":{
	 * "locationId":1,
	 * "isRepeat":true},
	 * "btiMessage":
	 * {
	 *				 "message":"Location detail fetched successfully",
	 *				 "messageShort":"LOCATION_RESULT"}
	 * }
	 * @throws Exception
	 */
	

	@RequestMapping(value = "/scheduleIdcheck", method = RequestMethod.POST)
	public ResponseMessage scheduleIdcheck(HttpServletRequest request, @RequestBody DtoAccrualSchedule dtoAccrualSchedule) throws Exception {
		log.info("scheduleIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			DtoAccrualSchedule dtoAccrualSchedulelObj = serviceAccrualSchedule.repeatByScheduleId(dtoAccrualSchedule.getScheduleId());
			responseMessage=displayMessage(dtoAccrualSchedulelObj, "ACCRUALSCHEDULEDETAIL_EXIST", "ACCRUALSCHEDULEDETAIL_REPEAT_ACCRUALSCHEDULEDETAIL_NOT_FOUND", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search RetirementFundSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccrualSchedule.searchAccrualSheduleIds(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ACCRUALSHEDULE_GET_ALL", "ACCRUALSHEDULE_LIST_NOT_GETTING", serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
}
