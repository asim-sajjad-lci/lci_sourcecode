package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePostDatedPayRates;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.dto.DtoActivateEmployeePostDatePayRate;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeMasterHcm;
import com.bti.hcm.model.dto.DtoEmployeePostDatedPayRates;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryActivateEmployeePostDatePayRate;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePostDatedPayRates;
import com.bti.hcm.repository.RepositoryPayCode;

@Service("serviceEmployeePostDatedPayRates")
public class ServiceEmployeePostDatedPayRates {

static Logger log = Logger.getLogger(ServiceEmployeePostDatedPayRates.class.getName());

	

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired(required=false)
	RepositoryEmployeePostDatedPayRates repositoryEmployeePostDatedPayRates;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired(required=false)
	RepositoryPayCode repositoryPayCode;
	
	
	@Autowired(required=false)
	RepositoryActivateEmployeePostDatePayRate repositoryActivateEmployeePostDatePayRate;
	
	
	
	public DtoEmployeePostDatedPayRates saveOrUpdate(DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates) {
		
		try {

			log.info("saveOrUpdate EmployeePostDatedPayRates Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			EmployeePostDatedPayRates employeePostDatedPayRates = null;
			if (dtoEmployeePostDatedPayRates.getId() != null && dtoEmployeePostDatedPayRates.getId() > 0) {
				
				employeePostDatedPayRates = repositoryEmployeePostDatedPayRates.findByIdAndIsDeleted(dtoEmployeePostDatedPayRates.getId(), false);
				employeePostDatedPayRates.setUpdatedBy(loggedInUserId);
				employeePostDatedPayRates.setUpdatedDate(new Date());
			} else {
				employeePostDatedPayRates = new EmployeePostDatedPayRates();
				employeePostDatedPayRates.setCreatedDate(new Date());
				
				Integer rowId = repositoryEmployeePostDatedPayRates.getCountOfTotalEmployeePostDatedPayRates();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				employeePostDatedPayRates.setRowId(increment);
			}
			
			EmployeeMaster employeeMaster =null;
			PayCode payCode = null;
			
			if(dtoEmployeePostDatedPayRates.getEmployeeMaster()!=null && dtoEmployeePostDatedPayRates.getEmployeeMaster().getEmployeeIndexId()>0){
				employeeMaster = repositoryEmployeeMaster.findOne(dtoEmployeePostDatedPayRates.getEmployeeMaster().getEmployeeIndexId());
			
			}
			
			if(dtoEmployeePostDatedPayRates.getPayCode()!=null && dtoEmployeePostDatedPayRates.getPayCode().getId()!=null && dtoEmployeePostDatedPayRates.getPayCode().getId()>0){
				payCode = repositoryPayCode.findOne(dtoEmployeePostDatedPayRates.getPayCode().getId());
			
			}
			
			
			employeePostDatedPayRates.setEmployeeMaster(employeeMaster);
			
			employeePostDatedPayRates.setPayCode(payCode);
			
			employeePostDatedPayRates.setCurrentPayRate(dtoEmployeePostDatedPayRates.getCurrentPayRate());
			employeePostDatedPayRates.setNewPayRate(dtoEmployeePostDatedPayRates.getNewPayRate());
			employeePostDatedPayRates.setEffectiveDate(dtoEmployeePostDatedPayRates.getEffectiveDate());
			employeePostDatedPayRates.setReasonforChange(dtoEmployeePostDatedPayRates.getReasonforChange());
			employeePostDatedPayRates = repositoryEmployeePostDatedPayRates.saveAndFlush(employeePostDatedPayRates);
			
			log.debug("EmployeePostDatedPayRates is:"+employeePostDatedPayRates.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeePostDatedPayRates;
	
	}
	
	
	public DtoSearch getDtoSearch(DtoSearch dtoSearch) {
		String condition="";
		
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			if(dtoSearch.getSortOn().equals("reasonforChange")
				|| dtoSearch.getSortOn().equals("employeeMaster.employeeId") || 
				dtoSearch.getSortOn().equals("payCode.payCodeId") || dtoSearch.getSortOn().equals("effectiveDate")
				) {
				condition = dtoSearch.getSortOn();
			
		}else {
			condition = "id";
		}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
			dtoSearch.setSortOn(condition);
		return dtoSearch;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAll(DtoSearch dtoSearch) {
		try {

			log.info("search EmployeePostDatedPayRates Method");
			
			if(dtoSearch != null && dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				String searchWord=dtoSearch.getSearchKeyword();
				dtoSearch = getDtoSearch(dtoSearch);
				dtoSearch.setTotalCount(this.repositoryEmployeePostDatedPayRates.predictiveEmployeePostDatedPayRatesSearchTotalCount("%"+searchWord+"%"));
				List<EmployeePostDatedPayRates> employeePostDatedPayRateslist =null;
				
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeePostDatedPayRateslist = this.repositoryEmployeePostDatedPayRates.predictiveEmployeePostDatedPayRatesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeePostDatedPayRateslist = this.repositoryEmployeePostDatedPayRates.predictiveEmployeePostDatedPayRatesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getSortOn()));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeePostDatedPayRateslist = this.repositoryEmployeePostDatedPayRates.predictiveEmployeePostDatedPayRatesSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getSortOn()));
					}
				List<DtoEmployeePostDatedPayRates> dtoEmployeePostDatedPayRatesList = getdtoEmployeePostDatedPayRatesList(employeePostDatedPayRateslist);
				dtoSearch.setRecords(dtoEmployeePostDatedPayRatesList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	
	public DtoEmployeePostDatedPayRates delete(List<Integer> ids) {
		log.info("delete EmployeePostDatedPayRates Method");
		DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates = new DtoEmployeePostDatedPayRates();
		dtoEmployeePostDatedPayRates
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_DELETED", false));
		dtoEmployeePostDatedPayRates.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_ASSOCIATED", false));
		List<DtoEmployeePostDatedPayRates> delete = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer id : ids) {
				EmployeePostDatedPayRates employeePositionHistory = repositoryEmployeePostDatedPayRates.findOne(id);
				if(employeePositionHistory!=null) {
					DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates1 = new DtoEmployeePostDatedPayRates();
					dtoEmployeePostDatedPayRates1.setId(employeePositionHistory.getId());
					repositoryEmployeePostDatedPayRates.deleteSingleEmployeePostDatedPayRates(true, loggedInUserId, id);
					delete.add(dtoEmployeePostDatedPayRates1);
	
				}else {
					inValidDelete = true;
				}
				
			}
			if(inValidDelete){
				dtoEmployeePostDatedPayRates.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoEmployeePostDatedPayRates.setMessageType("");
				
			}
			
			dtoEmployeePostDatedPayRates.setDelete(delete);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeePostDatedPayRates :"+dtoEmployeePostDatedPayRates.getId());
		return dtoEmployeePostDatedPayRates;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch findById(DtoSearch dtoSearch) {
		try {

			log.info("search EmployeePositionHistory Method");
			
			EmployeePostDatedPayRates employeePostDatedPayRates = repositoryEmployeePostDatedPayRates.findByIdAndIsDeleted(dtoSearch.getId(), false);
				
				
					DtoEmployeePostDatedPayRates	dtoEmployeePostDatedPayRates = new DtoEmployeePostDatedPayRates();
						
					if(employeePostDatedPayRates.getPayCode()!=null) {
						DtoPayCode dtoPayCode = new DtoPayCode();
						dtoPayCode.setId(employeePostDatedPayRates.getPayCode().getId());
						dtoPayCode.setPayCodeId(employeePostDatedPayRates.getPayCode().getPayCodeId());
						
						dtoEmployeePostDatedPayRates.setPayCode(dtoPayCode);
					}
					
					
					if(employeePostDatedPayRates.getEmployeeMaster()!=null) {
						DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
						dtoEmployeeMaster.setEmployeeIndexId(employeePostDatedPayRates.getEmployeeMaster().getEmployeeIndexId());
						dtoEmployeeMaster.setEmployeeId(employeePostDatedPayRates.getEmployeeMaster().getEmployeeId());
						dtoEmployeeMaster.setEmployeeFirstName(employeePostDatedPayRates.getEmployeeMaster().getEmployeeFirstName());
						dtoEmployeeMaster.setEmployeeLastName(employeePostDatedPayRates.getEmployeeMaster().getEmployeeLastName());
						dtoEmployeePostDatedPayRates.setEmployeeMaster(dtoEmployeeMaster);
					}
					dtoEmployeePostDatedPayRates.setEffectiveDate(employeePostDatedPayRates.getEffectiveDate());
					dtoEmployeePostDatedPayRates.setCurrentPayRate(employeePostDatedPayRates.getCurrentPayRate());
					dtoEmployeePostDatedPayRates.setNewPayRate(employeePostDatedPayRates.getNewPayRate());
					dtoEmployeePostDatedPayRates.setReasonforChange(employeePostDatedPayRates.getReasonforChange());
					dtoEmployeePostDatedPayRates.setId(employeePostDatedPayRates.getId());
						
					dtoSearch.setRecords(dtoEmployeePostDatedPayRates);
				
			
			log.debug("Search EmployeePostDatedPayRates Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	public DtoEmployeePostDatedPayRates repeatByEmployeeId(int employeeIndexId) {
		log.info("repeatByEmployeeId Method");
		DtoEmployeePostDatedPayRates dtopostDate = new DtoEmployeePostDatedPayRates();
		try {
			List<EmployeePostDatedPayRates> misenroll=repositoryEmployeePostDatedPayRates.findByEmployeeId(employeeIndexId);
			if(misenroll!=null && !misenroll.isEmpty()) {
				dtopostDate.setIsRepeat(true);
				
			}else {
				dtopostDate.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtopostDate;
	}

	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoActivateEmployeePostDatePayRate getAllForActiveEmployee(DtoActivateEmployeePostDatePayRate dtoSearchPostDated) {
		try {

			log.info("search ActiveEmployeePostDatedPayRates Method");
			
			if(dtoSearchPostDated != null){
				String searchWordpostDated=dtoSearchPostDated.getSearchKeyword();
				String condtionforsorting="";
				
				if(dtoSearchPostDated.getSortOn()!=null || dtoSearchPostDated.getSortBy()!=null){
					if(dtoSearchPostDated.getSortOn().equals("reasonforChange")
						|| dtoSearchPostDated.getSortOn().equals("employeeMaster.employeeId") || 
						dtoSearchPostDated.getSortOn().equals("payCode.payCodeId") || dtoSearchPostDated.getSortOn().equals("effectiveDate")
						) {
						condtionforsorting = dtoSearchPostDated.getSortOn();
					
				}else {
					condtionforsorting = "id";
					dtoSearchPostDated.setSortOn("");
					dtoSearchPostDated.setSortBy("");
				}
			}else{
				condtionforsorting+="id";
				dtoSearchPostDated.setSortOn("");
				dtoSearchPostDated.setSortBy("");
				
			}
				dtoSearchPostDated.setTotalCount(this.repositoryEmployeePostDatedPayRates.predictiveActiveEmployeePostDatedPayRatesSearchTotalCount(dtoSearchPostDated.getId(), searchWordpostDated, dtoSearchPostDated.getEffectiveDate()));
				List<EmployeePostDatedPayRates> employeePostDatedPayRateslist =null;
				
				
				if(dtoSearchPostDated.getRanges()!=null) {
					if(dtoSearchPostDated.getRanges().equals("effectiveDates")) {
						if(dtoSearchPostDated.getFromDate()!=null && dtoSearchPostDated.getToDate()!=null) {
							employeePostDatedPayRateslist = repositoryEmployeePostDatedPayRates.findEffectiveDates(dtoSearchPostDated.getFromDate(),dtoSearchPostDated.getToDate());
						}
						
					}else if(dtoSearchPostDated.getRanges().equals("employeeId")) {
						if(dtoSearchPostDated.getFromemployeeIndexId()!=null &&dtoSearchPostDated.getToemployeeIndexId()!=null) {
							employeePostDatedPayRateslist=repositoryEmployeePostDatedPayRates.findEmployeeId(dtoSearchPostDated.getFromemployeeIndexId(),dtoSearchPostDated.getToemployeeIndexId());
						}
						
					}else if(dtoSearchPostDated.getRanges().equals("payCodes")) {
						if(dtoSearchPostDated.getFromPayCodeId()!=null && dtoSearchPostDated.getToPayCodeId()!=null) {
							employeePostDatedPayRateslist =repositoryEmployeePostDatedPayRates.findPayCodeId(dtoSearchPostDated.getFromPayCodeId(), dtoSearchPostDated.getToPayCodeId());
						}
						
					}else if(dtoSearchPostDated.getRanges().equals("reasonForChange") && dtoSearchPostDated.getFromReasion()!=null && dtoSearchPostDated.getToReasion()!=null) {
							employeePostDatedPayRateslist =repositoryEmployeePostDatedPayRates.findResonForChange(dtoSearchPostDated.getFromReasion(), dtoSearchPostDated.getToReasion());
					}
				}
				
				List<DtoEmployeePostDatedPayRates> dtoEmployeePostDatedPayRatesList = getdtoEmployeePostDatedPayRatesList(employeePostDatedPayRateslist);
				dtoSearchPostDated.setRecords(dtoEmployeePostDatedPayRatesList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearchPostDated;
	}
	
	
	
	
	
	
	
	public List<DtoEmployeePostDatedPayRates> dtoEmployeePostDatedPayRatesList(DtoSearch dtoSearch,String searchWord){
		dtoSearch.setTotalCount(this.repositoryEmployeePostDatedPayRates.predictiveEmployeePostDatedPayRatesSearchTotalCounts("%"+searchWord+"%"));
		List<EmployeePostDatedPayRates> employeePostDatedPayRatesList =null;
		if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
			
			if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
				employeePostDatedPayRatesList = this.repositoryEmployeePostDatedPayRates.predictiveEmployeePostDatedPayRatesSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if(dtoSearch.getSortBy().equals("ASC")){
				employeePostDatedPayRatesList = this.repositoryEmployeePostDatedPayRates.predictiveEmployeePostDatedPayRatesSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getSortOn()));
			}else if(dtoSearch.getSortBy().equals("DESC")){
				employeePostDatedPayRatesList = this.repositoryEmployeePostDatedPayRates.predictiveEmployeePostDatedPayRatesSearchWithPaginations("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getSortOn()));
			}
			
		}
		return getdtoEmployeePostDatedPayRatesList(employeePostDatedPayRatesList);
	}
	
	public List<DtoEmployeePostDatedPayRates>  getdtoEmployeePostDatedPayRatesList(List<EmployeePostDatedPayRates> employeePostDatedPayRatesList){
		List<DtoEmployeePostDatedPayRates> dtoEmployeePostDatedPayRatesList = new ArrayList<>();
		if(employeePostDatedPayRatesList != null && !employeePostDatedPayRatesList.isEmpty()){
			DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates=null;
			for (EmployeePostDatedPayRates employeePostDatedPayRates : employeePostDatedPayRatesList) {
				dtoEmployeePostDatedPayRates = new DtoEmployeePostDatedPayRates(employeePostDatedPayRates);
				
				DtoEmployeeMasterHcm employeeMasterHcm = new DtoEmployeeMasterHcm();
				if(employeePostDatedPayRates.getEmployeeMaster()!=null) {
					employeeMasterHcm.setEmployeeIndexId(employeePostDatedPayRates.getEmployeeMaster().getEmployeeIndexId());
					employeeMasterHcm.setEmployeeFirstName(employeePostDatedPayRates.getEmployeeMaster().getEmployeeFirstName());
					employeeMasterHcm.setEmployeeMiddleName(employeePostDatedPayRates.getEmployeeMaster().getEmployeeMiddleName());
					employeeMasterHcm.setEmployeeLastName(employeePostDatedPayRates.getEmployeeMaster().getEmployeeLastName());
					employeeMasterHcm.setEmployeeId(employeePostDatedPayRates.getEmployeeMaster().getEmployeeId());
					dtoEmployeePostDatedPayRates.setEmployeeMasterHcm(employeeMasterHcm);
			
				}
				DtoPayCode payCode=new DtoPayCode();
				if(employeePostDatedPayRates.getPayCode()!=null) {
					payCode.setId(employeePostDatedPayRates.getPayCode().getId());
					payCode.setPayCodeId(employeePostDatedPayRates.getPayCode().getPayCodeId());
					payCode.setDescription(employeePostDatedPayRates.getPayCode().getDescription());
					payCode.setArbicDescription(employeePostDatedPayRates.getPayCode().getArbicDescription());
					payCode.setBaseOnPayCode(employeePostDatedPayRates.getPayCode().getBaseOnPayCode());
					payCode.setBaseOnPayCodeAmount(employeePostDatedPayRates.getPayCode().getBaseOnPayCodeAmount());
					payCode.setPayFactor(employeePostDatedPayRates.getPayCode().getPayFactor());
					payCode.setPayRate(employeePostDatedPayRates.getPayCode().getPayRate());
					payCode.setUnitofPay(employeePostDatedPayRates.getPayCode().getUnitofPay());
					payCode.setPayperiod(employeePostDatedPayRates.getPayCode().getPayperiod());
					payCode.setInActive(employeePostDatedPayRates.getPayCode().isInActive());
					dtoEmployeePostDatedPayRates.setPayCode(payCode);
					
				}
				
				dtoEmployeePostDatedPayRates.setId(employeePostDatedPayRates.getId());
				dtoEmployeePostDatedPayRates.setEffectiveDate(employeePostDatedPayRates.getEffectiveDate());
				dtoEmployeePostDatedPayRates.setCurrentPayRate(employeePostDatedPayRates.getCurrentPayRate());
				dtoEmployeePostDatedPayRates.setNewPayRate(employeePostDatedPayRates.getNewPayRate());
				dtoEmployeePostDatedPayRates.setReasonforChange(employeePostDatedPayRates.getReasonforChange());
				dtoEmployeePostDatedPayRates.setId(employeePostDatedPayRates.getId());
				dtoEmployeePostDatedPayRatesList.add(dtoEmployeePostDatedPayRates);
				
			}
			
		}
		return dtoEmployeePostDatedPayRatesList;
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		
		try {

			log.info("ActiveEmployeePostDatedPayRates Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				dtoSearch = getDtoSearch(dtoSearch);  
				List<DtoEmployeePostDatedPayRates> list = 	dtoEmployeePostDatedPayRatesList(dtoSearch,searchWord);
					dtoSearch.setRecords(list);
			}
			
			} catch (Exception e) {
				log.error(e);
			}
			
		return dtoSearch;  
	}

		
	public List<DtoEmployeePostDatedPayRates> getAllReasonForDropDown() {
		log.info("getAllReasonForDropDown  Method");
		List<DtoEmployeePostDatedPayRates> employeePostDatedPayRatesList = new ArrayList<>();
		try {
			List<EmployeePostDatedPayRates> list = repositoryEmployeePostDatedPayRates.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (EmployeePostDatedPayRates employeePostDatedPayRates : list) {
					DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates = new DtoEmployeePostDatedPayRates();
					dtoEmployeePostDatedPayRates.setId(employeePostDatedPayRates.getId());
					dtoEmployeePostDatedPayRates.setReasonforChange(employeePostDatedPayRates.getReasonforChange());
					employeePostDatedPayRatesList.add(dtoEmployeePostDatedPayRates);
				}
			}
			log.debug("PayCode is:" + employeePostDatedPayRatesList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return employeePostDatedPayRatesList;
	}
	
}
