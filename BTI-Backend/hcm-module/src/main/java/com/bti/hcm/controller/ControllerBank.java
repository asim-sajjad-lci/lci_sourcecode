package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoBank;
import com.bti.hcm.service.ServiceBank;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/bank")
public class ControllerBank extends BaseController {

	
	private static final Logger LOGGER = Logger.getLogger(ControllerBank.class);

	/**
	 * @Description serviceBank Autowired here using annotation of spring for
	 *              use of serviceBank method in this controller
	 */
	@Autowired(required = true)
	ServiceBank serviceBank;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllBankDropDownList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllBankDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoBank> bankList = serviceBank.getAllBankDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_GET_ALL", false), bankList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return responseMessage;
	}


}
