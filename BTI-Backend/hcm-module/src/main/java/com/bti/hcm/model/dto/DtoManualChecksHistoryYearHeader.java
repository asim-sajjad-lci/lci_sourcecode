package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DtoManualChecksHistoryYearHeader extends DtoBase {

	private Integer id;
	private Integer year;
	private String paymentDescription;
	private Integer checkNumber;
	private Date checkDate;
	private Date checkPostDate;
	private Integer employeeId;
	private BigDecimal taoalGrossAmount;
	private BigDecimal totalNetAmpount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public Integer getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public Date getCheckPostDate() {
		return checkPostDate;
	}

	public void setCheckPostDate(Date checkPostDate) {
		this.checkPostDate = checkPostDate;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public BigDecimal getTaoalGrossAmount() {
		return taoalGrossAmount;
	}

	public void setTaoalGrossAmount(BigDecimal taoalGrossAmount) {
		this.taoalGrossAmount = taoalGrossAmount;
	}

	public BigDecimal getTotalNetAmpount() {
		return totalNetAmpount;
	}

	public void setTotalNetAmpount(BigDecimal totalNetAmpount) {
		this.totalNetAmpount = totalNetAmpount;
	}

}
