package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.PayScheduleSetupSchedulePeriods;

public class DtoPayScheduleSetupSchedulePeriods extends DtoBase{
	
	private Integer payScheduleIndexId;
	private int periodId;
	private int year;
	private String periodName;
	private Date periodStartDate;
	private Date periodEndDate;
	private List<DtoPayScheduleSetupSchedulePeriods> deletePayScheduleSetupSchedulePeriods;
	private Integer payScheduleSetupId;
	private String periodNameId;
	public Integer getPayScheduleIndexId() {
		return payScheduleIndexId;
	}
	public void setPayScheduleIndexId(Integer payScheduleIndexId) {
		this.payScheduleIndexId = payScheduleIndexId;
	}
	public int getPeriodId() {
		return periodId;
	}
	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getPeriodName() {
		return periodName;
	}
	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}
	public Date getPeriodStartDate() {
		return periodStartDate;
	}
	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	}
	public Date getPeriodEndDate() {
		return periodEndDate;
	}
	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}
	
	public List<DtoPayScheduleSetupSchedulePeriods> getDeletePayScheduleSetupSchedulePeriods() {
		return deletePayScheduleSetupSchedulePeriods;
	}
	public void setDeletePayScheduleSetupSchedulePeriods(
			List<DtoPayScheduleSetupSchedulePeriods> deletePayScheduleSetupSchedulePeriods) {
		this.deletePayScheduleSetupSchedulePeriods = deletePayScheduleSetupSchedulePeriods;
	}
	
	public DtoPayScheduleSetupSchedulePeriods() {
		
	}
	public DtoPayScheduleSetupSchedulePeriods(PayScheduleSetupSchedulePeriods payScheduleSetupSchedulePeriods) {
	
	}
	public Integer getPayScheduleSetupId() {
		return payScheduleSetupId;
	}
	public void setPayScheduleSetupId(Integer payScheduleSetupId) {
		this.payScheduleSetupId = payScheduleSetupId;
	}
	public String getPeriodNameId() {
		return periodNameId;
	}
	public void setPeriodNameId(String periodNameId) {
		this.periodNameId = periodNameId;
	}

}
