package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeDependent;

@Repository("repositoryEmployeeDependent")
public interface RepositoryEmployeeDependent extends JpaRepository<EmployeeDependent, Integer>{

	EmployeeDependent findByIdAndIsDeleted(Integer id, boolean b);

	EmployeeDependent saveAndFlush(EmployeeDependent employeeDependent);

	EmployeeDependent findOne(Integer planId);
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDependent e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.id =:id ")
	public void deleteSingleEmployeeDependent(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from EmployeeDependent e where (e.empFistName LIKE :searchKeyWord  or e.empMidleName LIKE :searchKeyWord or e.empLastName LIKE :searchKeyWord  or e.comments LIKE :searchKeyWord  "
			+ "or e.dateOfBirth LIKE :searchKeyWord  or e.phoneNumber LIKE :searchKeyWord or e.workNumber LIKE :searchKeyWord or e.address LIKE :searchKeyWord or e.city LIKE :searchKeyWord ) and e.isDeleted=false")
	Integer predictiveEmployeeDependentSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);

	@Query("select e from EmployeeDependent e where (e.empFistName LIKE :searchKeyWord  or e.empMidleName LIKE :searchKeyWord or e.empLastName LIKE :searchKeyWord  or e.comments LIKE :searchKeyWord  "
			+ "or e.dateOfBirth LIKE :searchKeyWord  or e.phoneNumber LIKE :searchKeyWord or e.workNumber LIKE :searchKeyWord or e.address LIKE :searchKeyWord or e.city LIKE :searchKeyWord ) and e.isDeleted=false")
	List<EmployeeDependent> predictiveEmployeeDependentSearchWithPagination(@Param("searchKeyWord")String searchKeyWord, Pageable pageable);

	List<EmployeeDependent> findByIsDeleted(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDependent e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.employeeMaster.employeeIndexId =:id ")
	void deleteByEmployeeId(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,@Param("id")Integer searchKeyWord);

	@Query("select count(*) from EmployeeDependent d where ( d.empFistName like :searchKeyWord or d.empMidleName like :searchKeyWord or d.empLastName like :searchKeyWord or d.comments like :searchKeyWord or d.phoneNumber like :searchKeyWord or d.workNumber like :searchKeyWord or d.address like :searchKeyWord) and d.employeeMaster.employeeIndexId =:id and d.isDeleted=false")
	Integer getDependentByEmployeeIdCount(@Param("searchKeyWord") String searchKeyWord,@Param("id")Integer employeeId);

	
	@Query("select d from EmployeeDependent d where( d.empFistName like :searchKeyWord or d.empMidleName like :searchKeyWord or d.empLastName like :searchKeyWord or d.comments like :searchKeyWord or d.phoneNumber like :searchKeyWord or d.workNumber like :searchKeyWord or d.address like :searchKeyWord) and d.employeeMaster.employeeIndexId =:id and d.isDeleted=false")
	List<EmployeeDependent> getDependentByEmployeeIdCount(@Param("searchKeyWord") String searchKeyWord, @Param("id")Integer employeeId, Pageable pageRequest);

	@Query("select count(*) from EmployeeDependent e ")
	public Integer getCountOfTotalEmployeeDependent();

}
