/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoOrientationPredefinedCheckListItem;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceOrientationPredefinedCheckListItem;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerOrientationPredefinedCheckListItem
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/orientationPredefinedCheckListItem")
public class ControllerOrientationPredefinedCheckListItem extends BaseController{

	
	/**
	 * @Description LOGGER use for put a logger in OrientationPredefinedCheckListItem Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerOrientationPredefinedCheckListItem.class);
	
	/**
	 * @Description serviceOrientationPredefinedCheckListItem Autowired here using annotation of spring for use of serviceOrientationPredefinedCheckListItem method in this controller
	 */
	@Autowired(required=true)
	ServiceOrientationPredefinedCheckListItem serviceOrientationPredefinedCheckListItem;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @description : Create OrientationPredefinedCheckListItem
	 * @param request
	 * @param dtoOrientationPredefinedCheckListItem
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "preDefineCheckListType":2,
		 "preDefineCheckListItemDescription":"Descriptions",
		  "preDefineCheckListItemDescriptionArabic":"Arabic Descriptions"
		  
		}
		
		@response
		{
				"code": 201,
				"status": "CREATED",
				"result": {
				"id": 0,
				"preDefineCheckListType": 2,
				"preDefineCheckListItemDescription": "Descriptions",
				"preDefineCheckListItemDescriptionArabic": "Arabic Descriptions"
			},
			"btiMessage": {
			"message": "OrientationPredefinedChecklistItem created successfully.",
			"messageShort": "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_CREATED"
			}
		}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createOrientationPredefinedCheckListItem(HttpServletRequest request, @RequestBody DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem) throws Exception {
		LOGGER.info("Create OrientationPredefinedCheckListItem Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoOrientationPredefinedCheckListItem = serviceOrientationPredefinedCheckListItem.saveOrUpdateOrientationPredefinedCheckListItem(dtoOrientationPredefinedCheckListItem);
			responseMessage=displayMessage(dtoOrientationPredefinedCheckListItem, "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_CREATED", "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create OrientationPredefinedCheckListItem Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description update OrientationPredefinedCheckListItem
	 * @param request
	 * @param dtoOrientationPredefinedCheckListItem
	 * @return
	 * @throws Exception
	 * @request
	 * {
			  "id":1,
			  "preDefineCheckListType":2,
			 "preDefineCheckListItemDescription":"Descriptionssss",
			  "preDefineCheckListItemDescriptionArabic":"Arabic Descriptionsss"
			  
			}
			
	 @response
	 
	 {
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": 1,
		"preDefineCheckListType": 2,
		"preDefineCheckListItemDescription": "Descriptionssss",
		"preDefineCheckListItemDescriptionArabic": "Arabic Descriptionsss"
		},
	"btiMessage": {
			"message": "OrientationPredefinedChecklistItem updated successfully.",
			"messageShort": "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_UPDATED_SUCCESS"
		}
	}
	
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateOrientationPredefinedCheckListItem(HttpServletRequest request, @RequestBody DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem) throws Exception {
		LOGGER.info("Update OrientationPredefinedCheckListItem Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoOrientationPredefinedCheckListItem = serviceOrientationPredefinedCheckListItem.saveOrUpdateOrientationPredefinedCheckListItem(dtoOrientationPredefinedCheckListItem);
			responseMessage=displayMessage(dtoOrientationPredefinedCheckListItem, "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_UPDATED_SUCCESS", MessageConstant.ORIENTATION_PREDEFINED_CHECKLIST_ITEM_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update OrientationPredefinedCheckListItem Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @description delete OrientationPredefinedCheckListItem
	 * @param request
	 * @param dtoOrientationPredefinedCheckListItem
	 * @return
	 * @throws Exception
	 * @request
			 * {
		  "ids":[1]
		}
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": 0,
			"preDefineCheckListType": 0,
			"deleteMessage": "OrientationPredefinedChecklistItem deleted successfully.",
			"associateMessage": "N/A",
			"deleteOrientationPredefinedCheckListItem": [
			  {
					"id": 1,
					"preDefineCheckListType": 2,
					"preDefineCheckListItemDescription": "Descriptionssss",
					"preDefineCheckListItemDescriptionArabic": "Arabic Descriptionsss"
			}
		],
	},
		"btiMessage": {
			"message": "OrientationPredefinedChecklistItem deleted successfully.",
			"messageShort": "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_DELETED"
		}
	}
	 */
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteOrientationPredefinedCheckListItem(HttpServletRequest request, @RequestBody DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem) throws Exception {
		LOGGER.info("Delete OrientationPredefinedCheckListItem Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoOrientationPredefinedCheckListItem.getIds() != null && !dtoOrientationPredefinedCheckListItem.getIds().isEmpty()) {
				DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem2 = serviceOrientationPredefinedCheckListItem.deleteOrientationPredefinedCheckListItem(dtoOrientationPredefinedCheckListItem.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ORIENTATION_PREDEFINED_CHECKLIST_ITEM_DELETED", false), dtoOrientationPredefinedCheckListItem2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete OrientationPredefinedCheckListItem Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description search OrientationPredefinedCheckListItem
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 * @request{
		  "searchKeyword":"t",
		  "pageNumber" : 0,
		  "pageSize" :2
		 
		}
		 * @response{
			"code": 200,
			"status": "OK",
			"result": {
			"searchKeyword": "t",
			"pageNumber": 0,
			"pageSize": 2,
			"totalCount": 8,
			"records": [
			  {
				"id": 1,
					"preDefineCheckListType": 2,
					"preDefineCheckListItemDescription": "Descriptionssss",
					"preDefineCheckListItemDescriptionArabic": "Arabic Descriptionsss"
			}
				],
			},
			"btiMessage": {
				"message": "OrientationPredefinedChecklistItem list fetched successfully",
				"messageShort": "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_GET_ALL"
				}
			}
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchOrientationPredefinedCheckListItem(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search OrientationPredefinedCheckListItem Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceOrientationPredefinedCheckListItem.searchOrientationPredefinedCheckListItem(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_GET_ALL", "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search OrientationPredefinedCheckListItem Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	/**
	 * @description get All OrientationPredefinedCheckListItem
	 * @param request
	 * @param dtoOrientationPredefinedCheckListItem
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.POST)
	public ResponseMessage getAllOrientationPredefinedCheckListItem(HttpServletRequest request, @RequestBody DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem) throws Exception {
		LOGGER.info("Get All OrientationPredefinedCheckListItem Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceOrientationPredefinedCheckListItem.getAllOrientationPredefinedCheckListItem(dtoOrientationPredefinedCheckListItem);
			responseMessage=displayMessage(dtoSearch, "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_GET_ALL", "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All OrientationPredefinedCheckListItem Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description get OrientationPredefinedCheckListItem By Id
	 * @param request
	 * @param dtoOrientationPredefinedCheckListItem
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "id":2
		}
		
	 *@response
	 *{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": 2,
			"preDefineCheckListType": 0,
			"preDefineCheckListItemDescription": "Descriptions",
			"preDefineCheckListItemDescriptionArabic": "Arabic Descriptions"
			},
			"btiMessage": {
			"message": "OrientationPredefinedChecklistItem class list fetched successfully.",
			"messageShort": "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_GET_DETAIL"
			}
		}
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getOrientationPredefinedCheckListItemById(HttpServletRequest request, @RequestBody DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem) throws Exception {
		LOGGER.info("Get OrientationPredefinedCheckListItem ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItemObj = serviceOrientationPredefinedCheckListItem.getDtoOrientationPredefinedCheckListItemById(dtoOrientationPredefinedCheckListItem.getId());
			responseMessage=displayMessage(dtoOrientationPredefinedCheckListItemObj, "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_GET_DETAIL", "ORIENTATION_PREDEFINED_CHECKLIST_ITEM_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get OrientationPredefinedCheckListItem ById Method:"+dtoOrientationPredefinedCheckListItem.getId());
		return responseMessage;
	}
	
	/**
	 * @description Id is exiest or not
	 * @param request
	 * @param dtoOrientationPredefinedCheckListItem
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "id":2
		}
		
	 *@response
	 
	 	{
		"code": 302,
		"status": "FOUND",
		"result": {
		"id": 0,
		"preDefineCheckListType": 0,
		"isRepeat": true
		},
		"btiMessage": {
			"message": "OrientationPredefinedChecklistItem ID already exists.",
			"messageShort": "ORIENTATION_PREDEFINED_CHECKLIST_ITEMT_RESULT"
		}
	}
	 */
	@RequestMapping(value = "/orientationPredefinedCheckListItemIdcheck", method = RequestMethod.POST)
	public ResponseMessage departmetnIdCheck(HttpServletRequest request, @RequestBody DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItem) throws Exception {
		LOGGER.info("OrientationPredefinedCheckListItem ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoOrientationPredefinedCheckListItem dtoOrientationPredefinedCheckListItemObj = serviceOrientationPredefinedCheckListItem.repeatById(dtoOrientationPredefinedCheckListItem.getId());
			responseMessage=displayMessage(dtoOrientationPredefinedCheckListItemObj, "ORIENTATION_PREDEFINED_CHECKLIST_ITEMT_RESULT", "ORIENTATION_PREDEFINED_CHECKLIST_ITEMT_REPEAT_ID_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllList", method = RequestMethod.GET)
	public ResponseMessage getAllList( HttpServletRequest request) throws Exception {
	DtoSearch dtoSearch = new DtoSearch();
		LOGGER.info("Search getAllList Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceOrientationPredefinedCheckListItem.getAllList(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ORIENTATION_PREDEFINE_CHECKLIST_ITEM_GET_ALL", "ORIENTATION_PREDEFINE_CHECKLIST_ITEM_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search OrientationPredefinedCheckListItem Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
}
