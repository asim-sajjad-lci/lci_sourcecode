package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecksHistoryYearDistributions;

@Repository
public interface RepositoryManualChecksHistoryYearDistribution
		extends JpaRepository<ManualChecksHistoryYearDistributions, Integer> {

}
