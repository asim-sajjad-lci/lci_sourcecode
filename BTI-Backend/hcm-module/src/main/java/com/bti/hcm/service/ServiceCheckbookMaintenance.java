/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.CheckbookMaintenance;
import com.bti.hcm.model.dto.DtoCheckbookMaintenance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryCheckBookMaintenance;

/**
 * Description: Service Price Group Setup
 * Project: Ims 
 * Version: 0.0.1
 */
@Service("serviceCheckbookMaintenance")
public class ServiceCheckbookMaintenance {
	/**
	 * @Description LOGGER use for put a logger in ServiceMaterialsSetupBill Service
	 */
	static Logger log = Logger.getLogger(ServiceCheckbookMaintenance.class.getName());


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in MaterialsSetupBill service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in MaterialsSetupBill service
	 */
	@Autowired(required=false)
	ServiceCheckbookMaintenance serviceItemCategorySetup;
	
	@Autowired(required=false) 
	RepositoryCheckBookMaintenance repositoryCheckbookMaintenance;

	@Autowired
	ServiceResponse response;
	
	public DtoCheckbookMaintenance saveOrUpdate(DtoCheckbookMaintenance dtoCheckbookMaintenance) {
		
		log.info("MaterialsSetupBill saveOrUpdate Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		CheckbookMaintenance checkbookMaintenance= null;
		if (dtoCheckbookMaintenance.getId() != null && dtoCheckbookMaintenance.getId() > 0) {
			
			checkbookMaintenance = repositoryCheckbookMaintenance.findByIdAndIsDeleted(dtoCheckbookMaintenance.getId(), false);
			checkbookMaintenance.setUpdatedBy(loggedInUserId);
			checkbookMaintenance.setUpdatedDate(new Date());
		} else {
			checkbookMaintenance = new CheckbookMaintenance();
			checkbookMaintenance.setCreatedDate(new Date());
			Integer rowId = repositoryCheckbookMaintenance.findAll().size();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			checkbookMaintenance.setRowId(increment);
		}
		
		checkbookMaintenance.setCheckbookId(dtoCheckbookMaintenance.getCheckbookId());
		checkbookMaintenance.setCheckbookDescription(dtoCheckbookMaintenance.getCheckbookDescription());
		checkbookMaintenance.setCheckbookArabicDescription(dtoCheckbookMaintenance.getCheckbookArabicDescription());
		checkbookMaintenance.setBankIdAddress(dtoCheckbookMaintenance.getBankIdAddress());
		checkbookMaintenance.setAccountTableRowIndex(dtoCheckbookMaintenance.getAccountTableRowIndex());
		checkbookMaintenance.setNextCheckNumber(dtoCheckbookMaintenance.getNextCheckNumber());
		checkbookMaintenance.setNextDepositNumber(dtoCheckbookMaintenance.getNextDepositNumber());
		
		
		checkbookMaintenance.setRowId(loggedInUserId);
		checkbookMaintenance = repositoryCheckbookMaintenance.saveAndFlush(checkbookMaintenance);
		log.debug("materialsSetupBill is:"+checkbookMaintenance.getId());
		
		return dtoCheckbookMaintenance;
	}
	
	public DtoCheckbookMaintenance deleteCheckbookMaintenance(List<Integer> ids) {
		
		log.info("deleteMaterialsSetupBill Method");
		DtoCheckbookMaintenance dtoCheckbookMaintenance = new DtoCheckbookMaintenance();
		dtoCheckbookMaintenance
				.setDeleteMessage(response.getStringMessageByShortAndIsDeleted("PRICE_GROUP_SETUP_DELETED", false));
		dtoCheckbookMaintenance.setAssociateMessage(response.getStringMessageByShortAndIsDeleted("PRICE_GROUP_SETUP_DELETED", false));
		List<DtoCheckbookMaintenance> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		
		try {
			for (Integer positionId : ids) {
			//	ItemCategorySetup ItemCategorySetup = repositoryItemCategorySetup.findOne(positionId);
				
				DtoCheckbookMaintenance dtocheckbookMaintenance = new DtoCheckbookMaintenance();
					dtocheckbookMaintenance.setId(positionId);
					repositoryCheckbookMaintenance.deleteSingleCheckbookMaintenance(true, loggedInUserId, positionId);
					//deletePosition.add(dtoCheckbookMaintenance);
			}
			
			dtoCheckbookMaintenance.setDelete(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete deleteHelthInsurance :"+dtoCheckbookMaintenance.getId());
		return dtoCheckbookMaintenance;
	}
	
	
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				if(dtoSearch.getSortOn().equals("priceGroup") || dtoSearch.getSortOn().equals("description") 
						|| dtoSearch.getSortOn().equals("arabicDescription") 
						) {
					if(dtoSearch.getSortOn().equals("benefitsId")) {
						dtoSearch.setSortOn("BenefitsId");
					}
					condition = dtoSearch.getSortOn();
				
			}else {
				condition = "id";
				
			}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
			
		}
			
				dtoSearch.setTotalCount(this.repositoryCheckbookMaintenance.predictiveCheckbookMaintenanceSearchTotalCount("%"+searchWord+"%"));	
		
			
			List<CheckbookMaintenance> checkbookMaintenance =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					checkbookMaintenance = this.repositoryCheckbookMaintenance.predictiveCheckbookMaintenanceSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					checkbookMaintenance = this.repositoryCheckbookMaintenance.predictiveCheckbookMaintenanceSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					checkbookMaintenance = this.repositoryCheckbookMaintenance.predictiveCheckbookMaintenanceSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
					
		/*	dtoSearch.setTotalCount(this.repositoryCheckbookMaintenance.predictiveCheckbookMaintenanceSearchTotalCount("%"+searchWord+"%"));
			List<CheckbookMaintenance> CheckbookMaintenance =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				CheckbookMaintenance = this.repositoryCheckbookMaintenance.predictiveItemCategorySetupSearchWithPagination("%"+searchWord+"%");
			}else {
				CheckbookMaintenance = this.repositoryCheckbookMaintenance.predictiveCheckbookMaintenanceSearchWithPagination("%"+searchWord+"%");
			}*/
		
			if(checkbookMaintenance != null && !checkbookMaintenance.isEmpty()){
				List<DtoCheckbookMaintenance> dtoCheckbookMaintenanceLists = new ArrayList<>();
				DtoCheckbookMaintenance dtoCheckbookMaintenance=null;
				for (CheckbookMaintenance ItemCategorySetup1 : checkbookMaintenance) {
					dtoCheckbookMaintenance = new DtoCheckbookMaintenance(ItemCategorySetup1);
					dtoCheckbookMaintenance.setId(ItemCategorySetup1.getId());
					dtoCheckbookMaintenance.setCheckbookId(ItemCategorySetup1.getCheckbookId());
					dtoCheckbookMaintenance.setCheckbookDescription(ItemCategorySetup1.getCheckbookDescription());
					dtoCheckbookMaintenance.setCheckbookArabicDescription(ItemCategorySetup1.getCheckbookArabicDescription());
					dtoCheckbookMaintenance.setBankIdAddress(ItemCategorySetup1.getBankIdAddress());
					dtoCheckbookMaintenance.setAccountTableRowIndex(ItemCategorySetup1.getAccountTableRowIndex());
					dtoCheckbookMaintenance.setNextCheckNumber(ItemCategorySetup1.getNextCheckNumber());
					dtoCheckbookMaintenance.setNextDepositNumber(ItemCategorySetup1.getNextDepositNumber());
					dtoCheckbookMaintenanceLists.add(dtoCheckbookMaintenance);
				}
				dtoSearch.setRecords(dtoCheckbookMaintenanceLists);
			}
		}
		log.debug("Search searchMaterialsSetupBill Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	public DtoCheckbookMaintenance repeatByCompanyId(Integer positionId) {
		log.info("repeatByCompanyId Method");
		DtoCheckbookMaintenance dtoCheckbookMaintenance = new DtoCheckbookMaintenance();
		try {
			CheckbookMaintenance checkbookMaintenance = null;
			if(positionId!=null) {
				checkbookMaintenance	= repositoryCheckbookMaintenance.findById(positionId);
			}
			
			if(checkbookMaintenance!=null ) {
				dtoCheckbookMaintenance.setId(checkbookMaintenance.getId());
				
				
				
				dtoCheckbookMaintenance.setId(checkbookMaintenance.getId());
				dtoCheckbookMaintenance.setCheckbookId(checkbookMaintenance.getCheckbookId());
				dtoCheckbookMaintenance.setCheckbookDescription(checkbookMaintenance.getCheckbookDescription());
				dtoCheckbookMaintenance.setCheckbookArabicDescription(checkbookMaintenance.getCheckbookArabicDescription());
				dtoCheckbookMaintenance.setBankIdAddress(checkbookMaintenance.getBankIdAddress());
				dtoCheckbookMaintenance.setAccountTableRowIndex(checkbookMaintenance.getAccountTableRowIndex());
				dtoCheckbookMaintenance.setNextCheckNumber(checkbookMaintenance.getNextCheckNumber());
				dtoCheckbookMaintenance.setNextDepositNumber(checkbookMaintenance.getNextDepositNumber());
				
				
				dtoCheckbookMaintenance.setIsRepeat(true);
			}else {
				dtoCheckbookMaintenance.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoCheckbookMaintenance;
	}

	public boolean uniqueCheck(String price) {
		List<CheckbookMaintenance> check = repositoryCheckbookMaintenance.uniqueCheck(price);

		if (check == null || check.isEmpty())
			return true;
		else
			return false;
	}

	public List<CheckbookMaintenance> getAllData() {
		List<CheckbookMaintenance> check = repositoryCheckbookMaintenance.getAllData();
		return check;
	}

	public DtoSearch getAllDroupdown(DtoSearch dtoSearch) {
		log.info("search Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			
					
		
			
			List<CheckbookMaintenance> checkbookMaintenance =null;
			checkbookMaintenance = this.repositoryCheckbookMaintenance.getAllDroupdown("%"+searchWord+"%");
			
					
		/*	dtoSearch.setTotalCount(this.repositoryCheckbookMaintenance.predictiveCheckbookMaintenanceSearchTotalCount("%"+searchWord+"%"));
			List<CheckbookMaintenance> CheckbookMaintenance =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				CheckbookMaintenance = this.repositoryCheckbookMaintenance.predictiveItemCategorySetupSearchWithPagination("%"+searchWord+"%");
			}else {
				CheckbookMaintenance = this.repositoryCheckbookMaintenance.predictiveCheckbookMaintenanceSearchWithPagination("%"+searchWord+"%");
			}*/
		
			if(checkbookMaintenance != null && !checkbookMaintenance.isEmpty()){
				List<DtoCheckbookMaintenance> dtoCheckbookMaintenanceLists = new ArrayList<>();
				DtoCheckbookMaintenance dtoCheckbookMaintenance=null;
				for (CheckbookMaintenance ItemCategorySetup1 : checkbookMaintenance) {
					dtoCheckbookMaintenance = new DtoCheckbookMaintenance(ItemCategorySetup1);
					dtoCheckbookMaintenance.setId(ItemCategorySetup1.getId());
					dtoCheckbookMaintenance.setCheckbookId(ItemCategorySetup1.getCheckbookId());
					dtoCheckbookMaintenance.setCheckbookDescription(ItemCategorySetup1.getCheckbookDescription());
					dtoCheckbookMaintenance.setCheckbookArabicDescription(ItemCategorySetup1.getCheckbookArabicDescription());
					dtoCheckbookMaintenance.setBankIdAddress(ItemCategorySetup1.getBankIdAddress());
					dtoCheckbookMaintenance.setAccountTableRowIndex(ItemCategorySetup1.getAccountTableRowIndex());
					dtoCheckbookMaintenance.setNextCheckNumber(ItemCategorySetup1.getNextCheckNumber());
					dtoCheckbookMaintenance.setNextDepositNumber(ItemCategorySetup1.getNextDepositNumber());
					dtoCheckbookMaintenanceLists.add(dtoCheckbookMaintenance);
				}
				dtoSearch.setRecords(dtoCheckbookMaintenanceLists);
			}
		}
		return dtoSearch;
	}


}
