/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.Requisitions;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Requisitions class having getter and setter for fields
 * (POJO) Name Name of Project: Hcm Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoRequisitions extends DtoBase{

	private Integer id;
	private short stats;
	private Date internalPostDate;
	private Date internalCloseDate;
	private Date openingDate;
	private String recruiterName;
	private int companyId;
	private String companyName;
	private String managerName;
	private short jobPosting;
	private String advertisingField1;
	private String advertisingField2;
	private String advertisingField3;
	private String advertisingField4;
	private int positionsAvailable;
	private int positionsFilled;
	private int applicantsApplied;
	private int applicantsInterviewed;
	private BigDecimal costadvirs;
	private BigDecimal costrecri;
	private Integer departmentId;
	private Integer divisionId;
	private Integer supervisorId;
	private Integer positionId;

	private List<DtoRequisitions> deleteRequisitions;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public short getStats() {
		return stats;
	}

	public void setStats(short stats) {
		this.stats = stats;
	}

	public Date getInternalPostDate() {
		return internalPostDate;
	}

	public void setInternalPostDate(Date internalPostDate) {
		this.internalPostDate = internalPostDate;
	}

	public Date getInternalCloseDate() {
		return internalCloseDate;
	}

	public void setInternalCloseDate(Date internalCloseDate) {
		this.internalCloseDate = internalCloseDate;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public String getRecruiterName() {
		return recruiterName;
	}

	public void setRecruiterName(String recruiterName) {
		this.recruiterName = recruiterName;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public short getJobPosting() {
		return jobPosting;
	}

	public void setJobPosting(short jobPosting) {
		this.jobPosting = jobPosting;
	}

	public String getAdvertisingField1() {
		return advertisingField1;
	}

	public void setAdvertisingField1(String advertisingField1) {
		this.advertisingField1 = advertisingField1;
	}

	public String getAdvertisingField2() {
		return advertisingField2;
	}

	public void setAdvertisingField2(String advertisingField2) {
		this.advertisingField2 = advertisingField2;
	}

	public String getAdvertisingField3() {
		return advertisingField3;
	}

	public void setAdvertisingField3(String advertisingField3) {
		this.advertisingField3 = advertisingField3;
	}

	public String getAdvertisingField4() {
		return advertisingField4;
	}

	public void setAdvertisingField4(String advertisingField4) {
		this.advertisingField4 = advertisingField4;
	}

	public int getPositionsAvailable() {
		return positionsAvailable;
	}

	public void setPositionsAvailable(int positionsAvailable) {
		this.positionsAvailable = positionsAvailable;
	}

	public int getPositionsFilled() {
		return positionsFilled;
	}

	public void setPositionsFilled(int positionsFilled) {
		this.positionsFilled = positionsFilled;
	}

	public int getApplicantsApplied() {
		return applicantsApplied;
	}

	public void setApplicantsApplied(int applicantsApplied) {
		this.applicantsApplied = applicantsApplied;
	}

	public int getApplicantsInterviewed() {
		return applicantsInterviewed;
	}

	public void setApplicantsInterviewed(int applicantsInterviewed) {
		this.applicantsInterviewed = applicantsInterviewed;
	}

	public BigDecimal getCostadvirs() {
		return costadvirs;
	}

	public void setCostadvirs(BigDecimal costadvirs) {
		this.costadvirs = costadvirs;
	}

	public BigDecimal getCostrecri() {
		return costrecri;
	}

	public void setCostrecri(BigDecimal costrecri) {
		this.costrecri = costrecri;
	}

	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public Integer getPositionId() {
		return positionId;
	}

	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}

	public List<DtoRequisitions> getDeleteRequisitions() {
		return deleteRequisitions;
	}

	public void setDeleteRequisitions(List<DtoRequisitions> deleteRequisitions) {
		this.deleteRequisitions = deleteRequisitions;
	}

	public DtoRequisitions() {

	}

	public DtoRequisitions(Requisitions requisitions) {

	}

}
