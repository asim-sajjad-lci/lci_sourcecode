package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00106",indexes = {
        @Index(columnList = "EMPPSINDX")
})
public class EmployeePositionHistory extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPPSINDX")
	private int id;
	
	
	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;
	
	@JoinColumn(name = "EMPEFCDT")
	private Date positionEffectiveDate;
	
	@ManyToOne
	@JoinColumn(name = "DIVINDX")
	private Division division;
	
	@ManyToOne
	@JoinColumn(name = "DEPINDX")
	private Department department;
	
	@ManyToOne
	@JoinColumn(name = "POTINDX")
	private Position position;
	
	@ManyToOne
	@JoinColumn(name = "LOCINDX")
	private Location location;
	
	@ManyToOne
	@JoinColumn(name = "SUPRINDX")
	private Supervisor supervisor;
	
	@ManyToOne
	@JoinColumn(name = "EMPPSINDXD")
	private EmployeePositionReason employeePositionReason;
	
	@Column(name = "employeeType")
	private short employeeType;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public Date getPositionEffectiveDate() {
		return positionEffectiveDate;
	}

	public void setPositionEffectiveDate(Date positionEffectiveDate) {
		this.positionEffectiveDate = positionEffectiveDate;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}

	

	public EmployeePositionReason getEmployeePositionReason() {
		return employeePositionReason;
	}

	public void setEmployeePositionReason(EmployeePositionReason employeePositionReason) {
		this.employeePositionReason = employeePositionReason;
	}

	public short getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(short employeeType) {
		this.employeeType = employeeType;
	}

	
	
	
}
