/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoLocation;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceLocation;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerDivision
 * Name of Project: Hcm
 * Version: 0.0.1
 */

@RestController
@RequestMapping("/location")
public class ControllerLocation extends BaseController{
	
	private static final Logger LOGGER = Logger.getLogger(ControllerLocation.class);
	
	@Autowired
	ServiceLocation serviceLocation;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * 	"locationId": 2,
		"description":"Testing Location",
		"arabicDescription":"موقع الاختبار",
		"contactName":"Gaurav Sortekar",
		"locationAddress":"Ahmedabad",
		"city":"AHD",
		"country":"India",
		"phone":"(123) 123 1010",
		"fax":"(044) 420 4400"

	 * }
	 * @response{
	 * 	"code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":2,
	 * "description":"Testing Location",
	 * "arabicDescription":"موقع الاختبار",
	 * "contactName":"Gaurav Sortekar",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * 	"btiMessage":{
	 * 					"message":"Location has been created successfully",
	 * 					"messageShort":"LOCATION_CREATED"}
	 * }
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createLocation(HttpServletRequest request, @RequestBody DtoLocation dtoLocation) throws Exception {
		LOGGER.info("Create Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoLocation  = serviceLocation.saveOrUpdateLocation(dtoLocation);
			if (dtoLocation != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("LOCATION_CREATED", false), dtoLocation);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LOCATION_NOT_CREATED", false), dtoLocation);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Create Location Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "pageNumber":"0",
 		"pageSize":"10"
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"pageNumber":0,
	 * "pageSize":10,
	 * "totalCount":1,
	 * "records":[
	 * {"locationId":1,
	 * "description":"Test Location",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}
	 * ]
	 * },
	 * 
	 * 		"btiMessage":{"message":"All locations fetched successfully",
	 * 		"messageShort":"LOCATION_GET_ALL"}
	 * }
	 * @throws Exception
	 */
	
	
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAllLocation(HttpServletRequest request, @RequestBody DtoLocation dtoLocation) throws Exception {
		LOGGER.info("Get All Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceLocation.getAllLocation(dtoLocation);
			responseMessage=displayMessage(dtoSearch, MessageConstant.LOCATION_GET_ALL, MessageConstant.LOCATION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All Location Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * 	"id" : 2,
		"locationId": 2,
		"description":"Testing Location",
		"arabicDescription":"موقع الاختبار",	
		"contactName":"Gaurav",
		"locationAddress":"Surat",
		"city":"Surat",
		"country":"India",
   		"phone":"(123) 123 1010",
		"fax":"(044) 420 4400"
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"id":2,
	 * "locationId":2,
	 * "description":"Testing Location",
	 * "arabicDescription":"موقع الاختبار",
	 * "contactName":"Gaurav",
	 * "locationAddress":"Surat",
	 * "city":"Surat",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * "btiMessage":{
	 * 					"message":"Location updated successfully",
	 * 					"messageShort":"LOCATION_UPDATE_SUCCESS"}
	 * }
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateLocation(HttpServletRequest request, @RequestBody DtoLocation dtoLocation) throws Exception {
	LOGGER.info("Update Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			dtoLocation = serviceLocation.saveOrUpdateLocation(dtoLocation);
			responseMessage=displayMessage(dtoLocation, "LOCATION_UPDATE_SUCCESS", MessageConstant.LOCATION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Location Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "ids" : [2]
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":0,
	 * "deleteMessage":"Division deleted successfully",
	 * "associateMessage":"N/A",
	 * "deleteLocation":[
	 * {"id":2,
	 * "locationId":2,
	 * "description":"Testing Location",
	 * "contactName":"Gaurav",
	 * "locationAddress":"Surat",
	 * "city":"Surat",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}]},
	 * "btiMessage":{
	 * 					"message":"Location deleted successfully",
	 * 					"messageShort":"LOCATION_DELETED"}
	 * }
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteLocation(HttpServletRequest request, @RequestBody DtoLocation dtoLocation) throws Exception {
		LOGGER.info("Delete Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoLocation.getIds() != null && !dtoLocation.getIds().isEmpty()) {
				DtoLocation dtoLocation2 =serviceLocation.deleteLocation(dtoLocation.getIds());
				if (dtoLocation2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("LOCATION_DELETED", false), dtoLocation2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("LOCATION_NOT_DELETE_ID_MESSAGE", false), dtoLocation2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Location Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * 		"id" : 1,
			"pageNumber" : 0,
			"pageSize" : 10

	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":1,
	 * "description":"Test Location",
	 * "arabicDescription":"",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * "btiMessage":{
	 * 							"message":"Location detail fetched successfully",
	 * 							"messageShort":"LOCATION_GET_DETAIL"}
	 * }
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/getLocationById", method = RequestMethod.POST)
	public ResponseMessage getLocationById(HttpServletRequest request, @RequestBody DtoLocation dtoLocation) throws Exception {
		LOGGER.info("Get Location ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoLocation dtoLocationObj = serviceLocation.getLocationById(dtoLocation.getId());
			responseMessage=displayMessage(dtoLocationObj, "LOCATION_GET_DETAIL", "LOCATION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Location ById Method:"+dtoLocation.getId());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @request{
	 * 	"searchKeyword" : "AHD",
		"pageNumber":"0",
		"pageSize":"10"
	 * }
	 * @response{
	 * "code":200,
	 * "status":"OK",
	 * "result":{
	 * "searchKeyword":"AHD",
	 * "pageNumber":0,
	 * "pageSize":10,
	 * "totalCount":1,
	 * "records":[
	 * {"locationId":1,
	 * "description":"Test Location",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}
	 * ]
	 * },
	 * "btiMessage":{
	 * 					"message":"All locations fetched successfully",
	 * 					"messageShort":"LOCATION_GET_ALL"}
	 * }
	 * @throws Exception
	 */
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchLocation(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceLocation.searchLocation(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.LOCATION_GET_ALL, MessageConstant.LOCATION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Locations Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "locationId" : 1
	 * }
	 * @response{
	 * "code":302,
	 * "status":"FOUND",
	 * "result":{
	 * "locationId":1,
	 * "isRepeat":true},
	 * "btiMessage":
	 * {
	 *				 "message":"Location detail fetched successfully",
	 *				 "messageShort":"LOCATION_RESULT"}
	 * }
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/locationIdcheck", method = RequestMethod.POST)
	public ResponseMessage locationIdCheck(HttpServletRequest request, @RequestBody DtoLocation dtoLocation) throws Exception {
		LOGGER.info("locationIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			DtoLocation dtoLocationObj = serviceLocation.repeatByLocationId(dtoLocation.getLocationId());
			if (dtoLocationObj != null) {
				if (dtoLocationObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("LOCATION_EXIST", false), dtoLocationObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoLocationObj.getMessageType(), false),
							dtoLocationObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LOCATION_REPEAT_LOCATIONID_NOT_FOUND", false), dtoLocationObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchLocationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchLocationId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceLocation.searchLocationId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.LOCATION_GET_ALL, MessageConstant.LOCATION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Location Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllLocationDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllDivisionDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoLocation> locationList = serviceLocation.getAllLocationDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("LOCATION_GET_ALL", false), locationList);
			}else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/searchAllLocationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAllLocationId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("searchAllLocationId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceLocation.searchAllLocationId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.LOCATION_GET_ALL,MessageConstant.LOCATION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Location Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	
}
