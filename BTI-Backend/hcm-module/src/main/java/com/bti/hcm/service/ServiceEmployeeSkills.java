package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeeSkills;
import com.bti.hcm.model.EmployeeSkillsDesc;
import com.bti.hcm.model.SkillSetSetup;
import com.bti.hcm.model.SkillsSetup;
import com.bti.hcm.model.dto.DtoEmployeeMasterHcm;
import com.bti.hcm.model.dto.DtoEmployeeSkillDisplay;
import com.bti.hcm.model.dto.DtoEmployeeSkills;
import com.bti.hcm.model.dto.DtoEmployeeSkillsDesc;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSkillSetSteup;
import com.bti.hcm.model.dto.DtoSkillSteup;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeeSkills;
import com.bti.hcm.repository.RepositoryEmployeeSkillsDesc;
import com.bti.hcm.repository.RepositorySkillSetDesc;
import com.bti.hcm.repository.RepositorySkillSetSetup;
import com.bti.hcm.repository.RepositorySkillSteps;

@Service("serviceEmployeeSkills")
public class ServiceEmployeeSkills {

	
	static Logger log = Logger.getLogger(ServiceEmployeeSkills.class.getName());

	

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositorySkillSetDesc repositorySkillSetDesc;
	
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired(required=false)
	RepositoryEmployeeSkills repositoryEmployeeSkills;
	
	@Autowired(required=false)
	RepositoryEmployeeSkillsDesc repositoryEmployeeSkillsDesc;
	
	@Autowired(required=false)
	RepositorySkillSetSetup repositorySkillSetSetup;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired(required=false)
	RepositorySkillSteps repositorySkillSteps;
	
	
	public DtoEmployeeSkills saveOrUpdate(DtoEmployeeSkills dtoEmployeeSkills) {
		log.info("saveOrUpdate EmployeeSkills Method");
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			EmployeeSkills employeeSkills = null;
			if (dtoEmployeeSkills.getId() != null && dtoEmployeeSkills.getId() > 0) {
				
				employeeSkills = repositoryEmployeeSkills.findByIdAndIsDeleted(dtoEmployeeSkills.getId(), false);
				employeeSkills.setUpdatedBy(loggedInUserId);
				employeeSkills.setUpdatedDate(new Date());
				
				if(employeeSkills.getId()!=null ) {
					repositoryEmployeeSkillsDesc.deleteBySkillId(true,loggedInUserId,employeeSkills.getId());
				}
			} else {
				employeeSkills = new EmployeeSkills();
				employeeSkills.setCreatedDate(new Date());
			}
			SkillSetSetup skillSetSetup = null;
			EmployeeMaster employeeMaster = null;
			if(dtoEmployeeSkills.getSkillSetSetup()!=null) {
				skillSetSetup = repositorySkillSetSetup.findOne(dtoEmployeeSkills.getSkillSetSetup().getId());
			}
			
			if(dtoEmployeeSkills.getEmployeeMaster()!=null) {
				employeeMaster = repositoryEmployeeMaster.findOne(dtoEmployeeSkills.getEmployeeMaster().getEmployeeIndexId());
			}
			employeeSkills.setEmployeeMaster(employeeMaster);
			employeeSkills.setSkillSetSetup(skillSetSetup);
			
			employeeSkills = repositoryEmployeeSkills.saveAndFlush(employeeSkills);
			log.debug("Time EmployeeSkills is:"+employeeSkills.getId());
			
			int sequence = 1; 
			
			for (DtoEmployeeSkillsDesc dtoEmployeeSkillsDesc : dtoEmployeeSkills.getListSkills()) {
				
				
				
				
				EmployeeSkillsDesc employeeSkillsDesc = new EmployeeSkillsDesc();
				SkillsSetup skillsSetup = null;
				if(dtoEmployeeSkillsDesc.getSkillsSetup()!=null) {
					skillsSetup = repositorySkillSteps.findOne(dtoEmployeeSkillsDesc.getSkillsSetup().getId());
				}
				employeeSkillsDesc.setSkillsSetup(skillsSetup);
				employeeSkillsDesc.setEmployeeSkills(employeeSkills);
				employeeSkillsDesc.setSkillSetSequence(sequence);
				employeeSkillsDesc.setObtained(dtoEmployeeSkillsDesc.isObtained());
				employeeSkillsDesc.setSelected(dtoEmployeeSkillsDesc.isSelected());
				employeeSkillsDesc.setProficiency(dtoEmployeeSkillsDesc.getProficiency());
				employeeSkillsDesc.setRequired(dtoEmployeeSkillsDesc.isRequired());
				employeeSkillsDesc.setSkillComment(dtoEmployeeSkillsDesc.getSkillComment());
				employeeSkillsDesc.setSkillExpirationDate(dtoEmployeeSkillsDesc.getSkillExpirationDate());
				
				repositoryEmployeeSkillsDesc.saveAndFlush(employeeSkillsDesc);
				sequence++;
				
			}	
		}catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeSkills;
	
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchByEmployeeId(DtoSearch dtoSearch) {
		try {
			if(dtoSearch != null){
				dtoSearch = getDtoSerach(dtoSearch);
				
				List<EmployeeSkills> skillsList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						skillsList = this.repositoryEmployeeSkills.findByEmployeeId(dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						skillsList = this.repositoryEmployeeSkills.findByEmployeeId(dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						skillsList = this.repositoryEmployeeSkills.findByEmployeeId(dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
					}
					
				}
				dtoSearch.setTotalCount(this.repositoryEmployeeSkills.findByEmployeeIdCount(dtoSearch.getId()));
				
				if(skillsList != null && !skillsList.isEmpty()){
					List<DtoEmployeeSkills> dtoEmployeeSkillsList = new ArrayList<>();
					DtoEmployeeSkills dtoEmployeeSkills=null;
					for (EmployeeSkills employeeSkills : skillsList) {
						dtoEmployeeSkills = getDtoEmployeeSkillsDesc(employeeSkills);
						dtoEmployeeSkillsList.add(dtoEmployeeSkills);
					}
					dtoSearch.setRecords(dtoEmployeeSkillsList);
				}
			}
			
		}catch (Exception e) {
			log.error(e);
		}
	return dtoSearch;
  }
	
	public DtoEmployeeSkills getDtoEmployeeSkillsDesc(EmployeeSkills employeeSkills) {
		DtoEmployeeSkills dtoEmployeeSkills = new DtoEmployeeSkills(employeeSkills);
		try {
			dtoEmployeeSkills.setId(employeeSkills.getId());
			
			if(employeeSkills.getSkillSetSetup()!=null) {
				DtoSkillSetSteup dtoSkillSetSteup = new DtoSkillSetSteup();
				dtoSkillSetSteup.setId(employeeSkills.getSkillSetSetup().getId());
				dtoSkillSetSteup.setSkillSetId(employeeSkills.getSkillSetSetup().getSkillSetId());
				dtoSkillSetSteup.setSkillSetDiscription(employeeSkills.getSkillSetSetup().getSkillSetDiscription());
				dtoEmployeeSkills.setSkillSetSetup(dtoSkillSetSteup);
			}
			if(employeeSkills.getEmployeeMaster()!=null){
				DtoEmployeeMasterHcm dtoEmployeeMaster = new DtoEmployeeMasterHcm();
				dtoEmployeeMaster.setEmployeeIndexId(employeeSkills.getEmployeeMaster().getEmployeeIndexId());
				dtoEmployeeMaster.setEmployeeFirstName(employeeSkills.getEmployeeMaster().getEmployeeFirstName());
				dtoEmployeeMaster.setEmployeeLastName(employeeSkills.getEmployeeMaster().getEmployeeLastName());
				dtoEmployeeSkills.setEmployeeMaster(dtoEmployeeMaster);
			}
			
			List<DtoEmployeeSkillsDesc> list = new ArrayList<>();
			for (EmployeeSkillsDesc employeeSkillsDesc : employeeSkills.getListEmployeeSkillsDesc()) {
				DtoEmployeeSkillsDesc dtoEmployeeSkillsDesc = new DtoEmployeeSkillsDesc();
				if(employeeSkillsDesc.getSkillsSetup()!=null) {
					DtoSkillSteup skillsSetup = new DtoSkillSteup();
					skillsSetup.setId(employeeSkillsDesc.getSkillsSetup().getId());
					skillsSetup.setSkillId(employeeSkillsDesc.getSkillsSetup().getSkillId());
					skillsSetup.setSkillDesc(employeeSkillsDesc.getSkillsSetup().getSkillDesc());
					skillsSetup.setArabicDesc(employeeSkillsDesc.getSkillsSetup().getArabicDesc());
					dtoEmployeeSkillsDesc.setSkillsSetup(skillsSetup);
				}
				dtoEmployeeSkillsDesc.setId(employeeSkillsDesc.getId());
				dtoEmployeeSkillsDesc.setSkillSetSequence(employeeSkillsDesc.getSkillSetSequence());
				dtoEmployeeSkillsDesc.setObtained(employeeSkillsDesc.isObtained());
				dtoEmployeeSkillsDesc.setSelected(employeeSkillsDesc.isSelected());
				dtoEmployeeSkillsDesc.setProficiency(employeeSkillsDesc.getProficiency());
				dtoEmployeeSkillsDesc.setRequired(employeeSkillsDesc.isRequired());
				dtoEmployeeSkillsDesc.setSkillComment(employeeSkillsDesc.getSkillComment());
				dtoEmployeeSkillsDesc.setSkillExpirationDate(employeeSkillsDesc.getSkillExpirationDate());
				list.add(dtoEmployeeSkillsDesc);
			}
			dtoEmployeeSkills.setListSkills(list);
			
		}catch (Exception e) {
			log.error(e);
		}
			
		return dtoEmployeeSkills;
	}
	
	public DtoSearch getDtoSerach(DtoSearch dtoSearch) {
		try {String condition="";
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			if(dtoSearch.getSortOn().equals("id")
					) {
				condition = dtoSearch.getSortOn();
			
		}else {
			condition = "id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
		}
	}else{
		condition+="id";
		dtoSearch.setSortOn("");
		dtoSearch.setSortBy("");
		
	}
		dtoSearch.setSortOn(condition);
	}catch (Exception e) {
		log.error(e);
	}

				return dtoSearch;
}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAll(DtoSearch dtoSearch) {
		log.info("search EmployeeSkills Method");
		try {if(dtoSearch != null){
			dtoSearch = getDtoSerach(dtoSearch);
			
			List<EmployeeSkills> skillList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					skillList = this.repositoryEmployeeSkills.getAll(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					skillList = this.repositoryEmployeeSkills.getAll(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getSortOn()));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					skillList = this.repositoryEmployeeSkills.getAll(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getSortOn()));
				}
				
			}
			
			
			int count = 0;
			if(skillList != null && !skillList.isEmpty()){
				List<DtoEmployeeSkills> dtoEmployeeSkillsList = new ArrayList<>();
				DtoEmployeeSkills dtoEmployeeSkills=null;
				for (EmployeeSkills employeeSkills : skillList) {
					dtoEmployeeSkills = new DtoEmployeeSkills(employeeSkills);
					dtoEmployeeSkills.setId(employeeSkills.getId());
					
					if(employeeSkills.getSkillSetSetup()!=null) {
						DtoSkillSetSteup dtoSkillSetSteup = new DtoSkillSetSteup();
						dtoSkillSetSteup.setId(employeeSkills.getSkillSetSetup().getId());
						dtoSkillSetSteup.setSkillSetId(employeeSkills.getSkillSetSetup().getSkillSetId());
						dtoSkillSetSteup.setSkillSetDiscription(employeeSkills.getSkillSetSetup().getSkillSetDiscription());
						dtoEmployeeSkills.setSkillSetSetup(dtoSkillSetSteup);
					}
					if(employeeSkills.getEmployeeMaster()!=null){
						DtoEmployeeMasterHcm dtoEmployeeMaster = new DtoEmployeeMasterHcm();
						dtoEmployeeMaster.setEmployeeIndexId(employeeSkills.getEmployeeMaster().getEmployeeIndexId());
						dtoEmployeeMaster.setEmployeeFirstName(employeeSkills.getEmployeeMaster().getEmployeeFirstName());
						dtoEmployeeMaster.setEmployeeLastName(employeeSkills.getEmployeeMaster().getEmployeeLastName());
						dtoEmployeeMaster.setEmployeeId(employeeSkills.getEmployeeMaster().getEmployeeMiddleName());
						dtoEmployeeMaster.setEmployeeId(employeeSkills.getEmployeeMaster().getEmployeeId());
						dtoEmployeeSkills.setEmployeeMaster(dtoEmployeeMaster);
					}
					
					List<DtoEmployeeSkillsDesc> list = new ArrayList<>();
					for (EmployeeSkillsDesc employeeSkillsDesc : employeeSkills.getListEmployeeSkillsDesc()) {
						if(employeeSkillsDesc.getSkillsSetup()!=null) {
							if(!employeeSkillsDesc.getSkillsSetup().getIsDeleted() && employeeSkillsDesc.getSkillsSetup()!=null) {
								DtoEmployeeSkillsDesc dtoEmployeeSkillsDesc = new DtoEmployeeSkillsDesc();
								
									DtoSkillSteup skillsSetup = new DtoSkillSteup();
									skillsSetup.setId(employeeSkillsDesc.getSkillsSetup().getId());
									skillsSetup.setSkillId(employeeSkillsDesc.getSkillsSetup().getSkillId());
									skillsSetup.setSkillDesc(employeeSkillsDesc.getSkillsSetup().getSkillDesc());
									skillsSetup.setArabicDesc(employeeSkillsDesc.getSkillsSetup().getArabicDesc());
									dtoEmployeeSkillsDesc.setSkillsSetup(skillsSetup);
								
								dtoEmployeeSkillsDesc.setId(employeeSkillsDesc.getId());
								dtoEmployeeSkillsDesc.setSkillSetSequence(employeeSkillsDesc.getSkillSetSequence());
								dtoEmployeeSkillsDesc.setObtained(employeeSkillsDesc.isObtained());
								dtoEmployeeSkillsDesc.setSelected(employeeSkillsDesc.isSelected());
								dtoEmployeeSkillsDesc.setProficiency(employeeSkillsDesc.getProficiency());
								dtoEmployeeSkillsDesc.setRequired(employeeSkillsDesc.isRequired());
								dtoEmployeeSkillsDesc.setSkillComment(employeeSkillsDesc.getSkillComment());
								
								dtoEmployeeSkillsDesc.setSkillExpirationDate(employeeSkillsDesc.getSkillExpirationDate());
								
								list.add(dtoEmployeeSkillsDesc);
							}
						}
						
						
					}
					count = count+list.size();
					dtoEmployeeSkills.setListSkills(list);
					dtoEmployeeSkillsList.add(dtoEmployeeSkills);
				}
				
				
				dtoSearch.setTotalCount(this.repositoryEmployeeSkillsDesc.getList().size());
				dtoSearch.setRecords(dtoEmployeeSkillsList);
			}
		}
		log.debug("Search EmployeeSkills Size is:"+dtoSearch.getTotalCount());
	}catch (Exception e) {
		log.error(e);
	}
		
			return dtoSearch;
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch listSkills(DtoSearch dtoSearchEmployee) {
		log.info("search EmployeeSkills Method");
		try {
			if(dtoSearchEmployee != null){
				String searchWord=dtoSearchEmployee.getSearchKeyword();
				String condition="";
				
				if(dtoSearchEmployee.getSortOn()!=null || dtoSearchEmployee.getSortBy()!=null){
					if(dtoSearchEmployee.getSortOn().equals("id")
							) {
						condition = dtoSearchEmployee.getSortOn();
					
				}else {
					condition = "id";
				}
			}else{
				condition+="id";
			}
			
				
				List<EmployeeSkillsDesc> employeeSkillsList =null;
				dtoSearchEmployee.setTotalCount(this.repositoryEmployeeSkillsDesc.getAllListCount("%"+searchWord+"%"));
				if(dtoSearchEmployee.getPageNumber()!=null && dtoSearchEmployee.getPageSize()!=null){
					
					if(dtoSearchEmployee.getSortBy().equals("") && dtoSearchEmployee.getSortOn().equals("")){
						employeeSkillsList = this.repositoryEmployeeSkillsDesc.getAllList("%"+searchWord+"%",(new PageRequest(dtoSearchEmployee.getPageNumber(), dtoSearchEmployee.getPageSize(), Sort.Direction.DESC, "id")));
					}
					if(dtoSearchEmployee.getSortBy().equals("ASC")){
						employeeSkillsList = this.repositoryEmployeeSkillsDesc.getAllList("%"+searchWord+"%",(new PageRequest(dtoSearchEmployee.getPageNumber(), dtoSearchEmployee.getPageSize(), Sort.Direction.ASC, condition)));
					}else if(dtoSearchEmployee.getSortBy().equals("DESC")){
						employeeSkillsList = this.repositoryEmployeeSkillsDesc.getAllList("%"+searchWord+"%",(new PageRequest(dtoSearchEmployee.getPageNumber(), dtoSearchEmployee.getPageSize(), Sort.Direction.DESC, condition)));
					}
					
				}
				
				List<DtoEmployeeSkillDisplay> list = new ArrayList<>();
				if(employeeSkillsList != null && !employeeSkillsList.isEmpty()){
					DtoEmployeeSkillDisplay dtoEmployeeSkillDisplay=null;
					for (EmployeeSkillsDesc employeeSkillsDesc : employeeSkillsList) {
						dtoEmployeeSkillDisplay = new DtoEmployeeSkillDisplay();
						dtoEmployeeSkillDisplay.setEmployeeSkillId(employeeSkillsDesc.getId());
						if(employeeSkillsDesc.getSkillsSetup()!=null) {
							dtoEmployeeSkillDisplay.setSkillPrimaryId(employeeSkillsDesc.getSkillsSetup().getId());
							dtoEmployeeSkillDisplay.setSkillId(employeeSkillsDesc.getSkillsSetup().getSkillId());
							dtoEmployeeSkillDisplay.setSkillDesc(employeeSkillsDesc.getSkillsSetup().getSkillDesc());
						}
						dtoEmployeeSkillDisplay.setObtain(employeeSkillsDesc.isObtained());
						dtoEmployeeSkillDisplay.setProficiency(employeeSkillsDesc.getProficiency());
						dtoEmployeeSkillDisplay.setRequire(employeeSkillsDesc.isRequired());
						dtoEmployeeSkillDisplay.setComment(employeeSkillsDesc.getSkillComment());
						
						dtoEmployeeSkillDisplay.setDate(employeeSkillsDesc.getSkillExpirationDate());
						
						if(employeeSkillsDesc.getEmployeeSkills().getEmployeeMaster()!=null){
							dtoEmployeeSkillDisplay.setEmployeePrimaryId(employeeSkillsDesc.getEmployeeSkills().getEmployeeMaster().getEmployeeIndexId());
							dtoEmployeeSkillDisplay.setEmployeeId(employeeSkillsDesc.getEmployeeSkills().getEmployeeMaster().getEmployeeId());
							dtoEmployeeSkillDisplay.setId(employeeSkillsDesc.getEmployeeSkills().getId());
						}
						
						if(employeeSkillsDesc.getEmployeeSkills().getSkillSetSetup()!=null) {
							dtoEmployeeSkillDisplay.setSkillSetPrimaryId(employeeSkillsDesc.getEmployeeSkills().getSkillSetSetup().getId());
							dtoEmployeeSkillDisplay.setSkillSetId(employeeSkillsDesc.getEmployeeSkills().getSkillSetSetup().getSkillSetId());
						
						}
						list.add(dtoEmployeeSkillDisplay);
					}
				}
				dtoSearchEmployee.setRecords(list);
				dtoSearchEmployee.setTotalCount(this.repositoryEmployeeSkillsDesc.getList().size());
			}
			log.debug("Search EmployeeSkills Size is:"+dtoSearchEmployee.getTotalCount());
		
		}catch (Exception e) {
			log.error(e);
		}
		
			return dtoSearchEmployee;
	}
	
	
	public DtoEmployeeSkillsDesc delete(List<Integer> ids) {
		log.info("delete EmployeeSkillsDesc Method");
		DtoEmployeeSkillsDesc dtoEmployeeSkillsDesc = new DtoEmployeeSkillsDesc();
		try {
			
			dtoEmployeeSkillsDesc
					.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_DELETED", false));
			dtoEmployeeSkillsDesc.setAssociateMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_ASSOCIATED", false));
			List<DtoEmployeeSkillsDesc> deleteEmployeeSkillsDesc = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
			
				for (Integer skillDescId : ids) {
					EmployeeSkillsDesc employeeSkills = repositoryEmployeeSkillsDesc.findOne(skillDescId);
					if(employeeSkills!=null) {
						DtoEmployeeSkillsDesc desc = new DtoEmployeeSkillsDesc();
						desc.setId(employeeSkills.getId());
							repositoryEmployeeSkillsDesc.deleteSingleEmployeeSkillsDesc(true, loggedInUserId,skillDescId);
							deleteEmployeeSkillsDesc.add(desc);
							
					}else {
						inValidDelete = true;
					}
					
				}
				if(inValidDelete){
					dtoEmployeeSkillsDesc.setMessageType(invlidDeleteMessage.toString());
					
				}
				if(!inValidDelete){
					dtoEmployeeSkillsDesc.setMessageType("");
					
				}
				
				dtoEmployeeSkillsDesc.setDelete(deleteEmployeeSkillsDesc);
			
			log.debug("Delete EmployeeSkillsDesc :"+dtoEmployeeSkillsDesc.getId());
			
		}catch (Exception e) {
			log.error(e);
		}
		
		return dtoEmployeeSkillsDesc;
	}
	
	public DtoEmployeeSkills getById(int id) {
		DtoEmployeeSkills dtoEmployeeSkills  = new DtoEmployeeSkills();
		try {
			if (id > 0) {
				EmployeeSkills employeeSkills = repositoryEmployeeSkills.findByIdAndIsDeleted(id, false);
				if (dtoEmployeeSkills != null) {
					dtoEmployeeSkills = getDtoEmployeeSkillsDesc(employeeSkills);
					SkillSetSetup skillSetSetupList = repositorySkillSetSetup.findByIdAndIsDeleted(employeeSkills.getSkillSetSetup().getId(), false);
					
					List<DtoEmployeeSkillsDesc> listDesc = new ArrayList<>();
					if(skillSetSetupList!=null) {
							List<Integer>	ids = repositorySkillSetDesc.getDescId(skillSetSetupList.getId());
							
							if(ids!=null && !ids.isEmpty()) {
							List<SkillsSetup> list1 =	repositorySkillSteps.findByIds(ids);
							
							for (SkillsSetup skillsSetup : list1) {
								DtoEmployeeSkillsDesc dtoEmployeeSkillsDesc = new DtoEmployeeSkillsDesc();
									DtoSkillSteup dtoSkillSteup = new DtoSkillSteup();
									dtoSkillSteup.setId(skillsSetup.getId());
									dtoSkillSteup.setSkillId(skillsSetup.getSkillId());
									dtoSkillSteup.setSkillDesc(skillsSetup.getSkillDesc());
									dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
									dtoSkillSteup.setFrequency(skillsSetup.getFrequency());
									dtoSkillSteup.setCompensation(skillsSetup.getCompensation());
									dtoEmployeeSkillsDesc.setSkillsSetup(dtoSkillSteup);
									listDesc.add(dtoEmployeeSkillsDesc);
									
								}
							Iterator<DtoEmployeeSkillsDesc> iterator = listDesc.iterator();
							while(iterator.hasNext()){
								DtoEmployeeSkillsDesc desc = iterator.next();
								if(desc.getSkillsSetup()!=null) {
									for (DtoEmployeeSkillsDesc skillsSetup : dtoEmployeeSkills.getListSkills()) {
										if(skillsSetup.getSkillsSetup()!=null && skillsSetup.getSkillsSetup().getId()>0 && skillsSetup.getSkillsSetup().getId().equals(desc.getSkillsSetup().getId())) {
												iterator.remove();
										}
										
									}
								}
								
							}
							dtoEmployeeSkills.getListSkills().addAll(listDesc);
							
						
							}
						
							
					}
					
				} 
			} else {
				dtoEmployeeSkills.setMessageType("INVALID_ID");

			}
	
		}catch (Exception e) {
			log.error(e);
		}
		
				return dtoEmployeeSkills;
	}

	@Transactional
	public DtoEmployeeSkills getByEmployeeId(DtoEmployeeSkills dtoEmployeeSkills) {
		
		EmployeeSkills employeeSkills=repositoryEmployeeSkills.findAllByEmployeeId(dtoEmployeeSkills.getEmployeeId());
		if(employeeSkills!=null) {
			dtoEmployeeSkills.setId(employeeSkills.getId());
			DtoEmployeeMasterHcm dtoEmployeeMasterHcm=new DtoEmployeeMasterHcm();
			DtoSkillSetSteup dtoSkillSetSteup = new DtoSkillSetSteup();
			if(employeeSkills.getSkillSetSetup()!=null) {
				dtoSkillSetSteup.setId(employeeSkills.getSkillSetSetup().getId());
				dtoSkillSetSteup.setSkillSetId(employeeSkills.getSkillSetSetup().getSkillSetId());
				dtoSkillSetSteup.setSkillSetDiscription(employeeSkills.getSkillSetSetup().getSkillSetDiscription());
			}
			
			if(employeeSkills.getEmployeeMaster()!=null) {
				dtoEmployeeMasterHcm.setEmployeeIndexId(employeeSkills.getEmployeeMaster().getEmployeeIndexId());
				dtoEmployeeMasterHcm.setEmployeeId(employeeSkills.getEmployeeMaster().getEmployeeId());
				dtoEmployeeMasterHcm.setEmployeeFirstName(employeeSkills.getEmployeeMaster().getEmployeeFirstName());
				dtoEmployeeMasterHcm.setEmployeeMiddleName(employeeSkills.getEmployeeMaster().getEmployeeMiddleName());
				dtoEmployeeMasterHcm.setEmployeeLastName(employeeSkills.getEmployeeMaster().getEmployeeLastName());
				
			}
			List<DtoEmployeeSkillsDesc> list = new ArrayList<>();
			for (EmployeeSkillsDesc employeeSkillsDesc : employeeSkills.getListEmployeeSkillsDesc()) {
				DtoEmployeeSkillsDesc dtoEmployeeSkillsDesc = new DtoEmployeeSkillsDesc();
				if(employeeSkillsDesc.getSkillsSetup()!=null) {
					DtoSkillSteup skillsSetup = new DtoSkillSteup();
					skillsSetup.setId(employeeSkillsDesc.getSkillsSetup().getId());
					skillsSetup.setSkillId(employeeSkillsDesc.getSkillsSetup().getSkillId());
					skillsSetup.setSkillDesc(employeeSkillsDesc.getSkillsSetup().getSkillDesc());
					skillsSetup.setArabicDesc(employeeSkillsDesc.getSkillsSetup().getArabicDesc());
					dtoEmployeeSkillsDesc.setSkillsSetup(skillsSetup);
				}
				dtoEmployeeSkillsDesc.setId(employeeSkillsDesc.getId());
				dtoEmployeeSkillsDesc.setSkillSetSequence(employeeSkillsDesc.getSkillSetSequence());
				dtoEmployeeSkillsDesc.setObtained(employeeSkillsDesc.isObtained());
				dtoEmployeeSkillsDesc.setSelected(employeeSkillsDesc.isSelected());
				dtoEmployeeSkillsDesc.setProficiency(employeeSkillsDesc.getProficiency());
				dtoEmployeeSkillsDesc.setRequired(employeeSkillsDesc.isRequired());
				dtoEmployeeSkillsDesc.setSkillComment(employeeSkillsDesc.getSkillComment());
				dtoEmployeeSkillsDesc.setSkillExpirationDate(employeeSkillsDesc.getSkillExpirationDate());
				list.add(dtoEmployeeSkillsDesc);
			}
			dtoEmployeeSkills.setRecords(list);
			dtoEmployeeSkills.setDtoEmployeeMasterHcm(dtoEmployeeMasterHcm);
			dtoEmployeeSkills.setSkillSetSetup(dtoSkillSetSteup);
		}
		
		
		
		return dtoEmployeeSkills;
	}

	
	public DtoEmployeeSkills helthInsurancecheck(DtoEmployeeSkills dtoEmployeeSkills) {

		List<EmployeeSkills>employeeSkills=repositoryEmployeeSkills.findAllByEmployeeIdandSkillId(dtoEmployeeSkills.getEmployeeId(),dtoEmployeeSkills.getSkillId());
		if(!employeeSkills.isEmpty()) {
			dtoEmployeeSkills.setIsRepeat(true);
		}else
			dtoEmployeeSkills.setIsRepeat(false);
		return dtoEmployeeSkills;
	}
	
	
}
