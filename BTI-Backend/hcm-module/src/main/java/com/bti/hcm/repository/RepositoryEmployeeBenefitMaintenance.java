
package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeePayCodeMaintenance;

@Repository("/repositoryEmployeeBenefitMaintenance")
public interface RepositoryEmployeeBenefitMaintenance extends JpaRepository<EmployeeBenefitMaintenance, Integer>{

	EmployeeBenefitMaintenance findByIdAndIsDeleted(Integer id, boolean b);

	EmployeeBenefitMaintenance saveAndFlush(EmployeeBenefitMaintenance employeeBenefitMaintenance);

	EmployeeBenefitMaintenance findOne(Integer planId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeBenefitMaintenance e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.id =:id ")
	public void deleteSingleEmployeeBenefitMaintenance(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	@Query("select e from EmployeeBenefitMaintenance e where e.isDeleted=false")
	List<EmployeeBenefitMaintenance> findByIsDeleted(Pageable pageable);
	
	//@Cacheable("employeeBenefitMaintenance")
	@Query("select e from EmployeeBenefitMaintenance e where e.employeeMaster.employeeIndexId =:id and e.isDeleted=false")
	List<EmployeeBenefitMaintenance> findByEmployeeId(@Param("id") Integer id);
	
	@Query("select e from EmployeeBenefitMaintenance e where e.employeeMaster.employeeIndexId =:id and e.isDeleted=false and (DATE(e.startDate) <= :startDate and DATE(e.endDate) >= :endDate)")
	List<EmployeeBenefitMaintenance> findByEmployeeId1(@Param("id") Integer id,@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	

	@Query("select e from EmployeeBenefitMaintenance e where  e.isDeleted=false and (DATE(e.startDate) <= :startDate and DATE(e.endDate) >= :endDate)")
	List<EmployeeBenefitMaintenance> findByEmployeeId2(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	/*//replica
	@Query("select e from EmployeeBenefitMaintenance e where (e.id between e.startDate and e.endDate) and e.employeeMaster.employeeIndexId =:id and e.isDeleted=false")
	List<EmployeeBenefitMaintenance> findByEmployeeIdHM(@Param("id") Integer id);*/

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeBenefitMaintenance e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.employeeMaster.employeeIndexId =:id ")
	void deleteByEmployeeId(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,@Param("id") Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeBenefitMaintenance e set e.startDate =:startDate ,e.endDate =:endDate,e.inactive=:inactive where e.id =:id ")
	public void deleteSingleEmployeeBenefitMaintenance1(@Param("startDate") Date startDate, @Param("endDate") Date endDate,@Param("inactive")boolean inactive,
			@Param("id") Integer id);

	@Query("select count(*) from EmployeeBenefitMaintenance e ")
	public Integer getCountOfTotalEmployeeBenefitMaintenance();

	@Query("select count(*) from EmployeeBenefitMaintenance e where (e.employeeMaster.employeeId LIKE :searchKeyWord or e.benefitCode.benefitId LIKE :searchKeyWord) and e.isDeleted=false")
	public Integer predictiveEmployeeBenefitMaintenanceSearchTotalCount(@Param("searchKeyWord")  String searchKeyWord);

	@Query("select e from EmployeeBenefitMaintenance e where (e.employeeMaster.employeeId LIKE :searchKeyWord or e.benefitCode.benefitId  LIKE :searchKeyWord) and e.isDeleted=false")
	public List<EmployeeBenefitMaintenance> predictiveEmployeeBenefitMaintenanceSearchWithPagination(@Param("searchKeyWord")String string,Pageable pageable);

	List<EmployeeBenefitMaintenance> findByIsDeletedAndInactive(boolean b, boolean c);
	
	@Query("select m.benefitCode from EmployeeBenefitMaintenance m where  (DATE(m.startDate) <= :startDate and DATE(m.endDate) >= :endDate) and m.inactive=false and m.isDeleted=false")
	List<BenefitCode> findByStartEndDate(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	

	
	EmployeeBenefitMaintenance findByEmployeeMasterEmployeeIndexIdAndBenefitCodeId(int empId, int benId);
	
	@Query("select e from EmployeeBenefitMaintenance e where e.employeeMaster.employeeIndexId =:employeeId and e.benefitCode.id =:benefitCode and e.isDeleted=false")
	List<EmployeeBenefitMaintenance> validate(@Param("employeeId")Integer employeeId,@Param("benefitCode")Integer benefitCode);
	
	
	@Query("select m from EmployeeBenefitMaintenance m where m.benefitCode.id  =:benefitId and m.isDeleted=false")
	public List<EmployeeBenefitMaintenance> findByBenefitId(@Param("benefitId") Integer benefitId);
	
	@Query("select m.benefitCode from EmployeeBenefitMaintenance m where  m.inactive=false and m.isDeleted=false")
	List<BenefitCode> findByBenefitId1();
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeBenefitMaintenance e set e.inactive=:inactive where e.id =:id ")
	public void deleteSingleEmployeeBenefitMaintenance2(@Param("inactive")boolean inactive,
			@Param("id") Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeBenefitMaintenance e set e.noOfDays=:noOfDays where e.id =:id ")
	public void deleteSingleEmployeeBenefitMaintenance3(@Param("noOfDays")Integer noOfDays,
			@Param("id") Integer id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeBenefitMaintenance e set e.endDateDays=:endDateDays where e.id =:id ")
	public void deleteSingleEmployeeBenefitMaintenance4(@Param("endDateDays")Integer endDateDays,
			@Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeBenefitMaintenance e set e.transactionRequired=:transactionRequired where e.id =:id ")
	public void deleteSingleEmployeeBenefitMaintenance5(@Param("transactionRequired")Boolean transactionRequired,
			@Param("id") Integer id);
	
	
	@Query("select m.benefitCode from EmployeeBenefitMaintenance m where  (DATE(m.startDate) >= :startDate or DATE(m.endDate) >= :endDate) and m.inactive=false and m.isDeleted=false")
	List<BenefitCode> findByStartEndDate1(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	@Query("select e from EmployeeBenefitMaintenance e where e.employeeMaster.employeeIndexId =:id and e.isDeleted=false and (DATE(e.startDate) >= :startDate or DATE(e.endDate) >= :endDate)")
	List<EmployeeBenefitMaintenance> findByEmployeeId3(@Param("id") Integer id,@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	@Query("select d from EmployeeBenefitMaintenance d where d.benefitCode.id=:id and d.startDate <= d.endDate and (d.endDate >= :endDate or d.endDate between :startDate and :endDate) and d.startDate < :endDate and d.isDeleted=false")
	List<EmployeeBenefitMaintenance> findByStartDateAfterAndEndDateBeforeAndBenefitId(@Param("id")Integer id,@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
	
	//for ESB Calc in Loan API.getEmpDetails 
	@Query("select m from EmployeeBenefitMaintenance m where m.employeeMaster.employeeIndexId =:id and m.isDeleted=false and m.inactive=false and m.benefitCode.id IN :benCodeIds "
			+ " and :datez between m.startDate and m.endDate ")
	List<EmployeeBenefitMaintenance> findByEmpIdAndIsDeletedAndIsInactiveAndBenefitCodeIdsAndBetweenDates(@Param("id") int id,
			@Param("benCodeIds") List<Integer> benCodeIds, @Param("datez") Date datez);
	
	
	
}
