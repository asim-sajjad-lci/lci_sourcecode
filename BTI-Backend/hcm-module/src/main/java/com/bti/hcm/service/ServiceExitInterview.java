package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.ExitInterview;
import com.bti.hcm.model.dto.DtoExitInterview;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAccrualSchedule;
import com.bti.hcm.repository.RepositoryExitInterview;
import com.bti.hcm.repository.RepositoryPayCode;

@Service("serviceExitInterview")
public class ServiceExitInterview {

	/**
	 * @Description LOGGER use for put a logger in ExitInterview Service
	 */
	static Logger log = Logger.getLogger(ServiceExitInterview.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in ExitInterview service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in ExitInterview service
	 */
	@Autowired
	ServiceResponse response;
	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in ExitInterview service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	
	/**
	 * @Description repositoryExitInterview Autowired here using annotation of spring for access of repositoryExitInterview method in ExitInterview service
	 */
	@Autowired(required=false)
	RepositoryExitInterview repositoryExitInterview;
	
	/**
	 * @Description RepositoryPayCode Autowired here using annotation of spring for access of RepositoryTimeCode method in ExitInterview service
	 */
	@Autowired(required=false)
	RepositoryPayCode repositoryPayCode;
	
	@Autowired(required=false)
	RepositoryAccrualSchedule repositoryAccrualSchedule;
	
	/**
	 * @param dtoExitInterview
	 * @return
	 */
	
	public DtoExitInterview saveOrUpdate(DtoExitInterview dtoExitInterview) {
		try {

			log.info("saveOrUpdate ExitInterview Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			ExitInterview exitInterview = null;
			if (dtoExitInterview.getId() != null && dtoExitInterview.getId() > 0) {
				
				exitInterview = repositoryExitInterview.findByIdAndIsDeleted(dtoExitInterview.getId(), false);
				exitInterview.setUpdatedBy(loggedInUserId);
				exitInterview.setUpdatedDate(new Date());
			} else {
				exitInterview = new ExitInterview();
				exitInterview.setCreatedDate(new Date());
				Integer rowId = repositoryExitInterview.getCountOfTotalExitInterview();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					
					
					increment=1;
					
					
				}
				exitInterview.setRowId(increment);
			}
			
			List<ExitInterview> exitInterview1=repositoryExitInterview.findByExitInterviewId(dtoExitInterview.getExitInterviewId());
			
			exitInterview.setSequence(exitInterview1.size()+1);
			exitInterview.setExitInterviewId(dtoExitInterview.getExitInterviewId());
			exitInterview.setArbicDesc(dtoExitInterview.getArbicDesc());
			exitInterview.setDesc(dtoExitInterview.getDesc());
			exitInterview.setInActive(dtoExitInterview.isInActive());
			exitInterview = repositoryExitInterview.saveAndFlush(exitInterview);
			log.debug("Time ExitInterview is:"+exitInterview.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoExitInterview;
	
	}
	
	/**
	 * @param ids
	 * @return
	 */
	public DtoExitInterview delete(List<Integer> ids) {
		log.info("delete ExitInterview Method");
		DtoExitInterview dtoExitInterview = new DtoExitInterview();
		dtoExitInterview
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EXIT_INTERVIEW_DELETED", false));
		dtoExitInterview.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("EXIT_INTERVIEW_ASSOCIATED", false));
		List<DtoExitInterview> deleteExitInterview = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("EXIT_INTERVIEW_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer exitInterview : ids) {
				ExitInterview payCode = repositoryExitInterview.findOne(exitInterview);
				if(payCode!=null) {
					DtoExitInterview dtoTimeCode1 = new DtoExitInterview();
					dtoTimeCode1.setId(payCode.getId());
					repositoryExitInterview.deleteSingleExitInterview(true, loggedInUserId, exitInterview);
					deleteExitInterview.add(dtoTimeCode1);
	
				}else {
					inValidDelete = true;
				}
				
			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoExitInterview.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoExitInterview.setMessageType("");
				
			}
			
			dtoExitInterview.setDelete(deleteExitInterview);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete ExitInterview :"+dtoExitInterview.getId());
		return dtoExitInterview;
	}
	
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {

			log.info("search ExitInterview Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("exitInterviewId") || dtoSearch.getSortOn().equals("desc") || dtoSearch.getSortOn().equals("arbicDesc")
							) {
						condition = dtoSearch.getSortOn();
					
				}else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
				dtoSearch.setTotalCount(this.repositoryExitInterview.predictiveExitInterviewSearchTotalCount("%"+searchWord+"%"));
				List<ExitInterview> exitInterviewList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						exitInterviewList = this.repositoryExitInterview.predictiveExitInterviewSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						exitInterviewList = this.repositoryExitInterview.predictiveExitInterviewSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						exitInterviewList = this.repositoryExitInterview.predictiveExitInterviewSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				if(exitInterviewList != null && !exitInterviewList.isEmpty()){
					List<DtoExitInterview> dtoExitInterviewList = new ArrayList<>();
					DtoExitInterview dtoExitInterview1=null;
					for (ExitInterview exitInterview : exitInterviewList) {
						dtoExitInterview1 = new DtoExitInterview(exitInterview);
						dtoExitInterview1.setId(exitInterview.getId());
						dtoExitInterview1.setSequence(exitInterview.getSequence());
						dtoExitInterview1.setExitInterviewId(exitInterview.getExitInterviewId());
						dtoExitInterview1.setArbicDesc(exitInterview.getArbicDesc());
						dtoExitInterview1.setDesc(exitInterview.getDesc());
						dtoExitInterview1.setInActive(exitInterview.isInActive());
						dtoExitInterviewList.add(dtoExitInterview1);
					}
					dtoSearch.setRecords(dtoExitInterviewList);
				}
			}
			log.debug("Search ExitInterview Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	public DtoExitInterview getById(int id) {
		log.info("getById Method");
		DtoExitInterview dtoExitInterview = new DtoExitInterview();
		try {
			if (id > 0) {
				ExitInterview exitInterview = repositoryExitInterview.findByIdAndIsDeleted(id, false);
				if (exitInterview != null) {
					dtoExitInterview = new DtoExitInterview(exitInterview);
					dtoExitInterview.setExitInterviewId(exitInterview.getExitInterviewId());
					dtoExitInterview.setArbicDesc(exitInterview.getArbicDesc());
					dtoExitInterview.setSequence(exitInterview.getSequence());
					dtoExitInterview.setDesc(exitInterview.getDesc());
					dtoExitInterview.setInActive(exitInterview.isInActive());
					dtoExitInterview.setId(exitInterview.getId());
				} else {
					dtoExitInterview.setMessageType("TIME_CODE_NOT_GETTING");

				}
			} else {
				dtoExitInterview.setMessageType("INVALID_TIME_CODE_ID");

			}
			log.debug("ExitInterview By Id is:"+dtoExitInterview.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoExitInterview;
	}
	
	/**
	 * @param timeCodeId
	 * @return
	 */
	public DtoExitInterview repeatByExitInterviewId(String timeCodeId) {
		log.info("repeatByExitInterviewId Method");
		DtoExitInterview dtoPayCode = new DtoExitInterview();
		try {
			List<ExitInterview> timeCode=repositoryExitInterview.findByExitInterviewId(timeCodeId);
			if(timeCode!=null && !timeCode.isEmpty()) {
				dtoPayCode.setIsRepeat(true);
			}else {
				dtoPayCode.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPayCode;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchExitInterviewId(DtoSearch dtoSearch) {
		try {
			log.info("searchExitInterviewId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				
				List<ExitInterview> exitInterviewList = this.repositoryExitInterview.predictiveExitInterviewSearchWithPagination("%"+searchWord+"%");
					if(!exitInterviewList.isEmpty()) {
						dtoSearch.setTotalCount(exitInterviewList.size());
						dtoSearch.setRecords(exitInterviewList.size());
					}
				dtoSearch.setRecords(exitInterviewList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public List<DtoExitInterview> getAllExitInterviewInActiveList() {
		log.info("getAllExitInterview InActive List  Method");
		List<DtoExitInterview> dtoExitInterviewList = new ArrayList<>();
		try {
			List<ExitInterview> list = repositoryExitInterview.findByIsDeletedAndInActive(false, false);
			
			if (list != null && !list.isEmpty()) {
				for (ExitInterview exitInterview : list) {
					DtoExitInterview dtoExitInterview = new DtoExitInterview();
					dtoExitInterview.setId(exitInterview.getId());
					dtoExitInterview.setDesc(exitInterview.getDesc());
					dtoExitInterview.setArbicDesc(exitInterview.getArbicDesc());
					dtoExitInterview.setInActive(exitInterview.isInActive());
					dtoExitInterviewList.add(dtoExitInterview);
				}
			}
			log.debug("Position is:" +dtoExitInterviewList.size());
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoExitInterviewList;
	}
}
