package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.DeductionCode;

@Repository("repositoryDeductionCode")
public interface RepositoryDeductionCode extends JpaRepository<DeductionCode, Integer>{

	DeductionCode findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from DeductionCode d where d.isDeleted=false")
	Integer getCountOfTotalDeductionCode();

	List<DeductionCode> findByIsDeleted(boolean b, Pageable pageable);

	List<DeductionCode> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select count(*) from DeductionCode d where (d.diductionId like :searchKeyWord  or d.discription like :searchKeyWord or d.arbicDiscription like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveDeductionCodeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from DeductionCode d where (d.diductionId like :searchKeyWord  or d.discription like :searchKeyWord or d.arbicDiscription like :searchKeyWord) and d.isDeleted=false")
	List<DeductionCode> predictiveDeductionCodeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select d from DeductionCode d where (d.diductionId like :searchKeyWord  or d.discription like :searchKeyWord or d.arbicDiscription like :searchKeyWord) and d.isDeleted=false")
	List<DeductionCode> predictiveDeductionCodeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update DeductionCode d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleDeductionCode(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select p from DeductionCode p where (p.diductionId =:departmentId) and p.isDeleted=false")
	List<DeductionCode> findByDeductionCodeId(@Param("departmentId")String departmentId);
	
	public List<DeductionCode> findByIsDeleted(Boolean deleted);
	
	public List<DeductionCode> findByIsDeletedAndInActive(Boolean deleted, Boolean inActive);
	
	//replica
	@Query("select m from DeductionCode m where (m.startDate >= :startDate and m.endDate <= :endDate) and m.inActive=false and m.isDeleted=false")
	List<DeductionCode> findByStartEndDate(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	
	@Query("select m from DeductionCode m where (m.startDate <= :startDate and m.endDate >= :endDate) and m.inActive=false and m.isDeleted=false")
	List<DeductionCode> findByStartEndDate2(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	public DeductionCode findByIsDeletedAndInActiveAndId(Boolean deleted, Boolean inActive,Integer id);

	
	@Query("select d from DeductionCode d where (d.diductionId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<DeductionCode> predictiveSearchDiductionIdWithPagination(@Param("searchKeyWord")String diductionId);

	@Query("select count(*) from DeductionCode d ")
	Integer getCountOfTotalDeductionCodes();
	
	@Query("select d from DeductionCode d where (d.id = :id) and d.isDeleted=false")
	List<DeductionCode> findByIdAndIsDeleted(@Param("id") int id);
	
	@Query("select d from DeductionCode d where d.isDeleted=false and d.allPaycode=true order by d.id desc")
	public List<DeductionCode> findByIsDeletedAndAllPaycode();
	
	
	@Query("select d from DeductionCode d where d.id=:id")
	public DeductionCode findByDeductionId(@Param("id")Integer id);
	
//	select * from hcm_smartsoft.hr40903  where  ((bencstdt between "2019-06-01" and "2019-06-30") or (benceddt between "2019-06-01" and "2019-06-30")) and bencinctv=0 and is_deleted=0;

	
	@Query("select d from DeductionCode d where d.startDate <= d.endDate and (d.endDate >= :endDate or d.endDate between :startDate and :endDate) and d.startDate < :endDate and d.inActive=false and d.isDeleted=false")
	List<DeductionCode> findByStartDateAfterAndEndDateBefore(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
	@Query("select d from DeductionCode d where d.id=:id and d.startDate <= d.endDate and (d.endDate >= :endDate or d.endDate between :startDate and :endDate) and d.startDate < :endDate and d.inActive=false and d.isDeleted=false")
	DeductionCode findByStartDateAfterAndEndDateBeforeAndDeductionId(@Param("id")Integer id,@Param("startDate")Date startDate,@Param("endDate")Date endDate);

}












