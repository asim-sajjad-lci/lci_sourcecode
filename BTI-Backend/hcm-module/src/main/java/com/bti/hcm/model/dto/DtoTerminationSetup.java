package com.bti.hcm.model.dto;

import java.sql.Date;
import java.util.List;

import com.bti.hcm.model.OrientationPredefinedCheckListItem;
import com.bti.hcm.model.TerminationSetup;
import com.bti.hcm.util.UtilRandomKey;

public class DtoTerminationSetup extends DtoBase{
	private Integer id;

	private Integer orientationPredefinedCheckListItemId;
	private OrientationPredefinedCheckListItem orientationPredefinedCheckListItem;

	private String preDefineCheckListItemDescription;
	private String preDefineCheckListItemDescriptionArabic;
	private short preDefineCheckListType;

	private String terminationId;
	private String terminationDesc;
	private String terminationArbicDesc;
	private Integer predefinecheckListtypeId;

	private String checklistitem;
	private Integer sequence;
	private String personResponsible;
	private Date completeDate;

	private List<DtoTerminationSetup> delete;

	private Integer terminationDetailSequence;
	private String terminationDetailDesc;
	private String terminationDetailArbicDesc;
	private Date terminationDetailDate;
	private String terminationDetailPersonName;
	private Integer terminationDetailId;

	private List<DtoTerminationSetupDetails> subItems;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTerminationId() {
		return terminationId;
	}

	public void setTerminationId(String terminationId) {
		this.terminationId = terminationId;
	}

	public String getTerminationDesc() {
		return terminationDesc;
	}

	public void setTerminationDesc(String terminationDesc) {
		this.terminationDesc = terminationDesc;
	}

	public String getTerminationArbicDesc() {
		return terminationArbicDesc;
	}

	public void setTerminationArbicDesc(String terminationArbicDesc) {
		this.terminationArbicDesc = terminationArbicDesc;
	}

	public List<DtoTerminationSetup> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoTerminationSetup> delete) {
		this.delete = delete;
	}

	public Integer getOrientationPredefinedCheckListItemId() {
		return orientationPredefinedCheckListItemId;
	}

	public Integer getPredefinecheckListtypeId() {
		return predefinecheckListtypeId;
	}

	public void setPredefinecheckListtypeId(Integer predefinecheckListtypeId) {
		this.predefinecheckListtypeId = predefinecheckListtypeId;
	}

	public void setOrientationPredefinedCheckListItemId(Integer orientationPredefinedCheckListItemId) {
		this.orientationPredefinedCheckListItemId = orientationPredefinedCheckListItemId;
	}

	public OrientationPredefinedCheckListItem getOrientationPredefinedCheckListItem() {
		return orientationPredefinedCheckListItem;
	}

	public void setOrientationPredefinedCheckListItem(
			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem) {
		this.orientationPredefinedCheckListItem = orientationPredefinedCheckListItem;
	}

	public String getChecklistitem() {
		return checklistitem;
	}

	public void setChecklistitem(String checklistitem) {
		this.checklistitem = checklistitem;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getPersonResponsible() {
		return personResponsible;
	}

	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public List<DtoTerminationSetupDetails> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<DtoTerminationSetupDetails> subItems) {
		this.subItems = subItems;
	}

	public DtoTerminationSetup(TerminationSetup terminationSetup) {
		this.id = terminationSetup.getId();

		if (UtilRandomKey.isNotBlank(terminationSetup.getTerminationId())) {
			this.terminationId = terminationSetup.getTerminationId();
		} else {
			this.terminationId = "";
		}
		if (UtilRandomKey.isNotBlank(terminationSetup.getTerminationDesc())) {
			this.terminationDesc = terminationSetup.getTerminationDesc();
		} else {
			this.terminationDesc = "";
		}
		if (UtilRandomKey.isNotBlank(terminationSetup.getTerminationArbicDesc())) {
			this.terminationArbicDesc = terminationSetup.getTerminationArbicDesc();
		} else {
			this.terminationArbicDesc = "";
		}
	}

	public DtoTerminationSetup() {
	}

	public Integer getTerminationDetailSequence() {
		return terminationDetailSequence;
	}

	public void setTerminationDetailSequence(Integer terminationDetailSequence) {
		this.terminationDetailSequence = terminationDetailSequence;
	}

	public String getTerminationDetailDesc() {
		return terminationDetailDesc;
	}

	public void setTerminationDetailDesc(String terminationDetailDesc) {
		this.terminationDetailDesc = terminationDetailDesc;
	}

	public String getTerminationDetailArbicDesc() {
		return terminationDetailArbicDesc;
	}

	public void setTerminationDetailArbicDesc(String terminationDetailArbicDesc) {
		this.terminationDetailArbicDesc = terminationDetailArbicDesc;
	}

	public Date getTerminationDetailDate() {
		return terminationDetailDate;
	}

	public void setTerminationDetailDate(Date terminationDetailDate) {
		this.terminationDetailDate = terminationDetailDate;
	}

	public String getTerminationDetailPersonName() {
		return terminationDetailPersonName;
	}

	public void setTerminationDetailPersonName(String terminationDetailPersonName) {
		this.terminationDetailPersonName = terminationDetailPersonName;
	}

	public Integer getTerminationDetailId() {
		return terminationDetailId;
	}

	public void setTerminationDetailId(Integer terminationDetailId) {
		this.terminationDetailId = terminationDetailId;
	}

	public String getPreDefineCheckListItemDescription() {
		return preDefineCheckListItemDescription;
	}

	public void setPreDefineCheckListItemDescription(String preDefineCheckListItemDescription) {
		this.preDefineCheckListItemDescription = preDefineCheckListItemDescription;
	}

	public String getPreDefineCheckListItemDescriptionArabic() {
		return preDefineCheckListItemDescriptionArabic;
	}

	public void setPreDefineCheckListItemDescriptionArabic(String preDefineCheckListItemDescriptionArabic) {
		this.preDefineCheckListItemDescriptionArabic = preDefineCheckListItemDescriptionArabic;
	}

	public short getPreDefineCheckListType() {
		return preDefineCheckListType;
	}

	public void setPreDefineCheckListType(short preDefineCheckListType) {
		this.preDefineCheckListType = preDefineCheckListType;
	}

}
