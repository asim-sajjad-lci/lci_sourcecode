package com.bti.hcm.model.dto;

import java.util.List;

public class DtoBuildCheckBatchMarkOrUnMark extends DtoBase{
	
	private List<Integer> batchId;

	public List<Integer> getBatchId() {
		return batchId;
	}

	public void setBatchId(List<Integer> batchId) {
		this.batchId = batchId;
	}

	
}
