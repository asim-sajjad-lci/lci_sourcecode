/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the Location database table. Name of
 * Project: Hcm Version: 0.0.1
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40104",indexes = {
        @Index(columnList = "LOCINDX")
})
@NamedQuery(name = "Location.findAll", query = "SELECT l FROM Location l")
public class Location extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LOCINDX")
	private int id;

	@Column(name = "LOCID", columnDefinition = "char(15)")
	private String locationId;

	@Column(name = "LOCDSCR", columnDefinition = "char(31)")
	private String description;

	@Column(name = "LOCDSCRA", columnDefinition = "char(61)")
	private String arabicDescription;

	@Column(name = "LOCCONTC", columnDefinition = "char(80)")
	private String contactName;

	@Column(name = "LOCADDRS", columnDefinition = "char(61)")
	private String locationAddress;

	@Column(name = "LOCPHONE", columnDefinition = "char(21)")
	private String phone;

	@Column(name = "LOCFAX", columnDefinition = "char(21)")	/*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "location")
	@Where(clause = "is_deleted = false")
	private List<Location> location;*/
	private String fax;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "location")
	@Where(clause = "is_deleted = false")
	private List<PayScheduleSetupLocation> payScheduleSetupLocation;
	
	
	/*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "location")
	@Where(clause = "is_deleted = false")
	private List<Location> location;*/

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "location")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<EmployeeMaster> employeeMaster;

	@OneToMany(mappedBy = "location",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeePositionHistory> listEmployeePositionHistory;

	@OneToMany(mappedBy = "location",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<PayrollAccounts> listPayrollAccounts;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getLocationAddress() {
		return locationAddress;
	}

	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}

	@ManyToOne
	@JoinColumn(name = "LOCCOUNTY")
	private HrCountry country;

	@ManyToOne
	@JoinColumn(name = "LOCSTATE")
	private HrState state;

	@ManyToOne
	@JoinColumn(name = "LOCCITY")
	private HrCity city;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public HrCountry getCountry() {
		return country;
	}

	public void setCountry(HrCountry country) {
		this.country = country;
	}

	public HrState getState() {
		return state;
	}

	public void setState(HrState state) {
		this.state = state;
	}

	public HrCity getCity() {
		return city;
	}

	public void setCity(HrCity city) {
		this.city = city;
	}

	public List<PayScheduleSetupLocation> getPayScheduleSetupLocation() {
		return payScheduleSetupLocation;
	}

	public void setPayScheduleSetupLocation(List<PayScheduleSetupLocation> payScheduleSetupLocation) {
		this.payScheduleSetupLocation = payScheduleSetupLocation;
	}

	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public List<EmployeePositionHistory> getListEmployeePositionHistory() {
		return listEmployeePositionHistory;
	}

	public void setListEmployeePositionHistory(List<EmployeePositionHistory> listEmployeePositionHistory) {
		this.listEmployeePositionHistory = listEmployeePositionHistory;
	}

	public List<PayrollAccounts> getListPayrollAccounts() {
		return listPayrollAccounts;
	}

	public void setListPayrollAccounts(List<PayrollAccounts> listPayrollAccounts) {
		this.listPayrollAccounts = listPayrollAccounts;
	}

	/*public List<Location> getLocation() {
		return location;
	}

	public void setLocation(List<Location> location) {
		this.location = location;
	}*/

}
