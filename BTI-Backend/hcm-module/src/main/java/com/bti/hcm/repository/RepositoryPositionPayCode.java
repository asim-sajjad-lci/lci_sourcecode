package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PositionPayCode;

@Repository("repositoryPositionPayCode")
public interface RepositoryPositionPayCode extends JpaRepository<PositionPayCode, Integer>{

	PositionPayCode findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from PositionPayCode s where s.isDeleted=false")
	Integer getCountOfTotalPosition();

	List<PositionPayCode> findByIsDeleted(boolean b, Pageable pageable);

	List<PositionPayCode> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PositionPayCode d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSinglePosition(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from PositionPayCode d where (d.positionPalnSetup.positionPlanId like :searchKeyWord  or d.payCode.payCodeId like :searchKeyWord or d.positionPalnSetup.positionPlanDesc like :searchKeyWord) and d.isDeleted=false")
	Integer predictivePositionPayRoleSearchTotalCount(@Param("searchKeyWord")String string);

	@Query("select d from PositionPayCode d where (d.positionPalnSetup.positionPlanId like :searchKeyWord  or d.payCode.payCodeId like :searchKeyWord or d.positionPalnSetup.positionPlanDesc like :searchKeyWord) and d.isDeleted=false")
	List<PositionPayCode> predictivePositionPayRoleSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Query("select d.positionPalnSetup.positionPlanId from PositionPayCode d where (d.positionPalnSetup.positionPlanId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<String> predictivePositionPayCodeIdSearchWithPagination(@Param("searchKeyWord")String string);

}
