package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.SalaryMatrixRow;

public class DtoSalaryMatrixRow extends DtoBase{

	private Integer id;
	private String salaryMatrixSetupId;
	private Integer salaryMatrixId;
	private String desc;
	private List<DtoSalaryMatrixRowValue> listSalaryMatrixRowValue;
	private String arbic;
	private List<DtoSalaryMatrixRow> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getArbic() {
		return arbic;
	}
	public void setArbic(String arbic) {
		this.arbic = arbic;
	}
	
	public List<DtoSalaryMatrixRow> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoSalaryMatrixRow> delete) {
		this.delete = delete;
	}
	public Integer getSalaryMatrixId() {
		return salaryMatrixId;
	}
	public void setSalaryMatrixId(Integer salaryMatrixId) {
		this.salaryMatrixId = salaryMatrixId;
	}
	public String getSalaryMatrixSetupId() {
		return salaryMatrixSetupId;
	}
	public void setSalaryMatrixSetupId(String salaryMatrixSetupId) {
		this.salaryMatrixSetupId = salaryMatrixSetupId;
	}
	public DtoSalaryMatrixRow() {
	}
	
	public List<DtoSalaryMatrixRowValue> getListSalaryMatrixRowValue() {
		return listSalaryMatrixRowValue;
	}
	public void setListSalaryMatrixRowValue(List<DtoSalaryMatrixRowValue> listSalaryMatrixRowValue) {
		this.listSalaryMatrixRowValue = listSalaryMatrixRowValue;
	}
	public DtoSalaryMatrixRow(SalaryMatrixRow salaryMatrixRow) {
	
	}
}
