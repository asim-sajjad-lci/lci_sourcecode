package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AuditTrialCodes;

@Repository("/repositoryAuditTrialCodes")
public interface RepositoryAuditTrialCodes extends JpaRepository<AuditTrialCodes, Integer>{
	
	
	@Query("select b from AuditTrialCodes b where b.serialId =:id and b.isDeleted=false")
	List<AuditTrialCodes> findBySerialId(@Param("id") Integer serialId);
	
	@Query("select b from AuditTrialCodes b where b.defaultId.id =:id and b.isDeleted=false")
	List<AuditTrialCodes> findByDefaultId(@Param("id") Integer serialId);


}
