package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.TrainingCourseDetail;
import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTrainingCourseDetail;
import com.bti.hcm.model.dto.DtoTraningCourse;
import com.bti.hcm.repository.RepositoryTrainingCourseDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceTrainingCourseDetail")
public class ServiceTrainingCourseDetail {

	
	static Logger log = Logger.getLogger(ServiceTrainingCourseDetail.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryTrainingCourseDetail repositoryTrainingCourseDetail;
	
	
	
	
	
	public DtoTrainingCourseDetail saveOrUpdateTraningCourseDetail(DtoTrainingCourseDetail dtoTrainingCourseDetail) {
		log.info("enter into save or update TrainingCourseDetail");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		TrainingCourseDetail trainingCourseDetail=null;
		if (dtoTrainingCourseDetail.getId() != null && dtoTrainingCourseDetail.getId() > 0) {
			trainingCourseDetail = repositoryTrainingCourseDetail.findByIdAndIsDeleted(dtoTrainingCourseDetail.getId(), false);
			trainingCourseDetail.setUpdatedBy(loggedInUserId);
			trainingCourseDetail.setUpdatedDate(new Date());
		} else {
			trainingCourseDetail = new TrainingCourseDetail();
			trainingCourseDetail.setCreatedDate(new Date());
			
			Integer rowId = repositoryTrainingCourseDetail.getCountOfTotalTrainingCourseDetail();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			trainingCourseDetail.setRowId(increment);
		}
		trainingCourseDetail.setId(dtoTrainingCourseDetail.getId());
		trainingCourseDetail.setClassId(dtoTrainingCourseDetail.getClassId());
        trainingCourseDetail.setClassName(dtoTrainingCourseDetail.getClassName());
        trainingCourseDetail.setStartDate(dtoTrainingCourseDetail.getStartDate());
        trainingCourseDetail.setEndDate(dtoTrainingCourseDetail.getEndDate());
        trainingCourseDetail.setStartTime(dtoTrainingCourseDetail.getStartTime());
        trainingCourseDetail.setEndTime(dtoTrainingCourseDetail.getEndTime());
        trainingCourseDetail.setInstructorName(dtoTrainingCourseDetail.getInstructorName());
        trainingCourseDetail.setEnrolled(dtoTrainingCourseDetail.getEnrolled());
        trainingCourseDetail.setMaximum(dtoTrainingCourseDetail.getMaximum());
        trainingCourseDetail.setClassLocation(dtoTrainingCourseDetail.getClassLocation());
        
	
        trainingCourseDetail.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryTrainingCourseDetail.saveAndFlush(trainingCourseDetail);
		return dtoTrainingCourseDetail;
	}
	
	
	
	public DtoTrainingCourseDetail deleteTraningCourseDetail(List<Integer> ids) {
		log.info("delete TrainingCourseDetail Method");
		DtoTrainingCourseDetail dtoTrainingCourseDetail = new DtoTrainingCourseDetail();
		dtoTrainingCourseDetail.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRANINGCOURSEDETAIL_DELETED", false));
		dtoTrainingCourseDetail.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRANINGCOURSEDETAIL__ASSOCIATED", false));
		List<DtoTrainingCourseDetail> deleteTrainingCourseDetail = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer id : ids) {
				TrainingCourseDetail trainingCourseDetail = repositoryTrainingCourseDetail.findOne(id);
				DtoTrainingCourseDetail dtoTrainingCourseDetail2 = new DtoTrainingCourseDetail();
				
				dtoTrainingCourseDetail2.setId(trainingCourseDetail.getId());
				dtoTrainingCourseDetail2.setClassId(trainingCourseDetail.getClassId());
				dtoTrainingCourseDetail2.setClassName(trainingCourseDetail.getClassName());
				dtoTrainingCourseDetail2.setStartDate(trainingCourseDetail.getStartDate());
				dtoTrainingCourseDetail2.setEndDate(trainingCourseDetail.getEndDate());
				dtoTrainingCourseDetail2.setStartTime(trainingCourseDetail.getStartTime());
				dtoTrainingCourseDetail2.setEndTime(trainingCourseDetail.getEndTime());
				dtoTrainingCourseDetail2.setInstructorName(trainingCourseDetail.getInstructorName());
				dtoTrainingCourseDetail2.setEnrolled(trainingCourseDetail.getEnrolled());
				dtoTrainingCourseDetail2.setMaximum(trainingCourseDetail.getMaximum());
				dtoTrainingCourseDetail2.setClassLocation(trainingCourseDetail.getClassLocation());

				repositoryTrainingCourseDetail.deleteSingleTraningCourseDetail(true, loggedInUserId, id);
				deleteTrainingCourseDetail.add(dtoTrainingCourseDetail2);

			}
			dtoTrainingCourseDetail.setDelete(deleteTrainingCourseDetail);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete AccrualScheduleDetail :"+dtoTrainingCourseDetail.getId());
		return dtoTrainingCourseDetail;
	}	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchTraningCourseDetail(DtoSearch dtoSearch) {
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("traningId") || dtoSearch.getSortOn().equals("desc")|| dtoSearch.getSortOn().equals("arbicDesc")|| dtoSearch.getSortOn().equals("location")|| dtoSearch.getSortOn().equals("prerequisiteId")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
			
			dtoSearch.setTotalCount(this.repositoryTrainingCourseDetail.predictiveTrainingCourseDetailTotalCount("%"+searchWord+"%"));
			List<TraningCourse> traningCourseList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					traningCourseList = this.repositoryTrainingCourseDetail.predictiveTrainingCourseDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					traningCourseList = this.repositoryTrainingCourseDetail.predictiveTrainingCourseDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					traningCourseList = this.repositoryTrainingCourseDetail.predictiveTrainingCourseDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			if(traningCourseList != null && !traningCourseList.isEmpty()){
				List<DtoTraningCourse> dtoTraningCourseList = new ArrayList<>();
				DtoTraningCourse dtoTraningCourse=null;
				for (TraningCourse traningCourse : traningCourseList) {
					dtoTraningCourse = new DtoTraningCourse(traningCourse);
					
					
					
					dtoTraningCourse.setId(traningCourse.getId());
					dtoTraningCourse.setTraningId(traningCourse.getTraningId());
                    dtoTraningCourse.setDesc(traningCourse.getDesc());
                    dtoTraningCourse.setArbicDesc(traningCourse.getArbicDesc());
                    dtoTraningCourse.setLocation(traningCourse.getLocation());
                    dtoTraningCourse.setPrerequisiteId(traningCourse.getPrerequisiteId());
                    dtoTraningCourse.setEmployeeCost(traningCourse.getEmployeeCost());
                    dtoTraningCourse.setEmployerCost(traningCourse.getEmployerCost());
                    dtoTraningCourse.setSupplierCost(traningCourse.getSupplierCost());
                    dtoTraningCourse.setInstructorCost(traningCourse.getInstructorCost());
				
				 
                    dtoTraningCourseList.add(dtoTraningCourse);
					}
					
				
				dtoSearch.setRecords(dtoTraningCourseList);
				}
				
			}
		
		log.debug("Search searchPosition Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
		}

	
	
	
	
	
	public DtoTrainingCourseDetail repeatByClassId(String classId) {
		log.info("repeatByTrainingCourseDetailClassId Method");
		DtoTrainingCourseDetail dtoTrainingCourseDetail = new DtoTrainingCourseDetail();
		try {
			List<TrainingCourseDetail> trainingCourseDetail=repositoryTrainingCourseDetail.findByTrainingCourseDetailClassId(classId);
			if(trainingCourseDetail!=null && !trainingCourseDetail.isEmpty()) {
				dtoTrainingCourseDetail.setIsRepeat(true);
				dtoTrainingCourseDetail.setClassId(classId);
			}else {
				dtoTrainingCourseDetail.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoTrainingCourseDetail;
	}
	

	
	
	
	public DtoTrainingCourseDetail getTraningCourseDetailById(int id) {
		DtoTrainingCourseDetail dtoTrainingCourseDetail  = new DtoTrainingCourseDetail();
		if (id > 0) {
			TrainingCourseDetail trainingCourseDetail = repositoryTrainingCourseDetail.findByIdAndIsDeleted(id, false);
			if (trainingCourseDetail != null) {
				dtoTrainingCourseDetail = new DtoTrainingCourseDetail(trainingCourseDetail);
			} else {
				dtoTrainingCourseDetail.setMessageType("RETIREMENTPLANSETUP_NOT_GETTING");

			}
		} else {
			dtoTrainingCourseDetail.setMessageType("INVALID_RETIREMENTPLAN_ID");

		}
		return dtoTrainingCourseDetail;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllIds(DtoSearch dtoSearch) {
		log.info("search TraningCourseDetail Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("traningId") || dtoSearch.getSortOn().equals("desc")|| dtoSearch.getSortOn().equals("arbicDesc")|| dtoSearch.getSortOn().equals("location")|| dtoSearch.getSortOn().equals("prerequisiteId")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
			
			dtoSearch.setTotalCount(this.repositoryTrainingCourseDetail.predictiveTrainingCourseDetailTotalCount("%"+searchWord+"%"));
			List<TrainingCourseDetail> traningCourseList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					traningCourseList = this.repositoryTrainingCourseDetail.getAll("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					traningCourseList = this.repositoryTrainingCourseDetail.getAll("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					traningCourseList = this.repositoryTrainingCourseDetail.getAll("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			if(traningCourseList != null && !traningCourseList.isEmpty()){
				List<DtoTrainingCourseDetail> dtoTraningCourseList = new ArrayList<>();
				DtoTrainingCourseDetail dtoTraningCourse=null;
				for (TrainingCourseDetail traningCourse : traningCourseList) {
					dtoTraningCourse = new DtoTrainingCourseDetail(traningCourse);
					
					
					
					dtoTraningCourse.setId(traningCourse.getId());
					dtoTraningCourse.setClassId(traningCourse.getClassId());
					dtoTraningCourse.setClassName(traningCourse.getClassName());
					
                     
                    dtoTraningCourseList.add(dtoTraningCourse);
					}
					
				
				dtoSearch.setRecords(dtoTraningCourseList);
				}
				
			}
		
		return dtoSearch;
		}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllByTraningId(DtoSearch dtoSearch) {
		log.info("search TraningCourseDetail Method");
		if(dtoSearch != null){
			List<TrainingCourseDetail> traningCourseList =null;
			traningCourseList = this.repositoryTrainingCourseDetail.findByTrainingCourseAndIsDeleted(dtoSearch.getId());
			
			if(traningCourseList != null && !traningCourseList.isEmpty()){
				List<DtoTrainingCourseDetail> dtoTraningCourseList = new ArrayList<>();
				DtoTrainingCourseDetail dtoTraningCourse=null;
				for (TrainingCourseDetail traningCourse : traningCourseList) {
					dtoTraningCourse = new DtoTrainingCourseDetail(traningCourse);

					dtoTraningCourse.setTraningId(traningCourse.getTrainingCourse().getTraningId());

					dtoTraningCourse.setId(traningCourse.getId());
					dtoTraningCourse.setClassId(traningCourse.getClassId());
					dtoTraningCourse.setClassName(traningCourse.getClassName());
					
                     
                    dtoTraningCourseList.add(dtoTraningCourse);
					}
					
				
				dtoSearch.setRecords(dtoTraningCourseList);
				}
				
			}
		
		return dtoSearch;
		}

	
	
}
