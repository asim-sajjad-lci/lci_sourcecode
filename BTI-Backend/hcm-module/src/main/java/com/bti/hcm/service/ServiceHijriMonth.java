/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.HijriMonth;
import com.bti.hcm.model.dto.DtoGetMonthById;
import com.bti.hcm.repository.RepositoryHijriMonth;

/**
 * Description: Interface for Hijri Month Name of Project:Hcm Version: 0.0.1
 * Created on: February 12, 2018
 * 
 * @author Bansi
 *
 */
@Service("serviceHijriMonth")	
public class ServiceHijriMonth {

	/**
	 * @Description LOGGER use for put a logger in HijriMonth Service
	 */
	static Logger logger = Logger.getLogger(ServiceHijriMonth.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in HijriMonth service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in HijriMonth service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description Autowired spring annotation to access repositoryHijriDay method
	 *              in HijriDay service In short, access HijriMonth Query from
	 *              Database using repositoryHijriDay.
	 */
	@Autowired
	RepositoryHijriMonth repositoryHijriMonth;

	public DtoGetMonthById getMonthById(int hijriMonthId) {
		DtoGetMonthById dtoGetMonthById = new DtoGetMonthById();
		try {
			HijriMonth hijriMonth = repositoryHijriMonth.findByHijriMonthId(hijriMonthId);
			dtoGetMonthById.setId(hijriMonth.getId());
			dtoGetMonthById.setMonthName(hijriMonth.getMonth());
		} catch (Exception e) {
			logger.error(e);
		}
		return dtoGetMonthById;

	}
}
