package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.EmployeeSkills;

public class DtoEmployeeSkills extends DtoBase{

	private Integer id;
	private DtoSkillSetSteup skillSetSetup;
	private DtoEmployeeMasterHcm employeeMaster;
	private List<DtoEmployeeSkillsDesc> listSkills;
	private List<DtoEmployeeSkills> delete;
	private Integer employeeId;
	private Integer skillId;
	private DtoEmployeeMasterHcm dtoEmployeeMasterHcm;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public DtoSkillSetSteup getSkillSetSetup() {
		return skillSetSetup;
	}
	public void setSkillSetSetup(DtoSkillSetSteup skillSetSetup) {
		this.skillSetSetup = skillSetSetup;
	}
	
	public DtoEmployeeMasterHcm getEmployeeMaster() {
		return employeeMaster;
	}
	public void setEmployeeMaster(DtoEmployeeMasterHcm employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	public List<DtoEmployeeSkills> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoEmployeeSkills> delete) {
		this.delete = delete;
	}
	
	public List<DtoEmployeeSkillsDesc> getListSkills() {
		return listSkills;
	}
	public void setListSkills(List<DtoEmployeeSkillsDesc> listSkills) {
		this.listSkills = listSkills;
	}
	
	public DtoEmployeeSkills() {
	}
	
	public DtoEmployeeSkills(EmployeeSkills employeeSkills ) {
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public DtoEmployeeMasterHcm getDtoEmployeeMasterHcm() {
		return dtoEmployeeMasterHcm;
	}
	public void setDtoEmployeeMasterHcm(DtoEmployeeMasterHcm dtoEmployeeMasterHcm) {
		this.dtoEmployeeMasterHcm = dtoEmployeeMasterHcm;
	}
	public Integer getSkillId() {
		return skillId;
	}
	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}
	
}
