package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.VATSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoVatSetup;
import com.bti.hcm.repository.RepositoryVatSetup;

@Service("serviceVatSetup")
public class ServiceVatSetup {
	
	static Logger log = Logger.getLogger(ServiceVatSetup.class.getName());
	
	@Autowired
	RepositoryVatSetup repositoryVatSetup;
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	public DtoVatSetup saveOrUpdate(DtoVatSetup dtoVatSetup) {
		log.info("VatSetup saveOrUpdate Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		VATSetup vatSetup = null;
		if(dtoVatSetup.getId() != null && dtoVatSetup.getId() > 0) {
			vatSetup = repositoryVatSetup.findByIdAndIsDeleted(dtoVatSetup.getId(), false);
			vatSetup.setUpdatedBy(loggedInUserId);
			vatSetup.setUpdatedDate(new Date());
		} else {
			vatSetup = new VATSetup();
			vatSetup.setCreatedDate(new Date());
			Integer rowId = repositoryVatSetup.findAll().size();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			vatSetup.setRowId(increment);
			
		}
		vatSetup.setVatScheduleId(dtoVatSetup.getVatScheduleId());
		vatSetup.setVatDescription(dtoVatSetup.getVatDescription());
		vatSetup.setVatDescriptionArabic(dtoVatSetup.getVatDescriptionArabic());
		vatSetup.setVatSeriesType(dtoVatSetup.getVatSeriesType());
		vatSetup.setVatIdNumber(dtoVatSetup.getVatIdNumber());
		vatSetup.setAccountTableRowIndex(dtoVatSetup.getAccountTableRowIndex());
		vatSetup.setVatBasedOn(dtoVatSetup.getVatBasedOn());
		vatSetup.setPercentageBasedOn(dtoVatSetup.getPercentageBasedOn());
		vatSetup.setMinimumVatAmt(dtoVatSetup.getMinimumVatAmt());
		vatSetup.setMaximumVatAmt(dtoVatSetup.getMaximumVatAmt());
		vatSetup.setYtdTotalSales(dtoVatSetup.getYtdTotalSales());
		vatSetup.setYtdTotalTaxableSales(dtoVatSetup.getYtdTotalTaxableSales());
		vatSetup.setYtdTotalSalesTaxes(dtoVatSetup.getYtdTotalSalesTaxes());
		vatSetup.setLastYearTotalSales(dtoVatSetup.getLastYearTotalSales());
		vatSetup.setLastYearTotalTaxableSales(dtoVatSetup.getLastYearTotalTaxableSales());
		vatSetup.setLastYearTotalSalesTaxes(dtoVatSetup.getLastYearTotalSalesTaxes());
		vatSetup.setRowId(loggedInUserId);
		vatSetup = repositoryVatSetup.saveAndFlush(vatSetup);
		log.debug("vat setup is:" + vatSetup.getId());
		
		return dtoVatSetup;
	}
	
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search Method");
		if(dtoSearch !=null) {
			String searchWord = dtoSearch.getSearchKeyword();
			
			dtoSearch=searchByVatSetup(dtoSearch);
			dtoSearch.setTotalCount(repositoryVatSetup.predictiveVatSetupSearchTotalCount("%"+searchWord+"%"));
			List<VATSetup> vatSetupList = searchByPageSizeAndNumberInVATSetup(dtoSearch);
			if(!vatSetupList.isEmpty()) {
				List<DtoVatSetup> dtoVatSetupList = new ArrayList<>();
				DtoVatSetup dtoVatSetup = null;
				for (VATSetup vatSetup : vatSetupList) {
					dtoVatSetup = getAndSetDtoVatSetup(vatSetup);
					dtoVatSetupList.add(dtoVatSetup);
				}
				dtoSearch.setRecords(dtoVatSetupList);
			}
		}
		return dtoSearch;
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchByVatSetup(DtoSearch dtoSearch) {
		String condition = "";

		if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
			if (dtoSearch.getSortOn().equals("vatScheduleId") || dtoSearch.getSortOn().equals("vatDescription")
					|| dtoSearch.getSortOn().equals("vatSeriesType")) {
				if (dtoSearch.getSortOn().equals("minimumVatAmt")) {
					dtoSearch.setSortOn("maximumVatAmt");
				}
				condition = dtoSearch.getSortOn();

			} else {
				condition = "id";

			}
		} else {
			condition += "id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");

		}
		dtoSearch.setCondition(condition);
		return dtoSearch;
	}
	
	
	
	public List<VATSetup> searchByPageSizeAndNumberInVATSetup(DtoSearch dtoSearch) {
		List<VATSetup> vatSetupList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
			if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
				vatSetupList = this.repositoryVatSetup.predictiveVatSetupSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
								Sort.Direction.DESC, "id"));
			}
			if (dtoSearch.getSortBy().equals("ASC")) {
				vatSetupList = this.repositoryVatSetup.predictiveVatSetupSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
								Sort.Direction.ASC, dtoSearch.getCondition()));
			} else if (dtoSearch.getSortBy().equals("DESC")) {
				vatSetupList = this.repositoryVatSetup.predictiveVatSetupSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
								Sort.Direction.DESC, dtoSearch.getCondition()));
			}
		}
		
		return vatSetupList;
	}
	
	public List<DtoVatSetup> getAllData(){
		List<VATSetup> vatSetupList= repositoryVatSetup.getAllData();
		List<DtoVatSetup> dtoVatSetupList = new ArrayList<>();
		if(vatSetupList !=null && !vatSetupList.isEmpty()) {
			DtoVatSetup dtoVatSetup = null;
			for (VATSetup vatSetup : vatSetupList) {
				dtoVatSetup = getAndSetDtoVatSetup(vatSetup);
				dtoVatSetupList.add(dtoVatSetup);
				
			}
		}
		return dtoVatSetupList;
	}
	
	
	public List<DtoVatSetup> purchaseId(){
		List<VATSetup> vatSetupList= repositoryVatSetup.getAllData();
		List<DtoVatSetup> dtoVatSetupList = new ArrayList<>();
		if(vatSetupList !=null && !vatSetupList.isEmpty()) {
			DtoVatSetup dtoVatSetup = null;
			for (VATSetup vatSetup : vatSetupList) {
				if(vatSetup.getVatSeriesType().equals(1)) {
					dtoVatSetup = getAndSetDtoVatSetup(vatSetup);
					dtoVatSetupList.add(dtoVatSetup);
				}
			}
		}
		return dtoVatSetupList;
	}
	
	public List<DtoVatSetup> salesId(){
		List<VATSetup> vatSetupList= repositoryVatSetup.getAllData();
		List<DtoVatSetup> dtoVatSetupList = new ArrayList<>();
		if(vatSetupList !=null && !vatSetupList.isEmpty()) {
			DtoVatSetup dtoVatSetup = null;
			for (VATSetup vatSetup : vatSetupList) {
				if(vatSetup.getVatSeriesType().equals(2)) {
					dtoVatSetup = getAndSetDtoVatSetup(vatSetup);
					dtoVatSetupList.add(dtoVatSetup);
				}
			}
		}
		return dtoVatSetupList;
	}

	public List<DtoVatSetup> getAllDroupDown(DtoSearch search) {
		String searchKeyWord = search.getSearchKeyword();
		List<VATSetup> vatSetupList= repositoryVatSetup.getAllDroupDown("%"+searchKeyWord+"%");
		List<DtoVatSetup> dtoVatSetupList = new ArrayList<>();
		if(vatSetupList !=null && !vatSetupList.isEmpty()) {
			DtoVatSetup dtoVatSetup = null;
			for (VATSetup vatSetup : vatSetupList) {
				dtoVatSetup = getAndSetDtoVatSetup(vatSetup);
				dtoVatSetupList.add(dtoVatSetup);
				
			}
		}
		return dtoVatSetupList;
	}

	public DtoVatSetup getAndSetDtoVatSetup(VATSetup vatSetup) {
		DtoVatSetup dtoVatSetup = new DtoVatSetup();
		dtoVatSetup.setVatScheduleId(vatSetup.getVatScheduleId());
		dtoVatSetup.setVatDescription(vatSetup.getVatDescription());
		dtoVatSetup.setVatDescriptionArabic(vatSetup.getVatDescriptionArabic());
		dtoVatSetup.setVatSeriesType(vatSetup.getVatSeriesType());
		dtoVatSetup.setVatIdNumber(vatSetup.getVatIdNumber());
		dtoVatSetup.setAccountTableRowIndex(vatSetup.getAccountTableRowIndex());
		dtoVatSetup.setVatBasedOn(vatSetup.getVatBasedOn());
		dtoVatSetup.setPercentageBasedOn(vatSetup.getPercentageBasedOn());
		dtoVatSetup.setMinimumVatAmt(vatSetup.getMinimumVatAmt());
		dtoVatSetup.setMaximumVatAmt(vatSetup.getMaximumVatAmt());
		dtoVatSetup.setYtdTotalSales(vatSetup.getYtdTotalSales());
		dtoVatSetup.setYtdTotalTaxableSales(vatSetup.getYtdTotalTaxableSales());
		dtoVatSetup.setYtdTotalSalesTaxes(vatSetup.getYtdTotalSalesTaxes());
		dtoVatSetup.setLastYearTotalSales(vatSetup.getLastYearTotalSales());
		dtoVatSetup.setLastYearTotalTaxableSales(vatSetup.getLastYearTotalTaxableSales());
		dtoVatSetup.setLastYearTotalSalesTaxes(vatSetup.getLastYearTotalSalesTaxes());
		dtoVatSetup.setId(vatSetup.getId());
		return dtoVatSetup;
	}
}
