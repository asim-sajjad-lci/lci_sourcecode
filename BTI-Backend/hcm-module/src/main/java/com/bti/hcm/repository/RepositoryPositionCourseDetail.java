package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PositionCourseDetail;

@Repository("repositoryPositionCourseDetail")
public interface RepositoryPositionCourseDetail extends JpaRepository<PositionCourseDetail, Integer>{

	PositionCourseDetail findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from PositionCourseDetail p where p.isDeleted=false")
	Integer getCountOfTotalPosition();

	List<PositionCourseDetail> findByIsDeleted(boolean b, Pageable pageable);

	List<PositionCourseDetail> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PositionCourseDetail p set p.isDeleted =:deleted ,p.updatedBy =:updateById where p.id =:id ")
	void deleteSinglePosition(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from PositionCourseDetail p where (p.traningCourse.arbicDesc like :searchKeyWord or p.traningCourse.desc like :searchKeyWord) and p.isDeleted=false")
	Integer predictivePositionSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select p from PositionCourseDetail p where (p.traningCourse.arbicDesc like :searchKeyWord or p.traningCourse.desc like :searchKeyWord) and p.isDeleted=false")
	List<PositionCourseDetail> predictivePositionSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	@Query("select count(*) from PositionCourseDetail p ")
	public Integer getCountOfTotaPositionCourseDetail();

}
