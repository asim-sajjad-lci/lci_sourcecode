package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10320",indexes = {
        @Index(columnList = "HCMDEDINX")
})
public class BuildPayrollCheckByBenefits extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMDEDINX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "HCMBULDINX")
	private BuildChecks buildChecks;

	@ManyToOne
	@JoinColumn(name = "BENCINDX")
	@Where(clause = "BENCINCTV = false and is_deleted = false")
	private BenefitCode benefitCode;
	
	/*@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="buildPayrollCheckByBenefits")
	@Where(clause = "is_deleted = false")
	private List<Distribution>listDistribution;*/
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(BuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}

	public BenefitCode getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(BenefitCode benefitCode) {
		this.benefitCode = benefitCode;
	}

}
