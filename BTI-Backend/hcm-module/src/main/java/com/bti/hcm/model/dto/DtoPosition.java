package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.Position;
import com.bti.hcm.model.PositionClass;
import com.bti.hcm.model.PositionPalnSetup;
import com.bti.hcm.model.PositionSetup;
import com.bti.hcm.model.SkillSetSetup;

public class DtoPosition extends DtoBase{

	private Integer id;
	private SkillSetSetup skillSetSetup;
	private String positionId;
	private String description;
	private String arabicDescription;
	private PositionClass positionClass;
	private List<PositionPalnSetup> listPositionPlan;
	private List<DtoPositionPlanSetup> listDtoPositionPlanSetup;
	private List<PositionSetup> listPositionAttachment;
	private String reportToPostion;
	private String postionLongDesc;
	private String skillSetId;
	private String positionClassId;
	private Integer positionClassPrimaryId;
	private Integer skillsetPrimaryId;
	private int valId;
	
	private List<DtoPosition> deletePosition;

	private int valueId;
	private String valueDescription;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SkillSetSetup getSkillSetSetup() {
		return skillSetSetup;
	}

	public void setSkillSetSetup(SkillSetSetup skillSetSetup) {
		this.skillSetSetup = skillSetSetup;
	}

	public PositionClass getPositionClass() {
		return positionClass;
	}

	public void setPositionClass(PositionClass positionClass) {
		this.positionClass = positionClass;
	}

	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}
	public String getReportToPostion() {
		return reportToPostion;
	}

	public void setReportToPostion(String reportToPostion) {
		this.reportToPostion = reportToPostion;
	}

	public String getPostionLongDesc() {
		return postionLongDesc;
	}

	public void setPostionLongDesc(String postionLongDesc) {
		this.postionLongDesc = postionLongDesc;
	}
	

	public List<DtoPosition> getDeletePosition() {
		return deletePosition;
	}

	public void setDeletePosition(List<DtoPosition> deletePosition) {
		this.deletePosition = deletePosition;
	}

	public String getSkillSetId() {
		return skillSetId;
	}

	public void setSkillSetId(String skillSetId) {
		this.skillSetId = skillSetId;
	}

	public String getPositionClassId() {
		return positionClassId;
	}

	public void setPositionClassId(String positionClassId) {
		this.positionClassId = positionClassId;
	}

	public DtoPosition() {
	}

	public List<PositionPalnSetup> getListPositionPlan() {
		return listPositionPlan;
	}

	public void setListPositionPlan(List<PositionPalnSetup> listPositionPlan) {
		this.listPositionPlan = listPositionPlan;
	}

	public List<PositionSetup> getListPositionAttachment() {
		return listPositionAttachment;
	}

	public void setListPositionAttachment(List<PositionSetup> listPositionAttachment) {
		this.listPositionAttachment = listPositionAttachment;
	}

	public Integer getPositionClassPrimaryId() {
		return positionClassPrimaryId;
	}

	public void setPositionClassPrimaryId(Integer positionClassPrimaryId) {
		this.positionClassPrimaryId = positionClassPrimaryId;
	}

	public Integer getSkillsetPrimaryId() {
		return skillsetPrimaryId;
	}

	public void setSkillsetPrimaryId(Integer skillsetPrimaryId) {
		this.skillsetPrimaryId = skillsetPrimaryId;
	}

	public DtoPosition(Position position) {
		this.positionId = position.getPositionId();
	}

	public List<DtoPositionPlanSetup> getListDtoPositionPlanSetup() {
		return listDtoPositionPlanSetup;
	}

	public void setListDtoPositionPlanSetup(List<DtoPositionPlanSetup> listDtoPositionPlanSetup) {
		this.listDtoPositionPlanSetup = listDtoPositionPlanSetup;
	}

	public int getValueId() {
		return valueId;
	}

	public void setValueId(int valueId) {
		this.valueId = valueId;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	public int getValId() {
		return valId;
	}

	public void setValId(int valId) {
		this.valId = valId;
	}

	
	
}
