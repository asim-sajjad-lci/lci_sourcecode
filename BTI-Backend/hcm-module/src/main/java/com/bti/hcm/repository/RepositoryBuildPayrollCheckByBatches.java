package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.BuildPayrollCheckByBatches;

@Repository("/repositoryBuildPayrollCheckByBatches")
public interface RepositoryBuildPayrollCheckByBatches extends JpaRepository<BuildPayrollCheckByBatches, Integer>{

	@Query("select m from BuildPayrollCheckByBatches m where m.buildChecks.id =:id and m.isDeleted=false")
	List<BuildPayrollCheckByBatches> findByBuildChecksId(@Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BuildPayrollCheckByBatches m set m.isDeleted =:deleted ,m.updatedBy =:updateById where m.buildChecks.id =:id ")
	public void deleteByBuildCheckId(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId,@Param("id")Integer id);

	@Query("select d from BuildPayrollCheckByBatches d where d.batches.id =:id AND (d.batches.createdDate between :fromDate AND :toDate) AND d.isDeleted = false")
	List<BuildPayrollCheckByBatches> findByCreatedDate(@Param("fromDate")Date fromDate, @Param("toDate")Date toDate,@Param("id")Integer id);

	@Query("select d from BuildPayrollCheckByBatches d where ( d.batches.batchId LIKE :searchKeyWord or d.batches.description LIKE :searchKeyWord) and d.buildChecks.id =:id and d.isDeleted=false")
	List<BuildPayrollCheckByBatches> findByBuildChecksId(@Param("searchKeyWord")String string,@Param("id") Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BuildPayrollCheckByBatches m set m.isDeleted =:deleted ,m.updatedBy =:updateById where m.id =:id ")
	public void deleteByBuildCheckId1(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId,@Param("id")Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("DELETE FROM BuildPayrollCheckByBatches b  where b.id =:id ")
	void deleteSingleBuildPayrollCheckByBatches(@Param("id") Integer id);
}


