package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40202",indexes = {
        @Index(columnList = "YRINDX")
})
public class AccrualPeriod extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "YRINDX")
	private Integer id;
	
	@Column(name = "YEAR1")
	private Integer year;
	
	@Column(name = "YRACT")
	private Integer yearActive;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accrualPeriod")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<AccrualPeriodSetting> listAccrualPeriodSettings;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getYearActive() {
		return yearActive;
	}

	public void setYearActive(Integer yearActive) {
		this.yearActive = yearActive;
	}

	public List<AccrualPeriodSetting> getListAccrualPeriodSettings() {
		return listAccrualPeriodSettings;
	}

	public void setListAccrualPeriodSettings(List<AccrualPeriodSetting> listAccrualPeriodSettings) {
		this.listAccrualPeriodSettings = listAccrualPeriodSettings;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listAccrualPeriodSettings == null) ? 0 : listAccrualPeriodSettings.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		result = prime * result + ((yearActive == null) ? 0 : yearActive.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

	
	
}
