package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTraningCourseClassSkill;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTraningCourseClassSkill;

@RestController
@RequestMapping("/controllerTraningCourseClassSkill")
public class ControllerTraningCourseClassSkill extends BaseController{

private static final Logger LOGGER = Logger.getLogger(ControllerTraningCourseClassSkill.class);
	
	@Autowired(required=true)
	ServiceTraningCourseClassSkill serviceTraningCourseClassSkill;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoTraningCourseClassSkill dtoTraningCourseClassSkill) throws Exception {
		LOGGER.info("Create TraningCourse Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTraningCourseClassSkill = serviceTraningCourseClassSkill.saveOrUpdate(dtoTraningCourseClassSkill);
			responseMessage=displayMessage(dtoTraningCourseClassSkill, "TRAININGCOURSE_CREATED", "TRAININGCOURSE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create TraningCourse Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoTraningCourseClassSkill dtoTraningCourseClassSkill) throws Exception {
		LOGGER.info("Update TraningCourse Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTraningCourseClassSkill = serviceTraningCourseClassSkill.saveOrUpdate(dtoTraningCourseClassSkill);
			responseMessage=displayMessage(dtoTraningCourseClassSkill, "TRAININGCOURSE_UPDATED_SUCCESS", "TRAININGCOURSE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update TraningCourse Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoTraningCourseClassSkill dtoTraningCourse) throws Exception {
		LOGGER.info("Delete TraningCourse Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTraningCourse.getIds() != null && !dtoTraningCourse.getIds().isEmpty()) {
				DtoTraningCourseClassSkill dtoTraningCourse2 = serviceTraningCourseClassSkill.delete(dtoTraningCourse.getIds());
				
				if(dtoTraningCourse2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("TRAININGCOURSE_DELETED", false), dtoTraningCourse2);
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoTraningCourse2.getMessageType());
					btiMessageHcm.setMessageShort("TRAININGCOURSE_NOT_DELETE_ID_MESSAGE");
					
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							btiMessageHcm, dtoTraningCourse2);
				}
				

				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete TraningCourse Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search TraningCourse Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTraningCourseClassSkill.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "TRAININGCOURSE_GET_ALL", "TRAININGCOURSE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TraningCourse Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getTraningCourseById(HttpServletRequest request, @RequestBody DtoTraningCourseClassSkill dtoTraningCourseClassSkill) throws Exception {
		LOGGER.info("Get TraningCourse ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTraningCourseClassSkill dtoTraningCourseObj = serviceTraningCourseClassSkill.getById(dtoTraningCourseClassSkill.getId());
			responseMessage=displayMessage(dtoTraningCourseObj, "TRAININGCOURSE_GET_ALL", "TRAININGCOURSE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get TraningCourse by Id Method:"+dtoTraningCourseClassSkill.getId());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/searchClassId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchClassId(@RequestBody DtoTraningCourseClassSkill dtoTraningCourseClassSkill, HttpServletRequest request) throws Exception {
		LOGGER.info(" searchClassId Method");
		ResponseMessage responseMessage = null;
		DtoSearch dtoSearch = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTraningCourseClassSkill.searchClassId(dtoTraningCourseClassSkill.getId());
			responseMessage=displayMessage(dtoSearch, "CLASS_ID_GET_ALL", "CLASS_ID_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Search searchClassId Method:"+dtoTraningCourseClassSkill.getId());
		return responseMessage;
	}
	
	
	
}
