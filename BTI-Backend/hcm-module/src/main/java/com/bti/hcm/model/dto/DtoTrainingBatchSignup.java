package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.TrainingBatchSignup;
import com.bti.hcm.util.UtilRandomKey;

public class DtoTrainingBatchSignup extends DtoBase{
	private Integer id;
	private String selectedEmployes;
	private Date completeDate;
	private String courseComments;
	private Short trainingClassStatus;
	private List<DtoEmployeeMaster> employeeMaster;
	private DtoEmployeeMaster employeeMasteres;
	private DtoTrainingCourseDetail trainingClass;
	private List<DtoTrainingCourseDetail> trainingClassList;
	private DtoTraningCourse traningCourse;
	
	private List<DtoTrainingBatchSignup> delete;
	private Boolean completed;
	private List<DtoTrainingCourseDetail> dtoTrainingCourseDetail;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSelectedEmployes() {
		return selectedEmployes;
	}

	public void setSelectedEmployes(String selectedEmployes) {
		this.selectedEmployes = selectedEmployes;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public String getCourseComments() {
		return courseComments;
	}

	public void setCourseComments(String courseComments) {
		this.courseComments = courseComments;
	}

	public Short getTrainingClassStatus() {
		return trainingClassStatus;
	}

	public void setTrainingClassStatus(Short trainingClassStatus) {
		this.trainingClassStatus = trainingClassStatus;
	}

	public List<DtoTrainingBatchSignup> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoTrainingBatchSignup> delete) {
		this.delete = delete;
	}

	public DtoTrainingBatchSignup() {
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public List<DtoTrainingCourseDetail> getDtoTrainingCourseDetail() {
		return dtoTrainingCourseDetail;
	}

	public void setDtoTrainingCourseDetail(List<DtoTrainingCourseDetail> dtoTrainingCourseDetail) {
		this.dtoTrainingCourseDetail = dtoTrainingCourseDetail;
	}

	public DtoTrainingBatchSignup(TrainingBatchSignup trainingBatchSignup) {
		this.id = trainingBatchSignup.getId();

		if (UtilRandomKey.isNotBlank(trainingBatchSignup.getSelectedEmployes())) {
			this.selectedEmployes = trainingBatchSignup.getSelectedEmployes();
		} else {
			this.selectedEmployes = "";
		}
		if (UtilRandomKey.isNotBlank(trainingBatchSignup.getCourseComments())) {
			this.courseComments = trainingBatchSignup.getCourseComments();
		} else {
			this.courseComments = "";
		}

	}

	public List<DtoEmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<DtoEmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public DtoTrainingCourseDetail getTrainingClass() {
		return trainingClass;
	}

	public void setTrainingClass(DtoTrainingCourseDetail trainingClass) {
		this.trainingClass = trainingClass;
	}

	

	public DtoTraningCourse getTraningCourse() {
		return traningCourse;
	}

	public void setTraningCourse(DtoTraningCourse traningCourse) {
		this.traningCourse = traningCourse;
	}

	public DtoEmployeeMaster getEmployeeMasteres() {
		return employeeMasteres;
	}

	public void setEmployeeMasteres(DtoEmployeeMaster employeeMasteres) {
		this.employeeMasteres = employeeMasteres;
	}

	public List<DtoTrainingCourseDetail> getTrainingClassList() {
		return trainingClassList;
	}

	public void setTrainingClassList(List<DtoTrainingCourseDetail> trainingClassList) {
		this.trainingClassList = trainingClassList;
	}

	
	
	

}
