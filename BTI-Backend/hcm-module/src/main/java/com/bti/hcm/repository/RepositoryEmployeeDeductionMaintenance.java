package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
	
@Repository("/repositoryEmployeeDeductionMaintenance")
public interface RepositoryEmployeeDeductionMaintenance  extends JpaRepository<EmployeeDeductionMaintenance, Integer>{

	EmployeeDeductionMaintenance findByIdAndIsDeleted(Integer id, boolean b);

	EmployeeDeductionMaintenance saveAndFlush(EmployeeDeductionMaintenance employeeDeductionMaintenance);

	EmployeeDeductionMaintenance findOne(Integer planId);


	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDeductionMaintenance e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.id =:id ")
	public void deleteSingleEmployeeDeductionMaintenance(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDeductionMaintenance e set e.startDate =:startDate ,e.endDate =:endDate,e.inactive=:inactive  where e.id =:id ")
	public void deleteSingleEmployeeDeductionMaintenance1(@Param("startDate") Date startDate, @Param("endDate") Date endDate,@Param("inactive")boolean inactive, @Param("id") Integer id);


	@Query("select count(*) from EmployeeDeductionMaintenance e where (e.employeeMaster.employeeId LIKE :searchKeyWord or e.deductionCode.diductionId LIKE :searchKeyWord) and e.isDeleted=false")
	Integer predictiveEmployeeDeductionMaintenanceSearchTotalCount(@Param("searchKeyWord")  String searchKeyWord);

	
	@Query("select e from EmployeeDeductionMaintenance e where e.isDeleted=false")
	List<EmployeeDeductionMaintenance> findByIsDeleted(Pageable pageable);
	
	
	@Query("select count(*) from EmployeeDeductionMaintenance e where e.isDeleted=false")
	public Integer getCountOfTotalEmployeeDeductionMaintenance();
	
	@Query("select d from EmployeeDeductionMaintenance d where (d.employeeMaster.employeeId LIKE :searchKeyWord or d.deductionCode.diductionId LIKE :searchKeyWord) and d.isDeleted=false")
	List<EmployeeDeductionMaintenance> predictiveEmployeeDeductionMaintenanceSearchWithPagination(@Param("searchKeyWord")String string,Pageable pageable);

	
	@Query("select e from EmployeeDeductionMaintenance e where e.employeeMaster.employeeIndexId =:id and e.isDeleted=false")
	List<EmployeeDeductionMaintenance> getByEmployeeId(@Param("id") Integer id);
	

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDeductionMaintenance e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.employeeMaster.employeeIndexId =:id")
	public void deleteByEmployeeIdAndCodeId(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer empId);

	@Query("select count(*) from EmployeeDeductionMaintenance e ")
	public Integer getCountOfTotalEmployeeDeductionMaintenances();
	
	
	@Query("select m from EmployeeDeductionMaintenance m where m.employeeMaster.employeeIndexId  =:id and m.isDeleted=false")
	List<EmployeeDeductionMaintenance> findByEmployeeId(@Param("id") int id);
	
	@Query("select m from EmployeeDeductionMaintenance m where m.employeeMaster.employeeIndexId  =:id and m.isDeleted=false and (DATE(m.startDate) <= :startDate and DATE(m.endDate) >= :endDate)")
	List<EmployeeDeductionMaintenance> findByEmployeeId1(@Param("id") int id,@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	
	@Query("select m from EmployeeDeductionMaintenance m where m.employeeMaster.employeeIndexId  =:id and m.isDeleted=false and (DATE(m.startDate) >= :startDate and DATE(m.endDate) <= :endDate)")
	List<EmployeeDeductionMaintenance> findByEmployeeId2(@Param("id") int id,@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	/*//replica
	@Query("select m from EmployeeDeductionMaintenance m where (m.id between m.startDate and m.endDate) and m.employeeMaster.employeeIndexId  =:id and m.isDeleted=false")
	List<EmployeeDeductionMaintenance> findByEmployeeIdHm(@Param("id") int id);*/

	List<EmployeeDeductionMaintenance> findByIsDeletedAndInactive(boolean b, boolean c);
	
	@Query("select m from EmployeeDeductionMaintenance m where m.deductionCode.id  =:deductionId and (m.startDate >= :startDate and m.endDate <= :endDate) and m.isDeleted=false")
	public List<EmployeeDeductionMaintenance> findByBatchIdAndStartDateAndEndDate(@Param("deductionId") Integer deductionId, @Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	@Query("select m from EmployeeDeductionMaintenance m where m.deductionCode.id  =:deductionId and m.isDeleted=false")
	public List<EmployeeDeductionMaintenance> findByBatchId(@Param("deductionId") Integer deductionId);
	
	/*@Query("select m.deductionCode from EmployeeDeductionMaintenance m where (DATE(m.startDate) >= :startDate or DATE(m.endDate) <= :endDate) and m.inactive=false and m.isDeleted=false")
	List<DeductionCode> findByStartEndDate(@Param("startDate")Date startDate,@Param("endDate") Date endDate);*/
	
	EmployeeDeductionMaintenance findByEmployeeMasterEmployeeIndexIdAndDeductionCodeId(int empId, int deductionId);
	
	@Query("select e from EmployeeDeductionMaintenance e where e.employeeMaster.employeeIndexId =:employeeId and e.deductionCode.id =:deductionCode and e.isDeleted=false")
	List<EmployeeDeductionMaintenance> validate(@Param("employeeId")Integer employeeId,@Param("deductionCode")Integer deductionCode);
	
	@Query("select m.deductionCode from EmployeeDeductionMaintenance m where (DATE(m.startDate) <= :startDate and DATE(m.endDate) >= :endDate) and m.inactive=false and m.isDeleted=false")
	List<DeductionCode> findByStartEndDate(@Param("startDate")Date startDate,@Param("endDate") Date endDate);

	@Query("select m.deductionCode from EmployeeDeductionMaintenance m where m.deductionCode.id = :id and m.inactive=false and m.isDeleted=false")
	List<DeductionCode> findByDeductionCode(@Param("id") Integer id);
	
	@Query("select m.deductionCode from EmployeeDeductionMaintenance m where m.inactive=false and m.isDeleted=false")
	List<DeductionCode> findByDeductionCode1();
	//List<DeductionCode> findByStartDateAfterAndEndDateBefore(Date startDate,Date endDate);
	
	
	@Query("select m from EmployeeDeductionMaintenance m where m.inactive=false and m.isDeleted=false")
	List<EmployeeDeductionMaintenance> findByDeductionCode2();
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDeductionMaintenance e set e.inactive=:inactive where e.id =:id ")
	public void deleteSingleEmployeeDeductionMaintenance2(@Param("inactive")boolean inactive,@Param("id") Integer id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDeductionMaintenance e set e.noOfDays=:noOfDays where e.id =:id ")
	public void deleteSingleEmployeeDeductionMaintenance3(@Param("noOfDays")Integer noOfDays,
			@Param("id") Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDeductionMaintenance e set e.endDateDays=:endDateDays where e.id =:id ")
	public void deleteSingleEmployeeDeductionMaintenance4(@Param("endDateDays")Integer endDateDays,
			@Param("id") Integer id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDeductionMaintenance e set e.transactionRequired=:transactionRequired where e.id =:id ")
	public void deleteSingleEmployeeDeductionMaintenance5(@Param("transactionRequired")Boolean transactionRequired,
			@Param("id") Integer id);

	@Query("select m.deductionCode from EmployeeDeductionMaintenance m where ((m.startDate) <= :startDate and (m.endDate) >= :endDate) and m.inactive=false and m.isDeleted=false")
	List<DeductionCode> findByStartEndDate1(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	@Query(value="select m.deductionCode from EmployeeDeductionMaintenance m where m.startDate between :startDate and  :endDate and m.inactive=false and m.isDeleted=false")
	List<DeductionCode> findByStartEndDate2(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	

	@Query(value="select m.deductionCode from EmployeeDeductionMaintenance m where m.endDate between :startDate and  :endDate and m.inactive=false and m.isDeleted=false")
	List<DeductionCode> findByStartEndDate3(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	
	
	@Query("select   m.deductionCode from EmployeeDeductionMaintenance m where ((m.startDate) >= :startDate or (m.endDate) >= :endDate) and m.inactive=false and m.isDeleted=false")
	List<DeductionCode> findByStartEndDate4(@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	@Query("select  m from EmployeeDeductionMaintenance m where  (DATE(m.startDate) >= :startDate or DATE(m.endDate) >= :endDate) and m.employeeMaster.employeeIndexId  =:id and m.isDeleted=false")
	List<EmployeeDeductionMaintenance> findByEmployeeId3(@Param("id") int id,@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	
	@Query("select d from EmployeeDeductionMaintenance d where d.deductionCode.id=:id and d.startDate <= d.endDate and (d.endDate >= :endDate or d.endDate between :startDate and :endDate) and d.startDate < :endDate and d.isDeleted=false")
	List<EmployeeDeductionMaintenance> findByStartDateAfterAndEndDateBeforeAndDeductionId(@Param("id")Integer id,@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
	
	
	
	
	
}
