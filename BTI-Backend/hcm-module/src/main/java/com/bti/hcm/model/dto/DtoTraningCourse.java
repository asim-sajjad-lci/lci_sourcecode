package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.util.UtilRandomKey;

public class DtoTraningCourse extends DtoBase {

	private Integer id;
	private String traningId;
	private String desc;
	private String arbicDesc;
	private String location;
	private String prerequisiteId;
	private boolean courseInCompany;
	private BigDecimal employeeCost;
	private BigDecimal employerCost;
	private BigDecimal supplierCost;

	private short status;
	private BigDecimal instructorCost;
	private List<DtoTrainingCourseDetail> subItems;

	private List<DtoTraningCourse> delete;

	private Set<DtoEmployeeMaster> employeeMasters;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTraningId() {
		return traningId;
	}

	public void setTraningId(String traningId) {
		this.traningId = traningId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPrerequisiteId() {
		return prerequisiteId;
	}

	public void setPrerequisiteId(String prerequisiteId) {
		this.prerequisiteId = prerequisiteId;
	}

	public boolean isCourseInCompany() {
		return courseInCompany;
	}

	public void setCourseInCompany(boolean courseInCompany) {
		this.courseInCompany = courseInCompany;
	}

	public BigDecimal getEmployeeCost() {
		return employeeCost;
	}

	public void setEmployeeCost(BigDecimal employeeCost) {
		this.employeeCost = employeeCost;
	}

	public BigDecimal getEmployerCost() {
		return employerCost;
	}

	public void setEmployerCost(BigDecimal employerCost) {
		this.employerCost = employerCost;
	}

	public BigDecimal getSupplierCost() {
		return supplierCost;
	}

	public void setSupplierCost(BigDecimal supplierCost) {
		this.supplierCost = supplierCost;
	}

	public BigDecimal getInstructorCost() {
		return instructorCost;
	}

	public void setInstructorCost(BigDecimal instructorCost) {
		this.instructorCost = instructorCost;
	}

	
	public DtoTraningCourse() {
	}

	public DtoTraningCourse(TraningCourse traningCourse) {
		this.id = traningCourse.getId();

		if (UtilRandomKey.isNotBlank(traningCourse.getTraningId())) {
			this.traningId = traningCourse.getTraningId();
		} else {
			this.traningId = "";
		}
		if (UtilRandomKey.isNotBlank(traningCourse.getDesc())) {
			this.desc = traningCourse.getDesc();
		} else {
			this.desc = "";
		}
		if (UtilRandomKey.isNotBlank(traningCourse.getArbicDesc())) {
			this.arbicDesc = traningCourse.getArbicDesc();
		} else {
			this.arbicDesc = "";
		}

		if (UtilRandomKey.isNotBlank(traningCourse.getLocation())) {
			this.location = traningCourse.getLocation();
		} else {
			this.location = "";
		}

		if (UtilRandomKey.isNotBlank(traningCourse.getPrerequisiteId())) {
			this.prerequisiteId = traningCourse.getPrerequisiteId();
		} else {
			this.prerequisiteId = "";
		}
	}

	public List<DtoTraningCourse> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoTraningCourse> delete) {
		this.delete = delete;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public List<DtoTrainingCourseDetail> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<DtoTrainingCourseDetail> subItems) {
		this.subItems = subItems;
	}

	

	public Set<DtoEmployeeMaster> getEmployeeMasters() {
		return employeeMasters;
	}

	public void setEmployeeMasters(Set<DtoEmployeeMaster> employeeMasters) {
		this.employeeMasters = employeeMasters;
	}

}
