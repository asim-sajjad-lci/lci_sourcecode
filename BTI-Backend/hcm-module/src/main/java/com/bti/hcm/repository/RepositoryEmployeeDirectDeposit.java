package com.bti.hcm.repository;

import java.util.LinkedHashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeDirectDeposit;


@Repository("repositoryEmployeeDirectDeposit")
public interface RepositoryEmployeeDirectDeposit extends JpaRepository<EmployeeDirectDeposit, Integer>{

	EmployeeDirectDeposit findByIdAndIsDeleted(Integer id, boolean b);

	EmployeeDirectDeposit saveAndFlush(EmployeeDirectDeposit employeeDirectDeposit);

	EmployeeDirectDeposit findOne(Integer employeeDirectDepositId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDirectDeposit e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.id =:id ")
	public void deleteSingleEmployeeDirectDeposit(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from EmployeeDirectDeposit e where (( e.bankName like :searchKeyWord or e.accountNumber like :searchKeyWord or e.employeeMaster.employeeId like :searchKeyWord or  e.employeeMaster.employeeFirstName like :searchKeyWord) and e.isDeleted=false)")
	Integer predictiveEmployeeDirectDepositSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select e from EmployeeDirectDeposit e where (( e.bankName like :searchKeyWord or e.accountNumber like :searchKeyWord or e.employeeMaster.employeeId like :searchKeyWord or  e.employeeMaster.employeeFirstName like :searchKeyWord) and e.isDeleted=false)")
	List<EmployeeDirectDeposit> predictiveEmployeeDirectDepositSearchWithPaginationNew(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);
	
	@Query("select e from EmployeeDirectDeposit e where ( e.bankName like :searchKeyWord or e.accountNumber like :searchKeyWord) and e.isDeleted=false")
	List<EmployeeDirectDeposit> predictiveEmployeeDirectDepositSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select e from EmployeeDirectDeposit e where ( e.employeeMaster.employeeIndexId =:id) and e.isDeleted=false")
	List<EmployeeDirectDeposit> getEmployeeMaster(@Param("id")Integer id);
	
	@Query("select  e.employeeMaster.employeeId from EmployeeDirectDeposit e where e.isDeleted=false order by e.id desc")
	LinkedHashSet<String> getAllDisctint();


	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDirectDeposit e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.employeeMaster.employeeIndexId =:id ")
	void deleteByEmployeeId(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,@Param("id")Integer searchKeyWord);

	@Query("select count(*) from EmployeeDirectDeposit e ")
	public Integer getCountOfTotalEmployeeDirectDeposit();

}
