package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeContacts;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeContacts;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryEmployeeContacts;
import com.bti.hcm.repository.RepositoryEmployeeMaster;

@Service("serviceEmployeeContacts")
public class ServiceEmployeeContacts {


	/**
	 * @Description LOGGER use for put a logger in EmployeeContacts Service
	 */
	static Logger log = Logger.getLogger(ServiceEmployeeContacts.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */
	@Autowired
	ServiceResponse response;
	
	
	/**
	 * @Description RepositoryTimeCode Autowired here using annotation of spring for access of RepositoryTimeCode method in TimeCode service
	 */
	@Autowired(required=false)
	RepositoryEmployeeContacts repositoryEmployeeContacts;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	
	/**
	 * @param dtoEmployeeContacts
	 * @return
	 */
	public DtoEmployeeContacts saveOrUpdate(DtoEmployeeContacts dtoEmployeeContacts1) {
		
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userids"));
			log.info("saveOrUpdate EmployeeContacts Method");
			if(dtoEmployeeContacts1.getListContact().get(0)!=null && dtoEmployeeContacts1.getListContact().get(0).getEmployeeMaster().getEmployeeIndexId()>0) {
					repositoryEmployeeContacts.deleleByEmployeeId(true, loggedInUserId,dtoEmployeeContacts1.getListContact().get(0).getEmployeeMaster().getEmployeeIndexId());
			}
			
			dtoEmployeeContacts1 = 	saveData(dtoEmployeeContacts1, loggedInUserId);
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoEmployeeContacts1;
	
	}
	
	
	public DtoEmployeeContacts update(DtoEmployeeContacts dtoEmployeeContacts1) {
		
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			dtoEmployeeContacts1 = 	saveData(dtoEmployeeContacts1, loggedInUserId);
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoEmployeeContacts1;
	
	}
	
	/**
	 * @param ids
	 * @return
	 */
	public DtoEmployeeContacts delete(List<Integer> ids) {
		log.info("delete EmployeeContacts Method");
		DtoEmployeeContacts dtoEmployeeContacts = new DtoEmployeeContacts();
		dtoEmployeeContacts
				.setDeleteMessage(response.getStringMessageByShortAndIsDeleted("TIME_CODE_DELETED", false));
		dtoEmployeeContacts.setAssociateMessage(
				response.getStringMessageByShortAndIsDeleted("TIME_CODE_ASSOCIATED", false));
		List<DtoEmployeeContacts> delete = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer employeeContactsId : ids) {
				EmployeeContacts payCode = repositoryEmployeeContacts.findOne(employeeContactsId);
				if(payCode!=null) {
					DtoEmployeeContacts dtoEmployeeContacts1 = new DtoEmployeeContacts();
					dtoEmployeeContacts1.setId(payCode.getId());
					repositoryEmployeeContacts.deleteSingleEmployeeContacts(true, loggedInUserId, employeeContactsId);
					delete.add(dtoEmployeeContacts1);
	
				}else {
					inValidDelete = true;
				}
				
			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoEmployeeContacts.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoEmployeeContacts.setMessageType("");
				
			}
			
			dtoEmployeeContacts.setDelete(delete);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeeContacts :"+dtoEmployeeContacts.getId());
		return dtoEmployeeContacts;
	}
	
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search EmployeeContacts Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			dtoSearch = getDtoSearch(dtoSearch);
			dtoSearch.setTotalCount(this.repositoryEmployeeContacts.predictiveEmployeeContactsSearchTotalCount("%"+searchWord+"%"));
			List<EmployeeContacts> employeeContactsList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					employeeContactsList = this.repositoryEmployeeContacts.predictiveEmployeeContactsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					employeeContactsList = this.repositoryEmployeeContacts.predictiveEmployeeContactsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					employeeContactsList = this.repositoryEmployeeContacts.predictiveEmployeeContactsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
				}
				
			}
		
			if(employeeContactsList != null && !employeeContactsList.isEmpty()){
				List<DtoEmployeeContacts> dtoEmployeeContactsList = new ArrayList<>();
				DtoEmployeeContacts dtoEmployeeContacts=null;
				for (EmployeeContacts employeeContacts : employeeContactsList) {
					dtoEmployeeContacts = new DtoEmployeeContacts(employeeContacts);
					dtoEmployeeContacts.setId(employeeContacts.getId());
					dtoEmployeeContacts = getDtoEmployeeContacts(dtoEmployeeContacts,employeeContacts);
					dtoEmployeeContactsList.add(dtoEmployeeContacts);
				}
				dtoSearch.setRecords(dtoEmployeeContactsList);
			}
		}
		return dtoSearch;
	}
	
	public DtoSearch getDtoSearch(DtoSearch dtoSearch) {
		String condition="";
		
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			if(dtoSearch.getSortOn().equals("employeeMaster") ||
					dtoSearch.getSortOn().equals("employeeDepedentSequence") || 
					dtoSearch.getSortOn().equals("employeeContactSequence") ||
					dtoSearch.getSortOn().equals("employeeContactNameArabic") ||
					dtoSearch.getSortOn().equals("employeeContactNameRelationship") ||
					dtoSearch.getSortOn().equals("employeeContactHomePhone") ||
					dtoSearch.getSortOn().equals("employeeContactMobilePhone") ||
					dtoSearch.getSortOn().equals("employeeContactWorkPhone") ||
					dtoSearch.getSortOn().equals("employeeContactAddress") ||
					dtoSearch.getSortOn().equals("employeeContactWorkPhoneExt") ||
					dtoSearch.getSortOn().equals("employeeContactName")
					) {
				condition = dtoSearch.getSortOn();
			
		}else {
			condition = "id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
		}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
			
		}
		dtoSearch.setCondition(condition);
		return dtoSearch;
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getContectByEmployeeId(DtoSearch dtoSearch) {
		log.info("search EmployeeContacts Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			dtoSearch = getDtoSearch(dtoSearch);
			dtoSearch.setTotalCount(this.repositoryEmployeeContacts.getContectByEmployeeIdCount("%"+searchWord+"%",dtoSearch.getId()));
			List<EmployeeContacts> employeeContactsList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					employeeContactsList = this.repositoryEmployeeContacts.getContectByEmployeeId("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					employeeContactsList = this.repositoryEmployeeContacts.getContectByEmployeeId("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					employeeContactsList = this.repositoryEmployeeContacts.getContectByEmployeeId("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
				}
				
			}
		
			if(employeeContactsList != null && !employeeContactsList.isEmpty()){
				List<DtoEmployeeContacts> dtoEmployeeContactsList = new ArrayList<>();
				DtoEmployeeContacts dtoEmployeeContacts=null;
				for (EmployeeContacts employeeContacts : employeeContactsList) {
					dtoEmployeeContacts = new DtoEmployeeContacts(employeeContacts);
					dtoEmployeeContacts = getDtoEmployeeContacts(dtoEmployeeContacts,employeeContacts);
					dtoEmployeeContactsList.add(dtoEmployeeContacts);
				}
				dtoSearch.setRecords(dtoEmployeeContactsList);
			}
		}
		return dtoSearch;
	}
	
	public DtoEmployeeContacts getDtoEmployeeContacts(DtoEmployeeContacts dtoEmployeeContacts ,EmployeeContacts employeeContacts) {
		if(employeeContacts.getEmployeeMaster()!=null) {
			
			DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
			dtoEmployeeContacts.setEmployeeMaster(dtoEmployeeMaster);
			dtoEmployeeContacts.getEmployeeMaster().setEmployeeIndexId(employeeContacts.getEmployeeMaster().getEmployeeIndexId());
			dtoEmployeeContacts.getEmployeeMaster().setEmployeeId(employeeContacts.getEmployeeMaster().getEmployeeId());
			dtoEmployeeContacts.getEmployeeMaster().setEmployeeFirstName(employeeContacts.getEmployeeMaster().getEmployeeFirstName());
			dtoEmployeeContacts.getEmployeeMaster().setEmployeeFirstNameArabic(employeeContacts.getEmployeeMaster().getEmployeeFirstNameArabic());
			dtoEmployeeContacts.getEmployeeMaster().setEmployeeLastName(employeeContacts.getEmployeeMaster().getEmployeeLastName());
			dtoEmployeeContacts.getEmployeeMaster().setEmployeeLastNameArabic(employeeContacts.getEmployeeMaster().getEmployeeLastNameArabic());

		}
		
		dtoEmployeeContacts.setEmployeeDepedentSequence(employeeContacts.getEmployeeDepedentSequence());
		dtoEmployeeContacts.setEmployeeContactSequence(employeeContacts.getEmployeeContactSequence());
		dtoEmployeeContacts.setEmployeeContactNameArabic(employeeContacts.getEmployeeContactNameArabic());
		dtoEmployeeContacts.setEmployeeContactName(employeeContacts.getEmployeeContactName());
		dtoEmployeeContacts.setEmployeeContactNameRelationship(employeeContacts.getEmployeeContactNameRelationship());
		dtoEmployeeContacts.setEmployeeContactHomePhone(employeeContacts.getEmployeeContactHomePhone());
		dtoEmployeeContacts.setEmployeeContactMobilePhone(employeeContacts.getEmployeeContactMobilePhone());
		dtoEmployeeContacts.setEmployeeContactWorkPhone(employeeContacts.getEmployeeContactWorkPhone());
		dtoEmployeeContacts.setEmployeeContactAddress(employeeContacts.getEmployeeContactAddress());
		dtoEmployeeContacts.setEmployeeContactWorkPhoneExt(employeeContacts.getEmployeeContactWorkPhoneExt());
		return dtoEmployeeContacts;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	public DtoEmployeeContacts getById(int id) {
		log.info("getById Method");
		DtoEmployeeContacts dtoEmployeeContacts = new DtoEmployeeContacts();
		if (id > 0) {
			EmployeeContacts employeeContacts = repositoryEmployeeContacts.findByIdAndIsDeleted(id, false);
			if (employeeContacts != null) {
				dtoEmployeeContacts = new DtoEmployeeContacts(employeeContacts);
				
				dtoEmployeeContacts = getDtoEmployeeContacts(dtoEmployeeContacts,employeeContacts);
				} else {
				dtoEmployeeContacts.setMessageType("TIME_CODE_NOT_GETTING");

			}
		} else {
			dtoEmployeeContacts.setMessageType("INVALID_TIME_CODE_ID");

		}
		log.debug("payCode By Id is:"+dtoEmployeeContacts.getId());
		
		return dtoEmployeeContacts;
		
	}
		

	
	public List<DtoEmployeeContacts> getAllList() {
		log.info("getAllTimeCodeInActiveList  Method");
		List<DtoEmployeeContacts> dtoEmployeeContactsList = new ArrayList<>();
		try {
			List<EmployeeContacts> list = repositoryEmployeeContacts.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (EmployeeContacts employeeContacts : list) {
					DtoEmployeeContacts dtoEmployeeContacts = new DtoEmployeeContacts();
					dtoEmployeeContacts.setId(employeeContacts.getId());
					dtoEmployeeContacts = getDtoEmployeeContacts(dtoEmployeeContacts,employeeContacts);
			
					dtoEmployeeContactsList.add(dtoEmployeeContacts);
				}
			}
			log.debug("EmployeeContacts is:" +dtoEmployeeContactsList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeContactsList;
	}
	
	public DtoEmployeeContacts saveData(DtoEmployeeContacts dtoEmployeeContacts1, int loggedInUserId) {
		for (DtoEmployeeContacts dtoEmployeeContacts : dtoEmployeeContacts1.getListContact()) {
			EmployeeContacts employeeContacts = new EmployeeContacts();
				employeeContacts.setUpdatedBy(loggedInUserId);
				employeeContacts.setUpdatedDate(new Date());
			
			EmployeeMaster employeeMaster =null;
			if(dtoEmployeeContacts.getEmployeeMaster()!=null) {
				 employeeMaster = repositoryEmployeeMaster.findOne(dtoEmployeeContacts.getEmployeeMaster().getEmployeeIndexId());
			}
			
			employeeContacts.setEmployeeMaster(employeeMaster);
			employeeContacts.setEmployeeDepedentSequence(dtoEmployeeContacts.getEmployeeDepedentSequence());
			employeeContacts.setEmployeeContactSequence(dtoEmployeeContacts.getEmployeeContactSequence());
			employeeContacts.setEmployeeContactNameArabic(dtoEmployeeContacts.getEmployeeContactNameArabic());
			employeeContacts.setEmployeeContactName(dtoEmployeeContacts.getEmployeeContactName());
			employeeContacts.setEmployeeContactNameRelationship(dtoEmployeeContacts.getEmployeeContactNameRelationship());
			employeeContacts.setEmployeeContactHomePhone(dtoEmployeeContacts.getEmployeeContactHomePhone());
			employeeContacts.setEmployeeContactMobilePhone(dtoEmployeeContacts.getEmployeeContactMobilePhone());
			employeeContacts.setEmployeeContactWorkPhone(dtoEmployeeContacts.getEmployeeContactWorkPhone());
			employeeContacts.setEmployeeContactAddress(dtoEmployeeContacts.getEmployeeContactAddress());
			employeeContacts.setEmployeeContactWorkPhoneExt(dtoEmployeeContacts.getEmployeeContactWorkPhoneExt());
			employeeContacts = repositoryEmployeeContacts.saveAndFlush(employeeContacts);
			dtoEmployeeContacts.setId(employeeContacts.getId());
			log.debug("EmployeeContact is:"+employeeContacts.getId());
			
		}
		return dtoEmployeeContacts1;
		
	}
}