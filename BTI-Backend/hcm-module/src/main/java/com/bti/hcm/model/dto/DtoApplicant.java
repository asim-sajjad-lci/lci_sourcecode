package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.Applicant;

public class DtoApplicant extends DtoBase{

	private Integer id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String address;
	private String city;
	private String country;
	private String phone1;
	private String phone2;
	private String email;
	private short gender;
	private short age;
	private Date applicantDate;
	private short status;
	private short rejectReason;
	private String rejectedComents;
	private short referalSource;
	private String referalDesc;
	private int color;
	private String managerName;
	private List<DtoApplicant> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public short getGender() {
		return gender;
	}

	public void setGender(short gender) {
		this.gender = gender;
	}

	public short getAge() {
		return age;
	}

	public void setAge(short age) {
		this.age = age;
	}

	public Date getApplicantDate() {
		return applicantDate;
	}

	public void setApplicantDate(Date applicantDate) {
		this.applicantDate = applicantDate;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(short rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getRejectedComents() {
		return rejectedComents;
	}

	public void setRejectedComents(String rejectedComents) {
		this.rejectedComents = rejectedComents;
	}

	public short getReferalSource() {
		return referalSource;
	}

	public void setReferalSource(short referalSource) {
		this.referalSource = referalSource;
	}

	public String getReferalDesc() {
		return referalDesc;
	}

	public void setReferalDesc(String referalDesc) {
		this.referalDesc = referalDesc;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}


	public List<DtoApplicant> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoApplicant> delete) {
		this.delete = delete;
	}


	public DtoApplicant() {
		
		
	}
	public DtoApplicant(Applicant applicant) {
		
	}

}
