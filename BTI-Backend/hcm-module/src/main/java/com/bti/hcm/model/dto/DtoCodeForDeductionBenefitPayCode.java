package com.bti.hcm.model.dto;

import com.bti.hcm.model.CodeForDeductionBenefitPayCode;

public class DtoCodeForDeductionBenefitPayCode extends DtoBase {

	private int id;
	private Integer codeID; 
	private String desc;

	public DtoCodeForDeductionBenefitPayCode(CodeForDeductionBenefitPayCode codeForDeductionBenefitPayCode) {

	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getCodeID() {
		return codeID;
	}

	public void setCodeID(Integer codeID) {
		this.codeID = codeID;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
