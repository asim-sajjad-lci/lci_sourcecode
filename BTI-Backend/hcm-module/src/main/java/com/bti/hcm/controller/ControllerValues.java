package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.model.dto.DtoValues;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceValues;

@RestController
@RequestMapping("/miscellaneous/values")
public class ControllerValues extends BaseController {

	private Logger log = Logger.getLogger(ControllerValues.class);

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@Autowired
	ServiceValues serviceValues;

	@PostMapping("/create")
	public ResponseMessage createMiscellaneousValueEntry(HttpServletRequest request, @RequestBody DtoValues dtoValues)
			throws Exception {
		log.info("Create Miscellaneous Entry Values Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoValues = serviceValues.saveOrUpdate(dtoValues);
			responseMessage = displayMessage(dtoValues, "MISCELLANEOUS_VALUES_CREATED",
					"MISCELLANEOUS_VALUES_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create Miscellaneous Value Entry:" + responseMessage.getMessage());
		return responseMessage;
	}

	@PostMapping("/update")
	public ResponseMessage updateMiscellaneousValueEntry(HttpServletRequest request, @RequestBody DtoValues dtoValues)
			throws Exception {
		log.info("Update Miscellaneous Values Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoValues = serviceValues.saveOrUpdate(dtoValues);
			responseMessage = displayMessage(dtoValues, "MISCELLANEOUS_VALUES_UPDATED",
					"MISCELLANEOUS_VALUES_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update Miscellaneous Value Entry Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@PostMapping("/delete")
	public ResponseMessage deleteMiscellaneousValueEntry(HttpServletRequest request, @RequestBody DtoValues dtoValues)
			throws Exception {
		log.info("Delete Miscellaneous Values Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoValues.getIds() != null && !dtoValues.getIds().isEmpty()) {
				DtoValues dtoValues2 = serviceValues.deleteMiscellaneousValue(dtoValues.getIds());

				if (dtoValues2.getMessage() != null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("MISCELLANEOUS_VALUES_DELETED", false),
							dtoValues2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("MISCELLANEOUS_VALUES_NOT_DELETED", false),
							dtoValues2);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.LIST_IS_EMPTY, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete Miscellaneous Values Entry Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@PostMapping("/getAll")
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody DtoSearchActivity dtoSearchActivity)
			throws Exception {
		log.info("Search Miscellaneous Entry Values Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchActivity = this.serviceValues.searchMiscellaneousValueEntry(dtoSearchActivity);
			responseMessage = displayMessage(dtoSearchActivity, "MISCELLANEOUS_VALUES_ALL_RECORDS_FOUND",
					"MISCELLANEOUS_VALUES_RECORDS_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@PostMapping("/getById")
	public ResponseMessage getMiscellaneousValueEntryById(HttpServletRequest request, @RequestBody DtoValues dtoValues)
			throws Exception {
		log.info("Get Miscellaneous Entry Values ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoValues dtoValuestObj = serviceValues.getById(dtoValues.getId());
			responseMessage = displayMessage(dtoValuestObj, "MISCELLANEOUS_VALUS_ENTRY_FOUND",
					"MISCELLANEOUS_VALUE_ENTRY_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get Miscellaneous Entry Values by Id Method:" + dtoValues.getId());
		return responseMessage;
	}
	
	@PostMapping("/getByMiscellaneousId")
	public ResponseMessage getMiscellaneousValueEntryByMiscellaneousId(HttpServletRequest request, @RequestBody DtoValues dtoValues)
			throws Exception {
		log.info("Get Miscellaneous Entry Values ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearchActivity dtoValuestObj = serviceValues.getByMiscellanouesId(dtoValues);
			responseMessage = displayMessage(dtoValuestObj, "MISCELLANEOUS_VALUS_ENTRY_FOUND",
					"MISCELLANEOUS_VALUE_ENTRY_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get Miscellaneous Entry Values by Id Method:" + dtoValues.getId());
		return responseMessage;
	}
	
	@PostMapping("/valuesIdCheck")
	public ResponseMessage departmetnIdCheck(HttpServletRequest request, @RequestBody DtoValues dtoValues)
			throws Exception {
		log.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoValues dtoValuesObj = serviceValues.repeatByValuesId(dtoValues.getValueId());

			if (dtoValuesObj != null && dtoValuesObj.getIsRepeat()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("MISCELLANEOUS_VALUES_ENTRY_RESULT_REPEAT", false), dtoValuesObj);
			} else {
				responseMessage = new ResponseMessage(
						HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, serviceResponse
								.getMessageByShortAndIsDeleted("MISCELLANEOUS_VALUES_ENTRY_REPEAT_NOT_FOUND", false),
						dtoValuesObj);
			}
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	
	
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping("/getAllValuesDropDown")
	@ResponseBody
	public ResponseMessage getAllValuesDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoValues> valuesList = serviceValues.getAllValuesDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("VALUES_GET_ALL", false), valuesList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
			log.error(e);
		}

		return responseMessage;
	}
}
