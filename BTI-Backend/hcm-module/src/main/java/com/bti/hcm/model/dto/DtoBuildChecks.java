package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.BuildChecks;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoBuildChecks extends DtoBase {
	private Integer id;
	private String description;
	private DtoDefault default1;
	private Date checkDate;
	private String checkByUserId;
	private Short typeofPayRun;
	private Date dateFrom;
	private Date dateTo;
	private Boolean weekly;
	private Boolean biweekly;
	private Boolean semimonthly;
	private Boolean monthly;
	private Boolean quarterly;
	private Boolean semiannually;
	private Boolean annually;
	private Boolean dailyMisc;
	private Boolean allEmployees;
	private Integer fromEmployeeId;
	private Integer toEmployeeId;
	private Boolean allDepertment;
	private Integer fromDepartmentId;
	private boolean isDisable;
	private Integer toDepartmentId;

	private List<Integer> listDepartmentId;
	private List<Integer> listEmployeeId;
	
	private List<DtoDepartment> dtoDepartments;
	private List<DtoEmployeeMasterHcm>dtoEmployeeMasters;
	
	private List<DtoBuildPayrollCheckByBenefits>dtoBuildPayrollCheckByBenefits;
	private List<DtoBenefitCode>dtoBenefitCodes;
	
	private List<DtoBuildPayrollCheckByDeductions>dtoBuildPayrollCheckByDeductions;
	private List<DtoDeductionCode> dtoDeductionCodes;
	
	private List<DtoBuildPayrollCheckByPayCodes> dtoBuildPayrollCheckByPayCodes;
	private List<DtoPayCode>dtoPayCode;

	private List<DtoBuildChecks> delete;

	public DtoBuildChecks(BuildChecks buildChecks) {

	}

	public DtoBuildChecks() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public String getCheckByUserId() {
		return checkByUserId;
	}

	public void setCheckByUserId(String checkByUserId) {
		this.checkByUserId = checkByUserId;
	}

	public Short getTypeofPayRun() {
		return typeofPayRun;
	}

	public void setTypeofPayRun(Short typeofPayRun) {
		this.typeofPayRun = typeofPayRun;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Boolean getWeekly() {
		return weekly;
	}

	public void setWeekly(Boolean weekly) {
		this.weekly = weekly;
	}

	public Boolean getBiweekly() {
		return biweekly;
	}

	public void setBiweekly(Boolean biweekly) {
		this.biweekly = biweekly;
	}

	public Boolean getSemimonthly() {
		return semimonthly;
	}

	public void setSemimonthly(Boolean semimonthly) {
		this.semimonthly = semimonthly;
	}

	public Boolean getMonthly() {
		return monthly;
	}

	public void setMonthly(Boolean monthly) {
		this.monthly = monthly;
	}

	public Boolean getQuarterly() {
		return quarterly;
	}

	public void setQuarterly(Boolean quarterly) {
		this.quarterly = quarterly;
	}

	public Boolean getSemiannually() {
		return semiannually;
	}

	public void setSemiannually(Boolean semiannually) {
		this.semiannually = semiannually;
	}

	public Boolean getAnnually() {
		return annually;
	}

	public void setAnnually(Boolean annually) {
		this.annually = annually;
	}

	public Boolean getDailyMisc() {
		return dailyMisc;
	}

	public void setDailyMisc(Boolean dailyMisc) {
		this.dailyMisc = dailyMisc;
	}

	public Boolean getAllEmployees() {
		return allEmployees;
	}

	public void setAllEmployees(Boolean allEmployees) {
		this.allEmployees = allEmployees;
	}

	public Integer getFromEmployeeId() {
		return fromEmployeeId;
	}

	public void setFromEmployeeId(Integer fromEmployeeId) {
		this.fromEmployeeId = fromEmployeeId;
	}

	public Integer getToEmployeeId() {
		return toEmployeeId;
	}

	public void setToEmployeeId(Integer toEmployeeId) {
		this.toEmployeeId = toEmployeeId;
	}

	public Boolean getAllDepertment() {
		return allDepertment;
	}

	public void setAllDepertment(Boolean allDepertment) {
		this.allDepertment = allDepertment;
	}

	public Integer getFromDepartmentId() {
		return fromDepartmentId;
	}

	public void setFromDepartmentId(Integer fromDepartmentId) {
		this.fromDepartmentId = fromDepartmentId;
	}

	public Integer getToDepartmentId() {
		return toDepartmentId;
	}

	public void setToDepartmentId(Integer toDepartmentId) {
		this.toDepartmentId = toDepartmentId;
	}

	public List<DtoBuildChecks> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoBuildChecks> delete) {
		this.delete = delete;
	}

	public DtoDefault getDefault1() {
		return default1;
	}

	public void setDefault1(DtoDefault default1) {
		this.default1 = default1;
	}

	public List<Integer> getListDepartmentId() {
		return listDepartmentId;
	}

	public void setListDepartmentId(List<Integer> listDepartmentId) {
		this.listDepartmentId = listDepartmentId;
	}

	public List<Integer> getListEmployeeId() {
		return listEmployeeId;
	}

	public void setListEmployeeId(List<Integer> listEmployeeId) {
		this.listEmployeeId = listEmployeeId;
	}

	public List<DtoDepartment> getDtoDepartments() {
		return dtoDepartments;
	}

	public void setDtoDepartments(List<DtoDepartment> dtoDepartments) {
		this.dtoDepartments = dtoDepartments;
	}

	public List<DtoEmployeeMasterHcm> getDtoEmployeeMasters() {
		return dtoEmployeeMasters;
	}

	public void setDtoEmployeeMasters(List<DtoEmployeeMasterHcm> dtoEmployeeMasters) {
		this.dtoEmployeeMasters = dtoEmployeeMasters;
	}

	public boolean isDisable() {
		return isDisable;
	}

	public void setDisable(boolean isDisable) {
		this.isDisable = isDisable;
	}

	public List<DtoBuildPayrollCheckByBenefits> getDtoBuildPayrollCheckByBenefits() {
		return dtoBuildPayrollCheckByBenefits;
	}

	public void setDtoBuildPayrollCheckByBenefits(List<DtoBuildPayrollCheckByBenefits> dtoBuildPayrollCheckByBenefits) {
		this.dtoBuildPayrollCheckByBenefits = dtoBuildPayrollCheckByBenefits;
	}

	public List<DtoBenefitCode> getDtoBenefitCodes() {
		return dtoBenefitCodes;
	}

	public void setDtoBenefitCodes(List<DtoBenefitCode> dtoBenefitCodes) {
		this.dtoBenefitCodes = dtoBenefitCodes;
	}

	public List<DtoBuildPayrollCheckByDeductions> getDtoBuildPayrollCheckByDeductions() {
		return dtoBuildPayrollCheckByDeductions;
	}

	public void setDtoBuildPayrollCheckByDeductions(
			List<DtoBuildPayrollCheckByDeductions> dtoBuildPayrollCheckByDeductions) {
		this.dtoBuildPayrollCheckByDeductions = dtoBuildPayrollCheckByDeductions;
	}

	public List<DtoDeductionCode> getDtoDeductionCodes() {
		return dtoDeductionCodes;
	}

	public void setDtoDeductionCodes(List<DtoDeductionCode> dtoDeductionCodes) {
		this.dtoDeductionCodes = dtoDeductionCodes;
	}

	public List<DtoBuildPayrollCheckByPayCodes> getDtoBuildPayrollCheckByPayCodes() {
		return dtoBuildPayrollCheckByPayCodes;
	}

	public void setDtoBuildPayrollCheckByPayCodes(List<DtoBuildPayrollCheckByPayCodes> dtoBuildPayrollCheckByPayCodes) {
		this.dtoBuildPayrollCheckByPayCodes = dtoBuildPayrollCheckByPayCodes;
	}

	public List<DtoPayCode> getDtoPayCode() {
		return dtoPayCode;
	}

	public void setDtoPayCode(List<DtoPayCode> dtoPayCode) {
		this.dtoPayCode = dtoPayCode;
	}

}
