/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ActivityLog;

/**
 * Description: Interface for ActivityLog 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryActivityLog")
public interface RepositoryActivityLog extends JpaRepository<ActivityLog, Integer>{
	
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public ActivityLog findById(int id);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from ActivityLog a where (a.services like :searchKeyWord  or a.method like :searchKeyWord )")
	public Integer predictiveActivityLogSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	
	
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from ActivityLog a ")
	public Integer predictiveActivityLogSearchTotalCounts();

	/**
	 * 
	 * @param deleted
	 * @return
	 */

	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select a from ActivityLog a where (a.services like :searchKeyWord  or a.method like :searchKeyWord or a.activity like :searchKeyWord or a.userName like :searchKeyWord or a.companyName like :searchKeyWord or a.screenName like :searchKeyWord)")
	public List<ActivityLog> predictiveActivityLogSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Query("select a from ActivityLog a where (a.createdDate>= :startDate)and (a.createdDate<= :endDate)")
	public List<ActivityLog> predictiveActivityLogSearchWithByStartDateAndEndDateAndWithPagination(@Param("startDate") Date startDate,@Param("endDate") Date endDate,Pageable pageable);
	
	
	
	
	@Query("select count(*) from ActivityLog a where (a.createdDate>= :startDate)and (a.createdDate<= :endDate)")
	public Integer predictiveActivityLogSearchTotalCountByStartDateAndEndDate(@Param("startDate") Date startDate,@Param("endDate") Date endDate);
}
