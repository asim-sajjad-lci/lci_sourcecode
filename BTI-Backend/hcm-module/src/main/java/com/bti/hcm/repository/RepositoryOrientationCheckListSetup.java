/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.OrientationCheckListSetup;

/**
 * Description: Interface for OrientationCheckListSetup 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryOrientationCheckListSetup")
public interface RepositoryOrientationCheckListSetup extends JpaRepository<OrientationCheckListSetup, Integer>{
	
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public OrientationCheckListSetup findByIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<OrientationCheckListSetup> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from OrientationCheckListSetup o where o.isDeleted=false")
	public Integer getCountOfTotalOrientationCheckListSetup();
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<OrientationCheckListSetup> findByIsDeleted(Boolean deleted, Pageable pageable);
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update OrientationCheckListSetup o set o.isDeleted =:deleted, o.updatedBy=:updateById where o.id IN (:idList)")
	public void deleteOrientationCheckListSetup(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update OrientationCheckListSetup o set o.isDeleted =:deleted ,o.updatedBy =:updateById where o.id =:id ")
	public void deleteSingleOrientationCheckListSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public OrientationCheckListSetup findTop1ByOrderByIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select o from OrientationCheckListSetup o where (o.orientationChecklistItemDescription like :searchKeyWord  or o.orientationChecklistItemDescriptionArabic like :searchKeyWord ) and o.isDeleted=false")
	public List<OrientationCheckListSetup> predictiveOrientationCheckListSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<OrientationCheckListSetup> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from OrientationCheckListSetup o where (o.orientationChecklistItemDescription like :searchKeyWord  or o.orientationChecklistItemDescriptionArabic like :searchKeyWord ) and o.isDeleted=false")
	public Integer predictiveOrientationCheckListSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select o from OrientationCheckListSetup o where (o.orientationChecklistItemDescription like :searchKeyWord  or o.orientationChecklistItemDescriptionArabic like :searchKeyWord ) and o.isDeleted=false")
	public List<OrientationCheckListSetup> predictiveOrientationCheckListSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select o from OrientationCheckListSetup o where (o.id =:id) and o.isDeleted=false")
	public List<OrientationCheckListSetup> findById(@Param("id")int id);
		
	
	@Query("select o.id from OrientationCheckListSetup o where ( o.orientationPredefinedCheckListItem.id =:checkListId and o.id not in(:ids)) and o.isDeleted=false")
	public List<Integer> findAllIdForDelete(@Param("checkListId") Integer checkListId,@Param("ids")List<Integer> ids);
	
	 @Transactional
	 @Modifying
	@Query("delete from OrientationCheckListSetup p where p.id in(:ids) and p.isDeleted=false")
	public void deleteforAllId(@Param("ids")List<Integer> ids);


	 @Query("select p from OrientationCheckListSetup p where (p.orientationPredefinedCheckListItem.id =:id) and p.isDeleted=false")
	 public List<OrientationCheckListSetup> findByOrientationCheckListSetup(@Param("id")Integer id);

	 @Query("select count(*) from OrientationCheckListSetup o ")
	public Integer getCountOfTotaOrientationCheckListSetup();

}
