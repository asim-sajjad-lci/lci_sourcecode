/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the PayScheduleSetupSchedulePeriods database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40955",indexes = {
        @Index(columnList = "PYSCHDINDXS")
})
@NamedQuery(name = "PayScheduleSetupSchedulePeriods.findAll", query = "SELECT p FROM PayScheduleSetupSchedulePeriods p")
public class PayScheduleSetupSchedulePeriods extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PYSCHDINDXS")
	private int paySchedulePeriodIndexId;
	
	@ManyToOne
	@JoinColumn(name="PYSCHDINDX")
	private PayScheduleSetup payScheduleSetup;
	
	
	@Column(name = "PERDID")
	private int periodId;
	
	@Column(name = "YEAR1")
	private int year;
	
	
	@Column(name = "PERDNMN",columnDefinition="char(21)")
	private String periodName;
	
	@Column(name = "PERDFDT")
	private Date periodStartDate;
	
	@Column(name = "PERDTDT")
	private Date periodEndDate;

	public int getPaySchedulePeriodIndexId() {
		return paySchedulePeriodIndexId;
	}

	public void setPaySchedulePeriodIndexId(int paySchedulePeriodIndexId) {
		this.paySchedulePeriodIndexId = paySchedulePeriodIndexId;
	}

	public PayScheduleSetup getPayScheduleSetup() {
		return payScheduleSetup;
	}

	public void setPayScheduleSetup(PayScheduleSetup payScheduleSetup) {
		this.payScheduleSetup = payScheduleSetup;
	}

	public int getPeriodId() {
		return periodId;
	}

	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public Date getPeriodStartDate() {
		return periodStartDate;
	}

	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	}

	public Date getPeriodEndDate() {
		return periodEndDate;
	}

	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}
	
	
	
	

}
