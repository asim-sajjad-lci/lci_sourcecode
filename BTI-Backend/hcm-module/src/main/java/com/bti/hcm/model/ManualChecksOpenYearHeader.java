package com.bti.hcm.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90400")
public class ManualChecksOpenYearHeader extends HcmBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMPYMTINDX")
	private Integer id;

	@Column(name = "HCMPYDSCR")
	private String paymentDescription;

	@Column(name = "CHEKNMBR")
	private Integer checkNumber;


	@ManyToOne
	@JoinColumn(name = "HCMBATINDX")
	@Where(clause = "is_deleted = false")
	private Batches batches;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "manualChecksOpenYearHeader")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksOpenYearDetails> listManualChecksOpenYearDetails;

	@Column(name = "EMPLOYINDX")
	private Integer employeeId;

	@Column(name = "CHEKDTA")
	private Date checkDate;

	@Column(name = "CHEKPSTDT")
	private Date checkPostDate;

	@Column(name = "TOTLAMT",precision = 10, scale = 3)
	private BigDecimal totalGrossAmount;

	@Column(name = "TOTLNETAMT",precision = 10, scale = 3)
	private BigDecimal totalNetAmount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public Integer getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public Date getCheckPostDate() {
		return checkPostDate;
	}

	public void setCheckPostDate(Date checkPostDate) {
		this.checkPostDate = checkPostDate;
	}

	public BigDecimal getTotalGrossAmount() {
		return totalGrossAmount;
	}

	public void setTotalGrossAmount(BigDecimal totalGrossAmount) {
		this.totalGrossAmount = totalGrossAmount;
	}

	public BigDecimal getTotalNetAmount() {
		return totalNetAmount;
	}

	public void setTotalNetAmount(BigDecimal totalNetAmount) {
		this.totalNetAmount = totalNetAmount;
	}

	public Batches getBatches() {
		return batches;
	}

	public void setBatches(Batches batches) {
		this.batches = batches;
	}

	public List<ManualChecksOpenYearDetails> getListManualChecksOpenYearDetails() {
		return listManualChecksOpenYearDetails;
	}

	public void setListManualChecksOpenYearDetails(List<ManualChecksOpenYearDetails> listManualChecksOpenYearDetails) {
		this.listManualChecksOpenYearDetails = listManualChecksOpenYearDetails;
	}

}
