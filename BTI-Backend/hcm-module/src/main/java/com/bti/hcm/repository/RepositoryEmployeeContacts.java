package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeContacts;

@Repository("repositoryEmployeeContacts")
public interface RepositoryEmployeeContacts extends JpaRepository<EmployeeContacts, Integer>{

	EmployeeContacts findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeContacts d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleEmployeeContacts(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from EmployeeContacts d where ( d.employeeContactName like :searchKeyWord or d.employeeContactNameArabic like :searchKeyWord or d.employeeContactNameRelationship like :searchKeyWord or d.employeeContactHomePhone like :searchKeyWord or d.employeeContactMobilePhone like :searchKeyWord or d.employeeContactWorkPhone like :searchKeyWord or d.employeeContactAddress like :searchKeyWord or d.employeeContactWorkPhoneExt like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveEmployeeContactsSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from EmployeeContacts d where ( d.employeeContactName like :searchKeyWord or d.employeeContactNameArabic like :searchKeyWord or d.employeeContactNameRelationship like :searchKeyWord or d.employeeContactHomePhone like :searchKeyWord or d.employeeContactMobilePhone like :searchKeyWord or d.employeeContactWorkPhone like :searchKeyWord or d.employeeContactAddress like :searchKeyWord or d.employeeContactWorkPhoneExt like :searchKeyWord) and d.isDeleted=false")
	List<EmployeeContacts> predictiveEmployeeContactsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	public List<EmployeeContacts> findByIsDeleted(Boolean deleted);
	
	
	@Query("select count(*) from EmployeeContacts d where ( d.employeeContactName like :searchKeyWord or d.employeeContactNameArabic like :searchKeyWord or d.employeeContactNameRelationship like :searchKeyWord or d.employeeContactHomePhone like :searchKeyWord or d.employeeContactMobilePhone like :searchKeyWord or d.employeeContactWorkPhone like :searchKeyWord or d.employeeContactAddress like :searchKeyWord or d.employeeContactWorkPhoneExt like :searchKeyWord) and d.employeeMaster.employeeIndexId =:id and d.isDeleted=false")
	Integer getContectByEmployeeIdCount(@Param("searchKeyWord") String searchKeyWord,@Param("id")Integer employeeId);

	@Query("select d from EmployeeContacts d where ( d.employeeContactName like :searchKeyWord or d.employeeContactNameArabic like :searchKeyWord or d.employeeContactNameRelationship like :searchKeyWord or d.employeeContactHomePhone like :searchKeyWord or d.employeeContactMobilePhone like :searchKeyWord or d.employeeContactWorkPhone like :searchKeyWord or d.employeeContactAddress like :searchKeyWord or d.employeeContactWorkPhoneExt like :searchKeyWord) and d.employeeMaster.employeeIndexId =:id  and d.isDeleted=false")
	List<EmployeeContacts> getContectByEmployeeId(@Param("searchKeyWord") String searchKeyWord, @Param("id")Integer employeeId, Pageable pageRequest);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeContacts d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.employeeMaster.employeeIndexId =:id ")
	void deleleByEmployeeId(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId,@Param("id")Integer employeeId);

}
