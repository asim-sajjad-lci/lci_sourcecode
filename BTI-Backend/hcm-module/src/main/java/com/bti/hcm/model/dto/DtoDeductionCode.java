package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.PayCode;

public class DtoDeductionCode extends DtoBase{

	private Integer id;
	private String diductionId;
	private String discription;
	private String arbicDiscription;
	private Date startDate;
	private Date endDate;
	private Boolean transction;
	private Short method;
	private BigDecimal amount;
	private BigDecimal percent;
	private BigDecimal payFactor;
	
	private BigDecimal perPeriod;
	private BigDecimal perYear;
	private BigDecimal lifeTime;
	private Short frequency;
	private Boolean inActive;
	private List<DtoDeductionCode> delete;
	private String arabicDepartmentDescription;
	private boolean allPaycode;
	
	private List<Integer> paycodeId;
	private List<DeductionCode> deductionCodeList;
	private List<PayCode> payCodeList;
	
	private List<DtoPayCode> dtoPayCode;
	private boolean isCustomDate;
	
	private Integer deductionId;
	private Integer employeeId;
	private Integer benefitId;
	
	private Integer noOfDays;
	private Integer endDateDays;
	
	private Integer roundOf; // Me New dtd 4Sep
	private Integer deductionTypeId; // Me New dtd 4Sep

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getRoundOf() {
		return roundOf;
	}

	public void setRoundOf(Integer roundOf) {
		this.roundOf = roundOf;
	}

	public Integer getDeductionTypeId() {
		return deductionTypeId;
	}

	public void setDeductionTypeId(Integer deductionTypeId) {
		this.deductionTypeId = deductionTypeId;
	}

	public String getDiductionId() {
		return diductionId;
	}
	public void setDiductionId(String diductionId) {
		this.diductionId = diductionId;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public String getArbicDiscription() {
		return arbicDiscription;
	}
	public void setArbicDiscription(String arbicDiscription) {
		this.arbicDiscription = arbicDiscription;
	}
	
	public Boolean getTransction() {
		return transction;
	}
	public void setTransction(Boolean transction) {
		this.transction = transction;
	}
	public Short getMethod() {
		return method;
	}
	public void setMethod(Short method) {
		this.method = method;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getPercent() {
		return percent;
	}
	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}
	public BigDecimal getPerPeriod() {
		return perPeriod;
	}
	public void setPerPeriod(BigDecimal perPeriod) {
		this.perPeriod = perPeriod;
	}
	public BigDecimal getPerYear() {
		return perYear;
	}
	public void setPerYear(BigDecimal perYear) {
		this.perYear = perYear;
	}
	public BigDecimal getLifeTime() {
		return lifeTime;
	}
	public void setLifeTime(BigDecimal lifeTime) {
		this.lifeTime = lifeTime;
	}
	public Short getFrequency() {
		return frequency;
	}
	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}
	public String getArabicDepartmentDescription() {
		return arabicDepartmentDescription;
	}
	public void setArabicDepartmentDescription(String arabicDepartmentDescription) {
		this.arabicDepartmentDescription = arabicDepartmentDescription;
	}
	public DtoDeductionCode() {

	}
	

	public Boolean getInActive() {
		return inActive;
	}
	public void setInActive(Boolean inActive) {
		this.inActive = inActive;
	}
	public List<DtoDeductionCode> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoDeductionCode> delete) {
		this.delete = delete;
	}
	public DtoDeductionCode(DeductionCode deductionCode) {
/*		this.diductionId = deductionCode.getDiductionId();
		
		if (UtilRandomKey.isNotBlank(deductionCode.getDiscription())) {
			this.discription = deductionCode.getDiscription();
		} else {
			this.discription = "";
		}
		
		if (UtilRandomKey.isNotBlank(deductionCode.getArbicDiscription())) {
			this.arbicDiscription = deductionCode.getArbicDiscription();
		} else {
			this.arbicDiscription = "";
		}*/
	}
	public List<DtoPayCode> getDtoPayCode() {
		return dtoPayCode;
	}
	public void setDtoPayCode(List<DtoPayCode> dtoPayCode) {
		this.dtoPayCode = dtoPayCode;
	}
	public boolean isAllPaycode() {
		return allPaycode;
	}
	public void setAllPaycode(boolean allPaycode) {
		this.allPaycode = allPaycode;
	}
	public BigDecimal getPayFactor() {
		return payFactor;
	}
	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}
	public List<Integer> getPaycodeId() {
		return paycodeId;
	}
	public void setPaycodeId(List<Integer> paycodeId) {
		this.paycodeId = paycodeId;
	}
	public List<DeductionCode> getDeductionCodeList() {
		return deductionCodeList;
	}
	public void setDeductionCodeList(List<DeductionCode> deductionCodeList) {
		this.deductionCodeList = deductionCodeList;
	}
	public List<PayCode> getPayCodeList() {
		return payCodeList;
	}
	
	public void setPayCodeList(List<PayCode> payCodeList) {
		this.payCodeList = payCodeList;
	}
	public boolean isCustomDate() {
		return isCustomDate;
	}
	public void setCustomeDate(boolean isCustomeDate) {
		this.isCustomDate = isCustomeDate;
	}
	public Integer getDeductionId() {
		return deductionId;
	}
	public void setDeductionId(Integer deductionId) {
		this.deductionId = deductionId;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getBenefitId() {
		return benefitId;
	}
	public void setBenefitId(Integer benefitId) {
		this.benefitId = benefitId;
	}
	public void setCustomDate(boolean isCustomDate) {
		this.isCustomDate = isCustomDate;
	}
	public Integer getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}
	public Integer getEndDateDays() {
		return endDateDays;
	}
	public void setEndDateDays(Integer endDateDays) {
		this.endDateDays = endDateDays;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
