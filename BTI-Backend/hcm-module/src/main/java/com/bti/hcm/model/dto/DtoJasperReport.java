package com.bti.hcm.model.dto;

import java.math.BigDecimal;

public class DtoJasperReport {

	private String employeeId;
	private String employeeFirstName;
	private String department;
	private String position;
	private String company;
	private String location;
	private String taxCode;
	private String bankname;
	private String accountNo;
	private BigDecimal basicSalary = new BigDecimal(0.00);
	private BigDecimal transportation = new BigDecimal(0.00);
	private BigDecimal housing = new BigDecimal(0.00);
	private BigDecimal overtime = new BigDecimal(0.00);
	private BigDecimal otherEarning = new BigDecimal(0.00);
	private BigDecimal personalLoan = new BigDecimal(0.00);
	private BigDecimal housingLoan = new BigDecimal(0.00);
	private BigDecimal violation = new BigDecimal(0.00);
	private BigDecimal gozi = new BigDecimal(0.00);
	private BigDecimal otherDeduction = new BigDecimal(0.00);
	private BigDecimal totalEarning = new BigDecimal(0.00);
	private BigDecimal totalDeduction = new BigDecimal(0.00); 
	private BigDecimal netPay = new BigDecimal(0.00);
	
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTaxCode() {
		return taxCode;
	}
	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public BigDecimal getBasicSalary() {
		return basicSalary;
	}
	public void setBasicSalary(BigDecimal basicSalary) {
		this.basicSalary = basicSalary;
	}
	public BigDecimal getTransportation() {
		return transportation;
	}
	public void setTransportation(BigDecimal transportation) {
		this.transportation = transportation;
	}
	public BigDecimal getHousing() {
		return housing;
	}
	public void setHousing(BigDecimal housing) {
		this.housing = housing;
	}
	public BigDecimal getOvertime() {
		return overtime;
	}
	public void setOvertime(BigDecimal overtime) {
		this.overtime = overtime;
	}
	public BigDecimal getOtherEarning() {
		return otherEarning;
	}
	public void setOtherEarning(BigDecimal otherEarning) {
		this.otherEarning = otherEarning;
	}
	public BigDecimal getPersonalLoan() {
		return personalLoan;
	}
	public void setPersonalLoan(BigDecimal personalLoan) {
		this.personalLoan = personalLoan;
	}
	public BigDecimal getHousingLoan() {
		return housingLoan;
	}
	public void setHousingLoan(BigDecimal housingLoan) {
		this.housingLoan = housingLoan;
	}
	public BigDecimal getViolation() {
		return violation;
	}
	public void setViolation(BigDecimal violation) {
		this.violation = violation;
	}
	public BigDecimal getGozi() {
		return gozi;
	}
	public void setGozi(BigDecimal gozi) {
		this.gozi = gozi;
	}
	public BigDecimal getOtherDeduction() {
		return otherDeduction;
	}
	public void setOtherDeduction(BigDecimal otherDeduction) {
		this.otherDeduction = otherDeduction;
	}
	public BigDecimal getTotalEarning() {
		return totalEarning;
	}
	public void setTotalEarning(BigDecimal totalEarning) {
		this.totalEarning = totalEarning;
	}
	public BigDecimal getTotalDeduction() {
		return totalDeduction;
	}
	public void setTotalDeduction(BigDecimal totalDeduction) {
		this.totalDeduction = totalDeduction;
	}
	public BigDecimal getNetPay() {
		return netPay;
	}
	public void setNetPay(BigDecimal netPay) {
		this.netPay = netPay;
	} 
	
	
}
