package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.OrientationSetup;
import com.bti.hcm.model.OrientationSetupDetail;

@Repository("repositoryOrientationSetupDetail")
public interface RepositoryOrientationSetupDetail extends JpaRepository<OrientationSetupDetail, Integer>{

	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update OrientationSetupDetail o set o.isDeleted =:deleted ,o.updatedBy =:updateById where o.id =:id ")
	public void deleteSingleOrientationSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);	
	
	@Query("select count(*) from OrientationSetupDetail o where (o.personResponsible LIKE :searchKeyWord or o.checklistitem LIKE :searchKeyWord  or o.arabicDescription LIKE :searchKeyWord) and o.isDeleted=false")
	Integer predictiveOrientationSetupDetailSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);

	
	@Query("select o from OrientationSetupDetail o where (o.personResponsible LIKE :searchKeyWord or o.checklistitem LIKE :searchKeyWord or o.arabicDescription LIKE :searchKeyWord) and o.isDeleted=false")
	List<OrientationSetup> predictiveOrientationSetupDetailSearchWithPagination(@Param("searchKeyWord")String searchKeyWord, Pageable pageable);

	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update OrientationSetupDetail o set o.isDeleted =:deleted ,o.updatedBy =:updateById where o.id =:id ")
	public void deleteSingleOrientationSetupDetail(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

}

