package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PredefinecheckList;

@Repository("repositoryPredefinecheckList")
public interface RepositoryPredefinecheckList extends JpaRepository<PredefinecheckList, Integer>{

	List<PredefinecheckList> findAll();
}
