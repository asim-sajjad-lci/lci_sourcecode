package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.MiscellaneousBenefits;
import com.bti.hcm.model.dto.DtoMiscellaneousBenefits;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryMiscellaneousBenefits;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceMiscellaneousBenefits")
public class ServiceMiscellaneousBenefits {

	/**
	 * @Description LOGGER use for put a logger in MiscellaneousBenefit Service
	 */
	static Logger log = Logger.getLogger(ServiceMiscellaneousBenefits.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in MiscellaneousBenefits service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in MiscellaneousBenefit service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in MiscellaneousBenefit service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired(required=false) 
	RepositoryMiscellaneousBenefits repositoryMiscellaneousBenefits;
	
	
	@Autowired
	ServiceResponse response;

	public DtoMiscellaneousBenefits saveOrUpdate(DtoMiscellaneousBenefits dtoHelthInsurance) {
		try {
			log.info("saveOrUpdate Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			MiscellaneousBenefits position = null;
			if (dtoHelthInsurance.getId() != null && dtoHelthInsurance.getId() > 0) {
				
				position = repositoryMiscellaneousBenefits.findByIdAndIsDeleted(dtoHelthInsurance.getId(), false);
				position.setUpdatedBy(loggedInUserId);
				position.setUpdatedDate(new Date());
			} else {
				position = new MiscellaneousBenefits();
				position.setCreatedDate(new Date());
				Integer rowId = repositoryMiscellaneousBenefits.getCountOfTotaMiscellaneousBenefits();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				position.setRowId(increment);
			}
			position.setBenefitsId(dtoHelthInsurance.getBenefitsId());
			position.setDesc(dtoHelthInsurance.getDesc());
			position.setArbicDesc(dtoHelthInsurance.getArbicDesc());
			position.setFrequency(dtoHelthInsurance.getFrequency());
			position.setStartDate(dtoHelthInsurance.getStartDate());
			position.setEndDate(dtoHelthInsurance.getEndDate());
			position.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			position.setInactive(dtoHelthInsurance.isInactive());
			position.setMethod(dtoHelthInsurance.getMethod());
			position.setDudctionAmount(dtoHelthInsurance.getDudctionAmount());
			position.setDudctionPercent(dtoHelthInsurance.getDudctionPercent());
			position.setEmployerlifetimeAmount(dtoHelthInsurance.getMonthlyAmount());
			position.setYearlyAmount(dtoHelthInsurance.getYearlyAmount());
			position.setLifetimeAmount(dtoHelthInsurance.getLifetimeAmount());
			position.setEmpluyeeerinactive(dtoHelthInsurance.isEmpluyeeerinactive());
			position.setEmpluyeeermethod(dtoHelthInsurance.getEmpluyeeermethod());
			position.setBenefitAmount(dtoHelthInsurance.getBenefitAmount());
			position.setBenefitPercent(dtoHelthInsurance.getBenefitPercent());
			position.setEmployermonthlyAmount(dtoHelthInsurance.getEmployermonthlyAmount());
			position.setEmployeryearlyAmount(dtoHelthInsurance.getEmployeryearlyAmount());
			position.setEmployerlifetimeAmount(dtoHelthInsurance.getEmployerlifetimeAmount());
			position.setMonthlyAmount(dtoHelthInsurance.getMonthlyAmount());
			position = repositoryMiscellaneousBenefits.saveAndFlush(position);
			log.debug("HelthInsurance is:"+position.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
			return dtoHelthInsurance;
		
	}

	public DtoSearch getAll() {
		log.info("get MiscellaneousBenefits Method");
		DtoMiscellaneousBenefits dtoHelthInsurance = new DtoMiscellaneousBenefits();
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoHelthInsurance.getPageNumber());
			dtoSearch.setPageSize(dtoHelthInsurance.getPageSize());
			dtoSearch.setTotalCount(repositoryMiscellaneousBenefits.getCountOfTotalHelthInsurance());
			List<MiscellaneousBenefits> helthInsuranceList = null;
			if (dtoHelthInsurance.getPageNumber() != null && dtoHelthInsurance.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoHelthInsurance.getPageNumber(), dtoHelthInsurance.getPageSize(), Direction.DESC, "createdDate");
				helthInsuranceList = repositoryMiscellaneousBenefits.findByIsDeleted(false, pageable);
			} else {
				helthInsuranceList = repositoryMiscellaneousBenefits.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoMiscellaneousBenefits> dtoHelthInsuranceList=new ArrayList<>();
			if(helthInsuranceList!=null && !helthInsuranceList.isEmpty())
			{
				for (MiscellaneousBenefits position : helthInsuranceList) 
				{
					dtoHelthInsurance=new DtoMiscellaneousBenefits(position);
					dtoHelthInsurance.setId(position.getId());
					dtoHelthInsurance.setBenefitsId(position.getBenefitsId());
					dtoHelthInsurance.setBenefitsId(position.getBenefitsId());
					dtoHelthInsurance.setDesc(position.getDesc());
					dtoHelthInsurance.setArbicDesc(position.getArbicDesc());
					dtoHelthInsurance.setFrequency(position.getFrequency());
					dtoHelthInsurance.setStartDate(position.getStartDate());
					dtoHelthInsurance.setEndDate(position.getEndDate());
					dtoHelthInsurance.setInactive(position.isInactive());
					dtoHelthInsurance.setMethod(position.getMethod());
					dtoHelthInsurance.setDudctionAmount(position.getDudctionAmount());
					dtoHelthInsurance.setDudctionPercent(position.getDudctionPercent());
					dtoHelthInsurance.setEmployerlifetimeAmount(position.getMonthlyAmount());
					dtoHelthInsurance.setYearlyAmount(position.getYearlyAmount());
					dtoHelthInsurance.setLifetimeAmount(position.getLifetimeAmount());
					dtoHelthInsurance.setEmpluyeeerinactive(position.isEmpluyeeerinactive());
					dtoHelthInsurance.setEmpluyeeermethod(position.getEmpluyeeermethod());
					dtoHelthInsurance.setBenefitAmount(position.getBenefitAmount());
					dtoHelthInsurance.setBenefitPercent(position.getBenefitPercent());
					dtoHelthInsurance.setEmployermonthlyAmount(position.getEmployermonthlyAmount());
					dtoHelthInsurance.setEmployeryearlyAmount(position.getEmployeryearlyAmount());
					dtoHelthInsurance.setEmployerlifetimeAmount(position.getEmployerlifetimeAmount());
					dtoHelthInsurance.setMonthlyAmount(position.getMonthlyAmount());
					dtoHelthInsuranceList.add(dtoHelthInsurance);
				}
				dtoSearch.setRecords(dtoHelthInsuranceList);
			}
			log.debug("All MiscellaneousBenefits  Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoMiscellaneousBenefits deleteHelthInsurance(List<Integer> ids) {
		log.info("deleteHelthInsurance Method");
		DtoMiscellaneousBenefits dtoHelthInsurance = new DtoMiscellaneousBenefits();
		dtoHelthInsurance
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("MISCELLANEOUS_BENEFIT_DELETED", false));
		dtoHelthInsurance.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("MISCELLANEOUS_BENEFIT_DELETED", false));
		List<DtoMiscellaneousBenefits> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		try {
			for (Integer positionId : ids) {
				MiscellaneousBenefits position = repositoryMiscellaneousBenefits.findOne(positionId);
				if(position.getListMiscellaneousBenefirtEnrollment().isEmpty()) {
					DtoMiscellaneousBenefits dtoskillSetup2 = new DtoMiscellaneousBenefits();
					dtoskillSetup2.setId(positionId);
					dtoskillSetup2.setDesc(position.getDesc());
					repositoryMiscellaneousBenefits.deleteSingleMiscellaneousBenefits(true, loggedInUserId, positionId);
					deletePosition.add(dtoskillSetup2);
				}else {
					dtoHelthInsurance.setMessageType("MISCELLANEOUS_BENEFIT_NOT_DELETE_ID_MESSAGE");
				}
			}
			
			dtoHelthInsurance.setDelete(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete deleteHelthInsurance :"+dtoHelthInsurance.getId());
		return dtoHelthInsurance;
	}

	public DtoMiscellaneousBenefits getById(int parseInt) {
		log.info("getById Method");
		DtoMiscellaneousBenefits dtoMiscellaneousBenefits = new DtoMiscellaneousBenefits();
		try {
			if (parseInt > 0) {
				MiscellaneousBenefits miscellaneousBenefits = repositoryMiscellaneousBenefits.findByIdAndIsDeleted(parseInt, false);
				if (miscellaneousBenefits != null) {
					dtoMiscellaneousBenefits = new DtoMiscellaneousBenefits(miscellaneousBenefits);
					
					dtoMiscellaneousBenefits.setId(miscellaneousBenefits.getId());
					dtoMiscellaneousBenefits.setBenefitsId(miscellaneousBenefits.getBenefitsId());
					dtoMiscellaneousBenefits.setDesc(miscellaneousBenefits.getDesc());
					dtoMiscellaneousBenefits.setArbicDesc(miscellaneousBenefits.getArbicDesc());
					dtoMiscellaneousBenefits.setFrequency(miscellaneousBenefits.getFrequency());
					dtoMiscellaneousBenefits.setStartDate(miscellaneousBenefits.getStartDate());
					dtoMiscellaneousBenefits.setEndDate(miscellaneousBenefits.getEndDate());
					dtoMiscellaneousBenefits.setMethod(miscellaneousBenefits.getMethod());
					dtoMiscellaneousBenefits.setDudctionAmount(miscellaneousBenefits.getDudctionAmount());
					dtoMiscellaneousBenefits.setDudctionPercent(miscellaneousBenefits.getDudctionPercent());
					dtoMiscellaneousBenefits.setEmployerlifetimeAmount(miscellaneousBenefits.getMonthlyAmount());
					dtoMiscellaneousBenefits.setInactive(miscellaneousBenefits.isInactive());
					dtoMiscellaneousBenefits.setYearlyAmount(miscellaneousBenefits.getYearlyAmount());
					dtoMiscellaneousBenefits.setLifetimeAmount(miscellaneousBenefits.getLifetimeAmount());
					dtoMiscellaneousBenefits.setEmpluyeeerinactive(miscellaneousBenefits.isEmpluyeeerinactive());
					dtoMiscellaneousBenefits.setEmpluyeeermethod(miscellaneousBenefits.getEmpluyeeermethod());
					dtoMiscellaneousBenefits.setBenefitAmount(miscellaneousBenefits.getBenefitAmount());
					dtoMiscellaneousBenefits.setBenefitPercent(miscellaneousBenefits.getBenefitPercent());
					dtoMiscellaneousBenefits.setEmployermonthlyAmount(miscellaneousBenefits.getEmployermonthlyAmount());
					dtoMiscellaneousBenefits.setEmployeryearlyAmount(miscellaneousBenefits.getEmployeryearlyAmount());
					dtoMiscellaneousBenefits.setEmployerlifetimeAmount(miscellaneousBenefits.getEmployerlifetimeAmount());
					dtoMiscellaneousBenefits.setMonthlyAmount(miscellaneousBenefits.getMonthlyAmount());
				} else {
					dtoMiscellaneousBenefits.setMessageType("POSITION_NOT_GETTING");

				}
			} else {
				dtoMiscellaneousBenefits.setMessageType("POSITION_REPEAT_ID_NOT_FOUND");

			}
			log.debug("HelthInsurance By Id is:"+dtoMiscellaneousBenefits.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneousBenefits;
	}
	
	

/*	public DtoMiscellaneousBenefits repeatByCompanyId(String positionId) {
		log.info("repeatByCompanyId Method");
		DtoMiscellaneousBenefits dtoHelthInsurance = new DtoMiscellaneousBenefits();
		try {
			List<MiscellaneousBenefits> skillsteup = null;
			if(positionId!=null) {
				skillsteup	= repositoryMiscellaneousBenefits.findByBenefitsId(positionId);
			}
			
			if(skillsteup!=null && !skillsteup.isEmpty()) {
				dtoHelthInsurance.setIsRepeat(true);
			}else {
				dtoHelthInsurance.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoHelthInsurance;
	}
*/
	
	
	
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("search Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("endDate") || dtoSearch.getSortOn().equals("startDate") 
							|| dtoSearch.getSortOn().equals("frequency") || dtoSearch.getSortOn().equals("BenefitsId") 
							||dtoSearch.getSortOn().equals("benefitsId")|| dtoSearch.getSortOn().equals("desc") || 
							dtoSearch.getSortOn().equals("arbicDesc") 
							) {
						if(dtoSearch.getSortOn().equals("benefitsId")) {
							dtoSearch.setSortOn("BenefitsId");
						}
						condition = dtoSearch.getSortOn();
					
				}else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
				dtoSearch.setTotalCount(this.repositoryMiscellaneousBenefits.predictiveMiscellaneousBenefitsSearchTotalCount("%"+searchWord+"%"));
				List<MiscellaneousBenefits> miscellaneousBenefits =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						miscellaneousBenefits = this.repositoryMiscellaneousBenefits.predictiveMiscellaneousBenefitsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						miscellaneousBenefits = this.repositoryMiscellaneousBenefits.predictiveMiscellaneousBenefitsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						miscellaneousBenefits = this.repositoryMiscellaneousBenefits.predictiveMiscellaneousBenefitsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
				
				/*dtoSearch.setTotalCount(this.repositoryMiscellaneousBenefits.predictiveMiscellaneousBenefitsSearchTotalCount("%"+searchWord+"%"));
				List<MiscellaneousBenefits> miscellaneousBenefits =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					miscellaneousBenefits = this.repositoryMiscellaneousBenefits.predictiveMiscellaneousBenefitsSearchWithPagination("%"+searchWord+"%");
				}else {
					miscellaneousBenefits = this.repositoryMiscellaneousBenefits.predictiveMiscellaneousBenefitsSearchWithPagination("%"+searchWord+"%");
				}*/
			
				if(miscellaneousBenefits != null && !miscellaneousBenefits.isEmpty()){
					List<DtoMiscellaneousBenefits> dtoMiscellaneousBenefitsLists = new ArrayList<>();
					DtoMiscellaneousBenefits dtoMiscellaneousBenefits=null;
					for (MiscellaneousBenefits helthInsurance : miscellaneousBenefits) {
						dtoMiscellaneousBenefits = new DtoMiscellaneousBenefits(helthInsurance);
						dtoMiscellaneousBenefits.setId(helthInsurance.getId());
						dtoMiscellaneousBenefits.setBenefitsId(helthInsurance.getBenefitsId());
						dtoMiscellaneousBenefits.setDesc(helthInsurance.getDesc());
						dtoMiscellaneousBenefits.setArbicDesc(helthInsurance.getArbicDesc());
						dtoMiscellaneousBenefits.setFrequency(helthInsurance.getFrequency());
						dtoMiscellaneousBenefits.setStartDate(helthInsurance.getStartDate());
						dtoMiscellaneousBenefits.setEndDate(helthInsurance.getEndDate());
						dtoMiscellaneousBenefits.setMethod(helthInsurance.getMethod());
						dtoMiscellaneousBenefits.setDudctionAmount(helthInsurance.getDudctionAmount());
						dtoMiscellaneousBenefits.setDudctionPercent(helthInsurance.getDudctionPercent());
						dtoMiscellaneousBenefits.setEmployerlifetimeAmount(helthInsurance.getMonthlyAmount());
						dtoMiscellaneousBenefits.setInactive(helthInsurance.isInactive());
						dtoMiscellaneousBenefits.setYearlyAmount(helthInsurance.getYearlyAmount());
						dtoMiscellaneousBenefits.setLifetimeAmount(helthInsurance.getLifetimeAmount());
						dtoMiscellaneousBenefits.setEmpluyeeerinactive(helthInsurance.isEmpluyeeerinactive());
						dtoMiscellaneousBenefits.setEmpluyeeermethod(helthInsurance.getEmpluyeeermethod());
						dtoMiscellaneousBenefits.setBenefitAmount(helthInsurance.getBenefitAmount());
						dtoMiscellaneousBenefits.setBenefitPercent(helthInsurance.getBenefitPercent());
						dtoMiscellaneousBenefits.setEmployermonthlyAmount(helthInsurance.getEmployermonthlyAmount());
						dtoMiscellaneousBenefits.setEmployeryearlyAmount(helthInsurance.getEmployeryearlyAmount());
						dtoMiscellaneousBenefits.setEmployerlifetimeAmount(helthInsurance.getEmployerlifetimeAmount());
						
						dtoMiscellaneousBenefits.setMonthlyAmount(helthInsurance.getMonthlyAmount());
						dtoMiscellaneousBenefitsLists.add(dtoMiscellaneousBenefits);
					}
					dtoSearch.setRecords(dtoMiscellaneousBenefitsLists);
				}
			}
			log.debug("Search searchHelthInsurance Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
		}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchMiscellaneousId(DtoSearch dtoSearch) {
		try {
			log.info("searchMiscellaneousId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				List<MiscellaneousBenefits> divisionIdList =new ArrayList<>();
				
					divisionIdList = this.repositoryMiscellaneousBenefits.predictiveMiscellaneousIdSearchWithPagination("%"+searchWord+"%");
					List<DtoMiscellaneousBenefits> dtoMiscellaneousBenefitsLists = new ArrayList<>();
					DtoMiscellaneousBenefits dtoMiscellaneousBenefits=null;
					for (MiscellaneousBenefits miscellaneousBenefits : divisionIdList) {
						dtoMiscellaneousBenefits = new DtoMiscellaneousBenefits(miscellaneousBenefits);
						dtoMiscellaneousBenefits.setId(miscellaneousBenefits.getId());
						dtoMiscellaneousBenefits.setBenefitsId(miscellaneousBenefits.getBenefitsId());
						dtoMiscellaneousBenefits.setDesc(miscellaneousBenefits.getDesc());
						dtoMiscellaneousBenefits.setArbicDesc(miscellaneousBenefits.getArbicDesc());
						dtoMiscellaneousBenefits.setFrequency(miscellaneousBenefits.getFrequency());
						dtoMiscellaneousBenefits.setStartDate(miscellaneousBenefits.getStartDate());
						dtoMiscellaneousBenefits.setEndDate(miscellaneousBenefits.getEndDate());
						dtoMiscellaneousBenefits.setMethod(miscellaneousBenefits.getMethod());
						dtoMiscellaneousBenefits.setDudctionAmount(miscellaneousBenefits.getDudctionAmount());
						dtoMiscellaneousBenefits.setDudctionPercent(miscellaneousBenefits.getDudctionPercent());
						dtoMiscellaneousBenefits.setEmployerlifetimeAmount(miscellaneousBenefits.getMonthlyAmount());
						dtoMiscellaneousBenefits.setInactive(miscellaneousBenefits.isInactive());
						dtoMiscellaneousBenefits.setYearlyAmount(miscellaneousBenefits.getYearlyAmount());
						dtoMiscellaneousBenefits.setLifetimeAmount(miscellaneousBenefits.getLifetimeAmount());
						dtoMiscellaneousBenefits.setEmpluyeeerinactive(miscellaneousBenefits.isEmpluyeeerinactive());
						dtoMiscellaneousBenefits.setEmpluyeeermethod(miscellaneousBenefits.getEmpluyeeermethod());
						dtoMiscellaneousBenefits.setBenefitAmount(miscellaneousBenefits.getBenefitAmount());
						dtoMiscellaneousBenefits.setBenefitPercent(miscellaneousBenefits.getBenefitPercent());
						dtoMiscellaneousBenefits.setEmployermonthlyAmount(miscellaneousBenefits.getEmployermonthlyAmount());
						dtoMiscellaneousBenefits.setEmployeryearlyAmount(miscellaneousBenefits.getEmployeryearlyAmount());
						dtoMiscellaneousBenefits.setEmployerlifetimeAmount(miscellaneousBenefits.getEmployerlifetimeAmount());
						dtoMiscellaneousBenefits.setMonthlyAmount(miscellaneousBenefits.getMonthlyAmount());
						dtoMiscellaneousBenefitsLists.add(dtoMiscellaneousBenefits);
					}
					dtoSearch.setRecords(dtoMiscellaneousBenefitsLists);
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	
	
	
	public DtoMiscellaneousBenefits repeatByBenefitsId(String  benefitsId) {
		log.info("miscellaneousBenefitsIdcheck Method");
		DtoMiscellaneousBenefits dtoMiscellaneousBenefits = new DtoMiscellaneousBenefits();
		try {
			List<MiscellaneousBenefits> miscellaneousBenefits=repositoryMiscellaneousBenefits.miscellaneousBenefitsIdcheck(benefitsId);
			if(miscellaneousBenefits!=null && !miscellaneousBenefits.isEmpty()) {
				dtoMiscellaneousBenefits.setIsRepeat(true);
			}else {
				dtoMiscellaneousBenefits.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneousBenefits;
	}

	

	
	
}
