package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PositionBudget;

@Repository("repositoryPositionBudget")
public interface RepositoryPositionBudget extends JpaRepository<PositionBudget, Integer> {

	PositionBudget findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from PositionBudget s where s.isDeleted=false")
	Integer getCountOfTotalPosition();

	List<PositionBudget> findByIsDeleted(boolean b, Pageable pageable);

	List<PositionBudget> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PositionBudget d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSinglePositionBudget(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from PositionBudget d where (d.budgetDesc like :searchKeyWord or  d.budgetAmount like :searchKeyWord or d.positionPalnSetup.positionPlanDesc like :searchKeyWord or d.positionPalnSetup.positionPlanId like :searchKeyWord) and d.isDeleted=false")
	Integer predictivePositionBudgetSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select d from PositionBudget d where (d.budgetDesc like :searchKeyWord or  d.budgetAmount like :searchKeyWord or d.positionPalnSetup.positionPlanDesc like :searchKeyWord or d.positionPalnSetup.positionPlanId like :searchKeyWord) and d.isDeleted=false")
	List<PositionBudget> predictivePositionBudgetSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageRequest);

	@Query("select count(*) from Position p ")
	public Integer getCountOfTotaPositionBudget();

}
