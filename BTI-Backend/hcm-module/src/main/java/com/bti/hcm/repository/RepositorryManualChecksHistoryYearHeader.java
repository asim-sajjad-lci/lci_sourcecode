package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecksHistoryYearHeader;

@Repository("/repositorryManualChecksHistoryYearHeader")
public interface RepositorryManualChecksHistoryYearHeader extends JpaRepository<ManualChecksHistoryYearHeader, Integer>{

}
