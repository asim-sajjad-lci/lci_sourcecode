/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoInterviewTypeSetupDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceInterviewTypeSetupDetail;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerInterviewTypeSetupDetai
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/interviewTypeSetupDetail")
public class ControllerInterviewTypeSetupDetail extends BaseController{

	/**
	 * @Description LOGGER use for put a logger in InterviewTypeSetupDetail Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerInterviewTypeSetup.class);
	
	/**
	 * @Description serviceInterviewTypeSetupDetail Autowired here using annotation of spring for use of serviceInterviewTypeSetupDetail method in this controller
	 */
	@Autowired(required=true)
	ServiceInterviewTypeSetupDetail serviceInterviewTypeSetupDetail;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetupDetail
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "interviewTypeId":1,
		  "interviewTypeSetupDetailCategoryRowSequance":1,
		  "interviewTypeSetupDetailDescription":"Desc",
		  "interviewTypeSetupDetailCategorySequance":1,
		  "interviewTypeSetupDetailCategoryWeight":50,
		  "interviewTypeSetupId":1
		}
		
	 *@response
	 		{
					"code": 201,
					"status": "CREATED",
					"result": {
					"interviewTypeId": 4,
					"interviewTypeSetupDetailCategoryRowSequance": 4,
					"interviewTypeSetupDetailDescription": "Description",
					"interviewTypeSetupDetailCategorySequance": 4,
					"interviewTypeSetupDetailCategoryWeight": 100,
					"interviewTypeSetupId": 1
				},
				"btiMessage": {
					"message": "InterviewTypeSetupDetail created successfully.",
					"messageShort": "INTERVIEW_TYPE_SETUP_DETAIL_CREATED"
				}
		}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createInterviewTypeSetupDetail(HttpServletRequest request, @RequestBody DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail) throws Exception {
		LOGGER.info("Create InterviewTypeSetupDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoInterviewTypeSetupDetail = serviceInterviewTypeSetupDetail.saveOrUpdateInterviewTypeSetupDetail(dtoInterviewTypeSetupDetail);
			responseMessage=displayMessage(dtoInterviewTypeSetupDetail, "INTERVIEW_TYPE_SETUP_DETAIL_CREATED", "INTERVIEW_TYPE_SETUP_DETAIL_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create InterviewTypeSetupDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetupDetail
	 * @return
	 * @throws Exception
	 * @request 
	 * {
		  "id":1,
		  "interviewTypeId":4,
		  "interviewTypeSetupDetailCategoryRowSequance":4,
		  "interviewTypeSetupDetailDescription":"Description",
		  "interviewTypeSetupDetailCategorySequance":4,
		  "interviewTypeSetupDetailCategoryWeight":100,
		  "interviewTypeSetupId":1
		}
	 *@response
	 	{
					"code": 201,
					"status": "CREATED",
					"result": {
					"id": 1,
					"interviewTypeId": 4,
					"interviewTypeSetupDetailCategoryRowSequance": 4,
					"interviewTypeSetupDetailDescription": "Description",
					"interviewTypeSetupDetailCategorySequance": 4,
					"interviewTypeSetupDetailCategoryWeight": 100,
					"interviewTypeSetupId": 1
			},
			"btiMessage": {
				"message": "InterviewTypeSetupDetail update successfully.",
				"messageShort": "INTERVIEW_TYPE_SETUP_DETAIL_UPDATED_SUCCESS"
			}
		}
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateInterviewTypeSetupDetail(HttpServletRequest request, @RequestBody DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail) throws Exception {
		LOGGER.info("Update InterviewTypeSetupDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoInterviewTypeSetupDetail = serviceInterviewTypeSetupDetail.saveOrUpdateInterviewTypeSetupDetail(dtoInterviewTypeSetupDetail);
			responseMessage =displayMessage(dtoInterviewTypeSetupDetail, "INTERVIEW_TYPE_SETUP_DETAIL_UPDATED_SUCCESS", "INTERVIEW_TYPE_SETUP_DETAIL_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update InterviewTypeSetupDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetupDetail
	 * @return
	 * @throws Exception
	 * @request
	   {
		  "ids":[1]
		}
	 * @response
	 * {
			"code": 201,
			"status": "CREATED",
			"result": {
			"deleteMessage": "InterviewTypeSetupDetail deleted successfully.",
			"associateMessage": "N/A",
			"deleteInterviewTypeSetupDetail": [
			  		{
					"id": 1,
					"interviewTypeSetupDetailCategoryRowSequance": 4,
					"interviewTypeSetupDetailDescription": "Description",
					"interviewTypeSetupDetailCategorySequance": 4,
					"interviewTypeSetupDetailCategoryWeight": 100
					}
				],
			},
		"btiMessage": {
			"message": "InterviewTypeSetupDetail deleted successfully.",
			"messageShort": "INTERVIEW_TYPE_SETUP_DETAIL_DELETED"
		}
	}
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteInterviewTypeSetupDetail(HttpServletRequest request, @RequestBody DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail) throws Exception {
		LOGGER.info("Delete InterviewTypeSetupDetai Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoInterviewTypeSetupDetail.getIds() != null && !dtoInterviewTypeSetupDetail.getIds().isEmpty()) {
				DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail2 = serviceInterviewTypeSetupDetail.deleteInterviewTypeSetupDetail(dtoInterviewTypeSetupDetail.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("INTERVIEW_TYPE_SETUP_DETAIL_DELETED", false), dtoInterviewTypeSetupDetail2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete InterviewTypeSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 * @request
		{
		  "searchKeyword":"t",
		  "sortOn":"",
		  "sortBy":"DESC",
		 "pageNumber":"0",
		  "pageSize":"10"
		}
	 * @response
	 		{
				"code": 200,
				"status": "OK",
				"result": {
				"searchKeyword": "t",
				"pageNumber": 0,
				"pageSize": 10,
				"sortOn": "",
				"sortBy": "DESC",
				"totalCount": 2,
				"records": [
				  {
						"interviewTypeSetupDetailCategoryRowSequance": 4,
						"interviewTypeSetupDetailDescription": "Description",
						"interviewTypeSetupDetailCategorySequance": 4,
						"interviewTypeSetupDetailCategoryWeight": 100
				},
						  {
							"interviewTypeSetupDetailCategoryRowSequance": 4,
							"interviewTypeSetupDetailDescription": "Description",
							"interviewTypeSetupDetailCategorySequance": 4,
							"interviewTypeSetupDetailCategoryWeight": 100
							}
						],
					},
				"btiMessage": {
					"message": "N/A",
					"messageShort": "N/A"
					}
				}
	 
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchInterviewTypeSetupDetail(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search InterviewTypeSetupDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceInterviewTypeSetupDetail.searchInterviewTypeSetupDetail(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "INTERVIEW_TYPE_SETUP_DETAIL_GET_ALL", "INTERVIEW_TYPE_SETUP_DETAIL_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search InterviewTypeSetupDetail Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetupDetail
	 * @return
	 * @throws Exception
	 * @request
	 	{
		  "id":1
		}
	 * @response
	 	{
			"code": 201,
			"status": "CREATED",
			"result": {
				"id": 1,
				"interviewTypeSetupDetailCategoryRowSequance": 4,
				"interviewTypeSetupDetailDescription": "Description",
				"interviewTypeSetupDetailCategorySequance": 4,
				"interviewTypeSetupDetailCategoryWeight": 100
				},
			"btiMessage": {
				"message": "InterviewTypeSetupDetail Detail fetched successfully.",
				"messageShort": INTERVIEW_TYPE_SETUP_DETAIL_GET_DETAIL"
				}
			}
	 */
	@RequestMapping(value = "/getInterviewTypeSetupDetailById", method = RequestMethod.POST)
	public ResponseMessage getInterviewTypeSetupDetailById(HttpServletRequest request, @RequestBody DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail) throws Exception {
		LOGGER.info("Get InterviewTypeSetupDetail ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetailObj = serviceInterviewTypeSetupDetail.getInterviewTypeSetupDetailByInterviewTypeSetupDetailId(dtoInterviewTypeSetupDetail.getId());
			responseMessage=displayMessage(dtoInterviewTypeSetupDetailObj, "INTERVIEW_TYPE_SETUP_DETAIL_GET_DETAIL", "INTERVIEW_TYPE_SETUP_DETAIL_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get InterviewTypeSetup ById Method:"+dtoInterviewTypeSetupDetail.getId());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetupDetail
	 * @return
	 * @throws Exception
	 * @request
		{
		  "id":1
		}
	 * @response
	 	{
				"code": 302,
				"status": "FOUND",
				"result": {
				"isRepeat": false
				},
			"btiMessage": {
				"message": "InterviewTypeSetupDetail ID already exists",
				"messageShort": "INTERVIEW_TYPE_SETUP_DETAIL_RESULT"
			}
		}
	 */

	@RequestMapping(value = "/getAllInterviewTypeSetupDetailDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllInterviewTypeSetupDetailDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoInterviewTypeSetupDetail> dtoInterviewTypeSetupDetailList = serviceInterviewTypeSetupDetail.getAllInterviewTypeSetupDetailDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("INTERVIEW_TYPE_SETUP_DETAIL_GET_ALL", false), dtoInterviewTypeSetupDetailList);
			}else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return responseMessage;
	}
}
