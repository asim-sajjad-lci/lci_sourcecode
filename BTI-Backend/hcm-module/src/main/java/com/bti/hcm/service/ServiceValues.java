package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Miscellaneous;
import com.bti.hcm.model.Values;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.model.dto.DtoValues;
import com.bti.hcm.repository.RepositoryMiscellaneous;
import com.bti.hcm.repository.RepositoryValues;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceValues")
public class ServiceValues {

	static Logger log = Logger.getLogger(ServiceValues.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryValues repositoryValues;

	@Autowired
	RepositoryMiscellaneous repositoryMiscellaneous;

	/**
	 * @Description: save and update Values data
	 * @param dtoValues
	 * @return
	 *
	 */

	public DtoValues saveOrUpdate(DtoValues dtoValues) {

		log.info("Miscellaneous values save method");
		try {

			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Miscellaneous miscellaneous = repositoryMiscellaneous.findOne(dtoValues.getMiscId());
			Values values = null;
			if (dtoValues.getId() != 0 && dtoValues.getId() > 0) {
				values = repositoryValues.findByIdAndIsDeleted(dtoValues.getId(), false);
				values.setUpdatedBy(loggedInUserId);
				values.setUpdatedDate(new Date());
			} else {
				values = new Values();
				values.setCreatedDate(new Date());
				Integer rowId = repositoryValues.getCountOfTotalValuesEntries();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				values.setRowId(increment);
			}

			values.setValueId(dtoValues.getValueId());
			values.setValueDescription(dtoValues.getValueDescription());
			values.setValueArabicDescription(dtoValues.getValueArabicDescription());
			values.setMiscellaneous(miscellaneous);
			values.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryValues.saveAndFlush(values);
			dtoValues.setId(values.getId());

		} catch (Exception e) {
			log.error(e);
		}

		return dtoValues;
	}

	public DtoValues deleteMiscellaneousValue(List<Integer> ids) {
		log.info("deleteMiscellaneousValuesEntry Method");
		DtoValues dtoValues = new DtoValues();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			dtoValues.setDeleteMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("MISCELLANEOUS_VALUES_ENTRY_DELETED", false));
			dtoValues.setAssociateMessage(serviceResponse
					.getStringMessageByShortAndIsDeleted("MISCELLANEOUS_VALUES_ENTRY_ASSOCIATED", false));
			boolean inValidDelete = false;
			StringBuilder invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse
					.getMessageByShortAndIsDeleted("MISCELLANEOUS_VALUES_ENTRY_NOT_DELETE_ID_MESSAGE", false)
					.getMessage());
			List<DtoValues> deleteValues = new ArrayList<>();

			for (Integer valuesId : ids) {
				Values values = repositoryValues.findOne(valuesId);
				if (values.getEmployeeMaster().isEmpty())

				/*
				 * && miscellaneous.getListEmployeePositionHistory().isEmpty() &&
				 * miscellaneous.getRequisitions().isEmpty()&&miscellaneous.
				 * getPayScheduleSetupDepartment().isEmpty()
				 */

				{
					DtoValues dtoValues2 = new DtoValues();
					dtoValues2.setId(valuesId);
					dtoValues2.setValueArabicDescription(values.getValueArabicDescription());
					repositoryValues.deleteSingleValuesEntry(true, loggedInUserId, valuesId);
					deleteValues.add(dtoValues2);
				} else {
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoValues.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoValues.setMessageType("");

			}
			dtoValues.setDeleteMiscellaneousValues(deleteValues);

			log.debug("Delete Miscellaneous :" + dtoValues.getId());
		} catch (Exception e) {
			log.error(e);

		}
		return dtoValues;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearchActivity searchMiscellaneousValueEntry(DtoSearchActivity dtoSearchActivity) {
		log.info("searchMiscellaneousValuesEntry Method");
		try {
			if (dtoSearchActivity != null) {

				String searchWord = dtoSearchActivity.getSearchKeyword();
				dtoSearchActivity = searchByMiscellaneuosValues(dtoSearchActivity);
				dtoSearchActivity.setTotalCount(
						this.repositoryValues.predictiveValuesCodeSearchTotalCount("%" + searchWord + "%"));

				List<Values> valuesList = searchByPageSizeAndNumber(dtoSearchActivity);

				if (valuesList != null && !valuesList.isEmpty()) {
					List<DtoValues> dtoValuesList = new ArrayList<>();
					DtoValues dtoValues = null;
					for (Values values : valuesList) {
						dtoValues = new DtoValues();
						dtoValues.setId(values.getId());
						dtoValues.setValueId(values.getValueId());
						dtoValues.setValueDescription(values.getValueDescription());
						dtoValues.setValueArabicDescription(values.getValueArabicDescription());
						dtoValues.setMiscId(values.getMiscellaneous().getId());
						dtoValuesList.add(dtoValues);
					}
					dtoSearchActivity.setRecords(dtoValuesList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearchActivity;
	}

	/**
	 * 
	 * @param dtoSearchActivity
	 * @return
	 */
	public DtoSearchActivity searchByMiscellaneuosValues(DtoSearchActivity dtoSearchActivity) {

		String condition = "";

		if (dtoSearchActivity.getSortOn() != null || dtoSearchActivity.getSortBy() != null) {
			switch (dtoSearchActivity.getSortOn()) {
			case "valueId":
				condition += dtoSearchActivity.getSortOn();
				break;
			case "valueDescription":
				condition += dtoSearchActivity.getSortOn();
				break;
			case "valueArabicDescription":
				condition += dtoSearchActivity.getSortOn();
				break;
			default:
				condition += "id";
			}
		} else {
			condition += "id";
			dtoSearchActivity.setSortOn("");
			dtoSearchActivity.setSortBy("");
		}
		dtoSearchActivity.setCondition(condition);
		return dtoSearchActivity;

	}

	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public List<Values> searchByPageSizeAndNumber(DtoSearch dtoSearch) {
		List<Values> valuesList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

			if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
				valuesList = this.repositoryValues.predictiveValuesAllIdSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%",
						new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if (dtoSearch.getSortBy().equals("ASC")) {
				valuesList = this.repositoryValues.predictiveValuesAllIdSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
			} else if (dtoSearch.getSortBy().equals("DESC")) {
				valuesList = this.repositoryValues.predictiveValuesAllIdSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
			}

		}

		return valuesList;
	}

	/**
	 * @Description:get Miscellaneous values data by id
	 * @param id
	 * @return
	 */
	public DtoValues getById(int id) {
		log.info("getMiscellaneousValuesByValuesId Method");
		DtoValues dtoValues = new DtoValues();
		try {
			if (id > 0) {
				Values values = repositoryValues.findByIdAndIsDeleted(id, false);
				if (values != null) {
					dtoValues = new DtoValues();
					dtoValues.setId(values.getId());
					dtoValues.setValueId(values.getValueId());
					dtoValues.setValueDescription(values.getValueDescription());
					dtoValues.setValueArabicDescription(values.getValueArabicDescription());
					dtoValues.setMiscId(values.getMiscellaneous().getId());
				} else {
					dtoValues.setMessageType("MISCELLANEOUS_VALUE_ENTRY_NOT_FOUND");

				}
			} else {
				dtoValues.setMessageType("INVALID_MISCELLANEOUS_VALUE_ID");

			}
			log.debug("Miscellaneous Value By Value Id is:" + dtoValues.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoValues;
	}

	/*
	 * ValueId repeat check
	 * 
	 * @param valueId
	 * 
	 */
	public DtoValues repeatByValuesId(String valueId) {
		log.info("repeatByMiscellaneousValuesId Method");
		DtoValues dtoValues = new DtoValues();
		try {
			List<Values> values = repositoryValues.findByValueId(valueId.trim());
			if (values != null && !values.isEmpty()) {
				dtoValues.setIsRepeat(true);
			} else {
				dtoValues.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoValues;
	}

	public DtoSearchActivity getByMiscellanouesId(DtoValues dtoValues) {

		log.info("get by miscellaneous id method");
		DtoSearchActivity dtoSearchActivity = new DtoSearchActivity();
		try {
			if (dtoValues.getMiscId() > 0) {

				dtoSearchActivity.setTotalCount(repositoryValues.getCountOfTotalValuesEntriesByMiscellaneousId(dtoValues.getMiscId()));
				List<Values> valuesList = repositoryValues.findByMiscIdAndPagination(dtoValues.getMiscId(),
						new PageRequest(dtoValues.getPageNumber(), dtoValues.getPageSize(), Sort.Direction.DESC, "id"));

				if (valuesList != null && !valuesList.isEmpty()) {
					List<DtoValues> dtoValuesList = new ArrayList<>();
					for (Values values : valuesList) {
						dtoValues = new DtoValues();
						dtoValues.setId(values.getId());
						dtoValues.setValueId(values.getValueId());
						dtoValues.setValueDescription(values.getValueDescription());
						dtoValues.setValueArabicDescription(values.getValueArabicDescription());
						dtoValues.setMiscId(values.getMiscellaneous().getId());
						dtoValues.setCodeName(values.getMiscellaneous().getCodeName());
						dtoValuesList.add(dtoValues);
					}
					dtoSearchActivity.setRecords(dtoValuesList);
				} else {
					dtoValues.setMessageType("MISCELLANEOUS_VALUE_ENTRY_NOT_FOUND");
				}
			} else {
				dtoValues.setMessageType("INVALID_MISCELLANEOUS_VALUE_ID");
			}
			log.debug("Miscellaneous Value By Value Id is:" + dtoValues.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearchActivity;

	}

	@Transactional
	@Async
	public List<DtoValues> getAllValuesDropDownList() {
		log.info("getAllValuesList  Method");
		List<DtoValues> dtoValuesList = new ArrayList<>();
		try {
			List<Values> list = repositoryValues.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (Values values : list) {
					DtoValues dtoValues = new DtoValues();
					dtoValues.setId(values.getId());
					dtoValues.setValueId(values.getValueId());
					dtoValues.setValueDescription(values.getValueDescription());
					dtoValues.setValueArabicDescription(values.getValueArabicDescription());
					dtoValues.setMiscId(values.getMiscellaneous().getId());
					dtoValuesList.add(dtoValues);
				}
			}
			log.debug("Values is:" + dtoValuesList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoValuesList;
	}

}
