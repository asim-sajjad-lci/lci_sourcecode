package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import com.bti.hcm.model.CompnayInsurance;
import com.bti.hcm.model.RetirementEntranceDate;
import com.bti.hcm.model.RetirementFundSetup;
import com.bti.hcm.model.RetirementPlanSetup;
import com.bti.hcm.util.UtilRandomKey;

public class DtoRetirementPlanSetup extends DtoBase{
	private Integer id;
	private String retirementPlanId;

	private Integer helthInsuranceId;
	private CompnayInsurance helthInsurance;

	private Integer retirementEntranceDateId;
	private RetirementEntranceDate retirementEntranceDate;

	private Integer retirementFundSetupId;
	private RetirementFundSetup retirementFundSetup;

	private String retirementPlanDescription;
	private String retirementPlanArbicDescription;
	private String retirementPlanAccountNumber;
	private BigDecimal retirementPlanMatchPercent;
	private BigDecimal retirementPlanMaxPercent;
	private BigDecimal retirementPlanMaxAmount;
	private Boolean loans;
	private Boolean bonus;
	private Integer retirementAge;
	private Integer watingPeriods;
	private BigDecimal retirementContribution;
	private short watingMethod;
	private BigDecimal retirementPlanAmount;
	private BigDecimal retirementPlanPercent;
	private short retirementFrequency;
	
	private String insCompanyId;
	
	
	private Integer dateSequence;
	private Integer planId;
	private String entranceDescription;
	private String fundId;
	private Integer fundSequence;
	private String planDescription;
	private BigInteger fundActive;

	private List<DtoRetirementPlanSetup> delete;
	public List<DtoRetirementPlanSetup> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoRetirementPlanSetup> delete) {
		this.delete = delete;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRetirementPlanId() {
		return retirementPlanId;
	}

	public void setRetirementPlanId(String retirementPlanId) {
		this.retirementPlanId = retirementPlanId;
	}

	public String getRetirementPlanDescription() {
		return retirementPlanDescription;
	}

	public void setRetirementPlanDescription(String retirementPlanDescription) {
		this.retirementPlanDescription = retirementPlanDescription;
	}

	public String getRetirementPlanArbicDescription() {
		return retirementPlanArbicDescription;
	}

	public void setRetirementPlanArbicDescription(String retirementPlanArbicDescription) {
		this.retirementPlanArbicDescription = retirementPlanArbicDescription;
	}

	public String getRetirementPlanAccountNumber() {
		return retirementPlanAccountNumber;
	}

	public void setRetirementPlanAccountNumber(String retirementPlanAccountNumber) {
		this.retirementPlanAccountNumber = retirementPlanAccountNumber;
	}

	public BigDecimal getRetirementPlanMatchPercent() {
		return retirementPlanMatchPercent;
	}

	public void setRetirementPlanMatchPercent(BigDecimal retirementPlanMatchPercent) {
		this.retirementPlanMatchPercent = retirementPlanMatchPercent;
	}

	public BigDecimal getRetirementPlanMaxPercent() {
		return retirementPlanMaxPercent;
	}

	public void setRetirementPlanMaxPercent(BigDecimal retirementPlanMaxPercent) {
		this.retirementPlanMaxPercent = retirementPlanMaxPercent;
	}

	public BigDecimal getRetirementPlanMaxAmount() {
		return retirementPlanMaxAmount;
	}

	public void setRetirementPlanMaxAmount(BigDecimal retirementPlanMaxAmount) {
		this.retirementPlanMaxAmount = retirementPlanMaxAmount;
	}

	public Boolean getLoans() {
		return loans;
	}

	public void setLoans(Boolean loans) {
		this.loans = loans;
	}

	public Boolean getBonus() {
		return bonus;
	}

	public void setBonus(Boolean bonus) {
		this.bonus = bonus;
	}

	public Integer getRetirementAge() {
		return retirementAge;
	}

	public void setRetirementAge(Integer retirementAge) {
		this.retirementAge = retirementAge;
	}

	public Integer getWatingPeriods() {
		return watingPeriods;
	}

	public void setWatingPeriods(Integer watingPeriods) {
		this.watingPeriods = watingPeriods;
	}

	public BigDecimal getRetirementContribution() {
		return retirementContribution;
	}

	public void setRetirementContribution(BigDecimal retirementContribution) {
		this.retirementContribution = retirementContribution;
	}

	public short getWatingMethod() {
		return watingMethod;
	}

	public void setWatingMethod(short watingMethod) {
		this.watingMethod = watingMethod;
	}

	public BigDecimal getRetirementPlanAmount() {
		return retirementPlanAmount;
	}

	public void setRetirementPlanAmount(BigDecimal retirementPlanAmount) {
		this.retirementPlanAmount = retirementPlanAmount;
	}

	public BigDecimal getRetirementPlanPercent() {
		return retirementPlanPercent;
	}

	public void setRetirementPlanPercent(BigDecimal retirementPlanPercent) {
		this.retirementPlanPercent = retirementPlanPercent;
	}

	public short getRetirementFrequency() {
		return retirementFrequency;
	}

	public void setRetirementFrequency(short retirementFrequency) {
		this.retirementFrequency = retirementFrequency;
	}

	
	public Integer getHelthInsuranceId() {
		return helthInsuranceId;
	}

	public void setHelthInsuranceId(Integer helthInsuranceId) {
		this.helthInsuranceId = helthInsuranceId;
	}

	public CompnayInsurance getHelthInsurance() {
		return helthInsurance;
	}

	public void setHelthInsurance(CompnayInsurance helthInsurance) {
		this.helthInsurance = helthInsurance;
	}

	public Integer getRetirementEntranceDateId() {
		return retirementEntranceDateId;
	}

	public void setRetirementEntranceDateId(Integer retirementEntranceDateId) {
		this.retirementEntranceDateId = retirementEntranceDateId;
	}

	public RetirementEntranceDate getRetirementEntranceDate() {
		return retirementEntranceDate;
	}

	public void setRetirementEntranceDate(RetirementEntranceDate retirementEntranceDate) {
		this.retirementEntranceDate = retirementEntranceDate;
	}

	public Integer getRetirementFundSetupId() {
		return retirementFundSetupId;
	}

	public void setRetirementFundSetupId(Integer retirementFundSetupId) {
		this.retirementFundSetupId = retirementFundSetupId;
	}

	public RetirementFundSetup getRetirementFundSetup() {
		return retirementFundSetup;
	}

	public void setRetirementFundSetup(RetirementFundSetup retirementFundSetup) {
		this.retirementFundSetup = retirementFundSetup;
	}

	public Integer getDateSequence() {
		return dateSequence;
	}

	public void setDateSequence(Integer dateSequence) {
		this.dateSequence = dateSequence;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getEntranceDescription() {
		return entranceDescription;
	}

	public void setEntranceDescription(String entranceDescription) {
		this.entranceDescription = entranceDescription;
	}

	public String getFundId() {
		return fundId;
	}

	public void setFundId(String fundId) {
		this.fundId = fundId;
	}

	public Integer getFundSequence() {
		return fundSequence;
	}

	public void setFundSequence(Integer fundSequence) {
		this.fundSequence = fundSequence;
	}

	public String getPlanDescription() {
		return planDescription;
	}

	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}

	public BigInteger getFundActive() {
		return fundActive;
	}

	public void setFundActive(BigInteger fundActive) {
		this.fundActive = fundActive;
	}

	public DtoRetirementPlanSetup(RetirementPlanSetup retirementPlanSetup) {
		this.retirementPlanId = retirementPlanSetup.getRetirementPlanId();

		if (UtilRandomKey.isNotBlank(retirementPlanSetup.getRetirementPlanDescription())) {
			this.retirementPlanDescription = retirementPlanSetup.getRetirementPlanDescription();
		} else {
			this.retirementPlanDescription = "";
		}

		if (UtilRandomKey.isNotBlank(retirementPlanSetup.getRetirementPlanArbicDescription())) {
			this.retirementPlanArbicDescription = retirementPlanSetup.getRetirementPlanArbicDescription();
		} else {
			this.retirementPlanArbicDescription = "";
		}

		if (UtilRandomKey.isNotBlank(retirementPlanSetup.getRetirementPlanAccountNumber())) {
			this.retirementPlanAccountNumber = retirementPlanSetup.getRetirementPlanAccountNumber();
		} else {
			this.retirementPlanAccountNumber = "";
		}

	}

	public DtoRetirementPlanSetup() {

	}

	public String getInsCompanyId() {
		return insCompanyId;
	}

	public void setInsCompanyId(String insCompanyId) {
		this.insCompanyId = insCompanyId;
	}

	
}
