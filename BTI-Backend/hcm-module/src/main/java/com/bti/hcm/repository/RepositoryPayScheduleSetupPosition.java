/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Department;
import com.bti.hcm.model.PayScheduleSetupPosition;

/**
 * Description: Interface for PayScheduleSetupPositio 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryPayScheduleSetupPosition")
public interface RepositoryPayScheduleSetupPosition extends JpaRepository<PayScheduleSetupPosition, Integer>{
	
	/**
	 * 
	 * @param paySchedulePositionIndexId
	 * @param deleted
	 * @return
	 */
	public PayScheduleSetupPosition findByPaySchedulePositionIndexIdAndIsDeleted(int paySchedulePositionIndexId, boolean deleted);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupPosition> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupPosition p where p.isDeleted=false")
	public Integer getCountOfTotalPayScheduleSetupPosition();
	
	
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<PayScheduleSetupPosition> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupPosition d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deletePayScheduleSetupPosition(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupPosition d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSinglPayScheduleSetupPosition(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public Department findTop1ByOrderByPaySchedulePositionIndexIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select p from PayScheduleSetupPosition p where (p.positionDescription like :searchKeyWord  or p.positionDescriptionArabic like :searchKeyWord or p.payScheduleSetup.payScheduleId like :searchKeyWord or p.position.positionId like :searchKeyWord) and p.isDeleted=false")
	public List<PayScheduleSetupPosition> predictivePayScheduleSetupPositionSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupPosition> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupPosition p where (p.positionDescription like :searchKeyWord  or p.positionDescriptionArabic like :searchKeyWord or p.payScheduleSetup.payScheduleId like :searchKeyWord or p.position.positionId like :searchKeyWord) and p.isDeleted=false")
	public Integer predictivePayScheduleSetupPositionSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select p from PayScheduleSetupPosition p where (p.positionDescription like :searchKeyWord  or p.positionDescriptionArabic like :searchKeyWord or p.payScheduleSetup.payScheduleId like :searchKeyWord or p.position.positionId like :searchKeyWord) and p.isDeleted=false")
	public List<PayScheduleSetupPosition> predictivePayScheduleSetupPositionSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param paySchedulePositionIndexId
	 * @return
	 */
	@Query("select p from PayScheduleSetupPosition p where (p.paySchedulePositionIndexId =:paySchedulePositionIndexId) and p.isDeleted=false")
	public List<PayScheduleSetupPosition> findByPaySchedulePositionIndexId(@Param("paySchedulePositionIndexId")Integer paySchedulePositionIndexId);

	
	@Query("select p from PayScheduleSetupPosition p where (p.payScheduleSetup.payScheduleIndexId =:payScheduleIndexId) and p.isDeleted=false")
	public List<PayScheduleSetupPosition> findByPayScheduleSetup(@Param("payScheduleIndexId")Integer payScheduleIndexId);
	
	@Query("select count(*) from PayScheduleSetupPosition p ")
	public Integer getCountOfTotaPayScheduleSetupPosition();
}
