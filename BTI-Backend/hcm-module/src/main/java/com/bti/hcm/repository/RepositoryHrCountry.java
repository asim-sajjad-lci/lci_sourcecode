package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bti.hcm.model.HrCountry;

public interface RepositoryHrCountry extends JpaRepository<HrCountry, Integer>{

	@Query("select c from HrCountry c where (c.countryName like %:searchKeyWord% ) and c.isDeleted=false")
	public List<HrCountry> predictiveCountrySearch(@Param("searchKeyWord") String searchKeyWord);
	
	List<HrCountry> findByIsDeletedAndLanguageLanguageId(boolean deleted, int langId);
}
