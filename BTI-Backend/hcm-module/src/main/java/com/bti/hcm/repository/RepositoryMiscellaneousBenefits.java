package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.MiscellaneousBenefits;

@Repository("repositoryMiscellaneousBenefits")
public interface RepositoryMiscellaneousBenefits extends JpaRepository<MiscellaneousBenefits, Integer>{

	MiscellaneousBenefits findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from MiscellaneousBenefits s where s.isDeleted=false")
	Integer getCountOfTotalHelthInsurance();

	List<MiscellaneousBenefits> findByIsDeleted(boolean b, Pageable pageable);

	List<MiscellaneousBenefits> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update MiscellaneousBenefits d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleMiscellaneousBenefits(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from MiscellaneousBenefits d where (d.BenefitsId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveMiscellaneousBenefitsSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from MiscellaneousBenefits d where (d.BenefitsId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord) and d.isDeleted=false")
	List<MiscellaneousBenefits> predictiveMiscellaneousBenefitsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select d from MiscellaneousBenefits d where (d.BenefitsId like :searchKeyWord) and d.isDeleted=false")
	List<MiscellaneousBenefits> predictiveMiscellaneousIdSearchWithPagination(@Param("searchKeyWord")String string);

	
	@Query("select m from MiscellaneousBenefits m where (m.BenefitsId =:BenefitsId) and m.isDeleted=false")
	List<MiscellaneousBenefits> miscellaneousBenefitsIdcheck(@Param("BenefitsId")String BenefitsId);

	@Query("select count(*) from MiscellaneousBenefits m ")
	public Integer getCountOfTotaMiscellaneousBenefits();

}
