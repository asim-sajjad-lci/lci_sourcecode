/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayScheduleSetupLocation;

/**
 * Description: Interface for PayScheduleSetupLocation 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryPayScheduleSetupPositions")
public interface RepositoryPayScheduleSetupLocation extends JpaRepository<PayScheduleSetupLocation, Integer> {

	/**
	 * 
	 * @param payScheduleSetupLocationId
	 * @param deleted
	 * @return
	 */
	public PayScheduleSetupLocation findByPayScheduleSetupLocationIdAndIsDeleted(int payScheduleSetupLocationId, boolean deleted);

	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupLocation> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupLocation p where p.isDeleted=false")
	public Integer getCountOfTotalPayScheduleSetupLocation();
	
	
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<PayScheduleSetupLocation> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupLocation d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deletePayScheduleSetupLocation(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupLocation d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSinglPayScheduleSetupLocation(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	/**
	 * 
	 * @return
	 */
	public PayScheduleSetupLocation findTop1ByOrderByPayScheduleSetupLocationIdDesc();
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select p from PayScheduleSetupLocation p where (p.locationName like :searchKeyWord  or p.location.locationId like :searchKeyWord or p.payScheduleSetup.payScheduleId like :searchKeyWord ) and p.isDeleted=false")
	public List<PayScheduleSetupLocation> predictivePayScheduleSetupLocationSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupLocation> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupLocation p where (p.locationName like :searchKeyWord  or p.location.locationId like :searchKeyWord or p.payScheduleSetup.payScheduleId like :searchKeyWord ) and p.isDeleted=false")
	public Integer predictivePayScheduleSetupLocationSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select p from PayScheduleSetupLocation p where (p.locationName like :searchKeyWord  or p.location.locationId like :searchKeyWord or p.payScheduleSetup.payScheduleId like :searchKeyWord ) and p.isDeleted=false")
	public List<PayScheduleSetupLocation> predictivePayScheduleSetupLocationSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param payScheduleSetupLocationId
	 * @return
	 */
	@Query("select p from PayScheduleSetupLocation p where (p.payScheduleSetupLocationId =:payScheduleSetupLocationId) and p.isDeleted=false")
	public List<PayScheduleSetupLocation> findByPayScheduleSetupLocationId(@Param("payScheduleSetupLocationId")Integer payScheduleSetupLocationId);

	
	
	@Query("select p from PayScheduleSetupLocation p where (p.payScheduleSetup.payScheduleIndexId =:payScheduleIndexId) and p.isDeleted=false")
	public List<PayScheduleSetupLocation> findByPayScheduleSetup(@Param("payScheduleIndexId")Integer payScheduleIndexId);

	@Query("select count(*) from PayScheduleSetupLocation p ")
	public Integer getCountOfTotaPayScheduleSetupLocation();
}
