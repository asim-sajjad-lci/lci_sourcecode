package com.bti.hcm.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Company;
import com.bti.hcm.model.WorkFlowAssign;
import com.bti.hcm.model.WorkFlowUploadFile;
import com.bti.hcm.model.dto.DtoWorkFlowAssign;
import com.bti.hcm.repository.RepositoryCompany;
import com.bti.hcm.repository.RepositoryWorkFlow;
import com.bti.hcm.repository.RepositoryWorkFlowAssign;


@Service("workFlowAssign")
public class ServiceWorkFlowAssign {
	
	
	
	@Autowired
	RepositoryWorkFlow repositoryWorkFlow;
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	RepositoryWorkFlowAssign repositoryWorkFlowAssign;
	
	@Autowired
	RepositoryCompany repositoryCompany;
	
	public DtoWorkFlowAssign saveOrUpdate(DtoWorkFlowAssign dtoWorkFlowAssign) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("USER_ID"));
		
		WorkFlowAssign workFlowAssign=null;
		
		if(dtoWorkFlowAssign.getId()!=null&& dtoWorkFlowAssign.getId()>0) {
			workFlowAssign=repositoryWorkFlowAssign.findByIdAndIsDeleted(dtoWorkFlowAssign.getId(),false);
			workFlowAssign.setUpdatedDate(new Date());
			workFlowAssign.setUpdatedBy(loggedInUserId);
		}else {
			workFlowAssign=new WorkFlowAssign();
			workFlowAssign.setCreatedDate(new Date());
		}
		/*
		if(!dtoWorkFlowAssign.getDtoUsersList().isEmpty() && dtoWorkFlowAssign.getDtoUsersList()!=null) {
			ArrayList<User>usersList=new ArrayList<>();
			for (DtoUser dtoUser : dtoWorkFlowAssign.getDtoUsersList()) {
				User user=repositoryUser.findByuserIdAndIsDeleted(dtoUser.getUserId(),false);
				usersList.add(user);
			}
			workFlowAssign.setUserList(usersList);
		}*/
		
		
		
		
		WorkFlowUploadFile workFlowUploadFile=null;
		if(dtoWorkFlowAssign.getWorkFlowUploadFile()!=null && dtoWorkFlowAssign.getWorkFlowUploadFile().getWorFlowId()>0) {
			workFlowUploadFile=repositoryWorkFlow.findOne(dtoWorkFlowAssign.getWorkFlowUploadFile().getWorFlowId());
		}
		
		Company company=null;
		if(dtoWorkFlowAssign.getCompany()!=null && dtoWorkFlowAssign.getCompany().getCompanyId()>0) {
			company=repositoryCompany.findOne(dtoWorkFlowAssign.getCompany().getCompanyId());
		}
		workFlowAssign.setCompany(company);
		workFlowAssign.setWorkFlowUploadFile(workFlowUploadFile);
		repositoryWorkFlowAssign.saveAndFlush(workFlowAssign);
		
		return dtoWorkFlowAssign;
	}
}
