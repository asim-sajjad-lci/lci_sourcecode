package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ibm.icu.math.BigDecimal;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10402")
public class ManualChecksDistribution extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMPYMTINDXD")
	private Integer id;

	@Column(name = "DITPOTTYP")
	private Short transactionType;

	@Column(name = "HCMCODINX")
	private Integer codeId;

	@Column(name = "DEBTAMT")
	private BigDecimal debitAmount;

	@Column(name = "CRDTAMT")
	private BigDecimal creditAmount;

	@Column(name = "ACCTSEQN")
	private Integer accountNumberSequence;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "HCMPYMTINDX")
	private ManualChecks manualChecks;
	
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="ACTROWID")
	private AccountsTableAccumulation accountsTableAccumulation;
	
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="DEPINDX")
	private Department department;
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="POTINDX")
	private Position position;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Short transactionType) {
		this.transactionType = transactionType;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Integer getAccountNumberSequence() {
		return accountNumberSequence;
	}

	public void setAccountNumberSequence(Integer accountNumberSequence) {
		this.accountNumberSequence = accountNumberSequence;
	}

	public ManualChecks getManualChecks() {
		return manualChecks;
	}

	public void setManualChecks(ManualChecks manualChecks) {
		this.manualChecks = manualChecks;
	}

	public AccountsTableAccumulation getAccountsTableAccumulation() {
		return accountsTableAccumulation;
	}

	public void setAccountsTableAccumulation(AccountsTableAccumulation accountsTableAccumulation) {
		this.accountsTableAccumulation = accountsTableAccumulation;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

}
