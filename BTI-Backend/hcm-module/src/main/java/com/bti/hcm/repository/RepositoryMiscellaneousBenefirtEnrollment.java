package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.MiscellaneousBenefirtEnrollment;

@Repository("repositoryMiscellaneousBenefirtEnrollment")
public interface RepositoryMiscellaneousBenefirtEnrollment extends JpaRepository<MiscellaneousBenefirtEnrollment, Integer>{

	MiscellaneousBenefirtEnrollment findByIdAndIsDeleted(Integer id, boolean b);

	MiscellaneousBenefirtEnrollment saveAndFlush(MiscellaneousBenefirtEnrollment miscellaneousBenefirtEnrollment);

	MiscellaneousBenefirtEnrollment findOne(Integer planId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update MiscellaneousBenefirtEnrollment m set m.isDeleted =:deleted ,m.updatedBy =:updateById where m.id =:id ")
	public void deleteSingleMiscellaneousBenefirtEnrollment(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select m from MiscellaneousBenefirtEnrollment m where m.isDeleted=false")
	List<MiscellaneousBenefirtEnrollment> findByIsDeleted(PageRequest pageRequest);
	
	@Query("select m from MiscellaneousBenefirtEnrollment m where (m.miscellaneousBenefits.BenefitsId like :searchKeyWord or m.miscellaneousBenefits.desc like :searchKeyWord or m.employeeMaster.employeeFirstName like :searchKeyWord or m.employeeMaster.employeeId like :searchKeyWord) and m.isDeleted=false")
	List<MiscellaneousBenefirtEnrollment> predictiveMiscellaneousBenefitsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select count(*) from MiscellaneousBenefirtEnrollment m where (m.miscellaneousBenefits.BenefitsId like :searchKeyWord or m.miscellaneousBenefits.desc like :searchKeyWord or m.employeeMaster.employeeFirstName like :searchKeyWord or m.employeeMaster.employeeId like :searchKeyWord) and m.isDeleted=false")
	Integer predictiveMiscellaneousBenefitsTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select m from MiscellaneousBenefirtEnrollment m where m.employeeMaster.employeeIndexId =:id and m.isDeleted=false")
	List<MiscellaneousBenefirtEnrollment> findByEmployeeId(@Param("id") int id);

	@Query("select count(*) from MiscellaneousBenefirtEnrollment m where (m.miscellaneousBenefits.BenefitsId like :searchKeyWord or m.miscellaneousBenefits.desc like :searchKeyWord or m.employeeMaster.employeeFirstName like :searchKeyWord or m.employeeMaster.employeeId like :searchKeyWord) and m.miscellaneousBenefits.id =:id and m.isDeleted=false")
	Integer searchByBenefitsCodeCount(@Param("searchKeyWord") String searchKeyWord, @Param("id")Integer id);
	
	@Query("select m from MiscellaneousBenefirtEnrollment m where (m.miscellaneousBenefits.BenefitsId like :searchKeyWord or m.miscellaneousBenefits.desc like :searchKeyWord or m.employeeMaster.employeeFirstName like :searchKeyWord or m.employeeMaster.employeeId like :searchKeyWord) and m.miscellaneousBenefits.id =:id and m.isDeleted=false")
	List<MiscellaneousBenefirtEnrollment> searchByBenefitsCode(@Param("searchKeyWord") String searchKeyWord, @Param("id")Integer id, Pageable pageRequest);

	@Query("select count(*) from MiscellaneousBenefirtEnrollment m ")
	public Integer getCountOfTotaMiscellaneousBenefirtEnrollment();
}
