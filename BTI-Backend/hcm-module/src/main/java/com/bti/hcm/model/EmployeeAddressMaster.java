/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the EmployeeAddressMaster database
 * table. Name of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00102",indexes = {
        @Index(columnList = "EMPADDINDX")
})
@NamedQuery(name = "EmployeeAddressMaster.findAll", query = "SELECT e FROM EmployeeAddressMaster e")
public class EmployeeAddressMaster extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPADDINDX")
	private int employeeAddressIndexId;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "LOCCOUNTY")
	private HrCountry country;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "LOCSTATE")
	private HrState state;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "LOCCITY")
	private HrCity city;

	@Column(name = "EMPADDID", columnDefinition = "char(10)")
	private String addressId;

	@Column(name = "EMPADDR1", columnDefinition = "char(90)")
	private String address1;

	@Column(name = "EMPADDR2", columnDefinition = "char(90)")
	private String address2;

	@Column(name = "EMPEMAI", columnDefinition = "char(255)")
	private String personalEmail;

	@Column(name = "EMPBEMAI", columnDefinition = "char(255)")
	private String businessEmail;

	@Column(name = "EMPADDRA1", columnDefinition = "char(90)")
	private String addressArabic1;

	@Column(name = "EMPADDRA2", columnDefinition = "char(90)")
	private String addressArabic2;

	@Column(name = "EMPPOBOX", columnDefinition = "char(31)")
	private String pOBox;

	@Column(name = "EMPBOXCO")
	private int postCode;

	@Column(name = "EMPMOBI", columnDefinition = "char(21)")
	private String personalPhone;

	@Column(name = "EMPBMOBI", columnDefinition = "char(21)")
	private String businessPhone;

	@Column(name = "EMPPHONE", columnDefinition = "char(21)")
	private String otherPhone;

	@Column(name = "EMPPHEXT", columnDefinition = "char(10)")
	private String ext;

	@Column(name = "EMPLOCLNK", columnDefinition = "char(255)")
	private String locationLinkGoogleMap;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employeeAddressMaster")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<EmployeeMaster> employeeMaster;

	public int getEmployeeAddressIndexId() {
		return employeeAddressIndexId;
	}

	public void setEmployeeAddressIndexId(int employeeAddressIndexId) {
		this.employeeAddressIndexId = employeeAddressIndexId;
	}

	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getBusinessEmail() {
		return businessEmail;
	}

	public void setBusinessEmail(String businessEmail) {
		this.businessEmail = businessEmail;
	}

	public String getAddressArabic1() {
		return addressArabic1;
	}

	public void setAddressArabic1(String addressArabic1) {
		this.addressArabic1 = addressArabic1;
	}

	public String getAddressArabic2() {
		return addressArabic2;
	}

	public void setAddressArabic2(String addressArabic2) {
		this.addressArabic2 = addressArabic2;
	}

	public String getpOBox() {
		return pOBox;
	}

	public void setpOBox(String pOBox) {
		this.pOBox = pOBox;
	}

	public int getPostCode() {
		return postCode;
	}

	public void setPostCode(int postCode) {
		this.postCode = postCode;
	}

	public String getPersonalPhone() {
		return personalPhone;
	}

	public void setPersonalPhone(String personalPhone) {
		this.personalPhone = personalPhone;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getLocationLinkGoogleMap() {
		return locationLinkGoogleMap;
	}

	public void setLocationLinkGoogleMap(String locationLinkGoogleMap) {
		this.locationLinkGoogleMap = locationLinkGoogleMap;
	}

	public HrCountry getCountry() {
		return country;
	}

	public void setCountry(HrCountry country) {
		this.country = country;
	}

	public HrState getState() {
		return state;
	}

	public void setState(HrState state) {
		this.state = state;
	}

	public HrCity getCity() {
		return city;
	}

	public void setCity(HrCity city) {
		this.city = city;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address1 == null) ? 0 : address1.hashCode());
		result = prime * result + ((address2 == null) ? 0 : address2.hashCode());
		result = prime * result + ((addressArabic1 == null) ? 0 : addressArabic1.hashCode());
		result = prime * result + ((addressArabic2 == null) ? 0 : addressArabic2.hashCode());
		result = prime * result + ((addressId == null) ? 0 : addressId.hashCode());
		result = prime * result + ((businessEmail == null) ? 0 : businessEmail.hashCode());
		result = prime * result + ((businessPhone == null) ? 0 : businessPhone.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + employeeAddressIndexId;
		result = prime * result + ((employeeMaster == null) ? 0 : employeeMaster.hashCode());
		result = prime * result + ((ext == null) ? 0 : ext.hashCode());
		result = prime * result + ((locationLinkGoogleMap == null) ? 0 : locationLinkGoogleMap.hashCode());
		result = prime * result + ((otherPhone == null) ? 0 : otherPhone.hashCode());
		result = prime * result + ((pOBox == null) ? 0 : pOBox.hashCode());
		result = prime * result + ((personalEmail == null) ? 0 : personalEmail.hashCode());
		result = prime * result + ((personalPhone == null) ? 0 : personalPhone.hashCode());
		result = prime * result + postCode;
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
