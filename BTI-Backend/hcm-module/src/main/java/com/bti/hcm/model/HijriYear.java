package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Description: The persistent class for hijri year 
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 * 
 * @author Bansi
 *
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR_HIJRI_YEAR",indexes = {
        @Index(columnList = "HIJRIYEARINDX")
})
public class HijriYear implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HIJRIYEARINDX")
	private Integer Id;
	
	@Column(name="HIJRIYEAR", columnDefinition="int(4)")
	private Integer year;
	
	@Column(name = "is_deleted", columnDefinition = "tinyint(0) default 0")
	protected Boolean isDeleted;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
