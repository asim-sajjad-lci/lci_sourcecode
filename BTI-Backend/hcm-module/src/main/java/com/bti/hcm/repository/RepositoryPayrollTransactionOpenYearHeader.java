package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollTransactionOpenYearHeader;

@Repository("repositoryPayrollTransactionOpenYearHeader")
public interface RepositoryPayrollTransactionOpenYearHeader
		extends JpaRepository<PayrollTransactionOpenYearHeader, Integer> {

	@Query("SELECT pto FROM PayrollTransactionOpenYearHeader pto WHERE YEAR(pto.createdDate) =:year and pto.isDeleted = false")
	List<PayrollTransactionOpenYearHeader> findByYear(@Param("year") Integer year);

}
