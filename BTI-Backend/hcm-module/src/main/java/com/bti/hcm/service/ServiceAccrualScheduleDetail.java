package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Accrual;
import com.bti.hcm.model.AccrualSchedule;
import com.bti.hcm.model.AccrualScheduleDetail;
import com.bti.hcm.model.AccrualType;
import com.bti.hcm.model.dto.DtoAccrualScheduleDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAccrual;
import com.bti.hcm.repository.RepositoryAccrualSchedule;
import com.bti.hcm.repository.RepositoryAccrualScheduleDetail;
import com.bti.hcm.repository.RepositoryAccrualType;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service AccrualScheduleDetail
 * Project: Hcm 
 * Version: 0.0.1
 */

@Service("serviceAccrualScheduleDetail")
public class ServiceAccrualScheduleDetail {
	static Logger log = Logger.getLogger(ServiceAccrualScheduleDetail.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryAccrualScheduleDetail repositoryAccrualScheduleDetail;
	
	@Autowired
	RepositoryAccrualSchedule repositoryAccrualSchedule;
	
	@Autowired
	RepositoryAccrual repositoryAccrual;
	
	@Autowired
	RepositoryAccrualType repositoryAccrualType;
	
	
	public DtoAccrualScheduleDetail saveOrUpdateAccrualScheduleDetail(DtoAccrualScheduleDetail dtoAccrualScheduleDetail) {

		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			AccrualScheduleDetail accrualScheduleDetail=null;
			if (dtoAccrualScheduleDetail.getId() != null && dtoAccrualScheduleDetail.getId() > 0) {
				accrualScheduleDetail = repositoryAccrualScheduleDetail.findByIdAndIsDeleted(dtoAccrualScheduleDetail.getId(), false);
				accrualScheduleDetail.setUpdatedBy(loggedInUserId);
				accrualScheduleDetail.setUpdatedDate(new Date());
			} else {
				accrualScheduleDetail = new AccrualScheduleDetail();
				accrualScheduleDetail.setCreatedDate(new Date());
				Integer rowId = repositoryAccrualScheduleDetail.getCountOfTotalAccruals();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				accrualScheduleDetail.setRowId(increment);
			}
			
			
			AccrualSchedule accrualSchedule = null;
			if(dtoAccrualScheduleDetail.getAccrualScheduleId()!=null && dtoAccrualScheduleDetail.getAccrualScheduleId()>0) {
				accrualSchedule= repositoryAccrualSchedule.findOne(dtoAccrualScheduleDetail.getAccrualScheduleId());
			}
			
			Accrual accrual = null;
			if(dtoAccrualScheduleDetail.getAccrualSetupId()!=null && dtoAccrualScheduleDetail.getAccrualSetupId()>0) {
				accrual=repositoryAccrual.findOne(dtoAccrualScheduleDetail.getAccrualSetupId());
			}
			
			AccrualType accrualType=null;
			if(dtoAccrualScheduleDetail.getAccrualTypeId()!=null && dtoAccrualScheduleDetail.getAccrualTypeId()>0) {
				accrualType=repositoryAccrualType.findOne(dtoAccrualScheduleDetail.getAccrualTypeId());
			}
			
			accrualScheduleDetail.setAccrualType(accrualType);
			accrualScheduleDetail.setAccrual(accrual);
			accrualScheduleDetail.setAccrualSchedule(accrualSchedule);
			accrualScheduleDetail.setScheduleSequence(dtoAccrualScheduleDetail.getScheduleSequence());
			accrualScheduleDetail.setScheduleSeniority(dtoAccrualScheduleDetail.getScheduleSeniority());
			accrualScheduleDetail.setDescription(dtoAccrualScheduleDetail.getDescription());
			accrualScheduleDetail.setHours(dtoAccrualScheduleDetail.getHours());
			accrualScheduleDetail.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryAccrualScheduleDetail.saveAndFlush(accrualScheduleDetail);
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrualScheduleDetail;
	}
	
	
	
	public DtoAccrualScheduleDetail getAccrualScheduleDetailId(int id) {
		DtoAccrualScheduleDetail dtoAccrualScheduleDetail  = new DtoAccrualScheduleDetail();

		try {
			if (id > 0) {
				AccrualScheduleDetail accrualScheduleDetail = repositoryAccrualScheduleDetail.findByIdAndIsDeleted(id, false);
				if (accrualScheduleDetail != null) {
					dtoAccrualScheduleDetail = new DtoAccrualScheduleDetail(accrualScheduleDetail);
				} else {
					dtoAccrualScheduleDetail.setMessageType("_NOT_GETTING");

				}
			} else {
				dtoAccrualScheduleDetail.setMessageType("INVALID_ACCRUAL_SCHEDULE_DETAIL_ID");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrualScheduleDetail;
	}
	
	
	public DtoAccrualScheduleDetail deleteAccrualScheduleDetail(List<Integer> ids) {
		log.info("deleteAccrualScheduleDetail Method");
		DtoAccrualScheduleDetail dtoAccrualScheduleDetail = new DtoAccrualScheduleDetail();
		try {
			dtoAccrualScheduleDetail.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUALSCHEDULEDETAIL_DELETED", false));
			dtoAccrualScheduleDetail.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUALSCHEDULEDETAIL_ASSOCIATED", false));
			List<DtoAccrualScheduleDetail> deleteAccrualScheduleDetail = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
				for (Integer id : ids) {
					AccrualScheduleDetail accrualScheduleDetail = repositoryAccrualScheduleDetail.findOne(id);
					DtoAccrualScheduleDetail dtoAccrualScheduleDetail2 = new DtoAccrualScheduleDetail();
					dtoAccrualScheduleDetail2.setId(id);
					dtoAccrualScheduleDetail2.setScheduleSequence(accrualScheduleDetail.getScheduleSequence());
					dtoAccrualScheduleDetail2.setScheduleSeniority(accrualScheduleDetail.getScheduleSeniority());
					dtoAccrualScheduleDetail2.setDescription(accrualScheduleDetail.getDescription());
					dtoAccrualScheduleDetail2.setHours(accrualScheduleDetail.getHours());
					
					repositoryAccrualScheduleDetail.deleteSingleAccrualScheduleDetail(true, loggedInUserId, id);
					deleteAccrualScheduleDetail.add(dtoAccrualScheduleDetail2);

				}
				dtoAccrualScheduleDetail.setDelete(deleteAccrualScheduleDetail);
			log.debug("Delete AccrualScheduleDetail :"+dtoAccrualScheduleDetail.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrualScheduleDetail;
	}
	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchAccrualSheduleDetail(DtoSearch dtoSearch) {
	
		try {
			log.info("search AccrualSheduleDetail Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("scheduleSequence") || dtoSearch.getSortOn().equals("scheduleSeniority")|| dtoSearch.getSortOn().equals("description")|| dtoSearch.getSortOn().equals("hours")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				
				dtoSearch.setTotalCount(this.repositoryAccrualScheduleDetail.predictiveAccrualSheduleDetailSearchTotalCount("%"+searchWord+"%"));
				List<AccrualScheduleDetail> accrualScheduleDetailList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						accrualScheduleDetailList = this.repositoryAccrualScheduleDetail.predictiveAccrualSheduleDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						accrualScheduleDetailList = this.repositoryAccrualScheduleDetail.predictiveAccrualSheduleDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						accrualScheduleDetailList = this.repositoryAccrualScheduleDetail.predictiveAccrualSheduleDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(accrualScheduleDetailList != null && !accrualScheduleDetailList.isEmpty()){
					List<DtoAccrualScheduleDetail> dtoAccrualScheduleDetailList = new ArrayList<>();
					DtoAccrualScheduleDetail dtoAccrualScheduleDetail=null;
					for (AccrualScheduleDetail accrualScheduleDetail : accrualScheduleDetailList) {
						dtoAccrualScheduleDetail = new DtoAccrualScheduleDetail(accrualScheduleDetail);
						
						
						dtoAccrualScheduleDetail.setId(accrualScheduleDetail.getId());
						dtoAccrualScheduleDetail.setScheduleSequence(accrualScheduleDetail.getScheduleSequence());
						dtoAccrualScheduleDetail.setScheduleSeniority(accrualScheduleDetail.getScheduleSeniority());
						dtoAccrualScheduleDetail.setDescription(accrualScheduleDetail.getDescription());
						dtoAccrualScheduleDetail.setHours(accrualScheduleDetail.getHours());
						dtoAccrualScheduleDetailList.add(dtoAccrualScheduleDetail);
						}
						
					
					dtoSearch.setRecords(dtoAccrualScheduleDetailList);
					}
					
				}
			
			log.debug("Search searchPosition Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
		}
	
	
}
