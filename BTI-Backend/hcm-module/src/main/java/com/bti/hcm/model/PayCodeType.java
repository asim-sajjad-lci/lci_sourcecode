package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40901",indexes = {
        @Index(columnList = "PYCDTYPINDX")
})
public class PayCodeType extends HcmBaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PYCDTYPINDX")
	private Integer id;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "payType")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	private List<PayCode> listPayCode;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "payCodeType")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<EmployeePayCodeMaintenance> listEmployeePayCodeMaintenance;

	
	

	@Column(name = "PYCDTYPDSCR")
	private String desc;

	@Column(name = "PYCDTYPDSCRA")
	private String arabicDesc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

	public List<PayCode> getListPayCode() {
		return listPayCode;
	}

	public void setListPayCode(List<PayCode> listPayCode) {
		this.listPayCode = listPayCode;
	}

	public List<EmployeePayCodeMaintenance> getListEmployeePayCodeMaintenance() {
		return listEmployeePayCodeMaintenance;
	}

	public void setListEmployeePayCodeMaintenance(List<EmployeePayCodeMaintenance> listEmployeePayCodeMaintenance) {
		this.listEmployeePayCodeMaintenance = listEmployeePayCodeMaintenance;
	}

}
