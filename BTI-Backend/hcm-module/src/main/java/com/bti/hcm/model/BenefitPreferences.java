package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR41200",indexes = {
        @Index(columnList = "ROWID")
})
public class BenefitPreferences extends HcmBaseEntity implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROWID")
    private Integer id;
	
	@Column(name = "PSTESTM")
    private byte postEstimatedDate;
	
	@Column(name = "PSTPAYTD")
    private byte postPaymentDates;
	
	@Column(name = "NMRHRSDY")
    private int workingdaysWeek;
	
	@Column(name = "NMRHRDY")
    private int workingHoursDay;
	
	@Column(name = "PSTELGD")
    private int eligibilityDate;
	
	@Column(name = "PSTPAG")
    private int paperworkDeadlines;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte getPostEstimatedDate() {
		return postEstimatedDate;
	}

	public void setPostEstimatedDate(byte postEstimatedDate) {
		this.postEstimatedDate = postEstimatedDate;
	}

	public byte getPostPaymentDates() {
		return postPaymentDates;
	}

	public void setPostPaymentDates(byte postPaymentDates) {
		this.postPaymentDates = postPaymentDates;
	}

	public int getWorkingdaysWeek() {
		return workingdaysWeek;
	}

	public void setWorkingdaysWeek(int workingdaysWeek) {
		this.workingdaysWeek = workingdaysWeek;
	}

	public int getWorkingHoursDay() {
		return workingHoursDay;
	}

	public void setWorkingHoursDay(int workingHoursDay) {
		this.workingHoursDay = workingHoursDay;
	}

	public int getEligibilityDate() {
		return eligibilityDate;
	}

	public void setEligibilityDate(int eligibilityDate) {
		this.eligibilityDate = eligibilityDate;
	}

	public int getPaperworkDeadlines() {
		return paperworkDeadlines;
	}

	public void setPaperworkDeadlines(int paperworkDeadlines) {
		this.paperworkDeadlines = paperworkDeadlines;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + eligibilityDate;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + paperworkDeadlines;
		result = prime * result + postEstimatedDate;
		result = prime * result + postPaymentDates;
		result = prime * result + workingHoursDay;
		result = prime * result + workingdaysWeek;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}
	
	
	
}
