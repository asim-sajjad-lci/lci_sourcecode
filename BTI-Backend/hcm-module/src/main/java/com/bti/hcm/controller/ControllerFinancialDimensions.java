package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoFinancialDimensions;
import com.bti.hcm.service.ServiceFinancialDimensions;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/financialDimensions")
public class ControllerFinancialDimensions extends BaseController{
	
	

	private static final Logger LOGGER = Logger.getLogger(ControllerDefault.class);
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	ServiceFinancialDimensions serviceFinancialDimensions;

	
	

	@RequestMapping(value = "/getAllFinancialDimensionsDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllFinancialDimensionsDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoFinancialDimensions> dtoFinancialDimensionsList = serviceFinancialDimensions.getAllFinancialDimensionsDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("FINANCIALDIMENTIONS_LIST_NOT_GETTING", false), dtoFinancialDimensionsList);
			}else {
				responseMessage =unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return responseMessage;
	}
}
