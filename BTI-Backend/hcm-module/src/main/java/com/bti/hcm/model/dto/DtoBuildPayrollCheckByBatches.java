package com.bti.hcm.model.dto;

import java.util.List;

public class DtoBuildPayrollCheckByBatches extends DtoBase{
	private Integer id;
	private DtoBuildChecks buildChecks;
	private List<DtoBatches> batches;
	private List<DtoBuildPayrollCheckByBatches> listBuildChecks;
	private List<DtoBuildPayrollCheckByBatches> delete;
	private Boolean statusCheck;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DtoBuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(DtoBuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}


	public List<DtoBuildPayrollCheckByBatches> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoBuildPayrollCheckByBatches> delete) {
		this.delete = delete;
	}


	public List<DtoBatches> getBatches() {
		return batches;
	}

	public void setBatches(List<DtoBatches> batches) {
		this.batches = batches;
	}

	public List<DtoBuildPayrollCheckByBatches> getListBuildChecks() {
		return listBuildChecks;
	}

	public void setListBuildChecks(List<DtoBuildPayrollCheckByBatches> listBuildChecks) {
		this.listBuildChecks = listBuildChecks;
	}

	public Boolean getStatusCheck() {
		return statusCheck;
	}

	public void setStatusCheck(Boolean statusCheck) {
		this.statusCheck = statusCheck;
	}

}
