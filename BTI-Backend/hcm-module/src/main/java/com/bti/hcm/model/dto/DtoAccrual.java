package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.Accrual;
import com.bti.hcm.model.AccrualType;
import com.bti.hcm.util.UtilRandomKey;

public class DtoAccrual extends DtoBase{

	private Integer id;
	private String accuralId;
	private String desc;
	private String arbic;
	private short reasons;
	private short type;

	private Integer accrualTyepId;
	private AccrualType accrualType;

	private Integer numHour;
	private Integer numDay;
	private Integer maxAccrualHour;
	private Integer maxHour;
	private Integer workHour;
	private short period;
	private List<DtoAccrual> delete;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAccuralId() {
		return accuralId;
	}

	public void setAccuralId(String accuralId) {
		this.accuralId = accuralId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbic() {
		return arbic;
	}

	public void setArbic(String arbic) {
		this.arbic = arbic;
	}

	public AccrualType getAccrualType() {
		return accrualType;
	}

	public void setAccrualType(AccrualType accrualType) {
		this.accrualType = accrualType;
	}

	public Integer getNumHour() {
		return numHour;
	}

	public void setNumHour(Integer numHour) {
		this.numHour = numHour;
	}

	public Integer getNumDay() {
		return numDay;
	}

	public void setNumDay(Integer numDay) {
		this.numDay = numDay;
	}

	public Integer getMaxAccrualHour() {
		return maxAccrualHour;
	}

	public void setMaxAccrualHour(Integer maxAccrualHour) {
		this.maxAccrualHour = maxAccrualHour;
	}

	public Integer getMaxHour() {
		return maxHour;
	}

	public void setMaxHour(Integer maxHour) {
		this.maxHour = maxHour;
	}

	public Integer getWorkHour() {
		return workHour;
	}

	public void setWorkHour(Integer workHour) {
		this.workHour = workHour;
	}

	public short getPeriod() {
		return period;
	}

	public void setPeriod(short period) {
		this.period = period;
	}

	

	public List<DtoAccrual> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoAccrual> delete) {
		this.delete = delete;
	}

	public Integer getAccrualTyepId() {
		return accrualTyepId;
	}

	public void setAccrualTyepId(Integer accrualTyepId) {
		this.accrualTyepId = accrualTyepId;
	}

	public short getReasons() {
		return reasons;
	}

	public void setReasons(short reasons) {
		this.reasons = reasons;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public DtoAccrual() {
	}

	public DtoAccrual(Accrual accrual) {
		this.accuralId = accrual.getAccuralId();

		if (UtilRandomKey.isNotBlank(accrual.getDesc())) {
			this.desc = accrual.getDesc();
		} else {
			this.desc = "";
		}

		if (UtilRandomKey.isNotBlank(accrual.getArbic())) {
			this.arbic = accrual.getArbic();
		} else {
			this.arbic = "";
		}
	}
}
