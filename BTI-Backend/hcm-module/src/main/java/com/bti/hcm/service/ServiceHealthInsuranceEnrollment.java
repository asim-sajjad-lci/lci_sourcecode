package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.HealthInsuranceEnrollment;
import com.bti.hcm.model.HelthInsurance;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoHealthInsuranceEnrollment;
import com.bti.hcm.model.dto.DtoHelthInsurance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryHealthInsuranceEnrollment;
import com.bti.hcm.repository.RepositoryHelthInsurance;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceHealthInsuranceEnrollment")
public class ServiceHealthInsuranceEnrollment {
	
	
	

	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceHealthInsuranceEnrollment.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	
	@Autowired
	RepositoryHealthInsuranceEnrollment repositoryHealthInsuranceEnrollment;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	
	@Autowired(required=false)
	RepositoryHelthInsurance repositoryHelthInsurance;
	

	public DtoHealthInsuranceEnrollment saveOrUpdate(DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment) {
		try {

			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			HealthInsuranceEnrollment healthInsuranceEnrollment=null;
			if (dtoHealthInsuranceEnrollment.getId() != null && dtoHealthInsuranceEnrollment.getId() > 0) {
				healthInsuranceEnrollment = repositoryHealthInsuranceEnrollment.findByIdAndIsDeleted(dtoHealthInsuranceEnrollment.getId(), false);
				healthInsuranceEnrollment.setUpdatedBy(loggedInUserId);
				healthInsuranceEnrollment.setUpdatedDate(new Date());
			} else {
				healthInsuranceEnrollment = new HealthInsuranceEnrollment();
				healthInsuranceEnrollment.setCreatedDate(new Date());
				Integer rowId = repositoryHealthInsuranceEnrollment.getCountOfTotalHealthInsuranceEnrollment();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				healthInsuranceEnrollment.setRowId(increment);
			}
			
			
			EmployeeMaster employeeMaster=null;
			if(dtoHealthInsuranceEnrollment.getEmployeeMaster()!=null && dtoHealthInsuranceEnrollment.getEmployeeMaster().getEmployeeIndexId()>0) {
				employeeMaster=repositoryEmployeeMaster.findOne(dtoHealthInsuranceEnrollment.getEmployeeMaster().getEmployeeIndexId());
			}
			
			
			HelthInsurance helthInsurance=null;
			if(dtoHealthInsuranceEnrollment.getHelthInsurance()!=null && dtoHealthInsuranceEnrollment.getHelthInsurance().getId()>0) {
				helthInsurance=repositoryHelthInsurance.findOne(dtoHealthInsuranceEnrollment.getHelthInsurance().getId());
			}
			
			healthInsuranceEnrollment.setHelthInsurance(helthInsurance);
			healthInsuranceEnrollment.setEmployeeMaster(employeeMaster);
			healthInsuranceEnrollment.setStatus(dtoHealthInsuranceEnrollment.getStatus());
			healthInsuranceEnrollment.setEligibilityDate(dtoHealthInsuranceEnrollment.getEligibilityDate());
			healthInsuranceEnrollment.setBenefitStartDate(dtoHealthInsuranceEnrollment.getBenefitStartDate());
			healthInsuranceEnrollment.setBenefitEndDate(dtoHealthInsuranceEnrollment.getBenefitEndDate());
			healthInsuranceEnrollment.setOverrideCost(dtoHealthInsuranceEnrollment.getOverrideCost());
			healthInsuranceEnrollment.setInsuredAmount(dtoHealthInsuranceEnrollment.getInsuredAmount());
			healthInsuranceEnrollment.setCostToEmployee(dtoHealthInsuranceEnrollment.getCostToEmployee());
			healthInsuranceEnrollment.setCostToEmployeer(dtoHealthInsuranceEnrollment.getCostToEmployeer());
			healthInsuranceEnrollment.setAdditionalCost(dtoHealthInsuranceEnrollment.getAdditionalCost());
			healthInsuranceEnrollment.setPolicyNumber(dtoHealthInsuranceEnrollment.getPolicyNumber());
				

			healthInsuranceEnrollment.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryHealthInsuranceEnrollment.saveAndFlush(healthInsuranceEnrollment);
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoHealthInsuranceEnrollment;
	}



	public DtoHealthInsuranceEnrollment delete(List<Integer> ids) {
		log.info("delete HealthInsuranceEnrollment Method");
		DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment = new DtoHealthInsuranceEnrollment();
		dtoHealthInsuranceEnrollment.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("HEALTH_INSURANCE_ENROLLMENT_DELETED", false));
		dtoHealthInsuranceEnrollment.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("HEALTH_INSURANCE_ENROLLMENT_ASSOCIATED", false));
		List<DtoHealthInsuranceEnrollment> deleteDtoHealthInsuranceEnrollment = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("HEALTH_INSURANCE_ENROLLMENT_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer planId : ids) {
				HealthInsuranceEnrollment healthInsuranceEnrollment = repositoryHealthInsuranceEnrollment.findOne(planId);
	if(healthInsuranceEnrollment!=null) {
		DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment2=new DtoHealthInsuranceEnrollment();
		dtoHealthInsuranceEnrollment.setId(healthInsuranceEnrollment.getId()); 
			repositoryHealthInsuranceEnrollment.deleteSingleHealthInsuranceEnrollment(true, loggedInUserId, planId);
			deleteDtoHealthInsuranceEnrollment.add(dtoHealthInsuranceEnrollment2);	

	}else {
		inValidDelete = true;
	}
					
				}

			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoHealthInsuranceEnrollment.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoHealthInsuranceEnrollment.setMessageType("");
				
			}
			
				
			dtoHealthInsuranceEnrollment.setDelete(deleteDtoHealthInsuranceEnrollment);
		} 
		catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete HealthInsuranceEnrollment"
				+ " :"+dtoHealthInsuranceEnrollment.getId());
		return dtoHealthInsuranceEnrollment;
	}

	
	@Transactional
	public DtoHealthInsuranceEnrollment getById(int id) {
		DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment  = new DtoHealthInsuranceEnrollment();
		try {
			if (id > 0) {
				HealthInsuranceEnrollment healthInsuranceEnrollment = repositoryHealthInsuranceEnrollment.findByEmployeeId1(id);
				if (healthInsuranceEnrollment != null) {
					dtoHealthInsuranceEnrollment.setId(healthInsuranceEnrollment.getId());
					
					DtoEmployeeMaster dtoEmployeeMasterHcm=new DtoEmployeeMaster();
					if(healthInsuranceEnrollment.getEmployeeMaster()!=null) {
						dtoEmployeeMasterHcm.setEmployeeIndexId(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeIndexId());
						dtoEmployeeMasterHcm.setEmployeeId(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeId());
						dtoEmployeeMasterHcm.setEmployeeBirthDate(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeBirthDate());
						dtoEmployeeMasterHcm.setEmployeeCitizen(healthInsuranceEnrollment.getEmployeeMaster().isEmployeeCitizen());
						dtoEmployeeMasterHcm.setEmployeeFirstName(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeFirstName());
						dtoEmployeeMasterHcm.setEmployeeFirstNameArabic(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeFirstNameArabic());
						dtoEmployeeMasterHcm.setEmployeeGender(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeGender());
						dtoEmployeeMasterHcm.setEmployeeHireDate(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeHireDate());
						dtoEmployeeMasterHcm.setEmployeeImmigration(healthInsuranceEnrollment.getEmployeeMaster().isEmployeeImmigration());
						dtoEmployeeMasterHcm.setEmployeeInactive(healthInsuranceEnrollment.getEmployeeMaster().isEmployeeInactive());
						dtoEmployeeMasterHcm.setEmployeeInactiveDate(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeInactiveDate());
						dtoEmployeeMasterHcm.setEmployeeInactiveReason(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeInactiveReason());
						dtoEmployeeMasterHcm.setEmployeeLastName(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeLastName());
						dtoEmployeeMasterHcm.setEmployeeLastNameArabic(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeLastNameArabic());
						dtoEmployeeMasterHcm.setEmployeeLastWorkDate(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeLastWorkDate());
						dtoEmployeeMasterHcm.setEmployeeMaritalStatus(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeMaritalStatus());
						dtoEmployeeMasterHcm.setEmployeeMiddleName(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeMiddleName());
						dtoEmployeeMasterHcm.setEmployeeMiddleNameArabic(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeMiddleNameArabic());
						dtoEmployeeMasterHcm.setEmployeeTitle(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeTitle());
						dtoEmployeeMasterHcm.setEmployeeTitleArabic(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeTitleArabic());
					}
					
					DtoHelthInsurance dtoHelthInsurance=new DtoHelthInsurance();
					if(healthInsuranceEnrollment.getHelthInsurance()!=null) {
						dtoHelthInsurance.setId(healthInsuranceEnrollment.getHelthInsurance().getId());
						dtoHelthInsurance.setHelthInsuranceId(healthInsuranceEnrollment.getHelthInsurance().getHelthInsuranceId());
						dtoHelthInsurance.setArbicDesc(healthInsuranceEnrollment.getHelthInsurance().getArbicDesc());
						dtoHelthInsurance.setDesc(healthInsuranceEnrollment.getHelthInsurance().getDesc());
						dtoHelthInsurance.setFrequency(healthInsuranceEnrollment.getHelthInsurance().getFrequency());
						dtoHelthInsurance.setGroupNumber(healthInsuranceEnrollment.getHelthInsurance().getGroupNumber());
						dtoHelthInsurance.setMaxAge(healthInsuranceEnrollment.getHelthInsurance().getMaxAge());
						dtoHelthInsurance.setMinAge(healthInsuranceEnrollment.getHelthInsurance().getMinAge());
						dtoHelthInsurance.setEmployeePay(healthInsuranceEnrollment.getHelthInsurance().getEmployeePay());
						dtoHelthInsurance.setEmployerPay(healthInsuranceEnrollment.getHelthInsurance().getEmployerPay());
						dtoHelthInsurance.setExpenses(healthInsuranceEnrollment.getHelthInsurance().getExpenses());
						dtoHelthInsurance.setMaxCoverage(healthInsuranceEnrollment.getHelthInsurance().getMaxCoverage());
					}
					dtoHealthInsuranceEnrollment.setDtoHelthInsurance(dtoHelthInsurance);
					dtoHealthInsuranceEnrollment.setDtoEmployeeMaster(dtoEmployeeMasterHcm);
					dtoHealthInsuranceEnrollment.setStatus(healthInsuranceEnrollment.getStatus());
					dtoHealthInsuranceEnrollment.setEligibilityDate(healthInsuranceEnrollment.getEligibilityDate());
					dtoHealthInsuranceEnrollment.setBenefitStartDate(healthInsuranceEnrollment.getBenefitStartDate());
					dtoHealthInsuranceEnrollment.setBenefitEndDate(healthInsuranceEnrollment.getBenefitEndDate());
					dtoHealthInsuranceEnrollment.setOverrideCost(healthInsuranceEnrollment.getOverrideCost());
					dtoHealthInsuranceEnrollment.setInsuredAmount(healthInsuranceEnrollment.getInsuredAmount());
					dtoHealthInsuranceEnrollment.setCostToEmployee(healthInsuranceEnrollment.getCostToEmployee());
					dtoHealthInsuranceEnrollment.setCostToEmployeer(healthInsuranceEnrollment.getCostToEmployeer());
					dtoHealthInsuranceEnrollment.setAdditionalCost(healthInsuranceEnrollment.getAdditionalCost());
					dtoHealthInsuranceEnrollment.setPolicyNumber(healthInsuranceEnrollment.getPolicyNumber());
					
					
					
					
				} 
			} else {
				dtoHealthInsuranceEnrollment.setMessageType("INVALID_ID");

			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoHealthInsuranceEnrollment;
	}



	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("HealthInsuranceEnrollment Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("policyNumber")|| dtoSearch.getSortOn().equals("eligibilityDate")|| dtoSearch.getSortOn().equals("benefitStartDate")|| dtoSearch.getSortOn().equals("benefitEndDate")
								|| dtoSearch.getSortOn().equals("employeeMaster") || dtoSearch.getSortOn().equals("helthInsurance")) {
							if(dtoSearch.getSortOn().equals("employeeMaster")) {
								condition = dtoSearch.getSortOn()+".employeeId";
							} else if(dtoSearch.getSortOn().equals("helthInsurance")) {
								condition = dtoSearch.getSortOn()+".helthInsuranceId";
							} else {
								condition=dtoSearch.getSortOn();
							}
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				
				dtoSearch.setTotalCount(this.repositoryHealthInsuranceEnrollment.predictiveHealthInsuranceEnrollmentSearchTotalCount("%"+searchWord+"%"));
				List<HealthInsuranceEnrollment> healthInsuranceEnrollmentList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						healthInsuranceEnrollmentList = this.repositoryHealthInsuranceEnrollment.predictiveHealthInsuranceEnrollmentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						healthInsuranceEnrollmentList = this.repositoryHealthInsuranceEnrollment.predictiveHealthInsuranceEnrollmentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						healthInsuranceEnrollmentList = this.repositoryHealthInsuranceEnrollment.predictiveHealthInsuranceEnrollmentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(healthInsuranceEnrollmentList != null && !healthInsuranceEnrollmentList.isEmpty()){
					List<DtoHealthInsuranceEnrollment> dtoHealthInsuranceEnrollmentList = new ArrayList<>();
					DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment=null;
					for (HealthInsuranceEnrollment healthInsuranceEnrollment : healthInsuranceEnrollmentList) {
						dtoHealthInsuranceEnrollment = new DtoHealthInsuranceEnrollment(healthInsuranceEnrollment);
						
						DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
						if(healthInsuranceEnrollment.getEmployeeMaster()!=null) {
							employeeMaster.setEmployeeIndexId(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeIndexId());
							employeeMaster.setEmployeeId(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeId());
							employeeMaster.setEmployeeBirthDate(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeBirthDate());
							employeeMaster.setEmployeeCitizen(healthInsuranceEnrollment.getEmployeeMaster().isEmployeeCitizen());
							employeeMaster.setEmployeeFirstName(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeFirstName());
							employeeMaster.setEmployeeFirstNameArabic(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeFirstNameArabic());
							employeeMaster.setEmployeeGender(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeGender());
							employeeMaster.setEmployeeHireDate(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeHireDate());
							employeeMaster.setEmployeeImmigration(healthInsuranceEnrollment.getEmployeeMaster().isEmployeeImmigration());
							employeeMaster.setEmployeeInactive(healthInsuranceEnrollment.getEmployeeMaster().isEmployeeInactive());
							employeeMaster.setEmployeeInactiveDate(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeInactiveDate());
							employeeMaster.setEmployeeInactiveReason(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeInactiveReason());
							employeeMaster.setEmployeeLastName(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeLastName());
							employeeMaster.setEmployeeLastNameArabic(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeLastNameArabic());
							employeeMaster.setEmployeeLastWorkDate(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeLastWorkDate());
							employeeMaster.setEmployeeMaritalStatus(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeMaritalStatus());
							employeeMaster.setEmployeeMiddleName(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeMiddleName());
							employeeMaster.setEmployeeMiddleNameArabic(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeMiddleNameArabic());
							employeeMaster.setEmployeeTitle(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeTitle());
							employeeMaster.setEmployeeTitleArabic(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeTitleArabic());
							employeeMaster.setEmployeeType(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeType());
							employeeMaster.setEmployeeUserIdInSystem(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeUserIdInSystem());
							employeeMaster.setEmployeeWorkHourYearly(healthInsuranceEnrollment.getEmployeeMaster().getEmployeeWorkHourYearly());
							dtoHealthInsuranceEnrollment.setEmployeeMaster(employeeMaster);
					
						}
						
						DtoHelthInsurance helthInsurance= new DtoHelthInsurance();
						if(healthInsuranceEnrollment.getHelthInsurance()!=null) {
							
							helthInsurance.setId(healthInsuranceEnrollment.getHelthInsurance().getId());
							helthInsurance.setHelthInsuranceId(healthInsuranceEnrollment.getHelthInsurance().getHelthInsuranceId());
							helthInsurance.setDesc(healthInsuranceEnrollment.getHelthInsurance().getDesc());
							helthInsurance.setArbicDesc(healthInsuranceEnrollment.getHelthInsurance().getArbicDesc());
							helthInsurance.setFrequency(healthInsuranceEnrollment.getHelthInsurance().getFrequency());
							helthInsurance.setGroupNumber(healthInsuranceEnrollment.getHelthInsurance().getGroupNumber());
							helthInsurance.setMaxAge(healthInsuranceEnrollment.getHelthInsurance().getMaxAge());
							helthInsurance.setMinAge(healthInsuranceEnrollment.getHelthInsurance().getMinAge());
							helthInsurance.setEmployeePay(healthInsuranceEnrollment.getHelthInsurance().getEmployeePay());
							helthInsurance.setEmployerPay(healthInsuranceEnrollment.getHelthInsurance().getEmployerPay());
							helthInsurance.setExpenses(healthInsuranceEnrollment.getHelthInsurance().getExpenses());
							if(healthInsuranceEnrollment.getHelthInsurance().getHelthCoverageType()!=null) {
								helthInsurance.setHelthCoveragePrimaryTypeId(healthInsuranceEnrollment.getHelthInsurance().getHelthCoverageType().getId());
								helthInsurance.setHelthCoverageTypeId(healthInsuranceEnrollment.getHelthInsurance().getHelthCoverageType().getHelthCoverageId());
								helthInsurance.setHelthCoverageDesc(healthInsuranceEnrollment.getHelthInsurance().getHelthCoverageType().getDesc());
							}
							helthInsurance.setMaxCoverage(healthInsuranceEnrollment.getHelthInsurance().getMaxCoverage());
							dtoHealthInsuranceEnrollment.setHelthInsurance(helthInsurance);
						}
						dtoHealthInsuranceEnrollment.setId(healthInsuranceEnrollment.getId());
						dtoHealthInsuranceEnrollment.setStatus(healthInsuranceEnrollment.getStatus());
						dtoHealthInsuranceEnrollment.setEligibilityDate(healthInsuranceEnrollment.getEligibilityDate());
						dtoHealthInsuranceEnrollment.setBenefitStartDate(healthInsuranceEnrollment.getBenefitStartDate());
						dtoHealthInsuranceEnrollment.setBenefitEndDate(healthInsuranceEnrollment.getBenefitEndDate());
						dtoHealthInsuranceEnrollment.setOverrideCost(healthInsuranceEnrollment.getOverrideCost());
						dtoHealthInsuranceEnrollment.setInsuredAmount(healthInsuranceEnrollment.getInsuredAmount());
						dtoHealthInsuranceEnrollment.setCostToEmployee(healthInsuranceEnrollment.getCostToEmployee());
						dtoHealthInsuranceEnrollment.setCostToEmployeer(healthInsuranceEnrollment.getCostToEmployeer());
						dtoHealthInsuranceEnrollment.setAdditionalCost(healthInsuranceEnrollment.getAdditionalCost());
						dtoHealthInsuranceEnrollment.setPolicyNumber(healthInsuranceEnrollment.getPolicyNumber());
						
						dtoHealthInsuranceEnrollmentList.add(dtoHealthInsuranceEnrollment);
						}
					dtoSearch.setRecords(dtoHealthInsuranceEnrollmentList);
						}
			
			log.debug("Search HealthInsuranceEnrollment Size is:"+dtoSearch.getTotalCount());
			
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public DtoHealthInsuranceEnrollment repeatByHelthInsuranceId(DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment){
		log.info("repeatByEmployeeId Method");
		try {
				List<HealthInsuranceEnrollment> listHealthInsuranceEnrollment=repositoryHealthInsuranceEnrollment.findByEmployeeIds(dtoHealthInsuranceEnrollment.getEmployeeId(),dtoHealthInsuranceEnrollment.getHelthInsuranceId());
				if(listHealthInsuranceEnrollment!=null && !listHealthInsuranceEnrollment.isEmpty()) {
					dtoHealthInsuranceEnrollment.setIsRepeat(true);
				}else {
					dtoHealthInsuranceEnrollment.setIsRepeat(false);
				}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoHealthInsuranceEnrollment;
	}
	
}
