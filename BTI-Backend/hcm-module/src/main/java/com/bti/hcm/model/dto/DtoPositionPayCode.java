package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.PositionPalnSetup;
import com.bti.hcm.model.PositionPayCode;

public class DtoPositionPayCode extends DtoBase{
	
	private Integer id;
	private Integer positionplanId;
	private PositionPalnSetup positionPalnSetup;
	private Integer positionPaycodeSeqn;
	private short positionCode;
	private PayCode payCode;
	private DeductionCode deductionCode;
	
	
	private String deductionId;
	private String discription;
	
	private BenefitCode benefitCode;
	
	private Integer deductionMainId;
	private Integer benefitMainId;
	
	private Integer payCodeMainId;
	
	
	private Double ratePay;
	private List<PositionPayCode> positionPayCode;
	private List<DtoPositionPayCode> deletePosition;
	
	
	private short codeType;
	private String payCodeId;
	private Double payRate;
	private String positionplandescription;
	private String positiondescription;
	private String positionPlanDesc; 
	private String positionId;
	private String positionPlan; 
	
	
	
	
	public String getPositionPlan() {
		return positionPlan;
	}
	public void setPositionPlan(String positionPlan) {
		this.positionPlan = positionPlan;
	}
	public String getPositiondescription() {
		return positiondescription;
	}
	public void setPositiondescription(String positiondescription) {
		this.positiondescription = positiondescription;
	}
	
	public short getCodeType() {
		return codeType;
	}
	public void setCodeType(short codeType) {
		this.codeType = codeType;
	}
	
	
	public String getPayCodeId() {
		return payCodeId;
	}
	public void setPayCodeId(String payCodeId) {
		this.payCodeId = payCodeId;
	}
	public Double getPayRate() {
		return payRate;
	}
	public void setPayRate(Double payRate) {
		this.payRate = payRate;
	}
	public String getPositionplandescription() {
		return positionplandescription;
	}
	public void setPositionplandescription(String positionplandescription) {
		this.positionplandescription = positionplandescription;
	}
	
	public String getPositionId() {
		return positionId;
	}
	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public PositionPalnSetup getPositionPalnSetup() {
		return positionPalnSetup;
	}
	public void setPositionPalnSetup(PositionPalnSetup positionPalnSetup) {
		this.positionPalnSetup = positionPalnSetup;
	}
	public Integer getPositionPaycodeSeqn() {
		return positionPaycodeSeqn;
	}
	public void setPositionPaycodeSeqn(Integer positionPaycodeSeqn) {
		this.positionPaycodeSeqn = positionPaycodeSeqn;
	}
	
	public short getPositionCode() {
		return positionCode;
	}
	public void setPositionCode(short positionCode) {
		this.positionCode = positionCode;
	}
	public PayCode getPayCode() {
		return payCode;
	}
	public void setPayCode(PayCode payCode) {
		this.payCode = payCode;
	}
	public DeductionCode getDeductionCode() {
		return deductionCode;
	}
	public void setDeductionCode(DeductionCode deductionCode) {
		this.deductionCode = deductionCode;
	}
	public BenefitCode getBenefitCode() {
		return benefitCode;
	}
	public void setBenefitCode(BenefitCode benefitCode) {
		this.benefitCode = benefitCode;
	}
	
	public Double getRatePay() {
		return ratePay;
	}
	public void setRatePay(Double ratePay) {
		this.ratePay = ratePay;
	}
	
	public List<DtoPositionPayCode> getDeletePosition() {
		return deletePosition;
	}
	public void setDeletePosition(List<DtoPositionPayCode> deletePosition) {
		this.deletePosition = deletePosition;
	}
	
	public List<PositionPayCode> getPositionPayCode() {
		return positionPayCode;
	}
	public void setPositionPayCode(List<PositionPayCode> positionPayCode) {
		this.positionPayCode = positionPayCode;
	}
	public DtoPositionPayCode() {
	}
	
	public DtoPositionPayCode(PositionPayCode positionPayCode) {
	}
	public String getPositionPlanDesc() {
		return positionPlanDesc;
	}
	public void setPositionPlanDesc(String positionPlanDesc) {
		this.positionPlanDesc = positionPlanDesc;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	
	public Integer getBenefitMainId() {
		return benefitMainId;
	}
	public void setBenefitMainId(Integer benefitMainId) {
		this.benefitMainId = benefitMainId;
	}
	public Integer getDeductionMainId() {
		return deductionMainId;
	}
	public void setDeductionMainId(Integer deductionMainId) {
		this.deductionMainId = deductionMainId;
	}
	public String getDeductionId() {
		return deductionId;
	}
	public void setDeductionId(String deductionId) {
		this.deductionId = deductionId;
	}
	public Integer getPayCodeMainId() {
		return payCodeMainId;
	}
	public void setPayCodeMainId(Integer payCodeMainId) {
		this.payCodeMainId = payCodeMainId;
	}
	public Integer getPositionplanId() {
		return positionplanId;
	}
	public void setPositionplanId(Integer positionplanId) {
		this.positionplanId = positionplanId;
	}
	
	
}
