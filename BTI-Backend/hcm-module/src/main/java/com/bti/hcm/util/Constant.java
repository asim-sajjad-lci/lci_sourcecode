package com.bti.hcm.util;

public class Constant {

	
	public static final String WORKFLOW_USERNAME = "userName";
	public static final String WORKFLOW_CLIENT_ID = "clientId";
	public static final String WORKFLOW_CLIENT_TOKEN = "token";
	public static final String LANG_ID = "langId";
}
