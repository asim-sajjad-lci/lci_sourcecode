/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.HijriMonth;

/**
 * Description: Interface for Hijri Month
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 * 
 *
 */
@Repository("repositoryHijriMonth")
public interface RepositoryHijriMonth extends JpaRepository<HijriMonth, Integer> {

	/**
	 * @param deleted
	 * @return
	 */
	public List<HijriMonth> findByIsDeleted(Boolean deleted);
	
	
	@Query("select p from HijriMonth p where (p.id =:id) and p.isDeleted=false")
	public HijriMonth findByHijriMonthId(@Param("id")int id);
	
}
