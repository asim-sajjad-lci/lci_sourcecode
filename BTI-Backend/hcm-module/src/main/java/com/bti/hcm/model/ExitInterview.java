package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR41102",indexes = {
        @Index(columnList = "EXTITINDX")
})
@NamedQuery(name = "ExitInterview.findAll", query = "SELECT e FROM ExitInterview e")
public class ExitInterview extends HcmBaseEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EXTITINDX")
	private Integer id;
	
	
	 @Column(name = "EXTITSEQN")
     private Integer sequence;
	
	@Column(name = "EXTITID",columnDefinition="char(7)")
	private String exitInterviewId;
	
	@Column(name = "EXTITDSCR",columnDefinition="char(100)")
	private String desc;
	
	@Column(name = "EXTITDSCRA",columnDefinition="char(100)")
	private String arbicDesc;
	
	@Column(name = "EXTITINCT")
	private boolean inActive;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExitInterviewId() {
		return exitInterviewId;
	}

	public void setExitInterviewId(String exitInterviewId) {
		this.exitInterviewId = exitInterviewId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	
	
	
}
