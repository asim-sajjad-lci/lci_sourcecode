package com.bti.hcm.util;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.bti.hcm.model.dto.DtoDepartment;

@Service("preparePrintingCSV")
public class PreparePrintingCSV {

	
	public ArrayList<String> prepareArrayListForPrinting(ArrayList<DtoDepartment> toPrepare){
		
		ArrayList<String> Prepared = new ArrayList<>();
		
		toPrepare.stream().forEach(department -> {
			AtomicInteger departmentCounter = new AtomicInteger(0);
			String departmentData = "";
			departmentData = departmentCounter.getAndIncrement()+"";
			departmentData += ",";
			departmentData += department.getDepartmentId();
			departmentData += ",";
			departmentData +=department.getDepartmentName();
			Prepared.add(departmentData);
			department.getEmployeeList().stream().forEach(employee -> {
				String employeeData = "";
				employeeData += employee.getEmployeeCode();
				employeeData +=",";
				employeeData += employee.getName();
				employeeData +=",";
				employeeData += employee.getStartSate();
				employeeData +=",";
				
				employeeData += employee.getPayCodeList().stream()
                .map(n -> n.intValue()+"") // This will call paycode.initValue()
                .collect(Collectors.joining(", "));
				

				employeeData +=",";
				employeeData += employee.getBenefitList().stream()
		        .map(n -> n.intValue()+"") // This will call paycode.initValue()
		        .collect(Collectors.joining(", "));
				employeeData +=",";
				employeeData += employee.getDeductionList().stream()
		        .map(n -> n.intValue()+"") // This will call paycode.initValue()
		        .collect(Collectors.joining(", "));
				employeeData +=",";
				Prepared.add(employeeData);
				
			});
		});
		
		return Prepared;
	}
	
	public String[] convertToStrinArray(ArrayList<String> data) {
		
		
		String[] csvPrint = new String[data.size()];
		int i =0;
		for (String rec : data) {
			csvPrint[i++] = rec ;
		}
		
		return csvPrint;
	}

}
