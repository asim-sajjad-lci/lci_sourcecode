/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Description: The persistent class for the ApplicantCost database table. Name
 * of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00902",indexes = {
        @Index(columnList = "APLCSTINDX")
})
public class ApplicantCost extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "APLCSTINDX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "REQUIINDX")
	private Requisitions requisitions;

	@Column(name = "REQSEQCN")
	private Integer sequence;

	@Column(name = "APPLICINDX")
	private Applicant applicant;

	@Column(name = "CSTTRAVL", precision = 13, scale = 3)
	private BigDecimal travelCost;

	@Column(name = "CSTLDGN", precision = 13, scale = 3)
	private BigDecimal lodgingCost;

	@Column(name = "CSTMOV", precision = 13, scale = 3)
	private BigDecimal movingCost;

	@Column(name = "CSTOTHR", precision = 13, scale = 3)
	private BigDecimal otherCost;

	@Column(name = "REQTOTAL", precision = 13, scale = 3)
	private BigDecimal totalCost;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Requisitions getRequisitions() {
		return requisitions;
	}

	public void setRequisitions(Requisitions requisitions) {
		this.requisitions = requisitions;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public BigDecimal getTravelCost() {
		return travelCost;
	}

	public void setTravelCost(BigDecimal travelCost) {
		this.travelCost = travelCost;
	}

	public BigDecimal getLodgingCost() {
		return lodgingCost;
	}

	public void setLodgingCost(BigDecimal lodgingCost) {
		this.lodgingCost = lodgingCost;
	}

	public BigDecimal getMovingCost() {
		return movingCost;
	}

	public void setMovingCost(BigDecimal movingCost) {
		this.movingCost = movingCost;
	}

	public BigDecimal getOtherCost() {
		return otherCost;
	}

	public void setOtherCost(BigDecimal otherCost) {
		this.otherCost = otherCost;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((applicant == null) ? 0 : applicant.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lodgingCost == null) ? 0 : lodgingCost.hashCode());
		result = prime * result + ((movingCost == null) ? 0 : movingCost.hashCode());
		result = prime * result + ((otherCost == null) ? 0 : otherCost.hashCode());
		result = prime * result + ((requisitions == null) ? 0 : requisitions.hashCode());
		result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
		result = prime * result + ((totalCost == null) ? 0 : totalCost.hashCode());
		result = prime * result + ((travelCost == null) ? 0 : travelCost.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
