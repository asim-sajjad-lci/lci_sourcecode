package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90201")
public class ParollTransactionOpenYearDistribution extends HcmBaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RWPRLINDX")
	private Integer id;

	@Column(name = "HCMTRXPYRL")
	private String auditTransactionNumber;

	@Column(name = "EMPLOYINDX")
	private Integer employeeIndexId;

	@ManyToOne
	@JoinColumn(name = "POTINDX")
	private Position position;
	
	@ManyToOne
	@JoinColumn(name = "DEPINDX")
	private Department department;  
	
	@ManyToOne
	@JoinColumn(name = "DIMINXVALUE")
	private FinancialDimensionsValues financialDimensionsValues;  
	
	@Column(name = "ACTROWID")
	private Long accountTableRowIndex;

	@Column(name = "DITPOTTYP")
	private Short postingTypes;

	@Column(name = "HCMCODINX")
	private Integer codeIndexId;

	@Column(name = "DEBTAMT", precision = 10, scale = 3)
	private BigDecimal debitAmount;

	@Column(name = "CRDTAMT", precision = 10, scale = 3)
	private BigDecimal creaditAmount;

	@Column(name = "ACCTSEQN")
	private Integer accountSequence;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuditTransactionNumber() {
		return auditTransactionNumber;
	}

	public void setAuditTransactionNumber(String auditTransactionNumber) {
		this.auditTransactionNumber = auditTransactionNumber;
	}

	public Integer getEmployeeIndexId() {
		return employeeIndexId;
	}

	public void setEmployeeIndexId(Integer employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}

	public Long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(Long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Short getPostingTypes() {
		return postingTypes;
	}

	public void setPostingTypes(Short postingTypes) {
		this.postingTypes = postingTypes;
	}

	public Integer getCodeIndexId() {
		return codeIndexId;
	}

	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreaditAmount() {
		return creaditAmount;
	}

	public void setCreaditAmount(BigDecimal creaditAmount) {
		this.creaditAmount = creaditAmount;
	}

	public Integer getAccountSequence() {
		return accountSequence;
	}

	public void setAccountSequence(Integer accountSequence) {
		this.accountSequence = accountSequence;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public FinancialDimensionsValues getFinancialDimensionsValues() {
		return financialDimensionsValues;
	}

	public void setFinancialDimensionsValues(FinancialDimensionsValues financialDimensionsValues) {
		this.financialDimensionsValues = financialDimensionsValues;
	}

}
