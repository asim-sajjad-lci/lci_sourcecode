package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class DtoManualChecks extends DtoBase {

	private Integer id;
	private String description;
	private Short checkType;
	private Integer checkNumber;
	private Date checkDate;
	private Date postDate;
	private BigDecimal grossAmount;
	private BigDecimal netAmount;
	private List<DtoManualChecks> delete;
	
	
	private DtoEmployeeMasterHcm dtoEmployeeMasterHcm;
	private DtoManualChecksDistribution dtoManualChecksDistribution;
	private DtoManualChecksDetails dtoManualChecksDetails;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Short getCheckType() {
		return checkType;
	}

	public void setCheckType(Short checkType) {
		this.checkType = checkType;
	}

	public Integer getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public BigDecimal getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public List<DtoManualChecks> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoManualChecks> delete) {
		this.delete = delete;
	}

	public DtoEmployeeMasterHcm getDtoEmployeeMasterHcm() {
		return dtoEmployeeMasterHcm;
	}

	public void setDtoEmployeeMasterHcm(DtoEmployeeMasterHcm dtoEmployeeMasterHcm) {
		this.dtoEmployeeMasterHcm = dtoEmployeeMasterHcm;
	}

	public DtoManualChecksDistribution getDtoManualChecksDistribution() {
		return dtoManualChecksDistribution;
	}

	public void setDtoManualChecksDistribution(DtoManualChecksDistribution dtoManualChecksDistribution) {
		this.dtoManualChecksDistribution = dtoManualChecksDistribution;
	}

	public DtoManualChecksDetails getDtoManualChecksDetails() {
		return dtoManualChecksDetails;
	}

	public void setDtoManualChecksDetails(DtoManualChecksDetails dtoManualChecksDetails) {
		this.dtoManualChecksDetails = dtoManualChecksDetails;
	}

}
