package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.COAMainAccounts;


@Repository("repositoryCOAMainAccount")
public interface RepositoryCOAMainAccount  extends JpaRepository<COAMainAccounts, Integer>{
	
	COAMainAccounts findByIdAndIsDeleted(Integer id, boolean b);
	
	@Query("select a from COAMainAccounts a where a.id =:id")
	COAMainAccounts findByAccountNumber(@Param("id") int id);
	
	@Query("select d from COAMainAccounts d where d.isDeleted = false")
	List<COAMainAccounts> getAllData();
}
