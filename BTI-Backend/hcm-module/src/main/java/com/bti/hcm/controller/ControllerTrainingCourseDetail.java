package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTrainingCourseDetail;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTrainingCourseDetail;

@RestController
@RequestMapping("/trainingCourseDetail")
public class ControllerTrainingCourseDetail extends BaseController{
	
	
private static final Logger LOGGER = Logger.getLogger(ControllerTrainingCourseDetail.class);
	
	@Autowired(required=true)
	ServiceTrainingCourseDetail serviceTrainingCourseDetail;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoTrainingCourseDetail dtoTrainingCourseDetail) throws Exception {
		LOGGER.info("Create TrainingCourseDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTrainingCourseDetail = serviceTrainingCourseDetail.saveOrUpdateTraningCourseDetail(dtoTrainingCourseDetail);
			responseMessage=displayMessage(dtoTrainingCourseDetail, "TRAININGCOURSE_DETAIL_CREATED", "TRAININGCOURSE_DETAIL_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create TrainingCourseDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoTrainingCourseDetail dtoTrainingCourseDetail) throws Exception {
		LOGGER.info("Update TrainingCourseDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTrainingCourseDetail = serviceTrainingCourseDetail.saveOrUpdateTraningCourseDetail(dtoTrainingCourseDetail);
			responseMessage=displayMessage(dtoTrainingCourseDetail, "TRAININGCOURSE_UPDATED_SUCCESS", "TRAININGCOURSE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update TrainingCourseDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoTrainingCourseDetail dtoTrainingCourseDetail) throws Exception {
		LOGGER.info("Delete TrainingCourseDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTrainingCourseDetail.getIds() != null && !dtoTrainingCourseDetail.getIds().isEmpty()) {
				DtoTrainingCourseDetail dtoTrainingCourseDetail2 = serviceTrainingCourseDetail.deleteTraningCourseDetail(dtoTrainingCourseDetail.getIds());
				
				if(dtoTrainingCourseDetail2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("TRAININGCOURSEDETAIL_DELETED", false), dtoTrainingCourseDetail2);
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoTrainingCourseDetail2.getMessageType());
					btiMessageHcm.setMessageShort("TRAININGCOURSEDETAIL_NOT_DELETE_ID_MESSAGE");
					
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							btiMessageHcm, dtoTrainingCourseDetail2);
				}
				

				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete TrainingCourseDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search TrainingCourseDetail Methods");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTrainingCourseDetail.searchTraningCourseDetail(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.TRAININGCOURSEDETAIL_GET_ALL, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.TRAININGCOURSEDETAIL_LIST_NOT_GETTING, false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TrainingCourseDetails Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getTraningCourseDetailById", method = RequestMethod.POST)
	public ResponseMessage getTraningCourseDetailById(HttpServletRequest request, @RequestBody DtoTrainingCourseDetail dtoTrainingCourseDetail) throws Exception {
		LOGGER.info("Get TrainingCourseDetail ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTrainingCourseDetail dtoTrainingCourseDetailObj = serviceTrainingCourseDetail.getTraningCourseDetailById(dtoTrainingCourseDetail.getId());
			responseMessage=displayMessage(dtoTrainingCourseDetailObj, "TRAININGCOURSEDETAILLIST_GET_DETAIL", "TRAININGCOURSEDETAIL_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get TrainingCourseDetail by Id Method:"+dtoTrainingCourseDetail.getId());
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/classIdcheck", method = RequestMethod.POST)
	public ResponseMessage classIdcheck(HttpServletRequest request, @RequestBody DtoTrainingCourseDetail dtoTrainingCourseDetail) throws Exception {
		LOGGER.info("classIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTrainingCourseDetail dtoTrainingCourseDetailObj = serviceTrainingCourseDetail.repeatByClassId(dtoTrainingCourseDetail.getClassId());
			if (dtoTrainingCourseDetailObj != null) {
				if (dtoTrainingCourseDetailObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("TRANING_COURSE_RESULT", false), dtoTrainingCourseDetailObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoTrainingCourseDetailObj.getMessageType(), false),
							dtoTrainingCourseDetailObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("TRANING_COURSE_REPEAT_TRANINGID_NOT_FOUND", false), dtoTrainingCourseDetailObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search TrainingCourseDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTrainingCourseDetail.getAllIds(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "TRAININGCOURSEDETAIL_GET_ALL", "TRAININGCOURSEDETAIL_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TrainingCourseDetail Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllByTraningId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByTraningId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search TrainingCourseDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTrainingCourseDetail.getAllByTraningId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "TRAININGCOURSEDETAIL_GET_ALL", "TRAININGCOURSEDETAIL_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TrainingCourseDetail Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	

}
