/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Description: The persistent class for the EmployeeNationalities database table. 
 * Name of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00936",indexes = {
        @Index(columnList = "EMPNAINDX")
})
@NamedQuery(name = "EmployeeNationalities.findAll", query = "SELECT e FROM EmployeeNationalities e")
public class EmployeeNationalities extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPNAINDX")
	private int employeeNationalityIndexId;

	@Column(name = "EMPNAID", columnDefinition = "char(5)")
	private String employeeNationalityId;

	@Column(name = "EMPNADSCR", columnDefinition = "char(60)")
	private String employeeNationalityDescription;

	@Column(name = "EMPNADSCRA", columnDefinition = "char(90)")
	private String employeeNationalityDescriptionArabic;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "employeeNationalities")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<EmployeeMaster> listEmployeeMaster;


	public int getEmployeeNationalityIndexId() {
		return employeeNationalityIndexId;
	}

	public void setEmployeeNationalityIndexId(int employeeNationalityIndexId) {
		this.employeeNationalityIndexId = employeeNationalityIndexId;
	}

	public String getEmployeeNationalityId() {
		return employeeNationalityId;
	}

	public void setEmployeeNationalityId(String employeeNationalityId) {
		this.employeeNationalityId = employeeNationalityId;
	}

	public String getEmployeeNationalityDescription() {
		return employeeNationalityDescription;
	}

	public void setEmployeeNationalityDescription(String employeeNationalityDescription) {
		this.employeeNationalityDescription = employeeNationalityDescription;
	}

	public String getEmployeeNationalityDescriptionArabic() {
		return employeeNationalityDescriptionArabic;
	}

	public void setEmployeeNationalityDescriptionArabic(String employeeNationalityDescriptionArabic) {
		this.employeeNationalityDescriptionArabic = employeeNationalityDescriptionArabic;
	}

	public List<EmployeeMaster> getListEmployeeMaster() {
		return listEmployeeMaster;
	}

	public void setListEmployeeMaster(List<EmployeeMaster> listEmployeeMaster) {
		this.listEmployeeMaster = listEmployeeMaster;
	}

}
