/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeAddressMaster;

/**
 * Description: Interface for EmployeeAddressMaste 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryEmployeeAddressMaster")
public interface RepositoryEmployeeAddressMaster extends JpaRepository<EmployeeAddressMaster, Integer> {

	EmployeeAddressMaster findByEmployeeAddressIndexIdAndIsDeleted(Integer id, boolean b);
	
	public List<EmployeeAddressMaster> findByIsDeleted(Boolean deleted);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeAddressMaster d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteEmployeeAddressMaster(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);
	
	@Query("select count(*) from EmployeeAddressMaster d where ( d.addressId like :searchKeyWord or d.address1 like :searchKeyWord or d.address2 like :searchKeyWord or d.personalEmail like :searchKeyWord or d.businessEmail like :searchKeyWord or d.addressArabic1 like :searchKeyWord or d.addressArabic2 like :searchKeyWord or d.pOBox like :searchKeyWord  or d.personalPhone like :searchKeyWord or d.businessPhone like :searchKeyWord or d.otherPhone like :searchKeyWord or d.ext like :searchKeyWord or d.locationLinkGoogleMap like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveEmployeeAddressMasterSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from EmployeeAddressMaster d where ( d.addressId like :searchKeyWord or d.address1 like :searchKeyWord or d.address2 like :searchKeyWord or d.personalEmail like :searchKeyWord or d.businessEmail like :searchKeyWord or d.addressArabic1 like :searchKeyWord or d.addressArabic2 like :searchKeyWord or d.pOBox like :searchKeyWord  or d.personalPhone like :searchKeyWord or d.businessPhone like :searchKeyWord or d.otherPhone like :searchKeyWord or d.ext like :searchKeyWord or d.locationLinkGoogleMap like :searchKeyWord) and d.isDeleted=false")
	List<EmployeeAddressMaster> predictiveEmployeeAddressMasterSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	List<EmployeeAddressMaster> findByAddressId(String timeCodeId);

	@Query("select count(*) from EmployeeAddressMaster e ")
	Integer getCountOfTotalEmployeeAddressMaster();
}
