package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeDirectDeposit;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeDirectDeposit;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryEmployeeDirectDeposit;
import com.bti.hcm.repository.RepositoryEmployeeMaster;

@Service("serviceEmployeeDirectDeposit")
public class ServiceEmployeeDirectDeposit {
	
	


	/**
	 * @Description LOGGER use for put a logger in EmployeeContacts Service
	 */
	static Logger log = Logger.getLogger(ServiceEmployeeDirectDeposit.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */
	@Autowired
	ServiceResponse response;
	
	
	/**
	 * @Description RepositoryTimeCode Autowired here using annotation of spring for access of RepositoryTimeCode method in TimeCode service
	 */
	@Autowired(required=false)
	RepositoryEmployeeDirectDeposit repositoryEmployeeDirectDeposit;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	

	
	
	public DtoEmployeeDirectDeposit saveOrUpdate(DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit1) {
		try {
				List<String> d = dtoEmployeeDirectDeposit1.getListEmployeeDirectDeposit().stream()
		    .map(DtoEmployeeDirectDeposit::getAccountNumber)
		    .collect(Collectors.toList());
			
			 Set<String> s = new HashSet<>(d);
			 
			 if(d.size() !=s.size()) {
				 dtoEmployeeDirectDeposit1 = null;
				 return dtoEmployeeDirectDeposit1;
			 }

			
			log.info("saveOrUpdate EmployeeDirectDeposit Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			if(dtoEmployeeDirectDeposit1.getListEmployeeDirectDeposit().get(0)!=null &&	dtoEmployeeDirectDeposit1.getListEmployeeDirectDeposit().get(0).getEmployeeMaster().getEmployeeIndexId()>0) {
				repositoryEmployeeDirectDeposit.deleteByEmployeeId(true, loggedInUserId,dtoEmployeeDirectDeposit1.getListEmployeeDirectDeposit().get(0).getEmployeeMaster().getEmployeeIndexId());
			}
			
			
			for (DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit : dtoEmployeeDirectDeposit1.getListEmployeeDirectDeposit()) {
				
				EmployeeDirectDeposit employeeDirectDeposit = null;
				if (dtoEmployeeDirectDeposit.getId() != null && dtoEmployeeDirectDeposit.getId() > 0) {
					
					employeeDirectDeposit = repositoryEmployeeDirectDeposit.findByIdAndIsDeleted(dtoEmployeeDirectDeposit.getId(), false);
					employeeDirectDeposit.setUpdatedBy(loggedInUserId);
					employeeDirectDeposit.setUpdatedDate(new Date());
				} else {
					employeeDirectDeposit = new EmployeeDirectDeposit();
					employeeDirectDeposit.setCreatedDate(new Date());
					Integer rowId = repositoryEmployeeDirectDeposit.getCountOfTotalEmployeeDirectDeposit();
					Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					employeeDirectDeposit.setRowId(increment);
				}
				
				EmployeeMaster employeeMaster=null;
				if(dtoEmployeeDirectDeposit.getEmployeeMaster()!=null && dtoEmployeeDirectDeposit.getEmployeeMaster().getEmployeeIndexId()>0) {
					employeeMaster=repositoryEmployeeMaster.findOne(dtoEmployeeDirectDeposit.getEmployeeMaster().getEmployeeIndexId());
				}
				employeeDirectDeposit.setEmployeeMaster(employeeMaster);
				employeeDirectDeposit.setBankName(dtoEmployeeDirectDeposit.getBankName());
				employeeDirectDeposit.setAccountNumber(dtoEmployeeDirectDeposit.getAccountNumber());
				employeeDirectDeposit.setAmount(dtoEmployeeDirectDeposit.getAmount());
				employeeDirectDeposit.setPercent(dtoEmployeeDirectDeposit.getPercent());
				employeeDirectDeposit.setStatus(dtoEmployeeDirectDeposit.getStatus());
				employeeDirectDeposit.setAccountSequence(dtoEmployeeDirectDeposit.getAccountSequence());
				
				employeeDirectDeposit = repositoryEmployeeDirectDeposit.saveAndFlush(employeeDirectDeposit);
				dtoEmployeeDirectDeposit.setId(employeeDirectDeposit.getId());
				log.debug("EmployeeDirectDeposit is:"+employeeDirectDeposit.getId());
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDirectDeposit1;
	
	}



	
	
	
	public DtoEmployeeDirectDeposit delete(List<Integer> ids) {
		log.info("delete EmployeeDirectDeposit Method");
		DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit = new DtoEmployeeDirectDeposit();
		dtoEmployeeDirectDeposit.setDeleteMessage(response.getStringMessageByShortAndIsDeleted("EMPLOYEE_DIRECT_DEPOSIT_DELETED", false));
		dtoEmployeeDirectDeposit.setAssociateMessage(response.getStringMessageByShortAndIsDeleted("EMPLOYEE_DIRECT_DEPOSIT_ASSOCIATED", false));
		List<DtoEmployeeDirectDeposit> delete = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("EMPLOYEE_DIRECT_DEPOSIT_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer employeeDirectDepositId : ids) {
				
				List<EmployeeDirectDeposit> list = repositoryEmployeeDirectDeposit.getEmployeeMaster(employeeDirectDepositId);
				for (EmployeeDirectDeposit employeeDirectDeposit : list) {
					if(employeeDirectDeposit.getId()!=null) {
						repositoryEmployeeDirectDeposit.deleteSingleEmployeeDirectDeposit(true, loggedInUserId, employeeDirectDeposit.getId());
						DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit2 = new DtoEmployeeDirectDeposit();
						dtoEmployeeDirectDeposit2.setId(employeeDirectDeposit.getId());
						
						
						repositoryEmployeeDirectDeposit.deleteSingleEmployeeDirectDeposit(true, loggedInUserId, employeeDirectDepositId);
						delete.add(dtoEmployeeDirectDeposit2);	
					}else {
						inValidDelete = true;
					}
					
				}
				
				
				

			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoEmployeeDirectDeposit.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoEmployeeDirectDeposit.setMessageType("");
				
			}
			
			dtoEmployeeDirectDeposit.setDelete(delete);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeeDirectDeposit :"+dtoEmployeeDirectDeposit.getId());
		return dtoEmployeeDirectDeposit;
	}




	public DtoEmployeeDirectDeposit getById(int id) {
		log.info("getById Method");
		DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit = new DtoEmployeeDirectDeposit();
		try {

			if (id > 0) {
				EmployeeDirectDeposit employeeDirectDeposit = repositoryEmployeeDirectDeposit.findByIdAndIsDeleted(id, false);
				if (employeeDirectDeposit != null) {
					dtoEmployeeDirectDeposit = new DtoEmployeeDirectDeposit(employeeDirectDeposit);
					
					   
					    dtoEmployeeDirectDeposit.setId(employeeDirectDeposit.getId());
					    dtoEmployeeDirectDeposit.setBankName(employeeDirectDeposit.getBankName());
					    dtoEmployeeDirectDeposit.setAccountNumber(employeeDirectDeposit.getAccountNumber());
					    dtoEmployeeDirectDeposit.setAmount(employeeDirectDeposit.getAmount());
					    dtoEmployeeDirectDeposit.setPercent(employeeDirectDeposit.getPercent());
					    dtoEmployeeDirectDeposit.setStatus(employeeDirectDeposit.getStatus());
					    dtoEmployeeDirectDeposit.setAccountSequence(employeeDirectDeposit.getAccountSequence());
						
		             
					} else {
						dtoEmployeeDirectDeposit.setMessageType("EMPLOYEE_DIRECT_DEPOSIT_NOT_GETTING");

				}
			} else {
				dtoEmployeeDirectDeposit.setMessageType("INVALID_EMPLOYEE_DIRECT_DEPOSIT_ID");

			}
			log.debug("Get By Id is:"+dtoEmployeeDirectDeposit.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDirectDeposit;
		
	}



	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {

			log.info("search EmployeeDirectDeposit Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("bankName") ||
							dtoSearch.getSortOn().equals("accountNumber") || dtoSearch.getSortOn().equals("employeeId")) {
						
			if(dtoSearch.getSortOn().equals("employeeId"))	{
				dtoSearch.setSortBy("employeeMaster.employeeIndexId");
			}
			
			if(dtoSearch.getSortOn().equals("employeeName"))	{
				dtoSearch.setSortBy("employeeMaster.employeeFirstName");
			}
			condition = dtoSearch.getSortOn();
					
				}else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
			
				LinkedHashSet<String> list =	repositoryEmployeeDirectDeposit.getAllDisctint();
				
				
				dtoSearch.setTotalCount(this.repositoryEmployeeDirectDeposit.predictiveEmployeeDirectDepositSearchTotalCount("%"+searchWord+"%"));
				List<EmployeeDirectDeposit> employeeDirectDepositList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeeDirectDepositList = repositoryEmployeeDirectDeposit.predictiveEmployeeDirectDepositSearchWithPaginationNew("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeeDirectDepositList = repositoryEmployeeDirectDeposit.predictiveEmployeeDirectDepositSearchWithPaginationNew("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeeDirectDepositList = repositoryEmployeeDirectDeposit.predictiveEmployeeDirectDepositSearchWithPaginationNew("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				
				List<DtoEmployeeDirectDeposit> deposits = new ArrayList<>();
				List<DtoEmployeeDirectDeposit> depositsNewList = new ArrayList<>();
				for (String id : list) {
				List<EmployeeMaster> employeeMaster1 =	repositoryEmployeeMaster.findByEmployeeId(id);
					
					DtoEmployeeDirectDeposit deposit = new DtoEmployeeDirectDeposit();
					List<DtoEmployeeDirectDeposit> depositList = new ArrayList<>();
					deposit.setListEmployeeDirectDeposit(depositList);
					DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
					List<EmployeeDirectDeposit> listDirct = employeeDirectDepositList.stream().filter(d-> d.getEmployeeMaster().getEmployeeId().equals(id)).collect(Collectors.toList());
					
					if(!listDirct.isEmpty()) {
						for (EmployeeDirectDeposit employeeDirectDeposit : listDirct) {
							DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit = new DtoEmployeeDirectDeposit();
							dtoEmployeeDirectDeposit.setId(employeeDirectDeposit.getId());
						    dtoEmployeeDirectDeposit.setBankName(employeeDirectDeposit.getBankName());
						    dtoEmployeeDirectDeposit.setAccountNumber(employeeDirectDeposit.getAccountNumber());
						    dtoEmployeeDirectDeposit.setAmount(employeeDirectDeposit.getAmount());
						    dtoEmployeeDirectDeposit.setPercent(employeeDirectDeposit.getPercent());
						    dtoEmployeeDirectDeposit.setStatus(employeeDirectDeposit.getStatus());
						    dtoEmployeeDirectDeposit.setAccountSequence(employeeDirectDeposit.getAccountSequence());
						    deposit.getListEmployeeDirectDeposit().add(dtoEmployeeDirectDeposit);
							
						}
						
						if(!employeeMaster1.isEmpty()) {
							employeeMaster.setEmployeeIndexId(employeeMaster1.get(0).getEmployeeIndexId());
							employeeMaster.setEmployeeId(employeeMaster1.get(0).getEmployeeId());
							employeeMaster.setEmployeeBirthDate(employeeMaster1.get(0).getEmployeeBirthDate());
							employeeMaster.setEmployeeCitizen(employeeMaster1.get(0).isEmployeeCitizen());
							employeeMaster.setEmployeeFirstName(employeeMaster1.get(0).getEmployeeFirstName());
							employeeMaster.setEmployeeFirstNameArabic(employeeMaster1.get(0).getEmployeeFirstNameArabic());
							employeeMaster.setEmployeeGender(employeeMaster1.get(0).getEmployeeGender());
							employeeMaster.setEmployeeHireDate(employeeMaster1.get(0).getEmployeeHireDate());
							employeeMaster.setEmployeeImmigration(employeeMaster1.get(0).isEmployeeImmigration());
							employeeMaster.setEmployeeInactive(employeeMaster1.get(0).isEmployeeInactive());
							employeeMaster.setEmployeeInactiveDate(employeeMaster1.get(0).getEmployeeInactiveDate());
							employeeMaster.setEmployeeInactiveReason(employeeMaster1.get(0).getEmployeeInactiveReason());
							employeeMaster.setEmployeeLastName(employeeMaster1.get(0).getEmployeeLastName());
							employeeMaster.setEmployeeLastNameArabic(employeeMaster1.get(0).getEmployeeLastNameArabic());
							employeeMaster.setEmployeeLastWorkDate(employeeMaster1.get(0).getEmployeeLastWorkDate());
							employeeMaster.setEmployeeMaritalStatus(employeeMaster1.get(0).getEmployeeMaritalStatus());
							employeeMaster.setEmployeeMiddleName(employeeMaster1.get(0).getEmployeeMiddleName());
							employeeMaster.setEmployeeMiddleNameArabic(employeeMaster1.get(0).getEmployeeMiddleNameArabic());
							employeeMaster.setEmployeeTitle(employeeMaster1.get(0).getEmployeeTitle());
							employeeMaster.setEmployeeTitleArabic(employeeMaster1.get(0).getEmployeeTitleArabic());
							employeeMaster.setEmployeeType(employeeMaster1.get(0).getEmployeeType());
							employeeMaster.setEmployeeUserIdInSystem(employeeMaster1.get(0).getEmployeeUserIdInSystem());
							employeeMaster.setEmployeeWorkHourYearly(employeeMaster1.get(0).getEmployeeWorkHourYearly());
							deposit.setEmployeeMaster(employeeMaster);
						}
						
						deposits.add(deposit);
					}
					
					
				}
				
				dtoSearch.setTotalCount(deposits.size());
				if(dtoSearch.getPageNumber().equals(0)) {
					
					if(deposits.size() <= dtoSearch.getPageSize()) {
						dtoSearch.setRecords(deposits);
					}else {
						depositsNewList = deposits.subList(0, dtoSearch.getPageSize());
						dtoSearch.setRecords(depositsNewList);
					}
					
				}else {
					if(deposits.size() >  dtoSearch.getPageSize() * (dtoSearch.getPageNumber()+1)) {
						depositsNewList = deposits .subList(dtoSearch.getPageNumber() * dtoSearch.getPageSize() ,dtoSearch.getPageSize() * (dtoSearch.getPageNumber()+1));
						dtoSearch.setRecords(depositsNewList);
					}
					else {
						deposits.subList(0, dtoSearch.getPageSize()*dtoSearch.getPageNumber()-1).clear();
						dtoSearch.setRecords(deposits);
					}
					
				}
				
				
			}
			log.debug("Search EmployeeDirectDeposit Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}






	public DtoEmployeeDirectDeposit repeatByEmployeeIdcheckId(Integer id) {
		log.info("repeat EmployeeDirectDeposit Method");
		DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit = new DtoEmployeeDirectDeposit();
		try {
			List<EmployeeDirectDeposit> employeeDirectDeposit=repositoryEmployeeDirectDeposit.getEmployeeMaster(id);
			if(employeeDirectDeposit!=null && !employeeDirectDeposit.isEmpty()) {
				dtoEmployeeDirectDeposit.setIsRepeat(true);
			}else {
				dtoEmployeeDirectDeposit.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDirectDeposit;
	}
	
	
}
	
	
	


