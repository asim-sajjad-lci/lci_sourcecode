package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40914",indexes = {
        @Index(columnList = "SMTXRINDX")
})
public class SalaryMatrixRow extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SMTXRINDX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "SMTXINDX")
	private SalaryMatrixSetup salaryMatrixSetup;

	@Column(name = "SMTXRDSCR")
	private String desc;

	@Column(name = "SMTXRDSCRA")
	private String arbic;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "salaryMatrixRow")
	@Where(clause = "is_deleted = false")
	@OrderBy("sequence ASC")
	private List<SalaryMatrixRowValue> salaryMatrixRowValue;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SalaryMatrixSetup getSalaryMatrixSetup() {
		return salaryMatrixSetup;
	}

	public void setSalaryMatrixSetup(SalaryMatrixSetup salaryMatrixSetup) {
		this.salaryMatrixSetup = salaryMatrixSetup;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbic() {
		return arbic;
	}

	public void setArbic(String arbic) {
		this.arbic = arbic;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((arbic == null) ? 0 : arbic.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((salaryMatrixSetup == null) ? 0 : salaryMatrixSetup.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalaryMatrixRow other = (SalaryMatrixRow) obj;
		if (arbic == null) {
			if (other.arbic != null)
				return false;
		} else if (!arbic.equals(other.arbic))
			return false;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (salaryMatrixSetup == null) {
			if (other.salaryMatrixSetup != null)
				return false;
		} else if (!salaryMatrixSetup.equals(other.salaryMatrixSetup))
			return false;
		return true;
	}

	public List<SalaryMatrixRowValue> getSalaryMatrixRowValue() {
		return salaryMatrixRowValue;
	}

	public void setSalaryMatrixRowValue(List<SalaryMatrixRowValue> salaryMatrixRowValue) {
		this.salaryMatrixRowValue = salaryMatrixRowValue;
	}

}
