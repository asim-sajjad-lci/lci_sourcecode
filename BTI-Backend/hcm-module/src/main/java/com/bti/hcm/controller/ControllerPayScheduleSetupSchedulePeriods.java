/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPayScheduleSetupSchedulePeriods;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayScheduleSetupSchedulePeriods;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: Controller PayScheduleSetupSchedulePeriods
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/PayScheduleSetupSchedulePeriods")
public class ControllerPayScheduleSetupSchedulePeriods extends BaseController{
	
	

	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupSchedulePeriods Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerPayScheduleSetupSchedulePeriods.class);
	
	/**
	 * @Description servicePayScheduleSetupSchedulePeriods Autowired here using annotation of spring for use of servicePayScheduleSetupSchedulePeriods method in this controller
	 */
	@Autowired(required=true)
	ServicePayScheduleSetupSchedulePeriods servicePayScheduleSetupSchedulePeriods;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @description : Create PayScheduleSetupSchedulePeriods
	 * @param request
	 * @param dtoPayScheduleSetupSchedulePeriods
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createPayScheduleSetupSchedulePeriods(HttpServletRequest request, @RequestBody DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods) throws Exception {
		LOGGER.info("Create PayScheduleSetupSchedulePeriods Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupSchedulePeriods = servicePayScheduleSetupSchedulePeriods.saveOrUpdatePayScheduleSetupSchedulePeriod(dtoPayScheduleSetupSchedulePeriods);
			responseMessage=displayMessage(dtoPayScheduleSetupSchedulePeriods, "PAY_SCHEDULE_SETUP_PERIODS_CREATED", "PAY_SCHEDULE_SETUP_PERIODS_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PayScheduleSetupSchedulePeriods Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @description update PayScheduleSetupSchedulePeriods
	 * @param request
	 * @param dtoPayScheduleSetupSchedulePeriods
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePayScheduleSetupSchedulePeriods(HttpServletRequest request, @RequestBody DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods) throws Exception {
		LOGGER.info("Update PayScheduleSetupSchedulePeriods Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupSchedulePeriods = servicePayScheduleSetupSchedulePeriods.saveOrUpdatePayScheduleSetupSchedulePeriod(dtoPayScheduleSetupSchedulePeriods);
			responseMessage=displayMessage(dtoPayScheduleSetupSchedulePeriods, "PAY_SCHEDULE_SETUP_PERIODS_UPDATED_SUCCESS", "PAY_SCHEDULE_SETUP_PERIODS_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update PayScheduleSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description delete PayScheduleSetupSchedulePeriods
	 * @param request
	 * @param dtoPayScheduleSetupSchedulePeriods
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePayScheduleSetupSchedulePeriods(HttpServletRequest request, @RequestBody DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods) throws Exception {
		LOGGER.info("Delete PayScheduleSetupSchedulePeriods Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayScheduleSetupSchedulePeriods.getIds() != null && !dtoPayScheduleSetupSchedulePeriods.getIds().isEmpty()) {
				DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods2 = servicePayScheduleSetupSchedulePeriods.deletePayScheduleSetupSchedulePeriods(dtoPayScheduleSetupSchedulePeriods.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_PERIODS_DELETED", false), dtoPayScheduleSetupSchedulePeriods2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete PayScheduleSetupSchedulePeriods Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description search PayScheduleSetupSchedulePeriods
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPayScheduleSetupSchedulePeriods(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search PayScheduleSetupSchedulePeriods Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayScheduleSetupSchedulePeriods.searchPayScheduleSetupSchedulePeriods(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_SCHEDULE_SETUP_PERIODS_GET_ALL", "PAY_SCHEDULE_SETUP_PERIODS_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PayScheduleSetupSchedulePeriods Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupSchedulePeriods
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPayScheduleSetupSchedulePeriodsById(HttpServletRequest request, @RequestBody DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods) throws Exception {
		LOGGER.info("Get PayScheduleSetupSchedulePeriods ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriodsObj = servicePayScheduleSetupSchedulePeriods.repeatByPayScheduleSetupSchedulePeriodsId(dtoPayScheduleSetupSchedulePeriods.getPayScheduleIndexId());
			responseMessage=displayMessage(dtoPayScheduleSetupSchedulePeriodsObj, "PAY_SCHEDULE_SETUP_PERIODS_GET_DETAIL", "PAY_SCHEDULE_SETUP_PERIODS_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get PayScheduleSetupSchedulePeriods ById Method:"+dtoPayScheduleSetupSchedulePeriods.getPayScheduleIndexId());
		return responseMessage;
	}
}
