package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Company;
import com.bti.hcm.model.Department;
import com.bti.hcm.model.Division;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.Requisitions;
import com.bti.hcm.model.Supervisor;
import com.bti.hcm.model.dto.DtoRequisitions;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryCompany;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryDivision;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositoryRequisitions;
import com.bti.hcm.repository.RepositorySupervisor;

@Service("servicehcmequisitions")
@Transactional
public class ServiceRequisitions {
	static Logger log = Logger.getLogger(ServiceAccrualSchedule.class.getName());
	
	

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryRequisitions repositoryRequisitions;
	
	@Autowired
	RepositoryDepartment repositoryDepartment;
	
	@Autowired
	RepositoryDivision repositoryDivision;
	
	@Autowired
	RepositorySupervisor repositorySupervisor;
	
	@Autowired
	RepositoryPosition repositoryPosition;
	
	
	@Autowired
	RepositoryCompany repositoryCompany;

	/**
	 * 
	 * @param dtoHcmRequisitions
	 * @return
	 */
	public DtoRequisitions saveOrUpdate(DtoRequisitions dtoHcmRequisitions) {
		log.info("saveOrUpdate Hcm requitition");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		Requisitions  hcmRequisitions = null;
		Department department = null;
		Supervisor supervisor = null;
		Division division = null;
		Position position = null;
		try {
			if (dtoHcmRequisitions.getId() != null && dtoHcmRequisitions.getId() > 0) {
				hcmRequisitions = repositoryRequisitions.findByIdAndIsDeleted(dtoHcmRequisitions.getId(), false);
				hcmRequisitions.setUpdatedBy(loggedInUserId);
				hcmRequisitions.setUpdatedDate(new Date());
			} else {
				hcmRequisitions = new Requisitions();
				hcmRequisitions.setCreatedDate(new Date());
				
				Integer rowId = repositoryRequisitions.getCountOfTotaRequisitions();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				hcmRequisitions.setRowId(increment);
			}
			
			Company company =	repositoryCompany.findOne(dtoHcmRequisitions.getCompanyId());
			
			department= repositoryDepartment.findOne(dtoHcmRequisitions.getDepartmentId());
			division = repositoryDivision.findOne(dtoHcmRequisitions.getDivisionId());
			supervisor = repositorySupervisor.findOne(dtoHcmRequisitions.getSupervisorId());
			position = repositoryPosition.findOne(dtoHcmRequisitions.getPositionId());
			hcmRequisitions.setId(dtoHcmRequisitions.getId());
			hcmRequisitions.setStats(dtoHcmRequisitions.getStats());
			hcmRequisitions.setInternalPostDate(dtoHcmRequisitions.getInternalPostDate());
			hcmRequisitions.setInternalCloseDate(dtoHcmRequisitions.getInternalCloseDate());
			hcmRequisitions.setRecruiterName(dtoHcmRequisitions.getRecruiterName());
			hcmRequisitions.setOpeningDate(dtoHcmRequisitions.getOpeningDate());
			hcmRequisitions.setCompanyId(company);
			hcmRequisitions.setManagerName(dtoHcmRequisitions.getManagerName());
			hcmRequisitions.setJobPosting(dtoHcmRequisitions.getJobPosting());
			hcmRequisitions.setAdvertisingField1(dtoHcmRequisitions.getAdvertisingField1());
			hcmRequisitions.setAdvertisingField2(dtoHcmRequisitions.getAdvertisingField2());
			hcmRequisitions.setAdvertisingField3(dtoHcmRequisitions.getAdvertisingField3());
			hcmRequisitions.setAdvertisingField4(dtoHcmRequisitions.getAdvertisingField4());
			hcmRequisitions.setPositionsAvailable(dtoHcmRequisitions.getPositionsAvailable());
			hcmRequisitions.setPositionsFilled(dtoHcmRequisitions.getPositionsFilled());
			hcmRequisitions.setApplicantsApplied(dtoHcmRequisitions.getApplicantsApplied());
			hcmRequisitions.setApplicantsInterviewed(dtoHcmRequisitions.getApplicantsInterviewed());
			hcmRequisitions.setCostadvirs(dtoHcmRequisitions.getCostadvirs());
			hcmRequisitions.setCostrecri(dtoHcmRequisitions.getCostrecri());
			hcmRequisitions.setDepartment(department);
			hcmRequisitions.setDivision(division);
			hcmRequisitions.setSupervisor(supervisor);
			hcmRequisitions.setPosition(position);
			repositoryRequisitions.saveAndFlush(hcmRequisitions);
		} catch (Exception e) {
			log.error(e);
		}
		
		log.debug("Hcm requitition:"+dtoHcmRequisitions.getId());
		return dtoHcmRequisitions;
	}
	
	
	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoRequisitions deleteRequisition(List<Integer> ids) {
		log.info("deleteDepartment Method");
		DtoRequisitions dtoHcmRequisitions = new DtoRequisitions();
		dtoHcmRequisitions
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DEPARTMENT_DELETED", false));
		dtoHcmRequisitions.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("DEPARTMENT_ASSOCIATED", false));
		List<DtoRequisitions> deleteRequisition= new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer departmentId : ids) {
				DtoRequisitions dtoHcmRequisitions2 = new DtoRequisitions();
				dtoHcmRequisitions2.setId(departmentId);
				repositoryRequisitions.deleteSingleRequisition(true, loggedInUserId, departmentId);
				deleteRequisition.add(dtoHcmRequisitions);

			}
			
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Requisitions :"+dtoHcmRequisitions.getId());
		return dtoHcmRequisitions;
	}
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchRequistion(DtoSearch dtoSearch) {
		log.info("searchRequistion Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "stats" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "internalPostDate" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "internalCloseDate" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "openingDate" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "recruiterName" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "companyId" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "managerName" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "jobPosting" : 
					condition+=dtoSearch.getSortOn();
					break;
					
				case "advertisingField1" : 
					condition+=dtoSearch.getSortOn();
					break;
					
				case "advertisingField2" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "advertisingField3" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "advertisingField4" : 
					condition+=dtoSearch.getSortOn();
					break;	
				case "positionsAvailable" : 
					condition+=dtoSearch.getSortOn();
					break;	
				case "positionsFilled" : 
					condition+=dtoSearch.getSortOn();
					break;	
					
				case "applicantsInterviewed" : 
					condition+=dtoSearch.getSortOn();
					break;	
				case "costadvirs" : 
					condition+=dtoSearch.getSortOn();
					break;	
				case "costrecri" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositoryRequisitions.predictiveRequisitionsSearchTotalCount("%"+searchWord+"%"));
				List<Requisitions> requisitionsList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						requisitionsList = this.repositoryRequisitions.predictiveRequisitionsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						requisitionsList = this.repositoryRequisitions.predictiveRequisitionsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						requisitionsList = this.repositoryRequisitions.predictiveRequisitionsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}
				if(requisitionsList != null && !requisitionsList.isEmpty()){
					List<DtoRequisitions> dtoRequisitionsList = new ArrayList<>();
					DtoRequisitions dtoRequisitions=null;
					for (Requisitions requisitions : requisitionsList) {
						dtoRequisitions = new DtoRequisitions(requisitions);
						dtoRequisitions.setId(requisitions.getId());
						dtoRequisitions.setStats(requisitions.getStats());
						dtoRequisitions.setInternalPostDate(requisitions.getInternalPostDate());
						dtoRequisitions.setInternalCloseDate(requisitions.getInternalCloseDate());
						dtoRequisitions.setRecruiterName(requisitions.getRecruiterName());
						dtoRequisitions.setOpeningDate(requisitions.getOpeningDate());
						if(requisitions.getCompanyId()!=null) {
							dtoRequisitions.setCompanyId(requisitions.getCompanyId().getCompanyId());
							dtoRequisitions.setCompanyName(requisitions.getCompanyId().getCompanyName());
						}
						
						dtoRequisitions.setManagerName(requisitions.getManagerName());
						dtoRequisitions.setJobPosting(requisitions.getJobPosting());
						dtoRequisitions.setAdvertisingField1(requisitions.getAdvertisingField1());
						dtoRequisitions.setAdvertisingField2(requisitions.getAdvertisingField2());
						dtoRequisitions.setAdvertisingField3(requisitions.getAdvertisingField3());
						dtoRequisitions.setAdvertisingField4(requisitions.getAdvertisingField4());
						dtoRequisitions.setPositionsAvailable(requisitions.getPositionsAvailable());
						dtoRequisitions.setPositionsFilled(requisitions.getPositionsFilled());
						dtoRequisitions.setApplicantsApplied(requisitions.getApplicantsApplied());
						dtoRequisitions.setApplicantsInterviewed(requisitions.getApplicantsInterviewed());
						dtoRequisitions.setCostadvirs(requisitions.getCostadvirs());
						dtoRequisitions.setCostrecri(requisitions.getCostrecri());
						dtoRequisitions.setDepartmentId(requisitions.getDepartment().getId());
						dtoRequisitions.setDivisionId(requisitions.getDivision().getId());
						dtoRequisitions.setSupervisorId(requisitions.getSupervisor().getId());
						dtoRequisitions.setPositionId(requisitions.getPosition().getId());
						dtoRequisitionsList.add(dtoRequisitions);
					}
					dtoSearch.setRecords(dtoRequisitionsList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		log.debug("Search Requistion Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public DtoRequisitions getRequistionById(int id) {
		log.info("getRequistionById Method");
		DtoRequisitions dtoRequisitions = new DtoRequisitions();
		try {
			if (id > 0) {
				Requisitions requisitions = repositoryRequisitions.findByIdAndIsDeleted(id, false);
				if (requisitions != null) {
					dtoRequisitions = new DtoRequisitions(requisitions);
				} else {
					dtoRequisitions.setMessageType("REQUISITION_NOT_GETTING");

				}
			} else {
				dtoRequisitions.setMessageType("INVALID_REQUISITION_ID");

			}
			log.debug("Requistion By Id is:"+dtoRequisitions.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoRequisitions;
	}



	public DtoRequisitions repeatById(Integer id) {
		log.info("repeatById Method");
		DtoRequisitions dtoRequisitions = new DtoRequisitions();
		try {
			List<Requisitions> requisitions=repositoryRequisitions.findById(id);
			if(requisitions!=null && !requisitions.isEmpty()) {
				dtoRequisitions.setIsRepeat(true);
			}else {
				dtoRequisitions.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoRequisitions;
	}

	

}
