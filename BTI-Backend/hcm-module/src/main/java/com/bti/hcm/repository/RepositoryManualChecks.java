package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecks;

@Repository("/repositoryManualChecks")
public interface RepositoryManualChecks extends JpaRepository<ManualChecks, Integer>{
	

	@Query("select b from ManualChecks b where b.employeeMaster.employeeIndexId =:id and b.isDeleted=false")
	List<ManualChecks> findByEmployeeId(@Param("id") Integer employeeIndexId);

	ManualChecks findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from ManualChecks m")
	public Integer getCountOfTotaLocation();

}
