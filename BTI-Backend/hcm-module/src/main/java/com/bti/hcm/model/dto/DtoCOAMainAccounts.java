package com.bti.hcm.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoCOAMainAccounts extends DtoBase{
	
	private Integer id;
	private String mainAccNumber;
	private String mainAccDescription;
	private String mainAccArabicDescription;
	private Integer accountType;
	private Integer accountCategoryIndex;
	private Boolean active;
	private Short transactionAction;
	private String aliasAccount;
	private String aliasAccountArabic;
	private String userDefine1;
	private String userDefine2;
	private String userDefine3;
	private String userDefine4;
	private String userDefine5;
	private Boolean allowAccountTxEntry;
	private Boolean unitAccount;
	private Boolean fixedAllocationAccount;
	private int valueId;
	private String valueDescription;

	private int valId;
	
	
	private List<DtoCOAMainAccounts> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMainAccNumber() {
		return mainAccNumber;
	}
	public void setMainAccNumber(String mainAccNumber) {
		this.mainAccNumber = mainAccNumber;
	}
	public String getMainAccDescription() {
		return mainAccDescription;
	}
	public void setMainAccDescription(String mainAccDescription) {
		this.mainAccDescription = mainAccDescription;
	}
	public String getMainAccArabicDescription() {
		return mainAccArabicDescription;
	}
	public void setMainAccArabicDescription(String mainAccArabicDescription) {
		this.mainAccArabicDescription = mainAccArabicDescription;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public Integer getAccountCategoryIndex() {
		return accountCategoryIndex;
	}
	public void setAccountCategoryIndex(Integer accountCategoryIndex) {
		this.accountCategoryIndex = accountCategoryIndex;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Short getTransactionAction() {
		return transactionAction;
	}
	public void setTransactionAction(Short transactionAction) {
		this.transactionAction = transactionAction;
	}
	public String getAliasAccount() {
		return aliasAccount;
	}
	public void setAliasAccount(String aliasAccount) {
		this.aliasAccount = aliasAccount;
	}
	public String getAliasAccountArabic() {
		return aliasAccountArabic;
	}
	public void setAliasAccountArabic(String aliasAccountArabic) {
		this.aliasAccountArabic = aliasAccountArabic;
	}
	public String getUserDefine1() {
		return userDefine1;
	}
	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}
	public String getUserDefine2() {
		return userDefine2;
	}
	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}
	public String getUserDefine3() {
		return userDefine3;
	}
	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}
	public String getUserDefine4() {
		return userDefine4;
	}
	public void setUserDefine4(String userDefine4) {
		this.userDefine4 = userDefine4;
	}
	public String getUserDefine5() {
		return userDefine5;
	}
	public void setUserDefine5(String userDefine5) {
		this.userDefine5 = userDefine5;
	}
	public Boolean getAllowAccountTxEntry() {
		return allowAccountTxEntry;
	}
	public void setAllowAccountTxEntry(Boolean allowAccountTxEntry) {
		this.allowAccountTxEntry = allowAccountTxEntry;
	}
	public Boolean getUnitAccount() {
		return unitAccount;
	}
	public void setUnitAccount(Boolean unitAccount) {
		this.unitAccount = unitAccount;
	}
	public Boolean getFixedAllocationAccount() {
		return fixedAllocationAccount;
	}
	public void setFixedAllocationAccount(Boolean fixedAllocationAccount) {
		this.fixedAllocationAccount = fixedAllocationAccount;
	}
	public List<DtoCOAMainAccounts> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoCOAMainAccounts> delete) {
		this.delete = delete;
	}
	public int getValueId() {
		return valueId;
	}
	public void setValueId(int valueId) {
		this.valueId = valueId;
	}
	public String getValueDescription() {
		return valueDescription;
	}
	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}
	public int getValId() {
		return valId;
	}
	public void setValId(int valId) {
		this.valId = valId;
	}


}
