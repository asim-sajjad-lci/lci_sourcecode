package com.bti.hcm.model.dto;

public class DtoEmployeeLoanInfo extends DtoEmployeeInfo{

	private double netMonthlySalary;
	private double totalLoan;
	private double previousLoan;
	private double totalDeducted;
	private double monthlyInstallment;
	private double remaining;
	private double loanThreshold;
	
	
	public double getNetMonthlySalary() {
		return netMonthlySalary;
	}
	public void setNetMonthlySalary(double netMonthlySalary) {
		this.netMonthlySalary = netMonthlySalary;
	}
	public double getTotalLoan() {
		return totalLoan;
	}
	public void setTotalLoan(double totalLoan) {
		this.totalLoan = totalLoan;
	}
	public double getPreviousLoan() {
		return previousLoan;
	}
	public void setPreviousLoan(double previousLoan) {
		this.previousLoan = previousLoan;
	}
	public double getTotalDeducted() {
		return totalDeducted;
	}
	public void setTotalDeducted(double totalDeducted) {
		this.totalDeducted = totalDeducted;
	}
	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}
	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}
	public double getRemaining() {
		return remaining;
	}
	public void setRemaining(double remaining) {
		this.remaining = remaining;
	}
	public double getLoanThreshold() {
		return loanThreshold;
	}
	public void setLoanThreshold(double loanThreshold) {
		this.loanThreshold = loanThreshold;
	}
	
	
}
