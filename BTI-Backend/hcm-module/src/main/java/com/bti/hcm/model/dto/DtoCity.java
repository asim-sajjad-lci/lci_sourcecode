/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import com.bti.hcm.model.HrCity;
import com.bti.hcm.util.UtilRandomKey;
//import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * Description: DTO City class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoCity {
	
	private Integer cityId;
	private String cityName;
	private Integer stateId;
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public DtoCity() {

	}
	public DtoCity(HrCity hrCity) {
		this.cityId = hrCity.getCityId();

		if (UtilRandomKey.isNotBlank(hrCity.getCityName())) {
			this.cityName = hrCity.getCityName();
		} else {
			this.cityName = "";
		}

		

		
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	

}
