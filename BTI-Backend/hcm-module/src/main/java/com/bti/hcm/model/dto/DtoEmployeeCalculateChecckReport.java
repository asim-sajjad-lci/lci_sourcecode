package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

public class DtoEmployeeCalculateChecckReport {

	private String name;
	
	private String employeeCode;
	
	private String location;
	
	private String startSate;
	
	private List<BigDecimal> payCodeList;
	
	private List<BigDecimal> BenefitList;
	
	private List<BigDecimal> deductionList;
	
	private BigDecimal totalIncome;
	
	private BigDecimal totalDeducion;
	
	private BigDecimal totalSummry;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStartSate() {
		return startSate;
	}

	public void setStartSate(String startSate) {
		this.startSate = startSate;
	}

	public List<BigDecimal> getPayCodeList() {
		return payCodeList;
	}

	public void setPayCodeList(List<BigDecimal> payCodeList) {
		this.payCodeList = payCodeList;
	}

	public List<BigDecimal> getBenefitList() {
		return BenefitList;
	}

	public void setBenefitList(List<BigDecimal> benefitList) {
		BenefitList = benefitList;
	}

	public List<BigDecimal> getDeductionList() {
		return deductionList;
	}

	public void setDeductionList(List<BigDecimal> deductionList) {
		this.deductionList = deductionList;
	}

	public BigDecimal getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(BigDecimal totalIncome) {
		this.totalIncome = totalIncome;
	}

	public BigDecimal getTotalDeducion() {
		return totalDeducion;
	}

	public void setTotalDeducion(BigDecimal totalDeducion) {
		this.totalDeducion = totalDeducion;
	}

	public BigDecimal getTotalSummry() {
		return totalSummry;
	}

	public void setTotalSummry(BigDecimal totalSummry) {
		this.totalSummry = totalSummry;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	
	
	
}
