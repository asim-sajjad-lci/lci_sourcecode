package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSupervisor;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceSupervisor;

@RestController
@RequestMapping("/supervisor")
public class ControllerSupervisor extends BaseController{

	private static final Logger log = Logger.getLogger(ControllerSupervisor.class);
	
	@Autowired
	ServiceSupervisor serviceSupervisor;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
		
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createSupervisor(HttpServletRequest request, @RequestBody DtoSupervisor dtoSupervisor) throws Exception {
		log.info("Create Supervisor Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSupervisor  = serviceSupervisor.saveOrUpdateSupervisor(dtoSupervisor);
			if (dtoSupervisor != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_CREATED", false), dtoSupervisor);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVSIOR_NOT_CREATED", false), dtoSupervisor);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Create Supervisor Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "pageNumber":"0",
 		"pageSize":"10"
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"pageNumber":0,
	 * "pageSize":10,
	 * "totalCount":1,
	 * "records":[
	 * {"locationId":1,
	 * "description":"Test Location",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}
	 * ]
	 * },
	 * 
	 * 		"btiMessage":{"message":"All locations fetched successfully",
	 * 		"messageShort":"LOCATION_GET_ALL"}
	 * }
	 * @throws Exception
	 */
	
	
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAllSupervisor(HttpServletRequest request, @RequestBody DtoSupervisor dtoSupervisor) throws Exception {
		log.info("Get All Supervisor Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			com.bti.hcm.model.dto.DtoSearch dtoSearch = serviceSupervisor.getAllSupervisor(dtoSupervisor);
			if (dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SUPERVISOR_GET_ALL, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SUPERVISOR_LIST_NOT_GETTING, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Get All Supervisor Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * 	"id" : 2,
		"locationId": 2,
		"description":"Testing Location",
		"arabicDescription":"موقع الاختبار",	
		"contactName":"Gaurav",
		"locationAddress":"Surat",
		"city":"Surat",
		"country":"India",
   		"phone":"(123) 123 1010",
		"fax":"(044) 420 4400"
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"id":2,
	 * "locationId":2,
	 * "description":"Testing Location",
	 * "arabicDescription":"موقع الاختبار",
	 * "contactName":"Gaurav",
	 * "locationAddress":"Surat",
	 * "city":"Surat",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * "btiMessage":{
	 * 					"message":"Location updated successfully",
	 * 					"messageShort":"LOCATION_UPDATE_SUCCESS"}
	 * }
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateSupervisor(HttpServletRequest request, @RequestBody DtoSupervisor dtoSupervisor) throws Exception {
	log.info("Update Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSupervisor = serviceSupervisor.saveOrUpdateSupervisor(dtoSupervisor);
			if (dtoSupervisor != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_UPDATE_SUCCESS", false), dtoSupervisor);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SUPERVISOR_LIST_NOT_GETTING, false), dtoSupervisor);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Update Location Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "ids" : [2]
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":0,
	 * "deleteMessage":"Division deleted successfully",
	 * "associateMessage":"N/A",
	 * "deleteLocation":[
	 * {"id":2,
	 * "locationId":2,
	 * "description":"Testing Location",
	 * "contactName":"Gaurav",
	 * "locationAddress":"Surat",
	 * "city":"Surat",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}]},
	 * "btiMessage":{
	 * 					"message":"Location deleted successfully",
	 * 					"messageShort":"LOCATION_DELETED"}
	 * }
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteSupervisor(HttpServletRequest request, @RequestBody DtoSupervisor dtoSupervisor) throws Exception {
		log.info("Delete Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSupervisor.getIds() != null && !dtoSupervisor.getIds().isEmpty()) {
				DtoSupervisor dtoSupervisor2 =serviceSupervisor.deleteSupervisor(dtoSupervisor.getIds());
				if (dtoSupervisor2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_DELETED", false), dtoSupervisor2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_NOT_DELETE_ID_MESSAGE", false), dtoSupervisor2);
				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete Location Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * 		"id" : 1,
			"pageNumber" : 0,
			"pageSize" : 10

	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":1,
	 * "description":"Test Location",
	 * "arabicDescription":"",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * "btiMessage":{
	 * 							"message":"Location detail fetched successfully",
	 * 							"messageShort":"LOCATION_GET_DETAIL"}
	 * }
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/getSuperVisorbyCode", method = RequestMethod.POST)
	public ResponseMessage getSuperVisorbyCode(HttpServletRequest request, @RequestBody DtoSupervisor dtoSupervisor) throws Exception {
		log.info("Get Location ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSupervisor dtoSuperVisorObj = serviceSupervisor.getSuperVisorByCode(dtoSupervisor.getSuperVisionCode());
			if (dtoSuperVisorObj != null) {
				if (dtoSuperVisorObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_GET_DETAIL", false), dtoSuperVisorObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoSuperVisorObj.getMessageType(), false),
							dtoSuperVisorObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_NOT_GETTING", false), dtoSuperVisorObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Get Location ById Method:"+dtoSupervisor.getSuperVisionCode());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @request{
	 * 	"searchKeyword" : "AHD",
		"pageNumber":"0",
		"pageSize":"10"
	 * }
	 * @response{
	 * "code":200,
	 * "status":"OK",
	 * "result":{
	 * "searchKeyword":"AHD",
	 * "pageNumber":0,
	 * "pageSize":10,
	 * "totalCount":1,
	 * "records":[
	 * {"locationId":1,
	 * "description":"Test Location",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}
	 * ]
	 * },
	 * "btiMessage":{
	 * 					"message":"All locations fetched successfully",
	 * 					"messageShort":"LOCATION_GET_ALL"}
	 * }
	 * @throws Exception
	 */
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchSuperVisor(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search SuperVisor Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSupervisor.searchSupervisor(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SUPERVISOR_GET_ALL, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_LIST_NOT_GETTING", false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		if(dtoSearch!=null) {
			log.debug("Search SuperVisors Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "locationId" : 1
	 * }
	 * @response{
	 * "code":302,
	 * "status":"FOUND",
	 * "result":{
	 * "locationId":1,
	 * "isRepeat":true},
	 * "btiMessage":
	 * {
	 *				 "message":"Location detail fetched successfully",
	 *				 "messageShort":"LOCATION_RESULT"}
	 * }
	 * @throws Exception
	 *//*
	**/
	@RequestMapping(value = "/supervisorCodeCheck", method = RequestMethod.POST)
	public ResponseMessage supervisorCodeCheck(HttpServletRequest request, @RequestBody DtoSupervisor dtoSupervisor) throws Exception {
		log.info("supervisorCodeCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSupervisor dtoSupervisorObj = serviceSupervisor.repeatBySuperVisorCode(dtoSupervisor.getSuperVisionCode());
			if (dtoSupervisorObj != null) {
				if (dtoSupervisorObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_EXIST", false), dtoSupervisorObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoSupervisorObj.getMessageType(), false),
							dtoSupervisorObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_REPEAT_SUPERVISORCODE_NOT_FOUND", false), dtoSupervisorObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}

	
	@RequestMapping(value = "/searchUserbyId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeById(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search Location Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSupervisor.searchEmployeeById(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_FOUND", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_NOT_FOUND", false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		if(dtoSearch!=null) {
			log.debug("Search SuperVisors Methods:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchSupervisorId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchSupervisorId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("searchSupervisorId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSupervisor.searchSupervisorId(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_LIST_NOT_GETTING", false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		if(dtoSearch!=null) {
			log.debug("Search SuperVisor Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllSupervisorDropDownList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllSupervisorDropDownList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoSupervisor> supervisorList = serviceSupervisor.getAllSupervisorDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_GET_ALL", false), supervisorList);
			}else {
				responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return responseMessage;
	}
	
	
	
	
	
	@RequestMapping(value = "/getAllSuperVisorId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllSuperVisorId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info(" getAllSuperVisorId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSupervisor.getAllSuperVisorId(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted("SUERVISOR_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("SUPERVICSOR_LIST_NOT_GETTING", false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		if(dtoSearch!=null) {
			log.debug("Search SuperVisor Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	 
		@RequestMapping(value = "/getById", method = RequestMethod.POST)
		public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoSupervisor dtoSupervisor) throws Exception {
			log.info("Get SuperVisor ById Method");
			ResponseMessage responseMessage = null;
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				DtoSupervisor dtoSupervisorObj = serviceSupervisor.getById(dtoSupervisor);
				responseMessage=displayMessage(dtoSupervisorObj, "SUERVISOR_GET_ALL", "SUPERVICSOR_LIST_NOT_GETTING", serviceResponse);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			log.debug("Get SuperVisor ById Method:"+dtoSupervisor.getId());
			return responseMessage;
		}
	
}
