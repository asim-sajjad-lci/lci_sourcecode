package com.bti.hcm.controller;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoPositionCourseToLink;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePositionCourseToLink;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/positionCourseToLink")
public class ControllerPositionCourseToLink extends BaseController{

	@Autowired
	ServicePositionCourseToLink servicePositionCourseToLink;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerPositionCourseToLink.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @param request{
  		"sequence" : 1,
  		"positionId":1,
	  	"traningCourseId": 1,
	  	"pageNumber": "0",
	  	"pageSize": "10"
	}{
				"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"sequence": 1,
		"positionId": 1,
		"traningCourseId": 1,
		"traningCourse": null,
		"pageNumber": 0,
		"pageSize": 10,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		"btiMessage": {
	}
}
	 * @param dtoPositionCourseToLink
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoPositionCourseToLink dtoPositionCourseToLink) throws Exception{
		LOGGER.info("Create Postion Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionCourseToLink = servicePositionCourseToLink.saveOrUpdate(dtoPositionCourseToLink);
			responseMessage = displayMessage(dtoPositionCourseToLink,"POSITION_COURSE_LINK_CREATED","POSITION_COURSE_LINK_NOT_CREATED",response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create Postion Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	/**
	 * @param request{
  	
	  	"pageNumber": "0",
	  	"pageSize": "10"
	}@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"pageNumber": 0,
		"pageSize": 10,
		"totalCount": 2,
		"records": [
		  {
		"id": 2,
		"sequence": null,
		"positionId": null,
		"traningCourseId": null,
		"traningCourse": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		  {
		"id": 1,
		"sequence": null,
		"positionId": null,
		"traningCourseId": null,
		"traningCourse": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		}
		],
		},
		}
	 * @param dtoPositionCourseToLink
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody DtoPositionCourseToLink dtoPositionCourseToLink) throws Exception {
		LOGGER.info("Get All PositionCourseDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePositionCourseToLink.getAll(dtoPositionCourseToLink);
			
			responseMessage = displayMessage(dtoSearch,"POSITION_COURSE_LINK_GET_ALL","POSITION_COURSE_LINK_LIST_NOT_GETTING",response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get All PositionCourseDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  "id" : 1,
	  	"sequence" : 3,
	  	"positionId":1,
		  	"traningCourseId": 1,
		  	"pageNumber": "0",
		  	"pageSize": "10"
	} @param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": 1,
		"sequence": 3,
		"positionId": 1,
		"traningCourseId": 1,
		"traningCourse": null,
		"pageNumber": 0,
		"pageSize": 10,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		}
	 * @param dtoPositionCourseToLink
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePosition(HttpServletRequest request, @RequestBody DtoPositionCourseToLink dtoPositionCourseToLink) throws Exception {
		LOGGER.info("Update Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionCourseToLink = servicePositionCourseToLink.saveOrUpdate(dtoPositionCourseToLink);
			responseMessage = displayMessage(dtoPositionCourseToLink,"POSITION_COURSE_LINK_UPDATED_SUCCESS","POSITION_COURSE_LINK_NOT_UPDATED",response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update Position Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
 	"ids":[1]
  	
	}
	 * @param dtoPositionCourseToLink
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoPositionCourseToLink dtoPositionCourseToLink) throws Exception {
		LOGGER.info("Delete Position Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPositionCourseToLink.getIds() != null && !dtoPositionCourseToLink.getIds().isEmpty()) {
				DtoPositionCourseToLink dtoPosition1 = servicePositionCourseToLink.delete(dtoPositionCourseToLink.getIds());
				
				if (dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("POSITION_COURSE_LINK_DELETED", false), dtoPosition1);
				} else {
					
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoPosition1.getMessageType());
					btiMessageHcm.setMessageShort("POSITION_COURSE_TO_LINK_NOT_DELETE_ID_MESSAGE");
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoPosition1);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Delete Position Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  		"id" : 1
  		
		}@param response{
	"code": 201,
	"status": "CREATED",
	"result": {
	"id": null,
	"sequence": null,
	"positionId": null,
	"traningCourseId": null,
	"traningCourse": null,
	"pageNumber": null,
	"pageSize": null,
	"ids": null,
	"isActive": null,
	"messageType": null,
	"message": null,
	"deleteMessage": null,
	"associateMessage": null,
	"delete": null,
	"isRepeat": null
	},
	"btiMessage": {
	"message": "Skill Steup details fetched successfully",
	"messageShort": "POSITION_COURSE_LINK_GET_DETAIL"
	}
	}
	 * @param dtoPositionCourseToLink
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPositionId", method = RequestMethod.POST)
	public ResponseMessage getPositionCourseDetailById(HttpServletRequest request, @RequestBody DtoPositionCourseToLink dtoPositionCourseToLink) throws Exception {
		LOGGER.info("Get Position ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPositionCourseToLink dtoPositionObj = servicePositionCourseToLink.getByPositionId(dtoPositionCourseToLink.getId());
			responseMessage=displayMessage(dtoPositionObj, "POSITION_COURSE_LINK_GET_DETAIL", "POSITION_COURSE_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get Position by Id Method:"+dtoPositionCourseToLink.getId());
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch{
 		"searchKeyword":"t",
		  "pageNumber" : 0,
		  "pageSize" :2
  	
				}
		 @param response	{
		"code": 200,
		"status": "OK",
		"result": {
		"searchKeyword": "t",
		"pageNumber": 0,
		"pageSize": 2,
		"totalCount": 2,
		"records": [
		  {
		"id": null,
		"sequence": null,
		"positionId": null,
		"traningCourseId": null,
		"traningCourse": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		  {
		"id": null,
		"sequence": null,
		"positionId": null,
		"traningCourseId": null,
		"traningCourse": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		}
		],
		},
		"btiMessage": {
		"message": "Position list fetched successfully",
		"messageShort": "POSITION_COURSE_GET_ALL"
		}
		}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPosition(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search searchPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionCourseToLink.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.POSITION_COURSE_GET_ALL, "POSITION_COURSE_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search searchPosition Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchPositionId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPositionId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Position Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionCourseToLink.searchTraningId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "POSITION_COURSE_GET_ALL", "POSITION_COURSE_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search searchPosition Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
}
