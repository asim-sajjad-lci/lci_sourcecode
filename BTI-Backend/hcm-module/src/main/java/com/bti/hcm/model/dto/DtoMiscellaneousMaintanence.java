package com.bti.hcm.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoMiscellaneousMaintanence extends DtoBase {

	private int id, masterId, miscId, employeeId,valueIndexId;
	private String codeId, codeName, codeArabicName,valueId;
	private boolean dimension;
	private DtoMiscellaneousMaintanence dtoMiscellaneous;
	private DtoValues dtoValues;
	private DtoEmployeeMaster dtoEmployeeMaster;
	private List<DtoValues> dtoValuesList;
	private Boolean isDeleted;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public DtoMiscellaneousMaintanence getDtoMiscellaneous() {
		return dtoMiscellaneous;
	}

	public void setDtoMiscellaneous(DtoMiscellaneousMaintanence dtoMiscellaneous) {
		this.dtoMiscellaneous = dtoMiscellaneous;
	}

	public DtoValues getDtoValues() {
		return dtoValues;
	}

	public void setDtoValues(DtoValues dtoValues) {
		this.dtoValues = dtoValues;
	}

	public DtoEmployeeMaster getDtoEmployeeMaster() {
		return dtoEmployeeMaster;
	}

	public void setDtoEmployeeMaster(DtoEmployeeMaster dtoEmployeeMaster) {
		this.dtoEmployeeMaster = dtoEmployeeMaster;
	}


	public int getMiscId() {
		return miscId;
	}

	public void setMiscId(int miscId) {
		this.miscId = miscId;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getValueIndexId() {
		return valueIndexId;
	}

	public void setValueIndexId(int valueIndexId) {
		this.valueIndexId = valueIndexId;
	}

	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeArabicName() {
		return codeArabicName;
	}

	public void setCodeArabicName(String codeArabicName) {
		this.codeArabicName = codeArabicName;
	}


	public boolean isDimension() {
		return dimension;
	}

	public void setDimension(boolean dimension) {
		this.dimension = dimension;
	}

	public List<DtoValues> getDtoValuesList() {
		return dtoValuesList;
	}

	public void setDtoValuesList(List<DtoValues> dtoValuesList) {
		this.dtoValuesList = dtoValuesList;
	}

	public String getValueId() {
		return valueId;
	}

	public void setValueId(String valueId) {
		this.valueId = valueId;
	}

	public int getMasterId() {
		return masterId;
	}

	public void setMasterId(int masterId) {
		this.masterId = masterId;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	
}
