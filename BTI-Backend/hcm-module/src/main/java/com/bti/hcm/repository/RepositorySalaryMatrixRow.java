package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bti.hcm.model.SalaryMatrixRow;

public interface RepositorySalaryMatrixRow extends JpaRepository<SalaryMatrixRow, Integer>{

	SalaryMatrixRow findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from SalaryMatrixRow d where d.isDeleted=false")
	Integer getCountOfTotalSalaryMatrixRow();

	List<SalaryMatrixRow> findByIsDeleted(boolean b, Pageable pageable);

	List<SalaryMatrixRow> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select count(*) from SalaryMatrixRow d where (d.desc like :searchKeyWord  or d.arbic like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveSalaryMatrixSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from SalaryMatrixRow d where (d.desc like :searchKeyWord  or d.arbic like :searchKeyWord) and d.isDeleted=false")
	List<SalaryMatrixRow> predictiveSalaryMatrixSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update SalaryMatrixRow d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleSalaryMatrixSetupRow(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
	@Query("select s from SalaryMatrixRow s where (s.salaryMatrixSetup.id =:id)")
	public List<SalaryMatrixRow> findBySalaryMatrixId(@Param("id")Integer id);

	@Query("select count(*) from SalaryMatrixRow r ")
	public Integer getCountOfTotaSalaryMatrixRow();
	
	

}
