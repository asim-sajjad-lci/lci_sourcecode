package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.TotalPackageESB;
import com.bti.hcm.model.TypeFieldForCodes;
import com.bti.hcm.model.dto.DtoMultiselect;
import com.bti.hcm.model.dto.DtoTotalPackageESB;
import com.bti.hcm.repository.RepositoryTotalPackageESB;
import com.bti.hcm.repository.RepositoryTypeFieldForCodes;

/**
 * 
 * @author HAMID
 *
 */
@Service("/serviceTotalPackageESB")
public class ServiceTotalPackageESB {

	static Logger log = Logger.getLogger(ServiceTotalPackageESB.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryTotalPackageESB repositoryTotalPackageESB;

	@Autowired
	RepositoryTypeFieldForCodes repositoryTypefield;

	/**
	 * @param dtoAccrual
	 * @return
	 * @throws ParseException
	 */
	public DtoTotalPackageESB saveOrUpdate(DtoTotalPackageESB dtoTotalPackageESB) {
		try {
			log.info("saveOrUpdate TotalPackageESB Method");
			// int loggedInUserId =
			// Integer.parseInt(httpServletRequest.getHeader("userid"));
			Integer TYPEID = dtoTotalPackageESB.getTypeId();
			List<DtoMultiselect> dtoMultiselectList = dtoTotalPackageESB.getDtoMultiSelectList();
			List<DtoMultiselect> dtoMultiselectListDisplay = new ArrayList<DtoMultiselect>();

			List<TotalPackageESB> pkgESBDelList = repositoryTotalPackageESB.findByTypeIdAndIsDeleted(TYPEID);
			
			// Delete Existing (if any)
			//for (DtoMultiselect dtoItem : dtoMultiselectList) {
			for (TotalPackageESB pkgESBDel : pkgESBDelList) {
				//TotalPackageESB pkgESBDel = null;
				//List<TotalPackageESB> pkgESBDel = repositoryTotalPackageESB.findByTypeIdAndIsDeleted(TYPEID);	//findByTypeIdAndTypeFieldForCodesIndxidAndIsDeleted
				if (pkgESBDel != null && !pkgESBDel.equals(null)) {
//					log.info("deleting pkgESBDel:id" + pkgESBDel.getIdx() + " typeId:" + pkgESBDel.getTypeId()
//							+ " TypeIndxId:" + pkgESBDel.getTypeFieldForCodes().getIndxid());
					repositoryTotalPackageESB.delete(pkgESBDel); // One or More Whatever!
					// repositoryTotalPackageESB.saveAndFlush(pkgESBDel);
				} else
					log.info("this rec not deleted:" + pkgESBDel);
			}

			// Create
			for (DtoMultiselect dtoMSItem : dtoMultiselectList) {

				TotalPackageESB PkgESB = new TotalPackageESB();
				// no id setting as all of nature save
				PkgESB.setTypeId(TYPEID);
				TypeFieldForCodes typeFieldForCodes = repositoryTypefield.findByIndxidAndIsDeleted(dtoMSItem.getId(),
						false);
				PkgESB.setTypeFieldForCodes(typeFieldForCodes); // set typeIndexId by FK Way!
				PkgESB.setIsDeleted(false);
				repositoryTotalPackageESB.saveAndFlush(PkgESB);
				// display part
				DtoMultiselect dtoMSDisplay = new DtoMultiselect();
				dtoMSDisplay.setId(dtoMSItem.getId());
				dtoMSDisplay.setItemName(dtoMSItem.getItemName());
				dtoMultiselectListDisplay.add(dtoMSDisplay);
			}

			// for Display Part
			DtoTotalPackageESB dtoDisplay = new DtoTotalPackageESB();
			dtoDisplay.setTypeId(TYPEID);
			dtoDisplay.setDtoMultiSelectList(dtoMultiselectListDisplay);

			log.debug("TotalPackage/ESB id:" + dtoTotalPackageESB.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoTotalPackageESB;

	}

	public DtoTotalPackageESB getByTypeId(DtoTotalPackageESB dtoTotalPackageESB) { // typeId only!

		if (dtoTotalPackageESB.getTypeId() == null
				|| (dtoTotalPackageESB.getTypeId() != 1 && dtoTotalPackageESB.getTypeId() != 3))
			return null;
		// Get Fields of Dto
		Integer TYPEID = dtoTotalPackageESB.getTypeId();
		// List<DtoMultiselect> dtoMultiselectList; //
		// dtoTotalPackageESB.getDtoMultiSelectList();

		List<TotalPackageESB> totalPkgOrESBList = repositoryTotalPackageESB.findByTypeIdAndIsDeleted(TYPEID);

		List<DtoMultiselect> dtoMultiselectListDisplay = new ArrayList<DtoMultiselect>();
		// For Display
		DtoTotalPackageESB dtoDisplay = new DtoTotalPackageESB();
		int totalCount = 0;
		for (TotalPackageESB pkgESB : totalPkgOrESBList) {

			DtoMultiselect dtoMSDisplay = new DtoMultiselect();
			dtoMSDisplay.setId(pkgESB.getTypeFieldForCodes().getIndxid());
			dtoMSDisplay.setItemName(pkgESB.getTypeFieldForCodes().getDesc());
			dtoMultiselectListDisplay.add(dtoMSDisplay);
			totalCount++;
		}
		
		dtoDisplay.setTypeId(TYPEID);
		dtoDisplay.setDtoMultiSelectList(dtoMultiselectListDisplay);
		
		
		// dtoTotalPackageESB.setRecords(dtoDisplay);

		// Integer totalCount =
		// repositoryTotalPackageESB.countByTypeIdAndIsDeleted(dtoTotalPackageESB.getTypeId(),
		// false);
		dtoTotalPackageESB.setTotalCount(totalCount);
		dtoTotalPackageESB.setRecords(dtoDisplay);
		
		if(dtoMultiselectListDisplay == null || dtoMultiselectListDisplay.equals(null) || dtoMultiselectListDisplay.isEmpty())
			return null;
		
		return dtoTotalPackageESB;
	}

	

}
