/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
/**
 * Description: The persistent class for the LifeInsuranceSetup database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40331",indexes = {
        @Index(columnList = "LFINRINDX")
})	
@NamedQuery(name = "LifeInsuranceSetup.findAll", query = "SELECT l FROM LifeInsuranceSetup l")
public class LifeInsuranceSetup extends HcmBaseEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LFINRINDX")
	private Integer id;
	
	
	@Column(name = "LFINRID",columnDefinition="char(15)")
	private String lifeInsuranceSetupId;
	
	@Column(name = "LFINRDSCR",columnDefinition="char(31)")
	private String lifeInsuraceDescription;
	
	@Column(name = "LFINRDSCRA",columnDefinition="char(61)")
	private String lifeInsuraceDescriptionArabic;
	
	@Column(name = "HLINRGRNO",columnDefinition="char(31)")
	private String healthInsuranceGroupNumber;
	
	@Column(name = "LFINRFREQ")
	private Short lifeInsuranceFrequency;
	
	@Column(name = "LFINREMAMT",precision = 13,scale =3)
	private BigDecimal amount;
	
	@Column(name = "LFINRSPAMT",precision = 13,scale =3)
	private BigDecimal spouseAmount;
	
	@Column(name = "LFINRCHAMT",precision = 13,scale =3)
	private BigDecimal childAmount;
	
	@Column(name = "LFINRCOVAMT",precision = 13,scale =3)
	private BigDecimal coverageTotalAmount;

	@Column(name = "LFINRERAMT",precision = 13,scale =3)
	private BigDecimal employeePay;
	
	@Column(name = "LFINRPLSDT")
	private Date startDate;
	
	@Column(name = "LFINRPLEDT")
	private Date endDate;
	
	@Column(name = "LFINRTYP")
	private short insuranceType;
	
	@ManyToOne
	@JoinColumn(name="INSCOMINDX")
	private CompnayInsurance helthInsurance;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	

	public String getLifeInsuranceSetupId() {
		return lifeInsuranceSetupId;
	}

	public void setLifeInsuranceSetupId(String lifeInsuranceSetupId) {
		this.lifeInsuranceSetupId = lifeInsuranceSetupId;
	}

	public String getLifeInsuraceDescription() {
		return lifeInsuraceDescription;
	}

	public void setLifeInsuraceDescription(String lifeInsuraceDescription) {
		this.lifeInsuraceDescription = lifeInsuraceDescription;
	}

	public String getLifeInsuraceDescriptionArabic() {
		return lifeInsuraceDescriptionArabic;
	}

	public void setLifeInsuraceDescriptionArabic(String lifeInsuraceDescriptionArabic) {
		this.lifeInsuraceDescriptionArabic = lifeInsuraceDescriptionArabic;
	}

	public String getHealthInsuranceGroupNumber() {
		return healthInsuranceGroupNumber;
	}

	public void setHealthInsuranceGroupNumber(String healthInsuranceGroupNumber) {
		this.healthInsuranceGroupNumber = healthInsuranceGroupNumber;
	}

	public Short getLifeInsuranceFrequency() {
		return lifeInsuranceFrequency;
	}

	public void setLifeInsuranceFrequency(Short lifeInsuranceFrequency) {
		this.lifeInsuranceFrequency = lifeInsuranceFrequency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getSpouseAmount() {
		return spouseAmount;
	}

	public void setSpouseAmount(BigDecimal spouseAmount) {
		this.spouseAmount = spouseAmount;
	}

	public BigDecimal getChildAmount() {
		return childAmount;
	}

	public void setChildAmount(BigDecimal childAmount) {
		this.childAmount = childAmount;
	}

	public BigDecimal getCoverageTotalAmount() {
		return coverageTotalAmount;
	}

	public void setCoverageTotalAmount(BigDecimal coverageTotalAmount) {
		this.coverageTotalAmount = coverageTotalAmount;
	}

	public BigDecimal getEmployeePay() {
		return employeePay;
	}

	public void setEmployeePay(BigDecimal employeePay) {
		this.employeePay = employeePay;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public short getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(short insuranceType) {
		this.insuranceType = insuranceType;
	}

	public CompnayInsurance getHelthInsurance() {
		return helthInsurance;
	}

	public void setHelthInsurance(CompnayInsurance helthInsurance) {
		this.helthInsurance = helthInsurance;
	}
	
	
}
