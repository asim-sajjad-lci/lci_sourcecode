package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00117",indexes = {
        @Index(columnList = "EMPPSINDXD")
})
public class EmployeePositionReason extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPPSINDXD")
	private int id;
	
	@Column(name = "EMPRESON",columnDefinition="char(90)")
	private String positionReason;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "employeePositionReason")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeePositionHistory> listEmployeePositionHistory;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPositionReason() {
		return positionReason;
	}

	public void setPositionReason(String positionReason) {
		this.positionReason = positionReason;
	}

	public List<EmployeePositionHistory> getListEmployeePositionHistory() {
		return listEmployeePositionHistory;
	}

	public void setListEmployeePositionHistory(List<EmployeePositionHistory> listEmployeePositionHistory) {
		this.listEmployeePositionHistory = listEmployeePositionHistory;
	}
	
	
	
	
	
}
