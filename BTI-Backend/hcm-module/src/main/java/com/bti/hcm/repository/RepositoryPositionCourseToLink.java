package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PositionCourseToLink;

@Repository("repositoryPositionCourseToLink")
public interface RepositoryPositionCourseToLink extends JpaRepository<PositionCourseToLink	, Integer>{

	PositionCourseToLink findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from PositionCourseToLink p where p.isDeleted=false")
	Integer getCountOfTotalPosition();

	List<PositionCourseToLink> findByIdAndIsDeleted(boolean b, Pageable pageable);

	List<PositionCourseToLink> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PositionCourseToLink p set p.isDeleted =:deleted ,p.updatedBy =:updateById where p.id =:id ")
	void deleteSinglePosition(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	
	@Query("select count(*) from PositionCourseToLink p where (p.traningCourse.arbicDesc like :searchKeyWord or p.traningCourse.desc like :searchKeyWord or p.position.positionId like :searchKeyWord or p.position.arabicDescription like :searchKeyWord or p.position.description like :searchKeyWord) and p.position.id =:id and p.isDeleted=false")
	Integer predictivePositionSearchTotalCount(@Param("searchKeyWord") String searchKeyWord, @Param("id") Integer id);

	@Query("select p from PositionCourseToLink p where (p.traningCourse.arbicDesc like :searchKeyWord or p.traningCourse.desc like :searchKeyWord or p.position.positionId like :searchKeyWord or p.position.arabicDescription like :searchKeyWord or p.position.description like :searchKeyWord) and p.position.id =:id and p.isDeleted=false")
	List<PositionCourseToLink> predictivePositionSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,@Param("id") Integer id, Pageable pageable);

	List<PositionCourseToLink> findByIsDeleted(boolean b, Pageable pageable);

	@Query("select d.position.positionId from PositionCourseToLink d where (d.position.positionId like :searchKeyWord or d.position.description like :searchKeyWord or d.position.arabicDescription like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<String> predictiveTraningIdSearchWithPagination(@Param("searchKeyWord")String searchKeyWord);

	@Query("select count(*) from PositionCourseToLink p ")
	public Integer getCountOfTotaPositionCourseToLink();

}
