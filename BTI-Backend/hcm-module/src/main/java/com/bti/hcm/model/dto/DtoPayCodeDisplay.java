package com.bti.hcm.model.dto;

import java.util.List;

public class DtoPayCodeDisplay {
	private Integer payCodeIdPrimary;
	private String payCodeId;
	private String description;
	private Integer buildChecksId;
	private List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenances;
	public Integer getPayCodeIdPrimary() {
		return payCodeIdPrimary;
	}
	public void setPayCodeIdPrimary(Integer payCodeIdPrimary) {
		this.payCodeIdPrimary = payCodeIdPrimary;
	}
	public String getPayCodeId() {
		return payCodeId;
	}
	public void setPayCodeId(String payCodeId) {
		this.payCodeId = payCodeId;
	}
	public Integer getBuildChecksId() {
		return buildChecksId;
	}
	public void setBuildChecksId(Integer buildChecksId) {
		this.buildChecksId = buildChecksId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<DtoEmployeePayCodeMaintenance> getDtoEmployeePayCodeMaintenances() {
		return dtoEmployeePayCodeMaintenances;
	}
	public void setDtoEmployeePayCodeMaintenances(List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenances) {
		this.dtoEmployeePayCodeMaintenances = dtoEmployeePayCodeMaintenances;
	}

}
