package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollTransectionHistoryByEmployee;

@Repository("repositoryPayrollTransectionHistoryByEmployee")
public interface RepositoryPayrollTransectionHistoryByEmployee extends JpaRepository<PayrollTransectionHistoryByEmployee, Integer>{

	PayrollTransectionHistoryByEmployee findByIdAndIsDeleted(Integer id, boolean b);

}
