package com.bti.hcm.model.dto;

/**
 * 
 * @author HAMID
 *
 */
public class DtoTypeFieldForCodes extends DtoBase {

	private Integer id;
	private Integer typeId;
	private String desc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
