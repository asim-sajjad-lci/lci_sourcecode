package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "HR40110")
@NamedQuery(name = "Miscellaneous.findAll", query = "SELECT d FROM Miscellaneous d")
public class Miscellaneous extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 8174792801905707255L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MISCINDX")
	private int id;

	@Column(name = "CODEID", columnDefinition = "char(15)")
	private String codeId;

	@Column(name = "CODENAME", columnDefinition = "char(120)")
	private String codeName;

	@Column(name = "CODEARNAME", columnDefinition = "char(120)")
	private String codeArabicName;

	@Column(name = "DIMENSION")
	private boolean dimension;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	@JoinColumn(name="EMPLOYINDX")
	private List<EmployeeMaster> employeeMaster;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeArabicName() {
		return codeArabicName;
	}

	public void setCodeArabicName(String codeArabicName) {
		this.codeArabicName = codeArabicName;
	}

	public boolean isDimension() {
		return dimension;
	}

	public void setDimension(boolean dimension) {
		this.dimension = dimension;
	}

	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

}
