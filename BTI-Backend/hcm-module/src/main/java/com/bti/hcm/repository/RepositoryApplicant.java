package com.bti.hcm.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Applicant;

@Repository("repositoryApplicant")
public interface RepositoryApplicant extends JpaRepository<Applicant, Integer>{

	Applicant findByIdAndIsDeleted(Integer id, boolean b);

	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Applicant a set a.isDeleted =:deleted ,a.updatedBy =:updateById where a.id =:id ")
	public void deleteSingleApplicant(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from Applicant a ")
	public Integer getCountOfTotalApplicant();

}
