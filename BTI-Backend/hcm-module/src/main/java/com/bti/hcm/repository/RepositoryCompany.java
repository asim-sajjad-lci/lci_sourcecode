package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Company;

@Repository("repositoryCompany")
public interface RepositoryCompany extends JpaRepository<Company, Integer>{

	@Query("select p from Company p where p.companyName like :companyName")
	List<Company> predictiveCompanyIdSearchWithPagination(@Param("companyName")String companyName, Pageable pageRequest);

	
}
