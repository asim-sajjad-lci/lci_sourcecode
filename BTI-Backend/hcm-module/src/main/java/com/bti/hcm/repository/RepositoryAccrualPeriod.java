package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AccrualPeriod;

@Repository("repositoryAccrualPeriod")
public interface RepositoryAccrualPeriod extends JpaRepository<AccrualPeriod, Integer>{

	AccrualPeriod findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update AccrualPeriod d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleAccrualPeriod(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	List<AccrualPeriod> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select count(*) from AccrualPeriod a where a.isDeleted=false")
	Integer predictiveAccrualSearchTotalCount();

	@Query("select a from AccrualPeriod a where  a.isDeleted=false")
	List<AccrualPeriod> predictiveAccrualSearchWithPagination(Pageable pageRequest);

   @Query("select count(*) from AccrualPeriod a ")
   public Integer getCountOfTotalAccrualPeriod();

}
