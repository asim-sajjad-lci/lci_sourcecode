package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ShiftCode;

@Repository("repositoryShiftCode")
public interface RepositoryShiftCode extends JpaRepository<ShiftCode, Integer>{

	ShiftCode findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ShiftCode d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleShiftCode(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from ShiftCode d where ( d.shiftCodeId like :searchKeyWord or d.desc like :searchKeyWord or d.arabicDesc like :searchKeyWord  or d.amount like :searchKeyWord or d.percent like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveShiftCodeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from ShiftCode d where ( d.shiftCodeId like :searchKeyWord or d.desc like :searchKeyWord or d.arabicDesc like :searchKeyWord  or d.amount like :searchKeyWord or d.percent like :searchKeyWord) and d.isDeleted=false")
	List<ShiftCode> predictiveShiftCodeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select d from ShiftCode d where (d.shiftCodeId=:searchKeyWord) and d.isDeleted=false")
	List<ShiftCode> findByShiftCodeId(@Param("searchKeyWord") String id);

	@Query("select d from ShiftCode d where (d.shiftCodeId like :searchKeyWord or d.desc like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<ShiftCode> predictiveShiftCodeSearchWithPagination(@Param("searchKeyWord")String string);
	
	public List<ShiftCode> findByShiftCodeIdAndIsDeleted(String id, boolean b);
	
	public List<ShiftCode> findAll();
	
	public List<ShiftCode> findByIsDeletedAndInActive(Boolean deleted, Boolean inActive);

	@Query("select count(*) from ShiftCode s ")
	public Integer getCountOfTotalShiftCode();
	
}
