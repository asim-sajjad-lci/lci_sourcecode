package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTerminationSetupDetails;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTerminationSetupDetails;

@RestController
@RequestMapping("/terminationSetupDetails")
public class ControllerTerminationSetupDetails extends BaseController{
	
	private static final Logger LOGGER = Logger.getLogger(ControllerTerminationSetupDetails.class);

	@Autowired(required = true)
	ServiceTerminationSetupDetails serviceTerminationSetupDetails;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoTerminationSetupDetails dtoTerminationSetupDetails)
			throws Exception {
		LOGGER.info("Create TerminationSetupDetails Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTerminationSetupDetails = serviceTerminationSetupDetails.saveOrUpdateTerminationSetupDetails(dtoTerminationSetupDetails);
			responseMessage=displayMessage(dtoTerminationSetupDetails, "TERMINATIONSETUPDETAILS_CREATED", "TERMINATIONSETUPDETAILS_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create TerminationSetupDetails Method:" + responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoTerminationSetupDetails dtoTerminationSetupDetails)
			throws Exception {
		LOGGER.info("Update TerminationSetupDetails Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTerminationSetupDetails = serviceTerminationSetupDetails.saveOrUpdateTerminationSetupDetails(dtoTerminationSetupDetails);
			responseMessage=displayMessage(dtoTerminationSetupDetails, "TERMINATIONSETUPDETAILS_CREATED_UPDATED_SUCCESS", "TERMINATIONSETUPDETAILS_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update TerminationSetupDetails Method:" + responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoTerminationSetupDetails dtoTerminationSetupDetails)
			throws Exception {
		LOGGER.info("Delete TerminationSetupDetails Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTerminationSetupDetails.getIds() != null && !dtoTerminationSetupDetails.getIds().isEmpty()) {
				DtoTerminationSetupDetails dtoTerminationSetupDetails2 = serviceTerminationSetupDetails
						.deleteTerminationSetupDetails(dtoTerminationSetupDetails.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("TERMINATIONSETUPDETAILS_DELETED", false),
						dtoTerminationSetupDetails2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete TerminationSetupDetails Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search TerminationSetupDetails Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTerminationSetupDetails.searchTerminationSetupDetails(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "TERMINATIONSETUPDETAILS_GET_ALL", "TERMINATIONSETUPDETAILS_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TerminationSetupDetails Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getTerminationSetupDetailById", method = RequestMethod.POST)
	public ResponseMessage getTerminationSetupDetailById(HttpServletRequest request, @RequestBody DtoTerminationSetupDetails dtoTerminationSetupDetails) throws Exception {
		LOGGER.info("Get getTerminationSetupDetail ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTerminationSetupDetails dtoTerminationSetupDetailsObj = serviceTerminationSetupDetails.getRetirementPlanSetupDetailsById(dtoTerminationSetupDetails.getId());
			responseMessage=displayMessage(dtoTerminationSetupDetailsObj, "RETIREMENTPLANSETUPDETAILS_LIST_GET_DETAIL", "RETIREMENTPLANSETUPDETAIL_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get RetirementPlanSetup by Id Method:"+dtoTerminationSetupDetails.getId());
		return responseMessage;
	}
	
	
	

}
