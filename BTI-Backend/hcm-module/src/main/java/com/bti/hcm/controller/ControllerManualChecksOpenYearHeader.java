package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoManualChecksOpenYearHeader;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceManualChecksOpenYearHeader;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/manualChecksOpenYearHeader")
public class ControllerManualChecksOpenYearHeader extends BaseController{
	
	private static final Logger LOGGER = Logger.getLogger(ControllerMiscellaneousBenefirtEnrollment.class);
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	ServiceManualChecksOpenYearHeader serviceManualChecksOpenYearHeader;
	
	

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoManualChecksOpenYearHeader dtoManualChecksOpenYearHeader) throws Exception {
		LOGGER.info("Create ManualChecksOpenYearHeader Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoManualChecksOpenYearHeader  = serviceManualChecksOpenYearHeader.saveOrUpdate(dtoManualChecksOpenYearHeader);
			responseMessage=displayMessage(dtoManualChecksOpenYearHeader, "MANUAL_CHECK_OPEN_YEAR_HEADER_CREATED", "MANUAL_CHECK_OPEN_YEAR_HEADER_NOT_CREATED", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create ManualChecksOpenYearHeader Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	


}
