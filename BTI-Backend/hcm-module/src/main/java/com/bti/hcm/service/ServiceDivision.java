/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Division;
import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.model.dto.DtoDivision;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryDivision;
import com.bti.hcm.repository.RepositoryHrCity;
import com.bti.hcm.repository.RepositoryHrCountry;
import com.bti.hcm.repository.RepositoryHrState;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service Department
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("ServiceDivision")
public class ServiceDivision {

	
	/**
	 * @Description LOGGER use for put a logger in Divison Service
	 */
	static Logger log = Logger.getLogger(ServiceDivision.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in Divison service
	 */



	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in Divison service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in Divison service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryDivision Autowired here using annotation of spring for access of repositoryDivision method in Divison service
	 * 				In short Access Division Query from Database using repositoryDivision.
	 */
	@Autowired
	RepositoryDivision repositoryDivision;

	@Autowired
	RepositoryHrCountry repositoryHrCountry;
	
	@Autowired
	RepositoryHrState repositoryHrState;
	
	@Autowired
	RepositoryHrCity repositoryHrCity;
	
	
	/**
	 * @Description: save and update division data
	 * @param dtoDivision
	 * @return
	 */
	public DtoDivision saveOrUpdateDivision(DtoDivision dtoDivision) {

		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Division division = null;
			if (dtoDivision.getId() != null && dtoDivision.getId() > 0) {
				division = repositoryDivision.findByIdAndIsDeleted(dtoDivision.getId(), false);
				division.setUpdatedBy(loggedInUserId);
				division.setUpdatedDate(new Date());
			} else {
				division = new Division();
				division.setCreatedDate(new Date());
				Integer rowId = repositoryDivision.getCountOfTotalDivisions();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				division.setRowId(increment);
			}
			HrCountry hrCountry = repositoryHrCountry.findOne(dtoDivision.getCountryId());
			HrState hrState = repositoryHrState.findOne(dtoDivision.getStateId());
			HrCity hrCity = repositoryHrCity.findOne(dtoDivision.getCityId());
			
			division.setDivisionAddress(dtoDivision.getDivisionAddress());
			division.setEmail(dtoDivision.getEmail());
			division.setPhoneNumber(dtoDivision.getPhoneNumber());
			division.setFax(dtoDivision.getFax());
			division.setArabicDivisionDescription(dtoDivision.getArabicDivisionDescription());
			division.setCountry(hrCountry);
			division.setCity(hrCity);
			division.setState(hrState);
			division.setDivisionDescription(dtoDivision.getDivisionDescription());
			division.setDivisionId(dtoDivision.getDivisionId());
			division.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			division.setRowId(loggedInUserId);
			repositoryDivision.saveAndFlush(division);
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDivision;

	}

	/**
	 * @Description:get All division data
	 * @param dtoDivision
	 * @return
	 */
	public DtoSearch getAllDivison(DtoDivision dtoDivision) {
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoDivision.getPageNumber());
			dtoSearch.setPageSize(dtoDivision.getPageSize());
			dtoSearch.setTotalCount(repositoryDivision.getCountOfTotalDivision());
			List<Division> divisionList = null;
			if (dtoDivision.getPageNumber() != null && dtoDivision.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoDivision.getPageNumber(), dtoDivision.getPageSize(), Direction.DESC, "createdDate");
				divisionList = repositoryDivision.findByIsDeleted(false, pageable);
			} else {
				divisionList = repositoryDivision.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoDivision> dtoDivisionList=new ArrayList<>();
			if(divisionList!=null && !divisionList.isEmpty())
			{
				for (Division division : divisionList) 
				{
					dtoDivision=new DtoDivision(division);
					if(division.getCity()!=null){
						dtoDivision.setCityId(division.getCity().getCityId());
						dtoDivision.setCityName(division.getCity().getCityName());
						dtoDivision.setCity(division.getCity());
					}
					
					if(division.getCountry()!=null){
						dtoDivision.setCountryId(division.getCountry().getCountryId());
						dtoDivision.setCountryName(division.getCountry().getCountryName());
						dtoDivision.setCountry(division.getCountry());
					}
					
					if(division.getState()!=null){
						dtoDivision.setStateName(division.getState().getStateName());
						dtoDivision.setStateId(division.getState().getStateId());
						dtoDivision.setState(division.getState());
					}
					dtoDivision.setDivisionAddress(division.getDivisionAddress());
					dtoDivision.setArabicDivisionDescription(division.getArabicDivisionDescription());
					dtoDivision.setId(division.getId());
					dtoDivision.setDivisionId(division.getDivisionId());
					dtoDivisionList.add(dtoDivision);
				}
				dtoSearch.setRecords(dtoDivisionList);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description:search division data
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchDivision(DtoSearch dtoSearch) {

		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				
					String condition="";
				
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("divisionId") || dtoSearch.getSortOn().equals("divisionDescription") || dtoSearch.getSortOn().equals("arabicDivisionDescription") || dtoSearch.getSortOn().equals("divisionAddress")
							|| dtoSearch.getSortOn().equals("phoneNumber") || dtoSearch.getSortOn().equals("fax") || dtoSearch.getSortOn().equals("method") || dtoSearch.getSortOn().equals("email")|| dtoSearch.getSortOn().equals("city")) {
						
						     if(dtoSearch.getSortOn().equals("cityName")) {
						    	 dtoSearch.setSortOn("city");
						     }
						    	 
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	
				
				
				
				dtoSearch.setTotalCount(this.repositoryDivision.predictiveDivisionSearchTotalCount("%"+searchWord+"%"));
				List<Division> divisiontList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						divisiontList =this.repositoryDivision.predictiveDivisionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						divisiontList = this.repositoryDivision.predictiveDivisionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						divisiontList = this.repositoryDivision.predictiveDivisionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
				
				
				if(divisiontList != null && !divisiontList.isEmpty()){
					List<DtoDivision> dtoDivisionList = new ArrayList<>();
					for (Division division : divisiontList) {
						DtoDivision dtoDivision = new DtoDivision(division);
						dtoDivision.setArabicDivisionDescription(division.getArabicDivisionDescription());
						if(division.getCity()!=null){
							dtoDivision.setCityId(division.getCity().getCityId());
							dtoDivision.setCityName(division.getCity().getCityName());
							dtoDivision.setId(division.getId());
						}
						
						if(division.getCountry()!=null){
							dtoDivision.setCountryId(division.getCountry().getCountryId());
							dtoDivision.setCountryName(division.getCountry().getCountryName());
						}
						
						if(division.getState()!=null){
							dtoDivision.setStateName(division.getState().getStateName());
							dtoDivision.setStateId(division.getState().getStateId());
						}
						dtoDivision.setDivisionAddress(division.getDivisionAddress());
						dtoDivision.setDivisionId(division.getDivisionId());
						dtoDivision.setEmail(division.getEmail());
						dtoDivision.setFax(division.getFax());
						dtoDivisionList.add(dtoDivision);
					}
					dtoSearch.setRecords(dtoDivisionList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description:get  division data by id
	 * @param divisionId
	 * @return
	 */
	public DtoDivision getDivisionById(int id) {
		DtoDivision dtoDivision = new DtoDivision();

		try {
			if (id > 0) {
				Division division = repositoryDivision.findByIdAndIsDeleted(id, false);
				if (division != null) {
					dtoDivision = new DtoDivision(division);
					dtoDivision.setId(division.getId());
					dtoDivision.setDivisionAddress(division.getDivisionAddress());
					
					if(division.getCity()!=null){
						dtoDivision.setCityId(division.getCity().getCityId());
						dtoDivision.setCityName(division.getCity().getCityName());
						dtoDivision.setId(division.getId());
					}
					
					if(division.getCountry()!=null){
						dtoDivision.setCountryId(division.getCountry().getCountryId());
						dtoDivision.setCountryName(division.getCountry().getCountryName());
					}
					
					if(division.getState()!=null){
						dtoDivision.setStateName(division.getState().getStateName());
						dtoDivision.setStateId(division.getState().getStateId());
					}
				} else {
					dtoDivision.setMessageType("DIVISION_NOT_GETTING");

				}
			} else {
				dtoDivision.setMessageType("INVALID_DIVISION_ID");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDivision;
	}
	/**
	 * @Description:delete division 
	 * @param ids
	 * @return
	 */
	public DtoDivision deleteDivision(List<Integer> ids) {
		log.info("deleteDivision Method");
		DtoDivision dtoDivision = new DtoDivision();
		try {
			
			dtoDivision.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DIVISION_DELETED", false));
			dtoDivision.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DIVISION_ASSOCIATED", false));
			boolean inValidDelete = false;
			StringBuilder invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse
					.getMessageByShortAndIsDeleted("DIVISION_NOT_DELETE_ID_MESSAGE", false).getMessage());
			List<DtoDivision> deleteDivision = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			try {
				for (Integer divisionId : ids) {
					Division division = repositoryDivision.findOne(divisionId);
					if (division.getEmployeeMaster().isEmpty() && division.getListEmployeePositionHistory().isEmpty()&&division.getRequisitions().isEmpty()) {
						DtoDivision dtoDivision2 = new DtoDivision();
						dtoDivision2.setId(divisionId);
						dtoDivision2.setDivisionDescription(division.getDivisionDescription());
						dtoDivision2.setArabicDivisionDescription(division.getArabicDivisionDescription());
						dtoDivision2.setEmail(division.getEmail());
						dtoDivision2.setFax(division.getFax());
						dtoDivision2.setDivisionId(division.getDivisionId());
						repositoryDivision.deleteSingleDivision(true, loggedInUserId, divisionId);
						deleteDivision.add(dtoDivision2);
					} else {
						inValidDelete = true;
					}
				}
				if (inValidDelete) {
					dtoDivision.setMessageType(invlidDeleteMessage.toString());
				}
				if (!inValidDelete) {
					dtoDivision.setMessageType("");
				}
				dtoDivision.setDeleteDivision(deleteDivision);
			} catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete Divison :"+dtoDivision.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDivision;
	}
	
	
	
	/**
	 * @Description: check Repeat divisionId
	 * @param divisionId
	 * @return
	 */
	public DtoDivision repeatByDivisiontId(String divisionId) {
		log.info("repeatByDivisiontId Method");
		DtoDivision dtoDivision = new DtoDivision();
		try {
			List<Division> division=repositoryDivision.findByDivisionId(divisionId.trim());
			if(division!=null && !division.isEmpty()) {
				dtoDivision.setIsRepeat(true);
			}else {
				dtoDivision.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDivision;
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchDivisiontId(DtoSearch dtoSearch) {
		try {
			log.info("searchDivisiontId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				List<String> divisionIdList =new ArrayList<>();
				
					divisionIdList = this.repositoryDivision.predictiveDivisionIdSearchWithPagination("%"+searchWord+"%");
					if(!divisionIdList.isEmpty()) {
						dtoSearch.setTotalCount(divisionIdList.size());
						dtoSearch.setRecords(divisionIdList.size());
					}
				dtoSearch.setIds(divisionIdList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	public List<DtoDivision> getAllDivisionDropDownList() {
		log.info("getAllDivisionDropDownList  Method");
		List<DtoDivision> dtoDivisionList = new ArrayList<>();
		try {
			
			List<Division> list = repositoryDivision.findByIsDeleted(false);
			
			if (list != null && !list.isEmpty()) {
				for (Division division : list) {
					DtoDivision dtoDivision = new DtoDivision();
					dtoDivision.setId(division.getId());
					dtoDivision.setDivisionId(division.getDivisionId());
					dtoDivision.setDivisionDescription(division.getDivisionDescription());
					dtoDivisionList.add(dtoDivision);
				}
			}
			log.debug("Division is:"+dtoDivisionList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDivisionList;
	}
	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllDivisionId(DtoSearch dtoSearch) {

		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				
					String condition="";
				
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("divisionId") || dtoSearch.getSortOn().equals("divisionDescription") || dtoSearch.getSortOn().equals("arabicDivisionDescription") || dtoSearch.getSortOn().equals("divisionAddress")
							|| dtoSearch.getSortOn().equals("phoneNumber") || dtoSearch.getSortOn().equals("fax") || dtoSearch.getSortOn().equals("method") || dtoSearch.getSortOn().equals("email")|| dtoSearch.getSortOn().equals("city")) {
						
						     if(dtoSearch.getSortOn().equals("cityName")) {
						    	 dtoSearch.setSortOn("city");
						     }
						    	 
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	
				
				
				
				dtoSearch.setTotalCount(this.repositoryDivision.predictiveDivisionSearchTotalCount("%"+searchWord+"%"));
				List<Division> divisiontList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						divisiontList =this.repositoryDivision.predictiveDivisionAllIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						divisiontList = this.repositoryDivision.predictiveDivisionAllIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						divisiontList = this.repositoryDivision.predictiveDivisionAllIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
				
				
				if(divisiontList != null && !divisiontList.isEmpty()){
					List<DtoDivision> dtoDivisionList = new ArrayList<>();
					for (Division division : divisiontList) {
						DtoDivision dtoDivision = new DtoDivision(division);
						dtoDivision.setArabicDivisionDescription(division.getArabicDivisionDescription());
						dtoDivision.setDivisionAddress(division.getDivisionAddress());
						dtoDivision.setDivisionId(division.getDivisionId());
						dtoDivision.setId(division.getId());
						dtoDivisionList.add(dtoDivision);
					}
					dtoSearch.setRecords(dtoDivisionList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	
	
	
}
