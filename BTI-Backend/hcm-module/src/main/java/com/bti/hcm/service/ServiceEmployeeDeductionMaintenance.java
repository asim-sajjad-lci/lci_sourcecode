package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoDeductionCode;
import com.bti.hcm.model.dto.DtoEmployeeDeductionMaintenance;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceEmployeeDeductionMaintenance")
public class ServiceEmployeeDeductionMaintenance {

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletResponse method in
	 *              ServiceRetirementFundSetup service
	 */

	static Logger log = Logger.getLogger(ServiceEmployeeDeductionMaintenance.class.getName());

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in
	 *              ServiceRetirementFundSetup service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletResponse method in
	 *              ServiceRetirementFundSetup service
	 */

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;
	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;

	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;

	@Autowired(required = false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;

	public DtoEmployeeDeductionMaintenance saveOrUpdate(
			DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance) {

		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			EmployeeDeductionMaintenance employeeDeductionMaintenance = null;
			if (dtoEmployeeDeductionMaintenance.getId() != null && dtoEmployeeDeductionMaintenance.getId() > 0) {
				employeeDeductionMaintenance = repositoryEmployeeDeductionMaintenance
						.findByIdAndIsDeleted(dtoEmployeeDeductionMaintenance.getId(), false);
				employeeDeductionMaintenance.setUpdatedBy(loggedInUserId);
				employeeDeductionMaintenance.setUpdatedDate(new Date());
			} else {
				employeeDeductionMaintenance = new EmployeeDeductionMaintenance();
				employeeDeductionMaintenance.setCreatedDate(new Date());
				Integer rowId = repositoryEmployeeDeductionMaintenance.getCountOfTotalEmployeeDeductionMaintenances();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				employeeDeductionMaintenance.setRowId(increment);

			}

			EmployeeMaster employeeMaster = null;
			if (dtoEmployeeDeductionMaintenance.getEmployeeMaster() != null
					&& dtoEmployeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId() > 0) {
				employeeMaster = repositoryEmployeeMaster
						.findOne(dtoEmployeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId());
			}
			DeductionCode deductionCode = new DeductionCode();
			if (dtoEmployeeDeductionMaintenance.getDeductionCode() != null
					&& dtoEmployeeDeductionMaintenance.getDeductionCode().getId() > 0) {
				deductionCode = repositoryDeductionCode
						.findOne(dtoEmployeeDeductionMaintenance.getDeductionCode().getId());

			}
			employeeDeductionMaintenance.setDeductionCode(deductionCode);
			employeeDeductionMaintenance.setEmployeeMaster(employeeMaster);
			employeeDeductionMaintenance.setStartDate(dtoEmployeeDeductionMaintenance.getStartDate());
			if (deductionCode.getNoOfDays() != null && deductionCode.getEndDateDays() != null
					&& employeeDeductionMaintenance != null && employeeMaster != null) {
				employeeDeductionMaintenance.setStartDate(
						UtilDateAndTimeHr.startDate(deductionCode.getNoOfDays(), employeeMaster.getEmployeeHireDate()));
				employeeDeductionMaintenance.setEndDate(UtilDateAndTimeHr.endDate(deductionCode.getNoOfDays(),
						deductionCode.getEndDateDays(), employeeMaster.getEmployeeHireDate()));
			} else {
				employeeDeductionMaintenance.setStartDate(dtoEmployeeDeductionMaintenance.getStartDate());
				employeeDeductionMaintenance.setEndDate(dtoEmployeeDeductionMaintenance.getEndDate());
			}
			employeeDeductionMaintenance
					.setTransactionRequired(dtoEmployeeDeductionMaintenance.getTransactionRequired());
			employeeDeductionMaintenance.setBenefitMethod(dtoEmployeeDeductionMaintenance.getBenefitMethod());
			employeeDeductionMaintenance.setDeductionAmount(dtoEmployeeDeductionMaintenance.getDeductionAmount());
			employeeDeductionMaintenance.setDeductionPercent(dtoEmployeeDeductionMaintenance.getDeductionPercent());
			employeeDeductionMaintenance.setPerPeriord(dtoEmployeeDeductionMaintenance.getPerPeriord());
			employeeDeductionMaintenance.setPerYear(dtoEmployeeDeductionMaintenance.getPerYear());
			employeeDeductionMaintenance.setPayFactor(dtoEmployeeDeductionMaintenance.getPayFactor());
			employeeDeductionMaintenance.setLifeTime(dtoEmployeeDeductionMaintenance.getLifeTime());
			employeeDeductionMaintenance.setFrequency(dtoEmployeeDeductionMaintenance.getFrequency());
			employeeDeductionMaintenance.setInactive(dtoEmployeeDeductionMaintenance.getInactive());
			employeeDeductionMaintenance.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			employeeDeductionMaintenance.setNoOfDays(dtoEmployeeDeductionMaintenance.getNoOfDays());
			employeeDeductionMaintenance.setEndDateDays(dtoEmployeeDeductionMaintenance.getEndDateDays());
			repositoryEmployeeDeductionMaintenance.saveAndFlush(employeeDeductionMaintenance);

		} catch (Exception e) {
			log.debug("save EmployeeDeductionMaintenance" + " :" + dtoEmployeeDeductionMaintenance.getId());
		}
		return dtoEmployeeDeductionMaintenance;
	}

	public DtoEmployeeDeductionMaintenance delete(List<Integer> ids) {

		log.info("delete EmployeeDeductionMaintenance Method");

		DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance = new DtoEmployeeDeductionMaintenance();
		dtoEmployeeDeductionMaintenance.setDeleteMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_DEDUCTION_MAINTANENANCE_DELETED", false));
		dtoEmployeeDeductionMaintenance.setAssociateMessage(serviceResponse
				.getStringMessageByShortAndIsDeleted("EMPLOYEE_DEDUCTION_MAINTANENANCE_ASSOCIATED", false));
		List<DtoEmployeeDeductionMaintenance> deleteDtoEmployeeDeductionMaintenance = new ArrayList<>();
		List<TransactionEntryDetail> transactionEntryDetailList = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));

		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse
				.getMessageByShortAndIsDeleted("EMPLOYEE_DEDUCTION_MAINTANENANCE_NOT_DELETE_ID_MESSAGE", false)
				.getMessage());
		try {
			for (Integer planId : ids) {
				EmployeeDeductionMaintenance employeeDeductionMaintenance = repositoryEmployeeDeductionMaintenance
						.findOne(planId);

				if (employeeDeductionMaintenance != null) {
					// if(employeeDeductionMaintenance.getDeductionCode().getListBuildPayrollCheckByDeductions().isEmpty())
					// {
					transactionEntryDetailList = repositoryTransactionEntryDetail.findByTransactionEntry2(
							employeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId(),
							employeeDeductionMaintenance.getDeductionCode().getId());
					if (transactionEntryDetailList.isEmpty()) {
						DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance2 = new DtoEmployeeDeductionMaintenance();
						dtoEmployeeDeductionMaintenance.setId(employeeDeductionMaintenance.getId());

						repositoryEmployeeDeductionMaintenance.deleteSingleEmployeeDeductionMaintenance(true,
								loggedInUserId, planId);
						deleteDtoEmployeeDeductionMaintenance.add(dtoEmployeeDeductionMaintenance2);

					} else {
						inValidDelete = true;
					}

					/*
					 * }else { inValidDelete = true; }
					 */

				} else {
					inValidDelete = true;
				}

			}

			if (inValidDelete) {
				invlidDeleteMessage.replace(invlidDeleteMessage.length() - 1, invlidDeleteMessage.length(), "");
				dtoEmployeeDeductionMaintenance.setMessageType(invlidDeleteMessage.toString());

			}
			if (!inValidDelete) {
				dtoEmployeeDeductionMaintenance.setMessageType("");

			}

			dtoEmployeeDeductionMaintenance.setDelete(deleteDtoEmployeeDeductionMaintenance);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeeDeductionMaintenance" + " :" + dtoEmployeeDeductionMaintenance.getId());
		return dtoEmployeeDeductionMaintenance;
	}

	public boolean checkCodeInTransactionEntry(Integer employeeId, Integer codeType, Integer codeId) {	//New ME
		log.info("checkCodeInTransactionEntry Method");
		// TODO Auto-generated method stub
		
		//DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance = new DtoEmployeeDeductionMaintenance();
		//List<DtoEmployeeDeductionMaintenance> deleteDtoEmployeeDeductionMaintenance = new ArrayList<>();
		List<TransactionEntryDetail> transactionEntryDetailList = new ArrayList<>();
		//EmployeeDeductionMaintenance employeeDeductionMaintenance = null; //
		
		switch (codeType) {
		case 1:		//Deduction
			transactionEntryDetailList = repositoryTransactionEntryDetail.findByTransactionEntry2(employeeId, codeId);	//D
			break;
		case 2:		//payCode
			transactionEntryDetailList = repositoryTransactionEntryDetail.findByTransactionEntry4(employeeId, codeId);	//P
			break;
		case 3:		//Benefit
			transactionEntryDetailList = repositoryTransactionEntryDetail.findByTransactionEntry3(employeeId, codeId);	//B
			break;
		default:
			System.out.println("Warning! Wrong Code Type Entered!");
			break;
		}
		
		boolean isEmpty;
		//transactionEntryDetailList = repositoryTransactionEntryDetail.findByTransactionEntry2();
		if(transactionEntryDetailList.isEmpty()) 
			isEmpty = true;
		else
			isEmpty = false;
		
		System.out.println("isEmpty: " + isEmpty);
		System.out.println("Count: " + transactionEntryDetailList.size());

		//DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance = new DtoEmployeeDeductionMaintenance();
		//dtoEmployeeDeductionMaintenance.setId(employeeDeductionMaintenance.getId());
		//}
//		repositoryEmployeeDeductionMaintenance.deleteSingleEmployeeDeductionMaintenance(true,
//				loggedInUserId, planId);
//		deleteDtoEmployeeDeductionMaintenance.add(dtoEmployeeDeductionMaintenance2);
		return isEmpty;
	}

	public DtoEmployeeDeductionMaintenance getById(int id) {
		DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance = new DtoEmployeeDeductionMaintenance();
		try {

			if (id > 0) {
				EmployeeDeductionMaintenance employeeDeductionMaintenance = repositoryEmployeeDeductionMaintenance
						.findByIdAndIsDeleted(id, false);
				if (employeeDeductionMaintenance != null) {
					dtoEmployeeDeductionMaintenance = new DtoEmployeeDeductionMaintenance(employeeDeductionMaintenance);

					dtoEmployeeDeductionMaintenance.setStartDate(employeeDeductionMaintenance.getStartDate());
					dtoEmployeeDeductionMaintenance.setEndDate(employeeDeductionMaintenance.getEndDate());
					dtoEmployeeDeductionMaintenance
							.setTransactionRequired(dtoEmployeeDeductionMaintenance.getTransactionRequired());
					dtoEmployeeDeductionMaintenance.setBenefitMethod(employeeDeductionMaintenance.getBenefitMethod());
					dtoEmployeeDeductionMaintenance
							.setDeductionAmount(employeeDeductionMaintenance.getDeductionAmount());
					dtoEmployeeDeductionMaintenance
							.setDeductionPercent(employeeDeductionMaintenance.getDeductionPercent());
					dtoEmployeeDeductionMaintenance.setPerPeriord(employeeDeductionMaintenance.getPerPeriord());
					dtoEmployeeDeductionMaintenance.setPerYear(employeeDeductionMaintenance.getPerYear());
					dtoEmployeeDeductionMaintenance.setPayFactor(employeeDeductionMaintenance.getPayFactor());
					dtoEmployeeDeductionMaintenance.setLifeTime(employeeDeductionMaintenance.getLifeTime());
					dtoEmployeeDeductionMaintenance.setFrequency(employeeDeductionMaintenance.getFrequency());
					dtoEmployeeDeductionMaintenance.setInactive(employeeDeductionMaintenance.getInactive());
					dtoEmployeeDeductionMaintenance.setNoOfDays(employeeDeductionMaintenance.getNoOfDays());
					dtoEmployeeDeductionMaintenance.setEndDateDays(employeeDeductionMaintenance.getEndDateDays());
					dtoEmployeeDeductionMaintenance.setIsActive(employeeDeductionMaintenance.getInactive());
					List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
							.findByEmployeeId(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId());
					if (employeeDeductionMaintenance.getDeductionCode().getPayCodeList() != null
							&& !employeeDeductionMaintenance.getDeductionCode().getPayCodeList().isEmpty()) {
						List<Integer> listId = new ArrayList<>();
						for (PayCode payCode : employeeDeductionMaintenance.getDeductionCode().getPayCodeList()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance1 : employeePayCodeMaintenance) {
								if (payCode.getId() == employeePayCodeMaintenance1.getPayCode().getId()) {
									listId.add(payCode.getId());
								}
							}
						}

						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCode2.setAmount(payCode1.getBaseOnPayCodeAmount());
								payCode2.setPayRate(payCode1.getPayRate());
								payCode2.setPayFactor(payCode1.getPayFactor());
								payCodeList.add(payCode2);
							}
							dtoEmployeeDeductionMaintenance.setDtoPayCode(payCodeList);
						}

					}

					DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
					if (employeeDeductionMaintenance.getEmployeeMaster() != null) {
						employeeMaster.setEmployeeIndexId(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId());
						employeeMaster.setEmployeeId(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeId());
						employeeMaster.setEmployeeBirthDate(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeBirthDate());
						employeeMaster.setEmployeeCitizen(
								employeeDeductionMaintenance.getEmployeeMaster().isEmployeeCitizen());
						employeeMaster.setEmployeeFirstName(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeFirstName());
						employeeMaster.setEmployeeFirstNameArabic(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeFirstNameArabic());
						employeeMaster.setEmployeeGender(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeGender());
						employeeMaster.setEmployeeHireDate(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeHireDate());
						employeeMaster.setEmployeeImmigration(
								employeeDeductionMaintenance.getEmployeeMaster().isEmployeeImmigration());
						employeeMaster.setEmployeeInactive(
								employeeDeductionMaintenance.getEmployeeMaster().isEmployeeInactive());
						employeeMaster.setEmployeeInactiveDate(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactiveDate());
						employeeMaster.setEmployeeInactiveReason(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactiveReason());
						employeeMaster.setEmployeeLastName(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeLastName());
						employeeMaster.setEmployeeLastNameArabic(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeLastNameArabic());
						employeeMaster.setEmployeeLastWorkDate(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeLastWorkDate());
						employeeMaster.setEmployeeMaritalStatus(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeMaritalStatus());
						employeeMaster.setEmployeeMiddleName(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeMiddleName());
						employeeMaster.setEmployeeMiddleNameArabic(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeMiddleNameArabic());
						employeeMaster
								.setEmployeeTitle(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeTitle());
						employeeMaster.setEmployeeTitleArabic(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeTitleArabic());
						employeeMaster
								.setEmployeeType(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeType());
						employeeMaster.setEmployeeUserIdInSystem(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeUserIdInSystem());
						employeeMaster.setEmployeeWorkHourYearly(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeWorkHourYearly());
						dtoEmployeeDeductionMaintenance.setEmployeeMaster(employeeMaster);

					}

				}
			} else {
				dtoEmployeeDeductionMaintenance.setMessageType("INVALID_ID");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDeductionMaintenance;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {

		try {

			log.info("searchEmployeeDeductionMaintenance Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";
				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

					if (dtoSearch.getSortOn().equals("endDate") || dtoSearch.getSortOn().equals("startDate")
							|| dtoSearch.getSortOn().equals("employeeMaster.employeeId")
							|| dtoSearch.getSortOn().equals("deductionCode.diductionId")) {
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
					}

				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}

				dtoSearch.setTotalCount(this.repositoryEmployeeDeductionMaintenance
						.predictiveEmployeeDeductionMaintenanceSearchTotalCount("%" + searchWord + "%"));
				List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = null;
				
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						employeeDeductionMaintenanceList = this.repositoryEmployeeDeductionMaintenance
								.predictiveEmployeeDeductionMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						employeeDeductionMaintenanceList = this.repositoryEmployeeDeductionMaintenance
								.predictiveEmployeeDeductionMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						employeeDeductionMaintenanceList = this.repositoryEmployeeDeductionMaintenance
								.predictiveEmployeeDeductionMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.DESC, condition));
					}

				}
				
				if (employeeDeductionMaintenanceList != null && !employeeDeductionMaintenanceList.isEmpty()) {
					List<DtoEmployeeDeductionMaintenance> dtoEmployeeDeductionMaintenanceList = new ArrayList<>();
					DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance = null;
					for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenanceList) {
						
						dtoEmployeeDeductionMaintenance = new DtoEmployeeDeductionMaintenance(
								employeeDeductionMaintenance);

						DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
						if (employeeDeductionMaintenance.getEmployeeMaster() != null) {
							employeeMaster.setEmployeeIndexId(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId());
							employeeMaster
									.setEmployeeId(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeId());
							employeeMaster.setEmployeeBirthDate(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeBirthDate());
							employeeMaster.setEmployeeCitizen(
									employeeDeductionMaintenance.getEmployeeMaster().isEmployeeCitizen());
							employeeMaster.setEmployeeFirstName(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeFirstName());
							employeeMaster.setEmployeeFirstNameArabic(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeFirstNameArabic());
							employeeMaster.setEmployeeGender(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeGender());
							employeeMaster.setEmployeeHireDate(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeHireDate());
							employeeMaster.setEmployeeImmigration(
									employeeDeductionMaintenance.getEmployeeMaster().isEmployeeImmigration());
							employeeMaster.setEmployeeInactive(
									employeeDeductionMaintenance.getEmployeeMaster().isEmployeeInactive());
							employeeMaster.setEmployeeInactiveDate(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactiveDate());
							employeeMaster.setEmployeeInactiveReason(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactiveReason());
							employeeMaster.setEmployeeLastName(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeLastName());
							employeeMaster.setEmployeeLastNameArabic(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeLastNameArabic());
							employeeMaster.setEmployeeLastWorkDate(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeLastWorkDate());
							employeeMaster.setEmployeeMaritalStatus(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeMaritalStatus());
							employeeMaster.setEmployeeMiddleName(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeMiddleName());
							employeeMaster.setEmployeeMiddleNameArabic(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeMiddleNameArabic());
							employeeMaster.setEmployeeTitle(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeTitle());
							employeeMaster.setEmployeeTitleArabic(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeTitleArabic());
							employeeMaster.setEmployeeType(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeType());
							employeeMaster.setEmployeeUserIdInSystem(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeUserIdInSystem());
							employeeMaster.setEmployeeWorkHourYearly(
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeWorkHourYearly());
							dtoEmployeeDeductionMaintenance.setEmployeeMaster(employeeMaster);

						}
						DtoDeductionCode deductionCode = new DtoDeductionCode();
						if (employeeDeductionMaintenance.getDeductionCode() != null) {

							deductionCode.setId(employeeDeductionMaintenance.getDeductionCode().getId());
							deductionCode
									.setDiductionId(employeeDeductionMaintenance.getDeductionCode().getDiductionId());
							deductionCode
									.setDiscription(employeeDeductionMaintenance.getDeductionCode().getDiscription());
							deductionCode.setArbicDiscription(
									employeeDeductionMaintenance.getDeductionCode().getArbicDiscription());
							deductionCode.setStartDate(employeeDeductionMaintenance.getDeductionCode().getStartDate());
							deductionCode.setEndDate(employeeDeductionMaintenance.getDeductionCode().getEndDate());
							deductionCode.setTransction(employeeDeductionMaintenance.getDeductionCode().isTransction());
							deductionCode.setMethod(employeeDeductionMaintenance.getDeductionCode().getMethod());
							deductionCode.setAmount(employeeDeductionMaintenance.getDeductionCode().getAmount());
							deductionCode.setPercent(employeeDeductionMaintenance.getDeductionCode().getPercent());
							deductionCode.setPerPeriod(employeeDeductionMaintenance.getDeductionCode().getPerPeriod());
							deductionCode.setPerYear(employeeDeductionMaintenance.getPerYear());
							deductionCode.setLifeTime(employeeDeductionMaintenance.getLifeTime());
							deductionCode.setFrequency(deductionCode.getFrequency());
							// deductionCode.setInActive(deductionCode.getInActive());
							deductionCode.setIsActive(deductionCode.getIsActive());
							deductionCode.setCustomDate(employeeDeductionMaintenance.getDeductionCode().isCustomDate());
							
							deductionCode.setRoundOf(employeeDeductionMaintenance.getDeductionCode().getRoundOf());				//ME
							deductionCode.setDeductionTypeId(employeeDeductionMaintenance.getDeductionCode().getTypeField());	//ME
							
							dtoEmployeeDeductionMaintenance.setDeductionCode(deductionCode);
						}

						dtoEmployeeDeductionMaintenance.setId(employeeDeductionMaintenance.getId());
						if(employeeDeductionMaintenance.getEmployeeMaster() != null && 
								employeeDeductionMaintenance.getDeductionCode().getNoOfDays()!=null && employeeDeductionMaintenance.getDeductionCode().getEndDateDays() != null) {

							dtoEmployeeDeductionMaintenance.setStartDate(UtilDateAndTimeHr.startDate(employeeDeductionMaintenance.getDeductionCode().getNoOfDays(),employeeDeductionMaintenance.getEmployeeMaster().getEmployeeHireDate()));
							dtoEmployeeDeductionMaintenance.setEndDate(UtilDateAndTimeHr.endDate(employeeDeductionMaintenance.getDeductionCode().getNoOfDays(), employeeDeductionMaintenance.getDeductionCode().getEndDateDays(), employeeDeductionMaintenance.getEmployeeMaster().getEmployeeHireDate()));
						}else {
							dtoEmployeeDeductionMaintenance.setStartDate(employeeDeductionMaintenance.getStartDate());
							dtoEmployeeDeductionMaintenance.setEndDate(employeeDeductionMaintenance.getEndDate());
						}
//						dtoEmployeeDeductionMaintenance.setStartDate(employeeDeductionMaintenance.getStartDate());
//						dtoEmployeeDeductionMaintenance.setEndDate(employeeDeductionMaintenance.getEndDate());
						dtoEmployeeDeductionMaintenance
								.setTransactionRequired(employeeDeductionMaintenance.isTransactionRequired());
						dtoEmployeeDeductionMaintenance
								.setBenefitMethod(employeeDeductionMaintenance.getBenefitMethod());
						dtoEmployeeDeductionMaintenance
								.setDeductionAmount(employeeDeductionMaintenance.getDeductionAmount());
						dtoEmployeeDeductionMaintenance
								.setDeductionPercent(employeeDeductionMaintenance.getDeductionPercent());
						dtoEmployeeDeductionMaintenance.setPerPeriord(employeeDeductionMaintenance.getPerPeriord());
						dtoEmployeeDeductionMaintenance.setPerYear(employeeDeductionMaintenance.getPerYear());
						dtoEmployeeDeductionMaintenance.setLifeTime(employeeDeductionMaintenance.getLifeTime());
						dtoEmployeeDeductionMaintenance.setFrequency(employeeDeductionMaintenance.getFrequency());
						dtoEmployeeDeductionMaintenance.setInactive(employeeDeductionMaintenance.getInactive());

						dtoEmployeeDeductionMaintenanceList.add(dtoEmployeeDeductionMaintenance);
					}
					dtoSearch.setRecords(dtoEmployeeDeductionMaintenanceList);
				}

				log.debug("searchEmployeeDeductionMaintenance Size is:" + dtoSearch.getTotalCount());

			}

		} catch (Exception e) {
			log.error(e);
		}

		return dtoSearch;
	}

	public DtoSearch getByEmployeeId(int id) {

		DtoSearch dtoSearch = new DtoSearch();
		try {

			List<DtoEmployeeDeductionMaintenance> dtoEmployeeDeductionMaintenance = new ArrayList<>();
			if (id > 0) {
				List<EmployeeDeductionMaintenance> employeeDeductionMaintenance = repositoryEmployeeDeductionMaintenance
						.getByEmployeeId(id);

				for (EmployeeDeductionMaintenance employeeDeductionMaintenance2 : employeeDeductionMaintenance) {
					DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenanceEdu = new DtoEmployeeDeductionMaintenance(
							employeeDeductionMaintenance2);

					DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
					if (employeeDeductionMaintenance2.getEmployeeMaster() != null) {
						employeeMaster.setEmployeeIndexId(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeIndexId());
						employeeMaster.setEmployeeId(employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeId());
						employeeMaster.setEmployeeBirthDate(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeBirthDate());
						employeeMaster.setEmployeeCitizen(
								employeeDeductionMaintenance2.getEmployeeMaster().isEmployeeCitizen());
						employeeMaster.setEmployeeFirstName(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeFirstName());
						employeeMaster.setEmployeeFirstNameArabic(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeFirstNameArabic());
						employeeMaster.setEmployeeGender(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeGender());
						employeeMaster.setEmployeeHireDate(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeHireDate());
						employeeMaster.setEmployeeImmigration(
								employeeDeductionMaintenance2.getEmployeeMaster().isEmployeeImmigration());
						employeeMaster.setEmployeeInactive(
								employeeDeductionMaintenance2.getEmployeeMaster().isEmployeeInactive());
						employeeMaster.setEmployeeInactiveDate(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeInactiveDate());
						employeeMaster.setEmployeeInactiveReason(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeInactiveReason());
						employeeMaster.setEmployeeLastName(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeLastName());
						employeeMaster.setEmployeeLastNameArabic(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeLastNameArabic());
						employeeMaster.setEmployeeLastWorkDate(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeLastWorkDate());
						employeeMaster.setEmployeeMaritalStatus(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeMaritalStatus());
						employeeMaster.setEmployeeMiddleName(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeMiddleName());
						employeeMaster.setEmployeeMiddleNameArabic(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeMiddleNameArabic());
						employeeMaster
								.setEmployeeTitle(employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeTitle());
						employeeMaster.setEmployeeTitleArabic(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeTitleArabic());
						employeeMaster
								.setEmployeeType(employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeType());
						employeeMaster.setEmployeeUserIdInSystem(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeUserIdInSystem());
						employeeMaster.setEmployeeWorkHourYearly(
								employeeDeductionMaintenance2.getEmployeeMaster().getEmployeeWorkHourYearly());
						dtoEmployeeDeductionMaintenanceEdu.setEmployeeMaster(employeeMaster);

					}

					DtoDeductionCode dtoDeductionCode = new DtoDeductionCode();
					if (employeeDeductionMaintenance2.getDeductionCode() != null) {
						dtoDeductionCode.setId(employeeDeductionMaintenance2.getDeductionCode().getId());
						dtoDeductionCode
								.setDiductionId(employeeDeductionMaintenance2.getDeductionCode().getDiductionId());
						dtoDeductionCode.setId(employeeDeductionMaintenance2.getId());
						dtoDeductionCode
								.setDiscription(employeeDeductionMaintenance2.getDeductionCode().getDiscription());
						dtoDeductionCode.setArbicDiscription(
								employeeDeductionMaintenance2.getDeductionCode().getArbicDiscription());
						dtoDeductionCode.setStartDate(employeeDeductionMaintenance2.getDeductionCode().getStartDate());
						dtoDeductionCode.setEndDate(employeeDeductionMaintenance2.getDeductionCode().getEndDate());
						dtoDeductionCode.setTransction(employeeDeductionMaintenance2.getDeductionCode().isTransction());
						dtoDeductionCode.setMethod(employeeDeductionMaintenance2.getDeductionCode().getMethod());
						dtoDeductionCode.setAmount(employeeDeductionMaintenance2.getDeductionCode().getAmount());
						dtoDeductionCode.setPercent(employeeDeductionMaintenance2.getDeductionCode().getPercent());
						dtoDeductionCode.setPerPeriod(employeeDeductionMaintenance2.getDeductionCode().getPerPeriod());
						dtoDeductionCode.setPerYear(employeeDeductionMaintenance2.getPerYear());
						dtoDeductionCode.setLifeTime(employeeDeductionMaintenance2.getLifeTime());
						dtoDeductionCode.setFrequency(employeeDeductionMaintenance2.getFrequency());
						dtoEmployeeDeductionMaintenanceEdu.setDeductionCode(dtoDeductionCode);

					}
					if (employeeDeductionMaintenance2.getDeductionCode().getPayCodeList() != null
							&& !employeeDeductionMaintenance2.getDeductionCode().getPayCodeList().isEmpty()) {
						List<Integer> listId = new ArrayList<>();
						for (PayCode payCode : employeeDeductionMaintenance2.getDeductionCode().getPayCodeList()) {
							listId.add(payCode.getId());
						}
						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCodeList.add(payCode2);
							}
							dtoEmployeeDeductionMaintenanceEdu.setDtoPayCode(payCodeList);
						}

					}

					dtoEmployeeDeductionMaintenanceEdu.setId(employeeDeductionMaintenance2.getId());
					dtoEmployeeDeductionMaintenanceEdu.setStartDate(employeeDeductionMaintenance2.getStartDate());
					dtoEmployeeDeductionMaintenanceEdu.setEndDate(employeeDeductionMaintenance2.getEndDate());
					dtoEmployeeDeductionMaintenanceEdu
							.setTransactionRequired(employeeDeductionMaintenance2.isTransactionRequired());
					dtoEmployeeDeductionMaintenanceEdu
							.setBenefitMethod(employeeDeductionMaintenance2.getBenefitMethod());
					dtoEmployeeDeductionMaintenanceEdu
							.setDeductionAmount(employeeDeductionMaintenance2.getDeductionAmount());
					dtoEmployeeDeductionMaintenanceEdu
							.setDeductionPercent(employeeDeductionMaintenance2.getDeductionPercent());
					dtoEmployeeDeductionMaintenanceEdu.setPerPeriord(employeeDeductionMaintenance2.getPerPeriord());
					dtoEmployeeDeductionMaintenanceEdu.setPerYear(employeeDeductionMaintenance2.getPerYear());
					dtoEmployeeDeductionMaintenanceEdu.setLifeTime(employeeDeductionMaintenance2.getLifeTime());
					dtoEmployeeDeductionMaintenanceEdu.setFrequency(employeeDeductionMaintenance2.getFrequency());
					dtoEmployeeDeductionMaintenanceEdu.setInactive(employeeDeductionMaintenance2.getInactive());

					dtoEmployeeDeductionMaintenance.add(dtoEmployeeDeductionMaintenanceEdu);
				}

			}
			dtoSearch.setRecords(dtoEmployeeDeductionMaintenance);

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoEmployeeDeductionMaintenance validate(Integer employeeId, Integer deductionCode) {
		log.info("repeatByEmployeeId Method");
		DtoEmployeeDeductionMaintenance dto = new DtoEmployeeDeductionMaintenance();
		try {
			List<EmployeeDeductionMaintenance> list = repositoryEmployeeDeductionMaintenance.validate(employeeId,
					deductionCode);
			if (list != null && !list.isEmpty()) {
				dto.setIsRepeat(true);
			} else {
				dto.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dto;
	}

}
