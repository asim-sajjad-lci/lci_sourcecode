/**
 * 
 */
package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.BuildPayrollCheckByDeductions;
import com.bti.hcm.model.EmployeeVacationEntry;

/**
 * @author Sheikh Naser
 *
 */
@Repository("repositoryEmployeeVacationEntry")
public interface RepositoryEmployeeVacationEntry extends JpaRepository<EmployeeVacationEntry, Integer>{

}
