/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.HijriDay;

/**
 * Description: Interface for Hijri Day
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 *
 */
@Repository("repositoryHijriDay")
public interface RepositoryHijriDay extends JpaRepository<HijriDay, Integer> {

	/**
	 * @return
	 */
	@Query("select count(*) from HijriDay hd where hd.isDeleted=false")
	public Integer getCountOfTotalDepartment();
	
	/**
	 * @param deleted
	 * @return
	 */
	public List<HijriDay> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param Id
	 * @return
	 */
	
}
