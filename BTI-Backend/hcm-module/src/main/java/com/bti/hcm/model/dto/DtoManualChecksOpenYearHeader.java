package com.bti.hcm.model.dto;

import java.util.Date;

import com.ibm.icu.math.BigDecimal;

public class DtoManualChecksOpenYearHeader {

	private Integer id;
	private String paymentDescription;
	private Integer checkNumber;
	private Integer employeeId;
	private Date checkDate;
	private Date checkPostDate;
	private BigDecimal totalGrossAmount;
	private BigDecimal totalNetAmount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public Integer getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public Date getCheckPostDate() {
		return checkPostDate;
	}

	public void setCheckPostDate(Date checkPostDate) {
		this.checkPostDate = checkPostDate;
	}

	public BigDecimal getTotalGrossAmount() {
		return totalGrossAmount;
	}

	public void setTotalGrossAmount(BigDecimal totalGrossAmount) {
		this.totalGrossAmount = totalGrossAmount;
	}

	public BigDecimal getTotalNetAmount() {
		return totalNetAmount;
	}

	public void setTotalNetAmount(BigDecimal totalNetAmount) {
		this.totalNetAmount = totalNetAmount;
	}

}
