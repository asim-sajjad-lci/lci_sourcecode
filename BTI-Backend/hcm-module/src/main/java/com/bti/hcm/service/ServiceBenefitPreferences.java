package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.BenefitPreferences;
import com.bti.hcm.model.dto.DtoBenefitPreferences;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitPreferences;

@Service("serviceBenefitPreferences")
public class ServiceBenefitPreferences {
	/**
	 * @Description LOGGER use for put a logger in BenefitPreferences Service
	 */
	static Logger log = Logger.getLogger(ServiceBenefitPreferences.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in BenefitPreferences service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in BenefitPreferences service
	 */
	@Autowired
	ServiceResponse response;
	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in BenefitPreferences service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	
	/**
	 * @Description repositoryBenefitPreferences Autowired here using annotation of spring for access of repositoryBenefitPreferences method in BenefitPreferences service
	 */
	@Autowired(required=false)
	RepositoryBenefitPreferences repositoryBenefitPreferences;
	
	
	
	/**
	 * @param dtoBenefitPreferences
	 * @return
	 */
	
	public DtoBenefitPreferences saveOrUpdate(DtoBenefitPreferences dtoBenefitPreferences) {

		try {
			log.info("saveOrUpdate BenefitPreferences Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			BenefitPreferences benefitPreferences = null;
			if (dtoBenefitPreferences.getId() != null && dtoBenefitPreferences.getId() > 0) {
				
				benefitPreferences = repositoryBenefitPreferences.findByIdAndIsDeleted(dtoBenefitPreferences.getId(), false);
				benefitPreferences.setUpdatedBy(loggedInUserId);
				benefitPreferences.setUpdatedDate(new Date());
			} else {
				benefitPreferences = new BenefitPreferences();
				benefitPreferences.setCreatedDate(new Date());
				Integer rowId = repositoryBenefitPreferences.getCountOfTotalBenefitPreferences();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				benefitPreferences.setRowId(increment);
			}
			benefitPreferences.setPostEstimatedDate(dtoBenefitPreferences.getPostEstimatedDate());
			benefitPreferences.setPostPaymentDates(dtoBenefitPreferences.getPostPaymentDates());
			benefitPreferences.setWorkingdaysWeek(dtoBenefitPreferences.getWorkingdaysWeek());
			benefitPreferences.setWorkingHoursDay(dtoBenefitPreferences.getWorkingHoursDay());
			benefitPreferences.setEligibilityDate(dtoBenefitPreferences.getEligibilityDate());
			benefitPreferences.setPaperworkDeadlines(dtoBenefitPreferences.getPaperworkDeadlines());
			benefitPreferences = repositoryBenefitPreferences.saveAndFlush(benefitPreferences);
			log.debug("BenefitPreferences is:"+benefitPreferences.getId());
		} catch (Exception e) {
			log.error(e);
			
		}
		return dtoBenefitPreferences;
	
	}
	
	/**
	 * @param ids
	 * @return
	 */
	public DtoBenefitPreferences delete(List<Integer> ids) {
		log.info("delete BenefitPreferences Method");
		DtoBenefitPreferences dtoBenefitPreferences = new DtoBenefitPreferences();
		dtoBenefitPreferences
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("BENEFIT_PREFRENCES_DELETED", false));
		dtoBenefitPreferences.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("BENEFIT_PREFRENCES_DELETED", false));
		List<DtoBenefitPreferences> deleteBenefitPreferences = new ArrayList<>();
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("BENEFIT_PREFRENCES_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer id : ids) {
				BenefitPreferences benefitPreferences = repositoryBenefitPreferences.findOne(id);
				if(benefitPreferences!=null) {
					DtoBenefitPreferences dtoBenefitPreferences1 = new DtoBenefitPreferences();
					dtoBenefitPreferences1.setId(benefitPreferences.getId());
					deleteBenefitPreferences.add(dtoBenefitPreferences1);
	
				}else {
					inValidDelete = true;
				}
				
			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoBenefitPreferences.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoBenefitPreferences.setMessageType("");
				
			}
			
			dtoBenefitPreferences.setDelete(deleteBenefitPreferences);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete BenefitPreferences :"+dtoBenefitPreferences.getId());
		return dtoBenefitPreferences;
	}
	
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("search BenefitPreferences Method");
			if(dtoSearch != null){
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("postEstimatedDate") || dtoSearch.getSortOn().equals("postPaymentDates") || dtoSearch.getSortOn().equals("workingdaysWeek") || dtoSearch.getSortOn().equals("workingHoursDay")
						|| dtoSearch.getSortOn().equals("eligibilityDate")	|| dtoSearch.getSortOn().equals("paperworkDeadlines")) {
						condition = dtoSearch.getSortOn();
					
				}else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
				dtoSearch.setTotalCount(this.repositoryBenefitPreferences.predictiveBenefitPreferencesTotalCount());
				List<BenefitPreferences> payCodeList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						payCodeList = this.repositoryBenefitPreferences.predictiveBenefitPreferencesTotalCount(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						payCodeList = this.repositoryBenefitPreferences.predictiveBenefitPreferencesTotalCount(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						payCodeList = this.repositoryBenefitPreferences.predictiveBenefitPreferencesTotalCount(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				if(payCodeList != null && !payCodeList.isEmpty()){
					List<DtoBenefitPreferences> dtoBenefitPreferences1 = new ArrayList<>();
					DtoBenefitPreferences dtoBenefitPreferences=null;
					for (BenefitPreferences benefitPreferences : payCodeList) {
						dtoBenefitPreferences = new DtoBenefitPreferences(benefitPreferences);
						dtoBenefitPreferences.setId(benefitPreferences.getId());
						dtoBenefitPreferences.setPostEstimatedDate(benefitPreferences.getPostEstimatedDate());
						dtoBenefitPreferences.setPostPaymentDates(benefitPreferences.getPostPaymentDates());
						dtoBenefitPreferences.setWorkingdaysWeek(benefitPreferences.getWorkingdaysWeek());
						dtoBenefitPreferences.setWorkingHoursDay(benefitPreferences.getWorkingHoursDay());
						dtoBenefitPreferences.setEligibilityDate(benefitPreferences.getEligibilityDate());
						dtoBenefitPreferences.setPaperworkDeadlines(benefitPreferences.getPaperworkDeadlines());
						dtoBenefitPreferences1.add(dtoBenefitPreferences);
					}
					dtoSearch.setRecords(dtoBenefitPreferences1);
				}
			}
			log.debug("Search BenefitPreferences Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	public DtoBenefitPreferences getById(int id) {
		log.info("getById Method");
		DtoBenefitPreferences dtoBenefitPreferences = new DtoBenefitPreferences();
		try {
			if (id > 0) {
				BenefitPreferences benefitPreferences = repositoryBenefitPreferences.findByIdAndIsDeleted(id, false);
				if (benefitPreferences != null) {
					dtoBenefitPreferences = new DtoBenefitPreferences(benefitPreferences);
					
					dtoBenefitPreferences.setId(benefitPreferences.getId());
					dtoBenefitPreferences.setPostEstimatedDate(benefitPreferences.getPostEstimatedDate());
					dtoBenefitPreferences.setPostPaymentDates(benefitPreferences.getPostPaymentDates());
					dtoBenefitPreferences.setWorkingdaysWeek(benefitPreferences.getWorkingdaysWeek());
					dtoBenefitPreferences.setWorkingHoursDay(benefitPreferences.getWorkingHoursDay());
					dtoBenefitPreferences.setEligibilityDate(benefitPreferences.getEligibilityDate());
					dtoBenefitPreferences.setPaperworkDeadlines(benefitPreferences.getPaperworkDeadlines());
					dtoBenefitPreferences.setId(benefitPreferences.getId());
				} else {
					dtoBenefitPreferences.setMessageType("TIME_CODE_NOT_GETTING");

				}
			} else {
				dtoBenefitPreferences.setMessageType("INVALID_TIME_CODE_ID");

			}
			log.debug("BenefitPreferences By Id is:"+dtoBenefitPreferences.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBenefitPreferences;
	}
	

	public DtoSearch getIds(DtoSearch dtoSearch) {
		try {
			log.info("search BenefitPreferences Method");
			if(dtoSearch != null){
				List<BenefitPreferences> benefitPreferencesList = this.repositoryBenefitPreferences.findAll();
					if(!benefitPreferencesList.isEmpty()) {
						dtoSearch.setTotalCount(benefitPreferencesList.size());
						dtoSearch.setRecords(benefitPreferencesList.size());
					}
				dtoSearch.setRecords(benefitPreferencesList);
				dtoSearch.setTotalCount(benefitPreferencesList.size());
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
}
