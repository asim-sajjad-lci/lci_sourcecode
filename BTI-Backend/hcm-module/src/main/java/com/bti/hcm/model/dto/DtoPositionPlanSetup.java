package com.bti.hcm.model.dto;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.Position;
import com.bti.hcm.model.PositionPalnSetup;
import com.bti.hcm.util.UtilRandomKey;

public class DtoPositionPlanSetup extends DtoBase{
	
	private Integer id;
	private Position position;
	private String positionDescription;
	private String positionId;
	private String positionPlanId;
	private String positionPlanDesc;
	private String positionPlanArbicDesc;
	private Date positionPlanStart;
	private Date positionPlanEnd;
	private boolean inactive;
	private List<DtoPositionPlanSetup> deletePositionSetup;
	
	
	public String getPositionId() {
		return positionId;
	}
	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	
	public String getPositionPlanId() {
		return positionPlanId;
	}
	public void setPositionPlanId(String positionPlanId) {
		this.positionPlanId = positionPlanId;
	}
	public String getPositionPlanDesc() {
		return positionPlanDesc;
	}
	public void setPositionPlanDesc(String positionPlanDesc) {
		this.positionPlanDesc = positionPlanDesc;
	}
	public String getPositionPlanArbicDesc() {
		return positionPlanArbicDesc;
	}
	public void setPositionPlanArbicDesc(String positionPlanArbicDesc) {
		this.positionPlanArbicDesc = positionPlanArbicDesc;
	}
	public Date getPositionPlanStart() {
		return positionPlanStart;
	}
	public void setPositionPlanStart(Date positionPlanStart) {
		this.positionPlanStart = positionPlanStart;
	}
	public Date getPositionPlanEnd() {
		return positionPlanEnd;
	}
	public void setPositionPlanEnd(Date positionPlanEnd) {
		this.positionPlanEnd = positionPlanEnd;
	}
	public boolean isInactive() {
		return inactive;
	}
	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}
	public List<DtoPositionPlanSetup> getDeletePositionSetup() {
		return deletePositionSetup;
	}
	public void setDeletePositionSetup(List<DtoPositionPlanSetup> deletePositionSetup) {
		this.deletePositionSetup = deletePositionSetup;
	}
	public DtoPositionPlanSetup() {
	}
	
	
	public String getPositionDescription() {
		return positionDescription;
	}
	public void setPositionDescription(String positionDescription) {
		this.positionDescription = positionDescription;
	}
	public DtoPositionPlanSetup(PositionPalnSetup positionPalnSetup) {
		this.positionPlanId = positionPalnSetup.getPositionPlanId();
		
		if (UtilRandomKey.isNotBlank(positionPalnSetup.getPositionPlanDesc())) {
			this.positionPlanDesc = positionPalnSetup.getPositionPlanDesc();
		} else {
			this.positionPlanDesc = "";
		}
		if (UtilRandomKey.isNotBlank(positionPalnSetup.getPositionPlanArbicDesc())) {
			this.positionPlanArbicDesc = positionPalnSetup.getPositionPlanArbicDesc();
		} else {
			this.positionPlanArbicDesc = "";
		}
	}
	
}
