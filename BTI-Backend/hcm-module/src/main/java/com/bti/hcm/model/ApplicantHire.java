/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the ApplicantHire database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name="HR00904",indexes = {
        @Index(columnList = "APLHIRINDX")
})
@NamedQuery(name = "ApplicantHire.findAll", query = "SELECT a FROM ApplicantHire a")
public class ApplicantHire extends HcmBaseEntity implements Serializable  {

	
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "APLHIRINDX")
	private int id;
	

	@Column(name = "EMPLMTTYP")
	private short employmentType;
	
	
	@Column(name = "WRKHRYEALY")
	private int workHour;
	
	
	@Column(name = "HIREDT")
	private Date hireDate;
	
	
	@ManyToOne
	@JoinColumn(name="APPLICINDX")
	private Applicant applicant;
	
	@ManyToOne
	@JoinColumn(name="EMPLOYINDX")
	private EmployeeMaster employeeMaster;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(short employmentType) {
		this.employmentType = employmentType;
	}

	public int getWorkHour() {
		return workHour;
	}

	public void setWorkHour(int workHour) {
		this.workHour = workHour;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	
	
	

	
}
