package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.CheckbookMaintenance;
import com.bti.hcm.model.FinancialDimensions;
import com.bti.hcm.model.PayrollSetup;
import com.bti.hcm.model.VATSetup;
import com.bti.hcm.model.dto.DtoCheckbookMaintenance;
import com.bti.hcm.model.dto.DtoFinancialDimensions;
import com.bti.hcm.model.dto.DtoPayrollSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoVatSetup;
import com.bti.hcm.repository.RepositoryCheckBookMaintenance;
import com.bti.hcm.repository.RepositoryFinancialDimensions;
import com.bti.hcm.repository.RepositoryPayrollSetup;
import com.bti.hcm.repository.RepositoryVatSetup;

@Service("servicePayrollSetup")
public class ServicePayrollSetup {

	static Logger log = Logger.getLogger(ServicePayCodeType.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryPayrollSetup repositoryPayrollSetup;

	@Autowired
	RepositoryVatSetup repositoryVatSetup;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryFinancialDimensions repositoryFinancialDimensions;

	public DtoPayrollSetup saveOrUpdate(DtoPayrollSetup dtoPayrollSetup) {
		try {
			log.info("saveOrUpdate Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
			PayrollSetup payrollSetup = null;
			if (dtoPayrollSetup.getId() != null && dtoPayrollSetup.getId() > 0) {

				payrollSetup = repositoryPayrollSetup.findByIdAndIsDeleted(dtoPayrollSetup.getId(), false);
				payrollSetup.setUpdatedBy(loggedInUserId);
				payrollSetup.setUpdatedDate(new Date());
			} else {
				payrollSetup = new PayrollSetup();
				payrollSetup.setCreatedDate(new Date());

				Integer rowId = repositoryPayrollSetup.findAll().size();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				payrollSetup.setRowId(increment);
			}
			FinancialDimensions departmentDimensions = null;
			FinancialDimensions employeeDimensions = null;
			FinancialDimensions projectDimensions = null;
			CheckbookMaintenance payrollCheckbook = null;
			CheckbookMaintenance manualCheckCheckbook = null;
			VATSetup vatSetup = null;
			if (dtoPayrollSetup.getDepartmentDimensions() != null
					&& dtoPayrollSetup.getDepartmentDimensions().getId() > 0) {
				departmentDimensions = repositoryFinancialDimensions
						.findOne(dtoPayrollSetup.getDepartmentDimensions().getId());
			}

			if (dtoPayrollSetup.getEmployeeDimensions() != null
					&& dtoPayrollSetup.getEmployeeDimensions().getId() > 0) {
				employeeDimensions = repositoryFinancialDimensions
						.findOne(dtoPayrollSetup.getEmployeeDimensions().getId());
			}

			if (dtoPayrollSetup.getProjectDimensions() != null && dtoPayrollSetup.getProjectDimensions().getId() > 0) {
				projectDimensions = repositoryFinancialDimensions
						.findOne(dtoPayrollSetup.getProjectDimensions().getId());
			}

			if (dtoPayrollSetup.getPayrollCheckbook() != null && dtoPayrollSetup.getPayrollCheckbook().getId() > 0) {
				payrollCheckbook = repositoryCheckBookMaintenance
						.findOne(dtoPayrollSetup.getPayrollCheckbook().getId());
			}

			if (dtoPayrollSetup.getManualCheckCheckbook() != null
					&& dtoPayrollSetup.getManualCheckCheckbook().getId() > 0) {
				manualCheckCheckbook = repositoryCheckBookMaintenance
						.findOne(dtoPayrollSetup.getManualCheckCheckbook().getId());
			}

			if (dtoPayrollSetup.getVatSetup() != null && dtoPayrollSetup.getVatSetup().getId() > 0) {
				vatSetup = repositoryVatSetup.findOne(dtoPayrollSetup.getVatSetup().getId());
			}

			if (payrollCheckbook != null) {
				payrollSetup.setCheckNumber(payrollCheckbook.getNextCheckNumber());
			}
			if (manualCheckCheckbook != null) {
				payrollSetup.setManualcheckNumber(manualCheckCheckbook.getNextCheckNumber());
			}

			payrollSetup.setCodeswithYTDAmounts(dtoPayrollSetup.isCodeswithYTDAmounts());
			payrollSetup.setDisplayPayRate(dtoPayrollSetup.isDisplayPayRate());
			payrollSetup.setJvPerEmployee(dtoPayrollSetup.isJvPerEmployee());
			payrollSetup.setMultiPayrollProcesses(dtoPayrollSetup.isMultiPayrollProcesses());
			payrollSetup.setNextPaymentNumber(dtoPayrollSetup.getNextPaymentNumber());
			payrollSetup.setEmployeeDimensions(employeeDimensions);
			payrollSetup.setDepartmentDimensions(departmentDimensions);
			payrollSetup.setProjectDimensions(projectDimensions);
			payrollSetup.setPayrollCheckbook(payrollCheckbook);
			payrollSetup.setManualCheckCheckbook(manualCheckCheckbook);
			payrollSetup.setVatSetup(vatSetup);
			repositoryPayrollSetup.saveAndFlush(payrollSetup);
			dtoPayrollSetup.setId(payrollSetup.getId());
			log.debug("PayrollSetup is:" + payrollSetup.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayrollSetup;

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {

		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();
			String condition = "";

			if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
				if (dtoSearch.getSortOn().equals("payCodeId")) {
					condition = dtoSearch.getSortOn();
				} else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			} else {
				condition += "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}

			dtoSearch.setTotalCount(
					this.repositoryPayrollSetup.predictivePayrollSearchTotalCount("%" + searchWord + "%"));
			List<PayrollSetup> payrollSetupList = null;
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

				if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
					payrollSetupList = this.repositoryPayrollSetup.predictivePayrollSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, "id"));
				}
				if (dtoSearch.getSortBy().equals("ASC")) {
					payrollSetupList = this.repositoryPayrollSetup.predictivePayrollSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.ASC, condition));
				} else if (dtoSearch.getSortBy().equals("DESC")) {
					payrollSetupList = this.repositoryPayrollSetup.predictivePayrollSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, condition));
				}
			}

			List<DtoPayrollSetup> dtoPayrollSetupList = new ArrayList<>();
			for (PayrollSetup payrollSetup : payrollSetupList) {
				DtoPayrollSetup dtoPayrollSetup = new DtoPayrollSetup();
				dtoPayrollSetup = getDtoPayrollSetup(dtoPayrollSetup,payrollSetup);

				dtoPayrollSetupList.add(dtoPayrollSetup);
			}

			dtoSearch.setRecords(dtoPayrollSetupList);

		}

		return dtoSearch;
	}

	public DtoPayrollSetup getDtoPayrollSetup(DtoPayrollSetup dtoPayrollSetup,PayrollSetup payrollSetup) {
		
		dtoPayrollSetup.setId(payrollSetup.getId());
		dtoPayrollSetup.setNextPaymentNumber(payrollSetup.getNextPaymentNumber());
		dtoPayrollSetup.setCheckNumber(payrollSetup.getCheckNumber());
		dtoPayrollSetup.setMultiPayrollProcesses(payrollSetup.isMultiPayrollProcesses());
		dtoPayrollSetup.setJvPerEmployee(payrollSetup.isJvPerEmployee());
		dtoPayrollSetup.setDisplayPayRate(payrollSetup.isDisplayPayRate());
		dtoPayrollSetup.setCodeswithYTDAmounts(payrollSetup.isCodeswithYTDAmounts());

		if (payrollSetup.getDepartmentDimensions() != null) {
			DtoFinancialDimensions dimensions = new DtoFinancialDimensions();
			dimensions.setId(payrollSetup.getDepartmentDimensions().getId());
			dimensions.setDimensionColumnName(payrollSetup.getDepartmentDimensions().getDimensionColumnName());
			dimensions
					.setDimensionDescription(payrollSetup.getDepartmentDimensions().getDimensionDescription());
			dimensions.setDimensionArbicDescription(
					payrollSetup.getDepartmentDimensions().getDimensionArbicDescription());
			dtoPayrollSetup.setDepartmentDimensions(dimensions);
		}

		if (payrollSetup.getProjectDimensions() != null) {
			DtoFinancialDimensions dimensions = new DtoFinancialDimensions();
			dimensions.setId(payrollSetup.getProjectDimensions().getId());
			dimensions.setDimensionColumnName(payrollSetup.getProjectDimensions().getDimensionColumnName());
			dimensions.setDimensionDescription(payrollSetup.getProjectDimensions().getDimensionDescription());
			dimensions.setDimensionArbicDescription(
					payrollSetup.getProjectDimensions().getDimensionArbicDescription());
			dtoPayrollSetup.setProjectDimensions(dimensions);
		}

		if (payrollSetup.getEmployeeDimensions() != null) {
			DtoFinancialDimensions dimensions = new DtoFinancialDimensions();
			dimensions.setId(payrollSetup.getEmployeeDimensions().getId());
			dimensions.setDimensionColumnName(payrollSetup.getEmployeeDimensions().getDimensionColumnName());
			dimensions.setDimensionDescription(payrollSetup.getEmployeeDimensions().getDimensionDescription());
			dimensions.setDimensionArbicDescription(
					payrollSetup.getEmployeeDimensions().getDimensionArbicDescription());
			dtoPayrollSetup.setEmployeeDimensions(dimensions);
		}

		if (payrollSetup.getManualCheckCheckbook() != null) {
			DtoCheckbookMaintenance manualCheckCheckbook = new DtoCheckbookMaintenance();
			manualCheckCheckbook.setId(payrollSetup.getManualCheckCheckbook().getId());
			manualCheckCheckbook.setCheckbookId(payrollSetup.getManualCheckCheckbook().getCheckbookId());
			manualCheckCheckbook
					.setNextCheckNumber(payrollSetup.getManualCheckCheckbook().getNextCheckNumber());
			manualCheckCheckbook
					.setNextDepositNumber(payrollSetup.getManualCheckCheckbook().getNextDepositNumber());
			manualCheckCheckbook
					.setCheckbookDescription(payrollSetup.getManualCheckCheckbook().getCheckbookDescription());
			dtoPayrollSetup.setManualCheckCheckbook(manualCheckCheckbook);
		}

		if (payrollSetup.getPayrollCheckbook() != null) {
			DtoCheckbookMaintenance payrollCheckbook = new DtoCheckbookMaintenance();
			payrollCheckbook.setId(payrollSetup.getPayrollCheckbook().getId());
			payrollCheckbook.setCheckbookId(payrollSetup.getPayrollCheckbook().getCheckbookId());
			payrollCheckbook.setNextCheckNumber(payrollSetup.getPayrollCheckbook().getNextCheckNumber());
			payrollCheckbook.setNextDepositNumber(payrollSetup.getPayrollCheckbook().getNextDepositNumber());
			payrollCheckbook
					.setCheckbookDescription(payrollSetup.getPayrollCheckbook().getCheckbookDescription());
			dtoPayrollSetup.setPayrollCheckbook(payrollCheckbook);
		}

		if (payrollSetup.getVatSetup() != null) {
			DtoVatSetup dtoVatSetup = new DtoVatSetup();
			dtoVatSetup.setId(payrollSetup.getVatSetup().getId());
			dtoVatSetup.setVatScheduleId(payrollSetup.getVatSetup().getVatScheduleId());
			dtoVatSetup.setVatDescription(payrollSetup.getVatSetup().getVatDescription());
			dtoVatSetup.setVatDescriptionArabic(payrollSetup.getVatSetup().getVatDescriptionArabic());
			dtoPayrollSetup.setVatSetup(dtoVatSetup);
		}
		return dtoPayrollSetup;
	}
	
	
	
	
	public DtoPayrollSetup delete(List<Integer> ids) {
		log.info("delete Method");
		DtoPayrollSetup dtoPayrollSetup = new DtoPayrollSetup();
		dtoPayrollSetup
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAYROLL_SETUP_DELETED", false));
		dtoPayrollSetup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("PAYROLL_SETUP_DELETED", false));
		List<DtoPayrollSetup> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(
				serviceResponse.getMessageByShortAndIsDeleted("POSTION_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer positionId : ids) {
				PayrollSetup payrollSetup = repositoryPayrollSetup.findOne(positionId);
				if(payrollSetup!=null) {
					DtoPayrollSetup dtoskillSetup2 = new DtoPayrollSetup();
					dtoskillSetup2.setId(positionId);
					repositoryPayrollSetup.deleteSinglePayrollSetup(true, loggedInUserId, positionId);
					deletePosition.add(dtoskillSetup2);
				}else {
					inValidDelete = true;
				}
				

			}
			if (inValidDelete) {
				dtoPayrollSetup.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoPayrollSetup.setMessageType("");
			}
			dtoPayrollSetup.setDelete(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete PayrollSetup :" + dtoPayrollSetup.getId());
		return dtoPayrollSetup;
	}

	public DtoPayrollSetup getById(Integer id) {
		PayrollSetup payrollSetup = repositoryPayrollSetup.findOne(id);
		DtoPayrollSetup dtoPayrollSetup = new DtoPayrollSetup();
		dtoPayrollSetup = getDtoPayrollSetup(dtoPayrollSetup,payrollSetup);
		return dtoPayrollSetup;
	}

	public DtoSearch getAllDroupdownDimension(DtoSearch dtoSearch) {
		log.info("search Method");
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();

			List<FinancialDimensions> financialDimensions = null;
			financialDimensions = this.repositoryFinancialDimensions.getAllDroupdown("%" + searchWord + "%");

			if (financialDimensions != null && !financialDimensions.isEmpty()) {
				List<DtoFinancialDimensions> dtoCheckbookMaintenanceLists = new ArrayList<>();
				DtoFinancialDimensions dtoFinancialDimensions = null;
				for (FinancialDimensions dimensions : financialDimensions) {
					dtoFinancialDimensions = new DtoFinancialDimensions();
					dtoFinancialDimensions.setId(dimensions.getId());
					dtoFinancialDimensions.setDimensionDescription(dimensions.getDimensionDescription());
					dtoCheckbookMaintenanceLists.add(dtoFinancialDimensions);
				}
				dtoSearch.setRecords(dtoCheckbookMaintenanceLists);
			}
		}
		return dtoSearch;
	}

}
