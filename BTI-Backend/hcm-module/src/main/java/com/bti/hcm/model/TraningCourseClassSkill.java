package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40821",indexes = {
        @Index(columnList = "TRNSKINDX")
})
public class TraningCourseClassSkill extends HcmBaseEntity implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRNSKINDX")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "TRNCINDX")
	private TraningCourse trainingCourse;
	
	@ManyToOne
	@JoinColumn(name = "TRNCCLSINDX")
	private TrainingCourseDetail trainingClass;
	
	@ManyToOne
	@JoinColumn(name = "SKLSTINDX")
	private SkillSetSetup skillSetSetup;
	
	@Column(name = "SKLSEQNC")
	private Integer skillSequnce;
	
	@ManyToMany
	@JoinColumn
	private List<SkillsSetup> skillsSetup;
	
	@Column(name = "SKLDSCR")
	private String skillDesc;
	
	@Column(name = "SKLDSCRA")
	private String skillArabicDesc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public TraningCourse getTrainingCourse() {
		return trainingCourse;
	}

	public void setTrainingCourse(TraningCourse trainingCourse) {
		this.trainingCourse = trainingCourse;
	}

	public TrainingCourseDetail getTrainingClass() {
		return trainingClass;
	}

	public void setTrainingClass(TrainingCourseDetail trainingClass) {
		this.trainingClass = trainingClass;
	}

	

	

	public SkillSetSetup getSkillSetSetup() {
		return skillSetSetup;
	}

	public void setSkillSetSetup(SkillSetSetup skillSetSetup) {
		this.skillSetSetup = skillSetSetup;
	}

	public List<SkillsSetup> getSkillsSetup() {
		return skillsSetup;
	}

	public void setSkillsSetup(List<SkillsSetup> skillsSetup) {
		this.skillsSetup = skillsSetup;
	}

	public Integer getSkillSequnce() {
		return skillSequnce;
	}

	public void setSkillSequnce(Integer skillSequnce) {
		this.skillSequnce = skillSequnce;
	}


	
	public String getSkillDesc() {
		return skillDesc;
	}

	

	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}

	public String getSkillArabicDesc() {
		return skillArabicDesc;
	}

	public void setSkillArabicDesc(String skillArabicDesc) {
		this.skillArabicDesc = skillArabicDesc;
	}

	
}
