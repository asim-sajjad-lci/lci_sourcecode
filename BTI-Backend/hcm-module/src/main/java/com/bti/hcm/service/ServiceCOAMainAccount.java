package com.bti.hcm.service;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.COAMainAccounts;
import com.bti.hcm.model.dto.DtoCOAMainAccounts;
import com.bti.hcm.repository.RepositoryCOAMainAccount;


@Service("serviceCoaMainAccount")
public class ServiceCOAMainAccount {
	
static Logger log = Logger.getLogger(ServiceCOAMainAccount.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryCOAMainAccount repositoryCOAMainAccount;
	
	public List<DtoCOAMainAccounts> getAllData(){
		
			List<COAMainAccounts> acctList = repositoryCOAMainAccount.getAllData();
			List<DtoCOAMainAccounts> mainList = new ArrayList<>();
		if(acctList!=null && !acctList.isEmpty()) {
			DtoCOAMainAccounts dtomainAcct = null;
			for (COAMainAccounts coaMain : acctList) {
				dtomainAcct = new DtoCOAMainAccounts();
				dtomainAcct.setId(coaMain.getId());
				dtomainAcct.setAccountType(coaMain.getAccountType());
				dtomainAcct.setMainAccNumber(coaMain.getMainAccNumber());
				dtomainAcct.setMainAccDescription(coaMain.getMainAccDescription());
				dtomainAcct.setMainAccArabicDescription(coaMain.getMainAccArabicDescription());
				mainList.add(dtomainAcct);
			}
		}
	return mainList;
	}

}
