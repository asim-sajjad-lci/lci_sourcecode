/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.CheckbookMaintenance;
import com.bti.hcm.model.dto.DtoCheckbookMaintenance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceCheckbookMaintenance;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;


/**
 * Description: Controller Price Level Setup
 * Name of Project: Ims 
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/checkbookMaintenance")
public class ControllerCheckbookMaintenance {

	@Autowired
	ServiceCheckbookMaintenance serviceCheckbookMaintenance;

	@Autowired
	ServiceResponse response;

	private static final Logger LOGGER = Logger.getLogger(ControllerCheckbookMaintenance.class);

	@Autowired
	ServiceHcmHome serviceHome;
	
	
	/**
	 * @param request{
	"attribute3":"Gopal",
	"attribute2":"test",
	"attribute1":"test",
	"lotCategory":"test"
}
	 * @param dtoMaterialsSetupBill
	 * @return
	 * @return{
    "code": 201,
    "status": "CREATED",
    "result": {
        "id": null,
        "lotCategory": "test",
        "pageNumber": null,
        "pageSize": null,
        "ids": null,
        "isActive": null,
        "messageType": null,
        "message": null,
        "deleteMessage": null,
        "associateMessage": null,
        "delete": null,
        "isRepeat": null,
        "attribute2": "test",
        "attribute3": "Gopal",
        "attribute4": null,
        "attribute5": null,
        "attribute1": "test"
    },
    "btiMessage": {
        "message": "N/A",
        "messageShort": "N/A"
    }
}
	 * 
	 * 
	 * 
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createMaterialsSetupBill(HttpServletRequest request,@RequestBody DtoCheckbookMaintenance dtoCheckbookMaintenance) throws Exception{
		LOGGER.info("Create createMaterialsSetupBill Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHome.checkValidCompanyAccess();
		if (flag) {
			
			boolean check =serviceCheckbookMaintenance.uniqueCheck(dtoCheckbookMaintenance.getCheckbookId());
			if(check) {
				dtoCheckbookMaintenance = serviceCheckbookMaintenance.saveOrUpdate(dtoCheckbookMaintenance);
				if (dtoCheckbookMaintenance != null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_CREATED", false), dtoCheckbookMaintenance);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_CREATED_NOT", false), dtoCheckbookMaintenance);
				}
			}else {
				dtoCheckbookMaintenance.setIsRepeat(check);
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_NOT_UNIQUE", false), dtoCheckbookMaintenance);
			}
			
			
			
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Create createMaterialsSetupBill Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	/**
	 * @param request{
	"id":1,
	"attribute3":"Gopal",
	"attribute2":"test",
	"attribute1":"test",
	"lotCategory":"test"
}
	 * @param dtoMaterialsSetupBill
	 * @return{
    "code": 201,
    "status": "CREATED",
    "result": {
        "id": 1,
        "lotCategory": "test",
        "pageNumber": null,
        "pageSize": null,
        "ids": null,
        "isActive": null,
        "messageType": null,
        "message": null,
        "deleteMessage": null,
        "associateMessage": null,
        "delete": null,
        "isRepeat": null,
        "attribute2": "test",
        "attribute3": "Gopal",
        "attribute4": null,
        "attribute5": null,
        "attribute1": "test"
    },
    "btiMessage": {
        "message": "N/A",
        "messageShort": "N/A"
    }
}
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateMaterialsSetupBill(HttpServletRequest request, @RequestBody DtoCheckbookMaintenance dtoCheckbookMaintenance) throws Exception {
		LOGGER.info("Update MaterialsSetupBill Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoCheckbookMaintenance = serviceCheckbookMaintenance.saveOrUpdate(dtoCheckbookMaintenance);
			if (dtoCheckbookMaintenance != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_UPDATED_SUCCESS", false), dtoCheckbookMaintenance);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_NOT_UPDATED", false), dtoCheckbookMaintenance);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Update MaterialsSetupBill Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param request{
		  "ids" : [1]
		}
	 * @param dtoMaterialsSetupBill
	 * @return{
    "code": 201,
    "status": "CREATED",
    "result": {
        "id": null,
        "nextDocumentNumber": null,
        "userDefineField1": null,
        "userDefineField2": null,
        "userDefineField3": null,
        "userDefineField4": null,
        "allowOverrideOnQuantityShortage": false,
        "pageNumber": null,
        "pageSize": null,
        "ids": null,
        "isActive": null,
        "messageType": null,
        "message": null,
        "deleteMessage": "Miscellaneous benefit deleted successfully.",
        "associateMessage": "Miscellaneous benefit deleted successfully.",
        "delete": [
            {
                "id": 2,
                "nextDocumentNumber": null,
                "userDefineField1": null,
                "userDefineField2": null,
                "userDefineField3": null,
                "userDefineField4": null,
                "allowOverrideOnQuantityShortage": false,
                "pageNumber": null,
                "pageSize": null,
                "ids": null,
                "isActive": null,
                "messageType": null,
                "message": null,
                "deleteMessage": null,
                "associateMessage": null,
                "delete": null,
                "isRepeat": null,
                "allowLinkingofComponentandFinishedGoodSerialandLotNumbers": false,
                "benefitsId": 0
            }
        ],
        "isRepeat": null,
        "allowLinkingofComponentandFinishedGoodSerialandLotNumbers": false,
        "benefitsId": 0
    },
    "btiMessage": {
        "message": "N/A",
        "messageShort": "N/A"
    }
}
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteMaterialsSetupBill(HttpServletRequest request, @RequestBody DtoCheckbookMaintenance dtoCheckbookMaintenance) throws Exception {
		LOGGER.info("Delete MaterialsSetupBill Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoCheckbookMaintenance.getIds() != null && !dtoCheckbookMaintenance.getIds().isEmpty()) {
				DtoCheckbookMaintenance dtoPosition1 = serviceCheckbookMaintenance.deleteCheckbookMaintenance(dtoCheckbookMaintenance.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_DELETED", false), dtoPosition1);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete deleteMaterialsSetupBill Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param dtoSearch
				  {
					"searchKeyword":"",
					"sortOn":"",
					"sortBy":"",
					"pageNumber":"0",
					"pageSize":"10"
				}
	 * @param request
	 * @return{
    "code": 200,
    "status": "OK",
    "result": {
        "searchKeyword": "",
        "pageNumber": 0,
        "pageSize": 10,
        "sortOn": "",
        "sortBy": "",
        "totalCount": 2,
        "records": [
            {
                "id": 4,
                "lotCategory": "test",
                "pageNumber": null,
                "pageSize": null,
                "ids": null,
                "isActive": null,
                "messageType": null,
                "message": null,
                "deleteMessage": null,
                "associateMessage": null,
                "delete": null,
                "isRepeat": null,
                "attribute1": "test",
                "attribute2": "test",
                "attribute3": "test",
                "attribute4": null,
                "attribute5": null
            },
            {
                "id": 3,
                "lotCategory": "test",
                "pageNumber": null,
                "pageSize": null,
                "ids": null,
                "isActive": null,
                "messageType": null,
                "message": null,
                "deleteMessage": null,
                "associateMessage": null,
                "delete": null,
                "isRepeat": null,
                "attribute1": null,
                "attribute2": null,
                "attribute3": null,
                "attribute4": null,
                "attribute5": null
            }
        ]
    },
    "btiMessage": {
        "message": "Miscellaneous benefit fetched successfully.",
        "messageShort": "MISCELLANEOUS_BENEFIT_GET_ALL"
    }
}
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		ResponseMessage responseMessage = null;
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceCheckbookMaintenance.search(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.response.getMessageByShortAndIsDeleted(MessageConstant.PRICE_LEVEL_SETUP_GET_ALL, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						response.getMessageByShortAndIsDeleted(MessageConstant.PRICE_LEVEL_SETUP_NOT_GETTING, false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}

	
	/**
	 * @param request{
		  "id" : "1"
		}
	 * @param dtoMaterialsSetupBill
	 * @return{
    "code": 302,
    "status": "FOUND",
    "result": {
        "id": 4,
        "lotCategory": "test",
        "pageNumber": null,
        "pageSize": null,
        "ids": null,
        "isActive": null,
        "messageType": null,
        "message": null,
        "deleteMessage": null,
        "associateMessage": null,
        "delete": null,
        "isRepeat": true,
        "attribute1": "test",
        "attribute2": "test",
        "attribute3": "test",
        "attribute4": null,
        "attribute5": null
    },
    "btiMessage": {
        "message": "Miscellaneous benefit Id Alrady Exists.",
        "messageShort": "MISCELLANEOUS_BENEFIT_DETAIL_NOT_FATECHED"
    }
}V
	 * @throws Exception
	 */
	@RequestMapping(value = "/ItemLotCategorySetupcheck", method = RequestMethod.POST)
	public ResponseMessage miscellaneousBenefitscheck(HttpServletRequest request, @RequestBody DtoCheckbookMaintenance dtoCheckbookMaintenance) throws Exception {
		LOGGER.info("MaterialsSetupBill ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			if(dtoCheckbookMaintenance.getId() == null) {
				dtoCheckbookMaintenance.setId(0);
			}
			DtoCheckbookMaintenance dtopPositionObj = serviceCheckbookMaintenance.repeatByCompanyId(dtoCheckbookMaintenance.getId());
			if (dtopPositionObj != null) {
				if (dtopPositionObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_NOT_FATECHED", false), dtopPositionObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted(dtopPositionObj.getMessageType(), false),
							dtopPositionObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_NOT_FOUND", false), dtopPositionObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllData", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllData(HttpServletRequest request) throws Exception {
		LOGGER.info("Search MaterialsSetupBill Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			List<CheckbookMaintenance> dtoSearch = this.serviceCheckbookMaintenance.getAllData();
			if (dtoSearch != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_NOT_GETTING", false),dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllDroupdown", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllDroupdown(HttpServletRequest request,@RequestBody DtoSearch search) throws Exception {
		LOGGER.info("Search MaterialsSetupBill Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			search = this.serviceCheckbookMaintenance.getAllDroupdown(search);
			if (search != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_GET_ALL", false), search);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						response.getMessageByShortAndIsDeleted("PRICE_LEVEL_SETUP_NOT_GETTING", false),search);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Search MaterialsSetupBill Method:"+search);
		return responseMessage;
	}
}
