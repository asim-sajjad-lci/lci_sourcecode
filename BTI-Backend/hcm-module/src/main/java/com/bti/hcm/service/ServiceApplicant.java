package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Applicant;
import com.bti.hcm.model.dto.DtoApplicant;
import com.bti.hcm.repository.RepositoryApplicant;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceApplicant")
public class ServiceApplicant {
	
	
	
	
	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceApplicant.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryApplicant repositoryApplicant;


	
	
	
	
	public DtoApplicant saveOrUpdate(DtoApplicant dtoApplicant) {

		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Applicant applicant=null;
			if (dtoApplicant.getId() != null && dtoApplicant.getId() > 0) {
				applicant = repositoryApplicant.findByIdAndIsDeleted(dtoApplicant.getId(), false);
				applicant.setUpdatedBy(loggedInUserId);
				applicant.setUpdatedDate(new Date());
			} else {
				applicant = new Applicant();
				applicant.setCreatedDate(new Date());
				Integer rowId = repositoryApplicant.getCountOfTotalApplicant();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				applicant.setRowId(increment);
			}
			
			applicant.setFirstName(dtoApplicant.getFirstName());
			applicant.setMiddleName(dtoApplicant.getMiddleName());
			applicant.setLastName(dtoApplicant.getLastName());
			applicant.setAddress(dtoApplicant.getAddress());
			applicant.setCity(dtoApplicant.getCity());
			applicant.setCountry(dtoApplicant.getCountry());
			applicant.setPhone1(dtoApplicant.getPhone1());
			applicant.setPhone2(dtoApplicant.getPhone2());
			applicant.setEmail(dtoApplicant.getEmail());
			applicant.setGender(dtoApplicant.getGender());
			applicant.setAge(dtoApplicant.getAge());
			applicant.setApplicantDate(dtoApplicant.getApplicantDate());
			applicant.setStatus(dtoApplicant.getStatus());
			applicant.setRejectReason(dtoApplicant.getRejectReason());
			applicant.setRejectedComents(dtoApplicant.getRejectedComents());
			applicant.setReferalSource(dtoApplicant.getReferalSource());
			applicant.setReferalDesc(dtoApplicant.getReferalDesc());
			applicant.setColor(dtoApplicant.getColor());
			applicant.setManagerName(dtoApplicant.getManagerName());
			
		
			applicant.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryApplicant.saveAndFlush(applicant);
		} catch (Exception e) {
			log.error(e);
		}
		return dtoApplicant;
	}
	public DtoApplicant deleteApplicant(List<Integer> ids) {
			log.info("delete Applicant Method");
			DtoApplicant dtoApplicant = new DtoApplicant();
			dtoApplicant.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("APPLICANT_DELETED", false));
			dtoApplicant.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("APPLICANT_ASSOCIATED", false));
			List<DtoApplicant> deleteApplicant = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("APPLICANT_NOT_DELETE_ID_MESSAGE", false).getMessage());
			try {
				for (Integer planId : ids) {
					Applicant applicant = repositoryApplicant.findOne(planId);
		if(applicant!=null) {
			DtoApplicant dtoApplicant2=new DtoApplicant();
			dtoApplicant.setId(applicant.getId());
			dtoApplicant.setFirstName(applicant.getFirstName());
			
		     
			repositoryApplicant.deleteSingleApplicant(true, loggedInUserId, planId);
			deleteApplicant.add(dtoApplicant2);	
		
		}else {
			inValidDelete = true;
		}
						
					}

				if(inValidDelete){
					invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
					dtoApplicant.setMessageType(invlidDeleteMessage.toString());
					
				}
				if(!inValidDelete){
					dtoApplicant.setMessageType("");
					
				}
				
					
				dtoApplicant.setDelete(deleteApplicant);
			} 
			catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete Applicant :"+dtoApplicant.getId());
			return dtoApplicant;
		}

	
	public DtoApplicant getById(int id) {
		DtoApplicant dtoApplicant  = new DtoApplicant();
		try {
			if (id > 0) {
				Applicant applicant = repositoryApplicant.findByIdAndIsDeleted(id, false);
				if (applicant!= null) {
					dtoApplicant = new DtoApplicant(applicant);
					
					
					dtoApplicant.setFirstName(applicant.getFirstName());
					dtoApplicant.setMiddleName(applicant.getMiddleName());
					dtoApplicant.setLastName(applicant.getLastName());
					dtoApplicant.setAddress(applicant.getAddress());
					dtoApplicant.setCity(applicant.getCity());
					dtoApplicant.setCountry(applicant.getCountry());
					dtoApplicant.setPhone1(applicant.getPhone1());
					dtoApplicant.setPhone2(applicant.getPhone2());
					dtoApplicant.setEmail(applicant.getEmail());
					dtoApplicant.setGender(applicant.getGender());
					dtoApplicant.setAge(applicant.getAge());
					dtoApplicant.setStatus(applicant.getStatus());
					dtoApplicant.setRejectReason(applicant.getRejectReason());
					dtoApplicant.setRejectedComents(applicant.getRejectedComents());
					dtoApplicant.setReferalSource(applicant.getReferalSource());
					dtoApplicant.setReferalDesc(applicant.getReferalDesc());
					dtoApplicant.setColor(applicant.getColor());
					dtoApplicant.setManagerName(applicant.getManagerName());				
					
				} 
			} else {
				dtoApplicant.setMessageType("INVALID_ID");

			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoApplicant;
	}
	
}
