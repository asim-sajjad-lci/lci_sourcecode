package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.Miscellaneous;
import com.bti.hcm.model.MiscellaneousMaintanence;
import com.bti.hcm.model.Values;
import com.bti.hcm.model.dto.DtoMiscellaneous;
import com.bti.hcm.model.dto.DtoMiscellaneousMaintanence;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.model.dto.DtoValues;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryMiscellaneous;
import com.bti.hcm.repository.RepositoryMiscellneousMaintenance;
import com.bti.hcm.repository.RepositoryValues;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceMiscellaneousMaintenance")
public class ServiceMiscellaneousMaintenance {

	static Logger log = Logger.getLogger(ServiceMiscellaneousMaintenance.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryMiscellneousMaintenance repositoryMiscellaneousMaintenance;

	@Autowired
	RepositoryValues repositoryValues;

	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	RepositoryMiscellaneous repositoryMiscellaneous;

	public List<DtoMiscellaneousMaintanence> saveOrUpdate(
			List<DtoMiscellaneousMaintanence> dtoMiscellaneousMaintenance) {
		log.info("Miscellaneous Maintenance save method");
		try {
			DtoMiscellaneousMaintanence dtoMiscellaneousMaintanence = new DtoMiscellaneousMaintanence();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));

//			for (Iterator<DtoMiscellaneousMaintanence> iterator = dtoMiscellaneousMaintenance.iterator(); iterator
//					.hasNext();) {
//				dtoMiscellaneousMaintanence = iterator.next();
//				repositoryMiscellaneousMaintenance.deleteByEmployeeId(true, loggedInUserId,
//						dtoMiscellaneousMaintanence.getEmployeeId());
//			}
			for (Iterator<DtoMiscellaneousMaintanence> iterator = dtoMiscellaneousMaintenance.iterator(); iterator
					.hasNext();) {

				dtoMiscellaneousMaintanence = iterator.next();

				Values values = repositoryValues.findOne(dtoMiscellaneousMaintanence.getValueIndexId());
				Miscellaneous miscellaneous = repositoryMiscellaneous.findOne(dtoMiscellaneousMaintanence.getMiscId());
				EmployeeMaster employeeMaster = repositoryEmployeeMaster
						.findOne(dtoMiscellaneousMaintanence.getEmployeeId());
				MiscellaneousMaintanence miscellaneousMaintenance = null;
				if (dtoMiscellaneousMaintanence.getId() != 0 && dtoMiscellaneousMaintanence.getId() > 0) {

					miscellaneousMaintenance = repositoryMiscellaneousMaintenance
							.findByIdAndIsDeleted(dtoMiscellaneousMaintanence.getId(), false);
					miscellaneousMaintenance = repositoryMiscellaneousMaintenance
							.findOne(dtoMiscellaneousMaintanence.getId());
					miscellaneousMaintenance.setUpdatedBy(loggedInUserId);
					miscellaneousMaintenance.setUpdatedDate(new Date());
					miscellaneousMaintenance.setIsDeleted(dtoMiscellaneousMaintanence.getIsDeleted());
				} else {
					miscellaneousMaintenance = new MiscellaneousMaintanence();
					miscellaneousMaintenance.setCreatedDate(new Date());
					Integer rowId = repositoryMiscellaneousMaintenance.getCountOfTotalMiscellaneousEntries();
					Integer increment = 0;
					if (rowId != 0) {
						increment = rowId + 1;
					} else {
						increment = 1;
					}
					miscellaneousMaintenance.setRowId(increment);

				}

				miscellaneousMaintenance.setValues(values);
				miscellaneousMaintenance.setMiscellaneous(miscellaneous);
				miscellaneousMaintenance.setEmployeeMaster(employeeMaster);
				miscellaneousMaintenance.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				repositoryMiscellaneousMaintenance.saveAndFlush(miscellaneousMaintenance);
				dtoMiscellaneousMaintanence.setId(miscellaneousMaintenance.getId());
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneousMaintenance;
	}

	public DtoSearchActivity searchMiscellaneousEntryWithValues(DtoSearchActivity dtoSearchActivity) {
		try {
			if (dtoSearchActivity != null) {
				dtoSearchActivity = searchByMiscellaneuosEntry(dtoSearchActivity);

				dtoSearchActivity.setPageNumber(dtoSearchActivity.getPageNumber()); // ME
				dtoSearchActivity.setPageSize(dtoSearchActivity.getPageSize()); // ME

				List<Miscellaneous> miscellaneousList = searchByPageSizeAndNumber(dtoSearchActivity);
				// dtoSearchActivity.setTotalCount(miscellaneousList.size());
				//List<Miscellaneous> miscellaneousList = repositoryMiscellaneous.predictiveMiscellaneousAll();

				//Integer totalCount = repositoryMiscellaneousMaintenance.countByEmployeeId(dtoSearchActivity.getEmployeeId());

				Integer totalCount = repositoryMiscellaneous.countPredictiveMiscellaneousAll();
				log.info("totalCount:" + totalCount);
				dtoSearchActivity.setTotalCount(totalCount);

				List<MiscellaneousMaintanence> miscellaneousMaintenanceList = null;
				if (dtoSearchActivity.getPageNumber() == null && dtoSearchActivity.getPageSize() == null) {
					Pageable pageable = new PageRequest(dtoSearchActivity.getPageNumber(),
							dtoSearchActivity.getPageSize(), Direction.DESC, "createdDate");
					miscellaneousMaintenanceList = repositoryMiscellaneousMaintenance
							.findByEmployeeIdPageable(dtoSearchActivity.getEmployeeId(), pageable);
				} else {
					// findByIsDeletedOrderByCreatedDateDesc()
					miscellaneousMaintenanceList = repositoryMiscellaneousMaintenance
							.findByEmployeeId(dtoSearchActivity.getEmployeeId());
				}

				if (miscellaneousList != null && !miscellaneousList.isEmpty()) {
					List<DtoMiscellaneous> dtoMiscellaneousList = new ArrayList<>();
					DtoMiscellaneous dtoMiscellaneous = null;
					for (Miscellaneous miscellaneous : miscellaneousList) {
						dtoMiscellaneous = new DtoMiscellaneous();

						for (MiscellaneousMaintanence miscMain : miscellaneousMaintenanceList) {
							if (dtoSearchActivity.getEmployeeId()
									.equals(miscMain.getEmployeeMaster().getEmployeeIndexId())
									&& miscellaneous.getId() == miscMain.getMiscellaneous().getId()) {
								dtoMiscellaneous.setMasterId(miscMain.getId());
								dtoMiscellaneous.setValueIndexId(miscMain.getValues().getId());

							}
						}
						dtoMiscellaneous.setId(miscellaneous.getId());
						dtoMiscellaneous.setCodeArabicName(miscellaneous.getCodeArabicName());
						dtoMiscellaneous.setCodeId(miscellaneous.getCodeId());
						dtoMiscellaneous.setCodeName(miscellaneous.getCodeName());
						dtoMiscellaneous.setDimension(miscellaneous.isDimension());

						List<Values> valuesList = repositoryValues.findAllMiscId(miscellaneous.getId());
						if (valuesList != null && !valuesList.isEmpty()) {
							List<DtoValues> dtoValuesList = new ArrayList<>();
							for (Values values : valuesList) {
								DtoValues dtoValues = new DtoValues();
								dtoValues.setId(values.getId());
								dtoValues.setValueId(values.getValueId());
								dtoValues.setMiscId(values.getMiscellaneous().getId());
								dtoValuesList.add(dtoValues);
							}
							dtoMiscellaneous.setDtoValuesList(dtoValuesList);

						}

						dtoMiscellaneousList.add(dtoMiscellaneous);

					}
					dtoSearchActivity.setRecords(dtoMiscellaneousList);
				}

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearchActivity;
	}

	public List<Miscellaneous> searchByPageSizeAndNumber(DtoSearch dtoSearch) {
		List<Miscellaneous> miscellaneousList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

			if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
				miscellaneousList = this.repositoryMiscellaneous.predictiveMiscellaneousAllWithPagination(
						new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if (dtoSearch.getSortBy().equals("ASC")) {
				miscellaneousList = this.repositoryMiscellaneous
						.predictiveMiscellaneousAllWithPagination(new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
			} else if (dtoSearch.getSortBy().equals("DESC")) {
				miscellaneousList = this.repositoryMiscellaneous
						.predictiveMiscellaneousAllWithPagination(new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
			}

		}

		return miscellaneousList;
	}

	public DtoSearchActivity searchByMiscellaneuosEntry(DtoSearchActivity dtoSearchActivity) {

		String condition = "";

		if (dtoSearchActivity.getSortOn() != null || dtoSearchActivity.getSortBy() != null) {
			switch (dtoSearchActivity.getSortOn()) {
			case "valueId":
				condition += dtoSearchActivity.getSortOn();
				break;
			case "miscId":
				condition += dtoSearchActivity.getSortOn();
				break;
			case "employeeId":
				condition += dtoSearchActivity.getSortOn();
				break;
			default:
				condition += "id";
			}
		} else {
			condition += "id";
			dtoSearchActivity.setSortOn("");
			dtoSearchActivity.setSortBy("");
		}
		dtoSearchActivity.setCondition(condition);
		return dtoSearchActivity;

	}

}
