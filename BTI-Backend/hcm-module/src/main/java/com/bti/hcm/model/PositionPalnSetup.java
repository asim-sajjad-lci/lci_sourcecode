package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40133",indexes = {
        @Index(columnList = "POTPLINDX")
})
public class PositionPalnSetup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POTPLINDX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "POTINDX")
	private Position position;

	@Column(name = "POTPLID", columnDefinition = "char(15)")
	private String positionPlanId;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "positionPalnSetup")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<PositionPayCode> positionPayCode;

	@Column(name = "POTPLDSCR", columnDefinition = "char(31)")
	private String positionPlanDesc;

	@Column(name = "POTPLDSCRA", columnDefinition = "char(61)")
	private String positionPlanArbicDesc;

	@Column(name = "POTPLSTDT")
	private Date positionPlanStart;

	@Column(name = "POTPLEDDT")
	private Date positionPlanEnd;

	@Column(name = "POTPLINCT")
	private boolean inactive;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String getPositionPlanId() {
		return positionPlanId;
	}

	public void setPositionPlanId(String positionPlanId) {
		this.positionPlanId = positionPlanId;
	}

	public String getPositionPlanDesc() {
		return positionPlanDesc;
	}

	public void setPositionPlanDesc(String positionPlanDesc) {
		this.positionPlanDesc = positionPlanDesc;
	}

	public String getPositionPlanArbicDesc() {
		return positionPlanArbicDesc;
	}

	public void setPositionPlanArbicDesc(String positionPlanArbicDesc) {
		this.positionPlanArbicDesc = positionPlanArbicDesc;
	}

	public Date getPositionPlanStart() {
		return positionPlanStart;
	}

	public void setPositionPlanStart(Date positionPlanStart) {
		this.positionPlanStart = positionPlanStart;
	}

	public Date getPositionPlanEnd() {
		return positionPlanEnd;
	}

	public void setPositionPlanEnd(Date positionPlanEnd) {
		this.positionPlanEnd = positionPlanEnd;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public List<PositionPayCode> getPositionPayCode() {
		return positionPayCode;
	}

	public void setPositionPayCode(List<PositionPayCode> positionPayCode) {
		this.positionPayCode = positionPayCode;
	}

}
