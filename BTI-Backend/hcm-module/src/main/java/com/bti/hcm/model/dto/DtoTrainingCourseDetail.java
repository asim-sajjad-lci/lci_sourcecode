package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.TrainingCourseDetail;
import com.bti.hcm.util.UtilRandomKey;

public class DtoTrainingCourseDetail extends DtoBase {
	private Integer id;

	private String employeeName;

	private String employeeId;
	private String traningId;
	private Date completeDate;
	private String classId;
	private String className;
	private Date startDate;
	private Date endDate;
	private String startTime;
	private String endTime;
	private String instructorName;
	private Integer enrolled;
	private Integer maximum;
	private String classLocation;
	private List<DtoTrainingCourseDetail> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getInstructorName() {
		return instructorName;
	}

	public void setInstructorName(String instructorName) {
		this.instructorName = instructorName;
	}

	public Integer getEnrolled() {
		return enrolled;
	}

	public void setEnrolled(Integer enrolled) {
		this.enrolled = enrolled;
	}

	public Integer getMaximum() {
		return maximum;
	}

	public void setMaximum(Integer maximum) {
		this.maximum = maximum;
	}

	public String getClassLocation() {
		return classLocation;
	}

	public void setClassLocation(String classLocation) {
		this.classLocation = classLocation;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getTraningId() {
		return traningId;
	}

	public void setTraningId(String traningId) {
		this.traningId = traningId;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public DtoTrainingCourseDetail(TrainingCourseDetail trainingCourseDetail) {
		this.id = trainingCourseDetail.getId();

		if (UtilRandomKey.isNotBlank(trainingCourseDetail.getClassId())) {
			this.classId = trainingCourseDetail.getClassId();
		} else {
			this.classId = "";
		}
		if (UtilRandomKey.isNotBlank(trainingCourseDetail.getClassName())) {
			this.className = trainingCourseDetail.getClassName();
		} else {
			this.className = "";
		}
		if (UtilRandomKey.isNotBlank(trainingCourseDetail.getInstructorName())) {
			this.instructorName = trainingCourseDetail.getInstructorName();
		} else {
			this.instructorName = "";
		}

		if (UtilRandomKey.isNotBlank(trainingCourseDetail.getClassLocation())) {
			this.classLocation = trainingCourseDetail.getClassLocation();
		} else {
			this.classLocation = "";
		}
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public DtoTrainingCourseDetail() {
	}

	public List<DtoTrainingCourseDetail> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoTrainingCourseDetail> delete) {
		this.delete = delete;
	}

}
