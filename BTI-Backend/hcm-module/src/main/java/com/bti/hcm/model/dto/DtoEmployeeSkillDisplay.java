package com.bti.hcm.model.dto;

import java.util.Date;

public class DtoEmployeeSkillDisplay {

	private Integer id;
	private Integer employeeSkillId;
	private String skillId;
	private Integer employeePrimaryId;
	private Integer skillSetPrimaryId;
	private Integer skillPrimaryId;
	private String employeeId;
	private String skillSetId;
	private String comment;
	private String skillDesc;
	private boolean obtain;
	private Date date;
	private boolean require;
	private Integer proficiency;
	
	
	public Integer getEmployeeSkillId() {
		return employeeSkillId;
	}


	public void setEmployeeSkillId(Integer employeeSkillId) {
		this.employeeSkillId = employeeSkillId;
	}


	public String getSkillId() {
		return skillId;
	}


	public void setSkillId(String skillId) {
		this.skillId = skillId;
	}


	public Integer getEmployeePrimaryId() {
		return employeePrimaryId;
	}


	public void setEmployeePrimaryId(Integer employeePrimaryId) {
		this.employeePrimaryId = employeePrimaryId;
	}


	public Integer getSkillSetPrimaryId() {
		return skillSetPrimaryId;
	}


	public void setSkillSetPrimaryId(Integer skillSetPrimaryId) {
		this.skillSetPrimaryId = skillSetPrimaryId;
	}


	public Integer getSkillPrimaryId() {
		return skillPrimaryId;
	}


	public void setSkillPrimaryId(Integer skillPrimaryId) {
		this.skillPrimaryId = skillPrimaryId;
	}


	public String getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}


	public String getSkillSetId() {
		return skillSetId;
	}


	public void setSkillSetId(String skillSetId) {
		this.skillSetId = skillSetId;
	}


	public String getSkillDesc() {
		return skillDesc;
	}


	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}


	public boolean isObtain() {
		return obtain;
	}


	public void setObtain(boolean obtain) {
		this.obtain = obtain;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public boolean isRequire() {
		return require;
	}


	public void setRequire(boolean require) {
		this.require = require;
	}


	public Integer getProficiency() {
		return proficiency;
	}


	public void setProficiency(Integer proficiency) {
		this.proficiency = proficiency;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public DtoEmployeeSkillDisplay() {
		// TODO Auto-generated constructor stub
	}
}
