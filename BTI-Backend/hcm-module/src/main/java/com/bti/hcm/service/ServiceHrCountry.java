package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.dto.DtoHrCountry;
import com.bti.hcm.repository.RepositoryHrCountry;

@Service("serviceHrCountry")
public class ServiceHrCountry {
	
	static Logger log = Logger.getLogger(ServiceHrCountry.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryHrCountry repositoryHrCountry;
	
	
	public List<DtoHrCountry> getCountryListWithLanguage() {
		log.info("getCountryListWithLanguage  Method");
		String langId = httpServletRequest.getHeader("langid");
		List<DtoHrCountry> stateList = new ArrayList<>();
//		List<HrCountry> list = repositoryHrCountry.findByIsDeletedAndLanguageLanguageId(false, Integer.parseInt(langId));
		List<HrCountry> list = repositoryHrCountry.findByIsDeletedAndLanguageLanguageId(false, 1);
		if (list != null && !list.isEmpty()) {
			for (HrCountry hrState : list) {
				DtoHrCountry dtoCountry = new DtoHrCountry();
				dtoCountry.setCountryId(hrState.getCountryId());
				dtoCountry.setCountryName(hrState.getCountryName());
				dtoCountry.setCountryCode(hrState.getCountryCode());
				dtoCountry.setDefaultCountry(hrState.getDefaultCountry());
				stateList.add(dtoCountry);
				
			}
		}
		log.debug("Country is:"+stateList.size());
		return stateList;
	}
}
