package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.WorkFlowUploadFile;
@Repository("repositoryWorkFlowUploadFile")
public interface RepositoryWorkFlow extends JpaRepository<WorkFlowUploadFile, Integer> {
	
	public List<WorkFlowUploadFile> findByIsDeleted(Boolean isDeleted);
	
	public WorkFlowUploadFile findByworkFlowIdAndIsDeleted(int id, boolean deleted);

	@Query("select count(*) from WorkFlowUploadFile w where (w.workFlowId like :searchKeyword) and w.isDeleted=false")
	public Integer predictiveSearchTotalCount(@Param("searchKeyword")String searchKeyword);

	@Query("select w from WorkFlowUploadFile w where (w.workFlowId like :searchKeyword) and w.isDeleted=false")
	public List<WorkFlowUploadFile> predictiveSearchWithPaginations(@Param("searchKeyword")String searchKeyword, Pageable pageable);
}
