package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPositionCourseDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePositionCourseDetail;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/positionCourseDetail")
public class ControllerPositionCourseDetail extends BaseController{

	@Autowired
	ServicePositionCourseDetail servicePositionCourseDetail;

	@Autowired
	ServiceResponse response;

	private static final Logger LOGGER = Logger.getLogger(ControllerPositionCourseDetail.class);

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @param request{
	 *            "sequence" : 1, "positionCourseId":1, "traningCourseId": 1,
	 *            "pageNumber": "0", "pageSize": "10"
	 * 
	 *            }
	 * @param dtoPositionCourseDetail
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,
			@RequestBody DtoPositionCourseDetail dtoPositionCourseDetail) throws Exception {
		LOGGER.info("Create Postion Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionCourseDetail = servicePositionCourseDetail.saveOrUpdateCourseDetail(dtoPositionCourseDetail);
			responseMessage=displayMessage(dtoPositionCourseDetail, "COURSE_DETAL_CREATED", "COURSE_DETAL_NOT_CREATED", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		LOGGER.debug("Create Postion Method:" + responseMessage.getMessage());
		return responseMessage;

	}

	/**
	 * @param request{
	 * 
	 *            "pageNumber": "0", "pageSize": "10"
	 * 
	 *            }
	 * @param dtoPositionCourseDetail
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request,
			@RequestBody DtoPositionCourseDetail dtoPositionCourseDetail) throws Exception {
		LOGGER.info("Get All PositionCourseDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePositionCourseDetail.getAll(dtoPositionCourseDetail);
			responseMessage=displayMessage(dtoSearch, "COURSE_DETAL_GET_ALL", "COURSE_DETAL_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get All PositionCourseDetail Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param request{
	 *            "id" : 1, "sequence" : 9, "positionCourseId":1, "traningCourseId":
	 *            1, "pageNumber": "0", "pageSize": "10"
	 * 
	 *            }
	 * @param dtoPositionCourseDetail
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePosition(HttpServletRequest request,
			@RequestBody DtoPositionCourseDetail dtoPositionCourseDetail) throws Exception {
		LOGGER.info("Update Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionCourseDetail = servicePositionCourseDetail.saveOrUpdateCourseDetail(dtoPositionCourseDetail);
			responseMessage=displayMessage(dtoPositionCourseDetail, "COURSE_DETAL_UPDATED_SUCCESS", "COURSE_DETAL_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update Position Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request,
			@RequestBody DtoPositionCourseDetail dtoPositionCourseDetail) throws Exception {
		LOGGER.info("Delete Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPositionCourseDetail.getIds() != null && !dtoPositionCourseDetail.getIds().isEmpty()) {
				DtoPositionCourseDetail dtoPosition1 = servicePositionCourseDetail
						.delete(dtoPositionCourseDetail.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("COURSE_DETAL_DELETED", false), dtoPosition1);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Position Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param request{
	 * 
	 *            "id": 1 }
	 * @param dtoPositionCourseDetail
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPositionId", method = RequestMethod.POST)
	public ResponseMessage getPositionCourseDetailById(HttpServletRequest request,
			@RequestBody DtoPositionCourseDetail dtoPositionCourseDetail) throws Exception {
		LOGGER.info("Get Position ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPositionCourseDetail dtoPositionObj = servicePositionCourseDetail.getByPositionId(dtoPositionCourseDetail.getId());
			responseMessage=displayMessage(dtoPositionObj, "COURSE_DETAL_GET_DETAIL", "COURSE_DETAL_NOT_GETTING", response);
			
			if (dtoPositionObj != null) {
				if (dtoPositionObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("COURSE_DETAL_GET_DETAIL", false), dtoPositionObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted(dtoPositionObj.getMessageType(), false),
							dtoPositionObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("COURSE_DETAL_NOT_GETTING", false), dtoPositionObj);
			}
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get Position by Id Method:" + dtoPositionCourseDetail.getId());
		return responseMessage;
	}

	/**
	 * @param dtoSearch
	 * @param request{
	 * 
	 *            "searchKeyword":"t", "pageNumber" : 0, "pageSize" :2 }
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPosition(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("Search searchPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionCourseDetail.search(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.response.getMessageByShortAndIsDeleted("COURSE_DETAL_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						response.getMessageByShortAndIsDeleted("COURSE_DETAL_NOT_GETTING", false), dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search searchPosition Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
}
