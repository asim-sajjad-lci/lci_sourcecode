package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.SalaryMatrixRowValue;

@Repository("repositorySalaryMatrixRowValue")
public interface RepositorySalaryMatrixRowValue extends JpaRepository<SalaryMatrixRowValue, Integer>{

	SalaryMatrixRowValue findByIdAndIsDeleted(Integer id, boolean b);

	List<SalaryMatrixRowValue> findByIsDeleted(boolean b, Pageable pageable);

	List<SalaryMatrixRowValue> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select d from SalaryMatrixRowValue d where (d.salaryMatrixRow.desc like :searchKeyWord  or d.salaryMatrixRow.arbic like :searchKeyWord ) and d.isDeleted=false")
	List<SalaryMatrixRowValue> predictiveSalaryMatrixRowValueSearchWithPagination(String string);

	@Query("select count(*) from SalaryMatrixRowValue d where d.isDeleted=false")
	Integer getCountOfTotalSalaryMatrixRow();

	@Query("select count(*) from SalaryMatrixRowValue d where (d.salaryMatrixRow.desc like :searchKeyWord  or d.salaryMatrixRow.arbic like :searchKeyWord ) and d.isDeleted=false")
	Integer predictiveSalaryMatrixSetupSearchTotalCount(String string);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update SalaryMatrixRowValue d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleSalaryMatrixSetupRowValue(boolean b, int loggedInUserId, Integer id);

	@Query("select s from SalaryMatrixRowValue s where (s.salaryMatrixRow.id =:id)")
	public List<SalaryMatrixRowValue> findBySalaryMatrixRowId(@Param("id")Integer id);

	@Query("select count(*) from SalaryMatrixRowValue r ")
	public Integer getCountOfTotaSalaryMatrixRowValue();
	
}
