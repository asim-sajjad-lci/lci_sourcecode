package com.bti.hcm.model.dto;

import com.bti.hcm.model.Employee;
import com.bti.hcm.util.UtilRandomKey;

public class DtoEmployee {

	private int id;
	private String firstName;
	private String lastName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public DtoEmployee() {
		
	}
	
	public DtoEmployee(Employee employee) {
		this.id = employee.getId();
		if (UtilRandomKey.isNotBlank(employee.getFirstName())) {
			this.firstName = employee.getFirstName();
		} else {
			this.firstName = "";
		}
		
		if (UtilRandomKey.isNotBlank(employee.getLastName())) {
			this.lastName = employee.getLastName();
		} else {
			this.lastName = "";
		}
	}

}
