package com.bti.hcm.model.dto;

import java.sql.Date;
import java.util.List;

import com.bti.hcm.model.ProjectSetup;
import com.bti.hcm.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoProjectSetup extends DtoBase{

	private Integer id;
	private String projectId;
	private String projectName;
	private String projectArabicName;
	private String projectDescription;
	private String projectArabicDescription;
	private Date startDate;
	private Date endDate;
	List<DtoProjectSetup> deleteProjectSetup;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectArabicName() {
		return projectArabicName;
	}
	public void setProjectArabicName(String projectArabicName) {
		this.projectArabicName = projectArabicName;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public String getProjectArabicDescription() {
		return projectArabicDescription;
	}
	public void setProjectArabicDescription(String projectArabicDescription) {
		this.projectArabicDescription = projectArabicDescription;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public DtoProjectSetup() {
		
	}
	public DtoProjectSetup(ProjectSetup projectSetup) {
		this.projectId = projectSetup.getProjectId();


		if (UtilRandomKey.isNotBlank(projectSetup.getProjectName())) {
			this.projectName = projectSetup.getProjectName();
		} else {
			this.projectName = "";
		}
		
		if (UtilRandomKey.isNotBlank(projectSetup.getProjectArabicName())) {
			this.projectArabicName = projectSetup.getProjectArabicName();
		} else {
			this.projectArabicName = "";
		}

		
		if (UtilRandomKey.isNotBlank(projectSetup.getProjectDescription())) {
			this.projectDescription = projectSetup.getProjectDescription();
		} else {
			this.projectDescription = "";
		}
		
		
		if (UtilRandomKey.isNotBlank(projectSetup.getProjectArabicDescription())) {
			this.projectArabicDescription = projectSetup.getProjectArabicDescription();
		} else {
			this.projectArabicDescription = "";
		}
	
	}
	public List<DtoProjectSetup> getDeleteProjectSetup() {
		return deleteProjectSetup;
	}
	public void setDeleteProjectSetup(List<DtoProjectSetup> deleteProjectSetup) {
		this.deleteProjectSetup = deleteProjectSetup;
	}
	
	
}
