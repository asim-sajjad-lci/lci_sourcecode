/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PayScheduleSetupPosition;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO PayScheduleSetupPositi class having getter and setter for fields (POJO)
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoPayScheduleSetupPosition extends DtoBase{
	
	private Integer paySchedulePositionIndexId;
	private String positionDescription;
	private String positionDescriptionArabic;
	private Boolean payScheduleStatus;
	private Integer payScheduleSetupId;
	private Integer positionId;
	private List<DtoPayScheduleSetupPosition> deletePayScheduleSetupPosition;
	public Integer getPaySchedulePositionIndexId() {
		return paySchedulePositionIndexId;
	}
	public void setPaySchedulePositionIndexId(Integer paySchedulePositionIndexId) {
		this.paySchedulePositionIndexId = paySchedulePositionIndexId;
	}
	public String getPositionDescription() {
		return positionDescription;
	}
	public void setPositionDescription(String positionDescription) {
		this.positionDescription = positionDescription;
	}
	public String getPositionDescriptionArabic() {
		return positionDescriptionArabic;
	}
	public void setPositionDescriptionArabic(String positionDescriptionArabic) {
		this.positionDescriptionArabic = positionDescriptionArabic;
	}
	public Boolean getPayScheduleStatus() {
		return payScheduleStatus;
	}
	public void setPayScheduleStatus(Boolean payScheduleStatus) {
		this.payScheduleStatus = payScheduleStatus;
	}
	public Integer getPayScheduleSetupId() {
		return payScheduleSetupId;
	}
	public void setPayScheduleSetupId(Integer payScheduleSetupId) {
		this.payScheduleSetupId = payScheduleSetupId;
	}
	public Integer getPositionId() {
		return positionId;
	}
	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}
	

	
	public List<DtoPayScheduleSetupPosition> getDeletePayScheduleSetupPosition() {
		return deletePayScheduleSetupPosition;
	}
	public void setDeletePayScheduleSetupPosition(List<DtoPayScheduleSetupPosition> deletePayScheduleSetupPosition) {
		this.deletePayScheduleSetupPosition = deletePayScheduleSetupPosition;
	}
	public DtoPayScheduleSetupPosition() {
		
	}
	
	public DtoPayScheduleSetupPosition(PayScheduleSetupPosition payScheduleSetupPosition) {}

}
