package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40700",indexes = {
        @Index(columnList = "TERMINDX")
})
public class TerminationSetup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TERMINDX")
	private Integer id;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "terminationSetup")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<TerminationSetupDetails> listTerminationSetupDetails;

	@ManyToOne
	@JoinColumn(name = "PRCHKINDX")
	private OrientationPredefinedCheckListItem orientationPredefinedCheckListItem;

	private Integer predefinecheckListtypeId;

	@Column(name = "TERMTID", columnDefinition = "char(15)")
	private String terminationId;

	@Column(name = "TERMDSCR", columnDefinition = "char(31)")
	private String terminationDesc;

	@Column(name = "TERMDSCRA", columnDefinition = "char(61)")
	private String terminationArbicDesc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTerminationId() {
		return terminationId;
	}

	public void setTerminationId(String terminationId) {
		this.terminationId = terminationId;
	}

	public String getTerminationDesc() {
		return terminationDesc;
	}

	public void setTerminationDesc(String terminationDesc) {
		this.terminationDesc = terminationDesc;
	}

	public String getTerminationArbicDesc() {
		return terminationArbicDesc;
	}

	public void setTerminationArbicDesc(String terminationArbicDesc) {
		this.terminationArbicDesc = terminationArbicDesc;
	}

	public List<TerminationSetupDetails> getListTerminationSetupDetails() {
		return listTerminationSetupDetails;
	}

	public void setListTerminationSetupDetails(List<TerminationSetupDetails> listTerminationSetupDetails) {
		this.listTerminationSetupDetails = listTerminationSetupDetails;
	}

	public OrientationPredefinedCheckListItem getOrientationPredefinedCheckListItem() {
		return orientationPredefinedCheckListItem;
	}

	public void setOrientationPredefinedCheckListItem(
			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem) {
		this.orientationPredefinedCheckListItem = orientationPredefinedCheckListItem;
	}

	public Integer getPredefinecheckListtypeId() {
		return predefinecheckListtypeId;
	}

	public void setPredefinecheckListtypeId(Integer predefinecheckListtypeId) {
		this.predefinecheckListtypeId = predefinecheckListtypeId;
	}

}
