/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoInterviewTypeSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceInterviewTypeSetup;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerInterviewTypeSetup
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/interviewTypeSetup")
public class ControllerInterviewTypeSetup extends BaseController{
	
	
	/**
	 * @Description LOGGER use for put a logger in InterviewTypeSetup Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerInterviewTypeSetup.class);
	
	/**
	 * @Description serviceInterviewTypeSetup Autowired here using annotation of spring for use of serviceInterviewTypeSetup method in this controller
	 */
	@Autowired(required=true)
	ServiceInterviewTypeSetup serviceInterviewTypeSetup;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "interviewTypeId":"A101",
		  "interviewTypeDescription":"Testing",
		  "interviewTypeDescriptionArabic":"Testing arabic"
		}
	 * @response
	 * {
			"code": 201,
			"status": "CREATED",
			"result": {
				"interviewTypeId": "A101",
				"interviewTypeDescription": "Testing",
				"interviewTypeDescriptionArabic": "Testing arabic"
			},
		"btiMessage": {
			"message": "InterviewType created successfully.",
			"messageShort": "INTERVIEW_TYPE_SETUP_CREATED"
			}
		}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createInterviewTypeSetup(HttpServletRequest request, @RequestBody DtoInterviewTypeSetup dtoInterviewTypeSetup) throws Exception {
		LOGGER.info("Create InterviewTypeSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoInterviewTypeSetup = serviceInterviewTypeSetup.saveOrUpdateInterviewTypeSetup(dtoInterviewTypeSetup);
			responseMessage=displayMessage(dtoInterviewTypeSetup, "INTERVIEW_TYPE_SETUP_CREATED", "INTERVIEW_TYPE_SETUP_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create InterviewTypeSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "id":1,
		  "interviewTypeId":"A101",
		  "interviewTypeDescription":"Testingss",
		  "interviewTypeDescriptionArabic":"Testing arabics"
		}
	 * @response
	 * {
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": 1,
			"interviewTypeId": "A101",
			"interviewTypeDescription": "Testingss",
			"interviewTypeDescriptionArabic": "Testing arabics"
			},
		"btiMessage": {
			"message": "InterviewType updated successfully.",
			"messageShort": "INTERVIEW_TYPE_SETUPT_UPDATED_SUCCESS"
		}
	}
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateInterviewTypeSetup(HttpServletRequest request, @RequestBody DtoInterviewTypeSetup dtoInterviewTypeSetup) throws Exception {
		LOGGER.info("Update InterviewTypeSetupt Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoInterviewTypeSetup = serviceInterviewTypeSetup.saveOrUpdateInterviewTypeSetup(dtoInterviewTypeSetup);
			responseMessage=displayMessage(dtoInterviewTypeSetup, "INTERVIEW_TYPE_SETUPT_UPDATED_SUCCESS", "INTERVIEW_TYPE_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update InterviewTypeSetupt Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetup
	 * @return
	 * @throws Exception
	 * @request
		  {
			  "ids":[1]
			}
	* @response
	    {
			"code": 201,
			"status": "CREATED",
			"result": {
			"messageType": "INTERVIEW_TYPE_SETUP_DELETED",
			"deleteMessage": "InterviewType deleted successfully.",
			"associateMessage": "N/A",
			"deleteInterviewTypeSetup": [
						  {
								"id": 1,
								"interviewTypeId": "A101",
								"interviewTypeDescription": "Testingss",
								"interviewTypeDescriptionArabic": "Testing arabics",
								"interviewRange": 0
								}
						],
			},
			"btiMessage": {
				"message": "InterviewType deleted successfully.",
				"messageShort": "INTERVIEW_TYPE_SETUP_DELETED"
			}
	}
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteInterviewTypeSetup(HttpServletRequest request, @RequestBody DtoInterviewTypeSetup dtoInterviewTypeSetup) throws Exception {
		LOGGER.info("Delete InterviewTypeSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoInterviewTypeSetup.getIds() != null && dtoInterviewTypeSetup.getIds().isEmpty()) {
				DtoInterviewTypeSetup dtoInterviewTypeSetup2 = serviceInterviewTypeSetup.deleteInterviewTypeSetup(dtoInterviewTypeSetup.getIds());
				
				if(dtoInterviewTypeSetup2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("INTERVIEW_TYPE_SETUP_DELETED", false), dtoInterviewTypeSetup2);

				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoInterviewTypeSetup2.getMessageType());
					btiMessageHcm.setMessageShort("INTERVIEW_TYPE_SETUP_DELETED_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoInterviewTypeSetup2);

				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Delete InterviewTypeSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "searchKeyword":"t",
		  "sortOn":"interviewTypeDescription",
		  "sortBy":"DESC",
		 "pageNumber":"0",
		  "pageSize":"10"
		}
		@response
		{
					"code": 200,
					"status": "OK",
					"result": {
					"searchKeyword": "t",
					"pageNumber": 0,
					"pageSize": 10,
					"sortOn": "interviewTypeDescription",
					"sortBy": "DESC",
					"totalCount": 1,
					"records": [
					  {
							"id": 1,
							"interviewTypeId": "A101",
							"interviewTypeDescription": "Testingss",
							"interviewTypeDescriptionArabic": "Testing arabics",
							"interviewRange": 0
						}
					],
			},
				"btiMessage": {
					"message": "InterviewType list fetched successfully.",
					"messageShort": "INTERVIEW_TYPE_SETUP_GET_ALL"
				}
		}
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchInterviewTypeSetup(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search InterviewTypeSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceInterviewTypeSetup.searchInterviewTypeSetup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "INTERVIEW_TYPE_SETUP_GET_ALL", "INTERVIEW_TYPE_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search InterviewTypeSetup Method:"+dtoSearch.getTotalCount());	
		}
		
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetup
	 * @return
	 * @throws Exception
	 * @request
	 	{
		  "id":1
		}
	
	 * @response
	 	{
			"code": 201,
			"status": "CREATED",
			"result": {
				"interviewTypeId": "A101",
				"interviewTypeDescription": "Testingss",
				"interviewTypeDescriptionArabic": "Testing arabics"
			},
			"btiMessage": {
				"message": "IntervieType details fetched successfully",
				"messageShort": "INTERVIEW_TYPE_SETUP_GET_DETAIL"
				}
			}
	 */
	@RequestMapping(value = "/getInterviewTypeSetupById", method = RequestMethod.POST)
	public ResponseMessage getInterviewTypeSetupById(HttpServletRequest request, @RequestBody DtoInterviewTypeSetup dtoInterviewTypeSetup) throws Exception {
		LOGGER.info("Get InterviewTypeSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoInterviewTypeSetup dtoInterviewTypeSetupObj = serviceInterviewTypeSetup.repeatByDtoInterviewTypeSetupId(dtoInterviewTypeSetup.getInterviewTypeId());
			responseMessage=displayMessage(dtoInterviewTypeSetupObj, "INTERVIEW_TYPE_SETUP_GET_DETAIL", "INTERVIEW_TYPE_SETUP_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get InterviewTypeSetup ById Method:"+dtoInterviewTypeSetup.getId());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoInterviewTypeSetup
	 * @return
	 * @throws Exception
	 * @request
	   {
		  "interviewTypeId":"A101"
		}
	 * @response
	 * {
			"code": 302,
			"status": "FOUND",
			"result": {
			"isRepeat": true
			},
			"btiMessage": {
			"message": "InterviewType detail fetched successfully.",
			"messageShort": "INTERVIEW_TYPE_SETUP_RESULT"
			}
		}
	 */
	@RequestMapping(value = "/interviewTypeSetupIdcheck", method = RequestMethod.POST)
	public ResponseMessage interviewTypeSetupIdcheck(HttpServletRequest request, @RequestBody DtoInterviewTypeSetup dtoInterviewTypeSetup) throws Exception {
		LOGGER.info("interviewTypeSetupIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoInterviewTypeSetup dtoInterviewTypeSetupObj = serviceInterviewTypeSetup.repeatByDtoInterviewTypeSetupId(dtoInterviewTypeSetup.getInterviewTypeId());
			responseMessage=displayMessage(dtoInterviewTypeSetupObj, "INTERVIEW_TYPE_SETUP_RESULT", "INTERVIEW_TYPE_SETUP_REPEAT_INTERVIEW_TYPE_SETUP_ID_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
}
