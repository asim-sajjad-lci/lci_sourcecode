package com.bti.hcm.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90600")
public class ManualChecksHistoryYearHeader extends HcmBaseEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMPYMTINDX")
	private Integer id;

	@Column(name = "YEAR1")
	private Integer year;

	@Column(name = "HCMPYDSCR")
	private String paymentDescription;

	@Column(name = "CHEKNMBR")
	private Integer checkNumber;

	@Column(name = "CHEKDTA")
	private Date checkDate;

	@Column(name = "CHEKPSTDT")
	private Date checkPostDate;

	@Column(name = "EMPLOYINDX")
	private Integer employeeId;

	@ManyToOne
	@JoinColumn(name = "HCMBATINDX")
	private Batches batches;
	
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="manualChecksHistoryYearHeader")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksHistoryYearDistributions>listManualChecksHistoryYearDistributions;

	
	@Column(name = "TOTLAMT", precision = 10, scale = 3)
	private BigDecimal taoalGrossAmount;

	@Column(name = "TOTLNETAMT", precision = 10, scale = 3)
	private BigDecimal totalNetAmpount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public Integer getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public Date getCheckPostDate() {
		return checkPostDate;
	}

	public void setCheckPostDate(Date checkPostDate) {
		this.checkPostDate = checkPostDate;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Batches getBatches() {
		return batches;
	}

	public void setBatches(Batches batches) {
		this.batches = batches;
	}

	public BigDecimal getTaoalGrossAmount() {
		return taoalGrossAmount;
	}

	public void setTaoalGrossAmount(BigDecimal taoalGrossAmount) {
		this.taoalGrossAmount = taoalGrossAmount;
	}

	public BigDecimal getTotalNetAmpount() {
		return totalNetAmpount;
	}

	public void setTotalNetAmpount(BigDecimal totalNetAmpount) {
		this.totalNetAmpount = totalNetAmpount;
	}

	public List<ManualChecksHistoryYearDistributions> getListManualChecksHistoryYearDistributions() {
		return listManualChecksHistoryYearDistributions;
	}

	public void setListManualChecksHistoryYearDistributions(
			List<ManualChecksHistoryYearDistributions> listManualChecksHistoryYearDistributions) {
		this.listManualChecksHistoryYearDistributions = listManualChecksHistoryYearDistributions;
	}
}
