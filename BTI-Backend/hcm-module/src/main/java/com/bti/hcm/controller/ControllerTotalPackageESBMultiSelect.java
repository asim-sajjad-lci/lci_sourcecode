package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoTotalPackageESB;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTotalPackageESB;

@RestController
@RequestMapping("/packageESB")
public class ControllerTotalPackageESBMultiSelect extends BaseController {

	private Logger log = Logger.getLogger(ControllerTotalPackageESBMultiSelect.class);

	@Autowired
	ServiceTotalPackageESB serviceTotalPackageESB;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoTotalPackageESB dtoTotalPackageESB)
			throws Exception {
		log.info("inside Create packageESB Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTotalPackageESB = serviceTotalPackageESB.saveOrUpdate(dtoTotalPackageESB);
			responseMessage = displayMessage(dtoTotalPackageESB, "ACCRUAL_CREATED", "ACCRUALSETUP_NOT_CREATED",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create packageESB Method:" + responseMessage.getMessage());
		return responseMessage;
	}


	@RequestMapping(value = "/getByTypeId", method = RequestMethod.POST)
	public ResponseMessage getByTypeId(HttpServletRequest request, @RequestBody DtoTotalPackageESB dtoTotalPackageESB)
			throws Exception {
		log.info("Get TotalPackageESB By TypeId Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTotalPackageESB dtoTotalPackageESBDisplay = serviceTotalPackageESB.getByTypeId(dtoTotalPackageESB);
			responseMessage = displayMessage(dtoTotalPackageESBDisplay, "PACKAGE_OR_ESB_LIST_GET_DETAIL", "PACKAGE_OR_ESB_NOT_GETTING",
					serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get TotalPackageESB by Id Method:" + dtoTotalPackageESB.getId());
		return responseMessage;
	}
	

}
