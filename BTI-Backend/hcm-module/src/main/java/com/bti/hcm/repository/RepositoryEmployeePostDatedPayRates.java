package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeePostDatedPayRates;

@Repository("repositoryEmployeePostDatedPayRates")
public interface RepositoryEmployeePostDatedPayRates extends JpaRepository<EmployeePostDatedPayRates, Integer>{

	public EmployeePostDatedPayRates findByIdAndIsDeleted(int id, boolean deleted);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeePostDatedPayRates d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleEmployeePostDatedPayRates(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	@Query("select count(*) from EmployeePostDatedPayRates d where (d.reasonforChange like :searchKeyWord or d.employeeMaster.employeeId like :searchKeyWord or d.payCode.payCodeId like :searchKeyWord or d.employeeMaster.employeeFirstName like :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveEmployeePostDatedPayRatesSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	
	@Query("select d from EmployeePostDatedPayRates d where (d.reasonforChange like :searchKeyWord or d.employeeMaster.employeeId like :searchKeyWord or d.payCode.payCodeId like :searchKeyWord or d.employeeMaster.employeeFirstName like :searchKeyWord) and d.isDeleted=false")
	public List<EmployeePostDatedPayRates> predictiveEmployeePostDatedPayRatesSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageable);

	
	public List<EmployeePostDatedPayRates> findByIsDeleted(boolean isdeleted,Pageable pageRequest);
	
	@Query("select m from EmployeePostDatedPayRates m where m.employeeMaster.employeeIndexId =:id and m.isDeleted=false")
	List<EmployeePostDatedPayRates> findByEmployeeId(@Param("id") int id);

	@Query("select count(*) from EmployeePostDatedPayRates e ")
	public Integer getCountOfTotalEmployeePostDatedPayRates();

	
	@Query("select count(*) from EmployeePostDatedPayRates a where (a.employeeMaster.employeeIndexId =:id or a.payCode.id =:id or a.reasonforChange =:reasonforChange or a.effectiveDate =:date) and a.isDeleted=false")
	Integer predictiveActiveEmployeePostDatedPayRatesSearchTotalCount(@Param("id")Integer id,@Param("reasonforChange")String reasone, @Param("date")Date date);

	@Query("select a from EmployeePostDatedPayRates a where (a.employeeMaster.employeeIndexId =:id or a.payCode.id =:id or a.reasonforChange =:reasonforChange or a.effectiveDate =:date) and a.isDeleted=false")
	List<EmployeePostDatedPayRates> predictiveActiveEmployeePostDatedPayRatesSearchWithPagination(@Param("id")Integer id,@Param("reasonforChange")String reasone, @Param("date")Date date,Pageable pageRequest);

	
	@Query("select d from EmployeePostDatedPayRates d where (d.effectiveDate between :fromDate AND :toDate) AND d.isDeleted = false") 
	public List<EmployeePostDatedPayRates> findEffectiveDates(@Param("fromDate")Date fromDate, @Param("toDate")Date toDate);

	
	@Query("select m from EmployeePostDatedPayRates m where (m.employeeMaster.employeeIndexId between :fromemployeeIndexId AND :toemployeeIndexId) AND m.isDeleted = false")
	public List<EmployeePostDatedPayRates> findEmployeeId(@Param("fromemployeeIndexId")Integer fromemployeeIndexId, @Param("toemployeeIndexId")Integer toemployeeIndexId);
	

	@Query("select m from EmployeePostDatedPayRates m where (m.payCode.id between :fromPayCodeId AND :toPayCodeId) AND m.isDeleted = false")
	public List<EmployeePostDatedPayRates> findPayCodeId(@Param("fromPayCodeId")Integer fromPayCodeId, @Param("toPayCodeId")Integer toPayCodeId);
	
	@Query("select d from EmployeePostDatedPayRates d where (d.reasonforChange.id between :fromReasion AND :toReasion) AND d.isDeleted = false") 
	public List<EmployeePostDatedPayRates> findResonForChange(@Param("fromReasion")Integer fromReasion, @Param("toReasion")Integer toReasion);
	
	@Query("select count(*) from EmployeePostDatedPayRates e where (e.reasonforChange LIKE :searchKeyWord ) and e.isDeleted=false")
	public Integer predictiveEmployeePostDatedPayRatesSearchTotalCounts(@Param("searchKeyWord") String searchKeyWord);

	@Query("select e from EmployeePostDatedPayRates e where (e.reasonforChange LIKE :searchKeyWord ) and e.isDeleted=false")
	public List<EmployeePostDatedPayRates> predictiveEmployeePostDatedPayRatesSearchWithPaginations(@Param("searchKeyWord")String searchKeyWord,Pageable pageable);

	public List<EmployeePostDatedPayRates> findByIsDeleted(boolean isdeleted);

	
}
