package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.Company;

public class DtoCompany extends DtoBase{
    private Integer companyId;
    private String companyName;
    private List<DtoCompany> delete;
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public List<DtoCompany> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoCompany> delete) {
		this.delete = delete;
	}
    
    public DtoCompany() {
	}
    
    public DtoCompany(Company company) {
   	}
}
