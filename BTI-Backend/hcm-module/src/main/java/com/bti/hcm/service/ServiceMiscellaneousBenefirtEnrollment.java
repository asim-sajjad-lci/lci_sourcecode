package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.MiscellaneousBenefirtEnrollment;
import com.bti.hcm.model.MiscellaneousBenefits;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoMiscellaneousBenefirtEnrollment;
import com.bti.hcm.model.dto.DtoMiscellaneousBenefits;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryMiscellaneousBenefirtEnrollment;
import com.bti.hcm.repository.RepositoryMiscellaneousBenefits;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceMiscellaneousBenefirtEnrollment ")
public class ServiceMiscellaneousBenefirtEnrollment {
	
	
	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceEmployeeBenefitMaintenance.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired(required=false)
	RepositoryMiscellaneousBenefirtEnrollment repositoryMiscellaneousBenefirtEnrollment;
	

	@Autowired(required=false)
	RepositoryMiscellaneousBenefits repositoryMiscellaneousBenefits;
	
	
	
	
	public DtoMiscellaneousBenefirtEnrollment saveOrUpdate(DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment) {
		try {

			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			MiscellaneousBenefirtEnrollment  miscellaneousBenefirtEnrollment=null;
			if (dtoMiscellaneousBenefirtEnrollment.getId() != null && dtoMiscellaneousBenefirtEnrollment.getId() > 0) {
				miscellaneousBenefirtEnrollment = repositoryMiscellaneousBenefirtEnrollment.findByIdAndIsDeleted(dtoMiscellaneousBenefirtEnrollment.getId(), false);
				miscellaneousBenefirtEnrollment.setUpdatedBy(loggedInUserId);
				miscellaneousBenefirtEnrollment.setUpdatedDate(new Date());
			} else {
				miscellaneousBenefirtEnrollment = new MiscellaneousBenefirtEnrollment();
				miscellaneousBenefirtEnrollment.setCreatedDate(new Date());
				Integer rowId = repositoryMiscellaneousBenefirtEnrollment.getCountOfTotaMiscellaneousBenefirtEnrollment();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				miscellaneousBenefirtEnrollment.setRowId(increment);
			}
			EmployeeMaster employeeMaster=null;
			if(dtoMiscellaneousBenefirtEnrollment.getEmployeeMaster()!=null && dtoMiscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeIndexId()>0) {
				employeeMaster=repositoryEmployeeMaster.findOne(dtoMiscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeIndexId());
			}
			
			MiscellaneousBenefits miscellaneousBenefits=null;
			if(dtoMiscellaneousBenefirtEnrollment.getDtoMiscellaneousBenefits()!=null && dtoMiscellaneousBenefirtEnrollment.getDtoMiscellaneousBenefits().getId()>0) {
				miscellaneousBenefits=repositoryMiscellaneousBenefits.findOne(dtoMiscellaneousBenefirtEnrollment.getDtoMiscellaneousBenefits().getId());
			}
			miscellaneousBenefirtEnrollment.setMiscellaneousBenefits(miscellaneousBenefits);
			miscellaneousBenefirtEnrollment.setEmployeeMaster(employeeMaster);
			miscellaneousBenefirtEnrollment.setStatus(dtoMiscellaneousBenefirtEnrollment.getStatus());
			miscellaneousBenefirtEnrollment.setStartDate(dtoMiscellaneousBenefirtEnrollment.getStartDate());
			miscellaneousBenefirtEnrollment.setEndDate(dtoMiscellaneousBenefirtEnrollment.getEndDate());
			miscellaneousBenefirtEnrollment.setInActive(dtoMiscellaneousBenefirtEnrollment.getInActive());
			miscellaneousBenefirtEnrollment.setMethod(dtoMiscellaneousBenefirtEnrollment.getMethod());
			miscellaneousBenefirtEnrollment.setDudctionAmount(dtoMiscellaneousBenefirtEnrollment.getDudctionAmount());
			miscellaneousBenefirtEnrollment.setDudctionPercent(dtoMiscellaneousBenefirtEnrollment.getDudctionPercent());
			miscellaneousBenefirtEnrollment.setMonthlyAmount(dtoMiscellaneousBenefirtEnrollment.getMonthlyAmount());
			miscellaneousBenefirtEnrollment.setYearlyAmount(dtoMiscellaneousBenefirtEnrollment.getYearlyAmount());
			miscellaneousBenefirtEnrollment.setLifetimeAmount(dtoMiscellaneousBenefirtEnrollment.getLifetimeAmount());
			miscellaneousBenefirtEnrollment.setEmpluyeeerinactive(dtoMiscellaneousBenefirtEnrollment.getEmpluyeeerinactive());
			miscellaneousBenefirtEnrollment.setEmpluyeeermethod(dtoMiscellaneousBenefirtEnrollment.getEmpluyeeermethod());
			miscellaneousBenefirtEnrollment.setBenefitAmount(dtoMiscellaneousBenefirtEnrollment.getBenefitAmount());
			miscellaneousBenefirtEnrollment.setBenefitPercent(dtoMiscellaneousBenefirtEnrollment.getBenefitPercent());
			miscellaneousBenefirtEnrollment.setEmployermonthlyAmount(dtoMiscellaneousBenefirtEnrollment.getEmployermonthlyAmount());
			miscellaneousBenefirtEnrollment.setEmployeryearlyAmount(dtoMiscellaneousBenefirtEnrollment.getEmployeryearlyAmount());
			miscellaneousBenefirtEnrollment.setEmployerlifetimeAmount(dtoMiscellaneousBenefirtEnrollment.getEmployerlifetimeAmount());
			miscellaneousBenefirtEnrollment.setFrequency(dtoMiscellaneousBenefirtEnrollment.getFrequency());
			miscellaneousBenefirtEnrollment.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryMiscellaneousBenefirtEnrollment.saveAndFlush(miscellaneousBenefirtEnrollment);
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneousBenefirtEnrollment;
	}
	public DtoMiscellaneousBenefirtEnrollment delete(List<Integer> ids) {
		log.info("delete Missleniceenrolment Method");
		DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment = new DtoMiscellaneousBenefirtEnrollment();
		try {
			dtoMiscellaneousBenefirtEnrollment.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTANENANCE_DELETED", false));
			dtoMiscellaneousBenefirtEnrollment.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTANENANCE_ASSOCIATED", false));
			List<DtoMiscellaneousBenefirtEnrollment> deleteDtoMiscellaneousBenefirtEnrollment = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTANENANCE_NOT_DELETE_ID_MESSAGE", false).getMessage());
			
				for (Integer planId : ids) {
					MiscellaneousBenefirtEnrollment miscellaneousBenefirtEnrollment = repositoryMiscellaneousBenefirtEnrollment.findOne(planId);
		if(miscellaneousBenefirtEnrollment!=null) {
			DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment2=new DtoMiscellaneousBenefirtEnrollment();
			dtoMiscellaneousBenefirtEnrollment.setId(miscellaneousBenefirtEnrollment.getId());
				
				
			     
				repositoryMiscellaneousBenefirtEnrollment.deleteSingleMiscellaneousBenefirtEnrollment(true, loggedInUserId, planId);
				deleteDtoMiscellaneousBenefirtEnrollment.add(dtoMiscellaneousBenefirtEnrollment2);	
			
		}else {
			inValidDelete = true;
		}
						
					}

				if(inValidDelete){
					invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
					dtoMiscellaneousBenefirtEnrollment.setMessageType(invlidDeleteMessage.toString());
					
				}
				if(!inValidDelete){
					dtoMiscellaneousBenefirtEnrollment.setMessageType("");
					
				}
				
					
				dtoMiscellaneousBenefirtEnrollment.setDelete(deleteDtoMiscellaneousBenefirtEnrollment);
			
			
			log.debug("Delete Missleniceenrolment"
					+ " :"+dtoMiscellaneousBenefirtEnrollment.getId());
	
		}catch (Exception e) {
			log.error(e);
		}
				return dtoMiscellaneousBenefirtEnrollment;
	}

	

	public DtoMiscellaneousBenefirtEnrollment getById(int id) {
		DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment  = new DtoMiscellaneousBenefirtEnrollment();
		try {
			if (id > 0) {
				MiscellaneousBenefirtEnrollment miscellaneousBenefirtEnrollment = repositoryMiscellaneousBenefirtEnrollment.findByIdAndIsDeleted(id, false);
					dtoMiscellaneousBenefirtEnrollment = new DtoMiscellaneousBenefirtEnrollment(miscellaneousBenefirtEnrollment);
					dtoMiscellaneousBenefirtEnrollment = getdtoMiscellaneousBenefirtEnrollment(dtoMiscellaneousBenefirtEnrollment,miscellaneousBenefirtEnrollment);
			} else {
				dtoMiscellaneousBenefirtEnrollment.setMessageType("INVALID_ID");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscellaneousBenefirtEnrollment;
	}

	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("searchEmployeeBenefitMaintenance Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				dtoSearch=searchByMiscellaneousBenefirtEnrollment(dtoSearch);
				dtoSearch.setTotalCount(this.repositoryMiscellaneousBenefirtEnrollment.predictiveMiscellaneousBenefitsTotalCount("%"+searchWord+"%"));
				List<MiscellaneousBenefirtEnrollment> miscellaneousBenefirtEnrollmentList =searchByPageSizeAndNumberMiscellaneousBenefirtEnrollment(dtoSearch);
				List<DtoMiscellaneousBenefirtEnrollment> list = getDtoMiscellaneousBenefirtEnrollmentList(miscellaneousBenefirtEnrollmentList);
				dtoSearch.setRecords(list);
			log.debug("Search OrientationSetup Size is:"+dtoSearch.getTotalCount());
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchByMiscellaneousBenefirtEnrollment(DtoSearch dtoSearch) {
		try {
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("endDate")|| dtoSearch.getSortOn().equals("startDate") || 
							dtoSearch.getSortOn().equals("employeeMaster.employeeId") || dtoSearch.getSortOn().equals("miscellaneousBenefits.BenefitsId")
							|| dtoSearch.getSortOn().equals("miscellaneousBenefits.desc") || dtoSearch.getSortOn().equals("employeeMaster.employeeFirstName")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			 dtoSearch.setCondition(condition);
			
		}catch (Exception e) {
			log.error(e);
		}
			return dtoSearch;
		
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public List<MiscellaneousBenefirtEnrollment> searchByPageSizeAndNumberMiscellaneousBenefirtEnrollment(DtoSearch dtoSearch) {
		List<MiscellaneousBenefirtEnrollment> miscellaneousBenefirtEnrollmentList = null;
		try {
			
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
				

				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					miscellaneousBenefirtEnrollmentList = this.repositoryMiscellaneousBenefirtEnrollment.predictiveMiscellaneousBenefitsSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					miscellaneousBenefirtEnrollmentList = this.repositoryMiscellaneousBenefirtEnrollment.predictiveMiscellaneousBenefitsSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					miscellaneousBenefirtEnrollmentList = this.repositoryMiscellaneousBenefirtEnrollment.predictiveMiscellaneousBenefitsSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
				}
				
			
			}
	
		}catch (Exception e) {
			log.error(e);
		}
				
		return miscellaneousBenefirtEnrollmentList;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchByBenefitCode(DtoSearch dtoSearch) {
		try {
			log.info("searchEmployeeBenefitMaintenance Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				dtoSearch = searchByMiscellaneousBenefirtEnrollment(dtoSearch);  
				dtoSearch.setTotalCount(this.repositoryMiscellaneousBenefirtEnrollment.searchByBenefitsCodeCount("%"+searchWord+"%",dtoSearch.getId()));
				List<MiscellaneousBenefirtEnrollment> miscellaneousBenefirtEnrollmentList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						miscellaneousBenefirtEnrollmentList = this.repositoryMiscellaneousBenefirtEnrollment.searchByBenefitsCode("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						miscellaneousBenefirtEnrollmentList = this.repositoryMiscellaneousBenefirtEnrollment.searchByBenefitsCode("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						miscellaneousBenefirtEnrollmentList = this.repositoryMiscellaneousBenefirtEnrollment.searchByBenefitsCode("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
					}
					
				}
				if(miscellaneousBenefirtEnrollmentList != null && !miscellaneousBenefirtEnrollmentList.isEmpty()){
					List<DtoMiscellaneousBenefirtEnrollment> list = getDtoMiscellaneousBenefirtEnrollmentList(miscellaneousBenefirtEnrollmentList);
					dtoSearch.setRecords(list);
				}
				
			log.debug("Search OrientationSetup Size is:"+dtoSearch.getTotalCount());
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public List<DtoMiscellaneousBenefirtEnrollment> getDtoMiscellaneousBenefirtEnrollmentList(List<MiscellaneousBenefirtEnrollment> miscellaneousBenefirtEnrollmentList){
		List<DtoMiscellaneousBenefirtEnrollment> dtoMiscellaneousBenefirtEnrollmentList = new ArrayList<>();
		try {
			
			DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment=null;
			for (MiscellaneousBenefirtEnrollment miscellaneousBenefirtEnrollment : miscellaneousBenefirtEnrollmentList) {
				dtoMiscellaneousBenefirtEnrollment = new DtoMiscellaneousBenefirtEnrollment(miscellaneousBenefirtEnrollment);

				DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
				if(miscellaneousBenefirtEnrollment.getEmployeeMaster()!=null) {
					
					employeeMaster.setEmployeeIndexId(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeIndexId());
					employeeMaster.setEmployeeId(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeId());
					employeeMaster.setEmployeeBirthDate(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeBirthDate());
					employeeMaster.setEmployeeCitizen(miscellaneousBenefirtEnrollment.getEmployeeMaster().isEmployeeCitizen());
					employeeMaster.setEmployeeFirstName(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeFirstName());
					employeeMaster.setEmployeeFirstNameArabic(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeFirstNameArabic());
					employeeMaster.setEmployeeGender(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeGender());
					employeeMaster.setEmployeeHireDate(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeHireDate());
					employeeMaster.setEmployeeImmigration(miscellaneousBenefirtEnrollment.getEmployeeMaster().isEmployeeImmigration());
					employeeMaster.setEmployeeInactive(miscellaneousBenefirtEnrollment.getEmployeeMaster().isEmployeeInactive());
					employeeMaster.setEmployeeInactiveDate(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeInactiveDate());
					employeeMaster.setEmployeeInactiveReason(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeInactiveReason());
					employeeMaster.setEmployeeLastName(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeLastName());
					employeeMaster.setEmployeeLastNameArabic(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeLastNameArabic());
					employeeMaster.setEmployeeLastWorkDate(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeLastWorkDate());
					employeeMaster.setEmployeeMaritalStatus(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeMaritalStatus());
					employeeMaster.setEmployeeMiddleName(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeMiddleName());
					employeeMaster.setEmployeeMiddleNameArabic(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeMiddleNameArabic());
					employeeMaster.setEmployeeTitle(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeTitle());
					employeeMaster.setEmployeeTitleArabic(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeTitleArabic());
					employeeMaster.setEmployeeType(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeType());
					employeeMaster.setEmployeeUserIdInSystem(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeUserIdInSystem());
					employeeMaster.setEmployeeWorkHourYearly(miscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeWorkHourYearly());
					dtoMiscellaneousBenefirtEnrollment.setEmployeeMaster(employeeMaster);
			
				}
				DtoMiscellaneousBenefits miscellaneousBenefits =new DtoMiscellaneousBenefits();
				if(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits()!=null) {
					
					miscellaneousBenefits.setId(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getId());
					miscellaneousBenefits.setBenefitsId(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getBenefitsId());
					miscellaneousBenefits.setDesc(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getDesc());
					miscellaneousBenefits.setArbicDesc(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getArbicDesc());
					miscellaneousBenefits.setFrequency(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getFrequency());
					miscellaneousBenefits.setStartDate(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getStartDate());
					miscellaneousBenefits.setEndDate(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getEndDate());
					miscellaneousBenefits.setInactive(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().isInactive());
					miscellaneousBenefits.setMethod(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getMethod());
					miscellaneousBenefits.setDudctionAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getDudctionAmount());
					miscellaneousBenefits.setDudctionPercent(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getDudctionPercent());
					miscellaneousBenefits.setEmployerlifetimeAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getMonthlyAmount());
					miscellaneousBenefits.setYearlyAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getYearlyAmount());
					miscellaneousBenefits.setLifetimeAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getLifetimeAmount());
					miscellaneousBenefits.setEmpluyeeerinactive(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().isEmpluyeeerinactive());
					miscellaneousBenefits.setEmpluyeeermethod(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getEmpluyeeermethod());
					miscellaneousBenefits.setBenefitAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getBenefitAmount());
					miscellaneousBenefits.setBenefitPercent(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getBenefitPercent());
					miscellaneousBenefits.setEmployermonthlyAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getEmployermonthlyAmount());
					miscellaneousBenefits.setEmployeryearlyAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getEmployeryearlyAmount());
					miscellaneousBenefits.setEmployerlifetimeAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getEmployerlifetimeAmount());
					miscellaneousBenefits.setMonthlyAmount(miscellaneousBenefirtEnrollment.getMiscellaneousBenefits().getMonthlyAmount());
					dtoMiscellaneousBenefirtEnrollment.setDtoMiscellaneousBenefits(miscellaneousBenefits);
				}
				dtoMiscellaneousBenefirtEnrollment. setId(miscellaneousBenefirtEnrollment.getId());
				dtoMiscellaneousBenefirtEnrollment.setStatus(miscellaneousBenefirtEnrollment.getStatus());
				dtoMiscellaneousBenefirtEnrollment.setStartDate(miscellaneousBenefirtEnrollment.getStartDate());
				dtoMiscellaneousBenefirtEnrollment.setEndDate(miscellaneousBenefirtEnrollment.getEndDate());
				dtoMiscellaneousBenefirtEnrollment.setInActive(miscellaneousBenefirtEnrollment.getInActive());
				dtoMiscellaneousBenefirtEnrollment.setMethod(miscellaneousBenefirtEnrollment.getMethod());
				dtoMiscellaneousBenefirtEnrollment.setDudctionAmount(miscellaneousBenefirtEnrollment.getDudctionAmount());
				dtoMiscellaneousBenefirtEnrollment.setDudctionPercent(miscellaneousBenefirtEnrollment.getDudctionPercent());
				dtoMiscellaneousBenefirtEnrollment.setMonthlyAmount(miscellaneousBenefirtEnrollment.getMonthlyAmount());
				dtoMiscellaneousBenefirtEnrollment.setYearlyAmount(miscellaneousBenefirtEnrollment.getYearlyAmount());
				dtoMiscellaneousBenefirtEnrollment.setLifetimeAmount(miscellaneousBenefirtEnrollment.getLifetimeAmount());
				dtoMiscellaneousBenefirtEnrollment.setEmpluyeeerinactive(miscellaneousBenefirtEnrollment.getEmpluyeeerinactive());
				dtoMiscellaneousBenefirtEnrollment.setEmpluyeeermethod(miscellaneousBenefirtEnrollment.getEmpluyeeermethod());
				dtoMiscellaneousBenefirtEnrollment.setBenefitAmount(miscellaneousBenefirtEnrollment.getBenefitAmount());
				dtoMiscellaneousBenefirtEnrollment.setBenefitPercent(miscellaneousBenefirtEnrollment.getBenefitPercent());
				dtoMiscellaneousBenefirtEnrollment.setEmployermonthlyAmount(miscellaneousBenefirtEnrollment.getEmployermonthlyAmount());
				dtoMiscellaneousBenefirtEnrollment.setEmployeryearlyAmount(miscellaneousBenefirtEnrollment.getEmployeryearlyAmount());
				dtoMiscellaneousBenefirtEnrollment.setEmployerlifetimeAmount(miscellaneousBenefirtEnrollment.getEmployerlifetimeAmount());
				dtoMiscellaneousBenefirtEnrollment.setFrequency(miscellaneousBenefirtEnrollment.getFrequency());
				
				getAndSetDtoMiscellaneousBenefirtEnrollment(miscellaneousBenefirtEnrollment);
				dtoMiscellaneousBenefirtEnrollmentList.add(dtoMiscellaneousBenefirtEnrollment);
				}
	
		}catch (Exception e) {
			log.error(e);
		}
				return dtoMiscellaneousBenefirtEnrollmentList;
			
	}
	public DtoMiscellaneousBenefirtEnrollment repeatByEmployeeId(int employeeIndexId) {
		log.info("repeatByEmployeeId Method");
		DtoMiscellaneousBenefirtEnrollment dtoMiscEnroll = new DtoMiscellaneousBenefirtEnrollment();
		try {
			List<MiscellaneousBenefirtEnrollment> misenroll=repositoryMiscellaneousBenefirtEnrollment.findByEmployeeId(employeeIndexId);
			if(misenroll!=null && !misenroll.isEmpty()) {
				dtoMiscEnroll.setIsRepeat(true);
				
			}else {
				dtoMiscEnroll.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoMiscEnroll;
	}


	/**
	 * 
	 * @param miscellaneousBenefirtEnrollment
	 * @return
	 */
	public DtoMiscellaneousBenefirtEnrollment getAndSetDtoMiscellaneousBenefirtEnrollment(MiscellaneousBenefirtEnrollment miscellaneousBenefirtEnrollment) {
		DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment = new DtoMiscellaneousBenefirtEnrollment();
		try {
			dtoMiscellaneousBenefirtEnrollment = getdtoMiscellaneousBenefirtEnrollment(dtoMiscellaneousBenefirtEnrollment,miscellaneousBenefirtEnrollment);
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoMiscellaneousBenefirtEnrollment;
	}
	
	
	public DtoMiscellaneousBenefirtEnrollment getdtoMiscellaneousBenefirtEnrollment(DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment,MiscellaneousBenefirtEnrollment miscellaneousBenefirtEnrollment) {
		try {
			dtoMiscellaneousBenefirtEnrollment. setId(miscellaneousBenefirtEnrollment.getId());
			dtoMiscellaneousBenefirtEnrollment.setStatus(miscellaneousBenefirtEnrollment.getStatus());
			dtoMiscellaneousBenefirtEnrollment.setStartDate(miscellaneousBenefirtEnrollment.getStartDate());
			dtoMiscellaneousBenefirtEnrollment.setEndDate(miscellaneousBenefirtEnrollment.getEndDate());
			dtoMiscellaneousBenefirtEnrollment.setInActive(miscellaneousBenefirtEnrollment.getInActive());
			dtoMiscellaneousBenefirtEnrollment.setMethod(miscellaneousBenefirtEnrollment.getMethod());
			dtoMiscellaneousBenefirtEnrollment.setDudctionAmount(miscellaneousBenefirtEnrollment.getDudctionAmount());
			dtoMiscellaneousBenefirtEnrollment.setDudctionPercent(miscellaneousBenefirtEnrollment.getDudctionPercent());
			dtoMiscellaneousBenefirtEnrollment.setMonthlyAmount(miscellaneousBenefirtEnrollment.getMonthlyAmount());
			dtoMiscellaneousBenefirtEnrollment.setYearlyAmount(miscellaneousBenefirtEnrollment.getYearlyAmount());
			dtoMiscellaneousBenefirtEnrollment.setLifetimeAmount(miscellaneousBenefirtEnrollment.getLifetimeAmount());
			dtoMiscellaneousBenefirtEnrollment.setEmpluyeeerinactive(miscellaneousBenefirtEnrollment.getEmpluyeeerinactive());
			dtoMiscellaneousBenefirtEnrollment.setEmpluyeeermethod(miscellaneousBenefirtEnrollment.getEmpluyeeermethod());
			dtoMiscellaneousBenefirtEnrollment.setBenefitAmount(miscellaneousBenefirtEnrollment.getBenefitAmount());
			dtoMiscellaneousBenefirtEnrollment.setBenefitPercent(miscellaneousBenefirtEnrollment.getBenefitPercent());
			dtoMiscellaneousBenefirtEnrollment.setEmployermonthlyAmount(miscellaneousBenefirtEnrollment.getEmployermonthlyAmount());
			dtoMiscellaneousBenefirtEnrollment.setEmployeryearlyAmount(miscellaneousBenefirtEnrollment.getEmployeryearlyAmount());
			dtoMiscellaneousBenefirtEnrollment.setEmployerlifetimeAmount(miscellaneousBenefirtEnrollment.getEmployerlifetimeAmount());
			dtoMiscellaneousBenefirtEnrollment.setFrequency(miscellaneousBenefirtEnrollment.getFrequency());
	
		}catch (Exception e) {
			log.error(e);
		}
			return dtoMiscellaneousBenefirtEnrollment;
	}
	
		
	
}
