package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.EmployeeDirectDeposit;

public class DtoEmployeeDirectDeposit extends DtoBase{

	private Integer id;
	private String bankName;
	private String accountNumber;
	private BigDecimal amount;
	private BigDecimal percent;
	private short status;
	private int accountSequence;

	
	private DtoEmployeeMaster employeeMaster;
	private List<DtoEmployeeDirectDeposit> listEmployeeDirectDeposit;

	private List<DtoEmployeeDirectDeposit> delete;

	public DtoEmployeeDirectDeposit(EmployeeDirectDeposit employeeDirectDeposit) {
		
	}

	public DtoEmployeeDirectDeposit() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}


	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public int getAccountSequence() {
		return accountSequence;
	}

	public void setAccountSequence(int accountSequence) {
		this.accountSequence = accountSequence;
	}

	public List<DtoEmployeeDirectDeposit> getListEmployeeDirectDeposit() {
		return listEmployeeDirectDeposit;
	}

	public void setListEmployeeDirectDeposit(List<DtoEmployeeDirectDeposit> listEmployeeDirectDeposit) {
		this.listEmployeeDirectDeposit = listEmployeeDirectDeposit;
	}


	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public List<DtoEmployeeDirectDeposit> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeeDirectDeposit> delete) {
		this.delete = delete;
	}


	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

}
