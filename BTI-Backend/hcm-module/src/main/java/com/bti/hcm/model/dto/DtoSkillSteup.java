package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.SkillsSetup;

public class DtoSkillSteup extends DtoBase {

	private Integer id;
	private String skillId;
	private String skillDesc;
	private String arabicDesc;
	private BigDecimal compensation;
	private Short frequency;
	private List<DtoSkillSteup> deleteSkillSteup;
	
	private List<SkillsSetup> skillsSetup;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSkillId() {
		return skillId;
	}

	public void setSkillId(String skillId) {
		this.skillId = skillId;
	}

	public String getSkillDesc() {
		return skillDesc;
	}

	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}

	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

	public BigDecimal getCompensation() {
		return compensation;
	}

	public void setCompensation(BigDecimal compensation) {
		this.compensation = compensation;
	}

	public Short getFrequency() {
		return frequency;
	}

	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}

	public List<DtoSkillSteup> getDeleteSkillSteup() {
		return deleteSkillSteup;
	}

	public void setDeleteSkillSteup(List<DtoSkillSteup> deleteSkillSteup) {
		this.deleteSkillSteup = deleteSkillSteup;
	}

	public DtoSkillSteup() {

	}

	public DtoSkillSteup(SkillsSetup skillsSetup) {
		this.skillId = skillsSetup.getSkillId();

	}

	public List<SkillsSetup> getSkillsSetup() {
		return skillsSetup;
	}

	public void setSkillsSetup(List<SkillsSetup> skillsSetup) {
		this.skillsSetup = skillsSetup;
	}

}
