package com.bti.hcm.model.dto;

public class DtoCompanyInfo {
	
	private String name;
	private String nameArabic;
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNameArabic() {
		return nameArabic;
	}
	public void setNameArabic(String nameArabic) {
		this.nameArabic = nameArabic;
	}
	
	
}
