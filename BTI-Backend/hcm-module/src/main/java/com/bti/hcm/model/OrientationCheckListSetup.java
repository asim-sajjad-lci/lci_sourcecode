/**
 * BTI - BAAN for Technology And Trade IntL. 

 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Description: The persistent class for the OrientationCheckListSetup database
 * table. Name of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40501",indexes = {
        @Index(columnList = "ORNTCHKINDX")
})
@NamedQuery(name = "OrientationCheckListSetup.findAll", query = "SELECT o FROM OrientationCheckListSetup o")
public class OrientationCheckListSetup extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORNTCHKINDX")
	private int id;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "orientationCheckListSetup")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<OrientationSetup> listOrientationSetup;

	@Column(name = "ORNTCHKDSCR", columnDefinition = "char(150)")
	private String orientationChecklistItemDescription;

	@Column(name = "ORNTCHKDSCRA", columnDefinition = "char(150)")
	private String orientationChecklistItemDescriptionArabic;

	@ManyToOne
	@JoinColumn(name = "PRCHKINDX")
	private OrientationPredefinedCheckListItem orientationPredefinedCheckListItem;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrientationChecklistItemDescription() {
		return orientationChecklistItemDescription;
	}

	public void setOrientationChecklistItemDescription(String orientationChecklistItemDescription) {
		this.orientationChecklistItemDescription = orientationChecklistItemDescription;
	}

	public String getOrientationChecklistItemDescriptionArabic() {
		return orientationChecklistItemDescriptionArabic;
	}

	public void setOrientationChecklistItemDescriptionArabic(String orientationChecklistItemDescriptionArabic) {
		this.orientationChecklistItemDescriptionArabic = orientationChecklistItemDescriptionArabic;
	}

	public OrientationPredefinedCheckListItem getOrientationPredefinedCheckListItem() {
		return orientationPredefinedCheckListItem;
	}

	public void setOrientationPredefinedCheckListItem(
			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem) {
		this.orientationPredefinedCheckListItem = orientationPredefinedCheckListItem;
	}

	public List<OrientationSetup> getListOrientationSetup() {
		return listOrientationSetup;
	}

	public void setListOrientationSetup(List<OrientationSetup> listOrientationSetup) {
		this.listOrientationSetup = listOrientationSetup;
	}

}
