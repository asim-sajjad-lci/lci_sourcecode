package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoCodeForDeductionBenefitPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceCodeForDeductionBenefitPayCode;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/CodeForDeductionBenefitPayCode")
public class ControllerCodeForDeductionBenefitPayCode extends BaseController{
	
	
	private static final Logger LOGGER = Logger.getLogger(ControllerCodeForDeductionBenefitPayCode.class);
	

	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	@Autowired
	ServiceCodeForDeductionBenefitPayCode serviceCodeForDeductionBenefitPayCode;

	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search codeForDeductionBenefitPayCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceCodeForDeductionBenefitPayCode.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.CODE_FOR_ALLSETUP_CODE_GET_ALL, MessageConstant.CODE_FOR_ALLSETUP_CODE__LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoCodeForDeductionBenefitPayCode> dtoCodeForDeductionBenefitPayCodeList = serviceCodeForDeductionBenefitPayCode.getAllDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("CODE_FOR_ALLSETUP_CODE_GET_ALL", false), dtoCodeForDeductionBenefitPayCodeList);
			}else {
				responseMessage =unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
	
	
}
