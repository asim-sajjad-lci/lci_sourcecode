package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AccountsTableAccumulation;

@Repository("/repositoryAccountsTableAccumulation")
public interface RepositoryAccountsTableAccumulation extends JpaRepository<AccountsTableAccumulation, Integer>{

}
