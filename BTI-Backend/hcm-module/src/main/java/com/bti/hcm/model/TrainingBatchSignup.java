package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40811",indexes = {
        @Index(columnList = "TRNCCLSRWID")
})
public class TrainingBatchSignup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRNCCLSRWID")
	private Integer id;

	@Column(name = "EMPLFUNME", columnDefinition = "char(150)")
	private String selectedEmployes;

	@Column(name = "TRNCCLSCMDT")
	private Date completeDate;
	
	@ManyToMany
	@JoinTable(
	        name = "hr40811_employee_master", 
	        joinColumns = { @JoinColumn(name = "training_batch_signup_trncclsrwid") }, 
	        inverseJoinColumns = { @JoinColumn(name = "employee_master_employindx") }
	    )
	private List<EmployeeMaster> employeeMaster;
	
	
	
	//@ManyToMany(cascade= {CascadeType.ALL})
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(
	        name = "HR40811_HR40801", 
	        joinColumns = { @JoinColumn(name = "training_batch_signup_id") }, 
	        inverseJoinColumns = { @JoinColumn(name = "training_course_detail_id") }
	    )
	private List<TrainingCourseDetail> trainingCourseDetails;
	
	@ManyToOne
	@JoinColumn(name = "TRNCINDX")
	private TraningCourse trainingCourse;

	@Column(name = "TRNCCLSCOMT", columnDefinition = "char(61)")
	private String courseComments;

	@Column(name = "TRNCCLSSTST")
	private Short trainingClassStatus;
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSelectedEmployes() {
		return selectedEmployes;
	}

	public void setSelectedEmployes(String selectedEmployes) {
		this.selectedEmployes = selectedEmployes;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public String getCourseComments() {
		return courseComments;
	}

	public void setCourseComments(String courseComments) {
		this.courseComments = courseComments;
	}

	public Short getTrainingClassStatus() {
		return trainingClassStatus;
	}

	public void setTrainingClassStatus(Short trainingClassStatus) {
		this.trainingClassStatus = trainingClassStatus;
	}

	
	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	

	public List<TrainingCourseDetail> getTrainingCourseDetails() {
		return trainingCourseDetails;
	}

	public void setTrainingCourseDetails(List<TrainingCourseDetail> trainingCourseDetails) {
		this.trainingCourseDetails = trainingCourseDetails;
	}

	public TraningCourse getTrainingCourse() {
		return trainingCourse;
	}

	public void setTrainingCourse(TraningCourse trainingCourse) {
		this.trainingCourse = trainingCourse;
	}

	

	
	
}
