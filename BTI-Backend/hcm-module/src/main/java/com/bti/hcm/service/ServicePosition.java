package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Position;
import com.bti.hcm.model.PositionClass;
import com.bti.hcm.model.PositionPalnSetup;
import com.bti.hcm.model.SkillSetSetup;
import com.bti.hcm.model.dto.DtoPosition;
import com.bti.hcm.model.dto.DtoPositionPlanSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositoryPositionClass;
import com.bti.hcm.repository.RepositorySkillSetSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("servicePosition")
public class ServicePosition {

	/**
	 * @Description LOGGER use for put a logger in Department Service
	 */
	static Logger log = Logger.getLogger(ServiceDepartment.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in Position service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in Position service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in Position service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired(required=false) 
	RepositorySkillSetSetup repositorySkillSetSetup;
	
	
	@Autowired(required=false) 
	RepositoryPositionClass  repositoryPositionClass;

	/**
	 * @Description repositoryPosition Autowired here using annotation of spring for access of repositoryPosition method in Position service
	 * 				In short Access Position Query from Database using repositoryDepartment.
	 */
	@Autowired
	RepositoryPosition repositoryPosition;
	
	@Autowired
	ServiceResponse response;

	
	
	public DtoPosition saveOrUpdatePosition(DtoPosition dtoPosition) {
		try {
			log.info("saveOrUpdatePosition Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Position position = null;
			if (dtoPosition.getId() != null && dtoPosition.getId() > 0) {
				
				position = repositoryPosition.findByIdAndIsDeleted(dtoPosition.getId(), false);
				position.setUpdatedBy(loggedInUserId);
				position.setUpdatedDate(new Date());
				dtoPosition.setMessage("POSITION_UPDATED");
			} else {
				position = new Position();
				position.setCreatedDate(new Date());
				
				Integer rowId = repositoryPosition.getCountOfTotaPosition();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				position.setRowId(increment);
			}
			
			SkillSetSetup skillSetSetup=null;
			if(dtoPosition.getSkillsetPrimaryId()!=null) {
				skillSetSetup =  repositorySkillSetSetup.findOne(dtoPosition.getSkillsetPrimaryId());
			}
			PositionClass positionClass=null;
			if(dtoPosition.getPositionClassPrimaryId()!=null){
				positionClass = repositoryPositionClass.findOne(dtoPosition.getPositionClassPrimaryId());	
			}
			position.setReportToPostion(dtoPosition.getReportToPostion());
			position.setPositionClass(positionClass);
			position.setSkillSetSetup(skillSetSetup);
			position.setPositionId(dtoPosition.getPositionId());
			position.setDescription(dtoPosition.getDescription());
			position.setPostionLongDesc(dtoPosition.getPostionLongDesc());
			position.setArabicDescription(dtoPosition.getArabicDescription());
			
			position.setPostionLongDesc(dtoPosition.getPostionLongDesc());
			position.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			position = repositoryPosition.saveAndFlush(position);
			dtoPosition.setId(position.getId());
			log.debug("Position is:"+position.getId());
		} catch (Exception e) {
			log.error(e);
		}
			return dtoPosition;
		
	}

	public DtoSearch getAllPostion(DtoPosition dtoPosition) {
		log.info("getAllPostion Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoPosition.getPageNumber());
			dtoSearch.setPageSize(dtoPosition.getPageSize());
			dtoSearch.setTotalCount(repositoryPosition.getCountOfTotalPosition());
			List<Position> skillPositionList = null;
			if (dtoPosition.getPageNumber() != null && dtoPosition.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoPosition.getPageNumber(), dtoPosition.getPageSize(), Direction.DESC, "createdDate");
				skillPositionList = repositoryPosition.findByIsDeleted(false, pageable);
			} else {
				skillPositionList = repositoryPosition.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoPosition> dtoPOsitionList=new ArrayList<>();
			if(skillPositionList!=null && !skillPositionList.isEmpty())
			{
				for (Position position : skillPositionList) 
				{
					Hibernate.initialize(position.getSkillSetSetup());
					Hibernate.initialize(position.getPositionClass());
					dtoPosition=new DtoPosition(position);
					if(position.getPositionClass()!=null){
						dtoPosition.setPositionClassId(position.getPositionClass().getPositionClassId());
					}
					 if(position.getSkillSetSetup()!=null){
						 dtoPosition.setSkillSetId(position.getSkillSetSetup().getSkillSetId());
					 }
					dtoPosition.setReportToPostion(position.getReportToPostion());
					dtoPosition.setDescription(position.getDescription());
					dtoPosition.setReportToPostion(position.getReportToPostion());
					dtoPosition.setPositionId(position.getPositionId());
					dtoPosition.setPositionClass(position.getPositionClass());
					dtoPosition.setSkillSetSetup(position.getSkillSetSetup());
					dtoPosition.setArabicDescription(position.getArabicDescription());
					dtoPosition.setId(position.getId());
					dtoPosition.setPositionClass(null);
					dtoPosition.setSkillSetSetup(null);
					dtoPOsitionList.add(dtoPosition);
				}
				dtoSearch.setRecords(dtoPOsitionList);
			}
			log.debug("All Postion List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoPosition deletePosition(List<Integer> ids) {
		log.info("deletePosition Method");
		DtoPosition dtoPosition = new DtoPosition();
		dtoPosition
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("POSITION_DELETED", false));
		dtoPosition.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("POSTION_ASSOCIATED", false));
		List<DtoPosition> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage
				.append(response.getMessageByShortAndIsDeleted("POSTION_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer positionId : ids) {
				Position position = repositoryPosition.findOne(positionId);
				if(position.getListPositionPlan().isEmpty() && position.getListEmployeePositionHistory().isEmpty() 
						&& position.getListPositionAttachment().isEmpty()&& position.getRequisitions().isEmpty()
						&&position.getEmployeeMaster().isEmpty()&&position.getPayScheduleSetupPosition().isEmpty()){
					DtoPosition dtoskillSetup2 = new DtoPosition();
					dtoskillSetup2.setId(positionId);
					dtoskillSetup2.setDescription(position.getDescription());
					repositoryPosition.deleteSinglePosition(true, loggedInUserId, positionId);
					deletePosition.add(dtoskillSetup2);
				}else{
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoPosition.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoPosition.setMessageType("");
			}
			dtoPosition.setDeletePosition(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete SkillSteup :"+dtoPosition.getId());
		return dtoPosition;
	}

	public DtoPosition getByPositionId(int parseInt) {
		log.info("getByPositionId Method");
		DtoPosition dtoPosition = new DtoPosition();
		try {
			if (parseInt > 0) {
				Position position = repositoryPosition.findByIdAndIsDeleted(parseInt, false);
				List<DtoPosition> dtoPositionLists = new ArrayList<>();
				
				if (position != null) {
					dtoPosition = new DtoPosition(position);
					dtoPosition.setArabicDescription(position.getArabicDescription());
					dtoPosition.setDescription(position.getDescription());
					dtoPosition.setId(position.getId());
					dtoPosition.setReportToPostion(position.getReportToPostion());
					dtoPosition.setPostionLongDesc(position.getPostionLongDesc());
					if(position.getSkillSetSetup()!=null) {
						dtoPosition.setSkillsetPrimaryId(position.getSkillSetSetup().getId());
						dtoPosition.setSkillSetId(position.getSkillSetSetup().getSkillSetId());	
					}
					if(position.getPositionClass()!=null) {
						dtoPosition.setPositionClassPrimaryId(position.getPositionClass().getId());
						dtoPosition.setPositionClassId(position.getPositionClass().getPositionClassId());
					}
					if (position.getListPositionPlan() == null) {
						List<DtoPositionPlanSetup> dtoPositionPlanList = new ArrayList<>();
						dtoPosition.setListDtoPositionPlanSetup(dtoPositionPlanList);
					} 
					List<PositionPalnSetup> listPositionPlanSetup = position.getListPositionPlan();
					for (PositionPalnSetup positionPalnSetup : listPositionPlanSetup) {
						
						List<DtoPositionPlanSetup> listDtoPositionPlanSetup = new ArrayList<>();
						dtoPosition.setListDtoPositionPlanSetup(listDtoPositionPlanSetup);
						
						DtoPositionPlanSetup dtoPositionPlanSetup = new DtoPositionPlanSetup();
						dtoPositionPlanSetup.setId(positionPalnSetup.getId());
						dtoPositionPlanSetup.setPositionPlanDesc(position.getDescription());
						dtoPositionPlanSetup.setPositionPlanArbicDesc(position.getArabicDescription());
						dtoPosition.getListDtoPositionPlanSetup().add(dtoPositionPlanSetup);
					}
					dtoPositionLists.add(dtoPosition);
					
				} else {
					dtoPosition.setMessageType("POSITION_NOT_GETTING");

				}
			} else {
				dtoPosition.setMessageType("POSITION_REPEAT_NOT_FOUND");

			}
			log.debug("position By Id is:"+dtoPosition.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPosition;
	}

	public DtoPosition repeatBySkillSteupId(String positionId) {
		log.info("repeatByPositionId Method");
		DtoPosition dtoPositioneup = new DtoPosition();
		try {
			List<Position> skillsteup=repositoryPosition.findBypositionId(positionId.trim());
			if(skillsteup!=null && !skillsteup.isEmpty()) {
				dtoPositioneup.setIsRepeat(true);
			}else {
				dtoPositioneup.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPositioneup;
	}

	public DtoSearch searchPosition(DtoSearch dtoSearch) {
		try {
			log.info("searchPosition Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
			
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("positionId") || dtoSearch.getSortOn().equals("description") || dtoSearch.getSortOn().equals("arabicDescription") || dtoSearch.getSortOn().equals("reportToPostion")
								|| dtoSearch.getSortOn().equals("postionLongDesc")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
					
				dtoSearch.setTotalCount(this.repositoryPosition.predictivePositionSearchTotalCount("%"+searchWord+"%"));
				List<Position> positionList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						positionList = this.repositoryPosition.predictivePositionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						positionList = this.repositoryPosition.predictivePositionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						positionList =this.repositoryPosition.predictivePositionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
					
					
					
					
					
				}
				if(positionList != null && !positionList.isEmpty()){
					List<DtoPosition> dtoPositionLists = new ArrayList<>();
					DtoPosition dtoPosition=null;
					for (Position position : positionList) {
						List<PositionPalnSetup> listPositionPlanSetup = position.getListPositionPlan();
						dtoPosition = new DtoPosition(position);
						dtoPosition.setId(position.getId());
						
						dtoPosition.setDescription(position.getDescription());
						dtoPosition.setArabicDescription(position.getArabicDescription());
						dtoPosition.setReportToPostion(position.getReportToPostion());
						dtoPosition.setPostionLongDesc(position.getPostionLongDesc());
						if(position.getSkillSetSetup()!=null) {
							dtoPosition.setSkillsetPrimaryId(position.getSkillSetSetup().getId());
							dtoPosition.setSkillSetId(position.getSkillSetSetup().getSkillSetId());	
						}
						if(position.getPositionClass()!=null) {
							dtoPosition.setPositionClassPrimaryId(position.getPositionClass().getId());
							dtoPosition.setPositionClassId(position.getPositionClass().getPositionClassId());
						}
						if (position.getListPositionPlan() == null) {
							List<DtoPositionPlanSetup> dtoPositionPlanList = new ArrayList<>();
							dtoPosition.setListDtoPositionPlanSetup(dtoPositionPlanList);
						} 
						for (PositionPalnSetup positionPalnSetup : listPositionPlanSetup) {
							
							List<DtoPositionPlanSetup> listDtoPositionPlanSetup = new ArrayList<>();
							dtoPosition.setListDtoPositionPlanSetup(listDtoPositionPlanSetup);
							
							DtoPositionPlanSetup dtoPositionPlanSetup = new DtoPositionPlanSetup();
							dtoPositionPlanSetup.setId(positionPalnSetup.getId());
							dtoPositionPlanSetup.setPositionPlanDesc(position.getDescription());
							dtoPositionPlanSetup.setPositionPlanArbicDesc(position.getArabicDescription());
							dtoPosition.getListDtoPositionPlanSetup().add(dtoPositionPlanSetup);
						}
						dtoPositionLists.add(dtoPosition);
					}
					dtoSearch.setRecords(dtoPositionLists);
				}
			}
			log.debug("Search searchPosition Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public DtoSearch searchPosition1(DtoSearch dtoSearch) {
		try {
			log.info("searchPosition Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				dtoSearch.setTotalCount(this.repositoryPosition.predictivePositionSearchTotalCount1(Integer.parseInt(searchWord)));
				List<Position> positionList =null;
				/*if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					positionList = this.repositoryPosition.predictivePositionSearchWithPagination1(Integer.parseInt(searchWord));
				}else {*/
					positionList = this.repositoryPosition.predictivePositionSearchWithPagination1(Integer.parseInt(searchWord));
				/*s*/
			
				if(positionList != null && !positionList.isEmpty()){
					List<DtoPosition> dtoPosition = new ArrayList<>();
					DtoPosition dtoPosition1=null;
					for (Position position : positionList) {
						dtoPosition1 = new DtoPosition(position);
						dtoPosition1.setReportToPostion(position.getReportToPostion());
						dtoPosition1.setArabicDescription(position.getArabicDescription());
						dtoPosition.add(dtoPosition1);
					}
					dtoSearch.setRecords(dtoPosition);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPositionId(DtoSearch dtoSearch) {
		try {
			log.info("searchPositionId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				
				 List<String> positionIdList = this.repositoryPosition.predictivePositionIdSearchWithPagination("%"+searchWord+"%");
					if(!positionIdList.isEmpty()) {
						dtoSearch.setRecords(positionIdList.size());
					}
				dtoSearch.setIds(positionIdList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public List<DtoPosition> getAllPostionDropDownList() {
		log.info("getAllPostionDropDownList  Method");
		List<DtoPosition> dtoPositionList = new ArrayList<>();
		try {
			List<Position> list = repositoryPosition.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (Position position : list) {
					DtoPosition dtoPosition = new DtoPosition();
					dtoPosition.setId(position.getId());
					dtoPosition.setPositionId(position.getPositionId());
					dtoPosition.setDescription(position.getDescription());
					dtoPosition.setArabicDescription(position.getArabicDescription());
					dtoPositionList.add(dtoPosition);
				}
			}
			log.debug("Position is:"+dtoPositionList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPositionList;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllPositionId(DtoSearch dtoSearch) {
		log.info("getAllPositionId Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "positionId" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "description" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "arabicDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "postionLongDesc" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositoryPosition.predictivePositionSearchTotalCount("%"+searchWord+"%"));
				List<Position> positionList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						positionList = this.repositoryPosition.predictiveGetAllPositionIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						positionList = this.repositoryPosition.predictiveGetAllPositionIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						positionList = this.repositoryPosition.predictiveGetAllPositionIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(positionList != null && !positionList.isEmpty()){
					List<DtoPosition> dtoPositionList = new ArrayList<>();
					DtoPosition dtoPosition=null;
					for (Position position : positionList) {
						dtoPosition=new DtoPosition(position);
					
						dtoPosition.setId(position.getId());
						dtoPosition.setPositionId(position.getPositionId());
						dtoPosition.setDescription(position.getDescription());
						dtoPosition.setArabicDescription(position.getArabicDescription());
						dtoPositionList.add(dtoPosition);
					}
					dtoSearch.setRecords(dtoPositionList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		log.debug("getAllPositionId Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoSearch searchAllPostionIds(DtoSearch dtoSearch) {
		try {
			log.info("searchAllPostionIds Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				List<Position> positionList = this.repositoryPosition.predictiveSearchAllPostionIdsWithPagination("%"+searchWord+"%");
					if(!positionList.isEmpty()) {
						
						if(!positionList.isEmpty()){
							List<DtoPosition> dtoPosition = new ArrayList<>();
							DtoPosition dtoPosition2=null;
							for (Position position : positionList) {
								dtoPosition2=new DtoPosition(position);
								
								dtoPosition2.setId(position.getId());
								dtoPosition2.setPositionId(position.getPositionId());
								dtoPosition2.setDescription(position.getPositionId()+"  | "+position.getDescription());
								dtoPosition.add(dtoPosition2);
							}
							dtoSearch.setRecords(dtoPosition);
						}

						dtoSearch.setTotalCount(positionList.size());
					}
				
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
}
