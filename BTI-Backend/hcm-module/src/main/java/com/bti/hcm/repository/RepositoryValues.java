package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Values;

@Repository("repositoryValues")
public interface RepositoryValues extends JpaRepository<Values, Integer> {

	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	public Values findByIdAndIsDeleted(int id, boolean deleted);

	
	
	@Query("select p from Values p where (p.miscellaneous.id =:miscId) and p.isDeleted=false")
	public Values findByMiscId(@Param("miscId")int id);
	
	
	
	
	/**
	 * @param id
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	@Query("select p from Values p where (p.miscellaneous.id =:miscId) and p.isDeleted=false")
	public List<Values> findByMiscIdAndPagination(@Param("miscId")int id,Pageable pageable);
	
	
	/**
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Values d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleValuesEntry(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Values d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteAllValuesEntries(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);

	/**
	 * @param deleted
	 * @return
	 */
	public List<Values> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	@Query("select count(*) from Values d where d.isDeleted=false")
	public Integer getCountOfTotalValuesEntries();
	
	@Query("select count(d.miscellaneous.id) from Values d where d.miscellaneous.id =:miscId and d.isDeleted=false")
	public Integer getCountOfTotalValuesEntriesByMiscellaneousId(@Param("miscId") int miscId);

	@Query("select p from Values p where (p.valueId =:id) and p.isDeleted=false")
	public List<Values> findByValueId(@Param("id") String miscId);

	@Query("select d from Values d where (d.valueId like :searchKeyWord) and d.isDeleted=false")
	public List<Values> predictiveValuesAllIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	public List<Values> findByIsDeleted(boolean b, Pageable pageable);

	@Query("select count(*) from Values d where (d.valueId like :searchKeyWord  or d.valueDescription like :searchKeyWord  or d.valueArabicDescription like :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveValuesCodeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);



	public List<Values> findByIsDeleted(boolean b);


	@Query("select p from Values p where (p.miscellaneous.id =:miscId) and p.isDeleted=false")
	public List<Values> findAllMiscId(@Param("miscId")int id);

}
