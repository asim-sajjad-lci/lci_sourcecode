package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10360",indexes = {
        @Index(columnList = "HCMPYAINX")
})
public class BuildPayrollCheckByPayCodes extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMPYAINX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "HCMBULDINX")
	private BuildChecks buildChecks;

	@ManyToOne
	@JoinColumn(name = "PYCDINDX")
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	private PayCode payCode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(BuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}

	public PayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(PayCode payCode) {
		this.payCode = payCode;
	}

}
