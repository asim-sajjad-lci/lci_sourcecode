package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.PayCodeType;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoBasedOnPayCode;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeePayCodeMaintenance;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoPayCodeType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByPayCodes;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryPayCodeType;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceEmployeePayCodeMaintenance ")
public class ServiceEmployeePayCodeMaintenance {

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletResponse method in
	 *              ServiceRetirementFundSetup service
	 */

	static Logger log = Logger.getLogger(ServiceEmployeePayCodeMaintenance.class.getName());

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in
	 *              ServiceRetirementFundSetup service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;

	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;

	@Autowired(required = false)
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletResponse method in
	 *              ServiceRetirementFundSetup service
	 */

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;

	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryPayCodeType repositoryPayCodeType;

	@Autowired(required = false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	RepositoryBuildPayrollCheckByPayCodes repositoryBuildPayrollCheckByPayCodes;

	public DtoEmployeePayCodeMaintenance saveOrUpdate(DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance) {
		try {

			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			EmployeePayCodeMaintenance employeePayCodeMaintenance = null;
			if (dtoEmployeePayCodeMaintenance.getId() != null && dtoEmployeePayCodeMaintenance.getId() > 0) {
				employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
						.findByIdAndIsDeleted(dtoEmployeePayCodeMaintenance.getId(), false);
				employeePayCodeMaintenance.setUpdatedBy(loggedInUserId);
				employeePayCodeMaintenance.setUpdatedDate(new Date());
			} else {
				employeePayCodeMaintenance = new EmployeePayCodeMaintenance();
				employeePayCodeMaintenance.setCreatedDate(new Date());
				Integer rowId = repositoryEmployeePayCodeMaintenance.getCountOfTotalEmployeePayCodeMaintenance();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				employeePayCodeMaintenance.setRowId(increment);
			}

			EmployeeMaster employeeMaster = null;
			if (dtoEmployeePayCodeMaintenance.getEmployeeMaster() != null
					&& dtoEmployeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId() > 0) {
				employeeMaster = repositoryEmployeeMaster
						.findOne(dtoEmployeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId());
			}

			PayCode payCode = null;
			if (dtoEmployeePayCodeMaintenance.getPayCode() != null
					&& dtoEmployeePayCodeMaintenance.getPayCode().getId() > 0) {
				payCode = repositoryPayCode.findOne(dtoEmployeePayCodeMaintenance.getPayCode().getId());
			}

			PayCodeType payCodeType = null;
			if (dtoEmployeePayCodeMaintenance.getPayCodeType() != null
					&& dtoEmployeePayCodeMaintenance.getPayCodeType().getId() > 0) {
				payCodeType = repositoryPayCodeType.findOne(dtoEmployeePayCodeMaintenance.getPayCodeType().getId());
			}
			employeePayCodeMaintenance.setPayCodeType(payCodeType);
			employeePayCodeMaintenance.setPayCode(payCode);
			employeePayCodeMaintenance.setEmployeeMaster(employeeMaster);
			employeePayCodeMaintenance.setBaseOnPayCode(dtoEmployeePayCodeMaintenance.getBaseOnPayCode());
			employeePayCodeMaintenance.setBaseOnPayCodeId(dtoEmployeePayCodeMaintenance.getBaseOnPayCodeId());
			employeePayCodeMaintenance.setAmount(dtoEmployeePayCodeMaintenance.getAmount());
			employeePayCodeMaintenance.setPayFactory(dtoEmployeePayCodeMaintenance.getPayFactory());
			employeePayCodeMaintenance.setPayRate(dtoEmployeePayCodeMaintenance.getPayRate());
			employeePayCodeMaintenance.setUnitOfPay(dtoEmployeePayCodeMaintenance.getUnitOfPay());
			employeePayCodeMaintenance.setPayPeriod(dtoEmployeePayCodeMaintenance.getPayPeriod());
			employeePayCodeMaintenance.setInactive(dtoEmployeePayCodeMaintenance.getInactive());
			employeePayCodeMaintenance.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryEmployeePayCodeMaintenance.saveAndFlush(employeePayCodeMaintenance);

		} catch (Exception e) {
			log.error(e);
		}

		return dtoEmployeePayCodeMaintenance;
	}

	public DtoEmployeePayCodeMaintenance delete(List<Integer> ids) {
		log.info("delete EmployeePayCodeMaintenance Method");
		DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance();
		dtoEmployeePayCodeMaintenance.setDeleteMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_PAY_CODE_MAINTANENANCE_DELETED", false));
		dtoEmployeePayCodeMaintenance.setAssociateMessage(serviceResponse
				.getStringMessageByShortAndIsDeleted("EMPLOYEE_PAY_CODE_MAINTANENANCE_ASSOCIATED", false));
		List<DtoEmployeePayCodeMaintenance> deleteDtoEmployeePayCodeMaintenance = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		List<TransactionEntryDetail> transactionEntryDetailsList = new ArrayList<>();
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse
				.getMessageByShortAndIsDeleted("EMPLOYEE_PAY_CODE_MAINTANENANCE_NOT_DELETE_ID_MESSAGE", false)
				.getMessage());
		try {
			for (Integer planId : ids) {
				EmployeePayCodeMaintenance employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
						.findOne(planId);
				if (employeePayCodeMaintenance != null) {
					// if(employeePayCodeMaintenance.getPayCode().getBuildPayrollCheckByPayCodes().isEmpty())
					// {
					transactionEntryDetailsList = repositoryTransactionEntryDetail.findByTransactionEntry4(
							employeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId(),
							employeePayCodeMaintenance.getPayCode().getId());
					if (transactionEntryDetailsList.isEmpty()) {
						DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance2 = new DtoEmployeePayCodeMaintenance();
						dtoEmployeePayCodeMaintenance.setId(employeePayCodeMaintenance.getId());

						repositoryEmployeePayCodeMaintenance.deleteSingleEmployeePayCodeMaintenance(true,
								loggedInUserId, planId);
						deleteDtoEmployeePayCodeMaintenance.add(dtoEmployeePayCodeMaintenance2);
						dtoEmployeePayCodeMaintenance.setDelete(deleteDtoEmployeePayCodeMaintenance);
					} else {
						inValidDelete = true;
					}

					/*
					 * }else { inValidDelete = true; }
					 */

				} else {
					inValidDelete = true;
				}

			}

			if (inValidDelete) {
				invlidDeleteMessage.replace(invlidDeleteMessage.length() - 1, invlidDeleteMessage.length(), "");
				dtoEmployeePayCodeMaintenance.setMessageType(invlidDeleteMessage.toString());

			}
			if (!inValidDelete) {
				dtoEmployeePayCodeMaintenance.setMessageType("");

			}

		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeePayCodeMaintenance" + " :" + dtoEmployeePayCodeMaintenance.getId());
		return dtoEmployeePayCodeMaintenance;
	}

	public DtoEmployeePayCodeMaintenance getById(int id) {
		DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance();
		try {
			if (id > 0) {
				EmployeePayCodeMaintenance employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
						.findByIdAndIsDeleted(id, false);
				if (employeePayCodeMaintenance != null) {
					dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance(employeePayCodeMaintenance);

					DtoPayCode payCode = new DtoPayCode();
					if (employeePayCodeMaintenance.getPayCode() != null) {
						payCode.setId(employeePayCodeMaintenance.getPayCode().getId());
						payCode.setPayCodeId(employeePayCodeMaintenance.getPayCode().getPayCodeId());
						payCode.setArbicDescription(employeePayCodeMaintenance.getPayCode().getArbicDescription());
						payCode.setBaseOnPayCode(employeePayCodeMaintenance.getPayCode().getBaseOnPayCode());
						payCode.setBaseOnPayCodeAmount(
								employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeAmount());
						payCode.setPayFactor(employeePayCodeMaintenance.getPayCode().getPayFactor());
						payCode.setPayRate(employeePayCodeMaintenance.getPayCode().getPayRate());
						payCode.setUnitofPay(employeePayCodeMaintenance.getPayCode().getUnitofPay());
						payCode.setPayperiod(employeePayCodeMaintenance.getPayCode().getPayperiod());
						payCode.setDescription(employeePayCodeMaintenance.getPayCode().getDescription());
						payCode.setInActive(employeePayCodeMaintenance.getPayCode().isInActive());
						payCode.setBaseOnPayCodeId(employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeId());
						dtoEmployeePayCodeMaintenance.setPayCode(payCode);

					}

					DtoPayCodeType payCodeType = new DtoPayCodeType();
					if (employeePayCodeMaintenance.getPayCodeType() != null) {
						payCodeType.setId(employeePayCodeMaintenance.getPayCodeType().getId());
						payCodeType.setDesc(employeePayCodeMaintenance.getPayCodeType().getDesc());
						payCodeType.setArabicDesc(employeePayCodeMaintenance.getPayCodeType().getArabicDesc());
						dtoEmployeePayCodeMaintenance.setPayCodeType(payCodeType);
					}
					DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
					if (employeePayCodeMaintenance.getEmployeeMaster() != null) {
						employeeMaster.setEmployeeIndexId(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId());
						employeeMaster.setEmployeeId(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeId());
						employeeMaster.setEmployeeBirthDate(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeBirthDate());
						employeeMaster
								.setEmployeeCitizen(employeePayCodeMaintenance.getEmployeeMaster().isEmployeeCitizen());
						employeeMaster.setEmployeeFirstName(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeFirstName());
						employeeMaster.setEmployeeFirstNameArabic(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeFirstNameArabic());
						employeeMaster
								.setEmployeeGender(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeGender());
						employeeMaster.setEmployeeHireDate(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeHireDate());
						employeeMaster.setEmployeeImmigration(
								employeePayCodeMaintenance.getEmployeeMaster().isEmployeeImmigration());
						employeeMaster.setEmployeeInactive(
								employeePayCodeMaintenance.getEmployeeMaster().isEmployeeInactive());
						employeeMaster.setEmployeeInactiveDate(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeInactiveDate());
						employeeMaster.setEmployeeInactiveReason(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeInactiveReason());
						employeeMaster.setEmployeeLastName(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeLastName());
						employeeMaster.setEmployeeLastNameArabic(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeLastNameArabic());
						employeeMaster.setEmployeeLastWorkDate(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeLastWorkDate());
						employeeMaster.setEmployeeMaritalStatus(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeMaritalStatus());
						employeeMaster.setEmployeeMiddleName(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeMiddleName());
						employeeMaster.setEmployeeMiddleNameArabic(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeMiddleNameArabic());
						employeeMaster
								.setEmployeeTitle(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeTitle());
						employeeMaster.setEmployeeTitleArabic(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeTitleArabic());
						employeeMaster
								.setEmployeeType(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeType());
						employeeMaster.setEmployeeUserIdInSystem(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeUserIdInSystem());
						employeeMaster.setEmployeeWorkHourYearly(
								employeePayCodeMaintenance.getEmployeeMaster().getEmployeeWorkHourYearly());
						dtoEmployeePayCodeMaintenance.setEmployeeMaster(employeeMaster);

					}

					dtoEmployeePayCodeMaintenance.setId(employeePayCodeMaintenance.getId());
					dtoEmployeePayCodeMaintenance.setBaseOnPayCodeId(employeePayCodeMaintenance.getBaseOnPayCodeId());
					dtoEmployeePayCodeMaintenance.setBaseOnPayCode(payCode.getBaseOnPayCode());
					dtoEmployeePayCodeMaintenance.setAmount(employeePayCodeMaintenance.getAmount());
					dtoEmployeePayCodeMaintenance.setPayFactory(employeePayCodeMaintenance.getPayFactory());
					dtoEmployeePayCodeMaintenance.setPayRate(employeePayCodeMaintenance.getPayRate());
					dtoEmployeePayCodeMaintenance.setUnitOfPay(employeePayCodeMaintenance.getUnitOfPay());
					dtoEmployeePayCodeMaintenance.setPayPeriod(employeePayCodeMaintenance.getPayPeriod());
					dtoEmployeePayCodeMaintenance.setInactive(employeePayCodeMaintenance.getInactive());
					dtoEmployeePayCodeMaintenance.setBaseOnPayCode(employeePayCodeMaintenance.getBaseOnPayCode());

				}
			} else {
				dtoEmployeePayCodeMaintenance.setMessageType("INVALID_ID");

			}

		} catch (Exception e) {
			log.error(e);
		}

		return dtoEmployeePayCodeMaintenance;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {

		//try {

			log.info("EmployeePayCodeMaintenance Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";
				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

					if (dtoSearch.getSortOn().equals("baseOnPayCode")) {
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
					}

				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}

				dtoSearch.setTotalCount(this.repositoryEmployeePayCodeMaintenance
						.predictiveEmployeePayCodeMaintenanceSearchTotalCount("%" + searchWord + "%"));
				List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = null;
				
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						employeePayCodeMaintenanceList = this.repositoryEmployeePayCodeMaintenance
								.predictiveEmployeePayCodeMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						employeePayCodeMaintenanceList = this.repositoryEmployeePayCodeMaintenance
								.predictiveEmployeePayCodeMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						employeePayCodeMaintenanceList = this.repositoryEmployeePayCodeMaintenance
								.predictiveEmployeePayCodeMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.DESC, condition));
					}

				}
				
				if (employeePayCodeMaintenanceList != null && !employeePayCodeMaintenanceList.isEmpty()) {
					List<DtoEmployeePayCodeMaintenance> dtoEmployeeBenefitMaintenanceList = new ArrayList<>();
					DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance = null;
					for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
						
						dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance(employeePayCodeMaintenance);

						DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
						if (employeePayCodeMaintenance.getEmployeeMaster() != null) {
							employeeMaster.setEmployeeIndexId(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId());
							employeeMaster
									.setEmployeeId(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeId());
							employeeMaster.setEmployeeBirthDate(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeBirthDate());
							employeeMaster.setEmployeeCitizen(
									employeePayCodeMaintenance.getEmployeeMaster().isEmployeeCitizen());
							employeeMaster.setEmployeeFirstName(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeFirstName());
							employeeMaster.setEmployeeFirstNameArabic(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeFirstNameArabic());
							employeeMaster.setEmployeeGender(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeGender());
							employeeMaster.setEmployeeHireDate(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeHireDate());
							employeeMaster.setEmployeeImmigration(
									employeePayCodeMaintenance.getEmployeeMaster().isEmployeeImmigration());
							employeeMaster.setEmployeeInactive(
									employeePayCodeMaintenance.getEmployeeMaster().isEmployeeInactive());
							employeeMaster.setEmployeeInactiveDate(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeInactiveDate());
							employeeMaster.setEmployeeInactiveReason(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeInactiveReason());
							employeeMaster.setEmployeeLastName(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeLastName());
							employeeMaster.setEmployeeLastNameArabic(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeLastNameArabic());
							employeeMaster.setEmployeeLastWorkDate(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeLastWorkDate());
							employeeMaster.setEmployeeMaritalStatus(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeMaritalStatus());
							employeeMaster.setEmployeeMiddleName(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeMiddleName());
							employeeMaster.setEmployeeMiddleNameArabic(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeMiddleNameArabic());
							employeeMaster.setEmployeeTitle(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeTitle());
							employeeMaster.setEmployeeTitleArabic(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeTitleArabic());
							employeeMaster
									.setEmployeeType(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeType());
							employeeMaster.setEmployeeUserIdInSystem(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeUserIdInSystem());
							employeeMaster.setEmployeeWorkHourYearly(
									employeePayCodeMaintenance.getEmployeeMaster().getEmployeeWorkHourYearly());
							dtoEmployeePayCodeMaintenance.setEmployeeMaster(employeeMaster);

						}

						DtoPayCode payCode = new DtoPayCode();
						if (employeePayCodeMaintenance.getPayCode() != null) {
							payCode.setId(employeePayCodeMaintenance.getPayCode().getId());
							payCode.setPayCodeId(employeePayCodeMaintenance.getPayCode().getPayCodeId());
							payCode.setDescription(employeePayCodeMaintenance.getPayCode().getDescription());
							payCode.setArbicDescription(employeePayCodeMaintenance.getPayCode().getArbicDescription());
							payCode.setBaseOnPayCode(employeePayCodeMaintenance.getPayCode().getBaseOnPayCode());
							payCode.setBaseOnPayCodeAmount(
									employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeAmount());
							payCode.setPayFactor(employeePayCodeMaintenance.getPayCode().getPayFactor());
							payCode.setPayRate(employeePayCodeMaintenance.getPayCode().getPayRate());
							payCode.setUnitofPay(employeePayCodeMaintenance.getPayCode().getUnitofPay());
							payCode.setPayperiod(employeePayCodeMaintenance.getPayCode().getPayperiod());
							payCode.setInActive(employeePayCodeMaintenance.getPayCode().isInActive());
							payCode.setBaseOnPayCodeId(employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeId());
							
							payCode.setRoundOf(employeePayCodeMaintenance.getPayCode().getRoundOf());			//ME
							payCode.setPayTypeId(employeePayCodeMaintenance.getPayCode().getTypeField());		//ME 
							
							dtoEmployeePayCodeMaintenance.setPayCode(payCode);

						}

						DtoPayCodeType payCodeType = new DtoPayCodeType();
						if (employeePayCodeMaintenance.getPayCodeType() != null) {
							payCodeType.setId(employeePayCodeMaintenance.getPayCodeType().getId());
							payCodeType.setDesc(employeePayCodeMaintenance.getPayCodeType().getDesc());
							payCodeType.setArabicDesc(employeePayCodeMaintenance.getPayCodeType().getArabicDesc());
							dtoEmployeePayCodeMaintenance.setPayCodeType(payCodeType);
						}
						dtoEmployeePayCodeMaintenance.setPayCodeType(payCodeType);
						dtoEmployeePayCodeMaintenance.setPayCode(payCode);
						dtoEmployeePayCodeMaintenance.setEmployeeMaster(employeeMaster);
						dtoEmployeePayCodeMaintenance.setId(employeePayCodeMaintenance.getId());
						dtoEmployeePayCodeMaintenance.setBaseOnPayCode(employeePayCodeMaintenance.getBaseOnPayCode());
						dtoEmployeePayCodeMaintenance.setAmount(employeePayCodeMaintenance.getAmount());
						dtoEmployeePayCodeMaintenance.setPayFactory(employeePayCodeMaintenance.getPayFactory());
						dtoEmployeePayCodeMaintenance.setPayRate(employeePayCodeMaintenance.getPayRate());
						dtoEmployeePayCodeMaintenance.setUnitOfPay(employeePayCodeMaintenance.getUnitOfPay());
						dtoEmployeePayCodeMaintenance.setPayPeriod(employeePayCodeMaintenance.getPayPeriod());
						dtoEmployeePayCodeMaintenance.setInactive(employeePayCodeMaintenance.getInactive());
						dtoEmployeePayCodeMaintenance
								.setBaseOnPayCodeId(employeePayCodeMaintenance.getBaseOnPayCodeId());

						dtoEmployeeBenefitMaintenanceList.add(dtoEmployeePayCodeMaintenance);
					}
					dtoSearch.setRecords(dtoEmployeeBenefitMaintenanceList);
				}

				log.debug("Search EmployeePayCodeMaintenance Size is:" + dtoSearch.getTotalCount());

			}

//		} catch (Exception e) {
//			log.error(e);
//		}

		return dtoSearch;
	}

	public List<DtoEmployeePayCodeMaintenance> getAllEmployeePaycodeMainteneceDropDownList() {

		log.info("getAllEmployeePaycodeMainteneceDropDownList InActive List  Method");
		List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenanceList = new ArrayList<>();
		try {
			List<EmployeePayCodeMaintenance> list = repositoryEmployeePayCodeMaintenance.findByIsDeleted(false);

			if (list != null && !list.isEmpty()) {
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance : list) {
					DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance();

					dtoEmployeePayCodeMaintenance.setId(employeePayCodeMaintenance.getId());
					dtoEmployeePayCodeMaintenance.setBaseOnPayCode(employeePayCodeMaintenance.getBaseOnPayCode());
					dtoEmployeePayCodeMaintenance.setAmount(employeePayCodeMaintenance.getAmount());
					dtoEmployeePayCodeMaintenance.setPayFactory(employeePayCodeMaintenance.getPayFactory());
					dtoEmployeePayCodeMaintenance.setPayRate(employeePayCodeMaintenance.getPayRate());
					dtoEmployeePayCodeMaintenance.setBaseOnPayCodeId(employeePayCodeMaintenance.getBaseOnPayCodeId());
					DtoPayCode payCode = new DtoPayCode();
					if (employeePayCodeMaintenance.getPayCode() != null) {
						payCode.setId(employeePayCodeMaintenance.getPayCode().getId());
						payCode.setPayCodeId(employeePayCodeMaintenance.getPayCode().getPayCodeId());
						if (employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeAmount() != null
								|| employeePayCodeMaintenance.getPayCode().getPayFactor() != null
								|| employeePayCodeMaintenance.getPayCode().getPayRate() != null) {
							payCode.setAmount(employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeAmount());
							payCode.setPayFactor(employeePayCodeMaintenance.getPayCode().getPayFactor());
							payCode.setPayRate(employeePayCodeMaintenance.getPayCode().getPayRate());
							payCode.setBaseOnPayCodeId(employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeId());
						}

						dtoEmployeePayCodeMaintenance.setPayCode(payCode);
						dtoEmployeePayCodeMaintenanceList.add(dtoEmployeePayCodeMaintenance);
					}
				}
			}
			log.debug("getAllEmployeePaycodeMainteneceDropDownList is:" + dtoEmployeePayCodeMaintenanceList.size());

		} catch (Exception e) {
			log.error(e);
		}

		return dtoEmployeePayCodeMaintenanceList;
	}

	public DtoEmployeePayCodeMaintenance repeatByEmployeeId(int employeeIndexId) {
		log.info("repeatByEmployeeId Method");
		DtoEmployeePayCodeMaintenance dtoPaycode = new DtoEmployeePayCodeMaintenance();
		try {
			List<EmployeePayCodeMaintenance> listpaycode = repositoryEmployeePayCodeMaintenance
					.findByEmployeeId(employeeIndexId);
			if (listpaycode != null && !listpaycode.isEmpty()) {
				dtoPaycode.setIsRepeat(true);

			} else {
				dtoPaycode.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}

		return dtoPaycode;
	}

	public DtoSearch findCodeByEmployeeId(DtoSearch dtoSearch) {
		log.info("repeatByEmployeeId Method");
		List<DtoPayCode> dtoPaycode = new ArrayList<>();

		try {

			if (dtoSearch.getType() == 1) {
				List<EmployeePayCodeMaintenance> list = repositoryEmployeePayCodeMaintenance
						.findByEmployeeId(dtoSearch.getId());
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance : list) {
					DtoPayCode code = new DtoPayCode();

					if (employeePayCodeMaintenance.getPayCode() != null) {
						if (employeePayCodeMaintenance.getInactive() == false) {
							code.setId(employeePayCodeMaintenance.getPayCode().getId());
							code.setPayCodeId(employeePayCodeMaintenance.getPayCode().getPayCodeId());
							code.setPayCodeTypeDesc(employeePayCodeMaintenance.getPayCode().getDescription());
							if (employeePayCodeMaintenance.getAmount() != null) {
								code.setAmount(employeePayCodeMaintenance.getAmount());
								code.setPayFactor(employeePayCodeMaintenance.getPayFactory());
								code.setPayRate(employeePayCodeMaintenance.getPayRate());
								code.setBaseOnPayCodeId(employeePayCodeMaintenance.getBaseOnPayCodeId());
							} else {
								code.setAmount(employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeAmount());
							}

							dtoPaycode.add(code);
						}

					}

				}

			}

			if (dtoSearch.getType() == 2) {
				List<EmployeeDeductionMaintenance> ls = repositoryEmployeeDeductionMaintenance
						.findByEmployeeId(dtoSearch.getId());

				for (EmployeeDeductionMaintenance employeeDeductionMaintenance : ls) {
					DtoPayCode code = new DtoPayCode();
					if (employeeDeductionMaintenance.getDeductionCode() != null) {
						if (employeeDeductionMaintenance.isTransactionRequired() == true) {
							if (employeeDeductionMaintenance.getInactive() == false) {
								code.setId(employeeDeductionMaintenance.getDeductionCode().getId());
								code.setPayCodeId(employeeDeductionMaintenance.getDeductionCode().getDiductionId());
								code.setPayCodeTypeDesc(
										employeeDeductionMaintenance.getDeductionCode().getDiscription());
								if (employeeDeductionMaintenance.getDeductionAmount() != null) {
									code.setAmount(employeeDeductionMaintenance.getDeductionAmount());
									code.setPayRate(employeeDeductionMaintenance.getDeductionPercent());
								} else {
									code.setAmount(employeeDeductionMaintenance.getDeductionCode().getAmount());
								}

								dtoPaycode.add(code);
							}

						}

					}

				}
			}

			if (dtoSearch.getType() == 3) {
				List<EmployeeBenefitMaintenance> ls = repositoryEmployeeBenefitMaintenance
						.findByEmployeeId(dtoSearch.getId());

				for (EmployeeBenefitMaintenance employeeBenefitMaintenance : ls) {
					DtoPayCode code = new DtoPayCode();
					if (employeeBenefitMaintenance.getBenefitCode() != null) {
						if (employeeBenefitMaintenance.isTransactionRequired() == true) {
							if (employeeBenefitMaintenance.getInactive() == false) {
								code.setId(employeeBenefitMaintenance.getBenefitCode().getId());
								code.setPayCodeId(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
								code.setPayCodeTypeDesc(employeeBenefitMaintenance.getBenefitCode().getDesc());
								if (employeeBenefitMaintenance.getBenefitAmount() != null) {
									code.setAmount(employeeBenefitMaintenance.getBenefitAmount());
									code.setPayRate(employeeBenefitMaintenance.getBenefitPercent());
								} else {
									code.setAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
								}

								dtoPaycode.add(code);
							}
						}

					}

				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		dtoSearch.setRecords(dtoPaycode);
		return dtoSearch;
	}

	public DtoBasedOnPayCode findBasedOnCodeByEmployeeId(DtoBasedOnPayCode dtoBasedOnPayCode) {

		List<DtoBasedOnPayCode> dtoEmployeePayCodeMaintenanceList = new ArrayList<>();
		List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance
				.findByEmployeeIdAndCodeIdS(dtoBasedOnPayCode.getEmployeeId(), dtoBasedOnPayCode.getPayCodeId());
		if (employeePayCodeMaintenanceList != null && !employeePayCodeMaintenanceList.isEmpty())
			for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
				DtoBasedOnPayCode dtoBasedOnPayCode1 = new DtoBasedOnPayCode();
				PayCode payCode = repositoryPayCode.findByIdAndIsDeleted(dtoBasedOnPayCode.getPayCodeId(), false);
				if (employeePayCodeMaintenance.getPayCode().getId() == payCode.getId()) {

					dtoBasedOnPayCode1.setId(employeePayCodeMaintenance.getId());
					dtoBasedOnPayCode1.setEmployeeId(employeePayCodeMaintenance.getId());
					dtoBasedOnPayCode1.setBaseOnPayCode(employeePayCodeMaintenance.getBaseOnPayCode());
					dtoBasedOnPayCode1.setPayFactory(employeePayCodeMaintenance.getPayFactory());
					dtoBasedOnPayCode1.setAmount(employeePayCodeMaintenance.getAmount());
					dtoBasedOnPayCode1.setPayRate(employeePayCodeMaintenance.getPayRate());
					dtoBasedOnPayCode1.setBasePayCodeId(employeePayCodeMaintenance.getBaseOnPayCodeId());

				} else {
					dtoBasedOnPayCode1.setBasePayCodeId(employeePayCodeMaintenance.getBaseOnPayCodeId());
					dtoBasedOnPayCode1.setId(employeePayCodeMaintenance.getId());
					dtoBasedOnPayCode1.setPayRate(employeePayCodeMaintenance.getPayRate());
					dtoBasedOnPayCode1.setEmployeeId(employeePayCodeMaintenance.getId());
					dtoBasedOnPayCode1.setBaseOnPayCode(employeePayCodeMaintenance.getBaseOnPayCode());
					dtoBasedOnPayCode1.setPayFactory(employeePayCodeMaintenance.getPayFactory());
					dtoBasedOnPayCode1.setBasePayCodeId(payCode.getId());
					dtoBasedOnPayCode1.setAmount(new java.math.BigDecimal(0));
				}
				dtoEmployeePayCodeMaintenanceList.add(dtoBasedOnPayCode1);
				dtoBasedOnPayCode.setRecords(dtoEmployeePayCodeMaintenanceList);
			}

		return dtoBasedOnPayCode;
	}

	public DtoBasedOnPayCode findBasedOnCodeByEmployeeId1(DtoBasedOnPayCode dtoBasedOnPayCode) {

		List<DtoBasedOnPayCode> dtoEmployeePayCodeMaintenanceList = new ArrayList<>();
		List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance
				.findByEmployeeIds(dtoBasedOnPayCode.getEmployeeId());
		if (employeePayCodeMaintenanceList != null && !employeePayCodeMaintenanceList.isEmpty())
			for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
				DtoBasedOnPayCode dtoBasedOnPayCode1 = new DtoBasedOnPayCode();
				// List<DtoPayCode> dtopayCodeList = new ArrayList<>();
				DtoPayCode dtoPayCode = new DtoPayCode();
				List<PayCode> payCode = repositoryPayCode
						.findAllPayCodeListId1(employeePayCodeMaintenance.getPayCode().getId());
				if (!payCode.isEmpty()) {
					for (PayCode payCode2 : payCode) {
						dtoPayCode = new DtoPayCode();
						dtoPayCode.setPayCodeId(payCode2.getPayCodeId());
						dtoPayCode.setId(payCode2.getId());
						dtoPayCode.setPayCodeId(payCode2.getPayCodeId());
						dtoPayCode.setDescription(payCode2.getDescription());
						dtoPayCode.setAmount(payCode2.getBaseOnPayCodeAmount());
						dtoPayCode.setPayFactor(payCode2.getPayFactor());
						dtoPayCode.setPayRate(payCode2.getPayRate());
						dtoPayCode.setBaseOnPayCodeId(payCode2.getBaseOnPayCodeId());
						dtoPayCode.setBaseOnPayCode(payCode2.getBaseOnPayCode());
						dtoPayCode.setBaseOnPayCodeAmount(payCode2.getBaseOnPayCodeAmount());
						// dtopayCodeList.add(dtoPayCode);
					}
				}

				dtoBasedOnPayCode1.setDtoPayCode(dtoPayCode);
				// dtoBasedOnPayCode1.setDtoPaycodeList(dtoPayCode);
				dtoBasedOnPayCode1.setId(employeePayCodeMaintenance.getId());
				dtoBasedOnPayCode1.setEmployeeId(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId());
				dtoBasedOnPayCode1.setBaseOnPayCode(employeePayCodeMaintenance.getBaseOnPayCode());
				dtoBasedOnPayCode1.setPayFactory(employeePayCodeMaintenance.getPayFactory());
				dtoBasedOnPayCode1.setAmount(employeePayCodeMaintenance.getAmount());
				dtoBasedOnPayCode1.setPayRate(employeePayCodeMaintenance.getPayRate());
				dtoBasedOnPayCode1.setBasePayCodeId(employeePayCodeMaintenance.getBaseOnPayCodeId());
				dtoBasedOnPayCode1
						.setEmployeeFirstName(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeFirstName());
				dtoBasedOnPayCode1.setBasePayCodeId(employeePayCodeMaintenance.getBaseOnPayCodeId());
				dtoBasedOnPayCode1.setId(employeePayCodeMaintenance.getId());
				dtoBasedOnPayCode1.setPayRate(employeePayCodeMaintenance.getPayRate());
				dtoBasedOnPayCode1.setEmployeeId(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId());
				dtoBasedOnPayCode1.setBaseOnPayCode(employeePayCodeMaintenance.getBaseOnPayCode());
				dtoBasedOnPayCode1.setPayFactory(employeePayCodeMaintenance.getPayFactory());
				dtoBasedOnPayCode1
						.setEmployeeFirstName(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeFirstName());
				dtoBasedOnPayCode1.setAmount(new java.math.BigDecimal(0));
				dtoEmployeePayCodeMaintenanceList.add(dtoBasedOnPayCode1);
				dtoBasedOnPayCode.setRecords(dtoEmployeePayCodeMaintenanceList);
			}

		return dtoBasedOnPayCode;
	}

	public DtoBasedOnPayCode checkDuplicateByEmployeeIdAndPayCodeId(DtoBasedOnPayCode dtoBasedOnPayCode) {

		List<EmployeePayCodeMaintenance> employeePayCodeMaintenancesList = repositoryEmployeePayCodeMaintenance
				.findByEmployeeIdAndCodeIdes(dtoBasedOnPayCode.getEmployeeId(), dtoBasedOnPayCode.getPayCodeId());
		if (employeePayCodeMaintenancesList != null && !employeePayCodeMaintenancesList.isEmpty()) {
			dtoBasedOnPayCode.setIsRepeat(true);
		} else {
			dtoBasedOnPayCode.setIsRepeat(false);
		}
		return dtoBasedOnPayCode;
	}

}
