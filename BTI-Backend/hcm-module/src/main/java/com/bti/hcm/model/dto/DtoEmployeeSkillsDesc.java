package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.EmployeeSkillsDesc;

public class DtoEmployeeSkillsDesc extends DtoBase{

	private Integer id;
	private DtoEmployeeSkills employeeSkills;
	private Integer skillSetSequence;
	private DtoSkillSteup skillsSetup;
	private boolean obtained;
	private boolean isSelected;
	private Integer proficiency;
	private boolean required;
	private String skillComment;
	private Date skillExpirationDate;
	private DtoEmployeeMasterHcm dtoEmployeeMaster;
	private DtoSkillSetSteup dtoSkillSetSteup;
	private List<DtoEmployeeSkillsDesc> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public DtoEmployeeSkills getEmployeeSkills() {
		return employeeSkills;
	}
	public void setEmployeeSkills(DtoEmployeeSkills employeeSkills) {
		this.employeeSkills = employeeSkills;
	}
	public Integer getSkillSetSequence() {
		return skillSetSequence;
	}
	public void setSkillSetSequence(Integer skillSetSequence) {
		this.skillSetSequence = skillSetSequence;
	}
	public DtoSkillSteup getSkillsSetup() {
		return skillsSetup;
	}
	public void setSkillsSetup(DtoSkillSteup skillsSetup) {
		this.skillsSetup = skillsSetup;
	}
	public boolean isObtained() {
		return obtained;
	}
	public void setObtained(boolean obtained) {
		this.obtained = obtained;
	}
	public Integer getProficiency() {
		return proficiency;
	}
	public void setProficiency(Integer proficiency) {
		this.proficiency = proficiency;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public String getSkillComment() {
		return skillComment;
	}
	public void setSkillComment(String skillComment) {
		this.skillComment = skillComment;
	}
	public Date getSkillExpirationDate() {
		return skillExpirationDate;
	}
	public void setSkillExpirationDate(Date skillExpirationDate) {
		this.skillExpirationDate = skillExpirationDate;
	}
	public List<DtoEmployeeSkillsDesc> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoEmployeeSkillsDesc> delete) {
		this.delete = delete;
	}
	
	public DtoEmployeeSkillsDesc() {
	
	}
	
	public DtoEmployeeSkillsDesc(EmployeeSkillsDesc desc ) {
		
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public DtoEmployeeMasterHcm getDtoEmployeeMaster() {
		return dtoEmployeeMaster;
	}
	public void setDtoEmployeeMaster(DtoEmployeeMasterHcm dtoEmployeeMaster) {
		this.dtoEmployeeMaster = dtoEmployeeMaster;
	}
	public DtoSkillSetSteup getDtoSkillSetSteup() {
		return dtoSkillSetSteup;
	}
	public void setDtoSkillSetSteup(DtoSkillSetSteup dtoSkillSetSteup) {
		this.dtoSkillSetSteup = dtoSkillSetSteup;
	}
	
	
	
	
}
