package com.bti.hcm.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="hr40110")
public class MiscellaneousHeader extends HcmBaseEntity{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="miscindx")
	private int miscellaneousIndex;
	
	@Column(name="codeid")
	private int codeId;
	
	@Column(name="codename")
	private String codeName;
	
	@Column(name="codearname")
	private String codeArName;
	
	@Column(name="dimension")
	private int dimention;
	
	@OneToMany
	@JoinColumn(name="miscindx")
	List<MiscellaneousDetails> miscellaneousDetails;

	public int getMiscellaneousIndex() {
		return miscellaneousIndex;
	}

	public void setMiscellaneousIndex(int miscellaneousIndex) {
		this.miscellaneousIndex = miscellaneousIndex;
	}

	public int getCodeId() {
		return codeId;
	}

	public void setCodeId(int codeId) {
		this.codeId = codeId;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeArName() {
		return codeArName;
	}

	public void setCodeArName(String codeArName) {
		this.codeArName = codeArName;
	}

	public int getDimention() {
		return dimention;
	}

	public void setDimention(int dimention) {
		this.dimention = dimention;
	}

	public List<MiscellaneousDetails> getMiscellaneousDetails() {
		return miscellaneousDetails;
	}

	public void setMiscellaneousDetails(List<MiscellaneousDetails> miscellaneousDetails) {
		this.miscellaneousDetails = miscellaneousDetails;
	}
	
	
}
