package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.VATSetup;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoVatSetup extends DtoBase {

	private Integer id;
	private String vatScheduleId;
	private String vatDescription;
	private String vatDescriptionArabic;
	private Integer vatSeriesType;
	private Integer vatIdNumber;
	private Long accountTableRowIndex;
	private Integer vatBasedOn;
	private BigDecimal percentageBasedOn;
	private BigDecimal minimumVatAmt;
	private BigDecimal maximumVatAmt;
	private BigDecimal ytdTotalSales;
	private BigDecimal ytdTotalTaxableSales;
	private BigDecimal ytdTotalSalesTaxes;
	private BigDecimal lastYearTotalSales;
	private BigDecimal lastYearTotalTaxableSales;
	private BigDecimal lastYearTotalSalesTaxes;
	private List<DtoVatSetup> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVatScheduleId() {
		return vatScheduleId;
	}

	public void setVatScheduleId(String vatScheduleId) {
		this.vatScheduleId = vatScheduleId;
	}

	public String getVatDescription() {
		return vatDescription;
	}

	public void setVatDescription(String vatDescription) {
		this.vatDescription = vatDescription;
	}

	public String getVatDescriptionArabic() {
		return vatDescriptionArabic;
	}

	public void setVatDescriptionArabic(String vatDescriptionArabic) {
		this.vatDescriptionArabic = vatDescriptionArabic;
	}

	public Integer getVatSeriesType() {
		return vatSeriesType;
	}

	public void setVatSeriesType(Integer vatSeriesType) {
		this.vatSeriesType = vatSeriesType;
	}

	public Integer getVatIdNumber() {
		return vatIdNumber;
	}

	public void setVatIdNumber(Integer vatIdNumber) {
		this.vatIdNumber = vatIdNumber;
	}

	public Long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(Long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Integer getVatBasedOn() {
		return vatBasedOn;
	}

	public void setVatBasedOn(Integer vatBasedOn) {
		this.vatBasedOn = vatBasedOn;
	}

	public BigDecimal getPercentageBasedOn() {
		return percentageBasedOn;
	}

	public void setPercentageBasedOn(BigDecimal percentageBasedOn) {
		this.percentageBasedOn = percentageBasedOn;
	}

	public BigDecimal getMinimumVatAmt() {
		return minimumVatAmt;
	}

	public void setMinimumVatAmt(BigDecimal minimumVatAmt) {
		this.minimumVatAmt = minimumVatAmt;
	}

	public BigDecimal getMaximumVatAmt() {
		return maximumVatAmt;
	}

	public void setMaximumVatAmt(BigDecimal maximumVatAmt) {
		this.maximumVatAmt = maximumVatAmt;
	}

	public BigDecimal getYtdTotalSales() {
		return ytdTotalSales;
	}

	public void setYtdTotalSales(BigDecimal ytdTotalSales) {
		this.ytdTotalSales = ytdTotalSales;
	}

	public BigDecimal getYtdTotalTaxableSales() {
		return ytdTotalTaxableSales;
	}

	public void setYtdTotalTaxableSales(BigDecimal ytdTotalTaxableSales) {
		this.ytdTotalTaxableSales = ytdTotalTaxableSales;
	}

	public BigDecimal getYtdTotalSalesTaxes() {
		return ytdTotalSalesTaxes;
	}

	public void setYtdTotalSalesTaxes(BigDecimal ytdTotalSalesTaxes) {
		this.ytdTotalSalesTaxes = ytdTotalSalesTaxes;
	}

	public BigDecimal getLastYearTotalSales() {
		return lastYearTotalSales;
	}

	public void setLastYearTotalSales(BigDecimal lastYearTotalSales) {
		this.lastYearTotalSales = lastYearTotalSales;
	}

	public BigDecimal getLastYearTotalTaxableSales() {
		return lastYearTotalTaxableSales;
	}

	public void setLastYearTotalTaxableSales(BigDecimal lastYearTotalTaxableSales) {
		this.lastYearTotalTaxableSales = lastYearTotalTaxableSales;
	}

	public BigDecimal getLastYearTotalSalesTaxes() {
		return lastYearTotalSalesTaxes;
	}

	public void setLastYearTotalSalesTaxes(BigDecimal lastYearTotalSalesTaxes) {
		this.lastYearTotalSalesTaxes = lastYearTotalSalesTaxes;
	}

	public DtoVatSetup() {

	}

	public DtoVatSetup(VATSetup vatSetup) {
		this.vatScheduleId = vatSetup.getVatScheduleId();

	}

	public List<DtoVatSetup> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoVatSetup> delete) {
		this.delete = delete;
	}

}
