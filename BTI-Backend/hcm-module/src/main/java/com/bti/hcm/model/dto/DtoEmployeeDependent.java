package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.EmployeeDependent;

public class DtoEmployeeDependent extends DtoBase{
	private Integer id;
	private int sequence;
	private String empFistName;
	private String empMidleName;
	private String empLastName;
	private Integer age;
	private String completeName;
	private String comments;
	private short gender;
	private Date dateOfBirth;
	private String phoneNumber;
	private String workNumber;
	private String address;

	private List<DtoEmployeeDependent> listEmployeeDependent;

	private DtoEmployeeMaster employeeMaster;
	private DtoEmployeeDependents employeeDependents;

	private List<DtoEmployeeDependent> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getEmpFistName() {
		return empFistName;
	}

	public void setEmpFistName(String empFistName) {
		this.empFistName = empFistName;
	}

	public String getEmpMidleName() {
		return empMidleName;
	}

	public void setEmpMidleName(String empMidleName) {
		this.empMidleName = empMidleName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public short getGender() {
		return gender;
	}

	public void setGender(short gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getWorkNumber() {
		return workNumber;
	}

	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/*
	 * public HrCity getCity() { return city; }
	 * 
	 * public void setCity(HrCity city) { this.city = city; }
	 * 
	 * public HrState getState() { return state; }
	 * 
	 * public void setState(HrState state) { this.state = state; }
	 * 
	 * public HrCountry getCountry() { return country; }
	 * 
	 * public void setCountry(HrCountry country) { this.country = country; }
	 */

	public DtoEmployeeDependent(EmployeeDependent employeeDependent) {
	}

	public DtoEmployeeDependent() {

	}

	public List<DtoEmployeeDependent> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeeDependent> delete) {
		this.delete = delete;
	}

	public DtoEmployeeDependents getEmployeeDependents() {
		return employeeDependents;
	}

	public void setEmployeeDependents(DtoEmployeeDependents employeeDependents) {
		this.employeeDependents = employeeDependents;
	}

	public List<DtoEmployeeDependent> getListEmployeeDependent() {
		return listEmployeeDependent;
	}

	public void setListEmployeeDependent(List<DtoEmployeeDependent> listEmployeeDependent) {
		this.listEmployeeDependent = listEmployeeDependent;
	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	
	public String getCompleteName() {
		return completeName;
	}

	public void setCompleteName(String completeName) {
		this.completeName = completeName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
