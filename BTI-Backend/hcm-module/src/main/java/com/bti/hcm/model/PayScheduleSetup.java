/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the PayScheduleSetup database table.
 * Name of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40905",indexes = {
        @Index(columnList = "PYSCHDINDX")
})
@NamedQuery(name = "PayScheduleSetup.findAll", query = "SELECT p FROM PayScheduleSetup p")
public class PayScheduleSetup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PYSCHDINDX")
	private int payScheduleIndexId;

	@Column(name = "PYSCHDID", columnDefinition = "char(15)")
	private String payScheduleId;

	@Column(name = "PYSCHDDSCR", columnDefinition = "char(31)")
	private String payScheduleDescription;

	@Column(name = "PYSCHDDSCRA", columnDefinition = "char(61)")
	private String payScheduleDescriptionArabic;

	@Column(name = "PYSCHDPYPER")
	private short paySchedulePayPeriod;

	@Column(name = "PYSCHDBGDT")
	private Date payScheduleBeggingDate;

	@Column(name = "PYSCHDYR")
	private int payScheduleYear;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payScheduleSetup")
	@Where(clause = "is_deleted = false")
	private List<PayScheduleSetupLocation> payScheduleSetupLocation;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payScheduleSetup")
	@Where(clause = "is_deleted = false")
	private List<PayScheduleSetupPosition> payScheduleSetupPosition;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payScheduleSetup")
	@Where(clause = "is_deleted = false")
	private List<PayScheduleSetupDepartment> payScheduleSetupDepartment;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payScheduleSetup")
	@Where(clause = "is_deleted = false")
	private List<PayScheduleSetupEmployee> payScheduleSetupEmployee;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payScheduleSetup")
	@Where(clause = "is_deleted = false")
	private List<PayScheduleSetupSchedulePeriods> payScheduleSetupSchedulePeriods;

	public int getPayScheduleIndexId() {
		return payScheduleIndexId;
	}

	public void setPayScheduleIndexId(int payScheduleIndexId) {
		this.payScheduleIndexId = payScheduleIndexId;
	}

	public String getPayScheduleId() {
		return payScheduleId;
	}

	public void setPayScheduleId(String payScheduleId) {
		this.payScheduleId = payScheduleId;
	}

	public String getPayScheduleDescription() {
		return payScheduleDescription;
	}

	public void setPayScheduleDescription(String payScheduleDescription) {
		this.payScheduleDescription = payScheduleDescription;
	}

	public String getPayScheduleDescriptionArabic() {
		return payScheduleDescriptionArabic;
	}

	public void setPayScheduleDescriptionArabic(String payScheduleDescriptionArabic) {
		this.payScheduleDescriptionArabic = payScheduleDescriptionArabic;
	}

	public short getPaySchedulePayPeriod() {
		return paySchedulePayPeriod;
	}

	public void setPaySchedulePayPeriod(short paySchedulePayPeriod) {
		this.paySchedulePayPeriod = paySchedulePayPeriod;
	}

	public Date getPayScheduleBeggingDate() {
		return payScheduleBeggingDate;
	}

	public void setPayScheduleBeggingDate(Date payScheduleBeggingDate) {
		this.payScheduleBeggingDate = payScheduleBeggingDate;
	}

	public int getPayScheduleYear() {
		return payScheduleYear;
	}

	public void setPayScheduleYear(int payScheduleYear) {
		this.payScheduleYear = payScheduleYear;
	}

	public List<PayScheduleSetupLocation> getPayScheduleSetupLocation() {
		return payScheduleSetupLocation;
	}

	public void setPayScheduleSetupLocation(List<PayScheduleSetupLocation> payScheduleSetupLocation) {
		this.payScheduleSetupLocation = payScheduleSetupLocation;
	}

	public List<PayScheduleSetupPosition> getPayScheduleSetupPosition() {
		return payScheduleSetupPosition;
	}

	public void setPayScheduleSetupPosition(List<PayScheduleSetupPosition> payScheduleSetupPosition) {
		this.payScheduleSetupPosition = payScheduleSetupPosition;
	}

	public List<PayScheduleSetupDepartment> getPayScheduleSetupDepartment() {
		return payScheduleSetupDepartment;
	}

	public void setPayScheduleSetupDepartment(List<PayScheduleSetupDepartment> payScheduleSetupDepartment) {
		this.payScheduleSetupDepartment = payScheduleSetupDepartment;
	}

	public List<PayScheduleSetupEmployee> getPayScheduleSetupEmployee() {
		return payScheduleSetupEmployee;
	}

	public void setPayScheduleSetupEmployee(List<PayScheduleSetupEmployee> payScheduleSetupEmployee) {
		this.payScheduleSetupEmployee = payScheduleSetupEmployee;
	}

	public List<PayScheduleSetupSchedulePeriods> getPayScheduleSetupSchedulePeriods() {
		return payScheduleSetupSchedulePeriods;
	}

	public void setPayScheduleSetupSchedulePeriods(
			List<PayScheduleSetupSchedulePeriods> payScheduleSetupSchedulePeriods) {
		this.payScheduleSetupSchedulePeriods = payScheduleSetupSchedulePeriods;
	}

}
