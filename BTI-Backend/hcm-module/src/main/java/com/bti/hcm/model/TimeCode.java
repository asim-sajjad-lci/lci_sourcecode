package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40204",indexes = {
        @Index(columnList = "TMECDINDX")
})
public class TimeCode extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TMECDINDX")
	private int id;
	
	@Column(name = "TMECDID",length= 15)
	private String timeCodeId;
	
	@Column(name = "TMECDINCT")
	private boolean inActive;
	
	@Column(name = "TMECDDSCR",length = 31)
	private String desc;
	
	@Column(name = "TMECDDSCRA",length = 61)
	private String arbicDesc;
	
	@Column(name = "ACCRPERD")
	private Short accrualPeriod;
	
	@Column(name = "TMECDTMTY")
	private Short timeType;
	
	@ManyToOne
	@JoinColumn(name = "ACCSCHINDX")
	private AccrualSchedule accrualSchedule;
	
	@ManyToOne
	@JoinColumn(name = "PYCDINDX")
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	private PayCode payCode;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTimeCodeId() {
		return timeCodeId;
	}

	public void setTimeCodeId(String timeCodeId) {
		this.timeCodeId = timeCodeId;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public Short getAccrualPeriod() {
		return accrualPeriod;
	}

	public void setAccrualPeriod(Short accrualPeriod) {
		this.accrualPeriod = accrualPeriod;
	}

	public Short getTimeType() {
		return timeType;
	}

	public void setTimeType(Short timeType) {
		this.timeType = timeType;
	}

	public AccrualSchedule getAccrualSchedule() {
		return accrualSchedule;
	}

	public void setAccrualSchedule(AccrualSchedule accrualSchedule) {
		this.accrualSchedule = accrualSchedule;
	}

	public PayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(PayCode payCode) {
		this.payCode = payCode;
	}
	
	
}
