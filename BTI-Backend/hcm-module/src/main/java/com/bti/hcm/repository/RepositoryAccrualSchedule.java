package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AccrualSchedule;

@Repository("repositoryAccrualSchedule")
public interface RepositoryAccrualSchedule extends JpaRepository<AccrualSchedule, Integer> {

	AccrualSchedule findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from AccrualSchedule b where b.isDeleted=false")
	Integer getCountOfTotalAccrualSchedule();

	List<AccrualSchedule> findByIsDeleted(boolean b, Pageable pageable);

	List<AccrualSchedule> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update AccrualSchedule b set b.isDeleted =:deleted ,b.updatedBy =:updateById where b.id =:id ")
	void deleteSingleAccrualSchedule(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	List<AccrualSchedule> findByScheduleId(String scheduleId);

	@Query("select count(*) from AccrualSchedule a where (a.scheduleId LIKE :searchKeyWord or a.desc LIKE :searchKeyWord or a.arbicDesc LIKE :searchKeyWord) and a.isDeleted=false")
	Integer predictiveAccrualSheduleSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select a from AccrualSchedule a where (a.scheduleId LIKE :searchKeyWord or a.desc LIKE :searchKeyWord or a.arbicDesc LIKE :searchKeyWord) and a.isDeleted=false")
	List<AccrualSchedule> predictiveAccrualSheduleSearchWithPagination(@Param("searchKeyWord")String searchKeyWord, Pageable pageable);

	@Query("select d from AccrualSchedule d where (d.scheduleId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<AccrualSchedule> predictiveAccrualScheduleIdSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Query("select count(*) from AccrualSchedule a ")
	public Integer getCountOfTotalAccrualSchedules();

}
