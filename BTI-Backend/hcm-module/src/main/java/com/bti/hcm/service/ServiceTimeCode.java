package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.AccrualSchedule;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.TimeCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTimeCode;
import com.bti.hcm.repository.RepositoryAccrualSchedule;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryTimeCode;

@Service("serviceTimeCode")
public class ServiceTimeCode {


	/**
	 * @Description LOGGER use for put a logger in TimeCode Service
	 */
	static Logger log = Logger.getLogger(ServiceTimeCode.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */
	@Autowired
	ServiceResponse response;
	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in TimeCode service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	
	/**
	 * @Description RepositoryTimeCode Autowired here using annotation of spring for access of RepositoryTimeCode method in TimeCode service
	 */
	@Autowired(required=false)
	RepositoryTimeCode repositoryTimeCode;
	
	/**
	 * @Description RepositoryPayCode Autowired here using annotation of spring for access of RepositoryTimeCode method in PayCode service
	 */
	@Autowired(required=false)
	RepositoryPayCode repositoryPayCode;
	
	@Autowired(required=false)
	RepositoryAccrualSchedule repositoryAccrualSchedule;
	
	/**
	 * @param dtoTimeCode
	 * @return
	 */
	public DtoTimeCode saveOrUpdate(DtoTimeCode dtoTimeCode) {
		log.info("saveOrUpdate TimeCode Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		TimeCode timeCode = null;
		if (dtoTimeCode.getId() != null && dtoTimeCode.getId() > 0) {
			
			timeCode = repositoryTimeCode.findByIdAndIsDeleted(dtoTimeCode.getId(), false);
			timeCode.setUpdatedBy(loggedInUserId);
			timeCode.setUpdatedDate(new Date());
		} else {
			timeCode = new TimeCode();
			timeCode.setCreatedDate(new Date());
			Integer rowId = repositoryTimeCode.getCountOfTotalTimeCode();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			timeCode.setRowId(increment);
		}
		PayCode payCode =null;
		if(dtoTimeCode.getPayPrimaryId()!=null && dtoTimeCode.getPayPrimaryId()>0) {
			 payCode = repositoryPayCode.findOne(dtoTimeCode.getPayPrimaryId());
		}
		
		AccrualSchedule accrualSchedule = null;
		if(dtoTimeCode.getSchedulePrimaryId()!=null&&dtoTimeCode.getSchedulePrimaryId()>0) {
			accrualSchedule = repositoryAccrualSchedule.findOne(dtoTimeCode.getSchedulePrimaryId());
		}
		
		timeCode.setAccrualSchedule(accrualSchedule);
		timeCode.setPayCode(payCode);
		timeCode.setTimeType(dtoTimeCode.getTimeType());
		timeCode.setAccrualPeriod(dtoTimeCode.getAccrualPeriod());
		timeCode.setTimeCodeId(dtoTimeCode.getTimeCodeId());
		timeCode.setArbicDesc(dtoTimeCode.getArbicDesc());
		timeCode.setDesc(dtoTimeCode.getDesc());
		timeCode.setInActive(dtoTimeCode.isInActive());
		timeCode = repositoryTimeCode.saveAndFlush(timeCode);
		log.debug("Time Code is:"+timeCode.getId());
		return dtoTimeCode;
	
	}
	
	/**
	 * @param ids
	 * @return
	 */
	public DtoTimeCode delete(List<Integer> ids) {
		log.info("delete TimeCode Method");
		DtoTimeCode dtoTimeCode = new DtoTimeCode();
		dtoTimeCode.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_DELETED", false));
		dtoTimeCode.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_ASSOCIATED", false));
		List<DtoTimeCode> deletePayCode = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer timeCodeId : ids) {
				TimeCode timeCode = repositoryTimeCode.findOne(timeCodeId);
				if(timeCode!=null) {
					DtoTimeCode dtoTimeCode1 = new DtoTimeCode();
					dtoTimeCode1.setId(timeCodeId);
					repositoryTimeCode.deleteSingleTimeCode(true, loggedInUserId, timeCodeId);
					deletePayCode.add(dtoTimeCode1);
				}else {
					inValidDelete = true;
				}
			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoTimeCode.setMessageType(invlidDeleteMessage.toString());
			}
			if(!inValidDelete){
				dtoTimeCode.setMessageType("");
				
			}
			dtoTimeCode.setDelete(deletePayCode);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete TimeCode :"+dtoTimeCode.getId());
		return dtoTimeCode;
	}
	
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search TimeCode Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				if(dtoSearch.getSortOn().equals("timeCodeId") || dtoSearch.getSortOn().equals("desc") || dtoSearch.getSortOn().equals("arbicDesc") || dtoSearch.getSortOn().equals("accrualSchedule")
						) {
					condition = dtoSearch.getSortOn();
				
			}else {
				condition = "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
			
		}
			dtoSearch.setTotalCount(this.repositoryTimeCode.predictiveTimeCodeSearchTotalCount("%"+searchWord+"%"));
			List<TimeCode> payCodeList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					payCodeList = this.repositoryTimeCode.predictiveTimeCodeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					payCodeList = this.repositoryTimeCode.predictiveTimeCodeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					payCodeList = this.repositoryTimeCode.predictiveTimeCodeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
		
			if(payCodeList != null && !payCodeList.isEmpty()){
				List<DtoTimeCode> dtoTimeCodeList = new ArrayList<>();
				DtoTimeCode dtoTimeCode=null;
				for (TimeCode payCode : payCodeList) {
					dtoTimeCode = new DtoTimeCode(payCode);
					dtoTimeCode.setId(payCode.getId());
					if(payCode.getAccrualSchedule()!=null && payCode.getAccrualSchedule().getScheduleId()!=null) {
						//dtoTimeCode.setScheduleId(Integer.parseInt(payCode.getAccrualSchedule().getScheduleId()));
						dtoTimeCode.setDesc(payCode.getAccrualSchedule().getDesc());
						dtoTimeCode.setSchedulePrimaryId(payCode.getAccrualSchedule().getId());
						dtoTimeCode.setScheduleIds(payCode.getAccrualSchedule().getScheduleId());
					}
					
					
					if(payCode.getPayCode()!=null && payCode.getPayCode().getPayCodeId()!=null) {
						dtoTimeCode.setPayCodeId(payCode.getPayCode().getPayCodeId());
						dtoTimeCode.setPayPrimaryId(payCode.getPayCode().getId());
					}
					dtoTimeCode.setTimeType(payCode.getTimeType());
					dtoTimeCode.setAccrualPeriod(payCode.getAccrualPeriod());
					dtoTimeCode.setTimeCodeId(payCode.getTimeCodeId());
					dtoTimeCode.setArbicDesc(payCode.getArbicDesc());
					dtoTimeCode.setDesc(payCode.getDesc());
					dtoTimeCode.setInActive(payCode.isInActive());
					dtoTimeCode.setTimeCodeId(payCode.getTimeCodeId());
					dtoTimeCodeList.add(dtoTimeCode);
				}
				dtoSearch.setRecords(dtoTimeCodeList);
			}
		}
		log.debug("Search TimeCode Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	public DtoTimeCode getById(int id) {
		log.info("getById Method");
		DtoTimeCode dtoTimeCode = new DtoTimeCode();
		if (id > 0) {
			TimeCode payCode = repositoryTimeCode.findByIdAndIsDeleted(id, false);
			if (payCode != null) {
				dtoTimeCode = new DtoTimeCode(payCode);
				
				if(payCode.getAccrualSchedule()!=null && payCode.getAccrualSchedule().getScheduleId()!=null) {
					//dtoTimeCode.setScheduleId(Integer.parseInt(payCode.getAccrualSchedule().getScheduleId()));
					dtoTimeCode.setSchedulePrimaryId(payCode.getAccrualSchedule().getId());
					dtoTimeCode.setScheduleIds(payCode.getAccrualSchedule().getScheduleId());
				}
				
				if(payCode.getPayCode()!=null && payCode.getPayCode().getPayCodeId()!=null) {
					dtoTimeCode.setPayCodeId(payCode.getPayCode().getPayCodeId());
					dtoTimeCode.setId(payCode.getPayCode().getId());
				}
				
				if(payCode.getPayCode()!=null && payCode.getPayCode().getPayCodeId()!=null) {
					dtoTimeCode.setPayCodeId(payCode.getPayCode().getPayCodeId());
					dtoTimeCode.setPayPrimaryId(payCode.getPayCode().getId());
				}
				dtoTimeCode.setTimeType(payCode.getTimeType());
				dtoTimeCode.setAccrualPeriod(payCode.getAccrualPeriod());
				dtoTimeCode.setTimeCodeId(payCode.getTimeCodeId());
				dtoTimeCode.setArbicDesc(payCode.getArbicDesc());
				dtoTimeCode.setDesc(payCode.getDesc());
				dtoTimeCode.setInActive(payCode.isInActive());
				dtoTimeCode.setId(payCode.getId());
			} else {
				dtoTimeCode.setMessageType("TIME_CODE_NOT_GETTING");

			}
		} else {
			dtoTimeCode.setMessageType("INVALID_TIME_CODE_ID");

		}
		log.debug("payCode By Id is:"+dtoTimeCode.getId());
		return dtoTimeCode;
	}
	
	/**
	 * @param timeCodeId
	 * @return
	 */
	public DtoTimeCode repeatByTimeCodeId(String timeCodeId) {
		log.info("repeatByTimeCodeId Method");
		DtoTimeCode dtoPayCode = new DtoTimeCode();
		try {
			List<TimeCode> timeCode=repositoryTimeCode.findByTimeCodeId(timeCodeId);
			if(timeCode!=null && !timeCode.isEmpty()) {
				dtoPayCode.setIsRepeat(true);
			}else {
				dtoPayCode.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPayCode;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchTimeCodeId(DtoSearch dtoSearch) {
		log.info("searchTimeCodeId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			List<String> timeCodeIdList =null;
			
				timeCodeIdList = this.repositoryTimeCode.predictiveTimeCodeSearchWithPagination("%"+searchWord+"%");
				if(!timeCodeIdList.isEmpty()) {
					dtoSearch.setTotalCount(timeCodeIdList.size());
					dtoSearch.setRecords(timeCodeIdList.size());
				}
			dtoSearch.setIds(timeCodeIdList);
		}
		
		return dtoSearch;
	}
	
	public List<DtoTimeCode> getAllTimeCodeInActiveList() {
		log.info("getAllTimeCodeInActiveList  Method");
		List<DtoTimeCode> dtoTimeCodeList = new ArrayList<>();
		try {
			List<TimeCode> list = repositoryTimeCode.findByIsDeletedAndInActive(false, false);
			if (list != null && !list.isEmpty()) {
				for (TimeCode timeCode : list) {
					DtoTimeCode dtoTimeCode = new DtoTimeCode();
					dtoTimeCode.setId(timeCode.getId());
					dtoTimeCode.setDesc(timeCode.getTimeCodeId()+" | "+timeCode.getDesc());
					dtoTimeCode.setTimeCodeId(timeCode.getTimeCodeId());
					dtoTimeCode.setInActive(timeCode.isInActive());
					dtoTimeCodeList.add(dtoTimeCode);
				}
			}
			log.debug("DeducaitonCode is:" +dtoTimeCodeList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoTimeCodeList;
	}
	
	
	public DtoSearch searchAllTimeCodeId(DtoSearch dtoSearch) {
		log.info("searchAllTimeCodeId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			List<TimeCode> timeCodeList =null;
			
				timeCodeList = this.repositoryTimeCode.predictivesearchAllTimeCodeIdWithPagination("%"+searchWord+"%");
				if(!timeCodeList.isEmpty()) {
	
					if(!timeCodeList.isEmpty()){
						List<DtoTimeCode> dtoTimeCode = new ArrayList<>();
						DtoTimeCode dtoTimeCode1=null;
						for (TimeCode timeCode : timeCodeList) {
							dtoTimeCode1=new DtoTimeCode(timeCode);
							dtoTimeCode1.setId(timeCode.getId());
							dtoTimeCode1.setTimeCodeId(timeCode.getTimeCodeId());
							dtoTimeCode1.setDesc(timeCode.getTimeCodeId()+"  |  "+timeCode.getDesc());
		                    dtoTimeCode.add(dtoTimeCode1);
						}
						dtoSearch.setRecords(dtoTimeCode);
					}

					dtoSearch.setTotalCount(timeCodeList.size());
				}
			
		}
		
		return dtoSearch;
	}

		
	
	
	
	
	
}
 