package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.AccrualPeriodSetting;

public class DtoAccrualPeriodSetting extends DtoBase{

	private Integer id;
	private Integer year;
	private short periodType;
	private Integer sequence;
	private Date startDate;
	private Date endDate;
	private boolean accrued;
	private List<DtoAccrualPeriodSetting> delete;
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public short getPeriodType() {
		return periodType;
	}

	public void setPeriodType(short periodType) {
		this.periodType = periodType;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isAccrued() {
		return accrued;
	}

	public void setAccrued(boolean accrued) {
		this.accrued = accrued;
	}

	
	public List<DtoAccrualPeriodSetting> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoAccrualPeriodSetting> delete) {
		this.delete = delete;
	}

	public DtoAccrualPeriodSetting() {
	}
	
	public DtoAccrualPeriodSetting(AccrualPeriodSetting accrualPeriodSetting) {
	}
}
