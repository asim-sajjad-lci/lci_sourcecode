package com.bti.hcm.model.dto;

import java.math.BigDecimal;

public class DtoManualChecksOpenYearDistribution extends DtoBase{


	private Integer id;
	private Short postingTypes;
	private Integer codeId;
	private BigDecimal debitAmount;
	private BigDecimal creditAmount;
	private Integer accountSequence;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Short getPostingTypes() {
		return postingTypes;
	}
	public void setPostingTypes(Short postingTypes) {
		this.postingTypes = postingTypes;
	}
	public Integer getCodeId() {
		return codeId;
	}
	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}
	public BigDecimal getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}
	public BigDecimal getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}
	public Integer getAccountSequence() {
		return accountSequence;
	}
	public void setAccountSequence(Integer accountSequence) {
		this.accountSequence = accountSequence;
	}
	
}
