/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeNationalities;

/**
 * Description: Interface for EmployeeNationalities 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryEmployeeNationalities")
public interface RepositoryEmployeeNationalities extends JpaRepository<EmployeeNationalities, Integer> {

	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	public EmployeeNationalities findByEmployeeNationalityIndexIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * @return
	 */
	@Query("select count(*) from EmployeeNationalities e where e.isDeleted = false")
	public Integer getCountOfTotalDepartment();
	
	/**
	 * @param deleted
	 * @return
	 */
	public List<EmployeeNationalities> findByIsDeleted(Boolean deleted);
	
	/**
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<EmployeeNationalities> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeNationalities e set e.isDeleted =:deleted, e.updatedBy =:updateById where e.employeeNationalityIndexId IN (:idList)")
	public void deleteEmployeeNationalities(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeNationalities e set e.isDeleted =:deleted, e.updatedBy =:updateById where e.employeeNationalityIndexId =:id")
	public void deleteSingleEmployeeNationalities(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select e from EmployeeNationalities e where (e.employeeNationalityId like :searchKeyWord  or e.employeeNationalityDescription like :searchKeyWord or e.employeeNationalityDescriptionArabic like :searchKeyWord) and e.isDeleted = false")
	public List<EmployeeNationalities> predictiveEmployeeNationalitiesSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<EmployeeNationalities> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from EmployeeNationalities e where (e.employeeNationalityId like :searchKeyWord  or e.employeeNationalityDescription like :searchKeyWord or e.employeeNationalityDescriptionArabic like :searchKeyWord) and e.isDeleted = false")
	public Integer predictiveEmployeeNationalitiesSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select e from EmployeeNationalities e where (e.employeeNationalityId like :searchKeyWord  or e.employeeNationalityDescription like :searchKeyWord or e.employeeNationalityDescriptionArabic like :searchKeyWord) and e.isDeleted = false")
	public List<EmployeeNationalities> predictiveEmployeeNationalitiesSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param departmentId
	 * @return
	 */
	@Query("select e from EmployeeNationalities e where (e.employeeNationalityId =:employeeNationalityId) and e.isDeleted = false")
	public List<EmployeeNationalities> findByEmployeeNationalityId(@Param("employeeNationalityId")String employeeNationalityId);
	
	@Query("select e.employeeNationalityId from EmployeeNationalities e where (e.employeeNationalityId like :searchKeyWord) and e.isDeleted = false order by e.employeeNationalityIndexId desc")
	public List<String> predictiveEmployeeNationalityIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select e from EmployeeNationalities e where (e.employeeNationalityId like :searchKeyWord) and e.isDeleted = false")
	public List<EmployeeNationalities> predictiveEmployeeNationalitiesAllIdsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageable);

	@Query("select count(*) from EmployeeNationalities e ")
	public Integer getCountOfTotalEmployeeNationalities();

	@Query("select e from EmployeeNationalities e where (e.employeeNationalityId like :searchKeyWord  or e.employeeNationalityDescription like :searchKeyWord or e.employeeNationalityDescriptionArabic like :searchKeyWord) and e.isDeleted = false")
	public List<EmployeeNationalities> predictiveEmployeeNationalitiesAllIdsSearchWithPagination(@Param("searchKeyWord")String searchKeyWord);
}
