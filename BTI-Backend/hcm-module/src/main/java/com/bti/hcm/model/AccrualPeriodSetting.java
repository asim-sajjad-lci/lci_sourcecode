package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40212",indexes = {
        @Index(columnList = "ACCRROWID")
})
public class AccrualPeriodSetting extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACCRROWID")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "YRINDX")
	private AccrualPeriod accrualPeriod;

	@Column(name = "YEAR1")
	private Integer year;

	@Column(name = "PERDTYP")
	private short periodType;

	@Column(name = "PERDSEQC")
	private Integer sequence;

	@Column(name = "STRTDT")
	private Date startDate;

	@Column(name = "ENDDT")
	private Date endData;

	@Column(name = "ACCRUD")
	private boolean accrued;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public short getPeriodType() {
		return periodType;
	}

	public void setPeriodType(short periodType) {
		this.periodType = periodType;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndData() {
		return endData;
	}

	public void setEndData(Date endData) {
		this.endData = endData;
	}

	public boolean isAccrued() {
		return accrued;
	}

	public void setAccrued(boolean accrued) {
		this.accrued = accrued;
	}

	public AccrualPeriod getAccrualPeriod() {
		return accrualPeriod;
	}

	public void setAccrualPeriod(AccrualPeriod accrualPeriod) {
		this.accrualPeriod = accrualPeriod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accrualPeriod == null) ? 0 : accrualPeriod.hashCode());
		result = prime * result + (accrued ? 1231 : 1237);
		result = prime * result + ((endData == null) ? 0 : endData.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + periodType;
		result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
