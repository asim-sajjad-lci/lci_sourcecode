package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.EmployeeNationalities;
import com.bti.hcm.util.UtilRandomKey;

public class DtoEmployeeNationalities extends DtoBase{

	private Integer employeeNationalityIndexId;
	private String employeeNationalityId;
	private String employeeNationalityDescriptionArabic;
	private String employeeNationalityDescription;
	private List<DtoEmployeeNationalities> deleteEmployeeNationalities;
	private int valueId;
	private String valueDescription;
	private int valId;


	public DtoEmployeeNationalities() {

	}

	public DtoEmployeeNationalities(EmployeeNationalities employeeNationalities) {
		this.employeeNationalityId = employeeNationalities.getEmployeeNationalityId();

		if (UtilRandomKey.isNotBlank(employeeNationalities.getEmployeeNationalityDescription())) {
			this.employeeNationalityDescription = employeeNationalities.getEmployeeNationalityDescription();
		} else {
			this.employeeNationalityDescription = "";
		}
	}

	public Integer getEmployeeNationalityIndexId() {
		return employeeNationalityIndexId;
	}

	public void setEmployeeNationalityIndexId(Integer employeeNationalityIndexId) {
		this.employeeNationalityIndexId = employeeNationalityIndexId;
	}

	public String getEmployeeNationalityId() {
		return employeeNationalityId;
	}

	public void setEmployeeNationalityId(String employeeNationalityId) {
		this.employeeNationalityId = employeeNationalityId;
	}

	public String getEmployeeNationalityDescriptionArabic() {
		return employeeNationalityDescriptionArabic;
	}

	public void setEmployeeNationalityDescriptionArabic(String employeeNationalityDescriptionArabic) {
		this.employeeNationalityDescriptionArabic = employeeNationalityDescriptionArabic;
	}

	public String getEmployeeNationalityDescription() {
		return employeeNationalityDescription;
	}

	public void setEmployeeNationalityDescription(String employeeNationalityDescription) {
		this.employeeNationalityDescription = employeeNationalityDescription;
	}


	public List<DtoEmployeeNationalities> getDeleteEmployeeNationalities() {
		return deleteEmployeeNationalities;
	}

	public void setDeleteEmployeeNationalities(List<DtoEmployeeNationalities> deleteEmployeeNationalities) {
		this.deleteEmployeeNationalities = deleteEmployeeNationalities;
	}

	public int getValueId() {
		return valueId;
	}

	public void setValueId(int valueId) {
		this.valueId = valueId;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	public int getValId() {
		return valId;
	}

	public void setValId(int valId) {
		this.valId = valId;
	}

	
}
