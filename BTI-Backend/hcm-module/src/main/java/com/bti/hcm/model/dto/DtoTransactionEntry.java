package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.TransactionEntry;

public class DtoTransactionEntry extends DtoBase{

	private Integer id;
	private Integer entryNumber;
	private Date entryDate;
	private String description;
	private String arabicDescription;
	private DtoBatches batches;
	private Date fromDate;
	private Date toDate;
	private DtoTransactionEntryDetail transactionEntryDetail;
	private DtoFinancialDimensions dimensions;
	private List<DtoTransactionEntry> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEntryNumber() {
		return entryNumber;
	}

	public void setEntryNumber(Integer entryNumber) {
		this.entryNumber = entryNumber;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public DtoBatches getBatches() {
		return batches;
	}

	public void setBatches(DtoBatches batches) {
		this.batches = batches;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<DtoTransactionEntry> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoTransactionEntry> delete) {
		this.delete = delete;
	}

	public DtoFinancialDimensions getDimensions() {
		return dimensions;
	}

	public void setDimensions(DtoFinancialDimensions dimensions) {
		this.dimensions = dimensions;
	}

	public DtoTransactionEntryDetail getTransactionEntryDetail() {
		return transactionEntryDetail;
	}

	public void setTransactionEntryDetail(DtoTransactionEntryDetail transactionEntryDetail) {
		this.transactionEntryDetail = transactionEntryDetail;
	}

	public DtoTransactionEntry() {
	}

	public DtoTransactionEntry(TransactionEntry transactionEntry) {
	}
}
