package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoLifeInsuranceSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceLifeInsuranceSetup;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/lifeInsuranceSetup")
public class ControllerLifeInsuranceSetup extends BaseController{
	
	
	/**
	 * @Description LOGGER use for put a logger in LifeInsuranceSetup Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerLifeInsuranceSetup.class);
	
	/**
	 * @Description serviceLifeInsuranceSetup Autowired here using annotation of spring for use of serviceLifeInsuranceSetup method in this controller
	 */
	@Autowired(required=true)
	ServiceLifeInsuranceSetup serviceLifeInsuranceSetup;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @Description create LifeInsuranceSetup
	 * @param request
	 * @param dtoLifeInsuranceSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
			"lifeInsuraceId":"A0201",
			  "lifeInsuraceDescription":"testing",
			  "lifeInsuraceDescriptionArabic":"testingArabic",
			  "healthInsuranceGroupNumber":"B1020",
			  "lifeInsuranceFrequency":1,
			  "amount":100.00,
			  "spouseAmount":50.00,
			  "childAmount":85.00,
			  "coverageTotalAmount":60.00,
			  "employeePay":56.00,
			  "startDate":102120120,
			  "endDate":102120120,
			  "insuranceType":1,
			  "healthInsuranceId":1

		}
		
		@response
				{
					"code": 201,
					"status": "CREATED",
					"result": {
					"lifeInsuraceId": "A0201",
					"lifeInsuraceDescription": "testing",
					"lifeInsuraceDescriptionArabic": "testingArabic",
					"healthInsuranceGroupNumber": "B1020",
					"lifeInsuranceFrequency": 1,
					"amount": 100,
					"spouseAmount": 50,
					"childAmount": 85,
					"coverageTotalAmount": 60,
					"employeePay": 56,
					"startDate": 102120120,
					"endDate": 102120120,
					"insuranceType": 1,
					"healthInsuranceId": 1
				},
					"btiMessage": {
					"message": "LifeInsuranceSetup created successfully.",
					"messageShort": "LIFE_INSURANCE_SETUP_CREATED"
					}
				}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoLifeInsuranceSetup dtoLifeInsuranceSetup) throws Exception {
		LOGGER.info("Create LifeInsuranceSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoLifeInsuranceSetup = serviceLifeInsuranceSetup.saveOrUpdateLifeInsuranceSetup(dtoLifeInsuranceSetup);
			responseMessage=displayMessage(dtoLifeInsuranceSetup, "LIFE_INSURANCE_SETUP_CREATED", "LIFE_INSURANCE_SETUP_CREATED_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create LifeInsuranceSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @Description update LifeInsuranceSetup
	 * @param request
	 * @param dtoLifeInsuranceSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "id":1,
		"lifeInsuraceId":"A0202",
		  "lifeInsuraceDescription":"testings",
		  "lifeInsuraceDescriptionArabic":"testingArabic",
		  "healthInsuranceGroupNumber":"B1020",
		  "lifeInsuranceFrequency":1,
		  "amount":100.00,
		  "spouseAmount":50.00,
		  "childAmount":85.00,
		  "coverageTotalAmount":60.00,
		  "employeePay":56.00,
		  "startDate":102120120,
		  "endDate":102120120,
		  "insuranceType":1,
		  "healthInsuranceId":1
		
		}
		
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": 1,
			"lifeInsuraceId": "A0202",
			"lifeInsuraceDescription": "testings",
			"lifeInsuraceDescriptionArabic": "testingArabic",
			"healthInsuranceGroupNumber": "B1020",
			"lifeInsuranceFrequency": 1,
			"amount": 100,
			"spouseAmount": 50,
			"childAmount": 85,
			"coverageTotalAmount": 60,
			"employeePay": 56,
			"startDate": 102120120,
			"endDate": 102120120,
			"insuranceType": 1,
			"healthInsuranceId": 1
			},
				"btiMessage": {
				"message": "LifeInsuranceSetup updated successfully.",
				"messageShort": "LIFE_INSURANCE_SETUP_UPDATED_SUCCESS"
				}
			}
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoLifeInsuranceSetup dtoLifeInsuranceSetup) throws Exception {
		LOGGER.info("Update LifeInsuranceSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoLifeInsuranceSetup = serviceLifeInsuranceSetup.saveOrUpdateLifeInsuranceSetup(dtoLifeInsuranceSetup);
			responseMessage=displayMessage(dtoLifeInsuranceSetup, "LIFE_INSURANCE_SETUP_UPDATED_SUCCESS", "LIFE_INSURANCE_SETUP_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update LifeInsuranceSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @Description delete LifeInsuranceSetup
	 * @param request
	 * @param dtoLifeInsuranceSetup
	 * @return
	 * @throws Exception
	 * @request
			 * {
		  "ids":[1]
		}
		
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"insuranceType": 0,
			"deleteMessage": "LifeInsuranceSetup deleted successfully.",
			"associateMessage": "N/A",
			"deleteLifeInsuranceSetup": [
			  {
					"id": 1,
					"lifeInsuraceDescription": "testings",
					"lifeInsuraceDescriptionArabic": "testingArabic",
					"healthInsuranceGroupNumber": "B1020",
					"lifeInsuranceFrequency": 1,
					"amount": 100,
					"spouseAmount": 50,
					"childAmount": 85,
					"coverageTotalAmount": 60,
					"employeePay": 56,
					"startDate": 102120000,
					"endDate": 102120000,
					"insuranceType": 1
					}
			],
			},
			"btiMessage": {
				"message": "LifeInsuranceSetup deleted successfully.",
				"messageShort": "LIFE_INSURANCE_SETUP_DELETED"
				}
			}
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoLifeInsuranceSetup dtoLifeInsuranceSetup) throws Exception {
		LOGGER.info("Delete LifeInsuranceSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoLifeInsuranceSetup.getIds() != null && !dtoLifeInsuranceSetup.getIds().isEmpty()) {
				DtoLifeInsuranceSetup dtoLifeInsuranceSetup2 = serviceLifeInsuranceSetup.deleteLifeInsuranceSetup(dtoLifeInsuranceSetup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("LIFE_INSURANCE_SETUP_DELETED", false), dtoLifeInsuranceSetup2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @Description getAll LifeInsuranceSetup
	 * @param request
	 * @param dtoLifeInsuranceSetup
	 * @return
	 * @throws Exception
	 * @request
	 * {
		  "searchKeyword":"t",
		  "sortOn":"",
		  "sortBy":"DESC",
		 "pageNumber":"0",
		  "pageSize":"10"
		}
		
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"searchKeyword": "t",
			"pageNumber": 0,
			"pageSize": 10,
			"sortOn": "",
			"sortBy": "DESC",
			"totalCount": 5,
			"records": [
				  {
				"lifeInsuraceDescription": "sfhfghdd",
				"lifeInsuraceDescriptionArabic": "asfdgb t",
				"healthInsuranceGroupNumber": "1",
				"lifeInsuranceFrequency": 3,
				"amount": 4354364,
				"spouseAmount": 1243455,
				"childAmount": 53646,
				"coverageTotalAmount": 2424,
				"employeePay": 343533,
				"startDate": 1521311400000,
				"endDate": 1521916200000,
				"insuranceType": 1
				},
				  {
				"lifeInsuraceDescription": "sfhfghdd",
				"lifeInsuraceDescriptionArabic": "asfdgb t",
				"healthInsuranceGroupNumber": "5",
				"lifeInsuranceFrequency": 1,
				"amount": 2435,
				"spouseAmount": 4354364,
				"childAmount": 242,
				"coverageTotalAmount": 53646,
				"employeePay": 34353,
				"startDate": 1521052200000,
				"endDate": 1521225000000,
				"insuranceType": 1
				},
				  {
				"lifeInsuraceDescription": "testing",
				"lifeInsuraceDescriptionArabic": "testingArabic",
				"healthInsuranceGroupNumber": "1",
				"lifeInsuranceFrequency": 4,
				"amount": 50,
				"spouseAmount": 1,
				"childAmount": 60,
				"coverageTotalAmount": 85,
				"employeePay": 56,
				"startDate": 1521225000000,
				"endDate": 1521311400000,
				"insuranceType": 2
				},
			  {
			"lifeInsuraceDescription": "testing",
			"lifeInsuraceDescriptionArabic": "testingArabic",
			"healthInsuranceGroupNumber": "B1020",
			"lifeInsuranceFrequency": 1,
			"amount": 100,
			"spouseAmount": 50,
			"childAmount": 85,
			"coverageTotalAmount": 60,
			"employeePay": 56,
			"startDate": 102120000,
			"endDate": 102120000,
			"insuranceType": 1
			},
			  {
			"lifeInsuraceDescription": "testings",
			"lifeInsuraceDescriptionArabic": "testingArabic",
			"healthInsuranceGroupNumber": "B1020",
			"lifeInsuranceFrequency": 1,
			"amount": 100,
			"spouseAmount": 50,
			"childAmount": 85,
			"coverageTotalAmount": 60,
			"employeePay": 56,
			"startDate": 102120000,
			"endDate": 102120000,
			"insuranceType": 1
			}
		],
	},
		"btiMessage": {
			"message": "LifeInsuranceSetup list fetched successfully.",
			"messageShort": "LIFE_INSURANCE_SETUP_GET_ALL"
			}
		}
		
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody  DtoSearch dtoSearch) throws Exception {
		LOGGER.info("Get All LifeInsuranceSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			 dtoSearch = serviceLifeInsuranceSetup.searchLifeInsuranceSetup(dtoSearch);
			 responseMessage=displayMessage(dtoSearch, "LIFE_INSURANCE_SETUP_GET_ALL", "LIFE_INSURANCE_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All LifeInsuranceSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @Description Get LifeInsuranceSetup By Id
	 * @param request
	 * @param dtoLifeInsuranceSetup
	 * @return
	 * @throws Exception
	 * @Request
	 * {
		 "lifeInsuraceId":"B202"
	  }
	  @Response
	  {
			"code": 201,
			"status": "CREATED",
			"result": {
			"insuranceType": 0,
			"isRepeat": true
		},
			"btiMessage": {
				"message": "LifeInsuranceSetup details fetched successfully.",
				"messageShort": "LIFE_INSURANCE_SETUP_LIST_GET_DETAIL"
			}
		}
	 */
	@RequestMapping(value = "/getLifeInsuranceSetupId", method = RequestMethod.POST)
	public ResponseMessage getLifeInsuranceSetupId(HttpServletRequest request, @RequestBody DtoLifeInsuranceSetup dtoLifeInsuranceSetup) throws Exception {
		LOGGER.info("Get LifeInsuranceSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoLifeInsuranceSetup dtoLifeInsuranceSetupObj = serviceLifeInsuranceSetup.repeatByLifeInsuranceSetupId(dtoLifeInsuranceSetup.getLifeInsuraceId());
			responseMessage=displayMessage(dtoLifeInsuranceSetupObj, "LIFE_INSURANCE_SETUP_LIST_GET_DETAIL", "LIFE_INSURANCE_SETUP_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Position by Id Method:"+dtoLifeInsuranceSetup.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/searchId", method = RequestMethod.POST)
	public ResponseMessage searchId(HttpServletRequest request, @RequestBody  DtoSearch dtoSearch) throws Exception {
		LOGGER.info("Get All LifeInsuranceSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			 dtoSearch = serviceLifeInsuranceSetup.searchIds(dtoSearch);
			 responseMessage=displayMessage(dtoSearch, "LIFE_INSURANCE_SETUP_GET_ALL", "LIFE_INSURANCE_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All LifeInsuranceSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
}
