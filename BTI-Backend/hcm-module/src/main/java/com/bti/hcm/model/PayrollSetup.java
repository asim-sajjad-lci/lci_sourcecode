package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40000",indexes = {
        @Index(columnList = "RWPRINDX")
})
public class PayrollSetup extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RWPRINDX")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "DIMINXDD")
	private FinancialDimensions departmentDimensions;


	@ManyToOne
	@JoinColumn(name = "DIMINXDE")
	private FinancialDimensions employeeDimensions;
	
	@ManyToOne
	@JoinColumn(name = "DIMINXDP")
	private FinancialDimensions projectDimensions;
	
	
	@ManyToOne
	@JoinColumn(name = "DEFCHLPRL")
	private CheckbookMaintenance payrollCheckbook;
	
	@ManyToOne
	@JoinColumn(name = "DEFCHLMNL")
	private CheckbookMaintenance manualCheckCheckbook;
	
	@ManyToOne
	@JoinColumn(name = "TAXSCHDID")
	private VATSetup vatSetup;
	
	@Column(name = "NXTPYMTNM")
	private Integer nextPaymentNumber;
	
	@Column(name = "NXTPRLNM")
	private Integer checkNumber;
	
	@Column(name = "NXTMNLNM")
	private Integer manualcheckNumber;
	
	@Column(name = "MKMULPRL")
	private boolean multiPayrollProcesses;
	
	@Column(name = "MKSGJVPRL")
	private boolean jvPerEmployee;
	
	@Column(name = "DSPPYRATN")
	private boolean displayPayRate;
	
	@Column(name = "INCPYYTD")
	private boolean codeswithYTDAmounts;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public FinancialDimensions getDepartmentDimensions() {
		return departmentDimensions;
	}

	public void setDepartmentDimensions(FinancialDimensions departmentDimensions) {
		this.departmentDimensions = departmentDimensions;
	}

	public FinancialDimensions getEmployeeDimensions() {
		return employeeDimensions;
	}

	public void setEmployeeDimensions(FinancialDimensions employeeDimensions) {
		this.employeeDimensions = employeeDimensions;
	}

	public FinancialDimensions getProjectDimensions() {
		return projectDimensions;
	}

	public void setProjectDimensions(FinancialDimensions projectDimensions) {
		this.projectDimensions = projectDimensions;
	}

	public Integer getNextPaymentNumber() {
		return nextPaymentNumber;
	}

	public void setNextPaymentNumber(Integer nextPaymentNumber) {
		this.nextPaymentNumber = nextPaymentNumber;
	}

	public Integer getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}

	public boolean isMultiPayrollProcesses() {
		return multiPayrollProcesses;
	}

	public void setMultiPayrollProcesses(boolean multiPayrollProcesses) {
		this.multiPayrollProcesses = multiPayrollProcesses;
	}

	public boolean isJvPerEmployee() {
		return jvPerEmployee;
	}

	public void setJvPerEmployee(boolean jvPerEmployee) {
		this.jvPerEmployee = jvPerEmployee;
	}

	public boolean isDisplayPayRate() {
		return displayPayRate;
	}

	public void setDisplayPayRate(boolean displayPayRate) {
		this.displayPayRate = displayPayRate;
	}

	public boolean isCodeswithYTDAmounts() {
		return codeswithYTDAmounts;
	}

	public void setCodeswithYTDAmounts(boolean codeswithYTDAmounts) {
		this.codeswithYTDAmounts = codeswithYTDAmounts;
	}

	public CheckbookMaintenance getPayrollCheckbook() {
		return payrollCheckbook;
	}

	public void setPayrollCheckbook(CheckbookMaintenance payrollCheckbook) {
		this.payrollCheckbook = payrollCheckbook;
	}

	public CheckbookMaintenance getManualCheckCheckbook() {
		return manualCheckCheckbook;
	}

	public void setManualCheckCheckbook(CheckbookMaintenance manualCheckCheckbook) {
		this.manualCheckCheckbook = manualCheckCheckbook;
	}

	public VATSetup getVatSetup() {
		return vatSetup;
	}

	public void setVatSetup(VATSetup vatSetup) {
		this.vatSetup = vatSetup;
	}

	public Integer getManualcheckNumber() {
		return manualcheckNumber;
	}

	public void setManualcheckNumber(Integer manualcheckNumber) {
		this.manualcheckNumber = manualcheckNumber;
	}
	
	
	
	

}
