package com.bti.hcm.model.dto;

import java.util.Date;

public class DtoEmployeeMonthlyInstallment {
	
	private Integer id;
	private Date monthYear;
	private Double amount;
	private Boolean isPostponed;
	private Boolean isPaid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(Date monthYear) {
		this.monthYear = monthYear;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getIsPostponed() {
		return isPostponed;
	}

	public void setIsPostponed(Boolean isPostponed) {
		this.isPostponed = isPostponed;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

}
