/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.HijriDay;
import com.bti.hcm.model.dto.DtoHijriDay;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryHijriDay;

/**
 * Description: Interface for Hijri Day 
 * Name of Project:Hcm 
 * Version: 0.0.1
 * Created on: February 12, 2018
 * 
 * @author Bansi
 *
 */
@Service("serviceHijriDay")
public class ServiceHijriDay {

	/**
	 * @Description LOGGER use for put a logger in HijriDay Service
	 */
	static Logger logger = Logger.getLogger(ServiceHijriDay.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in HijriDay service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in HijriDay service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description Autowired spring annotation to access repositoryHijriDay method
	 *              in HijriDay service In short, access Hijri day Query from
	 *              Database using repositoryHijriDay.
	 */
	@Autowired
	RepositoryHijriDay repositoryHijriDay;

	/**
	 * @Description: get All HIjri date
	 * @param dtoHijriDay,
	 *            dtoHijriMonth, dtoHijriYear
	 * @return
	 */
	public DtoSearch getAllHijriDay(/* , DtoHijriMonth dtoHijriMonth, DtoHijriYear dtoHijriYear */) {
		logger.info("getAllHijriDay Method called!!");
		DtoHijriDay dtoHijriDay = null;
		DtoSearch dtoSearch = new DtoSearch();
		List<HijriDay> hijriDayList = repositoryHijriDay.findAll();

		List<DtoHijriDay> dtoHijriDayList = new ArrayList<>();
		if (hijriDayList != null && hijriDayList.size() > 0) {
			for (HijriDay hijriDay : hijriDayList) {
				dtoHijriDay = new DtoHijriDay();
				dtoHijriDay.setDay(hijriDay.getDay());
				dtoHijriDay.setId(hijriDay.getId());
				dtoHijriDayList.add(dtoHijriDay);
			}
			dtoSearch.setRecords(dtoHijriDayList);

		}
		logger.debug("All Hijari day List Size is:" + dtoSearch.getTotalCount());
		logger.info("dtoSearch: " + dtoSearch);
		return dtoSearch;
	}
	
}
