package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoTransactionEntry;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceNewTransactionEntry;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/newTransactionEntry")
public class ControllerNewTransactionEntry extends BaseController {
	
	private static final Logger log = Logger.getLogger(ControllerTypeAccurual.class);
	
	@Autowired(required=true)
	ServiceNewTransactionEntry serviceNewTransactionEntry;
	
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody List<DtoTransactionEntry> dtoAccrual) throws Exception {
		log.info("Create TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrual = serviceNewTransactionEntry.saveOrUpdate(dtoAccrual);
			
			responseMessage = displayMessage(dtoAccrual,"TRANSCATION_ENTRY_CREATED","TRANSCATION_ENTRY_NOT_CREATED",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(responseMessage!=null) {
			log.debug("Create TransactionEntry Method:"+responseMessage.getMessage());
		}
	
		return responseMessage;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody List<DtoTransactionEntry> dtoAccrual) throws Exception {
		log.info("Create TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrual = serviceNewTransactionEntry.saveOrUpdate(dtoAccrual);
			responseMessage = displayMessage(dtoAccrual,MessageConstant.TRANSCATION_ENTRY_UPDATED,MessageConstant.TRANSCATION_ENTRY_NOT_UPDATED,serviceResponse);
		} else {
			unauthorizedMsg(serviceResponse);
		}
		if(responseMessage!=null) {
			log.debug("Create TransactionEntry Method:"+responseMessage.getMessage());
		}
		
		return responseMessage;
	}

}
