package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40900", indexes = { @Index(columnList = "PYCDINDX") })
public class PayCode extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PYCDINDX")
	private Integer id;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payCode")
	// @LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "TMECDINCT = false and is_deleted = false")
	private List<TimeCode> listTimeCode;

	@Column(name = "PYCDID", columnDefinition = "char(15)")
	private String payCodeId;

	@Column(name = "PYCDDSCR", columnDefinition = "char(31)")
	private String description;

	@Column(name = "PYCDDSCRA", columnDefinition = "char(31)")
	private String arbicDescription;

	@ManyToOne
	@JoinColumn(name = "PYCDTYPINDX")
	private PayCodeType payType;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payCode")
	// @LazyCollection(LazyCollectionOption.FALSE)
	private List<EmployeePayCodeMaintenance> listEmployeePayCodeMaintenance;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payCode")
	// @LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<BuildPayrollCheckByPayCodes> listBuildPayrollCheckByPayCodes;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payCode")
	// @LazyCollection(LazyCollectionOption.FALSE)
	private List<ActivateEmployeePostDatePayRate> listActivateEmployeePostDatePayRate;

	/*
	 * @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="payCode")
	 * private List<DeductionCode>listDeductionCode;
	 */

	/*
	 * @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="payCode")
	 * private List<BenefitCode> listBenefitCode;
	 */

	@Column(name = "PYCDBASCD", columnDefinition = "char(31)")
	private String baseOnPayCode;

	@Column(name = "PYCDBASCDVAL", precision = 10, scale = 3)
	private BigDecimal baseOnPayCodeAmount;

	@Column(name = "PYCDFACTR", precision = 10, scale = 3)
	private BigDecimal payFactor;

	@Column(name = "PYCDRATE", precision = 10, scale = 3)
	private BigDecimal payRate;

	@Column(name = "PYCDUNTPY")
	private Short unitofPay;

	@Column(name = "PYCDPERD")
	private Short payperiod;

	@Column(name = "BASEONPAYCODEID")
	private Integer baseOnPayCodeId;

	@Column(name = "PYCDINCTV")
	private boolean inActive;

	@ManyToMany(mappedBy = "payCodeList")
	private List<DeductionCode> deductionCodeList;

	@ManyToMany(mappedBy = "payCodeList")
	private List<BenefitCode> benefitCodeList;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "payCode")
	private List<BuildPayrollCheckByPayCodes> buildPayrollCheckByPayCodes;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "code")
	private List<TransactionEntryDetail> transactionEntryDetail;

	@Column(name = "is_deleted")
	private boolean isDeleted;

	
	@Column(name = "type_field")	// ME dtd 03Sep
	private Integer typeField;		//for new "Type" field binding 

	
	@Column(name = "roundOf")		// ME dtd 04Sep
	private Integer roundOf;		
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTypeField() {
		return typeField;
	}

	public void setTypeField(Integer typeField) {
		this.typeField = typeField;
	}

	public Integer getRoundOf() {
		return roundOf;
	}

	public void setRoundOf(Integer roundOf) {
		this.roundOf = roundOf;
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getPayCodeId() {
		return payCodeId;
	}

	public void setPayCodeId(String payCodeId) {
		this.payCodeId = payCodeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArbicDescription() {
		return arbicDescription;
	}

	public void setArbicDescription(String arbicDescription) {
		this.arbicDescription = arbicDescription;
	}

	public PayCodeType getPayType() {
		return payType;
	}

	public void setPayType(PayCodeType payType) {
		this.payType = payType;
	}

	public String getBaseOnPayCode() {
		return baseOnPayCode;
	}

	public void setBaseOnPayCode(String baseOnPayCode) {
		this.baseOnPayCode = baseOnPayCode;
	}

	public BigDecimal getBaseOnPayCodeAmount() {
		return baseOnPayCodeAmount;
	}

	public void setBaseOnPayCodeAmount(BigDecimal baseOnPayCodeAmount) {
		this.baseOnPayCodeAmount = baseOnPayCodeAmount;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

	public BigDecimal getPayRate() {
		return payRate;
	}

	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}

	public Short getUnitofPay() {
		return unitofPay;
	}

	public void setUnitofPay(Short unitofPay) {
		this.unitofPay = unitofPay;
	}

	public Short getPayperiod() {
		return payperiod;
	}

	public void setPayperiod(Short payperiod) {
		this.payperiod = payperiod;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public List<TimeCode> getListTimeCode() {
		return listTimeCode;
	}

	public void setListTimeCode(List<TimeCode> listTimeCode) {
		this.listTimeCode = listTimeCode;
	}

	public List<EmployeePayCodeMaintenance> getListEmployeePayCodeMaintenance() {
		return listEmployeePayCodeMaintenance;
	}

	public void setListEmployeePayCodeMaintenance(List<EmployeePayCodeMaintenance> listEmployeePayCodeMaintenance) {
		this.listEmployeePayCodeMaintenance = listEmployeePayCodeMaintenance;
	}

	public List<BuildPayrollCheckByPayCodes> getListBuildPayrollCheckByPayCodes() {
		return listBuildPayrollCheckByPayCodes;
	}

	public void setListBuildPayrollCheckByPayCodes(List<BuildPayrollCheckByPayCodes> listBuildPayrollCheckByPayCodes) {
		this.listBuildPayrollCheckByPayCodes = listBuildPayrollCheckByPayCodes;
	}

	public List<ActivateEmployeePostDatePayRate> getListActivateEmployeePostDatePayRate() {
		return listActivateEmployeePostDatePayRate;
	}

	public void setListActivateEmployeePostDatePayRate(
			List<ActivateEmployeePostDatePayRate> listActivateEmployeePostDatePayRate) {
		this.listActivateEmployeePostDatePayRate = listActivateEmployeePostDatePayRate;
	}

	/*
	 * public List<DeductionCode> getListDeductionCode() { return listDeductionCode;
	 * }
	 * 
	 * public void setListDeductionCode(List<DeductionCode> listDeductionCode) {
	 * this.listDeductionCode = listDeductionCode; }
	 */

	/*
	 * public List<BenefitCode> getListBenefitCode() { return listBenefitCode; }
	 * 
	 * public void setListBenefitCode(List<BenefitCode> listBenefitCode) {
	 * this.listBenefitCode = listBenefitCode; }
	 */
	public List<DeductionCode> getDeductionCodeList() {
		return deductionCodeList;
	}

	public void setDeductionCodeList(List<DeductionCode> deductionCodeList) {
		this.deductionCodeList = deductionCodeList;
	}

	public List<BenefitCode> getBenefitCodeList() {
		return benefitCodeList;
	}

	public void setBenefitCodeList(List<BenefitCode> benefitCodeList) {
		this.benefitCodeList = benefitCodeList;
	}

	public Integer getBaseOnPayCodeId() {
		return baseOnPayCodeId;
	}

	public void setBaseOnPayCodeId(Integer baseOnPayCodeId) {
		this.baseOnPayCodeId = baseOnPayCodeId;
	}

	public List<BuildPayrollCheckByPayCodes> getBuildPayrollCheckByPayCodes() {
		return buildPayrollCheckByPayCodes;
	}

	public void setBuildPayrollCheckByPayCodes(List<BuildPayrollCheckByPayCodes> buildPayrollCheckByPayCodes) {
		this.buildPayrollCheckByPayCodes = buildPayrollCheckByPayCodes;
	}

	public List<TransactionEntryDetail> getTransactionEntryDetail() {
		return transactionEntryDetail;
	}

	public void setTransactionEntryDetail(List<TransactionEntryDetail> transactionEntryDetail) {
		this.transactionEntryDetail = transactionEntryDetail;
	}

	/*
	 * public EmployeePayCodeMaintenance getEmployeePayCodeMaintenance() { return
	 * employeePayCodeMaintenance; }
	 * 
	 * public void setEmployeePayCodeMaintenance(EmployeePayCodeMaintenance
	 * employeePayCodeMaintenance) { this.employeePayCodeMaintenance =
	 * employeePayCodeMaintenance; }
	 */

}
