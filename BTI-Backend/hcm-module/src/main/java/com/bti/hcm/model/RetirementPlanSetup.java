package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40341",indexes = {
        @Index(columnList = "RETRINDX")
})
public class RetirementPlanSetup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RETRINDX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "INSCOMINDX")
	private CompnayInsurance helthInsurance;

	@ManyToOne
	@JoinColumn(name = "RETRPLNINDX")
	private RetirementEntranceDate retirementEntranceDate;

	@ManyToOne
	@JoinColumn(name = "RETRFNDINDX")
	private RetirementFundSetup retirementFundSetup;

	@Column(name = "RETRID", columnDefinition = "char(15)")
	private String retirementPlanId;

	@Column(name = "RETRDSCR", columnDefinition = "char(31)")
	private String retirementPlanDescription;

	@Column(name = "RETRDSCRA", columnDefinition = "char(61)")
	private String retirementPlanArbicDescription;

	@Column(name = "RETRACCTN", columnDefinition = "char(31)")
	private String retirementPlanAccountNumber;

	@Column(name = "RETRMTPERC", precision = 10, scale = 5)
	private BigDecimal retirementPlanMatchPercent;

	@Column(name = "RETRMTPERCM", precision = 10, scale = 5)
	private BigDecimal retirementPlanMaxPercent;

	@Column(name = "RETREMPAMTY", precision = 10, scale = 3)
	private BigDecimal retirementPlanMaxAmount;

	@Column(name = "RETRALWLOD")
	private Boolean loans;

	@Column(name = "RETRALWBON")
	private Boolean bonus;

	@Column(name = "RETRMINAGE")
	private Integer retirementAge;

	@Column(name = "RETRWTDAYS")
	private Integer watingPeriods;

	@Column(name = "RETREMLYRAMT", precision = 10, scale = 3)
	private BigDecimal retirementContribution;

	@Column(name = "RETRMEDT")
	private short watingMethod;

	@Column(name = "RETRAMNT", precision = 10, scale = 3)
	private BigDecimal retirementPlanAmount;

	@Column(name = "RETRPERNT", precision = 10, scale = 5)
	private BigDecimal retirementPlanPercent;

	@Column(name = "RETRFREQ")
	private short retirementFrequency;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRetirementPlanId() {
		return retirementPlanId;
	}

	public void setRetirementPlanId(String retirementPlanId) {
		this.retirementPlanId = retirementPlanId;
	}

	public String getRetirementPlanDescription() {
		return retirementPlanDescription;
	}

	public void setRetirementPlanDescription(String retirementPlanDescription) {
		this.retirementPlanDescription = retirementPlanDescription;
	}

	public String getRetirementPlanArbicDescription() {
		return retirementPlanArbicDescription;
	}

	public void setRetirementPlanArbicDescription(String retirementPlanArbicDescription) {
		this.retirementPlanArbicDescription = retirementPlanArbicDescription;
	}

	public String getRetirementPlanAccountNumber() {
		return retirementPlanAccountNumber;
	}

	public void setRetirementPlanAccountNumber(String retirementPlanAccountNumber) {
		this.retirementPlanAccountNumber = retirementPlanAccountNumber;
	}

	public BigDecimal getRetirementPlanMatchPercent() {
		return retirementPlanMatchPercent;
	}

	public void setRetirementPlanMatchPercent(BigDecimal retirementPlanMatchPercent) {
		this.retirementPlanMatchPercent = retirementPlanMatchPercent;
	}

	public BigDecimal getRetirementPlanMaxPercent() {
		return retirementPlanMaxPercent;
	}

	public void setRetirementPlanMaxPercent(BigDecimal retirementPlanMaxPercent) {
		this.retirementPlanMaxPercent = retirementPlanMaxPercent;
	}

	public BigDecimal getRetirementPlanMaxAmount() {
		return retirementPlanMaxAmount;
	}

	public void setRetirementPlanMaxAmount(BigDecimal retirementPlanMaxAmount) {
		this.retirementPlanMaxAmount = retirementPlanMaxAmount;
	}

	public Boolean getLoans() {
		return loans;
	}

	public void setLoans(Boolean loans) {
		this.loans = loans;
	}

	public Boolean getBonus() {
		return bonus;
	}

	public void setBonus(Boolean bonus) {
		this.bonus = bonus;
	}

	public Integer getRetirementAge() {
		return retirementAge;
	}

	public void setRetirementAge(Integer retirementAge) {
		this.retirementAge = retirementAge;
	}

	public Integer getWatingPeriods() {
		return watingPeriods;
	}

	public void setWatingPeriods(Integer watingPeriods) {
		this.watingPeriods = watingPeriods;
	}

	public BigDecimal getRetirementContribution() {
		return retirementContribution;
	}

	public void setRetirementContribution(BigDecimal retirementContribution) {
		this.retirementContribution = retirementContribution;
	}

	public short getWatingMethod() {
		return watingMethod;
	}

	public void setWatingMethod(short watingMethod) {
		this.watingMethod = watingMethod;
	}

	public BigDecimal getRetirementPlanAmount() {
		return retirementPlanAmount;
	}

	public void setRetirementPlanAmount(BigDecimal retirementPlanAmount) {
		this.retirementPlanAmount = retirementPlanAmount;
	}

	public BigDecimal getRetirementPlanPercent() {
		return retirementPlanPercent;
	}

	public void setRetirementPlanPercent(BigDecimal retirementPlanPercent) {
		this.retirementPlanPercent = retirementPlanPercent;
	}

	public short getRetirementFrequency() {
		return retirementFrequency;
	}

	public void setRetirementFrequency(short retirementFrequency) {
		this.retirementFrequency = retirementFrequency;
	}

	public CompnayInsurance getHelthInsurance() {
		return helthInsurance;
	}

	public void setHelthInsurance(CompnayInsurance helthInsurance) {
		this.helthInsurance = helthInsurance;
	}

	public RetirementEntranceDate getRetirementEntranceDate() {
		return retirementEntranceDate;
	}

	public void setRetirementEntranceDate(RetirementEntranceDate retirementEntranceDate) {
		this.retirementEntranceDate = retirementEntranceDate;
	}

	public RetirementFundSetup getRetirementFundSetup() {
		return retirementFundSetup;
	}

	public void setRetirementFundSetup(RetirementFundSetup retirementFundSetup) {
		this.retirementFundSetup = retirementFundSetup;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bonus == null) ? 0 : bonus.hashCode());
		result = prime * result + ((helthInsurance == null) ? 0 : helthInsurance.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((loans == null) ? 0 : loans.hashCode());
		result = prime * result + ((retirementAge == null) ? 0 : retirementAge.hashCode());
		result = prime * result + ((retirementContribution == null) ? 0 : retirementContribution.hashCode());
		result = prime * result + ((retirementEntranceDate == null) ? 0 : retirementEntranceDate.hashCode());
		result = prime * result + retirementFrequency;
		result = prime * result + ((retirementFundSetup == null) ? 0 : retirementFundSetup.hashCode());
		result = prime * result + ((retirementPlanAccountNumber == null) ? 0 : retirementPlanAccountNumber.hashCode());
		result = prime * result + ((retirementPlanAmount == null) ? 0 : retirementPlanAmount.hashCode());
		result = prime * result
				+ ((retirementPlanArbicDescription == null) ? 0 : retirementPlanArbicDescription.hashCode());
		result = prime * result + ((retirementPlanDescription == null) ? 0 : retirementPlanDescription.hashCode());
		result = prime * result + ((retirementPlanId == null) ? 0 : retirementPlanId.hashCode());
		result = prime * result + ((retirementPlanMatchPercent == null) ? 0 : retirementPlanMatchPercent.hashCode());
		result = prime * result + ((retirementPlanMaxAmount == null) ? 0 : retirementPlanMaxAmount.hashCode());
		result = prime * result + ((retirementPlanMaxPercent == null) ? 0 : retirementPlanMaxPercent.hashCode());
		result = prime * result + ((retirementPlanPercent == null) ? 0 : retirementPlanPercent.hashCode());
		result = prime * result + watingMethod;
		result = prime * result + ((watingPeriods == null) ? 0 : watingPeriods.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}
	

}
