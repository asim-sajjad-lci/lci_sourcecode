package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PositionSetup;

@Repository("repositoryPositionSteup")
public interface RepositoryPositionSteup extends JpaRepository<PositionSetup, Integer>{

	PositionSetup findByIdAndIsDeleted(Integer id, boolean b);

	List<PositionSetup> findByIsDeleted(boolean b, Pageable pageable);

	List<PositionSetup> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select count(*) from PositionSetup s where s.isDeleted=false")
	Integer getCountOfTotalPosition();

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PositionSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSinglePosition(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from PositionSetup d where (  d.documentAttachmenDesc like :searchKeyWord  or d.documentAttachmentName like :searchKeyWord) and d.isDeleted=false")
	Integer predictivePositionSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from PositionSetup d where (  d.documentAttachmenDesc like :searchKeyWord  or d.documentAttachmentName like :searchKeyWord) and d.isDeleted=false")
	List<PositionSetup> predictivePositionSearchSetupWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query("select count(*) from PositionSetup d  where ( d.position.positionId like :searchKeyWord or d.documentAttachmenDesc like :searchKeyWord) and d.position.id =:positionId and d.isDeleted=false")
	Integer findAttachmentByPositionIdCount(@Param("searchKeyWord") String searchKeyWord, @Param("positionId") Integer positionId);

	@Query("select d from PositionSetup d  where ( d.position.positionId like :searchKeyWord or d.documentAttachmenDesc like :searchKeyWord) and d.position.id =:positionId and d.isDeleted=false")
	List<PositionSetup> findAttachmentByPositionId(@Param("searchKeyWord") String searchKeyWord, @Param("positionId") Integer positionId,Pageable pageable);
	
	@Query("select  count(*)  from PositionSetup d  where d.position.id =:positionId and d.isDeleted=false")
	Integer findAttachmentByPositionIdCount(@Param("positionId") Integer positionId);

	@Query("select count(*) from PositionSetup p ")
	public Integer getCountOfTotaPositionSteup();

}
