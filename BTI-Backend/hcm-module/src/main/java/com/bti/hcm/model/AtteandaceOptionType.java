package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40211",indexes = {
        @Index(columnList = "id")
})
@NamedQuery(name = "AtteandaceOptionType.findAll", query = "SELECT d FROM AtteandaceOptionType d")
public class AtteandaceOptionType extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "RESOSECQN")
	private Integer seqn;

	@Column(name = "RESOINDX")
	private Integer reasonIndx;

	@Column(name = "RESODSCR", columnDefinition = "char(31)")
	private String reasonDesc;

	@Column(name = "ATTTYSECQN")
	private Integer atteandanceTypeSeqn;

	@Column(name = "ATTTYINDX")
	private Integer atteandanceTypeIndx;

	@Column(name = "ATTTYDSCR", columnDefinition = "char(31)")
	private String atteandanceTypeDesc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSeqn() {
		return seqn;
	}

	public void setSeqn(Integer seqn) {
		this.seqn = seqn;
	}

	public Integer getReasonIndx() {
		return reasonIndx;
	}

	public void setReasonIndx(Integer reasonIndx) {
		this.reasonIndx = reasonIndx;
	}

	public String getReasonDesc() {
		return reasonDesc;
	}

	public void setReasonDesc(String reasonDesc) {
		this.reasonDesc = reasonDesc;
	}

	public Integer getAtteandanceTypeSeqn() {
		return atteandanceTypeSeqn;
	}

	public void setAtteandanceTypeSeqn(Integer atteandanceTypeSeqn) {
		this.atteandanceTypeSeqn = atteandanceTypeSeqn;
	}

	public Integer getAtteandanceTypeIndx() {
		return atteandanceTypeIndx;
	}

	public void setAtteandanceTypeIndx(Integer atteandanceTypeIndx) {
		this.atteandanceTypeIndx = atteandanceTypeIndx;
	}

	public String getAtteandanceTypeDesc() {
		return atteandanceTypeDesc;
	}

	public void setAtteandanceTypeDesc(String atteandanceTypeDesc) {
		this.atteandanceTypeDesc = atteandanceTypeDesc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((atteandanceTypeDesc == null) ? 0 : atteandanceTypeDesc.hashCode());
		result = prime * result + ((atteandanceTypeIndx == null) ? 0 : atteandanceTypeIndx.hashCode());
		result = prime * result + ((atteandanceTypeSeqn == null) ? 0 : atteandanceTypeSeqn.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((reasonDesc == null) ? 0 : reasonDesc.hashCode());
		result = prime * result + ((reasonIndx == null) ? 0 : reasonIndx.hashCode());
		result = prime * result + ((seqn == null) ? 0 : seqn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
