package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoMiscellaneousBenefits;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceMiscellaneousBenefits;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/miscellaneousBenefits")
public class ControllerMiscellaneousBenefits extends BaseController{

	@Autowired
	ServiceMiscellaneousBenefits serviceMiscellaneousBenefits;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerMiscellaneousBenefits.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @param request{
		  
		  "BenefitsId" : "1",
		  "desc" : "test ",
		  "arbicDesc" : "test",
		  "inactive" : true,
		  "dudctionAmount" : 0,
		  "dudctionPercent" : 0,
		  "monthlyAmount" : 0,
				  "yearlyAmount" :2
		}
	 * @param dtoMiscellaneousBenefits
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createMiscellaneousBenefits(HttpServletRequest request,@RequestBody DtoMiscellaneousBenefits dtoMiscellaneousBenefits) throws Exception{
		LOGGER.info("Create createMiscellaneousBenefits Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoMiscellaneousBenefits = serviceMiscellaneousBenefits.saveOrUpdate(dtoMiscellaneousBenefits);
			responseMessage=displayMessage(dtoMiscellaneousBenefits, "MISCELLANEOUS_BENEFIT_CREATED", "MISCELLANEOUS_BENEFIT_NOT_CREATED", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		LOGGER.debug("Create createMiscellaneousBenefits Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	/**
	 * @param request{
		    "pageNumber": "0",
			 "pageSize": "10"
			
		}
	 * @param dtoMiscellaneousBenefits
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllBenefitId", method = RequestMethod.GET)
	public ResponseMessage getAllMiscellaneousBenefits(HttpServletRequest request) throws Exception {
		LOGGER.info("Get All getAllMiscellaneousBenefits Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceMiscellaneousBenefits.getAll();
			responseMessage=displayMessage(dtoSearch, MessageConstant.MISCELLANEOUS_BENEFIT_GET_ALL, "MISCELLANEOUS_BENEFIT_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get All getAllMiscellaneousBenefits Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
		  "id" : 1,
				  "BenefitsId" : "1",
		  "desc" : "test ",
		  "arbicDesc" : "test",
		  
		  "inactive" : true,
		  
		  "dudctionAmount" : 0,
		  "dudctionPercent" : 0,
		  "monthlyAmount" : 0,
				  "yearlyAmount" :2
		}
	 * @param dtoMiscellaneousBenefits
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateMiscellaneousBenefits(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefits dtoMiscellaneousBenefits) throws Exception {
		LOGGER.info("Update MiscellaneousBenefits Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoMiscellaneousBenefits = serviceMiscellaneousBenefits.saveOrUpdate(dtoMiscellaneousBenefits);
			responseMessage=displayMessage(dtoMiscellaneousBenefits, "MISCELLANEOUS_BENEFIT_UPDATED_SUCCESS", "MISCELLANEOUS_BENEFIT_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update MiscellaneousBenefits Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
		  "ids" : [1]
		}
	 * @param dtoMiscellaneousBenefits
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteMiscellaneousBenefits(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefits dtoMiscellaneousBenefits) throws Exception {
		LOGGER.info("Delete MiscellaneousBenefits Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoMiscellaneousBenefits.getIds() != null && !dtoMiscellaneousBenefits.getIds().isEmpty()) {
				DtoMiscellaneousBenefits dtoPosition1 = serviceMiscellaneousBenefits.deleteHelthInsurance(dtoMiscellaneousBenefits.getIds());
				if(dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("MISCELLANEOUS_BENEFIT_DELETED", false), dtoPosition1);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("MISCELLANEOUS_BENEFIT_NOT_DELETE_ID_MESSAGE", false), dtoPosition1);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete deletePositionBudget Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  
		  "id" : 1
  
}
	 * @param dtoMiscellaneousBenefits
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getMiscellaneousBenefitsId", method = RequestMethod.POST)
	public ResponseMessage getMiscellaneousBenefitsId(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefits dtoMiscellaneousBenefits) throws Exception {
		LOGGER.info("getMiscellaneousBenefitsId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			DtoMiscellaneousBenefits dtoPositionObj = serviceMiscellaneousBenefits.getById(dtoMiscellaneousBenefits.getId());
			responseMessage=displayMessage(dtoPositionObj, "MISCELLANEOUS_BENEFIT_GET_DETAIL", MessageConstant.MISCELLANEOUS_BENEFIT_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("getMiscellaneousBenefitsId Method:"+dtoMiscellaneousBenefits.getId());
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch{
  
			"searchKeyword":"t",
		  "pageNumber" : 0,
		  "pageSize" :2
	}
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search MiscellaneousBenefits Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceMiscellaneousBenefits.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "MISCELLANEOUS_BENEFIT_GET_ALL", "MISCELLANEOUS_BENEFIT_NOT_GETTING", response);
			
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search MiscellaneousBenefits Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	
	/**
	 * @param request{
		  "BenefitsId" : "1"
  
		}
	 * @param dtoMiscellaneousBenefits
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/searchMiscellaneousId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchMiscellaneousId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Miscellaneous Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceMiscellaneousBenefits.searchMiscellaneousId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "MISCELLANEOUS_BENEFIT_GET_ALL", "MISCELLANEOUS_BENEFIT_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search MiscellaneousBenefits Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/miscellaneousBenefitsIdcheck", method = RequestMethod.POST)
	public ResponseMessage miscellaneousBenefitsIdcheck(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefits dtoMiscellaneousBenefits) throws Exception {
		LOGGER.info("miscellaneousBenefitsIdcheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoMiscellaneousBenefits  dtoMiscellaneousBenefitsObj= serviceMiscellaneousBenefits.repeatByBenefitsId(dtoMiscellaneousBenefits.getBenefitsId());
			if (dtoMiscellaneousBenefitsObj != null) {
				if (dtoMiscellaneousBenefitsObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("MISCELLANEOUS_BENEFIT_EXIST", false), dtoMiscellaneousBenefitsObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted(dtoMiscellaneousBenefitsObj.getMessageType(), false),
							dtoMiscellaneousBenefitsObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("MISCELLANEOUS_BENEFIT_REPEAT_NOT_FOUND", false), dtoMiscellaneousBenefitsObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
}
