package com.bti.hcm.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.ManualChecksHistoryYearDetails;
import com.bti.hcm.model.ManualChecksHistoryYearDistributions;
import com.bti.hcm.model.ManualChecksHistoryYearHeader;
import com.bti.hcm.model.ManualChecksOpenYearDetails;
import com.bti.hcm.model.ManualChecksOpenYearDistribution;
import com.bti.hcm.model.ManualChecksOpenYearHeader;
import com.bti.hcm.model.ParollTransactionOpenYearDistribution;
import com.bti.hcm.model.PayrollTransactionHistoryYearDetail;
import com.bti.hcm.model.PayrollTransactionHistoryYearDistribution;
import com.bti.hcm.model.PayrollTransactionHistoryYearHeader;
import com.bti.hcm.model.PayrollTransactionOpenYearDetails;
import com.bti.hcm.model.PayrollTransactionOpenYearHeader;
import com.bti.hcm.repository.ReposioryManualChecksOpenYearHeader;
import com.bti.hcm.repository.RepositorryManualChecksHistoryYearHeader;
import com.bti.hcm.repository.RepositoryManualChecksHistoryYearDetails;
import com.bti.hcm.repository.RepositoryManualChecksHistoryYearDistribution;
import com.bti.hcm.repository.RepositoryManualChecksOpenYearDetails;
import com.bti.hcm.repository.RepositoryManualChecksOpenYearDistribution;
import com.bti.hcm.repository.RepositoryParollTransactionOpenYearDistribution;
import com.bti.hcm.repository.RepositoryPayrollTransactionHistoryYearDetail;
import com.bti.hcm.repository.RepositoryPayrollTransactionHistoryYearDistribution;
import com.bti.hcm.repository.RepositoryPayrollTransactionHistoryYearHeader;
import com.bti.hcm.repository.RepositoryPayrollTransactionOpenYearDetails;
import com.bti.hcm.repository.RepositoryPayrollTransactionOpenYearHeader;

@Service
public class ServiceRoutinesUtilities {

	@Autowired
	private RepositoryPayrollTransactionOpenYearDetails transactionOpenYearDetails;

	@Autowired
	private RepositoryPayrollTransactionHistoryYearDetail historyYearDetail;

	@Autowired
	private RepositoryPayrollTransactionOpenYearHeader transactionOpenYearHeader;

	@Autowired
	private RepositoryPayrollTransactionHistoryYearHeader historyYearHeader;

	@Autowired
	private RepositoryParollTransactionOpenYearDistribution transactionOpenYearDistribution;

	@Autowired
	private RepositoryPayrollTransactionHistoryYearDistribution historyYearDistribution;

	@Autowired
	private ReposioryManualChecksOpenYearHeader manualChecksOpenYearHeader;

	@Autowired
	private RepositorryManualChecksHistoryYearHeader historyYearHeader2;

	@Autowired
	private RepositoryManualChecksOpenYearDetails manualChecksOpenYearDetails;

	@Autowired
	private RepositoryManualChecksHistoryYearDetails historyYearDetails;

	@Autowired
	private RepositoryManualChecksOpenYearDistribution checksOpenYearDistribution;

	@Autowired
	private RepositoryManualChecksHistoryYearDistribution historyYearDistribution2;

	public void getAndSave(Integer year) {

		List<PayrollTransactionOpenYearDetails> list = this.transactionOpenYearDetails.findByYear(year);
		for (PayrollTransactionOpenYearDetails openYearDetails : list) {
			PayrollTransactionHistoryYearDetail detail = new PayrollTransactionHistoryYearDetail();
			detail.setCreatedDate(new Date());
			detail.setIsDeleted(false);
			detail.setYear(year);
			detail.setAuditTransactionNumber(openYearDetails.getAuditTransactionNumber());
			detail.setEmployeeCodeSequence(openYearDetails.getCodeSequence());
			detail.setCodeType(openYearDetails.getCodeType());
			detail.setCodeIndexId(openYearDetails.getCodeIndexId());
			detail.setTotalAmount(openYearDetails.getTotalAmount());
			detail.setTotalActualAmount(openYearDetails.getTotalActualAmount());
			detail.setTotalPercentCode(openYearDetails.getTotalPercentcode());
			detail.setTotalHours(openYearDetails.getTotalHour());
			detail.setRateofBaseOnCode(openYearDetails.getRateBaseOnPayCode());
			detail.setCodeBaseOnCodeIndex(openYearDetails.getCodeBaseOnCodeIndex());
			detail.setCodeFromPeriodDate(openYearDetails.getCodeFromPeriodDate());
			detail.setCodeToPeriodDate(openYearDetails.getCodeToPeriodDate());
			detail.setPayrollPostByUserId(openYearDetails.getPayrollPostByUserId());
			detail.setPayrollPostDate(openYearDetails.getPayrollPostDate());
			this.historyYearDetail.saveAndFlush(detail);
			openYearDetails.setIsDeleted(true);
			this.transactionOpenYearDetails.saveAndFlush(openYearDetails);
		}

		List<PayrollTransactionOpenYearHeader> list1 = this.transactionOpenYearHeader.findByYear(year);
		for (PayrollTransactionOpenYearHeader openYearHeader : list1) {
			PayrollTransactionHistoryYearHeader header = new PayrollTransactionHistoryYearHeader();
			header.setCreatedDate(new Date());
			header.setIsDeleted(false);
			header.setYear(year);
			header.setBuildCheck(openYearHeader.getBuildCheck());
			header.setBuildCheckByUserId(openYearHeader.getBuildCheckByUserId());
			header.setPayPeriodfromDate(openYearHeader.getPayPeriodfromDate());
			header.setPayPeriodToDate(openYearHeader.getPayPeriodToDate());
			header.setPayrollPostDate(openYearHeader.getPayrollPostDate());
			header.setIncludePayPeriodsWeekly(openYearHeader.getIncludePayPeriodsWeekly());
			header.setIncludePayPeriodsByWeekly(openYearHeader.getIncludePayPeriodsByWeekly());
			header.setIncludePayPeriodsMonthly(openYearHeader.getIncludePayPeriodsMonthly());
			header.setIncludePayPeriodsSemimonthly(openYearHeader.getIncludePayPeriodsSemimonthly());
			header.setIncludePayPeriodsQuarterly(openYearHeader.getIncludePayPeriodsQuarterly());
			header.setIncludePayPeriodsAnnually(openYearHeader.getIncludePayPeriodsAnnually());
			header.setIncludePayPeriodsSemiannually(openYearHeader.getIncludePayPeriodsSemiannually());
			header.setIncludePayPeriodsDailyMisc(openYearHeader.getIncludePayPeriodsDailyMisc());
			header.setAllEmployees(openYearHeader.getAllEmployees());
			header.setFromEmployeeIndexId(openYearHeader.getFromEmployeeIndexId());
			header.setToEmployeeIndexID(openYearHeader.getToEmployeeIndexID());
			header.setAllDepartment(openYearHeader.getAllDepartment());
			header.setFromDepartmentIndexId(openYearHeader.getFromDepartmentIndexId());
			header.setToDepartmentIndexId(openYearHeader.getToDepartmentIndexId());
			this.historyYearHeader.saveAndFlush(header);
			openYearHeader.setIsDeleted(true);
			this.transactionOpenYearHeader.saveAndFlush(openYearHeader);
		}

		List<ParollTransactionOpenYearDistribution> list2 = this.transactionOpenYearDistribution.findByYear(year);
		for (ParollTransactionOpenYearDistribution openYearDistribution : list2) {
			PayrollTransactionHistoryYearDistribution distribution = new PayrollTransactionHistoryYearDistribution();
			distribution.setCreatedDate(new Date());
			distribution.setIsDeleted(false);
			distribution.setYear(year);
			distribution.setAuditTransactionNumber(openYearDistribution.getAuditTransactionNumber());
			distribution.setEmployeeId(openYearDistribution.getEmployeeIndexId());
			distribution.setAccountTableRowIndex(openYearDistribution.getAccountTableRowIndex());
			distribution.setPostingTypes(openYearDistribution.getPostingTypes());
			distribution.setCodeIndexId(openYearDistribution.getCodeIndexId());
			distribution.setDebitAmount(openYearDistribution.getDebitAmount());
			distribution.setCreditAmount(openYearDistribution.getCreaditAmount());
			distribution.setAccountSequence(openYearDistribution.getAccountSequence());
			this.historyYearDistribution.saveAndFlush(distribution);
			openYearDistribution.setIsDeleted(true);
			this.transactionOpenYearDistribution.saveAndFlush(openYearDistribution);
		}

		List<ManualChecksOpenYearHeader> list3 = this.manualChecksOpenYearHeader.findByYear(year);
		for (ManualChecksOpenYearHeader openYearHeader : list3) {
			ManualChecksHistoryYearHeader header = new ManualChecksHistoryYearHeader();
			header.setCreatedDate(new Date());
			header.setIsDeleted(false);
			header.setYear(year);
			header.setPaymentDescription(openYearHeader.getPaymentDescription());
			header.setBatches(openYearHeader.getBatches());
			header.setCheckDate(openYearHeader.getCheckDate());
			header.setCheckPostDate(openYearHeader.getCheckPostDate());
			header.setCheckNumber(openYearHeader.getCheckNumber());
			header.setEmployeeId(openYearHeader.getEmployeeId());
			header.setTotalNetAmpount(openYearHeader.getTotalNetAmount());
			header.setTaoalGrossAmount(openYearHeader.getTotalGrossAmount());
			this.historyYearHeader2.saveAndFlush(header);
			openYearHeader.setIsDeleted(true);
			this.manualChecksOpenYearHeader.saveAndFlush(openYearHeader);
		}

		List<ManualChecksOpenYearDetails> list4 = this.manualChecksOpenYearDetails.findByYear(year);
		for (ManualChecksOpenYearDetails openYearDetails : list4) {
			ManualChecksHistoryYearDetails details = new ManualChecksHistoryYearDetails();
			details.setCreatedDate(new Date());
			details.setIsDeleted(false);
			details.setYear(year);
			details.setPaymentSequence(openYearDetails.getPaymentSequence());
			details.setPaymentCodeType(openYearDetails.getPaymentCodeType());
			details.setCodeId(openYearDetails.getCodeId());
			details.setFromPeriodDate(openYearDetails.getFromPeriodDate());
			details.setToPeriodDate(openYearDetails.getToPeriodDate());
			details.setTotalAmount(openYearDetails.getTotalAmount());
			details.setTotalHours(openYearDetails.getTotalHours());
			details.setWeeksWorked(openYearDetails.getWeaksWorked());
			this.historyYearDetails.saveAndFlush(details);
			openYearDetails.setIsDeleted(true);
			this.manualChecksOpenYearDetails.saveAndFlush(openYearDetails);
		}

		List<ManualChecksOpenYearDistribution> list5 = this.checksOpenYearDistribution.findByYear(year);
		for (ManualChecksOpenYearDistribution openYearDistribution : list5) {
			ManualChecksHistoryYearDistributions distributions = new ManualChecksHistoryYearDistributions();
			distributions.setCreatedDate(new Date());
			distributions.setIsDeleted(false);
			distributions.setYear(year);
			distributions.setPostingTypes(openYearDistribution.getPostingTypes());
			distributions.setCodeId(openYearDistribution.getCodeId());
			distributions.setDebitAmount(openYearDistribution.getDebitAmount());
			distributions.setCreditAmount(openYearDistribution.getCreditAmount());
			distributions.setAccountSequence(openYearDistribution.getAccountSequence());
			distributions.setDepartment(openYearDistribution.getDepartment());
			distributions.setPosition(openYearDistribution.getPosition());
			this.historyYearDistribution2.saveAndFlush(distributions);
			openYearDistribution.setIsDeleted(true);
			this.checksOpenYearDistribution.saveAndFlush(openYearDistribution);
		}

	}

}
