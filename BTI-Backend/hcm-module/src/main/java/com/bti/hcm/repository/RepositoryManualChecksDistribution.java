package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecksDistribution;

@Repository("manualChecksDistribution")
public interface RepositoryManualChecksDistribution extends JpaRepository<ManualChecksDistribution, Integer>{

	 @Query("select count(*) from ManualChecksDistribution m ")
	public Integer getCountOfTotaLocation();

	public ManualChecksDistribution findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select e from ManualChecksDistribution e where e.isDeleted=false)")
	public List<ManualChecksDistribution> predictiveManualChecksDistributionSearchWithPagination(Pageable pageable);
	
	@Query("select count(*) from ManualChecksDistribution e where e.isDeleted=false)")
	public Integer predictiveManualChecksDistributionSearchTotalCount();

}
