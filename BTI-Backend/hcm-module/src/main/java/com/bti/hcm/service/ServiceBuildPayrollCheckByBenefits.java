package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.BuildChecks;
import com.bti.hcm.model.BuildPayrollCheckByBenefits;
import com.bti.hcm.model.dto.DtoBenefitCode;
import com.bti.hcm.model.dto.DtoBenefitCodeDisplay;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByBenefits;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByBenefits;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByDeductions;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByPayCodes;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;

@Service("/serviceBuildPayrollCheckByBenefits")
public class ServiceBuildPayrollCheckByBenefits {
	
	
static Logger log = Logger.getLogger(ServiceBuildPayrollCheckByBenefits.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired(required = false)
	RepositoryBuildPayrollCheckByPayCodes repositoryBuildPayrollCheckByPayCodes;
	
	@Autowired(required = false)
	RepositoryPayCode repositoryPayCode;
	
	@Autowired(required = false)
	RepositoryBuildChecks repositoryBuildChecks;
	
	@Autowired(required = false)
	RepositoryBuildPayrollCheckByDeductions repositoryBuildPayrollCheckByDeductions;

	@Autowired(required = false)
	RepositoryDeductionCode repositoryDeductionCode;
	
	@Autowired(required = false)
	RepositoryBuildPayrollCheckByBenefits repositoryBuildPayrollCheckByBenefits;
	
	@Autowired(required = false)
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	

	public DtoBuildPayrollCheckByBenefits saveOrUpdate(DtoBuildPayrollCheckByBenefits dtoBuildPayrollCheckByBenefits) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByBenefits.getListBuildChecks() != null && dtoBuildPayrollCheckByBenefits.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByBenefits.getListBuildChecks().get(0).getBuildChecks().getId());	
			}
			
			
			if(dtoBuildPayrollCheckByBenefits.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByBenefits.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
				repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByBenefits.getListBuildChecks().get(0).getBuildChecks().getId());
				}
				
				for (DtoBenefitCode  dtoBenefitCode : dtoBuildPayrollCheckByBenefits.getBenefitCode()) {
					BenefitCode benefitCode=repositoryBenefitCode.findByIdAndIsDeleted(dtoBenefitCode.getId(),false);
					BuildPayrollCheckByBenefits buildPayrollCheckByBenefits=new  BuildPayrollCheckByBenefits();
					buildPayrollCheckByBenefits.setCreatedDate(new Date());
					Integer rowId = repositoryBuildPayrollCheckByBenefits.findAll().size();
					Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					
					buildPayrollCheckByBenefits.setRowId(increment);
					buildPayrollCheckByBenefits.setBenefitCode(benefitCode);
					buildPayrollCheckByBenefits.setBuildChecks(buildChecks);
					repositoryBuildPayrollCheckByBenefits.saveAndFlush(buildPayrollCheckByBenefits);
		
				}
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBuildPayrollCheckByBenefits;
	}

	public DtoSearch getAllByCodeType(DtoBuildPayrollCheckByBenefits dtoBuildPayrollCheckByBenefits) {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoBenefitCodeDisplay> listResponse= new ArrayList<>();
		List<BuildPayrollCheckByBenefits> buildcheckList=this.repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(dtoBuildPayrollCheckByBenefits.getId());
		for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildcheckList) {
			DtoBenefitCodeDisplay dtoBuildPayrollCheckByBenefitss =new DtoBenefitCodeDisplay();
			
			if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
				dtoBuildPayrollCheckByBenefitss.setBenifitCodePrimaryId(buildPayrollCheckByBenefits.getBenefitCode().getId());
				dtoBuildPayrollCheckByBenefitss.setBenefitId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
		}
		if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
			dtoBuildPayrollCheckByBenefitss.setBuildChecksId(buildPayrollCheckByBenefits.getBuildChecks().getId());
		}
		listResponse.add(dtoBuildPayrollCheckByBenefitss);
		}		
		List<BenefitCode>list=this.repositoryBenefitCode.findByIsDeletedAndInActive(false, false);
		if(list!=null && !list.isEmpty()) {
			for (BenefitCode benefitCode : list) {
				if(benefitCode.isTransction()==false) {
					long count =listResponse.stream().filter(d-> d.getBenifitCodePrimaryId().equals(benefitCode.getId())).count();
					if(count == 0) {
						DtoBenefitCodeDisplay dtoBuildPayrollCheckByBenefitss=new DtoBenefitCodeDisplay();
						dtoBuildPayrollCheckByBenefitss.setBenifitCodePrimaryId(benefitCode.getId());
						dtoBuildPayrollCheckByBenefitss.setBenefitId(benefitCode.getBenefitId());
						listResponse.add(dtoBuildPayrollCheckByBenefitss);
					}
				}
			}
		}
		
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	}
	
	public DtoSearch getAllByCodeTypeHm(DtoBuildPayrollCheckByBenefits dtoBuildPayrollCheckByBenefits) throws ParseException {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoBenefitCodeDisplay> listResponse= new ArrayList<>();
		List<BuildPayrollCheckByBenefits> buildcheckList=this.repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(dtoBuildPayrollCheckByBenefits.getId());
		for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildcheckList) {
			DtoBenefitCodeDisplay dtoBuildPayrollCheckByBenefitss =new DtoBenefitCodeDisplay();
			
			if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
				dtoBuildPayrollCheckByBenefitss.setBenifitCodePrimaryId(buildPayrollCheckByBenefits.getBenefitCode().getId());
				dtoBuildPayrollCheckByBenefitss.setBenefitId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
		}
		if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
			dtoBuildPayrollCheckByBenefitss.setBuildChecksId(buildPayrollCheckByBenefits.getBuildChecks().getId());
		}
		listResponse.add(dtoBuildPayrollCheckByBenefitss);
		}
        //List<BenefitCode>list=this.repositoryBenefitCode.findByIsDeletedAndInActive(false, false);
		List<BenefitCode>list=this.repositoryEmployeeBenefitMaintenance.findByStartEndDate(dtoBuildPayrollCheckByBenefits.getStartDate(),dtoBuildPayrollCheckByBenefits.getEndDate());
		
		if(list!=null && !list.isEmpty()) {
			for (BenefitCode benefitCode : list) {
				if(benefitCode.isTransction()==false) {
					if(benefitCode.isInActive()==false) {
						long count =listResponse.stream().filter(d-> d.getBenifitCodePrimaryId().equals(benefitCode.getId())).count();
						if(count == 0) {
							DtoBenefitCodeDisplay dtoBuildPayrollCheckByBenefitss=new DtoBenefitCodeDisplay();
							dtoBuildPayrollCheckByBenefitss.setBenifitCodePrimaryId(benefitCode.getId());
							dtoBuildPayrollCheckByBenefitss.setBenefitId(benefitCode.getBenefitId());
							listResponse.add(dtoBuildPayrollCheckByBenefitss);
						
						}
					}
						
					
				}
				
				
			}
		}
		
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	}

	
	
	public DtoSearch getAllByCodeTypeHm1(DtoBuildPayrollCheckByBenefits dtoBuildPayrollCheckByBenefits) throws ParseException {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoBenefitCodeDisplay> listResponse= new ArrayList<>();
		List<BuildPayrollCheckByBenefits> buildcheckList=this.repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(dtoBuildPayrollCheckByBenefits.getId());
		for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildcheckList) {
			DtoBenefitCodeDisplay dtoBuildPayrollCheckByBenefitss =new DtoBenefitCodeDisplay();
			
			if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
				dtoBuildPayrollCheckByBenefitss.setBenifitCodePrimaryId(buildPayrollCheckByBenefits.getBenefitCode().getId());
				dtoBuildPayrollCheckByBenefitss.setBenefitId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
		}
		if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
			dtoBuildPayrollCheckByBenefitss.setBuildChecksId(buildPayrollCheckByBenefits.getBuildChecks().getId());
		}
		listResponse.add(dtoBuildPayrollCheckByBenefitss);
		}
        //List<BenefitCode>list=this.repositoryBenefitCode.findByIsDeletedAndInActive(false, false);
		//List<BenefitCode>list=this.repositoryEmployeeBenefitMaintenance.findByStartEndDate(dtoBuildPayrollCheckByBenefits.getStartDate(),dtoBuildPayrollCheckByBenefits.getEndDate());
		List<BenefitCode>list=this.repositoryEmployeeBenefitMaintenance.findByBenefitId1();
		if(list!=null && !list.isEmpty()) {
			for (BenefitCode benefitCode : list) {
				if(benefitCode.isTransction()==false) {
					if(benefitCode.isInActive()==false) {
						if(benefitCode.getStartDate()!=null && benefitCode.getEndDate()!=null) {
							if(dtoBuildPayrollCheckByBenefits.getStartDate().getYear()+1900>=benefitCode.getStartDate().getYear()+1900) {
								if(benefitCode.getStartDate().getMonth()+1<=dtoBuildPayrollCheckByBenefits.getStartDate().getMonth()+1) {
									if(benefitCode.getEndDate().getMonth()+1>=dtoBuildPayrollCheckByBenefits.getEndDate().getMonth()+1) {
										long count =listResponse.stream().filter(d-> d.getBenifitCodePrimaryId().equals(benefitCode.getId())).count();
										if(count == 0) {
											DtoBenefitCodeDisplay dtoBuildPayrollCheckByBenefitss=new DtoBenefitCodeDisplay();
											dtoBuildPayrollCheckByBenefitss.setBenifitCodePrimaryId(benefitCode.getId());
											dtoBuildPayrollCheckByBenefitss.setBenefitId(benefitCode.getBenefitId());
											listResponse.add(dtoBuildPayrollCheckByBenefitss);
										
										}
									}
									
								}
							}
						}
						
					}
						
					
				}
				
				
			}
		}
		
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	}
	
	public DtoSearch getAllByCodeTypeHm2(DtoBuildPayrollCheckByBenefits dtoBuildPayrollCheckByBenefits) throws ParseException {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoBenefitCodeDisplay> listResponse= new ArrayList<>();
		List<BuildPayrollCheckByBenefits> buildcheckList=this.repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(dtoBuildPayrollCheckByBenefits.getId());
		for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildcheckList) {
			DtoBenefitCodeDisplay dtoBuildPayrollCheckByBenefitss =new DtoBenefitCodeDisplay();
			
			if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
				dtoBuildPayrollCheckByBenefitss.setBenifitCodePrimaryId(buildPayrollCheckByBenefits.getBenefitCode().getId());
				dtoBuildPayrollCheckByBenefitss.setBenefitId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
		}
		if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
			dtoBuildPayrollCheckByBenefitss.setBuildChecksId(buildPayrollCheckByBenefits.getBuildChecks().getId());
		}
		listResponse.add(dtoBuildPayrollCheckByBenefitss);
		}
        //List<BenefitCode>list=this.repositoryBenefitCode.findByIsDeletedAndInActive(false, false);
		List<BenefitCode>list=this.repositoryEmployeeBenefitMaintenance.findByStartEndDate1(dtoBuildPayrollCheckByBenefits.getStartDate(),dtoBuildPayrollCheckByBenefits.getEndDate());
		
		if(list!=null && !list.isEmpty()) {
			for (BenefitCode benefitCode : list) {
				if(benefitCode.isTransction()==false) {
					if(benefitCode.isInActive()==false) {
						long count =listResponse.stream().filter(d-> d.getBenifitCodePrimaryId().equals(benefitCode.getId())).count();
						if(count == 0) {
							DtoBenefitCodeDisplay dtoBuildPayrollCheckByBenefitss=new DtoBenefitCodeDisplay();
							dtoBuildPayrollCheckByBenefitss.setBenifitCodePrimaryId(benefitCode.getId());
							dtoBuildPayrollCheckByBenefitss.setBenefitId(benefitCode.getBenefitId());
							listResponse.add(dtoBuildPayrollCheckByBenefitss);
						
						}
					}
						
					
				}
				
				
			}
		}
		
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	}
}