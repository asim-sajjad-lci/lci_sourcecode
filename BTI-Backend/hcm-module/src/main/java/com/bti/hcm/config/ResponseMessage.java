/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.config;

import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: ResponseMessage
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseMessage {

	private int code;
	private org.springframework.http.HttpStatus status;
	private String message;
	private Object result;
	private Object btiMessage;

	public ResponseMessage() {
	}

	/**
	 * @param code
	 * @param status
	 * @param message
	 */
	public ResponseMessage(int code, org.springframework.http.HttpStatus status, String message) {
		this.code = code;
		this.status = status;
		this.message = message;
	}

	/**
	 * @param code
	 * @param status
	 * @param message
	 */
	public ResponseMessage(int code, org.springframework.http.HttpStatus status, DtoBtiMessageHcm message) {
		this.code = code;
		this.status = status;
		this.btiMessage = message;
	}

	/**
	 * @param code
	 * @param status
	 * @param message
	 * @param result
	 */
	public ResponseMessage(int code, org.springframework.http.HttpStatus status, DtoBtiMessageHcm message, Object result) {
		this.code = code;
		this.status = status;
		this.btiMessage = message;
		this.result = result;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public org.springframework.http.HttpStatus getStatus() {
		return status;
	}

	public void setStatus(org.springframework.http.HttpStatus status) {
		this.status = status;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Object getBtiMessage() {
		return btiMessage;
	}

	public void setBtiMessage(Object btiMessage) {
		this.btiMessage = btiMessage;
	}

}

