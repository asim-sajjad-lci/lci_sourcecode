package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TraningCourse;

@Repository("repositoryTraningCourse")
public interface RepositoryTraningCourse extends JpaRepository<TraningCourse, Integer> {

	TraningCourse findByIdAndIsDeleted(Integer id, boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TraningCourse t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	public void deleteSingleTraningCourse(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	
	
	@Query("select count(*) from TraningCourse t where (t.traningId LIKE :searchKeyWord or t.desc LIKE :searchKeyWord or t.arbicDesc LIKE :searchKeyWord or "
			+ " t.location Like :searchKeyWord or t.prerequisiteId LIKE :searchKeyWord ) and t.isDeleted=false")
	Integer predictiveTraningCourseSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);

	
	@Query("select t from TraningCourse t where (t.traningId LIKE :searchKeyWord or t.desc LIKE :searchKeyWord  or t.arbicDesc LIKE :searchKeyWord or "
			+ " t.location LIKE :searchKeyWord or t.prerequisiteId LIKE :searchKeyWord ) and t.isDeleted=false")
	List<TraningCourse> predictiveTraningCourseSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select t from TraningCourse t where (t.traningId =:traningId) and t.isDeleted=false")
	List<TraningCourse> findByTrainingCourseTraningId(@Param("traningId")String traningId);

	@Query("select t from TraningCourse t where (t.traningId LIKE :searchKeyWord or t.desc LIKE :searchKeyWord  or t.arbicDesc LIKE :searchKeyWord) and t.isDeleted=false")
	List<TraningCourse> predictivegetAllTraningCourseIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	
	@Query("select t from TraningCourse t where (t.traningId like :searchKeyWord) and t.isDeleted=false order by t.id desc")
	List<TraningCourse> predictiveTrainingIdSearchWithPagination(@Param("searchKeyWord")String string);

	
	
	@Query("select t from TraningCourse t where (t.id =:id) and t.isDeleted=false")
	List<TraningCourse> predictivegetAllTraningCourseIdSearchWithPagination(@Param("id") Integer id,Pageable pageable);

	@Query("select t.traningId from TraningCourse t where (t.traningId like :traningId) and t.isDeleted=false order by t.id desc")
	List<String> predictiveSearchTraningIdWithPagination(@Param("traningId")String traningId);
	
	
	List<TraningCourse> findByIsDeleted(boolean isdeleted);
	@Query("select t from TraningCourse t where (t.traningId like :searchKeyWord) and t.isDeleted=false order by t.id desc")
	List<TraningCourse> predictiveSearchAllTraningIdWithPagination(@Param("searchKeyWord")String traningId);

	@Query("select count(*) from TraningCourse t ")
	public Integer getCountOfTotalTraningCourse();

	
}
