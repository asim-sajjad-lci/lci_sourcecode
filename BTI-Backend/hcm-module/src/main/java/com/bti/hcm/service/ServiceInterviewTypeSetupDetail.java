/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.InterviewTypeSetup;
import com.bti.hcm.model.InterviewTypeSetupDetail;
import com.bti.hcm.model.dto.DtoInterviewTypeSetupDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryInterviewTypeSetup;
import com.bti.hcm.repository.RepositoryInterviewTypeSetupDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service InterviewTypeSetupDetail
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("serviceInterviewTypeSetupDetail")
public class ServiceInterviewTypeSetupDetail {
	
	

	/**
	 * @Description LOGGER use for put a logger in InterviewTypeSetupDetail  Service
	 */
	static Logger log = Logger.getLogger(ServiceInterviewTypeSetup.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in InterviewTypeSetupDetail  service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in InterviewTypeSetupDetail  service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in InterviewTypeSetupDetail  service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryInterviewTypeSetupDetail Autowired here using annotation of spring for access of repositoryInterviewTypeSetupDetail method in InterviewTypeSetup service
	 * 				In short Access InterviewTypeSetupDetail Query from Database using repositoryInterviewTypeSetupDetail.
	 */
	@Autowired
	RepositoryInterviewTypeSetupDetail repositoryInterviewTypeSetupDetail;
	
	/**
	 * @Description repositoryInterviewTypeSetup Autowired here using annotation of spring for access of repositoryInterviewTypeSetup method in InterviewTypeSetup service
	 * 				In short Access InterviewTypeSetup Query from Database using repositoryInterviewTypeSetup.
	 */
	@Autowired
	RepositoryInterviewTypeSetup repositoryInterviewTypeSetup;
	
	
	/**
	 *  @Description: save and update InterviewTypeSetupDetail  data
	 * @param dtoInterviewTypeSetupDetail
	 * @return
	 * @throws ParseException
	 */
	public DtoInterviewTypeSetupDetail saveOrUpdateInterviewTypeSetupDetail(DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail) throws ParseException {
		log.info("saveOrUpdateInterviewTypeSetupDetail Method");
		InterviewTypeSetupDetail interviewTypeSetupDetail = null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			InterviewTypeSetup interviewTypeSetup = repositoryInterviewTypeSetup.findOne(dtoInterviewTypeSetupDetail.getInterviewTypeSetupId());
			if (dtoInterviewTypeSetupDetail.getId() != null && dtoInterviewTypeSetupDetail.getId() > 0) {
				interviewTypeSetupDetail = repositoryInterviewTypeSetupDetail.findByIdAndIsDeleted(dtoInterviewTypeSetupDetail.getId(), false);
				interviewTypeSetupDetail.setUpdatedBy(loggedInUserId);
				interviewTypeSetupDetail.setUpdatedDate(new Date());
			} else {
				interviewTypeSetupDetail = new InterviewTypeSetupDetail();
				interviewTypeSetupDetail.setCreatedDate(new Date());
				Integer rowId = repositoryInterviewTypeSetupDetail.getCountOfTotalInterviewTypeSetupDetails();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				interviewTypeSetupDetail.setRowId(increment);
				
			}
			interviewTypeSetupDetail.setInterviewTypeSetupDetailDescription(dtoInterviewTypeSetupDetail.getInterviewTypeSetupDetailDescription());
			interviewTypeSetupDetail.setInterviewTypeSetupDetailCategoryRowSequance(dtoInterviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryRowSequance());
			interviewTypeSetupDetail.setInterviewTypeSetupDetailCategorySequance(dtoInterviewTypeSetupDetail.getInterviewTypeSetupDetailCategorySequance());
			interviewTypeSetupDetail.setInterviewTypeSetupDetailCategoryWeight(dtoInterviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryWeight());
			interviewTypeSetupDetail.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			interviewTypeSetupDetail.setInterviewTypeSetup(interviewTypeSetup);
			repositoryInterviewTypeSetupDetail.saveAndFlush(interviewTypeSetupDetail);
			log.debug("InterviewTypeSetup is:"+dtoInterviewTypeSetupDetail.getId());
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoInterviewTypeSetupDetail;

	}
	
	public DtoInterviewTypeSetupDetail getDtoInterviewTypeSetupDetail(InterviewTypeSetupDetail interviewTypeSetupDetail) {
	
		DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail = new DtoInterviewTypeSetupDetail(interviewTypeSetupDetail);
		
		dtoInterviewTypeSetupDetail.setId(interviewTypeSetupDetail.getId());
		dtoInterviewTypeSetupDetail.setInterviewDetailId(interviewTypeSetupDetail.getId());
		
		dtoInterviewTypeSetupDetail.setInterviewTypeSetupDetailDescription(interviewTypeSetupDetail.getInterviewTypeSetupDetailDescription());
		dtoInterviewTypeSetupDetail.setInterviewTypeSetupDetailCategoryRowSequance(interviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryRowSequance());
		dtoInterviewTypeSetupDetail.setInterviewTypeSetupDetailCategorySequance(interviewTypeSetupDetail.getInterviewTypeSetupDetailCategorySequance());
		dtoInterviewTypeSetupDetail.setInterviewTypeSetupDetailCategoryWeight(interviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryWeight());
		
		if(interviewTypeSetupDetail.getInterviewTypeSetup()!=null) {
			if(interviewTypeSetupDetail.getInterviewTypeSetup().getInterviewTypeId()!=null) {
				dtoInterviewTypeSetupDetail.setInterviewTypeId(interviewTypeSetupDetail.getInterviewTypeSetup().getInterviewTypeId());
			}
			dtoInterviewTypeSetupDetail.setInterviewSetupId(interviewTypeSetupDetail.getInterviewTypeSetup().getId());
			dtoInterviewTypeSetupDetail.setInterviewTypeDesc(interviewTypeSetupDetail.getInterviewTypeSetup().getInterviewTypeDescription());
			dtoInterviewTypeSetupDetail.setInterviewTypeArbicDesc(interviewTypeSetupDetail.getInterviewTypeSetup().getInterviewTypeDescriptionArabic());
			dtoInterviewTypeSetupDetail.setInterviewTypePrimaryId(interviewTypeSetupDetail.getInterviewTypeSetup().getId());
		}
		return dtoInterviewTypeSetupDetail;
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchInterviewTypeSetupDetail(DtoSearch dtoSearch) {
		
		try {

			log.info("searchInterviewTypeSetupDetail Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";

				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "interviewTypeSetupDetailCategoryRowSequance" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "interviewTypeSetupDetailDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "interviewTypeSetupDetailCategorySequance" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "interviewTypeSetupDetailCategoryWeight" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
					
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
				
			}
				dtoSearch.setTotalCount(this.repositoryInterviewTypeSetupDetail.predictiveInterviewTypeSetupDetailSearchTotalCount("%"+searchWord+"%"));
				List<InterviewTypeSetupDetail> interviewTypeSetupDetailList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						interviewTypeSetupDetailList = this.repositoryInterviewTypeSetupDetail.predictiveInterviewTypeSetupDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						interviewTypeSetupDetailList = this.repositoryInterviewTypeSetupDetail.predictiveInterviewTypeSetupDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						interviewTypeSetupDetailList = this.repositoryInterviewTypeSetupDetail.predictiveInterviewTypeSetupDetailSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(interviewTypeSetupDetailList != null && !interviewTypeSetupDetailList.isEmpty()){
					List<DtoInterviewTypeSetupDetail> dtoInterviewTypeSetupDetailList = new ArrayList<>();
					for (InterviewTypeSetupDetail interviewTypeSetupDetail : interviewTypeSetupDetailList) {
						DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail = getDtoInterviewTypeSetupDetail(interviewTypeSetupDetail);
						dtoInterviewTypeSetupDetailList.add(dtoInterviewTypeSetupDetail);
					}
					dtoSearch.setRecords(dtoInterviewTypeSetupDetailList);
				}
			}
			log.debug("Search InterviewTypeSetupDetail Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public DtoInterviewTypeSetupDetail getInterviewTypeSetupDetailByInterviewTypeSetupDetailId(int id) {
		log.info("getInterviewTypeSetupDetailByInterviewTypeSetupDetailId Method");
		DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail = new DtoInterviewTypeSetupDetail();
		try {
			if (id > 0) {
				InterviewTypeSetupDetail interviewTypeSetupDetail = repositoryInterviewTypeSetupDetail.findByIdAndIsDeleted(id, false);
				if (interviewTypeSetupDetail != null) {
					 dtoInterviewTypeSetupDetail = getDtoInterviewTypeSetupDetail(interviewTypeSetupDetail);
				} else {
					dtoInterviewTypeSetupDetail.setMessageType("INTERVIEW_TYPE_SETUP_DETAIL_NOT_GETTING");
				}
			} else {
				dtoInterviewTypeSetupDetail.setMessageType("INVALID_INTERVIEW_TYPE_SETUP_DETAIL_ID");

			}
			log.debug("InterviewTypeSetupDetail By Id is:"+dtoInterviewTypeSetupDetail.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoInterviewTypeSetupDetail;
	}

	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoInterviewTypeSetupDetail deleteInterviewTypeSetupDetail(List<Integer> ids) {
		log.info("deleteInterviewTypeSetupDetail Method");
		DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail = new DtoInterviewTypeSetupDetail();
		dtoInterviewTypeSetupDetail
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("INTERVIEW_TYPE_SETUP_DETAIL_DELETED", false));
		dtoInterviewTypeSetupDetail.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("INTERVIEW_TYPE_SETUP_DETAIL_ASSOCIATED", false));
		List<DtoInterviewTypeSetupDetail> deleteDtoInterviewTypeSetupDetailList = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer id : ids) {
				InterviewTypeSetupDetail interviewTypeSetupDetail = repositoryInterviewTypeSetupDetail.findOne(id);
				DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail2 = new DtoInterviewTypeSetupDetail();
				dtoInterviewTypeSetupDetail2.setId(id);
				dtoInterviewTypeSetupDetail2.setInterviewTypeSetupDetailDescription(interviewTypeSetupDetail.getInterviewTypeSetupDetailDescription());
				dtoInterviewTypeSetupDetail2.setInterviewTypeSetupDetailCategoryRowSequance(interviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryRowSequance());
				dtoInterviewTypeSetupDetail2.setInterviewTypeSetupDetailCategorySequance(interviewTypeSetupDetail.getInterviewTypeSetupDetailCategorySequance());
				dtoInterviewTypeSetupDetail2.setInterviewTypeSetupDetailCategoryWeight(interviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryWeight());
				repositoryInterviewTypeSetupDetail.deleteSingleInterviewTypeSetupDetail(true, loggedInUserId, id);
				deleteDtoInterviewTypeSetupDetailList.add(dtoInterviewTypeSetupDetail2);

			}
			dtoInterviewTypeSetupDetail.setDeleteInterviewTypeSetupDetail(deleteDtoInterviewTypeSetupDetailList);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete InterviewTypeSetupDetail :"+dtoInterviewTypeSetupDetail.getId());
		return dtoInterviewTypeSetupDetail;
	}

	/**
	 * 
	 * @param integer
	 * @return
	 */
	public DtoInterviewTypeSetupDetail repeatByDtoInterviewTypeSetupDetailId(Integer integer) {
		log.info("repeatByDtoInterviewTypeSetupDetailId Method");
		DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail = new DtoInterviewTypeSetupDetail();
		try {
			List<InterviewTypeSetupDetail> interviewTypeSetupDetailList=repositoryInterviewTypeSetupDetail.findByInterviewTypeSetupDetailId(integer);
			if(interviewTypeSetupDetailList!=null && !interviewTypeSetupDetailList.isEmpty()) {
				dtoInterviewTypeSetupDetail.setIsRepeat(true);
			}else {
				dtoInterviewTypeSetupDetail.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoInterviewTypeSetupDetail;
	}
	
	public List<DtoInterviewTypeSetupDetail> getAllInterviewTypeSetupDetailDropDownList() {
		log.info("getAllInterviewTypeSetupDetailDropDownList  Method");
		List<DtoInterviewTypeSetupDetail> dtoInterviewTypeSetupDetailList = new ArrayList<>();
		try {
			List<InterviewTypeSetupDetail> list = repositoryInterviewTypeSetupDetail.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (InterviewTypeSetupDetail interviewTypeSetupDetail : list) {
					DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail = getDtoInterviewTypeSetupDetail(interviewTypeSetupDetail);
					dtoInterviewTypeSetupDetailList.add(dtoInterviewTypeSetupDetail);
				}
			}
			log.debug("InterviewTypeSetupDetail is:"+dtoInterviewTypeSetupDetailList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoInterviewTypeSetupDetailList;
	}
}
