package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR80000", indexes = { @Index(columnList = "HCMBATINDX") })
public class Batches extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMBATINDX")
	private Integer id;

	@Column(name = "BATCHID")
	private String batchId;

	@Column(name = "BADSCR")
	private String description;

	@Column(name = "BADSCRA")
	private String arabicDescription;

	@Column(name = "POSTINGDATE")
	private Date postingDate;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "batches")
	// @LazyCollection(LazyCollectionOption.FALSE)
	// @Where(clause = "is_deleted = false")
	private List<BuildPayrollCheckByBatches> listBuildPayrollCheckByBatches;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "batches")
	// @LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<TransactionEntry> listTransactionEntry;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "batches")
	// @LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<PayrollTransactionOpenYearDetails> listPayrollTransactionOpenYearDetails;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "batches")
	@Where(clause = "is_deleted = false")
	List<ManualChecksOpenYearHeader> listManualChecksOpenYearHeader;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "batches")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksHistoryYearHeader> listManualChecksHistoryYearHeader;

	@Column(name = "BACHTYPE")
	private short transactionType;

	@Column(name = "TOTTRXNO")
	private Integer quantityTotal;

	@Column(name = "TOTTRXAMT")
	private Double totalTransactions;

	@Column(name = "APPRVAL")
	private boolean approved;

	@Column(name = "APPRDDT")
	private Date approvedDate;

	@Column(name = "BACHSTAS")
	private short status;

	@Column(name = "APPRUSR")
	private String userID;

	@Column(name = "BATCHFROMBUILD")
	private Boolean fromBuild;

	@Column(name = "BATCHFORTRANX")
	private Boolean fromtranscation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public short getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(short transactionType) {
		this.transactionType = transactionType;
	}

	public Integer getQuantityTotal() {
		return quantityTotal;
	}

	public void setQuantityTotal(Integer quantityTotal) {
		this.quantityTotal = quantityTotal;
	}

	public Double getTotalTransactions() {
		return totalTransactions;
	}

	public void setTotalTransactions(Double totalTransactions) {
		this.totalTransactions = totalTransactions;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public List<BuildPayrollCheckByBatches> getListBuildPayrollCheckByBatches() {
		return listBuildPayrollCheckByBatches;
	}

	public void setListBuildPayrollCheckByBatches(List<BuildPayrollCheckByBatches> listBuildPayrollCheckByBatches) {
		this.listBuildPayrollCheckByBatches = listBuildPayrollCheckByBatches;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public List<TransactionEntry> getListTransactionEntry() {
		return listTransactionEntry;
	}

	public void setListTransactionEntry(List<TransactionEntry> listTransactionEntry) {
		this.listTransactionEntry = listTransactionEntry;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public List<PayrollTransactionOpenYearDetails> getListPayrollTransactionOpenYearDetails() {
		return listPayrollTransactionOpenYearDetails;
	}

	public void setListPayrollTransactionOpenYearDetails(
			List<PayrollTransactionOpenYearDetails> listPayrollTransactionOpenYearDetails) {
		this.listPayrollTransactionOpenYearDetails = listPayrollTransactionOpenYearDetails;
	}

	public Boolean getFromBuild() {
		return fromBuild;
	}

	public void setFromBuild(Boolean fromBuild) {
		this.fromBuild = fromBuild;
	}

	public List<ManualChecksOpenYearHeader> getListManualChecksOpenYearHeader() {
		return listManualChecksOpenYearHeader;
	}

	public void setListManualChecksOpenYearHeader(List<ManualChecksOpenYearHeader> listManualChecksOpenYearHeader) {
		this.listManualChecksOpenYearHeader = listManualChecksOpenYearHeader;
	}

	public List<ManualChecksHistoryYearHeader> getListManualChecksHistoryYearHeader() {
		return listManualChecksHistoryYearHeader;
	}

	public void setListManualChecksHistoryYearHeader(
			List<ManualChecksHistoryYearHeader> listManualChecksHistoryYearHeader) {
		this.listManualChecksHistoryYearHeader = listManualChecksHistoryYearHeader;
	}

	public Boolean getFromtranscation() {
		return fromtranscation;
	}

	public void setFromtranscation(Boolean fromtranscation) {
		this.fromtranscation = fromtranscation;
	}
	

}
