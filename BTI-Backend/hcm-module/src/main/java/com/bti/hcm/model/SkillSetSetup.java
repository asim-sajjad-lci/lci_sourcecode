package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40601",indexes = {
        @Index(columnList = "SKLSTINDX")
})
public class SkillSetSetup extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SKLSTINDX")
	private int id;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, mappedBy = "skillSetSetup")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<SkillSetDesc> listSkillSetDesc;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, mappedBy = "skillSetSetup")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<Position> listPosition;

	@Column(name = "SKLSTID", columnDefinition = "char(15)")
	private String skillSetId;

	@Column(name = "SKLSTDSCR", columnDefinition = "char(31)")
	private String skillSetDiscription;

	@Column(name = "SKLSTDSCRA", columnDefinition = "char(61)")
	private String arabicDiscription;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSkillSetId() {
		return skillSetId;
	}

	public void setSkillSetId(String skillSetId) {
		this.skillSetId = skillSetId;
	}

	public String getSkillSetDiscription() {
		return skillSetDiscription;
	}

	public void setSkillSetDiscription(String skillSetDiscription) {
		this.skillSetDiscription = skillSetDiscription;
	}

	public String getArabicDiscription() {
		return arabicDiscription;
	}

	public void setArabicDiscription(String arabicDiscription) {
		this.arabicDiscription = arabicDiscription;
	}

	public List<SkillSetDesc> getListSkillSetDesc() {
		return listSkillSetDesc;
	}

	public void setListSkillSetDesc(List<SkillSetDesc> listSkillSetDesc) {
		this.listSkillSetDesc = listSkillSetDesc;
	}

	public List<Position> getListPosition() {
		return listPosition;
	}

	public void setListPosition(List<Position> listPosition) {
		this.listPosition = listPosition;
	}

}
