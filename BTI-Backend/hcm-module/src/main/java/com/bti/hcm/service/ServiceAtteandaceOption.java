package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.AtteandaceOption;
import com.bti.hcm.model.dto.DtoAtteandaceOption;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAtteandaceOption;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceAtteandaceOption")
public class ServiceAtteandaceOption {
	/**
	 * @Description LOGGER use for put a logger in AtteandaceOption Service
	 */
	static Logger log = Logger.getLogger(ServiceAtteandaceOption.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in AtteandaceOption service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in Department service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in AtteandaceOption service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryAtteandaceOption Autowired here using annotation of spring for access of repositoryAtteandaceOption method in AtteandaceOption service
	 * 				In short Access AtteandaceOption Query from Database using repositoryAtteandaceOption.
	 */
	@Autowired
	RepositoryAtteandaceOption repositoryAtteandaceOption;
	
	public DtoAtteandaceOption saveOrUpdate(DtoAtteandaceOption dtoAtteandaceOption) {

		 try {
				log.info("saveOrUpdate Method");
				int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
				AtteandaceOption attaeandace = null;
				if (dtoAtteandaceOption.getId() != null && dtoAtteandaceOption.getId() > 0) {
					attaeandace = repositoryAtteandaceOption.findByIdAndIsDeleted(dtoAtteandaceOption.getId(), false);
					attaeandace.setUpdatedBy(loggedInUserId);
					attaeandace.setUpdatedDate(new Date());
				} else {
					attaeandace = new AtteandaceOption();
					attaeandace.setCreatedDate(new Date());
					Integer rowId = repositoryAtteandaceOption.getCountOfTotalAtteandaces();
					Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					attaeandace.setRowId(increment);
				}
				attaeandace.setSeqn(dtoAtteandaceOption.getSeqn());
				attaeandace.setReasonIndx(dtoAtteandaceOption.getReasonIndx());
				attaeandace.setReasonDesc(dtoAtteandaceOption.getReasonDesc());
				attaeandace.setAtteandanceTypeDesc(dtoAtteandaceOption.getAtteandanceTypeDesc());
				attaeandace.setAtteandanceTypeIndx(dtoAtteandaceOption.getAtteandanceTypeIndx());
				attaeandace.setAtteandanceTypeSeqn(dtoAtteandaceOption.getAtteandanceTypeSeqn());
				attaeandace.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				 repositoryAtteandaceOption.saveAndFlush(attaeandace);
				log.debug("AtteandaceOption is:"+dtoAtteandaceOption.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAtteandaceOption;
	}

	public DtoAtteandaceOption delete(List<Integer> ids) {
		log.info("deleteAtteandace Method");
		DtoAtteandaceOption dtoAttedanceOption = new DtoAtteandaceOption();
		dtoAttedanceOption
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ATTEDANCE_OPTION_DELETED", false));
		dtoAttedanceOption.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("ATTEDANCE_OPTION_ASSOCIATED", false));
		List<DtoAtteandaceOption> deleteAtteandaceOption = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer atteandaceOptionId : ids) {
				AtteandaceOption atteandaceOption = repositoryAtteandaceOption.findOne(atteandaceOptionId);
				DtoAtteandaceOption dtoAtteandaceOption2 = new DtoAtteandaceOption();
				dtoAtteandaceOption2.setId(atteandaceOption.getId());
				repositoryAtteandaceOption.deleteSingleAtteandaceOption(true, loggedInUserId, atteandaceOptionId);
				deleteAtteandaceOption.add(dtoAtteandaceOption2);

			}
			dtoAttedanceOption.setDelete(deleteAtteandaceOption);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Attandance :"+dtoAttedanceOption.getId());
		return dtoAttedanceOption;
	}

	public DtoSearch search(DtoSearch dtoSearch) {

		try {
			log.info("search Attandance Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "reasonIndx" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "reasonDesc" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "atteandanceTypeSeqn" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "atteandanceTypeIndx" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "atteandanceTypeDesc" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositoryAtteandaceOption.predictiveAtteandaceSearchTotalCount("%"+searchWord+"%"));
				List<AtteandaceOption>atteandaceList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						atteandaceList = this.repositoryAtteandaceOption.predictiveAtteandaceOptionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						atteandaceList = this.repositoryAtteandaceOption.predictiveAtteandaceOptionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						atteandaceList = this.repositoryAtteandaceOption.predictiveAtteandaceOptionSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				if(atteandaceList != null && !atteandaceList.isEmpty()){
					List<DtoAtteandaceOption> dtoAttedanceOptionsList = new ArrayList<>();
					for (AtteandaceOption department : atteandaceList) {
						DtoAtteandaceOption dtoAttedanceOption = getDtoAtteandaceOption(department);
						dtoAttedanceOptionsList.add(dtoAttedanceOption);
					}
					dtoSearch.setRecords(dtoAttedanceOptionsList);
				}
			}
			log.debug("Search Attandance Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public DtoAtteandaceOption getByAtteandaceId(Integer id) {
		log.info("getByAtteandaceId Method");
		DtoAtteandaceOption dtoPosition = new DtoAtteandaceOption();
		try {
			if (id > 0) {
				AtteandaceOption position = repositoryAtteandaceOption.findByIdAndIsDeleted(id, false);
				if (position != null) {
					dtoPosition = new DtoAtteandaceOption(position);
				} else {
					dtoPosition.setMessageType("SKILL_STEUP_NOT_GETTING");

				}
			} else {
				dtoPosition.setMessageType("INVALID_DEPARTMENT_ID");

			}
			log.debug("Atteandace By Id is:"+dtoPosition.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPosition;
	}
	
	public DtoSearch getAll(DtoAtteandaceOption dtoPositionBudget) {
		log.info("getAll Atteandace Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoPositionBudget.getPageNumber());
			dtoSearch.setPageSize(dtoPositionBudget.getPageSize());
			dtoSearch.setTotalCount(repositoryAtteandaceOption.getCountOfTotalAtteandace());
			List<AtteandaceOption> skillPositionList = null;
			if (dtoPositionBudget.getPageNumber() != null && dtoPositionBudget.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoPositionBudget.getPageNumber(), dtoPositionBudget.getPageSize(), Direction.DESC, "createdDate");
				skillPositionList = repositoryAtteandaceOption.findByIsDeleted(false, pageable);
			} else {
				skillPositionList = repositoryAtteandaceOption.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoAtteandaceOption> dtoPOsitionList=new ArrayList<>();
			if(skillPositionList!=null && !skillPositionList.isEmpty())
			{
				for (AtteandaceOption position : skillPositionList) 
				{
					dtoPositionBudget=new DtoAtteandaceOption(position);
					
					dtoPositionBudget.setSeqn(position.getSeqn());
					dtoPositionBudget.setId(position.getId());
					dtoPositionBudget.setReasonIndx(position.getReasonIndx());
					dtoPositionBudget.setReasonDesc(position.getReasonDesc());
					dtoPositionBudget.setAtteandanceTypeDesc(position.getAtteandanceTypeDesc());
					dtoPositionBudget.setAtteandanceTypeIndx(position.getAtteandanceTypeIndx());
					dtoPositionBudget.setAtteandanceTypeSeqn(position.getAtteandanceTypeSeqn());
					dtoPOsitionList.add(dtoPositionBudget);
				}
				dtoSearch.setRecords(dtoPOsitionList);
			}
			log.debug("All Atteandace List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoSearch getAllAccrualTypeId(DtoSearch dtoSearch) {

		try {
			log.info("getAllAccrualTypeId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				List<AtteandaceOption> timeCodeIdList = this.repositoryAtteandaceOption.getAllAccrualTypeId("%"+searchWord+"%");
					if(timeCodeIdList != null && !timeCodeIdList.isEmpty()){
						List<DtoAtteandaceOption> dtoAttedanceOptionsList = new ArrayList<>();
						for (AtteandaceOption department : timeCodeIdList) {
							DtoAtteandaceOption dtoAttedanceOption = getDtoAtteandaceOption(department);
							dtoAttedanceOptionsList.add(dtoAttedanceOption);
						}
						dtoSearch.setRecords(dtoAttedanceOptionsList);
					}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	public  DtoAtteandaceOption getDtoAtteandaceOption(AtteandaceOption attedanceOption) {
		
		DtoAtteandaceOption dtoAttedanceOption = new DtoAtteandaceOption(attedanceOption);
		dtoAttedanceOption.setSeqn(attedanceOption.getSeqn());
		dtoAttedanceOption.setId(attedanceOption.getId());
		dtoAttedanceOption.setReasonIndx(attedanceOption.getReasonIndx());
		dtoAttedanceOption.setReasonDesc(attedanceOption.getReasonDesc());
		dtoAttedanceOption.setAtteandanceTypeDesc(attedanceOption.getAtteandanceTypeDesc());
		dtoAttedanceOption.setAtteandanceTypeIndx(attedanceOption.getAtteandanceTypeIndx());
		dtoAttedanceOption.setAtteandanceTypeSeqn(attedanceOption.getAtteandanceTypeSeqn());
	return dtoAttedanceOption;
	}
}





