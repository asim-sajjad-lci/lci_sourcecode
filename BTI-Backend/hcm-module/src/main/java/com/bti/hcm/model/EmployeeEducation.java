package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00935",indexes = {
        @Index(columnList = "EDUCINDX")
})
@NamedQuery(name = "EmployeeEducation.findAll", query = "SELECT d FROM EmployeeEducation d")
public class EmployeeEducation extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EDUCINDX")
	private Integer id;

	@Column(name = "EDUCSCHNM", columnDefinition = "char(90)")
	private String schoolName;

	@Column(name = "EDUCSCMAJ", columnDefinition = "char(90)")
	private String major;
	
	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;

	@Column(name = "EDUCSCYR")
	private Integer endYear;

	@Column(name = "EDUCSCFYR")
	private Integer startYear;

	@Column(name = "EDUCSCDR",columnDefinition = "char(30)")
	private String degree;

	@Column(name = "EDUCSCGP")
	private BigDecimal gpa;

	@Column(name = "EDUCSCNO", columnDefinition = "char(150)")
	private String comments;

	@Column(name = "EDUCSEQN")
	private Integer educationSequence;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Integer getEndYear() {
		return endYear;
	}

	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}

	public Integer getStartYear() {
		return startYear;
	}

	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}
	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public BigDecimal getGpa() {
		return gpa;
	}

	public void setGpa(BigDecimal gpa) {
		this.gpa = gpa;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getEducationSequence() {
		return educationSequence;
	}

	public void setEducationSequence(Integer educationSequence) {
		this.educationSequence = educationSequence;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

}
