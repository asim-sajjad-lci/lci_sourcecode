/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.CheckbookMaintenance;
import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * Description: DTO PriceLevelSetup class having getter and setter for fields (POJO) Name
 * Name of Project: Ims
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoCheckbookMaintenance extends DtoBase{
	
	private Integer id;
	private String checkbookId;
	private String checkbookDescription;
	private String checkbookArabicDescription;
	private String bankIdAddress;
	private long accountTableRowIndex;
	private Integer nextCheckNumber;
	private Integer nextDepositNumber;
	private List<DtoCheckbookMaintenance> delete;
	
	public DtoCheckbookMaintenance() {
		
	}
	
	public DtoCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookId = checkbookMaintenance.getCheckbookId();
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getCheckbookDescription() {
		return checkbookDescription;
	}

	public void setCheckbookDescription(String checkbookDescription) {
		this.checkbookDescription = checkbookDescription;
	}

	public String getCheckbookArabicDescription() {
		return checkbookArabicDescription;
	}

	public void setCheckbookArabicDescription(String checkbookArabicDescription) {
		this.checkbookArabicDescription = checkbookArabicDescription;
	}

	public String getBankIdAddress() {
		return bankIdAddress;
	}

	public void setBankIdAddress(String bankIdAddress) {
		this.bankIdAddress = bankIdAddress;
	}

	public long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Integer getNextCheckNumber() {
		return nextCheckNumber;
	}

	public void setNextCheckNumber(Integer nextCheckNumber) {
		this.nextCheckNumber = nextCheckNumber;
	}

	public Integer getNextDepositNumber() {
		return nextDepositNumber;
	}

	public void setNextDepositNumber(Integer nextDepositNumber) {
		this.nextDepositNumber = nextDepositNumber;
	}


	public List<DtoCheckbookMaintenance> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoCheckbookMaintenance> delete) {
		this.delete = delete;
	}


}
