package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PredefinecheckList;
import com.bti.hcm.util.UtilRandomKey;

public class DtoPredefinecheckList extends  DtoBase{
	private Integer id;
	private short checklistType;
	private String checklistDesc;
	private String checklistArbicDesc;
	private List<DtoPredefinecheckList> delete;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public short getChecklistType() {
		return checklistType;
	}

	public void setChecklistType(short checklistType) {
		this.checklistType = checklistType;
	}

	public String getChecklistDesc() {
		return checklistDesc;
	}

	public void setChecklistDesc(String checklistDesc) {
		this.checklistDesc = checklistDesc;
	}

	public String getChecklistArbicDesc() {
		return checklistArbicDesc;
	}

	public void setChecklistArbicDesc(String checklistArbicDesc) {
		this.checklistArbicDesc = checklistArbicDesc;
	}
	

	public List<DtoPredefinecheckList> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoPredefinecheckList> delete) {
		this.delete = delete;
	}
	public DtoPredefinecheckList(PredefinecheckList predefinecheckList) {
		this.id = predefinecheckList.getId();

		if (UtilRandomKey.isNotBlank(predefinecheckList.getChecklistDesc())) {
			this.checklistDesc = predefinecheckList.getChecklistDesc();
		} else {
			this.checklistDesc = "";
		}
		if (UtilRandomKey.isNotBlank(predefinecheckList.getChecklistArbicDesc())) {
			this.checklistArbicDesc = predefinecheckList.getChecklistArbicDesc();
		} else {
			this.checklistArbicDesc = "";
		}


	}


}
