package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSkillSteup;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceSkillSteps;

@RestController
@RequestMapping("/hrSkillSteup")
public class ControllerSkillSteup extends BaseController {

	private static final Logger LOGGER = Logger.getLogger(ControllerSkillSteup.class);

	@Autowired
	ServiceSkillSteps serviceSkillSteps;

	@Autowired
	ServiceResponse response;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@RequestMapping(value = "/createSkillSteup", method = RequestMethod.POST)
	public ResponseMessage createSkillSteup(HttpServletRequest request, @RequestBody DtoSkillSteup dtoSkillSteup)
			throws Exception {
		ResponseMessage responseMessage = null;
		LOGGER.info("Create Skill Steup Info");
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSkillSteup = serviceSkillSteps.saveOrUpdateSkillSteup(dtoSkillSteup);
			responseMessage = displayMessage(dtoSkillSteup, "SKILL_STEUP_CREATED", "SKILL_STEUP_NOT_CREATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}

	public ResponseMessage saveOrUpdateSkillSteup(DtoSkillSteup dtoSkillSteup, String sucessMsg, String failMsg)
			throws Exception {
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSkillSteup = serviceSkillSteps.saveOrUpdateSkillSteup(dtoSkillSteup);
			responseMessage = displayMessage(dtoSkillSteup, sucessMsg, failMsg, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllSkillSetupId", method = RequestMethod.GET)
	public ResponseMessage getAllSkillSteup(HttpServletRequest request) throws Exception {
		LOGGER.info("Get All Skill Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceSkillSteps.getAllSkillSteup();
			responseMessage = displayMessage(dtoSearch, "SKILL_STEUP_GET_ALL", "SKILL_STEUP_LIST_NOT_GETTING",
					response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Get All Skill Steup Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoSkillSteup dtoSkillSteup)
			throws Exception {
		LOGGER.info("Update Skill Steup Method");
		return saveOrUpdateSkillSteup(dtoSkillSteup, "SKILL_STEUP_UPDATED_SUCCESS", "SKILL_STEUP_NOT_UPDATED");
	}

	/*
	 * @RequestMapping(value = "/delete", method = RequestMethod.PUT) public
	 * ResponseMessage deleteSkillSteup(HttpServletRequest request, @RequestBody
	 * DtoSkillSteup dtoSkillSteup) throws Exception {
	 * LOGGER.info("Delete Skill Steup Method"); ResponseMessage responseMessage =
	 * null; boolean flag =serviceHcmHome.checkValidCompanyAccess(); if (flag) { if
	 * (dtoSkillSteup.getIds() != null && !dtoSkillSteup.getIds().isEmpty()) {
	 * DtoSkillSteup dtoSkillSteup2 =
	 * serviceSkillSteps.deleteSkillSteup(dtoSkillSteup.getIds()); responseMessage =
	 * displayMessage(dtoSkillSteup2,"SKILL_STEUP_DELETED",
	 * "SKILL_SETUP_UNABLE_DELETED_ID_MESSAGE",response);
	 * 
	 * } else { responseMessage = new
	 * ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
	 * response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false)); }
	 * 
	 * } else { responseMessage = unauthorizedMsg(response); }
	 * LOGGER.debug("Delete Skill Steup Method:"+responseMessage.getMessage());
	 * return responseMessage; }
	 */

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteDepartment(HttpServletRequest request, @RequestBody DtoSkillSteup dtoSkillSteup)
			throws Exception {
		LOGGER.info("Delete Skill Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSkillSteup.getIds() != null && !dtoSkillSteup.getIds().isEmpty()) {
				DtoSkillSteup dtoSkillSteup2 = serviceSkillSteps.deleteSkillSteup(dtoSkillSteup.getIds());
				if (dtoSkillSteup2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("SKILL_STEUP_DELETED", false), dtoSkillSteup2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("SKILL_SETUP_UNABLE_DELETED_ID_MESSAGE", false),
							dtoSkillSteup2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Skill Steup Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Skill Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSearch.getPageSize() != null && dtoSearch.getPageSize() > 0) {
				dtoSearch = this.serviceSkillSteps.searchSkillSteup(dtoSearch);
			} else {
				dtoSearch = this.serviceSkillSteps.searchSkillSetupId(dtoSearch);
			}
			responseMessage = displayMessage(dtoSearch, MessageConstant.SKILL_SET_STEUP_GET_ALL,
					MessageConstant.SKILL_SET_STEUP_LIST_NOT_GETTING, response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}

		if (dtoSearch != null) {
			LOGGER.debug("Search Skill Steup Methods:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	/*
	 * @RequestMapping(value = "/getSkillSteupById", method = RequestMethod.POST)
	 * public ResponseMessage getSkillSteuptById(HttpServletRequest
	 * request, @RequestBody DtoSkillSteup dtoSkillSteup) throws Exception {
	 * LOGGER.info("Get Skill Steup ById Method"); ResponseMessage responseMessage =
	 * null; boolean flag =serviceHcmHome.checkValidCompanyAccess(); if (flag) {
	 * DtoSkillSteup dtoSkillSteupObj =
	 * serviceSkillSteps.getBySkillSteupId(dtoSkillSteup.getId()); if
	 * (dtoSkillSteupObj != null) { if (dtoSkillSteupObj.getMessageType() == null) {
	 * responseMessage = new ResponseMessage(HttpStatus.CREATED.value(),
	 * HttpStatus.CREATED,
	 * response.getMessageByShortAndIsDeleted("SKILL_STEUP_GET_DETAIL", false),
	 * dtoSkillSteupObj); } else { responseMessage = new
	 * ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
	 * response.getMessageByShortAndIsDeleted(dtoSkillSteupObj.getMessageType(),
	 * false), dtoSkillSteupObj); }
	 * 
	 * } else { responseMessage = new
	 * ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
	 * response.getMessageByShortAndIsDeleted("SKILL_STEUP_LIST_NOT_GETTING",
	 * false), dtoSkillSteupObj); } } else { responseMessage =
	 * unauthorizedMsg(response); }
	 * LOGGER.debug("Get Skill Steup ById Method:"+dtoSkillSteup.getId()); return
	 * responseMessage; }
	 */

	@RequestMapping(value = "/getSkillSteupById", method = RequestMethod.POST)
	public ResponseMessage getDepartmentById(HttpServletRequest request, @RequestBody DtoSkillSteup dtoSkillSteup)
			throws Exception {
		LOGGER.info("Get Skill Steup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSkillSteup dtoSkillSteupObj = serviceSkillSteps.getBySkillSteupId(dtoSkillSteup.getId());
			responseMessage = displayMessage(dtoSkillSteupObj, "SKILL_STEUP_GET_DETAIL", "SKILL_STEUP_LIST_NOT_GETTING",
					response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get Skill Steup ById Method:" + dtoSkillSteup.getId());
		return responseMessage;
	}

	/**
	 * @param request       { "skillId":3 }
	 * @param response      { "code": 201, "status": "CREATED", "result": { "id":
	 *                      null, "skillId": "3", "skillDesc": "test3",
	 *                      "arabicDesc": "test4", "compensation": null,
	 *                      "frequency": null, "pageNumber": null, "pageSize": null,
	 *                      "ids": null, "isActive": null, "messageType": null,
	 *                      "message": null, "deleteMessage": null,
	 *                      "associateMessage": null, "deleteSkillSteup": null,
	 *                      "isRepeat": null }, "btiMessage": { "message": "Skill
	 *                      Steup details fetched successfully", "messageShort":
	 *                      "SKILL_STEUP_GET_DETAIL" } }
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/skillSteupIdcheck", method = RequestMethod.POST)
	public ResponseMessage skillSteupIdcheck(HttpServletRequest request, @RequestBody DtoSkillSteup dtoSkillSteup)
			throws Exception {
		LOGGER.info("skillSteupIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSkillSteup dtoSkillSteupObj = serviceSkillSteps.repeatBySkillSteupId(dtoSkillSteup.getSkillId());

			if (dtoSkillSteupObj != null && dtoSkillSteupObj.getIsRepeat()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						response.getMessageByShortAndIsDeleted("SKILL_STEUP_RESULT", false), dtoSkillSteupObj);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("SKILLS_SET_STEUP_LIST_NOT_GETTING", false),
						dtoSkillSteupObj);
			}
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/searchSkillSetupId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchSkillSetupId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("searchSkillSetupId Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSteps.searchSkillSetupId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "SKILL_SET_STEUP_GET_ALL",
					MessageConstant.SKILL_SET_STEUP_LIST_NOT_GETTING, response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			LOGGER.debug("Search Skills Steups Method:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	@RequestMapping(value = "/searchIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Skill Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSteps.searchIds(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "SKILL_SET_STEUP_GET_ALL", "SKILL_SET_STEUP_LIST_NOT_GETTING",
					response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			LOGGER.debug("Search Skill Steup Method:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	@RequestMapping(value = "/searchAllSkillId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAllSkillId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("searchAllSkillId Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSteps.searchAllSkillId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "SKILL_SET_STEUP_CODE_GET_ALL",
					"SKILL_SET_STEUP_LIST_NOT_GETTING", response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			LOGGER.debug("Search Skill Steup Method:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

}
