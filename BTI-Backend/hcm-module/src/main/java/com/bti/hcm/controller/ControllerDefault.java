package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoDefault;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceDefault;
import com.bti.hcm.service.ServiceEmployeeBenefitMaintenance;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/default")
public class ControllerDefault extends BaseController{
	

	private static final Logger LOGGER = Logger.getLogger(ControllerDefault.class);
	
	@Autowired
	ServiceEmployeeBenefitMaintenance serviceEmployeeBenefitMaintenance;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	@Autowired
	ServiceDefault serviceDefault;

	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createEmployeeDependent(HttpServletRequest request, @RequestBody DtoDefault dtoDefault) throws Exception {
		LOGGER.info("Create Default Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDefault  = serviceDefault.saveOrUpdate(dtoDefault);
			responseMessage=displayMessage(dtoDefault, "DEFAULT_CREATED", "DEFAULT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Default Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoDefault dtoDefault) throws Exception {
		LOGGER.info("Update Default Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDefault = serviceDefault.saveOrUpdate(dtoDefault);
			responseMessage=displayMessage(dtoDefault, "DEFAULT_UPDATED", "DEFAULT_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Default Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoDefault dtoDefault) throws Exception {
		LOGGER.info("Delete Default Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoDefault.getIds() != null && !dtoDefault.getIds().isEmpty()) {
				DtoDefault  dtoDefault2 = serviceDefault.delete(dtoDefault.getIds());
				if(dtoDefault2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_DELETED", false), dtoDefault2);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_NOT_DELETE_ID_MESSAGE", false), dtoDefault2);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Default Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoDefault dtoDefault) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoDefault dtoDefaultObj = serviceDefault.getById(dtoDefault.getId());
			responseMessage=displayMessage(dtoDefaultObj, "DEFAULT_LIST_GET_DETAIL", "DEFAULT_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoDefault.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeBenefitMaintenance(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Default Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDefault.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DEFAULT_GET_ALL, MessageConstant.DEFAULT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllForCalculateCheck", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllForCalculateCheck(@RequestBody DtoSearch dtoSearch,HttpServletRequest request) throws Exception {
		LOGGER.info("Search Default Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDefault.getAllForCalculateCheck(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DEFAULT_GET_ALL, MessageConstant.DEFAULT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchDefaultIdForCalculateChecks", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchDefaultIdForCalculateChecks(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("search DefaultId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDefault.searchDefaultIdForCalculateChecks(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "DEFAULT_GET_ALL", MessageConstant.DEFAULT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchDefaultId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchDefaultId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("search DefaultId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDefault.searchDefaultId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "DEFAULT_GET_ALL", "DEFAULT_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	
	@RequestMapping(value = "/getAllDefaultDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllDefaultDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoDefault> dtoDefaultList = serviceDefault.getAllDefaultDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_LIST_NOT_GETTING", false), dtoDefaultList);
			}else {
				responseMessage =unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/defaultIdcheck", method = RequestMethod.POST)
	public ResponseMessage dfaultIdcheck(HttpServletRequest request, @RequestBody DtoDefault dtoDefault) throws Exception {
		LOGGER.info("dfaultIdcheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoDefault dtoDefaultObj = serviceDefault.repeatByDefaultId(dtoDefault.getDefaultID());
			 if (dtoDefaultObj !=null && dtoDefaultObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                         serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_RESULT", false), dtoDefaultObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                         serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_NOT_FOUND", false),
                         dtoDefaultObj);
             }
			
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
}
