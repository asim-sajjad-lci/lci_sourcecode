
package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.Department;
import com.bti.hcm.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * Description: DTO Department class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoDepartment extends DtoBase{
	
	
	private Integer id;
	private String departmentId;
	private String departmentName;
	private String departmentDescription;
	private List<DtoDepartment> deleteDepartment;
	private String arabicDepartmentDescription;
	private String gregorianDate;
	private String hijriDate;
	private String moduleName;
	private String companyName;
	private String screenName;
	private Integer userId;
	private String userName;
	private int numberOfEmployee;
	private String projectId;
	private String projectName;
	private String projectArabicName;
	private int miscellaneousId;
	private String miscellaneousCode;
	private String miscellaneousArabicCode;
	
	
	private List<DtoEmployeeCalculateChecckReport> employeeList;
	
	private BigDecimal totalBasic;
	private BigDecimal totalHousing;
	private BigDecimal totalTransportation;
	private BigDecimal totalFixed;
	private BigDecimal totalOtherPaycode;
	
	private BigDecimal totalBouns;
	private BigDecimal totalOvertime;
	private BigDecimal totalOtherBenefit;
	
	
	private BigDecimal totalIncomes;
	
	private BigDecimal totalGOSI;
	private BigDecimal totalLoan;
	private BigDecimal totalAbsent;
	private BigDecimal totalOtherDeduction;
	private BigDecimal totalDeductions;
	
	private BigDecimal totalSummry;
	
	private int valueId;
	private String valueDescription;

	private int valId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getDepartmentDescription() {
		return departmentDescription;
	}

	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}


	
	public List<DtoDepartment> getDeleteDepartment() {
		return deleteDepartment;
	}

	public void setDeleteDepartment(List<DtoDepartment> deleteDepartment) {
		this.deleteDepartment = deleteDepartment;
	}

	

	public String getArabicDepartmentDescription() {
		return arabicDepartmentDescription;
	}

	public void setArabicDepartmentDescription(String arabicDepartmentDescription) {
		this.arabicDepartmentDescription = arabicDepartmentDescription;
	}
	
	


	public DtoDepartment() {
		
		totalBasic = BigDecimal.ZERO;
		totalHousing = BigDecimal.ZERO;
		totalTransportation= BigDecimal.ZERO;
		totalFixed= BigDecimal.ZERO;
		totalOtherPaycode= BigDecimal.ZERO;
		
		totalBouns= BigDecimal.ZERO;
		totalOvertime= BigDecimal.ZERO;
		totalOtherBenefit= BigDecimal.ZERO;
		
		
		totalIncomes= BigDecimal.ZERO;
		
		totalGOSI= BigDecimal.ZERO;
		totalLoan= BigDecimal.ZERO;
		totalAbsent= BigDecimal.ZERO;
		totalOtherDeduction= BigDecimal.ZERO;
		
		totalSummry= BigDecimal.ZERO;
		totalDeductions = BigDecimal.ZERO;
		
	}
	
	public DtoDepartment(Department department) {
		this.departmentId = department.getDepartmentId();


		if (UtilRandomKey.isNotBlank(department.getDepartmentDescription())) {
			this.departmentDescription = department.getDepartmentDescription();
		} else {
			this.departmentDescription = "";
		}

	
	}

	

	public String getGregorianDate() {
		return gregorianDate;
	}

	public void setGregorianDate(String gregorianDate) {
		this.gregorianDate = gregorianDate;
	}

	public String getHijriDate() {
		return hijriDate;
	}

	public void setHijriDate(String hijriDate) {
		this.hijriDate = hijriDate;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<DtoEmployeeCalculateChecckReport> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<DtoEmployeeCalculateChecckReport> employeeList) {
		this.employeeList = employeeList;
	}

	public int getNumberOfEmployee() {
		return numberOfEmployee;
	}

	public void setNumberOfEmployee(int numberOfEmployee) {
		this.numberOfEmployee = numberOfEmployee;
	}

	public BigDecimal getTotalBasic() {
		return totalBasic;
	}

	public void setTotalBasic(BigDecimal totalBasic) {
		this.totalBasic = totalBasic;
	}

	public BigDecimal getTotalHousing() {
		return totalHousing;
	}

	public void setTotalHousing(BigDecimal totalHousing) {
		this.totalHousing = totalHousing;
	}

	public BigDecimal getTotalTransportation() {
		return totalTransportation;
	}

	public void setTotalTransportation(BigDecimal totalTransportation) {
		this.totalTransportation = totalTransportation;
	}

	public BigDecimal getTotalFixed() {
		return totalFixed;
	}

	public void setTotalFixed(BigDecimal totalFixed) {
		this.totalFixed = totalFixed;
	}

	public BigDecimal getTotalOtherPaycode() {
		return totalOtherPaycode;
	}

	public void setTotalOtherPaycode(BigDecimal totalOtherPaycode) {
		this.totalOtherPaycode = totalOtherPaycode;
	}

	public BigDecimal getTotalBouns() {
		return totalBouns;
	}

	public void setTotalBouns(BigDecimal totalBouns) {
		this.totalBouns = totalBouns;
	}

	public BigDecimal getTotalOvertime() {
		return totalOvertime;
	}

	public void setTotalOvertime(BigDecimal totalOvertime) {
		this.totalOvertime = totalOvertime;
	}

	public BigDecimal getTotalOtherBenefit() {
		return totalOtherBenefit;
	}

	public void setTotalOtherBenefit(BigDecimal totalOtherBenefit) {
		this.totalOtherBenefit = totalOtherBenefit;
	}

	public BigDecimal getTotalIncomes() {
		return totalIncomes;
	}

	public void setTotalIncomes(BigDecimal totalIncomes) {
		this.totalIncomes = totalIncomes;
	}

	public BigDecimal getTotalGOSI() {
		return totalGOSI;
	}

	public void setTotalGOSI(BigDecimal totalGOSI) {
		this.totalGOSI = totalGOSI;
	}

	public BigDecimal getTotalLoan() {
		return totalLoan;
	}

	public void setTotalLoan(BigDecimal totalLoan) {
		this.totalLoan = totalLoan;
	}

	public BigDecimal getTotalAbsent() {
		return totalAbsent;
	}

	public void setTotalAbsent(BigDecimal totalAbsent) {
		this.totalAbsent = totalAbsent;
	}

	public BigDecimal getTotalOtherDeduction() {
		return totalOtherDeduction;
	}

	public void setTotalOtherDeduction(BigDecimal totalOtherDeduction) {
		this.totalOtherDeduction = totalOtherDeduction;
	}

	public BigDecimal getTotalSummry() {
		return totalSummry;
	}

	public void setTotalSummry(BigDecimal totalSummry) {
		this.totalSummry = totalSummry;
	}

	public BigDecimal getTotalDeductions() {
		return totalDeductions;
	}

	public void setTotalDeductions(BigDecimal totalDeductions) {
		this.totalDeductions = totalDeductions;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectArabicName() {
		return projectArabicName;
	}

	public void setProjectArabicName(String projectArabicName) {
		this.projectArabicName = projectArabicName;
	}

	public int getMiscellaneousId() {
		return miscellaneousId;
	}

	public void setMiscellaneousId(int miscellaneousId) {
		this.miscellaneousId = miscellaneousId;
	}

	public String getMiscellaneousCode() {
		return miscellaneousCode;
	}

	public void setMiscellaneousCode(String miscellaneousCode) {
		this.miscellaneousCode = miscellaneousCode;
	}

	public String getMiscellaneousArabicCode() {
		return miscellaneousArabicCode;
	}

	public void setMiscellaneousArabicCode(String miscellaneousArabicCode) {
		this.miscellaneousArabicCode = miscellaneousArabicCode;
	}

	public int getValueId() {
		return valueId;
	}

	public void setValueId(int valueId) {
		this.valueId = valueId;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	public int getValId() {
		return valId;
	}

	public void setValId(int valId) {
		this.valId = valId;
	}
	

}
