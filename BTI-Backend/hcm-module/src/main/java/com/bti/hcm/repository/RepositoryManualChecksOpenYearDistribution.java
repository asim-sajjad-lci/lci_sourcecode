package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecksOpenYearDistribution;

@Repository("repositoryManualChecksOpenYearDistribution")
public interface RepositoryManualChecksOpenYearDistribution
		extends JpaRepository<ManualChecksOpenYearDistribution, Integer> {

	@Query("SELECT pto FROM ManualChecksOpenYearDistribution pto WHERE YEAR(pto.createdDate) =:year and pto.isDeleted = false")
	List<ManualChecksOpenYearDistribution> findByYear(@Param("year") Integer year);

}
