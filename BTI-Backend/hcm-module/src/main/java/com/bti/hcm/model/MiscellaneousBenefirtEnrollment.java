package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00913",indexes = {
        @Index(columnList = "MISCBENINDX")
})
@NamedQuery(name = "MiscellaneousBenefirtEnrollment.findAll", query = "SELECT d FROM MiscellaneousBenefirtEnrollment d")
public class MiscellaneousBenefirtEnrollment extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MISCBENINDX")
	private Integer id;

	@Column(name = "ACCSTATS")
	private Short status;
	
	

	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;
	
	@ManyToOne
	@JoinColumn(name = "MISCINDX")
	private MiscellaneousBenefits miscellaneousBenefits;
	

	@Column(name = "MISCFREQ")
	private Integer frequency;

	@Column(name = "MISCSTRDT")
	private Date startDate;

	@Column(name = "MISCENDDT")
	private Date endDate;

	@Column(name = "MISCEMPL")
	private Boolean inActive;

	@Column(name = "MISCMETOD")
	private Short method;

	@Column(name = "MISCAMNT", precision = 13, scale = 3)
	private BigDecimal dudctionAmount;

	@Column(name = "MISCPRNT", precision = 13, scale = 5)
	private BigDecimal dudctionPercent;

	@Column(name = "MISCMAXAMTP", precision = 13, scale = 3)
	private BigDecimal monthlyAmount;

	@Column(name = "MISCMAXAMTC", precision = 15, scale = 5)
	private BigDecimal yearlyAmount;

	@Column(name = "MISCMAXAMTL", precision = 15, scale = 5)
	private BigDecimal lifetimeAmount;

	@Column(name = "MISCEMPLOR")
	private Boolean empluyeeerinactive;

	@Column(name = "MISCMETODR")
	private Short empluyeeermethod;

	@Column(name = "MISCAMNTR", precision = 13, scale = 3)
	private BigDecimal benefitAmount;

	@Column(name = "MISCPRNTR", precision = 15, scale = 5)
	private BigDecimal benefitPercent;

	@Column(name = "MISCMAXAMTRP", precision = 13, scale = 3)
	private BigDecimal employermonthlyAmount;

	@Column(name = "MISCMAXAMTRC", precision = 13, scale = 3)
	private BigDecimal employeryearlyAmount;

	@Column(name = "MISCMAXAMTRL", precision = 13, scale = 3)
	private BigDecimal employerlifetimeAmount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getInActive() {
		return inActive;
	}

	public void setInActive(Boolean inActive) {
		this.inActive = inActive;
	}

	public Short getMethod() {
		return method;
	}

	public void setMethod(Short method) {
		this.method = method;
	}

	public BigDecimal getDudctionAmount() {
		return dudctionAmount;
	}

	public void setDudctionAmount(BigDecimal dudctionAmount) {
		this.dudctionAmount = dudctionAmount;
	}

	public BigDecimal getDudctionPercent() {
		return dudctionPercent;
	}

	public void setDudctionPercent(BigDecimal dudctionPercent) {
		this.dudctionPercent = dudctionPercent;
	}

	public BigDecimal getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(BigDecimal monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public BigDecimal getYearlyAmount() {
		return yearlyAmount;
	}

	public void setYearlyAmount(BigDecimal yearlyAmount) {
		this.yearlyAmount = yearlyAmount;
	}

	public BigDecimal getLifetimeAmount() {
		return lifetimeAmount;
	}

	public void setLifetimeAmount(BigDecimal lifetimeAmount) {
		this.lifetimeAmount = lifetimeAmount;
	}

	public Boolean getEmpluyeeerinactive() {
		return empluyeeerinactive;
	}

	public void setEmpluyeeerinactive(Boolean empluyeeerinactive) {
		this.empluyeeerinactive = empluyeeerinactive;
	}

	public Short getEmpluyeeermethod() {
		return empluyeeermethod;
	}

	public void setEmpluyeeermethod(Short empluyeeermethod) {
		this.empluyeeermethod = empluyeeermethod;
	}

	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public BigDecimal getBenefitPercent() {
		return benefitPercent;
	}

	public void setBenefitPercent(BigDecimal benefitPercent) {
		this.benefitPercent = benefitPercent;
	}

	public BigDecimal getEmployermonthlyAmount() {
		return employermonthlyAmount;
	}

	public void setEmployermonthlyAmount(BigDecimal employermonthlyAmount) {
		this.employermonthlyAmount = employermonthlyAmount;
	}

	public BigDecimal getEmployeryearlyAmount() {
		return employeryearlyAmount;
	}

	public void setEmployeryearlyAmount(BigDecimal employeryearlyAmount) {
		this.employeryearlyAmount = employeryearlyAmount;
	}

	public BigDecimal getEmployerlifetimeAmount() {
		return employerlifetimeAmount;
	}

	public void setEmployerlifetimeAmount(BigDecimal employerlifetimeAmount) {
		this.employerlifetimeAmount = employerlifetimeAmount;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public MiscellaneousBenefits getMiscellaneousBenefits() {
		return miscellaneousBenefits;
	}

	public void setMiscellaneousBenefits(MiscellaneousBenefits miscellaneousBenefits) {
		this.miscellaneousBenefits = miscellaneousBenefits;
	}

}
