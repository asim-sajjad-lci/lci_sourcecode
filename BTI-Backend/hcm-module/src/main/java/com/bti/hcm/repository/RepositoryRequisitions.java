package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Requisitions;

@Repository("repositoryhcmrequisitions")
public interface RepositoryRequisitions extends JpaRepository<Requisitions, Integer> {
	
	/**
	 * 
	 * @param id
	 * @param b
	 * @return
	 */
	public Requisitions findByIdAndIsDeleted(Integer id,boolean b);
	
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<Requisitions> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Requisitions d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteRequisition(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);

	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Requisitions d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleRequisition(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from Requisitions d where d.isDeleted=false")
	public Integer getCountOfTotalRequisitions();
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<Requisitions> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<Requisitions> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select r from Requisitions r where (r.recruiterName like :searchKeyWord  or r.managerName like :searchKeyWord or r.advertisingField1 like :searchKeyWord or r.advertisingField2 like :searchKeyWord or r.advertisingField3 like :searchKeyWord or r.advertisingField4 like :searchKeyWord) and r.isDeleted=false")
	public List<Requisitions> predictiveRequisitionsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from Requisitions r where (r.recruiterName like :searchKeyWord  or r.managerName like :searchKeyWord or r.advertisingField1 like :searchKeyWord or r.advertisingField2 like :searchKeyWord or r.advertisingField3 like :searchKeyWord or r.advertisingField4 like :searchKeyWord) and r.isDeleted=false")
	public Integer predictiveRequisitionsSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select r from Requisitions r where (r.recruiterName like :searchKeyWord  or r.managerName like :searchKeyWord or r.advertisingField1 like :searchKeyWord or r.advertisingField2 like :searchKeyWord or r.advertisingField3 like :searchKeyWord or r.advertisingField4 like :searchKeyWord) and r.isDeleted=false")
	public List<Requisitions> predictiveRequisitionsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from Requisitions d where (d.id=:searchKeyWord) and d.isDeleted=false")
	public List<Requisitions> findById(@Param("searchKeyWord") Integer id);


	@Query("select count(*) from Requisitions r ")
	public Integer getCountOfTotaRequisitions();

}


