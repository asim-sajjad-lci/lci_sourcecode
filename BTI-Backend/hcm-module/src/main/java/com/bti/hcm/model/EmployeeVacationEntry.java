package com.bti.hcm.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "hr10710")
public class EmployeeVacationEntry extends HcmBaseEntity2 {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDX")
	private Integer idx;

	@Column(name = "wrkflwreqid")
	private Integer workflowRequestId;

	@Column(name = "apl_fr")
	private String applyingfor; 
	
	@Column(name = "res_fr_emp")
	private Boolean reserveForEmployee; 
	
//	selectedDependents long 

	@Column(name = "finl_dest")
	String finalDestination; 
	
	@Column(name = "wth_tckts")
	private Boolean withTickets; 
//	tickets long 
	
	@Column(name = "typ_of_vctn")
	private String typeOfVacation; 
	
	@Column(name = "vctn_otsd_cntry")
	private Boolean vacationOutsideCountry; 

	@Column(name = "frm_date")
	private Date fromDate; 
	
	@Column(name = "to_date")
	private Date toDate; 
	
//	@Column(name = "frm_date")
//	private Integer vacationDays; 

	@Column(name = "rplcmnt_emp")
	private String replacementEmployeeName; 

	@Column(name = "nd_vsa")
	private Boolean needVisas; 
	
	@Column(name = "vsa_typ")
	private String visaType; 
	
	@Column(name = "nm_of_dys")
	private Double numberOfDays;
	
//	advanceRequestedFileID long 
//	exitReEntryVisaFileID long 
//	reservationFileID long
	
	@ManyToOne
	@JoinColumn(name = "empid")
	private EmployeeMaster  employeeMaster;
	
	
	@OneToMany
	@JoinColumn(name = "")
	private List <EmployeeVacationDependant> employeeVacationDependantsList;


	@OneToOne
	@JoinColumn(name = "tkt_id")
	private EmployeeVactionTicket employeeVactionTicket;


	public Integer getIdx() {
		return idx;
	}


	public void setIdx(Integer idx) {
		this.idx = idx;
	}


	public Integer getWorkflowRequestId() {
		return workflowRequestId;
	}


	public void setWorkflowRequestId(Integer workflowRequestId) {
		this.workflowRequestId = workflowRequestId;
	}


	public String getApplyingfor() {
		return applyingfor;
	}


	public void setApplyingfor(String applyingfor) {
		this.applyingfor = applyingfor;
	}


	public Boolean getReserveForEmployee() {
		return reserveForEmployee;
	}


	public void setReserveForEmployee(Boolean reserveForEmployee) {
		this.reserveForEmployee = reserveForEmployee;
	}


	public String getFinalDestination() {
		return finalDestination;
	}


	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}


	public Boolean getWithTickets() {
		return withTickets;
	}


	public void setWithTickets(Boolean withTickets) {
		this.withTickets = withTickets;
	}


	public String getTypeOfVacation() {
		return typeOfVacation;
	}


	public void setTypeOfVacation(String typeOfVacation) {
		this.typeOfVacation = typeOfVacation;
	}


	public Boolean getVacationOutsideCountry() {
		return vacationOutsideCountry;
	}


	public void setVacationOutsideCountry(Boolean vacationOutsideCountry) {
		this.vacationOutsideCountry = vacationOutsideCountry;
	}


	public Date getFromDate() {
		return fromDate;
	}


	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public Date getToDate() {
		return toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	public String getReplacementEmployeeName() {
		return replacementEmployeeName;
	}


	public void setReplacementEmployeeName(String replacementEmployeeName) {
		this.replacementEmployeeName = replacementEmployeeName;
	}


	public Boolean getNeedVisas() {
		return needVisas;
	}


	public void setNeedVisas(Boolean needVisas) {
		this.needVisas = needVisas;
	}


	public String getVisaType() {
		return visaType;
	}


	public void setVisaType(String visaType) {
		this.visaType = visaType;
	}


	public Double getNumberOfDays() {
		return numberOfDays;
	}


	public void setNumberOfDays(Double numberOfDays) {
		this.numberOfDays = numberOfDays;
	}


	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}


	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}


	public List<EmployeeVacationDependant> getEmployeeVacationDependantsList() {
		return employeeVacationDependantsList;
	}


	public void setEmployeeVacationDependantsList(List<EmployeeVacationDependant> employeeVacationDependantsList) {
		this.employeeVacationDependantsList = employeeVacationDependantsList;
	}


	public EmployeeVactionTicket getEmployeeVactionTicket() {
		return employeeVactionTicket;
	}


	public void setEmployeeVactionTicket(EmployeeVactionTicket employeeVactionTicket) {
		this.employeeVactionTicket = employeeVactionTicket;
	}
	
	
	
	
}
