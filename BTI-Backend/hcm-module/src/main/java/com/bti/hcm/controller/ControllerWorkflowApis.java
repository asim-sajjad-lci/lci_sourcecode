package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoEmployeeLoanEntry;
import com.bti.hcm.model.dto.DtoEmployeeLoanInfo;
import com.bti.hcm.model.dto.DtoEmployeeVacation;
import com.bti.hcm.model.dto.DtoEmployeeVacationEntry;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceWorkflowApis;
import com.bti.hcm.service.ServiceWorkflowVacationApis;
import com.bti.hcm.util.Constant;

@RestController
@RequestMapping("/workflowapis")
public class ControllerWorkflowApis extends BaseController {

	
	@Autowired
	ServiceHcmHome serviceHome;
	
	@Autowired
	ServiceWorkflowApis serviceWorkflowApis;
	
	@Autowired
	ServiceWorkflowVacationApis serviceWorkflowVacationApis;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	
	private static final Logger LOGGER = Logger.getLogger(ControllerWorkflowApis.class);
	
	@RequestMapping(value = "/getEmployeeLoanDetails", method = RequestMethod.GET)
	public ResponseMessage getEmployeeLoanDetails(HttpServletRequest request) throws Exception {
		ResponseMessage responseMessage = null;
		
		String userName = request.getParameter(Constant.WORKFLOW_USERNAME);
		
		if (serviceHome.validateClient()) {
			
		
			DtoEmployeeLoanInfo dtoEmployeeLoanInfo= serviceWorkflowApis.getEmployeeLoanInfo(userName);
			
			if (dtoEmployeeLoanInfo != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.GET_EMPLOYEE_LOAN_INFO_SUCCESS, false), dtoEmployeeLoanInfo);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.DATA_NOT_FOUND, false), dtoEmployeeLoanInfo);
			}
		} else {
			
			responseMessage = new ResponseMessage(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.UNAUTHORIZED_SUCCESS, false), false);
		}
		
		
		return responseMessage;
	}

	@RequestMapping(value = "/postEmployeeLoanEntry", method=RequestMethod.POST)
	public ResponseMessage postEmployeeLoanEntry(@RequestBody DtoEmployeeLoanEntry dtoEmployeeLoanEntry) throws Exception{
		ResponseMessage responseMessage = null;
		String serviceResult = "";
		if (serviceHome.validateClient()) {
			serviceResult = serviceWorkflowApis.postEmployeeLoanEntry(dtoEmployeeLoanEntry);
		
			int code = -1; 
			HttpStatus status = null; 
			DtoBtiMessageHcm message = null;
			Boolean result = false;
			
			switch (serviceResult) {
				case MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_SUCCESS:
					code = HttpStatus.OK.value();
					status = HttpStatus.OK;
					result = true;
					break;
				
				case MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_DUPLICATE_REQUEST:
				case MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_EMPLOYEE_NOT_FOUND:
				case MessageConstant.POST_EMPLOYEE_LOAN_ENTRY_FAILURE:	
					
					code = HttpStatus.BAD_REQUEST.value();
					status = HttpStatus.BAD_REQUEST;
					result = false;
					break;
					
				default:
					code = HttpStatus.BAD_REQUEST.value();
					status = HttpStatus.BAD_REQUEST;
					result = false;
					break;
			}

			message = serviceResponse.getMessageByShortAndIsDeleted(serviceResult, false);
			responseMessage = new ResponseMessage(code, status, message, result);
				
			
		} else {
			
			responseMessage = new ResponseMessage(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.UNAUTHORIZED_SUCCESS, false), false);
		}		
		
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/postEmployeeVacationEntry", method=RequestMethod.POST)
	public ResponseMessage postEmployeeVacationEntry(@RequestBody DtoEmployeeVacationEntry dtoEmployeeVacationEntry) throws Exception{
		ResponseMessage responseMessage = null;
		String serviceResult = "";
		if (serviceHome.validateClient()) {
			serviceResult = serviceWorkflowVacationApis.postEmployeeVacationEntry(dtoEmployeeVacationEntry);
		
			int code = -1; 
			HttpStatus status = null; 
			DtoBtiMessageHcm message = null;
			Boolean result = false;
			
			switch (serviceResult) {
				case MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_SUCCESS:
					code = HttpStatus.OK.value();
					status = HttpStatus.OK;
					result = true;
					break;
				
				case MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_DUPLICATE_REQUEST:
				case MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_EMPLOYEE_NOT_FOUND:
				case MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_FAILURE:	
					
					code = HttpStatus.BAD_REQUEST.value();
					status = HttpStatus.BAD_REQUEST;
					result = false;
					break;
					
				default:
					code = HttpStatus.BAD_REQUEST.value();
					status = HttpStatus.BAD_REQUEST;
					result = false;
					break;
			}

			message = serviceResponse.getMessageByShortAndIsDeleted(serviceResult, false);
			responseMessage = new ResponseMessage(code, status, message, result);
				
			
		} else {
			
			responseMessage = new ResponseMessage(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.UNAUTHORIZED_SUCCESS, false), false);
		}		
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getEmployeeVacationDetails", method = RequestMethod.GET)
	public ResponseMessage getEmployeeVacationDetails(HttpServletRequest request) throws Exception {
		ResponseMessage responseMessage = null;
		
		String userName = request.getParameter(Constant.WORKFLOW_USERNAME);
		
		if (serviceHome.validateClient()) {
			
		
			DtoEmployeeVacation dtoEmployeeVacation= serviceWorkflowVacationApis.getEmployeeVacationDetails(userName);
			
			if (dtoEmployeeVacation != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.GET_EMPLOYEE_LOAN_INFO_SUCCESS, false), dtoEmployeeVacation);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.DATA_NOT_FOUND, false), dtoEmployeeVacation);
			}
		} else {
			
			responseMessage = new ResponseMessage(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.UNAUTHORIZED_SUCCESS, false), false);
		}
		
		
		return responseMessage;
	}
	
}
