package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.Loan;
import com.bti.hcm.model.LoanDetails;
import com.bti.hcm.model.Location;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.TotalPackageESB;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeMasterForLoan;
import com.bti.hcm.model.dto.DtoLoan;
import com.bti.hcm.model.dto.DtoLoanDetails;
import com.bti.hcm.model.dto.DtoLoansPopup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryLoan;
import com.bti.hcm.repository.RepositoryLoanDetails;
import com.bti.hcm.repository.RepositoryLocation;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositoryTotalPackageESB;

@Service("serviceLoanPayment")
public class ServiceLoanPayment {

	static Logger log = Logger.getLogger(ServiceLoanPayment.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse response;

	@Autowired
	private RepositoryLoan repositoryLoan;

	@Autowired
	private RepositoryLoanDetails repositoryLoanDetails;

	@Autowired
	private RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	private RepositoryLocation repositoryLocation;

	@Autowired
	private RepositoryPosition repositoryPosition;

	@Autowired
	private RepositoryPayCode repositoryPayCode;

	@Autowired
	private RepositoryBenefitCode repositoryBenefitCode;

	@Autowired
	private RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	@Autowired
	private RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;

	@Autowired
	private RepositoryTotalPackageESB repositoryTotalPackageESB;

	// @Autowired
	// private ModelMapper modelMapper;

	public List<DtoEmployeeMaster> getAllEmployeeList() {
		log.info("getAllEmployeeList  Method");
		List<DtoEmployeeMaster> dtoEmployeeMasterList = new ArrayList<>();
		List<EmployeeMaster> list = repositoryEmployeeMaster.findByIsDeleted(false);

		if (list != null && !list.isEmpty()) {
			for (EmployeeMaster employeeMaster : list) {
				DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();

				dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
				dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
				// dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());

				log.info("hf007  : " + employeeMaster.getEmployeeId() + employeeMaster.getEmployeeFirstName());
				dtoEmployeeMasterList.add(dtoEmployeeMaster);
			}
		}
		log.debug("Size of Employee is:" + dtoEmployeeMasterList.size());
		return dtoEmployeeMasterList;
	}

	public DtoEmployeeMasterForLoan getEmployeeDetailsById(Integer id) {
		log.info("getEmployeeDetails  Method");
		DtoEmployeeMasterForLoan dtoEmpM = new DtoEmployeeMasterForLoan();
		if (id == null || id == 0)
			return null;
		EmployeeMaster employeeMaster = repositoryEmployeeMaster.findByEmployeeIndexIdAndIsDeleted(id, false); // findByEmployeeId(id);
		if (employeeMaster == null || employeeMaster.equals(null))
			return null;

		log.info("listEmp item: " + employeeMaster);
		dtoEmpM.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
		dtoEmpM.setEmployeeId(employeeMaster.getEmployeeId());

		String EmpName = employeeMaster.getEmployeeFirstName() + employeeMaster.getEmployeeLastName();
		dtoEmpM.setEmpFullName(EmpName);

		dtoEmpM.setEmployeeHireDate(employeeMaster.getEmployeeHireDate());

		Integer posId = repositoryEmployeeMaster.getPositionByEmpId(employeeMaster.getEmployeeIndexId());
		Position position = repositoryPosition.findByIdAndIsDeleted(posId, false);
		dtoEmpM.setPositionName(position.getDescription());

		Integer locId = repositoryEmployeeMaster.getLocationByEmpId(employeeMaster.getEmployeeIndexId()); // Site
		Location location = repositoryLocation.findByIdAndIsDeleted(locId, false);
		dtoEmpM.setLocationName(location.getDescription());

		// dtoEmpM.setTotalPackage(0); // for Now as per Req.
		Double totalPackage = 0.0;
		try {
			// getting TypeIndices from hr40960
			List<TotalPackageESB> PkgList = repositoryTotalPackageESB.findByTypeIdAndIsDeleted(1); // 1=Pkg,3=ESB.

			// getting PayCodes for TypeIndices
			List<Integer> PkgPayCodeIds = new ArrayList<Integer>();
			for (TotalPackageESB pkg : PkgList) {
				PkgPayCodeIds.add(pkg.getTypeFieldForCodes().getIndxid());
			}

			/*
			 * List<PayCode> payCodes =
			 * repositoryPayCode.findByTypeFieldIdsAndIsDeleted(PkgPayCodeIds);
			 * 
			 * List<Integer> payCodeIds = new ArrayList<Integer>();
			 * //repositoryPayCode.findByIdsTypeFieldIdsAndIsDeleted(PkgPayCodeIds); for
			 * (PayCode payCode : payCodes) { payCodeIds.add(payCode.getId()); }
			 */
			List<Integer> payCodeIds = repositoryPayCode.findIdsByTypeFieldIdsAndIsDeleted(PkgPayCodeIds);

			// Getting Recs from EmpPayCodeMntnc
			List<EmployeePayCodeMaintenance> empMntncList = repositoryEmployeePayCodeMaintenance
					.findByEmpIdAndIsDeletedAndIsInactiveAndPayCodeIds(employeeMaster.getEmployeeIndexId(), payCodeIds);

			Double sumTotalPayRates = 0.0;
			for (EmployeePayCodeMaintenance m : empMntncList) {
				log.info("this.payRate:" + m.getPayRate().doubleValue());
				sumTotalPayRates = sumTotalPayRates + m.getPayRate().doubleValue();
			}
			totalPackage =  sumTotalPayRates;	// intValue();

		} catch (Exception e) {
			log.debug("Some Error during TotalPackage Calc for This Empl:" + employeeMaster.getEmployeeId());
			totalPackage = 0.0;
		}

		dtoEmpM.setTotalPackage(totalPackage);
		log.info("emp.id:" + employeeMaster.getEmployeeIndexId());
		//dtoEmpM.setCurrentESB(0); // for Now as per Req.
		Double totalESB = 0.0;
		try {
			// getting TypeIndices from hr40960
			List<TotalPackageESB> ESBList = repositoryTotalPackageESB.findByTypeIdAndIsDeleted(3); // 1=Pkg,3=ESB.

			// getting BenefitCodes for TypeIndices
			List<Integer> ESBBenefitCodeIds = new ArrayList<Integer>();
			for (TotalPackageESB ESB : ESBList) {
				ESBBenefitCodeIds.add(ESB.getTypeFieldForCodes().getIndxid());
			}

			List<Integer> benefitIds = repositoryBenefitCode.findIdsByTypeFieldIdsAndIsDeleted(ESBBenefitCodeIds);

			// Getting Recs from EmpBenefitCodeMntnc
			List<EmployeeBenefitMaintenance> empBenMntncList = repositoryEmployeeBenefitMaintenance
					.findByEmpIdAndIsDeletedAndIsInactiveAndBenefitCodeIdsAndBetweenDates(employeeMaster.getEmployeeIndexId(), benefitIds, new Date());

			Double sumESBs = 0.0;
			for (EmployeeBenefitMaintenance bm : empBenMntncList) {
				log.info("this.payRate:" + bm.getBenefitAmount().doubleValue());
				sumESBs = sumESBs + bm.getBenefitAmount().doubleValue();
			}
			totalESB = sumESBs;			//.intValue();

		} catch (Exception e) {
			log.debug("There was some error in getting Full ESB's for This Employee:" + employeeMaster.getEmployeeId());
			totalESB = 0.0;
		}

		dtoEmpM.setCurrentESB(totalESB);

		// simple query method used
		/*
		 * List<Integer> previousLoanAmts = repositoryLoan
		 * .findAllLoansForThisEmployee(employeeMaster.getEmployeeIndexId()); Integer
		 * previousLoanAmtTotal = 0; for (Integer integerAmt : previousLoanAmts) {
		 * previousLoanAmtTotal = previousLoanAmtTotal + integerAmt; }
		 */

		Double previousLoanAmtTotal = repositoryLoan.sumAllLoansAmtForThisEmployee(employeeMaster.getEmployeeIndexId());
		dtoEmpM.setPreviousLoan(previousLoanAmtTotal);

		// is evaluated from all loan.detailsRecs
		List<Double> deductionsPerLoan = repositoryLoanDetails
				.sumOfAllDeductionsForThisEmployee(employeeMaster.getEmployeeIndexId());
		Double totalDeductions = (double) 0;
		for (Double deductionsPerLoanObj : deductionsPerLoan) {
			if (deductionsPerLoanObj != null && !deductionsPerLoanObj.equals(null) && deductionsPerLoanObj > 0)
				totalDeductions = totalDeductions + deductionsPerLoanObj;
		}
		dtoEmpM.setTotalDeduction(totalDeductions);

		// is evaluated from all loan.detailsRecs
		List<Double> remainingsPerLoan = repositoryLoanDetails
				.sumOfAllRemainingsForThisEmployee(employeeMaster.getEmployeeIndexId());
		Double totalRemainings = (double) 0;
		for (Double remainingsPerLoanObj : remainingsPerLoan) {
			if (remainingsPerLoanObj != null && !remainingsPerLoanObj.equals(null) && remainingsPerLoanObj > 0)
				totalRemainings = totalRemainings + remainingsPerLoanObj;
		}
		dtoEmpM.setTotalRemaining(totalRemainings);

		/*
		 * ******************** Detail;s Button : A Separate API ()
		 * **********************
		 */
		// dtoEmpM.setPreviousLoan(previousLoan);

		dtoEmpM.setCityName(employeeMaster.getDepartment().getDepartmentDescription());
		dtoEmpM.setCountryName(employeeMaster.getLocation().getDescription());

		return dtoEmpM;
	}

	public Loan saveUpdateLoan(DtoLoan dtoLoan, Integer loggedInUserId) {
		log.info("ServiceLoanPayment saveUpdateLoan  Method");

		Loan loan;
		if (dtoLoan.getLoanId() != null && dtoLoan.getLoanId() > 0) { // Update
			loan = repositoryLoan.findByIdAndIsDeleted(dtoLoan.getLoanId(), false);
			loan.setCreatedBy(loggedInUserId);
			loan.setCreatedDate(new Date());
		} else { // Create
			loan = new Loan();
			loan.setUpdatedBy(loggedInUserId);
			loan.setUpdatedDate(new Date());
			loan.setIsDeleted(false);
			// loan.setIsCompleted(false);
		}

		loan.setLoanAmt(dtoLoan.getLoanAmt());
		loan.setNumOfMonths(dtoLoan.getNumOfMonths());
		loan.setDeductionAmt(dtoLoan.getDeductionPerMonthDefault());
		log.info("dtoLoan.getDeductionPerMonthDefault():" + dtoLoan.getDeductionPerMonthDefault());
		loan.setTransactionDate(dtoLoan.getStartDate());
		loan.setStartDate(dtoLoan.getStartDate());
		loan.setEndDate(dtoLoan.getEndDate());

		Integer idx = dtoLoan.getEmployeeIdx();
		if (idx != null && idx > 0) {
			EmployeeMaster employeeMaster = repositoryEmployeeMaster.findByEmployeeIndexIdAndIsDeleted(idx, false);
			log.info("empId:" + employeeMaster.getEmployeeIndexId());
			loan.setEmployeeMaster(employeeMaster);
		}

		// Float deductionPerMonthDefault = null;
		// deductionPerMonthDefault = dtoLoan.getDeductionPerMonthDefault();
		// log.info("getDeductionPerMonthDefault" + deductionPerMonthDefault);
		// loan.setDeductionAmt(deductionPerMonthDefault);

		// loan.getWorkflowReqId(1); //unknown now

		/*
		 * //Unused now code short numOfMonthsStack; // Yes (will be used in LoanDetails
		 * & for NumOfMonths) // if (dtoLoan.getDeductionPerMonthDefault() != null && //
		 * dtoLoan.getDeductionPerMonthDefault() > 0) { if (dtoLoan.getNumOfMonths() !=
		 * null && dtoLoan.getNumOfMonths() > 0) { numOfMonthsStack =
		 * dtoLoan.getNumOfMonths(); loan.setNumOfMonths(dtoLoan.getNumOfMonths());
		 * deductionPerMonthDefault = (Float) dtoLoan.getLoanAmt() /
		 * dtoLoan.getNumOfMonths(); } else { deductionPerMonthDefault = (Float)
		 * dtoLoan.getDeductionPerMonthDefault(); numOfMonthsStack = (short)
		 * Math.ceil(dtoLoan.getLoanAmt() / dtoLoan.getDeductionPerMonthDefault());
		 * loan.setNumOfMonths(numOfMonthsStack); log.info("numOfMonthsStack:" +
		 * numOfMonthsStack); }
		 */

		loan = repositoryLoan.save(loan);

		String refId = "LN-" + loan.getId().toString();
		loan.setRefId(refId); // dtoLoan.getRefId()); // ??
		// loan = repositoryLoan.save(loan);

		loan = repositoryLoan.saveAndFlush(loan);

		return loan; // dtoLoanDisplay;
	}

	public LoanDetails saveUpdateLoanDetails(DtoLoanDetails dtoLoanDetails, Integer loanId, Integer loggedInUserId) {
		log.info("ServiceLoanPayment saveUpdateLoanDetails  Method");

		if (loanId == null || loanId == 0)
			return null;

		if (dtoLoanDetails == null || dtoLoanDetails.equals(null))
			return null;

		LoanDetails loanDetails;
		if (dtoLoanDetails.getLoanDetailsId() != null && dtoLoanDetails.getLoanDetailsId() > 0) { // Update
			loanDetails = repositoryLoanDetails.findByIdAndIsDeletedAndLoanId(dtoLoanDetails.getLoanDetailsId(), false,
					loanId);
			if (loanDetails == null || loanDetails.equals(null))
				return null;

			loanDetails.setId(dtoLoanDetails.getLoanDetailsId());
			loanDetails.setUpdatedBy(loggedInUserId);
			loanDetails.setUpdatedDate(new Date());
			// loanDetails.setIsPaid(dtoLoanDetails.getIsPaid());
			// loanDetails.setIsPostpone(dtoLoanDetails.getIsPostpone());
		} else { // Create
			loanDetails = new LoanDetails(); // loanDetails.id is set AUTO
			loanDetails.setCreatedBy(loggedInUserId);
			loanDetails.setIsDeleted(false);
			loanDetails.setCreatedDate(new Date());
			// loanDetails.setIsPaid(false);
			// loanDetails.setIsPostpone(false);
		}

		loanDetails.setIsPaid(dtoLoanDetails.getIsPaid());
		loanDetails.setIsPostpone(dtoLoanDetails.getIsPostpone());

		Loan loan = repositoryLoan.findByIdAndIsDeleted(loanId, false);
		loanDetails.setLoan(loan); // Loan.id
		log.info("done setting loan details");
		loanDetails.setAmt((Float) dtoLoanDetails.getDeductionAmount());
		loanDetails.setMonthYear(dtoLoanDetails.getMonthYear());

		loanDetails = repositoryLoanDetails.saveAndFlush(loanDetails);

		return loanDetails;

	}

	public DtoLoan saveOrUpdate(DtoLoan dtoLoan) { // Callable Master Function
		log.info("ServiceLoanPayment saveOrUpdateMaster  Method");
		Integer loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		log.info("loggedInUserId:" + loggedInUserId);
		if (dtoLoan == null)
			return dtoLoan;

		/* Saving Loan */
		Loan loan0 = saveUpdateLoan(dtoLoan, loggedInUserId); // Fn Call

		if (loan0.getId() == null || loan0.getId() == 0) {
			log.info("loan.id is Empty! :: " + loan0.getId());
			return null;
		}

		/* Getting Saved Loan */
		Loan loan = repositoryLoan.findByIdAndIsDeleted(loan0.getId(), false);

		if (loan.getId() == null || loan.equals(null)) {
			log.info("loan Entity Not found for this id: " + loan.getId());
			return null;
		}

		// test
		log.info("loan.getDeductionAmt():" + loan.getDeductionAmt());

		/* Getting all LoanDetails Records */
		List<DtoLoanDetails> dtoLoanDetailsList = dtoLoan.getLoanDetailsList(); // THIS

		if (dtoLoanDetailsList == null || dtoLoanDetailsList.isEmpty() || dtoLoanDetailsList.equals(null))
			log.info("Are You trying to SaveOrUpdate a Loan Without LoanDetails (Innstallments) Records ?");

		for (DtoLoanDetails dtoLoanDetails : dtoLoanDetailsList) {
			log.info("inside for loop of save LoanDetails");
			LoanDetails loanDetails = saveUpdateLoanDetails(dtoLoanDetails, loan.getId(), loggedInUserId);
			if (loanDetails != null && !loanDetails.equals(null))
				log.info("All Save Methods Executed Succeffully!");
		}

		log.info("Now.... Retuirning the Diplay Part...!");

		DtoLoan dtoLoanDisplay = new DtoLoan(); // For Display - Loan

		dtoLoanDisplay.setLoanId(loan.getId());
		dtoLoanDisplay.setLoanAmt(loan.getLoanAmt());
		dtoLoanDisplay.setNumOfMonths(loan.getNumOfMonths());
		dtoLoanDisplay.setRefId(loan.getRefId());
		dtoLoanDisplay.setStartDate(loan.getStartDate());
		dtoLoanDisplay.setDeductionPerMonthDefault(loan.getDeductionAmt());

		List<DtoLoanDetails> dtoLoanDetailsDisplayList = new ArrayList<>(); // For Display LoanDetails List
		List<LoanDetails> LoanDetailsList = repositoryLoanDetails.findByIsDeletedAndLoanId(false, loan.getId());
		for (LoanDetails loanDetails : LoanDetailsList) {
			log.info("inside for loop of Displaying LoanDetails");
			DtoLoanDetails dtoLoanDetailsDisplay = new DtoLoanDetails();
			dtoLoanDetailsDisplay.setLoanDetailsId(loanDetails.getId()); // Q
			dtoLoanDetailsDisplay.setReqIdx(loanDetails.getLoan().getId()); // Q //LoanId
			dtoLoanDetailsDisplay.setDeductionAmount(loanDetails.getAmt());
			dtoLoanDetailsDisplay.setMonthYear(loanDetails.getMonthYear());
			dtoLoanDetailsDisplay.setIsPostpone(loanDetails.getIsPostpone());
			dtoLoanDetailsDisplay.setIsPaid(loanDetails.getIsPaid());

			dtoLoanDetailsDisplayList.add(dtoLoanDetailsDisplay);

		}

		dtoLoanDisplay.setLoanDetailsList(dtoLoanDetailsDisplayList); // dtoLoanDetailsList);

		return dtoLoanDisplay; // tobe updated ME
	}

	public DtoSearchActivity viewLoan(DtoLoan dl) {
		log.info("view loan service is calling");
		DtoSearchActivity dtoSearchActivity = new DtoSearchActivity();
		String searchKeyword = "%" + dl.getSearchKeyword() + "%";
		log.info("searchKeyword:" + searchKeyword);
		Pageable pageable = new PageRequest(dl.getPageNumber(), dl.getPageSize(), Sort.Direction.DESC, "id");
		List<Loan> loanList = repositoryLoan.viewLoan(searchKeyword, pageable);
		// log.info(loanList.size());
		List<DtoLoan> dtoLoanList = new ArrayList<DtoLoan>();
		for (Loan loan : loanList) {
			DtoLoan dtoLoan = new DtoLoan();
			log.info(loan.toString());
			dtoLoan.setLoanId(loan.getId());
			dtoLoan.setRefId(loan.getRefId());
			dtoLoan.setEmployeeName(loan.getEmployeeMaster() == null ? "" : loan.getEmployeeMaster().getEmployeeId());
			dtoLoan.setTransactionDate(loan.getTransactionDate());
			dtoLoan.setLoanAmt(loan.getLoanAmt());

			dtoLoanList.add(dtoLoan);
		}
		Integer totalCount = repositoryLoan.countByIsDeleted(searchKeyword);
		dtoSearchActivity.setTotalCount(totalCount);
		dtoSearchActivity.setRecords(dtoLoanList);

		log.info(dtoLoanList);

		return dtoSearchActivity;
	}

	public DtoLoan getLoanDetails(Integer loanId) {
		log.info("Loan detail  service is calling");
		// List<LoanDetails> loanDetailsList = new ArrayList<LoanDetails>();
		if (loanId == null || loanId == 0)
			return null;
		Loan loan = repositoryLoan.findByIdAndIsDeleted(loanId, false);
		// myLoan.get
		// Loan l = new Loan();
		// l.setRefId(dtoLoan.getLoanId().toString());
		List<LoanDetails> loanDetailsList = repositoryLoanDetails.findByIsDeletedAndLoanId(false, loanId);
		// Loan ls = repositoryLoan.findByIdAndIsDeleted(loanId, false);
		DtoLoan dtoLoan = new DtoLoan();
		List<DtoLoanDetails> dtoLoanDetailsList = new ArrayList<DtoLoanDetails>();

		dtoLoan.setEmployeeIdx(loan.getEmployeeMaster().getEmployeeIndexId());
		dtoLoan.setRefId(loan.getRefId());

		dtoLoan.setLoanId(loan.getId());
		dtoLoan.setLoanAmt(loan.getLoanAmt());
		dtoLoan.setNumOfMonths(loan.getNumOfMonths());
		dtoLoan.setStartDate(loan.getStartDate());
		dtoLoan.setDeductionPerMonthDefault(loan.getDeductionAmt());
		dtoLoan.setTransactionDate(loan.getTransactionDate());

		for (LoanDetails loanDetails : loanDetailsList) {
			// LoanDetails ls = new LoanDetails();
			DtoLoanDetails dtoLoanDetailsDisplay = new DtoLoanDetails();

			dtoLoanDetailsDisplay.setLoanDetailsId(loanDetails.getId()); // Q
			dtoLoanDetailsDisplay.setReqIdx(loanDetails.getLoan().getId()); // Q //LoanId
			dtoLoanDetailsDisplay.setDeductionAmount(loanDetails.getAmt());
			dtoLoanDetailsDisplay.setMonthYear(loanDetails.getMonthYear());
			dtoLoanDetailsDisplay.setIsPostpone(loanDetails.getIsPostpone());
			dtoLoanDetailsDisplay.setIsPaid(loanDetails.getIsPaid());

			dtoLoanDetailsList.add(dtoLoanDetailsDisplay);
		}

		dtoLoan.setLoanDetailsList(dtoLoanDetailsList);

		// loanDetailsList.add(ls);

		// }
		return dtoLoan;
	}

	public DtoSearch getAllPreviousLoansRemainingsByEmpId(DtoEmployeeMasterForLoan dtoEmployeeMasterForLoan) { // Integer
																												// empId)
																												// { //
																												// PopUp
		// List<DtoLoan>
		log.info("inside getAllPreviousLoans method in service");
		DtoSearch dtoSearch = new DtoSearch();
		List<DtoLoansPopup> dtoLoansRecordsList = new ArrayList<DtoLoansPopup>();
		Integer empId = dtoEmployeeMasterForLoan.getEmployeeIndexId();
		if (empId == null || empId == 0)
			return null;
		EmployeeMaster employeeMaster = repositoryEmployeeMaster.findByEmployeeIndexIdAndIsDeleted(empId, false); // findByEmployeeId(id);
		if (employeeMaster == null || employeeMaster.equals(null))
			return null;
		empId = employeeMaster.getEmployeeIndexId();
		List<Loan> Loans = repositoryLoan.findByEmployeeMasterEmployeeIndexId(empId);
		if (Loans == null || Loans.isEmpty())
			return null;
		int i = 0;
		// for (Loan loan : Loans) {
		List<LoanDetails> loanDetailsList = repositoryLoanDetails.findByEmployeeIdAndIsDeleted(empId,
				new PageRequest(dtoEmployeeMasterForLoan.getPageNumber(), dtoEmployeeMasterForLoan.getPageSize(),
						Sort.Direction.ASC, "id"));

		for (LoanDetails loanDetails : loanDetailsList) {
			DtoLoansPopup dtoLoansRecord = new DtoLoansPopup();
			log.info("loanDetails : " + loanDetails.getIsPaid() + loanDetails.getIsDeleted() + loanDetails);
//			if (!loanDetails.getIsPaid().equals(null) && loanDetails.getIsPaid().equals(false) &&
//				!loanDetails.getIsPostpone().equals(null) && loanDetails.getIsPostpone().equals(false) ) {
			dtoLoansRecord.setRefId(loanDetails.getLoan().getRefId());
			dtoLoansRecord.setMonthYear(loanDetails.getMonthYear());
			dtoLoansRecord.setDeductionAmount(loanDetails.getAmt());
			dtoLoansRecordsList.add(dtoLoansRecord);
			i++;
//			}
		}
		log.info("i : " + i);
		// }
		Integer totalCount = repositoryLoanDetails.countByEmployeeIdAndIsDeleted(empId);
		log.info("totalCount : " + totalCount);
		dtoSearch.setTotalCount(totalCount); // i);
		dtoSearch.setRecords(dtoLoansRecordsList);

		return dtoSearch; // dtoLoansList;

	}

}
