/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayScheduleSetup;

/**
 * Description: Interface for PayScheduleSetup  
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryPayScheduleSetup")
public interface RepositoryPayScheduleSetup extends JpaRepository<PayScheduleSetup, Integer>{
	
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public PayScheduleSetup findByPayScheduleIndexIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetup> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetup p where p.isDeleted=false")
	public Integer getCountOfTotalPayScheduleSetup();
	
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<PayScheduleSetup> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetup p set p.isDeleted =:deleted, p.updatedBy=:updateById where p.id IN (:idList)")
	public void deletePayScheduleSetup(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetup p set p.isDeleted =:deleted ,p.updatedBy =:updateById where p.id =:id ")
	public void deleteSinglePayScheduleSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public PayScheduleSetup findTop1ByOrderByPayScheduleIndexIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select p from PayScheduleSetup p where (p.payScheduleId like :searchKeyWord  or p.payScheduleDescription like :searchKeyWord or p.payScheduleDescriptionArabic like :searchKeyWord or p.paySchedulePayPeriod like :searchKeyWord) and p.isDeleted=false")
	public List<PayScheduleSetup> predictivePayScheduleSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetup> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetup p where (p.payScheduleId like :searchKeyWord  or p.payScheduleDescription like :searchKeyWord or p.payScheduleDescriptionArabic like :searchKeyWord or p.paySchedulePayPeriod like :searchKeyWord) and p.isDeleted=false")
	public Integer predictivePayScheduleSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select p from PayScheduleSetup p where (p.payScheduleId like :searchKeyWord  or p.payScheduleDescription like :searchKeyWord or p.payScheduleDescriptionArabic like :searchKeyWord or p.paySchedulePayPeriod like :searchKeyWord) and p.isDeleted=false")
	public List<PayScheduleSetup> predictivePayScheduleSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param payScheduleIndexId
	 * @return
	 */
	@Query("select p from PayScheduleSetup p where (p.payScheduleId =:payScheduleId) and p.isDeleted=false")
	public List<PayScheduleSetup> findByPayScheduleSetup(@Param("payScheduleId")String payScheduleId);

	@Query("select p from PayScheduleSetup p where (p.payScheduleId like :searchKeyWord) and p.isDeleted=false order by p.id desc")
	public List<PayScheduleSetup> predictiveSearchPayScheduleIdWithPagination(@Param("searchKeyWord")String payScheduleId);

	@Query("select count(*) from PayScheduleSetup p ")
	public Integer getCountOfTotaPayScheduleSetup();
}
