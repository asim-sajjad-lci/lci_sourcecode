/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.OrientationPredefinedCheckListItem;
import com.bti.hcm.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO OrientationPredefinedCheckListItem class having getter and
 * setter for fields (POJO) Name Name of Project: Hcm Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoOrientationPredefinedCheckListItem extends DtoBase{

	private int id;
	private short preDefineCheckListType;
	private String preDefineCheckListItemDescription;
	private String preDefineCheckListItemDescriptionArabic;
	private String orientationChecklistItemDescription;
	private String orientationChecklistItemDescriptionArabic;
	private Integer orientationPredefinedCheckListItemId;
	private List<DtoOrientationCheckListSetup> subItems;
	private List<DtoOrientationPredefinedCheckListItem> deleteOrientationPredefinedCheckListItem;

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public short getPreDefineCheckListType() {
		return preDefineCheckListType;
	}

	public void setPreDefineCheckListType(short preDefineCheckListType) {
		this.preDefineCheckListType = preDefineCheckListType;
	}

	public String getPreDefineCheckListItemDescription() {
		return preDefineCheckListItemDescription;
	}

	public void setPreDefineCheckListItemDescription(String preDefineCheckListItemDescription) {
		this.preDefineCheckListItemDescription = preDefineCheckListItemDescription;
	}

	public String getPreDefineCheckListItemDescriptionArabic() {
		return preDefineCheckListItemDescriptionArabic;
	}

	public void setPreDefineCheckListItemDescriptionArabic(String preDefineCheckListItemDescriptionArabic) {
		this.preDefineCheckListItemDescriptionArabic = preDefineCheckListItemDescriptionArabic;
	}
	public List<DtoOrientationPredefinedCheckListItem> getDeleteOrientationPredefinedCheckListItem() {
		return deleteOrientationPredefinedCheckListItem;
	}

	public void setDeleteOrientationPredefinedCheckListItem(
			List<DtoOrientationPredefinedCheckListItem> deleteOrientationPredefinedCheckListItem) {
		this.deleteOrientationPredefinedCheckListItem = deleteOrientationPredefinedCheckListItem;
	}

	public String getOrientationChecklistItemDescription() {
		return orientationChecklistItemDescription;
	}

	public void setOrientationChecklistItemDescription(String orientationChecklistItemDescription) {
		this.orientationChecklistItemDescription = orientationChecklistItemDescription;
	}

	public String getOrientationChecklistItemDescriptionArabic() {
		return orientationChecklistItemDescriptionArabic;
	}

	public void setOrientationChecklistItemDescriptionArabic(String orientationChecklistItemDescriptionArabic) {
		this.orientationChecklistItemDescriptionArabic = orientationChecklistItemDescriptionArabic;
	}

	public Integer getOrientationPredefinedCheckListItemId() {
		return orientationPredefinedCheckListItemId;
	}

	public void setOrientationPredefinedCheckListItemId(Integer orientationPredefinedCheckListItemId) {
		this.orientationPredefinedCheckListItemId = orientationPredefinedCheckListItemId;
	}

	public List<DtoOrientationCheckListSetup> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<DtoOrientationCheckListSetup> subItems) {
		this.subItems = subItems;
	}

	public DtoOrientationPredefinedCheckListItem(
			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem) {

		this.id = orientationPredefinedCheckListItem.getId();

		if (UtilRandomKey.isNotBlank(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescription())) {
			this.preDefineCheckListItemDescription = orientationPredefinedCheckListItem
					.getPreDefineCheckListItemDescription();
		} else {
			this.preDefineCheckListItemDescription = "";
		}

		if (UtilRandomKey.isNotBlank(orientationPredefinedCheckListItem.getPreDefineCheckListItemDescriptionArabic())) {
			this.preDefineCheckListItemDescriptionArabic = orientationPredefinedCheckListItem
					.getPreDefineCheckListItemDescriptionArabic();
		} else {
			this.preDefineCheckListItemDescriptionArabic = "";
		}
	}

	public DtoOrientationPredefinedCheckListItem() {

	}

}
