package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.CompnayInsurance;
import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.model.dto.DtoCompnayInsurance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryCompanyInsurance;
import com.bti.hcm.repository.RepositoryHrCity;
import com.bti.hcm.repository.RepositoryHrCountry;
import com.bti.hcm.repository.RepositoryHrState;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceHelthInsurance")
public class ServiceCompanyInsurance {
	/**
	 * @Description LOGGER use for put a logger in CompanyInsurance Service
	 */
	static Logger log = Logger.getLogger(ServiceCompanyInsurance.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in CompanyInsurance service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in CompanyInsurance service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in CompanyInsurance service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired(required = false)
	RepositoryCompanyInsurance repositoryCompanyInsurance;

	@Autowired
	RepositoryHrCountry repositoryHrCountry;

	@Autowired
	RepositoryHrState repositoryHrState;

	@Autowired
	RepositoryHrCity repositoryHrCity;

	@Autowired
	ServiceResponse response;

	public DtoCompnayInsurance saveOrUpdate(DtoCompnayInsurance dtoHelthInsurance) {

		try {
			log.info("saveOrUpdate Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			CompnayInsurance helthInsurance = null;
			if (dtoHelthInsurance.getId() != null && dtoHelthInsurance.getId() > 0) {

				helthInsurance = repositoryCompanyInsurance.findByIdAndIsDeleted(dtoHelthInsurance.getId(), false);
				helthInsurance.setUpdatedBy(loggedInUserId);
				helthInsurance.setUpdatedDate(new Date());
			} else {
				helthInsurance = new CompnayInsurance();
				helthInsurance.setCreatedDate(new Date());
				Integer rowId = repositoryCompanyInsurance.getCountOfTotalCompanyInsurance();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				helthInsurance.setRowId(increment);
			}

			HrCountry hrCountry = repositoryHrCountry.findOne(dtoHelthInsurance.getCountryId());
			HrState hrState = repositoryHrState.findOne(dtoHelthInsurance.getStateId());
			HrCity hrCity = repositoryHrCity.findOne(dtoHelthInsurance.getCityId());

			helthInsurance.setPhoneNo(dtoHelthInsurance.getPhoneNo());
			helthInsurance.setInsCompanyId(dtoHelthInsurance.getInsCompanyId());
			helthInsurance.setFax(dtoHelthInsurance.getFax());
			helthInsurance.setEmail(dtoHelthInsurance.getEmail());
			helthInsurance.setDesc(dtoHelthInsurance.getDesc());
			helthInsurance.setContactPerson(dtoHelthInsurance.getContactPerson());
			helthInsurance.setCountry(hrCountry);
			helthInsurance.setCity(hrCity);
			helthInsurance.setState(hrState);
			helthInsurance.setArbicDesc(dtoHelthInsurance.getArbicDesc());
			helthInsurance.setAddress(dtoHelthInsurance.getAddress());
			helthInsurance.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			helthInsurance = repositoryCompanyInsurance.saveAndFlush(helthInsurance);
			log.debug("HelthInsurance is:" + helthInsurance.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoHelthInsurance;

	}

	public DtoSearch getAll(DtoCompnayInsurance dtoHelthInsurance) {
		log.info("getHelthInsurance Method");
		DtoSearch dtoSearch = new DtoSearch();

		try {
			
			dtoSearch.setPageNumber(dtoHelthInsurance.getPageNumber());
			dtoSearch.setPageSize(dtoHelthInsurance.getPageSize());
			dtoSearch.setTotalCount(repositoryCompanyInsurance.getCountOfTotalHelthInsurance());
			List<CompnayInsurance> helthInsuranceList = null;
			if (dtoHelthInsurance.getPageNumber() != null && dtoHelthInsurance.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoHelthInsurance.getPageNumber(), dtoHelthInsurance.getPageSize(),
						Direction.DESC, "createdDate");
				helthInsuranceList = repositoryCompanyInsurance.findByIsDeleted(false, pageable);
			} else {
				helthInsuranceList = repositoryCompanyInsurance.findByIsDeletedOrderByCreatedDateDesc(false);
			}

			List<DtoCompnayInsurance> dtoHelthInsuranceList = new ArrayList<>();
			if (helthInsuranceList != null && !helthInsuranceList.isEmpty()) {
				for (CompnayInsurance helthInsurance : helthInsuranceList) {

					dtoHelthInsurance = new DtoCompnayInsurance(helthInsurance);

					dtoHelthInsurance.setPhoneNo(helthInsurance.getPhoneNo());
					dtoHelthInsurance.setId(helthInsurance.getId());
					dtoHelthInsurance.setInsCompanyId(helthInsurance.getInsCompanyId());
					dtoHelthInsurance.setFax(helthInsurance.getFax());
					dtoHelthInsurance.setEmail(helthInsurance.getEmail());
					dtoHelthInsurance.setDesc(helthInsurance.getDesc());
					dtoHelthInsurance.setContactPerson(helthInsurance.getContactPerson());

					if (helthInsurance.getCity() != null) {
						dtoHelthInsurance.setCityId(helthInsurance.getCity().getCityId());
						dtoHelthInsurance.setCityName(helthInsurance.getCity().getCityName());
						dtoHelthInsurance.setCity(helthInsurance.getCity());
					}

					if (helthInsurance.getCountry() != null) {
						dtoHelthInsurance.setCountryId(helthInsurance.getCountry().getCountryId());
						dtoHelthInsurance.setCountryName(helthInsurance.getCountry().getCountryName());
						dtoHelthInsurance.setCountry(helthInsurance.getCountry());
					}

					if (helthInsurance.getState() != null) {
						dtoHelthInsurance.setStateName(helthInsurance.getState().getStateName());
						dtoHelthInsurance.setStateId(helthInsurance.getState().getStateId());
						dtoHelthInsurance.setState(helthInsurance.getState());
					}

					dtoHelthInsurance.setArbicDesc(helthInsurance.getArbicDesc());
					dtoHelthInsurance.setAddress(helthInsurance.getAddress());
					dtoHelthInsuranceList.add(dtoHelthInsurance);
				}
				dtoSearch.setRecords(dtoHelthInsuranceList);
			}
			log.debug("All helthInsuranceList  Size is:" + dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoCompnayInsurance deleteHelthInsurance(List<Integer> ids) {
		log.info("deleteHelthInsurance Method");
		DtoCompnayInsurance dtoHelthInsurance = new DtoCompnayInsurance();
		dtoHelthInsurance.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("HELTH_INSURANCE_COMPANY_DELETED", false));
		dtoHelthInsurance.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("POSTION_ASSOCIATED", false));
		List<DtoCompnayInsurance> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("HELTH_INSURANCE__COMPANY_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer helthinscId : ids) {
				CompnayInsurance helthinsc = repositoryCompanyInsurance.findOne(helthinscId);
				
				if(helthinsc.getListRetirementPlanSetup().isEmpty()&& helthinsc.getHelthInsuranceList().isEmpty()) {
					DtoCompnayInsurance dtoHelthInsurance2 = new DtoCompnayInsurance();
					dtoHelthInsurance2.setId(helthinscId);
					repositoryCompanyInsurance.deleteSingleHelthInsurance(true, loggedInUserId, helthinscId);
					deletePosition.add(dtoHelthInsurance2);
				}else{
					inValidDelete = true;
					//invlidDeleteMessage.append(helthinsc.getInsCompanyId()+","); 
				}
				
				
			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoHelthInsurance.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoHelthInsurance.setMessageType("");
				
			}

			dtoHelthInsurance.setDelete(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete SkillSteup :" + dtoHelthInsurance.getId());
		return dtoHelthInsurance;
	}

	public DtoCompnayInsurance getById(int parseInt) {
		log.info("getById Method");
		DtoCompnayInsurance dtoHelthInsurance = new DtoCompnayInsurance();
		try {
			if (parseInt > 0) {
				CompnayInsurance compnayInsurance = repositoryCompanyInsurance.findByIdAndIsDeleted(parseInt, false);
				if (compnayInsurance != null) {
					dtoHelthInsurance = new DtoCompnayInsurance(compnayInsurance);
					dtoHelthInsurance.setId(compnayInsurance.getId());
					dtoHelthInsurance.setPhoneNo(compnayInsurance.getPhoneNo());
					dtoHelthInsurance.setInsCompanyId(compnayInsurance.getInsCompanyId());
					dtoHelthInsurance.setFax(compnayInsurance.getFax());
					dtoHelthInsurance.setEmail(compnayInsurance.getEmail());
					dtoHelthInsurance.setDesc(compnayInsurance.getDesc());
					dtoHelthInsurance.setContactPerson(compnayInsurance.getContactPerson());
					if (compnayInsurance.getCity() != null) {
						dtoHelthInsurance.setCityId(compnayInsurance.getCity().getCityId());
						dtoHelthInsurance.setCityName(compnayInsurance.getCity().getCityName());
					}
					if (compnayInsurance.getCountry() != null) {
						dtoHelthInsurance.setCountryId(compnayInsurance.getCountry().getCountryId());
						dtoHelthInsurance.setCountryName(compnayInsurance.getCountry().getCountryName());
					}
					if (compnayInsurance.getState() != null) {
						dtoHelthInsurance.setStateName(compnayInsurance.getState().getStateName());
						dtoHelthInsurance.setStateId(compnayInsurance.getState().getStateId());
					}
					dtoHelthInsurance.setArbicDesc(compnayInsurance.getArbicDesc());
					dtoHelthInsurance.setAddress(compnayInsurance.getAddress());
				} else {
					dtoHelthInsurance.setMessageType("POSITION_NOT_GETTING");

				}
			} else {
				dtoHelthInsurance.setMessageType("POSITION_REPEAT_ID_NOT_FOUND");

			}
			log.debug("HelthInsurance By Id is:" + dtoHelthInsurance.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoHelthInsurance;
	}

	public DtoCompnayInsurance repeatByCompanyId(String positionId) {
		log.info("repeatByCompanyId Method");
		DtoCompnayInsurance dtoHelthInsurance = new DtoCompnayInsurance();
		try {
			List<CompnayInsurance> skillsteup = repositoryCompanyInsurance.findByInsCompanyId(positionId.trim());
			if (skillsteup != null && !skillsteup.isEmpty()) {
				dtoHelthInsurance.setIsRepeat(true);
			} else {
				dtoHelthInsurance.setIsRepeat(false);
			}

		} catch (Exception e) {
			log.error(e);
		}

		return dtoHelthInsurance;
	}

	public DtoSearch searchHelthInsurance(DtoSearch dtoSearch) {
		
		try {
			log.info("searchHelthInsurance Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";

				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

					if (dtoSearch.getSortOn().equals("insCompanyId") || dtoSearch.getSortOn().equals("desc")
							|| dtoSearch.getSortOn().equals("arbicDesc") || dtoSearch.getSortOn().equals("address")
							|| dtoSearch.getSortOn().equals("phoneNo") || dtoSearch.getSortOn().equals("fax")
							|| dtoSearch.getSortOn().equals("contactPerson") || dtoSearch.getSortOn().equals("email") || dtoSearch.getSortOn().equals("cityName")) {
						
						if(dtoSearch.getSortOn().equals("cityName")) {
							dtoSearch.setSortOn("city");
						}
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
					}

				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");

				}

				dtoSearch.setTotalCount(
						this.repositoryCompanyInsurance.predictivePositionSearchTotalCount("%" + searchWord + "%"));
				List<CompnayInsurance> helthInsuranceList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						helthInsuranceList = this.repositoryCompanyInsurance.predictivePositionSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						helthInsuranceList = this.repositoryCompanyInsurance.predictivePositionSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						helthInsuranceList = this.repositoryCompanyInsurance.predictivePositionSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.DESC, condition));
					}

				}

				if (helthInsuranceList != null && !helthInsuranceList.isEmpty()) {
					List<DtoCompnayInsurance> dtoCompanyInsuranceList = new ArrayList<>();
					DtoCompnayInsurance dtoHelthInsurance = null;
					for (CompnayInsurance helthInsurance : helthInsuranceList) {
						dtoHelthInsurance = new DtoCompnayInsurance(helthInsurance);
						dtoHelthInsurance.setId(helthInsurance.getId());
						dtoHelthInsurance.setPhoneNo(helthInsurance.getPhoneNo());
						dtoHelthInsurance.setInsCompanyId(helthInsurance.getInsCompanyId());
						dtoHelthInsurance.setFax(helthInsurance.getFax());
						dtoHelthInsurance.setEmail(helthInsurance.getEmail());
						dtoHelthInsurance.setDesc(helthInsurance.getDesc());
						dtoHelthInsurance.setContactPerson(helthInsurance.getContactPerson());

						
						if (helthInsurance.getCity() != null) {
							dtoHelthInsurance.setCityId(helthInsurance.getCity().getCityId());
							dtoHelthInsurance.setCityName(helthInsurance.getCity().getCityName());
						}

						if (helthInsurance.getCountry() != null) {
							dtoHelthInsurance.setCountryId(helthInsurance.getCountry().getCountryId());
							dtoHelthInsurance.setCountryName(helthInsurance.getCountry().getCountryName());
						}

						if (helthInsurance.getState() != null) {
							dtoHelthInsurance.setStateName(helthInsurance.getState().getStateName());
							dtoHelthInsurance.setStateId(helthInsurance.getState().getStateId());
						}

						dtoHelthInsurance.setArbicDesc(helthInsurance.getArbicDesc());
						dtoHelthInsurance.setAddress(helthInsurance.getAddress());
						dtoCompanyInsuranceList.add(dtoHelthInsurance);
					}
					dtoSearch.setRecords(dtoCompanyInsuranceList);
				}
			}
			log.debug("Search searchHelthInsurance Size is:" + dtoSearch.getTotalCount());
			
		} catch (Exception e) {
		log.error(e);
		}
		return dtoSearch;
	}

	public DtoSearch searchCompanyId(DtoSearch dtoSearch) {
		try {
			log.info("searchCompanyId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				List<String> helthInsuranceList = this.repositoryCompanyInsurance.predictiveCompanyIdSearchWithPagination("%"+searchWord+"%");
					if(!helthInsuranceList.isEmpty()) {
						dtoSearch.setTotalCount(helthInsuranceList.size());
						dtoSearch.setRecords(helthInsuranceList.size());
					}
				dtoSearch.setIds(helthInsuranceList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	/** 
	 * @return DtoSearch
	 */
	public DtoSearch getAllCompanyInsurance() {
		DtoCompnayInsurance dtoCompnayInsurance;
		DtoSearch dtoSearch = new DtoSearch();

		try {
			
			List<CompnayInsurance> compnayInsuranceList = null;
			compnayInsuranceList = repositoryCompanyInsurance.findByIsDeleted(false);
			List<DtoCompnayInsurance> dtoCompnayInsuranceList=new ArrayList<>();
			if(compnayInsuranceList!=null && !compnayInsuranceList.isEmpty())
			{
				for (CompnayInsurance compnayInsurance : compnayInsuranceList) 
				{
					dtoCompnayInsurance=new DtoCompnayInsurance(compnayInsurance);
					dtoCompnayInsurance.setId(compnayInsurance.getId());
					dtoCompnayInsurance.setInsCompanyId(compnayInsurance.getInsCompanyId());
					dtoCompnayInsurance.setDesc(compnayInsurance.getDesc());
					dtoCompnayInsurance.setContactPerson(compnayInsurance.getContactPerson());
					dtoCompnayInsuranceList.add(dtoCompnayInsurance);
				}
				dtoSearch.setRecords(dtoCompnayInsuranceList);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
}
