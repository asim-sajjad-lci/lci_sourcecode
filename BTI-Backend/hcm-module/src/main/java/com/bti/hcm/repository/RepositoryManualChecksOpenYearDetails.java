package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecksOpenYearDetails;

@Repository("repositoryManualChecksOpenYearDetails")
public interface RepositoryManualChecksOpenYearDetails extends JpaRepository<ManualChecksOpenYearDetails, Integer> {

	@Query("SELECT pto FROM ManualChecksOpenYearDetails pto WHERE YEAR(pto.createdDate) =:year and pto.isDeleted = false")
	List<ManualChecksOpenYearDetails> findByYear(@Param("year") Integer year);
}
