/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.PayScheduleSetup;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO PayScheduleSetup class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoPayScheduleSetup extends DtoBase{
	
	private int payScheduleIndexId;
	private String payScheduleId;
	private String payScheduleDescription;
	private List<DtoPayScheduleSetupSchedulePeriods> subItems;
	private List<DtoPayScheduleSetupLocation> payScheduleLocation;
	private List<DtoPayScheduleSetupPosition> payScheduleSetupPositions;
	private List<DtoPayScheduleSetupDepartment> payScheduleSetupDepartments;
	private List<DtoPayScheduleSetupEmployee> payScheduleSetupEmployees;
	private String payScheduleDescriptionArabic;
	private short paySchedulePayPeriod;
	private Date payScheduleBeggingDate;
	private int payScheduleYear;
	private List<DtoPayScheduleSetup> deletePayScheduleSetup;
	public int getPayScheduleIndexId() {
		return payScheduleIndexId;
	}
	public void setPayScheduleIndexId(int payScheduleIndexId) {
		this.payScheduleIndexId = payScheduleIndexId;
	}
	public String getPayScheduleId() {
		return payScheduleId;
	}
	public void setPayScheduleId(String payScheduleId) {
		this.payScheduleId = payScheduleId;
	}
	public String getPayScheduleDescription() {
		return payScheduleDescription;
	}
	public void setPayScheduleDescription(String payScheduleDescription) {
		this.payScheduleDescription = payScheduleDescription;
	}
	public String getPayScheduleDescriptionArabic() {
		return payScheduleDescriptionArabic;
	}
	public void setPayScheduleDescriptionArabic(String payScheduleDescriptionArabic) {
		this.payScheduleDescriptionArabic = payScheduleDescriptionArabic;
	}
	public short getPaySchedulePayPeriod() {
		return paySchedulePayPeriod;
	}
	public void setPaySchedulePayPeriod(short paySchedulePayPeriod) {
		this.paySchedulePayPeriod = paySchedulePayPeriod;
	}
	public Date getPayScheduleBeggingDate() {
		return payScheduleBeggingDate;
	}
	public void setPayScheduleBeggingDate(Date payScheduleBeggingDate) {
		this.payScheduleBeggingDate = payScheduleBeggingDate;
	}
	public int getPayScheduleYear() {
		return payScheduleYear;
	}
	public void setPayScheduleYear(int payScheduleYear) {
		this.payScheduleYear = payScheduleYear;
	}
	public List<DtoPayScheduleSetup> getDeletePayScheduleSetup() {
		return deletePayScheduleSetup;
	}
	public void setDeletePayScheduleSetup(List<DtoPayScheduleSetup> deletePayScheduleSetup) {
		this.deletePayScheduleSetup = deletePayScheduleSetup;
	}	
	public DtoPayScheduleSetup() {
		
	}
	public DtoPayScheduleSetup(PayScheduleSetup payScheduleSetup) {
	
	}
	public List<DtoPayScheduleSetupSchedulePeriods> getSubItems() {
		return subItems;
	}
	public void setSubItems(List<DtoPayScheduleSetupSchedulePeriods> subItems) {
		this.subItems = subItems;
	}
	public List<DtoPayScheduleSetupLocation> getPayScheduleLocation() {
		return payScheduleLocation;
	}
	public void setPayScheduleLocation(List<DtoPayScheduleSetupLocation> payScheduleLocation) {
		this.payScheduleLocation = payScheduleLocation;
	}
	public List<DtoPayScheduleSetupPosition> getPayScheduleSetupPositions() {
		return payScheduleSetupPositions;
	}
	public void setPayScheduleSetupPositions(List<DtoPayScheduleSetupPosition> payScheduleSetupPositions) {
		this.payScheduleSetupPositions = payScheduleSetupPositions;
	}
	public List<DtoPayScheduleSetupDepartment> getPayScheduleSetupDepartments() {
		return payScheduleSetupDepartments;
	}
	public void setPayScheduleSetupDepartments(List<DtoPayScheduleSetupDepartment> payScheduleSetupDepartments) {
		this.payScheduleSetupDepartments = payScheduleSetupDepartments;
	}
	public List<DtoPayScheduleSetupEmployee> getPayScheduleSetupEmployees() {
		return payScheduleSetupEmployees;
	}
	public void setPayScheduleSetupEmployees(List<DtoPayScheduleSetupEmployee> payScheduleSetupEmployees) {
		this.payScheduleSetupEmployees = payScheduleSetupEmployees;
	}

	

}
