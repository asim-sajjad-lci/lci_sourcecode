package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ExitInterview;

@Repository("repositoryExitInterview")
public interface RepositoryExitInterview extends JpaRepository<ExitInterview, Integer>{

	ExitInterview findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ExitInterview d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleExitInterview(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from ExitInterview d where ( d.exitInterviewId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord ) and d.isDeleted=false")
	Integer predictiveExitInterviewSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from ExitInterview d where ( d.exitInterviewId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord) and d.isDeleted=false")
	List<ExitInterview> predictiveExitInterviewSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select d from ExitInterview d where (d.exitInterviewId=:searchKeyWord) and d.isDeleted=false")
	List<ExitInterview> findByExitInterviewId(@Param("searchKeyWord") String id);

	@Query("select d from ExitInterview d where (d.exitInterviewId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<ExitInterview> predictiveExitInterviewSearchWithPagination(@Param("searchKeyWord")String string);
	
	List<ExitInterview> findByIsDeletedAndInActive(Boolean deleted, Boolean inActive);

	@Query("select count(*) from ExitInterview e ")
	public Integer getCountOfTotalExitInterview();
}
