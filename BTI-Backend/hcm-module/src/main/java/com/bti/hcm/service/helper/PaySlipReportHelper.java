package com.bti.hcm.service.helper;

import static org.mockito.Mockito.verifyZeroInteractions;

import java.io.FileOutputStream;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.bti.hcm.model.dto.DtoBank;
import com.bti.hcm.model.dto.DtoCompanyInfo;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeePayrollDetails;
import com.bti.hcm.model.dto.DtoLocation;
import com.bti.hcm.model.dto.DtoPosition;
import com.bti.hcm.service.ServiceCalculateChecks;
import com.bti.hcm.util.CommonUtils;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PaySlipReportHelper {

	static Logger log = Logger.getLogger(PaySlipReportHelper.class.getName());

	private static final int POS_LEFT_HEADER_CELL = 0; // left header cell
	private static final int POS_RIGHT_HEADER_CELL = 1;
	
	public static final String STR_PAYSLIP_HEADING = "Payslip";
	public static final String STR_PAYSLIP_HEADING_AR = "كشف راتب";

	public static final String STR_EMPLOYEE_ID = "Employee ID";
	public static final String STR_EMPLOYEE_ID_AR = "رقم الموظف";

	public static final String STR_EMPLOYEE_NAME = "Employee ID";
	public static final String STR_EMPLOYEE_NAME_AR = "اسم الموظف";

	public static final String STR_FROM_PERIOD = "For the Period";
	public static final String STR_FROM_PERIOD_AR = "من فترة";

	public static final String STR_TO_PERIOD = "To";
	public static final String STR_TO_PERIOD_AR = "الى";

	public static final String STR_COMPANY = "Company";
	public static final String STR_COMPANY_AR = "الشركة";

	public static final String STR_DEPTARMENT = "Department";
	public static final String STR_DEPTARMENT_AR = "القسم";

	public static final String STR_LOCATION = "Location";
	public static final String STR_LOCATION_AR = "المنطقة";

	public static final String STR_POSITION = "Postion";
	public static final String STR_POSTION_AR = "المسمى الوظيفي";

	public static final String STR_VAT_CODE = "VAT Code";
	public static final String STR_VAT_CODE_AR = "رمز الضريبي";
	
	public static final String STR_INCOME_HEADING = "المستحقات - Income";
	public static final String STR_DEDUCTIONS_HEADING = "الخصومات - Deductions";
	public static final String STR_AMOUNT = "المبلغ Amount";

	public static final String STR_INCOME_TOTAL_HEADING = "مجمل الدخل - Total Income";
	public static final String STR_DEDUCTIONS_TOTAL_HEADING = "مجموع المخصوم - Total Deductions";
	
	public static final String STR_BASIC_SALARY_HEADING = "الراتب الاساسي - Basic Salary";
	public static final String STR_LOAN_HEADING = "سلفه - Loan";
	
	public static final String STR_TRANSPORTAION_HEADING = "بدل مواصلات - Transportation";
	public static final String STR_ABSENT_HEADING = "غياب - Absent";

	public static final String STR_HOUSING_HEADING = "بدل السكن - Housing";
	public static final String STR_GOSI_HEADING = "التأمينات الاجتماعية - GOSI";

	public static final String STR_OTHER_INCOME_1_HEADING = "1 بدلات اخرى - Others 1";
	public static final String STR_OTHER_DEDUCTIONS_HEADING = "خصومات أخرى - Other";

	public static final String STR_FIXIED_HEADING = "بدل ثابت - Fixed";
	public static final String STR_OVERTIME_HEADING = "بدل اضافي - Overtime";

	public static final String STR_BONUS_HEADING = "مكافات - Bonus";
	public static final String STR_OTHER_INCOME_2_HEADING = "2 بدلات اخرى - Others 2";

	public static final String STR_SUMMARY_HEADING = "مخلص SUMMARY";
	public static final String STR_BANK_INFO_HEADING = "Bank Account Info";

	public static final String STR_TOTAL_EARNINGS_HEADING = "مجموع المدفوع Total Earnings";
	public static final String STR_TOTAL_DEDUCTIONS_HEADING = "مجموع المخصوم Total Deductions";

	public static final String STR_BANK_NAME_HEADING = "اسم البنك Bank Name";
	public static final String STR_ACCOUNT_NUMBER_HEADING = "Bank Account No.(IBAN)";
	public static final String STR_ACCOUNT_NUMBER_HEADING_AR = "رقم الحساب - الايبان";

	public static final String STR_NET_PAY_HEADING = "اجمالي الراتب Net Pay";
	
	private static final String STR_PRINTED_DATE = "Printed On : ";
	
	
	private BaseFont calibriBaseFont;
	private BaseFont calibriBoldBaseFont;
	private BaseFont andalusBaseFont;	
	private Font headerFont;
	private Font headingFont;
	private Font textFont;
	private Font smallBoldFont;
	
	
	private String filePath;
	private DtoCompanyInfo dtoCompanyInfo;
	private DtoEmployeePayrollDetails dtoEmployeePayrollDetails;
    
    public PaySlipReportHelper(String filePath, DtoCompanyInfo dtoCompanyInfo) {
    	try {
	    	calibriBaseFont = BaseFont.createFont("/fonts/calibri.ttf", BaseFont.IDENTITY_H, true);
	    	Log.info("calibriBaseFont: " + calibriBaseFont);
	    	
	    	calibriBoldBaseFont = BaseFont.createFont("/fonts/calibrib.ttf", BaseFont.IDENTITY_H, true);
	    	Log.info("calibriBoldBaseFont: " + calibriBoldBaseFont);
	    	
	    	andalusBaseFont = BaseFont.createFont("/fonts/andlso.ttf", BaseFont.IDENTITY_H, true);
	    	Log.info("andalusBaseFont: " + andalusBaseFont);

	    	headerFont = new Font(andalusBaseFont, 14);
	        headingFont = new Font(calibriBoldBaseFont, 11);
	        smallBoldFont = new Font(calibriBoldBaseFont, 8);
	        textFont = new Font(calibriBaseFont, 11);
	        
	        this.filePath = filePath;
	        this.dtoCompanyInfo = dtoCompanyInfo;
	        
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		log.error("exception : " + ex);
    	}
    	
    }
    
    public void generatePaySlip(String empId, DtoEmployeePayrollDetails dtoEmployeePayrollDetails) throws Exception {
    	this.dtoEmployeePayrollDetails = dtoEmployeePayrollDetails;
		Document document = new Document();
		try
		{
			
			log.debug("Start ");
		    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
		    document.open();
		    
		    //Set attributes here
		    document.addAuthor("LCI IT Team");
		    document.addCreationDate();
		    document.addCreator("Algoras HCM");
		    document.addTitle("Pay Slip");
		    document.addSubject("Pay Slip");

		    
		    PdfPTable headerTable = generateHeaderTable();
	        document.add(headerTable);		    
		    log.debug("Header Generated");
	        
		    PdfPTable employeeHeaderTable = generateEmployeeHeaderTable();
		    document.add(employeeHeaderTable);
		    log.debug("Employee Header Generated");

	        
		    PdfPTable paymentDetailsTable = generatePaymentDetailsTable();
		    document.add(paymentDetailsTable);
		    log.debug("Payment Details Generated");
		    
		    PdfPTable summaryAndBankTable = generateSummaryAndBankTable();
		    document.add(summaryAndBankTable);
		    log.debug("Summary Generated");
		    
		    PdfPTable printedDateTable = generatePrintedDateTable();
		    document.add(printedDateTable);
		    log.debug("Printed Date Generated");
		    
	        document.close();
		    writer.close();
		    
			log.debug("Finish ");
		} catch (Exception e)
		{
		    e.printStackTrace();
		    log.error(e);
		    log.debug("exception: " + e);
		    log.error(e.getStackTrace());
		    throw e;
		}

    	
    }
	
	public PdfPTable generateHeaderTable() {
	    PdfPTable table = new PdfPTable(2); // 3 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table
 
        //Set Column widths
        float[] columnWidths = {1f, 1f};
        try {
			table.setWidths(columnWidths);
 
	        PdfPCell cellEnglishHeading = new PdfPCell(new Paragraph(STR_PAYSLIP_HEADING + " - ", headerFont));
	        cellEnglishHeading.setBorderColor(BaseColor.BLACK);
	        cellEnglishHeading.setPaddingLeft(10);
	        cellEnglishHeading.setPaddingBottom(5);
	        cellEnglishHeading.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        cellEnglishHeading.setVerticalAlignment(Element.ALIGN_TOP);
	        cellEnglishHeading.setBorderWidth(1.5f);
	        cellEnglishHeading.disableBorderSide(Rectangle.RIGHT);
	        
	        PdfPCell cellArabicHeading = new PdfPCell(new Paragraph(STR_PAYSLIP_HEADING_AR, headerFont));
	        cellArabicHeading.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
	        cellArabicHeading.setBorderColor(BaseColor.BLACK);
	        cellArabicHeading.setPaddingLeft(10);
	        cellArabicHeading.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        cellArabicHeading.setVerticalAlignment(Element.ALIGN_CENTER);
	        cellArabicHeading.setBorderWidth(1.5f);
	        cellArabicHeading.disableBorderSide(Rectangle.LEFT);
	 
	        table.addCell(cellEnglishHeading);
	        table.addCell(cellArabicHeading);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        return table;

	}

	public PdfPTable generateEmployeeHeaderTable() {
		
		DtoEmployeeMaster dtoEmployeeMaster = dtoEmployeePayrollDetails.getDtoEmployeeMaster();
		DtoDepartment dtoDepartment = null;
		DtoLocation dtoLocation = null;
		DtoPosition dtoPosition = null;
		
		String employeeFullName = "";
		String employeeFullNameArabic = "";
		
		String companyName = "";
		String companyNameArabic = "";
		
		String departmentDescription = "";
		String departmentDescriptionArabic = "";
		
		String locationDescription = "";
		String locationDescriptionArabic = "";
		
		String positionDescription = "";
		String positionDescriptionArabic = "";
		
		if (dtoEmployeeMaster != null) {
			employeeFullName = CommonUtils.removeNull(dtoEmployeeMaster.getEmployeeFullName());
			employeeFullNameArabic = CommonUtils.removeNull(dtoEmployeeMaster.getEmployeeFullNameArabic());
			dtoDepartment = dtoEmployeeMaster.getDtoDepartment();
			dtoLocation = dtoEmployeeMaster.getDtoLocation();
			dtoPosition = dtoEmployeeMaster.getDtoPosition();
		}
		
		if (dtoCompanyInfo != null) {
			companyName = CommonUtils.removeNull(dtoCompanyInfo.getName());
			companyNameArabic = CommonUtils.removeNull(dtoCompanyInfo.getNameArabic());
		}
		
		if (dtoDepartment != null) {
			departmentDescription =  CommonUtils.removeNull(dtoDepartment.getDepartmentDescription());
			departmentDescriptionArabic = CommonUtils.removeNull(dtoDepartment.getArabicDepartmentDescription());
		}
		
		if (dtoLocation != null) {
			locationDescription =  CommonUtils.removeNull(dtoLocation.getDescription());
			locationDescriptionArabic = CommonUtils.removeNull(dtoLocation.getArabicDescription());
		}
		
		if (dtoPosition != null) {
			positionDescription =  CommonUtils.removeNull(dtoPosition.getDescription());
			positionDescriptionArabic = CommonUtils.removeNull(dtoPosition.getArabicDescription());
		}
		
		float [] pointColumnWidths1 = {0.55f, 0.01f, 0.44f};       
		PdfPTable table = new PdfPTable(pointColumnWidths1);                             
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table
	    

        
		  PdfPCell empIdCell = new PdfPCell(generateEmployeeIdTable());       
		  empIdCell.setBorderWidth(1.0f);
		  table.addCell(empIdCell);             


	      PdfPCell spaceCell = new PdfPCell(new Paragraph(""));
	      spaceCell.disableBorderSide(Rectangle.TOP);
	      spaceCell.disableBorderSide(Rectangle.BOTTOM);
	      table.addCell(spaceCell);
	      


	      PdfPCell periodCell = new PdfPCell(generatePeriodTable());       
	      periodCell.setBorderWidth(1.0f);
	      table.addCell(periodCell);             
	      
	      PdfPTable employeeNameTable = generateTableForHeaderCell(STR_EMPLOYEE_NAME, STR_EMPLOYEE_NAME_AR, employeeFullName , employeeFullNameArabic, POS_LEFT_HEADER_CELL);
		  PdfPCell empNameCell = new PdfPCell(employeeNameTable);       
		  empNameCell.setBorderWidth(1.0f);
		  table.addCell(empNameCell);             


	      spaceCell = new PdfPCell(new Paragraph(""));
	      spaceCell.disableBorderSide(Rectangle.TOP);
	      spaceCell.disableBorderSide(Rectangle.BOTTOM);
	      table.addCell(spaceCell);
	      
	   
	      PdfPTable companyTable = generateTableForHeaderCell(STR_COMPANY, STR_COMPANY_AR, companyName, companyNameArabic, POS_RIGHT_HEADER_CELL);
	      PdfPCell companyNameCell = new PdfPCell(companyTable);       
	      companyNameCell.setBorderWidth(1.0f);
	      table.addCell(companyNameCell);             
	      

	      PdfPTable departmentTable = generateTableForHeaderCell(STR_DEPTARMENT, STR_DEPTARMENT_AR, departmentDescription, departmentDescriptionArabic, POS_LEFT_HEADER_CELL);
	      PdfPCell departmentCell = new PdfPCell(departmentTable);       
		  departmentCell.setBorderWidth(1.0f);
		  table.addCell(departmentCell);             


	      spaceCell = new PdfPCell(new Paragraph(""));
	      spaceCell.disableBorderSide(Rectangle.TOP);
	      spaceCell.disableBorderSide(Rectangle.BOTTOM);
	      table.addCell(spaceCell);
	      
	   
	      PdfPTable lcoationTable = generateTableForHeaderCell(STR_LOCATION, STR_LOCATION_AR, locationDescription, locationDescriptionArabic, POS_RIGHT_HEADER_CELL);
	      PdfPCell locationCell = new PdfPCell(lcoationTable);       
	      locationCell.setBorderWidth(1.0f);
	      table.addCell(locationCell);             

	      PdfPTable postionTable = generateTableForHeaderCell(STR_POSITION, STR_POSTION_AR, positionDescription, positionDescriptionArabic, POS_LEFT_HEADER_CELL);
	      PdfPCell postionCell = new PdfPCell(postionTable);       
		  postionCell.setBorderWidth(1.0f);
		  table.addCell(postionCell);             


	      spaceCell = new PdfPCell(new Paragraph(""));
	      spaceCell.disableBorderSide(Rectangle.TOP);
	      spaceCell.disableBorderSide(Rectangle.BOTTOM);
	      table.addCell(spaceCell);
	      
	   
	      PdfPTable vatCodeTable = generateTableForHeaderCell(STR_VAT_CODE, STR_VAT_CODE_AR, "", "", POS_RIGHT_HEADER_CELL);
	      PdfPCell vatCodeCell = new PdfPCell(vatCodeTable);       
	      vatCodeCell.setBorderWidth(1.0f);
	      table.addCell(vatCodeCell);             

	      return table;
	}

	public PdfPTable generatePaymentDetailsTable() {
		
		float [] pointColumnWidths1 = {0.55f, 0.01f, 0.44f};       
		PdfPTable table = new PdfPTable(pointColumnWidths1);                             
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table
	    
	      PdfPCell incomeHeadingCell = new PdfPCell(generateIncomeHeadingRow());
	      incomeHeadingCell.setBorderWidth(1.0f);
	      incomeHeadingCell.disableBorderSide(Rectangle.LEFT);
	      table.addCell(incomeHeadingCell);

	      PdfPCell spaceCell = new PdfPCell(new Paragraph(""));
	      spaceCell.setBorder(0);
	      table.addCell(spaceCell);

	      PdfPCell deductionsHeadingCell = new PdfPCell(generateDeductionHeadingRow());       
	      table.addCell(deductionsHeadingCell);
	      
	      PdfPCell basicSalaryCell = new PdfPCell(generateLeftDetailRow(STR_BASIC_SALARY_HEADING, "", "SR " + dtoEmployeePayrollDetails.getBasic().toString()));
	      basicSalaryCell.setBorder(0);
	      table.addCell(basicSalaryCell);
	      
	      table.addCell(spaceCell);
	      
	      PdfPCell loanCell = new PdfPCell(generateDeductionDetailRow(STR_LOAN_HEADING, "", "SR " + dtoEmployeePayrollDetails.getLoan().toString()));
	      loanCell.setBorder(0);
	      table.addCell(loanCell);
	      
	      PdfPCell transportationCell = new PdfPCell(generateLeftDetailRow(STR_TRANSPORTAION_HEADING, "", "SR " + dtoEmployeePayrollDetails.getTransportation().toString()));
	      transportationCell.setBorder(0);
	      table.addCell(transportationCell);
	      
	      table.addCell(spaceCell);
	      
	      PdfPCell absentCell = new PdfPCell(generateDeductionDetailRow(STR_ABSENT_HEADING, "", "SR " + dtoEmployeePayrollDetails.getAbsent()));
	      absentCell.setBorder(0);
	      table.addCell(absentCell);
	      
	      
	      
	      PdfPCell housingCell = new PdfPCell(generateLeftDetailRow(STR_HOUSING_HEADING, "", "SR " + dtoEmployeePayrollDetails.getHousing()));
	      housingCell.setBorder(0);
	      table.addCell(housingCell);
	      
	      table.addCell(spaceCell);
	      
	      PdfPCell gosiCell = new PdfPCell(generateDeductionDetailRow(STR_GOSI_HEADING, "", "SR " + dtoEmployeePayrollDetails.getGosi().toString()));
	      gosiCell.setBorder(0);
	      table.addCell(gosiCell);
	      
	      PdfPCell other1Cell = new PdfPCell(generateLeftDetailRow(STR_OTHER_INCOME_1_HEADING, "", "SR " + dtoEmployeePayrollDetails.getOthersBenefits1().toString()));
	      other1Cell.setBorder(0);
	      table.addCell(other1Cell);
	      
	      table.addCell(spaceCell);
	      
	      PdfPCell otherDeductionsCell = new PdfPCell(generateDeductionDetailRow(STR_OTHER_DEDUCTIONS_HEADING, "", "SR " + dtoEmployeePayrollDetails.getOtherDeductions().toString()));
	      otherDeductionsCell.setBorder(0);
	      table.addCell(otherDeductionsCell);
	      
	      PdfPCell fixedIncomeCell = new PdfPCell(generateLeftDetailRow(STR_FIXIED_HEADING, "", "SR " + dtoEmployeePayrollDetails.getFixed().toString()));
	      fixedIncomeCell.setBorder(0);
	      table.addCell(fixedIncomeCell);
	      
	      table.addCell(spaceCell);
	      
	      PdfPCell emptyDeductionsCell = new PdfPCell(generateDeductionDetailRow("", "", ""));
	      emptyDeductionsCell.setBorder(0);
	      table.addCell(emptyDeductionsCell);

	      PdfPCell overtimeCell = new PdfPCell(generateLeftDetailRow(STR_OVERTIME_HEADING, "", "SR " + dtoEmployeePayrollDetails.getOvertime().toString()));
	      overtimeCell.setBorder(0);
	      table.addCell(overtimeCell);
	      
	      table.addCell(spaceCell);
	      
	      table.addCell(emptyDeductionsCell);
	      
	      
	      PdfPCell bonusCell = new PdfPCell(generateLeftDetailRow(STR_BONUS_HEADING, "", "SR " + dtoEmployeePayrollDetails.getBonus()));
	      bonusCell.setBorder(0);
	      table.addCell(bonusCell);
	      
	      table.addCell(spaceCell);
	      
	      table.addCell(emptyDeductionsCell);

	      PdfPCell other2Cell = new PdfPCell(generateLeftDetailRow(STR_OTHER_INCOME_2_HEADING, "", "SR " + dtoEmployeePayrollDetails.getOthersBenefits2()));
	      other2Cell.setBorder(0);
	      table.addCell(other2Cell);
	      
	      table.addCell(spaceCell);
	      
	      table.addCell(emptyDeductionsCell);

	      
	      PdfPCell incomeTotalCell = new PdfPCell(generateIncomeTotalRow("SR " + dtoEmployeePayrollDetails.getTotalIncome()));
	      incomeTotalCell.setBorder(0);
	      table.addCell(incomeTotalCell);

	      table.addCell(spaceCell);
	      


	      PdfPCell deductionsTotalCell = new PdfPCell(generateDeductionTotalRow("SR " + dtoEmployeePayrollDetails.getTotalDeductions()));       
	      deductionsTotalCell.setBorder(0);
	      table.addCell(deductionsTotalCell);
	      
	      return table;
	}

	public PdfPTable generateSummaryAndBankTable() {
		float [] columnsWidths = {0.55f, 0.45f};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

		PdfPCell summaryHeadingCell = new PdfPCell(new Paragraph(STR_SUMMARY_HEADING, headingFont));       
		summaryHeadingCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		summaryHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		summaryHeadingCell.setBorderWidth(1.0f);
		summaryHeadingCell.setPaddingBottom(3);
		summaryHeadingCell.disableBorderSide(Rectangle.BOTTOM);
		table.addCell(summaryHeadingCell);             

		PdfPCell bankInfoHeadingCell = new PdfPCell(new Paragraph(STR_BANK_INFO_HEADING, headingFont));       
		bankInfoHeadingCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		bankInfoHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		bankInfoHeadingCell.setBorderWidth(1.0f);
		bankInfoHeadingCell.setPaddingBottom(3);
		bankInfoHeadingCell.disableBorderSide(Rectangle.BOTTOM);
		table.addCell(bankInfoHeadingCell);         

		PdfPCell summaryCell = new PdfPCell();
		summaryCell.setBorderWidth(1.0f);
		summaryCell.addElement(generateSummaryTable());
		summaryCell.disableBorderSide(Rectangle.TOP);
		table.addCell(summaryCell);
		
		
		PdfPCell bankInfoCell = new PdfPCell();
		bankInfoCell.setBorderWidth(1.0f);
		bankInfoCell.addElement(generateBankInfoTable());
		bankInfoCell.disableBorderSide(Rectangle.TOP);
		table.addCell(bankInfoCell);

        return table;
	}

	public PdfPTable generatePrintedDateTable() {
		float [] columnsWidths = {1f};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

		PdfPCell summaryHeadingCell = new PdfPCell(new Paragraph(STR_PRINTED_DATE + dtoEmployeePayrollDetails.getPrintedDate(), smallBoldFont));       
		summaryHeadingCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		summaryHeadingCell.setBorder(0);
		table.addCell(summaryHeadingCell);
		return table;
	}	
	
	public PdfPTable generateSummaryTable() {
		float [] columnsWidths = {1f};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%

        PdfPCell cell = new PdfPCell();
        cell.addElement(generateSummaryTableRow(STR_TOTAL_EARNINGS_HEADING, "SR " + dtoEmployeePayrollDetails.getTotalIncome()));
        cell.setBorder(0);
		table.addCell(cell);
		
		cell = new PdfPCell();
        cell.addElement(generateSummaryTableRow(STR_TOTAL_DEDUCTIONS_HEADING, "SR " + dtoEmployeePayrollDetails.getTotalDeductions()));
        cell.setBorder(0);
		table.addCell(cell);

		cell = new PdfPCell();
        cell.addElement(generateSummaryTableRow("", " "));
        cell.setBorder(0);
		table.addCell(cell);

		cell = new PdfPCell();
        cell.addElement(generateSummaryTableRow(STR_NET_PAY_HEADING, "SR " + dtoEmployeePayrollDetails.getNetPay()));
        cell.setBorder(0);
		table.addCell(cell);
		

        return table;
	}
	
	public PdfPTable generateSummaryTableRow(String heading, String data) {
		float [] columnsWidths = {0.70f, 0.30f};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%

		  PdfPCell summaryHeadingCell = new PdfPCell(new Paragraph(heading, headingFont));       
		  summaryHeadingCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  summaryHeadingCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  summaryHeadingCell.setBorder(0);
		  summaryHeadingCell.setPaddingBottom(3);
		  table.addCell(summaryHeadingCell);             


	      PdfPCell dataCell = new PdfPCell(new Paragraph(data, textFont));       
	      dataCell.setBorder(0);
	      table.addCell(dataCell);

        return table;
	}
	
	
	public PdfPTable generateBankInfoTable() {
		
		String bankDescription = "";
		String bankDescriptionArabic = "";
		
		DtoEmployeeMaster dtoEmployeeMaster = dtoEmployeePayrollDetails.getDtoEmployeeMaster();
		DtoBank dtoBank = null;
		
		if (dtoEmployeeMaster != null) {
			dtoBank = dtoEmployeeMaster.getDtoBank();
		}
		
		if (dtoBank != null ) {
			bankDescription = CommonUtils.removeNull(dtoBank.getBankDescription());
			bankDescriptionArabic = CommonUtils.removeNull(dtoBank.getBankDescriptionArabic());
		}
		
		
		float [] columnsWidths = {0.45f, 0.55f};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%

        
//        table.addCell(generateBankInfoTableRow(STR_BANK_NAME_HEADING, "IMMA BANK"));
		  PdfPCell bankNameHeadingCell = new PdfPCell(new Paragraph(STR_BANK_NAME_HEADING, headingFont));       
		  bankNameHeadingCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  bankNameHeadingCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  bankNameHeadingCell.setBorder(0);
		  bankNameHeadingCell.setPaddingBottom(3);
		  table.addCell(bankNameHeadingCell);             


	      PdfPCell bankNameDataCell = new PdfPCell(new Paragraph(bankDescription + " - " + bankDescriptionArabic, textFont));       
	      bankNameDataCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
	      bankNameDataCell.setBorder(0);
	      table.addCell(bankNameDataCell);

		  PdfPCell ibanCell = new PdfPCell(new Paragraph(STR_ACCOUNT_NUMBER_HEADING, headingFont));       
		  ibanCell.setBorder(0);
		  ibanCell.setPaddingBottom(3);
		  table.addCell(ibanCell);

		  String accountNumber = "";
		  accountNumber = CommonUtils.removeNull(dtoEmployeeMaster.getEmployeeBankAccountNumber());
		  
	      PdfPCell ibanDataCell = new PdfPCell(new Paragraph(accountNumber, textFont));       
	      ibanDataCell.setBorder(0);
	      ibanDataCell.setRowspan(2);
	      ibanDataCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      ibanDataCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
    	  table.addCell(ibanDataCell);

    	  PdfPCell ibanArabicCell = new PdfPCell(new Paragraph(STR_ACCOUNT_NUMBER_HEADING_AR, headingFont));       
		  ibanArabicCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  ibanArabicCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  ibanArabicCell.setBorder(0);
		  ibanArabicCell.setPaddingBottom(3);
		  table.addCell(ibanArabicCell);

//	      PdfPCell ibanArabicdataCell = new PdfPCell(new Paragraph("SA3492322999000002323223324", textFont));       
//	      ibanArabicdataCell.setBorder(0);
////	      dataCell.setRowspan(2);
//    	  table.addCell(ibanArabicdataCell);

        return table;
	}

	public PdfPTable generateBankInfoTableRow(String heading, String data) {
		float [] columnsWidths = {0.45f, 0.55f};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%

		  PdfPCell summaryHeadingCell = new PdfPCell(new Paragraph(heading, headingFont));       
		  summaryHeadingCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  summaryHeadingCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  summaryHeadingCell.setBorder(0);
		  summaryHeadingCell.setPaddingBottom(3);
		  table.addCell(summaryHeadingCell);             


	      PdfPCell dataCell = new PdfPCell(new Paragraph(data, textFont));       
	      dataCell.setBorder(0);
    	  table.addCell(dataCell);

        return table;
	}
	
	public PdfPTable generateEmployeeIdTable() {
		float [] columnsWidths = {1f, 0.6f, 0.4f};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%
		
        PdfPCell empIdEnlishCell = new PdfPCell(new Paragraph(STR_EMPLOYEE_ID, headingFont));
        empIdEnlishCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		empIdEnlishCell.setBorder(0);
		table.addCell(empIdEnlishCell);             
		
        PdfPCell empIdCell = new PdfPCell(new Paragraph(dtoEmployeePayrollDetails.getEmployeeCode(), textFont));
        empIdCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		empIdCell.setBorder(0);
		empIdCell.setRowspan(2);
		table.addCell(empIdCell);
		
        PdfPCell empIdEmptyCell = new PdfPCell(new Paragraph(""));       
        empIdEmptyCell.setBorder(0);
		table.addCell(empIdEmptyCell);
		
        PdfPCell empIdArabicCell = new PdfPCell(new Paragraph(STR_EMPLOYEE_ID_AR, headingFont));
        empIdArabicCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
        empIdArabicCell.setBorderWidth(1.0f);
		empIdArabicCell.setBorder(0);
		empIdArabicCell.setPaddingBottom(7);
		empIdArabicCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        empIdArabicCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(empIdArabicCell);             
		
		empIdCell.setBorder(0);
		table.addCell(empIdEmptyCell);
        empIdEmptyCell.setBorder(0);
		table.addCell(empIdEmptyCell);
		
		
		return table;
	}
	
	public PdfPTable generatePeriodTable() {
		float [] columnsWidths = {0.35f, 0.30f, 0.10f, 0.25f};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%
		
        PdfPCell fromPeriodCell = new PdfPCell(new Paragraph(STR_FROM_PERIOD, headingFont));
        fromPeriodCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		fromPeriodCell.setBorder(0);
		table.addCell(fromPeriodCell);             
		
        PdfPCell fromPeriodDateCell = new PdfPCell(new Paragraph(dtoEmployeePayrollDetails.getStrPeriodFromDate(), textFont));
        fromPeriodDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        fromPeriodDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		fromPeriodDateCell.setBorder(0);
		fromPeriodDateCell.setRowspan(2);
		table.addCell(fromPeriodDateCell);
		
        PdfPCell toPeriodCell = new PdfPCell(new Paragraph(STR_TO_PERIOD, headingFont));       
        toPeriodCell.setBorder(0);
		table.addCell(toPeriodCell);
		
        PdfPCell toPeriodDateCell = new PdfPCell(new Paragraph(dtoEmployeePayrollDetails.getStrPeriodToDate(), textFont));       
        toPeriodDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        toPeriodDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        toPeriodDateCell.setBorder(0);
        toPeriodDateCell.setRowspan(2);
		table.addCell(toPeriodDateCell);

		PdfPCell fromPeriodArabicCell = new PdfPCell(new Paragraph(STR_FROM_PERIOD_AR, headingFont));
        fromPeriodArabicCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		fromPeriodArabicCell.setBorder(0);
		fromPeriodArabicCell.setPaddingBottom(7);
		fromPeriodArabicCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        fromPeriodArabicCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(fromPeriodArabicCell);             

        PdfPCell toPeriodArabicCell = new PdfPCell(new Paragraph(STR_TO_PERIOD_AR, headingFont));       
        toPeriodArabicCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
        toPeriodArabicCell.setBorder(0);
        toPeriodArabicCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        toPeriodArabicCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(toPeriodArabicCell);

		return table;
	}



	public PdfPTable generateTableForHeaderCell(String englishHeading, String arabicHeading, String englishText, String arabicText, int position) {
		float firstColumnWidth = 1f;
		float secondColumnWidth = 1f;
		
		switch (position) {
			case POS_LEFT_HEADER_CELL:
				firstColumnWidth = 0.45f;
				secondColumnWidth = 0.55f;
				break;
	
			case POS_RIGHT_HEADER_CELL:
				firstColumnWidth = 0.25f;
				secondColumnWidth = 0.75f;
				break;
		}
		
		float [] columnsWidths = {firstColumnWidth, secondColumnWidth};
		PdfPTable table = new PdfPTable(columnsWidths);
        table.setWidthPercentage(100); //Width 100%
		
        PdfPCell englishHeadingCell = new PdfPCell(new Paragraph(englishHeading, headingFont));
        englishHeadingCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		englishHeadingCell.setBorder(0);
		table.addCell(englishHeadingCell);             
		
        PdfPCell englishDataCell = new PdfPCell(new Paragraph(englishText, textFont));
        englishDataCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		englishDataCell.setBorder(0);
		table.addCell(englishDataCell);
		
        PdfPCell arabicHeadingCell = new PdfPCell(new Paragraph(arabicHeading, headingFont));
        arabicHeadingCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
        arabicHeadingCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        arabicHeadingCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        arabicHeadingCell.setRowspan(2);
		arabicHeadingCell.setBorder(0);
		arabicHeadingCell.setPaddingBottom(7);
		table.addCell(arabicHeadingCell);             
		
        PdfPCell arabicDataCell = new PdfPCell(new Paragraph(arabicText, textFont));
        arabicDataCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
        arabicDataCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        arabicDataCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        arabicDataCell.setRowspan(2);
		arabicDataCell.setBorder(0);
		table.addCell(arabicDataCell);

		return table;
	}
	

	

	public PdfPTable generateIncomeHeadingRow() {
		float [] pointColumnWidths1 = {0.55f, 0.20f, 0.25f};       
		PdfPTable table = new PdfPTable(pointColumnWidths1);                             
        table.setWidthPercentage(100); //Width 100%
        
		  PdfPCell incomeHeadingCell = new PdfPCell(new Paragraph(STR_INCOME_HEADING, headingFont));       
		  incomeHeadingCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  incomeHeadingCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  incomeHeadingCell.setBorderWidth(1.0f);
		  incomeHeadingCell.disableBorderSide(Rectangle.RIGHT);
		  incomeHeadingCell.setPaddingBottom(3);
		  table.addCell(incomeHeadingCell);             

	      PdfPCell spaceCell = new PdfPCell(new Paragraph(""));
	      spaceCell.setBorderWidth(1.0f);
	      spaceCell.disableBorderSide(Rectangle.LEFT);
	      table.addCell(spaceCell);

	      PdfPCell amountCell = new PdfPCell(new Paragraph(STR_AMOUNT, headingFont));       
	      amountCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
	      amountCell.setBorderWidth(1.0f);
	      table.addCell(amountCell);
        
		
        return table;
	}
	
	public PdfPTable generateDeductionHeadingRow() {
		float [] pointColumnWidths1 = {0.50f, 0.20f, 0.30f};       
		PdfPTable table = new PdfPTable(pointColumnWidths1);                             
        table.setWidthPercentage(100); //Width 100%
        
		  PdfPCell deductionHeadingCell = new PdfPCell(new Paragraph(STR_DEDUCTIONS_HEADING, headingFont));       
		  deductionHeadingCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  deductionHeadingCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  deductionHeadingCell.setBorderWidth(1.0f);
		  deductionHeadingCell.disableBorderSide(Rectangle.RIGHT);
		  deductionHeadingCell.setPaddingBottom(3);
		  table.addCell(deductionHeadingCell);             

	      PdfPCell spaceCell = new PdfPCell(new Paragraph(""));
	      spaceCell.setBorderWidth(1.0f);
	      spaceCell.disableBorderSide(Rectangle.LEFT);
	      table.addCell(spaceCell);

	      PdfPCell amountCell = new PdfPCell(new Paragraph(STR_AMOUNT, headingFont));       
	      amountCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
	      amountCell.setBorderWidth(1.0f);
	      table.addCell(amountCell);
        
		
        return table;
	}
	
	public PdfPTable generateIncomeTotalRow(String totalAmount) {
		float [] pointColumnWidths1 = {0.70f, 0.30f};       
		PdfPTable table = new PdfPTable(pointColumnWidths1);                             
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(3f); //Space after table
        
		  PdfPCell incomeTotalCell = new PdfPCell(new Paragraph(STR_INCOME_TOTAL_HEADING, headingFont));       
		  incomeTotalCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  incomeTotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  incomeTotalCell.setBorderWidth(1.0f);
		  incomeTotalCell.disableBorderSide(Rectangle.RIGHT);
		  incomeTotalCell.setPaddingBottom(3);
		  table.addCell(incomeTotalCell);             


	      PdfPCell amountCell = new PdfPCell(new Paragraph(totalAmount, headingFont));       
	      amountCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
	      amountCell.disableBorderSide(Rectangle.LEFT);
	      amountCell.setBorderWidth(1.0f);
	      table.addCell(amountCell);
        
		
        return table;
	}
	
	public PdfPTable generateDeductionTotalRow(String totalAmount) {
		float [] pointColumnWidths1 = {0.70f, 0.30f};       
		PdfPTable table = new PdfPTable(pointColumnWidths1);                             
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(3f); //Space after table
        
		  PdfPCell deductionTotalCell = new PdfPCell(new Paragraph(STR_DEDUCTIONS_TOTAL_HEADING, headingFont));       
		  deductionTotalCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  deductionTotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  deductionTotalCell.setBorderWidth(1.0f);
		  deductionTotalCell.disableBorderSide(Rectangle.RIGHT);
		  deductionTotalCell.setPaddingBottom(3);
		  table.addCell(deductionTotalCell);             

	      PdfPCell amountCell = new PdfPCell(new Paragraph(totalAmount, headingFont));       
	      amountCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
	      amountCell.disableBorderSide(Rectangle.LEFT);
	      amountCell.setBorderWidth(1.0f);
	      table.addCell(amountCell);
        
		
        return table;
	}

	public PdfPTable generateLeftDetailRow(String heading, String factor, String data) {
		float [] pointColumnWidths1 = {0.55f, 0.20f, 0.25f};       
		PdfPTable table = new PdfPTable(pointColumnWidths1);                             
        table.setWidthPercentage(100); //Width 100%
        
		  PdfPCell incomeDetailCell = new PdfPCell(new Paragraph(heading, headingFont));       
		  incomeDetailCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  incomeDetailCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  incomeDetailCell.setBorder(0);
		  incomeDetailCell.setPaddingBottom(3);
		  table.addCell(incomeDetailCell);             

	      PdfPCell factorCell = new PdfPCell(new Paragraph(factor, textFont));
	      factorCell.setBorder(0);
	      factorCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      table.addCell(factorCell);

	      PdfPCell dataCell = new PdfPCell(new Paragraph(data, textFont));       
	      dataCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
	      dataCell.setBorder(0);
	      table.addCell(dataCell);
        
		
        return table;
	}
	
	public PdfPTable generateDeductionDetailRow(String heading, String factor, String data) {
		float [] pointColumnWidths1 = {0.50f, 0.20f, 0.30f};       
		PdfPTable table = new PdfPTable(pointColumnWidths1);                             
        table.setWidthPercentage(100); //Width 100%
        
		  PdfPCell deductionDetailCell = new PdfPCell(new Paragraph(heading, headingFont));       
		  deductionDetailCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		  deductionDetailCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  deductionDetailCell.setBorder(0);
		  deductionDetailCell.setPaddingBottom(3);
		  table.addCell(deductionDetailCell);             

	      PdfPCell factorCell = new PdfPCell(new Paragraph(factor, textFont));
	      factorCell.setBorder(0);
	      factorCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      table.addCell(factorCell);

	      PdfPCell dataCell = new PdfPCell(new Paragraph(data, textFont));       
	      dataCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
	      dataCell.setBorder(0);
	      table.addCell(dataCell);
        
		
        return table;
	}

}
