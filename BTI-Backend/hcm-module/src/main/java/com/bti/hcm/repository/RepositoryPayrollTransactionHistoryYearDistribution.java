package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollTransactionHistoryYearDistribution;

@Repository("repositoryPayrollTransactionHistoryYearDistribution")
public interface RepositoryPayrollTransactionHistoryYearDistribution extends JpaRepository<PayrollTransactionHistoryYearDistribution, Integer>{

}
