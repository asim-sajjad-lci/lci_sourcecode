package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeMasterForLoan;
import com.bti.hcm.model.dto.DtoLoan;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceLoanPayment;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerLoanPayment Name of Project: Hcm Version: 0.0.1
 */
@RestController
@RequestMapping("/loanPayment")
public class ControllerLoanPayment extends BaseController {

	private static final Logger LOGGER = Logger.getLogger(ControllerLoanPayment.class);

	@Autowired(required = true)
	ServiceLoanPayment serviceLoanPayment;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @param request
	 * @param dtoBatches
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST) // Create Loan Details Once Only!
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoLoan dtoLoan) throws Exception {
		LOGGER.info("Create LoanPAy Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoLoan = serviceLoanPayment.saveOrUpdate(dtoLoan);
			responseMessage = displayMessage(dtoLoan, "LOANS_CREATED", "LOANS_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Batches Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST) // Update Loan Details (Payments Stacks)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoLoan dtoLoan) throws Exception {
		LOGGER.info("Update Batches Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoLoan = serviceLoanPayment.saveOrUpdate(dtoLoan); // inside dtoLoan will be dtoLoanDetailsList
			responseMessage = displayMessage(dtoLoan, "LOANS_UPDATED_SUCCESS", "LOANS_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Batches Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllEmployeeList", method = RequestMethod.POST)
	public ResponseMessage getAllEmployeeList(HttpServletRequest request) throws Exception {
		LOGGER.debug("inside getAllEmployeeList Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		LOGGER.debug("flag: " + flag);
		if (flag) {
			List<DtoEmployeeMaster> employeeMList = serviceLoanPayment.getAllEmployeeList();
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.EMPLOYEE_GET_ALL, false),
					employeeMList);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}

		return responseMessage;
	}

	@RequestMapping(value = "/getEmployeeDetailsById", method = RequestMethod.POST)
	public ResponseMessage getEmployeeDetailsById(HttpServletRequest request, @RequestBody DtoLoan dtoLoan)
			throws Exception {
		LOGGER.info("inside getEmployeeDetails Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			// LOGGER.info("dtoEmployeeMaster.getIdNumber :" + dtoLoan.getEmployeeIdx());
			DtoEmployeeMasterForLoan employeeDetails = serviceLoanPayment
					.getEmployeeDetailsById(dtoLoan.getEmployeeIdx());
			responseMessage = displayMessage(employeeDetails, "EMPLOYEE_GET_ALL", "EMPLOYEE_NOT_GETTING",
					serviceResponse);
//			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
//					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.EMPLOYEE_GET_ALL, false),
//					employeeDetails);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}

		return responseMessage;
	}

	@RequestMapping(value = "/viewLoans", method = RequestMethod.POST)
	public ResponseMessage getAllLoan(HttpServletRequest request, @RequestBody DtoLoan dtoLoan) throws Exception {
		LOGGER.info("inside viewLoans api");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		// for api jst comment the flag = true; for testing purpose i m putting it true
		// flag = true;
		if (flag) {
			DtoSearchActivity dtoSearchActivity = serviceLoanPayment.viewLoan(dtoLoan);
			responseMessage = displayMessage(dtoSearchActivity, "viewLoans", "viewLoan", serviceResponse);

		} else {
			LOGGER.debug("unauthorized access");
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getLoanDetailsByLoanId", method = RequestMethod.POST)
	public ResponseMessage getLoanDetailsById(HttpServletRequest request, @RequestBody DtoLoan dtoLoan)
			throws Exception {
		LOGGER.info("inside loanDetails api");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		// for api jst comment the flag = true; for testing purpose i m putting it true
		// flag = true;
		if (flag) {
			// List<LoanDetails> dtoLoanDetails =
			DtoLoan dtoLoanDisplay = serviceLoanPayment.getLoanDetails(dtoLoan.getLoanId());
			responseMessage = displayMessage(dtoLoanDisplay, "getLoanDetailsById", "getLoanDetailsById",
					serviceResponse);

		} else {
			LOGGER.debug("unauthorized access");
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllRemainingsByEmpId", method = RequestMethod.POST)	
	public ResponseMessage getAllPreviousLoansByEmpId(HttpServletRequest request,
			@RequestBody DtoEmployeeMasterForLoan dtoEmployeeMasterForLoan) throws Exception {	//PopUp
		LOGGER.info("inside loanDetails api");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		// for api jst comment the flag = true; for testing purpose i m putting it true
		// flag = true;
		if (flag) {
			// List<LoanDetails> dtoLoanDetails =
			DtoSearch dtoLoansDisplay = serviceLoanPayment.getAllPreviousLoansRemainingsByEmpId(dtoEmployeeMasterForLoan);
			responseMessage = displayMessage(dtoLoansDisplay, "getLoanDetailsById", "getLoanDetailsById",
					serviceResponse);

		} else {
			LOGGER.debug("unauthorized access");
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

}
