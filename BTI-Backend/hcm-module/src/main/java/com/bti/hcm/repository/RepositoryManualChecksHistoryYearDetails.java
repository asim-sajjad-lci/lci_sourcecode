package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecksHistoryYearDetails;

@Repository("repositoryManualChecksHistoryYearDetails")
public interface RepositoryManualChecksHistoryYearDetails extends JpaRepository<ManualChecksHistoryYearDetails, Integer> {

}
