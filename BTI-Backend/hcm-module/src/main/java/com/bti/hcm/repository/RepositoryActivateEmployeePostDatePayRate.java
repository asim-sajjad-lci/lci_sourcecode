package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ActivateEmployeePostDatePayRate;

@Repository("/repositoryActivateEmployeePostDatePayRate")
public interface RepositoryActivateEmployeePostDatePayRate extends JpaRepository<ActivateEmployeePostDatePayRate, Integer>{

	ActivateEmployeePostDatePayRate findByIdAndIsDeleted(Integer id, boolean b);
	
	ActivateEmployeePostDatePayRate findOne(Integer planId);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ActivateEmployeePostDatePayRate a set a.isDeleted =:deleted ,a.updatedBy =:updateById where a.id =:id ")
	public void deleteSingleActivateEmployeePostDatePayRate(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);


	@Query("select count(*) from ActivateEmployeePostDatePayRate a where (a.effectiveDate LIKE :searchKeyWord) and a.isDeleted=false")
	Integer predictiveActivatePayRateSearchTotalCount(@Param("searchKeyWord")  String searchKeyWord);

	@Query("select a from ActivateEmployeePostDatePayRate a where a.isDeleted=false")
	List<ActivateEmployeePostDatePayRate> predictiveActivatePayRateSearchWithPagination(Pageable pageable);
	
	
	@Query("select count(*) from ActivateEmployeePostDatePayRate d ")
	public Integer getCountOfTotalActivateEmployeePostDatePayRate();

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ActivateEmployeePostDatePayRate a set a.isDeleted =:deleted ,a.updatedBy =:updateById where a.employeeMaster.employeeIndexId =:id ")
	public void deleteByEmployeeId(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId,@Param("id")Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ActivateEmployeePostDatePayRate a set a.isDeleted =:deleted ,a.updatedBy =:updateById where a.payCode.id =:id ")
	public void deleteByPayCodeId(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId,@Param("id")Integer id);

}
