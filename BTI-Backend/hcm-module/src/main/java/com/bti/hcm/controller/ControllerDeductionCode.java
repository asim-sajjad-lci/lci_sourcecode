package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoDeductionCode;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceDeductionCode;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/deductionCode")
public class ControllerDeductionCode extends BaseController {
	/**
	 * @Description LOGGER use for put a logger in DeductionCode Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerDeductionCode.class);

	/**
	 * @Description serviceDeductionCode Autowired here using annotation of spring
	 *              for use of serviceDeductionCode method in this controller
	 */
	@Autowired(required = true)
	ServiceDeductionCode serviceDeductionCode;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @param                  request{
	 * 
	 *                         "diductionId" : "1", "discription" : "test ",
	 *                         "arbicDiscription" : "test", "transction" : true,
	 *                         "method" : 0, "amount" : 0
	 * 
	 *                         }
	 * @param dtoDeductionCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoDeductionCode dtoDeductionCode)
			throws Exception {
		LOGGER.info("Create Department Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDeductionCode = serviceDeductionCode.saveOrUpdate(dtoDeductionCode);
			responseMessage = displayMessage(dtoDeductionCode, "DEDUCTION_CODE_CREATED", "DEDUCTION_CODE_NOT_CREATED",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create DEDUCTION_CODE Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param                  request{ "id" : 1, "diductionId" : "1", "discription"
	 *                         : "test ", "arbicDiscription" : "test", "transction"
	 *                         : true, "method" : 0, "amount" : 0
	 * 
	 *                         }
	 * @param dtoDeductionCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoDeductionCode dtoDeductionCode)
			throws Exception {
		LOGGER.info("Update DeductionCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDeductionCode = serviceDeductionCode.saveOrUpdate(dtoDeductionCode);
			responseMessage = displayMessage(dtoDeductionCode, MessageConstant.DEDUCTION_CODE_UPDATED_SUCCESS,
					MessageConstant.DEDUCTION_CODE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update DeductionCode Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param               request{
	 * 
	 *                      "ids":[1] }
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoDeductionCode dtoDeductionCode)
			throws Exception {
		LOGGER.info("Delete DeductionCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoDeductionCode.getIds() != null && !dtoDeductionCode.getIds().isEmpty()) {
				DtoDeductionCode dtoDepartment2 = serviceDeductionCode.delete(dtoDeductionCode.getIds());

				if (dtoDepartment2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("DEDUCTION_CODE_DELETED", false),
							dtoDepartment2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, serviceResponse
							.getMessageByShortAndIsDeleted("DEDUCTION_CODE_NOT_DELETE_ID_ALREADY_IN", false),
							dtoDepartment2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete DeductionCode Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param         dtoSearch{
	 * 
	 *                "searchKeyword":"t", "pageNumber" : 0, "pageSize" :2 }
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search DeductionCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDeductionCode.search(dtoSearch);
			responseMessage = displayMessage(dtoSearch, MessageConstant.DEDUCTION_CODE_GET_ALL,
					MessageConstant.DEDUCTION_CODE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	/**
	 * @param               request{
	 * 
	 *                      "pageNumber": "0", "pageSize": "10" }
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody DtoDeductionCode dtoDepartment)
			throws Exception {
		LOGGER.info("Get All DeductionCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceDeductionCode.getAllDeductionCode(dtoDepartment);
			responseMessage = displayMessage(dtoSearch, "DEDUCTION_CODE_GET_ALL", "DEDUCTION_CODE_LIST_NOT_GETTING",
					serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All DeductionCode Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param               request{ "id" : 1 }
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getDeductionCodeById", method = RequestMethod.POST)
	public ResponseMessage getDeductionCodeById(HttpServletRequest request,
			@RequestBody DtoDeductionCode dtoDeductionCode) throws Exception {
		LOGGER.info("Get DeductionCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoDeductionCode dtoDeductionCodeObj = serviceDeductionCode.getById(dtoDeductionCode.getId());
			responseMessage = displayMessage(dtoDeductionCodeObj, "DEDUCTION_CODE_GET_DETAIL",
					"DEDUCTION_CODE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get DeductionCode ById Method:" + dtoDeductionCode.getId());
		return responseMessage;
	}

	/**
	 * @param               request{ "diductionId" : "1" }
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deductionIdcheck", method = RequestMethod.POST)
	public ResponseMessage deductionIdcheck(HttpServletRequest request, @RequestBody DtoDeductionCode dtoDeductionCode)
			throws Exception {
		LOGGER.info("DeductionCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoDeductionCode dtoDepartmentObj = serviceDeductionCode
					.repeatByDeductionCodeId(dtoDeductionCode.getDiductionId());
			responseMessage = displayMessage(dtoDepartmentObj, "DEDUCTION_CODE_RESULT",
					"DEDUCTION_CODE_REPEAT_DEDUCTION_CODEID_NOT_FOUND", serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllDeductionDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllDeductionDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoDeductionCode> deductionList = serviceDeductionCode.getAllDtoDeductionCodeInActiveList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEDUCTION_CODE_GET_DETAIL", false),
						deductionList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return responseMessage;
	}

	@RequestMapping(value = "/searchdiductionId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchdiductionId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("searchdiductionId Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDeductionCode.searchdiductionId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, MessageConstant.DEDUCTION_CODE_GET_ALL,
					MessageConstant.DEDUCTION_CODE_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/findCodeByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage findCodeByEmployeeId(@RequestBody DtoPayCode dtoPayCode, HttpServletRequest request)
			throws Exception {
		LOGGER.info("Search EmployeePayCodeMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayCode = this.serviceDeductionCode.deducationCodeByPaycode(dtoPayCode);
			responseMessage = displayMessage(dtoPayCode, "EMPLOYEE_PAY_CODET_MAINTENENCE_GET_ALL",
					"EMPLOYEE_PAY_CODET_MAINTENENCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if (dtoPayCode != null) {
			LOGGER.debug("Search EmployeePayCodeMaintenance Method:" + dtoPayCode.getTotalCount());
		}

		return responseMessage;
	}

}
