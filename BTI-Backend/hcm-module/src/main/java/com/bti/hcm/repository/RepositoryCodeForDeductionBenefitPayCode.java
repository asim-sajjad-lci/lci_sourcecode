package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.CodeForDeductionBenefitPayCode;

@Repository("repositoryCodeForDeductionBenefitPayCode")
public interface RepositoryCodeForDeductionBenefitPayCode extends JpaRepository<CodeForDeductionBenefitPayCode, Integer>{

	@Query("select count(*) from CodeForDeductionBenefitPayCode c where (c.desc like :searchKeyword) and c.isDeleted=false")
	Integer predictiveSearchTotalCount(@Param("searchKeyword")String searchKeyword);

	@Query("select c from CodeForDeductionBenefitPayCode c where (c.desc like :searchKeyword) and c.isDeleted=false")
	public List<CodeForDeductionBenefitPayCode> predictiveCodeForDeductionBenefitPayCodeSearchWithPaginations(@Param("searchKeyword")String searchKeyword,Pageable pageable);

	List<CodeForDeductionBenefitPayCode> findByIsDeleted(boolean b);
}
