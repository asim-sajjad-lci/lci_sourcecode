package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00203",indexes = { @Index(columnList = "EMPPINDX")})
public class EmployeePayCodeMaintenance extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPPINDX")
	private Integer id;

	@Column(name = "PYCDBASCD", columnDefinition = "char(15)")
	private String baseOnPayCode;

	@Column(name = "PYCDBASCDVAL", precision = 13, scale = 3)
	private BigDecimal amount;

	@Column(name = "PYCDFACTR", precision = 13, scale = 3)
	private BigDecimal payFactory;

	@Column(name = "PYCDRATE", precision = 13, scale = 3)
	private BigDecimal payRate;

	@Column(name = "PYCDUNTPY")
	private Short unitOfPay;

	@Column(name = "PYCDPERD")
	private Short payPeriod;

	@Column(name = "PYCDINCTV")
	private Boolean inactive;

	@Column(name = "BASEONPAYCODEID")
	private Integer baseOnPayCodeId;

	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private EmployeeMaster employeeMaster;

	@ManyToOne
	@JoinColumn(name = "PYCDINDX")
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	private PayCode payCode;

	@ManyToOne
	@JoinColumn(name = "PYCDTYPINDX")
	private PayCodeType payCodeType;

	@Column(name = "is_deleted")		//ME added 16Jul19
	private Boolean isDeleted;
	
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBaseOnPayCode() {
		return baseOnPayCode;
	}

	public void setBaseOnPayCode(String baseOnPayCode) {
		this.baseOnPayCode = baseOnPayCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPayFactory() {
		return payFactory;
	}

	public void setPayFactory(BigDecimal payFactory) {
		this.payFactory = payFactory;
	}

	public BigDecimal getPayRate() {
		return payRate;
	}

	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}

	public Short getUnitOfPay() {
		return unitOfPay;
	}

	public void setUnitOfPay(Short unitOfPay) {
		this.unitOfPay = unitOfPay;
	}

	public Short getPayPeriod() {
		return payPeriod;
	}

	public void setPayPeriod(Short payPeriod) {
		this.payPeriod = payPeriod;
	}

	public Boolean getInactive() {
		return inactive;
	}

	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public PayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(PayCode payCode) {
		this.payCode = payCode;
	}

	public PayCodeType getPayCodeType() {
		return payCodeType;
	}

	public void setPayCodeType(PayCodeType payCodeType) {
		this.payCodeType = payCodeType;
	}

	public Integer getBaseOnPayCodeId() {
		return baseOnPayCodeId;
	}

	public void setBaseOnPayCodeId(Integer baseOnPayCodeId) {
		this.baseOnPayCodeId = baseOnPayCodeId;
	}
	
}
