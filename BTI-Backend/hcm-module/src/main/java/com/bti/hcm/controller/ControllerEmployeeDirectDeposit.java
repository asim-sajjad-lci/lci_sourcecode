package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeDirectDeposit;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeDirectDeposit;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeDirectDeposit")
public class ControllerEmployeeDirectDeposit extends BaseController{
	
	@Autowired
	ServiceEmployeeDirectDeposit serviceEmployeeDirectDeposit;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeDirectDeposit.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	

	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit) throws Exception{
		LOGGER.info("Create Direct Deposit Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeDirectDeposit = serviceEmployeeDirectDeposit.saveOrUpdate(dtoEmployeeDirectDeposit);
			responseMessage=displayMessage(dtoEmployeeDirectDeposit, "EMPLOYEE_DIRECT_DEPOSIT_CREATED", "EMPLOYEE_DIRECT_DEPOSIT_NOT_CREATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create Direct Deposit Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit) throws Exception {
		LOGGER.info("Update Direct Deposit Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeDirectDeposit = serviceEmployeeDirectDeposit.saveOrUpdate(dtoEmployeeDirectDeposit);
			responseMessage=displayMessage(dtoEmployeeDirectDeposit, "EMPLOYEE_DIRECT_DEPOSIT_UPDATED_SUCCESS", "EMPLOYEE_DIRECT_DEPOSIT_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update Direct Deposit Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit) throws Exception {
		LOGGER.info("Delete DirectDeposit Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeDirectDeposit.getIds() != null && !dtoEmployeeDirectDeposit.getIds().isEmpty()) {
				DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit2 = serviceEmployeeDirectDeposit.delete(dtoEmployeeDirectDeposit.getIds());
				if(dtoEmployeeDirectDeposit2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("EMPLOYEE_DIRECT_DEPOSIT_DELETED", false), dtoEmployeeDirectDeposit2);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("EMPLOYEE_DIRECT_DEPOSIT_NOT_DELETE_ID_MESSAGE", false), dtoEmployeeDirectDeposit2);

				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete DirectDeposit Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit) throws Exception {
		LOGGER.info("Get DirectDeposit ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit2 = serviceEmployeeDirectDeposit.getById(dtoEmployeeDirectDeposit.getId());
			responseMessage=displayMessage(dtoEmployeeDirectDeposit2, "EMPLOYEE_DIRECT_DEPOSIT_GET_DETAIL", "EMPLOYEE_DIRECT_DEPOSIT_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get DirectDeposit by Id Method:"+dtoEmployeeDirectDeposit.getId());
		return responseMessage;
	}
		
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
    public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
	LOGGER.info("Search DirectDeposit Method");
	ResponseMessage responseMessage = null;
	boolean flag =serviceHcmHome.checkValidCompanyAccess();
	if (flag) {
		dtoSearch = this.serviceEmployeeDirectDeposit.search(dtoSearch);
		responseMessage=displayMessage(dtoSearch, "EMPLOYEE_DIRECT_DEPOSIT_GET_DETAIL", "EMPLOYEE_DIRECT_DEPOSIT_NOT_GETTING", response);
	} else {
		responseMessage = unauthorizedMsg(response);
	}
	return responseMessage;
}
	
	@RequestMapping(value = "/employeeIdcheck", method = RequestMethod.POST)
	public ResponseMessage employeeIdcheck(HttpServletRequest request, @RequestBody DtoEmployeeDirectDeposit dtoEmployeeDirectDeposit) throws Exception {
		LOGGER.info("employeeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeDirectDeposit dtoEmployeeDirectDepositObj = serviceEmployeeDirectDeposit.repeatByEmployeeIdcheckId(dtoEmployeeDirectDeposit.getId());
			responseMessage=displayMessage(dtoEmployeeDirectDepositObj, "EMPLOYEE_RESULT", "EMPLOYEE_REPEAT_EMPLOYEEID_NOT_FOUND", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}
	
}
