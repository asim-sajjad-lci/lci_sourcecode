package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoAccrualPeriod;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceAccrualPeriod;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/accrualPeriod")
public class ControllerAccrualPeriod extends BaseController{
	private  Logger log = Logger.getLogger(ControllerAccrualPeriod.class);
	
	@Autowired
	ServiceAccrualPeriod serviceAccrualPeriod;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createAccrualPeriod(HttpServletRequest request, @RequestBody DtoAccrualPeriod dtoAccrualPeriod) throws Exception {
		log.info("Create AccrualPeriod Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrualPeriod  = serviceAccrualPeriod.saveOrUpdate(dtoAccrualPeriod);
			responseMessage=displayMessage(dtoAccrualPeriod, "ACCRUAL_PERIOED_CREATED", "ACCRUAL_PERIOED_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create AccrualPeriod Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search AccrualPeriod Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccrualPeriod.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.ACCRUAL_PERIOED_GET_ALL, MessageConstant.ACCRUAL_PERIOED_LIST_NOT_GETTING, serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoAccrualPeriod dtoAccrualPeriod) throws Exception {
		log.info("Update AccrualPeriod Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrualPeriod = serviceAccrualPeriod.saveOrUpdate(dtoAccrualPeriod);
			responseMessage=displayMessage(dtoAccrualPeriod, "ACCRUAL_PERIOED_UPDATED", "ACCRUAL_PERIOED_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update AccrualPeriod Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoAccrualPeriod dtoAccrualPeriod) throws Exception {
		log.info("Delete AccrualPeriod Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoAccrualPeriod.getIds() != null && !dtoAccrualPeriod.getIds().isEmpty()) {
				DtoAccrualPeriod dtoAccrual2 = serviceAccrualPeriod.delete(dtoAccrualPeriod.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ACCRUAL_PERIOED_DELETED", false), dtoAccrual2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete AccrualPeriod Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getaccuralId(HttpServletRequest request, @RequestBody DtoAccrualPeriod dtoAccrualPeriod) throws Exception {
		log.info("Get AccrualPeriod ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAccrualPeriod dtoAccrualObj = serviceAccrualPeriod.getByAccrualPeriodId(dtoAccrualPeriod.getId());
			responseMessage=displayMessage(dtoAccrualObj, MessageConstant.ACCRUAL_PERIOED_GET_ALL, MessageConstant.ACCRUAL_PERIOED_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get AccrualPeriod by Id Method:"+dtoAccrualPeriod.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllAccrualPeriod", method = RequestMethod.GET)
	public ResponseMessage getAllAccurals(HttpServletRequest request) throws Exception {
		DtoSearch dtoSearch = null;
		log.info("Get getAllAccrualPeriod Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccrualPeriod.getAll();
			responseMessage=displayMessage(dtoSearch, MessageConstant.ACCRUAL_PERIOED_GET_ALL, MessageConstant.ACCRUAL_PERIOED_NOT_GETTING, serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
