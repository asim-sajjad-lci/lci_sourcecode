package com.bti.hcm.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.chrono.HijrahChronology;
import java.time.chrono.HijrahDate;
import java.time.chrono.HijrahEra;
import java.time.chrono.IsoChronology;
import java.util.Calendar;
import java.util.Date;

public class DateConversion {
	public static void main(String[] args) throws ParseException {
	
		String startDate = DateConversion.dateConverts("Tue Jan 01 00:00:00 IST 2019");
		Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
	
		System.out.println(date1);
	
	}
	
	

	public static HijrahDate convertGregorianToHijriDate(int dateOfMonth,int month,int year) throws ParseException {
		
		
		String expectedPattern = "MM/dd/yyyy";
	    SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
		
		   String userInput = month+"/"+dateOfMonth+"/"+year;
		   Date date=formatter.parse(userInput);
		   Calendar cl=Calendar.getInstance();
		cl.setTime(date);
		
		HijrahDate islamyDate=HijrahChronology.INSTANCE.date(LocalDate.of(cl.get(Calendar.YEAR),cl.get(Calendar.MONTH)+1, cl.get(Calendar.DATE)));
		System.out.println("HijrahDate is:---->"+islamyDate);
		return islamyDate;
		
	}
	
	public static LocalDate convertHijriDateToGregorian(int prolepticYear,int month,int dayOfMonth) throws ParseException {
		  HijrahDate date = HijrahChronology.INSTANCE.date(prolepticYear, month, dayOfMonth);
		LocalDate localDate = IsoChronology.INSTANCE.date(date);
			System.out.println("LocalDate is:-->"+localDate);
		return localDate;
		
	}
	public HijrahEra eraOf(int eraValue) {
		return null;
		
	}
	
	public static String dateConverts(String dateStr) throws ParseException {
		
	//	String dateStr = "Mon Jun 18 00:00:00 IST 2012";
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		Date date = (Date)formatter.parse(dateStr);
		System.out.println(date);        

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String formatedDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" +         cal.get(Calendar.DATE);
		System.out.println("formatedDate : " + formatedDate);
		return formatedDate;    
	}
	
}
