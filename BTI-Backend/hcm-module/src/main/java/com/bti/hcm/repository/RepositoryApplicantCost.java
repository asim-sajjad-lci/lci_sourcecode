package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ApplicantCost;

@Repository("repositoryApplicantCost")
public interface RepositoryApplicantCost extends JpaRepository<ApplicantCost, Integer>{

	ApplicantCost findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ApplicantCost d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleApplicantCost(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);
		
	List<ApplicantCost> findByIsDeleted(boolean isDeleted,Pageable pageRequest);
	
	List<ApplicantCost> findByIsDeleted(boolean isDeleted);
	
	ApplicantCost findById(Integer id);

	@Query("select count(*) from ApplicantCost a ")
	Integer getCountOfTotalApplicantCost();
}
