package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.SkillSetDesc;
import com.bti.hcm.model.SkillSetSetup;
import com.bti.hcm.model.SkillsSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSkillSetDesc;
import com.bti.hcm.model.dto.DtoSkillSetSteup;
import com.bti.hcm.model.dto.DtoSkillSteup;
import com.bti.hcm.repository.RepositorySkillSetDesc;
import com.bti.hcm.repository.RepositorySkillSetSetup;
import com.bti.hcm.repository.RepositorySkillSteps;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceSkillSetSteup")
public class ServiceSkillSetSteup {
	/**
	 * @Description LOGGER use for put a logger in SkillSetSteup Service
	 */
	static Logger log = Logger.getLogger(ServiceSkillSetSteup.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in SkillSetSteup service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in SkillSetSteup service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in SkillSetSteup service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description RepositorySkillSetSetup Autowired here using annotation of
	 *              spring for access of RepositorySkillSetSetup method in
	 *              SkillSetSetup service In short Access SkillSetSetup Query from
	 *              Database using RepositorySkillSetSetup.
	 */
	@Autowired
	RepositorySkillSetSetup repositorySkillSetSetup;

	@Autowired
	RepositorySkillSetDesc repositorySkillSetDesc;

	@Autowired
	RepositorySkillSteps repositorySkillSteps;

	/**
	 * @param dtoSkillSetSteup
	 * @return
	 */
	public DtoSkillSetSteup saveOrUpdateSkillSteup(DtoSkillSetSteup dtoSkillSetSteup) {
		log.info("saveOrUpdateSkillSteup Method");
		SkillSetSetup skillSetSetup = null;
		SkillSetDesc skillSetDesc = null;
		List<SkillsSetup> skillsSetup = new ArrayList<>();
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			if (dtoSkillSetSteup.getId() != null && dtoSkillSetSteup.getId() > 0) {
				skillSetSetup = repositorySkillSetSetup.findByIdAndIsDeleted(dtoSkillSetSteup.getId(), false);
				skillSetSetup.setUpdatedBy(loggedInUserId);
				skillSetSetup.setUpdatedDate(new Date());
				skillSetDesc = repositorySkillSetDesc.findByIdAndIsDeleted(dtoSkillSetSteup.getSkillSetDescId(), false);
				skillSetDesc.setUpdatedBy(loggedInUserId);
				skillSetDesc.setUpdatedDate(new Date());

			} else {
				skillSetSetup = new SkillSetSetup();
				skillSetDesc = new SkillSetDesc();
				skillSetSetup.setCreatedDate(new Date());
				skillSetDesc.setCreatedDate(new Date());
			}

			skillSetSetup.setSkillSetId(dtoSkillSetSteup.getSkillSetId());
			skillSetSetup.setSkillSetDiscription(dtoSkillSetSteup.getSkillSetDiscription());
			skillSetSetup.setArabicDiscription(dtoSkillSetSteup.getArabicDiscription());
			skillSetSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			skillSetDesc.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			skillSetSetup.setRowId(loggedInUserId);

			for (DtoSkillSteup dtoSkillSteup : dtoSkillSetSteup.getListSkillSteup()) {
				SkillsSetup setup = repositorySkillSteps.findOne(dtoSkillSteup.getId());
				skillsSetup.add(setup);
			}

			skillSetDesc.setSkillsSetup(skillsSetup);
			skillSetDesc.setRowId(loggedInUserId);
			skillSetDesc.setComment(dtoSkillSetSteup.getComment());
			skillSetDesc.setSkillRequired(dtoSkillSetSteup.isSkillRequired());
			skillSetDesc.setSkillSetSeqn(dtoSkillSetSteup.getSkillSetSeqn());
			skillSetDesc.setSkillSetSetup(skillSetSetup);
			repositorySkillSetSetup.saveAndFlush(skillSetSetup);
			repositorySkillSetDesc.saveAndFlush(skillSetDesc);
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSkillSetSteup;
	}

	/**
	 * @param dtoSkillSetSteup
	 * @return
	 */
	public DtoSearch getAllSkillSteup(DtoSkillSetSteup dtoSkillSetSteup) {
		log.info("getAllSkillSteup Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSkillSetSteup.getPageNumber());
		dtoSearch.setPageSize(dtoSkillSetSteup.getPageSize());
		dtoSearch.setTotalCount(repositorySkillSetSetup.getCountOfTotalSkillSetSetup());
		List<SkillSetSetup> skillSteupList = null;
		if (dtoSkillSetSteup.getPageNumber() != null && dtoSkillSetSteup.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSkillSetSteup.getPageNumber(), dtoSkillSetSteup.getPageSize(),
					Direction.DESC, "createdDate");
			skillSteupList = repositorySkillSetSetup.findByIsDeleted(false, pageable);
		} else {
			skillSteupList = repositorySkillSetSetup.findByIsDeletedOrderByCreatedDateDesc(false);
		}

		List<DtoSkillSetSteup> dtoSkillSteupList = new ArrayList<>();
		if (skillSteupList != null && !skillSteupList.isEmpty()) {
			for (SkillSetSetup skillsSetup : skillSteupList) {

				dtoSkillSetSteup = new DtoSkillSetSteup(skillsSetup);
				dtoSkillSetSteup.setSkillSetId(skillsSetup.getSkillSetId());
				dtoSkillSetSteup.setSkillSetDiscription(skillsSetup.getSkillSetDiscription());
				dtoSkillSetSteup.setArabicDiscription(skillsSetup.getArabicDiscription());
				dtoSkillSteupList.add(dtoSkillSetSteup);
			}
			dtoSearch.setRecords(dtoSkillSteupList);
		}
		log.debug("All Skill Set Steup List Size is:" + dtoSearch.getTotalCount());
		return dtoSearch;
	}

	/**
	 * @param ids
	 * @return
	 */
	public DtoSkillSetSteup deleteSkillSteup(List<Integer> ids) {
		log.info("deleteSkillSetSteup Method");
		DtoSkillSetSteup dtoSkillSteup = new DtoSkillSetSteup();
		dtoSkillSteup.setDeleteMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SKILL_SET_STEUP_DELETED", false));
		dtoSkillSteup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SKILL_SET_STEUP_DELETED", false));
		List<DtoSkillSetSteup> delete = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		// "Position is already in use,you are not able to Delete id : "
		invlidDeleteMessage.append(serviceResponse
				.getMessageByShortAndIsDeleted("SKILL_SET_SETUP_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer skillStepId : ids) {
				SkillSetSetup skillsteup = repositorySkillSetSetup.findOne(skillStepId);

				if (skillsteup.getListSkillSetDesc().isEmpty() && skillsteup.getListPosition().isEmpty()) {
					DtoSkillSetSteup dtoskillSetup2 = new DtoSkillSetSteup();
					dtoskillSetup2.setId(skillStepId);
					dtoskillSetup2.setId(skillsteup.getId());
					dtoskillSetup2.setSkillSetId(skillsteup.getSkillSetId());
					dtoskillSetup2.setSkillSetDiscription(skillsteup.getSkillSetDiscription());
					dtoskillSetup2.setArabicDiscription(skillsteup.getArabicDiscription());

					repositorySkillSetSetup.deleteSingleSkillSetSteup(true, loggedInUserId, skillStepId);
					delete.add(dtoskillSetup2);
				} else {
					inValidDelete = true;
					invlidDeleteMessage.append(skillsteup.getSkillSetId() + ",");
				}

				if (inValidDelete) {
					invlidDeleteMessage.replace(invlidDeleteMessage.length() - 1, invlidDeleteMessage.length(), "");
					dtoSkillSteup.setMessageType(invlidDeleteMessage.toString());

				}
				if (!inValidDelete) {
					dtoSkillSteup.setMessageType("");

				}

			}
			dtoSkillSteup.setDeleteSkillSet(delete);
		} catch (NumberFormatException e) {

			log.error("Error in deleteSkillSteup", e);
		}
		log.debug("Delete SkillSetSteup :" + dtoSkillSteup.getId());
		return dtoSkillSteup;
	}

	/**
	 * @param id
	 * @return
	 */
	public DtoSkillSetSteup getById(Integer id) {
		log.info("getBySkillSteupId Method");
		DtoSkillSetSteup dtoSkillSetSteup = new DtoSkillSetSteup();
		if (id > 0) {
			SkillSetSetup skillSetSetup = repositorySkillSetSetup.findByIdAndIsDeleted(id, false);
			if (skillSetSetup != null) {
				dtoSkillSetSteup = new DtoSkillSetSteup(skillSetSetup);

				dtoSkillSetSteup.setId(skillSetSetup.getId());
				dtoSkillSetSteup.setSkillSetId(skillSetSetup.getSkillSetId());
				dtoSkillSetSteup.setSkillSetDiscription(skillSetSetup.getSkillSetDiscription());
				dtoSkillSetSteup.setArabicDiscription(skillSetSetup.getArabicDiscription()); // ME

				List<SkillSetDesc> descs = skillSetSetup.getListSkillSetDesc();

				for (SkillSetDesc skillSetDesc : descs) {
					if (skillSetDesc.getSkillsId() != null) {
						dtoSkillSetSteup.setSkillId(skillSetDesc.getSkillsId().getId());
						dtoSkillSetSteup.setSkillSetIndexId(skillSetDesc.getSkillsId().getId());
						dtoSkillSetSteup.setSkillSetSeqn(skillSetDesc.getSkillSetSeqn());
						dtoSkillSetSteup.setSkillIdParam(skillSetDesc.getSkillsId().getSkillId());

					}

					dtoSkillSetSteup.setSkillRequired(skillSetDesc.isSkillRequired());

					dtoSkillSetSteup.setSkillSetDescId(skillSetDesc.getId());
					dtoSkillSetSteup.setComment(skillSetDesc.getComment());

				}

				dtoSkillSetSteup.setArabicDiscription(skillSetSetup.getArabicDiscription());
			} else {
				dtoSkillSetSteup.setMessageType("SKILL_STEUP_NOT_GETTING");

			}
		} else {
			dtoSkillSetSteup.setMessageType("SKILL_STEUP_NOT_GETTING");

		}
		log.debug("SkillSetSteup By Id is:" + dtoSkillSetSteup.getId());
		return dtoSkillSetSteup;
	}

	/**
	 * @param skillId
	 * @return
	 */
	public DtoSkillSetSteup repeatBySkillSetSteupId(String skillId) {

		log.info("repeatBySkillSetSteupId Method");
		DtoSkillSetSteup dtoSkillsteup = new DtoSkillSetSteup();
		try {
			List<SkillSetSetup> skillsteup = repositorySkillSetSetup.findByskillSetId(skillId.trim());
			if (skillsteup != null && !skillsteup.isEmpty()) {
				dtoSkillsteup.setIsRepeat(true);
			} else {
				dtoSkillsteup.setIsRepeat(false);
			}

		} catch (Exception e) {
			log.error("Error in repeatBySkillSetSteupId", e);
		}

		return dtoSkillsteup;
	}

	public DtoSearch searchSkillSetSteup(DtoSearch dtoSearch) {
		log.info("searchSkillSetup Method");
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();

			String condition = "";
			if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

				if (dtoSearch.getSortOn().equals("desc") || dtoSearch.getSortOn().equals("skillSetId")
						|| dtoSearch.getSortOn().equals("skillSetDiscription")
						|| dtoSearch.getSortOn().equals("arabicDiscription")) {
					condition = dtoSearch.getSortOn();
				} else {
					condition = "id";
				}

			} else {
				condition += "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}

			dtoSearch.setTotalCount(
					this.repositorySkillSetSetup.predictiveSkillSetSteupSearchTotalCount("%" + searchWord + "%"));
			List<SkillSetSetup> skillSteupList = null;
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

				if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
					skillSteupList = this.repositorySkillSetSetup.predictiveSkillsSetSetupSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, "id"));
				}
				if (dtoSearch.getSortBy().equals("ASC")) {
					skillSteupList = this.repositorySkillSetSetup.predictiveSkillsSetSetupSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.ASC, condition));
				} else if (dtoSearch.getSortBy().equals("DESC")) {
					skillSteupList = this.repositorySkillSetSetup.predictiveSkillsSetSetupSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, condition));
				}

			}

			if (skillSteupList != null && !skillSteupList.isEmpty()) {

				List<DtoSkillSetSteup> dtoSkillSteupList = new ArrayList<>();

				DtoSkillSetSteup dtoSkillSetSteup = null;
				int i = 0; // Test
				for (SkillSetSetup skillSetSetup : skillSteupList) {
					List<DtoSkillSteup> dtoSkillList = new ArrayList<>();
					dtoSkillSetSteup = new DtoSkillSetSteup(skillSetSetup);
					dtoSkillSetSteup.setId(skillSetSetup.getId());
					dtoSkillSetSteup.setSkillSetId(skillSetSetup.getSkillSetId());
					dtoSkillSetSteup.setSkillSetDiscription(skillSetSetup.getSkillSetDiscription());
					dtoSkillSetSteup.setArabicDiscription(skillSetSetup.getArabicDiscription()); //
					// List<SkillSetDesc> descs1 = skillSetSetup.getListSkillSetDesc(); //need to be
					// changed
					// SkillSetDesc descs = new SkillSetDesc();

					// List<SkillSetDesc> descs =
					// repositorySkillSetDesc.findBySkillSetSetupId(skillSetSetup.getId());

					Integer a = repositorySkillSetDesc.findBySkillSetSetupIdTest1(skillSetSetup.getId());
					log.info("HFTest1:" + a);
					List<SkillSetDesc> descs = repositorySkillSetDesc.findBySkillSetSetupIdTest2(a);

//					if (descs == null)
//						log.info("descs == null");
//					if (descs.equals(null))
//						log.info("descs eq null");
//					if (descs.size() == 0)
//						log.info("descs.size = 0");

					log.info("descs.size():" + descs.size());

					// log.info("HFTest0:" + descs.get(0).getId());
//					if (i >= 0 && i < descs.size())
//						log.info("HFTest2:" + descs.get(i).getId());
//					else
//						log.info("List Size Err!");

					i++; // Test
					String skillId = "";

					if (descs != null && !descs.equals(null)) {
						for (SkillSetDesc skillSetDesc : descs) {
							if (skillSetDesc.getSkillsId() != null) {
								dtoSkillSetSteup.setSkillId(skillSetDesc.getSkillsId().getId());
								dtoSkillSetSteup.setSkillSetIndexId(skillSetDesc.getSkillsId().getId());
								dtoSkillSetSteup.setSkillIdParam(skillSetDesc.getSkillsId().getSkillId());
							}
							dtoSkillSetSteup.setSkillSetSeqn(skillSetDesc.getSkillSetSeqn());

							if (skillSetDesc.getSkillsSetup() != null) {

								for (SkillsSetup skillsSetup : skillSetDesc.getSkillsSetup()) {

									skillId += skillsSetup.getSkillId() + ",";

									DtoSkillSteup dtoSkillSteup = new DtoSkillSteup();
									dtoSkillSteup.setId(skillsSetup.getId());
									dtoSkillSteup
											.setSkillId(skillsSetup.getSkillId() + "  " + skillsSetup.getSkillDesc());
									dtoSkillSteup.setSkillDesc(skillsSetup.getSkillDesc());
									dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
									dtoSkillList.add(dtoSkillSteup);

								}
							}
							dtoSkillSetSteup.setListSkillSteup(dtoSkillList);
							dtoSkillSetSteup.setSkillsIds(skillId);
							dtoSkillSetSteup.setSkillRequired(skillSetDesc.isSkillRequired());

							dtoSkillSetSteup.setSkillSetDescId(skillSetDesc.getId());
							dtoSkillSetSteup.setComment(skillSetDesc.getComment());
							dtoSkillSetSteup.setSequence(skillSetDesc.getSkillSetSeqn());

						}
						dtoSkillSteupList.add(dtoSkillSetSteup);
					}
				}
				dtoSearch.setRecords(dtoSkillSteupList);
			}
		}
		if (dtoSearch != null) {
			log.debug("Search SkillSetSteup Size is:" + dtoSearch.getTotalCount());
		}

		return dtoSearch;
	}

	public DtoSearch searchSkillSetId(DtoSearch dtoSearch) {
		log.info("searchSkillSetId Method");
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();

			List<SkillSetSetup> skillSetIdList = null;
			List<DtoSkillSetSteup> listSkillSetIdList = new ArrayList<>();

			skillSetIdList = this.repositorySkillSetSetup
					.predictiveSkillSetIdSearchWithPagination("%" + searchWord + "%");

			for (SkillSetSetup skillSetSetup : skillSetIdList) {
				DtoSkillSetSteup dtoSkillSetSteup = new DtoSkillSetSteup();
				dtoSkillSetSteup.setId(skillSetSetup.getId());
				dtoSkillSetSteup.setSkillSetDiscription(skillSetSetup.getSkillSetDiscription());
				listSkillSetIdList.add(dtoSkillSetSteup);

			}
			dtoSearch.setRecords(listSkillSetIdList);
		}

		return dtoSearch;
	}

	public DtoSearch getIds(DtoSearch dtoSearch) {
		if (dtoSearch != null) {

			List<SkillSetSetup> skillSetIdList = null;
			List<DtoSkillSetSteup> dtoSkillSetSteupList = new ArrayList<>();
			skillSetIdList = this.repositorySkillSetSetup.findAll();

			for (SkillSetSetup skillSetSetup : skillSetIdList) {
				DtoSkillSetSteup dtoSkillSetSteup = new DtoSkillSetSteup();
				dtoSkillSetSteup.setId(skillSetSetup.getId());
				dtoSkillSetSteup.setSkillSetId(skillSetSetup.getSkillSetId());
				dtoSkillSetSteupList.add(dtoSkillSetSteup);
			}
			dtoSearch.setRecords(dtoSkillSetSteupList);
		}

		return dtoSearch;
	}

	public DtoSearch searchSkillSetSetupId(DtoSearch dtoSearch) {
		log.info("searchSkillSetSetupId Method");
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();

			List<SkillSetSetup> skillSetSetupList = null;

			skillSetSetupList = this.repositorySkillSetSetup
					.predictivesearchSkillSetSetupIdWithPagination("%" + searchWord + "%");
			if (!skillSetSetupList.isEmpty()) {

				if (!skillSetSetupList.isEmpty()) {
					List<DtoSkillSetSteup> dtoSkillSetSteup = new ArrayList<>();
					DtoSkillSetSteup dtoSkillSetSteup1 = null;
					for (SkillSetSetup skillSetSetup : skillSetSetupList) {
						dtoSkillSetSteup1 = new DtoSkillSetSteup(skillSetSetup);

						dtoSkillSetSteup1.setId(skillSetSetup.getId());
						dtoSkillSetSteup1.setSkillSetDiscription(
								skillSetSetup.getSkillSetId() + " | " + skillSetSetup.getSkillSetDiscription());

						dtoSkillSetSteup.add(dtoSkillSetSteup1);
					}
					dtoSearch.setRecords(dtoSkillSetSteup);
				}

				dtoSearch.setTotalCount(skillSetSetupList.size());
			}

		}

		return dtoSearch;
	}

	public DtoSearch findById(DtoSearch dtoSearch) {
		log.info("searchSkillSetSetupId Method");
		if (dtoSearch != null) {
			SkillSetSetup skillSetSetupList = this.repositorySkillSetSetup.findByIdAndIsDeleted(dtoSearch.getId(),
					false);
			if (skillSetSetupList != null) {
				List<DtoSkillSetDesc> dtoSkillSetSteup = new ArrayList<>();
				DtoSkillSetSteup dtoSkillSetSteup1 = null;

				dtoSkillSetSteup1 = new DtoSkillSetSteup(skillSetSetupList);

				dtoSkillSetSteup1.setId(skillSetSetupList.getId());
				dtoSkillSetSteup1.setSkillSetDiscription(skillSetSetupList.getSkillSetId());
				for (SkillSetDesc dtoSkillSetSteup2 : skillSetSetupList.getListSkillSetDesc()) {
					DtoSkillSetDesc dtoSkillSetDesc = new DtoSkillSetDesc();
					dtoSkillSetDesc.setId(dtoSkillSetSteup2.getId());

					if (dtoSkillSetSteup2.getSkillsId() != null) {
						DtoSkillSteup dtoSkillSteup = new DtoSkillSteup();
						SkillsSetup setup = dtoSkillSetSteup2.getSkillsId();

						dtoSkillSteup.setFrequency(setup.getFrequency());
						dtoSkillSteup.setCompensation(setup.getCompensation());
						dtoSkillSteup.setId(setup.getId());
						dtoSkillSteup.setSkillId(setup.getSkillId());
						dtoSkillSteup.setSkillDesc(setup.getSkillDesc());
						dtoSkillSetDesc.setSkillsId(dtoSkillSteup);
					}

					dtoSkillSetSteup.add(dtoSkillSetDesc);

				}

				List<Integer> ids = repositorySkillSetDesc.getDescId(skillSetSetupList.getId());

				if (ids != null && !ids.isEmpty()) {
					List<SkillsSetup> list = repositorySkillSteps.findByIds(ids);
					List<DtoSkillSteup> skillIds = new ArrayList<>();

					for (SkillsSetup skillsSetup : list) {
						DtoSkillSteup dtoSkillSteup = new DtoSkillSteup();
						dtoSkillSteup.setId(skillsSetup.getId());
						dtoSkillSteup.setSkillId(skillsSetup.getSkillId());
						dtoSkillSteup.setSkillDesc(skillsSetup.getSkillDesc());
						dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
						dtoSkillSteup.setFrequency(skillsSetup.getFrequency());
						dtoSkillSteup.setCompensation(skillsSetup.getCompensation());
						skillIds.add(dtoSkillSteup);

					}

					dtoSkillSetSteup1.setSkillIds(skillIds);
				}

				dtoSkillSetSteup1.setListDtoSkillSetDesc(dtoSkillSetSteup);
				dtoSearch.setRecords(dtoSkillSetSteup1);
			}

		}

		return dtoSearch;
	}

}
