package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Company;
import com.bti.hcm.model.dto.DtoCompany;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryCompany;

@Service("serviceCompany")
public class ServiceCompany {

	static Logger log = Logger.getLogger(ServiceCompany.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired(required = false)
	RepositoryCompany repositoryCompany;
	
	
	public DtoSearch searchCompanyId(DtoSearch dtoSearch) {

		try {
			log.info("searchCompanyId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				List<DtoCompany> dtoHelthInsuranceList =new ArrayList<>();
				
				List<Company> helthInsuranceList= this.repositoryCompany.predictiveCompanyIdSearchWithPagination("%"+searchWord+"%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
							Sort.Direction.DESC, "companyId"));
					
					for (Company company : helthInsuranceList) {
						DtoCompany dtoCompany = new DtoCompany();
						dtoCompany.setCompanyId(company.getCompanyId());
						dtoCompany.setCompanyName(company.getCompanyName());
						dtoHelthInsuranceList.add(dtoCompany);
					}
					dtoSearch.setRecords(dtoHelthInsuranceList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
}
