package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40311",indexes = {
        @Index(columnList = "HLCOVRINDX")
})
public class HelthCoverageType extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HLCOVRINDX")
	private Integer id;
	
	
	@Column(name = "HLCOVRID",length = 15)
	private String helthCoverageId;
	
	@Column(name = "HLCOVRDSCR",length = 31)
	private String desc;
	
	@Column(name = "HLCOVRDSCRA",length = 61)
	private String arbicDesc;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "helthCoverageType")
	@Where(clause = "is_deleted = false")
	private List<HelthInsurance> helthInsurance;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public String getHelthCoverageId() {
		return helthCoverageId;
	}

	public void setHelthCoverageId(String helthCoverageId) {
		this.helthCoverageId = helthCoverageId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public List<HelthInsurance> getHelthInsurance() {
		return helthInsurance;
	}

	public void setHelthInsurance(List<HelthInsurance> helthInsurance) {
		this.helthInsurance = helthInsurance;
	}

}
