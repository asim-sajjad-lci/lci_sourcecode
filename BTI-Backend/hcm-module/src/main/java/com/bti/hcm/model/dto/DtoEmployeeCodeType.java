package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DtoEmployeeCodeType {

	private Integer id;
	private Date startDate;
	private Date endDate;
	private BigDecimal amount;
	private BigDecimal payRate;
	private short method;
	private BigDecimal percent;
	private Integer codeId;
	private boolean inActive;
	private String baseOnPayCode;
	private BigDecimal payFactory;
	private Integer empolyeeMaintananceId;
	private Integer employeeId;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public DtoEmployeeCodeType() {

	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public short getMethod() {
		return method;
	}

	public void setMethod(short method) {
		this.method = method;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	public BigDecimal getPayRate() {
		return payRate;
	}

	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}

	public String getBaseOnPayCode() {
		return baseOnPayCode;
	}

	public void setBaseOnPayCode(String baseOnPayCode) {
		this.baseOnPayCode = baseOnPayCode;
	}

	public BigDecimal getPayFactory() {
		return payFactory;
	}

	public void setPayFactory(BigDecimal payFactory) {
		this.payFactory = payFactory;
	}

	public Integer getEmpolyeeMaintananceId() {
		return empolyeeMaintananceId;
	}

	public void setEmpolyeeMaintananceId(Integer empolyeeMaintananceId) {
		this.empolyeeMaintananceId = empolyeeMaintananceId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	
}
