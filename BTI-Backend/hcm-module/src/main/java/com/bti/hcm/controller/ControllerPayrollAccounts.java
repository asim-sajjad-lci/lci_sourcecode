package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoPayrollAccounts;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayrollAccounts;
import com.bti.hcm.service.ServiceResponse;
@RestController
@RequestMapping("/payrollAccounts")
public class ControllerPayrollAccounts extends BaseController{

	
private static final Logger LOGGER = Logger.getLogger(ControllerPayrollAccounts.class);
	
	@Autowired
	ServicePayrollAccounts  servicePayrollAccounts;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoPayrollAccounts  dtoPayrollAccounts) throws Exception {
		LOGGER.info("Create PayRollAccounts  Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayrollAccounts  = servicePayrollAccounts.saveOrUpdate(dtoPayrollAccounts);
			responseMessage=displayMessage(dtoPayrollAccounts, "PAYROLL_ACCOUNTS_CREATED", "PAYROLL_ACCOUNTS_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PayRollAccounts Method:"+responseMessage.getMessage());
		return responseMessage;
	}

}
