package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PayCodeType;

public class DtoPayCodeType extends DtoBase{

	private Integer id;
	private String desc;
	private String arabicDesc;
	private List<DtoPayCodeType> delete;
	public DtoPayCodeType(PayCodeType payScheduleSetup) {
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getArabicDesc() {
		return arabicDesc;
	}
	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}
	public List<DtoPayCodeType> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoPayCodeType> delete) {
		this.delete = delete;
	}
	public DtoPayCodeType() {
		
	}
	
	
	
}
