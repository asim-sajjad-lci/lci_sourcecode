package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollAccounts;

@Repository("/repositoryPayrollAccounts")
public interface RepositoryPayrollAccounts extends JpaRepository<PayrollAccounts, Integer> {

	PayrollAccounts findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select t from PayrollAccounts t where (" //t.financialDimensions.id =:projectId and "
			+ "t.department.id =:departmentId )  and t.isDeleted=false")
	List<PayrollAccounts> getstatus( // @Param("projectId") Integer projectId, 
			@Param("departmentId") Integer department);

	@Query("select t from PayrollAccounts t where (t.department.id =:departmentId )  and t.isDeleted=false")
	List<PayrollAccounts> getstatusByDepartment(@Param("departmentId") Integer department);
}
