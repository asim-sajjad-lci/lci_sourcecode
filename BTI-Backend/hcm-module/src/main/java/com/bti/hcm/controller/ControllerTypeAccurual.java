package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoAccrualType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceAccurualType;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

/**
 * @author Gaurav
 *
 */
@RestController
@RequestMapping("/accurualType")
public class ControllerTypeAccurual extends BaseController{
	/**
	 * @Description LOGGER use for put a logger in TypeAccurual Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerTypeAccurual.class);
	
	/**
	 * @Description serviceAccurual Autowired here using annotation of spring for use of serviceAccurual method in this controller
	 */
	@Autowired(required=true)
	ServiceAccurualType serviceAccurual;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @param request{
  
  	"type" : 1,
  	"desc" : "test"
 
	}
	 * @param dtoAccrual
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoAccrualType dtoAccrual) throws Exception {
		LOGGER.info("Create Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrual = serviceAccurual.saveOrUpdate(dtoAccrual);
			responseMessage = displayMessage(dtoAccrual,"ACCRUAL_CREATED","THIS ACCRUAL DESCRIPTION ALREADY_EXITS",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Accrual Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
		  "id":1,
		  "type" : 1,
		  "desc" : "test"
 
		}
	 * @param dtoAccrual
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoAccrualType dtoAccrual) throws Exception {
		LOGGER.info("Update Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrual = serviceAccurual.saveOrUpdate(dtoAccrual);
			
			responseMessage = displayMessage(dtoAccrual,"ACCRUAL_UPDATED_SUCCESS","ACCRUAL_NOT_UPDATED",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Accrual Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  			"ids": [1]
		}
	 * @param dtoAccrual
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoAccrualType dtoAccrual) throws Exception {
		LOGGER.info("Delete Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoAccrual.getIds() != null && !dtoAccrual.getIds().isEmpty()) {
				DtoAccrualType dtoAccrual2 = serviceAccurual.delete(dtoAccrual.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ACCRUAL_DELETED", false), dtoAccrual2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Delete Attendace Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{
   	"searchKeyword":"test"
		
 
}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccurual.search(dtoSearch);
			responseMessage = displayMessage(dtoSearch,MessageConstant.ACCRUAL_GET_ALL,MessageConstant.ACCRUAL_LIST_NOT_GETTING,serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Attendace Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	/**
	 * @param request{
  "pageNumber":"0",
		  "pageSize":"10"
 
}
	 * @param dtoAccrual
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody  DtoAccrualType dtoAccrual) throws Exception {
		LOGGER.info("Get All Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceAccurual.getAll(dtoAccrual);
			responseMessage = displayMessage(dtoSearch,MessageConstant.ACCRUAL_GET_ALL,MessageConstant.ACCRUAL_LIST_NOT_GETTING,serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All Accrual Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param request{
 
 	 	"id":1
	}
	 * @param dtoAccrual
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAtteandaceId", method = RequestMethod.POST)
	public ResponseMessage getPositionBudgetId(HttpServletRequest request, @RequestBody DtoAccrualType dtoAccrual) throws Exception {
		LOGGER.info("Get Position ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAccrualType dtoAccrualObj = serviceAccurual.getByAccrualId(dtoAccrual.getId());
			responseMessage = displayMessage(dtoAccrualObj,MessageConstant.ACCRUAL_GET_ALL,MessageConstant.ACCRUAL_LIST_NOT_GETTING,serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Accrual by Id Method:"+dtoAccrual.getId());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllAccuralTypes", method = RequestMethod.POST)
	public ResponseMessage getAllAccuralTypes(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) throws Exception {
		LOGGER.info("Get AllAccuralsTypes Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccurual.getAllAccrualsTypesId(dtoSearch);
			responseMessage = displayMessage(dtoSearch,MessageConstant.ACCRUAL_GET_ALL,MessageConstant.ACCRUAL_LIST_NOT_GETTING,serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Attendace Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/accrualDescriptionCheck", method = RequestMethod.POST)
	public ResponseMessage accrualDescriptionCheck(HttpServletRequest request, @RequestBody DtoAccrualType dtoaccrualType) throws Exception {
		LOGGER.info("DeductionCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAccrualType dtoAccrualObj = serviceAccurual.repeatByAccrualDesc(dtoaccrualType.getDesc());
			responseMessage = displayMessage(dtoAccrualObj,MessageConstant.ACCRUAL_GET_ALL,MessageConstant.ACCRUAL_LIST_NOT_GETTING,serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
}
