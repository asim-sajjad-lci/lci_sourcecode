package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.model.Language;

public class DtoHrState extends DtoBase{

	private Integer countryId;
	private String stateCode;
	private HrCountry countryMaster;
	private String country;
	private Language language;
	private List<DtoHrState> delete;
	public DtoHrState() {
	}
	public DtoHrState(HrState hrCity) {
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public HrCountry getCountryMaster() {
		return countryMaster;
	}
	public void setCountryMaster(HrCountry countryMaster) {
		this.countryMaster = countryMaster;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Language getLanguage() {
		return language;
	}
	public void setLanguage(Language language) {
		this.language = language;
	}
	public List<DtoHrState> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoHrState> delete) {
		this.delete = delete;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	
	
}
