package com.bti.hcm.model.dto;

/**
 * 
 * @author HAMID
 *
 */
public class DtoMultiselect extends DtoBase {

	private Integer id;	//ids.id
	private String itemName;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


}
