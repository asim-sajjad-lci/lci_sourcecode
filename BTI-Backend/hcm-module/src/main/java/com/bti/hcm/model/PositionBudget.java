package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40153",indexes = {
        @Index(columnList = "POTSCHINDX")
})
public class PositionBudget extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POTSCHINDX")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="POTPLINDX")
	private PositionPalnSetup positionPalnSetup;
	
	@Column(name="POTSCHSEQN")
	private Integer budgetScheduleSeqn;
	
	@Column(name="POTSCHSTDT")
	private Date budgetScheduleStart;
	
	@Column(name="POTSCHENDT")
	private Date budgetScheduleEnd;
	
	@Column(name="POTSCHDSCR")
	private String budgetDesc;
	
	@Column(name="POTSCHDSCRA")
	private String budgetArbicDesc;
	
	
	@Column(name="POTSCHAMT",precision = 13,scale = 3)
	private BigDecimal budgetAmount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PositionPalnSetup getPositionPalnSetup() {
		return positionPalnSetup;
	}

	public void setPositionPalnSetup(PositionPalnSetup positionPalnSetup) {
		this.positionPalnSetup = positionPalnSetup;
	}

	public Integer getBudgetScheduleSeqn() {
		return budgetScheduleSeqn;
	}

	public void setBudgetScheduleSeqn(Integer budgetScheduleSeqn) {
		this.budgetScheduleSeqn = budgetScheduleSeqn;
	}

	public Date getBudgetScheduleStart() {
		return budgetScheduleStart;
	}

	public void setBudgetScheduleStart(Date budgetScheduleStart) {
		this.budgetScheduleStart = budgetScheduleStart;
	}

	public Date getBudgetScheduleEnd() {
		return budgetScheduleEnd;
	}

	public void setBudgetScheduleEnd(Date budgetScheduleEnd) {
		this.budgetScheduleEnd = budgetScheduleEnd;
	}

	public String getBudgetDesc() {
		return budgetDesc;
	}

	public void setBudgetDesc(String budgetDesc) {
		this.budgetDesc = budgetDesc;
	}

	public BigDecimal getBudgetAmount() {
		return budgetAmount;
	}

	public void setBudgetAmount(BigDecimal budgetAmount) {
		this.budgetAmount = budgetAmount;
	}

	public String getBudgetArbicDesc() {
		return budgetArbicDesc;
	}

	public void setBudgetArbicDesc(String budgetArbicDesc) {
		this.budgetArbicDesc = budgetArbicDesc;
	}

	

	
	
	
	

}
