/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayScheduleSetupSchedulePeriods;

/**
 * Description: Interface for PayScheduleSetupSchedulePeriods  
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryPayScheduleSetupSchedulePeriods")
public interface RepositoryPayScheduleSetupSchedulePeriods  extends JpaRepository<PayScheduleSetupSchedulePeriods, Integer>{

	
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public PayScheduleSetupSchedulePeriods findByPaySchedulePeriodIndexIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupSchedulePeriods> findByIsDeleted(Boolean deleted);
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetup p where p.isDeleted=false")
	public Integer getCountOfTotalPayScheduleSetupSchedulePeriods();
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<PayScheduleSetupSchedulePeriods> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupSchedulePeriods p set p.isDeleted =:deleted, p.updatedBy=:updateById where p.id IN (:idList)")
	public void deletePayScheduleSetupSchedulePeriods(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupSchedulePeriods p set p.isDeleted =:deleted ,p.updatedBy =:updateById where p.id =:id ")
	public void deleteSinglePayScheduleSetupSchedulePeriods(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public PayScheduleSetupSchedulePeriods findTop1ByOrderByPaySchedulePeriodIndexIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select p from PayScheduleSetupSchedulePeriods p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.periodName like :searchKeyWord ) and p.isDeleted=false")
	public List<PayScheduleSetupSchedulePeriods> predictivePayScheduleSetupSchedulePeriodsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	/**
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupSchedulePeriods> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupSchedulePeriods p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.periodName like :searchKeyWord ) and p.isDeleted=false")
	public Integer predictivePayScheduleSetupSchedulePeriodsSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select p from PayScheduleSetupSchedulePeriods p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.periodName like :searchKeyWord ) and p.isDeleted=false")
	public List<PayScheduleSetupSchedulePeriods> predictivePayScheduleSetupSchedulePeriodsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param paySchedulePeriodIndexId
	 * @return
	 */
	@Query("select p from PayScheduleSetupSchedulePeriods p where (p.paySchedulePeriodIndexId =:paySchedulePeriodIndexId) and p.isDeleted=false")
	public List<PayScheduleSetupSchedulePeriods> findByPayScheduleSetupSchedulePeriods(@Param("paySchedulePeriodIndexId")int paySchedulePeriodIndexId);
	
	
	@Query("select p from PayScheduleSetupSchedulePeriods p where (p.payScheduleSetup.payScheduleIndexId =:payScheduleIndexId)")
	public List<PayScheduleSetupSchedulePeriods> findByPayScheduleSetup(@Param("payScheduleIndexId")Integer payScheduleIndexId);

	
	@Query("select count(*) from PayScheduleSetupSchedulePeriods p ")
	public Integer getCountOfTotaPayScheduleSetupSchedulePeriods();
	
	
	
	
	
	
}
