package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "GL00103_h",indexes = {
        @Index(columnList = "DIMINXVALUE")
})
public class FinancialDimensionsValues extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DIMINXVALUE")
	private Integer id;

	@Column(name = "DIMVALUE", columnDefinition = "char(31)")
	private String dimensionValue;

	@Column(name = "DIMDESCR", columnDefinition = "char(31)")
	private String dimensionDescription;

	@Column(name = "DIMDESCRA", columnDefinition = "char(61)")
	private String dimensionArbicDescription;

	@ManyToOne
	@JoinColumn(name = "DIMINXD")
	private FinancialDimensions financialDimensions;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "financialDimensionsValues")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<ManualChecksDetails> listManualChecksDetails;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "financialDimensionsValues")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<ParollTransactionOpenYearDistribution> listParollTransactionOpenYearDistribution;   

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "financialDimensionsValues")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<PayrollTransactionOpenYearDetails> listPayrollTransactionOpenYearDetails; 
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public String getDimensionDescription() {
		return dimensionDescription;
	}

	public void setDimensionDescription(String dimensionDescription) {
		this.dimensionDescription = dimensionDescription;
	}

	public String getDimensionArbicDescription() {
		return dimensionArbicDescription;
	}

	public void setDimensionArbicDescription(String dimensionArbicDescription) {
		this.dimensionArbicDescription = dimensionArbicDescription;
	}

	public FinancialDimensions getFinancialDimensions() {
		return financialDimensions;
	}

	public void setFinancialDimensions(FinancialDimensions financialDimensions) {
		this.financialDimensions = financialDimensions;
	}

	public List<ManualChecksDetails> getListManualChecksDetails() {
		return listManualChecksDetails;
	}

	public void setListManualChecksDetails(List<ManualChecksDetails> listManualChecksDetails) {
		this.listManualChecksDetails = listManualChecksDetails;
	}

	public List<ParollTransactionOpenYearDistribution> getListParollTransactionOpenYearDistribution() {
		return listParollTransactionOpenYearDistribution;
	}

	public void setListParollTransactionOpenYearDistribution(
			List<ParollTransactionOpenYearDistribution> listParollTransactionOpenYearDistribution) {
		this.listParollTransactionOpenYearDistribution = listParollTransactionOpenYearDistribution;
	}

	public List<PayrollTransactionOpenYearDetails> getListPayrollTransactionOpenYearDetails() {
		return listPayrollTransactionOpenYearDetails;
	}

	public void setListPayrollTransactionOpenYearDetails(
			List<PayrollTransactionOpenYearDetails> listPayrollTransactionOpenYearDetails) {
		this.listPayrollTransactionOpenYearDetails = listPayrollTransactionOpenYearDetails;
	}

}
