package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.EmployeePositionReason;

public class DtoEmployeePositionReason extends DtoBase{

	private Integer id;
	private String positionReason;
	private List<DtoEmployeePositionReason> delete;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPositionReason() {
		return positionReason;
	}
	public void setPositionReason(String positionReason) {
		this.positionReason = positionReason;
	}
	public List<DtoEmployeePositionReason> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoEmployeePositionReason> delete) {
		this.delete = delete;
	}
	
	public DtoEmployeePositionReason() {
	}
	
	public DtoEmployeePositionReason(EmployeePositionReason employeePositionReason) {
	}
	

}
