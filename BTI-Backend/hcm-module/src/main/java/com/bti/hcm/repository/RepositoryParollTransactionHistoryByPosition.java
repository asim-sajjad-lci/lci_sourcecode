package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ParollTransactionHistoryByPosition;

@Repository("parollTransactionHistoryByPosition")
public interface RepositoryParollTransactionHistoryByPosition extends JpaRepository<ParollTransactionHistoryByPosition, Integer>{

}
