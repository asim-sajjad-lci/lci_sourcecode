package com.bti.hcm.util;

import java.util.Date;

import com.bti.hcm.model.ActivityLog;

public class UtilActivityLog {
	
	
	public static String activitylogMessage(String activityLogMessage) {
		
		return activityLogMessage;
		
	}
	
	public static ActivityLog activityLogs(Integer userId,String serviceNmae,String methodName) {
		
		ActivityLog activityLog = new ActivityLog();
		activityLog.setCreatedDate(new Date());
		activityLog.setMethod(methodName);
		activityLog.setUserId(userId);
		activityLog.setServices(serviceNmae);
		
		return activityLog;
		
	}

	private UtilActivityLog(){
		
	}
}
