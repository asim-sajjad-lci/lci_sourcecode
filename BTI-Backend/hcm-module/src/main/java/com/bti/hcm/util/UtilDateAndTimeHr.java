/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Description: DateAndTime Utility Class Project: Hcm Version: 0.0.1
 */
public class UtilDateAndTimeHr {

	/**
	 * @Description localToUTC Method use to convert localtime to UTC.
	 * @return
	 */
	public static Date localToUTC() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date gmt = new Date(sdf.format(date));
		return gmt;
	}

	private UtilDateAndTimeHr() {

	}

	public static Date startDate(Integer noOfDays, Date hireDate) {
		Calendar c = Calendar.getInstance();
		if (noOfDays != 0) {
			c.setTime(hireDate); // Now hire date.
			c.add(Calendar.DATE, noOfDays); // Adding days
			hireDate = c.getTime();
		}
		return hireDate;
	}

	public static Date endDate(Integer noOfDays, Integer endDateDays, Date hireDate) {
		Calendar c = Calendar.getInstance();
		if (noOfDays != 0) {
			c.setTime(hireDate); // Now hire date.
			c.add(Calendar.DATE, noOfDays); // Adding days
			hireDate = c.getTime();
		}
		if (endDateDays != 0) {
			c.setTime(hireDate);
			c.add(Calendar.DATE, endDateDays);
			hireDate = c.getTime();

		}
		return hireDate;
	}

	
	public static Long startDate2(Integer noOfDays, Date hireDate) {
		Calendar c = Calendar.getInstance();
		if (noOfDays != 0) {
			c.setTime(hireDate); // Now hire date.
			c.add(Calendar.DATE, noOfDays); // Adding days
			hireDate = c.getTime();
		}
		return hireDate.getTime();
	}
	public static Long endDate2(Integer noOfDays, Integer endDateDays, Date hireDate) {
		Calendar c = Calendar.getInstance();
		if (noOfDays != 0) {
			c.setTime(hireDate); // Now hire date.
			c.add(Calendar.DATE, noOfDays); // Adding days
			hireDate = c.getTime();
		}
		if (endDateDays != 0) {
			c.setTime(hireDate);
			c.add(Calendar.DATE, endDateDays);
			hireDate = c.getTime();

		}
		return hireDate.getTime();
	}

}
