package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.SalaryMatrixColSetup;
import com.bti.hcm.model.SalaryMatrixRow;
import com.bti.hcm.model.SalaryMatrixRowValue;
import com.bti.hcm.model.SalaryMatrixSetup;
import com.bti.hcm.model.dto.DtoSalaryMatrixColSetup;
import com.bti.hcm.model.dto.DtoSalaryMatrixRow;
import com.bti.hcm.model.dto.DtoSalaryMatrixRowValue;
import com.bti.hcm.model.dto.DtoSalaryMatrixSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositorySalaryMatrixColSetup;
import com.bti.hcm.repository.RepositorySalaryMatrixRow;
import com.bti.hcm.repository.RepositorySalaryMatrixRowValue;
import com.bti.hcm.repository.RepositorySalaryMatrixSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceSalaryMatrixSetup")
public class ServiceSalaryMatrixSetup {
	/**
	 * @Description LOGGER use for put a logger in SalaryMatrixSetup Service
	 */
	static Logger log = Logger.getLogger(ServiceSalaryMatrixSetup.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in SalaryMatrixSetup service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in SalaryMatrixSetup service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in SalaryMatrixSetup service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositorySalaryMatrixSetup Autowired here using annotation of spring for access of repositorySalaryMatrixSetup method in SalaryMatrixSetup service
	 * 				In short Access SalaryMatrixSetup Query from Database using repositorySalaryMatrixSetup.
	 */
	@Autowired
	RepositorySalaryMatrixSetup repositorySalaryMatrixSetup;
	
	@Autowired
	RepositorySalaryMatrixRow repositorySalaryMatrixRow;
	
	@Autowired
	RepositorySalaryMatrixRowValue repositorySalaryMatrixRowValue;

	@Autowired
	RepositorySalaryMatrixColSetup repositorySalaryMatrixColSetup;





	
	/**
	 * @param dtoSalaryMatrixSetup
	 * @return
	 * @throws ParseException
	 */
	public DtoSalaryMatrixSetup saveOrUpdate(DtoSalaryMatrixSetup dtoSalaryMatrixSetup){
		log.info("saveOrUpdate SalaryMatrixSetup Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		SalaryMatrixSetup salaryMatrixSetup = null;
		if (dtoSalaryMatrixSetup.getId() != null && dtoSalaryMatrixSetup.getId() > 0) {
			salaryMatrixSetup = repositorySalaryMatrixSetup.findByIdAndIsDeleted(dtoSalaryMatrixSetup.getId(), false);
			salaryMatrixSetup.setUpdatedBy(loggedInUserId);
			salaryMatrixSetup.setUpdatedDate(new Date());
		} else {
			salaryMatrixSetup = new SalaryMatrixSetup();
			salaryMatrixSetup.setCreatedDate(new Date());
			Integer rowId = repositorySalaryMatrixColSetup.getCountOfTotaSalaryMatrixColSetups();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			salaryMatrixSetup.setRowId(increment);
		}
		
		salaryMatrixSetup.setSalaryMatrixId(dtoSalaryMatrixSetup.getSalaryMatrixId());
		salaryMatrixSetup.setArabicSalaryMatrixDescription(dtoSalaryMatrixSetup.getArabicSalaryMatrixDescription());
		salaryMatrixSetup.setPayUnit(dtoSalaryMatrixSetup.getPayUnit());
		salaryMatrixSetup.setTotalRow(dtoSalaryMatrixSetup.getTotalRow());
		salaryMatrixSetup.setDescription(dtoSalaryMatrixSetup.getDescription());
		salaryMatrixSetup.setTotalColumns(dtoSalaryMatrixSetup.getTotalColumns());
		salaryMatrixSetup.setTotalRowValues(dtoSalaryMatrixSetup.getTotalRowValues());
		
		
		salaryMatrixSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		
		salaryMatrixSetup = repositorySalaryMatrixSetup.saveAndFlush(salaryMatrixSetup);
		
		List<SalaryMatrixRow> salaryMatrixRowList = repositorySalaryMatrixRow.findBySalaryMatrixId(salaryMatrixSetup.getId());
		for (SalaryMatrixRow salaryMatrixRow : salaryMatrixRowList) {
			repositorySalaryMatrixRow.delete(salaryMatrixRow.getId());
		}
		
		for (DtoSalaryMatrixRow dtoSalaryMatrixRow : dtoSalaryMatrixSetup.getListSalaryMatrixRow()) {
			SalaryMatrixRow salaryMatrixRow =  new SalaryMatrixRow();
			salaryMatrixRow.setCreatedDate(new Date());
			salaryMatrixRow.setSalaryMatrixSetup(salaryMatrixSetup);
			salaryMatrixRow.setDesc(dtoSalaryMatrixRow.getDesc());
			salaryMatrixRow.setArbic(dtoSalaryMatrixRow.getArbic());
			salaryMatrixRow.setUpdatedDate(new Date());
			repositorySalaryMatrixRow.saveAndFlush(salaryMatrixRow);
			List<SalaryMatrixRowValue> salaryMatrixRowValueList = repositorySalaryMatrixRowValue.findBySalaryMatrixRowId(salaryMatrixRow.getId());
			for (SalaryMatrixRowValue salaryMatrixRowValue : salaryMatrixRowValueList) {
				repositorySalaryMatrixRowValue.delete(salaryMatrixRowValue.getId());
			}
			for (DtoSalaryMatrixRowValue dtoSalaryMatrixRowValue : dtoSalaryMatrixRow.getListSalaryMatrixRowValue()) {
				SalaryMatrixRowValue matrixRowValue = new SalaryMatrixRowValue();
				matrixRowValue.setAmount(dtoSalaryMatrixRowValue.getAmount());
				matrixRowValue.setSequence(dtoSalaryMatrixRowValue.getSequence());
				matrixRowValue.setDesc(dtoSalaryMatrixRowValue.getDesc());
				matrixRowValue.setCreatedDate(new Date());
				matrixRowValue.setUpdatedDate(new Date());
				matrixRowValue.setSalaryMatrixRow(salaryMatrixRow);
				
				repositorySalaryMatrixRowValue.saveAndFlush(matrixRowValue);
			}
			
		}
		List<SalaryMatrixColSetup> salaryMatrixColSetupList = repositorySalaryMatrixColSetup.findBySalaryMatrixId(salaryMatrixSetup.getId());
		for (SalaryMatrixColSetup salaryMatrixColSetup : salaryMatrixColSetupList) {
			repositorySalaryMatrixColSetup.delete(salaryMatrixColSetup.getColSalaryMatrixIndexId());
		}
		for (DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup : dtoSalaryMatrixSetup.getListSalaryMatrixColSetup()) {
			SalaryMatrixColSetup salaryMatrixColSetup = new SalaryMatrixColSetup();
			salaryMatrixColSetup.setColSalaryMatrixDescription(dtoSalaryMatrixColSetup.getColSalaryMatrixDescription());
			salaryMatrixColSetup.setArabicColSalaryMatrixDescription(dtoSalaryMatrixColSetup.getArabicColSalaryMatrixDescription());
			salaryMatrixColSetup.setCreatedDate(new Date());
			salaryMatrixColSetup.setUpdatedDate(new Date());
			salaryMatrixColSetup.setSalaryMatrixSetup(salaryMatrixSetup);
			repositorySalaryMatrixColSetup.saveAndFlush(salaryMatrixColSetup);
		}
		
		log.debug("SalaryMatrixSetup is:"+dtoSalaryMatrixSetup.getId());
		return dtoSalaryMatrixSetup;

	}

	
	/**
	 * @param dtoSalaryMatrixSetup
	 * @return
	 */
	public DtoSearch getAll(DtoSalaryMatrixSetup dtoSalaryMatrixSetup) {
		log.info("getAllDeductionCode Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSalaryMatrixSetup.getPageNumber());
		dtoSearch.setPageSize(dtoSalaryMatrixSetup.getPageSize());
		dtoSearch.setTotalCount(repositorySalaryMatrixSetup.getCountOfTotalSalaryMatrix());
		List<SalaryMatrixSetup> salaryMatrixSetupList = null;
		if (dtoSalaryMatrixSetup.getPageNumber() != null && dtoSalaryMatrixSetup.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSalaryMatrixSetup.getPageNumber(), dtoSalaryMatrixSetup.getPageSize(), Direction.DESC, "createdDate");
			salaryMatrixSetupList = repositorySalaryMatrixSetup.findByIsDeleted(false, pageable);
		} else {
			salaryMatrixSetupList = repositorySalaryMatrixSetup.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		
		List<DtoSalaryMatrixSetup> dtoSalaryMatrixSetupList=new ArrayList<>();
		List<DtoSalaryMatrixRow> dtoSalaryMatrixRowList = new ArrayList<>();
		List<DtoSalaryMatrixColSetup> dtoSalaryMatrixColSetupList = new ArrayList<>();
		if(salaryMatrixSetupList!=null && !salaryMatrixSetupList.isEmpty())
		{
			for (SalaryMatrixSetup salaryMatrixSetup : salaryMatrixSetupList) 
			{
				dtoSalaryMatrixSetup=new DtoSalaryMatrixSetup(salaryMatrixSetup);
				dtoSalaryMatrixSetup.setId(salaryMatrixSetup.getId());
				dtoSalaryMatrixSetup.setSalaryMatrixId(salaryMatrixSetup.getSalaryMatrixId());
				dtoSalaryMatrixSetup.setArabicSalaryMatrixDescription(salaryMatrixSetup.getArabicSalaryMatrixDescription());
				dtoSalaryMatrixSetup.setPayUnit(salaryMatrixSetup.getPayUnit());
				dtoSalaryMatrixSetup.setTotalRow(salaryMatrixSetup.getTotalRow());
				dtoSalaryMatrixSetup.setDescription(salaryMatrixSetup.getDescription());
				dtoSalaryMatrixSetup.setTotalColumns(salaryMatrixSetup.getTotalColumns());
				dtoSalaryMatrixSetup.setTotalRowValues(salaryMatrixSetup.getTotalRowValues());
				for (SalaryMatrixRow salaryMatrixRow : salaryMatrixSetup.getSalaryMatrixRow()) {
					DtoSalaryMatrixRow dtoSalaryMatrixRow = new DtoSalaryMatrixRow(salaryMatrixRow);
					dtoSalaryMatrixRow.setId(salaryMatrixRow.getId());
					dtoSalaryMatrixRow.setDesc(salaryMatrixRow.getDesc());
					dtoSalaryMatrixRow.setArbic(salaryMatrixRow.getArbic());
					dtoSalaryMatrixRow.setSalaryMatrixSetupId(salaryMatrixRow.getSalaryMatrixSetup().getSalaryMatrixId());
					dtoSalaryMatrixRow.setSalaryMatrixId(salaryMatrixRow.getSalaryMatrixSetup().getId());
					dtoSalaryMatrixRowList.add(dtoSalaryMatrixRow);
				}
				for (SalaryMatrixColSetup salaryMatrixColSetup : salaryMatrixSetup.getSalaryMatrixColSetup()) {
					DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup = new DtoSalaryMatrixColSetup(salaryMatrixColSetup);
					dtoSalaryMatrixColSetup.setColSalaryMatrixIndexId(salaryMatrixColSetup.getColSalaryMatrixIndexId());
					dtoSalaryMatrixColSetup.setSalaryMatrixId(salaryMatrixColSetup.getSalaryMatrixSetup().getId());
					dtoSalaryMatrixColSetup.setColSalaryMatrixDescription(salaryMatrixColSetup.getColSalaryMatrixDescription());
					dtoSalaryMatrixColSetup.setArabicColSalaryMatrixDescription(salaryMatrixColSetup.getArabicColSalaryMatrixDescription());
					dtoSalaryMatrixColSetupList.add(dtoSalaryMatrixColSetup);
				}
				dtoSalaryMatrixSetup.setListSalaryMatrixColSetup(dtoSalaryMatrixColSetupList);
				dtoSalaryMatrixSetup.setListSalaryMatrixRow(dtoSalaryMatrixRowList);
				dtoSalaryMatrixSetupList.add(dtoSalaryMatrixSetup);
			}
			dtoSearch.setRecords(dtoSalaryMatrixSetupList);
		}
		log.debug("All DtoSalaryMatrixSetup List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search SalaryMatrixSetup Method");
		if(dtoSearch != null){
			
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			switch(dtoSearch.getSortOn()){
			case "salaryMatrixId" : 
				condition+=dtoSearch.getSortOn();
				break;
			case "description" : 
				condition+=dtoSearch.getSortOn();
				break;
			case "arabicSalaryMatrixDescription" : 
				condition+=dtoSearch.getSortOn();
				break;
			default:
				condition+="id";
				
			}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
			
			
		}
			dtoSearch.setTotalCount(this.repositorySalaryMatrixSetup.predictiveSalaryMatrixSetupSearchTotalCount("%"+searchWord+"%"));
			List<SalaryMatrixSetup> salaryMatrixSetupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					salaryMatrixSetupList = this.repositorySalaryMatrixSetup.predictiveSalaryMatrixSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					salaryMatrixSetupList = this.repositorySalaryMatrixSetup.predictiveSalaryMatrixSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					salaryMatrixSetupList = this.repositorySalaryMatrixSetup.predictiveSalaryMatrixSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
			}
		
			if(salaryMatrixSetupList != null && !salaryMatrixSetupList.isEmpty()){
				List<DtoSalaryMatrixSetup> dtoSalaryMatrixSetups = new ArrayList<>();
				DtoSalaryMatrixSetup dtoSalaryMatrixSetup=null;
				for (SalaryMatrixSetup salaryMatrixSetup : salaryMatrixSetupList) {
					dtoSalaryMatrixSetup = new DtoSalaryMatrixSetup(salaryMatrixSetup);
					
					
					dtoSalaryMatrixSetup.setId(salaryMatrixSetup.getId());
					dtoSalaryMatrixSetup.setSalaryMatrixId(salaryMatrixSetup.getSalaryMatrixId());
					dtoSalaryMatrixSetup.setArabicSalaryMatrixDescription(salaryMatrixSetup.getArabicSalaryMatrixDescription());
					dtoSalaryMatrixSetup.setPayUnit(salaryMatrixSetup.getPayUnit());
					dtoSalaryMatrixSetup.setTotalRow(salaryMatrixSetup.getTotalRow());
					dtoSalaryMatrixSetup.setDescription(salaryMatrixSetup.getDescription());
					dtoSalaryMatrixSetup.setTotalColumns(salaryMatrixSetup.getTotalColumns());
					dtoSalaryMatrixSetup.setTotalRowValues(salaryMatrixSetup.getTotalRowValues());
					
					
					List<DtoSalaryMatrixRow> dtoSalaryMatrixRowList = new ArrayList<>();
					for (SalaryMatrixRow salaryMatrixRow : salaryMatrixSetup.getSalaryMatrixRow()) {
						DtoSalaryMatrixRow dtoSalaryMatrixRow = new DtoSalaryMatrixRow(salaryMatrixRow);
						dtoSalaryMatrixRow.setId(salaryMatrixRow.getId());
						dtoSalaryMatrixRow.setDesc(salaryMatrixRow.getDesc());
						dtoSalaryMatrixRow.setArbic(salaryMatrixRow.getArbic());
						dtoSalaryMatrixRow.setSalaryMatrixSetupId(salaryMatrixRow.getSalaryMatrixSetup().getSalaryMatrixId());
						dtoSalaryMatrixRow.setSalaryMatrixId(salaryMatrixRow.getSalaryMatrixSetup().getId());
						List<DtoSalaryMatrixRowValue> dtoSalaryMatrixRowValueList = new ArrayList<>();
						for (SalaryMatrixRowValue salaryMatrixRowValue : salaryMatrixRow.getSalaryMatrixRowValue()) {
							DtoSalaryMatrixRowValue dtoSalaryMatrixRowValue = new DtoSalaryMatrixRowValue(salaryMatrixRowValue);
							dtoSalaryMatrixRowValue.setId(salaryMatrixRowValue.getId());
							dtoSalaryMatrixRowValue.setSalaryMatrixRowId(salaryMatrixRowValue.getId());
							dtoSalaryMatrixRowValue.setSequence(salaryMatrixRowValue.getSequence());
							dtoSalaryMatrixRowValue.setAmount(salaryMatrixRowValue.getAmount());
							dtoSalaryMatrixRowValue.setDesc(salaryMatrixRowValue.getDesc());
							dtoSalaryMatrixRowValueList.add(dtoSalaryMatrixRowValue);
						}
						dtoSalaryMatrixRow.setListSalaryMatrixRowValue(dtoSalaryMatrixRowValueList);
						dtoSalaryMatrixRowList.add(dtoSalaryMatrixRow);
					}
					List<DtoSalaryMatrixColSetup> dtoSalaryMatrixColSetupList = new ArrayList<>();
					for (SalaryMatrixColSetup salaryMatrixColSetup : salaryMatrixSetup.getSalaryMatrixColSetup()) {
						DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup = new DtoSalaryMatrixColSetup(salaryMatrixColSetup);
						dtoSalaryMatrixColSetup.setColSalaryMatrixIndexId(salaryMatrixColSetup.getColSalaryMatrixIndexId());
						dtoSalaryMatrixColSetup.setSalaryMatrixId(salaryMatrixColSetup.getSalaryMatrixSetup().getId());
						dtoSalaryMatrixColSetup.setColSalaryMatrixDescription(salaryMatrixColSetup.getColSalaryMatrixDescription());
						dtoSalaryMatrixColSetup.setArabicColSalaryMatrixDescription(salaryMatrixColSetup.getArabicColSalaryMatrixDescription());
						dtoSalaryMatrixColSetupList.add(dtoSalaryMatrixColSetup);
					}
					dtoSalaryMatrixSetup.setListSalaryMatrixColSetup(dtoSalaryMatrixColSetupList);
					dtoSalaryMatrixSetup.setListSalaryMatrixRow(dtoSalaryMatrixRowList);

					dtoSalaryMatrixSetups.add(dtoSalaryMatrixSetup);
				}
				dtoSearch.setRecords(dtoSalaryMatrixSetups);
			}
		}
		log.debug("Search SalaryMatrixSetup Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	
	/**
	 * @param id
	 * @return
	 */
	@Transactional
	public DtoSalaryMatrixSetup getById(int id) {
		log.info("getById Method");
		DtoSalaryMatrixSetup dtoSalaryMatrixSetup = new DtoSalaryMatrixSetup();
		if (id > 0) {
			SalaryMatrixSetup salaryMatrixSetup2 = repositorySalaryMatrixSetup.findByIdAndIsDeleted(id, false);
				if (salaryMatrixSetup2 != null) {
					dtoSalaryMatrixSetup.setId(salaryMatrixSetup2.getId());
					dtoSalaryMatrixSetup.setDescription(salaryMatrixSetup2.getDescription());
					dtoSalaryMatrixSetup.setArabicSalaryMatrixDescription(salaryMatrixSetup2.getArabicSalaryMatrixDescription());
					dtoSalaryMatrixSetup.setPayUnit(salaryMatrixSetup2.getPayUnit());
					dtoSalaryMatrixSetup.setTotalColumns(salaryMatrixSetup2.getTotalColumns());
					dtoSalaryMatrixSetup.setTotalRowValues(salaryMatrixSetup2.getTotalRowValues());
					dtoSalaryMatrixSetup.setTotalRow(salaryMatrixSetup2.getTotalRow());
					dtoSalaryMatrixSetup.setSalaryMatrixId(salaryMatrixSetup2.getSalaryMatrixId());
					
				} else {
					dtoSalaryMatrixSetup.setMessageType("SALARY_MATRIX_LIST_NOT_GETTING");

				}
			
			
		} else {
			dtoSalaryMatrixSetup.setMessageType("SALARY_MATRIX_LIST_NOT_GETTING");

		}
		log.debug("SalaryMatrixSetup By Id is:"+dtoSalaryMatrixSetup.getId());
		return dtoSalaryMatrixSetup;
	}


	
	
	public DtoSalaryMatrixSetup delete(List<Integer> ids) {
		log.info("delete SalaryMatrixSetup Method");
		DtoSalaryMatrixSetup dtoSalaryMatrixSetup = new DtoSalaryMatrixSetup();
		dtoSalaryMatrixSetup
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SALARY_MATRIX_DELETED", false));
		dtoSalaryMatrixSetup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SALARY_MATRIX_DELETED", false));
		List<DtoSalaryMatrixSetup> delete = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer Id : ids) {
				SalaryMatrixSetup salaryMatrixSetup = repositorySalaryMatrixSetup.findOne(Id);
				if(salaryMatrixSetup.getSalaryMatrixColSetup().isEmpty() && salaryMatrixSetup.getSalaryMatrixRow().isEmpty()) {
					DtoSalaryMatrixSetup dtoSalaryMatrixSetup2 = new DtoSalaryMatrixSetup();
					dtoSalaryMatrixSetup2.setId(salaryMatrixSetup.getId());
					repositorySalaryMatrixSetup.deleteSingleSalaryMatrixSetup(true, loggedInUserId, Id);
					delete.add(dtoSalaryMatrixSetup2);
				}
			}
			dtoSalaryMatrixSetup.setDeleteSalaryMatrixSetup(delete);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete SalaryMatrixSetup :"+dtoSalaryMatrixSetup.getId());
		return dtoSalaryMatrixSetup;
	}
	
	
	/**
	 * @param salaryMatrixSetupId
	 * @return
	 */
	public DtoSalaryMatrixSetup repeatBySalaryMatrixSetupId(String salaryMatrixSetupId) {
		log.info("repeatBySalaryMatrixSetupId Method");
		DtoSalaryMatrixSetup dtoSalaryMatrixSetup = new DtoSalaryMatrixSetup();
		try {
			List<SalaryMatrixSetup> salaryMatrixSetup=repositorySalaryMatrixSetup.findBySalaryMatrixSetupId(salaryMatrixSetupId.trim());
			if(salaryMatrixSetup!=null && !salaryMatrixSetup.isEmpty()) {
				dtoSalaryMatrixSetup.setIsRepeat(true);
			}else {
				dtoSalaryMatrixSetup.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSalaryMatrixSetup;
	}


		
	public DtoSearch searchSalaryMatrixId(DtoSearch dtoSearch) {
		log.info("searchSalaryMatrixId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			List<SalaryMatrixSetup> salaryMatrixSetupList =null;
			
				salaryMatrixSetupList = this.repositorySalaryMatrixSetup.predictivesearchSalaryMatrixIdWithPagination("%"+searchWord+"%");
				if(!salaryMatrixSetupList.isEmpty()) {
					
					
					
					
					if(!salaryMatrixSetupList.isEmpty()){
						List<DtoSalaryMatrixSetup> dtoSalaryMatrixSetup = new ArrayList<>();
						DtoSalaryMatrixSetup dtoSalaryMatrixSetup1=null;
						for (SalaryMatrixSetup salaryMatrixSetup : salaryMatrixSetupList) {
							
							dtoSalaryMatrixSetup1 = new DtoSalaryMatrixSetup(salaryMatrixSetup);
							
							
							dtoSalaryMatrixSetup1.setId(salaryMatrixSetup.getId());
							dtoSalaryMatrixSetup1.setSalaryMatrixId(salaryMatrixSetup.getSalaryMatrixId());
							dtoSalaryMatrixSetup1.setDescription(salaryMatrixSetup.getSalaryMatrixId()+"  -  "+salaryMatrixSetup.getDescription());
	
							
							dtoSalaryMatrixSetup.add(dtoSalaryMatrixSetup1);
						}
						dtoSearch.setRecords(dtoSalaryMatrixSetup);
					}

					dtoSearch.setTotalCount(salaryMatrixSetupList.size());
				}
			
		}
		
		return dtoSearch;
	}


	public DtoSalaryMatrixSetup getById(DtoSalaryMatrixSetup dtoSalaryMatrixSetup) {
		// TODO Auto-generated method stub
		return null;
	}



	
	
}
