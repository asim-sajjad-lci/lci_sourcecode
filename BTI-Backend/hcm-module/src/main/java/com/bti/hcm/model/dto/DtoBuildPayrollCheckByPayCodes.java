package com.bti.hcm.model.dto;

import java.util.List;

public class DtoBuildPayrollCheckByPayCodes {
	private Integer id;
	private DtoBuildChecks buildChecks;
	private DtoPayCode payCode;
	private List<DtoPayCode> dtoPayCode;
	private List<DtoBuildPayrollCheckByPayCodes> listBuildChecks;
	private List<Integer> departmentList;
	private String payCodeId;
	private String description;
	
	private Integer pageNumber;
	private Integer pageSize;
	private List<Integer> ids;
	private Boolean isActive;
	private String messageType;
	private String message;
	private String deleteMessage;
	private String associateMessage;
	private List<DtoBuildPayrollCheckByPayCodes> delete;
	
	private Boolean isRepeat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DtoBuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(DtoBuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}

	public List<DtoPayCode> getDtoPayCode() {
		return dtoPayCode;
	}

	public void setDtoPayCode(List<DtoPayCode> dtoPayCode) {
		this.dtoPayCode = dtoPayCode;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeleteMessage() {
		return deleteMessage;
	}

	public void setDeleteMessage(String deleteMessage) {
		this.deleteMessage = deleteMessage;
	}

	public String getAssociateMessage() {
		return associateMessage;
	}

	public void setAssociateMessage(String associateMessage) {
		this.associateMessage = associateMessage;
	}

	public List<DtoBuildPayrollCheckByPayCodes> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoBuildPayrollCheckByPayCodes> delete) {
		this.delete = delete;
	}

	public Boolean getIsRepeat() {
		return isRepeat;
	}

	public void setIsRepeat(Boolean isRepeat) {
		this.isRepeat = isRepeat;
	}

	public DtoPayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(DtoPayCode payCode) {
		this.payCode = payCode;
	}

	public List<DtoBuildPayrollCheckByPayCodes> getListBuildChecks() {
		return listBuildChecks;
	}

	public void setListBuildChecks(List<DtoBuildPayrollCheckByPayCodes> listBuildChecks) {
		this.listBuildChecks = listBuildChecks;
	}

	public String getPayCodeId() {
		return payCodeId;
	}

	public void setPayCodeId(String payCodeId) {
		this.payCodeId = payCodeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Integer> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<Integer> departmentList) {
		this.departmentList = departmentList;
	}

	
	

}
