package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.SkillSetSetup;
import com.bti.hcm.model.SkillsSetup;
import com.bti.hcm.model.TrainingCourseDetail;
import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.model.TraningCourseClassSkill;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSkillSetSteup;
import com.bti.hcm.model.dto.DtoSkillSteup;
import com.bti.hcm.model.dto.DtoTrainingCourseDetail;
import com.bti.hcm.model.dto.DtoTraningCourse;
import com.bti.hcm.model.dto.DtoTraningCourseClassSkill;
import com.bti.hcm.repository.RepositorySkillSetSetup;
import com.bti.hcm.repository.RepositorySkillSteps;
import com.bti.hcm.repository.RepositoryTrainingCourseDetail;
import com.bti.hcm.repository.RepositoryTraningCourse;
import com.bti.hcm.repository.RepositoryTraningCourseClassSkill;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceTraningCourseClassSkill")
public class ServiceTraningCourseClassSkill {

	static Logger log = Logger.getLogger(ServiceTraningCourseClassSkill.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryTraningCourse repositoryTraningCourse;
	
	@Autowired
	RepositoryTrainingCourseDetail repositoryTrainingCourseDetail;
	
	@Autowired
	RepositoryTraningCourseClassSkill repositoryTraningCourseClassSkill;
	
	@Autowired
	RepositorySkillSteps repositorySkillSteps;
	
	@Autowired
	RepositorySkillSetSetup repositorySkillSetSetup;
	
	
	
	public DtoTraningCourseClassSkill saveOrUpdate(DtoTraningCourseClassSkill dtoTraningCourseClassSkill) {
		log.info("enter into save or update TraningCourse Schdule");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		TraningCourseClassSkill traningCourseClassSkill=null;
		if (dtoTraningCourseClassSkill.getId() != null && dtoTraningCourseClassSkill.getId() > 0) {
			traningCourseClassSkill = repositoryTraningCourseClassSkill.findByIdAndIsDeleted(dtoTraningCourseClassSkill.getId(), false);
			traningCourseClassSkill.setUpdatedBy(loggedInUserId);
			traningCourseClassSkill.setUpdatedDate(new Date());
		} else {
			traningCourseClassSkill = new TraningCourseClassSkill();
			traningCourseClassSkill.setCreatedDate(new Date());
			
			Integer rowId = repositoryTraningCourseClassSkill.getCountOfTotalTraningCourseClassSkill();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			traningCourseClassSkill.setRowId(increment);
		}
		
		List<SkillsSetup> list = new ArrayList<>();
		
		for (DtoSkillSteup skillSetSetup : dtoTraningCourseClassSkill.getSkillsSetup()) {
			SkillsSetup skillsSetup = 	repositorySkillSteps.findOne(skillSetSetup.getId());
			list.add(skillsSetup);
		}
		
		SkillSetSetup skillSetSetup = null;
		if (dtoTraningCourseClassSkill.getSkillSetSetup()!=null) {
			skillSetSetup = 	repositorySkillSetSetup.findOne(dtoTraningCourseClassSkill.getSkillSetSetup().getId());
			
		}
		
		
		
		TraningCourse traningCourse = 	repositoryTraningCourse.findOne(dtoTraningCourseClassSkill.getTrainingCourse().getId());
		TrainingCourseDetail trainingCourseDetail = 	repositoryTrainingCourseDetail.findOne(dtoTraningCourseClassSkill.getTrainingClass().getId());
		
		traningCourseClassSkill.setTrainingClass(trainingCourseDetail);
		traningCourseClassSkill.setTrainingCourse(traningCourse);
		traningCourseClassSkill.setSkillSetSetup(skillSetSetup);
		traningCourseClassSkill.setSkillsSetup(list);
		traningCourseClassSkill.setSkillSequnce(dtoTraningCourseClassSkill.getSkillSequnce());
		traningCourseClassSkill.setSkillDesc(dtoTraningCourseClassSkill.getSkillDesc());
		traningCourseClassSkill.setSkillArabicDesc(dtoTraningCourseClassSkill.getSkillArabicDesc());
        traningCourseClassSkill.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryTraningCourseClassSkill.saveAndFlush(traningCourseClassSkill);
		return dtoTraningCourseClassSkill;
	}
	
	
	
	
	
	public DtoTraningCourseClassSkill delete(List<Integer> ids) {
		log.info("delete TraningCourse Method");
		DtoTraningCourseClassSkill dtoTraningCourse = new DtoTraningCourseClassSkill();
		dtoTraningCourse.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRAININGCOURSE_DELETED", false));
		dtoTraningCourse.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRAININGCOURSE_DELETED", false));
		List<DtoTraningCourseClassSkill> deleteTraningCourse = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TRAININGCOURSE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer id : ids) {
				TraningCourseClassSkill traningCourse = repositoryTraningCourseClassSkill.findOne(id);
				if(traningCourse!=null) {
					DtoTraningCourseClassSkill dtoTraningCourse2 = new DtoTraningCourseClassSkill();
					dtoTraningCourse2.setId(traningCourse.getId());

					repositoryTraningCourseClassSkill.deleteSingleTraningCourseClassSkill(true, loggedInUserId, id);
					deleteTraningCourse.add(dtoTraningCourse2);
				}else {
					inValidDelete = true;
				}
				
				

			}
			dtoTraningCourse.setDelete(deleteTraningCourse);
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoTraningCourse.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoTraningCourse.setMessageType("");
				
			}
			dtoTraningCourse.setDelete(deleteTraningCourse);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete TraningCourse :"+dtoTraningCourse.getId());
		return dtoTraningCourse;
	}	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search TraningCourse Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("traningId") || dtoSearch.getSortOn().equals("desc")|| dtoSearch.getSortOn().equals("arbicDesc")|| dtoSearch.getSortOn().equals("location")|| dtoSearch.getSortOn().equals("prerequisiteId")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
			
			dtoSearch.setTotalCount(this.repositoryTraningCourseClassSkill.predictiveTraningCourseClassSkillSearchTotalCount("%"+searchWord+"%"));
			List<TraningCourseClassSkill> traningCourseList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					traningCourseList = this.repositoryTraningCourseClassSkill.predictiveTraningCourseClassSkillSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					traningCourseList = this.repositoryTraningCourseClassSkill.predictiveTraningCourseClassSkillSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					traningCourseList = this.repositoryTraningCourseClassSkill.predictiveTraningCourseClassSkillSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			if(traningCourseList != null && !traningCourseList.isEmpty()){
				List<DtoTraningCourseClassSkill> dtoTraningCourseList = new ArrayList<>();
				DtoTraningCourseClassSkill dtoTraningCourse=null;
				for (TraningCourseClassSkill traningCourse : traningCourseList) {
					dtoTraningCourse = new DtoTraningCourseClassSkill(traningCourse);
					
					dtoTraningCourse.setId(traningCourse.getId());
					dtoTraningCourse.setSkillArabicDesc(traningCourse.getSkillArabicDesc());
					dtoTraningCourse.setSkillDesc(traningCourse.getSkillDesc());
					dtoTraningCourse.setSkillSequnce(traningCourse.getSkillSequnce());
					if(dtoTraningCourse.getTrainingCourse()==null) {
						DtoTraningCourse course = new DtoTraningCourse();
						dtoTraningCourse.setTrainingCourse(course);
						if(traningCourse.getTrainingCourse()!=null) {
							dtoTraningCourse.getTrainingCourse().setId(traningCourse.getTrainingCourse().getId());
							dtoTraningCourse.getTrainingCourse().setTraningId(traningCourse.getTrainingCourse().getTraningId());
							dtoTraningCourse.getTrainingCourse().setDesc(traningCourse.getTrainingCourse().getDesc());
							dtoTraningCourse.getTrainingCourse().setArbicDesc(traningCourse.getTrainingCourse().getArbicDesc());
						}
						
					}
					
					if(dtoTraningCourse.getTrainingClass()==null) {
						DtoTrainingCourseDetail trainingClass = new DtoTrainingCourseDetail();
						dtoTraningCourse.setTrainingClass(trainingClass);
						if(traningCourse.getTrainingCourse()!=null) {
							dtoTraningCourse.getTrainingClass().setId(traningCourse.getTrainingClass().getId());
							dtoTraningCourse.getTrainingClass().setClassName(traningCourse.getTrainingClass().getClassName());
							dtoTraningCourse.getTrainingClass().setClassId(traningCourse.getTrainingClass().getClassId());
						}
						
					}
					
					List<DtoSkillSteup> dtoSkillSetSteup = new ArrayList<>();
					
					for (SkillsSetup skillSetSetup : traningCourse.getSkillsSetup()) {
						DtoSkillSteup dtoSkillSetSteup1 = new DtoSkillSteup();
						
							dtoSkillSetSteup1.setId(skillSetSetup.getId());
							dtoSkillSetSteup1.setSkillId(skillSetSetup.getSkillId());
							dtoSkillSetSteup1.setSkillDesc(skillSetSetup.getSkillDesc());
							dtoSkillSetSteup.add(dtoSkillSetSteup1);
						
					}
					dtoTraningCourse.setSkillsSetup(dtoSkillSetSteup);
					
					if(traningCourse.getSkillsSetup()!=null) {
					SkillSetSetup skillsSetup =	traningCourse.getSkillSetSetup();
					DtoSkillSetSteup dtoSkillSteup = new DtoSkillSetSteup();
					dtoSkillSteup.setId(skillsSetup.getId());
					dtoSkillSteup.setSkillSetId(skillsSetup.getSkillSetId());
					dtoSkillSteup.setSkillSetDiscription(skillsSetup.getSkillSetDiscription());
					
					dtoTraningCourse.setSkillSetSetup(dtoSkillSteup);
					
					}
					
					
					dtoTraningCourseList.add(dtoTraningCourse);
					}		
				dtoSearch.setRecords(dtoTraningCourseList);
				}
				
			}
		
		log.debug("Search TraningCourse Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
		}
	
	
	
	public DtoTraningCourseClassSkill getById(int id) {
		DtoTraningCourseClassSkill dtoTraningCourse  = new DtoTraningCourseClassSkill();
		if (id > 0) {
			TraningCourseClassSkill traningCourse = repositoryTraningCourseClassSkill.findByIdAndIsDeleted(id, false);
			if (traningCourse != null) {
				dtoTraningCourse = new DtoTraningCourseClassSkill(traningCourse);
				dtoTraningCourse.setId(traningCourse.getId());
				
				dtoTraningCourse.setId(traningCourse.getId());
				dtoTraningCourse.setSkillArabicDesc(traningCourse.getSkillArabicDesc());
				dtoTraningCourse.setSkillDesc(traningCourse.getSkillDesc());
				dtoTraningCourse.setSkillSequnce(traningCourse.getSkillSequnce());
				if(dtoTraningCourse.getTrainingCourse()==null) {
					DtoTraningCourse course = new DtoTraningCourse();
					dtoTraningCourse.setTrainingCourse(course);
					if(traningCourse.getTrainingCourse()!=null) {
						dtoTraningCourse.getTrainingCourse().setId(traningCourse.getTrainingCourse().getId());
						dtoTraningCourse.getTrainingCourse().setTraningId(traningCourse.getTrainingCourse().getTraningId());
					}
					
				}
				
				if(dtoTraningCourse.getTrainingClass()==null) {
					DtoTrainingCourseDetail trainingClass = new DtoTrainingCourseDetail();
					dtoTraningCourse.setTrainingClass(trainingClass);
					if(traningCourse.getTrainingCourse()!=null) {
						dtoTraningCourse.getTrainingClass().setId(traningCourse.getTrainingClass().getId());
						dtoTraningCourse.getTrainingClass().setClassName(traningCourse.getTrainingClass().getClassName());
					}
					
				}
				List<DtoSkillSteup> dtoSkillSetSteup = new ArrayList<>();
				
				for (SkillsSetup skillSetSetup : traningCourse.getSkillsSetup()) {
					DtoSkillSteup dtoSkillSetSteup1 = new DtoSkillSteup();
					
						dtoSkillSetSteup1.setId(skillSetSetup.getId());
						dtoSkillSetSteup1.setSkillId(skillSetSetup.getSkillId());
						dtoSkillSetSteup1.setSkillDesc(skillSetSetup.getSkillDesc());
						dtoSkillSetSteup.add(dtoSkillSetSteup1);
					
				}
				dtoTraningCourse.setSkillsSetup(dtoSkillSetSteup);
				
				if(traningCourse.getSkillsSetup()!=null) {
				SkillSetSetup skillsSetup =	traningCourse.getSkillSetSetup();
				DtoSkillSetSteup dtoSkillSteup = new DtoSkillSetSteup();
				dtoSkillSteup.setId(skillsSetup.getId());
				dtoSkillSteup.setSkillSetDiscription(skillsSetup.getSkillSetDiscription());
				
				dtoTraningCourse.setSkillSetSetup(dtoSkillSteup);
				}
				
			} else {
				dtoTraningCourse.setMessageType("TRAININGCOURSE_LIST_NOT_GETTING");

			}
		} else {
			dtoTraningCourse.setMessageType("TRAININGCOURSE_LIST_NOT_GETTING");

		}
		return dtoTraningCourse;
	}



//////
	
	
	
	public DtoSearch searchClassId(int id) {
		
		DtoSearch dtoSearch = new DtoSearch();
		List<DtoTraningCourseClassSkill> listdtoTraningCourse  = new ArrayList<>();
		if (id > 0) {
			TraningCourseClassSkill traningCourse = repositoryTraningCourseClassSkill.findByIdAndIsDeleted(id, false);
			if (traningCourse != null) {
				DtoTraningCourseClassSkill dtoTraningCourse = new DtoTraningCourseClassSkill(traningCourse);
				dtoTraningCourse.setId(traningCourse.getId());
				
				dtoTraningCourse.setId(traningCourse.getId());
				dtoTraningCourse.setSkillArabicDesc(traningCourse.getSkillArabicDesc());
				dtoTraningCourse.setSkillDesc(traningCourse.getSkillDesc());
				dtoTraningCourse.setSkillSequnce(traningCourse.getSkillSequnce());
				if(dtoTraningCourse.getTrainingCourse()==null) {
					DtoTraningCourse course = new DtoTraningCourse();
					dtoTraningCourse.setTrainingCourse(course);
					if(traningCourse.getTrainingCourse()!=null) {
						dtoTraningCourse.getTrainingCourse().setId(traningCourse.getTrainingCourse().getId());
						dtoTraningCourse.getTrainingCourse().setTraningId(traningCourse.getTrainingCourse().getTraningId());
					}
					
				}
				
				if(dtoTraningCourse.getTrainingClass()==null) {
					DtoTrainingCourseDetail trainingClass = new DtoTrainingCourseDetail();
					dtoTraningCourse.setTrainingClass(trainingClass);
					if(traningCourse.getTrainingCourse()!=null) {
						dtoTraningCourse.getTrainingClass().setId(traningCourse.getTrainingClass().getId());
						dtoTraningCourse.getTrainingClass().setClassName(traningCourse.getTrainingClass().getClassName());
					}
					
				}
				List<DtoSkillSteup> dtoSkillSetSteup = new ArrayList<>();
				
				for (SkillsSetup skillSetSetup : traningCourse.getSkillsSetup()) {
					DtoSkillSteup dtoSkillSetSteup1 = new DtoSkillSteup();
					
						dtoSkillSetSteup1.setId(skillSetSetup.getId());
						dtoSkillSetSteup1.setSkillId(skillSetSetup.getSkillId());
						dtoSkillSetSteup1.setSkillDesc(skillSetSetup.getSkillDesc());
						dtoSkillSetSteup.add(dtoSkillSetSteup1);
					
				}
				dtoTraningCourse.setSkillsSetup(dtoSkillSetSteup);
				
				if(traningCourse.getSkillsSetup()!=null) {
				SkillSetSetup skillsSetup =	traningCourse.getSkillSetSetup();
				DtoSkillSetSteup dtoSkillSteup = new DtoSkillSetSteup();
				dtoSkillSteup.setId(skillsSetup.getId());
				dtoSkillSteup.setSkillSetDiscription(skillsSetup.getSkillSetDiscription());
				
				dtoTraningCourse.setSkillSetSetup(dtoSkillSteup);
				listdtoTraningCourse.add(dtoTraningCourse);
				}
			}
			dtoSearch.setRecords(listdtoTraningCourse);
		}
		return dtoSearch;
	}

	
	
	
	
	
	
	
	
	
	
	
}
