/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Bank;
import com.bti.hcm.model.dto.DtoBank;
import com.bti.hcm.repository.RepositoryActivityLog;
import com.bti.hcm.repository.RepositoryBank;

/**
 * Description: Service Department Project: Hcm Version: 0.0.1
 */
@Service("serviceBank")
public class ServiceBank {

	/**
	 * @Description LOGGER use for put a logger in Department Service
	 */
	static Logger log = Logger.getLogger(ServiceBank.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in Department service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in Department service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in Department service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryDepartment Autowired here using annotation of spring
	 *              for access of repositoryDepartment method in Department service
	 *              In short Access Department Query from Database using
	 *              repositoryDepartment.
	 */
	@Autowired
	RepositoryBank repositoryBank;

	@Autowired
	RepositoryActivityLog repositoryActivityLog;

	@Autowired
	ServiceActivityLog serviceActivityLog;

	@Transactional
	@Async
	public List<DtoBank> getAllBankDropDownList() {
		log.info("getAllBankList  Method");
		List<DtoBank> dtoBankList = new ArrayList<>();
		try {
			List<Bank> list = repositoryBank.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (Bank bank : list) {
					DtoBank dtoBank = new DtoBank();
					dtoBank.setId(bank.getId());
					dtoBank.setBankDescription(bank.getBankDescription());
					dtoBank.setBankDescriptionArabic(bank.getBankDescriptionArabic());
					dtoBank.setBankDescriptionArabic(bank.getBankDescriptionArabic());
					dtoBankList.add(dtoBank);
				}
			}
			log.debug("Department is:" + dtoBankList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoBankList;
	}

}
