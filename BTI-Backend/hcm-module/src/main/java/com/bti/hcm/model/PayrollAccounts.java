package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR49999",indexes = {
        @Index(columnList = "RWPRLINDX")
})
public class PayrollAccounts extends HcmBaseEntity implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RWPRLINDX")
	private Integer id;

	@Column(name = "PRLACTTPY")
	private Short accountType;

	@Column(name = "PYROLINDX")
	private Integer payrollCode;

	@ManyToOne
	@JoinColumn(name = "DEPINDX")
	private Department department;

//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "ACTROWID")
//	private AccountsTableAccumulation accountsTableAccumulation;

//	@ManyToOne
//	@JoinColumn(name = "DIMINXD")
//	private FinancialDimensions financialDimensions;

	@ManyToOne
	@JoinColumn(name = "POTINDX")
	private Position position;

	@ManyToOne
	@JoinColumn(name = "LOCINDX")
	private Location location;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getAccountType() {
		return accountType;
	}

	public void setAccountType(Short accountType) {
		this.accountType = accountType;
	}

	public Integer getPayrollCode() {
		return payrollCode;
	}

	public void setPayrollCode(Integer payrollCode) {
		this.payrollCode = payrollCode;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public AccountsTableAccumulation getAccountsTableAccumulation() {
//		return accountsTableAccumulation;
		return null;
	}

	public void setAccountsTableAccumulation(AccountsTableAccumulation accountsTableAccumulation) {
//		this.accountsTableAccumulation = accountsTableAccumulation;
	}

	public FinancialDimensions getFinancialDimensions() {
//		return financialDimensions;
		return null;
	}

	public void setFinancialDimensions(FinancialDimensions financialDimensions) {
//		this.financialDimensions = financialDimensions;
	}

}
