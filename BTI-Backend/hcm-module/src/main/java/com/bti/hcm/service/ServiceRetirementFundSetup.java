package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.RetirementFundSetup;
import com.bti.hcm.model.dto.DtoRetirementFundSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryRetirementFundSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceRetirementFundSetup")
public class ServiceRetirementFundSetup {
	
	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceRetirementFundSetup.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryRetirementFundSetup repositoryRetirementFundSetup;
	
	public DtoRetirementFundSetup saveOrUpdateRetirementFundSetup(DtoRetirementFundSetup dtoRetirementFundSetup) {
	
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		RetirementFundSetup retirementFundSetup=null;
		if (dtoRetirementFundSetup.getId() != null && dtoRetirementFundSetup.getId() > 0) {
			retirementFundSetup = repositoryRetirementFundSetup.findByIdAndIsDeleted(dtoRetirementFundSetup.getId(), false);
			retirementFundSetup.setUpdatedBy(loggedInUserId);
			retirementFundSetup.setUpdatedDate(new Date());
		} else {
			retirementFundSetup = new RetirementFundSetup();
			retirementFundSetup.setCreatedDate(new Date());
			
			Integer rowId = repositoryRetirementFundSetup.getCountOfTotaRetirementFundSetup();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			retirementFundSetup.setRowId(increment);
		}
		
		retirementFundSetup.setPlanId(dtoRetirementFundSetup.getPlanId());
		retirementFundSetup.setFundId(dtoRetirementFundSetup.getFundId());
		retirementFundSetup.setFundSequence(dtoRetirementFundSetup.getFundSequence());
		retirementFundSetup.setPlanDescription(dtoRetirementFundSetup.getPlanDescription());
		retirementFundSetup.setFundActive(dtoRetirementFundSetup.getFundActive());
		retirementFundSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryRetirementFundSetup.saveAndFlush(retirementFundSetup);
		return dtoRetirementFundSetup;
	}

	

	public DtoRetirementFundSetup deleteRetirementFundSetup(List<Integer> ids) {
			log.info("delete RetirementFundSetup Method");
			DtoRetirementFundSetup dtoRetirementFundSetup = new DtoRetirementFundSetup();
			dtoRetirementFundSetup.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("RETIREMENTENTFUNDSETUP_DELETED", false));
			dtoRetirementFundSetup.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("RETIREMENTENTFUNDSETUP_ASSOCIATED", false));
			List<DtoRetirementFundSetup> deleteRetirementFundSetup = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("RETIREMENT_FUND_SETUP_NOT_DELETE_ID_MESSAGE", false).getMessage());
			try {
				for (Integer planId : ids) {
					RetirementFundSetup retirementFundSetup = repositoryRetirementFundSetup.findOne(planId);
					if(retirementFundSetup.getListRetirementPlanSetup().isEmpty()) {
						
						DtoRetirementFundSetup dtoRetirementFundSetup2 = new DtoRetirementFundSetup();
						
						
					     dtoRetirementFundSetup.setId(retirementFundSetup.getId());
					     dtoRetirementFundSetup.setPlanId(retirementFundSetup.getPlanId());
					     dtoRetirementFundSetup.setFundId(retirementFundSetup.getFundId());
					     dtoRetirementFundSetup.setFundSequence(retirementFundSetup.getFundSequence());
					     dtoRetirementFundSetup.setPlanDescription(retirementFundSetup.getPlanDescription());
					     dtoRetirementFundSetup.setFundActive(retirementFundSetup.getFundActive());
					     
						repositoryRetirementFundSetup.deleteSingleRetirementFundSetup(true, loggedInUserId, planId);
						deleteRetirementFundSetup.add(dtoRetirementFundSetup2);	
						
					}
					else{
						inValidDelete = true;
						invlidDeleteMessage.append(retirementFundSetup.getPlanId()+","); 
					}

				}
				
				if(inValidDelete){
					invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
					dtoRetirementFundSetup.setMessageType(invlidDeleteMessage.toString());
					
				}
				if(!inValidDelete){
					dtoRetirementFundSetup.setMessageType("");
					
				}
				
					
				dtoRetirementFundSetup.setDelete(deleteRetirementFundSetup);
			} 
			catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete RetirementFundSetup :"+dtoRetirementFundSetup.getId());
			return dtoRetirementFundSetup;
		}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchRetirementFundSetup(DtoSearch dtoSearch) {
		log.info("search RetirementFundSetup Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("fundId") || dtoSearch.getSortOn().equals("planDescription")|| dtoSearch.getSortOn().equals("fundSequence")|| dtoSearch.getSortOn().equals("fundActive")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
			
			dtoSearch.setTotalCount(this.repositoryRetirementFundSetup.predictiveRetirementFundSetupSearchTotalCount("%"+searchWord+"%"));
			List<RetirementFundSetup> retirementFundSetupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					retirementFundSetupList = this.repositoryRetirementFundSetup.predictiveRetirementFundSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					retirementFundSetupList = this.repositoryRetirementFundSetup.predictiveRetirementFundSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					retirementFundSetupList = this.repositoryRetirementFundSetup.predictiveRetirementFundSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			if(retirementFundSetupList != null && !retirementFundSetupList.isEmpty()){
				List<DtoRetirementFundSetup> dtoRetirementFundSetupList = new ArrayList<>();
				DtoRetirementFundSetup dtoRetirementFundSetup=null;
				for (RetirementFundSetup retirementFundSetup : retirementFundSetupList) {
					dtoRetirementFundSetup = new DtoRetirementFundSetup(retirementFundSetup);
					
					
					dtoRetirementFundSetup.setId(retirementFundSetup.getId());
					dtoRetirementFundSetup.setPlanId(retirementFundSetup.getPlanId());
					dtoRetirementFundSetup.setFundId(retirementFundSetup.getFundId());
					dtoRetirementFundSetup.setFundSequence(retirementFundSetup.getFundSequence());
					dtoRetirementFundSetup.setPlanDescription(retirementFundSetup.getPlanDescription());
					dtoRetirementFundSetup.setFundActive(retirementFundSetup.getFundActive());
					dtoRetirementFundSetupList.add(dtoRetirementFundSetup);
					}
					
				
				dtoSearch.setRecords(dtoRetirementFundSetupList);
				}
				
			}
		
		return dtoSearch;
		}

	public DtoRetirementFundSetup getRetirementFundSetupById(int id) {
		DtoRetirementFundSetup dtoRetirementFundSetup  = new DtoRetirementFundSetup();
		if (id > 0) {
			RetirementFundSetup retirementFundSetup = repositoryRetirementFundSetup.findByIdAndIsDeleted(id, false);
			if (retirementFundSetup != null) {
				dtoRetirementFundSetup = new DtoRetirementFundSetup(retirementFundSetup);
			} else {
				dtoRetirementFundSetup.setMessageType("RETIREMENTFUNDSETUP_NOT_GETTING");

			}
		} else {
			dtoRetirementFundSetup.setMessageType("INVALID_RETIREMENTPLAN_ID");

		}
		return dtoRetirementFundSetup;
	}

	public DtoRetirementFundSetup repeatByRetirementPlanId(Integer planId) {
		log.info("repeatByRetirementPlanId Method");
		DtoRetirementFundSetup dtoRetirementFundSetup = new DtoRetirementFundSetup();
		try {
			List<RetirementFundSetup> retirementFundSetup=repositoryRetirementFundSetup.findByRetirementPlanId(planId);
			if(retirementFundSetup!=null && !retirementFundSetup.isEmpty()) {
				dtoRetirementFundSetup.setIsRepeat(true);
				dtoRetirementFundSetup.setPlanId(planId);
			}else {
				dtoRetirementFundSetup.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoRetirementFundSetup;
	}

}
