package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "HR40111")
public class Values extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = -7663258364681124485L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "VALUEINDX")
	private int id;

	@Column(name = "VALUEID", columnDefinition = "char(120)")
	private String valueId;

	@Column(name = "VALUEDESCR", columnDefinition = "char(120)")
	private String valueDescription;

	@Column(name = "VALUEARDESCR", columnDefinition = "char(120)")
	private String valueArabicDescription;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "MISCINDX")
	private Miscellaneous miscellaneous;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<EmployeeMaster> employeeMaster;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValueId() {
		return valueId;
	}

	public void setValueId(String valueId) {
		this.valueId = valueId;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	public String getValueArabicDescription() {
		return valueArabicDescription;
	}

	public void setValueArabicDescription(String valueArabicDescription) {
		this.valueArabicDescription = valueArabicDescription;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

}
