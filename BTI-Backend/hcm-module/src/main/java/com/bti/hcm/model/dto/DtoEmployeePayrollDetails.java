package com.bti.hcm.model.dto;

import java.math.BigDecimal;

public class DtoEmployeePayrollDetails {
	
	private String wps;
	private String sequnce;
	
	private String employeeCode;
	private String employeeName;
	private String employeeNameArabic;
	
	private String employeeIBAN;
	private String otherBank;
	private String swiftCode;
	private String country;
	private String currency;
	private String transferType;
	private String transferDest;
	
	private String employeeIdNumber;
	
	private BigDecimal basic = new BigDecimal(0.0);
	private BigDecimal housing = new BigDecimal(0.0);

//	private BigDecimal benefits = new BigDecimal(0.0);
//	private BigDecimal dedudctions = new BigDecimal(0.0);
	

	private BigDecimal transportation = new BigDecimal(0.0);
	private BigDecimal othersBenefits1 = new BigDecimal(0.0);
	private BigDecimal fixed = new BigDecimal(0.0);
	private BigDecimal overtime = new BigDecimal(0.0);
	private BigDecimal bonus = new BigDecimal(0.0);
	private BigDecimal othersBenefits2 = new BigDecimal(0.0);
	
	private BigDecimal loan = new BigDecimal(0.0);
	private BigDecimal absent = new BigDecimal(0.0);
	private BigDecimal gosi = new BigDecimal(0.0);
	private BigDecimal otherDeductions = new BigDecimal(0.0);
	
	private String strPeriodFromDate = "";
	private String strPeriodToDate = "";
	private String printedDate = "";
	
	
	private DtoEmployeeMaster dtoEmployeeMaster;
	
	

	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	
	
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	public String getEmployeeNameArabic() {
		return employeeNameArabic;
	}
	public void setEmployeeNameArabic(String employeeNameArabic) {
		this.employeeNameArabic = employeeNameArabic;
	}
	public String getEmployeeIdNumber() {
		return employeeIdNumber;
	}
	public void setEmployeeIdNumber(String employeeIdNumber) {
		this.employeeIdNumber = employeeIdNumber;
	}

	
	public BigDecimal getTotalIncome() {
		BigDecimal totalIncome = new BigDecimal(0.0);
		totalIncome = this.basic.add(this.housing);
		totalIncome = totalIncome.add(this.transportation);
		totalIncome = totalIncome.add(this.othersBenefits1);

		totalIncome = totalIncome.add(this.fixed);
		totalIncome = totalIncome.add(this.overtime);
		totalIncome = totalIncome.add(this.bonus);
		totalIncome = totalIncome.add(this.othersBenefits2);
		
		return totalIncome;
	}
	
	public BigDecimal getTotalDeductions() {
		BigDecimal totalDeductions = new BigDecimal(0.0);
		totalDeductions = this.loan.add(this.absent);
		totalDeductions = totalDeductions.add(this.gosi);
		totalDeductions = totalDeductions.add(this.otherDeductions);
		
		return totalDeductions;
	}
	
	public BigDecimal getNetPay() {
		BigDecimal netPay = new BigDecimal(0.0);
		BigDecimal totalIncome = this.getTotalIncome();
		BigDecimal totalDeductions = this.getTotalDeductions();
		netPay = totalIncome.subtract(totalDeductions);
		return netPay;
	}
	
	public boolean isTotalPayPositive() {
		if (getNetPay().doubleValue() > 0.0) {
			return true;
		} else { 
			return false;
		}
	}
	
	public BigDecimal getBasic() {
		return basic;
	}
	public void setBasic(BigDecimal basic) {
		this.basic = basic;
	}
	public BigDecimal getHousing() {
		return housing;
	}
	public void setHousing(BigDecimal housing) {
		this.housing = housing;
	}
	public BigDecimal getBenefitsForBankReport() {
//		return benefits;
//		BigDecimal benefitsForBank = this.getTotalIncome().subtract(this.basic);
//		benefitsForBank = benefitsForBank.subtract(this.housing);

		BigDecimal benefitsForBankReport = new BigDecimal(0.0);		
				
		benefitsForBankReport = benefitsForBankReport.add(this.transportation);
		benefitsForBankReport = benefitsForBankReport.add(this.othersBenefits1);
		benefitsForBankReport = benefitsForBankReport.add(this.fixed);
		benefitsForBankReport = benefitsForBankReport.add(this.overtime);
		benefitsForBankReport = benefitsForBankReport.add(this.bonus);
		benefitsForBankReport = benefitsForBankReport.add(this.othersBenefits2);
		
		return benefitsForBankReport;
	}
//	public void setBenefits(BigDecimal benefits) {
//		this.benefits = benefits;
//	}
//	public BigDecimal getDedudctions() {
//		return dedudctions;
//	}
//	public void setDedudctions(BigDecimal dedudctions) {
//		this.dedudctions = dedudctions;
//	}
	public String getWps() {
		return wps;
	}
	public void setWps(String wps) {
		this.wps = wps;
	}
	public String getSequnce() {
		return sequnce;
	}
	public void setSequnce(String sequnce) {
		this.sequnce = sequnce;
	}
	public String getEmployeeIBAN() {
		return employeeIBAN;
	}
	public void setEmployeeIBAN(String employeeIBAN) {
		this.employeeIBAN = employeeIBAN;
	}
	public String getOtherBank() {
		return otherBank;
	}
	public void setOtherBank(String otherBank) {
		this.otherBank = otherBank;
	}
	public String getSwiftCode() {
		return swiftCode;
	}
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTransferType() {
		return transferType;
	}
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	public String getTransferDest() {
		return transferDest;
	}
	public void setTransferDest(String transferDest) {
		this.transferDest = transferDest;
	}
	public DtoEmployeeMaster getDtoEmployeeMaster() {
		return dtoEmployeeMaster;
	}
	public void setDtoEmployeeMaster(DtoEmployeeMaster dtoEmployeeMaster) {
		this.dtoEmployeeMaster = dtoEmployeeMaster;
	}
	public BigDecimal getTransportation() {
		return transportation;
	}
	public void setTransportation(BigDecimal transportation) {
		this.transportation = transportation;
	}
	public BigDecimal getOthersBenefits1() {
		return othersBenefits1;
	}
	public void setOthersBenefits1(BigDecimal othersBenefits1) {
		this.othersBenefits1 = othersBenefits1;
	}
	public BigDecimal getFixed() {
		return fixed;
	}
	public void setFixed(BigDecimal fixed) {
		this.fixed = fixed;
	}
	public BigDecimal getOvertime() {
		return overtime;
	}
	public void setOvertime(BigDecimal overtime) {
		this.overtime = overtime;
	}
	public BigDecimal getBonus() {
		return bonus;
	}
	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}
	public BigDecimal getOthersBenefits2() {
		return othersBenefits2;
	}
	public void setOthersBenefits2(BigDecimal othersBenefits2) {
		this.othersBenefits2 = othersBenefits2;
	}
	public BigDecimal getLoan() {
		return loan;
	}
	public void setLoan(BigDecimal loan) {
		this.loan = loan;
	}
	public BigDecimal getAbsent() {
		return absent;
	}
	public void setAbsent(BigDecimal absent) {
		this.absent = absent;
	}
	public BigDecimal getGosi() {
		return gosi;
	}
	public void setGosi(BigDecimal gosi) {
		this.gosi = gosi;
	}
	public BigDecimal getOtherDeductions() {
		return otherDeductions;
	}
	public void setOtherDeductions(BigDecimal otherDeductions) {
		this.otherDeductions = otherDeductions;
	}
	public String getStrPeriodFromDate() {
		return strPeriodFromDate;
	}
	public void setStrPeriodFromDate(String strPeriodFromDate) {
		this.strPeriodFromDate = strPeriodFromDate;
	}
	public String getStrPeriodToDate() {
		return strPeriodToDate;
	}
	public void setStrPeriodToDate(String strPeriodToDate) {
		this.strPeriodToDate = strPeriodToDate;
	}
	public void setPrintedDate(String printedDate) {
		this.printedDate = printedDate;
	}
	public String getPrintedDate() {
		return this.printedDate;
	}

	
	public void addToBasic(BigDecimal val) {
		if (this.basic == null) {
			this.basic = new BigDecimal(0.0);
		}
		this.basic = this.basic.add(val);
	}
	
	public void addToHousing(BigDecimal val) {
		if (this.housing == null) {
			this.housing = new BigDecimal(0.0);
		}
		this.housing = this.housing.add(val);
	}

	public void addToTransportation(BigDecimal val) {
		if (this.transportation == null) {
			this.transportation = new BigDecimal(0.0);
		}
		this.transportation = this.transportation.add(val);
	}
	
	public void addToOthersBenefits1(BigDecimal val) {
		if (this.othersBenefits1 == null) {
			this.othersBenefits1 = new BigDecimal(0.0);
		}
		this.othersBenefits1 = this.othersBenefits1.add(val);
	}
	
	public void addToFixed(BigDecimal val) {
		if (this.fixed == null) {
			this.fixed = new BigDecimal(0.0);
		}
		this.fixed = this.fixed.add(val);
	}
	
	public void addToOvertime(BigDecimal val) {
		if (this.overtime == null) {
			this.overtime = new BigDecimal(0.0);
		}
		this.overtime = this.overtime.add(val);
	}
	
	public void addToBonus(BigDecimal val) {
		if (this.bonus == null) {
			this.bonus = new BigDecimal(0.0);
		}
		this.bonus = this.bonus.add(val);
	}

	public void addToOthersBenefits2(BigDecimal val) {
		if (this.othersBenefits2 == null) {
			this.othersBenefits2 = new BigDecimal(0.0);
		}
		this.othersBenefits2 = this.othersBenefits2.add(val);
	}

	public void addToLoan(BigDecimal val) {
		if (this.loan == null) {
			this.loan = new BigDecimal(0.0);
		}
		this.loan = this.loan.add(val);
	}
	
	public void addToAbsent(BigDecimal val) {
		if (this.absent == null) {
			this.absent = new BigDecimal(0.0);
		}
		this.absent = this.absent.add(val);
	}
	
	public void addToGosi(BigDecimal val) {
		if (this.gosi == null) {
			this.gosi = new BigDecimal(0.0);
		}
		this.gosi = this.gosi.add(val);
	}
	
	public void addToOtherDeductions(BigDecimal val) {
		if (this.otherDeductions == null) {
			this.otherDeductions = new BigDecimal(0.0);
		}
		this.otherDeductions = this.otherDeductions.add(val);
	}
	
}
