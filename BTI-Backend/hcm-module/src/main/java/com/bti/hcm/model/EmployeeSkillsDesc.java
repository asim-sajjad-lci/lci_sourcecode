package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00110",indexes = {
        @Index(columnList = "EMPSKINDXD")
})
public class EmployeeSkillsDesc extends HcmBaseEntity implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPSKINDXD")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "EMPSKINDX")
	private EmployeeSkills employeeSkills;
	
	@Column(name = "SKLSTSEQN")
	private Integer skillSetSequence;
	
	@ManyToOne
	@JoinColumn(name = "SKLINDX")
	private SkillsSetup skillsSetup;
	
	@Column(name = "SKLOBTAIN")
	private boolean obtained;
	
	@Column(name = "SKLO")
	private boolean isSelected;
	
	@Column(name = "SKLPROF")
	private Integer proficiency;
	
	@Column(name = "SKLSTREQ")
	private boolean required;
	
	@Column(name = "Comment")
	private String skillComment;
	
	@Column(name = "SKLEXPDT")
	private Date skillExpirationDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmployeeSkills getEmployeeSkills() {
		return employeeSkills;
	}

	public void setEmployeeSkills(EmployeeSkills employeeSkills) {
		this.employeeSkills = employeeSkills;
	}

	public Integer getSkillSetSequence() {
		return skillSetSequence;
	}

	public void setSkillSetSequence(Integer skillSetSequence) {
		this.skillSetSequence = skillSetSequence;
	}

	public SkillsSetup getSkillsSetup() {
		return skillsSetup;
	}

	public void setSkillsSetup(SkillsSetup skillsSetup) {
		this.skillsSetup = skillsSetup;
	}

	public boolean isObtained() {
		return obtained;
	}

	public void setObtained(boolean obtained) {
		this.obtained = obtained;
	}

	public Integer getProficiency() {
		return proficiency;
	}

	public void setProficiency(Integer proficiency) {
		this.proficiency = proficiency;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getSkillComment() {
		return skillComment;
	}

	public void setSkillComment(String skillComment) {
		this.skillComment = skillComment;
	}

	public Date getSkillExpirationDate() {
		return skillExpirationDate;
	}

	public void setSkillExpirationDate(Date skillExpirationDate) {
		this.skillExpirationDate = skillExpirationDate;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	
	
}
