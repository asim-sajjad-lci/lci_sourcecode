package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.RetirementEntranceDate;
import com.bti.hcm.model.dto.DtoRetirementEntranceDate;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryRetirementEntranceDate;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceRetirementEntranceDate")
public class ServiceRetirementEntranceDate {
	
	
	
	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementEntranceDate service
	 */
	static Logger log = Logger.getLogger(ServiceRetirementEntranceDate.class.getName());
	
	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementEntranceDate service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementEntranceDate service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryRetirementEntranceDate repositoryRetirementEntranceDate;
	
	public DtoRetirementEntranceDate saveOrUpdateRetirementPlanSetup(DtoRetirementEntranceDate dtoRetirementEntranceDate) {
	
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		RetirementEntranceDate retirementEntranceDate=null;
		if (dtoRetirementEntranceDate.getId() != null && dtoRetirementEntranceDate.getId() > 0) {
			retirementEntranceDate = repositoryRetirementEntranceDate.findByIdAndIsDeleted(dtoRetirementEntranceDate.getId(), false);
			retirementEntranceDate.setUpdatedBy(loggedInUserId);
			retirementEntranceDate.setUpdatedDate(new Date());
		} else {
			retirementEntranceDate = new RetirementEntranceDate();
			retirementEntranceDate.setCreatedDate(new Date());
			
			Integer rowId = repositoryRetirementEntranceDate.getCountOfTotaRetirementEntranceDate();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			retirementEntranceDate.setRowId(increment);
		}
		retirementEntranceDate.setDateSequence(dtoRetirementEntranceDate.getDateSequence());
		retirementEntranceDate.setPlanId(dtoRetirementEntranceDate.getPlanId());
		retirementEntranceDate.setEntranceDescription(dtoRetirementEntranceDate.getEntranceDescription());
		retirementEntranceDate.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryRetirementEntranceDate.saveAndFlush(retirementEntranceDate);
		return dtoRetirementEntranceDate;
	}
	public DtoRetirementEntranceDate deleteRetirementEntranceDate(List<Integer> ids) {
			log.info("delete RetirementEntranceDate Method");
			DtoRetirementEntranceDate dtoRetirementEntranceDate = new DtoRetirementEntranceDate();
			dtoRetirementEntranceDate.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("RETIREMENTENTRANCEDATE_DELETED", false));
			dtoRetirementEntranceDate.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("RETIREMENTENTRANCEDATE_ASSOCIATED", false));
			List<DtoRetirementEntranceDate> deleteRetirementEntranceDate = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("RETIREMENT_ENTRANCE_NOT_DELETE_ID_MESSAGE", false).getMessage());
			try {
				for (Integer planId : ids) {
					RetirementEntranceDate retirementEntranceDate = repositoryRetirementEntranceDate.findOne(planId);
					if(retirementEntranceDate.getListRetirementPlanSetup().isEmpty()) {
						DtoRetirementEntranceDate dtoRetirementEntranceDate2 = new DtoRetirementEntranceDate();
				        dtoRetirementEntranceDate.setId(retirementEntranceDate.getId());
					    dtoRetirementEntranceDate.setDateSequence(retirementEntranceDate.getDateSequence());
					    dtoRetirementEntranceDate.setPlanId(retirementEntranceDate.getPlanId());
					    dtoRetirementEntranceDate.setEntranceDescription(retirementEntranceDate.getEntranceDescription());
						repositoryRetirementEntranceDate.deleteSingleRetirementEntranceDate(true, loggedInUserId, planId);
						deleteRetirementEntranceDate.add(dtoRetirementEntranceDate2);	
					
					}
					else
					{
						inValidDelete = true;
						invlidDeleteMessage.append(retirementEntranceDate.getPlanId()+","); 
					}

				}
				
				if(inValidDelete){
					invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
					dtoRetirementEntranceDate.setMessageType(invlidDeleteMessage.toString());
					
				}
				if(!inValidDelete){
					dtoRetirementEntranceDate.setMessageType("");
					
				}
				
				dtoRetirementEntranceDate.setDelete(deleteRetirementEntranceDate);
			} catch (NumberFormatException e) {
				log.error(e);
			}
			log.debug("Delete RetirementEntranceDate :"+dtoRetirementEntranceDate.getId());
			return dtoRetirementEntranceDate;
		}
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchRetirementEntranceDate(DtoSearch dtoSearch) {
		log.info("search RetirementEntranceDate Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("dateSequence") || dtoSearch.getSortOn().equals("planId") || dtoSearch.getSortOn().equals("entranceDescription")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
			
			dtoSearch.setTotalCount(this.repositoryRetirementEntranceDate.predictiveRetirementEntranceDateSearchTotalCount("%"+searchWord+"%"));
			List<RetirementEntranceDate> retirementEntranceDateList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					retirementEntranceDateList = this.repositoryRetirementEntranceDate.predictiveRetirementEntranceDateSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					retirementEntranceDateList = this.repositoryRetirementEntranceDate.predictiveRetirementEntranceDateSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					retirementEntranceDateList = this.repositoryRetirementEntranceDate.predictiveRetirementEntranceDateSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			if(retirementEntranceDateList != null && !retirementEntranceDateList.isEmpty()){
				List<DtoRetirementEntranceDate> dtoRetirementEntranceDateList = new ArrayList<>();
				DtoRetirementEntranceDate dtoRetirementEntranceDate=null;
				for (RetirementEntranceDate retirementEntranceDate : retirementEntranceDateList) {
					dtoRetirementEntranceDate = new DtoRetirementEntranceDate(retirementEntranceDate);
					
					dtoRetirementEntranceDate.setId(retirementEntranceDate.getId());
					dtoRetirementEntranceDate.setDateSequence(retirementEntranceDate.getDateSequence());
					dtoRetirementEntranceDate.setPlanId(retirementEntranceDate.getPlanId());
					dtoRetirementEntranceDate.setEntranceDescription(retirementEntranceDate.getEntranceDescription());
					}
					
				dtoRetirementEntranceDateList.add(dtoRetirementEntranceDate);
				dtoSearch.setRecords(retirementEntranceDateList);
				}
				
			}
		
		return dtoSearch;
		}
	
	
	
	
	public DtoRetirementEntranceDate getRetirementEntranceDateById(int id) {
		DtoRetirementEntranceDate dtoRetirementEntranceDate  = new DtoRetirementEntranceDate();
		if (id > 0) {
			RetirementEntranceDate retirementEntranceDate = repositoryRetirementEntranceDate.findByIdAndIsDeleted(id, false);
			if (retirementEntranceDate != null) {
				dtoRetirementEntranceDate = new DtoRetirementEntranceDate(retirementEntranceDate);
			} else {
				dtoRetirementEntranceDate.setMessageType("RETIREMENTPLANSETUP_NOT_GETTING");

			}
		} else {
			dtoRetirementEntranceDate.setMessageType("INVALID_RETIREMENTPLAN_ID");

		}
		return dtoRetirementEntranceDate;
	}

	public DtoRetirementEntranceDate repeatByRetirementPlanId(Integer planId) {
		log.info("repeatByRetirementPlanId Method");
		DtoRetirementEntranceDate dtoRetirementEntranceDate = new DtoRetirementEntranceDate();
		try {
			List<RetirementEntranceDate> retirementEntranceDate=repositoryRetirementEntranceDate.findByRetirementPlanId(planId);
			if(retirementEntranceDate!=null && !retirementEntranceDate.isEmpty()) {
				dtoRetirementEntranceDate.setIsRepeat(true);
				dtoRetirementEntranceDate.setPlanId(planId);
			}else {
				dtoRetirementEntranceDate.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoRetirementEntranceDate;
	}
	
	
	
	
	
	

}
