package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AtteandaceOptionType;

@Repository("repositoryAtteandaceOptionType")
public interface RepositoryAtteandaceOptionType extends JpaRepository<AtteandaceOptionType, Integer>{

	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public AtteandaceOptionType findByIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<AtteandaceOptionType> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from AtteandaceOptionType a where a.isDeleted=false")
	public Integer getCountOfTotalAtteandaceOptionType();
	
	/**
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<AtteandaceOptionType> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update AtteandaceOptionType a set a.isDeleted =:deleted, a.updatedBy=:updateById where a.id IN (:idList)")
	public void deleteAtteandaceOptionType(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update AtteandaceOptionType a set a.isDeleted =:deleted ,a.updatedBy =:updateById where a.id =:id ")
	public void deleteSingleAtteandaceOptionType(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public AtteandaceOptionType findTop1ByOrderByIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select a from AtteandaceOptionType a where (a.reasonDesc like :searchKeyWord  or a.atteandanceTypeDesc like :searchKeyWord ) and a.isDeleted=false")
	public List<AtteandaceOptionType> predictiveAtteandaceOptionTypeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<AtteandaceOptionType> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from AtteandaceOptionType a where (a.reasonDesc like :searchKeyWord  or a.atteandanceTypeDesc like :searchKeyWord) and a.isDeleted=false")
	public Integer predictiveAtteandaceOptionTypeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select a from AtteandaceOptionType a where (a.reasonDesc like :searchKeyWord  or a.atteandanceTypeDesc like :searchKeyWord) and a.isDeleted=false")
	public List<AtteandaceOptionType> predictiveAtteandaceOptionTypeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	@Query("select count(*) from AtteandaceOptionType a ")
	public Integer getCountOfTotalAtteandaces();
}
