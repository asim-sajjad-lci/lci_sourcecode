package com.bti.hcm.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "hr10601")
public class EmployeeMonthlyInstallment extends HcmBaseEntity2 {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idx")
	private Integer id;
	
	@Column(name ="mnthyr")
	private Date monthYear;
	
	@Column(name = "amt")
	private Double amount;
	
	@Column(name = "pstpn")
	private Boolean isPostponed;
	
	@Column(name = "ispaid")
	private Boolean isPaid;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "reqidx")
	private EmployeeLoanEntry employeeLoanEntry;
	

	
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Date getMonthYear() {
		return monthYear;
	}


	public void setMonthYear(Date monthYear) {
		this.monthYear = monthYear;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public Boolean getIsPostponed() {
		return isPostponed;
	}


	public void setIsPostponed(Boolean isPostponed) {
		this.isPostponed = isPostponed;
	}


	public Boolean getIsPaid() {
		return isPaid;
	}


	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}


	public EmployeeLoanEntry getEmployeeLoanEntry() {
		return employeeLoanEntry;
	}


	public void setEmployeeLoanEntry(EmployeeLoanEntry employeeLoanEntry) {
		this.employeeLoanEntry = employeeLoanEntry;
	}

	


}
