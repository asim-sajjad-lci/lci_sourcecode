package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00201",indexes = {
        @Index(columnList = "PSTDTINDX")
})
public class EmployeePostDatedPayRates extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PSTDTINDX")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;
	
	@ManyToOne
	@JoinColumn(name = "PYCDINDX")
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	private PayCode payCode;
	
	@Column(name = "PYCURRTE", precision = 13,scale = 3) 
	private BigDecimal currentPayRate;
	
	@Column(name = "PYNEWRTE" , precision = 13,scale = 3) 
	private BigDecimal newPayRate;
	
	@Column(name = "PYEFFTDT") 
	private Date effectiveDate;
	
	@Column(name = "PYRSNCH") 
	private String reasonforChange;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public PayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(PayCode payCode) {
		this.payCode = payCode;
	}

	

	public BigDecimal getCurrentPayRate() {
		return currentPayRate;
	}

	public void setCurrentPayRate(BigDecimal currentPayRate) {
		this.currentPayRate = currentPayRate;
	}

	public BigDecimal getNewPayRate() {
		return newPayRate;
	}

	public void setNewPayRate(BigDecimal newPayRate) {
		this.newPayRate = newPayRate;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getReasonforChange() {
		return reasonforChange;
	}

	public void setReasonforChange(String reasonforChange) {
		this.reasonforChange = reasonforChange;
	}
	
	
	
}
