package com.bti.hcm.model.dto;

public class DtoBenefitCodeDisplay {
	private Integer benifitCodePrimaryId;
	private String benefitId;
	private Integer buildChecksId;

	public Integer getBenifitCodePrimaryId() {
		return benifitCodePrimaryId;
	}

	public void setBenifitCodePrimaryId(Integer benifitCodePrimaryId) {
		this.benifitCodePrimaryId = benifitCodePrimaryId;
	}

	public String getBenefitId() {
		return benefitId;
	}

	public void setBenefitId(String benefitId) {
		this.benefitId = benefitId;
	}

	public Integer getBuildChecksId() {
		return buildChecksId;
	}

	public void setBuildChecksId(Integer buildChecksId) {
		this.buildChecksId = buildChecksId;
	}

}
