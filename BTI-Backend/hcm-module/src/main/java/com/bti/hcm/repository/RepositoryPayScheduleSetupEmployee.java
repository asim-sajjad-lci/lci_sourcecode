/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayScheduleSetupEmployee;

/**
 * Description: Interface for PayScheduleSetupEmployee 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryPayScheduleSetupEmployee")
public interface RepositoryPayScheduleSetupEmployee extends JpaRepository<PayScheduleSetupEmployee, Integer>{

	/**
	 * 
	 * @param paySchedulEmployeeIndexId
	 * @param deleted
	 * @return
	 */
	public PayScheduleSetupEmployee findBypaySchedulEmployeeIndexIdAndIsDeleted(int paySchedulEmployeeIndexId, boolean deleted);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupEmployee> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupEmployee p where p.isDeleted=false")
	public Integer getCountOfTotalPayScheduleSetupEmployee();
	
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<PayScheduleSetupEmployee> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupEmployee d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deletePayScheduleSetupEmployee(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupEmployee d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSinglPayScheduleSetupEmployee(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
	/**
	 * 
	 * @return
	 */
	public PayScheduleSetupEmployee findTop1ByOrderByPaySchedulEmployeeIndexIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select p from PayScheduleSetupEmployee p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.employeeMaster.employeeId like :searchKeyWord or p.employeeFullName like :searchKeyWord ) and p.isDeleted=false")
	public List<PayScheduleSetupEmployee> predictivePayScheduleSetupEmployeeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupEmployee> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupEmployee p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.employeeMaster.employeeId like :searchKeyWord or p.employeeFullName like :searchKeyWord  ) and p.isDeleted=false")
	public Integer predictivePayScheduleSetupEmployeeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select p from PayScheduleSetupEmployee p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.employeeMaster.employeeId like :searchKeyWord or p.employeeFullName like :searchKeyWord   ) and p.isDeleted=false")
	public List<PayScheduleSetupEmployee> predictivePayScheduleSetupEmployeeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param payScheduleDepartmentIndexId
	 * @return
	 */
	@Query("select p from PayScheduleSetupEmployee p where (p.paySchedulEmployeeIndexId =:paySchedulEmployeeIndexId) and p.isDeleted=false")
	public List<PayScheduleSetupEmployee> findByPaySchedulEmployeeIndexId(@Param("paySchedulEmployeeIndexId")Integer payScheduleDepartmentIndexId);

	
	@Query("select p from PayScheduleSetupEmployee p where (p.payScheduleSetup.payScheduleIndexId =:payScheduleIndexId) and p.isDeleted=false")
	public List<PayScheduleSetupEmployee> findByPayScheduleSetup(@Param("payScheduleIndexId")Integer payScheduleIndexId);
}
