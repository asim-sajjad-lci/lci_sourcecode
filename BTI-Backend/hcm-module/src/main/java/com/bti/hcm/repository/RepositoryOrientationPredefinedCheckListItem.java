/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.OrientationPredefinedCheckListItem;

/**
 * Description: Interface for OrientationPredefinedCheckListItem 
 * Name of Project: Hcm
 * Version: 0.0.1	
 */
@Repository("repositoryOrientationPredefinedCheckListItem")
public interface RepositoryOrientationPredefinedCheckListItem extends JpaRepository<OrientationPredefinedCheckListItem, Integer>{

	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public OrientationPredefinedCheckListItem findByIdAndIsDeleted(int id, boolean deleted);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<OrientationPredefinedCheckListItem> findByIsDeleted(Boolean deleted);
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from OrientationPredefinedCheckListItem o where o.isDeleted=false")
	public Integer getCountOfTotalOrientationPredefinedCheckListItem();
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<OrientationPredefinedCheckListItem> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update OrientationPredefinedCheckListItem o set o.isDeleted =:deleted, o.updatedBy=:updateById where o.id IN (:idList)")
	public void deleteOrientationPredefinedCheckListItem(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update OrientationPredefinedCheckListItem o set o.isDeleted =:deleted ,o.updatedBy =:updateById where o.id =:id ")
	public void deleteSingleOrientationPredefinedCheckListItem(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public OrientationPredefinedCheckListItem findTop1ByOrderByIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select o from OrientationPredefinedCheckListItem o where (o.preDefineCheckListItemDescription like :searchKeyWord  or o.preDefineCheckListItemDescriptionArabic like :searchKeyWord ) and o.isDeleted=false")
	public List<OrientationPredefinedCheckListItem> predictiveOrientationPredefinedCheckListItemSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<OrientationPredefinedCheckListItem> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from OrientationPredefinedCheckListItem o where (o.preDefineCheckListItemDescription like :searchKeyWord  or o.preDefineCheckListItemDescriptionArabic like :searchKeyWord ) and o.isDeleted=false")
	public Integer predictiveOrientationPredefinedCheckListItemSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select o from OrientationPredefinedCheckListItem o where (o.preDefineCheckListItemDescription like :searchKeyWord  or o.preDefineCheckListItemDescriptionArabic like :searchKeyWord ) and o.isDeleted=false")
	public List<OrientationPredefinedCheckListItem> predictiveOrientationPredefinedCheckListItemSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select o from OrientationPredefinedCheckListItem o where (o.id =:id) and o.isDeleted=false")
	public List<OrientationPredefinedCheckListItem> findById(@Param("id")int id);
	
	@Query("select o from OrientationPredefinedCheckListItem o where (o.preDefineCheckListType =:type) and o.isDeleted=false and (o.preDefineCheckListItemDescription is not null and o.preDefineCheckListItemDescriptionArabic is not null)")
	public List<OrientationPredefinedCheckListItem> getAllOriation(@Param("type") short type);
	
	@Query("select count(*) from OrientationPredefinedCheckListItem o ")
	public Integer getCountOfTotaOrientationPredefinedCheckListItem();

	
	
}
