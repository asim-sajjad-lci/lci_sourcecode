package com.bti.hcm.model.dto;

import java.util.List;

public class DtoFinancialDimensionsValues extends DtoBase {

	private Integer id;
	private String dimensionValue;
	private String dimensionDescription;
	private String dimensionArbicDescription;

	private DtoFinancialDimensions financialDimensions;
	private List<DtoFinancialDimensions> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public String getDimensionDescription() {
		return dimensionDescription;
	}

	public void setDimensionDescription(String dimensionDescription) {
		this.dimensionDescription = dimensionDescription;
	}

	public String getDimensionArbicDescription() {
		return dimensionArbicDescription;
	}

	public void setDimensionArbicDescription(String dimensionArbicDescription) {
		this.dimensionArbicDescription = dimensionArbicDescription;
	}

	public List<DtoFinancialDimensions> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoFinancialDimensions> delete) {
		this.delete = delete;
	}

	public DtoFinancialDimensions getFinancialDimensions() {
		return financialDimensions;
	}

	public void setFinancialDimensions(DtoFinancialDimensions financialDimensions) {
		this.financialDimensions = financialDimensions;
	}

}
