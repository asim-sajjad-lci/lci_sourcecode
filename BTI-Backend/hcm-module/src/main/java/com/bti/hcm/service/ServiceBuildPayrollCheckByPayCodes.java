package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.BuildChecks;
import com.bti.hcm.model.BuildPayrollCheckByPayCodes;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByPayCodes;
import com.bti.hcm.model.dto.DtoEmployeePayCodeMaintenance;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoPayCodeDisplay;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByPayCodes;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;

@Service("/serviceBuildPayrollCheckByPayCodes")
public class ServiceBuildPayrollCheckByPayCodes {

	static Logger log = Logger.getLogger(ServiceBuildPayrollCheckByPayCodes.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired(required = false)
	RepositoryBuildPayrollCheckByPayCodes repositoryBuildPayrollCheckByPayCodes;
	
	@Autowired(required = false)
	RepositoryPayCode repositoryPayCode;
	
	@Autowired(required = false)
	RepositoryBuildChecks repositoryBuildChecks;
	
	
	@Autowired(required=false)
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;
	
	public DtoBuildPayrollCheckByPayCodes saveOrUpdate(DtoBuildPayrollCheckByPayCodes dtoBuildPayrollCheckByPayCodes) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByPayCodes.getListBuildChecks() != null && dtoBuildPayrollCheckByPayCodes.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByPayCodes.getListBuildChecks().get(0).getBuildChecks().getId());	
			}
			
			
			if(dtoBuildPayrollCheckByPayCodes.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByPayCodes.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
					repositoryBuildPayrollCheckByPayCodes.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByPayCodes.getListBuildChecks().get(0).getBuildChecks().getId());
				}
			
				for (DtoPayCode dtoPayCode : dtoBuildPayrollCheckByPayCodes.getDtoPayCode()) {
					PayCode code = repositoryPayCode.findByIdAndIsDeleted(dtoPayCode.getId(), false);
					BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes = new BuildPayrollCheckByPayCodes();
						buildPayrollCheckByPayCodes.setCreatedDate(new Date());
						Integer rowId = repositoryBuildPayrollCheckByPayCodes.findAll().size();
						Integer increment=0;
						if(rowId!=0) {
							increment= rowId+1;
						}else {
							increment=1;
						}
						buildPayrollCheckByPayCodes.setRowId(increment);
						buildPayrollCheckByPayCodes.setPayCode(code);
						buildPayrollCheckByPayCodes.setBuildChecks(buildChecks);
						
						repositoryBuildPayrollCheckByPayCodes.saveAndFlush(buildPayrollCheckByPayCodes);
						
					}
		
			
		}
		
			
		 catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildPayrollCheckByPayCodes;
	}	
	
	public DtoSearch getAllByCodeType(DtoBuildPayrollCheckByPayCodes dtoBuildPayrollCheckByPayCodes) {
		DtoSearch dtoSearch = new DtoSearch();
		List<DtoPayCodeDisplay>listResponse= new ArrayList<>();
		
		List<BuildPayrollCheckByPayCodes> buildChecksList = this.repositoryBuildPayrollCheckByPayCodes.findByBuildChecksId(dtoBuildPayrollCheckByPayCodes.getId());
		for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecksList) {
			DtoPayCodeDisplay dtoBuildPayrollCheckByPayCodess = new DtoPayCodeDisplay();
			if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
				dtoBuildPayrollCheckByPayCodess.setPayCodeIdPrimary(buildPayrollCheckByPayCodes.getPayCode().getId());
				dtoBuildPayrollCheckByPayCodess.setPayCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
			}
			if(buildPayrollCheckByPayCodes.getBuildChecks()!=null) {
				dtoBuildPayrollCheckByPayCodess.setBuildChecksId(buildPayrollCheckByPayCodes.getBuildChecks().getId());
			}
			listResponse.add(dtoBuildPayrollCheckByPayCodess);
		 }
			List<PayCode> list = this.repositoryPayCode.findByIsDeletedAndInActive(false, false);
			if (list != null && !list.isEmpty()) {
				for (PayCode payCode : list) {
					if(payCode.isInActive()==false) {
						long count =listResponse.stream().filter(d-> d.getPayCodeIdPrimary().equals(payCode.getId())).count();
						if(count == 0) {
							DtoPayCodeDisplay dtoBuildPayrollCheckByPayCodess = new DtoPayCodeDisplay();
							dtoBuildPayrollCheckByPayCodess.setPayCodeIdPrimary(payCode.getId());
							dtoBuildPayrollCheckByPayCodess.setPayCodeId(payCode.getPayCodeId());
							listResponse.add(dtoBuildPayrollCheckByPayCodess);
						}
					}
				
					
				}
			}
		
		
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	
	}
	
	
	
	public DtoSearch getAllForEmployeePaycode(DtoBuildPayrollCheckByPayCodes dtoBuildPayrollCheckByPayCodes) {
		DtoSearch dtoSearch = new DtoSearch();
		//List<DtoPayCodeDisplay>listResponse= new ArrayList<>();
		
	/*	List<BuildPayrollCheckByPayCodes> buildChecksList = this.repositoryBuildPayrollCheckByPayCodes.findByBuildChecksId(dtoBuildPayrollCheckByPayCodes.getId());
		for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecksList) {
			DtoPayCodeDisplay dtoBuildPayrollCheckByPayCodess = new DtoPayCodeDisplay();
			if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
				dtoBuildPayrollCheckByPayCodess.setPayCodeIdPrimary(buildPayrollCheckByPayCodes.getPayCode().getId());
				dtoBuildPayrollCheckByPayCodess.setPayCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
			}
			if(buildPayrollCheckByPayCodes.getBuildChecks()!=null) {
				dtoBuildPayrollCheckByPayCodess.setBuildChecksId(buildPayrollCheckByPayCodes.getBuildChecks().getId());
			}
			listResponse.add(dtoBuildPayrollCheckByPayCodess);
		 }*/
			/*List<PayCode> list = this.repositoryPayCode.findByIsDeletedAndInActive(false, false);
			if (list != null && !list.isEmpty()) {
				for (PayCode payCode : list) {
					long count =listResponse.stream().filter(d-> d.getPayCodeIdPrimary().equals(payCode.getId())).count();
					if(count == 0) {
						DtoPayCodeDisplay dtoBuildPayrollCheckByPayCodess = new DtoPayCodeDisplay();
						dtoBuildPayrollCheckByPayCodess.setPayCodeIdPrimary(payCode.getId());
						dtoBuildPayrollCheckByPayCodess.setPayCodeId(payCode.getPayCodeId());
						listResponse.add(dtoBuildPayrollCheckByPayCodess);
					}
					
				}
			}*/
		
		DtoPayCodeDisplay dtoBuildPayrollCheckByPayCodess = new DtoPayCodeDisplay();
			List<EmployeePayCodeMaintenance>listEmployeePayCodeMaintenance=this.repositoryEmployeePayCodeMaintenance.findByIsDeletedAndInactive(false, false);
			List<DtoEmployeePayCodeMaintenance> employeePayCodeMaintenances = new ArrayList<>();
			if(listEmployeePayCodeMaintenance!=null &&!listEmployeePayCodeMaintenance.isEmpty()) {
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance : listEmployeePayCodeMaintenance) {
						if(employeePayCodeMaintenance.getPayCode()!=null && employeePayCodeMaintenance.getPayCode().getId()>0) {
							DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance=new DtoEmployeePayCodeMaintenance();
							dtoEmployeePayCodeMaintenance.setPayCodeIdPrimary(employeePayCodeMaintenance.getPayCode().getId());
							dtoEmployeePayCodeMaintenance.setPayCodeId(employeePayCodeMaintenance.getPayCode().getPayCodeId());
							dtoEmployeePayCodeMaintenance.setDescription(employeePayCodeMaintenance.getPayCode().getDescription());
							employeePayCodeMaintenances.add(dtoEmployeePayCodeMaintenance);
							BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes = new BuildPayrollCheckByPayCodes();
							buildPayrollCheckByPayCodes.setCreatedDate(new Date());
							buildPayrollCheckByPayCodes.setPayCode(employeePayCodeMaintenance.getPayCode());
							buildPayrollCheckByPayCodes.setUpdatedDate(new Date());
							repositoryBuildPayrollCheckByPayCodes.save(buildPayrollCheckByPayCodes);
						}
				}
			}
			dtoBuildPayrollCheckByPayCodess.setDtoEmployeePayCodeMaintenances(employeePayCodeMaintenances);
			
		dtoSearch.setRecords(dtoBuildPayrollCheckByPayCodess);
		
		return dtoSearch;
	
	}

}
