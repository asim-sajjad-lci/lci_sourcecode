package com.bti.hcm.model.dto;

import java.util.Date;
/**
 * @author HAMID
 */
public class DtoLoanDetails extends DtoBase {

	private Integer loanDetailsId;	//id
	private Date monthYear;
	private Float deductionAmount;	//amt
	private Boolean isPostpone;
	private Boolean isPaid;

	private Integer reqIdx; // loanId; //reqIdx
	// one more field will come later as per FK to Table hr10200.NameUnknown)
	//private Integer refId; // ReferenceNuumber

	private Integer NumberOfThisDtoInCurrentLoan; // Unused
	private Boolean isEdit; // Unused

	public Boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}

	public Integer getNumberOfThisDtoInCurrentLoan() {
		return NumberOfThisDtoInCurrentLoan;
	}

	public void setNumberOfThisDtoInCurrentLoan(Integer numberOfThisDtoInCurrentLoan) {
		NumberOfThisDtoInCurrentLoan = numberOfThisDtoInCurrentLoan;
	}

	public Integer getLoanDetailsId() {
		return loanDetailsId;
	}

	public void setLoanDetailsId(Integer loanDetailsId) {
		this.loanDetailsId = loanDetailsId;
	}

	public Date getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(Date monthYear) {
		this.monthYear = monthYear;
	}

	public Boolean getIsPostpone() {
		return isPostpone;
	}

	public void setIsPostpone(Boolean isPostpone) {
		this.isPostpone = isPostpone;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

	public Integer getReqIdx() {
		return reqIdx;
	}

	public void setReqIdx(Integer reqIdx) {
		this.reqIdx = reqIdx;
	}

	public Float getDeductionAmount() {
		return deductionAmount;
	}

	public void setDeductionAmount(Float deductionAmount) {
		this.deductionAmount = deductionAmount;
	}

}
