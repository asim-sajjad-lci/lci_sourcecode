package com.bti.hcm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "hr10711")
public class EmployeeVacationDependant extends HcmBaseEntity2 {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDX")
	private Integer idx;

	@Column(name = "emp_vac_req_id")
	private Integer employeeVacationRequestId;
	
	@Column(name = "dep_nme")
	private String name; 

	@Column(name = "dep_rel")
	private String relation;
	
	@Column(name = "dep_age")
	private Integer age; 

	@OneToOne
	@JoinColumn(name = "tkt_id")
	private EmployeeVactionTicket employeeVactionTicket;

	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}

	public Integer getEmployeeVacationRequestId() {
		return employeeVacationRequestId;
	}

	public void setEmployeeVacationRequestId(Integer employeeVacationRequestId) {
		this.employeeVacationRequestId = employeeVacationRequestId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public EmployeeVactionTicket getEmployeeVactionTicket() {
		return employeeVactionTicket;
	}

	public void setEmployeeVactionTicket(EmployeeVactionTicket employeeVactionTicket) {
		this.employeeVactionTicket = employeeVactionTicket;
	}

	
	
}
