package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoActivateEmployeePostDatePayRate;
import com.bti.hcm.model.dto.DtoEmployeePostDatedPayRates;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeePostDatedPayRates;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeePostDatedPayRates")
public class ControllerEmployeePostDatedPayRates extends BaseController{

	@Autowired
	ServiceEmployeePostDatedPayRates serviceEmployeePostDatedPayRates;
	
	
	@Autowired
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeePostDatedPayRates.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates) throws Exception{
		LOGGER.info("Create EmployeePostDatedPayRates Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeePostDatedPayRates = serviceEmployeePostDatedPayRates.saveOrUpdate(dtoEmployeePostDatedPayRates);
			responseMessage=displayMessage(dtoEmployeePostDatedPayRates, "PAY_RATES_CREATED", "PAY_RATES_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeePostDatedPayRates Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates) throws Exception {
		LOGGER.info("Update EmployeePostDatedPayRates Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeePostDatedPayRates = serviceEmployeePostDatedPayRates.saveOrUpdate(dtoEmployeePostDatedPayRates);
			responseMessage=displayMessage(dtoEmployeePostDatedPayRates, "PAY_RATES_UPDATED_SUCCESS", "PAY_RATES_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeePostDatedPayRates Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByEmployeeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeePostDatedPayRatess Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePostDatedPayRates.getAll(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.PAY_RATES_GET_DETAIL, MessageConstant.PAY_RATES_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/findById", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage findById(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Searchs EmployeePostDatedPayRates Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePostDatedPayRates.findById(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.PAY_RATES_GET_DETAIL, MessageConstant.PAY_RATES_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("search employeePostDatedPayRates Methods:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteEmployeeSkill(HttpServletRequest request, @RequestBody DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates) throws Exception {
		LOGGER.info("Delete EmployeePostDatedPayRates Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeePostDatedPayRates.getIds() != null && !dtoEmployeePostDatedPayRates.getIds().isEmpty()) {
				DtoEmployeePostDatedPayRates dtoEmployeePositionHistory1 = serviceEmployeePostDatedPayRates.delete(dtoEmployeePostDatedPayRates.getIds());
				if(dtoEmployeePositionHistory1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("PAY_RATES_DELETED", false), dtoEmployeePositionHistory1);

				}else {
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("PAY_RATES_NOT_DELETE_ID_MESSAGE", false), dtoEmployeePositionHistory1);


				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeePostDatedPayRates Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/employeeIdCheck", method = RequestMethod.POST)
	public ResponseMessage employeeIdCheck(HttpServletRequest request, @RequestBody DtoEmployeePostDatedPayRates dtoEmployeePostDatedPayRates) throws Exception {
		LOGGER.info("employeeIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			DtoEmployeePostDatedPayRates dtopostDate = serviceEmployeePostDatedPayRates.repeatByEmployeeId(dtoEmployeePostDatedPayRates.getEmployeeMaster().getEmployeeIndexId());
			responseMessage=displayMessage(dtopostDate, "POST_DATE_EXIST", "POST_DATE_REPEAT_POST_DATE_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getAllForActiveEmployee", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllForActiveEmployee(@RequestBody DtoActivateEmployeePostDatePayRate dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeePostDatedPayRates Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePostDatedPayRates.getAllForActiveEmployee(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_RATES_GET_DETAIL", "PAY_RATES_GET_DETAIL", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("search employeePostDatedPayRates Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	// for getting 
	@RequestMapping(value = "/getAllData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllEmployeePostDate(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeePostDatedPayRates Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePostDatedPayRates.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_PAY_RATES_GET_ALL", "EMPLOYEE_PAY_RATES_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("search employeePostDatedPayRates Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllReasonForDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllReasonForDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoEmployeePostDatedPayRates> dtoEmployeePostDatedPayRatesList = serviceEmployeePostDatedPayRates.getAllReasonForDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_PAY_RATES_LIST_ALL_DROP_DOWN_GETTING", false), dtoEmployeePostDatedPayRatesList);
			}else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
	
	
	
}
