package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.AccrualPeriod;
import com.bti.hcm.model.AccrualPeriodSetting;
import com.bti.hcm.model.dto.DtoAccrualPeriod;
import com.bti.hcm.model.dto.DtoAccrualPeriodSetting;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAccrualPeriod;
import com.bti.hcm.repository.RepositoryAccrualPeriodSetting;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceAccrualPeriod")
public class ServiceAccrualPeriod {
static Logger log = Logger.getLogger(ServiceAccurualType.class.getName());

	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	
	@Autowired
	RepositoryAccrualPeriodSetting repositoryAccrualPeriodSetting;
	
	/**
	 * @Description repositoryAccrualPeriod Autowired here using annotation of spring for access of repositoryAccrualPeriod method in AccrualPeriod service
	 * 				In short Access AccrualPeriod Query from Database using repositoryAccrualPeriod.
	 */
	@Autowired
	RepositoryAccrualPeriod repositoryAccrualPeriod;
	
	
	public DtoAccrualPeriod saveOrUpdate(DtoAccrualPeriod dtoAccrualPeriod) {
		try {
			
			
			log.info("saveOrUpdate Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			AccrualPeriod accrualPeriod = null;
			if (dtoAccrualPeriod.getId() != null && dtoAccrualPeriod.getId() > 0) {
				accrualPeriod = repositoryAccrualPeriod.findByIdAndIsDeleted(dtoAccrualPeriod.getId(), false);
				accrualPeriod.setUpdatedBy(loggedInUserId);
				accrualPeriod.setUpdatedDate(new Date());
			} else {
				accrualPeriod = new AccrualPeriod();
				accrualPeriod.setCreatedDate(new Date());
				Integer rowId = repositoryAccrualPeriod.getCountOfTotalAccrualPeriod();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				accrualPeriod.setRowId(increment);
			}
			
			accrualPeriod.setYear(dtoAccrualPeriod.getYear());
			accrualPeriod.setYearActive(dtoAccrualPeriod.getYearActive());
			accrualPeriod.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			accrualPeriod.setRowId(loggedInUserId);
			accrualPeriod = repositoryAccrualPeriod.saveAndFlush(accrualPeriod);
			
			for (DtoAccrualPeriodSetting dtoAccrualPeriodSetting : dtoAccrualPeriod.getSubitems()) {
				AccrualPeriodSetting accrualPeriodSetting =  null;
				
				
				if(dtoAccrualPeriodSetting.getId()!=null && dtoAccrualPeriodSetting.getId()>0) {
					accrualPeriodSetting = repositoryAccrualPeriodSetting.findOne(dtoAccrualPeriodSetting.getId());
				}else {
					accrualPeriodSetting = new AccrualPeriodSetting();
				}
				
				accrualPeriodSetting.setAccrualPeriod(accrualPeriod);
				accrualPeriodSetting.setYear(dtoAccrualPeriod.getYear());
				accrualPeriodSetting.setPeriodType(dtoAccrualPeriod.getPeriodType());
				accrualPeriodSetting.setStartDate(dtoAccrualPeriodSetting.getStartDate());
				accrualPeriodSetting.setEndData(dtoAccrualPeriodSetting.getEndDate());
				accrualPeriodSetting.setSequence(dtoAccrualPeriodSetting.getSequence());
				
				repositoryAccrualPeriodSetting.saveAndFlush(accrualPeriodSetting);
			}
			
			log.debug("AccrualPeriod is:"+dtoAccrualPeriod.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoAccrualPeriod;
	}
	
	public DtoAccrualPeriod delete(List<Integer> ids) {
		log.info("delete DtoAccrualPeriod Method");
		DtoAccrualPeriod dtoAccrualPeriod = new DtoAccrualPeriod();
		dtoAccrualPeriod.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted(MessageConstant.ACCRUAL_PERIOED_DELETED, false));
		dtoAccrualPeriod.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted(MessageConstant.ACCRUAL_PERIOED_DELETED, false));
		List<DtoAccrualPeriod> deleteAccrualPeriod = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.ACCRUAL_PERIOED_NOT_DELETE_ID_MESSAGE, false).getMessage());
		try {
			for (Integer deAccrualId : ids) {
				AccrualPeriod accrualPeriod = repositoryAccrualPeriod.findOne(deAccrualId);
				
				
				if(accrualPeriod.getListAccrualPeriodSettings().isEmpty()) {
					DtoAccrualPeriod dtoAccrual2 = new DtoAccrualPeriod();
					dtoAccrual2.setId(accrualPeriod.getId());
					repositoryAccrualPeriod.deleteSingleAccrualPeriod(true, loggedInUserId, deAccrualId);
					deleteAccrualPeriod.add(dtoAccrual2);
				}
				
				
				
				else{
					inValidDelete = true;
					invlidDeleteMessage.append(accrualPeriod.getId()+","); 
				}
			}
			
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoAccrualPeriod.setMessageType(invlidDeleteMessage.toString());
				
			}
			
			
			if(!inValidDelete){
				dtoAccrualPeriod.setMessageType("ACCRUAL_PERIOED_DELETED");
				
			}
			
			dtoAccrualPeriod.setDelete(deleteAccrualPeriod);
		} 
		catch (NumberFormatException e) {
			log.error(e);
		}
		
		
		log.debug("Delete AccrualPeriod :"+dtoAccrualPeriod.getId());
		return dtoAccrualPeriod;
	}

	
	
	public DtoSearch search(DtoSearch dtoSearch) {
              try {
          		log.info("search AccrualPeriod Method");
        		if(dtoSearch != null){
        			String condition="";
        			
        			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
        				
        				if(dtoSearch.getSortOn().equals("year") || dtoSearch.getSortOn().equals("yearActive")
        						|| dtoSearch.getSortOn().equals("rowDateIndexing")
        						|| dtoSearch.getSortOn().equals("rowIdIndexing")) {
        					condition=dtoSearch.getSortOn();
        				}else {
        					condition="id";
        				}
        				
        			}else{
        				condition+="id";
        				dtoSearch.setSortOn("");
        				dtoSearch.setSortBy("");
        			}
        			
        			dtoSearch.setTotalCount(this.repositoryAccrualPeriod.predictiveAccrualSearchTotalCount());
        			List<AccrualPeriod> accrualList =null;
        			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
        				
        				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
        					accrualList = this.repositoryAccrualPeriod.predictiveAccrualSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
        				}
        				if(dtoSearch.getSortBy().equals("ASC")){
        					accrualList =  this.repositoryAccrualPeriod.predictiveAccrualSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
        				}else if(dtoSearch.getSortBy().equals("DESC")){
        					accrualList =  this.repositoryAccrualPeriod.predictiveAccrualSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
        				}
        				
        			}
        			
        		
        			if(accrualList != null && !accrualList.isEmpty()){
        				List<DtoAccrualPeriod> dtoAccrualPeriodList = new ArrayList<>();
        				DtoAccrualPeriod dtoAccrualPeriod=null;
        				for (AccrualPeriod accrual : accrualList) {
        					dtoAccrualPeriod = new DtoAccrualPeriod(accrual);
        					dtoAccrualPeriod.setId(accrual.getId());
        					dtoAccrualPeriod.setYear(accrual.getYear());
        					dtoAccrualPeriod.setYearActive(accrual.getYearActive());
        					
        					
        						List<DtoAccrualPeriodSetting> subitems = new ArrayList<>();
        						
        						for (AccrualPeriodSetting accrualPeriod : accrual.getListAccrualPeriodSettings()) {
        							DtoAccrualPeriodSetting accrualPeriodSetting = new DtoAccrualPeriodSetting();
        							
        							accrualPeriodSetting.setId(accrualPeriod.getId());
        							accrualPeriodSetting.setYear(accrualPeriod.getYear());
        							accrualPeriodSetting.setPeriodType(accrualPeriod.getPeriodType());
        							accrualPeriodSetting.setStartDate(accrualPeriod.getStartDate());
        							accrualPeriodSetting.setEndDate(accrualPeriod.getEndData());
        							accrualPeriodSetting.setSequence(accrualPeriod.getSequence());
        							subitems.add(accrualPeriodSetting);
        						}
        						dtoAccrualPeriod.setSubitems(subitems);
        					
        					dtoAccrualPeriodList.add(dtoAccrualPeriod);
        				}
        				dtoSearch.setRecords(dtoAccrualPeriodList);
        			}
        		}
        		log.debug("Search AccrualPeriod Size is:"+dtoSearch.getTotalCount());
            	  
			} catch (Exception e) {
				log.error(e);
			}
		return dtoSearch;
	}
	
	public DtoAccrualPeriod getByAccrualPeriodId(Integer id) {
		log.info("getByAccrualPeriodId Method");
		DtoAccrualPeriod dtoAccrualPeriod = new DtoAccrualPeriod();
		try {
			
			if (id > 0) {
				AccrualPeriod accrualPeriod = repositoryAccrualPeriod.findByIdAndIsDeleted(id, false);
				if (accrualPeriod != null) {
					dtoAccrualPeriod = new DtoAccrualPeriod(accrualPeriod);
					dtoAccrualPeriod.setId(accrualPeriod.getId());
					dtoAccrualPeriod.setYear(accrualPeriod.getYear());
					dtoAccrualPeriod.setYearActive(accrualPeriod.getYearActive());
					
					
					List<DtoAccrualPeriodSetting> subitems = new ArrayList<>();
					
					for (AccrualPeriodSetting accrualPeriod1 : accrualPeriod.getListAccrualPeriodSettings()) {
						DtoAccrualPeriodSetting accrualPeriodSetting = new DtoAccrualPeriodSetting();
						
						accrualPeriodSetting.setId(accrualPeriod1.getId());
						accrualPeriodSetting.setYear(accrualPeriod1.getYear());
						accrualPeriodSetting.setPeriodType(accrualPeriod1.getPeriodType());
						accrualPeriodSetting.setStartDate(accrualPeriod1.getStartDate());
						accrualPeriodSetting.setEndDate(accrualPeriod1.getEndData());
						accrualPeriodSetting.setSequence(accrualPeriod1.getSequence());
						subitems.add(accrualPeriodSetting);
					}
					dtoAccrualPeriod.setSubitems(subitems);
				} else {
					dtoAccrualPeriod.setMessageType("ACCRUAL_PERIOEDS_NOT_GETTING");

				}
			} else {
				dtoAccrualPeriod.setMessageType("ACCRUAL_PERIOEDS_NOT_GETTING");

			}
			log.debug("AccrualPeriod By Id is:"+dtoAccrualPeriod.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrualPeriod;
	}
	
	public DtoSearch getAll() {
		log.info("getAll AccrualPeriod Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			
			List<AccrualPeriod> accrualPeriodList = null;
			accrualPeriodList = repositoryAccrualPeriod.findByIsDeletedOrderByCreatedDateDesc(false);
			List<DtoAccrualPeriod> dtoAccrualPeriodList=new ArrayList<>();
			if(accrualPeriodList!=null && !accrualPeriodList.isEmpty())
			{
				for (AccrualPeriod accrualPeriod : accrualPeriodList) 
				{
					DtoAccrualPeriod dtoAccrual=new DtoAccrualPeriod(accrualPeriod);
					dtoAccrual.setId(accrualPeriod.getId());
					dtoAccrual.setYear(accrualPeriod.getYear());
					dtoAccrual.setYearActive(accrualPeriod.getYearActive());
					
					dtoAccrualPeriodList.add(dtoAccrual);
				}
				dtoSearch.setRecords(dtoAccrualPeriodList);
			}
			log.debug("All AccrualPeriod List Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}

		return dtoSearch;
	}
	
	public DtoAccrualPeriod getById(int id) {
		DtoAccrualPeriod dtoAccrualPeriod  = new DtoAccrualPeriod();

		try {
			
			if (id > 0) {
				AccrualPeriod accrualPeriodSetting = repositoryAccrualPeriod.findByIdAndIsDeleted(id, false);
				if (accrualPeriodSetting != null) {
					dtoAccrualPeriod = new DtoAccrualPeriod(accrualPeriodSetting);
					dtoAccrualPeriod.setId(accrualPeriodSetting.getId());
					dtoAccrualPeriod.setYear(accrualPeriodSetting.getYear());
					dtoAccrualPeriod.setYearActive(accrualPeriodSetting.getYearActive());
					
					List<DtoAccrualPeriodSetting> subitems = new ArrayList<>();
					
					for (AccrualPeriodSetting accrualPeriod1 : accrualPeriodSetting.getListAccrualPeriodSettings()) {
						DtoAccrualPeriodSetting accrualPeriodSetting1 = new DtoAccrualPeriodSetting();
						
						accrualPeriodSetting1.setId(accrualPeriod1.getId());
						accrualPeriodSetting1.setYear(accrualPeriod1.getYear());
						accrualPeriodSetting1.setPeriodType(accrualPeriod1.getPeriodType());
						accrualPeriodSetting1.setStartDate(accrualPeriod1.getStartDate());
						accrualPeriodSetting1.setEndDate(accrualPeriod1.getEndData());
						accrualPeriodSetting1.setSequence(accrualPeriod1.getSequence());
						subitems.add(accrualPeriodSetting1);
					}
				} else {
					dtoAccrualPeriod.setMessageType("ACCRUAL_PERIOED_NOT_GETTING");

				}
			} else {
				dtoAccrualPeriod.setMessageType("ACCRUAL_PERIOED_NOT_GETTING");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrualPeriod;
	}
}
