/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPayScheduleSetupPosition;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayScheduleSetupPosition;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: Controller PayScheduleSetupPosition
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/payScheduleSetupPosition")
public class ControllerPayScheduleSetupPosition extends BaseController{

	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupPosition Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerPayScheduleSetupPosition.class);
	
	/**
	 * @Description servicePayScheduleSetupPosition  Autowired here using annotation of spring for use of servicePayScheduleSetupPosition method in this controller
	 */
	@Autowired(required=true)
	ServicePayScheduleSetupPosition servicePayScheduleSetupPosition;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupPosition
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createPayScheduleSetupPosition(HttpServletRequest request, @RequestBody DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition) throws Exception {
		LOGGER.info("Create PayScheduleSetupPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupPosition = servicePayScheduleSetupPosition.saveOrUpdatePayScheduleSetupPosition(dtoPayScheduleSetupPosition);
			responseMessage=displayMessage(dtoPayScheduleSetupPosition, "PAY_SCHEDULE_SETUP_POSITION_CREATED", "PAY_SCHEDULE_SETUP_POSITION_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PayScheduleSetupPosition Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupPosition
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePayScheduleSetupPosition(HttpServletRequest request, @RequestBody DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition) throws Exception {
		LOGGER.info("Update PayScheduleSetupPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupPosition = servicePayScheduleSetupPosition.saveOrUpdatePayScheduleSetupPosition(dtoPayScheduleSetupPosition);
			responseMessage=displayMessage(dtoPayScheduleSetupPosition, "PAY_SCHEDULE_SETUP_POSITION_UPDATED_SUCCESS", "PAY_SCHEDULE_SETUP_POSITION_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update PayScheduleSetupPosition Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupPosition
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePayScheduleSetupPosition(HttpServletRequest request, @RequestBody DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition) throws Exception {
		LOGGER.info("Delete PayScheduleSetupPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayScheduleSetupPosition.getIds() != null && !dtoPayScheduleSetupPosition.getIds().isEmpty()) {
				DtoPayScheduleSetupPosition dtoPayScheduleSetupPosition2 = servicePayScheduleSetupPosition.deletePayScheduleSetupPosition(dtoPayScheduleSetupPosition.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_POSITION_LIST_DELETED", false), dtoPayScheduleSetupPosition2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete PayScheduleSetupPosition Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPayScheduleSetupPosition(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search PayScheduleSetupPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayScheduleSetupPosition.searchPayScheduleSetupPosition(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_SCHEDULE_SETUP_POSITION_GET_ALL", "PAY_SCHEDULE_SETUP_POSITION_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PayScheduleSetupPosition Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

}
