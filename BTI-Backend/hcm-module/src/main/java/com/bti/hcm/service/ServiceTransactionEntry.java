package com.bti.hcm.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Batches;
import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.BuildPayrollCheckByBatches;
import com.bti.hcm.model.CalculateChecks;
import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.Distribution;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.FinancialDimensions;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.PayrollAccounts;
import com.bti.hcm.model.TransactionEntry;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoBenefitCode;
import com.bti.hcm.model.dto.DtoBuildCheckBatchMarkOrUnMark;
import com.bti.hcm.model.dto.DtoBuildDefaultCheck;
import com.bti.hcm.model.dto.DtoDeductionCode;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoEmployeeMasterHcm;
import com.bti.hcm.model.dto.DtoFinancialDimensions;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTransactionEntry;
import com.bti.hcm.model.dto.DtoTransactionEntryDetail;
import com.bti.hcm.model.dto.DtoTransactionEntryDisplay;
import com.bti.hcm.repository.RepositoryBatches;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryCalculateChecks;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryDistribution;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryFinancialDimensions;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryPayrollAccounts;
import com.bti.hcm.repository.RepositoryTransactionEntry;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;

@Service("serviceTransactionEntry")
public class ServiceTransactionEntry {

	static Logger log = Logger.getLogger(ServiceTransactionEntry.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryTransactionEntry repositoryTransactionEntry;
	
	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;
	
	@Autowired
	RepositoryBatches repositoryBatches;
	
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired
	RepositoryDepartment repositoryDepartment;
	
	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired
	RepositoryFinancialDimensions repositoryFinancialDimensions;
	
	@Autowired
	RepositoryPayCode repositoryPayCode;
	
	@Autowired
	RepositoryPayrollAccounts repositoryPayrollAccounts;
	
	@Autowired
	RepositoryCalculateChecks repositoryCalculateChecks;
	
	@Autowired
	RepositoryEmployeePayCodeMaintenance employeePayCodeMaintenance;
	
	@Autowired
	RepositoryBuildChecks repositoryBuildChecks;
	
	@Autowired
	RepositoryDistribution repositoryDistribution;
	
	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;
	
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	
	/*public DtoTransactionEntry saveOrUpdate(DtoTransactionEntry dtoTransactionEntry) {
		log.info("enter into save or update TransactionEntry");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		TransactionEntry transactionEntry=null;
		try {
			if (dtoTransactionEntry.getId() != null && dtoTransactionEntry.getId() > 0) {
				transactionEntry = repositoryTransactionEntry.findByIdAndIsDeletedAndFromBuild(dtoTransactionEntry.getId(), false,false);
				transactionEntry.setUpdatedBy(loggedInUserId);
				transactionEntry.setUpdatedDate(new Date());
				transactionEntry.setCreatedDate(new Date());
				try {
					repositoryTransactionEntryDetail.deleteByTransactionEntry(true, loggedInUserId, transactionEntry.getId());	
				}catch (Exception e) {
					log.error(e);
				}
				
			} else {
				
				transactionEntry = new TransactionEntry();
				transactionEntry.setCreatedDate(new Date());
				TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
				//Integer rowId = repositoryTransactionEntry.findAll().size();
				Integer increment=0;
				if(transactionEntry2.getRowId()!=0) {
					increment= transactionEntry2.getRowId()+1;
				}else {
					increment=1;
				}
				transactionEntry.setRowId(increment);
				
			}
			Batches batches = null;
			if(dtoTransactionEntry.getBatches()!=null) {
				batches = repositoryBatches.findByIdAndIsDeleted(dtoTransactionEntry.getBatches().getId(), false);
			}
			transactionEntry.setUpdatedRow(new Date());
			transactionEntry.setBatches(batches);
			transactionEntry.setEntryNumber(repositoryTransactionEntry.findAll().size()+1);
			transactionEntry.setEntryDate(new Date());
			transactionEntry.setFromDate(dtoTransactionEntry.getFromDate());
			transactionEntry.setToDate(dtoTransactionEntry.getToDate());
			transactionEntry.setDescription(dtoTransactionEntry.getDescription());
			transactionEntry.setArabicDescription(dtoTransactionEntry.getArabicDescription());
			transactionEntry.setFromBuild(false);
			transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
			
			TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
				
				EmployeeMaster employeeMaster = null;
				PayCode payCode = null;
				DeductionCode deductionCode = null;
				BenefitCode benefitCode = null;
				FinancialDimensions financialDimensions = null; 
				if(dtoTransactionEntry.getTransactionEntryDetail().getEmployeeMaster()!=null) {
					employeeMaster = repositoryEmployeeMaster.findOne(dtoTransactionEntry.getTransactionEntryDetail().getEmployeeMaster().getEmployeeIndexId());
				}
				
				if(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType()==1) {
					payCode = repositoryPayCode.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
				}else if(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType() == 2) {
					deductionCode = repositoryDeductionCode.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
				}else {
					benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
				}
				
				if(dtoTransactionEntry.getDimensions()!= null && dtoTransactionEntry.getDimensions().getId()>0) {
					financialDimensions = repositoryFinancialDimensions.findOne(dtoTransactionEntry.getDimensions().getId());
				}
				TransactionEntryDetail transactionEntryDetail = null;
				if(dtoTransactionEntry.getTransactionEntryDetail()!=null && dtoTransactionEntry.getTransactionEntryDetail().getId()!=null && dtoTransactionEntry.getTransactionEntryDetail().getId()>0) {
					transactionEntryDetail = repositoryTransactionEntryDetail.findOne(dtoTransactionEntry.getTransactionEntryDetail().getId());
				}else {
					transactionEntryDetail = new TransactionEntryDetail();
				}
				transactionEntryDetail.setDimensions(financialDimensions);
				transactionEntryDetail.setTransactionEntry(transactionEntry);
				transactionEntryDetail.setTransactionType(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType());
				transactionEntryDetail.setEntryNumber(transactionEntryDetailLastRecord.getId()+1);
				transactionEntryDetail.setDetailsSequence(transactionEntryDetailLastRecord.getId()+1);
				transactionEntryDetail.setCode(payCode);
				transactionEntryDetail.setBenefitCode(benefitCode);
				transactionEntryDetail.setDeductionCode(deductionCode);
				transactionEntryDetail.setEmployeeMaster(employeeMaster);
				if(employeeMaster!=null) {
					transactionEntryDetail.setDepartment(employeeMaster.getDepartment());
				}
				transactionEntryDetail.setCreatedDate(new Date());
				transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
				transactionEntryDetail.setUpdatedRow(new Date());
				transactionEntryDetail.setUpdatedBy(loggedInUserId);
				transactionEntryDetail.setAmount(dtoTransactionEntry.getTransactionEntryDetail().getAmount());
				transactionEntryDetail.setPayRate(dtoTransactionEntry.getTransactionEntryDetail().getPayRate());
				transactionEntryDetail.setFromDate(transactionEntry.getFromDate());
				transactionEntryDetail.setToDate(transactionEntry.getToDate());
				if(transactionEntryDetail.getTransactionType()==1) {
					transactionEntryDetail.setFromBuild(false);
				}else {
					transactionEntryDetail.setFromBuild(true);
				}
				transactionEntryDetail.setFromBuild(true);
				//transactionEntryDetail.setBatches(batches);
				repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
				
			
		}catch (Exception e) {
			log.error(e);
			
		}
		return dtoTransactionEntry;
	}*/
	public DtoTransactionEntry saveOrUpdate(DtoTransactionEntry dtoTransactionEntry) {
		  log.info("enter into save or update TransactionEntry");
		  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		  TransactionEntry transactionEntry=null;
		  try {
				 if (dtoTransactionEntry.getId() != null && dtoTransactionEntry.getId() > 0) {
					   transactionEntry = repositoryTransactionEntry.findByIdAndIsDeletedAndFromBuild(dtoTransactionEntry.getId(), false,false);
					   transactionEntry.setUpdatedBy(loggedInUserId);
					   transactionEntry.setUpdatedDate(new Date());
					   transactionEntry.setCreatedDate(new Date());
					   try {
							  repositoryTransactionEntryDetail.deleteByTransactionEntry(true, loggedInUserId, transactionEntry.getId());    
					   }catch (Exception e) {
							  log.error(e);
					   }
					   
				 } else {
					   
					   transactionEntry = new TransactionEntry();
					   transactionEntry.setCreatedDate(new Date());
					   TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
					   //Integer rowId = repositoryTransactionEntry.findAll().size();
					   Integer increment=0;
					   if(transactionEntry2 != null) {
							  if(transactionEntry2.getRowId()!=0) {
									 increment= transactionEntry2.getRowId()+1;
							  }else {
									 increment=1;
							  }
							  transactionEntry.setRowId(increment);
					   }else {
							  increment = 1;
					   }
					   
					   
				 }
				 Batches batches = null;
				 if(dtoTransactionEntry.getBatches()!=null) {
					   batches = repositoryBatches.findByIdAndIsDeleted(dtoTransactionEntry.getBatches().getId(), false);
				 }
				 transactionEntry.setUpdatedRow(new Date());
				 transactionEntry.setBatches(batches);
				 transactionEntry.setEntryNumber(repositoryTransactionEntry.findAll().size()+1);
				 transactionEntry.setEntryDate(new Date());
				 transactionEntry.setFromDate(dtoTransactionEntry.getFromDate());
				 transactionEntry.setToDate(dtoTransactionEntry.getToDate());
				 transactionEntry.setDescription(dtoTransactionEntry.getDescription());
				 transactionEntry.setArabicDescription(dtoTransactionEntry.getArabicDescription());
				 transactionEntry.setFromBuild(false);
				 transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
				 TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
					   
					   EmployeeMaster employeeMaster = null;
					   PayCode payCode = null;
					   DeductionCode deductionCode = null;
					   BenefitCode benefitCode = null;
					   FinancialDimensions financialDimensions = null; 
						if(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType()==1) {
							  payCode = repositoryPayCode.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
					   }else if(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType() == 2) {
							  deductionCode = repositoryDeductionCode.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
					   }else {
							  benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
					   }
					   
			if(dtoTransactionEntry.getTransactionEntryDetail().getEmployeeMaster()!=null) {
				employeeMaster = repositoryEmployeeMaster.findOne(dtoTransactionEntry.getTransactionEntryDetail().getEmployeeMaster().getEmployeeIndexId());
			}

						if(dtoTransactionEntry.getDimensions()!= null && dtoTransactionEntry.getDimensions().getId()>0) {
							  financialDimensions = repositoryFinancialDimensions.findOne(dtoTransactionEntry.getDimensions().getId());
						}
						TransactionEntryDetail transactionEntryDetail = null;
						if(dtoTransactionEntry.getTransactionEntryDetail()!=null && dtoTransactionEntry.getTransactionEntryDetail().getId()!=null && dtoTransactionEntry.getTransactionEntryDetail().getId()>0) {
							  transactionEntryDetail = repositoryTransactionEntryDetail.findOne(dtoTransactionEntry.getTransactionEntryDetail().getId());
						}else {
							  transactionEntryDetail = new TransactionEntryDetail();
						}
						transactionEntryDetail.setDimensions(financialDimensions);
						transactionEntryDetail.setTransactionEntry(transactionEntry);
						transactionEntryDetail.setTransactionType(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType());
					   if(transactionEntryDetailLastRecord != null) {
							  transactionEntryDetail.setEntryNumber(transactionEntryDetailLastRecord.getId()+1);
					   }else {
							  transactionEntryDetail.setEntryNumber(1);
					   }
					   if(transactionEntryDetailLastRecord != null) {
							  transactionEntryDetail.setDetailsSequence(transactionEntryDetailLastRecord.getId()+1);
					   }else {
							  transactionEntryDetail.setDetailsSequence(1);
					   }

					   transactionEntryDetail.setCode(payCode);
					   transactionEntryDetail.setBenefitCode(benefitCode);
					   transactionEntryDetail.setDeductionCode(deductionCode);
					   transactionEntryDetail.setEmployeeMaster(employeeMaster);
					   if(employeeMaster!=null) {
							  transactionEntryDetail.setDepartment(employeeMaster.getDepartment());
					   }
					   transactionEntryDetail.setCreatedDate(new Date());
					   if(transactionEntryDetailLastRecord != null) {
							  transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
					   }else {
							  transactionEntryDetail.setRowId(1);
					   }

					   transactionEntryDetail.setUpdatedRow(new Date());
					   transactionEntryDetail.setUpdatedBy(loggedInUserId);
					transactionEntryDetail.setAmount(dtoTransactionEntry.getTransactionEntryDetail().getAmount());
					transactionEntryDetail.setFromTranscationEntry(true);
					
					
				 transactionEntryDetail.setPayRate(dtoTransactionEntry.getTransactionEntryDetail().getPayRate());
				 transactionEntryDetail.setFromDate(transactionEntry.getFromDate());
					   transactionEntryDetail.setToDate(transactionEntry.getToDate());
					   /*if(transactionEntryDetail.getTransactionType()==1) {
							  transactionEntryDetail.setFromBuild(false);
					   }else {
							  transactionEntryDetail.setFromBuild(true);
					   }*/
					   transactionEntryDetail.setFromBuild(true);
					   //transactionEntryDetail.setBatches(batches);
					   repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
					   
				 
		 }catch (Exception e) {
			 log.error(e);
		}
		return dtoTransactionEntry;
	}

	
	//@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchByBatchId(DtoSearch dtoSearch) {
		log.info("search TransactionEntries Method");


		
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("1") || dtoSearch.getSortOn().equals("2")||  dtoSearch.getSortOn().equals("4")) {
							
							if(dtoSearch.getSortOn().equals("1")) {
								dtoSearch.setSortOn("employeeMaster.employeeId");
							}else if(dtoSearch.getSortOn().equals("2")) {
								dtoSearch.setSortOn("department.departmentId");
							}else if(dtoSearch.getSortOn().equals("4")) {
								dtoSearch.setSortOn("dimensions.dimensionDescription");
							}
	
							
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				 List<TransactionEntry> list =  repositoryTransactionEntry.findByBatches(dtoSearch.getId());
					
				 List<Integer> ids =  list.stream().map(TransactionEntry :: getId).collect(Collectors.toList());
				 
				dtoSearch.setTotalCount(this.repositoryTransactionEntryDetail.searchByTransctionEntryCount(dtoSearch.getId(),"%"+searchWord+"%"));
				List<TransactionEntryDetail> traningCourseList =null;
				
				if(!ids.isEmpty()) {
					if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
						
						if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
							traningCourseList = this.repositoryTransactionEntryDetail.searchByTransctionEntry(ids,"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
						}
						if(dtoSearch.getSortBy().equals("ASC")){
							traningCourseList = this.repositoryTransactionEntryDetail.searchByTransctionEntry(ids,"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
						}else if(dtoSearch.getSortBy().equals("DESC")){
							traningCourseList = this.repositoryTransactionEntryDetail.searchByTransctionEntry(ids,"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
						}
						
					}	
				}else {
					traningCourseList = new ArrayList<>();
				}
				
				//				List<EmployeeBenefitMaintenance> benefits = repositoryEmployeeBenefitMaintenance.findAll();
				List<DtoTransactionEntryDisplay> displays = new ArrayList<>();
				for (TransactionEntryDetail transactionEntryDetail : traningCourseList) {
					DtoTransactionEntryDisplay dtoTransactionEntryDisplay = new DtoTransactionEntryDisplay();
					dtoTransactionEntryDisplay.setTranscationNumber(transactionEntryDetail.getEntryNumber());
					
					dtoTransactionEntryDisplay.setTranscationPrimaryId(transactionEntryDetail.getTransactionEntry().getId());
					dtoTransactionEntryDisplay.setTranscationSubListId(transactionEntryDetail.getId());
					
					if(transactionEntryDetail.getEmployeeMaster()!=null) {
						dtoTransactionEntryDisplay.setEmployeePrimaryId(transactionEntryDetail.getEmployeeMaster().getEmployeeIndexId());
						dtoTransactionEntryDisplay.setEmployeeId(transactionEntryDetail.getEmployeeMaster().getEmployeeId());
						dtoTransactionEntryDisplay.setEmployeeName(transactionEntryDetail.getEmployeeMaster().getEmployeeFirstName() + " "+transactionEntryDetail.getEmployeeMaster().getEmployeeLastName());
						if(transactionEntryDetail.getEmployeeMaster().getDepartment()!=null) {
							dtoTransactionEntryDisplay.setDepartmentId(transactionEntryDetail.getEmployeeMaster().getDepartment().getDepartmentId());
						}

					List<EmployeeBenefitMaintenance> benefits = transactionEntryDetail.getEmployeeMaster().getListEmployeeBenefitMaintenance();
						for(EmployeeBenefitMaintenance ment : benefits) { 
							if(ment.getBenefitCode() != null && transactionEntryDetail.getBenefitCode() != null && ment.getBenefitCode().getBenefitId().equals(transactionEntryDetail.getBenefitCode().getBenefitId()))

								dtoTransactionEntryDisplay.setPayFactor(ment.getBenefitAmount());
						}
					}

					//similarly for other transactiontype. match "TransactionEntryDetail" code with "ment" code and set display object<br/>	
					// Added by Zia end

					List<EmployeeDeductionMaintenance> deductions = transactionEntryDetail.getEmployeeMaster().getListEmployeeDeductionMaintenance();

					for(EmployeeDeductionMaintenance ment : deductions) { 
						if(transactionEntryDetail.getDeductionCode() != null && ment.getDeductionCode()!=null && ment.getDeductionCode().getDiductionId().equals(transactionEntryDetail.getDeductionCode().getDiductionId())) {

							{ 
								dtoTransactionEntryDisplay.setPayFactor(ment.getDeductionAmount());
							}

						}
					}
					

					if(transactionEntryDetail.getTransactionEntry()!=null && transactionEntryDetail.getTransactionEntry().getBatches()!=null && transactionEntryDetail.getTransactionEntry().getBatches().getListBuildPayrollCheckByBatches()!=null) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : transactionEntryDetail.getTransactionEntry().getBatches().getListBuildPayrollCheckByBatches()) {
							Date date =buildPayrollCheckByBatches.getBuildChecks().getCreatedDate();
							dtoTransactionEntryDisplay.setBuildDate(date);
							SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
							String time = localDateFormat.format(date);
							dtoTransactionEntryDisplay.setBuildtime(time);
							buildPayrollCheckByBatches.getBuildChecks().getId();
							
						}	
					}
					
					
					if(transactionEntryDetail.getDimensions()!=null) {
						dtoTransactionEntryDisplay.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());	
						dtoTransactionEntryDisplay.setProjectPrimaryId(transactionEntryDetail.getDimensions().getId());
					}
					
					

					if(transactionEntryDetail.getTransactionType()==1) {
						if(transactionEntryDetail.getCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getCode().getId());
						}
					}
					




					if(transactionEntryDetail.getTransactionType()==2) {
						if(transactionEntryDetail.getDeductionCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getDeductionCode().getId());
							

						}
					}
					


					if(transactionEntryDetail.getTransactionType()==3) {
						if(transactionEntryDetail.getBenefitCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getBenefitCode().getId());

						}
					}
					
					dtoTransactionEntryDisplay.setTrxType(transactionEntryDetail.getTransactionType());
					dtoTransactionEntryDisplay.setAmount(transactionEntryDetail.getAmount());
					dtoTransactionEntryDisplay.setPayRate(transactionEntryDetail.getPayRate());
					
					displays.add(dtoTransactionEntryDisplay);
					}
				
				
				if(dtoSearch.getSortOn()!=null) {
					if(dtoSearch.getSortOn().equals("3")) {
						Collections.sort(displays, new DtoTransactionEntryDisplay());
					}
					
					if(dtoSearch.getSortOn().equals("employee")) {
						DtoTransactionEntryDisplay e = new DtoTransactionEntryDisplay();
						DtoTransactionEntryDisplay.SortByEmployee sortbyEmployee = e.new SortByEmployee(); 
						Collections.sort(displays, sortbyEmployee);
					}
					
					if(dtoSearch.getSortOn().equals("department")) {
						DtoTransactionEntryDisplay e = new DtoTransactionEntryDisplay();
						DtoTransactionEntryDisplay.SortByDepartment sortByDepartment = e.new SortByDepartment(); 
						Collections.sort(displays, sortByDepartment);
					}
					
					if(dtoSearch.getSortOn().equals("codeId")) {
						DtoTransactionEntryDisplay e = new DtoTransactionEntryDisplay();
						DtoTransactionEntryDisplay.SortByCodeId sortByCodeId = e.new SortByCodeId(); 
						Collections.sort(displays, sortByCodeId);
					}
					
					if(dtoSearch.getSortOn().equals("projectId")) {
						DtoTransactionEntryDisplay e = new DtoTransactionEntryDisplay();
						DtoTransactionEntryDisplay.SortByProjectId sortByProjectId = e.new SortByProjectId(); 
						Collections.sort(displays, sortByProjectId);
					}
					if(dtoSearch.getSortOn().equals("transcationPrimaryId")) {
						DtoTransactionEntryDisplay e = new DtoTransactionEntryDisplay();
						DtoTransactionEntryDisplay.SortByTransactionId sortByTransactionId = e.new SortByTransactionId();
						Collections.sort(displays,sortByTransactionId);
					}
				}
				dtoSearch.setRecords(displays);
				}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		
		log.debug("Search TransactionEntry Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
		}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAll(DtoSearch dtoSearch) {
		log.info("search TransactionEntry Method");
		
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("1") || dtoSearch.getSortOn().equals("2")||  dtoSearch.getSortOn().equals("4")) {
							
							if(dtoSearch.getSortOn().equals("1")) {
								dtoSearch.setSortOn("employeeMaster.employeeId");
							}else if(dtoSearch.getSortOn().equals("2")) {
								dtoSearch.setSortOn("department.departmentId");
							}else if(dtoSearch.getSortOn().equals("4")) {
								dtoSearch.setSortOn("dimensions.dimensionDescription");
							}
	
							
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
				
				List<TransactionEntryDetail> traningCourseList =null;
				
			
					if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
						
						if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
							traningCourseList = this.repositoryTransactionEntryDetail.findByIsDeleted(false,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
						}
						if(dtoSearch.getSortBy().equals("ASC")){
							traningCourseList = this.repositoryTransactionEntryDetail.findByIsDeleted(false,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
						}else if(dtoSearch.getSortBy().equals("DESC")){
							traningCourseList = this.repositoryTransactionEntryDetail.findByIsDeleted(false,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
						}
						
					}	
				
					dtoSearch.setTotalCount(traningCourseList.size());
				
				List<DtoTransactionEntryDisplay> displays = new ArrayList<>();
				for (TransactionEntryDetail transactionEntryDetail : traningCourseList) {
					DtoTransactionEntryDisplay dtoTransactionEntryDisplay = new DtoTransactionEntryDisplay();
					dtoTransactionEntryDisplay.setTranscationNumber(transactionEntryDetail.getEntryNumber());
					
					
					if(transactionEntryDetail.getTransactionEntry()!=null && transactionEntryDetail.getTransactionEntry().getBatches()!=null && transactionEntryDetail.getTransactionEntry().getBatches().getListBuildPayrollCheckByBatches()!=null) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : transactionEntryDetail.getTransactionEntry().getBatches().getListBuildPayrollCheckByBatches()) {
							Date date =buildPayrollCheckByBatches.getBuildChecks().getCreatedDate();
							dtoTransactionEntryDisplay.setBuildDate(date);
							SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
							String time = localDateFormat.format(date);
							dtoTransactionEntryDisplay.setBuildtime(time);
							buildPayrollCheckByBatches.getBuildChecks().getId();
							
						}	
					}
					
					dtoTransactionEntryDisplay.setTranscationPrimaryId(transactionEntryDetail.getTransactionEntry().getId());
					dtoTransactionEntryDisplay.setTranscationSubListId(transactionEntryDetail.getId());
					
					if(transactionEntryDetail.getEmployeeMaster()!=null) {
						dtoTransactionEntryDisplay.setEmployeePrimaryId(transactionEntryDetail.getEmployeeMaster().getEmployeeIndexId());
						dtoTransactionEntryDisplay.setEmployeeId(transactionEntryDetail.getEmployeeMaster().getEmployeeId());
						dtoTransactionEntryDisplay.setEmployeeName(transactionEntryDetail.getEmployeeMaster().getEmployeeFirstName() + " "+transactionEntryDetail.getEmployeeMaster().getEmployeeLastName());
						if(transactionEntryDetail.getEmployeeMaster().getDepartment()!=null) {
							dtoTransactionEntryDisplay.setDepartmentId(transactionEntryDetail.getEmployeeMaster().getDepartment().getDepartmentId());
						}
					}
					
					if(transactionEntryDetail.getDimensions()!=null) {
						dtoTransactionEntryDisplay.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());	
						dtoTransactionEntryDisplay.setProjectPrimaryId(transactionEntryDetail.getDimensions().getId());
					}
					
					
					if(transactionEntryDetail.getTransactionType()==1) {
						if(transactionEntryDetail.getCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getCode().getId());
						}
					}
					
					if(transactionEntryDetail.getTransactionType()==2) {
						if(transactionEntryDetail.getDeductionCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getDeductionCode().getId());
						}
					}
					
					if(transactionEntryDetail.getTransactionType()==3) {
						if(transactionEntryDetail.getBenefitCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getBenefitCode().getId());
						}
					}
					
					dtoTransactionEntryDisplay.setTrxType(transactionEntryDetail.getTransactionType());
					dtoTransactionEntryDisplay.setAmount(transactionEntryDetail.getAmount());
					dtoTransactionEntryDisplay.setPayRate(transactionEntryDetail.getPayRate());
					
					displays.add(dtoTransactionEntryDisplay);
					}
				
				if(dtoSearch.getSortOn().equals("3")) {
					Collections.sort(displays, new DtoTransactionEntryDisplay());
				}
				dtoSearch.setRecords(displays);
				}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		
		log.debug("Search TransactionEntry Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
		}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllCalculateCheck(DtoSearch dtoSearch) {
		log.info("search TransactionEntry Method");
		
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("traningId") || dtoSearch.getSortOn().equals("desc")|| dtoSearch.getSortOn().equals("arbicDesc")|| dtoSearch.getSortOn().equals("location")|| dtoSearch.getSortOn().equals("prerequisiteId")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	
				 
				dtoSearch.setTotalCount(this.repositoryTransactionEntryDetail.searchByCalculateCheckCount("%"+searchWord+"%"));
				List<TransactionEntryDetail> traningCourseList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					/*if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){*/
						traningCourseList = this.repositoryTransactionEntryDetail.searchByCalculateCheck("%"+searchWord+"%");
					/*}
					if(dtoSearch.getSortBy().equals("ASC")){
						traningCourseList = this.repositoryTransactionEntryDetail.searchByCalculateCheck("%"+searchWord+"%");
					}else if(dtoSearch.getSortBy().equals("DESC")){
						traningCourseList = this.repositoryTransactionEntryDetail.searchByCalculateCheck("%"+searchWord+"%");
					}*/
					
				}
				
				List<DtoTransactionEntryDisplay> displays = new ArrayList<>();
				List<DtoTransactionEntryDisplay> depositsNewList = new ArrayList<>();
				List<CalculateChecks> calculateChecks = 	repositoryCalculateChecks.findByIsDeleted(false);
				for (CalculateChecks calculateChecks2 : calculateChecks) {
					DtoTransactionEntryDisplay dtoTransactionEntryDisplay = new DtoTransactionEntryDisplay();
					dtoTransactionEntryDisplay.setTranscationNumber(Integer.parseInt(calculateChecks2.getAuditTransNum()));
					
					if(calculateChecks2.getEmployeeMaster()!=null) {
						dtoTransactionEntryDisplay.setEmployeePrimaryId(calculateChecks2.getEmployeeMaster().getEmployeeIndexId());
						dtoTransactionEntryDisplay.setEmployeeId(calculateChecks2.getEmployeeMaster().getEmployeeId());
						dtoTransactionEntryDisplay.setEmployeeName(calculateChecks2.getEmployeeMaster().getEmployeeFirstName() + " "+calculateChecks2.getEmployeeMaster().getEmployeeLastName());
						if(calculateChecks2.getEmployeeMaster().getDepartment()!=null) {
							dtoTransactionEntryDisplay.setDepartmentId(calculateChecks2.getEmployeeMaster().getDepartment().getDepartmentId());
						}
					}
					
					if(calculateChecks2.getDimensions()!=null) {
						dtoTransactionEntryDisplay.setProjectId(calculateChecks2.getDimensions().getDimensionDescription());	
						dtoTransactionEntryDisplay.setProjectPrimaryId(calculateChecks2.getDimensions().getId());
					}
					dtoTransactionEntryDisplay.setTrxType(calculateChecks2.getCodeType());
					dtoTransactionEntryDisplay.setAmount(calculateChecks2.getTotalAmount());
					displays.add(dtoTransactionEntryDisplay);
				}
				
				
				for (TransactionEntryDetail transactionEntryDetail : traningCourseList) {
					DtoTransactionEntryDisplay dtoTransactionEntryDisplay = new DtoTransactionEntryDisplay();
					
					List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatus( // transactionEntryDetail.getDimensions().getId(),
							transactionEntryDetail.getDepartment().getId());
					if(!listAccount.isEmpty() ) {
						for (PayrollAccounts payrollAccounts : listAccount) {
							if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
								String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
								if(accountnum!=null) {
									dtoTransactionEntryDisplay.setStatus(true);
								}
							}
							
						}
						
					}
					
					dtoTransactionEntryDisplay.setTranscationNumber(transactionEntryDetail.getEntryNumber());
					
					dtoTransactionEntryDisplay.setTranscationPrimaryId(transactionEntryDetail.getTransactionEntry().getId());
					dtoTransactionEntryDisplay.setTranscationSubListId(transactionEntryDetail.getId());
					
					if(transactionEntryDetail.getEmployeeMaster()!=null) {
						dtoTransactionEntryDisplay.setEmployeePrimaryId(transactionEntryDetail.getEmployeeMaster().getEmployeeIndexId());
						dtoTransactionEntryDisplay.setEmployeeId(transactionEntryDetail.getEmployeeMaster().getEmployeeId());
						dtoTransactionEntryDisplay.setEmployeeName(transactionEntryDetail.getEmployeeMaster().getEmployeeFirstName() + " "+transactionEntryDetail.getEmployeeMaster().getEmployeeLastName());
						if(transactionEntryDetail.getEmployeeMaster().getDepartment()!=null) {
							dtoTransactionEntryDisplay.setDepartmentId(transactionEntryDetail.getEmployeeMaster().getDepartment().getDepartmentId());
						}
					}
					
					if(transactionEntryDetail.getDimensions()!=null) {
						dtoTransactionEntryDisplay.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());	
						dtoTransactionEntryDisplay.setProjectPrimaryId(transactionEntryDetail.getDimensions().getId());
					}
					
					
					if(transactionEntryDetail.getTransactionType()==1) {
						if(transactionEntryDetail.getCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getCode().getId());
						}
					}
					
					if(transactionEntryDetail.getTransactionType()==2) {
						if(transactionEntryDetail.getDeductionCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getDeductionCode().getId());
						}
					}
					
					if(transactionEntryDetail.getTransactionType()==3) {
						if(transactionEntryDetail.getBenefitCode()!=null) {
							dtoTransactionEntryDisplay.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
							dtoTransactionEntryDisplay.setCodePrimaryId(transactionEntryDetail.getBenefitCode().getId());
						}
					}
					
					dtoTransactionEntryDisplay.setTrxType(transactionEntryDetail.getTransactionType());
					dtoTransactionEntryDisplay.setAmount(transactionEntryDetail.getAmount());
					dtoTransactionEntryDisplay.setPayRate(transactionEntryDetail.getPayRate());
					
					displays.add(dtoTransactionEntryDisplay);
					}
				
				
				dtoSearch.setTotalCount(displays.size());
				if(dtoSearch.getPageNumber().equals(0)) {
					
					if(displays.size() <= dtoSearch.getPageSize()) {
						dtoSearch.setRecords(displays);
					}else {
						depositsNewList = displays.subList(0, dtoSearch.getPageSize());
						dtoSearch.setRecords(depositsNewList);
					}
					
				}else {
					if(displays.size() >  dtoSearch.getPageSize() * (dtoSearch.getPageNumber()+1)) {
						depositsNewList = displays .subList(dtoSearch.getPageNumber() * dtoSearch.getPageSize() ,dtoSearch.getPageSize() * (dtoSearch.getPageNumber()+1));
						dtoSearch.setRecords(depositsNewList);
					}
					else {
						displays.subList(0, dtoSearch.getPageSize()*dtoSearch.getPageNumber()-1).clear();
						dtoSearch.setRecords(displays);
					}
					
				}
				
				}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		
		log.debug("Search TransactionEntry Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
		}
	
	public DtoTransactionEntry deleteBatch(List<Integer> list2) {
		log.info("delete TransactionEntry Method");
		DtoTransactionEntry dtoTraningCourse = new DtoTransactionEntry();
		
		
		
		for (Integer id : list2) {
			
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TRANSCATION_ENTRY_DELETED", false).getMessage());
			
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			dtoTraningCourse.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRANSCATION_ENTRY_DELETED", false));
			dtoTraningCourse.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRANSCATION_ENTRY_DELETED", false));
			
			Batches batches = repositoryBatches.findByIdAndIsDeleted(id, false);
			if(!batches.getListTransactionEntry().isEmpty() && !batches.getListBuildPayrollCheckByBatches().isEmpty()) {
			Set<Integer> listIds = 	batches.getListTransactionEntry().stream().map(t->t.getId()).collect(Collectors.toSet());
				if(!listIds.isEmpty()) {
					Set<Distribution> list=	repositoryDistribution.findByDistributionByTransactionId(listIds);	
					if(!list.isEmpty()) {
						inValidDelete = true;
					}
				}
			}
			
			
			if(!inValidDelete) {
				//repositoryBatches.deleteSingleBatches(true, loggedInUserId, id);
				List<DtoTransactionEntry> deleteTransactionEntry = new ArrayList<>();
				
				try {
					List<TransactionEntry> list =  repositoryTransactionEntry.findByBatches(id);
					for (TransactionEntry transactionEntry : list) {
						//if(transactionEntry.getId()!=null && transactionEntry.getTransDetailList().isEmpty() && transactionEntry.getBatches().getListBuildPayrollCheckByBatches().isEmpty()) {
						if(transactionEntry.getId()!=null &&  transactionEntry.getBatches().getListBuildPayrollCheckByBatches().isEmpty()) {
							repositoryTransactionEntry.deleteSingleTransactionEntry(true, loggedInUserId, transactionEntry.getId());	
						}else {
							inValidDelete = true;
						}
						/*if(!transactionEntry.getBatches().getListBuildPayrollCheckByBatches().isEmpty()) {
							for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : transactionEntry.getBatches().getListBuildPayrollCheckByBatches()) {
								if(buildPayrollCheckByBatches.getBatches().getStatus()==3) {
									repositoryTransactionEntry.deleteSingleTransactionEntry(true, loggedInUserId, transactionEntry.getId());
								}
							}
						}*/
					}
					dtoTraningCourse.setDelete(deleteTransactionEntry);
					
				//	dtoTraningCourse.setDelete(deleteTransactionEntry);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
				} catch (NumberFormatException e) {
					log.error(e);
				}
			}
			if(inValidDelete){
				dtoTraningCourse.setMessageType("");
			}
			if(!inValidDelete){
				
				dtoTraningCourse.setMessageType(invlidDeleteMessage.toString());
			}
			
			
		}
		
		
		log.debug("Delete TransactionEntry :"+dtoTraningCourse.getId());
		return dtoTraningCourse;
	}	
	
	public DtoTransactionEntryDetail deleteBatchRow(List<Integer> ids) {
		log.info("delete TransactionEntry Method");
		DtoTransactionEntryDetail dtoTransactionEntryDetail = new DtoTransactionEntryDetail();
		dtoTransactionEntryDetail.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRANSCATION_ENTRY_DELETED", false));
		dtoTransactionEntryDetail.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRANSCATION_ENTRY_DELETED", false));
		List<DtoTransactionEntryDetail> deleteTransactionEntryDetail = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TRANSCATION_ENTRY_DELETED", false).getMessage());
		try {
			for (Integer id : ids) {
				TransactionEntryDetail transactionEntryDetail = repositoryTransactionEntryDetail.findOne(id);
				if(transactionEntryDetail!=null) {
					if(transactionEntryDetail.getBuildChecks()==null) {
						DtoTransactionEntryDetail dtoTransactionEntry2 = new DtoTransactionEntryDetail();
						dtoTransactionEntry2.setId(transactionEntryDetail.getId());

						repositoryTransactionEntryDetail.deleteSingleTransactionEntryDetail(true, loggedInUserId, id);
						deleteTransactionEntryDetail.add(dtoTransactionEntry2);
					}else {
						inValidDelete = true;
					}
					
				}else {
					inValidDelete = true;
				}
				
				

			}
			dtoTransactionEntryDetail.setDelete(deleteTransactionEntryDetail);
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoTransactionEntryDetail.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoTransactionEntryDetail.setMessageType("");
				
			}
			dtoTransactionEntryDetail.setDelete(deleteTransactionEntryDetail);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete TransactionEntry :"+dtoTransactionEntryDetail.getId());
		return dtoTransactionEntryDetail;
	}

	
	public DtoSearch getAllForFromDateToDate(DtoTransactionEntry dtoTransactionEntry) {

		return null;
	}

	public DtoTransactionEntry repeatByBatchesIdForTransaction(DtoTransactionEntry dtoTransactionEntry) {
		log.info("repeatByBatchesId Method");
		dtoTransactionEntry.setIsRepeat(false);
		try {
			List<TransactionEntryDetail> transactionEntriesDetail=repositoryTransactionEntryDetail.repeatByBatchesIdForTransaction(dtoTransactionEntry.getBatches().getId(), dtoTransactionEntry.getTransactionEntryDetail().getEmployeeMaster().getEmployeeIndexId(), dtoTransactionEntry.getTransactionEntryDetail().getTransactionType());
		   if(transactionEntriesDetail!=null &&!transactionEntriesDetail.isEmpty()) {
			   for (TransactionEntryDetail transactionEntryDetail : transactionEntriesDetail) {
				   if(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType()==1
						   && transactionEntryDetail.getCode().getId().equals(dtoTransactionEntry.getTransactionEntryDetail().getCodeId())) {
					   dtoTransactionEntry.setIsRepeat(true);
				   }
				   if(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType()==2
						   && transactionEntryDetail.getDeductionCode().getId().equals(dtoTransactionEntry.getTransactionEntryDetail().getCodeId())) {
					   dtoTransactionEntry.setIsRepeat(true);
				   }
				   if(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType()==3
						   && transactionEntryDetail.getBenefitCode().getId().equals(dtoTransactionEntry.getTransactionEntryDetail().getCodeId())) {
					   dtoTransactionEntry.setIsRepeat(true);
				   }
				 
			}
		   }
		}catch (Exception e) {
		 log.error(e);
		}
		return dtoTransactionEntry;
	}	
	
	public DtoBuildCheckBatchMarkOrUnMark checkmarkOrUnMark(DtoBuildCheckBatchMarkOrUnMark buildCheckBatchMarkOrUnMark) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if(!buildCheckBatchMarkOrUnMark.getBatchId().isEmpty()) {
				List<TransactionEntry> transactionEntryList = repositoryTransactionEntry.getIncludeBatches(buildCheckBatchMarkOrUnMark.getBatchId());
				if(!transactionEntryList.isEmpty()) {
					for (TransactionEntry transactionEntry : transactionEntryList) {
						for (Integer integer:buildCheckBatchMarkOrUnMark.getBatchId()) {
							List<TransactionEntryDetail> transactionEntryDetailsList = repositoryTransactionEntryDetail.findByTransactionEntry1(transactionEntry.getId());
								if(!transactionEntryDetailsList.isEmpty()) {
									for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailsList) {
										if(integer.equals(transactionEntry.getBatches().getId())) {
											repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, false, transactionEntryDetail.getId());
										}else {
											repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, true, transactionEntryDetail.getId());
										}
										
									}
								}
								
						}
						
					}
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return buildCheckBatchMarkOrUnMark;
		
	}
	
	public DtoBuildDefaultCheck checkAndUnCheckedFromBatchBuildCheck(DtoBuildDefaultCheck dtoBuildDefaultCheck) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if(!dtoBuildDefaultCheck.getBatchId().isEmpty() && dtoBuildDefaultCheck.getBatchId()!=null) {
					List<TransactionEntry> transactionEntryList = repositoryTransactionEntry.getIncludeBatches(dtoBuildDefaultCheck.getBatchId());
					for (TransactionEntry transactionEntry : transactionEntryList) {
						
						List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByTransactionEntry1(transactionEntry.getId());
						for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
							if(transactionEntryDetail.getIsDeleted()==false && transactionEntryDetail.getFromBuild()==true) {
								repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, false, transactionEntryDetail.getId());
							}else if(transactionEntryDetail.getIsDeleted()==false && transactionEntryDetail.getFromBuild()==false) {
								repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, true, transactionEntryDetail.getId());
							}
							 
						}
						
					}
					
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildDefaultCheck;
		
	}


	public DtoTransactionEntryDetail getById(Integer id) {
		DtoTransactionEntryDetail dtoTransactionEntryDetail=new DtoTransactionEntryDetail();
		TransactionEntryDetail transactionEntryDetail=repositoryTransactionEntryDetail.findById(id);
		if(transactionEntryDetail!=null) {
			dtoTransactionEntryDetail.setId(transactionEntryDetail.getId());
			dtoTransactionEntryDetail.setAmount(transactionEntryDetail.getAmount());
			dtoTransactionEntryDetail.setPayRate(transactionEntryDetail.getPayRate());
			dtoTransactionEntryDetail.setTransactionType(transactionEntryDetail.getTransactionType());
			DtoEmployeeMasterHcm dtoEmployeeMasterHcm=new DtoEmployeeMasterHcm();
				if(transactionEntryDetail.getEmployeeMaster()!=null) {
					dtoEmployeeMasterHcm.setEmployeeIndexId(transactionEntryDetail.getEmployeeMaster().getEmployeeIndexId());
					dtoEmployeeMasterHcm.setEmployeeFirstName(transactionEntryDetail.getEmployeeMaster().getEmployeeFirstName());
					dtoEmployeeMasterHcm.setEmployeeId(transactionEntryDetail.getEmployeeMaster().getEmployeeId());
					dtoEmployeeMasterHcm.setEmployeeMiddleName(transactionEntryDetail.getEmployeeMaster().getEmployeeMiddleName());
					dtoEmployeeMasterHcm.setEmployeeLastName(transactionEntryDetail.getEmployeeMaster().getEmployeeLastName());
					dtoTransactionEntryDetail.setEmployeeMaster(dtoEmployeeMasterHcm);
				}
				DtoPayCode dtoPayCode=new DtoPayCode();
				if(transactionEntryDetail.getCode()!=null) {
					dtoPayCode.setId(transactionEntryDetail.getCode().getId());
					dtoPayCode.setArbicDescription(transactionEntryDetail.getCode().getArbicDescription());
					dtoPayCode.setAmount(transactionEntryDetail.getCode().getBaseOnPayCodeAmount());
					dtoPayCode.setPayRate(transactionEntryDetail.getCode().getPayRate());
					dtoPayCode.setPayFactor(transactionEntryDetail.getCode().getPayFactor());
					dtoPayCode.setPayCodeId(transactionEntryDetail.getCode().getPayCodeId());
					dtoTransactionEntryDetail.setCode(dtoPayCode);
				}
				DtoBenefitCode dtoBenefitCode=new DtoBenefitCode();
				if(transactionEntryDetail.getBenefitCode()!=null) {
					dtoBenefitCode.setId(transactionEntryDetail.getBenefitCode().getId());
					dtoBenefitCode.setBenefitId(transactionEntryDetail.getBenefitCode().getBenefitId());
					dtoBenefitCode.setDesc(transactionEntryDetail.getBenefitCode().getDesc());
					dtoBenefitCode.setArbicDesc(transactionEntryDetail.getBenefitCode().getArbicDesc());
					dtoBenefitCode.setAmount(transactionEntryDetail.getBenefitCode().getAmount());
					dtoBenefitCode.setPayFactor(transactionEntryDetail.getBenefitCode().getPayFactor());
					dtoBenefitCode.setPercent(transactionEntryDetail.getBenefitCode().getPercent());
					dtoTransactionEntryDetail.setBenefitCode(dtoBenefitCode);
				}
				DtoDeductionCode dtoDeductionCode=new DtoDeductionCode();
				if(transactionEntryDetail.getDeductionCode()!=null) {
					dtoDeductionCode.setId(transactionEntryDetail.getDeductionCode().getId());
					dtoDeductionCode.setDiductionId(transactionEntryDetail.getDeductionCode().getDiductionId());
					dtoDeductionCode.setDiscription(transactionEntryDetail.getDeductionCode().getDiscription());
					dtoDeductionCode.setArbicDiscription(transactionEntryDetail.getDeductionCode().getArbicDiscription());
					dtoDeductionCode.setAmount(transactionEntryDetail.getDeductionCode().getAmount());
					dtoDeductionCode.setPayFactor(transactionEntryDetail.getDeductionCode().getPayFactor());
					dtoDeductionCode.setPercent(transactionEntryDetail.getDeductionCode().getPercent());
					dtoTransactionEntryDetail.setDeductionCode(dtoDeductionCode);
				}
				DtoDepartment dtoDepartment=new DtoDepartment();
				if(transactionEntryDetail.getDepartment()!=null) {
					dtoDepartment.setId(transactionEntryDetail.getDepartment().getId());
					dtoDepartment.setDepartmentId(transactionEntryDetail.getDepartment().getDepartmentId());
					dtoDepartment.setArabicDepartmentDescription(transactionEntryDetail.getDepartment().getArabicDepartmentDescription());
					dtoDepartment.setDepartmentDescription(transactionEntryDetail.getDepartment().getDepartmentDescription());
					dtoTransactionEntryDetail.setDepartment(dtoDepartment);
				}
				DtoFinancialDimensions dtoFinancialDimensions=new DtoFinancialDimensions();
					if(transactionEntryDetail.getDimensions()!=null) {
						dtoFinancialDimensions.setId(transactionEntryDetail.getDimensions().getId());
						dtoFinancialDimensions.setDimensionColumnName(transactionEntryDetail.getDimensions().getDimensionColumnName());
						dtoFinancialDimensions.setDimensionArbicDescription(transactionEntryDetail.getDimensions().getDimensionArbicDescription());
						dtoFinancialDimensions.setDimensionArbicDescription(transactionEntryDetail.getDimensions().getDimensionDescription());
						dtoTransactionEntryDetail.setDtoFinancialDimensions(dtoFinancialDimensions);
					}
				}
		return dtoTransactionEntryDetail;
	}
	
}
