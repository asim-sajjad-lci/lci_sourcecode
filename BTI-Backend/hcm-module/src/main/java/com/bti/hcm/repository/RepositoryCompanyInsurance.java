package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.CompnayInsurance;
import com.bti.hcm.model.HelthInsurance;

/**
 * @author gaurav
 *
 */
@Repository("repositoryCompanyInsurance")
public interface RepositoryCompanyInsurance extends JpaRepository<CompnayInsurance, Integer> {

	CompnayInsurance findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from CompnayInsurance d where d.isDeleted=false")
	Integer getCountOfTotalHelthInsurance();

	List<CompnayInsurance> findByIsDeleted(boolean b, Pageable pageable);

	List<CompnayInsurance> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update CompnayInsurance d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleHelthInsurance(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);


	@Query("select p from CompnayInsurance p where (p.insCompanyId =:insCompanyId) and p.isDeleted=false")
	List<CompnayInsurance> findByInsCompanyId(@Param("insCompanyId") String searchKeyWord);

	@Query("select count(*) from CompnayInsurance p where (p.contactPerson like :searchKeyWord or p.email like :searchKeyWord or p.fax like :searchKeyWord or p.phoneNo like :searchKeyWord or p.address like :searchKeyWord or p.arbicDesc like :searchKeyWord or p.insCompanyId like :searchKeyWord or p.insCompanyId like :searchKeyWord) and p.isDeleted=false")
	Integer predictivePositionSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select p from CompnayInsurance p where ( p.contactPerson like :searchKeyWord or p.email like :searchKeyWord or p.fax like :searchKeyWord or p.phoneNo like :searchKeyWord  or p.address like :searchKeyWord or p.arbicDesc like :searchKeyWord or p.insCompanyId like :searchKeyWord or p.insCompanyId like :searchKeyWord) and p.isDeleted=false")
	List<CompnayInsurance> predictivePositionSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	 @Query("select d.insCompanyId from CompnayInsurance d where (d.insCompanyId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<String> predictiveCompanyIdSearchWithPagination(@Param("searchKeyWord") String string);
	 
	 public List<CompnayInsurance> findByIsDeleted(Boolean deleted);

	 
	 @Query("select d from HelthInsurance d where (d.helthInsuranceId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<HelthInsurance> predictiveHelthInsuranceIdSearchWithPagination(@Param("searchKeyWord")String helthInsuranceId);

	@Query("select count(*) from CompnayInsurance c ")
	public Integer getCountOfTotalCompanyInsurance();




	 
}
