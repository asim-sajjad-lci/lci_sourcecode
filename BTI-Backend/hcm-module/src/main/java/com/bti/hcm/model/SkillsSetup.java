package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the Skill Set table.
 * Name of Project: Hrm
 * Version: 0.0.1
 * @author Gaurav
 *
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40600",indexes = {
        @Index(columnList = "SKLINDX")
})
@NamedQuery(name = "SkillsSetup.findAll", query = "SELECT s FROM SkillsSetup s")
public class SkillsSetup extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SKLINDX")
	private Integer id;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "skillsId")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<SkillSetDesc> listSkillsetDesc;
	
	@ManyToMany(mappedBy = "skillsSetup",cascade = {CascadeType.ALL})
	private List<SkillSetDesc> listDesc;
	
	@Column(name = "SKLID",columnDefinition="char(15)")
	private String skillId;
	
	@Column(name = "SKLDSCR",columnDefinition="char(31)")
	private String skillDesc;
	
	@Column(name = "SKLDSCRA",columnDefinition="char(61)")
	private String arabicDesc;
	
	
	@Column(name = "SKLCOMAMT",precision = 13,scale =3)
	private BigDecimal compensation;
	
	@Column(name = "SKLCOMPER")
	private Short frequency;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	
	

	public BigDecimal getCompensation() {
		return compensation;
	}

	public void setCompensation(BigDecimal compensation) {
		this.compensation = compensation;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Short getFrequency() {
		return frequency;
	}

	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}

	public String getSkillId() {
		return skillId;
	}

	public void setSkillId(String skillId) {
		this.skillId = skillId;
	}

	public String getSkillDesc() {
		return skillDesc;
	}

	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}

	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

	public List<SkillSetDesc> getListSkillsetDesc() {
		return listSkillsetDesc;
	}

	public void setListSkillsetDesc(List<SkillSetDesc> listSkillsetDesc) {
		this.listSkillsetDesc = listSkillsetDesc;
	}

	public List<SkillSetDesc> getListDesc() {
		return listDesc;
	}

	public void setListDesc(List<SkillSetDesc> listDesc) {
		this.listDesc = listDesc;
	}
	
	
	
}
