package com.bti.hcm.model.dto;

import java.util.Date;

public class DtoEmployeeVactionTicket {

	private Integer idx;
	private Integer employeeVacationRequestId;
	private Boolean isDependant;
	private String departFrom;
	private String arrivedTo;
	private Date departureDate;
	private String ticketClass;
	private String notes;

	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}

	public Integer getEmployeeVacationRequestId() {
		return employeeVacationRequestId;
	}

	public void setEmployeeVacationRequestId(Integer employeeVacationRequestId) {
		this.employeeVacationRequestId = employeeVacationRequestId;
	}

	public Boolean getIsDependant() {
		return isDependant;
	}

	public void setIsDependant(Boolean isDependant) {
		this.isDependant = isDependant;
	}

	public String getDepartFrom() {
		return departFrom;
	}

	public void setDepartFrom(String departFrom) {
		this.departFrom = departFrom;
	}

	public String getArrivedTo() {
		return arrivedTo;
	}

	public void setArrivedTo(String arrivedTo) {
		this.arrivedTo = arrivedTo;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getTicketClass() {
		return ticketClass;
	}

	public void setTicketClass(String ticketClass) {
		this.ticketClass = ticketClass;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	} 

}
