package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.PayCodeType;
import com.bti.hcm.model.dto.DtoPayCodeType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPayCodeType;

@Service("servicePayCodeType")
public class ServicePayCodeType {

	static Logger log = Logger.getLogger(ServicePayCodeType.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryPayCodeType repositoryPayCodeType;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPayCodeTypeIds(DtoSearch dtoSearch) {
		try {
			log.info("searchPayCodeType Method");
			if(dtoSearch != null){
				List<PayCodeType> listPayCodeType = null;
				listPayCodeType = this.repositoryPayCodeType.findAll();
					
				if(listPayCodeType != null && !listPayCodeType.isEmpty()){
					List<DtoPayCodeType> dtoPayCodeTypeList = new ArrayList<DtoPayCodeType>();
					DtoPayCodeType dtoPayCodeType=null;
					for (PayCodeType payScheduleSetup : listPayCodeType) {
						dtoPayCodeType = new DtoPayCodeType(payScheduleSetup);
						dtoPayCodeType.setId(payScheduleSetup.getId());
						dtoPayCodeType.setDesc(payScheduleSetup.getDesc());
						dtoPayCodeType.setArabicDesc(payScheduleSetup.getArabicDesc());
						dtoPayCodeTypeList.add(dtoPayCodeType);
					}
					dtoSearch.setRecords(dtoPayCodeTypeList);
					dtoSearch.setTotalCount(dtoPayCodeTypeList.size());
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	
	public List<DtoPayCodeType> getAllPayCodeTypeDropDown() {
		log.info("getAllPayCodeTypeDropDown Method");
		List<DtoPayCodeType> dtoPayCodeTypeList = new ArrayList<>();
		try {
			List<PayCodeType> list = repositoryPayCodeType.findByIsDeleted(false);
			
			if (list != null && !list.isEmpty()) {
				for (PayCodeType payCodeType : list) {
					DtoPayCodeType dtoPayCodeType = new DtoPayCodeType();
					
					dtoPayCodeType.setId(payCodeType.getId());
					dtoPayCodeType.setDesc(payCodeType.getDesc());
					dtoPayCodeType.setArabicDesc(payCodeType.getArabicDesc());
					dtoPayCodeTypeList.add(dtoPayCodeType);
				}
			}
			log.debug("Position is:"+dtoPayCodeTypeList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayCodeTypeList;
	}
	
}
