package com.bti.hcm.model.dto;

import java.sql.Date;
import java.util.List;

public class DtoOrientationSetupDetail extends DtoBase{
	private Integer id;
	private Integer sequence;
	private String checklistitem;
	private String arabicDescription;
	private Date startdate;
	private Date enddate;
	private Date starttime;
	private Date endtime;
	private String personResponsible;
	private List<DtoOrientationSetupDetail> delete;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getChecklistitem() {
		return checklistitem;
	}

	public void setChecklistitem(String checklistitem) {
		this.checklistitem = checklistitem;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	public String getPersonResponsible() {
		return personResponsible;
	}

	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}
	public List<DtoOrientationSetupDetail> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoOrientationSetupDetail> delete) {
		this.delete = delete;
	}

}
