package com.bti.hcm.model.dto;

import java.util.List;

/**
 * 
 * @author HAMID
 *
 */
public class DtoTotalPackageESB extends DtoBase {

	private Integer id; // no need (as we are using saveAll for saveOrUpdate Approach)
	private Integer typeId; // 1 or 3
	// private Integer typeIndexId; // For SAVE tobe inside dtoMSList
	private List<DtoMultiselect> dtoMultiSelectList; // typeIndexIds (contains id)

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public List<DtoMultiselect> getDtoMultiSelectList() {
		return dtoMultiSelectList;
	}

	public void setDtoMultiSelectList(List<DtoMultiselect> dtoMultiSelectList) {
		this.dtoMultiSelectList = dtoMultiSelectList;
	}

}
