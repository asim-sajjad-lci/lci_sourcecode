package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoDateConvertReq {
	
	private String gregorianDate;
	private String hijriDate;
	private String convertToHijri;
	private String convertToGregorian;
	public String getGregorianDate() {
		return gregorianDate;
	}
	public void setGregorianDate(String gregorianDate) {
		this.gregorianDate = gregorianDate;
	}
	public String getHijriDate() {
		return hijriDate;
	}
	public void setHijriDate(String hijriDate) {
		this.hijriDate = hijriDate;
	}
	public String getConvertToHijri() {
		return convertToHijri;
	}
	public void setConvertToHijri(String convertToHijri) {
		this.convertToHijri = convertToHijri;
	}
	public String getConvertToGregorian() {
		return convertToGregorian;
	}
	public void setConvertToGregorian(String convertToGregorian) {
		this.convertToGregorian = convertToGregorian;
	}
	
	
	

}
