package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.Default;

public class DtoDefault extends DtoBase{

	private Integer id;
	private String defaultID;
	private String desc;
	private String arabicDesc;
	private Integer createdBy;
	private Date payrollDate;
	private Date buildDate;
	private String buildtime;
	private Integer userId;
	private List<DtoDefault> delete;
	private DtoDistribrution dtoDistribrution;
	private Date postingDate;

	public DtoDefault(Default default1) {
		
	}

	public DtoDefault() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDefaultID() {
		return defaultID;
	}

	public void setDefaultID(String defaultID) {
		this.defaultID = defaultID;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}


	public List<DtoDefault> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoDefault> delete) {
		this.delete = delete;
	}


	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getPayrollDate() {
		return payrollDate;
	}

	public void setPayrollDate(Date payrollDate) {
		this.payrollDate = payrollDate;
	}


	public Date getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}

	public String getBuildtime() {
		return buildtime;
	}

	public void setBuildtime(String buildtime) {
		this.buildtime = buildtime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public DtoDistribrution getDtoDistribrution() {
		return dtoDistribrution;
	}

	public void setDtoDistribrution(DtoDistribrution dtoDistribrution) {
		this.dtoDistribrution = dtoDistribrution;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}
	

}
