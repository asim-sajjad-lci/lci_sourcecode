package com.bti.hcm.model.dto;

import java.math.BigDecimal;

public class DtoParollTransactionOpenYearDistribution  extends DtoBase{
	private Integer id;
	private String auditTransactionNumber;
	private Integer employeeIndexId;
	private Long accountTableRowIndex;
	private Short postingTypes;
	private Integer codeIndexId;
	private BigDecimal debitAmount;
	private BigDecimal creaditAmount;
	private Integer accountSequence;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAuditTransactionNumber() {
		return auditTransactionNumber;
	}
	public void setAuditTransactionNumber(String auditTransactionNumber) {
		this.auditTransactionNumber = auditTransactionNumber;
	}
	public Integer getEmployeeIndexId() {
		return employeeIndexId;
	}
	public void setEmployeeIndexId(Integer employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}
	public Long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}
	public void setAccountTableRowIndex(Long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}
	public Short getPostingTypes() {
		return postingTypes;
	}
	public void setPostingTypes(Short postingTypes) {
		this.postingTypes = postingTypes;
	}
	public Integer getCodeIndexId() {
		return codeIndexId;
	}
	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}
	public BigDecimal getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}
	public BigDecimal getCreaditAmount() {
		return creaditAmount;
	}
	public void setCreaditAmount(BigDecimal creaditAmount) {
		this.creaditAmount = creaditAmount;
	}
	public Integer getAccountSequence() {
		return accountSequence;
	}
	public void setAccountSequence(Integer accountSequence) {
		this.accountSequence = accountSequence;
	}

}
