package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoManualChecksDistribution;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceManualChecksDistribution;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/manualChecksDistribution")
public class ControllerManualChecksDistribution extends BaseController{
	
	private static final Logger LOGGER = Logger.getLogger(ControllerManualChecksDistribution.class);

	@Autowired
	ServiceManualChecksDistribution serviceManualChecksDistribution;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoManualChecksDistribution dtoManualChecksDistribution) throws Exception {
		LOGGER.info("Create manualChecksDistribution Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoManualChecksDistribution  = serviceManualChecksDistribution.saveOrUpdate(dtoManualChecksDistribution);
			if (dtoManualChecksDistribution != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("MANUAL_CHECKS_DISTRIBUTION", false), dtoManualChecksDistribution);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("MANUAL_CHECKS_DISTRIBUTION_NOT_CREATED", false), dtoManualChecksDistribution);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Create manualChecksDistribution Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search manualChecksDistribution Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceManualChecksDistribution.getAll(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "MANUAL_CHECKS_DISTRIBUTION_GET_ALL", "MANUAL_CHECKS_DISTRIBUTION_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search manualChecksDistribution Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	
}
