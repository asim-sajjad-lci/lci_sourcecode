/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.InterviewTypeSetup;
import com.bti.hcm.model.InterviewTypeSetupDetail;
import com.bti.hcm.model.dto.DtoInterviewTypeSetup;
import com.bti.hcm.model.dto.DtoInterviewTypeSetupDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryInterviewTypeSetup;
import com.bti.hcm.repository.RepositoryInterviewTypeSetupDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service InterviewTypeSetup
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("serviceInterviewTypeSetup")
public class ServiceInterviewTypeSetup {
	
	
	
	/**
	 * @Description LOGGER use for put a logger in InterviewTypeSetup Service
	 */
	static Logger log = Logger.getLogger(ServiceInterviewTypeSetup.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in InterviewTypeSetup service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in InterviewTypeSetup service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in InterviewTypeSetup service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryInterviewTypeSetup Autowired here using annotation of spring for access of repositoryInterviewTypeSetup method in InterviewTypeSetup service
	 * 				In short Access InterviewTypeSetup Query from Database using repositoryInterviewTypeSetup.
	 */
	@Autowired
	RepositoryInterviewTypeSetup repositoryInterviewTypeSetup;
	
	@Autowired
	RepositoryInterviewTypeSetupDetail repositoryInterviewTypeSetupDetail;
	
	/**
	 * @Description: save and update InterviewTypeSetup data
	 * @param dtoInterviewTypeSetup
	 * @return
	 * @throws ParseException
	 */
	public DtoInterviewTypeSetup saveOrUpdateInterviewTypeSetup(DtoInterviewTypeSetup dtoInterviewTypeSetup) throws ParseException {
		try {
			log.info("saveOrUpdateInterviewTypeSetup Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			InterviewTypeSetup interviewTypeSetup = null;
			
			if (dtoInterviewTypeSetup.getId() != null && dtoInterviewTypeSetup.getId() > 0) {
				interviewTypeSetup = repositoryInterviewTypeSetup.findByIdAndIsDeleted(dtoInterviewTypeSetup.getId(), false);
				interviewTypeSetup.setUpdatedBy(loggedInUserId);
				interviewTypeSetup.setUpdatedDate(new Date());
			} else {
				interviewTypeSetup = new InterviewTypeSetup();
				interviewTypeSetup.setCreatedDate(new Date());
				
				Integer rowId = repositoryInterviewTypeSetup.getCountOfTotalInterviewTypeSetups();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				interviewTypeSetup.setRowId(increment);
			}
			interviewTypeSetup.setInterviewTypeId(dtoInterviewTypeSetup.getInterviewTypeId());
			interviewTypeSetup.setInterviewTypeDescriptionArabic(dtoInterviewTypeSetup.getInterviewTypeDescriptionArabic());
			interviewTypeSetup.setInterviewTypeDescription(dtoInterviewTypeSetup.getInterviewTypeDescription());
			interviewTypeSetup.setInterviewRange(dtoInterviewTypeSetup.getInterviewRange());
			interviewTypeSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			interviewTypeSetup = repositoryInterviewTypeSetup.saveAndFlush(interviewTypeSetup);
			if(dtoInterviewTypeSetup.getDtoInterviewTypeSetupDetail()!=null && !dtoInterviewTypeSetup.getDtoInterviewTypeSetupDetail().isEmpty()) {
				for(DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail: dtoInterviewTypeSetup.getDtoInterviewTypeSetupDetail()) {
					InterviewTypeSetupDetail interviewTypeSetupDetail = null;
					if(dtoInterviewTypeSetupDetail.getId()!=null && dtoInterviewTypeSetupDetail.getId()>0) {
						interviewTypeSetupDetail   = repositoryInterviewTypeSetupDetail.findByIdAndIsDeleted(dtoInterviewTypeSetupDetail.getId(), false);
					}else {
						interviewTypeSetupDetail = new InterviewTypeSetupDetail();
						interviewTypeSetupDetail.setCreatedDate(new Date());
					}
					interviewTypeSetupDetail.setInterviewTypeSetupDetailDescription(dtoInterviewTypeSetupDetail.getInterviewTypeSetupDetailDescription());
					interviewTypeSetupDetail.setInterviewTypeSetupDetailCategoryRowSequance(dtoInterviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryRowSequance());
					interviewTypeSetupDetail.setInterviewTypeSetupDetailCategorySequance(dtoInterviewTypeSetupDetail.getInterviewTypeSetupDetailCategorySequance());
					interviewTypeSetupDetail.setInterviewTypeSetupDetailCategoryWeight(dtoInterviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryWeight());
					interviewTypeSetupDetail.setInterviewTypeSetup(interviewTypeSetup);
					interviewTypeSetupDetail.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
					repositoryInterviewTypeSetupDetail.saveAndFlush(interviewTypeSetupDetail);
				}
			}
			
			log.debug("InterviewTypeSetup is:"+dtoInterviewTypeSetup.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoInterviewTypeSetup;
	}

	
	/**
	 * @Description: Search InterviewTypeSetup data
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchInterviewTypeSetup(DtoSearch dtoSearch) {
		try {

			log.info("searchInterviewTypeSetup Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";

				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "interviewTypeId" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "interviewTypeDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "interviewTypeDescriptionArabic" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "interviewRange" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
					
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
				
			}
				dtoSearch.setTotalCount(this.repositoryInterviewTypeSetup.predictiveInterviewTypeSetupSearchTotalCount("%"+searchWord+"%"));
				List<InterviewTypeSetup> interviewTypeSetupList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						interviewTypeSetupList = this.repositoryInterviewTypeSetup.predictiveInterviewTypeSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						interviewTypeSetupList = this.repositoryInterviewTypeSetup.predictiveInterviewTypeSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						interviewTypeSetupList = this.repositoryInterviewTypeSetup.predictiveInterviewTypeSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(interviewTypeSetupList != null && !interviewTypeSetupList.isEmpty()){
					List<DtoInterviewTypeSetup> dtoInterviewTypeSetupList = new ArrayList<>();
					DtoInterviewTypeSetup dtoInterviewTypeSetup=null;
					for (InterviewTypeSetup interviewTypeSetup : interviewTypeSetupList) {
						dtoInterviewTypeSetup = new DtoInterviewTypeSetup(interviewTypeSetup);
						dtoInterviewTypeSetup.setId(interviewTypeSetup.getId());
						dtoInterviewTypeSetup.setInterviewTypeId(interviewTypeSetup.getInterviewTypeId());
						dtoInterviewTypeSetup.setInterviewTypeDescription(interviewTypeSetup.getInterviewTypeDescription());
						dtoInterviewTypeSetup.setInterviewTypeDescriptionArabic(interviewTypeSetup.getInterviewTypeDescriptionArabic());
						dtoInterviewTypeSetup.setInterviewRange(interviewTypeSetup.getInterviewRange());
						List<DtoInterviewTypeSetupDetail> dtoInterviewTypeSetupDetailList = new ArrayList<>();
						if(interviewTypeSetup.getInterviewTypeSetupDetail()!=null) {
							for(InterviewTypeSetupDetail interviewTypeSetupDetail : interviewTypeSetup.getInterviewTypeSetupDetail()) {
								if(interviewTypeSetupDetail.getIsDeleted()==false) {
									DtoInterviewTypeSetupDetail dtoInterviewTypeSetupDetail = new DtoInterviewTypeSetupDetail(interviewTypeSetupDetail);
									dtoInterviewTypeSetupDetail.setId(interviewTypeSetupDetail.getId());
									dtoInterviewTypeSetupDetail.setInterviewDetailId(interviewTypeSetupDetail.getInterviewTypeSetup().getId());
									dtoInterviewTypeSetupDetail.setInterviewTypeSetupDetailCategoryRowSequance(interviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryRowSequance());
									dtoInterviewTypeSetupDetail.setInterviewTypeSetupDetailDescription(interviewTypeSetupDetail.getInterviewTypeSetupDetailDescription());
									dtoInterviewTypeSetupDetail.setInterviewTypeSetupDetailCategoryWeight(interviewTypeSetupDetail.getInterviewTypeSetupDetailCategoryWeight());
									dtoInterviewTypeSetupDetailList.add(dtoInterviewTypeSetupDetail);
								}
									
							}
							
						}
						dtoInterviewTypeSetup.setDtoInterviewTypeSetupDetail(dtoInterviewTypeSetupDetailList);
						dtoInterviewTypeSetupList.add(dtoInterviewTypeSetup);
					}
					dtoSearch.setRecords(dtoInterviewTypeSetupList);
				}
			}
			log.debug("Search InterviewTypeSetup Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description: get InterviewTypeSetup data by id
	 * @param id
	 * @return
	 */
	public DtoInterviewTypeSetup getInterviewTypeSetupByInterviewTypeSetupId(int id) {
		log.info("getInterviewTypeSetupByInterviewTypeSetupId Method");
		DtoInterviewTypeSetup dtoInterviewTypeSetup = new DtoInterviewTypeSetup();
		try {
			if (id > 0) {
				InterviewTypeSetup interviewTypeSetup = repositoryInterviewTypeSetup.findByIdAndIsDeleted(id, false);
				if (interviewTypeSetup != null) {
					dtoInterviewTypeSetup = new DtoInterviewTypeSetup(interviewTypeSetup);
				} else {
					dtoInterviewTypeSetup.setMessageType("INTERVIEW_TYPE_SETUP_NOT_GETTING");

				}
			} else {
				dtoInterviewTypeSetup.setMessageType("INVALID_INTERVIEW_TYPE_SETUP_ID");

			}
			log.debug("InterviewTypeSetup By Id is:"+dtoInterviewTypeSetup.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoInterviewTypeSetup;
	}
	
	/**
	 * @Description: delete InterviewTypeSetup
	 * @param ids
	 * @return
	 */
	public DtoInterviewTypeSetup deleteInterviewTypeSetup(List<Integer> ids) {
		log.info("deleteInterviewTypeSetup Method");
		DtoInterviewTypeSetup dtoInterviewTypeSetup = new DtoInterviewTypeSetup();
		dtoInterviewTypeSetup
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("INTERVIEW_TYPE_SETUP_DELETED", false));
		dtoInterviewTypeSetup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("INTERVIEW_TYPE_SETUP_ASSOCIATED", false));
		List<DtoInterviewTypeSetup> deleteDtoInterviewTypeSetupt = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("INTERVIEW_TYPE_SETUP_DELETED_ID_MESSAGE", false).getMessage());
		try {
			for (Integer id : ids) {
				InterviewTypeSetup interviewTypeSetup = repositoryInterviewTypeSetup.findOne(id);
				if(interviewTypeSetup.getInterviewTypeSetupDetail().isEmpty()) {
					List<InterviewTypeSetupDetail> checkInterViewTypeSetupDetail=repositoryInterviewTypeSetupDetail.findByInterViewTypeSetupId(id);
					if(checkInterViewTypeSetupDetail.isEmpty()) {
						DtoInterviewTypeSetup dtoInterviewTypeSetup2 = new DtoInterviewTypeSetup();
						dtoInterviewTypeSetup2.setId(id);
						dtoInterviewTypeSetup2.setInterviewTypeId(interviewTypeSetup.getInterviewTypeId());
						dtoInterviewTypeSetup2.setInterviewTypeDescription(interviewTypeSetup.getInterviewTypeDescription());
						dtoInterviewTypeSetup2.setInterviewTypeDescriptionArabic(interviewTypeSetup.getInterviewTypeDescriptionArabic());
						dtoInterviewTypeSetup2.setInterviewRange(interviewTypeSetup.getInterviewRange());
						repositoryInterviewTypeSetup.deleteSingleInterviewTypeSetup(true, loggedInUserId, id);
						deleteDtoInterviewTypeSetupt.add(dtoInterviewTypeSetup2);
				}
				}else {
					inValidDelete = true;
					invlidDeleteMessage.append(interviewTypeSetup.getId()+",");
				}
			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoInterviewTypeSetup.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoInterviewTypeSetup.setMessageType("INTERVIEW_TYPE_SETUP_DELETED");
			}
			dtoInterviewTypeSetup.setDeleteInterviewTypeSetup(deleteDtoInterviewTypeSetupt);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete InterviewTypeSetup :"+dtoInterviewTypeSetup.getId());
		return dtoInterviewTypeSetup;
	}
	
	/**
	 * @Description: check Repeat interviewTypeSetupId
	 * @param interviewTypeSetupId
	 * @return
	 */
	public DtoInterviewTypeSetup repeatByDtoInterviewTypeSetupId(String interviewTypeSetupId) {
		log.info("repeatByDtoInterviewTypeSetupId Method");
		DtoInterviewTypeSetup dtoInterviewTypeSetup = new DtoInterviewTypeSetup();
		try {
			List<InterviewTypeSetup> interviewTypeSetup=repositoryInterviewTypeSetup.findByInterviewTypeId(interviewTypeSetupId);
			if(interviewTypeSetup!=null && !interviewTypeSetup.isEmpty()) {
				dtoInterviewTypeSetup.setIsRepeat(true);
			}else {
				dtoInterviewTypeSetup.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoInterviewTypeSetup;
	}
}
