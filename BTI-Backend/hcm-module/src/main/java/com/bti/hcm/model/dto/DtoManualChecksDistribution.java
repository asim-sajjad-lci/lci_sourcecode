package com.bti.hcm.model.dto;

import com.bti.hcm.model.ManualChecksDistribution;
import com.ibm.icu.math.BigDecimal;

public class DtoManualChecksDistribution extends DtoBase {

	private Integer id;
	private Short transactionType;
	private Integer codeId;
	private BigDecimal debitAmount;
	private BigDecimal creditAmount;
	private Integer accountNumberSequence;
	
	private DtoDepartment dtoDepartment;
	private DtoPosition dtoPosition;
	private DtoManualChecks dtoManualChecks;
	private DtoAccountsTableAccumulation dtoAccountsTableAccumulation;

	public DtoManualChecksDistribution(DtoManualChecksDistribution dtoManualChecksDistribution) {

	}

	public DtoManualChecksDistribution(ManualChecksDistribution manualChecksDistribution) {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Short transactionType) {
		this.transactionType = transactionType;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Integer getAccountNumberSequence() {
		return accountNumberSequence;
	}

	public void setAccountNumberSequence(Integer accountNumberSequence) {
		this.accountNumberSequence = accountNumberSequence;
	}

	public DtoDepartment getDtoDepartment() {
		return dtoDepartment;
	}

	public void setDtoDepartment(DtoDepartment dtoDepartment) {
		this.dtoDepartment = dtoDepartment;
	}

	public DtoPosition getDtoPosition() {
		return dtoPosition;
	}

	public void setDtoPosition(DtoPosition dtoPosition) {
		this.dtoPosition = dtoPosition;
	}

	public DtoManualChecks getDtoManualChecks() {
		return dtoManualChecks;
	}

	public void setDtoManualChecks(DtoManualChecks dtoManualChecks) {
		this.dtoManualChecks = dtoManualChecks;
	}

	public DtoAccountsTableAccumulation getDtoAccountsTableAccumulation() {
		return dtoAccountsTableAccumulation;
	}

	public void setDtoAccountsTableAccumulation(DtoAccountsTableAccumulation dtoAccountsTableAccumulation) {
		this.dtoAccountsTableAccumulation = dtoAccountsTableAccumulation;
	}

	
}
