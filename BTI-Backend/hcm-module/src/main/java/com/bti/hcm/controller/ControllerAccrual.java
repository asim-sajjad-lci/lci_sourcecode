package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoAccrual;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceAccrual;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/accrualSetup")
public class ControllerAccrual extends BaseController{
	
	private Logger log = Logger.getLogger(ControllerAccrual.class);
	
	@Autowired
	ServiceAccrual serviceAccrual;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	/**
	 * @param request{
		  
   "accuralId":"1",
   "desc":"test",
   "arbic":"test1223",
   "numHour":4,
   "maxAccrualHour":"8",
   "maxHour":"9",
   "workHour":"3"
		  
		}
	 * @param dtoDeductionCode
	 * @return
	 * @throws Exception
	 */
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createAccrual(HttpServletRequest request, @RequestBody DtoAccrual dtoAccrual) throws Exception{
		log.info("Create Accrual Method");
		ResponseMessage responseMessage = null;
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtoAccrual  = serviceAccrual.saveOrUpdate(dtoAccrual);
				responseMessage = displayMessage(dtoAccrual,"ACCRUAL_CREATED","ACCRUALSETUP_NOT_CREATED",serviceResponse);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			log.debug("Create Accrual Method:"+responseMessage.getMessage());
		    return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccrual.searAccrual(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.ACCRUAL_GET_ALL, MessageConstant.ACCRUAL_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoAccrual dtoAccrual) throws Exception {
		log.info("Update Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrual = serviceAccrual.saveOrUpdate(dtoAccrual);
			responseMessage=displayMessage(dtoAccrual, "ACCRUAL_UPDATED", "ACCRUAL_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update Accrual Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoAccrual dtoAccrual) throws Exception {
		log.info("Delete Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoAccrual.getIds() != null && !dtoAccrual.getIds().isEmpty()) {
				DtoAccrual dtoAccrual2 = serviceAccrual.deleteAccrual(dtoAccrual.getIds());
				
				if(dtoAccrual2.getMessage()!=null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.ACCRUAL_DELETED, false), dtoAccrual2);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.ACCRUAL_NOT_DELETED, false), dtoAccrual2);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.LIST_IS_EMPTY, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete Accrual Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getaccuralId", method = RequestMethod.POST)
	public ResponseMessage getaccuralId(HttpServletRequest request, @RequestBody DtoAccrual dtoAccrual) throws Exception {
		log.info("Get getAccrual ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAccrual dtoAccrualObj = serviceAccrual.getaccrualById(dtoAccrual.getId());
			responseMessage=displayMessage(dtoAccrualObj, "ACCRUAL_LIST_GET_DETAIL", "ACCRUAL_NOT_GETTING", serviceResponse);
			
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get Accrual by Id Method:"+dtoAccrual.getId());
		return responseMessage;
	}
	
	
	
	
	
	@RequestMapping(value = "/accuralidcheck", method = RequestMethod.POST)
	public ResponseMessage divisionIdCheck(HttpServletRequest request, @RequestBody DtoAccrual dtoAccrual) throws Exception {
		log.info("accuralIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAccrual dtoAccrualObj = serviceAccrual.repeatByAccrualId(dtoAccrual.getAccuralId());
			responseMessage=displayMessage(dtoAccrualObj, "ACCRUAL_LIST_REPEAT", "ACCRUAL_REPEAT_ACCRUAL_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllAccurals", method = RequestMethod.POST)
	public ResponseMessage getAllAccurals(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) throws Exception {
		log.info("Get AllAccurals Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccrual.getAllAccrualSetupId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.ACCRUAL_GET_ALL, MessageConstant.ACCRUAL_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/searchAccrualId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAccrualId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search Accrual Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccrual.searchAccrualId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.ACCRUAL_GET_ALL, MessageConstant.ACCRUAL_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
}
