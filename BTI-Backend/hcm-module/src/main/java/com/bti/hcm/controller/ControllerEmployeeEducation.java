package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeEducation;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeEducation;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeEducation")
public class ControllerEmployeeEducation extends BaseController{
	

	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeEducation.class);
	
	@Autowired
	ServiceEmployeeEducation serviceEmployeeEducation;
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoEmployeeEducation dtoEmployeeEducation) throws Exception {
		LOGGER.info("Create HealthInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeEducation  = serviceEmployeeEducation.saveOrUpdate(dtoEmployeeEducation);
			responseMessage=displayMessage(dtoEmployeeEducation, "EMPLOYEE_EDUCATION_CREATED", "EMPLOYEE_EDUCATION_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create HealthInsurance Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeeEducation dtoEmployeeEducation) throws Exception {
		LOGGER.info("Update HealthInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeEducation = serviceEmployeeEducation.saveOrUpdate(dtoEmployeeEducation);
			responseMessage=displayMessage(dtoEmployeeEducation, "EMPLOYEE_EDUCATION_UPDATED", "EMPLOYEE_EDUCATION_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update HealthInsurance Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoEmployeeEducation dtoEmployeeEducation) throws Exception {
		LOGGER.info("Delete HealthInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeEducation.getIds() != null && !dtoEmployeeEducation.getIds().isEmpty()) {
				DtoEmployeeEducation dtoEmployeeEducation2 = serviceEmployeeEducation.delete(dtoEmployeeEducation.getIds());
				if(dtoEmployeeEducation2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_EDUCATION_DELETED", false), dtoEmployeeEducation2);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_EDUCATION_NOT_DELETED", false), dtoEmployeeEducation2);

				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_EDUCATION_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete HealthInsurance Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoEmployeeEducation dtoEmployeeEducation) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeEducation dtoEmployeeEducationObj = serviceEmployeeEducation.getById(dtoEmployeeEducation.getId());
			responseMessage=displayMessage(dtoEmployeeEducationObj, "EMPLOYEE_EDUCATION_LIST_GET_DETAIL", "EMPLOYEE_EDUCATION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoEmployeeEducation.getId());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeDeductionMaintenance(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search HealthInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeEducation.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_EDUCATION_GET_ALL", "EMPLOYEE_EDUCATION_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getByEmployeeId", method = RequestMethod.POST)
	public ResponseMessage getByEmployeeId(HttpServletRequest request, @RequestBody DtoEmployeeEducation dtoEmployeeEducation) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		DtoSearch dtoSearch = null;
		if (flag) {
			dtoSearch = serviceEmployeeEducation.getByEmployeeId(dtoEmployeeEducation.getId());
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_EDUCATION_LIST_GET_DETAIL", "EMPLOYEE_EDUCATION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoEmployeeEducation.getId());
		return responseMessage;
	}
	

	
	
}
