/**
 * 
 */
package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.BuildPayrollCheckByDeductions;
import com.bti.hcm.model.EmployeeLoanEntry;

/**
 * @author Sheikh Naser
 *
 */
@Repository("repositoryEmployeeLoanEntry")
public interface RepositoryEmployeeLoanEntry extends JpaRepository<EmployeeLoanEntry, Integer>{

}
