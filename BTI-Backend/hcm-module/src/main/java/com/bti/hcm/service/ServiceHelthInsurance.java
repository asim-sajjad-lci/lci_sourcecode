package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.CompnayInsurance;
import com.bti.hcm.model.HelthCoverageType;
import com.bti.hcm.model.HelthInsurance;
import com.bti.hcm.model.dto.DtoHelthInsurance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryCompanyInsurance;
import com.bti.hcm.repository.RepositoryHelthCoverageType;
import com.bti.hcm.repository.RepositoryHelthInsurance;

@Service("serviceHelthInsurances")
public class ServiceHelthInsurance {

	
	/**
	 * @Description LOGGER use for put a logger in HelthInsurance Service
	 */
	static Logger log = Logger.getLogger(ServiceHelthInsurance.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in HelthInsurance service
	 */


	@Autowired
	ServiceResponse response;
	
	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in HelthInsurance service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in HelthInsurance service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	
	/**
	 * @Description RepositoryHelthInsurance Autowired here using annotation of spring for access of RepositoryHelthInsurance method in HelthInsurance service
	 */
	@Autowired(required=false)
	RepositoryHelthInsurance repositoryHelthInsurance;
	
	/**
	 * @Description RepositoryHelthCoverageType Autowired here using annotation of spring for access of RepositoryHelthCoverageType method in HelthCoverageType service
	 */
	@Autowired(required=false)
	RepositoryHelthCoverageType repositoryHelthCoverageType;
	
	
	/**
	 * @Description RepositoryCompanyInsurance Autowired here using annotation of spring for access of RepositoryCompanyInsurance method in CompanyInsurance service
	 */
	@Autowired(required=false)
	RepositoryCompanyInsurance repositoryCompanyInsurance;
	
	
	/**
	 * @param dtoHelthInsurance
	 * @return
	 */
	public DtoHelthInsurance saveOrUpdate(DtoHelthInsurance dtoHelthInsurance) {
		try {

			log.info("saveOrUpdate Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			HelthInsurance helthInsurance = null;
			if (dtoHelthInsurance.getId() != null && dtoHelthInsurance.getId() > 0) {
				
				helthInsurance = repositoryHelthInsurance.findByIdAndIsDeleted(dtoHelthInsurance.getId(), false);
				helthInsurance.setUpdatedBy(loggedInUserId);
				helthInsurance.setUpdatedDate(new Date());
			} else {
				helthInsurance = new HelthInsurance();
				helthInsurance.setCreatedDate(new Date());
				
				Integer rowId = repositoryHelthInsurance.getCountOfTotalHelthInsurance();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				helthInsurance.setRowId(increment);
			}
			
			HelthCoverageType helthCoverageType = null;
			CompnayInsurance companyInsurance =null;
			
			if(dtoHelthInsurance.getHelthCoverageTypeId()!=null) {
				 helthCoverageType =repositoryHelthCoverageType.findOne(dtoHelthInsurance.getHelthInsurancePrimaryId());	
			}
			
			if(dtoHelthInsurance.getCompnayInsuranceId()!=null) {
				companyInsurance =repositoryCompanyInsurance.findOne(dtoHelthInsurance.getCompnayInsurancePrimaryId());	
			}
			
			helthInsurance.setCompnayInsurance(companyInsurance);
			helthInsurance.setHelthCoverageType(helthCoverageType);
			helthInsurance.setHelthInsuranceId(dtoHelthInsurance.getHelthInsuranceId());
			helthInsurance.setDesc(dtoHelthInsurance.getDesc());
			helthInsurance.setArbicDesc(dtoHelthInsurance.getArbicDesc());
			helthInsurance.setFrequency(dtoHelthInsurance.getFrequency());
			helthInsurance.setGroupNumber(dtoHelthInsurance.getGroupNumber());
			helthInsurance.setMaxAge(dtoHelthInsurance.getMaxAge());
			helthInsurance.setMinAge(dtoHelthInsurance.getMinAge());
			helthInsurance.setEmployeePay(dtoHelthInsurance.getEmployeePay());
			helthInsurance.setEmployerPay(dtoHelthInsurance.getEmployerPay());
			helthInsurance.setExpenses(dtoHelthInsurance.getExpenses());
			helthInsurance.setMaxCoverage(dtoHelthInsurance.getMaxCoverage());
			repositoryHelthInsurance.saveAndFlush(helthInsurance);
			log.debug("HelthInsurance is:"+helthInsurance.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoHelthInsurance;
	
	}
	
	/**
	 * @param ids
	 * @return
	 */
	public DtoHelthInsurance delete(List<Integer> ids) {
		log.info("delete HelthInsurance Method");
		DtoHelthInsurance dtoHelthInsurance = new DtoHelthInsurance();
		dtoHelthInsurance
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("HELTH_INSURANCE_DELETED", false));
		dtoHelthInsurance.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("HELTH_INSURANCE_DELETED_ASSOCIATED", false));
		List<DtoHelthInsurance> deleteInsurance = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		//"Position is already in use,you are not able to  Delete id : "
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("HELTH_INSURANCE_NOT_DELETED", false).getMessage());
		try {
			for (Integer helthInsurance : ids) {
					HelthInsurance helth = repositoryHelthInsurance.findOne(helthInsurance);
					if(helth.getListHealthInsuranceEnrollment().isEmpty()) {
						DtoHelthInsurance dtoPayCode1 = new DtoHelthInsurance();
						dtoPayCode1.setId(helth.getId());
						repositoryHelthInsurance.deleteSingleHelthInsurance(true, loggedInUserId, helthInsurance);
						deleteInsurance.add(dtoPayCode1);
					}else {
						inValidDelete = true;
					}
			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoHelthInsurance.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoHelthInsurance.setMessageType("");
			}
			dtoHelthInsurance.setDelete(deleteInsurance);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete HelthInsurance :"+dtoHelthInsurance.getId());
		return dtoHelthInsurance;
	}
	
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {

			log.info("search HelthInsurance Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("helthInsuranceId") || dtoSearch.getSortOn().equals("desc") || dtoSearch.getSortOn().equals("arbicDesc") || dtoSearch.getSortOn().equals("groupNumber")
							||dtoSearch.getSortOn().equals("employeePay") || dtoSearch.getSortOn().equals("employerPay")) {
						condition = dtoSearch.getSortOn();
					
				}else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
				dtoSearch.setTotalCount(this.repositoryHelthInsurance.predictiveHelthInsuranceSearchTotalCount("%"+searchWord+"%"));
				List<HelthInsurance> helthInsuranceList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						helthInsuranceList = this.repositoryHelthInsurance.predictiveHelthInsuranceSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						helthInsuranceList = this.repositoryHelthInsurance.predictiveHelthInsuranceSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						helthInsuranceList = this.repositoryHelthInsurance.predictiveHelthInsuranceSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				if(helthInsuranceList != null && !helthInsuranceList.isEmpty()){
					List<DtoHelthInsurance> dtoHelthInsurance = new ArrayList<>();
					DtoHelthInsurance dtoHelthInsurance1=null;
					for (HelthInsurance helthInsurance : helthInsuranceList) {
						dtoHelthInsurance1 = new DtoHelthInsurance(helthInsurance);
						dtoHelthInsurance1.setId(helthInsurance.getId());
						
						if(helthInsurance.getCompnayInsurance()!=null) {
							dtoHelthInsurance1.setCompnayInsuranceId(helthInsurance.getCompnayInsurance().getInsCompanyId());
							dtoHelthInsurance1.setCompnayInsurancePrimaryId(helthInsurance.getCompnayInsurance().getId());
						}
						if(helthInsurance.getHelthCoverageType()!=null) {
							dtoHelthInsurance1.setHelthCoverageTypeId(helthInsurance.getHelthCoverageType().getHelthCoverageId());	
							dtoHelthInsurance1.setHelthInsurancePrimaryId(helthInsurance.getHelthCoverageType().getId());
							dtoHelthInsurance1.setHelthCoverageDesc(helthInsurance.getHelthCoverageType().getDesc());
						}
						dtoHelthInsurance1.setHelthInsuranceId(helthInsurance.getHelthInsuranceId());
						dtoHelthInsurance1.setId(helthInsurance.getId());
						dtoHelthInsurance1.setDesc(helthInsurance.getDesc());
						dtoHelthInsurance1.setArbicDesc(helthInsurance.getArbicDesc());
						dtoHelthInsurance1.setFrequency(helthInsurance.getFrequency());
						dtoHelthInsurance1.setGroupNumber(helthInsurance.getGroupNumber());
						dtoHelthInsurance1.setMaxAge(helthInsurance.getMaxAge());
						dtoHelthInsurance1.setMinAge(helthInsurance.getMinAge());
						dtoHelthInsurance1.setEmployeePay(helthInsurance.getEmployeePay());
						dtoHelthInsurance1.setEmployerPay(helthInsurance.getEmployerPay());
						dtoHelthInsurance1.setExpenses(helthInsurance.getExpenses());
						dtoHelthInsurance1.setMaxCoverage(helthInsurance.getMaxCoverage());
						if(helthInsurance.getCompnayInsurance()!=null) {
							dtoHelthInsurance1.setCompnayInsurancePrimaryId(helthInsurance.getCompnayInsurance().getId());
							dtoHelthInsurance1.setCompnayInsuranceId(helthInsurance.getCompnayInsurance().getInsCompanyId());
							dtoHelthInsurance1.setCompnayInsuranceDesc(helthInsurance.getCompnayInsurance().getDesc());
							
						}
						
						dtoHelthInsurance.add(dtoHelthInsurance1);
					}
					dtoSearch.setRecords(dtoHelthInsurance);
				}
			}
			log.debug("Search helthInsurance Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	public DtoHelthInsurance getById(int id) {
		log.info("getById Method");
		DtoHelthInsurance dtoHelthInsurance = new DtoHelthInsurance();
		try {
			if (id > 0) {
				HelthInsurance helthInsurance = repositoryHelthInsurance.findByIdAndIsDeleted(id, false);
				if (helthInsurance != null) {
					dtoHelthInsurance = new DtoHelthInsurance(helthInsurance);
					dtoHelthInsurance.setId(helthInsurance.getId());
					if(helthInsurance.getCompnayInsurance()!=null) {
						dtoHelthInsurance.setCompnayInsuranceId(helthInsurance.getCompnayInsurance().getInsCompanyId());
						dtoHelthInsurance.setCompnayInsurancePrimaryId(helthInsurance.getCompnayInsurance().getId());
					}
					if(helthInsurance.getHelthCoverageType()!=null) {
						dtoHelthInsurance.setHelthCoverageTypeId(helthInsurance.getHelthCoverageType().getHelthCoverageId());	
						dtoHelthInsurance.setHelthInsurancePrimaryId(helthInsurance.getHelthCoverageType().getId());
						dtoHelthInsurance.setHelthCoveragePrimaryTypeId(helthInsurance.getHelthCoverageType().getId());
						dtoHelthInsurance.setHelthCoverageDesc(helthInsurance.getHelthCoverageType().getDesc());
					}
					dtoHelthInsurance.setHelthInsuranceId(helthInsurance.getHelthInsuranceId());
					dtoHelthInsurance.setId(helthInsurance.getId());
					dtoHelthInsurance.setDesc(helthInsurance.getDesc());
					dtoHelthInsurance.setArbicDesc(helthInsurance.getArbicDesc());
					dtoHelthInsurance.setFrequency(helthInsurance.getFrequency());
					dtoHelthInsurance.setGroupNumber(helthInsurance.getGroupNumber());
					dtoHelthInsurance.setMaxAge(helthInsurance.getMaxAge());
					dtoHelthInsurance.setMinAge(helthInsurance.getMinAge());
					dtoHelthInsurance.setEmployeePay(helthInsurance.getEmployeePay());
					dtoHelthInsurance.setEmployerPay(helthInsurance.getEmployerPay());
					dtoHelthInsurance.setExpenses(helthInsurance.getExpenses());
					dtoHelthInsurance.setMaxCoverage(helthInsurance.getMaxCoverage());
					if(helthInsurance.getCompnayInsurance()!=null) {
						dtoHelthInsurance.setCompnayInsurancePrimaryId(helthInsurance.getCompnayInsurance().getId());
						dtoHelthInsurance.setCompnayInsuranceId(helthInsurance.getCompnayInsurance().getInsCompanyId());
						dtoHelthInsurance.setCompnayInsuranceDesc(helthInsurance.getCompnayInsurance().getDesc());
					}
					
				} else {
					dtoHelthInsurance.setMessageType("PAY_CODE_NOT_GETTING");

				}
			} else {
				dtoHelthInsurance.setMessageType("INVALID_PAY_CODE_ID");

			}
			log.debug("HelthInsurance By Id is:"+dtoHelthInsurance.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoHelthInsurance;
	}
	
	/**
	 * @param helthInsurance
	 * @return
	 */
	public DtoHelthInsurance repeatByHelthInsuranceId(String helthInsurance) {
		log.info("repeatByHelthInsuranceId Method");
		DtoHelthInsurance dtoHelthInsurance = new DtoHelthInsurance();
		try {
			List<HelthInsurance> helthInsurances=repositoryHelthInsurance.findByHelthInsuranceId(helthInsurance.trim());
			if(helthInsurances!=null && !helthInsurances.isEmpty()) {
				dtoHelthInsurance.setIsRepeat(true);
			}else {
				dtoHelthInsurance.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoHelthInsurance;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchHelthInsuranceId(DtoSearch dtoSearch) {
		
		try {

			log.info("searchHelthInsuranceId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				List<HelthInsurance> helthInsuranceList =new ArrayList<>();
				
					helthInsuranceList = this.repositoryCompanyInsurance.predictiveHelthInsuranceIdSearchWithPagination("%"+searchWord+"%");
					if(!helthInsuranceList.isEmpty()) {
						if(helthInsuranceList != null && !helthInsuranceList.isEmpty()){
							List<DtoHelthInsurance> dtoHelthInsurance = new ArrayList<>();
							DtoHelthInsurance dtoHelthInsurance1=null;
							for (HelthInsurance helthInsurance : helthInsuranceList) {
								dtoHelthInsurance1 = new DtoHelthInsurance(helthInsurance);
								dtoHelthInsurance1.setId(helthInsurance.getId());
								dtoHelthInsurance1.setDesc(helthInsurance.getHelthInsuranceId()+" | "+helthInsurance.getDesc());
								dtoHelthInsurance.add(dtoHelthInsurance1);
							}
							dtoSearch.setRecords(dtoHelthInsurance);
						}
						dtoSearch.setTotalCount(helthInsuranceList.size());
					}
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	public List<DtoHelthInsurance> getAllHelthInsuranceDropDown() {
		log.info("getAllHealthInsuranceList Method");
		List<DtoHelthInsurance> dtoHelthInsuranceList = new ArrayList<>();
		try {
			List<HelthInsurance> list = repositoryHelthInsurance.findByIsDeleted(false);
			
			if (list != null && !list.isEmpty()) {
				for (HelthInsurance helthInsurance : list) {
					DtoHelthInsurance dtoHelthInsurance = new DtoHelthInsurance();
					
					dtoHelthInsurance.setHelthInsuranceId(helthInsurance.getHelthInsuranceId());
					dtoHelthInsurance.setId(helthInsurance.getId());
					dtoHelthInsurance.setDesc(helthInsurance.getDesc());
					dtoHelthInsurance.setArbicDesc(helthInsurance.getArbicDesc());
					dtoHelthInsurance.setFrequency(helthInsurance.getFrequency());
					dtoHelthInsurance.setGroupNumber(helthInsurance.getGroupNumber());
					if(helthInsurance.getHelthCoverageType()!=null) {
						dtoHelthInsurance.setHelthCoverageTypeId(helthInsurance.getHelthCoverageType().getHelthCoverageId());
						dtoHelthInsurance.setHelthCoverageDesc(helthInsurance.getHelthCoverageType().getDesc());
						dtoHelthInsurance.setHelthCoveragePrimaryTypeId(helthInsurance.getHelthCoverageType().getId());
					}
					
					dtoHelthInsurance.setMaxAge(helthInsurance.getMaxAge());
					dtoHelthInsurance.setMinAge(helthInsurance.getMinAge());
					dtoHelthInsurance.setEmployeePay(helthInsurance.getEmployeePay());
					dtoHelthInsurance.setEmployerPay(helthInsurance.getEmployerPay());
					dtoHelthInsurance.setExpenses(helthInsurance.getExpenses());
					dtoHelthInsurance.setMaxCoverage(helthInsurance.getMaxCoverage());
					dtoHelthInsuranceList.add(dtoHelthInsurance);
				}
			}
			log.debug("Position is:"+dtoHelthInsuranceList.size());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoHelthInsuranceList;
	}
	}
	
