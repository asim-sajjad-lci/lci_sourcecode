package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSkillSetDesc;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceSkillSetDesc;

@RestController
@RequestMapping("/skillSetDescDetail")
public class ControllerSkillSetDescDetail extends BaseController {

	private static final Logger LOGGER = Logger.getLogger(ControllerSkillSetDescDetail.class);

	@Autowired
	ServiceSkillSetDesc serviceSkillSetDesc;

	@Autowired
	ServiceResponse response;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @param                 request{ "skillSetSeqn": 1, "reasonIndx": true,
	 *                        "comment":"test" }
	 * @param dtoSkillSetDesc
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoSkillSetDesc dtoSkillSetDesc)
			throws Exception {
		LOGGER.info("Create Skill Desc Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSkillSetDesc = serviceSkillSetDesc.saveOrUpdate(dtoSkillSetDesc);
			responseMessage = displayMessage(dtoSkillSetDesc, "SKILL_DETAIL_CREATED", "SKILL_DETAIL_NOT_CREATED",
					response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create Skill Set Steup Method:" + responseMessage.getMessage());
		return responseMessage;

	}

	/**
	 * @param request
	 * @param         dtoSkillSetDesc{ "pageNumber" : 0, "pageSize" :2 }
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody DtoSkillSetDesc dtoSkillSetDesc)
			throws Exception {
		LOGGER.info("Get All Skill Desc Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceSkillSetDesc.getAll(dtoSkillSetDesc);
			responseMessage = displayMessage(dtoSearch, "SKILL_DETAIL_GET_ALL", "SKILL_DETAIL_LIST_NOT_GETTING",
					response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get All Skill Desc Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param               request{ "id" : 2, "skillSetSeqn": 12, "reasonIndx":
	 *                      true, "comment":"test" }
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoSkillSetDesc dtoSkillSteup)
			throws Exception {
		LOGGER.info("Update Skill Desc Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSkillSteup = serviceSkillSetDesc.saveOrUpdate(dtoSkillSteup);
			responseMessage = displayMessage(dtoSkillSteup, "SKILL_DETAIL_UPDATED_SUCCESS", "SKILL_DETAIL_NOT_UPDATED",
					response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update Skill Desc Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param               request{ "ids" : [1] }
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteSkillSteup(HttpServletRequest request, @RequestBody DtoSkillSetDesc dtoSkillSteup)
			throws Exception {
		LOGGER.info("Delete Skill Desc Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSkillSteup.getIds() != null && !dtoSkillSteup.getIds().isEmpty()) {
				DtoSkillSetDesc dtoSkillSetSteup = serviceSkillSetDesc.delete(dtoSkillSteup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("SKILL_DETAIL_DELETED", false), dtoSkillSetSteup);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Skill Desc Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param dtoSearch
	 * @param           request{ "searchKeyword":"t", "pageNumber" : 0, "pageSize"
	 *                  :2 }
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Skill Desc Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSetDesc.search(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "SKILL_DETAIL_GET_ALL", "SKILL_DETAIL_LIST_NOT_GETTING",
					response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			LOGGER.debug("Search Skill Desc Method:" + dtoSearch.getTotalCount());
		}

		return responseMessage;
	}

	/**
	 * @param               request{ "id" : 1 }
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoSkillSetDesc dtoSkillSteup)
			throws Exception {
		LOGGER.info("Get Skill Desc ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSkillSetDesc dtoSkillSteupObj = serviceSkillSetDesc.getById(dtoSkillSteup.getId());
			responseMessage = displayMessage(dtoSkillSteupObj, "SKILL_DETAIL_GET_DETAIL", "SKILL_DETAIL_NOT_GETTING",
					response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get Skill Desc ById Method:" + dtoSkillSteup.getId());
		return responseMessage;
	}

}
