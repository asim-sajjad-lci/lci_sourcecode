/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PayScheduleSetupDepartment;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO PayScheduleSetupDepartment class having getter and setter for fields (POJO)
 * Name of Project: Hrm Created on: December 08, 2017
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoPayScheduleSetupDepartment extends DtoBase{
	
	private Integer payScheduleDepartmentIndexId;
	private Integer payScheduleSetupId;
	private Integer departmentId;
	private String departmentDescription;
	private String arabicDepartmentDescription;
	private Boolean payScheduleStatus;
	private List<DtoPayScheduleSetupDepartment> deletePayScheduleSetupDepartment;
	
	
	public Integer getPayScheduleDepartmentIndexId() {
		return payScheduleDepartmentIndexId;
	}
	public void setPayScheduleDepartmentIndexId(Integer payScheduleDepartmentIndexId) {
		this.payScheduleDepartmentIndexId = payScheduleDepartmentIndexId;
	}
	public Integer getPayScheduleSetupId() {
		return payScheduleSetupId;
	}
	public void setPayScheduleSetupId(Integer payScheduleSetupId) {
		this.payScheduleSetupId = payScheduleSetupId;
	}
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentDescription() {
		return departmentDescription;
	}
	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}
	public String getArabicDepartmentDescription() {
		return arabicDepartmentDescription;
	}
	public void setArabicDepartmentDescription(String arabicDepartmentDescription) {
		this.arabicDepartmentDescription = arabicDepartmentDescription;
	}
	public Boolean getPayScheduleStatus() {
		return payScheduleStatus;
	}
	public void setPayScheduleStatus(Boolean payScheduleStatus) {
		this.payScheduleStatus = payScheduleStatus;
	}
	
	public DtoPayScheduleSetupDepartment() {
	}
	
	public DtoPayScheduleSetupDepartment(PayScheduleSetupDepartment payScheduleSetupDepartment) {}
	
	public List<DtoPayScheduleSetupDepartment> getDeletePayScheduleSetupDepartment() {
		return deletePayScheduleSetupDepartment;
	}
	public void setDeletePayScheduleSetupDepartment(List<DtoPayScheduleSetupDepartment> deletePayScheduleSetupDepartment) {
		this.deletePayScheduleSetupDepartment = deletePayScheduleSetupDepartment;
	}

}
