package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.HelthCoverageType;

@Repository("repositoryHelthCoverageType")
public interface RepositoryHelthCoverageType extends JpaRepository<HelthCoverageType, Integer>{

	HelthCoverageType findByIdAndIsDeleted(Integer id, boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update HelthCoverageType d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleHelyhCovergareType(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from HelthCoverageType d where ( d.helthCoverageId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveHelthCoverageTypeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from HelthCoverageType d where ( d.helthCoverageId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord) and d.isDeleted=false")
	List<HelthCoverageType> predictiveHelthCoverageTypeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageRequest);

	@Query("select count(*) from HelthCoverageType s where s.isDeleted=false")
	Integer getCountOfTotalAccrual();

	List<HelthCoverageType> findByIsDeleted(boolean b, Pageable pageable);
	
	List<HelthCoverageType> findByIsDeleted(boolean b);

	List<HelthCoverageType> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select p from HelthCoverageType p where (p.helthCoverageId =:helthCoverageId) and p.isDeleted=false")
	List<HelthCoverageType> findByHelthCoverageTypeId(@Param("helthCoverageId")String helthCoverageId);

	@Query("select d.helthCoverageId from HelthCoverageType d where (d.helthCoverageId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<String> predictiveHelthCoverageIdSearchWithPagination(@Param("searchKeyWord")String string);

	@Query("select count(*) from HelthCoverageType h ")
	public Integer getCountOfTotalHelthCoverageType();

}
