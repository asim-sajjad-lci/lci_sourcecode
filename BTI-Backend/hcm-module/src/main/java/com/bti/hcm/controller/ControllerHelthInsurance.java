package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoHelthInsurance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceHelthInsurance;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/HelthInsuranceSetUp")
public class ControllerHelthInsurance extends BaseController{
	
	@Autowired
	ServiceHelthInsurance serviceHelthInsurance;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerHelthInsurance.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 *
 	@param request{
		  "helthInsuranceId" : "1",
		  "helthCoverageTypeId" : 1,
		  "desc" : "1asa",
		  "arbicDesc" : "1sdada",
		  "frequency" : 1,
		  "compnayInsuranceId" : 1,
		  "groupNumber" : "1",
		  "maxAge" : 1,
		  "minAge" : 1,"employeePay" : 1,
		  "employerPay" : 1,
		  "expenses" : 1
		}
		 @param response
	 {
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"helthInsuranceId": "1",
		"helthCoverageTypeId": 1,
		"helthCoverageType": null,
		"desc": "1asa",
		"arbicDesc": "1sdada",
		"frequency": 1,
		"compnayInsuranceId": 1,
		"compnayInsurance": null,
		"groupNumber": "1",
		"maxAge": 1,
		"minAge": 1,
		"employeePay": 1,
		"employerPay": 1,
		"expenses": 1,
		"maxCoverage": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		"btiMessage": {
		}
		}
	 * @param dtoHelthInsurance
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoHelthInsurance dtoHelthInsurance) throws Exception{
		LOGGER.info("Create HelthInsurance Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHelthInsurance = serviceHelthInsurance.saveOrUpdate(dtoHelthInsurance);
			responseMessage=displayMessage(dtoHelthInsurance, "HELTH_INSURANCE_CREATED", "HELTH_INSURANCE_NOT_CREATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create HelthInsurance Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	/**
	 * @param request{
  		"id" : 1,
  			"helthInsuranceId" : "1",
		  "helthCoverageTypeId" : 1,
		  "desc" : "1asa",
		  "arbicDesc" : "1sdsasassada",
		  "frequency" : 1,
		  "compnayInsuranceId" : 1,
		  "groupNumber" : "1",
		  "maxAge" : 1,
		  "minAge" : 1,"employeePay" : 1,
		  "employerPay" : 1,
		  "expenses" : 1
		}
		@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": 1,
		"helthInsuranceId": "1",
		"helthCoverageTypeId": 1,
		"helthCoverageType": null,
		"desc": "1asa",
		"arbicDesc": "1sdsasassada",
		"frequency": 1,
		"compnayInsuranceId": 1,
		"compnayInsurance": null,
		"groupNumber": "1",
		"maxAge": 1,
		"minAge": 1,
		"employeePay": 1,
		"employerPay": 1,
		"expenses": 1,
		"maxCoverage": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Helth Insurance updated successfully.",
		"messageShort": "HELTH_INSURANCE_UPDATED_SUCCESS"
		}
		}
	 * @param dtoHelthInsurance
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoHelthInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("Update HelthInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHelthInsurance = serviceHelthInsurance.saveOrUpdate(dtoHelthInsurance);
			responseMessage=displayMessage(dtoHelthInsurance, MessageConstant.HELTH_INSURANCE_UPDATED_SUCCESS, MessageConstant.HELTH_INSURANCE_LIST_NOT_GETTING, response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		LOGGER.debug("Update HelthInsurance Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoHelthInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("Delete HelthInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoHelthInsurance.getIds() != null && !dtoHelthInsurance.getIds().isEmpty()) {
				DtoHelthInsurance dtoHelthInsurance2 = serviceHelthInsurance.delete(dtoHelthInsurance.getIds());
				
				if(dtoHelthInsurance2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("HELTH_INSURANCE_DELETED", false), dtoHelthInsurance2);	
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("HELTH_INSURANCE_NOT_DELETED", false), dtoHelthInsurance2);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete HelthInsurance Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	
	/**
	 * @param request{
	  "id" : 1
	}
	@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"helthInsuranceId": "1",
		"helthCoverageTypeId": null,
		"helthCoverageType": null,
		"desc": "1asa",
		"arbicDesc": "1sdsasassada",
		"frequency": 0,
		"compnayInsuranceId": null,
		"compnayInsurance": null,
		"groupNumber": "1",
		"maxAge": null,
		"minAge": null,
		"employeePay": null,
		"employerPay": null,
		"expenses": null,
		"maxCoverage": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Helth Insurance fetched successfully.",
		"messageShort": "HELTH_INSURANCE_GET_DETAIL"
		}
		}
	 * @param dtoHelthInsurance
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoHelthInsurance dtoHelthInsurance) throws Exception {
		LOGGER.info("Get HelthInsurance ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoHelthInsurance dtoPayCodeObj = serviceHelthInsurance.getById(dtoHelthInsurance.getId());
			responseMessage=displayMessage(dtoPayCodeObj, "HELTH_INSURANCE_GET_DETAIL", "HELTH_INSURANCE_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get HelthInsurance by Id Method:"+dtoHelthInsurance.getId());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  "payCodeId" : "1"
	}
	@param request{
		"code": 302,
		"status": "FOUND",
		"result": {
		"id": null,
		"helthInsuranceId": null,
		"helthCoverageTypeId": null,
		"helthCoverageType": null,
		"desc": null,
		"arbicDesc": null,
		"frequency": 0,
		"compnayInsuranceId": null,
		"compnayInsurance": null,
		"groupNumber": null,
		"maxAge": null,
		"minAge": null,
		"employeePay": null,
		"employerPay": null,
		"expenses": null,
		"maxCoverage": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": true
		},
		"btiMessage": {
		"message": "Helth Insurance Id already Exists.",
		"messageShort": "HELTH_INSURANCE_RESULT"
		}
		}
	 * @param dtoHelthInsurance
	 * @return
	 * @throws Exception
	 *
	 */
	
	
	@RequestMapping(value = "/helthInsurancecheck", method = RequestMethod.POST)
	public ResponseMessage helthInsurancecheck(HttpServletRequest request, @RequestBody DtoHelthInsurance dtoHelthInsurances)
			throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoHelthInsurance dtoHelthInsurancesObj = serviceHelthInsurance.repeatByHelthInsuranceId(dtoHelthInsurances.getHelthInsuranceId());
			
			 if (dtoHelthInsurancesObj !=null && dtoHelthInsurancesObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                		 response.getMessageByShortAndIsDeleted("HELTH_INSURANCE_RESULT", false), dtoHelthInsurancesObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                		 response.getMessageByShortAndIsDeleted("HELTH_INSURANCE_REPEAT_ID_NOT_FOUND", false),
                         dtoHelthInsurancesObj);
             }
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}

	
	
	/**
	 * @param dtoSearch
	 * @param request{
		  "sortOn" : "helthInsuranceId",
		  "sortBy" : "DESC",
		 "searchKeyword" : "",
		"pageNumber":"0",
		"pageSize":"10"
		}
		@param response{
		"code": 200,
		"status": "OK",
		"result": {
		"searchKeyword": "",
		"pageNumber": 0,
		"pageSize": 10,
		"sortOn": "helthInsuranceId",
		"sortBy": "DESC",
		"totalCount": 5,
		"records": [
		  {
		"id": 1,
		"helthInsuranceId": "1",
		"helthCoverageTypeId": null,
		"helthCoverageType": {
		"createdDate": 1518428990000,
		"updatedDate": 1518429058000,
		"updatedBy": 2,
		"isDeleted": true,
		"updatedRow": 1518409257000,
		"rowId": 2,
		"id": 1,
		"helthCoverageId": "1",
		"desc": "test",
		"arbicDesc": "tssaassest"
		},
		"btiMessage": {
		"message": "Helth Insurance fetched successfully.",
		"messageShort": "HELTH_INSURANCE_GET_ALL"
		}
		}
			 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search HelthInsurance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceHelthInsurance.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "HELTH_INSURANCE_GET_ALL", "HELTH_INSURANCE_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search HelthInsurance Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	 /**
	 * @param dtoSearch
	 * @param request{
	  "searchKeyword" : ""
	}@param response{
	"code": 200,
	"status": "OK",
	"result": {
	"searchKeyword": "",
	"ids": [
	  "1",
	  "1",
	  "1",
	  "1",
	  "1"
	],
	"totalCount": 5,
	"records": 5
	},
	"btiMessage": {
	"message": "Helth Insurance fetched successfully.",
	"messageShort": "HELTH_INSURANCE_GET_ALL"
	}
	}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchHelthInsuranceId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
		public ResponseMessage searchHelthInsuranceId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
			LOGGER.info("Search HelthInsurance Method");
			ResponseMessage responseMessage = null;
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtoSearch = this.serviceHelthInsurance.searchHelthInsuranceId(dtoSearch);
				responseMessage=displayMessage(dtoSearch, "HELTH_INSURANCE_GET_ALL", "HELTH_INSURANCE_LIST_NOT_GETTING", response);
			} else {
				responseMessage = unauthorizedMsg(response);
			}
			if(dtoSearch!=null) {
				LOGGER.debug("Search HelthInsurance Method:"+dtoSearch.getTotalCount());
			}
			return responseMessage;
		}
	
	
	
	
	
	
	@RequestMapping(value = "/getAllHelthInsuranceDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllHelthInsuranceDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoHelthInsurance>helthInsuranceList = serviceHelthInsurance.getAllHelthInsuranceDropDown();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("HEALTHINSURANCE_GET_ALL", false), helthInsuranceList);
			} else {
				responseMessage = unauthorizedMsg(response);
			}

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return responseMessage;
	}
	
	
	
	
}
