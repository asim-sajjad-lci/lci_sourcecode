package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoRetirementEntranceDate;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceRetirementEntranceDate;

@RestController
@RequestMapping("/retirementEntranceDate")
public class ControllerRetirementEntranceDate extends BaseController{

private static final Logger LOGGER = Logger.getLogger(ControllerRetirementEntranceDate.class);
	
	
	@Autowired(required=true)
	ServiceRetirementEntranceDate serviceRetirementEntranceDate;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @description : Create RetirementEntranceDate
	 * @param request
	 * @param dtoRetirementEntranceDate
	 * @return
	 * @throws Exception
	 * @request
	*{
		  
	 	"dateSequence":19994,
	 	"planId":786,
	 	"entranceDescription":"welcome123Hello",
			"pageNumber":"0",
			"pageSize":"10"

	}*/
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoRetirementEntranceDate dtoRetirementEntranceDate) throws Exception {
		LOGGER.info("Create RetirementEntranceDate Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoRetirementEntranceDate = serviceRetirementEntranceDate.saveOrUpdateRetirementPlanSetup(dtoRetirementEntranceDate);
			responseMessage=displayMessage(dtoRetirementEntranceDate, "RETIREMENTENTRANCEDATE_CREATED", "RETIREMENTENTRANCEDATE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create RetirementEntranceDate Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description : Create RetirementEntranceDate
	 * @param request
	 * @param dtoRetirementEntranceDate
	 * @return
	 * @throws Exception
	 * @request
	*{
		  
	 	"dateSequence":19994,
	 	"planId":786,
	 	"entranceDescription":"welcome123Hello",
			"pageNumber":"0",
			"pageSize":"10"

	}
	*/
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoRetirementEntranceDate dtoRetirementEntranceDate) throws Exception {
		LOGGER.info("Update RetirementEntranceDate Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoRetirementEntranceDate = serviceRetirementEntranceDate.saveOrUpdateRetirementPlanSetup(dtoRetirementEntranceDate);
			responseMessage=displayMessage(dtoRetirementEntranceDate, "RETIREMENTENTRANCEDATE_CREATED_UPDATED_SUCCESS", "RETIREMENTPLANSETUP_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update RetirementEntranceDate Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description : Create RetirementEntranceDate
	 * @param request
	 * @param dtoRetirementEntranceDate
	 * @return
	 * @throws Exception
	 * @request
	*{
	 "id" : [2]

		}*/
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoRetirementEntranceDate dtoRetirementEntranceDate) throws Exception {
		LOGGER.info("Delete RetirementEntranceDate Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoRetirementEntranceDate.getIds() != null && !dtoRetirementEntranceDate.getIds().isEmpty()) {
				DtoRetirementEntranceDate dtoRetirementEntranceDate2 = serviceRetirementEntranceDate.deleteRetirementEntranceDate(dtoRetirementEntranceDate.getIds());
				
				
				if(dtoRetirementEntranceDate2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("RETIREMENTENTRANCEDATE_DELETED", false), dtoRetirementEntranceDate2);
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoRetirementEntranceDate2.getMessageType());
					btiMessageHcm.setMessageShort("RETIREMENT_ENTRANCE_NOT_DELETE_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoRetirementEntranceDate2);
				}
				
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete RetirementEntranceDate Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search RetirementEntranceDate Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceRetirementEntranceDate.searchRetirementEntranceDate(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "RETIREMENTENTRANCEDATE_GET_ALL", "RETIREMENTENTRANCEDATE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search RetirementEntranceDate Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getPlanId", method = RequestMethod.POST)
	public ResponseMessage getretirementPlanId(HttpServletRequest request, @RequestBody DtoRetirementEntranceDate dtoRetirementEntranceDate) throws Exception {
		LOGGER.info("Get getRetirementEntranceDate ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoRetirementEntranceDate dtoRetirementEntranceDateObj = serviceRetirementEntranceDate.getRetirementEntranceDateById(dtoRetirementEntranceDate.getId());
			responseMessage=displayMessage(dtoRetirementEntranceDateObj, "RETIREMENENTRANCETDATE_LIST_GET_DETAIL", "RETIREMENTENTRANCEDATE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get RetirementEntranceDate by Id Method:"+dtoRetirementEntranceDate.getId());
		return responseMessage;
	}
	
	
	
	
}
