package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPayrollTransectionHistoryByEmployee;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayrollTransactionHistoryByDepartment;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/payrollTransactionHistoryByDepartment")
public class ControllerPayrollTransactionHistoryByDepartment {

	

	@Autowired
	ServiceResponse response;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerPayrollTransectionHistoryByEmployee.class);
	
	@Autowired
	ServicePayrollTransactionHistoryByDepartment servicePayrollTransactionHistoryByDepartment;
	
	

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoPayrollTransectionHistoryByEmployee  dtoPayrollTransectionHistoryByEmployee) throws Exception {
		LOGGER.info("Create PayrollTransactionHistoryByDepartment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayrollTransectionHistoryByEmployee  = servicePayrollTransactionHistoryByDepartment.saveOrUpdate(dtoPayrollTransectionHistoryByEmployee);
			if (dtoPayrollTransectionHistoryByEmployee != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("PAYROLL_TRANSACTION_HISTORY_BY_DEPARTMENT_CREATED", false), dtoPayrollTransectionHistoryByEmployee);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("PAYROLL_TRANSACTION_HISTORY_BY_DEPARTMENT_NOT_CREATED", false), dtoPayrollTransectionHistoryByEmployee);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Create PayrollTransactionHistoryByDepartment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
}
