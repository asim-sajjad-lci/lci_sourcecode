package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.model.TransactionEntryDetail;

@Repository("repositoryTransactionEntryDetail")
public interface RepositoryTransactionEntryDetail extends JpaRepository<TransactionEntryDetail, Integer>{

	TransactionEntryDetail findByIdAndIsDeleted(Integer id, boolean b);
	
	List<TransactionEntryDetail> findByIsDeleted(boolean b,Pageable pageable);
	
	List<TransactionEntryDetail> findByIsDeleted(boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TransactionEntryDetail t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	public void deleteSingleTransactionEntryDetail(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TransactionEntryDetail t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.transactionEntry.id =:id ")
	public void deleteByTransactionEntry(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	TraningCourse findByEntryNumberAndIsDeleted(Integer entryNumber,boolean b);
	
	
	@Query("select count(*) from TransactionEntryDetail t where  t.isDeleted=false")
	Integer predictivegetAllTransactionEntryDetailSearchCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select count(*) from TransactionEntryDetail t where (t.employeeMaster.employeeFirstName like :searchKeyWord or t.employeeMaster.employeeId like :searchKeyWord) and t.transactionEntry.batches.id =:id and t.isDeleted=false and t.fromTranscationEntry=true")
	Integer searchByTransctionEntryCount(@Param("id")Integer id, @Param("searchKeyWord")String searchKeyWord);
	
	@Query("select t from TransactionEntryDetail t where (t.employeeMaster.employeeFirstName like :searchKeyWord or t.employeeMaster.employeeId like :searchKeyWord) and t.transactionEntry.id IN (:ids) and t.isDeleted=false and t.fromTranscationEntry=true")
	List<TransactionEntryDetail> searchByTransctionEntry(@Param("ids")List<Integer> ids, @Param("searchKeyWord")String searchKeyWord, Pageable pageable);
	
	@Query("select t from TransactionEntryDetail t where t.dimensions.id =:id and  t.isDeleted=false")
	TransactionEntryDetail getByDimensions(@Param("id")Integer id);
	
	@Query("select count(*) from TransactionEntryDetail t where (t.employeeMaster.employeeFirstName like :searchKeyWord or t.employeeMaster.employeeId like :searchKeyWord)  and t.isDeleted=false")
	Integer searchByCalculateCheckCount(@Param("searchKeyWord")String searchKeyWord);
	
	@Query("select t from TransactionEntryDetail t where (t.employeeMaster.employeeFirstName like :searchKeyWord or t.employeeMaster.employeeId like :searchKeyWord)  and t.isDeleted=false")
	List<TransactionEntryDetail> searchByCalculateCheck(@Param("searchKeyWord")String searchKeyWord);
	
	@Query("select t from TransactionEntryDetail t where (t.employeeMaster.employeeIndexId =:id)  and t.isDeleted=false and t.fromBuild=false")
	List<TransactionEntryDetail> searchByEmployeeId(@Param("id")Integer id,Pageable pageable);
	
	@Query("select count(*) from TransactionEntryDetail t where (t.employeeMaster.employeeIndexId =:id) and t.buildChecks.id =:buildId  and t.isDeleted=false and t.fromBuild=false")
	Integer getCountOfTotalTranscationEntryDetail(@Param("id")Integer id,@Param("buildId")Integer buildId);
	
	
	@Query("select t from TransactionEntryDetail t where (t.employeeMaster.employeeIndexId =:id) and t.buildChecks.id =:buildId  and t.isDeleted=false and t.fromBuild=false")
	List<TransactionEntryDetail> searchByEmployeeId1(@Param("id")Integer id,@Param("buildId")Integer buildId);
	
	@Query("select DISTINCT count(*) from TransactionEntryDetail t where  t.buildChecks.id =:buildId  and t.isDeleted=false and t.fromBuild=false")
	Integer predictivegetAllTransactionEntryDetailEmployeeCount(@Param("buildId")Integer buildId);
	
	@Query("select t from TransactionEntryDetail t where (t.employeeMaster.employeeIndexId =:id)  and t.isDeleted=false and t.fromBuild=false")
	List<TransactionEntryDetail> searchByEmployeeId12(@Param("id")Integer id,Pageable pageable);
	
	
	@Query("select t from TransactionEntryDetail t where (t.employeeMaster.employeeIndexId =:id)  and t.isDeleted=false")
	List<TransactionEntryDetail> searchByEmployeeId(@Param("id")Integer id);
	
	@Query("select t from TransactionEntryDetail t where (t.transactionEntry.batches.id =:id)  and t.isDeleted=false and t.fromTranscationEntry=true")
	List<TransactionEntryDetail> searchByBatchId(@Param("id")Integer id);
	
	@Query("select t from TransactionEntryDetail t where (t.transactionEntry.batches.id =:batchId and t.employeeMaster.employeeIndexId =:employeeId and t.transactionType =:transactionType)  and t.isDeleted=false")
	List<TransactionEntryDetail> repeatByBatchesIdForTransaction(@Param("batchId")Integer batchId,@Param("employeeId")Integer employeeId,@Param("transactionType")short transactionType);
	
	//@Cacheable("transactionEntryDetail")
	TransactionEntryDetail findTopByOrderByIdDesc();
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TransactionEntryDetail t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.transactionEntry.id =:id ")
	void deleteSingleTranscationEntryDetailRow(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("DELETE FROM TransactionEntryDetail t where t.transactionEntry.id =:id ")
	void deleteSingleTranscationEntryDetailRow(@Param("id") Integer id);
	
	@Query("select t from TransactionEntryDetail t where t.buildChecks.id =:id")
	List<TransactionEntryDetail> findByBuildChecks(@Param("id")Integer id);
	
	@Query("select t from TransactionEntryDetail t where t.buildChecks.id =:id and (DATE(t.deductionCode.startDate) >= :startDate and DATE(t.deductionCode.endDate) >= :endDate)")
	List<TransactionEntryDetail> findByBuildChecks2(@Param("id")Integer id,@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	
	@Query("select t from TransactionEntryDetail t where t.buildChecks.id =:id")
	List<TransactionEntryDetail> findByBuildChecks1(@Param("id")Integer id);
	
	

	
	@Query("select t from TransactionEntryDetail t where t.transactionEntry.id =:id  and t.isDeleted=true")
	List<TransactionEntryDetail> findByTransactionEntry(@Param("id")Integer id);
	
	
	@Query("select t from TransactionEntryDetail t where t.transactionEntry.id =:id  and t.isDeleted=false")
	List<TransactionEntryDetail> findByTransactionEntry1(@Param("id")Integer id);
	
	

	
	/*@Modifying(clearAutomatically = true)
	@Transactional
	 @Query("update TransactionEntryDetail t set t.isDeleted =:deleted ,t.fromBuild = :t.deleted1, updatedBy =:updateById where t.id =:id ")
	void deleteSingleTranscationEntryDetailRow1(@Param("deleted") Boolean deleted,@Param("deleted") Boolean deleted1, @Param("updateById") Integer updateById,
			@Param("id") Integer id);*/
	
	
	/*@Query("select d from EmployeeMaster d where d.employeeIndexId in(:list) and d.isDeleted=false")
	public List<EmployeeMaster> findAllEmployeeListByEmployeeIndexId(@Param("list") List<Integer> list);
	*/
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TransactionEntryDetail t set t.isDeleted =:deleted ,t.fromBuild =:fromBuild ,t.updatedBy =:updateById where t.id =:id ")
	void deleteSingleTranscationEntryDetail(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,@Param("fromBuild") Boolean fromBuild,
			@Param("id") Integer id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("DELETE FROM TransactionEntryDetail t  where t.id =:id ")
	void deleteSingleTranscationEntryDetail(@Param("id") Integer id);
	
	@Query("select t from TransactionEntryDetail t where t.benefitCode.id =:id  and t.isDeleted=true and t.fromBuild=false")
	List<TransactionEntryDetail> findByBenefitCode(@Param("id")Integer id);
	/*@Query("select t from TransactionEntryDetail t where t.batches.id =:id and t.buildChecks.id =:idBuildCheck and t.isDeleted=false and t.fromBuild=false")
	List<TransactionEntryDetail> findByBatchesAndBuildChecks(@Param("id")Integer id,@Param("idBuildCheck")Integer idBuildCheck);*/
	
	@Query("select t from TransactionEntryDetail t where t.id=:id and t.isDeleted=false")
	TransactionEntryDetail findById(@Param("id")Integer id);
	
	
	
	@Query("select t from TransactionEntryDetail t where t.employeeMaster.employeeIndexId =:id  and t.deductionCode.id =:deductionCode and t.isDeleted=false")
	List<TransactionEntryDetail> findByTransactionEntry2(@Param("id")Integer id,@Param("deductionCode")Integer deductionCode);
	
	
	@Query("select t from TransactionEntryDetail t where t.employeeMaster.employeeIndexId =:id  and t.benefitCode.id =:benefitCode and t.isDeleted=false")
	List<TransactionEntryDetail> findByTransactionEntry3(@Param("id")Integer id,@Param("benefitCode")Integer benefitCode);
	
	
	@Query("select t from TransactionEntryDetail t where t.employeeMaster.employeeIndexId =:id  and t.code.id =:code and t.isDeleted=false")
	List<TransactionEntryDetail> findByTransactionEntry4(@Param("id")Integer id,@Param("code")Integer code);

}
