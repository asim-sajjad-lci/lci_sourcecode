package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayCode;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/payCode")
public class ControllerPayCode extends BaseController {

	@Autowired
	ServicePayCode servicePayCode;

	@Autowired
	ServiceResponse response;

	private static final Logger log = Logger.getLogger(ControllerPayCode.class);

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 *
	 * @param            request{ "payCodeId" : "1", "description":"test",
	 *                   "arbicDescription":"test", "payType":1,
	 *                   "baseOnPayCode":"12", "payFactor":1, "payRate":1,
	 *                   "unitofPay":1, "inActive":true }
	 * @param response   { "code": 201, "status": "CREATED", "result": { "id": null,
	 *                   "payCodeId": "1", "description": "test",
	 *                   "arbicDescription": "test", "payType": 1, "baseOnPayCode":
	 *                   "12", "baseOnPayCodeAmount": null, "payFactor": 1,
	 *                   "payRate": 1, "unitofPay": 1, "payperiod": null,
	 *                   "inActive": true, "pageNumber": null, "pageSize": null,
	 *                   "ids": null, "isActive": null, "messageType": null,
	 *                   "message": null, "deleteMessage": null, "associateMessage":
	 *                   null, "delete": null, "isRepeat": null }, "btiMessage": {
	 *                   "message": "Pay Code created successfully.",
	 *                   "messageShort": "PAY_CODE_CREATED" } }
	 * @param dtoPayCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createPosition(HttpServletRequest request, @RequestBody DtoPayCode dtoPayCode)
			throws Exception {
		log.info("Create Postion Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayCode = servicePayCode.saveOrUpdate(dtoPayCode);
			responseMessage = displayMessage(dtoPayCode, "PAY_CODE_CREATED", "PAY_CODE_NOT_CREATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Create payCode Method:" + responseMessage.getMessage());
		return responseMessage;

	}

	/**
	 *
	 * @param            request{ "id":1, "payCodeId" : "1", "description":"test",
	 *                   "arbicDescription":"test", "payType":1,
	 *                   "baseOnPayCode":"12", "payFactor":1, "payRate":1,
	 *                   "unitofPay":1, "inActive":true }
	 * @param response   { "code": 201, "status": "CREATED", "result": { "id": null,
	 *                   "payCodeId": "1", "description": "test",
	 *                   "arbicDescription": "test", "payType": 1, "baseOnPayCode":
	 *                   "12", "baseOnPayCodeAmount": null, "payFactor": 1,
	 *                   "payRate": 1, "unitofPay": 1, "payperiod": null,
	 *                   "inActive": true, "pageNumber": null, "pageSize": null,
	 *                   "ids": null, "isActive": null, "messageType": null,
	 *                   "message": null, "deleteMessage": null, "associateMessage":
	 *                   null, "delete": null, "isRepeat": null }, "btiMessage": {
	 *                   "message": "Pay Code update successfully.", "messageShort":
	 *                   "PAY_UPDATE_SUCESSFULLY" } }
	 * @param dtoPayCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoPayCode dtoPayCode) throws Exception {
		log.info("Update Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayCode = servicePayCode.saveOrUpdate(dtoPayCode);
			responseMessage = displayMessage(dtoPayCode, "PAY_CODE_UPDATED_SUCCESS", "PAY_CODE_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Update Position Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoPayCode dtoPayCode) throws Exception {
		log.info("Delete PayCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayCode.getIds() != null && !dtoPayCode.getIds().isEmpty()) {
				DtoPayCode dtoPosition1 = servicePayCode.delete(dtoPayCode.getIds());
				if (dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("PAY_CODE_DELETED", false), dtoPosition1);
				} else {

					DtoBtiMessageHcm btiMessageHcm = new DtoBtiMessageHcm(null, "");
					btiMessageHcm.setMessage(dtoPosition1.getMessageType());
					btiMessageHcm.setMessageShort("PAY_CODE_NOT_DELETE_ID_MESSAGE");

					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, btiMessageHcm,
							dtoPosition1);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete Pay Code Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param             request{ "id" : 1 }
	 * @param             response{ "code": 201, "status": "CREATED", "result": {
	 *                    "id": null, "payCodeId": "1", "description": "test",
	 *                    "arbicDescription": "test", "payType": null,
	 *                    "baseOnPayCode": null, "baseOnPayCodeAmount": null,
	 *                    "payFactor": null, "payRate": null, "unitofPay": null,
	 *                    "payperiod": null, "inActive": false, "pageNumber": null,
	 *                    "pageSize": null, "ids": null, "isActive": null,
	 *                    "messageType": null, "message": null, "deleteMessage":
	 *                    null, "associateMessage": null, "delete": null,
	 *                    "isRepeat": null }, "btiMessage": { "message": "Pay code
	 *                    fetched successfully.", "messageShort": "PAYCODE_DETAIL" }
	 *                    }
	 * @param dtoPosition
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoPayCode dtoPosition)
			throws Exception {
		log.info("Get payCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayCode dtoPayCodeObj = servicePayCode.getById(dtoPosition.getId());
			responseMessage = displayMessage(dtoPayCodeObj, "PAY_CODE_GET_DETAIL", "PAY_CODE_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Get pay Code by Id Method:" + dtoPosition.getId());
		return responseMessage;
	}

	/**
	 * @param            request{ "payCodeId" : "1" }
	 * @param            request{ "code": 302, "status": "FOUND", "result": { "id":
	 *                   null, "payCodeId": null, "description": null,
	 *                   "arbicDescription": null, "payType": null, "baseOnPayCode":
	 *                   null, "baseOnPayCodeAmount": null, "payFactor": null,
	 *                   "payRate": null, "unitofPay": null, "payperiod": null,
	 *                   "inActive": false, "pageNumber": null, "pageSize": null,
	 *                   "ids": null, "isActive": null, "messageType": null,
	 *                   "message": null, "deleteMessage": null, "associateMessage":
	 *                   null, "delete": null, "isRepeat": true }, "btiMessage": {
	 *                   "message": "Pay Code Id is already created.",
	 *                   "messageShort": "PAYCODE_DETAIL_FATECHED" } }
	 * @param dtoPayCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/payCodeIdcheck", method = RequestMethod.POST)
	public ResponseMessage payCodeIdcheck(HttpServletRequest request, @RequestBody DtoPayCode dtoPayCode)
			throws Exception {
		log.info("payCodeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayCode dtoPayCodeObj = servicePayCode.repeatBypayCodeId(dtoPayCode.getPayCodeId());
			responseMessage = displayMessage(dtoPayCodeObj, "PAY_CODE_DETAIL_FATECHED",
					"PAY_CODE_REPEAT_PAY_CODE_ID_NOT_FOUND", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}

	/**
	 * @param dtoSearch
	 * @param           request{ "sortOn" : "payCodeId", "sortBy" : "DESC",
	 *                  "searchKeyword" : "", "pageNumber":"0", "pageSize":"10" }
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search search Pay Code Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayCode.search(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "PAY_CODE_GET_ALL", "PAY_CODE_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			log.debug("Searchs Pay Code Method:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search1(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search search Pay Code Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayCode.search(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "PAY_CODE_GET_ALL", "PAY_CODE_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			log.debug("Search Pay Code Methods:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	@RequestMapping(value = "/searchPayCodeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPayCodeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		log.info("Search Position Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayCode.searchPositionPayCodeId(dtoSearch);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			log.debug("Search Pay Code Methods:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllPayCodeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllPayCodeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		log.info("Search PayCode Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayCode.getAllPayCodeId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "PAYCODE_GET_ALL", "PAYCODE_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			log.debug("Search Pay Code Method:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllPayCodeDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllPayCodeDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoPayCode> dtoPayCodeList = servicePayCode.getAllPayCodeInActiveList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("PAYCODE_GET_ALL", false), dtoPayCodeList);
			} else {
				responseMessage = unauthorizedMsg(response);
			}

		} catch (Exception e) {
			log.error(e);
		}

		return responseMessage;
	}

	@RequestMapping(value = "/searchAllPayCodeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAllPayCodeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		log.info("searchAllPayCodeId Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayCode.searchAllPayCodeId(dtoSearch);
			responseMessage = displayMessage(dtoSearch, "PAYCODE_CODE_GET_ALL", "PAYCODE_CODE_LIST_NOT_GETTING",
					response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if (dtoSearch != null) {
			log.debug("Search Pay Code Method:" + dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getAllPaycodeforDeduction", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllPaycodeforDeduction(@RequestBody DtoPayCode dtoPayCode, HttpServletRequest request)
			throws Exception {
		log.info("Search BuildPayrollCheckByPaycode Method");
		DtoSearch dtoSearch = null;
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayCode.getAllPaycodeforDeduction(dtoPayCode);
			responseMessage = displayMessage(dtoSearch, "BUILD_CHECK_PAY_CODE_GET_ALL", "BUILD_CHECK_NOT_GETTING",
					response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}

}
