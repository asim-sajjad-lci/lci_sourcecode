package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoOrientationSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceOrientationSetup;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/orientationSetup")
public class ControllerOrientationSetup extends BaseController{

	private static final Logger LOGGER = Logger.getLogger(ControllerOrientationSetup.class);

	@Autowired(required = true)
	ServiceOrientationSetup serviceOrientationSetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request 
	{
		"orientationId":"11",
		"orientationDesc":"TESTDFDF",
		"orientationArbicDesc":"welcome"
		}*/
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request 
	{
	    "code": 201,
	    "status": "CREATED",
	    "result": {
	        "id": null,
	        "orientationId": "11",
	        "orientationDesc": "TESTDFDF",
	        "orientationArbicDesc": "dhfdfdfjhdjf",
	        "ids": null,
	        "isActive": null,
	        "messageType": null,
	        "message": null,
	        "deleteMessage": null,
	        "associateMessage": null,
	        "delete": null,
	        "isRepeat": null
	    },
	    "btiMessage": {
	        "message": "N/A",
	        "messageShort": "N/A"
	    }
	}
*/
	
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoOrientationSetup dtoOrientationSetup) throws Exception {
		LOGGER.info("Create OrientationSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoOrientationSetup = serviceOrientationSetup.saveOrUpdateOrientationSetup(dtoOrientationSetup);
			responseMessage = displayMessage(dtoOrientationSetup,"ORIENTATIONSETUP_CREATED","ORIENTATIONSETUP__NOT_CREATED",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create OrientationSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request 
	{
		"orientationId":"11",
		"orientationDesc":"TESTDFDF",
		"orientationArbicDesc":"welcome"
		}*/
	
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request {
	    "code": 201,
	    "status": "CREATED",
	    "result": {
	        "id": null,
	        "orientationId": "11",
	        "orientationDesc": "TESTDFDF",
	        "orientationArbicDesc": "welcome",
	        "ids": null,
	        "isActive": null,
	        "messageType": null,
	        "message": null,
	        "deleteMessage": null,
	        "associateMessage": null,
	        "delete": null,
	        "isRepeat": null
	    },
	    "btiMessage": {
	        "message": "N/A",
	        "messageShort": "N/A"
	    }
	}*/
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoOrientationSetup dtoOrientationSetup) throws Exception {
		LOGGER.info("Update OrientationSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoOrientationSetup = serviceOrientationSetup.saveOrUpdateOrientationSetup(dtoOrientationSetup);
			responseMessage = displayMessage(dtoOrientationSetup,"ORIENTATIONSETUP_UPDATED_SUCCESS","ORIENTATIONPLANSETUP_NOT_UPDATED",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update OrientationSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request {
		"ids":[2]
		}*/
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request 	{
	    "code": 201,
	    "status": "CREATED",
	    "result": {
	        "id": null,
	        "orientationId": null,
	        "orientationDesc": null,
	        "orientationArbicDesc": null,
	        "ids": null,
	        "isActive": null,
	        "messageType": null,
	        "message": null,
	        "deleteMessage": "N/A",
	        "associateMessage": "N/A",
	        "delete": [
	            {
	                "id": 2,
	                "orientationId": null,
	                "orientationDesc": "TESTDFDF",
	                "orientationArbicDesc": "welcome",
	                "ids": null,
	                "isActive": null,
	                "messageType": null,
	                "message": null,
	                "deleteMessage": null,
	                "associateMessage": null,
	                "delete": null,
	                "isRepeat": null
	            }
	        ],
	        "isRepeat": null
	    },
	    "btiMessage": {
	        "message": "N/A",
	        "messageShort": "N/A"
	    }
	}
	
	*/
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoOrientationSetup dtoOrientationSetup) throws Exception {
		LOGGER.info("Delete OrientationSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoOrientationSetup.getIds() != null && !dtoOrientationSetup.getIds().isEmpty()) {
				DtoOrientationSetup dtoOrientationSetup2 = serviceOrientationSetup.deleteOrientationSetup(dtoOrientationSetup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ORIENTATIONSETUP_DELETED", false), dtoOrientationSetup2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Delete OrientationSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request {
     	
		"searchKeyword" : "",
		"sortOn" : "",
		 "sortBy" : "ASC",
		"pageNumber":"0",
		"pageSize":"10"
		}*/
	
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request {
	    "code": 200,
	    "status": "OK",
	    "result": {
	        "searchKeyword": "",
	        "pageNumber": 0,
	        "pageSize": 10,
	        "sortOn": "",
	        "sortBy": "ASC",
	        "totalCount": 2,
	        "records": [
	            {
	                "id": 1,
	                "orientationId": "11",
	                "orientationDesc": "TESTDFDF",
	                "orientationArbicDesc": "dhfdfdfjhdjf",
	                "ids": null,
	                "isActive": null,
	                "messageType": null,
	                "message": null,
	                "deleteMessage": null,
	                "associateMessage": null,
	                "delete": null,
	                "isRepeat": null
	            },
	            {
	                "id": 3,
	                "orientationId": "17",
	                "orientationDesc": "fgfgfg",
	                "orientationArbicDesc": "fgfgffg",
	                "ids": null,
	                "isActive": null,
	                "messageType": null,
	                "message": null,
	                "deleteMessage": null,
	                "associateMessage": null,
	                "delete": null,
	                "isRepeat": null
	            }
	        ]
	    },
	    "btiMessage": {
	        "message": "N/A",
	        "messageShort": "N/A"
	    }
	}*/
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search OrientationSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceOrientationSetup.searchOrientationSetup(dtoSearch);
			responseMessage = displayMessage(dtoSearch,"ORIENTATIONSETUP_GET_ALL","ORIENTATIONSETUP_LIST_NOT_GETTING",serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search OrientationSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request 	{
     	
		"id":3
		}
	*/
	
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request 
	{
	    "code": 201,
	    "status": "CREATED",
	    "result": {
	        "id": 3,
	        "orientationId": "17",
	        "orientationDesc": "fgfgfg",
	        "orientationArbicDesc": "fgfgffg",
	        "ids": null,
	        "isActive": null,
	        "messageType": null,
	        "message": null,
	        "deleteMessage": null,
	        "associateMessage": null,
	        "delete": null,
	        "isRepeat": null
	    },
	    "btiMessage": {
	        "message": "N/A",
	        "messageShort": "N/A"
	    }
	}
	*/
	
	@RequestMapping(value = "/getOrientationId", method = RequestMethod.POST)
	public ResponseMessage getretirementPlanId(HttpServletRequest request, @RequestBody DtoOrientationSetup dtoOrientationSetup) throws Exception {
		LOGGER.info("Get OrientationSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoOrientationSetup dtoOrientationSetupObj = serviceOrientationSetup.getOrientationSetupById(dtoOrientationSetup.getId());
			responseMessage = displayMessage(dtoOrientationSetupObj,"ORIENTATIONSETUP_LIST_GET_DETAIL","ORIENTATIONSETUP_NOT_GETTING",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get OrientationSetup by Id Method:"+dtoOrientationSetup.getId());
		return responseMessage;
	}
	
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request {
     	
		"orientationId":"17"
		}*/
	
	
	
	
	

	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request {
	    "code": 302,
	    "status": "FOUND",
	    "result": {
	        "id": null,
	        "orientationId": null,
	        "orientationDesc": null,
	        "orientationArbicDesc": null,
	        "ids": null,
	        "isActive": null,
	        "messageType": null,
	        "message": null,
	        "deleteMessage": null,
	        "associateMessage": null,
	        "delete": null,
	        "isRepeat": true
	    },
	    "btiMessage": {
	        "message": "N/A",
	        "messageShort": "N/A"
	    }
	}
	*/
	
	@RequestMapping(value = "/orientationIdCheck", method = RequestMethod.POST)
	public ResponseMessage divisionIdCheck(HttpServletRequest request, @RequestBody DtoOrientationSetup dtoOrientationSetup) throws Exception {
		LOGGER.info("orientationIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoOrientationSetup dtoOrientationSetupObj = serviceOrientationSetup.repeatByorientationId(dtoOrientationSetup.getOrientationId());
			responseMessage = displayMessage(dtoOrientationSetupObj,"ORIENTATIONSETUP_EXIST","ORIENTATIONSETUP_REPEAT_NOT_FOUND",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/searchOrientationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchOrientationId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search OrientationId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceOrientationSetup.searchOrientationId(dtoSearch);
			responseMessage = displayMessage(dtoSearch,"ORIENTATION_SETUP_GET_ALL","ORIENTATION_SETUP_NOT_GETTING",serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search OrientationSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	

}
