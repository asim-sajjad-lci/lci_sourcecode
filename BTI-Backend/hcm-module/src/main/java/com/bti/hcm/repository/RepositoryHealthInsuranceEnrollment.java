package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.HealthInsuranceEnrollment;

@Repository("repositoryHealthInsuranceEnrollment")

public interface RepositoryHealthInsuranceEnrollment  extends JpaRepository<HealthInsuranceEnrollment, Integer>{

	HealthInsuranceEnrollment findByIdAndIsDeleted(Integer id, boolean b);

	HealthInsuranceEnrollment findOne(Integer planId);
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update HealthInsuranceEnrollment h set h.isDeleted =:deleted ,h.updatedBy =:updateById where h.id =:id ")
	public void deleteSingleHealthInsuranceEnrollment(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	@Query("select count(*) from HealthInsuranceEnrollment h where (h.policyNumber LIKE :searchKeyWord) and h.isDeleted=false")
	Integer predictiveHealthInsuranceEnrollmentSearchTotalCount(@Param("searchKeyWord")  String searchKeyWord);

	@Query("select h from HealthInsuranceEnrollment h where ((h.policyNumber like :searchKeyWord or h.helthInsurance.helthInsuranceId like :searchKeyWord) and h.isDeleted=false)")
	List<HealthInsuranceEnrollment> predictiveHealthInsuranceEnrollmentSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Query("select h from HealthInsuranceEnrollment h where h.employeeMaster.employeeIndexId =:id and h.isDeleted=false")
	List<HealthInsuranceEnrollment> findByEmployeeId(@Param("id") int id);


	@Query("select count(*) from HealthInsuranceEnrollment h ")
	public Integer getCountOfTotalHealthInsuranceEnrollment();

	@Query("select h from HealthInsuranceEnrollment h where h.employeeMaster.employeeIndexId =:id or h.helthInsurance.helthInsuranceId=:id and h.isDeleted=false")	
	List<HealthInsuranceEnrollment> findByEmployeeIds(@Param("id")Integer employeeId, @Param("id")Integer helthInsuranceId);

	@Query("select h from HealthInsuranceEnrollment h where h.employeeMaster.employeeIndexId =:id and h.isDeleted=false")
	HealthInsuranceEnrollment findByEmployeeId1(@Param("id")int id);

}
