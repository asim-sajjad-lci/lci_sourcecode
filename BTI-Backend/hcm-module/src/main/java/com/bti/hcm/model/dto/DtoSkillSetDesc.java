package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.SkillSetDesc;
import com.bti.hcm.model.SkillSetSetup;
import com.bti.hcm.util.UtilRandomKey;

public class DtoSkillSetDesc extends DtoBase {

	private Integer id;
	private String skillSetDesc;
	private String skillSetArbicDesc;
	private SkillSetSetup skillSetSetup;
	private int skillSetSeqn;
	private DtoSkillSteup skillsId;
	private String skillSetId;
	private Integer skillsSetId;
	private Integer skillsSetPrimaryId;
	private Integer skillId;
	private boolean skillRequired;
	private String comment;
	private List<DtoSkillSetDesc> deleteSkillSet;
	private DtoSkillSteup dtoSkillSteup;
	
	private List<DtoSkillSteup> dtoSkillSteupList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SkillSetSetup getSkillSetSetup() {
		return skillSetSetup;
	}

	public void setSkillSetSetup(SkillSetSetup skillSetSetup) {
		this.skillSetSetup = skillSetSetup;
	}

	public int getSkillSetSeqn() {
		return skillSetSeqn;
	}

	public void setSkillSetSeqn(int skillSetSeqn) {
		this.skillSetSeqn = skillSetSeqn;
	}

	public DtoSkillSteup getSkillsId() {
		return skillsId;
	}

	public void setSkillsId(DtoSkillSteup skillsId) {
		this.skillsId = skillsId;
	}

	public boolean isSkillRequired() {
		return skillRequired;
	}

	public void setSkillRequired(boolean skillRequired) {
		this.skillRequired = skillRequired;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<DtoSkillSetDesc> getDeleteSkillSet() {
		return deleteSkillSet;
	}

	public void setDeleteSkillSet(List<DtoSkillSetDesc> deleteSkillSet) {
		this.deleteSkillSet = deleteSkillSet;
	}

	public Integer getSkillsSetId() {
		return skillsSetId;
	}

	public void setSkillsSetId(Integer skillsSetId) {
		this.skillsSetId = skillsSetId;
	}

	public Integer getSkillId() {
		return skillId;
	}

	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}

	public DtoSkillSetDesc() {
	}

	public Integer getSkillsSetPrimaryId() {
		return skillsSetPrimaryId;
	}

	public void setSkillsSetPrimaryId(Integer skillsSetPrimaryId) {
		this.skillsSetPrimaryId = skillsSetPrimaryId;
	}

	public String getSkillSetDesc() {
		return skillSetDesc;
	}

	public void setSkillSetDesc(String skillSetDesc) {
		this.skillSetDesc = skillSetDesc;
	}

	public String getSkillSetArbicDesc() {
		return skillSetArbicDesc;
	}

	public void setSkillSetArbicDesc(String skillSetArbicDesc) {
		this.skillSetArbicDesc = skillSetArbicDesc;
	}

	public DtoSkillSetDesc(SkillSetDesc skillSetDesc) {
		this.id = skillSetDesc.getId();

		if (UtilRandomKey.isNotBlank(skillSetDesc.getComment())) {
			this.comment = skillSetDesc.getComment();
		} else {
			this.comment = "";
		}
	}

	public String getSkillSetId() {
		return skillSetId;
	}

	public void setSkillSetId(String skillSetId) {
		this.skillSetId = skillSetId;
	}

	public DtoSkillSteup getDtoSkillSteup() {
		return dtoSkillSteup;
	}

	public void setDtoSkillSteup(DtoSkillSteup dtoSkillSteup) {
		this.dtoSkillSteup = dtoSkillSteup;
	}

	public List<DtoSkillSteup> getDtoSkillSteupList() {
		return dtoSkillSteupList;
	}

	public void setDtoSkillSteupList(List<DtoSkillSteup> dtoSkillSteupList) {
		this.dtoSkillSteupList = dtoSkillSteupList;
	}

}
