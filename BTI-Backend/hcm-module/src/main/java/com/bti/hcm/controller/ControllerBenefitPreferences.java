package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBenefitPreferences;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceBenefitPreferences;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/benefitPreferences")
public class ControllerBenefitPreferences extends BaseController{
	
	@Autowired
	ServiceBenefitPreferences serviceBenefitPreferences;
	
	@Autowired
	ServiceResponse response;
	
	private Logger log = Logger.getLogger(ControllerBenefitPreferences.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	
	
	/**
	 * @param request
	 * 
	 * {  
		 "postEstimatedDate" : 1,
		   "postPaymentDates" : 12,
		   "workingdaysWeek" : 13,
		   "workingHoursDay" : 14,
		   "eligibilityDate" : 15,
		   "paperworkDeadlines" : 16
		}
		 @param response{
				 
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"postEstimatedDate": 1,
		"postPaymentDates": 12,
		"workingdaysWeek": 13,
		"workingHoursDay": 14,
		"eligibilityDate": 15,
		"paperworkDeadlines": 16,
		"inActive": false,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isRepeat": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null
		 
		 }
	 * @param dtoBenefitPreferences
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoBenefitPreferences dtoBenefitPreferences) throws Exception{
		log.info("Create BenefitPreferences Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBenefitPreferences = serviceBenefitPreferences.saveOrUpdate(dtoBenefitPreferences);
			responseMessage=displayMessage(dtoBenefitPreferences, "BENEFIT_PREFRENCES_CREATED", "BENEFIT_PREFRENCES_NOT_CREATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Create BenefitPreferences Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	
	/**
	 * @param request
		  {  
			"id":1,      
	    "postEstimatedDate" : 11,
	     "postPaymentDates" : 5,
	     "workingdaysWeek" : 6,
	     "workingHoursDay" : 7,
	     "eligibilityDate" : 9,
	     "paperworkDeadlines" : 16
			}
			 @param response{
					 
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": null,
			"postEstimatedDate": 1,
			"postPaymentDates": 12,
			"workingdaysWeek": 13,
			"workingHoursDay": 14,
			"eligibilityDate": 15,
			"paperworkDeadlines": 16,
			"inActive": false,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isRepeat": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"delete": null
			 
			 }
	 * @param dtoBenefitPreferences
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoBenefitPreferences dtoBenefitPreferences) throws Exception {
		log.info("Update BenefitPreferences Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBenefitPreferences = serviceBenefitPreferences.saveOrUpdate(dtoBenefitPreferences);
			responseMessage=displayMessage(dtoBenefitPreferences, "BENEFIT_PREFRENCES_UPDATED_SUCCESS", "BENEFIT_PREFRENCES_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Update BenefitPreferences Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
		  
				 "ids":[1]
		}
		
		@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"timeCodeId": null,
		"inActive": false,
		"desc": null,
		"arbicDesc": null,
		"accrualPeriod": null,
		"timeType": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": "Position deleted successfully.",
		"associateMessage": "Position has been assigned.",
		"delete": [
		  {
		"id": 1,
		"timeCodeId": null,
		"inActive": false,
		"desc": null,
		"arbicDesc": null,
		"accrualPeriod": null,
		"timeType": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		}
		],
		"isRepeat": null
		},
		"btiMessage": {
		"message": "N/A",
		"messageShort": "N/A"
		}
		}
	 * @param dtoBenefitPreferences
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoBenefitPreferences dtoBenefitPreferences) throws Exception {
		log.info("Delete BenefitPreferences Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoBenefitPreferences.getIds() != null && !dtoBenefitPreferences.getIds().isEmpty()) {
				DtoBenefitPreferences dtoBenefitPreferences1 = serviceBenefitPreferences.delete(dtoBenefitPreferences.getIds());
				if(dtoBenefitPreferences1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("BENEFIT_PREFRENCES_DELETED", false), dtoBenefitPreferences1);

				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoBenefitPreferences1.getMessageType());
					btiMessageHcm.setMessageShort("BENEFIT_PREFRENCES_NOT_DELETE_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoBenefitPreferences1);

				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete BenefitPreferences Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  "id" : 1
	}
	@param response{
				
		"code": 201,
"status": "CREATED",
"result": {
"code": 201,
"status": "CREATED",
"result": {
"id": 1,
"postEstimatedDate": 11,
"postPaymentDates": 5,
"workingdaysWeek": 6,
"workingHoursDay": 7,
"eligibilityDate": 9,
"paperworkDeadlines": 16,
		
		}
	 * @param dtoBenefitPreferences
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getBenefitPreferencesId(HttpServletRequest request, @RequestBody DtoBenefitPreferences dtoBenefitPreferences) throws Exception {
		log.info("Get ShiftCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoBenefitPreferences dtoBenefitPreferences1 = serviceBenefitPreferences.getById(dtoBenefitPreferences.getId());
			responseMessage=displayMessage(dtoBenefitPreferences1, "BENEFIT_PREFRENCES_GET_DETAIL", MessageConstant.BENEFIT_PREFRENCES_LIST_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Get BenefitPreferences by Id Method:"+dtoBenefitPreferences.getId());
		return responseMessage;
	}
	
	
	/**
	 * @param dtoSearch
	 * @param request{  
		 "searchKeyword" : ""
		}response{
			
"code": 200,
"status": "OK",
"result": {
"searchKeyword": "",
"totalCount": 1,
"records": [
  {
"createdDate": 1522058981000,
"updatedDate": 1522059198000,
"updatedBy": 2,
"isDeleted": false,
"updatedRow": null,
"rowId": 0,
"id": 1,
"postEstimatedDate": 11,
"postPaymentDates": 5,
"workingdaysWeek": 6,
"workingHoursDay": 7,
"eligibilityDate": 9,
"paperworkDeadlines": 16
}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search BenefitPreferences Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBenefitPreferences.getIds(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "BENEFIT_PREFRENCES_GET_ALL", "BENEFIT_PREFRENCES_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{  
    "sortOn" : "",
		  "sortBy" : "",
		 "searchKeyword" : "",
		"pageNumber":"0",
		"pageSize":"10"
  }@param response{
"code": 200,
"status": "OK",
"result": {
"searchKeyword": "",
"pageNumber": 0,
"pageSize": 10,
"sortOn": "",
"sortBy": "",
"totalCount": 1,
"records": [
  {
"id": 1,
"postEstimatedDate": 11,
"postPaymentDates": 5,
"workingdaysWeek": 6,
"workingHoursDay": 7,
"eligibilityDate": 9,
"paperworkDeadlines": 16,
"inActive": false,
"pageNumber": null,
"pageSize": null,
"ids": null,
"isRepeat": null,
"messageType": null,
"message": null,
"deleteMessage": null,
"associateMessage": null,
"delete": null
}
],
}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search search BenefitPreferences Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBenefitPreferences.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "BENEFIT_PREFRENCES_GET_ALL", "BENEFIT_PREFRENCES_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}
	
}
