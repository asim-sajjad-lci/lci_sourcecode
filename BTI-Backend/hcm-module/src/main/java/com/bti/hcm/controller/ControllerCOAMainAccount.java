package com.bti.hcm.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoCOAMainAccounts;
import com.bti.hcm.service.ServiceCOAMainAccount;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;



@RestController
@RequestMapping("/coaMainAccount")
public class ControllerCOAMainAccount extends BaseController{
	
	@Autowired
	ServiceResponse response;

	private static final Logger LOGGER = Logger.getLogger(ControllerCOAMainAccount.class);

	@Autowired
	ServiceHcmHome serviceHome;
	
	@Autowired
	ServiceCOAMainAccount serviceMainAcct;
	
	
	@RequestMapping(value = "/getAllData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllData(HttpServletRequest request) throws Exception {
		LOGGER.info("Search ItemClassAccountSetup Method");
		ResponseMessage responseMessage = null;
		List<DtoCOAMainAccounts> dtoSearch = new ArrayList<>();
		boolean flag =serviceHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceMainAcct.getAllData();
			responseMessage=displayMessage(dtoSearch, "ITEM_CLASS_ACCOUNT_GET_ALL", "ITEM_CLASS_ACCOUNT_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Search ItemClassAccountSetup Method:"+dtoSearch);
		return responseMessage;
	}
}
