package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Batches;
import com.bti.hcm.model.BuildChecks;
import com.bti.hcm.model.BuildPayrollCheckByBatches;
import com.bti.hcm.model.Distribution;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.TransactionEntry;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoBatches;
import com.bti.hcm.model.dto.DtoBatchesDisplay;
import com.bti.hcm.model.dto.DtoBuildChecks;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByBatches;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBatches;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByBatches;
import com.bti.hcm.repository.RepositoryDistribution;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryTransactionEntry;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;

@Service("/serviceBuildPayrollCheckByBatches")
public class ServiceBuildPayrollCheckByBatches {
	
	
	
static Logger log = Logger.getLogger(ServiceBuildPayrollCheckByBatches.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	
	@Autowired(required = false)
	RepositoryBuildChecks repositoryBuildChecks;

	@Autowired(required = false)
	RepositoryBuildPayrollCheckByBatches repositoryBuildPayrollCheckByBatches;
	
	@Autowired(required = false)
	RepositoryBatches repositoryBatches;
	
	@Autowired(required = false)
	RepositoryDistribution repositoryDistribution;
	
	@Autowired
	RepositoryTransactionEntry repositoryTransactionEntry;
	
	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;

	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;
	
	public DtoBuildPayrollCheckByBatches saveOrUpdate(DtoBuildPayrollCheckByBatches dtoBuildPayrollCheckByBatches) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByBatches.getListBuildChecks() != null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());	
			}
			
			
			
			
				if(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
					repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());
					}
				
				for (DtoBatches  dtobatches : dtoBuildPayrollCheckByBatches.getBatches()) {
					Batches batches=repositoryBatches.findByIdAndIsDeleted(dtobatches.getId(),false);
					BuildPayrollCheckByBatches buildPayrollCheckByBatches=new BuildPayrollCheckByBatches();
					buildPayrollCheckByBatches.setCreatedDate(new Date());
					Integer rowId = repositoryBuildPayrollCheckByBatches.findAll().size();
					Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					
					buildPayrollCheckByBatches.setRowId(increment);
					buildPayrollCheckByBatches.setBatches(batches);
					buildPayrollCheckByBatches.setBuildChecks(buildChecks);
					repositoryBuildPayrollCheckByBatches.saveAndFlush(buildPayrollCheckByBatches);
					if(batches.getStatus()==1) {
						
					}
					if(batches.getStatus()!=2) {
						batches.setStatus((byte)1);	
					}
					repositoryBatches.saveAndFlush(batches);
					
					List<TransactionEntry> trList = this.repositoryTransactionEntry.findByBatches(batches.getId());
					for (TransactionEntry transactionEntry1 : trList) {
						TransactionEntry transactionEntry=null;
						transactionEntry = new TransactionEntry();
						transactionEntry.setCreatedDate(new Date());
						
						TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
						Integer increment11=0;
						if(transactionEntry2 != null) {
					//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
						if(transactionEntry2.getRowId()!=0) {
							increment11= transactionEntry2.getRowId()+1;
						}else {
							increment11=1;
						}
					}else {
						increment11=1;
					}
						transactionEntry.setRowId(increment11);
						transactionEntry.setBatches(batches);
						if(transactionEntry2 != null) {
						transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
						}else {
							transactionEntry.setEntryNumber(1);
						}
						transactionEntry.setEntryDate(new Date());
						transactionEntry.setFromDate(new Date());
						transactionEntry.setToDate(new Date());
						transactionEntry.setDescription(transactionEntry1.getDescription());
						transactionEntry.setArabicDescription(transactionEntry1.getArabicDescription());
						transactionEntry.setFromBuild(false);
						transactionEntry.setIsDeleted(false);
						transactionEntry.setUpdatedRow(new Date());
						transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
						if(!transactionEntry1.getTransDetailList().isEmpty()) {
							for (TransactionEntryDetail detailList : transactionEntry1.getTransDetailList()) {
								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
								TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
								if(transactionEntryDetailLastRecord!=null) {
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
								}else {
									transactionEntryDetail.setRowId(1);
								}
								transactionEntryDetail.setTransactionEntry(transactionEntry);
								transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
								transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
								transactionEntryDetail.setFromBuild(false);
								transactionEntryDetail.setTransactionType((short)1);
								transactionEntryDetail.setIsDeleted(false);
								transactionEntryDetail.setCreatedDate(new Date());
								transactionEntryDetail.setUpdatedBy(loggedInUserId);
								transactionEntryDetail.setUpdatedDate(new Date());
								transactionEntryDetail.setUpdatedRow(new Date());
								transactionEntryDetail.setBuildChecks(buildChecks);
								transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
								if(detailList.getBenefitCode() != null) {
								transactionEntryDetail.setBenefitCode(detailList.getBenefitCode());
								}
								if(detailList.getDeductionCode() != null) {
									transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
								}
								if(detailList.getCode() != null) {
									transactionEntryDetail.setCode(detailList.getCode());
								}
								if(detailList.getDimensions() != null) {
									transactionEntryDetail.setDimensions(detailList.getDimensions());
								}
								transactionEntryDetail.setDepartment(detailList.getDepartment());
								transactionEntryDetail.setAmount(detailList.getAmount());
								transactionEntryDetail.setPayRate(detailList.getPayRate());
								transactionEntryDetail.setToDate(detailList.getToDate());
								transactionEntryDetail.setFromDate(detailList.getFromDate());
								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
							}
						}
					}
					/*TransactionEntry transactionEntry=null;
					transactionEntry = new TransactionEntry();
					transactionEntry.setCreatedDate(new Date());
					
					TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
				//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
					Integer increment11=0;
					if(transactionEntry2.getRowId()!=0) {
						increment11= transactionEntry2.getRowId()+1;
					}else {
						increment11=1;
					}
					transactionEntry.setRowId(increment11);
					transactionEntry.setBatches(batches);
					transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
					transactionEntry.setEntryDate(new Date());
					transactionEntry.setFromDate(new Date());
					transactionEntry.setToDate(new Date());
					transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK BATCH");
					transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK BATCH");
					transactionEntry.setFromBuild(false);
					transactionEntry.setIsDeleted(false);.
					transactionEntry.setUpdatedRow(new Date());
					transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
					TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
					TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
					transactionEntryDetail.setTransactionEntry(transactionEntry);
					transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
					transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
					transactionEntryDetail.setFromBuild(false);
					transactionEntryDetail.setTransactionType((short)1);
					transactionEntryDetail.setIsDeleted(false);
					transactionEntryDetail.setCreatedDate(new Date());
					transactionEntryDetail.setUpdatedBy(loggedInUserId);
					transactionEntryDetail.setUpdatedDate(new Date());
					transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
					transactionEntryDetail.setUpdatedRow(new Date());
					transactionEntryDetail.setBuildChecks(buildChecks);
					transactionEntryDetail.setBatches(batches);
					repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
				
					
				}
				
				List<Integer> ids = dtoBuildPayrollCheckByBatches.getBatches().stream().map(m->m.getId()).collect(Collectors.toList());
				
				if(!ids.isEmpty()) {
					List<Batches> listIds = repositoryBatches.getExculdeBatches(ids);
					List<Integer> listBatchesId = new ArrayList<>();
					//List<TransactionEntryDetail> transactionEntryDetails = new ArrayList<>();
					for (Batches batches : listIds) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
							if(buildPayrollCheckByBatches.getBuildChecks().getId().equals(buildChecks.getId())) {
								
								listBatchesId.add(batches.getId());
							}
						}
						
						/*List<TransactionEntryDetail> transcationEntryDetailList = repositoryTransactionEntryDetail.findByBatchesAndBuildChecks(batches.getId(),buildChecks.getId());
						for (TransactionEntryDetail transactionEntryDetail : transcationEntryDetailList) {
							repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(true, loggedInUserId, true, transactionEntryDetail.getId());
						}*/
					}
					
					if(!listBatchesId.isEmpty()) {
						repositoryBatches.updateExculdeBatches(listBatchesId);
					}
					/*if(!transactionEntryDetails.isEmpty()) {
						
					}*/
					System.out.println(listIds);
				}
				
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		return dtoBuildPayrollCheckByBatches;
	}

	public DtoSearch getAllByCodeType(DtoBuildPayrollCheckByBatches dtoBuildPayrollCheckByBatches) {
		DtoSearch dtoSearch=new DtoSearch();
		String searchKeyword = dtoBuildPayrollCheckByBatches.getSearchKeyword();
		List<DtoBatchesDisplay> listResponse=new ArrayList<>();
		List<BuildPayrollCheckByBatches>buildcheckList=this.repositoryBuildPayrollCheckByBatches.findByBuildChecksId("%"+searchKeyword+"%",dtoBuildPayrollCheckByBatches.getId());
		boolean isProcess = false;
		for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildcheckList) {
			DtoBatchesDisplay dtoBuildPayrollCheckByBatchess=new DtoBatchesDisplay();
			
			if(buildPayrollCheckByBatches.getBuildChecks()!=null) {
				List<Distribution> list =repositoryDistribution.getAllByBuildId(buildPayrollCheckByBatches.getBuildChecks().getId());
				if(!list.isEmpty()) {
					isProcess = true;	
				}
			}
			
			
			if(buildPayrollCheckByBatches.getBatches()!=null) {
				dtoBuildPayrollCheckByBatchess.setBatchPrimaryId(buildPayrollCheckByBatches.getBatches().getId());
				dtoBuildPayrollCheckByBatchess.setBatchId(buildPayrollCheckByBatches.getBatches().getBatchId());
				dtoBuildPayrollCheckByBatchess.setDescription(buildPayrollCheckByBatches.getBatches().getDescription());
				dtoBuildPayrollCheckByBatchess.setStatus(buildPayrollCheckByBatches.getBatches().getStatus());
				dtoBuildPayrollCheckByBatchess.setUserID(buildPayrollCheckByBatches.getBatches().getUserID());
			}
			if(buildPayrollCheckByBatches.getBatches()!=null) {
				dtoBuildPayrollCheckByBatchess.setBuildChecksId(buildPayrollCheckByBatches.getBuildChecks().getId());
				dtoBuildPayrollCheckByBatchess.setStatusCheck(buildPayrollCheckByBatches.getBuildChecks().getMonthly());
				
			}
			listResponse.add(dtoBuildPayrollCheckByBatchess);
		}
		
		/*List<Batches>list=this.repositoryBatches.findByIsDeleted(false);
		if(list!=null && !list.isEmpty()) {
			for (Batches batches : list) {
				long count =listResponse.stream().filter(d-> d.getBatchPrimaryId().equals(batches.getId())).count();
				if(count == 0) {
					DtoBatchesDisplay dtoBuildPayrollCheckByBatchess=new DtoBatchesDisplay();
					dtoBuildPayrollCheckByBatchess.setBatchPrimaryId(batches.getId());
					dtoBuildPayrollCheckByBatchess.setBatchId(batches.getBatchId());
					dtoBuildPayrollCheckByBatchess.setDescription(batches.getDescription());
					dtoBuildPayrollCheckByBatchess.setStatus(batches.isStatus());
					dtoBuildPayrollCheckByBatchess.setUserID(batches.getUserID());
					listResponse.add(dtoBuildPayrollCheckByBatchess);
				}
				
			}
		}*/
		
		
		if(!isProcess) {
			BuildChecks buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildPayrollCheckByBatches.getId(),false);
			if(buildChecks!=null) {
				//List<Batches> listBatch = repositoryBatches.getByFromAndToDate(buildChecks.getDateFrom(),buildChecks.getDateTo());
				List<Batches> listBatch = repositoryBatches.predictiveBatchesSearch("%"+searchKeyword+"%");
				if(listBatch!=null && !listBatch.isEmpty()) {
					for (Batches batches : listBatch) {
						long count =listResponse.stream().filter(d->d.getBatchPrimaryId()!=null && d.getBatchPrimaryId().equals(batches.getId())).count();
						if(count == 0) {
							if(batches.getStatus() != 0 && batches.getStatus()!=1 && batches.getStatus()!=2) {
								DtoBatchesDisplay dtoBuildPayrollCheckByBatchess=new DtoBatchesDisplay();
								dtoBuildPayrollCheckByBatchess.setBatchPrimaryId(batches.getId());
								dtoBuildPayrollCheckByBatchess.setBatchId(batches.getBatchId());
								dtoBuildPayrollCheckByBatchess.setDescription(batches.getDescription());
								dtoBuildPayrollCheckByBatchess.setStatus(batches.getStatus());
								dtoBuildPayrollCheckByBatchess.setUserID(batches.getUserID());
								dtoBuildPayrollCheckByBatchess.setStatusCheck(false);
								listResponse.add(dtoBuildPayrollCheckByBatchess);	
							}
							
						}
						
					}
				}
		}
		
		
		}
		
		dtoSearch.setTotalCount(listResponse.size());
	   /*===============>PASINATION RELETED CODE=====================>*/
		
		if(dtoBuildPayrollCheckByBatches.getPageNumber()!=null && dtoBuildPayrollCheckByBatches.getPageSize()!=null) {
		
			Integer from = ((dtoBuildPayrollCheckByBatches.getPageNumber()) * (dtoBuildPayrollCheckByBatches.getPageSize()));
			Integer to = from+dtoBuildPayrollCheckByBatches.getPageSize();
			
			if(listResponse.size()>=from && listResponse.size()>=to) { 
				listResponse = listResponse.subList(from, to);
			}else {
				if(from>listResponse.size()) {
					listResponse.subList(0, listResponse.size()).clear();
				}else {
					listResponse.subList(0, from).clear();
				}
				
			}
			
		}
		
		dtoSearch.setPageNumber(dtoBuildPayrollCheckByBatches.getPageNumber());
		dtoSearch.setPageSize(dtoBuildPayrollCheckByBatches.getPageSize());
		dtoSearch.setSortOn(dtoBuildPayrollCheckByBatches.getSortOn());
		dtoSearch.setSortBy(dtoBuildPayrollCheckByBatches.getSortBy());
		dtoSearch.setRecords(listResponse);
		log.debug("Search TransactionEntry Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	public DtoSearch getAllForFromDateToDate(DtoBuildChecks dtoBuildChecks) {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoBatchesDisplay> listResponse=new ArrayList<>();
		List<BuildPayrollCheckByBatches>buildcheckList=this.repositoryBuildPayrollCheckByBatches.findByCreatedDate(dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo(),dtoBuildChecks.getId());
		for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildcheckList) {
			DtoBatchesDisplay dtoBuildPayrollCheckByBatchess=new DtoBatchesDisplay();
			
			if(buildPayrollCheckByBatches.getBatches()!=null) {
				dtoBuildPayrollCheckByBatchess.setBatchPrimaryId(buildPayrollCheckByBatches.getBatches().getId());
				dtoBuildPayrollCheckByBatchess.setBatchId(buildPayrollCheckByBatches.getBatches().getBatchId());
				dtoBuildPayrollCheckByBatchess.setDescription(buildPayrollCheckByBatches.getBatches().getDescription());
				dtoBuildPayrollCheckByBatchess.setStatus(buildPayrollCheckByBatches.getBatches().getStatus());
				dtoBuildPayrollCheckByBatchess.setUserID(buildPayrollCheckByBatches.getBatches().getUserID());
			}
			if(buildPayrollCheckByBatches.getBatches()!=null) {
				dtoBuildPayrollCheckByBatchess.setBuildChecksId(buildPayrollCheckByBatches.getBuildChecks().getId());
				
			}
			listResponse.add(dtoBuildPayrollCheckByBatchess);
		}
		
		List<Batches>list=this.repositoryBatches.findByIsDeleted(false);
		if(list!=null && !list.isEmpty()) {
			for (Batches batches : list) {
				long count =listResponse.stream().filter(d-> d.getBatchPrimaryId().equals(batches.getId())).count();
				if(count == 0) {
					DtoBatchesDisplay dtoBuildPayrollCheckByBatchess=new DtoBatchesDisplay();
					dtoBuildPayrollCheckByBatchess.setBatchPrimaryId(batches.getId());
					dtoBuildPayrollCheckByBatchess.setBatchId(batches.getBatchId());
					dtoBuildPayrollCheckByBatchess.setDescription(batches.getDescription());
					dtoBuildPayrollCheckByBatchess.setStatus(batches.getStatus());
					dtoBuildPayrollCheckByBatchess.setUserID(batches.getUserID());
					listResponse.add(dtoBuildPayrollCheckByBatchess);
				}
				
			}
		}
		dtoSearch.setRecords(listResponse);
		
		return dtoSearch;
	}
	
	public DtoBuildPayrollCheckByBatches saveOrUpdate1(DtoBuildPayrollCheckByBatches dtoBuildPayrollCheckByBatches) {
		try {
			

			
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			
			/*if(dtoBuildPayrollCheckByBatches.getBuildChecks().getId()!=null) {
				List<TransactionEntryDetail> tranEntryDetail = repositoryTransactionEntryDetail.findByBuildChecks(dtoBuildPayrollCheckByBatches.getBuildChecks().getId());
				for (TransactionEntryDetail transactionEntryDetail : tranEntryDetail) {
					if(transactionEntryDetail!=null) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
					
				}
			}*/
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByBatches.getListBuildChecks() != null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());	
				
			}
			
			
			
			
			
				if(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
					repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());
					}
				
				for (DtoBatches  dtobatches : dtoBuildPayrollCheckByBatches.getBatches()) {
					

					
					List<Batches> batches=repositoryBatches.getIncludeBatches12(dtobatches.getId());
					
					if(batches!=null) {
						for (Batches batches2 : batches) {

							
							
							BuildPayrollCheckByBatches buildPayrollCheckByBatches=new BuildPayrollCheckByBatches();
							buildPayrollCheckByBatches.setCreatedDate(new Date());
							Integer rowId = repositoryBuildPayrollCheckByBatches.findAll().size();
							Integer increment=0;
							if(rowId!=0) {
								increment= rowId+1;
							}else {
								increment=1;
							}
							
							buildPayrollCheckByBatches.setRowId(increment);
							buildPayrollCheckByBatches.setBatches(batches2);
							buildPayrollCheckByBatches.setBuildChecks(buildChecks);
							repositoryBuildPayrollCheckByBatches.saveAndFlush(buildPayrollCheckByBatches);
							if(batches2.getStatus()==1) {
								
							}
							if(batches2.getStatus()!=2) {
								batches2.setStatus((byte)1);	
							}
							//repositoryBatches.saveAndFlush(batches2);
							
							List<TransactionEntry> trList = this.repositoryTransactionEntry.findByBatches1(batches2.getId());
							for (TransactionEntry transactionEntry1 : trList) {
								
							/*	TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								Integer increment11=0;
								if(transactionEntry2 != null) {
							//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
							}else {
								increment11=1;
							}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(batches2);
								if(transactionEntry2 != null) {
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								}else {
									transactionEntry.setEntryNumber(1);
								}
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription(transactionEntry1.getDescription());
								transactionEntry.setArabicDescription(transactionEntry1.getArabicDescription());
								transactionEntry.setFromBuild(false);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);*/
								if(!transactionEntry1.getTransDetailList().isEmpty()) {
									for (TransactionEntryDetail detailList : transactionEntry1.getTransDetailList()) {
										
										if(detailList.getCode()!=null) {
											List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance.findByEmployeeIds(detailList.getEmployeeMaster().getEmployeeIndexId());
											for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
												if(employeePayCodeMaintenance.getInactive()==false) {
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													if(transactionEntryDetailLastRecord!=null) {
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													}else {
														transactionEntryDetail.setRowId(1);
													}
													transactionEntryDetail.setTransactionEntry(transactionEntry1);
													transactionEntryDetail.setEntryNumber(transactionEntry1.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry1.getRowId()+1);
													transactionEntryDetail.setFromBuild(false);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
													transactionEntryDetail.setCode(detailList.getCode());
													if(detailList.getDeductionCode() != null) {
														transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
													}
													if(detailList.getDimensions() != null) {
														transactionEntryDetail.setDimensions(detailList.getDimensions());
													}
													transactionEntryDetail.setDepartment(detailList.getDepartment());
													transactionEntryDetail.setAmount(detailList.getAmount());
													transactionEntryDetail.setPayRate(detailList.getPayRate());
													transactionEntryDetail.setToDate(detailList.getToDate());
													transactionEntryDetail.setFromDate(detailList.getFromDate());
													transactionEntryDetail.setCreatedDate(detailList.getCreatedDate());
													transactionEntryDetail.setUpdatedDate(detailList.getUpdatedDate());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
											
										}
										/*if(detailList.getBenefitCode() != null) {
											List<EmployeeBenefitMaintenance> employeeBenefitMainList = repositoryEmployeeBenefitMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());	
											//List<BenefitCode> benefitCodes= repositoryBenefitCode.findByStartEndDate1(buildChecks.getDateFrom(), buildChecks.getDateTo());
											if(employeeBenefitMainList!=null) {
												for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMainList) {
													if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
														if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
															TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
															TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
															if(transactionEntryDetailLastRecord!=null) {
																transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
															}else {
																transactionEntryDetail.setRowId(1);
															}
															transactionEntryDetail.setTransactionEntry(transactionEntry);
															transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
															transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
															transactionEntryDetail.setFromBuild(false);
															transactionEntryDetail.setTransactionType((short)1);
															transactionEntryDetail.setIsDeleted(false);
															transactionEntryDetail.setCreatedDate(new Date());
															transactionEntryDetail.setUpdatedBy(loggedInUserId);
															transactionEntryDetail.setUpdatedDate(new Date());
															transactionEntryDetail.setUpdatedRow(new Date());
															transactionEntryDetail.setBuildChecks(buildChecks);
															transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
															
															transactionEntryDetail.setBenefitCode(detailList.getBenefitCode());
															if(detailList.getDeductionCode() != null) {
																transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
															}
															if(detailList.getCode() != null) {
																transactionEntryDetail.setCode(detailList.getCode());
															}
															if(detailList.getDimensions() != null) {
																transactionEntryDetail.setDimensions(detailList.getDimensions());
															}
															transactionEntryDetail.setDepartment(detailList.getDepartment());
															transactionEntryDetail.setAmount(detailList.getAmount());
															transactionEntryDetail.setPayRate(detailList.getPayRate());
															transactionEntryDetail.setToDate(detailList.getToDate());
															transactionEntryDetail.setFromDate(detailList.getFromDate());
															repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
															
														}
													}
												}
												
											}
									
										}*/
										
										
										if(detailList.getBenefitCode()!=null) {
													List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													if(!employeeBenefitMaintenanceList.isEmpty()) {
														for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
															if(employeeBenefitMaintenance.getStartDate()!=null && employeeBenefitMaintenance.getEndDate()!=null) {
																if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																	if(employeeBenefitMaintenance.getInactive()==false) {
																		if(detailList.getBenefitCode()!=null) {
																			if(detailList.getBenefitCode().isInActive()==false) {
																				if(detailList.getBenefitCode().getId()== employeeBenefitMaintenance.getBenefitCode().getId()) {

																					TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																					TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																					if(transactionEntryDetailLastRecord!=null) {
																						transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																					}else {
																						transactionEntryDetail1.setRowId(1);
																					}
																					transactionEntryDetail1.setTransactionEntry(transactionEntry1);
																					transactionEntryDetail1.setEntryNumber(transactionEntry1.getRowId()+1);
																					transactionEntryDetail1.setDetailsSequence(transactionEntry1.getRowId()+1);
																					transactionEntryDetail1.setFromBuild(false);
																					transactionEntryDetail1.setTransactionType((short)1);
																					transactionEntryDetail1.setIsDeleted(false);
																					transactionEntryDetail1.setCreatedDate(new Date());
																					transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																					transactionEntryDetail1.setUpdatedDate(new Date());
																					transactionEntryDetail1.setUpdatedRow(new Date());
																					transactionEntryDetail1.setBuildChecks(buildChecks);
																					transactionEntryDetail1.setEmployeeMaster(detailList.getEmployeeMaster());
																					
																					transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																					
																					if(detailList.getDimensions() != null) {
																						transactionEntryDetail1.setDimensions(detailList.getDimensions());
																					}
																					transactionEntryDetail1.setDepartment(detailList.getDepartment());
																					transactionEntryDetail1.setAmount(detailList.getAmount());
																					transactionEntryDetail1.setPayRate(detailList.getPayRate());
																					transactionEntryDetail1.setToDate(detailList.getToDate());
																					transactionEntryDetail1.setFromDate(detailList.getFromDate());
																					transactionEntryDetail1.setFromTranscationEntry(false);
																					repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																				
																				}
																			}
																			
																		}
																		}
																		
																}
															}
															
															
														}
													}
														
											
										
									
										}
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										if(detailList.getDeductionCode()!=null) {
													List<EmployeeDeductionMaintenance> employeeDeductionMainList = repositoryEmployeeDeductionMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													if(!employeeDeductionMainList.isEmpty()) {
														for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMainList) {
															if(employeeDeductionMaintenance.getStartDate()!=null && employeeDeductionMaintenance.getEndDate()!=null) {
																if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																	if(employeeDeductionMaintenance.getInactive()==false) {
																		if(detailList.getDeductionCode()!=null) {
																			if(detailList.getDeductionCode().isInActive()==false) {
																				if(detailList.getDeductionCode().getId()== employeeDeductionMaintenance.getDeductionCode().getId()) {
																					
																					TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																					TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																					if(transactionEntryDetailLastRecord!=null) {
																						transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																					}else {
																						transactionEntryDetail1.setRowId(1);
																					}
																					transactionEntryDetail1.setTransactionEntry(transactionEntry1);
																					transactionEntryDetail1.setEntryNumber(transactionEntry1.getRowId()+1);
																					transactionEntryDetail1.setDetailsSequence(transactionEntry1.getRowId()+1);
																					transactionEntryDetail1.setFromBuild(false);
																					transactionEntryDetail1.setTransactionType((short)1);
																					transactionEntryDetail1.setIsDeleted(false);
																					transactionEntryDetail1.setCreatedDate(new Date());
																					transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																					transactionEntryDetail1.setUpdatedDate(new Date());
																					transactionEntryDetail1.setUpdatedRow(new Date());
																					transactionEntryDetail1.setBuildChecks(buildChecks);
																					transactionEntryDetail1.setEmployeeMaster(detailList.getEmployeeMaster());
																					
																					//transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																					if(detailList.getDeductionCode() != null) {
																						transactionEntryDetail1.setDeductionCode(detailList.getDeductionCode());
																					}
																					/*if(detailList.getCode() != null) {
																						transactionEntryDetail1.setCode(detailList.getCode());
																					}*/
																					if(detailList.getDimensions() != null) {
																						transactionEntryDetail1.setDimensions(detailList.getDimensions());
																					}
																					transactionEntryDetail1.setDepartment(detailList.getDepartment());
																					transactionEntryDetail1.setAmount(detailList.getAmount());
																					transactionEntryDetail1.setPayRate(detailList.getPayRate());
																					transactionEntryDetail1.setToDate(detailList.getToDate());
																					transactionEntryDetail1.setFromDate(detailList.getFromDate());
																					transactionEntryDetail1.setFromTranscationEntry(false);
																					repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																				
																				}
																			}
																			
																		}
																		
																	}
																}
															}
															
															
														}
													}
														
											
									
										}
										/*TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										if(transactionEntryDetailLastRecord!=null) {
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										}else {
											transactionEntryDetail.setRowId(1);
										}
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(false);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
										
										
										if(detailList.getDeductionCode() != null) {
											transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
										}
										if(detailList.getCode() != null) {
											transactionEntryDetail.setCode(detailList.getCode());
										}
										if(detailList.getDimensions() != null) {
											transactionEntryDetail.setDimensions(detailList.getDimensions());
										}
										transactionEntryDetail.setDepartment(detailList.getDepartment());
										transactionEntryDetail.setAmount(detailList.getAmount());
										transactionEntryDetail.setPayRate(detailList.getPayRate());
										transactionEntryDetail.setToDate(detailList.getToDate());
										transactionEntryDetail.setFromDate(detailList.getFromDate());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
									}
								}
							}
							/*TransactionEntry transactionEntry=null;
							transactionEntry = new TransactionEntry();
							transactionEntry.setCreatedDate(new Date());
							
							TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
						//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
							Integer increment11=0;
							if(transactionEntry2.getRowId()!=0) {
								increment11= transactionEntry2.getRowId()+1;
							}else {
								increment11=1;
							}
							transactionEntry.setRowId(increment11);
							transactionEntry.setBatches(batches);
							transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
							transactionEntry.setEntryDate(new Date());
							transactionEntry.setFromDate(new Date());
							transactionEntry.setToDate(new Date());
							transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK BATCH");
							transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK BATCH");
							transactionEntry.setFromBuild(false);
							transactionEntry.setIsDeleted(false);.
							transactionEntry.setUpdatedRow(new Date());
							transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
							TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
							TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
							transactionEntryDetail.setTransactionEntry(transactionEntry);
							transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
							transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
							transactionEntryDetail.setFromBuild(false);
							transactionEntryDetail.setTransactionType((short)1);
							transactionEntryDetail.setIsDeleted(false);
							transactionEntryDetail.setCreatedDate(new Date());
							transactionEntryDetail.setUpdatedBy(loggedInUserId);
							transactionEntryDetail.setUpdatedDate(new Date());
							transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
							transactionEntryDetail.setUpdatedRow(new Date());
							transactionEntryDetail.setBuildChecks(buildChecks);
							transactionEntryDetail.setBatches(batches);
							repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
						}
					
					}
					
				
					
				}
				
				List<Integer> ids = dtoBuildPayrollCheckByBatches.getBatches().stream().map(m->m.getId()).collect(Collectors.toList());
				
				if(!ids.isEmpty()) {
					List<Batches> listIds = repositoryBatches.getExculdeBatches(ids);
					List<Integer> listBatchesId = new ArrayList<>();
					for (Batches batches : listIds) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
							if(buildPayrollCheckByBatches.getBuildChecks().getId().equals(buildChecks.getId())) {
								
								listBatchesId.add(batches.getId());
							}
						}
						
						
					}
					
					if(!listBatchesId.isEmpty()) {
						repositoryBatches.updateExculdeBatches(listBatchesId);
					}
					
					System.out.println(listIds);
				}
				
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		return dtoBuildPayrollCheckByBatches;
	}
	
	
	
	public DtoBuildPayrollCheckByBatches saveOrUpdate12(DtoBuildPayrollCheckByBatches dtoBuildPayrollCheckByBatches) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByBatches.getListBuildChecks() != null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());	
			}
			
			
			
			
				if(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
					repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());
					}
				
				for (DtoBatches  dtobatches : dtoBuildPayrollCheckByBatches.getBatches()) {
					Batches batches=repositoryBatches.findByIdAndIsDeleted(dtobatches.getId(),false);
					if(batches!=null) {
						BuildPayrollCheckByBatches buildPayrollCheckByBatches=new BuildPayrollCheckByBatches();
						buildPayrollCheckByBatches.setCreatedDate(new Date());
						Integer rowId = repositoryBuildPayrollCheckByBatches.findAll().size();
						Integer increment=0;
						if(rowId!=0) {
							increment= rowId+1;
						}else {
							increment=1;
						}
						
						buildPayrollCheckByBatches.setRowId(increment);
						buildPayrollCheckByBatches.setBatches(batches);
						buildPayrollCheckByBatches.setBuildChecks(buildChecks);
						repositoryBuildPayrollCheckByBatches.saveAndFlush(buildPayrollCheckByBatches);
						if(batches.getStatus()==1) {
							
						}
						if(batches.getStatus()!=2) {
							batches.setStatus((byte)1);	
						}
						repositoryBatches.saveAndFlush(batches);
						
						List<TransactionEntry> trList = this.repositoryTransactionEntry.findByBatches(batches.getId());
						if(!trList.isEmpty()) {
							for (TransactionEntry transactionEntry1 : trList) {
								TransactionEntry transactionEntry= new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								Integer increment11=0;
								if(transactionEntry2 != null) {
							//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
							}else {
								increment11=1;
							}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(batches);
								if(transactionEntry2 != null) {
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								}else {
									transactionEntry.setEntryNumber(1);
								}
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription(transactionEntry1.getDescription());
								transactionEntry.setArabicDescription(transactionEntry1.getArabicDescription());
								transactionEntry.setFromBuild(false);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
								if(!transactionEntry1.getTransDetailList().isEmpty()) {
									for (TransactionEntryDetail detailList : transactionEntry1.getTransDetailList()) {
										
										if(detailList.getCode()!=null) {
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											if(transactionEntryDetailLastRecord!=null) {
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											}else {
												transactionEntryDetail.setRowId(1);
											}
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(false);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setUpdatedRow(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
											if(detailList.getDeductionCode() != null) {
												transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
											}
											if(detailList.getDimensions() != null) {
												transactionEntryDetail.setDimensions(detailList.getDimensions());
											}
											transactionEntryDetail.setDepartment(detailList.getDepartment());
											transactionEntryDetail.setAmount(detailList.getAmount());
											transactionEntryDetail.setPayRate(detailList.getPayRate());
											transactionEntryDetail.setToDate(detailList.getToDate());
											transactionEntryDetail.setFromDate(detailList.getFromDate());
											transactionEntryDetail.setCreatedDate(detailList.getCreatedDate());
											transactionEntryDetail.setUpdatedDate(detailList.getUpdatedDate());
											transactionEntryDetail.setFromTranscationEntry(false);
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											
										}
										
										
										if(detailList.getBenefitCode()!=null) {
											List<TransactionEntryDetail> traDetail = repositoryTransactionEntryDetail.searchByBatchId(batches.getId());
											if(!traDetail.isEmpty()) {
												for (TransactionEntryDetail transactionEntryDetail : traDetail) {
													List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													if(!employeeBenefitMaintenanceList.isEmpty()) {
														for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
															if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
																	if(transactionEntryDetail.getBenefitCode().getId()== employeeBenefitMaintenance.getBenefitCode().getId()) {

																		if(transactionEntryDetail.getEmployeeMaster().getEmployeeIndexId()== employeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId()) {
																			TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																			TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																			if(transactionEntryDetailLastRecord!=null) {
																				transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																			}else {
																				transactionEntryDetail1.setRowId(1);
																			}
																			transactionEntryDetail1.setTransactionEntry(transactionEntry);
																			transactionEntryDetail1.setEntryNumber(transactionEntry.getRowId()+1);
																			transactionEntryDetail1.setDetailsSequence(transactionEntry.getRowId()+1);
																			transactionEntryDetail1.setFromBuild(false);
																			transactionEntryDetail1.setTransactionType((short)1);
																			transactionEntryDetail1.setIsDeleted(false);
																			transactionEntryDetail1.setCreatedDate(new Date());
																			transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																			transactionEntryDetail1.setUpdatedDate(new Date());
																			transactionEntryDetail1.setUpdatedRow(new Date());
																			transactionEntryDetail1.setBuildChecks(buildChecks);
																			transactionEntryDetail1.setEmployeeMaster(transactionEntryDetail.getEmployeeMaster());
																			
																			transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																			if(detailList.getDeductionCode() != null) {
																				transactionEntryDetail1.setDeductionCode(detailList.getDeductionCode());
																			}
																			if(detailList.getCode() != null) {
																				transactionEntryDetail1.setCode(detailList.getCode());
																			}
																			if(detailList.getDimensions() != null) {
																				transactionEntryDetail1.setDimensions(detailList.getDimensions());
																			}
																			transactionEntryDetail1.setDepartment(detailList.getDepartment());
																			transactionEntryDetail1.setAmount(detailList.getAmount());
																			transactionEntryDetail1.setPayRate(detailList.getPayRate());
																			transactionEntryDetail1.setToDate(detailList.getToDate());
																			transactionEntryDetail1.setFromDate(detailList.getFromDate());
																			transactionEntryDetail1.setFromTranscationEntry(false);
																			repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																		
																		}
																		}
																		
																		
																}
															}
															
														}
													}
														
												}
											}
											
										
									
										}
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										if(detailList.getDeductionCode()!=null) {
											List<TransactionEntryDetail> traDetail = repositoryTransactionEntryDetail.searchByBatchId(batches.getId());
											if(!traDetail.isEmpty()) {
												for (TransactionEntryDetail transactionEntryDetail : traDetail) {
													List<EmployeeDeductionMaintenance> employeeDeductionMainList = repositoryEmployeeDeductionMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													if(!employeeDeductionMainList.isEmpty()) {
														for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMainList) {
															if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
																	if(transactionEntryDetail.getDeductionCode().getId()== employeeDeductionMaintenance.getDeductionCode().getId()) {
																		if(transactionEntryDetail.getEmployeeMaster().getEmployeeIndexId()== employeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId()) {
																			TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																			TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																			if(transactionEntryDetailLastRecord!=null) {
																				transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																			}else {
																				transactionEntryDetail1.setRowId(1);
																			}
																			transactionEntryDetail1.setTransactionEntry(transactionEntry);
																			transactionEntryDetail1.setEntryNumber(transactionEntry.getRowId()+1);
																			transactionEntryDetail1.setDetailsSequence(transactionEntry.getRowId()+1);
																			transactionEntryDetail1.setFromBuild(false);
																			transactionEntryDetail1.setTransactionType((short)1);
																			transactionEntryDetail1.setIsDeleted(false);
																			transactionEntryDetail1.setCreatedDate(new Date());
																			transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																			transactionEntryDetail1.setUpdatedDate(new Date());
																			transactionEntryDetail1.setUpdatedRow(new Date());
																			transactionEntryDetail1.setBuildChecks(buildChecks);
																			transactionEntryDetail1.setEmployeeMaster(transactionEntryDetail.getEmployeeMaster());
																			
																			transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																			if(detailList.getDeductionCode() != null) {
																				transactionEntryDetail1.setDeductionCode(detailList.getDeductionCode());
																			}
																			if(detailList.getCode() != null) {
																				transactionEntryDetail1.setCode(detailList.getCode());
																			}
																			if(detailList.getDimensions() != null) {
																				transactionEntryDetail1.setDimensions(detailList.getDimensions());
																			}
																			transactionEntryDetail1.setDepartment(detailList.getDepartment());
																			transactionEntryDetail1.setAmount(detailList.getAmount());
																			transactionEntryDetail1.setPayRate(detailList.getPayRate());
																			transactionEntryDetail1.setToDate(detailList.getToDate());
																			transactionEntryDetail1.setFromDate(detailList.getFromDate());
																			transactionEntryDetail1.setFromTranscationEntry(false);
																			repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																		
																		}
																		}
																		
																}
															}
															
														}
													}
														
												}
											}
											
											
									
										}
									
									}
								}
							}
						}
						
						
					}
					
				
					
				}
				
				List<Integer> ids = dtoBuildPayrollCheckByBatches.getBatches().stream().map(m->m.getId()).collect(Collectors.toList());
				
				if(!ids.isEmpty()) {
					List<Batches> listIds = repositoryBatches.getExculdeBatches(ids);
					List<Integer> listBatchesId = new ArrayList<>();
					for (Batches batches : listIds) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
							if(buildPayrollCheckByBatches.getBuildChecks().getId().equals(buildChecks.getId())) {
								
								listBatchesId.add(batches.getId());
							}
						}
						
					
					}
					
					if(!listBatchesId.isEmpty()) {
						repositoryBatches.updateExculdeBatches(listBatchesId);
					}
					
				}
				
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		return dtoBuildPayrollCheckByBatches;
	}
	
	
	
	public DtoBuildPayrollCheckByBatches saveOrUpdate123(DtoBuildPayrollCheckByBatches dtoBuildPayrollCheckByBatches) {
		try {
			

			
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			
			/*if(dtoBuildPayrollCheckByBatches.getBuildChecks().getId()!=null) {
				List<TransactionEntryDetail> tranEntryDetail = repositoryTransactionEntryDetail.findByBuildChecks(dtoBuildPayrollCheckByBatches.getBuildChecks().getId());
				for (TransactionEntryDetail transactionEntryDetail : tranEntryDetail) {
					if(transactionEntryDetail!=null) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
					
				}
			}*/
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByBatches.getListBuildChecks() != null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());	
				
			}
			
			
			
			
			
				if(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
					repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());
					}
				
				for (DtoBatches  dtobatches : dtoBuildPayrollCheckByBatches.getBatches()) {
					

					
					List<Batches> batches=repositoryBatches.getIncludeBatches12(dtobatches.getId());
					
					if(batches!=null) {
						for (Batches batches2 : batches) {

							
							
							BuildPayrollCheckByBatches buildPayrollCheckByBatches=new BuildPayrollCheckByBatches();
							buildPayrollCheckByBatches.setCreatedDate(new Date());
							Integer rowId = repositoryBuildPayrollCheckByBatches.findAll().size();
							Integer increment=0;
							if(rowId!=0) {
								increment= rowId+1;
							}else {
								increment=1;
							}
							
							buildPayrollCheckByBatches.setRowId(increment);
							buildPayrollCheckByBatches.setBatches(batches2);
							buildPayrollCheckByBatches.setBuildChecks(buildChecks);
							repositoryBuildPayrollCheckByBatches.saveAndFlush(buildPayrollCheckByBatches);
							if(batches2.getStatus()==1) {
								
							}
							if(batches2.getStatus()!=2) {
								batches2.setStatus((byte)1);	
							}
							//repositoryBatches.saveAndFlush(batches2);
							
							List<TransactionEntry> trList = this.repositoryTransactionEntry.findByBatches1(batches2.getId());
							for (TransactionEntry transactionEntry1 : trList) {
								
							/*	TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								Integer increment11=0;
								if(transactionEntry2 != null) {
							//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
							}else {
								increment11=1;
							}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(batches2);
								if(transactionEntry2 != null) {
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								}else {
									transactionEntry.setEntryNumber(1);
								}
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription(transactionEntry1.getDescription());
								transactionEntry.setArabicDescription(transactionEntry1.getArabicDescription());
								transactionEntry.setFromBuild(false);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);*/
								if(!transactionEntry1.getTransDetailList().isEmpty()) {
									for (TransactionEntryDetail detailList : transactionEntry1.getTransDetailList()) {
										
										if(detailList.getCode()!=null) {
											List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance.findByEmployeeIds(detailList.getEmployeeMaster().getEmployeeIndexId());
											for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
												if(employeePayCodeMaintenance.getInactive()==false) {
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													if(transactionEntryDetailLastRecord!=null) {
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													}else {
														transactionEntryDetail.setRowId(1);
													}
													transactionEntryDetail.setTransactionEntry(transactionEntry1);
													transactionEntryDetail.setEntryNumber(transactionEntry1.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry1.getRowId()+1);
													transactionEntryDetail.setFromBuild(false);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
													transactionEntryDetail.setCode(detailList.getCode());
													if(detailList.getDeductionCode() != null) {
														transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
													}
													if(detailList.getDimensions() != null) {
														transactionEntryDetail.setDimensions(detailList.getDimensions());
													}
													transactionEntryDetail.setDepartment(detailList.getDepartment());
													transactionEntryDetail.setAmount(detailList.getAmount());
													transactionEntryDetail.setPayRate(detailList.getPayRate());
													transactionEntryDetail.setToDate(detailList.getToDate());
													transactionEntryDetail.setFromDate(detailList.getFromDate());
													transactionEntryDetail.setCreatedDate(detailList.getCreatedDate());
													transactionEntryDetail.setUpdatedDate(detailList.getUpdatedDate());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
											
										}
										/*if(detailList.getBenefitCode() != null) {
											List<EmployeeBenefitMaintenance> employeeBenefitMainList = repositoryEmployeeBenefitMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());	
											//List<BenefitCode> benefitCodes= repositoryBenefitCode.findByStartEndDate1(buildChecks.getDateFrom(), buildChecks.getDateTo());
											if(employeeBenefitMainList!=null) {
												for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMainList) {
													if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
														if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
															TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
															TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
															if(transactionEntryDetailLastRecord!=null) {
																transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
															}else {
																transactionEntryDetail.setRowId(1);
															}
															transactionEntryDetail.setTransactionEntry(transactionEntry);
															transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
															transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
															transactionEntryDetail.setFromBuild(false);
															transactionEntryDetail.setTransactionType((short)1);
															transactionEntryDetail.setIsDeleted(false);
															transactionEntryDetail.setCreatedDate(new Date());
															transactionEntryDetail.setUpdatedBy(loggedInUserId);
															transactionEntryDetail.setUpdatedDate(new Date());
															transactionEntryDetail.setUpdatedRow(new Date());
															transactionEntryDetail.setBuildChecks(buildChecks);
															transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
															
															transactionEntryDetail.setBenefitCode(detailList.getBenefitCode());
															if(detailList.getDeductionCode() != null) {
																transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
															}
															if(detailList.getCode() != null) {
																transactionEntryDetail.setCode(detailList.getCode());
															}
															if(detailList.getDimensions() != null) {
																transactionEntryDetail.setDimensions(detailList.getDimensions());
															}
															transactionEntryDetail.setDepartment(detailList.getDepartment());
															transactionEntryDetail.setAmount(detailList.getAmount());
															transactionEntryDetail.setPayRate(detailList.getPayRate());
															transactionEntryDetail.setToDate(detailList.getToDate());
															transactionEntryDetail.setFromDate(detailList.getFromDate());
															repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
															
														}
													}
												}
												
											}
									
										}*/
										
										
										if(detailList.getBenefitCode()!=null) {
												//	List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance.findByEmployeeId(detailList.getEmployeeMaster().getEmployeeIndexId());
													if(!employeeBenefitMaintenanceList.isEmpty()) {
														for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
															if(employeeBenefitMaintenance.getStartDate()!=null && employeeBenefitMaintenance.getEndDate()!=null) {
																if(buildChecks.getDateFrom().getYear()+1900<=employeeBenefitMaintenance.getStartDate().getYear()+1900 || buildChecks.getDateTo().getYear()+1900<=employeeBenefitMaintenance.getEndDate().getYear()+1900) {
																	if(buildChecks.getDateFrom().getMonth()+1>=employeeBenefitMaintenance.getStartDate().getMonth()+1) {
																		if(employeeBenefitMaintenance.getEndDate().getMonth()+1>=buildChecks.getDateTo().getMonth()+1) {
																			
																			if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																				if(employeeBenefitMaintenance.getInactive()==false) {
																					if(detailList.getBenefitCode()!=null) {
																						if(detailList.getBenefitCode().isInActive()==false) {
																							if(detailList.getBenefitCode().getId()== employeeBenefitMaintenance.getBenefitCode().getId()) {

																								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																								TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																								if(transactionEntryDetailLastRecord!=null) {
																									transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																								}else {
																									transactionEntryDetail1.setRowId(1);
																								}
																								transactionEntryDetail1.setTransactionEntry(transactionEntry1);
																								transactionEntryDetail1.setEntryNumber(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setDetailsSequence(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setFromBuild(false);
																								transactionEntryDetail1.setTransactionType((short)1);
																								transactionEntryDetail1.setIsDeleted(false);
																								transactionEntryDetail1.setCreatedDate(new Date());
																								transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																								transactionEntryDetail1.setUpdatedDate(new Date());
																								transactionEntryDetail1.setUpdatedRow(new Date());
																								transactionEntryDetail1.setBuildChecks(buildChecks);
																								transactionEntryDetail1.setEmployeeMaster(detailList.getEmployeeMaster());
																								
																								transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																								
																								if(detailList.getDimensions() != null) {
																									transactionEntryDetail1.setDimensions(detailList.getDimensions());
																								}
																								transactionEntryDetail1.setDepartment(detailList.getDepartment());
																								transactionEntryDetail1.setAmount(detailList.getAmount());
																								transactionEntryDetail1.setPayRate(detailList.getPayRate());
																								transactionEntryDetail1.setToDate(detailList.getToDate());
																								transactionEntryDetail1.setFromDate(detailList.getFromDate());
																								transactionEntryDetail1.setFromTranscationEntry(false);
																								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																							
																							}
																						}
																						
																					}
																					}
																					
																			}
																		}
																	}
																	
																		
																		
																		
																		
																	
																}
																
																
															}
															
															
														}
													}
														
											
										
									
										}
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										if(detailList.getDeductionCode()!=null) {
													//List<EmployeeDeductionMaintenance> employeeDeductionMainList = repositoryEmployeeDeductionMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													List<EmployeeDeductionMaintenance> employeeDeductionMainList = repositoryEmployeeDeductionMaintenance.findByEmployeeId(detailList.getEmployeeMaster().getEmployeeIndexId());
													if(!employeeDeductionMainList.isEmpty()) {
														for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMainList) {
															if(employeeDeductionMaintenance.getStartDate()!=null && employeeDeductionMaintenance.getEndDate()!=null) {
																if(buildChecks.getDateFrom().getYear()+1900<=employeeDeductionMaintenance.getStartDate().getYear()+1900 || buildChecks.getDateTo().getYear()+1900<=employeeDeductionMaintenance.getEndDate().getYear()+1900) {
																	if(buildChecks.getDateFrom().getMonth()+1>=employeeDeductionMaintenance.getStartDate().getMonth()+1) {
																		if(employeeDeductionMaintenance.getEndDate().getMonth()+1>=buildChecks.getDateTo().getMonth()+1) {
																			if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																				if(employeeDeductionMaintenance.getInactive()==false) {
																					if(detailList.getDeductionCode()!=null) {
																						if(detailList.getDeductionCode().isInActive()==false) {
																							if(detailList.getDeductionCode().getId()== employeeDeductionMaintenance.getDeductionCode().getId()) {
																								
																								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																								TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																								if(transactionEntryDetailLastRecord!=null) {
																									transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																								}else {
																									transactionEntryDetail1.setRowId(1);
																								}
																								transactionEntryDetail1.setTransactionEntry(transactionEntry1);
																								transactionEntryDetail1.setEntryNumber(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setDetailsSequence(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setFromBuild(false);
																								transactionEntryDetail1.setTransactionType((short)1);
																								transactionEntryDetail1.setIsDeleted(false);
																								transactionEntryDetail1.setCreatedDate(new Date());
																								transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																								transactionEntryDetail1.setUpdatedDate(new Date());
																								transactionEntryDetail1.setUpdatedRow(new Date());
																								transactionEntryDetail1.setBuildChecks(buildChecks);
																								transactionEntryDetail1.setEmployeeMaster(detailList.getEmployeeMaster());
																								
																								//transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																								if(detailList.getDeductionCode() != null) {
																									transactionEntryDetail1.setDeductionCode(detailList.getDeductionCode());
																								}
																								/*if(detailList.getCode() != null) {
																									transactionEntryDetail1.setCode(detailList.getCode());
																								}*/
																								if(detailList.getDimensions() != null) {
																									transactionEntryDetail1.setDimensions(detailList.getDimensions());
																								}
																								transactionEntryDetail1.setDepartment(detailList.getDepartment());
																								transactionEntryDetail1.setAmount(detailList.getAmount());
																								transactionEntryDetail1.setPayRate(detailList.getPayRate());
																								transactionEntryDetail1.setToDate(detailList.getToDate());
																								transactionEntryDetail1.setFromDate(detailList.getFromDate());
																								transactionEntryDetail1.setFromTranscationEntry(false);
																								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																							
																							}
																						}
																						
																					}
																					
																				}
																			}
																		}
																	}
																	
																		
																			
																		
																		
																}
																
															}
															
															
														}
													}
														
											
									
										}
										/*TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										if(transactionEntryDetailLastRecord!=null) {
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										}else {
											transactionEntryDetail.setRowId(1);
										}
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(false);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
										
										
										if(detailList.getDeductionCode() != null) {
											transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
										}
										if(detailList.getCode() != null) {
											transactionEntryDetail.setCode(detailList.getCode());
										}
										if(detailList.getDimensions() != null) {
											transactionEntryDetail.setDimensions(detailList.getDimensions());
										}
										transactionEntryDetail.setDepartment(detailList.getDepartment());
										transactionEntryDetail.setAmount(detailList.getAmount());
										transactionEntryDetail.setPayRate(detailList.getPayRate());
										transactionEntryDetail.setToDate(detailList.getToDate());
										transactionEntryDetail.setFromDate(detailList.getFromDate());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
									}
								}
							}
							/*TransactionEntry transactionEntry=null;
							transactionEntry = new TransactionEntry();
							transactionEntry.setCreatedDate(new Date());
							
							TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
						//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
							Integer increment11=0;
							if(transactionEntry2.getRowId()!=0) {
								increment11= transactionEntry2.getRowId()+1;
							}else {
								increment11=1;
							}
							transactionEntry.setRowId(increment11);
							transactionEntry.setBatches(batches);
							transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
							transactionEntry.setEntryDate(new Date());
							transactionEntry.setFromDate(new Date());
							transactionEntry.setToDate(new Date());
							transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK BATCH");
							transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK BATCH");
							transactionEntry.setFromBuild(false);
							transactionEntry.setIsDeleted(false);.
							transactionEntry.setUpdatedRow(new Date());
							transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
							TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
							TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
							transactionEntryDetail.setTransactionEntry(transactionEntry);
							transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
							transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
							transactionEntryDetail.setFromBuild(false);
							transactionEntryDetail.setTransactionType((short)1);
							transactionEntryDetail.setIsDeleted(false);
							transactionEntryDetail.setCreatedDate(new Date());
							transactionEntryDetail.setUpdatedBy(loggedInUserId);
							transactionEntryDetail.setUpdatedDate(new Date());
							transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
							transactionEntryDetail.setUpdatedRow(new Date());
							transactionEntryDetail.setBuildChecks(buildChecks);
							transactionEntryDetail.setBatches(batches);
							repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
						}
					
					}
					
				
					
				}
				
				List<Integer> ids = dtoBuildPayrollCheckByBatches.getBatches().stream().map(m->m.getId()).collect(Collectors.toList());
				
				if(!ids.isEmpty()) {
					List<Batches> listIds = repositoryBatches.getExculdeBatches(ids);
					List<Integer> listBatchesId = new ArrayList<>();
					for (Batches batches : listIds) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
							if(buildPayrollCheckByBatches.getBuildChecks().getId().equals(buildChecks.getId())) {
								
								listBatchesId.add(batches.getId());
							}
						}
						
						
					}
					
					if(!listBatchesId.isEmpty()) {
						repositoryBatches.updateExculdeBatches(listBatchesId);
					}
					
					System.out.println(listIds);
				}
				
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		return dtoBuildPayrollCheckByBatches;
	}

	public DtoBuildPayrollCheckByBatches saveOrUpdateFinal(DtoBuildPayrollCheckByBatches dtoBuildPayrollCheckByBatches) {
		try {
			

			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			
			/*if(dtoBuildPayrollCheckByBatches.getBuildChecks().getId()!=null) {
				List<TransactionEntryDetail> tranEntryDetail = repositoryTransactionEntryDetail.findByBuildChecks(dtoBuildPayrollCheckByBatches.getBuildChecks().getId());
				for (TransactionEntryDetail transactionEntryDetail : tranEntryDetail) {
					if(transactionEntryDetail!=null) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
					
				}
			}*/
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByBatches.getListBuildChecks() != null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());	
				
			}
			
			
			
			
			
				if(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
					repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());
					}
				
				for (DtoBatches  dtobatches : dtoBuildPayrollCheckByBatches.getBatches()) {
					

					
					List<Batches> batches=repositoryBatches.getIncludeBatches12(dtobatches.getId());
					
					if(batches!=null) {
						for (Batches batches2 : batches) {

							
							BuildPayrollCheckByBatches buildPayrollCheckByBatches=new BuildPayrollCheckByBatches();
							buildPayrollCheckByBatches.setCreatedDate(new Date());
							Integer rowId = repositoryBuildPayrollCheckByBatches.findAll().size();
							Integer increment=0;
							if(rowId!=0) {
								increment= rowId+1;
							}else {
								increment=1;
							}
							
							buildPayrollCheckByBatches.setRowId(increment);
							buildPayrollCheckByBatches.setBatches(batches2);
							buildPayrollCheckByBatches.setBuildChecks(buildChecks);
							repositoryBuildPayrollCheckByBatches.saveAndFlush(buildPayrollCheckByBatches);
							if(batches2.getStatus()==1) {
								
							}
							if(batches2.getStatus()!=2) {
								batches2.setStatus((byte)1);	
							}
							//repositoryBatches.saveAndFlush(batches2);
							
							List<TransactionEntry> trList = this.repositoryTransactionEntry.findByBatches1(batches2.getId());
							for (TransactionEntry transactionEntry1 : trList) {
								
							/*	TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								Integer increment11=0;
								if(transactionEntry2 != null) {
							//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
							}else {
								increment11=1;
							}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(batches2);
								if(transactionEntry2 != null) {
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								}else {
									transactionEntry.setEntryNumber(1);
								}
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription(transactionEntry1.getDescription());
								transactionEntry.setArabicDescription(transactionEntry1.getArabicDescription());
								transactionEntry.setFromBuild(false);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);*/
								if(!transactionEntry1.getTransDetailList().isEmpty()) {
									for (TransactionEntryDetail detailList : transactionEntry1.getTransDetailList()) {
										
										if(detailList.getCode()!=null) {
											List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance.findByEmployeeIds(detailList.getEmployeeMaster().getEmployeeIndexId());
											for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
												if(employeePayCodeMaintenance.getInactive()==false) {
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													if(transactionEntryDetailLastRecord!=null) {
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													}else {
														transactionEntryDetail.setRowId(1);
													}
													transactionEntryDetail.setTransactionEntry(transactionEntry1);
													transactionEntryDetail.setEntryNumber(transactionEntry1.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry1.getRowId()+1);
													transactionEntryDetail.setFromBuild(false);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
													transactionEntryDetail.setCode(detailList.getCode());
													if(detailList.getDeductionCode() != null) {
														transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
													}
													if(detailList.getDimensions() != null) {
														transactionEntryDetail.setDimensions(detailList.getDimensions());
													}
													transactionEntryDetail.setDepartment(detailList.getDepartment());
													transactionEntryDetail.setAmount(detailList.getAmount());
													transactionEntryDetail.setPayRate(detailList.getPayRate());
													transactionEntryDetail.setToDate(detailList.getToDate());
													transactionEntryDetail.setFromDate(detailList.getFromDate());
													transactionEntryDetail.setCreatedDate(detailList.getCreatedDate());
													transactionEntryDetail.setUpdatedDate(detailList.getUpdatedDate());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
											
										}
										/*if(detailList.getBenefitCode() != null) {
											List<EmployeeBenefitMaintenance> employeeBenefitMainList = repositoryEmployeeBenefitMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());	
											//List<BenefitCode> benefitCodes= repositoryBenefitCode.findByStartEndDate1(buildChecks.getDateFrom(), buildChecks.getDateTo());
											if(employeeBenefitMainList!=null) {
												for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMainList) {
													if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
														if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
															TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
															TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
															if(transactionEntryDetailLastRecord!=null) {
																transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
															}else {
																transactionEntryDetail.setRowId(1);
															}
															transactionEntryDetail.setTransactionEntry(transactionEntry);
															transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
															transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
															transactionEntryDetail.setFromBuild(false);
															transactionEntryDetail.setTransactionType((short)1);
															transactionEntryDetail.setIsDeleted(false);
															transactionEntryDetail.setCreatedDate(new Date());
															transactionEntryDetail.setUpdatedBy(loggedInUserId);
															transactionEntryDetail.setUpdatedDate(new Date());
															transactionEntryDetail.setUpdatedRow(new Date());
															transactionEntryDetail.setBuildChecks(buildChecks);
															transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
															
															transactionEntryDetail.setBenefitCode(detailList.getBenefitCode());
															if(detailList.getDeductionCode() != null) {
																transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
															}
															if(detailList.getCode() != null) {
																transactionEntryDetail.setCode(detailList.getCode());
															}
															if(detailList.getDimensions() != null) {
																transactionEntryDetail.setDimensions(detailList.getDimensions());
															}
															transactionEntryDetail.setDepartment(detailList.getDepartment());
															transactionEntryDetail.setAmount(detailList.getAmount());
															transactionEntryDetail.setPayRate(detailList.getPayRate());
															transactionEntryDetail.setToDate(detailList.getToDate());
															transactionEntryDetail.setFromDate(detailList.getFromDate());
															repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
															
														}
													}
												}
												
											}
									
										}*/
										
										
										if(detailList.getBenefitCode()!=null) {
													List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance.findByEmployeeId3(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													//List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance.findByEmployeeId(detailList.getEmployeeMaster().getEmployeeIndexId());
													if(!employeeBenefitMaintenanceList.isEmpty()) {
														for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
																			if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																				if(employeeBenefitMaintenance.getInactive()==false) {
																					if(detailList.getBenefitCode()!=null) {
																						if(detailList.getBenefitCode().isInActive()==false) {
																							if(detailList.getBenefitCode().getId()== employeeBenefitMaintenance.getBenefitCode().getId()) {

																								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																								TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																								if(transactionEntryDetailLastRecord!=null) {
																									transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																								}else {
																									transactionEntryDetail1.setRowId(1);
																								}
																								transactionEntryDetail1.setTransactionEntry(transactionEntry1);
																								transactionEntryDetail1.setEntryNumber(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setDetailsSequence(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setFromBuild(false);
																								transactionEntryDetail1.setTransactionType((short)1);
																								transactionEntryDetail1.setIsDeleted(false);
																								transactionEntryDetail1.setCreatedDate(new Date());
																								transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																								transactionEntryDetail1.setUpdatedDate(new Date());
																								transactionEntryDetail1.setUpdatedRow(new Date());
																								transactionEntryDetail1.setBuildChecks(buildChecks);
																								transactionEntryDetail1.setEmployeeMaster(detailList.getEmployeeMaster());
																								
																								transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																								
																								if(detailList.getDimensions() != null) {
																									transactionEntryDetail1.setDimensions(detailList.getDimensions());
																								}
																								transactionEntryDetail1.setDepartment(detailList.getDepartment());
																								transactionEntryDetail1.setAmount(detailList.getAmount());
																								transactionEntryDetail1.setPayRate(detailList.getPayRate());
																								transactionEntryDetail1.setToDate(detailList.getToDate());
																								transactionEntryDetail1.setFromDate(detailList.getFromDate());
																								transactionEntryDetail1.setFromTranscationEntry(false);
																								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																							
																							}
																						}
																						
																					}
																					}
																					
																			}
																	
																		
																		
																		
																		
																	
																
																
															
															
														}
													}
														
											
										
									
										}
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										if(detailList.getDeductionCode()!=null) {
													List<EmployeeDeductionMaintenance> employeeDeductionMainList = repositoryEmployeeDeductionMaintenance.findByEmployeeId3(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													//List<EmployeeDeductionMaintenance> employeeDeductionMainList = repositoryEmployeeDeductionMaintenance.findByEmployeeId(detailList.getEmployeeMaster().getEmployeeIndexId());
													if(!employeeDeductionMainList.isEmpty()) {
														for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMainList) {
																			if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																				if(employeeDeductionMaintenance.getInactive()==false) {
																					if(detailList.getDeductionCode()!=null) {
																						if(detailList.getDeductionCode().isInActive()==false) {
																							if(detailList.getDeductionCode().getId()== employeeDeductionMaintenance.getDeductionCode().getId()) {
																								
																								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																								TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																								if(transactionEntryDetailLastRecord!=null) {
																									transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																								}else {
																									transactionEntryDetail1.setRowId(1);
																								}
																								transactionEntryDetail1.setTransactionEntry(transactionEntry1);
																								transactionEntryDetail1.setEntryNumber(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setDetailsSequence(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setFromBuild(false);
																								transactionEntryDetail1.setTransactionType((short)1);
																								transactionEntryDetail1.setIsDeleted(false);
																								transactionEntryDetail1.setCreatedDate(new Date());
																								transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																								transactionEntryDetail1.setUpdatedDate(new Date());
																								transactionEntryDetail1.setUpdatedRow(new Date());
																								transactionEntryDetail1.setBuildChecks(buildChecks);
																								transactionEntryDetail1.setEmployeeMaster(detailList.getEmployeeMaster());
																								
																								//transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																								if(detailList.getDeductionCode() != null) {
																									transactionEntryDetail1.setDeductionCode(detailList.getDeductionCode());
																								}
																								/*if(detailList.getCode() != null) {
																									transactionEntryDetail1.setCode(detailList.getCode());
																								}*/
																								if(detailList.getDimensions() != null) {
																									transactionEntryDetail1.setDimensions(detailList.getDimensions());
																								}
																								transactionEntryDetail1.setDepartment(detailList.getDepartment());
																								transactionEntryDetail1.setAmount(detailList.getAmount());
																								transactionEntryDetail1.setPayRate(detailList.getPayRate());
																								transactionEntryDetail1.setToDate(detailList.getToDate());
																								transactionEntryDetail1.setFromDate(detailList.getFromDate());
																								transactionEntryDetail1.setFromTranscationEntry(false);
																								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																							
																							}
																						}
																						
																					}
																					
																				}
																			}
																	
																		
																			
																		
																		
																
															
															
														}
													}
														
											
									
										}
										/*TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										if(transactionEntryDetailLastRecord!=null) {
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										}else {
											transactionEntryDetail.setRowId(1);
										}
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(false);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
										
										
										if(detailList.getDeductionCode() != null) {
											transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
										}
										if(detailList.getCode() != null) {
											transactionEntryDetail.setCode(detailList.getCode());
										}
										if(detailList.getDimensions() != null) {
											transactionEntryDetail.setDimensions(detailList.getDimensions());
										}
										transactionEntryDetail.setDepartment(detailList.getDepartment());
										transactionEntryDetail.setAmount(detailList.getAmount());
										transactionEntryDetail.setPayRate(detailList.getPayRate());
										transactionEntryDetail.setToDate(detailList.getToDate());
										transactionEntryDetail.setFromDate(detailList.getFromDate());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
									}
								}
							}
							/*TransactionEntry transactionEntry=null;
							transactionEntry = new TransactionEntry();
							transactionEntry.setCreatedDate(new Date());
							
							TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
						//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
							Integer increment11=0;
							if(transactionEntry2.getRowId()!=0) {
								increment11= transactionEntry2.getRowId()+1;
							}else {
								increment11=1;
							}
							transactionEntry.setRowId(increment11);
							transactionEntry.setBatches(batches);
							transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
							transactionEntry.setEntryDate(new Date());
							transactionEntry.setFromDate(new Date());
							transactionEntry.setToDate(new Date());
							transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK BATCH");
							transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK BATCH");
							transactionEntry.setFromBuild(false);
							transactionEntry.setIsDeleted(false);.
							transactionEntry.setUpdatedRow(new Date());
							transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
							TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
							TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
							transactionEntryDetail.setTransactionEntry(transactionEntry);
							transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
							transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
							transactionEntryDetail.setFromBuild(false);
							transactionEntryDetail.setTransactionType((short)1);
							transactionEntryDetail.setIsDeleted(false);
							transactionEntryDetail.setCreatedDate(new Date());
							transactionEntryDetail.setUpdatedBy(loggedInUserId);
							transactionEntryDetail.setUpdatedDate(new Date());
							transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
							transactionEntryDetail.setUpdatedRow(new Date());
							transactionEntryDetail.setBuildChecks(buildChecks);
							transactionEntryDetail.setBatches(batches);
							repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
						}
					
					}
					
				
					
				}
				
				List<Integer> ids = dtoBuildPayrollCheckByBatches.getBatches().stream().map(m->m.getId()).collect(Collectors.toList());
				
				if(!ids.isEmpty()) {
					List<Batches> listIds = repositoryBatches.getExculdeBatches(ids);
					List<Integer> listBatchesId = new ArrayList<>();
					for (Batches batches : listIds) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
							if(buildPayrollCheckByBatches.getBuildChecks().getId().equals(buildChecks.getId())) {
								
								listBatchesId.add(batches.getId());
							}
						}
						
						
					}
					
					if(!listBatchesId.isEmpty()) {
						repositoryBatches.updateExculdeBatches(listBatchesId);
					}
					
				}
				
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		return dtoBuildPayrollCheckByBatches;
	}
	
	
	
	public DtoBuildPayrollCheckByBatches saveOrUpdateFinalVersion(DtoBuildPayrollCheckByBatches dtoBuildPayrollCheckByBatches) {
		try {
			
			
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			
			/*if(dtoBuildPayrollCheckByBatches.getBuildChecks().getId()!=null) {
				List<TransactionEntryDetail> tranEntryDetail = repositoryTransactionEntryDetail.findByBuildChecks(dtoBuildPayrollCheckByBatches.getBuildChecks().getId());
				for (TransactionEntryDetail transactionEntryDetail : tranEntryDetail) {
					if(transactionEntryDetail!=null) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
					
				}
			}*/
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByBatches.getListBuildChecks() != null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());	
				
			}
			
			
			
			
			
				if(dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
					repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByBatches.getListBuildChecks().get(0).getBuildChecks().getId());
					
					}
				
				for (DtoBatches  dtobatches : dtoBuildPayrollCheckByBatches.getBatches()) {
					

					
					List<Batches> batches=repositoryBatches.getIncludeBatches12(dtobatches.getId());
					
					if(batches!=null) {
						for (Batches batches2 : batches) {
							if(batches2.getStatus()==3) {

							
							
							BuildPayrollCheckByBatches buildPayrollCheckByBatches=new BuildPayrollCheckByBatches();
							buildPayrollCheckByBatches.setCreatedDate(new Date());
							Integer rowId = repositoryBuildPayrollCheckByBatches.findAll().size();
							Integer increment=0;
							if(rowId!=0) {
								increment= rowId+1;
							}else {
								increment=1;
							}
							
							buildPayrollCheckByBatches.setRowId(increment);
							buildPayrollCheckByBatches.setBatches(batches2);
							buildPayrollCheckByBatches.setBuildChecks(buildChecks);
							repositoryBuildPayrollCheckByBatches.saveAndFlush(buildPayrollCheckByBatches);
							if(batches2.getStatus()==1) {
								
							}
							if(batches2.getStatus()!=2) {
								batches2.setStatus((byte)1);	
							}
							//repositoryBatches.saveAndFlush(batches2);
							
							List<TransactionEntry> trList = this.repositoryTransactionEntry.findByBatches1(batches2.getId());
							for (TransactionEntry transactionEntry1 : trList) {
								
							/*	TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								Integer increment11=0;
								if(transactionEntry2 != null) {
							//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
							}else {
								increment11=1;
							}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(batches2);
								if(transactionEntry2 != null) {
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								}else {
									transactionEntry.setEntryNumber(1);
								}
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription(transactionEntry1.getDescription());
								transactionEntry.setArabicDescription(transactionEntry1.getArabicDescription());
								transactionEntry.setFromBuild(false);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);*/
								if(!transactionEntry1.getTransDetailList().isEmpty()) {
									for (TransactionEntryDetail detailList : transactionEntry1.getTransDetailList()) {
										
										if(detailList.getCode()!=null) {
											List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance.findByEmployeeIds(detailList.getEmployeeMaster().getEmployeeIndexId());
											for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
												if(employeePayCodeMaintenance.getInactive()==false) {
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													if(transactionEntryDetailLastRecord!=null) {
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													}else {
														transactionEntryDetail.setRowId(1);
													}
													transactionEntryDetail.setTransactionEntry(transactionEntry1);
													transactionEntryDetail.setEntryNumber(transactionEntry1.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry1.getRowId()+1);
													transactionEntryDetail.setFromBuild(false);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
													transactionEntryDetail.setCode(detailList.getCode());
													if(detailList.getDeductionCode() != null) {
														transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
													}
													if(detailList.getDimensions() != null) {
														transactionEntryDetail.setDimensions(detailList.getDimensions());
													}
													transactionEntryDetail.setDepartment(detailList.getDepartment());
													transactionEntryDetail.setAmount(detailList.getAmount());
													transactionEntryDetail.setPayRate(detailList.getPayRate());
													transactionEntryDetail.setToDate(detailList.getToDate());
													transactionEntryDetail.setFromDate(detailList.getFromDate());
													transactionEntryDetail.setCreatedDate(detailList.getCreatedDate());
													transactionEntryDetail.setUpdatedDate(detailList.getUpdatedDate());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
											
										}
										/*if(detailList.getBenefitCode() != null) {
											List<EmployeeBenefitMaintenance> employeeBenefitMainList = repositoryEmployeeBenefitMaintenance.findByEmployeeId1(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());	
											//List<BenefitCode> benefitCodes= repositoryBenefitCode.findByStartEndDate1(buildChecks.getDateFrom(), buildChecks.getDateTo());
											if(employeeBenefitMainList!=null) {
												for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMainList) {
													if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
														if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
															TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
															TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
															if(transactionEntryDetailLastRecord!=null) {
																transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
															}else {
																transactionEntryDetail.setRowId(1);
															}
															transactionEntryDetail.setTransactionEntry(transactionEntry);
															transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
															transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
															transactionEntryDetail.setFromBuild(false);
															transactionEntryDetail.setTransactionType((short)1);
															transactionEntryDetail.setIsDeleted(false);
															transactionEntryDetail.setCreatedDate(new Date());
															transactionEntryDetail.setUpdatedBy(loggedInUserId);
															transactionEntryDetail.setUpdatedDate(new Date());
															transactionEntryDetail.setUpdatedRow(new Date());
															transactionEntryDetail.setBuildChecks(buildChecks);
															transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
															
															transactionEntryDetail.setBenefitCode(detailList.getBenefitCode());
															if(detailList.getDeductionCode() != null) {
																transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
															}
															if(detailList.getCode() != null) {
																transactionEntryDetail.setCode(detailList.getCode());
															}
															if(detailList.getDimensions() != null) {
																transactionEntryDetail.setDimensions(detailList.getDimensions());
															}
															transactionEntryDetail.setDepartment(detailList.getDepartment());
															transactionEntryDetail.setAmount(detailList.getAmount());
															transactionEntryDetail.setPayRate(detailList.getPayRate());
															transactionEntryDetail.setToDate(detailList.getToDate());
															transactionEntryDetail.setFromDate(detailList.getFromDate());
															repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
															
														}
													}
												}
												
											}
									
										}*/
										
										
										if(detailList.getBenefitCode()!=null) {
													List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance.findByEmployeeId3(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													//List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = repositoryEmployeeBenefitMaintenance.findByEmployeeId(detailList.getEmployeeMaster().getEmployeeIndexId());
													if(!employeeBenefitMaintenanceList.isEmpty()) {
														for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
																			if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																				if(employeeBenefitMaintenance.getInactive()==false) {
																					if(detailList.getBenefitCode()!=null) {
																						if(detailList.getBenefitCode().isInActive()==false) {
																							if(detailList.getBenefitCode().getId()== employeeBenefitMaintenance.getBenefitCode().getId()) {

																								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																								TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																								if(transactionEntryDetailLastRecord!=null) {
																									transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																								}else {
																									transactionEntryDetail1.setRowId(1);
																								}
																								transactionEntryDetail1.setTransactionEntry(transactionEntry1);
																								transactionEntryDetail1.setEntryNumber(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setDetailsSequence(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setFromBuild(false);
																								transactionEntryDetail1.setTransactionType((short)1);
																								transactionEntryDetail1.setIsDeleted(false);
																								transactionEntryDetail1.setCreatedDate(new Date());
																								transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																								transactionEntryDetail1.setUpdatedDate(new Date());
																								transactionEntryDetail1.setUpdatedRow(new Date());
																								transactionEntryDetail1.setBuildChecks(buildChecks);
																								transactionEntryDetail1.setEmployeeMaster(detailList.getEmployeeMaster());
																								
																								transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																								
																								if(detailList.getDimensions() != null) {
																									transactionEntryDetail1.setDimensions(detailList.getDimensions());
																								}
																								transactionEntryDetail1.setDepartment(detailList.getDepartment());
																								transactionEntryDetail1.setAmount(detailList.getAmount());
																								transactionEntryDetail1.setPayRate(detailList.getPayRate());
																								transactionEntryDetail1.setToDate(detailList.getToDate());
																								transactionEntryDetail1.setFromDate(detailList.getFromDate());
																								transactionEntryDetail1.setFromTranscationEntry(false);
																								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																							
																							}
																						}
																						
																					}
																					}
																					
																			}
																	
																		
																		
																		
																		
																	
																
																
															
															
														}
													}
														
											
										
									
										}
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										if(detailList.getDeductionCode()!=null) {
													List<EmployeeDeductionMaintenance> employeeDeductionMainList = repositoryEmployeeDeductionMaintenance.findByEmployeeId3(detailList.getEmployeeMaster().getEmployeeIndexId(),buildChecks.getDateFrom(), buildChecks.getDateTo());
													//List<EmployeeDeductionMaintenance> employeeDeductionMainList = repositoryEmployeeDeductionMaintenance.findByEmployeeId(detailList.getEmployeeMaster().getEmployeeIndexId());
													if(!employeeDeductionMainList.isEmpty()) {
														for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMainList) {
																			if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
																				if(employeeDeductionMaintenance.getInactive()==false) {
																					if(detailList.getDeductionCode()!=null) {
																						if(detailList.getDeductionCode().isInActive()==false) {
																							if(detailList.getDeductionCode().getId()== employeeDeductionMaintenance.getDeductionCode().getId()) {
																								
																								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
																								TransactionEntryDetail transactionEntryDetail1 = new TransactionEntryDetail();
																								if(transactionEntryDetailLastRecord!=null) {
																									transactionEntryDetail1.setRowId(transactionEntryDetailLastRecord.getId()+1);
																								}else {
																									transactionEntryDetail1.setRowId(1);
																								}
																								transactionEntryDetail1.setTransactionEntry(transactionEntry1);
																								transactionEntryDetail1.setEntryNumber(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setDetailsSequence(transactionEntry1.getRowId()+1);
																								transactionEntryDetail1.setFromBuild(false);
																								transactionEntryDetail1.setTransactionType((short)1);
																								transactionEntryDetail1.setIsDeleted(false);
																								transactionEntryDetail1.setCreatedDate(new Date());
																								transactionEntryDetail1.setUpdatedBy(loggedInUserId);
																								transactionEntryDetail1.setUpdatedDate(new Date());
																								transactionEntryDetail1.setUpdatedRow(new Date());
																								transactionEntryDetail1.setBuildChecks(buildChecks);
																								transactionEntryDetail1.setEmployeeMaster(detailList.getEmployeeMaster());
																								
																								//transactionEntryDetail1.setBenefitCode(detailList.getBenefitCode());
																								if(detailList.getDeductionCode() != null) {
																									transactionEntryDetail1.setDeductionCode(detailList.getDeductionCode());
																								}
																								/*if(detailList.getCode() != null) {
																									transactionEntryDetail1.setCode(detailList.getCode());
																								}*/
																								if(detailList.getDimensions() != null) {
																									transactionEntryDetail1.setDimensions(detailList.getDimensions());
																								}
																								transactionEntryDetail1.setDepartment(detailList.getDepartment());
																								transactionEntryDetail1.setAmount(detailList.getAmount());
																								transactionEntryDetail1.setPayRate(detailList.getPayRate());
																								transactionEntryDetail1.setToDate(detailList.getToDate());
																								transactionEntryDetail1.setFromDate(detailList.getFromDate());
																								transactionEntryDetail1.setFromTranscationEntry(false);
																								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail1);
																							
																							}
																						}
																						
																					}
																					
																				}
																			}
																	
																		
																			
																		
																		
																
															
															
														}
													}
														
											
									
										}
										/*TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										if(transactionEntryDetailLastRecord!=null) {
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										}else {
											transactionEntryDetail.setRowId(1);
										}
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(false);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setEmployeeMaster(detailList.getEmployeeMaster());
										
										
										if(detailList.getDeductionCode() != null) {
											transactionEntryDetail.setDeductionCode(detailList.getDeductionCode());
										}
										if(detailList.getCode() != null) {
											transactionEntryDetail.setCode(detailList.getCode());
										}
										if(detailList.getDimensions() != null) {
											transactionEntryDetail.setDimensions(detailList.getDimensions());
										}
										transactionEntryDetail.setDepartment(detailList.getDepartment());
										transactionEntryDetail.setAmount(detailList.getAmount());
										transactionEntryDetail.setPayRate(detailList.getPayRate());
										transactionEntryDetail.setToDate(detailList.getToDate());
										transactionEntryDetail.setFromDate(detailList.getFromDate());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
									}
								}
							}
							/*TransactionEntry transactionEntry=null;
							transactionEntry = new TransactionEntry();
							transactionEntry.setCreatedDate(new Date());
							
							TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
						//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
							Integer increment11=0;
							if(transactionEntry2.getRowId()!=0) {
								increment11= transactionEntry2.getRowId()+1;
							}else {
								increment11=1;
							}
							transactionEntry.setRowId(increment11);
							transactionEntry.setBatches(batches);
							transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
							transactionEntry.setEntryDate(new Date());
							transactionEntry.setFromDate(new Date());
							transactionEntry.setToDate(new Date());
							transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK BATCH");
							transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK BATCH");
							transactionEntry.setFromBuild(false);
							transactionEntry.setIsDeleted(false);.
							transactionEntry.setUpdatedRow(new Date());
							transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
							TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
							TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
							transactionEntryDetail.setTransactionEntry(transactionEntry);
							transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
							transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
							transactionEntryDetail.setFromBuild(false);
							transactionEntryDetail.setTransactionType((short)1);
							transactionEntryDetail.setIsDeleted(false);
							transactionEntryDetail.setCreatedDate(new Date());
							transactionEntryDetail.setUpdatedBy(loggedInUserId);
							transactionEntryDetail.setUpdatedDate(new Date());
							transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
							transactionEntryDetail.setUpdatedRow(new Date());
							transactionEntryDetail.setBuildChecks(buildChecks);
							transactionEntryDetail.setBatches(batches);
							repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);*/
							
								}
							
						}
					
					}
					
				
					
				}
				
				List<Integer> ids = dtoBuildPayrollCheckByBatches.getBatches().stream().map(m->m.getId()).collect(Collectors.toList());
				
				if(!ids.isEmpty()) {
					List<Batches> listIds = repositoryBatches.getExculdeBatches(ids);
					List<Integer> listBatchesId = new ArrayList<>();
					for (Batches batches : listIds) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : batches.getListBuildPayrollCheckByBatches()) {
							if(buildPayrollCheckByBatches.getBuildChecks().getId().equals(buildChecks.getId())) {
								
								listBatchesId.add(batches.getId());
							}
						}
						
						
					}
					
					if(!listBatchesId.isEmpty()) {
						repositoryBatches.updateExculdeBatches(listBatchesId);
					}
					
					System.out.println(listIds);
				}
				
			
		} catch (Exception e) {
			log.error(e);
		}
		
		
		return dtoBuildPayrollCheckByBatches;
	}


}
