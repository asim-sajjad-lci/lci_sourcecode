package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PositionPalnSetup;

@Repository("repositoryPositionPlanSetup")
public interface RepositoryPositionPlanSetup extends JpaRepository<PositionPalnSetup, Integer>{

	PositionPalnSetup findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from PositionPalnSetup s where s.isDeleted=false")
	Integer getCountOfTotalPosition();

	List<PositionPalnSetup> findByIsDeleted(boolean b, Pageable pageable);

	List<PositionPalnSetup> findByIsDeletedOrderByCreatedDateDesc(boolean b);
	
	public List<PositionPalnSetup> findByIsDeleted(Boolean deleted);
	
	public List<PositionPalnSetup> findByIsDeletedAndInactive(Boolean deleted, Boolean inActive);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PositionPalnSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSinglePosition(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select d from PositionPalnSetup d where d.positionPlanId =:positionPlanId and d.isDeleted=false")
	List<PositionPalnSetup> searchBypositionPlanId(@Param("positionPlanId") String positionPlanId);
	
	@Query("select s from PositionPalnSetup s   where s.position.id =:positionPlanId and s.isDeleted =false")
	List<PositionPalnSetup> findByAllPosionPlanPositionId(@Param("positionPlanId") Integer positionPlanId, Pageable pageable);

	@Query("select count(*) from PositionPalnSetup d where (d.positionPlanId like :searchKeyWord   or d.positionPlanDesc like :searchKeyWord  or d.positionPlanArbicDesc like :searchKeyWord) and d.isDeleted=false and d.position.id =:positionId")
	Integer predictivePositionPlanSearchTotalCount(@Param("searchKeyWord") String searchKeyWord, @Param("positionId") Integer positionId);

	@Query("select s from PositionPalnSetup s where (s.positionPlanId like :searchKeyWord    or s.positionPlanDesc like :searchKeyWord  or s.positionPlanArbicDesc like :searchKeyWord) and s.isDeleted=false and s.position.id =:positionId")
	List<PositionPalnSetup> predictivePositionPlanSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,@Param("positionId") Integer positionId, Pageable pageRequest);
	
	@Query("select count(*) from PositionPalnSetup d where (d.id =:searchKeyWord) and d.isDeleted=false")
    Integer predictivePositionPlanSearchTotalCount1(@Param("searchKeyWord") Integer searchKeyWord);

    @Query("select s from PositionPalnSetup s where (s.id =:searchKeyWord) and s.isDeleted=false")
    List<PositionPalnSetup> predictivePositionPlanSearchWithPagination1(@Param("searchKeyWord") Integer searchKeyWord);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update PositionPalnSetup d set d.inactive =:status ,d.updatedBy =:updateById where d.id =:id ")
    PositionPalnSetup changeStatus(@Param("status")Boolean isActive,@Param("id")Integer positionId);

    @Query("select d from PositionPalnSetup d where (d.positionPlanId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<PositionPalnSetup> predictivePositionPlanIdSearchWithPagination(@Param("searchKeyWord") String string);

    
    @Query("select count(*) from PositionPalnSetup p where (p.positionPlanId LIKE :searchKeyWord or p.positionPlanDesc LIKE :searchKeyWord  or p.positionPlanArbicDesc LIKE :searchKeyWord  or p.positionPlanStart LIKE :searchKeyWord  or p.positionPlanEnd LIKE :searchKeyWord) and p.isDeleted=false and p.position.id =:positionId")
	Integer predictivePositionPlanSearchTotalCount1(@Param("searchKeyWord") String searchKeyWord, @Param("positionId") Integer positionId);

    @Query("select p from PositionPalnSetup p where (p.positionPlanId LIKE :searchKeyWord or p.positionPlanDesc LIKE :searchKeyWord  or p.positionPlanArbicDesc LIKE :searchKeyWord  or p.positionPlanStart LIKE :searchKeyWord  or p.positionPlanEnd LIKE :searchKeyWord) and p.isDeleted=false")
	List<PositionPalnSetup> predictivePositionPlanSearchWithPagination1(@Param("searchKeyWord")String searchKeyWord, Pageable pageable); 
    
    @Query("select p from PositionPalnSetup p where (p.id =:id) and p.isDeleted=false")
   	PositionPalnSetup predictivePositionPlanByPosition(@Param("id") Integer id);

    @Query("select count(*) from PositionPalnSetup p ")
	Integer getCountOfTotaPositionPlanSetup(); 
}
