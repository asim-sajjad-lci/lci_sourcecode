package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.TraningCourseClassSkill;

public class DtoTraningCourseClassSkill  extends DtoBase{

	private Integer id;

	private DtoTraningCourse trainingCourse;
	private DtoTrainingCourseDetail trainingClass;

	private DtoSkillSetSteup skillSetSetup;
	private Integer skillSequnce;
	private List<DtoSkillSteup> skillsSetup;
	private String skillDesc;
	private String skillArabicDesc;
	private List<DtoTraningCourseClassSkill> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DtoTraningCourse getTrainingCourse() {
		return trainingCourse;
	}

	public void setTrainingCourse(DtoTraningCourse trainingCourse) {
		this.trainingCourse = trainingCourse;
	}

	public DtoTrainingCourseDetail getTrainingClass() {
		return trainingClass;
	}

	public void setTrainingClass(DtoTrainingCourseDetail trainingClass) {
		this.trainingClass = trainingClass;
	}

	public Integer getSkillSequnce() {
		return skillSequnce;
	}

	public void setSkillSequnce(Integer skillSequnce) {
		this.skillSequnce = skillSequnce;
	}

	public DtoSkillSetSteup getSkillSetSetup() {
		return skillSetSetup;
	}

	public void setSkillSetSetup(DtoSkillSetSteup skillSetSetup) {
		this.skillSetSetup = skillSetSetup;
	}

	public List<DtoSkillSteup> getSkillsSetup() {
		return skillsSetup;
	}

	public void setSkillsSetup(List<DtoSkillSteup> skillsSetup) {
		this.skillsSetup = skillsSetup;
	}

	public String getSkillDesc() {
		return skillDesc;
	}

	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}

	public String getSkillArabicDesc() {
		return skillArabicDesc;
	}

	public void setSkillArabicDesc(String skillArabicDesc) {
		this.skillArabicDesc = skillArabicDesc;
	}

	public List<DtoTraningCourseClassSkill> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoTraningCourseClassSkill> delete) {
		this.delete = delete;
	}

	public DtoTraningCourseClassSkill() {
	}

	public DtoTraningCourseClassSkill(TraningCourseClassSkill traningCourseClassSkill) {
	}
}
