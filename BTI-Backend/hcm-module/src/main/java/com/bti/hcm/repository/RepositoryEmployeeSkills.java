package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeSkills;

@Repository("repositoryEmployeeSkills")
public interface RepositoryEmployeeSkills extends JpaRepository<EmployeeSkills, Integer>{

	public EmployeeSkills findByIdAndIsDeleted(int id, boolean deleted);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeSkills d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleEmployeeSkills(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	@Query("select count(*) from EmployeeSkills d where d.employeeMaster.employeeIndexId =:id and d.isDeleted=false")
	public Integer findByEmployeeIdCount(@Param("id")Integer id);

	@Query("select d from EmployeeSkills d where d.employeeMaster.employeeIndexId =:id and d.isDeleted=false")
	public List<EmployeeSkills> findByEmployeeId(@Param("id")Integer id, Pageable pageRequest);

	
	@Query("select count(*) from EmployeeSkills d where  d.isDeleted=false")
	public Integer getAllCount();

	@Query("select d from EmployeeSkills d where  d.isDeleted=false")
	public List<EmployeeSkills> getAll(Pageable pageRequest);
	
	public List<EmployeeSkills> findByIsDeleted(boolean isdeleted);
	
	
	@Query("select d from EmployeeSkills d where d.employeeMaster.employeeIndexId =:id and  d.isDeleted=false")
	public EmployeeSkills findAllByEmployeeId(@Param("id")Integer id);
	
	
	@Query("select d from EmployeeSkills d where d.employeeMaster.employeeIndexId =:id or d.skillSetSetup.id=:id and d.isDeleted=false")
	public List<EmployeeSkills> findAllByEmployeeIdandSkillId(@Param("id")Integer id,@Param("id") Integer skillId);

}
