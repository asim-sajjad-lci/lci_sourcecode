package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.TransactionEntry;

public class DtoTransactionEntryDetail extends DtoBase {

	private Integer id;
	private TransactionEntry transactionEntry;
	private Integer entryNumber;
	private Integer codeId;
	private Integer detailsSequence;
	private DtoEmployeeMasterHcm employeeMaster;
	private short transactionType;
	private short viewBy;
	private DtoDepartment  department;
	private DtoPayCode code;
	private DtoBenefitCode benefitCode;
	private DtoDeductionCode deductionCode;
	private BigDecimal amount;
	private BigDecimal payRate;
	private Date fromDate;
	private Date toDate;
	private List<DtoTransactionEntryDetail> delete;
	private DtoFinancialDimensions dtoFinancialDimensions;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public TransactionEntry getTransactionEntry() {
		return transactionEntry;
	}
	public void setTransactionEntry(TransactionEntry transactionEntry) {
		this.transactionEntry = transactionEntry;
	}
	public Integer getEntryNumber() {
		return entryNumber;
	}
	public void setEntryNumber(Integer entryNumber) {
		this.entryNumber = entryNumber;
	}
	public Integer getDetailsSequence() {
		return detailsSequence;
	}
	public void setDetailsSequence(Integer detailsSequence) {
		this.detailsSequence = detailsSequence;
	}
	public DtoEmployeeMasterHcm getEmployeeMaster() {
		return employeeMaster;
	}
	public void setEmployeeMaster(DtoEmployeeMasterHcm employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	public short getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(short transactionType) {
		this.transactionType = transactionType;
	}
	
	public DtoDepartment getDepartment() {
		return department;
	}
	public void setDepartment(DtoDepartment department) {
		this.department = department;
	}
	public DtoPayCode getCode() {
		return code;
	}
	public void setCode(DtoPayCode code) {
		this.code = code;
	}
	public DtoBenefitCode getBenefitCode() {
		return benefitCode;
	}
	public void setBenefitCode(DtoBenefitCode benefitCode) {
		this.benefitCode = benefitCode;
	}
	public DtoDeductionCode getDeductionCode() {
		return deductionCode;
	}
	public void setDeductionCode(DtoDeductionCode deductionCode) {
		this.deductionCode = deductionCode;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getPayRate() {
		return payRate;
	}
	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Integer getCodeId() {
		return codeId;
	}
	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}
	public short getViewBy() {
		return viewBy;
	}
	public void setViewBy(short viewBy) {
		this.viewBy = viewBy;
	}
	public List<DtoTransactionEntryDetail> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoTransactionEntryDetail> delete) {
		this.delete = delete;
	}
	public DtoFinancialDimensions getDtoFinancialDimensions() {
		return dtoFinancialDimensions;
	}
	public void setDtoFinancialDimensions(DtoFinancialDimensions dtoFinancialDimensions) {
		this.dtoFinancialDimensions = dtoFinancialDimensions;
	}
	
}
