package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoMiscellaneousBenefirtEnrollment;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceMiscellaneousBenefirtEnrollment;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/miscellaneousBenefirtEnrollment")	
public class ControllerMiscellaneousBenefirtEnrollment extends BaseController{
	

	private static final Logger LOGGER = Logger.getLogger(ControllerMiscellaneousBenefirtEnrollment.class);
	
	@Autowired
	ServiceMiscellaneousBenefirtEnrollment serviceMiscellaneousBenefirtEnrollment;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;


	

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment) throws Exception {
		LOGGER.info("Create MiscellaneousBenefirtEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoMiscellaneousBenefirtEnrollment  = serviceMiscellaneousBenefirtEnrollment.saveOrUpdate(dtoMiscellaneousBenefirtEnrollment);
			responseMessage=displayMessage(dtoMiscellaneousBenefirtEnrollment, "MISLLENIUS_BENEFIT_ENROLLMENT_CREATED", "MISLLENIUS_BENEFIT_ENROLLMENT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create MiscellaneousBenefirtEnrollment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment) throws Exception {
		LOGGER.info("Update MiscellaneousBenefirtEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoMiscellaneousBenefirtEnrollment = serviceMiscellaneousBenefirtEnrollment.saveOrUpdate(dtoMiscellaneousBenefirtEnrollment);
			responseMessage=displayMessage(dtoMiscellaneousBenefirtEnrollment, "MISLLENIUS_BENEFIT_ENROLLMENT_UPDATED", "MISLLENIUS_BENEFIT_ENROLLMENT_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update MiscellaneousBenefirtEnrollment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment) throws Exception {
		LOGGER.info("Delete MiscellaneousBenefirtEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoMiscellaneousBenefirtEnrollment.getIds() != null && !dtoMiscellaneousBenefirtEnrollment.getIds().isEmpty()) {
				DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment2 = serviceMiscellaneousBenefirtEnrollment.delete(dtoMiscellaneousBenefirtEnrollment.getIds());
				if(dtoMiscellaneousBenefirtEnrollment2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("MISLLENIUS_BENEFIT_ENROLLMENT_DELETED", false), dtoMiscellaneousBenefirtEnrollment2);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("MISLLENIUS_BENEFIT_ENROLLMENT_NOT_DELETED", false), dtoMiscellaneousBenefirtEnrollment2);

				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("MISLLENIUS_BENEFIT_ENROLLMENT_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete MiscellaneousBenefirtEnrollment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollmentObj = serviceMiscellaneousBenefirtEnrollment.getById(dtoMiscellaneousBenefirtEnrollment.getId());
			responseMessage=displayMessage(dtoMiscellaneousBenefirtEnrollmentObj, "MISLLENIUS_BENEFIT_ENROLLMENT_LIST_GET_DETAIL", "MISLLENIUS_BENEFIT_ENROLLMENT_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoMiscellaneousBenefirtEnrollment.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeBenefitMaintenance(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search MiscellaneousBenefirtEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceMiscellaneousBenefirtEnrollment.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "MISLLENIUS_BENEFIT_ENROLLMENT_GET_ALL", "MISLLENIUS_BENEFIT_ENROLLMENT_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search MiscellaneousBenefirtEnrollment Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/searchByBenefitCode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchByBenefitCode(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search MiscellaneousBenefirtEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceMiscellaneousBenefirtEnrollment.searchByBenefitCode(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "MISLLENIUS_BENEFIT_ENROLLMENT_GET_ALL", "MISLLENIUS_BENEFIT_ENROLLMENT_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search MiscellaneousBenefirtEnrollment Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/miscellaneousBenEnrollIdCheck", method = RequestMethod.POST)
	public ResponseMessage miscellaneousBenEnrollIdCheck(HttpServletRequest request, @RequestBody DtoMiscellaneousBenefirtEnrollment dtoMiscellaneousBenefirtEnrollment) throws Exception {
		LOGGER.info("miscellaneousBenEnrollIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			DtoMiscellaneousBenefirtEnrollment dtoMiscEnroll = serviceMiscellaneousBenefirtEnrollment.repeatByEmployeeId(dtoMiscellaneousBenefirtEnrollment.getEmployeeMaster().getEmployeeIndexId());
			if (dtoMiscEnroll != null) {
				if (dtoMiscEnroll.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("MISCENROLL_EXIST", false), dtoMiscEnroll);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoMiscEnroll.getMessageType(), false),
							dtoMiscEnroll);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("MISCENROLL_REPEAT_MISCENROLL_NOT_FOUND", false), dtoMiscEnroll);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	
	

	
}
