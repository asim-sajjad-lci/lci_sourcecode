package com.bti.hcm.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Department;
import com.bti.hcm.model.Location;
import com.bti.hcm.model.PayrollAccounts;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.dto.DtoPayrollAccounts;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryLocation;
import com.bti.hcm.repository.RepositoryPayrollAccounts;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/servicePayrollAccounts")
public class ServicePayrollAccounts {
	

	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServicePayrollAccounts.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired(required=false)
	RepositoryDepartment repositoryDepartment;
	
	@Autowired(required=false)
	RepositoryPayrollAccounts repositoryPayrollAccounts;
	
	@Autowired(required=false)
	RepositoryLocation repositoryLocation;
	
	@Autowired(required=false)
	RepositoryPosition repositoryPosition;
	
	
		public DtoPayrollAccounts saveOrUpdate(DtoPayrollAccounts dtoPayrollAccounts) {
			try {
				int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
				PayrollAccounts payrollAccounts=null;
				if (dtoPayrollAccounts.getId() != null && dtoPayrollAccounts.getId() > 0) {
					payrollAccounts = repositoryPayrollAccounts.findByIdAndIsDeleted(dtoPayrollAccounts.getId(), false);
					payrollAccounts.setUpdatedBy(loggedInUserId);
					payrollAccounts.setUpdatedDate(new Date());
				} else {
					payrollAccounts = new PayrollAccounts();
					payrollAccounts.setCreatedDate(new Date());
					Integer rowId = repositoryPayrollAccounts.findAll().size();
					Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					payrollAccounts.setRowId(increment);
				}

				Department department=null;
				if(dtoPayrollAccounts.getDepartment()!=null && dtoPayrollAccounts.getDepartment().getId()>0) {
					department =repositoryDepartment.findOne(dtoPayrollAccounts.getDepartment().getId());
				}
				
				Location location=null;
				if(dtoPayrollAccounts.getLocation()!=null && dtoPayrollAccounts.getLocation().getId()>0) {
					location=repositoryLocation.findOne(dtoPayrollAccounts.getLocation().getId());
				}
				Position position=null;
				if(dtoPayrollAccounts.getPosition()!=null && dtoPayrollAccounts.getPosition().getId()>0) {
					position=repositoryPosition.findOne(dtoPayrollAccounts.getPosition().getId());
				}
				
				payrollAccounts.setPosition(position);
				payrollAccounts.setLocation(location);
				payrollAccounts.setDepartment(department);
				payrollAccounts.setAccountType(dtoPayrollAccounts.getAccountType());
				payrollAccounts.setPayrollCode(dtoPayrollAccounts.getPayrollCode());				
				payrollAccounts.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				repositoryPayrollAccounts.saveAndFlush(payrollAccounts);
			} catch (Exception e) {
				log.error(e);
			}
			return dtoPayrollAccounts;
		}
	}
