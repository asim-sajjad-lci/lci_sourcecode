package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10401")
public class ManualChecksDetails extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMPYMTINDXD")
	private Integer id;
	
	@Column(name = "HCMPYMTSEQ")
	private Integer transaction;
	
	@Column(name = "HCMPYMTYP")
	private Short transactiotype;
	
	@Column(name = "HCMCODINX")
	private Integer codeId;
	
	@Column(name = "HCMFDTA")
	private Date dateFrom;
	
	@Column(name = "HCMTDTA")
	private Date dateTo;
	
	@Column(name = "HCMAMNT")
	private BigDecimal totalAmount;

	@Column(name = "HCMAMNP")
	private BigDecimal totalPercent;
	
	
	@Column(name = "HCMHRS")
	private Integer hours;
	
	@Column(name = "HCMDYS")
	private Integer daysWorks;
	
	
	@Column(name = "HCMWKS")
	private Integer weeksWorks;

	@ManyToOne
	@JoinColumn(name = "DIMINXVALUE")
	private FinancialDimensionsValues financialDimensionsValues;
	
	@ManyToOne
	@JoinColumn(name="HCMPYMTINDX")
	private ManualChecks manualChecks;

	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getTransaction() {
		return transaction;
	}


	public void setTransaction(Integer transaction) {
		this.transaction = transaction;
	}


	public Short getTransactiotype() {
		return transactiotype;
	}


	public void setTransactiotype(Short transactiotype) {
		this.transactiotype = transactiotype;
	}


	public Integer getCodeId() {
		return codeId;
	}


	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}


	public Date getDateFrom() {
		return dateFrom;
	}


	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}


	public Date getDateTo() {
		return dateTo;
	}


	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}


	public BigDecimal getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}


	public BigDecimal getTotalPercent() {
		return totalPercent;
	}


	public void setTotalPercent(BigDecimal totalPercent) {
		this.totalPercent = totalPercent;
	}


	public Integer getHours() {
		return hours;
	}


	public void setHours(Integer hours) {
		this.hours = hours;
	}


	public Integer getDaysWorks() {
		return daysWorks;
	}


	public void setDaysWorks(Integer daysWorks) {
		this.daysWorks = daysWorks;
	}


	public Integer getWeeksWorks() {
		return weeksWorks;
	}


	public void setWeeksWorks(Integer weeksWorks) {
		this.weeksWorks = weeksWorks;
	}


	public FinancialDimensionsValues getFinancialDimensionsValues() {
		return financialDimensionsValues;
	}


	public void setFinancialDimensionsValues(FinancialDimensionsValues financialDimensionsValues) {
		this.financialDimensionsValues = financialDimensionsValues;
	}

	public ManualChecks getManualChecks() {
		return manualChecks;
	}


	public void setManualChecks(ManualChecks manualChecks) {
		this.manualChecks = manualChecks;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((codeId == null) ? 0 : codeId.hashCode());
		result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		result = prime * result + ((daysWorks == null) ? 0 : daysWorks.hashCode());
		result = prime * result + ((hours == null) ? 0 : hours.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((totalAmount == null) ? 0 : totalAmount.hashCode());
		result = prime * result + ((totalPercent == null) ? 0 : totalPercent.hashCode());
		result = prime * result + ((transaction == null) ? 0 : transaction.hashCode());
		result = prime * result + ((transactiotype == null) ? 0 : transactiotype.hashCode());
		result = prime * result + ((weeksWorks == null) ? 0 : weeksWorks.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ManualChecksDetails other = (ManualChecksDetails) obj;
		if (codeId == null) {
			if (other.codeId != null)
				return false;
		} else if (!codeId.equals(other.codeId))
			return false;
		if (dateFrom == null) {
			if (other.dateFrom != null)
				return false;
		} else if (!dateFrom.equals(other.dateFrom))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		if (daysWorks == null) {
			if (other.daysWorks != null)
				return false;
		} else if (!daysWorks.equals(other.daysWorks))
			return false;
		if (hours == null) {
			if (other.hours != null)
				return false;
		} else if (!hours.equals(other.hours))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (totalAmount == null) {
			if (other.totalAmount != null)
				return false;
		} else if (!totalAmount.equals(other.totalAmount))
			return false;
		if (totalPercent == null) {
			if (other.totalPercent != null)
				return false;
		} else if (!totalPercent.equals(other.totalPercent))
			return false;
		if (transaction == null) {
			if (other.transaction != null)
				return false;
		} else if (!transaction.equals(other.transaction))
			return false;
		if (transactiotype == null) {
			if (other.transactiotype != null)
				return false;
		} else if (!transactiotype.equals(other.transactiotype))
			return false;
		if (weeksWorks == null) {
			if (other.weeksWorks != null)
				return false;
		} else if (!weeksWorks.equals(other.weeksWorks))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "ManualChecksDetails [id=" + id + ", transaction=" + transaction + ", transactiotype=" + transactiotype
				+ ", codeId=" + codeId + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", totalAmount="
				+ totalAmount + ", totalPercent=" + totalPercent + ", hours=" + hours + ", daysWorks=" + daysWorks
				+ ", weeksWorks=" + weeksWorks + "]";
	}

}
