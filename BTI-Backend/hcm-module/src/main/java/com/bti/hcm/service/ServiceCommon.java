package com.bti.hcm.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.chrono.HijrahDate;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.dto.DtoDateConvertReq;
import com.bti.hcm.util.DateConversion;

@Service("serviceCommon")
public class ServiceCommon {

	/**
	 * @Description LOGGER use for put a logger in Department Service
	 */
	static Logger log = Logger.getLogger(ServiceDepartment.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in Department service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in Department service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in Department service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	public DtoDateConvertReq dateConverter(DtoDateConvertReq dtoDateRequest) throws ParseException {
	try {
		if (dtoDateRequest.getGregorianDate() != null) {
			String s = dtoDateRequest.getGregorianDate();
			Integer gregorianDay;
			Integer gregorianMonth;
			Integer gredorianYear;

			if (s.length() == 10) {
				gregorianDay = Integer.parseInt(s.substring(0, 2));
				gregorianMonth = Integer.parseInt(s.substring(3, 5));
				gredorianYear = Integer.parseInt(s.substring(6, 10));
			} else {
				gregorianDay = Integer.parseInt(s.substring(0, 2));
				gregorianMonth = Integer.parseInt(s.substring(4, 5));
				gredorianYear = Integer.parseInt(s.substring(6, 10));
			}
			System.out.println("gregorianDay: " + gregorianDay);
			System.out.println("gregorianMonth: " + gregorianMonth);
			System.out.println("gredorianYear: " + gredorianYear);
			HijrahDate hijriDate = DateConversion.convertGregorianToHijriDate(gregorianDay, gregorianMonth,
					gredorianYear);
			String hijriDateIs = hijriDate.toString();
			String finalHijriDate = hijriDateIs.substring(19, 29);
			System.out.println("Final HijriDate:-->" + finalHijriDate);
			// dtoDateRequest.setHijriDate(finalHijriDate);
			dtoDateRequest.setConvertToHijri(finalHijriDate);

		}

		if (dtoDateRequest.getHijriDate() != null && dtoDateRequest.getHijriDate() != "") {
			String h = dtoDateRequest.getHijriDate();
			Integer hijriYear;
			Integer hijriMonth;
			Integer hijriDay;
			if (h.length() == 10) {
				System.out.println("length: " + h.length());
				hijriYear = Integer.parseInt(h.substring(0, 4));
				hijriMonth = Integer.parseInt(h.substring(5, 7));
				hijriDay = Integer.parseInt(h.substring(8, 10));
			} else if (h.length() == 9) {
				System.out.println("length: " + h.length());
				hijriYear = Integer.parseInt(h.substring(0, 4));
				hijriMonth = Integer.parseInt(h.substring(5, 6));
				hijriDay = Integer.parseInt(h.substring(7, 9));
			} else {
				System.out.println("length: " + h.length());
				hijriYear = Integer.parseInt(h.substring(0, 4));
				hijriMonth = Integer.parseInt(h.substring(5, 6));
				hijriDay = Integer.parseInt(h.substring(7, 8));
			}
			System.out.println("hijriYear: " + hijriYear);
			System.out.println("hijriMonth: " + hijriMonth);
			System.out.println("hijriDay: " + hijriDay);
			LocalDate gregorianDate = DateConversion.convertHijriDateToGregorian(hijriYear, hijriMonth, hijriDay);
			Date localToDate = Date.from(gregorianDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
			dtoDateRequest.setConvertToGregorian(gregorianDate.toString());
			System.out.println("local Date:-->" + localToDate);
		}
	} catch (Exception e) {
		log.error(e);
	}

		return dtoDateRequest;
	}

}
