package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Attendance;
import com.bti.hcm.model.dto.DtoAtteandace;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAtteandace;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceAtteandace")
public class ServiceAtteandace {
	/**
	 * @Description LOGGER use for put a logger in Atteandace Service
	 */
	static Logger log = Logger.getLogger(ServiceAtteandace.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in Atteandace service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in Atteandace service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in Atteandace service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryAtteandace Autowired here using annotation of spring for access of repositoryAtteandace method in Atteandace service
	 * 				In short Access Atteandace Query from Database using repositoryAtteandace.
	 */
	@Autowired
	RepositoryAtteandace repositoryAtteandace;

	public DtoAtteandace saveOrUpdateAtteandace(DtoAtteandace dtoAtteandace) {

		try {
			log.info("saveOrUpdateAtteandace Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Attendance attaeandace = null;
			if (dtoAtteandace.getId() != null && dtoAtteandace.getId() > 0) {
				attaeandace = repositoryAtteandace.findByIdAndIsDeleted(dtoAtteandace.getId(), false);
				attaeandace.setUpdatedBy(loggedInUserId);
				attaeandace.setUpdatedDate(new Date());
			} else {
				attaeandace = new Attendance();
				attaeandace.setCreatedDate(new Date());
				Integer rowId = repositoryAtteandace.getCountOfTotalAtteandaces();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				attaeandace.setRowId(increment);
			}
			attaeandace.setAccrueType(dtoAtteandace.getAccrueType());
			attaeandace.setAttendance(dtoAtteandace.isAttendance());
			attaeandace.setCurrentyear(dtoAtteandace.getCurrentyear());
			attaeandace.setLatDayAcuurate(dtoAtteandace.getLatDayAcuurate());
			attaeandace.setNextTranscationNum(dtoAtteandace.getNextTranscationNum());
			attaeandace.setNumberOfDayInWeek(dtoAtteandace.getNumberOfDayInWeek());
			attaeandace.setNumberOfHourInDay(dtoAtteandace.getNumberOfHourInDay());
			attaeandace.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryAtteandace.saveAndFlush(attaeandace);
			log.debug("Attedance is:"+dtoAtteandace.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAtteandace;
	}

	public DtoAtteandace deleteAtteandace(List<Integer> ids) {
		log.info("deleteAtteandace Method");
		DtoAtteandace dtoAtteadance = new DtoAtteandace();
		dtoAtteadance
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ATTEDANCE_DELETED", false));
		dtoAtteadance.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("DEPARTMENT_ASSOCIATED", false));
		List<DtoAtteandace> deleteattedance = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer attedanceId : ids) {
				Attendance attendance = repositoryAtteandace.findOne(attedanceId);
				DtoAtteandace dtoDepartment2 = new DtoAtteandace();
				dtoDepartment2.setId(attedanceId);
				repositoryAtteandace.deleteSingleAtteandace(true, loggedInUserId, attendance.getId());
				deleteattedance.add(dtoDepartment2);

			}
			dtoAtteadance.setDelete(deleteattedance);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Attandance :"+dtoAtteadance.getId());
		return dtoAtteadance;
	}


	
	public DtoAtteandace getByAtteandaceId(Integer id) {
		log.info("getByAtteandaceId Method");
		DtoAtteandace dtoPosition = new DtoAtteandace();

		try {
			if (id > 0) {
				Attendance position = repositoryAtteandace.findByIdAndIsDeleted(id, false);
				if (position != null) {
					dtoPosition = new DtoAtteandace(position);
				} else {
					dtoPosition.setMessageType("SKILL_STEUP_NOT_GETTING");

				}
			} else {
				dtoPosition.setMessageType("INVALID_DEPARTMENT_ID");

			}
			log.debug("Atteandace By Id is:"+dtoPosition.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPosition;
	}
	
	public DtoSearch getAll(DtoAtteandace dtoPositionBudget) {
		log.info("getAll Atteandace Method");
		DtoSearch dtoSearch = new DtoSearch();

		try {
			dtoSearch.setPageNumber(dtoPositionBudget.getPageNumber());
			dtoSearch.setPageSize(dtoPositionBudget.getPageSize());
			dtoSearch.setTotalCount(repositoryAtteandace.getCountOfTotalAtteandace());
			List<Attendance> skillPositionList = null;
			if (dtoPositionBudget.getPageNumber() != null && dtoPositionBudget.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoPositionBudget.getPageNumber(), dtoPositionBudget.getPageSize(), Direction.DESC, "createdDate");
				skillPositionList = repositoryAtteandace.findByIsDeleted(false, pageable);
			} else {
				skillPositionList = repositoryAtteandace.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoAtteandace> dtoPOsitionList=new ArrayList<>();
			if(skillPositionList!=null && !skillPositionList.isEmpty())
			{
				for (Attendance position : skillPositionList) 
				{
					dtoPositionBudget=new DtoAtteandace(position);
					dtoPositionBudget.setAccrueType(position.getAccrueType());
					dtoPositionBudget.setAttendance(position.isAttendance());
					dtoPositionBudget.setCurrentyear(position.getCurrentyear());
					dtoPositionBudget.setLatDayAcuurate(position.getLatDayAcuurate());
					dtoPositionBudget.setNumberOfDayInWeek(position.getNumberOfDayInWeek());
					dtoPositionBudget.setNextTranscationNum(position.getNextTranscationNum());
					dtoPositionBudget.setNumberOfHourInDay(position.getNumberOfHourInDay());
					dtoPOsitionList.add(dtoPositionBudget);
				}
				dtoSearch.setRecords(dtoPOsitionList);
			}
			log.debug("All Atteandace List Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
}
