package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoShiftCode;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceShiftCode;

@RestController
@RequestMapping("/shiftCode")
public class ControllerShiftCode extends BaseController{

	@Autowired
	ServiceShiftCode serviceShiftCode;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger log = Logger.getLogger(ControllerShiftCode.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	/**
	 * @param request{
	  "shiftCodeId" : "sh1",
	"desc" : "shiftCode",
	 "inActive" : true,
	"arabicDesc":"Shift Arabic",
	"shitPremium":"1",
	  "amount":25,
	  "percent":50
	 } @param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"shiftCodeId": "sh1",
		"desc": "shiftCode",
		"arabicDesc": "Shift Arabic",
		"inActive": true,
		"shitPremium": 1,
		"amount": 25,
		"percent": 50,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		}
	 * @param dtoShiftCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoShiftCode dtoShiftCode) throws Exception{
		log.info("Create ShiftCode Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoShiftCode = serviceShiftCode.saveOrUpdate(dtoShiftCode);
			responseMessage=displayMessage(dtoShiftCode, "SHIFT_CODE_CREATED", "SHIFT_CODE_NOT_CREATED", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		log.debug("Create ShiftCode Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	
	/**
	 * @param request{
	  "id" : 1,
	  "shiftCodeId" : "sh1test",
	"desc" : "shiftCode test",
	 "inActive" : true,
	"arabicDesc":"Shift test Arabic",
	"shitPremium":"1",
	  "amount":26,
	  "percent":52
 }@param response{
"code": 201,
"status": "CREATED",
"result": {
"id": 1,
"shiftCodeId": "sh1test",
"desc": "shiftCode test",
"arabicDesc": "Shift test Arabic",
"inActive": true,
"shitPremium": 1,
"amount": 26,
"percent": 52,
"pageNumber": null,
"pageSize": null,
"ids": null,
"isActive": null,
"messageType": null,
"message": null,
"deleteMessage": null,
"associateMessage": null,
"delete": null,
"isRepeat": null
},
	 * @param dtoShiftCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoShiftCode dtoShiftCode) throws Exception {
		log.info("Update ShiftCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoShiftCode = serviceShiftCode.saveOrUpdate(dtoShiftCode);
			responseMessage=displayMessage(dtoShiftCode, MessageConstant.SHIFT_CODE_UPDATED_SUCCESS, MessageConstant.SHIFT_CODE_NOT_UPDATED, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Update ShiftCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
		  
				 "ids":[1]
		}
		
		
		"isRepeat": null
		},
		"btiMessage": {
		"message": "N/A",
		"messageShort": "N/A"
		}
		}
	 * @param dtoShiftCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoShiftCode dtoShiftCode) throws Exception {
		log.info("Delete ShiftCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoShiftCode.getIds() != null && !dtoShiftCode.getIds().isEmpty()) {
				DtoShiftCode dtoPosition1 = serviceShiftCode.delete(dtoShiftCode.getIds());
				if(dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("SHIFT_CODE_DELETED", false), dtoPosition1);

				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoPosition1.getMessageType());
					btiMessageHcm.setMessageShort("SHIFT_CODE_NOT_DELETE_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoPosition1);

				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete ShiftCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  "id" : 1
	}
	@param response{
				
		"code": 201,
"status": "CREATED",
"result": {
"id": 1,
"shiftCodeId": "sh1test",
"desc": "shiftCode test",
"arabicDesc": "Shift test Arabic",
"inActive": true,
"shitPremium": 1,
"amount": 26,
"percent": 52,
"pageNumber": null,
"pageSize": null,
"ids": null,
"isActive": null,
"messageType": null,
"message": null,
"deleteMessage": null,
"associateMessage": null,
"delete": null,
"isRepeat": null
		
		}
	 * @param dtoShiftCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoShiftCode dtoShiftCode) throws Exception {
		log.info("Get ShiftCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoShiftCode dtoShiftCode1 = serviceShiftCode.getById(dtoShiftCode.getId());
			responseMessage=displayMessage(dtoShiftCode1, "SHIFT_CODE_GET_DETAIL", MessageConstant.SHIFT_CODE_NOT_GETTING, response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		log.debug("Get ShiftCode by Id Method:"+dtoShiftCode.getId());
		return responseMessage;
	}
	
	/**
	 * @param request{
	 
  "shiftCodeId" : "sh1test"
	}
	@param request{
			"code": 302,
	"status": "FOUND",
	"result": {
	"id": null,
	"shiftCodeId": null,
	"desc": null,
	"arabicDesc": null,
	"inActive": false,
	"shitPremium": 0,
	"amount": null,
	"percent": null,
	"pageNumber": null,
	"pageSize": null,
	"ids": null,
	"isActive": null,
	"messageType": null,
	"message": null,
	"deleteMessage": null,
	"associateMessage": null,
	"delete": null,
	"isRepeat": true
	
		}
	 * @param stoShiftCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/shiftCodecheck", method = RequestMethod.POST)
	public ResponseMessage shiftCodecheck(HttpServletRequest request, @RequestBody DtoShiftCode stoShiftCode) throws Exception {
		log.info("timeCodeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoShiftCode dtoShiftCode = serviceShiftCode.repeatByShiftCodeId(stoShiftCode.getShiftCodeId());
			if (dtoShiftCode != null) {
				if (dtoShiftCode.getIsRepeat()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("SHIFT_CODE_REPEAT_FOUND", false), dtoShiftCode);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted("SHIFT_CODE_REPEAT_NOT_FOUND", false),
							dtoShiftCode);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("SHIFT_CODE_REPEAT_NOT_FOUND", false), dtoShiftCode);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	/**
//	 * @param dtoSearch
	 * @param request{
		  "sortOn" : "",
		  "sortBy" : "",
		 "searchKeyword" : "",
		"pageNumber":"0",
		"pageSize":"10"
		}
		 @param response{
			"code": 200,
			"status": "OK",
			"result": {
			"searchKeyword": "",
			"pageNumber": 0,
			"pageSize": 10,
			"sortOn": "",
			"sortBy": "",
			"totalCount": 1,
			"records": [
			  {
			"id": 1,
			"shiftCodeId": "sh1test",
			"desc": "shiftCode test",
			"arabicDesc": "Shift test Arabic",
			"inActive": true,
			"shitPremium": 1,
			"amount": 26,
			"percent": 52,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"delete": null,
			"isRepeat": null
			}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search search shiftCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceShiftCode.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.SHIFT_CODE_GET_ALL, MessageConstant.SHIFT_CODE_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search shiftCode Methods:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{
			 "searchKeyword" : ""
	}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchShiftCodeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchShiftCodeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search ShiftCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceShiftCode.searchShiftCodeId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SHIFT_CODE_GET_ALL", "SHIFT_CODE_NOT_GETTING", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search shiftCode Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{  
		 "searchKeyword" : ""
		}response{
			"code": 200,
			"status": "OK",
			"result": {
			"searchKeyword": "",
			"totalCount": 1,
			"records": [
			  {
			"createdDate": 1522046277000,
			"updatedDate": 1522046487000,
			"updatedBy": 2,
			"isDeleted": false,
			"updatedRow": null,
			"rowId": 0,
			"id": 1,
			"shiftCodeId": "sh1test",
			"desc": "shiftCode test",
			"arabicDesc": "Shift test Arabic",
			"inActive": true,
			"shitPremium": 1,
			"amount": 26,
			"percent": 52
			}
			],
			}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search ShiftCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceShiftCode.getIds(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SHIFT_CODE_GET_ALL", "SHIFT_CODE_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search shiftCode Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllShiftCodeInActive", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllShiftCodeInActive(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoShiftCode> shiftCodeList = serviceShiftCode.getAllShiftCodeInActiveList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("POSITION_PLAN_GET_ALL", false), shiftCodeList);
			}else {
				responseMessage = unauthorizedMsg(response);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return responseMessage;
	}
}
