package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40143",indexes = {
        @Index(columnList = "POTPLCINDX")
})
public class PositionPayCode extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POTPLCINDX")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "POTPLINDX")
	private PositionPalnSetup positionPalnSetup;
	
	@Column(name = "POTPLCSEQN")
	private Integer positionPaycodeSeqn;
	
	@Column(name = "POTPLCTYP")
	private short codeType;
	
	@ManyToOne
	@JoinColumn(name= "PYCDINDX")
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	private PayCode payCode;
	
	@ManyToOne
	@JoinColumn(name = "DEDCINDX")
	@Where(clause = "DEDCINCTV = false and is_deleted = false")
	private DeductionCode deductionCode;
	
	@ManyToOne
	@JoinColumn(name = "BENCINDX")
	@Where(clause = "BENCINCTV = false and is_deleted = false")
	private BenefitCode benefitCode;
	
	@Column(name = "POTPLCRTE",precision = 10,scale = 3)
	private Double ratePay;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PositionPalnSetup getPositionPalnSetup() {
		return positionPalnSetup;
	}

	public void setPositionPalnSetup(PositionPalnSetup positionPalnSetup) {
		this.positionPalnSetup = positionPalnSetup;
	}

	public Integer getPositionPaycodeSeqn() {
		return positionPaycodeSeqn;
	}

	public void setPositionPaycodeSeqn(Integer positionPaycodeSeqn) {
		this.positionPaycodeSeqn = positionPaycodeSeqn;
	}

	

	

	public short getCodeType() {
		return codeType;
	}

	public void setCodeType(short codeType) {
		this.codeType = codeType;
	}

	public PayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(PayCode payCode) {
		this.payCode = payCode;
	}

	public DeductionCode getDeductionCode() {
		return deductionCode;
	}

	public void setDeductionCode(DeductionCode deductionCode) {
		this.deductionCode = deductionCode;
	}

	public BenefitCode getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(BenefitCode benefitCode) {
		this.benefitCode = benefitCode;
	}

	public Double getRatePay() {
		return ratePay;
	}

	public void setRatePay(Double ratePay) {
		this.ratePay = ratePay;
	}

	
	

}
