package com.bti.hcm.repository;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.SkillSetDesc;

@Repository("RepositorySkillSetDesc")
public interface RepositorySkillSetDesc extends JpaRepository<SkillSetDesc, Integer>{

	SkillSetDesc findByIdAndIsDeleted(Integer id, boolean b);
	
	//ME
	//@NamedNativeQuery("select * from skill_desc_set d where d.is_deleted=false and (d.skillSetSetup.skillSetId like :searchKeyWord or d.skillSetSetup.arabicDiscription like :searchKeyWord or d.skillSetSetup.skillSetDiscription like :searchKeyWord ) ")
	@Query("select d from SkillSetDesc d where d.isDeleted=false and d.skillSetSetup.id IN (select s.id from SkillSetSetup s where s.isDeleted=false and s.id =:id) ")
	List<SkillSetDesc> findBySkillSetSetupId(@Param("id") Integer id);	//ME try dtd  13Aug 
	
	//ME Test1
	@Query("select s.id from SkillSetSetup s where s.isDeleted=false and s.id =:id) ")
	Integer findBySkillSetSetupIdTest1(@Param("id") Integer id);	//ME try dtd  13Aug 
	
	//ME Test2
	@Query("select d from SkillSetDesc d where d.isDeleted=false and d.skillSetSetup.id =:a ")
	List<SkillSetDesc> findBySkillSetSetupIdTest2(@Param("a") Integer a);
	
	
	@Query("select count(*) from SkillSetDesc s where s.isDeleted=false")
	Integer getCountOfTotalSkillSetSetup();

	List<SkillSetDesc> findByIsDeleted(boolean b, Pageable pageable);

	List<SkillSetDesc> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update SkillSetDesc d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleSkillDesc(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer skillStepId);
	
	@Query("select count(*) from SkillSetDesc d where (d.skillSetSetup.skillSetId like :searchKeyWord or d.skillSetSetup.arabicDiscription like :searchKeyWord or d.skillSetSetup.skillSetDiscription like :searchKeyWord ) and d.isDeleted=false")
	Integer predictiveSkillSteupDescSearchTotalCount(@Param("searchKeyWord")String string);

	@Query("select d from SkillSetDesc d where  (d.skillSetSetup.skillSetId like :searchKeyWord or d.skillSetSetup.arabicDiscription like :searchKeyWord or d.skillSetSetup.skillSetDiscription like :searchKeyWord ) and d.isDeleted=false")
	List<SkillSetDesc> predictiveSkillsSetupDescSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Query(value = "select setup_id from skill_desc_set  where desc_id =:id", nativeQuery = true)
	List<Integer> getDescId(@Param("id")Integer id);

	@Query("select count(*) from SkillSetDesc s ")
	public Integer getCountOfTotalSkillSetDesc();

}
