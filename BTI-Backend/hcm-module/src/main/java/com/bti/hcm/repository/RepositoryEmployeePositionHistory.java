package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeePositionHistory;

@Repository("repositoryEmployeePositionHistory")
public interface RepositoryEmployeePositionHistory extends JpaRepository<EmployeePositionHistory, Integer>{

	
	public EmployeePositionHistory findByIdAndIsDeleted(int id, boolean deleted);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeePositionHistory d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleEmployeePositionHistory(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	@Query("select count(*) from EmployeePositionHistory d where (d.position.positionId like :searchKeyWord or d.position.description like :searchKeyWord or d.division.divisionId like :searchKeyWord or d.department.departmentId like :searchKeyWord or d.location.locationId like :searchKeyWord or d.supervisor.superVisionCode like :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveEmployeePositionHistorySearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	
	@Query("select d from EmployeePositionHistory d where (d.position.positionId like :searchKeyWord or d.position.description like :searchKeyWord or d.division.divisionId like :searchKeyWord or d.department.departmentId like :searchKeyWord or d.location.locationId like :searchKeyWord or d.supervisor.superVisionCode like :searchKeyWord) and d.isDeleted=false")
	public List<EmployeePositionHistory> predictiveEmployeePositionHistorySearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	
	public List<EmployeePositionHistory> findByIsDeleted(boolean isdeleted,Pageable pageRequest);

	@Query("select count(*) from EmployeePositionHistory e ")
	public Integer getCountOfTotalEmployeePositionHistory();
}
