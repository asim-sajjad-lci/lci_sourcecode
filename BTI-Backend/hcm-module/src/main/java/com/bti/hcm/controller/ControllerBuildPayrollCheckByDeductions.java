package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByDeductions;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceBenefitCode;
import com.bti.hcm.service.ServiceBuildPayrollCheckByDeductions;
import com.bti.hcm.service.ServiceBuildPayrollCheckByPayCodes;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/buildPayrollCheckByDeductions")
public class ControllerBuildPayrollCheckByDeductions extends BaseController{

	/**
	 * @Description LOGGER use for put a logger in BenefitCode Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerBuildPayrollCheckByDeductions.class);

	/**
	 * @Description serviceBenefitCode Autowired here using annotation of spring for
	 *              use of serviceBenefitCode method in this controller
	 */
	@Autowired(required = true)
	ServiceBenefitCode serviceBenefitCode;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@Autowired
	ServiceBuildPayrollCheckByPayCodes serviceBuildPayrollCheckByPayCodes;

	@Autowired
	ServiceBuildPayrollCheckByDeductions serviceBuildPayrollCheckByDeductions;

	/**
	 * @description Create BeneditCode
	 * @param request
	 * @param dtoBenefitCode
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoBuildPayrollCheckByDeductions  dtoBuildPayrollCheckByDeductions) throws Exception {
		LOGGER.info("Create BuildPayrollCheckByDeduction Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBuildPayrollCheckByDeductions  = serviceBuildPayrollCheckByDeductions.saveOrUpdate(dtoBuildPayrollCheckByDeductions);
			responseMessage=displayMessage(dtoBuildPayrollCheckByDeductions, "BUILD_CHECK_DEDUCTION_CREATED", "BUILD_CHECK_DEDUCTION_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create BuildPayrollCheckByDeduction Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoBuildPayrollCheckByDeductions dtoBuildPayrollCheckByDeductions, HttpServletRequest request) throws Exception {
		LOGGER.info("Search BuildPayrollCheckByDeduction Method");
		DtoSearch dtoSearch = null;
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBuildPayrollCheckByDeductions.getAllByCodeType(dtoBuildPayrollCheckByDeductions);
			responseMessage=displayMessage(dtoSearch, "BUILD_CHECK_DEDUCTION_GET_ALL", "BUILD_CHECK_DEDUCTION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllByDates", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllDates(@RequestBody DtoBuildPayrollCheckByDeductions dtoBuildPayrollCheckByDeductions, HttpServletRequest request) throws Exception {
		LOGGER.info("Search BuildPayrollCheckByDeduction Method");
		DtoSearch dtoSearch = null;
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBuildPayrollCheckByDeductions.getAllByCodeTypeHm2(dtoBuildPayrollCheckByDeductions);
			responseMessage=displayMessage(dtoSearch, "BUILD_CHECK_DEDUCTION_GET_ALL", "BUILD_CHECK_DEDUCTION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
}
