package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSalaryMatrixRow;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceSalaryMatrixRow;

@RestController
@RequestMapping("/salaryMatrixRow")
public class ControllerSalaryMatrixRow extends BaseController{
	/**
	 * @Description LOGGER use for put a logger in SalaryMatrixRow Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerSalaryMatrixRow.class);

	/**
	 * @Description serviceSalaryMatrixRow Autowired here using annotation of spring
	 *              for use of serviceSalaryMatrixRow method in this controller
	 */
	@Autowired(required = true)
	ServiceSalaryMatrixRow serviceSalaryMatrixRow;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoSalaryMatrixRow dtoSalaryMatrixRow)
			throws Exception {
		LOGGER.info("Create SalaryMatrixRow Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSalaryMatrixRow = serviceSalaryMatrixRow.saveOrUpdate(dtoSalaryMatrixRow);
			responseMessage=displayMessage(dtoSalaryMatrixRow, "SALARY_MATRIX_CREATED", "SALARY_MATRIX_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create SalaryMatrixRow Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoSalaryMatrixRow dtoSalaryMatrixRow)
			throws Exception {
		LOGGER.info("Update SalaryMatrix Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSalaryMatrixRow = serviceSalaryMatrixRow.saveOrUpdate(dtoSalaryMatrixRow);
			responseMessage=displayMessage(dtoSalaryMatrixRow, "SALARY_MATRIX_UPDATED_SUCCESS", "SALARY_MATRIX_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update SalaryMatrix Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoSalaryMatrixRow dtoSalaryMatrixRow)
			throws Exception {
		LOGGER.info("Delete SalaryMatrixSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSalaryMatrixRow.getIds() != null && !dtoSalaryMatrixRow.getIds().isEmpty()) {
				DtoSalaryMatrixRow dtoSalaryMatrixSetup2 = serviceSalaryMatrixRow.delete(dtoSalaryMatrixRow.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("SALARY_MATRIX_DELETED", false),
						dtoSalaryMatrixSetup2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete SalaryMatrixSetup Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search SalaryMatrixSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSalaryMatrixRow.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SALARY_MATRIX_GET_ALL", "SALARY_MATRIX_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if (dtoSearch != null) {
			LOGGER.debug("Search SalaryMatrixRow Method:" + dtoSearch.getTotalCount());
		}

		return responseMessage;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody DtoSalaryMatrixRow dtoSalaryMatrixRow)
			throws Exception {
		LOGGER.info("Get All SalaryMatrixSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceSalaryMatrixRow.getAll(dtoSalaryMatrixRow);
			responseMessage=displayMessage(dtoSearch, "SALARY_MATRIX_GET_ALL", "SALARY_MATRIX_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All SalaryMatrixSetup Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getSalaryMatrixSetupId", method = RequestMethod.POST)
	public ResponseMessage getSalaryMatrixSetupId(HttpServletRequest request,
			@RequestBody DtoSalaryMatrixRow dtoSalaryMatrixRow41) throws Exception {
		LOGGER.info("Get SalaryMatrixSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSalaryMatrixRow dtoSalaryMatrixRow = serviceSalaryMatrixRow.getById(dtoSalaryMatrixRow41.getId());
			responseMessage=displayMessage(dtoSalaryMatrixRow, "SALARY_MATRIX_LIST_GET_DETAIL", "SALARY_MATRIX_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get SalaryMatrixSetup by Id Method:" + dtoSalaryMatrixRow41.getId());
		return responseMessage;
	}

}
