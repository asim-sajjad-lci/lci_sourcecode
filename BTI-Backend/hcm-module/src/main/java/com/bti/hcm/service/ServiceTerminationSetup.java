package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.OrientationPredefinedCheckListItem;
import com.bti.hcm.model.TerminationSetup;
import com.bti.hcm.model.TerminationSetupDetails;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTerminationSetup;
import com.bti.hcm.model.dto.DtoTerminationSetupDetails;
import com.bti.hcm.repository.RepositoryOrientationPredefinedCheckListItem;
import com.bti.hcm.repository.RepositoryTerminationSetup;
import com.bti.hcm.repository.RepositoryTerminationSetupDetails;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceTerminationSetup")
public class ServiceTerminationSetup {

	static Logger log = Logger.getLogger(ServiceTerminationSetup.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in TimeCode service
	 */
	@Autowired
	ServiceResponse response;

	@Autowired
	RepositoryOrientationPredefinedCheckListItem repositoryOrientationPredefinedCheckListItem;

	@Autowired
	RepositoryTerminationSetup repositoryTerminationSetup;

	@Autowired
	RepositoryTerminationSetupDetails repositoryTerminationSetupDetails;

	public DtoTerminationSetup saveOrUpdateTerminationSetup(DtoTerminationSetup dtoTerminationSetup) {
		TerminationSetup terminationSetup = null;
		TerminationSetupDetails terminationSetupDetails = null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));

			if (dtoTerminationSetup.getId() != null && dtoTerminationSetup.getId() > 0) {
				terminationSetup = repositoryTerminationSetup.findByIdAndIsDeleted(dtoTerminationSetup.getId(), false);
				terminationSetupDetails = repositoryTerminationSetupDetails
						.findByIdAndIsDeleted(dtoTerminationSetup.getTerminationDetailId(), false);
				terminationSetup.setUpdatedBy(loggedInUserId);
				terminationSetup.setUpdatedDate(new Date());
				terminationSetupDetails.setUpdatedBy(loggedInUserId);
				terminationSetupDetails.setUpdatedDate(new Date());
			} else {
				terminationSetup = new TerminationSetup();
				terminationSetupDetails = new TerminationSetupDetails();
				terminationSetup.setCreatedDate(new Date());
				terminationSetupDetails.setCreatedDate(new Date());
			}

			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem = null;
			if (dtoTerminationSetup.getOrientationPredefinedCheckListItemId() != null
					&& dtoTerminationSetup.getOrientationPredefinedCheckListItemId() > 0) {
				orientationPredefinedCheckListItem = repositoryOrientationPredefinedCheckListItem
						.findOne(dtoTerminationSetup.getOrientationPredefinedCheckListItemId());
			}

			terminationSetupDetails.setTerminationArbicDesc(dtoTerminationSetup.getTerminationDetailArbicDesc());
			terminationSetupDetails.setTerminationSequence(dtoTerminationSetup.getTerminationDetailSequence());
			terminationSetupDetails.setTerminationDesc(dtoTerminationSetup.getTerminationDesc());
			terminationSetupDetails.setTerminationDate(dtoTerminationSetup.getTerminationDetailDate());
			terminationSetupDetails.setTerminationPersonName(dtoTerminationSetup.getTerminationDetailPersonName());
			terminationSetupDetails.setTerminationSetup(terminationSetup);
			terminationSetup.setOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
			terminationSetup
					.setOrientationPredefinedCheckListItem(dtoTerminationSetup.getOrientationPredefinedCheckListItem());
			terminationSetup.setTerminationId(dtoTerminationSetup.getTerminationId());
			terminationSetup.setTerminationDesc(dtoTerminationSetup.getTerminationDesc());
			terminationSetup.setTerminationArbicDesc(dtoTerminationSetup.getTerminationArbicDesc());
			terminationSetup.setPredefinecheckListtypeId(dtoTerminationSetup.getPredefinecheckListtypeId());
			terminationSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			terminationSetup.setRowId(loggedInUserId);
			terminationSetup = repositoryTerminationSetup.saveAndFlush(terminationSetup);
			if (terminationSetup.getListTerminationSetupDetails() == null) {
				for (DtoTerminationSetupDetails dtoTerminationSetupDetails : dtoTerminationSetup.getSubItems()) {
					terminationSetupDetails = new TerminationSetupDetails();
					terminationSetupDetails.setTerminationSetup(terminationSetup);
					terminationSetupDetails.setTerminationSequence(dtoTerminationSetupDetails.getSequence());
					terminationSetupDetails.setTerminationDate(dtoTerminationSetupDetails.getCompleteDate());
					terminationSetupDetails.setTerminationPersonName(dtoTerminationSetupDetails.getPersonResponsible());
					terminationSetupDetails.setTerminationDesc(dtoTerminationSetupDetails.getChecklistitem());
					terminationSetupDetails.setCreatedDate(new Date());
					repositoryTerminationSetupDetails.saveAndFlush(terminationSetupDetails);
				}
			} else {
				for (DtoTerminationSetupDetails dtoTerminationSetupDetails : dtoTerminationSetup.getSubItems()) {
					TerminationSetupDetails terminationSetupDetailsUpdate = repositoryTerminationSetupDetails
							.findOne(dtoTerminationSetupDetails.getId());
					terminationSetupDetailsUpdate.setTerminationSequence(dtoTerminationSetupDetails.getSequence());
					terminationSetupDetailsUpdate.setTerminationDate(dtoTerminationSetupDetails.getCompleteDate());
					terminationSetupDetailsUpdate
							.setTerminationPersonName(dtoTerminationSetupDetails.getPersonResponsible());
					terminationSetupDetailsUpdate.setTerminationDesc(dtoTerminationSetupDetails.getChecklistitem());
					repositoryTerminationSetupDetails.saveAndFlush(terminationSetupDetailsUpdate);
				}
			}

		} catch (Exception e) {
			log.error(e);
		}

		return dtoTerminationSetup;
	}

	public DtoTerminationSetup deleteTerminationSetup(List<Integer> ids) {
		log.info("delete TerminationSetup Method");
		DtoTerminationSetup dtoTerminationSetup = new DtoTerminationSetup();
		dtoTerminationSetup.setDeleteMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("TERMINATIONSETUP_DELETED", false));
		dtoTerminationSetup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("TERMINATIONSETUP_ASSOCIATED", false));
		List<DtoTerminationSetup> deleteTerminationSetup = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));

		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(
				response.getMessageByShortAndIsDeleted("TERMINATION_SETUP_NOT_DELETE_ID_MESSAGE", false).getMessage());

		try {
			for (Integer terminationId : ids) {
				TerminationSetup terminationSetup = repositoryTerminationSetup.findOne(terminationId);

				if (terminationSetup.getListTerminationSetupDetails().isEmpty()) {
					DtoTerminationSetup dtoTerminationSetup2 = new DtoTerminationSetup();
					dtoTerminationSetup2.setId(terminationId);
					dtoTerminationSetup2.setTerminationDesc(terminationSetup.getTerminationDesc());
					dtoTerminationSetup2.setTerminationArbicDesc(terminationSetup.getTerminationArbicDesc());
					repositoryTerminationSetup.deleteSingleTeminationSetup(true, loggedInUserId, terminationId);
					deleteTerminationSetup.add(dtoTerminationSetup2);
				} else {
					inValidDelete = true;
				}
			}

			if (inValidDelete) {
				dtoTerminationSetup.setMessageType(invlidDeleteMessage.toString());
			}

			if (!inValidDelete) {
				dtoTerminationSetup.setMessageType(
						response.getMessageByShortAndIsDeleted("TERMINATION_SETUP_DELETED", false).getMessage());
			}

			dtoTerminationSetup.setDelete(deleteTerminationSetup);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Location :" + dtoTerminationSetup.getId());
		return dtoTerminationSetup;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchTerminationSetup(DtoSearch dtoSearch) {
		log.info("search TerminationSetup Method");
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();
			String condition = "";
			if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

				if (dtoSearch.getSortOn().equals("terminationId") || dtoSearch.getSortOn().equals("terminationDesc")
						|| dtoSearch.getSortOn().equals("terminationArbicDesc")) {
					condition = dtoSearch.getSortOn();
				} else {
					condition = "id";
				}

			} else {
				condition += "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}

			dtoSearch.setTotalCount(
					this.repositoryTerminationSetup.predictiveTerminationSetupSearchTotalCount("%" + searchWord + "%"));
			List<TerminationSetup> terminationSetupList = null;
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

				if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
					terminationSetupList = this.repositoryTerminationSetup
							.predictiveTerminationSetupSearchWithPagination("%" + searchWord + "%", new PageRequest(
									dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if (dtoSearch.getSortBy().equals("ASC")) {
					terminationSetupList = this.repositoryTerminationSetup
							.predictiveTerminationSetupSearchWithPagination("%" + searchWord + "%", new PageRequest(
									dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				} else if (dtoSearch.getSortBy().equals("DESC")) {
					terminationSetupList = this.repositoryTerminationSetup
							.predictiveTerminationSetupSearchWithPagination("%" + searchWord + "%",
									new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
											Sort.Direction.DESC, condition));
				}

			}
			if (terminationSetupList != null && !terminationSetupList.isEmpty()) {
				List<DtoTerminationSetup> dtoTerminationSetupList = new ArrayList<>();
				DtoTerminationSetup dtoTerminationSetup = null;
				for (TerminationSetup terminationSetup : terminationSetupList) {
					dtoTerminationSetup = new DtoTerminationSetup(terminationSetup);

					List<TerminationSetupDetails> list = terminationSetup.getListTerminationSetupDetails();

					dtoTerminationSetup.setId(terminationSetup.getId());
					dtoTerminationSetup.setTerminationId(terminationSetup.getTerminationId());
					dtoTerminationSetup.setTerminationDesc(terminationSetup.getTerminationDesc());
					dtoTerminationSetup.setPredefinecheckListtypeId(terminationSetup.getPredefinecheckListtypeId());
					dtoTerminationSetup.setTerminationArbicDesc(terminationSetup.getTerminationArbicDesc());
					
					if (terminationSetup.getOrientationPredefinedCheckListItem() != null) {
					
					dtoTerminationSetup.setPreDefineCheckListItemDescription(terminationSetup.getOrientationPredefinedCheckListItem().getPreDefineCheckListItemDescription());
					dtoTerminationSetup.setPreDefineCheckListType(terminationSetup.getOrientationPredefinedCheckListItem().getPreDefineCheckListType());
					
					
					}
					if (terminationSetup.getOrientationPredefinedCheckListItem() != null) {
						dtoTerminationSetup.setPreDefineCheckListItemDescription(terminationSetup.getOrientationPredefinedCheckListItem().getPreDefineCheckListItemDescription());
						dtoTerminationSetup.setOrientationPredefinedCheckListItemId(
								terminationSetup.getOrientationPredefinedCheckListItem().getId());
					}
					dtoTerminationSetup.setTerminationDetailId(terminationSetup.getId());


					if (dtoTerminationSetup.getSubItems() == null) {
						List<DtoTerminationSetupDetails> listTerminationSetup = new ArrayList<>();
						dtoTerminationSetup.setSubItems(listTerminationSetup);
					}
					for (TerminationSetupDetails terminationSetupDetails2 : list) {

						DtoTerminationSetupDetails dtoTerminationSetup1 = new DtoTerminationSetupDetails();
						dtoTerminationSetup1.setSequence(terminationSetupDetails2.getTerminationSequence());
						dtoTerminationSetup1.setCompleteDate(terminationSetupDetails2.getTerminationDate());
						dtoTerminationSetup1.setPersonResponsible(terminationSetupDetails2.getTerminationPersonName());
						dtoTerminationSetup1.setChecklistitem(terminationSetupDetails2.getTerminationDesc());
						dtoTerminationSetup1.setId(terminationSetupDetails2.getId());
						dtoTerminationSetup.getSubItems().add(dtoTerminationSetup1);
					}

					dtoTerminationSetupList.add(dtoTerminationSetup);
				}

				dtoSearch.setRecords(dtoTerminationSetupList);
			}

		}

		log.debug("Search searchPosition Size is:" + dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoTerminationSetup getTerminationSetupById(int id) {
		DtoTerminationSetup dtoTerminationSetup = new DtoTerminationSetup();
		if (id > 0) {
			TerminationSetup terminationSetup = repositoryTerminationSetup.findByIdAndIsDeleted(id, false);
			if (terminationSetup != null) {
				dtoTerminationSetup = new DtoTerminationSetup(terminationSetup);
			} else {
				dtoTerminationSetup.setMessageType("TERMINATIONSETUP_NOT_GETTING");

			}
		} else {
			dtoTerminationSetup.setMessageType("INVALID_TERMINATION_ID");

		}
		return dtoTerminationSetup;
	}

	public DtoTerminationSetup repeatByTerminationId(String terminationId) {
		log.info("repeatByTerminationId Method");
		DtoTerminationSetup dtoTerminationSetup = new DtoTerminationSetup();
		try {
			List<TerminationSetup> rerminationSetup = repositoryTerminationSetup.findByByTerminationId(terminationId);
			if (rerminationSetup != null && !rerminationSetup.isEmpty()) {
				dtoTerminationSetup.setIsRepeat(true);
				dtoTerminationSetup.setTerminationId(terminationId);
			} else {
				dtoTerminationSetup.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoTerminationSetup;
	}

}
