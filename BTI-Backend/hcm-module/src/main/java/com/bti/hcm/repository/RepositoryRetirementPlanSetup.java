package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.RetirementPlanSetup;

@Repository("repositoryRetirementPlanSetup")
public interface RepositoryRetirementPlanSetup extends JpaRepository<RetirementPlanSetup, Integer>{

	RetirementPlanSetup findByIdAndIsDeleted(Integer id, boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update RetirementPlanSetup r set r.isDeleted =:deleted ,r.updatedBy =:updateById where r.id =:id ")
	void deleteSingleRetirementPlanSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	
	@Query("select count(*) from RetirementPlanSetup r where (r.retirementPlanId LIKE :searchKeyWord or r.retirementPlanDescription LIKE :searchKeyWord or r.retirementPlanArbicDescription LIKE :searchKeyWord or "
			+ " r.retirementPlanAccountNumber Like :searchKeyWord or  r.loans LIKE :searchKeyWord or  r.bonus LIKE :searchKeyWord) and r.isDeleted=false")
	
	Integer predictiveRetirementPlanSetupSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);
	@Query("select r from RetirementPlanSetup r where (r.retirementPlanId LIKE :searchKeyWord or r.retirementPlanDescription LIKE :searchKeyWord  or r.retirementPlanArbicDescription LIKE :searchKeyWord or "
			+ " r.retirementPlanAccountNumber LIKE :searchKeyWord or  r.loans LIKE :searchKeyWord or  r.bonus LIKE :searchKeyWord) and r.isDeleted=false")
	List<RetirementPlanSetup> predictiveRetirementPlanSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select r from RetirementPlanSetup r where (r.retirementPlanId =:retirementPlanId) and r.isDeleted=false")
	List<RetirementPlanSetup> findByRetirementPlanId(@Param("retirementPlanId")String retirementPlanId);

	@Query("select count(*) from RetirementPlanSetup r ")
	public Integer getCountOfTotaRetirementPlanSetup();

}
