package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.service.ServiceEmployeeByTransactionType;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;




/*
 * @author Safwan
 * 
 * */
@RestController
@RequestMapping("/employeeByTransactionType")
public class ControllerEmployeeByTransactionType extends BaseController {


	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeByTransactionType.class);

	@Autowired
	ServiceEmployeeByTransactionType serviceEmployeeByTransactionType;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	/*
	 * param
	 * 
	 * type
	 * */
	@PostMapping("/search")
	public ResponseMessage search(@RequestBody DtoSearchActivity dtoSearchActivity, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Employee By Transaction Type Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchActivity = this.serviceEmployeeByTransactionType.findEmployeeByTransactionId(dtoSearchActivity);
			responseMessage=displayMessage(dtoSearchActivity, "GET_ALL_EMPLOYESS_BY_TRANSACTION_TYPE", "GET_ALL_EMPLOYESS_BY_TRANSACTION_TYPE_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearchActivity!=null) {
			LOGGER.debug("Search  Employee By Transaction Method:"+dtoSearchActivity.getTotalCount());
		}

		return responseMessage;
	}

	
	/*
	 * 
	 * param
	 * 
	 * id 
	 * 
	 * */

	@RequestMapping(value = "/findByPayCode", method = RequestMethod.POST)
	public ResponseMessage find(@RequestBody DtoSearchActivity dtoSearchActivity, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Employee By Pay Code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchActivity = this.serviceEmployeeByTransactionType.findEmployeeByPayCodeId(dtoSearchActivity);
			responseMessage=displayMessage(dtoSearchActivity, "GET_ALL_EMPLOYESS_BY_PAY_CODE", "GET_ALL_EMPLOYESS_BY__PAY_CODE_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearchActivity!=null) {
			LOGGER.debug("Search  Employee By Pay Code Method:"+dtoSearchActivity.getTotalCount());
		}

		return responseMessage;
	}
	
	/*
	 * 
	 * param
	 * 
	 * id
	 * startDate
	 * endDate
	 * 
	 * 
	 * */

	@RequestMapping(value = "/findByDeductionCode", method = RequestMethod.POST)
	public ResponseMessage findByDeduction(@RequestBody DtoSearchActivity dtoSearchActivity, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Employee By Deduction Code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchActivity = this.serviceEmployeeByTransactionType.findEmployeeByDeductionCodeId(dtoSearchActivity);
			responseMessage=displayMessage(dtoSearchActivity, "GET_ALL_EMPLOYESS_BY_DEDUCTION_CODE", "GET_ALL_EMPLOYESS_BY__DEDUCTION_CODE_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearchActivity!=null) {
			LOGGER.debug("Search  Employee By Deduction Code Method:"+dtoSearchActivity.getTotalCount());
		}

		return responseMessage;
	}
	
	
	/*
	 * 
	 * param
	 * 
	 * id
	 * startDate
	 * endDate
	 * 
	 * 
	 * */
	@RequestMapping(value = "/findByBenefitCode", method = RequestMethod.POST)
	public ResponseMessage findByBenefit(@RequestBody DtoSearchActivity dtoSearchActivity, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Employee By Benefit Code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchActivity = this.serviceEmployeeByTransactionType.findEmployeeByBenefitCodeId(dtoSearchActivity);
			responseMessage=displayMessage(dtoSearchActivity, "GET_ALL_EMPLOYESS_BY_Benefit_CODE", "GET_ALL_EMPLOYESS_BY__BENEFIT_CODE_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearchActivity!=null) {
			LOGGER.debug("Search  Employee By Benefit Code Method:"+dtoSearchActivity.getTotalCount());
		}

		return responseMessage;
	}
}
