package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoAccrualScheduleDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceAccrualScheduleDetail;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/accrualScheduleDetail")
public class ControllerAccrualScheduleDetail extends BaseController{

	private static final Logger LOGGER = Logger.getLogger(ControllerAccrualScheduleDetail.class);
	@Autowired
	ServiceAccrualScheduleDetail serviceAccrualScheduleDetail;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	

	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * 
	"scheduleId":1,
     "scheduleSequence":1,
     "description":"test"
      }

	 * }
	 * @response{
	 * 	"code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":2,
	 * "description":"Testing Location",
	 * "arabicDescription":"موقع الاختبار",
	 * "contactName":"Gaurav Sortekar",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * 	"btiMessage":{
	 * 					"message":"Location has been created successfully",
	 * 					"messageShort":"LOCATION_CREATED"}
	 * }
	 * @throws Exception
	 */
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createAccrualScheduleDetail(HttpServletRequest request, @RequestBody DtoAccrualScheduleDetail dtoAccrualScheduleDetail) throws Exception {
		LOGGER.info("Create AccrualScheduleDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrualScheduleDetail  = serviceAccrualScheduleDetail.saveOrUpdateAccrualScheduleDetail(dtoAccrualScheduleDetail);
			responseMessage=displayMessage(dtoAccrualScheduleDetail, "ACCRUALSCHEDULEDETAIL_CREATED", "ACCRUALSCHEDULEDETAIL_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create AccrualScheduleDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}


	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "pageNumber":"0",
 		"pageSize":"10"
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"pageNumber":0,
	 * "pageSize":10,
	 * "totalCount":1,
	 * "records":[
	 * {"locationId":1,
	 * "description":"Test Location",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}
	 * ]
	 * },
	 * 
	 * 		"btiMessage":{"message":"All locations fetched successfully",
	 * 		"messageShort":"LOCATION_GET_ALL"}
	 * }
	 * @throws Exception
	 */
		

	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * 		"scheduleId":1,
             "scheduleSequence":1,
             "description":"test123"

	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"id":2,
	 * "locationId":2,
	 * "description":"Testing Location",
	 * "arabicDescription":"موقع الاختبار",
	 * "contactName":"Gaurav",
	 * "locationAddress":"Surat",
	 * "city":"Surat",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * "btiMessage":{
	 * 					"message":"Location updated successfully",
	 * 					"messageShort":"LOCATION_UPDATE_SUCCESS"}
	 * }
	 * @throws Exception
	 */
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateAccrualScheduleDetail(HttpServletRequest request, @RequestBody DtoAccrualScheduleDetail dtoAccrualScheduleDetail) throws Exception {
	LOGGER.info("Update AccrualScheduleDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			dtoAccrualScheduleDetail = serviceAccrualScheduleDetail.saveOrUpdateAccrualScheduleDetail(dtoAccrualScheduleDetail);
			responseMessage=displayMessage(dtoAccrualScheduleDetail, "ACCRUALSCHEDULEDETAIL_UPDATE_SUCCESS", "ACCRUALSCHEDULEDETAIL_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update AccrualScheduleDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * "ids" : [2]
	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":0,
	 * "deleteMessage":"Division deleted successfully",
	 * "associateMessage":"N/A",
	 * "deleteLocation":[
	 * {"id":2,
	 * "locationId":2,
	 * "description":"Testing Location",
	 * "contactName":"Gaurav",
	 * "locationAddress":"Surat",
	 * "city":"Surat",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}]},
	 * "btiMessage":{
	 * 					"message":"AccrualScheduleDetail deleted successfully",
	 * 					"messageShort":"ACCRUALSCHEDULEDETAIL_DELETED"}
	 * }
	 * @throws Exception
	 */
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteAccrualScheduleDetail(HttpServletRequest request, @RequestBody DtoAccrualScheduleDetail dtoAccrualScheduleDetail) throws Exception {
		LOGGER.info("Delete AccrualScheduleDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoAccrualScheduleDetail.getIds() != null && !dtoAccrualScheduleDetail.getIds().isEmpty()) {
				DtoAccrualScheduleDetail dtoAccrualScheduleDetail2 =serviceAccrualScheduleDetail.deleteAccrualScheduleDetail(dtoAccrualScheduleDetail.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ACCRUALSHCEDULEDETAIL_DELETED", false), dtoAccrualScheduleDetail2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete AccrualScheduleDetail Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	

	/**
	 * 
	 * @param request
	 * @param dtoLocation
	 * @return
	 * @request{
	 * 		"id" : 1,
			"pageNumber" : 0,
			"pageSize" : 10

	 * }
	 * @response{
	 * "code":201,
	 * "status":"CREATED",
	 * "result":{"locationId":1,
	 * "description":"Test Location",
	 * "arabicDescription":"",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"},
	 * "btiMessage":{
	 * 							"message":"Location detail fetched successfully",
	 * 							"messageShort":"LOCATION_GET_DETAIL"}
	 * }
	 * @throws Exception
	 */
	
	
	
	@RequestMapping(value = "/getAccrualScheduleDetailById", method = RequestMethod.POST)
	public ResponseMessage getAccrualScheduleDetailById(HttpServletRequest request, @RequestBody DtoAccrualScheduleDetail dtoAccrualScheduleDetail) throws Exception {
		LOGGER.info("Get AccrualScheduleDetail ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAccrualScheduleDetail dtoAccrualScheduleDetailobj = serviceAccrualScheduleDetail.getAccrualScheduleDetailId(dtoAccrualScheduleDetail.getId());
			responseMessage=displayMessage(dtoAccrualScheduleDetailobj, "ACCRUALSCHEDULEDETAIL_GET_DETAIL", "ACCRUALSCHEDULEDETAIL_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get AccrualScheduleDetail ById Method:"+dtoAccrualScheduleDetail.getId());
		return responseMessage;
	}
	

	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @request{
	 * 	"searchKeyword" : "AHD",
		"pageNumber":"0",
		"pageSize":"10"
	 * }
	 * @response{
	 * "code":200,
	 * "status":"OK",
	 * "result":{
	 * "searchKeyword":"AHD",
	 * "pageNumber":0,
	 * "pageSize":10,
	 * "totalCount":1,
	 * "records":[
	 * {"locationId":1,
	 * "description":"Test Location",
	 * "contactName":"Dhaval Chauhan",
	 * "locationAddress":"Ahmedabad",
	 * "city":"AHD",
	 * "country":"India",
	 * "phone":"(123) 123 1010",
	 * "fax":"(044) 420 4400"}
	 * ]
	 * },
	 * "btiMessage":{
	 * 					"message":"All locations fetched successfully",
	 * 					"messageShort":"LOCATION_GET_ALL"}
	 * }
	 * @throws Exception
	 */
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search AccrualSheduleDetail Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAccrualScheduleDetail.searchAccrualSheduleDetail(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ACCRUALSHEDULEDETAIL_GET_ALL", "ACCRUALSHEDULEDETAIL_LIST_NOT_GETTING", serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
}
