package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoMiscellaneous;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceMiscellaneous;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/miscellaneous")
public class ControllerMiscellaneous extends BaseController {

	private Logger log = Logger.getLogger(ControllerMiscellaneous.class);

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@Autowired
	ServiceMiscellaneous serviceMiscellaneous;

	@PostMapping("/create")
	public ResponseMessage createMiscellaneousEntry(HttpServletRequest request,
			@RequestBody DtoMiscellaneous dtoMiscellaneous) throws Exception {
		log.info("Create Miscellaneous Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoMiscellaneous = serviceMiscellaneous.saveOrUpdate(dtoMiscellaneous);
			responseMessage = displayMessage(dtoMiscellaneous, "MISCELLANEOUS_ENTRY_CREATED",
					"MISCELLANEOUS_ENTRY_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create Miscellaneous Entry:" + responseMessage.getMessage());
		return responseMessage;
	}

	@PostMapping("/update")
	public ResponseMessage updateMiscellaneousEntry(HttpServletRequest request,
			@RequestBody DtoMiscellaneous dtoMiscellaneous) throws Exception {
		log.info("Update Miscellaneous Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoMiscellaneous = serviceMiscellaneous.saveOrUpdate(dtoMiscellaneous);
			responseMessage = displayMessage(dtoMiscellaneous, "MISCELLANEOUS_ENTRY_UPDATED",
					"MISCELLANEOUS_ENTRY_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update Miscellaneous Entry Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@PostMapping("/delete")
	public ResponseMessage deleteMiscellaneousEntry(HttpServletRequest request,
			@RequestBody DtoMiscellaneous dtoMiscellaneous) throws Exception {
		log.info("Delete Miscellaneous Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoMiscellaneous.getIds() != null && !dtoMiscellaneous.getIds().isEmpty()) {
				DtoMiscellaneous dtoMiscellaneous2 = serviceMiscellaneous
						.deleteMiscellaneousEntry(dtoMiscellaneous.getIds());

				if (dtoMiscellaneous2.getMessage() != null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("MISCELLANEOUS_ENTRY_DELETED", false),
							dtoMiscellaneous2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("MISCELLANEOUS_ENTRY_NOT_DELETED", false),
							dtoMiscellaneous2);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.LIST_IS_EMPTY, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete Miscellaneous Entry Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@PostMapping("/getAll")
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody DtoSearchActivity dtoSearchActivity)
			throws Exception {
		log.info("Search Miscellaneous Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchActivity = this.serviceMiscellaneous.searchMiscellaneousEntry(dtoSearchActivity);
			responseMessage = displayMessage(dtoSearchActivity, "MISCELLANEOUS_ENTRY_ALL_RECORDS_FOUND",
					"MISCELLANEOUS_ENTRY_RECORDS_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	@PostMapping("/getById")
	public ResponseMessage getMiscellaneousEntryById(HttpServletRequest request,
			@RequestBody DtoMiscellaneous dtoMiscellaneous) throws Exception {
		log.info("Get Miscellaneoud Entry ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoMiscellaneous dtoMiscellaneousObj = serviceMiscellaneous.getById(dtoMiscellaneous.getId());
			responseMessage = displayMessage(dtoMiscellaneousObj, "MISCELLANEOUS_ENTRY_FOUND",
					"MISCELLANEOUS_ENTRY_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get Miscellaneous Entry by Id Method:" + dtoMiscellaneous.getId());
		return responseMessage;
	}

	@PostMapping("miscellaneousIdCheck")
	public ResponseMessage miscellaneousIdCheck(HttpServletRequest request,
			@RequestBody DtoMiscellaneous dtoMiscellaneous) throws Exception {
		log.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoMiscellaneous dtoMiscellaneousObj = serviceMiscellaneous
					.repeatByMiscellaneousId(dtoMiscellaneous.getCodeId());

			if (dtoMiscellaneousObj != null && dtoMiscellaneousObj.getIsRepeat()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("MISCELLANEOUS_ENTRY_RESULT_REPEAT", false),
						dtoMiscellaneousObj);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("MISCELLANEOUS_ENTRY_REPEAT_NOT_FOUND", false),
						dtoMiscellaneousObj);
			}
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping("/getAllMiscellaneousDropDown")
	@ResponseBody
	public ResponseMessage getAllMiscellaneousDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoMiscellaneous> miscellaneousList = serviceMiscellaneous.getAllMiscellaneousDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("MISCELLANEOUS_GET_ALL", false),
						miscellaneousList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
			log.error(e);
		}

		return responseMessage;
	}

	@PostMapping("/getAllMiscellaneousForEmployee")
	public ResponseMessage getAllMiscellaneousForEmployee(HttpServletRequest request,
			@RequestBody DtoSearchActivity dtoSearchActivity) throws Exception {
		log.info("Search Miscellaneous for Employee Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchActivity = this.serviceMiscellaneous.searchMiscellaneousEntryWithValues(dtoSearchActivity);
			responseMessage = displayMessage(dtoSearchActivity, "MISCELLANEOUS_ENTRY_ALL_RECORDS_FOUND",
					"MISCELLANEOUS_ENTRY_RECORDS_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
