package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.PayCodeType;
import com.bti.hcm.model.TimeCode;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoPayCodeType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTimeCode;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryPayCodeType;

@Service("servicePayCode")
public class ServicePayCode {

	/**
	 * @Description LOGGER use for put a logger in PayCode Service
	 */
	static Logger log = Logger.getLogger(ServicePayCode.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in PayCode service
	 */

	@Autowired
	ServiceResponse response;

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in PayCode service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in PayCode service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description RepositoryPayCode Autowired here using annotation of spring for
	 *              access of RepositoryPayCode method in PayCode service
	 */
	@Autowired(required = false)
	RepositoryPayCode repositoryPayCode;

	@Autowired(required = false)
	RepositoryPayCodeType repositoryPayCodeType;

	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;

	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;

	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	/**
	 * @param dtoPayCode
	 * @return
	 */
	public DtoPayCode saveOrUpdate(DtoPayCode dtoPayCode) {
		try {
			log.info("saveOrUpdatePosition Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
			PayCode payCode = null;
			if (dtoPayCode.getId() != null && dtoPayCode.getId() > 0) {
				payCode = repositoryPayCode.findByIdAndIsDeleted(dtoPayCode.getId(), false);
				payCode.setUpdatedBy(loggedInUserId);
				payCode.setUpdatedDate(new Date());
				payCode.setBaseOnPayCodeId(dtoPayCode.getBaseOnPayCodeId());
				payCode.setBaseOnPayCode(dtoPayCode.getBaseOnPayCode());
				payCode.setBaseOnPayCodeAmount(dtoPayCode.getBaseOnPayCodeAmount());
				payCode.setPayFactor(dtoPayCode.getPayFactor());
				payCode.setPayRate(dtoPayCode.getPayRate());
				payCode.setUnitofPay(dtoPayCode.getUnitofPay());
				payCode.setPayperiod(dtoPayCode.getPayperiod());
				payCode.setTypeField(dtoPayCode.getPayTypeId()); // ME New
				payCode.setRoundOf(dtoPayCode.getRoundOf()); // ME New
				payCode.setInActive(dtoPayCode.getInActive());

				payCode.setBaseOnPayCodeId(dtoPayCode.getBaseOnPayCodeId());

				payCode = repositoryPayCode.saveAndFlush(payCode);

				if (dtoPayCode.getInActive() != null) {
					List<EmployeePayCodeMaintenance> employeePayCodeMaintananceList = repositoryEmployeePayCodeMaintenance
							.findBypayCode(dtoPayCode.getId());
					if (employeePayCodeMaintananceList != null) {
						for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintananceList) {
							repositoryEmployeePayCodeMaintenance.deleteSingleEmployeePayCodeMaintenance(
									dtoPayCode.getInActive(), employeePayCodeMaintenance.getId());
						}
					}
				}

			} else { // Update
				payCode = new PayCode();
				payCode.setCreatedDate(new Date());

				Integer rowId = repositoryPayCode.getCountOfTotaPayCode();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				payCode.setRowId(increment);
				PayCodeType payCodeType = null;

				if (dtoPayCode.getPayCodeId() != null) {
					payCodeType = repositoryPayCodeType.findOne(dtoPayCode.getPayCodeTypeId());
				}
				payCode.setPayCodeId(dtoPayCode.getPayCodeId());
				payCode.setDescription(dtoPayCode.getDescription());
				payCode.setArbicDescription(dtoPayCode.getArbicDescription());
				payCode.setPayType(payCodeType);
				payCode.setBaseOnPayCode(dtoPayCode.getBaseOnPayCode());
				payCode.setBaseOnPayCodeAmount(dtoPayCode.getBaseOnPayCodeAmount());
				payCode.setPayFactor(dtoPayCode.getPayFactor());
				payCode.setPayRate(dtoPayCode.getPayRate());
				payCode.setUnitofPay(dtoPayCode.getUnitofPay());
				payCode.setPayperiod(dtoPayCode.getPayperiod());
				payCode.setTypeField(dtoPayCode.getPayTypeId()); // ME New
				payCode.setRoundOf(dtoPayCode.getRoundOf()); // ME New
				payCode.setInActive(dtoPayCode.getInActive());
				payCode.setBaseOnPayCodeId(dtoPayCode.getBaseOnPayCodeId());

				payCode = repositoryPayCode.saveAndFlush(payCode);

				List<PayCode> paycodeList = new ArrayList<>();
				List<DeductionCode> deductionCodeList = repositoryDeductionCode.findByIsDeletedAndAllPaycode();
				for (DeductionCode deductionCode : deductionCodeList) {
					if (deductionCode.getId() != null) {
						paycodeList.add(payCode);
						deductionCode.setPayCodeList(paycodeList);
						repositoryDeductionCode.saveAndFlush(deductionCode);
					}
					for (PayCode payCode2 : deductionCode.getPayCodeList()) {
						paycodeList.add(payCode2);
					}

				}

			}

			log.debug("Position is:" + payCode.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayCode;

	}

	/**
	 * @param ids
	 * @return
	 */
	public DtoPayCode delete(List<Integer> ids) {
		log.info("delete Pay code Method");
		DtoPayCode dtoPayCode = new DtoPayCode();
		dtoPayCode.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAY_CODE_DELETED", false));
		dtoPayCode
				.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAY_CODE_ASSOCIATED", false));
		List<DtoPayCode> deletePayCode = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage
				.append(response.getMessageByShortAndIsDeleted("PAY_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer payCodeId : ids) {
				PayCode payCode = repositoryPayCode.findOne(payCodeId);
				if (payCode.getListTimeCode().isEmpty() && payCode.getListEmployeePayCodeMaintenance().isEmpty()
						&& payCode.getListActivateEmployeePostDatePayRate().isEmpty()
						&& payCode.getListBuildPayrollCheckByPayCodes().isEmpty()) {
					DtoPayCode dtoPayCode1 = new DtoPayCode();
					dtoPayCode1.setId(payCode.getId());
					dtoPayCode1.setDescription(payCode.getDescription());
					repositoryPayCode.deleteSinglePayCode(true, loggedInUserId, payCodeId);
					deletePayCode.add(dtoPayCode1);
				} else {
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoPayCode.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoPayCode.setMessageType("");
			}
			dtoPayCode.setDelete(deletePayCode);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete PayCode :" + dtoPayCode.getId());
		return dtoPayCode;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("search PayCode Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";
				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
					if (dtoSearch.getSortOn().equals("payCodeId") || dtoSearch.getSortOn().equals("description")
							|| dtoSearch.getSortOn().equals("arbicDescription")
							|| dtoSearch.getSortOn().equals("payType") || dtoSearch.getSortOn().equals("payperiod")
							|| dtoSearch.getSortOn().equals("unitofPay")) {
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}
				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
				dtoSearch.setTotalCount(
						this.repositoryPayCode.predictivePayCodeSearchTotalCount("%" + searchWord + "%"));
				List<PayCode> payCodeList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						payCodeList = this.repositoryPayCode.predictivePayCodeSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						payCodeList = this.repositoryPayCode.predictivePayCodeSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						payCodeList = this.repositoryPayCode.predictivePayCodeSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}

				}

				if (payCodeList != null && !payCodeList.isEmpty()) {
					List<DtoPayCode> dtoPayCodeList = new ArrayList<DtoPayCode>();

					DtoPayCode dtoPayCode = null;

					for (PayCode payCode : payCodeList) {
						List<TimeCode> timeCodeList = payCode.getListTimeCode();
						dtoPayCode = new DtoPayCode(payCode);
						dtoPayCode.setId(payCode.getId());
						dtoPayCode.setPayCodeId(payCode.getPayCodeId());
						dtoPayCode.setDescription(payCode.getDescription());
						dtoPayCode.setArbicDescription(payCode.getArbicDescription());

						if (payCode.getPayType() != null) {
							dtoPayCode.setPayCodeTypeId(payCode.getPayType().getId());
							dtoPayCode.setPayCodeTypeDesc(payCode.getPayType().getDesc());
							dtoPayCode.setArbicDescription(payCode.getPayType().getArabicDesc());
						}
						dtoPayCode.setArbicDescription(payCode.getArbicDescription());
						dtoPayCode.setBaseOnPayCode(payCode.getBaseOnPayCode());
						dtoPayCode.setBaseOnPayCodeAmount(payCode.getBaseOnPayCodeAmount());
						dtoPayCode.setPayFactor(payCode.getPayFactor());
						dtoPayCode.setPayRate(payCode.getPayRate());
						dtoPayCode.setUnitofPay(payCode.getUnitofPay());
						dtoPayCode.setPayperiod(payCode.getPayperiod());
						dtoPayCode.setInActive(payCode.isInActive());
						dtoPayCode.setBaseOnPayCodeId(payCode.getBaseOnPayCodeId());
						dtoPayCode.setRoundOf(payCode.getRoundOf());		//ME
						dtoPayCode.setPayTypeId(payCode.getTypeField());	//ME
						
						if (payCode.getListTimeCode() == null) {
							List<DtoTimeCode> dtoTimeCodeList = new ArrayList<>();
							dtoPayCode.setListTimeCode(dtoTimeCodeList);
						}

						for (TimeCode timeCode : timeCodeList) {
							DtoTimeCode dtoTimeCode = new DtoTimeCode();
							dtoTimeCode.setId(timeCode.getId());
							dtoTimeCode.setDesc(timeCode.getDesc());
							dtoTimeCode.setArbicDesc(timeCode.getArbicDesc());
							dtoTimeCode.setAccrualPeriod(timeCode.getAccrualPeriod());
							dtoTimeCode.setInActive(timeCode.isInActive());
							dtoPayCode.getListTimeCode().add(dtoTimeCode);
						}
						dtoPayCodeList.add(dtoPayCode);
					}
					dtoSearch.setRecords(dtoPayCodeList);
				}
			}
			log.debug("Search PayCode Size is:" + dtoSearch.getTotalCount());

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @param id
	 * @return
	 */
	public DtoPayCode getById(int id) {
		log.info("getById Method");
		DtoPayCode dtoPayCode = new DtoPayCode();
		if (id > 0) {
			PayCode payCode = repositoryPayCode.findByIdAndIsDeleted(id, false);
			if (payCode != null) {
				dtoPayCode.setPayCodeId(payCode.getPayCodeId());
				dtoPayCode.setId(payCode.getId());
				dtoPayCode.setArbicDescription(payCode.getArbicDescription());
				dtoPayCode.setBaseOnPayCode(payCode.getBaseOnPayCode());
				dtoPayCode.setBaseOnPayCodeAmount(payCode.getBaseOnPayCodeAmount());
				dtoPayCode.setDescription(payCode.getDescription());
				dtoPayCode.setPayCodeTypeId(payCode.getPayType().getId());
				dtoPayCode.setIsActive(false);
				dtoPayCode.setPayperiod(payCode.getPayperiod());
				dtoPayCode.setPayFactor(payCode.getPayFactor());
				dtoPayCode.setPayRate(payCode.getPayRate());
				dtoPayCode.setUnitofPay(payCode.getUnitofPay());
				dtoPayCode.setInActive(payCode.isInActive());
				dtoPayCode.setBaseOnPayCodeId(payCode.getBaseOnPayCodeId());
				dtoPayCode.setPayTypeId(payCode.getTypeField()); // ME New
				dtoPayCode.setRoundOf(payCode.getRoundOf()); // ME New
			} else {
				dtoPayCode.setMessageType("PAY_CODE_NOT_GETTING");
			}
		} else {
			dtoPayCode.setMessageType("INVALID_PAY_CODE_ID");

		}
		log.debug("payCode By Id is:" + dtoPayCode.getId());
		return dtoPayCode;
	}

	/**
	 * @param payCodeId
	 * @return
	 */
	public DtoPayCode repeatBypayCodeId(String payCodeId) {
		log.info("repeatBypayCodeId Method");
		DtoPayCode dtoPayCode = new DtoPayCode();
		try {
			List<PayCode> payCodeList = repositoryPayCode.findByPayCodeId(payCodeId);
			if (payCodeList != null && !payCodeList.isEmpty()) {
				dtoPayCode.setIsRepeat(true);
			} else {
				dtoPayCode.setIsRepeat(false);
			}

		} catch (Exception e) {
			log.error(e);
		}

		return dtoPayCode;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchPositionPayCodeId(DtoSearch dtoSearch) {
		try {
			log.info("searchPositionPayCodeId Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				List<PayCode> positionPayCodeIdList = new ArrayList<>();

				List<DtoPayCode> positionPayCod = new ArrayList<>();

				positionPayCodeIdList = this.repositoryPayCode
						.predictivePositionClassIdSearchWithPagination("%" + searchWord + "%");
				if (!positionPayCodeIdList.isEmpty()) {

					for (PayCode payCode : positionPayCodeIdList) {
						DtoPayCode dtoPayCode = new DtoPayCode();
						dtoPayCode.setId(payCode.getId());
						dtoPayCode.setPayCodeId(payCode.getPayCodeId());
						dtoPayCode.setPayRate(payCode.getPayRate());
						dtoPayCode.setBaseOnPayCodeAmount(payCode.getBaseOnPayCodeAmount());
						dtoPayCode.setBaseOnPayCodeId(payCode.getBaseOnPayCodeId());
						if (dtoSearch.getId() != null) {
							if (dtoSearch.getId() != payCode.getId()) {
								positionPayCod.add(dtoPayCode);
							}
						}

					}

					dtoSearch.setTotalCount(positionPayCodeIdList.size());
				}
				dtoSearch.setRecords(positionPayCod);
				;
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllPayCodeId(DtoSearch dtoSearch) {
		try {

			log.info("search PayCode Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";

				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
					if (dtoSearch.getSortOn().equals("payCodeId") || dtoSearch.getSortOn().equals("description")
							|| dtoSearch.getSortOn().equals("arbicDescription")
							|| dtoSearch.getSortOn().equals("payType") || dtoSearch.getSortOn().equals("payperiod")
							|| dtoSearch.getSortOn().equals("unitofPay")) {
						condition = dtoSearch.getSortOn();

					} else {
						condition = "id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}
				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");

				}
				dtoSearch.setTotalCount(
						this.repositoryPayCode.predictivePayCodeSearchTotalCount("%" + searchWord + "%"));
				List<PayCode> payCodeList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						payCodeList = this.repositoryPayCode.predictiveAllPayCodeIdSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						payCodeList = this.repositoryPayCode.predictiveAllPayCodeIdSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						payCodeList = this.repositoryPayCode.predictiveAllPayCodeIdSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}

				}

				if (payCodeList != null && !payCodeList.isEmpty()) {
					List<DtoPayCode> dtoPayCodeList = new ArrayList<DtoPayCode>();
					DtoPayCode dtoPayCode = null;
					for (PayCode payCode : payCodeList) {
						dtoPayCode = new DtoPayCode(payCode);
						dtoPayCode.setId(payCode.getId());
						dtoPayCode.setPayCodeId(payCode.getPayCodeId());
						dtoPayCode.setDescription(payCode.getDescription());
						dtoPayCode.setPayRate(payCode.getPayRate());
						dtoPayCode.setBaseOnPayCodeAmount(payCode.getBaseOnPayCodeAmount());
						dtoPayCode.setArbicDescription(payCode.getArbicDescription());
						dtoPayCode.setBaseOnPayCodeId(payCode.getBaseOnPayCodeId());
						dtoPayCodeList.add(dtoPayCode);
					}
					dtoSearch.setRecords(dtoPayCodeList);
				}
			}
			log.debug("Search PayCode Size is:" + dtoSearch.getTotalCount());

		} catch (Exception e) {
			log.error(e);
		}

		return dtoSearch;
	}

	public List<DtoPayCode> getAllPayCodeDropDownList() {
		log.info("getAllPayCodeDropDownList  Method");
		List<DtoPayCode> payCodeList = new ArrayList<>();
		try {
			List<PayCode> list = repositoryPayCode.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (PayCode payCode : list) {
					DtoPayCode dtoPayCode = new DtoPayCode();
					dtoPayCode.setId(payCode.getId());
					dtoPayCode.setDescription(payCode.getDescription());
					dtoPayCode.setPayCodeId(payCode.getPayCodeId());
					payCodeList.add(dtoPayCode);
				}
			}
			log.debug("PayCode is:" + payCodeList.size());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return payCodeList;
	}

	public List<DtoPayCode> getAllPayCodeInActiveList() {
		log.info("getAllPayCodeInActiveList  Method");
		List<DtoPayCode> payCodeList = new ArrayList<>();
		try {
			List<PayCode> list = repositoryPayCode.findByIsDeletedAndInActive(false, false);
			if (list != null && !list.isEmpty()) {
				for (PayCode payCode : list) {
					DtoPayCode dtoPayCode = new DtoPayCode();
					dtoPayCode.setId(payCode.getId());
					dtoPayCode.setDescription(payCode.getDescription());
					dtoPayCode.setPayCodeId(payCode.getPayCodeId());
					dtoPayCode.setArbicDescription(payCode.getArbicDescription());
					dtoPayCode.setInActive(payCode.isInActive());
					dtoPayCode.setPayperiod(payCode.getPayperiod());
					dtoPayCode.setUnitofPay(payCode.getUnitofPay());
					dtoPayCode.setPayRate(payCode.getPayRate());
					dtoPayCode.setPayFactor(payCode.getPayFactor());
					dtoPayCode.setBaseOnPayCodeId(payCode.getBaseOnPayCodeId());
					DtoPayCodeType dtoPayCode2 = new DtoPayCodeType();
					if (payCode.getPayType() != null) {
						dtoPayCode.setPayCodeTypeId(payCode.getPayType().getId());
						dtoPayCode.setPayCodeTypeDesc(payCode.getPayType().getDesc());
						dtoPayCode.setPayCodeTypeArabicDesc(payCode.getArbicDescription());
					}
					dtoPayCode.setDtoPayCodeType(dtoPayCode2);
					dtoPayCode.setBaseOnPayCodeAmount(payCode.getBaseOnPayCodeAmount());
					dtoPayCode.setBaseOnPayCode(payCode.getBaseOnPayCode());
					
					dtoPayCode.setRoundOf(payCode.getRoundOf());		//ME
					dtoPayCode.setPayTypeId(payCode.getTypeField());	//ME 
					
					payCodeList.add(dtoPayCode);
				}
			}
			log.debug("PayCode is:" + payCodeList.size());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return payCodeList;
	}

	public DtoSearch searchAllPayCodeId(DtoSearch dtoSearch) {
		try {
			log.info("searchAllPayCodeId Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				List<PayCode> payCodeList = new ArrayList<>();
				payCodeList = this.repositoryPayCode.predictiveSearchAllPayCodeIdWithPagination("%" + searchWord + "%");
				if (!payCodeList.isEmpty()) {
					if (payCodeList != null && !payCodeList.isEmpty()) {
						List<DtoPayCode> dtoPayCode = new ArrayList<>();
						DtoPayCode dtoPayCode1 = null;
						for (PayCode payCode : payCodeList) {
							dtoPayCode1 = new DtoPayCode(payCode);
							dtoPayCode1.setId(payCode.getId());
							dtoPayCode1.setDescription(payCode.getDescription());
							dtoPayCode1.setPayFactor(payCode.getPayFactor());
							dtoPayCode1.setPayRate(payCode.getPayRate());
							dtoPayCode1.setBaseOnPayCode(payCode.getBaseOnPayCode());
							dtoPayCode1.setBaseOnPayCodeAmount(payCode.getBaseOnPayCodeAmount());
							dtoPayCode1.setBaseOnPayCodeId(payCode.getBaseOnPayCodeId());
							dtoPayCode.add(dtoPayCode1);
						}
						dtoSearch.setRecords(dtoPayCode);
					}
					dtoSearch.setTotalCount(payCodeList.size());
				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoSearch getAllPaycodeforDeduction(DtoPayCode dtoPayCode) {
		DtoSearch dtoSearch = new DtoSearch();
		List<PayCode> listPayCode = this.repositoryPayCode.findByIsDeletedAndInActive(false, false);
		List<DtoPayCode> listDtoPayCode = new ArrayList<>();
		DtoPayCode dtoPayCodes = null;
		if (listPayCode != null && !listPayCode.isEmpty()) {
			for (PayCode payCode : listPayCode) {
				dtoPayCodes = new DtoPayCode(payCode);
				dtoPayCodes.setId(payCode.getId());
				dtoPayCodes.setPayCodeId(payCode.getPayCodeId());
				dtoPayCodes.setDescription(payCode.getDescription());
				dtoPayCodes.setAmount(payCode.getBaseOnPayCodeAmount());
				dtoPayCodes.setPayFactor(payCode.getPayFactor());
				dtoPayCodes.setPayRate(payCode.getPayRate());
				dtoPayCodes.setBaseOnPayCodeId(payCode.getBaseOnPayCodeId());
				listDtoPayCode.add(dtoPayCodes);
				dtoSearch.setRecords(listDtoPayCode);
			}
		}
		return dtoSearch;
	}

}
