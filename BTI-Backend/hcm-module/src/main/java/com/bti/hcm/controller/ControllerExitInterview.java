package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoExitInterview;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceExitInterview;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/exitInterview")
public class ControllerExitInterview extends BaseController{
	
	/**
	 * 
	 */
	@Autowired
	ServiceExitInterview serviceExitInterview;
	
	/**
	 * 
	 */
	@Autowired
	
	private static final Logger LOGGER = Logger.getLogger(ControllerExitInterview.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoExitInterview dtoExitInterview) throws Exception{
		LOGGER.info("Create ExitInterview Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoExitInterview = serviceExitInterview.saveOrUpdate(dtoExitInterview);
			responseMessage=displayMessage(dtoExitInterview, "EXIT_INTERVIEW_CREATED", "EXIT_INTERVIEW_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create ExitInterview Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoExitInterview dtoTimeCode) throws Exception {
		LOGGER.info("Update ExitInterview Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTimeCode = serviceExitInterview.saveOrUpdate(dtoTimeCode);
			responseMessage=displayMessage(dtoTimeCode, "EXIT_INTERVIEW_UPDATED_SUCCESS", "EXIT_INTERVIEW_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update ExitInterview Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoExitInterview dtoTimeCode) throws Exception {
		LOGGER.info("Delete ExitInterview Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTimeCode.getIds() != null && !dtoTimeCode.getIds().isEmpty()) {
				DtoExitInterview dtoPosition1 = serviceExitInterview.delete(dtoTimeCode.getIds());
				if(dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EXIT_INTERVIEW_DELETED", false), dtoPosition1);

				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoPosition1.getMessageType());
					btiMessageHcm.setMessageShort("EXIT_INTERVIEW_NOT_DELETE_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoPosition1);

				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete ExitInterview Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  "id" : 1
	}
	@param response{
				
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"timeCodeId": "1",
		"inActive": false,
		"desc": null,
		"arbicDesc": null,
		"accrualPeriod": null,
		"timeType": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"delete": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Position fetched successfully.",
		"messageShort": "EXIT_INTERVIEW_GET_DETAIL"
		
		}
		}
	 * @param dtoExitInterview
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoExitInterview dtoExitInterview) throws Exception {
		LOGGER.info("Get TimeCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoExitInterview dtoPayCodeObj = serviceExitInterview.getById(dtoExitInterview.getId());
			responseMessage=displayMessage(dtoPayCodeObj, MessageConstant.EXIT_INTERVIEW_GET_DETAIL, MessageConstant.EXIT_INTERVIEW_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get TimeCode by Id Method:"+dtoExitInterview.getId());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  "payCodeId" : "1"
	}
	@param request{
	"code": 302,
	"status": "FOUND",
	"result": {
	"id": null,
	"payCodeId": null,
	"description": null,
	"arbicDescription": null,
	"payType": null,
	"baseOnPayCode": null,
	"baseOnPayCodeAmount": null,
	"payFactor": null,
	"payRate": null,
	"unitofPay": null,
	"payperiod": null,
	"inActive": false,
	"pageNumber": null,
	"pageSize": null,
	"ids": null,
	"isActive": null,
	"messageType": null,
	"message": null,
	"deleteMessage": null,
	"associateMessage": null,
	"delete": null,
	"isRepeat": true
	},
	"btiMessage": {
	"message": "time Code Id is already created.",
	"messageShort": "EXIT_INTERVIEW_DETAIL_FATECHED"
	}
	}
	 * @param dtoExitInterview
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/exitInterviewIdcheck", method = RequestMethod.POST)
	public ResponseMessage exitInterviewIdcheck(HttpServletRequest request, @RequestBody DtoExitInterview dtoExitInterview) throws Exception {
		LOGGER.info("timeCodeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoExitInterview dtoPayCodeObj = serviceExitInterview.repeatByExitInterviewId(dtoExitInterview.getExitInterviewId());
			responseMessage=displayMessage(dtoPayCodeObj, "EXIT_INTERVIEW_REPEAT_FOUND", "EXIT_INTERVIEW_REPEAT_DEPARTMENTID_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{
		  "sortOn" : "timeCodeId",
		  "sortBy" : "DESC",
		 "searchKeyword" : "",
		"pageNumber":"0",
		"pageSize":"10"
		}
		 @param response{
			"code": 200,
			"status": "OK",
			"result": {
			"searchKeyword": "",
			"pageNumber": 0,
			"pageSize": 10,
			"sortOn": "timeCodeId",
			"sortBy": "DESC",
			"totalCount": 1,
			"records": [
			  {
			"id": 1,
			"timeCodeId": "1",
			"inActive": false,
			"desc": "1",
			"arbicDesc": "1",
			"accrualPeriod": 1,
			"timeType": 1,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"delete": null,
			"isRepeat": null
			}
			],
			},
			"btiMessage": {
			"message": "time code list fetched successfully.",
			"messageShort": "Time_CODE_GET_ALL"
			}
			}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search search Time code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceExitInterview.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EXIT_INTERVIEW_GET_DETAIL", "EXIT_INTERVIEW_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
					
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search search Time code Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{
			 "searchKeyword" : ""
	}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchExitInterviewId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchExitInterviewId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Time Code Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceExitInterview.searchExitInterviewId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EXIT_INTERVIEW_GET_DETAIL", "EXIT_INTERVIEW_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search search Time code Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllExitInterviewInActive", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllExitInterviewInActive(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoExitInterview> exitInterviewList = serviceExitInterview.getAllExitInterviewInActiveList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_GET_ALL", false), exitInterviewList);
			}else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return responseMessage;
	}
}
