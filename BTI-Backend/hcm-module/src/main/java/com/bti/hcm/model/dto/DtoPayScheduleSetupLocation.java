/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PayScheduleSetupLocation;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO PayScheduleSetupLocatio class having getter and setter for fields (POJO)
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoPayScheduleSetupLocation extends DtoBase{
	
	private Integer payScheduleSetupLocationId;
	private String locationName;
	private Boolean payScheduleStatus;
	private Integer locationId;
	private Integer payScheduleSetupId;
	private List<DtoLocation> dtoLocations;
	private List<DtoPayScheduleSetupLocation> deleteDtoPayScheduleSetupLocation;
	
	
	public Integer getPayScheduleSetupLocationId() {
		return payScheduleSetupLocationId;
	}
	public void setPayScheduleSetupLocationId(Integer payScheduleSetupLocationId) {
		this.payScheduleSetupLocationId = payScheduleSetupLocationId;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Boolean getPayScheduleStatus() {
		return payScheduleStatus;
	}
	public void setPayScheduleStatus(Boolean payScheduleStatus) {
		this.payScheduleStatus = payScheduleStatus;
	}
	public Integer getLocationId() {
		return locationId;
	}
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}
	public Integer getPayScheduleSetupId() {
		return payScheduleSetupId;
	}
	public void setPayScheduleSetupId(Integer payScheduleSetupId) {
		this.payScheduleSetupId = payScheduleSetupId;
	}
	public List<DtoPayScheduleSetupLocation> getDeleteDtoPayScheduleSetupLocation() {
		return deleteDtoPayScheduleSetupLocation;
	}
	public void setDeleteDtoPayScheduleSetupLocation(List<DtoPayScheduleSetupLocation> deleteDtoPayScheduleSetupLocation) {
		this.deleteDtoPayScheduleSetupLocation = deleteDtoPayScheduleSetupLocation;
	}
	public DtoPayScheduleSetupLocation() {
	}
	
	
	public DtoPayScheduleSetupLocation(PayScheduleSetupLocation payScheduleSetupLocation) {}
	public List<DtoLocation> getDtoLocations() {
		return dtoLocations;
	}
	public void setDtoLocations(List<DtoLocation> dtoLocations) {
		this.dtoLocations = dtoLocations;
	}
	
	
	

}
