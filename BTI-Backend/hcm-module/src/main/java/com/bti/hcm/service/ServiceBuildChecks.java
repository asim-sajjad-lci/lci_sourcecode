package com.bti.hcm.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Batches;
import com.bti.hcm.model.BuildChecks;
import com.bti.hcm.model.BuildPayrollCheckByBatches;
import com.bti.hcm.model.BuildPayrollCheckByBenefits;
import com.bti.hcm.model.BuildPayrollCheckByDeductions;
import com.bti.hcm.model.BuildPayrollCheckByPayCodes;
import com.bti.hcm.model.Default;
import com.bti.hcm.model.Department;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.TransactionEntry;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoBuildChecks;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByBenefits;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByDeductions;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByPayCodes;
import com.bti.hcm.model.dto.DtoDefaulCheck;
import com.bti.hcm.model.dto.DtoDefault;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoEmployeeMasterHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBatches;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByBatches;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByBenefits;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByDeductions;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByPayCodes;
import com.bti.hcm.repository.RepositoryDefault;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryTransactionEntry;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceBuildChecks ")
@CacheConfig(cacheNames={"employeeMaster"})
public class ServiceBuildChecks {
	

	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceBuildChecks.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryBuildChecks repositoryBuildChecks;
	
	@Autowired
	RepositoryDefault repositoryDefault;
	
	@Autowired
	RepositoryDepartment repositoryDepartment;
	
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired
	RepositoryTransactionEntry repositoryTransactionEntry;
	
	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;
	
	@Autowired
	RepositoryBatches repositoryBatches;
	
	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;
	
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;
	
	@Autowired
	RepositoryBuildPayrollCheckByBenefits repositoryBuildPayrollCheckByBenefits;
	
	
	@Autowired
	RepositoryBuildPayrollCheckByDeductions repositoryBuildPayrollCheckByDeductions;
	
	
	@Autowired
	RepositoryBuildPayrollCheckByBatches  repositoryBuildPayrollCheckByBatches;
	
	@Autowired
	RepositoryBuildPayrollCheckByPayCodes repositoryBuildPayrollCheckByPayCodes;
	
	public DtoBuildChecks saveOrUpdate(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userId"));
		try {
			
			BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				/*List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				for (TransactionEntry transactionEntry : listOfTranscationEntry) {
					repositoryTransactionEntry.deleteSingleTranscationEntryRow(true, loggedInUserId, transactionEntry.getId());
					repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(true, loggedInUserId, transactionEntry.getId());
				}
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
						if(!employeePayCodeMaintenance.isEmpty()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
								TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
							//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
								Integer increment11=0;
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(buildChecks.getBatches());
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setFromBuild(true);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
								TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
								transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
								transactionEntryDetail.setEmployeeMaster(employeeMaster2);
								transactionEntryDetail.setDepartment(department);
								transactionEntryDetail.setTransactionEntry(transactionEntry);
								transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
								transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
								transactionEntryDetail.setFromBuild(true);
								transactionEntryDetail.setTransactionType((short)1);
								transactionEntryDetail.setIsDeleted(false);
								transactionEntryDetail.setCreatedDate(new Date());
								transactionEntryDetail.setUpdatedBy(loggedInUserId);
								transactionEntryDetail.setUpdatedDate(new Date());
								transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
								transactionEntryDetail.setUpdatedRow(new Date());
								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
							}
						}
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(buildChecks.getBatches());
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(true);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setFromBuild(true);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setUpdatedRow(new Date());
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(buildChecks.getBatches());
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										
									}
								}
								

						
					}
						
					
					
				}*/
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=null) {
					if(rowId1!=0) {
						increment1= rowId1+1;
					}else {
						increment1=1;
					}
				}else {
					increment1=1;
				}
				
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(batches);
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(true);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setFromBuild(true);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setUpdatedRow(new Date());
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(batches);
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											
										}
									}
									

							
						}
							
						
						
					}
			}

		
			
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}
	
	
	public DtoBuildChecks delete(List<Integer> ids) {
		log.info("delete Buildcheck Method");
		DtoBuildChecks dtoBuildChecks = new DtoBuildChecks();
		dtoBuildChecks.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("BUILD_CHECK_DELETED", false));
		dtoBuildChecks.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("BUILD_CHECK_ASSOCIATED", false));
		List<DtoBuildChecks> deleteDtoBuildChecks = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("BUILD_CHECK_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer planId : ids) {
				BuildChecks buildChecks = repositoryBuildChecks.findOne(planId);
	
				if(buildChecks.getListBuildPayrollCheckByBatches().isEmpty()&& buildChecks.getListBuildPayrollCheckByBenefits().isEmpty()
				&& buildChecks.getListBuildPayrollCheckByDeductions().isEmpty() && buildChecks.getListBuildPayrollCheckByPayCodes().isEmpty()) {
						
						DtoBuildChecks dtoBuildChecks2=new DtoBuildChecks();
						dtoBuildChecks.setId(buildChecks.getId());
						repositoryBuildChecks.deleteSingleBuildChecks(true, loggedInUserId, planId);
						deleteDtoBuildChecks.add(dtoBuildChecks2);	
						
				}else {
					inValidDelete = true;
				
				}
				}

			if(inValidDelete){
				dtoBuildChecks.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoBuildChecks.setMessageType("");
				
			}
			
				
			dtoBuildChecks.setDelete(deleteDtoBuildChecks);
		} 
		catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeePayCodeMaintenance"+ " :"+dtoBuildChecks.getId());
		return dtoBuildChecks;
	}
	
	
	
	
	
	public DtoBuildChecks getById(int id) {
		DtoBuildChecks dtoBuildChecks  = new DtoBuildChecks();
		try {
			if (id > 0) {
				BuildChecks buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(id, false);
				if (buildChecks != null) {
					dtoBuildChecks = new DtoBuildChecks(buildChecks);
					
					DtoDefault default1=new DtoDefault();
					if(buildChecks.getDefault1()!=null) {
						default1.setId(buildChecks.getDefault1().getId());
						default1.setDefaultID(buildChecks.getDefault1().getDefaultID());
						default1.setDesc(buildChecks.getDefault1().getDesc());
						dtoBuildChecks.setDefault1(default1);
					}
					dtoBuildChecks.setId(buildChecks.getId());
					dtoBuildChecks.setDescription(buildChecks.getDescription());
					dtoBuildChecks.setCheckByUserId(buildChecks.getCheckByUserId());
					dtoBuildChecks.setCheckDate(buildChecks.getCheckDate());
					dtoBuildChecks.setTypeofPayRun(buildChecks.getTypeofPayRun());
					dtoBuildChecks.setDateFrom(buildChecks.getDateFrom());
					dtoBuildChecks.setDateTo(buildChecks.getDateTo());
					dtoBuildChecks.setWeekly(buildChecks.getWeekly());
					dtoBuildChecks.setBiweekly(buildChecks.getBiweekly());
					dtoBuildChecks.setSemiannually(buildChecks.getSemiannually());
					dtoBuildChecks.setMonthly(buildChecks.getMonthly());
					dtoBuildChecks.setQuarterly(buildChecks.getQuarterly());
					dtoBuildChecks.setSemimonthly(buildChecks.getSemimonthly());
					dtoBuildChecks.setMonthly(buildChecks.getMonthly());
					dtoBuildChecks.setAnnually(buildChecks.getAnnually());
					dtoBuildChecks.setDailyMisc(buildChecks.getDailyMisc());
					dtoBuildChecks.setAllEmployees(buildChecks.getAllEmployees());
					dtoBuildChecks.setFromEmployeeId(buildChecks.getFromEmployeeId());
					dtoBuildChecks.setToEmployeeId(buildChecks.getToEmployeeId());
					dtoBuildChecks.setAllDepertment(buildChecks.getAllDepertment());
					dtoBuildChecks.setFromDepartmentId(buildChecks.getFromDepartmentId());
					dtoBuildChecks.setToDepartmentId(buildChecks.getToDepartmentId());
				} 
			} else {
				dtoBuildChecks.setMessageType("INVALID_ID");

			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}	
	
	@Async
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		
		try {

			log.info("build checks Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("description")|| dtoSearch.getSortOn().equals("checkByUserId")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				
				dtoSearch.setTotalCount(this.repositoryBuildChecks.predictiveBuildCheckSearchTotalCount("%"+searchWord+"%"));
				List<BuildChecks> buildChecksList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						buildChecksList = this.repositoryBuildChecks.predictiveBuildcheckSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						buildChecksList = this.repositoryBuildChecks.predictiveBuildcheckSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						buildChecksList = this.repositoryBuildChecks.predictiveBuildcheckSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(buildChecksList != null && !buildChecksList.isEmpty()){
					List<DtoBuildChecks> dtoBuildChecksList = new ArrayList<>();
					DtoBuildChecks dtoBuildChecks=null;
					for (BuildChecks buildChecks : buildChecksList) {
						dtoBuildChecks = new DtoBuildChecks(buildChecks);
				
						DtoDefault default1=new DtoDefault();
						if(buildChecks.getDefault1()!=null) {
							default1.setId(buildChecks.getDefault1().getId());
							default1.setDefaultID(buildChecks.getDefault1().getDefaultID());
							default1.setDesc(buildChecks.getDefault1().getDesc());
							dtoBuildChecks.setDefault1(default1);
						}
						dtoBuildChecks.setId(buildChecks.getId());
						dtoBuildChecks.setDescription(buildChecks.getDescription());
						dtoBuildChecks.setCheckByUserId(buildChecks.getCheckByUserId());
						dtoBuildChecks.setCheckDate(buildChecks.getCheckDate());
						dtoBuildChecks.setTypeofPayRun(buildChecks.getTypeofPayRun());
						dtoBuildChecks.setDateFrom(buildChecks.getDateFrom());
						dtoBuildChecks.setDateTo(buildChecks.getDateTo());
						dtoBuildChecks.setWeekly(buildChecks.getWeekly());
						dtoBuildChecks.setBiweekly(buildChecks.getBiweekly());
						dtoBuildChecks.setSemiannually(buildChecks.getSemiannually());
						dtoBuildChecks.setMonthly(buildChecks.getMonthly());
						dtoBuildChecks.setQuarterly(buildChecks.getQuarterly());
						dtoBuildChecks.setSemimonthly(buildChecks.getSemimonthly());
						dtoBuildChecks.setMonthly(buildChecks.getMonthly());
						dtoBuildChecks.setAnnually(buildChecks.getAnnually());
						dtoBuildChecks.setDailyMisc(buildChecks.getDailyMisc());
						dtoBuildChecks.setAllEmployees(buildChecks.getAllEmployees());
						dtoBuildChecks.setFromEmployeeId(buildChecks.getFromEmployeeId());
						dtoBuildChecks.setToEmployeeId(buildChecks.getToEmployeeId());
						dtoBuildChecks.setAllDepertment(buildChecks.getAllDepertment());
						dtoBuildChecks.setFromDepartmentId(buildChecks.getFromDepartmentId());
						dtoBuildChecks.setToDepartmentId(buildChecks.getToDepartmentId());
						dtoBuildChecksList.add(dtoBuildChecks);
						}
					dtoSearch.setRecords(dtoBuildChecksList);
						}
			log.debug("Search BuildCheck Size is:"+dtoSearch.getTotalCount());
			
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}

	@Transactional
	@Async
	public DtoSearch getByDefaultId(Integer id) {
		DtoSearch dtoSearch = new DtoSearch();
	Default defaultEntity = 	repositoryDefault.findByIdAndIsDeleted(id, false);
	
		try {
			List<DtoBuildChecks> dtoBuildChecks  = new ArrayList<>();
			if (id > 0) {
				List<BuildChecks> buildChecks = repositoryBuildChecks.getByDefaultId(id);
					
				
				
					for (BuildChecks buildChecks2 : buildChecks) {
						DtoBuildChecks dtoBuildCheckss = new DtoBuildChecks(buildChecks2);
						
						DtoDefault default1=new DtoDefault();
						if(buildChecks2.getDefault1()!=null) {
							default1.setId(buildChecks2.getDefault1().getId());
							default1.setDesc(buildChecks2.getDefault1().getDesc());
							default1.setArabicDesc(buildChecks2.getDefault1().getArabicDesc());
							default1.setDefaultID(buildChecks2.getDefault1().getDefaultID());
						}
						if(defaultEntity!=null && !defaultEntity.getListDistribution().isEmpty()) {
							dtoBuildCheckss.setDisable(true);
						}
						
						dtoBuildCheckss.setDefault1(default1);
						dtoBuildCheckss.setId(buildChecks2.getId());
						dtoBuildCheckss.setDescription(buildChecks2.getDescription());
						dtoBuildCheckss.setCheckByUserId(buildChecks2.getCheckByUserId());
						dtoBuildCheckss.setCheckDate(buildChecks2.getCheckDate());
						dtoBuildCheckss.setTypeofPayRun(buildChecks2.getTypeofPayRun());
						dtoBuildCheckss.setDateFrom(buildChecks2.getDateFrom());
						dtoBuildCheckss.setDateTo(buildChecks2.getDateTo());
						dtoBuildCheckss.setWeekly(buildChecks2.getWeekly());
						dtoBuildCheckss.setBiweekly(buildChecks2.getBiweekly());
						dtoBuildCheckss.setSemiannually(buildChecks2.getSemiannually());
						dtoBuildCheckss.setMonthly(buildChecks2.getMonthly());
						dtoBuildCheckss.setQuarterly(buildChecks2.getQuarterly());
						dtoBuildCheckss.setSemimonthly(buildChecks2.getSemimonthly());
						dtoBuildCheckss.setMonthly(buildChecks2.getMonthly());
						dtoBuildCheckss.setAnnually(buildChecks2.getAnnually());
						dtoBuildCheckss.setDailyMisc(buildChecks2.getDailyMisc());
						dtoBuildCheckss.setAllEmployees(buildChecks2.getAllEmployees());
						dtoBuildCheckss.setFromEmployeeId(buildChecks2.getFromEmployeeId());
						dtoBuildCheckss.setToEmployeeId(buildChecks2.getToEmployeeId());
						dtoBuildCheckss.setAllDepertment(buildChecks2.getAllDepertment());
						dtoBuildCheckss.setFromDepartmentId(buildChecks2.getFromDepartmentId());
						dtoBuildCheckss.setToDepartmentId(buildChecks2.getToDepartmentId());
						List<DtoEmployeeMasterHcm> listEmp = new ArrayList<>();
						if(buildChecks2.getListEmployee()!=null && !buildChecks2.getListEmployee().isEmpty())
						{	
							DtoEmployeeMasterHcm dtoEmp = null;
							for(EmployeeMaster empList : buildChecks2.getListEmployee()) {
								dtoEmp = new DtoEmployeeMasterHcm();
								dtoEmp.setEmployeeIndexId(empList.getEmployeeIndexId());
								dtoEmp.setEmployeeFirstName(empList.getEmployeeFirstName());
								dtoEmp.setEmployeeId(empList.getEmployeeId());
								dtoEmp.setEmployeeLastName(empList.getEmployeeLastName());
								listEmp.add(dtoEmp);
							}
							//dtoBuildCheckss.setListEmployeeId(buildChecks2.getListEmployee().stream().map(m->m.getEmployeeIndexId()).collect(Collectors.toList()));	
							
						}
						dtoBuildCheckss.setDtoEmployeeMasters(listEmp);
						if(buildChecks2.getListDepartment()!=null && !buildChecks2.getListDepartment().isEmpty()) {
							
							DtoDepartment dtoDep=null;
							List<DtoDepartment> list=new ArrayList<>();
							for (Department deptList : buildChecks2.getListDepartment()) {
								dtoDep=new DtoDepartment();
								dtoDep.setId(deptList.getId());
								dtoDep.setDepartmentId(deptList.getDepartmentId());
								dtoDep.setDepartmentDescription(deptList.getDepartmentDescription());
								list.add(dtoDep);
								
							}
							
							//dtoBuildCheckss.setListDepartmentId(buildChecks2.getListDepartment().stream().map(d->d.getId()).collect(Collectors.toList()));
							dtoBuildCheckss.setDtoDepartments(list);
						}
						dtoBuildChecks.add(dtoBuildCheckss);
					}
					
					
					
				 
			}
			dtoSearch.setRecords(dtoBuildChecks);
			
		} catch (Exception e) {
			log.error(e);
		}
			return dtoSearch;
		}

	public DtoBuildChecks repeatByDefaultId(Integer id) {
		log.info("repeatByEmployeeId Method");
		DtoBuildChecks dtoBuildChecks = new DtoBuildChecks();
			try {
				List<BuildChecks> listBuildChecks=repositoryBuildChecks.findByDefauiltId(id);
				if(listBuildChecks!=null && !listBuildChecks.isEmpty()) {
					dtoBuildChecks.setIsRepeat(true);
					
				}else {
					dtoBuildChecks.setIsRepeat(false);
				}
			} catch (Exception e) {
				log.error(e);
		}
		
		return dtoBuildChecks;
	}


	public DtoBuildChecks deleteBuild(List<Integer> ids) {
	log.info("delete Default Method");
	DtoBuildChecks dtoBuildChecks = new DtoBuildChecks();
	dtoBuildChecks.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DEFAULT_DELETED", false));
	dtoBuildChecks.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DEFAULT_ASSOCIATED", false));
	List<DtoBuildChecks> deleteDtoBuildChecks = new ArrayList<>();
	int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
	
	boolean inValidDelete = false;
	StringBuilder  invlidDeleteMessage = new StringBuilder();
	invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_NOT_DELETE_ID_MESSAGE", false).getMessage());
	try {
		for (Integer planId : ids) {
			BuildChecks buildChecks = repositoryBuildChecks.findOne(planId);
			if(buildChecks!=null) {
	DtoBuildChecks dtoBuildChecks2=new DtoBuildChecks();
	dtoBuildChecks.setId(buildChecks.getId());
		
		
	     
	repositoryBuildChecks.deleteSingleBuildCheckss(true, loggedInUserId, planId);
	deleteDtoBuildChecks.add(dtoBuildChecks2);	
			}else {
	inValidDelete = true;
}
			
				
			}

		if(inValidDelete){
			invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
			dtoBuildChecks.setMessageType(invlidDeleteMessage.toString());
			
		}
		if(!inValidDelete){
			dtoBuildChecks.setMessageType("");
			
		}
		
			
		dtoBuildChecks.setDelete(deleteDtoBuildChecks);
	} 
	catch (NumberFormatException e) {
		log.error(e);
	}
	log.debug("Delete Default"
			+ " :"+dtoBuildChecks.getId());
	return dtoBuildChecks;
	}
	
	
// geting all data for buildData
	public DtoSearch getByDefaultIdWithAllData(Integer id) {
		DtoSearch dtoSearch = new DtoSearch();
		Default defaultEntity = 	repositoryDefault.findByIdAndIsDeleted(id, false);
	
		try {
			List<DtoBuildChecks> dtoBuildChecks  = new ArrayList<>();
			if (id > 0) {
				List<BuildChecks> buildChecks = repositoryBuildChecks.getByDefaultId(id);
					
				
				
					for (BuildChecks buildChecks2 : buildChecks) {
						DtoBuildChecks dtoBuildCheckss = new DtoBuildChecks(buildChecks2);
						
						DtoDefault default1=new DtoDefault();
						if(buildChecks2.getDefault1()!=null) {
							default1.setId(buildChecks2.getDefault1().getId());
							default1.setDesc(buildChecks2.getDefault1().getDesc());
							default1.setArabicDesc(buildChecks2.getDefault1().getArabicDesc());
							default1.setDefaultID(buildChecks2.getDefault1().getDefaultID());
						}
						if(defaultEntity!=null && !defaultEntity.getListDistribution().isEmpty()) {
							dtoBuildCheckss.setDisable(true);
						}
						
						dtoBuildCheckss.setDefault1(default1);
						dtoBuildCheckss.setId(buildChecks2.getId());
						dtoBuildCheckss.setDescription(buildChecks2.getDescription());
						dtoBuildCheckss.setCheckByUserId(buildChecks2.getCheckByUserId());
						dtoBuildCheckss.setCheckDate(buildChecks2.getCheckDate());
						dtoBuildCheckss.setTypeofPayRun(buildChecks2.getTypeofPayRun());
						dtoBuildCheckss.setDateFrom(buildChecks2.getDateFrom());
						dtoBuildCheckss.setDateTo(buildChecks2.getDateTo());
						dtoBuildCheckss.setWeekly(buildChecks2.getWeekly());
						dtoBuildCheckss.setBiweekly(buildChecks2.getBiweekly());
						dtoBuildCheckss.setSemiannually(buildChecks2.getSemiannually());
						dtoBuildCheckss.setMonthly(buildChecks2.getMonthly());
						dtoBuildCheckss.setQuarterly(buildChecks2.getQuarterly());
						dtoBuildCheckss.setSemimonthly(buildChecks2.getSemimonthly());
						dtoBuildCheckss.setMonthly(buildChecks2.getMonthly());
						dtoBuildCheckss.setAnnually(buildChecks2.getAnnually());
						dtoBuildCheckss.setDailyMisc(buildChecks2.getDailyMisc());
						dtoBuildCheckss.setAllEmployees(buildChecks2.getAllEmployees());
						dtoBuildCheckss.setFromEmployeeId(buildChecks2.getFromEmployeeId());
						dtoBuildCheckss.setToEmployeeId(buildChecks2.getToEmployeeId());
						dtoBuildCheckss.setAllDepertment(buildChecks2.getAllDepertment());
						dtoBuildCheckss.setFromDepartmentId(buildChecks2.getFromDepartmentId());
						dtoBuildCheckss.setToDepartmentId(buildChecks2.getToDepartmentId());
						
						if(buildChecks2.getListEmployee()!=null && !buildChecks2.getListEmployee().isEmpty())
						{	
							DtoEmployeeMasterHcm dtoEmp = null;
							List<DtoEmployeeMasterHcm> listEmp = new ArrayList<>();
							for(EmployeeMaster empList : buildChecks2.getListEmployee()) {
								dtoEmp = new DtoEmployeeMasterHcm();
								dtoEmp.setEmployeeIndexId(empList.getEmployeeIndexId());
								dtoEmp.setEmployeeFirstName(empList.getEmployeeFirstName());
								dtoEmp.setEmployeeId(empList.getEmployeeId());
								dtoEmp.setEmployeeLastName(empList.getEmployeeLastName());
								listEmp.add(dtoEmp);
							}
							dtoBuildCheckss.setDtoEmployeeMasters(listEmp);
							dtoBuildChecks.add(dtoBuildCheckss);
							
							//with employee primaryId wise data
							dtoBuildCheckss.setListEmployeeId(buildChecks2.getListEmployee().stream().map(m->m.getEmployeeIndexId()).collect(Collectors.toList()));	
							
						}
						
						if(buildChecks2.getListDepartment()!=null && !buildChecks2.getListDepartment().isEmpty()) {
							DtoDepartment dtoDep=null;
							List<DtoDepartment> list=new ArrayList<>();
							for (Department deptList : buildChecks2.getListDepartment()) {
								dtoDep=new DtoDepartment();
								dtoDep.setId(deptList.getId());
								dtoDep.setDepartmentId(deptList.getDepartmentId());
								dtoDep.setDepartmentDescription(deptList.getDepartmentDescription());
								list.add(dtoDep);
								
							}
							//with department departmentId wise data
							//dtoBuildCheckss.setListDepartmentId(buildChecks2.getListDepartment().stream().map(d->d.getId()).collect(Collectors.toList()));
							dtoBuildCheckss.setDtoDepartments(list);
						}
						
						
						//getting BenefitCode Wich have apply employee on departments wise
						if(buildChecks2.getListBuildPayrollCheckByBenefits()!=null && !buildChecks2.getListBuildPayrollCheckByBenefits().isEmpty()) {
						DtoBuildPayrollCheckByBenefits dtoBuildPayrollCheckByBenefits=null;
							List<DtoBuildPayrollCheckByBenefits>listBuildPayrollCheckByBenefits=new ArrayList<>();
							for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildChecks2.getListBuildPayrollCheckByBenefits()) {
								dtoBuildPayrollCheckByBenefits =new DtoBuildPayrollCheckByBenefits();
								
								// dtoBuildPayrollCheckByBenefits Id
								dtoBuildPayrollCheckByBenefits.setId(buildPayrollCheckByBenefits.getId());
								if(buildPayrollCheckByBenefits.getBenefitCode()!=null && buildPayrollCheckByBenefits.getBenefitCode().getId()>0) {
									// setup code id
									dtoBuildPayrollCheckByBenefits.setId(buildPayrollCheckByBenefits.getBenefitCode().getId());
									dtoBuildPayrollCheckByBenefits.setBenefitId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
									dtoBuildPayrollCheckByBenefits.setDesc(buildPayrollCheckByBenefits.getBenefitCode().getDesc());
								}
								listBuildPayrollCheckByBenefits.add(dtoBuildPayrollCheckByBenefits);
							}	
							dtoBuildCheckss.setDtoBuildPayrollCheckByBenefits(listBuildPayrollCheckByBenefits);
							dtoBuildChecks.add(dtoBuildCheckss);
						}
						
						//getting DiductionCode Wich have apply employee on departments wise
						if(buildChecks2.getListBuildPayrollCheckByDeductions()!=null &&! buildChecks2.getListBuildPayrollCheckByDeductions().isEmpty()) {
							DtoBuildPayrollCheckByDeductions dtoBuildPayrollCheckByDeductions=null;
							List<DtoBuildPayrollCheckByDeductions> listDtoBuildPayrollCheckByDeductions=new ArrayList<>();
							dtoBuildPayrollCheckByDeductions=new DtoBuildPayrollCheckByDeductions();
							for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildChecks2.getListBuildPayrollCheckByDeductions()) {
								//dtoBuildPayrollCheckByDeductions Id
								dtoBuildPayrollCheckByDeductions.setId(buildPayrollCheckByDeductions.getId());
								if(buildPayrollCheckByDeductions.getDeductionCode()!=null && buildPayrollCheckByDeductions.getDeductionCode().getId()>0) {
									//setup code id
									dtoBuildPayrollCheckByDeductions.setId(buildPayrollCheckByDeductions.getDeductionCode().getId());
									dtoBuildPayrollCheckByDeductions.setDiductionId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
									dtoBuildPayrollCheckByDeductions.setDiscription(buildPayrollCheckByDeductions.getDeductionCode().getDiscription());
								}
							}
							listDtoBuildPayrollCheckByDeductions.add(dtoBuildPayrollCheckByDeductions);
							dtoBuildCheckss.setDtoBuildPayrollCheckByDeductions(listDtoBuildPayrollCheckByDeductions);
						}
						
						if(buildChecks2.getListBuildPayrollCheckByPayCodes()!=null && !buildChecks2.getListBuildPayrollCheckByPayCodes().isEmpty()) {
							DtoBuildPayrollCheckByPayCodes dtoBuildPayrollCheckByPayCodes=null;
							List<DtoBuildPayrollCheckByPayCodes>listBuildPayrollCheckByPayCodes=new ArrayList<>();
							dtoBuildPayrollCheckByPayCodes=new DtoBuildPayrollCheckByPayCodes();
							for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecks2.getListBuildPayrollCheckByPayCodes()) {
								dtoBuildPayrollCheckByPayCodes.setId(buildPayrollCheckByPayCodes.getId());
								
								if(buildPayrollCheckByPayCodes.getPayCode()!=null && buildPayrollCheckByPayCodes.getPayCode().getId()>0) {
									dtoBuildPayrollCheckByPayCodes.setId(buildPayrollCheckByPayCodes.getPayCode().getId());
									dtoBuildPayrollCheckByPayCodes.setPayCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
									dtoBuildPayrollCheckByPayCodes.setDescription(buildPayrollCheckByPayCodes.getPayCode().getDescription());
								}
							}
							listBuildPayrollCheckByPayCodes.add(dtoBuildPayrollCheckByPayCodes);
							dtoBuildCheckss.setDtoBuildPayrollCheckByPayCodes(listBuildPayrollCheckByPayCodes);
						}
						dtoBuildChecks.add(dtoBuildCheckss);
					}
			}
			dtoSearch.setRecords(dtoBuildChecks);
			
		} catch (Exception e) {
			log.error(e);
		}
			return dtoSearch;
		}
	

	
	
	/*public DtoBuildChecks saveOrUpdate1(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
			
			if(dtoBuildChecks.getTypeofPayRun()==1) {
				try {
					BuildChecks buildChecks=null;
					if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
						buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
						buildChecks.setUpdatedBy(loggedInUserId);
						buildChecks.setUpdatedDate(new Date());
						Default default1=null;
						if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
							default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
						}
						if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
							List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
							buildChecks.setListDepartment(departments);
						}
						
						if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
							List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
							buildChecks.setListEmployee(employeeMasters);
						}
						buildChecks.setDefault1(default1);
						buildChecks.setDescription(dtoBuildChecks.getDescription());
						buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
						buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
						buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
						buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
						buildChecks.setDateTo(dtoBuildChecks.getDateTo());
						buildChecks.setWeekly(dtoBuildChecks.getWeekly());
						buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
						buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
						buildChecks.setMonthly(dtoBuildChecks.getMonthly());
						buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
						buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
						buildChecks.setMonthly(dtoBuildChecks.getMonthly());
						buildChecks.setAnnually(dtoBuildChecks.getAnnually());
						buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
						buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
						buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
						buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
						buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
						buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
						buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
						buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
						List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
						for (TransactionEntry transactionEntry : listOfTranscationEntry) {
							repositoryTransactionEntry.deleteSingleTranscationEntryRow(true, loggedInUserId, transactionEntry.getId());
							repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(true, loggedInUserId, transactionEntry.getId());
						}
						
						
						
						List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
						for (Department department : departmentsList) {
							List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
							for (EmployeeMaster employeeMaster2 : employeeMaster) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(buildChecks.getBatches());
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
									List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeBenefitMaintenances.isEmpty()) {
										for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(buildChecks.getBatches());
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
									}
									
										
										List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
										if(!employeeDeductionMaintenances.isEmpty()) {
											for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
												
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												
											}
										}
										

								
							}
								
							
							
						}
						
							buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
						
					} else {
						buildChecks = new BuildChecks();
						buildChecks.setCreatedDate(new Date());
						Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
						Integer increment=0;
						if(rowId!=0) {
							increment= rowId+1;
						}else {
							increment=1;
						}
						buildChecks.setRowId(increment);
						Default default1=null;
						if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
							default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
						}
						if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
							List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
							buildChecks.setListDepartment(departments);
						}
						
						if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
							List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
							buildChecks.setListEmployee(employeeMasters);
						}
						buildChecks.setDefault1(default1);
						buildChecks.setDescription(dtoBuildChecks.getDescription());
						buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
						buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
						buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
						buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
						buildChecks.setDateTo(dtoBuildChecks.getDateTo());
						buildChecks.setWeekly(dtoBuildChecks.getWeekly());
						buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
						buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
						buildChecks.setMonthly(dtoBuildChecks.getMonthly());
						buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
						buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
						buildChecks.setMonthly(dtoBuildChecks.getMonthly());
						buildChecks.setAnnually(dtoBuildChecks.getAnnually());
						buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
						buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
						buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
						buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
						buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
						buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
						buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
						buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
						Batches batches = null;
						batches = new Batches();
						batches.setCreatedDate(new Date());
						Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
						Integer increment1=0;
						if(rowId1!=0) {
							increment1= rowId1+1;
						}else {
							increment1=1;
						}
						batches.setRowId(increment1);
						batches.setPostingDate(new Date());
						batches.setApproved(true);
						batches.setArabicDescription("BATCH FOR BUILD CHECK");
						batches.setBatchId("BATCH FOR BUILD CHECK");
						batches.setDescription("BATCH FOR BUILD CHECK");
						batches.setApprovedDate(new Date());
						batches.setUserID(String.valueOf(loggedInUserId));
						batches.setUpdatedBy(loggedInUserId);
						batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
						batches.setFromBuild(true);
						batches.setIsDeleted(true);
						batches.setTotalTransactions((double)0);
						
						repositoryBatches.saveAndFlush(batches);
						buildChecks.setBatches(batches);
						buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
						dtoBuildChecks.setId(buildChecks.getId());
									
						
						
						
						
						List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
							for (Department department : departmentsList) {
								List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
								for (EmployeeMaster employeeMaster2 : employeeMaster) {
									List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeePayCodeMaintenance.isEmpty()) {
										for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
									}
									
										
										List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
										if(!employeeBenefitMaintenances.isEmpty()) {
											for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(batches);
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
										}
										
											
											List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
											if(!employeeDeductionMaintenances.isEmpty()) {
												for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
													
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
													Integer increment11=0;
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(batches);
													transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													
												}
											}
											

									
								}
									
								
								
							}
					}

				
					
						
				} catch (Exception e) {
					log.error(e);
				}
				
			}else {
				try {
					BuildChecks buildChecks=null;
					if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
						buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
						buildChecks.setUpdatedBy(loggedInUserId);
						buildChecks.setUpdatedDate(new Date());
						Default default1=null;
						if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
							default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
						}
						if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
							List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
							buildChecks.setListDepartment(departments);
						}
						
						if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
							List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
							buildChecks.setListEmployee(employeeMasters);
						}
						buildChecks.setDefault1(default1);
						buildChecks.setDescription(dtoBuildChecks.getDescription());
						buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
						buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
						buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
						buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
						buildChecks.setDateTo(dtoBuildChecks.getDateTo());
						buildChecks.setWeekly(dtoBuildChecks.getWeekly());
						buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
						buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
						buildChecks.setMonthly(dtoBuildChecks.getMonthly());
						buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
						buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
						buildChecks.setMonthly(dtoBuildChecks.getMonthly());
						buildChecks.setAnnually(dtoBuildChecks.getAnnually());
						buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
						buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
						buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
						buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
						buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
						buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
						buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
						buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
						List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
						for (TransactionEntry transactionEntry : listOfTranscationEntry) {
							repositoryTransactionEntry.deleteSingleTranscationEntryRow(true, loggedInUserId, transactionEntry.getId());
							repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(true, loggedInUserId, transactionEntry.getId());
						}
						
						
						
						List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
						for (Department department : departmentsList) {
							List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
							for (EmployeeMaster employeeMaster2 : employeeMaster) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(buildChecks.getBatches());
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
									List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeBenefitMaintenances.isEmpty()) {
										for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(buildChecks.getBatches());
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
									}
									
										
										List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
										if(!employeeDeductionMaintenances.isEmpty()) {
											for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
												
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												
											}
										}
										

								
							}
								
							
							
						}
						
							buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
						
					} else {
						buildChecks = new BuildChecks();
						buildChecks.setCreatedDate(new Date());
						Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
						Integer increment=0;
						if(rowId!=0) {
							increment= rowId+1;
						}else {
							increment=1;
						}
						buildChecks.setRowId(increment);
						Default default1=null;
						if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
							default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
						}
						if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
							List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
							buildChecks.setListDepartment(departments);
						}
						
						if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
							List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
							buildChecks.setListEmployee(employeeMasters);
						}
						buildChecks.setDefault1(default1);
						buildChecks.setDescription(dtoBuildChecks.getDescription());
						buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
						buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
						buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
						buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
						buildChecks.setDateTo(dtoBuildChecks.getDateTo());
						buildChecks.setWeekly(dtoBuildChecks.getWeekly());
						buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
						buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
						buildChecks.setMonthly(dtoBuildChecks.getMonthly());
						buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
						buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
						buildChecks.setMonthly(dtoBuildChecks.getMonthly());
						buildChecks.setAnnually(dtoBuildChecks.getAnnually());
						buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
						buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
						buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
						buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
						buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
						buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
						buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
						buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
						Batches batches = null;
						batches = new Batches();
						batches.setCreatedDate(new Date());
						Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
						Integer increment1=0;
						if(rowId1!=0) {
							increment1= rowId1+1;
						}else {
							increment1=1;
						}
						batches.setRowId(increment1);
						batches.setPostingDate(new Date());
						batches.setApproved(true);
						batches.setArabicDescription("BATCH FOR BUILD CHECK");
						batches.setBatchId("BATCH FOR BUILD CHECK");
						batches.setDescription("BATCH FOR BUILD CHECK");
						batches.setApprovedDate(new Date());
						batches.setUserID(String.valueOf(loggedInUserId));
						batches.setUpdatedBy(loggedInUserId);
						batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
						batches.setFromBuild(true);
						batches.setIsDeleted(true);
						batches.setTotalTransactions((double)0);
						
						repositoryBatches.saveAndFlush(batches);
						buildChecks.setBatches(batches);
						buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
						dtoBuildChecks.setId(buildChecks.getId());
									
						
						
						
						
						List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
							for (Department department : departmentsList) {
								List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
								for (EmployeeMaster employeeMaster2 : employeeMaster) {
									List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeePayCodeMaintenance.isEmpty()) {
										for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
									}
									
										
										List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
										if(!employeeBenefitMaintenances.isEmpty()) {
											for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(batches);
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
										}
										
											
											List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
											if(!employeeDeductionMaintenances.isEmpty()) {
												for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
													
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
													Integer increment11=0;
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(batches);
													transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													
												}
											}
											

									
								}
									
								
								
							}
					}

				
					
						
				} catch (Exception e) {
					log.error(e);
				}
			}
			
			
			
		return dtoBuildChecks;
	}*/
	
	
	public DtoDefaulCheck deleteFromTranscationByBenefit(DtoDefaulCheck dtoDefaulCheck) {
		DtoDefaulCheck defaulCheck = new DtoDefaulCheck();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			BuildChecks buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoDefaulCheck.getDefaultId(), false);
			if(buildChecks!=null) {
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				List<TransactionEntryDetail> transactionEntryDetails = repositoryTransactionEntryDetail.findByBuildChecks(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty() && !transactionEntryDetails.isEmpty()) {
					if(buildPayrollCheckByBenefitsList!=null && transactionEntryDetails!=null) {
						
						
							for (Integer beInteger : dtoDefaulCheck.getDeductionId()) {
								for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
								for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetails) {
									
									if(transactionEntryDetail.getIsDeleted()==true && transactionEntryDetail.getFromBuild()==false) {
										if(transactionEntryDetail.getBenefitCode()!=null) {
											if(buildPayrollCheckByBenefits.getBenefitCode().getId() !=null && transactionEntryDetail.getBenefitCode().getId()!=null) {
												if(buildPayrollCheckByBenefits.getBenefitCode().getId() == transactionEntryDetail.getBenefitCode().getId()) {
													if(dtoDefaulCheck.getBenefitId()!=null) {
														if(!dtoDefaulCheck.getDeductionId()	.isEmpty()) {
																if(transactionEntryDetail.getBenefitCode().getId().equals( beInteger)) {
																	
																	repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, true, transactionEntryDetail.getId());
																}
														}
													}
												}
												
											}
										}
										
									}else if(transactionEntryDetail.getIsDeleted()==false && transactionEntryDetail.getFromBuild()==true){
										if(transactionEntryDetail.getBenefitCode()!=null) {
											if(buildPayrollCheckByBenefits.getBenefitCode().getId() !=null && transactionEntryDetail.getBenefitCode().getId()!=null) {
												if(buildPayrollCheckByBenefits.getBenefitCode().getId().equals(transactionEntryDetail.getBenefitCode().getId())) {
													if(dtoDefaulCheck.getDeductionId()!=null) {
														if(!dtoDefaulCheck.getDeductionId().isEmpty()) {
																if(transactionEntryDetail.getBenefitCode().getId().equals(beInteger)) {
																	
																	repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, false, transactionEntryDetail.getId());
																}
														}
													}
												}
												
											}
										}
										
									}else if(transactionEntryDetail.getIsDeleted()==false && transactionEntryDetail.getFromBuild()==false) {
										if(transactionEntryDetail.getBenefitCode()!=null) {
											if(buildPayrollCheckByBenefits.getBenefitCode().getId() !=null && transactionEntryDetail.getBenefitCode().getId()!=null) {
												if(!buildPayrollCheckByBenefits.getBenefitCode().getId().equals( transactionEntryDetail.getBenefitCode().getId())) {
													if(dtoDefaulCheck.getDeductionId()!=null) {
														if(!dtoDefaulCheck.getDeductionId().isEmpty()) {
																if(transactionEntryDetail.getBenefitCode().getId().equals(beInteger)) {
																	
																	repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(true, loggedInUserId, true, transactionEntryDetail.getId());
																}
														}
													}
												}
												
											}
										}
										
									}else if(transactionEntryDetail.getIsDeleted()==true && transactionEntryDetail.getFromBuild()==true) {
										if(transactionEntryDetail.getBenefitCode()!=null) {
											if(buildPayrollCheckByBenefits.getBenefitCode().getId() !=null && transactionEntryDetail.getBenefitCode().getId()!=null) {
												if(!buildPayrollCheckByBenefits.getBenefitCode().getId().equals( transactionEntryDetail.getBenefitCode().getId())) {
													if(dtoDefaulCheck.getDeductionId()!=null) {
														if(!dtoDefaulCheck.getDeductionId().isEmpty()) {
																if(transactionEntryDetail.getBenefitCode().getId().equals(beInteger)) {
																	
																	repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, false, transactionEntryDetail.getId());
																}
														}
													}
												}
												
											}
										}
									}
									
									
									
									
									
									
								}
							}
							
							
							
						}
						
						
						
					}
					
				}
				
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return defaulCheck;
	}
		
	
	
	public DtoDefaulCheck deleteFromTranscationByDeduction(DtoDefaulCheck dtoDefaulCheck) {
		DtoDefaulCheck defaulCheck = new DtoDefaulCheck();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			List<BuildChecks> buildChecks = repositoryBuildChecks.findByDefauiltIds1(dtoDefaulCheck.getDefaultId());
			if(buildChecks!=null) {
			for (BuildChecks buildChecks2 : buildChecks) {
				
					List<TransactionEntryDetail> transactionEntryDetails = repositoryTransactionEntryDetail.findByBuildChecks(buildChecks2.getId());
					List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks2.getId());
					
					if(!buildPayrollCheckByDeductionsList.isEmpty() && !transactionEntryDetails.isEmpty()) {
						if(buildPayrollCheckByDeductionsList!=null && transactionEntryDetails!=null) {
							for (Integer beInteger : dtoDefaulCheck.getDeductionId()) {
							for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetails) {
								for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
									
							
								
									
								
										if(transactionEntryDetail.getDeductionCode()!=null) {
											if(transactionEntryDetail.getIsDeleted()==true && transactionEntryDetail.getFromBuild()==false) {
												if(buildPayrollCheckByDeductions.getDeductionCode().getId() !=null && transactionEntryDetail.getDeductionCode().getId()!=null) {
													if(buildPayrollCheckByDeductions.getDeductionCode().getId().equals(transactionEntryDetail.getDeductionCode().getId())) {
														if(dtoDefaulCheck.getDeductionId()!=null) {
															if(!dtoDefaulCheck.getDeductionId().isEmpty()) {
																	if(transactionEntryDetail.getDeductionCode().getId().equals(beInteger)) {
																		
																		repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, true, transactionEntryDetail.getId());
																	}
															}
														}
													}
													
												}
											}else if(transactionEntryDetail.getIsDeleted()==false && transactionEntryDetail.getFromBuild()==true){
												if(buildPayrollCheckByDeductions.getDeductionCode().getId() !=null && transactionEntryDetail.getDeductionCode().getId()!=null) {
													if(buildPayrollCheckByDeductions.getDeductionCode().getId().equals(transactionEntryDetail.getDeductionCode().getId())) {
														if(dtoDefaulCheck.getDeductionId()!=null) {
															if(!dtoDefaulCheck.getDeductionId().isEmpty()) {
																	if(transactionEntryDetail.getDeductionCode().getId().equals(beInteger)) {
																		
																		repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, false, transactionEntryDetail.getId());
																	}
															}
														}
													}
													
												}
											}else if(transactionEntryDetail.getIsDeleted()==false && transactionEntryDetail.getFromBuild()==false) {

												if(buildPayrollCheckByDeductions.getDeductionCode().getId() !=null && transactionEntryDetail.getDeductionCode().getId()!=null) {
													if(!buildPayrollCheckByDeductions.getDeductionCode().getId().equals( transactionEntryDetail.getDeductionCode().getId())) {
														if(dtoDefaulCheck.getDeductionId()!=null) {
															if(!dtoDefaulCheck.getDeductionId().isEmpty()) {
																	if(transactionEntryDetail.getDeductionCode().getId().equals(beInteger)) {
																		
																		repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(true, loggedInUserId, true, transactionEntryDetail.getId());
																	}
															}
														}
													}
													
												}
											
											}else if(transactionEntryDetail.getIsDeleted()==true && transactionEntryDetail.getFromBuild()==true) {

												if(buildPayrollCheckByDeductions.getDeductionCode().getId() !=null && transactionEntryDetail.getDeductionCode().getId()!=null) {
													if(!buildPayrollCheckByDeductions.getDeductionCode().getId().equals( transactionEntryDetail.getDeductionCode().getId())) {
														if(dtoDefaulCheck.getDeductionId()!=null) {
															if(!dtoDefaulCheck.getDeductionId().isEmpty()) {
																	if(transactionEntryDetail.getDeductionCode().getId().equals(beInteger)) {
																		
																		repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, false, transactionEntryDetail.getId());
																	}
															}
														}
													}
													
												}
											
											}
										}
										
									}
									
									
									
									
									
									
								}
								
								
							}
							
							
							
						}
						
					}
				}
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return defaulCheck;
	}
	
	
	public DtoBuildChecks saveOrUpdate12(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			
			
			BuildChecks buildChecks=null;
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				for (TransactionEntry transactionEntry : listOfTranscationEntry) {
					repositoryTransactionEntry.deleteSingleTranscationEntryRow(true, loggedInUserId, transactionEntry.getId());
					repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(true, loggedInUserId, transactionEntry.getId());
				}
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
						if(!employeePayCodeMaintenance.isEmpty()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
								TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								Integer increment11=0;
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(buildChecks.getBatches());
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setFromBuild(true);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
								TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
								transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
								transactionEntryDetail.setEmployeeMaster(employeeMaster2);
								transactionEntryDetail.setDepartment(department);
								transactionEntryDetail.setTransactionEntry(transactionEntry);
								transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
								transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
								transactionEntryDetail.setFromBuild(true);
								transactionEntryDetail.setTransactionType((short)1);
								transactionEntryDetail.setIsDeleted(false);
								transactionEntryDetail.setCreatedDate(new Date());
								transactionEntryDetail.setUpdatedBy(loggedInUserId);
								transactionEntryDetail.setUpdatedDate(new Date());
								transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
								transactionEntryDetail.setUpdatedRow(new Date());
								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
							}
						}
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(buildChecks.getBatches());
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(true);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setFromBuild(true);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setUpdatedRow(new Date());
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(buildChecks.getBatches());
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										
									}
								}
								

						
					}
						
					
					
				}
				
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}	
	
	
	public DtoBuildChecks saveOrUpdateDifferent(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			
			
			BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntryDetail> transactionEntryDetails = repositoryTransactionEntryDetail.findByBuildChecks(buildChecks.getId());
				for (TransactionEntryDetail transactionEntryDetail1 : transactionEntryDetails) {
					List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							if(employeeMaster2.getEmployeeIndexId()!= transactionEntryDetail1.getId()) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(buildChecks.getBatches());
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeBenefitMaintenances.isEmpty()) {
										for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(buildChecks.getBatches());
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
									}
									
										
										List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
										if(!employeeDeductionMaintenances.isEmpty()) {
											for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
												
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												
											}
										}
										
							}
							

							
						}
							
						
						
					}
				}
				
				
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(batches);
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(true);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setFromBuild(true);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setUpdatedRow(new Date());
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(batches);
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											
										}
									}
									

							
						}
							
						
						
					}
			}

		
			
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}
	

	public DtoBuildChecks saveOrUpdateFinal(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			
			
			BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(true, loggedInUserId, transactionEntry.getId());
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(true, loggedInUserId, transactionEntry.getId());
					}
				}
				
				List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByBuildChecks1(buildChecks.getId());
				if(!transactionEntryDetailList.isEmpty()) {
					for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(true, loggedInUserId, true, transactionEntryDetail.getId());
					}
				}
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId1(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
						if(!employeePayCodeMaintenance.isEmpty()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
								TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
							//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
								Integer increment11=0;
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(buildChecks.getBatches());
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setFromBuild(false);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
								TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
								transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
								transactionEntryDetail.setEmployeeMaster(employeeMaster2);
								transactionEntryDetail.setDepartment(department);
								transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
								transactionEntryDetail.setTransactionEntry(transactionEntry);
								transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
								transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
								transactionEntryDetail.setFromBuild(false);
								transactionEntryDetail.setTransactionType((short)1);
								transactionEntryDetail.setIsDeleted(false);
								transactionEntryDetail.setFromTranscationEntry(false);
								transactionEntryDetail.setCreatedDate(new Date());
								transactionEntryDetail.setUpdatedBy(loggedInUserId);
								transactionEntryDetail.setUpdatedDate(new Date());
								transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
								transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
								transactionEntryDetail.setUpdatedRow(new Date());
								transactionEntryDetail.setBuildChecks(buildChecks);
								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
							}
						}
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(buildChecks.getBatches());
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(true);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setFromBuild(true);
									if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
										if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
											  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
										} else {
											transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
										}
									}else {
										transactionEntryDetail.setPayRate(BigDecimal(0.0));
									}
									
									
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setUpdatedRow(new Date());
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setFromTranscationEntry(false);
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(buildChecks.getBatches());
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
											if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
												  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
											} else {
												transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
											}
										}else {
											transactionEntryDetail.setPayRate(BigDecimal(0.0));
										}
										
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setFromTranscationEntry(false);
										transactionEntryDetail.setBuildChecks(buildChecks);
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				batches.setFromtranscation(false);
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(batches);
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(false);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
									transactionEntryDetail.setFromBuild(false);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setUpdatedRow(new Date());
									transactionEntryDetail.setFromTranscationEntry(false);
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(batches);
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
											if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
												  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
											} else {
												transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
											}
										}else {
											transactionEntryDetail.setPayRate(BigDecimal(0.0));
										}
										
										//transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setFromTranscationEntry(false);
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
												if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
													  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
												} else {
													transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
												}
											}else {
												transactionEntryDetail.setPayRate(BigDecimal(0.0));
											}
											
											//transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											transactionEntryDetail.setFromTranscationEntry(false);
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											
										}
									}
									

							
						}
							
						
						
					}
			}

		
			
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}
	
	
	////today 

	/*public DtoBuildChecks saveOrUpdateFinal1(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			
			
			BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				if(buildChecks!=null) {
					repositoryBuildChecks.deleteSingleBuildCheckss(true, loggedInUserId, buildChecks.getId());
					List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
					if(!listOfTranscationEntry.isEmpty()) {
						for (TransactionEntry transactionEntry : listOfTranscationEntry) {
							repositoryTransactionEntry.deleteSingleTranscationEntryRow(true, loggedInUserId, transactionEntry.getId());
							repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(true, loggedInUserId, transactionEntry.getId());
						}
					}
					
					List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
					if(!buildPayrollCheckByBenefitsList.isEmpty()) {
						for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
							repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
						}
						
					}
					List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
					if(!buildPayrollCheckByDeductionsList.isEmpty()) {
						for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
							repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
						}
					}
					
					List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
					if(!buildPayrollCheckByBatchesList.isEmpty()) {
						for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
							repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBatches.getId());
						}
					}
				}
				

				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(batches);
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(false);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
									transactionEntryDetail.setFromBuild(false);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setUpdatedRow(new Date());
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(batches);
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											
										}
									}
									

							
						}
							
						
						
					}
			
				
				
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(batches);
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(false);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
									transactionEntryDetail.setFromBuild(false);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setUpdatedRow(new Date());
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(batches);
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										//	Integer rowId1 = repositoryTransactionEntry.findAll().size();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											
										}
									}
									

							
						}
							
						
						
					}
			}

		
			
				
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dtoBuildChecks;
	}
	*/
	
	
	
	
	
		private BigDecimal BigDecimal(double d) {
		double d1 = 0.0;
			BigDecimal bigDecimal =  BigDecimal.valueOf(d1);
			return bigDecimal;
		
	}


	public DtoBuildChecks saveOrUpdateFinal12(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			
			
			BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(true, loggedInUserId, transactionEntry.getId());
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(true, loggedInUserId, transactionEntry.getId());
					}
				}
				
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
						if(!employeePayCodeMaintenance.isEmpty()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
								TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								Integer increment11=0;
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(buildChecks.getBatches());
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setFromBuild(false);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
								TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
								transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
								transactionEntryDetail.setEmployeeMaster(employeeMaster2);
								transactionEntryDetail.setDepartment(department);
								transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
								transactionEntryDetail.setTransactionEntry(transactionEntry);
								transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
								transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
								transactionEntryDetail.setFromBuild(false);
								transactionEntryDetail.setTransactionType((short)1);
								transactionEntryDetail.setIsDeleted(false);
								transactionEntryDetail.setCreatedDate(new Date());
								transactionEntryDetail.setUpdatedBy(loggedInUserId);
								transactionEntryDetail.setUpdatedDate(new Date());
								transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
								transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
								transactionEntryDetail.setUpdatedRow(new Date());
								transactionEntryDetail.setBuildChecks(buildChecks);
								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
							}
						}
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									if((employeeBenefitMaintenance.getStartDate().after(dtoBuildChecks.getDateFrom())) && (employeeBenefitMaintenance.getEndDate().before(dtoBuildChecks.getDateTo()))) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(buildChecks.getBatches());
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										
										if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
											  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
										} else {
											transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
										}
										
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
									
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										if((employeeDeductionMaintenance.getStartDate().after(dtoBuildChecks.getDateFrom())) && (employeeDeductionMaintenance.getEndDate().before(dtoBuildChecks.getDateTo()))){
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(buildChecks.getBatches());
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
												  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
											} else {
												transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
											}
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
										
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(batches);
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(false);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
									transactionEntryDetail.setFromBuild(false);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setUpdatedRow(new Date());
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										if((employeeBenefitMaintenance.getStartDate().after(dtoBuildChecks.getDateFrom())) && (employeeBenefitMaintenance.getEndDate().before(dtoBuildChecks.getDateTo()))) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
										
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											if((employeeDeductionMaintenance.getStartDate().after(dtoBuildChecks.getDateFrom())) && (employeeDeductionMaintenance.getEndDate().before(dtoBuildChecks.getDateTo()))){
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(batches);
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
											
											
										}
									}
									

							
						}
							
						
						
					}
			}

		
			
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}
	
	
	
	
	
	
	
	public DtoBuildChecks saveOrUpdateFinal123(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			
			
			BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(transactionEntry.getId());
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(transactionEntry.getId());
						
					}
				}
				
				List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByBuildChecks1(buildChecks.getId());
				if(!transactionEntryDetailList.isEmpty()) {
					for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
				}
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId1(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
						if(!employeePayCodeMaintenance.isEmpty()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
								TransactionEntry transactionEntry=null;
								transactionEntry = new TransactionEntry();
								transactionEntry.setCreatedDate(new Date());
								
								TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
								Integer increment11=0;
								if(transactionEntry2.getRowId()!=0) {
									increment11= transactionEntry2.getRowId()+1;
								}else {
									increment11=1;
								}
								transactionEntry.setRowId(increment11);
								transactionEntry.setBatches(buildChecks.getBatches());
								transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
								transactionEntry.setEntryDate(new Date());
								transactionEntry.setFromDate(new Date());
								transactionEntry.setToDate(new Date());
								transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
								transactionEntry.setFromBuild(false);
								transactionEntry.setIsDeleted(false);
								transactionEntry.setUpdatedRow(new Date());
								transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
								TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
								TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
								transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
								transactionEntryDetail.setEmployeeMaster(employeeMaster2);
								transactionEntryDetail.setDepartment(department);
								transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
								transactionEntryDetail.setTransactionEntry(transactionEntry);
								transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
								transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
								transactionEntryDetail.setFromBuild(false);
								transactionEntryDetail.setTransactionType((short)1);
								transactionEntryDetail.setIsDeleted(false);
								transactionEntryDetail.setFromTranscationEntry(false);
								transactionEntryDetail.setCreatedDate(new Date());
								transactionEntryDetail.setUpdatedBy(loggedInUserId);
								transactionEntryDetail.setUpdatedDate(new Date());
								transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
								transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
								transactionEntryDetail.setUpdatedRow(new Date());
								transactionEntryDetail.setBuildChecks(buildChecks);
								repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
							}
						}
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(buildChecks.getBatches());
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(true);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setFromBuild(true);
									if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
										if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
											  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
										} else {
											transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
										}
									}else {
										transactionEntryDetail.setPayRate(BigDecimal(0.0));
									}
									
									
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setUpdatedRow(new Date());
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setFromTranscationEntry(false);
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(buildChecks.getBatches());
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
											if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
												  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
											} else {
												transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
											}
										}else {
											transactionEntryDetail.setPayRate(BigDecimal(0.0));
										}
										
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setFromTranscationEntry(false);
										transactionEntryDetail.setBuildChecks(buildChecks);
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				batches.setFromtranscation(false);
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									TransactionEntry transactionEntry=null;
									transactionEntry = new TransactionEntry();
									transactionEntry.setCreatedDate(new Date());
									
									TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
									Integer increment11=0;
									if(transactionEntry2.getRowId()!=0) {
										increment11= transactionEntry2.getRowId()+1;
									}else {
										increment11=1;
									}
									transactionEntry.setRowId(increment11);
									transactionEntry.setBatches(batches);
									transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
									transactionEntry.setEntryDate(new Date());
									transactionEntry.setFromDate(new Date());
									transactionEntry.setToDate(new Date());
									transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
									transactionEntry.setFromBuild(false);
									transactionEntry.setIsDeleted(false);
									transactionEntry.setUpdatedRow(new Date());
									transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
									TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
									TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
									transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
									transactionEntryDetail.setEmployeeMaster(employeeMaster2);
									transactionEntryDetail.setDepartment(department);
									transactionEntryDetail.setTransactionEntry(transactionEntry);
									transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
									transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
									transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
									transactionEntryDetail.setFromBuild(false);
									transactionEntryDetail.setTransactionType((short)1);
									transactionEntryDetail.setIsDeleted(false);
									transactionEntryDetail.setCreatedDate(new Date());
									transactionEntryDetail.setUpdatedBy(loggedInUserId);
									transactionEntryDetail.setUpdatedDate(new Date());
									transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
									transactionEntryDetail.setBuildChecks(buildChecks);
									transactionEntryDetail.setUpdatedRow(new Date());
									transactionEntryDetail.setFromTranscationEntry(false);
									repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
								}
							}
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										TransactionEntry transactionEntry=null;
										transactionEntry = new TransactionEntry();
										transactionEntry.setCreatedDate(new Date());
										
										TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
										Integer increment11=0;
										if(transactionEntry2.getRowId()!=0) {
											increment11= transactionEntry2.getRowId()+1;
										}else {
											increment11=1;
										}
										transactionEntry.setRowId(increment11);
										transactionEntry.setBatches(batches);
										transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
										transactionEntry.setEntryDate(new Date());
										transactionEntry.setFromDate(new Date());
										transactionEntry.setToDate(new Date());
										transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
										transactionEntry.setFromBuild(true);
										transactionEntry.setIsDeleted(false);
										transactionEntry.setUpdatedRow(new Date());
										transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
										TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
										TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
										transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
										transactionEntryDetail.setEmployeeMaster(employeeMaster2);
										transactionEntryDetail.setDepartment(department);
										if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
											if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
												  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
											} else {
												transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
											}
										}else {
											transactionEntryDetail.setPayRate(BigDecimal(0.0));
										}
										transactionEntryDetail.setTransactionEntry(transactionEntry);
										transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
										transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
										transactionEntryDetail.setFromBuild(true);
										transactionEntryDetail.setTransactionType((short)1);
										transactionEntryDetail.setIsDeleted(false);
										transactionEntryDetail.setCreatedDate(new Date());
										transactionEntryDetail.setUpdatedBy(loggedInUserId);
										transactionEntryDetail.setUpdatedDate(new Date());
										transactionEntryDetail.setBuildChecks(buildChecks);
										transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
										transactionEntryDetail.setUpdatedRow(new Date());
										transactionEntryDetail.setFromTranscationEntry(false);
										repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(batches);
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
												if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
													  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
												} else {
													transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
												}
											}else {
												transactionEntryDetail.setPayRate(BigDecimal(0.0));
											}
											
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setUpdatedRow(new Date());
											transactionEntryDetail.setFromTranscationEntry(false);
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											
										}
									}
									

							
						}
							
						
						
					}
			}

		
			
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}
	
	
	
	
	
	@Async
	@CachePut(value="employeeMaster")
	@Cacheable
	public DtoBuildChecks saveOrUpdateFinal1234(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if(dtoBuildChecks.getTypeofPayRun()==1){
				BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(transactionEntry.getId());
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(transactionEntry.getId());
						
					}
				}
				
				List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByBuildChecks1(buildChecks.getId());
				if(!transactionEntryDetailList.isEmpty()) {
					for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
				}
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId1(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						if(employeeMaster2.isEmployeeInactive()==false) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									if(employeePayCodeMaintenance2.getInactive()==false) {
										if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(buildChecks.getBatches());
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(false);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(false);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setFromTranscationEntry(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
											transactionEntryDetail.setUpdatedRow(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
										
									}
									
								}
							}
						}
						
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
										if(employeeBenefitMaintenance.getInactive()==false) {
											if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
													if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
														  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
													} else {
														transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
													}
												}else {
													transactionEntryDetail.setPayRate(BigDecimal(0.0));
												}
												
												
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setFromTranscationEntry(false);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
											
										}
										
									}
									
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
											if(employeeDeductionMaintenance.getInactive()==false) {
												if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(buildChecks.getBatches());
													transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
														if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													transactionEntryDetail.setBuildChecks(buildChecks);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
										}
										
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				batches.setFromtranscation(false);
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							if(employeeMaster2.isEmployeeInactive()==false) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										if(employeePayCodeMaintenance2.getInactive()==false) {
											if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(batches);
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(false);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
												transactionEntryDetail.setFromBuild(false);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setFromTranscationEntry(false);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
											
										}
										
									}
								}
							}
							
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
											if(employeeBenefitMaintenance.getInactive()==false) {
												if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(batches);
													transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
														if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
										}
										
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
												if(employeeDeductionMaintenance.getInactive()==false) {
													if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
														TransactionEntry transactionEntry=null;
														transactionEntry = new TransactionEntry();
														transactionEntry.setCreatedDate(new Date());
														
														TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
														Integer increment11=0;
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
														transactionEntry.setRowId(increment11);
														transactionEntry.setBatches(batches);
														transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
														transactionEntry.setEntryDate(new Date());
														transactionEntry.setFromDate(new Date());
														transactionEntry.setToDate(new Date());
														transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setFromBuild(true);
														transactionEntry.setIsDeleted(false);
														transactionEntry.setUpdatedRow(new Date());
														transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
														TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
														TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
														transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
														transactionEntryDetail.setEmployeeMaster(employeeMaster2);
														transactionEntryDetail.setDepartment(department);
														transactionEntryDetail.setTransactionEntry(transactionEntry);
														transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
														if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
															if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
																  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
															} else {
																transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
															}
														}else {
															transactionEntryDetail.setPayRate(BigDecimal(0.0));
														}
														
														transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
														transactionEntryDetail.setFromBuild(true);
														transactionEntryDetail.setTransactionType((short)1);
														transactionEntryDetail.setIsDeleted(false);
														transactionEntryDetail.setCreatedDate(new Date());
														transactionEntryDetail.setUpdatedBy(loggedInUserId);
														transactionEntryDetail.setUpdatedDate(new Date());
														transactionEntryDetail.setBuildChecks(buildChecks);
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
														transactionEntryDetail.setUpdatedRow(new Date());
														transactionEntryDetail.setFromTranscationEntry(false);
														repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													}
													
												}
												
											}
											
											
										}
									}
									

							
						}
							
						
						
					}
			}

			}else{
				BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(transactionEntry.getId());
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(transactionEntry.getId());
						
					}
				}
				
				List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByBuildChecks1(buildChecks.getId());
				if(!transactionEntryDetailList.isEmpty()) {
					for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
				}
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId1(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						if(employeeMaster2.isEmployeeInactive()==false) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									if(employeePayCodeMaintenance2.getInactive()==false) {
										if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2.getRowId()!=0) {
												increment11= transactionEntry2.getRowId()+1;
											}else {
												increment11=1;
											}
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(buildChecks.getBatches());
											transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setFromTranscationEntry(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
											transactionEntryDetail.setUpdatedRow(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
										
									}
									
								}
							}
						}
						
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									if(employeeBenefitMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
										if(employeeBenefitMaintenance.getInactive()==false) {
											if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
													if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
														  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
													} else {
														transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
													}
												}else {
													transactionEntryDetail.setPayRate(BigDecimal(0.0));
												}
												
												
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setFromTranscationEntry(false);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
											
										}
										
									}
									
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										if(employeeDeductionMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
											if(employeeDeductionMaintenance.getInactive()==false) {
												if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(buildChecks.getBatches());
													transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
														if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													transactionEntryDetail.setBuildChecks(buildChecks);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
										}
									
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				batches.setFromtranscation(false);
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							if(employeeMaster2.isEmployeeInactive()==false) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										if(employeePayCodeMaintenance2.getInactive()==false) {
											if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(batches);
												transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setFromTranscationEntry(false);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
											
										}
										
									}
								}
							}
							
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										if(employeeBenefitMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
											if(employeeBenefitMaintenance.getInactive()==false) {
												if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(batches);
													transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
														if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
										}
										
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											if(employeeDeductionMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
												if(employeeDeductionMaintenance.getInactive()==false) {
													if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
														TransactionEntry transactionEntry=null;
														transactionEntry = new TransactionEntry();
														transactionEntry.setCreatedDate(new Date());
														
														TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
														Integer increment11=0;
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
														transactionEntry.setRowId(increment11);
														transactionEntry.setBatches(batches);
														transactionEntry.setEntryNumber(transactionEntry2.getId()+1);
														transactionEntry.setEntryDate(new Date());
														transactionEntry.setFromDate(new Date());
														transactionEntry.setToDate(new Date());
														transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setFromBuild(true);
														transactionEntry.setIsDeleted(false);
														transactionEntry.setUpdatedRow(new Date());
														transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
														TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
														TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
														transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
														transactionEntryDetail.setEmployeeMaster(employeeMaster2);
														transactionEntryDetail.setDepartment(department);
														transactionEntryDetail.setTransactionEntry(transactionEntry);
														transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
														if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
															if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
																  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
															} else {
																transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
															}
														}else {
															transactionEntryDetail.setPayRate(BigDecimal(0.0));
														}
														
														transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
														transactionEntryDetail.setFromBuild(true);
														transactionEntryDetail.setTransactionType((short)1);
														transactionEntryDetail.setIsDeleted(false);
														transactionEntryDetail.setCreatedDate(new Date());
														transactionEntryDetail.setUpdatedBy(loggedInUserId);
														transactionEntryDetail.setUpdatedDate(new Date());
														transactionEntryDetail.setBuildChecks(buildChecks);
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
														transactionEntryDetail.setUpdatedRow(new Date());
														transactionEntryDetail.setFromTranscationEntry(false);
														repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													}
													
												}
												
											}
											
											
										}
									}
									

							
						}
							
						
						
					}
			}

			}
			
			
		
			
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}
	
	
	
	
	
	
	public DtoDefaulCheck deleteFromTranscationByPayCode(DtoDefaulCheck dtoDefaulCheck) {
		DtoDefaulCheck defaulCheck = new DtoDefaulCheck();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			BuildChecks buildChecks = repositoryBuildChecks.findByDefauiltIds(dtoDefaulCheck.getDefaultId());
			if(buildChecks!=null) {
				List<TransactionEntryDetail> transactionEntryDetails = repositoryTransactionEntryDetail.findByBuildChecks(buildChecks.getId());
				List<BuildPayrollCheckByPayCodes> buildPayrollCheckByPayCodeList = repositoryBuildPayrollCheckByPayCodes.findByBuildChecksId(buildChecks.getId());
				
				if(!buildPayrollCheckByPayCodeList.isEmpty() && !transactionEntryDetails.isEmpty()) {
					if(buildPayrollCheckByPayCodeList!=null && transactionEntryDetails!=null) {
						for (Integer beInteger : dtoDefaulCheck.getPaycodeId()) {
						for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetails) {
							for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildPayrollCheckByPayCodeList) {
								
						
							
								
							
									if(transactionEntryDetail.getCode()!=null) {
										if(transactionEntryDetail.getIsDeleted()==true && transactionEntryDetail.getFromBuild()==false) {
											if(buildPayrollCheckByPayCodes.getPayCode().getId() !=null && transactionEntryDetail.getCode().getId()!=null) {
												if(buildPayrollCheckByPayCodes.getPayCode().getId().equals(transactionEntryDetail.getCode().getId())) {
													if(dtoDefaulCheck.getPaycodeId()!=null) {
														if(!dtoDefaulCheck.getPaycodeId().isEmpty()) {
																if(transactionEntryDetail.getCode().getId().equals(beInteger)) {
																	
																	repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, true, transactionEntryDetail.getId());
																}
														}
													}
												}
												
											}
										}else if(transactionEntryDetail.getIsDeleted()==false && transactionEntryDetail.getFromBuild()==true){
											if(buildPayrollCheckByPayCodes.getPayCode().getId() !=null && transactionEntryDetail.getCode().getId()!=null) {
												if(buildPayrollCheckByPayCodes.getPayCode().getId().equals(transactionEntryDetail.getCode().getId())) {
													if(dtoDefaulCheck.getPaycodeId()!=null) {
														if(!dtoDefaulCheck.getPaycodeId().isEmpty()) {
																if(transactionEntryDetail.getCode().getId().equals(beInteger)) {
																	
																	repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, false, transactionEntryDetail.getId());
																}
														}
													}
												}
												
											}
										}else if(transactionEntryDetail.getIsDeleted()==false && transactionEntryDetail.getFromBuild()==false) {

											if(buildPayrollCheckByPayCodes.getPayCode().getId() !=null && transactionEntryDetail.getCode().getId()!=null) {
												if(!buildPayrollCheckByPayCodes.getPayCode().getId().equals( transactionEntryDetail.getCode().getId())) {
													if(dtoDefaulCheck.getPaycodeId()!=null) {
														if(!dtoDefaulCheck.getPaycodeId().isEmpty()) {
																if(transactionEntryDetail.getCode().getId().equals(beInteger)) {
																	
																	repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(true, loggedInUserId, true, transactionEntryDetail.getId());
																}
														}
													}
												}
												
											}
										
										}else if(transactionEntryDetail.getIsDeleted()==true && transactionEntryDetail.getFromBuild()==true) {

											if(buildPayrollCheckByPayCodes.getPayCode().getId() !=null && transactionEntryDetail.getCode().getId()!=null) {
												if(!buildPayrollCheckByPayCodes.getPayCode().getId().equals( transactionEntryDetail.getCode().getId())) {
													if(dtoDefaulCheck.getPaycodeId()!=null) {
														if(!dtoDefaulCheck.getPaycodeId().isEmpty()) {
																if(transactionEntryDetail.getCode().getId().equals(beInteger)) {
																	
																	repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(false, loggedInUserId, false, transactionEntryDetail.getId());
																}
														}
													}
												}
												
											}
										
										}
									}
									
								}
								
								
								
								
								
								
							}
							
							
						}
						
						
						
					}
					
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return defaulCheck;
	}

	
	
	
	//@Async
	//@CachePut(value="employeeMaster")
	//@Cacheable
	public DtoBuildChecks saveOrUpdateFinalVersion(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if(dtoBuildChecks.getTypeofPayRun()==1){
				BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(transactionEntry.getId());
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(transactionEntry.getId());
						
					}
				}
				
				List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByBuildChecks1(buildChecks.getId());
				if(!transactionEntryDetailList.isEmpty()) {
					for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
				}
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId1(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						if(employeeMaster2.isEmployeeInactive()==false) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									if(employeePayCodeMaintenance2.getInactive()==false) {
										if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2!=null) {
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
											}else {
												increment11=1;
											}
											
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(buildChecks.getBatches());
											transactionEntry.setEntryNumber(increment11+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(false);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(false);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setFromTranscationEntry(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
											transactionEntryDetail.setUpdatedRow(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
										
										
									}
									
								}
							}
						}
						
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
										if(employeeBenefitMaintenance.getInactive()==false) {
											if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
												
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2!=null) {
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
												}else {
													increment11=1;
												}
												
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(increment11+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
													if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
														  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
													} else {
														transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
													}
												}else {
													transactionEntryDetail.setPayRate(BigDecimal(0.0));
												}
												
												
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setFromTranscationEntry(false);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
											
										}
										
									}
									
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
											if(employeeDeductionMaintenance.getInactive()==false) {
												if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2!=null) {
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
													}else {
														increment11=1;
													}
													
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(buildChecks.getBatches());
													transactionEntry.setEntryNumber(increment11+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
														if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													transactionEntryDetail.setBuildChecks(buildChecks);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
												
											}
											
										}
										
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				batches.setFromtranscation(false);
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							if(employeeMaster2.isEmployeeInactive()==false) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										if(employeePayCodeMaintenance2.getInactive()==false) {
											if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2!=null) {
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
												}else {
													increment11=1;
												}
												
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(batches);
												transactionEntry.setEntryNumber(increment11+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(false);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
												transactionEntryDetail.setFromBuild(false);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setFromTranscationEntry(false);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
												
											
										}
										
									}
								}
							}
							
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
											if(employeeBenefitMaintenance.getInactive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2!=null) {
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
													}else {
														increment11=1;
													}
													
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(batches);
													transactionEntry.setEntryNumber(increment11+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
														if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												
											}
											
										}
										
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
												if(employeeDeductionMaintenance.getInactive()==false) {
														TransactionEntry transactionEntry=null;
														transactionEntry = new TransactionEntry();
														transactionEntry.setCreatedDate(new Date());
														
														TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
														Integer increment11=0;
														if(transactionEntry2!=null) {
															if(transactionEntry2.getRowId()!=0) {
																increment11= transactionEntry2.getRowId()+1;
															}else {
																increment11=1;
															}
														}else {
															increment11=1;
														}
														
														transactionEntry.setRowId(increment11);
														transactionEntry.setBatches(batches);
														transactionEntry.setEntryNumber(increment11+1);
														transactionEntry.setEntryDate(new Date());
														transactionEntry.setFromDate(new Date());
														transactionEntry.setToDate(new Date());
														transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setFromBuild(true);
														transactionEntry.setIsDeleted(false);
														transactionEntry.setUpdatedRow(new Date());
														transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
														TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
														TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
														transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
														transactionEntryDetail.setEmployeeMaster(employeeMaster2);
														transactionEntryDetail.setDepartment(department);
														transactionEntryDetail.setTransactionEntry(transactionEntry);
														transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
														if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
															if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
																  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
															} else {
																transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
															}
														}else {
															transactionEntryDetail.setPayRate(BigDecimal(0.0));
														}
														
														transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
														transactionEntryDetail.setFromBuild(true);
														transactionEntryDetail.setTransactionType((short)1);
														transactionEntryDetail.setIsDeleted(false);
														transactionEntryDetail.setCreatedDate(new Date());
														transactionEntryDetail.setUpdatedBy(loggedInUserId);
														transactionEntryDetail.setUpdatedDate(new Date());
														transactionEntryDetail.setBuildChecks(buildChecks);
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
														transactionEntryDetail.setUpdatedRow(new Date());
														transactionEntryDetail.setFromTranscationEntry(false);
														repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													
												}
												
											}
											
											
										}
									}
									

							
						}
							
						
						
					}
			}

			}else{
				BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(transactionEntry.getId());
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(transactionEntry.getId());
						
					}
				}
				
				List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByBuildChecks1(buildChecks.getId());
				if(!transactionEntryDetailList.isEmpty()) {
					for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
				}
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId1(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						if(employeeMaster2.isEmployeeInactive()==false) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									if(employeePayCodeMaintenance2.getInactive()==false) {
										if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
											TransactionEntry transactionEntry=null;
											transactionEntry = new TransactionEntry();
											transactionEntry.setCreatedDate(new Date());
											
											TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
											Integer increment11=0;
											if(transactionEntry2!=null) {
												if(transactionEntry2.getRowId()!=0) {
													increment11= transactionEntry2.getRowId()+1;
												}else {
													increment11=1;
												}
											}else {
												increment11=1;
											}
											
											transactionEntry.setRowId(increment11);
											transactionEntry.setBatches(buildChecks.getBatches());
											transactionEntry.setEntryNumber(increment11+1);
											transactionEntry.setEntryDate(new Date());
											transactionEntry.setFromDate(new Date());
											transactionEntry.setToDate(new Date());
											transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
											transactionEntry.setFromBuild(true);
											transactionEntry.setIsDeleted(false);
											transactionEntry.setUpdatedRow(new Date());
											transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
											TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
											TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
											transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
											transactionEntryDetail.setEmployeeMaster(employeeMaster2);
											transactionEntryDetail.setDepartment(department);
											transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
											transactionEntryDetail.setTransactionEntry(transactionEntry);
											transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
											transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
											transactionEntryDetail.setFromBuild(true);
											transactionEntryDetail.setTransactionType((short)1);
											transactionEntryDetail.setIsDeleted(false);
											transactionEntryDetail.setFromTranscationEntry(false);
											transactionEntryDetail.setCreatedDate(new Date());
											transactionEntryDetail.setUpdatedBy(loggedInUserId);
											transactionEntryDetail.setUpdatedDate(new Date());
											transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
											transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
											transactionEntryDetail.setUpdatedRow(new Date());
											transactionEntryDetail.setBuildChecks(buildChecks);
											repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
										}
											
										
									}
									
								}
							}
						}
						
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									if(employeeBenefitMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
										if(employeeBenefitMaintenance.getInactive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2!=null) {
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
												}else {
													increment11=1;
												}
												
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(increment11+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
													if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
														  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
													} else {
														transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
													}
												}else {
													transactionEntryDetail.setPayRate(BigDecimal(0.0));
												}
												
												
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setFromTranscationEntry(false);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											
										}
										
									}
									
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId1(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										if(employeeDeductionMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
											if(employeeDeductionMaintenance.getInactive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2!=null) {
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
													}else {
														increment11=1;
													}
													
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(buildChecks.getBatches());
													transactionEntry.setEntryNumber(increment11+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
														if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													transactionEntryDetail.setBuildChecks(buildChecks);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												
											}
											
										}
									
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				batches.setFromtranscation(false);
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							if(employeeMaster2.isEmployeeInactive()==false) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										if(employeePayCodeMaintenance2.getInactive()==false) {
											if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2!=null) {
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
												}else {
													increment11=1;
												}
												
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(batches);
												transactionEntry.setEntryNumber(increment11+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												transactionEntryDetail.setBuildChecks(buildChecks);
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setFromTranscationEntry(false);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
												
											
										}
										
									}
								}
							}
							
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										if(employeeBenefitMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
											if(employeeBenefitMaintenance.getInactive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2!=null) {
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
													}else {
														increment11=1;
													}
													
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(batches);
													transactionEntry.setEntryNumber(increment11+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
														if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												
											}
											
										}
										
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											if(employeeDeductionMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
												if(employeeDeductionMaintenance.getInactive()==false) {
														TransactionEntry transactionEntry=null;
														transactionEntry = new TransactionEntry();
														transactionEntry.setCreatedDate(new Date());
														
														TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
														Integer increment11=0;
														if(transactionEntry2!=null) {
															if(transactionEntry2.getRowId()!=0) {
																increment11= transactionEntry2.getRowId()+1;
															}else {
																increment11=1;
															}
														}else {
															increment11=1;
														}
														
														transactionEntry.setRowId(increment11);
														transactionEntry.setBatches(batches);
														transactionEntry.setEntryNumber(increment11+1);
														transactionEntry.setEntryDate(new Date());
														transactionEntry.setFromDate(new Date());
														transactionEntry.setToDate(new Date());
														transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setFromBuild(true);
														transactionEntry.setIsDeleted(false);
														transactionEntry.setUpdatedRow(new Date());
														transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
														TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
														TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
														transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
														transactionEntryDetail.setEmployeeMaster(employeeMaster2);
														transactionEntryDetail.setDepartment(department);
														transactionEntryDetail.setTransactionEntry(transactionEntry);
														transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
														if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
															if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
																  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
															} else {
																transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
															}
														}else {
															transactionEntryDetail.setPayRate(BigDecimal(0.0));
														}
														
														transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
														transactionEntryDetail.setFromBuild(true);
														transactionEntryDetail.setTransactionType((short)1);
														transactionEntryDetail.setIsDeleted(false);
														transactionEntryDetail.setCreatedDate(new Date());
														transactionEntryDetail.setUpdatedBy(loggedInUserId);
														transactionEntryDetail.setUpdatedDate(new Date());
														transactionEntryDetail.setBuildChecks(buildChecks);
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
														transactionEntryDetail.setUpdatedRow(new Date());
														transactionEntryDetail.setFromTranscationEntry(false);
														repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													
												}
												
											}
											
											
										}
									}
									

							
						}
							
						
						
					}
			}

			}
			
			
		
			
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}
	
	
	
	
	
	public DtoBuildChecks saveOrUpdateFinalVersion1(DtoBuildChecks dtoBuildChecks) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if(dtoBuildChecks.getTypeofPayRun()==1){
				BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(transactionEntry.getId());
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(transactionEntry.getId());
						
					}
				}
				
				List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByBuildChecks1(buildChecks.getId());
				if(!transactionEntryDetailList.isEmpty()) {
					for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
				}
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId1(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						if(employeeMaster2.isEmployeeInactive()==false) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									if(employeePayCodeMaintenance2.getInactive()==false) {
										if(employeePayCodeMaintenance2.getPayCode()!=null) {
											if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2!=null) {
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
												}else {
													increment11=1;
												}
												
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(increment11+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(false);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(false);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setFromTranscationEntry(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												if(transactionEntryDetailLastRecord!=null) {
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												}else {
													transactionEntryDetail.setRowId(1);
												}
												
												transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setBuildChecks(buildChecks);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
										}
										
										
										
									}
									
								}
							}
						}
						
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId3(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
										if(employeeBenefitMaintenance.getInactive()==false) {
											if(employeeBenefitMaintenance.getBenefitCode()!=null) {
												if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
													
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2!=null) {
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
													}else {
														increment11=1;
													}
													
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(buildChecks.getBatches());
													transactionEntry.setEntryNumber(increment11+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
														if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													
													
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													if(transactionEntryDetailLastRecord!=null) {
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													}else {
														transactionEntryDetail.setRowId(1);
													}
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
											}
											
											
										}
										
									}
									
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId3(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
											if(employeeDeductionMaintenance.getInactive()==false) {
												if(employeeDeductionMaintenance.getDeductionCode()!=null) {
													if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
														TransactionEntry transactionEntry=null;
														transactionEntry = new TransactionEntry();
														transactionEntry.setCreatedDate(new Date());
														
														TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
														Integer increment11=0;
														if(transactionEntry2!=null) {
															if(transactionEntry2.getRowId()!=0) {
																increment11= transactionEntry2.getRowId()+1;
															}else {
																increment11=1;
															}
														}else {
															increment11=1;
														}
														
														transactionEntry.setRowId(increment11);
														transactionEntry.setBatches(buildChecks.getBatches());
														transactionEntry.setEntryNumber(increment11+1);
														transactionEntry.setEntryDate(new Date());
														transactionEntry.setFromDate(new Date());
														transactionEntry.setToDate(new Date());
														transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setFromBuild(true);
														transactionEntry.setIsDeleted(false);
														transactionEntry.setUpdatedRow(new Date());
														transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
														TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
														TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
														transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
														transactionEntryDetail.setEmployeeMaster(employeeMaster2);
														transactionEntryDetail.setDepartment(department);
														transactionEntryDetail.setTransactionEntry(transactionEntry);
														transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
														transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
														transactionEntryDetail.setFromBuild(true);
														transactionEntryDetail.setTransactionType((short)1);
														transactionEntryDetail.setIsDeleted(false);
														transactionEntryDetail.setCreatedDate(new Date());
														if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
															if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
																  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
															} else {
																transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
															}
														}else {
															transactionEntryDetail.setPayRate(BigDecimal(0.0));
														}
														
														transactionEntryDetail.setUpdatedBy(loggedInUserId);
														transactionEntryDetail.setUpdatedDate(new Date());
														if(transactionEntryDetailLastRecord!=null) {
															transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
														}else {
															transactionEntryDetail.setRowId(1);
														}
														transactionEntryDetail.setUpdatedRow(new Date());
														transactionEntryDetail.setFromTranscationEntry(false);
														transactionEntryDetail.setBuildChecks(buildChecks);
														repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													}
												}
												
												
											}
											
										}
										
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				batches.setFromtranscation(false);
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							if(employeeMaster2.isEmployeeInactive()==false) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										if(employeePayCodeMaintenance2.getInactive()==false) {
											if(employeePayCodeMaintenance2.getPayCode()!=null) {
												if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
														if(transactionEntry2!=null) {
															if(transactionEntry2.getRowId()!=0) {
																increment11= transactionEntry2.getRowId()+1;
															}else {
																increment11=1;
															}
														}else {
															increment11=1;
														}
													
													
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(batches);
													transactionEntry.setEntryNumber(increment11+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(false);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
													transactionEntryDetail.setFromBuild(false);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													if(transactionEntryDetailLastRecord!=null) {
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													}else {
														transactionEntryDetail.setRowId(1);
													}
													
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
											}
											
												
											
										}
										
									}
								}
							}
							
							
								
								List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId3(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										if(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
											if(employeeBenefitMaintenance.getInactive()==false) {
												if(employeeBenefitMaintenance.getBenefitCode()!=null) {
													if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
														TransactionEntry transactionEntry=null;
														transactionEntry = new TransactionEntry();
														transactionEntry.setCreatedDate(new Date());
														
														TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
														Integer increment11=0;
														if(transactionEntry2!=null) {
															if(transactionEntry2.getRowId()!=0) {
																increment11= transactionEntry2.getRowId()+1;
															}else {
																increment11=1;
															}
														}else {
															increment11=1;
														}
														
														transactionEntry.setRowId(increment11);
														transactionEntry.setBatches(batches);
														transactionEntry.setEntryNumber(increment11+1);
														transactionEntry.setEntryDate(new Date());
														transactionEntry.setFromDate(new Date());
														transactionEntry.setToDate(new Date());
														transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setFromBuild(true);
														transactionEntry.setIsDeleted(false);
														transactionEntry.setUpdatedRow(new Date());
														transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
														TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
														TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
														transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
														transactionEntryDetail.setEmployeeMaster(employeeMaster2);
														transactionEntryDetail.setDepartment(department);
														if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
															if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
																  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
															} else {
																transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
															}
														}else {
															transactionEntryDetail.setPayRate(BigDecimal(0.0));
														}
														
														transactionEntryDetail.setTransactionEntry(transactionEntry);
														transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
														transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
														transactionEntryDetail.setFromBuild(true);
														transactionEntryDetail.setTransactionType((short)1);
														transactionEntryDetail.setIsDeleted(false);
														transactionEntryDetail.setCreatedDate(new Date());
														transactionEntryDetail.setUpdatedBy(loggedInUserId);
														transactionEntryDetail.setUpdatedDate(new Date());
														transactionEntryDetail.setBuildChecks(buildChecks);
														if(transactionEntryDetailLastRecord!=null) {
															transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
														}else {
															transactionEntryDetail.setRowId(1);
														}
														
														transactionEntryDetail.setUpdatedRow(new Date());
														transactionEntryDetail.setFromTranscationEntry(false);
														repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													}
												}
													
												
											}
											
										}
										
									}
								}
								
									
									List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId3(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											if(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()==false) {
												if(employeeDeductionMaintenance.getInactive()==false) {
													if(employeeDeductionMaintenance.getDeductionCode()!=null) {
														if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
															TransactionEntry transactionEntry=null;
															transactionEntry = new TransactionEntry();
															transactionEntry.setCreatedDate(new Date());
															
															TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
															Integer increment11=0;
															if(transactionEntry2!=null) {
																if(transactionEntry2.getRowId()!=0) {
																	increment11= transactionEntry2.getRowId()+1;
																}else {
																	increment11=1;
																}
															}else {
																increment11=1;
															}
															
															transactionEntry.setRowId(increment11);
															transactionEntry.setBatches(batches);
															transactionEntry.setEntryNumber(increment11+1);
															transactionEntry.setEntryDate(new Date());
															transactionEntry.setFromDate(new Date());
															transactionEntry.setToDate(new Date());
															transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
															transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
															transactionEntry.setFromBuild(true);
															transactionEntry.setIsDeleted(false);
															transactionEntry.setUpdatedRow(new Date());
															transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
															TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
															TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
															transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
															transactionEntryDetail.setEmployeeMaster(employeeMaster2);
															transactionEntryDetail.setDepartment(department);
															transactionEntryDetail.setTransactionEntry(transactionEntry);
															transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
															if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
																if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
																	  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
																} else {
																	transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
																}
															}else {
																transactionEntryDetail.setPayRate(BigDecimal(0.0));
															}
															
															transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
															transactionEntryDetail.setFromBuild(true);
															transactionEntryDetail.setTransactionType((short)1);
															transactionEntryDetail.setIsDeleted(false);
															transactionEntryDetail.setCreatedDate(new Date());
															transactionEntryDetail.setUpdatedBy(loggedInUserId);
															transactionEntryDetail.setUpdatedDate(new Date());
															transactionEntryDetail.setBuildChecks(buildChecks);
															if(transactionEntryDetailLastRecord!=null) {
																transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
															}else {
																transactionEntryDetail.setRowId(1);
															}
															
															transactionEntryDetail.setUpdatedRow(new Date());
															transactionEntryDetail.setFromTranscationEntry(false);
															repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
														}
													}
														
													
												}
												
											}
											
											
										}
									}
									

							
						}
							
						
						
					}
			}

			}else{
				BuildChecks buildChecks=null;
			if (dtoBuildChecks.getId() != null && dtoBuildChecks.getId() > 0) {
				buildChecks = repositoryBuildChecks.findByIdAndIsDeleted(dtoBuildChecks.getId(), false);
				
				buildChecks.setUpdatedBy(loggedInUserId);
				buildChecks.setUpdatedDate(new Date());
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				List<TransactionEntry> listOfTranscationEntry = repositoryTransactionEntry.findByBatches(buildChecks.getBatches().getId());
				if(!listOfTranscationEntry.isEmpty()) {
					for (TransactionEntry transactionEntry : listOfTranscationEntry) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetailRow(transactionEntry.getId());
						repositoryTransactionEntry.deleteSingleTranscationEntryRow(transactionEntry.getId());
						
					}
				}
				
				List<TransactionEntryDetail> transactionEntryDetailList = repositoryTransactionEntryDetail.findByBuildChecks1(buildChecks.getId());
				if(!transactionEntryDetailList.isEmpty()) {
					for (TransactionEntryDetail transactionEntryDetail : transactionEntryDetailList) {
						repositoryTransactionEntryDetail.deleteSingleTranscationEntryDetail(transactionEntryDetail.getId());
					}
				}
				List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBenefitsList.isEmpty()) {
					for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
						repositoryBuildPayrollCheckByBenefits.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByBenefits.getId());
					}
					
				}
				List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByDeductionsList.isEmpty()) {
					for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
						repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true, loggedInUserId, buildPayrollCheckByDeductions.getId());
					}
				}
				
				List<BuildPayrollCheckByBatches> buildPayrollCheckByBatchesList= repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.getId());
				if(!buildPayrollCheckByBatchesList.isEmpty()) {
					for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildPayrollCheckByBatchesList) {
						repositoryBuildPayrollCheckByBatches.deleteByBuildCheckId1(true, loggedInUserId, buildPayrollCheckByBatches.getId());
					}
				}
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
				for (Department department : departmentsList) {
					List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
					for (EmployeeMaster employeeMaster2 : employeeMaster) {
						if(employeeMaster2.isEmployeeInactive()==false) {
							List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
							if(!employeePayCodeMaintenance.isEmpty()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
									if(employeePayCodeMaintenance2.getInactive()==false) {
										if(employeePayCodeMaintenance2.getPayCode()!=null) {
											if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
												TransactionEntry transactionEntry=null;
												transactionEntry = new TransactionEntry();
												transactionEntry.setCreatedDate(new Date());
												
												TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
												Integer increment11=0;
												if(transactionEntry2!=null) {
													if(transactionEntry2.getRowId()!=0) {
														increment11= transactionEntry2.getRowId()+1;
													}else {
														increment11=1;
													}
												}else {
													increment11=1;
												}
												
												transactionEntry.setRowId(increment11);
												transactionEntry.setBatches(buildChecks.getBatches());
												transactionEntry.setEntryNumber(increment11+1);
												transactionEntry.setEntryDate(new Date());
												transactionEntry.setFromDate(new Date());
												transactionEntry.setToDate(new Date());
												transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
												transactionEntry.setFromBuild(true);
												transactionEntry.setIsDeleted(false);
												transactionEntry.setUpdatedRow(new Date());
												transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
												TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
												TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
												transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
												transactionEntryDetail.setEmployeeMaster(employeeMaster2);
												transactionEntryDetail.setDepartment(department);
												transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
												transactionEntryDetail.setTransactionEntry(transactionEntry);
												transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
												transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
												transactionEntryDetail.setFromBuild(true);
												transactionEntryDetail.setTransactionType((short)1);
												transactionEntryDetail.setIsDeleted(false);
												transactionEntryDetail.setFromTranscationEntry(false);
												transactionEntryDetail.setCreatedDate(new Date());
												transactionEntryDetail.setUpdatedBy(loggedInUserId);
												transactionEntryDetail.setUpdatedDate(new Date());
												if(transactionEntryDetailLastRecord!=null) {
													transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
												}else {
													transactionEntryDetail.setRowId(1);
												}
												
												transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
												transactionEntryDetail.setUpdatedRow(new Date());
												transactionEntryDetail.setBuildChecks(buildChecks);
												repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
											}
												
										}
										
										
									}
									
								}
							}
						}
						
						
							
						List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId3(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
							if(!employeeBenefitMaintenances.isEmpty()) {
								for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
									if(employeeBenefitMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
										if(employeeBenefitMaintenance.getInactive()==false) {
											if(employeeBenefitMaintenance.getBenefitCode()!=null) {
												if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2!=null) {
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
													}else {
														increment11=1;
													}
													
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(buildChecks.getBatches());
													transactionEntry.setEntryNumber(increment11+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setFromBuild(true);
													if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
														if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
															  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
														} else {
															transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
														}
													}else {
														transactionEntryDetail.setPayRate(BigDecimal(0.0));
													}
													
													
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													if(transactionEntryDetailLastRecord!=null) {
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													}else {
														transactionEntryDetail.setRowId(1);
													}
													
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
											}
												
											
										}
										
									}
									
								}
							}
							
								
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId3(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeDeductionMaintenances.isEmpty()) {
									for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
										if(employeeDeductionMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
											if(employeeDeductionMaintenance.getInactive()==false) {
												if(employeeDeductionMaintenance.getDeductionCode()!=null) {
													if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
														TransactionEntry transactionEntry=null;
														transactionEntry = new TransactionEntry();
														transactionEntry.setCreatedDate(new Date());
														
														TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
														Integer increment11=0;
														if(transactionEntry2!=null) {
															if(transactionEntry2.getRowId()!=0) {
																increment11= transactionEntry2.getRowId()+1;
															}else {
																increment11=1;
															}
														}else {
															increment11=1;
														}
														
														transactionEntry.setRowId(increment11);
														transactionEntry.setBatches(buildChecks.getBatches());
														transactionEntry.setEntryNumber(increment11+1);
														transactionEntry.setEntryDate(new Date());
														transactionEntry.setFromDate(new Date());
														transactionEntry.setToDate(new Date());
														transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setFromBuild(true);
														transactionEntry.setIsDeleted(false);
														transactionEntry.setUpdatedRow(new Date());
														transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
														TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
														TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
														transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
														transactionEntryDetail.setEmployeeMaster(employeeMaster2);
														transactionEntryDetail.setDepartment(department);
														transactionEntryDetail.setTransactionEntry(transactionEntry);
														transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
														transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
														transactionEntryDetail.setFromBuild(true);
														transactionEntryDetail.setTransactionType((short)1);
														transactionEntryDetail.setIsDeleted(false);
														transactionEntryDetail.setCreatedDate(new Date());
														if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
															if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
																  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
															} else {
																transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
															}
														}else {
															transactionEntryDetail.setPayRate(BigDecimal(0.0));
														}
														
														transactionEntryDetail.setUpdatedBy(loggedInUserId);
														transactionEntryDetail.setUpdatedDate(new Date());
														if(transactionEntryDetailLastRecord!=null) {
															transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
														}else {
															transactionEntryDetail.setRowId(1);
														}
														
														transactionEntryDetail.setUpdatedRow(new Date());
														transactionEntryDetail.setFromTranscationEntry(false);
														transactionEntryDetail.setBuildChecks(buildChecks);
														repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													}
												}
													
												
											}
											
										}
									
										
									}
								}
								

						
					}
						
					
					
				}
				
					
				
			} else {
				buildChecks = new BuildChecks();
				buildChecks.setCreatedDate(new Date());
				Integer rowId = repositoryBuildChecks.getCountOfTotalBuildChecks();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				buildChecks.setRowId(increment);
				Default default1=null;
				if(dtoBuildChecks.getDefault1()!=null && dtoBuildChecks.getDefault1().getId()>0){
					default1=repositoryDefault.findOne(dtoBuildChecks.getDefault1().getId());
				}
				if(dtoBuildChecks.getListDepartmentId()!=null && !dtoBuildChecks.getListDepartmentId().isEmpty()) {
					List<Department>departments=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					buildChecks.setListDepartment(departments);
				}
				
				if(dtoBuildChecks.getListEmployeeId()!=null && !dtoBuildChecks.getListEmployeeId().isEmpty()) {
					List<EmployeeMaster> employeeMasters=repositoryEmployeeMaster.findAllEmployeeListId(dtoBuildChecks.getListEmployeeId());
					buildChecks.setListEmployee(employeeMasters);
				}
				buildChecks.setDefault1(default1);
				buildChecks.setDescription(dtoBuildChecks.getDescription());
				buildChecks.setCheckByUserId(dtoBuildChecks.getCheckByUserId());
				buildChecks.setCheckDate(dtoBuildChecks.getCheckDate());
				buildChecks.setTypeofPayRun(dtoBuildChecks.getTypeofPayRun());
				buildChecks.setDateFrom(dtoBuildChecks.getDateFrom());
				buildChecks.setDateTo(dtoBuildChecks.getDateTo());
				buildChecks.setWeekly(dtoBuildChecks.getWeekly());
				buildChecks.setBiweekly(dtoBuildChecks.getBiweekly());
				buildChecks.setSemiannually(dtoBuildChecks.getSemiannually());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setQuarterly(dtoBuildChecks.getQuarterly());
				buildChecks.setSemimonthly(dtoBuildChecks.getSemimonthly());
				buildChecks.setMonthly(dtoBuildChecks.getMonthly());
				buildChecks.setAnnually(dtoBuildChecks.getAnnually());
				buildChecks.setDailyMisc(dtoBuildChecks.getDailyMisc());
				buildChecks.setAllEmployees(dtoBuildChecks.getAllEmployees());
				buildChecks.setFromEmployeeId(dtoBuildChecks.getFromEmployeeId());
				buildChecks.setToEmployeeId(dtoBuildChecks.getToEmployeeId());
				buildChecks.setAllDepertment(dtoBuildChecks.getAllDepertment());
				buildChecks.setFromDepartmentId(dtoBuildChecks.getFromDepartmentId());
				buildChecks.setToDepartmentId(dtoBuildChecks.getToDepartmentId());
				buildChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				Batches batches = null;
				batches = new Batches();
				batches.setCreatedDate(new Date());
				Integer rowId1 = repositoryBatches.getCountOfTotalAtteandaces();
				Integer increment1=0;
				if(rowId1!=0) {
					increment1= rowId1+1;
				}else {
					increment1=1;
				}
				batches.setRowId(increment1);
				batches.setPostingDate(new Date());
				batches.setApproved(true);
				batches.setArabicDescription("BATCH FOR BUILD CHECK");
				batches.setBatchId("BATCH FOR BUILD CHECK");
				batches.setDescription("BATCH FOR BUILD CHECK");
				batches.setApprovedDate(new Date());
				batches.setUserID(String.valueOf(loggedInUserId));
				batches.setUpdatedBy(loggedInUserId);
				batches.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
				batches.setFromBuild(true);
				batches.setIsDeleted(true);
				batches.setTotalTransactions((double)0);
				batches.setFromtranscation(false);
				repositoryBatches.saveAndFlush(batches);
				buildChecks.setBatches(batches);
				buildChecks = repositoryBuildChecks.saveAndFlush(buildChecks);
				dtoBuildChecks.setId(buildChecks.getId());
							
				
				
				
				
				List<Department>departmentsList=repositoryDepartment.findAllDepartmentListId(dtoBuildChecks.getListDepartmentId());
					for (Department department : departmentsList) {
						List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeByDepartmentList2(department.getId());
						for (EmployeeMaster employeeMaster2 : employeeMaster) {
							if(employeeMaster2.isEmployeeInactive()==false) {
								List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
								if(!employeePayCodeMaintenance.isEmpty()) {
									for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
										if(employeePayCodeMaintenance2.getInactive()==false) {
											if(employeePayCodeMaintenance2.getPayCode()!=null) {
												if(employeePayCodeMaintenance2.getPayCode().isInActive()==false) {
													TransactionEntry transactionEntry=null;
													transactionEntry = new TransactionEntry();
													transactionEntry.setCreatedDate(new Date());
													
													TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
													Integer increment11=0;
													if(transactionEntry2!=null) {
														if(transactionEntry2.getRowId()!=0) {
															increment11= transactionEntry2.getRowId()+1;
														}else {
															increment11=1;
														}
													}else {
														increment11=1;
													}
													
													transactionEntry.setRowId(increment11);
													transactionEntry.setBatches(batches);
													transactionEntry.setEntryNumber(increment11+1);
													transactionEntry.setEntryDate(new Date());
													transactionEntry.setFromDate(new Date());
													transactionEntry.setToDate(new Date());
													transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
													transactionEntry.setFromBuild(true);
													transactionEntry.setIsDeleted(false);
													transactionEntry.setUpdatedRow(new Date());
													transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
													TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
													TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
													transactionEntryDetail.setCode(employeePayCodeMaintenance2.getPayCode());
													transactionEntryDetail.setEmployeeMaster(employeeMaster2);
													transactionEntryDetail.setDepartment(department);
													transactionEntryDetail.setTransactionEntry(transactionEntry);
													transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
													transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
													transactionEntryDetail.setPayRate(employeePayCodeMaintenance2.getPayRate());
													transactionEntryDetail.setFromBuild(true);
													transactionEntryDetail.setTransactionType((short)1);
													transactionEntryDetail.setIsDeleted(false);
													transactionEntryDetail.setCreatedDate(new Date());
													transactionEntryDetail.setUpdatedBy(loggedInUserId);
													transactionEntryDetail.setUpdatedDate(new Date());
													if(transactionEntryDetailLastRecord!=null) {
														transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
													}else {
														transactionEntryDetail.setRowId(1);
													}
													
													transactionEntryDetail.setBuildChecks(buildChecks);
													transactionEntryDetail.setUpdatedRow(new Date());
													transactionEntryDetail.setFromTranscationEntry(false);
													repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
												}
											}
											
												
											
										}
										
									}
								}
							}
							
							
								
							List<EmployeeBenefitMaintenance>employeeBenefitMaintenances=repositoryEmployeeBenefitMaintenance.findByEmployeeId3(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
								if(!employeeBenefitMaintenances.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenances) {
										if(employeeBenefitMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
											if(employeeBenefitMaintenance.getInactive()==false) {
												if(employeeBenefitMaintenance.getBenefitCode()!=null) {
													if(employeeBenefitMaintenance.getBenefitCode().isInActive()==false) {
														TransactionEntry transactionEntry=null;
														transactionEntry = new TransactionEntry();
														transactionEntry.setCreatedDate(new Date());
														
														TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
														Integer increment11=0;
														if(transactionEntry2!=null) {
															if(transactionEntry2.getRowId()!=0) {
																increment11= transactionEntry2.getRowId()+1;
															}else {
																increment11=1;
															}
														}else {
															increment11=1;
														}
														
														transactionEntry.setRowId(increment11);
														transactionEntry.setBatches(batches);
														transactionEntry.setEntryNumber(increment11+1);
														transactionEntry.setEntryDate(new Date());
														transactionEntry.setFromDate(new Date());
														transactionEntry.setToDate(new Date());
														transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
														transactionEntry.setFromBuild(true);
														transactionEntry.setIsDeleted(false);
														transactionEntry.setUpdatedRow(new Date());
														transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
														TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
														TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
														transactionEntryDetail.setBenefitCode(employeeBenefitMaintenance.getBenefitCode());
														transactionEntryDetail.setEmployeeMaster(employeeMaster2);
														transactionEntryDetail.setDepartment(department);
														if(employeeBenefitMaintenance.getPerPeriord()!=null && employeeBenefitMaintenance.getBenefitAmount()!=null) {
															if(employeeBenefitMaintenance.getPerPeriord().compareTo(employeeBenefitMaintenance.getBenefitAmount()) < 0) {
																  transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getPerPeriord());
															} else {
																transactionEntryDetail.setPayRate(employeeBenefitMaintenance.getBenefitAmount());
															}
														}else {
															transactionEntryDetail.setPayRate(BigDecimal(0.0));
														}
														transactionEntryDetail.setTransactionEntry(transactionEntry);
														transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
														transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
														transactionEntryDetail.setFromBuild(true);
														transactionEntryDetail.setTransactionType((short)1);
														transactionEntryDetail.setIsDeleted(false);
														transactionEntryDetail.setCreatedDate(new Date());
														transactionEntryDetail.setUpdatedBy(loggedInUserId);
														transactionEntryDetail.setUpdatedDate(new Date());
														transactionEntryDetail.setBuildChecks(buildChecks);
														if(transactionEntryDetailLastRecord!=null) {
															transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
														}else {
															transactionEntryDetail.setRowId(1);
														}
														
														transactionEntryDetail.setUpdatedRow(new Date());
														transactionEntryDetail.setFromTranscationEntry(false);
														repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
													}
												}
												
													
												
											}
											
										}
										
									}
								}
								
									
								List<EmployeeDeductionMaintenance>employeeDeductionMaintenances=repositoryEmployeeDeductionMaintenance.findByEmployeeId3(employeeMaster2.getEmployeeIndexId(),dtoBuildChecks.getDateFrom(),dtoBuildChecks.getDateTo());
									if(!employeeDeductionMaintenances.isEmpty()) {
										for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenances) {
											if(employeeDeductionMaintenance.getEmployeeMaster().isEmployeeInactive()==false) {
												if(employeeDeductionMaintenance.getInactive()==false) {
													if(employeeDeductionMaintenance.getDeductionCode()!=null) {
														if(employeeDeductionMaintenance.getDeductionCode().isInActive()==false) {
															TransactionEntry transactionEntry=null;
															transactionEntry = new TransactionEntry();
															transactionEntry.setCreatedDate(new Date());
															
															TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
															Integer increment11=0;
															if(transactionEntry2!=null) {
																if(transactionEntry2.getRowId()!=0) {
																	increment11= transactionEntry2.getRowId()+1;
																}else {
																	increment11=1;
																}
															}else {
																increment11=1;
															}
															
															transactionEntry.setRowId(increment11);
															transactionEntry.setBatches(batches);
															transactionEntry.setEntryNumber(increment11+1);
															transactionEntry.setEntryDate(new Date());
															transactionEntry.setFromDate(new Date());
															transactionEntry.setToDate(new Date());
															transactionEntry.setDescription("TRANSACTION FOR BUILD CHECK");
															transactionEntry.setArabicDescription("TRANSACTION FOR BUILD CHECK");
															transactionEntry.setFromBuild(true);
															transactionEntry.setIsDeleted(false);
															transactionEntry.setUpdatedRow(new Date());
															transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
															TransactionEntryDetail transactionEntryDetailLastRecord =repositoryTransactionEntryDetail.findTopByOrderByIdDesc();
															TransactionEntryDetail transactionEntryDetail = new TransactionEntryDetail();
															transactionEntryDetail.setDeductionCode(employeeDeductionMaintenance.getDeductionCode());
															transactionEntryDetail.setEmployeeMaster(employeeMaster2);
															transactionEntryDetail.setDepartment(department);
															transactionEntryDetail.setTransactionEntry(transactionEntry);
															transactionEntryDetail.setEntryNumber(transactionEntry.getRowId()+1);
															if(employeeDeductionMaintenance.getDeductionAmount()!=null && employeeDeductionMaintenance.getPerPeriord()!=null) {
																if(employeeDeductionMaintenance.getPerPeriord().compareTo(employeeDeductionMaintenance.getDeductionAmount()) < 0) {
																	  transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getPerPeriord());
																} else {
																	transactionEntryDetail.setPayRate(employeeDeductionMaintenance.getDeductionAmount());
																}
															}else {
																transactionEntryDetail.setPayRate(BigDecimal(0.0));
															}
															
															transactionEntryDetail.setDetailsSequence(transactionEntry.getRowId()+1);
															transactionEntryDetail.setFromBuild(true);
															transactionEntryDetail.setTransactionType((short)1);
															transactionEntryDetail.setIsDeleted(false);
															transactionEntryDetail.setCreatedDate(new Date());
															transactionEntryDetail.setUpdatedBy(loggedInUserId);
															transactionEntryDetail.setUpdatedDate(new Date());
															transactionEntryDetail.setBuildChecks(buildChecks);
															if(transactionEntryDetailLastRecord!=null) {
																transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId()+1);
															}else {
																transactionEntryDetail.setRowId(1);
															}
															
															transactionEntryDetail.setUpdatedRow(new Date());
															transactionEntryDetail.setFromTranscationEntry(false);
															repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);
														}
													}
													
														
													
												}
												
											}
											
											
										}
									}
									

							
						}
							
						
						
					}
			}

			}
			
			
		
			
				
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoBuildChecks;
	}
	
	
	
	
	
	
	
	
}
