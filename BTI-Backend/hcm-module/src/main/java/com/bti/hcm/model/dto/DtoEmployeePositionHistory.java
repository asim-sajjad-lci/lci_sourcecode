package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.EmployeePositionHistory;

public class DtoEmployeePositionHistory extends DtoBase{

	
	private Integer id;
	private DtoEmployeePositionReason dtoEmployeePositionReason;
	private DtoEmployeeMaster employeeMaster;
	private Date positionEffectiveDate;
	private DtoDivision division;
	private DtoDepartment department;
	private DtoPosition position;
	private DtoLocation location;
	private DtoSupervisor supervisor;
	private short employeeType;
	
	private List<DtoEmployeePositionHistory> delete;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public DtoEmployeePositionReason getDtoEmployeePositionReason() {
		return dtoEmployeePositionReason;
	}
	public void setDtoEmployeePositionReason(DtoEmployeePositionReason dtoEmployeePositionReason) {
		this.dtoEmployeePositionReason = dtoEmployeePositionReason;
	}
	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}
	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	public Date getPositionEffectiveDate() {
		return positionEffectiveDate;
	}
	public void setPositionEffectiveDate(Date positionEffectiveDate) {
		this.positionEffectiveDate = positionEffectiveDate;
	}
	public DtoDivision getDivision() {
		return division;
	}
	public void setDivision(DtoDivision division) {
		this.division = division;
	}
	
	public DtoDepartment getDepartment() {
		return department;
	}
	public void setDepartment(DtoDepartment department) {
		this.department = department;
	}
	public DtoPosition getPosition() {
		return position;
	}
	public void setPosition(DtoPosition position) {
		this.position = position;
	}
	public DtoLocation getLocation() {
		return location;
	}
	public void setLocation(DtoLocation location) {
		this.location = location;
	}
	public DtoSupervisor getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(DtoSupervisor supervisor) {
		this.supervisor = supervisor;
	}
	
	public short getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(short employeeType) {
		this.employeeType = employeeType;
	}
	public List<DtoEmployeePositionHistory> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoEmployeePositionHistory> delete) {
		this.delete = delete;
	}
	
	public DtoEmployeePositionHistory() {
		
	}
	
	public DtoEmployeePositionHistory(EmployeePositionHistory employeePositionHistory) {
		
	}
	
}
