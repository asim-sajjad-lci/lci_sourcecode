package com.bti.hcm.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40721",indexes = {
        @Index(columnList = "TERMTINDXD")
})
public class TerminationSetupDetails extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TERMTINDXD")
	private Integer id;

	
	@ManyToOne
	@JoinColumn(name = "TERMINDX")
	private TerminationSetup terminationSetup;

	
	
	@Column(name = "TERMCHKSEQN")
	private Integer terminationSequence;

	@Column(name = "TERMCHKDSCR", columnDefinition = "char(150)")
	private String terminationDesc;

	@Column(name = "TERMCHKDSCRA", columnDefinition = "char(150)")
	private String terminationArbicDesc;

	@Column(name = "TERMCHKCOMDT")
	private Date terminationDate;

	@Column(name = "TERMCHKRESP", columnDefinition = "char(150)")
	private String terminationPersonName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTerminationSequence() {
		return terminationSequence;
	}

	public void setTerminationSequence(Integer terminationSequence) {
		this.terminationSequence = terminationSequence;
	}

	public String getTerminationDesc() {
		return terminationDesc;
	}

	public void setTerminationDesc(String terminationDesc) {
		this.terminationDesc = terminationDesc;
	}

	public String getTerminationArbicDesc() {
		return terminationArbicDesc;
	}

	public void setTerminationArbicDesc(String terminationArbicDesc) {
		this.terminationArbicDesc = terminationArbicDesc;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getTerminationPersonName() {
		return terminationPersonName;
	}

	public void setTerminationPersonName(String terminationPersonName) {
		this.terminationPersonName = terminationPersonName;
	}

	public TerminationSetup getTerminationSetup() {
		return terminationSetup;
	}

	public void setTerminationSetup(TerminationSetup terminationSetup) {
		this.terminationSetup = terminationSetup;
	}

}
