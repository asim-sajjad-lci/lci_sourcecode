/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.HijriYear;

/**
 * Description: Interface for Hijri Year
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 * 
 *
 */
@Repository("repositoryHijriYear")
public interface RepositoryHijriYear extends JpaRepository<HijriYear, Integer> {

	/**
	 * @param deleted
	 * @return
	 */
	public List<HijriYear> findByIsDeleted(Boolean deleted);	
	
	/**
	 * 
	 * @param Id
	 * @return
	 */
	
}
