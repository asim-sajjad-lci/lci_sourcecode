package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40213",indexes = {
        @Index(columnList = "ACCBYINDX")
})
public class AccrualType extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACCBYINDX")
	private Integer id;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accrualType")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<Accrual> listAccrual;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accrualType")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<AccrualScheduleDetail> listAccrualScheduleDetail;

	@Column(name = "ACCBYDSCR", length = 31)
	private String desc;

	@Column(name = "ACCBYTYP")
	private short type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public List<Accrual> getListAccrual() {
		return listAccrual;
	}

	public void setListAccrual(List<Accrual> listAccrual) {
		this.listAccrual = listAccrual;
	}

	public List<AccrualScheduleDetail> getListAccrualScheduleDetail() {
		return listAccrualScheduleDetail;
	}

	public void setListAccrualScheduleDetail(List<AccrualScheduleDetail> listAccrualScheduleDetail) {
		this.listAccrualScheduleDetail = listAccrualScheduleDetail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listAccrual == null) ? 0 : listAccrual.hashCode());
		result = prime * result + ((listAccrualScheduleDetail == null) ? 0 : listAccrualScheduleDetail.hashCode());
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
