/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoDivision;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceDivision;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerDivision
 * Name of Project: Hcm
 * Version: 0.0.1
 */

@RestController
@RequestMapping("/division")
public class ControllerDivision extends BaseController{
	
	/**
	 * @Description LOGGER use for put a logger in Divison Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerDivision.class);
	
	/**
	 * @Description serviceDivision Autowired here using annotation of spring for use of serviceDivision method in this controller
	 */
	
	@Autowired
	ServiceDivision serviceDivision;
	

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @param request
	 * @param dtoDivision
	 * @return
	 * @request{
		  "divisionId":"Division002",
		  "divisionDescription":"This is test division",
		  "arabicDivisionDescription":"This is test division",
		  		  "divisonAddress":"This is Division Address",
		  "city":"Surat",
		  "phoneNumber":"1878151230",
		  "fax":"1781541230",
		  "email":"sample@gmail.com"
		}
	 * @response{
			"code": 201,
			"status": "CREATED",
			"result": {
				"divisionId": "Division002",
				"divisionDescription": "This is test division",
				"arabicDivisionDescription": "This is test division",
				"phoneNumber": "1878151230",
				"fax": "1781541230",
				"email": "sample@gmail.com",
				"city": "Surat"
			},
				"btiMessage": {
					"message": "Division created successfully",
					"messageShort": "DIVISION_CREATED"
				}
			}	
	 * @throws Exception 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createDivison(HttpServletRequest request, @RequestBody DtoDivision dtoDivision) throws Exception {
		LOGGER.info("Create Division Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDivision = serviceDivision.saveOrUpdateDivision(dtoDivision);
			responseMessage=displayMessage(dtoDivision, "DIVISION_CREATED", "DIVISION_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Division Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoDivision
	 * @return
	 * @request{
			  "id":"1",
			  "divisionId":"Division001",
			  "divisionDescription":"This is division Description",
			  "arbicDivisionDescription":"This is Aarbic Descriptions",
			  "divisonAddress":"This is Division Address",
			  "city":"Surat",
			  "phoneNumber":"1878151230",
			  "fax":"1781541230",
			  "email":"sample@gmail.com"
			}
	 * @response{
					"code": 201,
					"status": "CREATED",
					"result": {
					"id": 1,
					"divisionId": "Division001",
					"divisionDescription": "This is division Description",
					"phoneNumber": "1878151230",
					"fax": "1781541230",
					"email": "sample@gmail.com",
					"city": "Surat"
				},
				"btiMessage": {
				"message": "Division updated successfully",
				"messageShort": "DIVISION_UPDATE_SUCCESS"
				}
			}
	 * @throws Exception 
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateDivision(HttpServletRequest request, @RequestBody DtoDivision dtoDivision) throws Exception {
	LOGGER.info("Update Division Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDivision = serviceDivision.saveOrUpdateDivision(dtoDivision);
			responseMessage=displayMessage(dtoDivision, MessageConstant.DIVISION_UPDATE_SUCCESS, MessageConstant.DIVISION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Division Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoDivision
	 * @return
	 * @request{
		  "ids":[1]
		}
	 * @response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"deleteMessage": "N/A",
			"associateMessage": "N/A",
			"deleteDivision": [
			  {
				"id": 1,
				"divisionDescription": "This is division Description"
					}
				],
			},
			"btiMessage": {
				"message": "Division delete successfully",
				"messageShort": "DIVISION_DELETED"
				}
			}
	 * @throws Exception 
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteDivision(HttpServletRequest request, @RequestBody DtoDivision dtoDivision) throws Exception {
		LOGGER.info("Delete Division Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoDivision.getIds() != null && !dtoDivision.getIds().isEmpty()) {
				DtoDivision dtoDivision2 =serviceDivision.deleteDivision(dtoDivision.getIds());
				if (dtoDivision2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("DIVISION_DELETED", false), dtoDivision2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("DIVISION_NOT_DELETE_ID_MESSAGE", false), dtoDivision2);
				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Division Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoDivision
	 * @return
	 * @request{
		 "pageNumber":"0",
		  "pageSize":"10"
		}
	 *@response{
					"code": 201,
					"status": "CREATED",
					"result": {
					"pageNumber": 0,
					"pageSize": 10,
					"totalCount": 7,
					"records": [
					  {
						"id": 7,
						"divisionId": "Division002",
						"divisionDescription": "This is test division",
						"arabicDivisionDescription": "This is test division",
						"divisionAddress": "",
						"phoneNumber": "1878151230",
						"fax": "1878151230",
						"email": "sample@gmail.com",
						"city": "Surat"
					},
					  {
						"id": 6,
						"divisionId": "A006",
						"divisionDescription": "TEST 2",
						"arabicDivisionDescription": "TEST 2",
						"divisionAddress": "Ahmedabad",
						"phoneNumber": "123456789",
						"fax": "123456789",
						"email": "ss@ss.ss",
						"city": "Ahmedabad"
					},
					  {
						"id": 5,
						"divisionId": "A005",
						"divisionDescription": "Test",
						"arabicDivisionDescription": "test",
						"divisionAddress": "Ahmedabad",
						"phoneNumber": "1234567890",
						"fax": "1234567890",
						"email": "a@a.aa",
						"city": "Ahmedabad"
					},
					  {
						"id": 4,
						"divisionId": "A004",
						"divisionDescription": "Food court",
						"arabicDivisionDescription": "Food court",
						"divisionAddress": "Ananad",
						"phoneNumber": "1234567890",
						"fax": "1234567890",
						"email": "a@a.aa",
						"city": "Ananad"
					},
					  {
						"id": 3,
						"divisionId": "A003",
						"divisionDescription": "This is test division",
						"arabicDivisionDescription": "his is test division",
						"divisionAddress": "Surat",
						"phoneNumber": "1878151230",
						"fax": "1878151230",
						"email": "sample@gmail.com",
						"city": "Surat"
					},
					  {
						"id": 2,
						"divisionId": "A002",
						"divisionDescription": "This is test division",
						"arabicDivisionDescription": "This is test division",
						"divisionAddress": "Ahmedabad",
						"phoneNumber": "1878151230",
						"fax": "1878151230",
						"email": "sample@gmail.com",
						"city": "Ahmedabad"
					},
					  {
								"id": 1,
								"divisionId": "A001",
								"divisionDescription": "This is division Description",
								"arabicDivisionDescription": "This is Aarbic Description",
								"divisionAddress": "Surat",
								"phoneNumber": "1878151230",
								"fax": "1878151230",
								"email": "sample@gmail.com",
								"city": "Surat"
							}
						],
					},
							"btiMessage": {
								"message": "Division list fetched successfully",
								"messageShort": "DIVISON_GET_ALL"
							}
		}
	 * @throws Exception 

	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAllDivision(HttpServletRequest request, @RequestBody DtoDivision dtoDivision) throws Exception {
		LOGGER.info("Get All Division Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			com.bti.hcm.model.dto.DtoSearch dtoSearch = serviceDivision.getAllDivison(dtoDivision);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DIVISON_GET_ALL, MessageConstant.DIVISION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All Division Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	/**
	 * @param request
	 * @param dtoDivision
	 * @return
	 * @request{
			 "id":"1"
		}
	
	 * @response{
					"code": 201,
					"status": "CREATED",
					"result": {
					"divisionId": "A001",
					"divisionDescription": "This is division Description",
					"arabicDivisionDescription": "This is Aarbic Description",
					"divisionAddress": "Surat",
					"phoneNumber": "1878151230",
					"fax": "1878151230",
					"email": "sample@gmail.com",
					"city": "Surat"
				},
				"btiMessage": {
					"message": "Division details fetched successfully",
					"messageShort": "DIVISION_GET_DETAIL"
				}
			}
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getDivisionById", method = RequestMethod.POST)
	public ResponseMessage getDivisionById(HttpServletRequest request, @RequestBody DtoDivision dtoDivision) throws Exception {
		LOGGER.info("Get Division ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoDivision dtoDivisionObj = serviceDivision.getDivisionById(dtoDivision.getId());
			responseMessage=displayMessage(dtoDivisionObj, "DIVISION_GET_DETAIL", "DIVISION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Division ById Method:"+dtoDivision.getId());
		return responseMessage;
	}

	/**
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @request{
		  "searchKeyword":"This",
		  "pageNumber" : 0,
		  "pageSize" :2
		 
		}
	 * @response{
			"code": 200,
			"status": "OK",
			"result": {
			"searchKeyword": "This",
			"pageNumber": 0,
			"pageSize": 2,
			"totalCount": 4,
			"records": [
			  {
					"divisionId": "Division002",
					"divisionDescription": "This is test division",
					"arabicDivisionDescription": "This is test division",
					"divisionAddress": "",
					"phoneNumber": "1878151230",
					"fax": "1878151230",
					"email": "sample@gmail.com",
					"city": "Surat"
			},
			  {
					"divisionId": "A003",
					"divisionDescription": "This is test division",
					"arabicDivisionDescription": "This is test division",
					"divisionAddress": "Surat",
					"phoneNumber": "1878151230",
					"fax": "1878151230",
					"email": "sample@gmail.com",
					"city": "Surat"
				}
			],
			},
			"btiMessage": {
					"message": "Division list fetched successfully",
					"messageShort": "DIVISON_GET_ALL"
				}
			}
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchDepartment(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDivision.searchDivision(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DIVISON_GET_ALL, MessageConstant.DIVISION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoDivision
	 * @return
	 * @throws Exception 
	 * @request {
		  "divisionId":"Div001"
		 
		}
	 * @response{
				"code": 302,
				"status": "FOUND",
				"result": {
					"isRepeat": true
				},
				"btiMessage": {
					"message": "Division result found",
					"messageShort": "DIVISION_RESULT"
				}
		}
	 */
	@RequestMapping(value = "/divisionIdcheck", method = RequestMethod.POST)
	public ResponseMessage divisionIdCheck(HttpServletRequest request, @RequestBody DtoDivision dtoDivision) throws Exception {
		LOGGER.info("departmetnIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoDivision dtoDivisionObj = serviceDivision.repeatByDivisiontId(dtoDivision.getDivisionId());
			responseMessage=displayMessage(dtoDivisionObj, "DIVISION_EXIST", "DIVISION_REPEAT_DIVISIONID_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchDivisionId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchDivisionId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Division Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDivision.searchDivisiontId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "DIVISON_GET_ALL", "DIVISION_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllDivisionDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllDivisionDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoDivision> divisionList = serviceDivision.getAllDivisionDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DIVISON_GET_ALL", false), divisionList);
			}else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getAllDivisionId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllDivisionId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Division Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDivision.getAllDivisionId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DIVISION_GET_ALL, MessageConstant.DIVISION_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	
}
