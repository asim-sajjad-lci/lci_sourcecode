package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40163",indexes = {
        @Index(columnList = "POTCRSINDX")
})
public class PositionCourseToLink extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POTCRSINDX")
	private Integer id;

	@Column(name = "POTCRSSEQN")
	private Integer sequence;

	@ManyToOne
	@JoinColumn(name = "TRNCCLSINDX")
	private TrainingCourseDetail trainingClass;

	@ManyToOne
	@JoinColumn(name = "POTINDX")
	private Position position;

	@Transient
	private Integer traningCourseId;

	@ManyToOne
	@JoinColumn(name = "TRNCINDX")
	private TraningCourse traningCourse;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "positionCourse")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<PositionCourseDetail> listPositionCourseDetail;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public TraningCourse getTraningCourse() {
		return traningCourse;
	}

	public void setTraningCourse(TraningCourse traningCourse) {
		this.traningCourse = traningCourse;
	}

	public Integer getTraningCourseId() {
		return traningCourseId;
	}

	public void setTraningCourseId(Integer traningCourseId) {
		this.traningCourseId = traningCourseId;
	}

	public List<PositionCourseDetail> getListPositionCourseDetail() {
		return listPositionCourseDetail;
	}

	public void setListPositionCourseDetail(List<PositionCourseDetail> listPositionCourseDetail) {
		this.listPositionCourseDetail = listPositionCourseDetail;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public TrainingCourseDetail getTrainingClass() {
		return trainingClass;
	}

	public void setTrainingClass(TrainingCourseDetail trainingClass) {
		this.trainingClass = trainingClass;
	}

}
