/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the Requisitions database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR41101",indexes = {
        @Index(columnList = "REQUIINDX")
})
@NamedQuery(name = "Requisitions.findAll", query = "SELECT r FROM Requisitions r")
public class Requisitions extends HcmBaseEntity implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REQUIINDX")
	private Integer id;
	
	@Column(name = "STATS")
	private short stats;
	
	@Column(name="INTRPOSTDT")
	private Date internalPostDate;
	
	@Column(name="INTRCLSDT")
	private Date internalCloseDate;
	
	
	@Column(name="OPNDT")
	private Date openingDate;
	
	@Column(name="RECRNMA")
	private String recruiterName;
	
	@ManyToOne
	@JoinColumn(name="COMID")
	private Company companyId;
	
	@Column(name="MANGRNAM", columnDefinition = "char(150)")
	private String managerName;
	
	@Column(name="JOBPOST")
	private short jobPosting;
	
	@Column(name="ADVERSF1", columnDefinition = "char(90)")
	private String advertisingField1;
	
	@Column(name="ADVERSF2", columnDefinition = "char(90)")
	private String advertisingField2;
	
	@Column(name="ADVERSF3",columnDefinition = "char(90)")
	private String advertisingField3;
	
	@Column(name="ADVERSF4",columnDefinition = "char(90)")
	private String advertisingField4;
	
	@Column(name="POSTAVAVLE")
	private int positionsAvailable;
	
	@Column(name="POSTFILL")
	private int positionsFilled;
	
	@Column(name="APPLAPLY")
	private int applicantsApplied;
	
	@Column(name="APPLINTERV")
	private int applicantsInterviewed;
	
	@Column(name="COSTADVIRS",precision = 13,scale=3)
	private BigDecimal costadvirs;
	
	
	@Column(name="COSTRECRI",precision = 13,scale=3)
	private BigDecimal costrecri;
	
	@ManyToOne
	@JoinColumn(name="DEPINDX")
	private Department department;
	
	@ManyToOne
	@JoinColumn(name="DIVINDX")
	private Division division;
	
	
	@ManyToOne
	@JoinColumn(name="SUPRINDX")
	private Supervisor supervisor;
	
	@ManyToOne
	@JoinColumn(name="POTINDX")
	private Position position;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public short getStats() {
		return stats;
	}

	public void setStats(short stats) {
		this.stats = stats;
	}

	public Date getInternalPostDate() {
		return internalPostDate;
	}

	public void setInternalPostDate(Date internalPostDate) {
		this.internalPostDate = internalPostDate;
	}

	public Date getInternalCloseDate() {
		return internalCloseDate;
	}

	public void setInternalCloseDate(Date internalCloseDate) {
		this.internalCloseDate = internalCloseDate;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public String getRecruiterName() {
		return recruiterName;
	}

	public void setRecruiterName(String recruiterName) {
		this.recruiterName = recruiterName;
	}

	

	public Company getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Company companyId) {
		this.companyId = companyId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public short getJobPosting() {
		return jobPosting;
	}

	public void setJobPosting(short jobPosting) {
		this.jobPosting = jobPosting;
	}

	public String getAdvertisingField1() {
		return advertisingField1;
	}

	public void setAdvertisingField1(String advertisingField1) {
		this.advertisingField1 = advertisingField1;
	}

	public String getAdvertisingField2() {
		return advertisingField2;
	}

	public void setAdvertisingField2(String advertisingField2) {
		this.advertisingField2 = advertisingField2;
	}

	public String getAdvertisingField3() {
		return advertisingField3;
	}

	public void setAdvertisingField3(String advertisingField3) {
		this.advertisingField3 = advertisingField3;
	}

	public String getAdvertisingField4() {
		return advertisingField4;
	}

	public void setAdvertisingField4(String advertisingField4) {
		this.advertisingField4 = advertisingField4;
	}

	public int getPositionsAvailable() {
		return positionsAvailable;
	}

	public void setPositionsAvailable(int positionsAvailable) {
		this.positionsAvailable = positionsAvailable;
	}

	public int getPositionsFilled() {
		return positionsFilled;
	}

	public void setPositionsFilled(int positionsFilled) {
		this.positionsFilled = positionsFilled;
	}

	public int getApplicantsApplied() {
		return applicantsApplied;
	}

	public void setApplicantsApplied(int applicantsApplied) {
		this.applicantsApplied = applicantsApplied;
	}

	public int getApplicantsInterviewed() {
		return applicantsInterviewed;
	}

	public void setApplicantsInterviewed(int applicantsInterviewed) {
		this.applicantsInterviewed = applicantsInterviewed;
	}

	public BigDecimal getCostadvirs() {
		return costadvirs;
	}

	public void setCostadvirs(BigDecimal costadvirs) {
		this.costadvirs = costadvirs;
	}

	public BigDecimal getCostrecri() {
		return costrecri;
	}

	public void setCostrecri(BigDecimal costrecri) {
		this.costrecri = costrecri;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
	
	




	
		

	
	
}
