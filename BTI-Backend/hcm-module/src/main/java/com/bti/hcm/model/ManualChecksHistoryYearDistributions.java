package com.bti.hcm.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90602")
public class ManualChecksHistoryYearDistributions extends HcmBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMPYMTINDXD")
	private Integer id;

	@Column(name = "YEAR1")
	private Integer year;

	@Column(name = "DITPOTTYP")
	private Short postingTypes;

	@Column(name = "HCMCODINX")
	private Integer codeId;

	@Column(name = "DEBTAMT", precision = 10, scale = 3)
	private BigDecimal debitAmount;

	@Column(name = "CRDTAMT", precision = 10, scale = 3)
	private BigDecimal creditAmount;

	@Column(name = "ACCTSEQN")
	private Integer accountSequence;
	
	@ManyToOne
	@JoinColumn(name="HCMPYMTINDX")
	private ManualChecksHistoryYearHeader manualChecksHistoryYearHeader;
	
	@ManyToOne
	@JoinColumn(name="DEPINDX")
	private Department department;
	
	@ManyToOne
	@JoinColumn(name="POTINDX")
	private Position position;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Short getPostingTypes() {
		return postingTypes;
	}

	public void setPostingTypes(Short postingTypes) {
		this.postingTypes = postingTypes;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Integer getAccountSequence() {
		return accountSequence;
	}

	public void setAccountSequence(Integer accountSequence) {
		this.accountSequence = accountSequence;
	}

	public ManualChecksHistoryYearHeader getManualChecksHistoryYearHeader() {
		return manualChecksHistoryYearHeader;
	}

	public void setManualChecksHistoryYearHeader(ManualChecksHistoryYearHeader manualChecksHistoryYearHeader) {
		this.manualChecksHistoryYearHeader = manualChecksHistoryYearHeader;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
}
