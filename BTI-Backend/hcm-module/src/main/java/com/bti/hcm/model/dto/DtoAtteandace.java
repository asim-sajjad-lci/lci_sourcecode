package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.Attendance;

public class DtoAtteandace extends DtoBase{
	
	private Integer id;
	private short accrueType;
	private boolean attendance;
	private Integer currentyear;
	private Date latDayAcuurate;
	private Integer numberOfDayInWeek;
	private Integer numberOfHourInDay;
	private Integer nextTranscationNum;
	private List<DtoAtteandace> delete;
	private String arabicDepartmentDescription;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public short getAccrueType() {
		return accrueType;
	}
	public void setAccrueType(short accrueType) {
		this.accrueType = accrueType;
	}
	public boolean isAttendance() {
		return attendance;
	}
	public void setAttendance(boolean attendance) {
		this.attendance = attendance;
	}
	public Integer getCurrentyear() {
		return currentyear;
	}
	public void setCurrentyear(Integer currentyear) {
		this.currentyear = currentyear;
	}
	public Date getLatDayAcuurate() {
		return latDayAcuurate;
	}
	public void setLatDayAcuurate(Date latDayAcuurate) {
		this.latDayAcuurate = latDayAcuurate;
	}
	public Integer getNumberOfDayInWeek() {
		return numberOfDayInWeek;
	}
	public void setNumberOfDayInWeek(Integer numberOfDayInWeek) {
		this.numberOfDayInWeek = numberOfDayInWeek;
	}
	public Integer getNumberOfHourInDay() {
		return numberOfHourInDay;
	}
	public void setNumberOfHourInDay(Integer numberOfHourInDay) {
		this.numberOfHourInDay = numberOfHourInDay;
	}
	public Integer getNextTranscationNum() {
		return nextTranscationNum;
	}
	public void setNextTranscationNum(Integer nextTranscationNum) {
		this.nextTranscationNum = nextTranscationNum;
	}
	
	public List<DtoAtteandace> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoAtteandace> delete) {
		this.delete = delete;
	}
	public String getArabicDepartmentDescription() {
		return arabicDepartmentDescription;
	}
	public void setArabicDepartmentDescription(String arabicDepartmentDescription) {
		this.arabicDepartmentDescription = arabicDepartmentDescription;
	}
	public DtoAtteandace() {
	
	}
	public DtoAtteandace(Attendance attendance) {
		
	}
	
	
}
