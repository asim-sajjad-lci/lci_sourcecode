package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.SalaryMatrixSetup;

@Repository("repositorySalaryMatrixSetup")
public interface RepositorySalaryMatrixSetup extends JpaRepository<SalaryMatrixSetup, Integer>{

	SalaryMatrixSetup findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select count(*) from SalaryMatrixSetup d where d.isDeleted=false")
	Integer getCountOfTotalSalaryMatrix();

	List<SalaryMatrixSetup> findByIsDeleted(boolean b, Pageable pageable);

	List<SalaryMatrixSetup> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select count(*) from SalaryMatrixSetup d where (d.salaryMatrixId like :searchKeyWord  or d.description like :searchKeyWord or d.arabicSalaryMatrixDescription like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveSalaryMatrixSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	
	@Query("select d from SalaryMatrixSetup d where (d.salaryMatrixId like :searchKeyWord  or d.description like :searchKeyWord or d.arabicSalaryMatrixDescription like :searchKeyWord) and d.isDeleted=false")
	List<SalaryMatrixSetup> predictiveSalaryMatrixSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	@Query("select d from SalaryMatrixSetup d where (d.salaryMatrixId like :searchKeyWord  or d.description like :searchKeyWord or d.arabicSalaryMatrixDescription like :searchKeyWord) and d.isDeleted=false")
	List<SalaryMatrixSetup> predictiveSalaryMatrixSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update SalaryMatrixSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleSalaryMatrixSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select p from SalaryMatrixSetup p where (p.salaryMatrixId =:salaryMatrixId) and p.isDeleted=false")
	List<SalaryMatrixSetup> findBySalaryMatrixSetupId(@Param("salaryMatrixId")String salaryMatrixId);

	
	@Query("select s from SalaryMatrixSetup s where (s.salaryMatrixId like :searchKeyWord) and s.isDeleted=false order by s.id desc")
	List<SalaryMatrixSetup> predictivesearchSalaryMatrixIdWithPagination(@Param("searchKeyWord")String salaryMatrixId);

}
