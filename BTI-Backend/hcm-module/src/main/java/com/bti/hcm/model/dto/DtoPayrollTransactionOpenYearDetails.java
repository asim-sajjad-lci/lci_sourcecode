package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.sql.Date;

public class DtoPayrollTransactionOpenYearDetails extends DtoBase{
	
	private Integer id;
	private String auditTransactionNumber;
	private Integer codeSequence;
	private Short codeType;
	private Integer codeIndexId;
	private BigDecimal totalAmount;
	private BigDecimal totalActualAmount;
	private BigDecimal totalPercentcode;
	private Integer totalHour;
	private BigDecimal rateBaseOnPayCode;
	private Integer codeBaseOnCodeIndex;
	private Date codeFromPeriodDate;
	private Date codeToPeriodDate;
	private Date payrollPostDate;
	private String payrollPostByUserId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAuditTransactionNumber() {
		return auditTransactionNumber;
	}
	public void setAuditTransactionNumber(String auditTransactionNumber) {
		this.auditTransactionNumber = auditTransactionNumber;
	}
	public Integer getCodeSequence() {
		return codeSequence;
	}
	public void setCodeSequence(Integer codeSequence) {
		this.codeSequence = codeSequence;
	}
	public Short getCodeType() {
		return codeType;
	}
	public void setCodeType(Short codeType) {
		this.codeType = codeType;
	}
	public Integer getCodeIndexId() {
		return codeIndexId;
	}
	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}
	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}
	public BigDecimal getTotalPercentcode() {
		return totalPercentcode;
	}
	public void setTotalPercentcode(BigDecimal totalPercentcode) {
		this.totalPercentcode = totalPercentcode;
	}
	public Integer getTotalHour() {
		return totalHour;
	}
	public void setTotalHour(Integer totalHour) {
		this.totalHour = totalHour;
	}
	public BigDecimal getRateBaseOnPayCode() {
		return rateBaseOnPayCode;
	}
	public void setRateBaseOnPayCode(BigDecimal rateBaseOnPayCode) {
		this.rateBaseOnPayCode = rateBaseOnPayCode;
	}
	public Integer getCodeBaseOnCodeIndex() {
		return codeBaseOnCodeIndex;
	}
	public void setCodeBaseOnCodeIndex(Integer codeBaseOnCodeIndex) {
		this.codeBaseOnCodeIndex = codeBaseOnCodeIndex;
	}
	public Date getCodeFromPeriodDate() {
		return codeFromPeriodDate;
	}
	public void setCodeFromPeriodDate(Date codeFromPeriodDate) {
		this.codeFromPeriodDate = codeFromPeriodDate;
	}
	public Date getCodeToPeriodDate() {
		return codeToPeriodDate;
	}
	public void setCodeToPeriodDate(Date codeToPeriodDate) {
		this.codeToPeriodDate = codeToPeriodDate;
	}
	public Date getPayrollPostDate() {
		return payrollPostDate;
	}
	public void setPayrollPostDate(Date payrollPostDate) {
		this.payrollPostDate = payrollPostDate;
	}
	public String getPayrollPostByUserId() {
		return payrollPostByUserId;
	}
	public void setPayrollPostByUserId(String payrollPostByUserId) {
		this.payrollPostByUserId = payrollPostByUserId;
	}


}
