package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.EmployeeBenefitMaintenance;

public class DtoEmployeeBenefitMaintenance extends DtoBase {
	private Integer id;
	private Date startDate;
	private Date endDate;
	private Boolean transactionRequired;
	private Short benefitMethod;
	private BigDecimal benefitAmount;
	private BigDecimal benefitPercent;
	private BigDecimal perPeriord;
	private BigDecimal perYear;
	private BigDecimal lifeTime;
	private Short frequency;
	private Boolean inactive;
	private Integer deductionId;
	private Integer employeeId;
	private Integer benefitId;
	private List<DtoPayCode> dtoPayCode;
	private DtoEmployeeMaster employeeMasters;
	private BigDecimal payFactor;
	private DtoEmployeeMasterHcm employeeMaster;
	private DtoBenefitCode benefitCode;

	private List<DtoEmployeeBenefitMaintenance> delete;

	private Integer noOfDays;
	private Integer endDateDays;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getTransactionRequired() {
		return transactionRequired;
	}

	public void setTransactionRequired(Boolean transactionRequired) {
		this.transactionRequired = transactionRequired;
	}

	public Short getBenefitMethod() {
		return benefitMethod;
	}

	public void setBenefitMethod(Short benefitMethod) {
		this.benefitMethod = benefitMethod;
	}

	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public BigDecimal getBenefitPercent() {
		return benefitPercent;
	}

	public void setBenefitPercent(BigDecimal benefitPercent) {
		this.benefitPercent = benefitPercent;
	}

	public BigDecimal getPerPeriord() {
		return perPeriord;
	}

	public void setPerPeriord(BigDecimal perPeriord) {
		this.perPeriord = perPeriord;
	}

	public BigDecimal getPerYear() {
		return perYear;
	}

	public void setPerYear(BigDecimal perYear) {
		this.perYear = perYear;
	}

	public BigDecimal getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(BigDecimal lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Short getFrequency() {
		return frequency;
	}

	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}

	public Boolean getInactive() {
		return inactive;
	}

	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}

	public List<DtoEmployeeBenefitMaintenance> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeeBenefitMaintenance> delete) {
		this.delete = delete;
	}

	public DtoEmployeeBenefitMaintenance() {

	}

	public DtoEmployeeBenefitMaintenance(EmployeeBenefitMaintenance employeeBenefitMaintenance) {

	}

	public DtoBenefitCode getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(DtoBenefitCode benefitCode) {
		this.benefitCode = benefitCode;
	}

	public DtoEmployeeMasterHcm getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMasterHcm employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getEndDateDays() {
		return endDateDays;
	}

	public void setEndDateDays(Integer endDateDays) {
		this.endDateDays = endDateDays;
	}

	public Integer getDeductionId() {
		return deductionId;
	}

	public void setDeductionId(Integer deductionId) {
		this.deductionId = deductionId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getBenefitId() {
		return benefitId;
	}

	public void setBenefitId(Integer benefitId) {
		this.benefitId = benefitId;
	}

	public List<DtoPayCode> getDtoPayCode() {
		return dtoPayCode;
	}

	public void setDtoPayCode(List<DtoPayCode> dtoPayCode) {
		this.dtoPayCode = dtoPayCode;
	}

	public DtoEmployeeMaster getEmployeeMasters() {
		return employeeMasters;
	}

	public void setEmployeeMasters(DtoEmployeeMaster employeeMasters) {
		this.employeeMasters = employeeMasters;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

}
