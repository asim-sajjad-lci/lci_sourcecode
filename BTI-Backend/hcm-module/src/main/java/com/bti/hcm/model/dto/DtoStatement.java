package com.bti.hcm.model.dto;

import java.math.BigDecimal;

public class DtoStatement extends DtoBase {

	private short postingTypes;
	private BigDecimal debitAmount;
	private BigDecimal creditAmount;
	private Integer sequence;

	private String employeeId;
	private Integer employeePrimaryId;
	private String employeeFirstName;

	private Integer positionPrimaryId;
	private String positionId;
	private String descriptions;

	private Integer locationPrimaryId;
	private String locationId;
	private String descriptionss;

	private Integer departmentPrimaryId;
	private String departmentId;
	private String departmentDescription;

	private Integer bankAcountPrimaryId;
	private String bankAcountnumber;
	private String bankName;

	private BigDecimal totalDeduction;
	private BigDecimal totalBenefit;

	private Integer auditTransactionNo;
	private BigDecimal totalAmont;
	private BigDecimal totalNetSalary;
	private BigDecimal totalEarning;
	private BigDecimal otherEarning;
	private BigDecimal housing;
	private BigDecimal transportation;
	private BigDecimal basicSalary;
	private BigDecimal employeeBasicWage;
	private BigDecimal fees;
	private BigDecimal legal;
	private BigDecimal amount;
	private BigDecimal paymentMethod;
	public short getPostingTypes() {
		return postingTypes;
	}
	public void setPostingTypes(short postingTypes) {
		this.postingTypes = postingTypes;
	}
	public BigDecimal getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}
	public BigDecimal getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getEmployeePrimaryId() {
		return employeePrimaryId;
	}
	public void setEmployeePrimaryId(Integer employeePrimaryId) {
		this.employeePrimaryId = employeePrimaryId;
	}
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}
	public Integer getPositionPrimaryId() {
		return positionPrimaryId;
	}
	public void setPositionPrimaryId(Integer positionPrimaryId) {
		this.positionPrimaryId = positionPrimaryId;
	}
	public String getPositionId() {
		return positionId;
	}
	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}
	public String getDescriptions() {
		return descriptions;
	}
	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}
	public Integer getLocationPrimaryId() {
		return locationPrimaryId;
	}
	public void setLocationPrimaryId(Integer locationPrimaryId) {
		this.locationPrimaryId = locationPrimaryId;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getDescriptionss() {
		return descriptionss;
	}
	public void setDescriptionss(String descriptionss) {
		this.descriptionss = descriptionss;
	}
	public Integer getDepartmentPrimaryId() {
		return departmentPrimaryId;
	}
	public void setDepartmentPrimaryId(Integer departmentPrimaryId) {
		this.departmentPrimaryId = departmentPrimaryId;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentDescription() {
		return departmentDescription;
	}
	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}
	public Integer getBankAcountPrimaryId() {
		return bankAcountPrimaryId;
	}
	public void setBankAcountPrimaryId(Integer bankAcountPrimaryId) {
		this.bankAcountPrimaryId = bankAcountPrimaryId;
	}
	public String getBankAcountnumber() {
		return bankAcountnumber;
	}
	public void setBankAcountnumber(String bankAcountnumber) {
		this.bankAcountnumber = bankAcountnumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public BigDecimal getTotalDeduction() {
		return totalDeduction;
	}
	public void setTotalDeduction(BigDecimal totalDeduction) {
		this.totalDeduction = totalDeduction;
	}
	public BigDecimal getTotalBenefit() {
		return totalBenefit;
	}
	public void setTotalBenefit(BigDecimal totalBenefit) {
		this.totalBenefit = totalBenefit;
	}
	public Integer getAuditTransactionNo() {
		return auditTransactionNo;
	}
	public void setAuditTransactionNo(Integer auditTransactionNo) {
		this.auditTransactionNo = auditTransactionNo;
	}
	public BigDecimal getTotalAmont() {
		return totalAmont;
	}
	public void setTotalAmont(BigDecimal totalAmont) {
		this.totalAmont = totalAmont;
	}
	public BigDecimal getTotalNetSalary() {
		return totalNetSalary;
	}
	public void setTotalNetSalary(BigDecimal totalNetSalary) {
		this.totalNetSalary = totalNetSalary;
	}
	public BigDecimal getTotalEarning() {
		return totalEarning;
	}
	public void setTotalEarning(BigDecimal totalEarning) {
		this.totalEarning = totalEarning;
	}
	public BigDecimal getOtherEarning() {
		return otherEarning;
	}
	public void setOtherEarning(BigDecimal otherEarning) {
		this.otherEarning = otherEarning;
	}
	public BigDecimal getHousing() {
		return housing;
	}
	public void setHousing(BigDecimal housing) {
		this.housing = housing;
	}
	public BigDecimal getTransportation() {
		return transportation;
	}
	public void setTransportation(BigDecimal transportation) {
		this.transportation = transportation;
	}
	public BigDecimal getBasicSalary() {
		return basicSalary;
	}
	public void setBasicSalary(BigDecimal basicSalary) {
		this.basicSalary = basicSalary;
	}
	public BigDecimal getEmployeeBasicWage() {
		return employeeBasicWage;
	}
	public void setEmployeeBasicWage(BigDecimal employeeBasicWage) {
		this.employeeBasicWage = employeeBasicWage;
	}
	public BigDecimal getFees() {
		return fees;
	}
	public void setFees(BigDecimal fees) {
		this.fees = fees;
	}
	public BigDecimal getLegal() {
		return legal;
	}
	public void setLegal(BigDecimal legal) {
		this.legal = legal;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(BigDecimal paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

}
