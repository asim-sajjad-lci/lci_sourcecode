package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.AccountsTableAccumulation;
import com.bti.hcm.model.Department;
import com.bti.hcm.model.ManualChecks;
import com.bti.hcm.model.ManualChecksDistribution;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.dto.DtoAccountsTableAccumulation;
import com.bti.hcm.model.dto.DtoManualChecks;
import com.bti.hcm.model.dto.DtoManualChecksDistribution;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAccountsTableAccumulation;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryManualChecks;
import com.bti.hcm.repository.RepositoryManualChecksDistribution;
import com.bti.hcm.repository.RepositoryPosition;

@Service("serviceManualChecksDistribution")
public class ServiceManualChecksDistribution {

	static Logger log = Logger.getLogger(ServiceLocation.class.getName());
	
	@Autowired
	RepositoryManualChecksDistribution repositoryManualChecksDistribution;
	
	@Autowired
	RepositoryManualChecks repositoryManualChecks;
	
	@Autowired
	RepositoryDepartment repositoryDepartment; 
	
	@Autowired
	RepositoryPosition  repositoryPosition;
	
	@Autowired
	RepositoryAccountsTableAccumulation repositoryAccountsTableAccumulation;
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	public DtoManualChecksDistribution saveOrUpdate(DtoManualChecksDistribution dtoManualChecksDistribution) {
            try {
            	int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
            	ManualChecksDistribution manualChecksDistribution=null;
            	if(dtoManualChecksDistribution.getId()!=null && dtoManualChecksDistribution.getId()>0) {
            		manualChecksDistribution=repositoryManualChecksDistribution.findByIdAndIsDeleted(dtoManualChecksDistribution.getId(),false);
            		manualChecksDistribution.setUpdatedBy(loggedInUserId);
            		manualChecksDistribution.setUpdatedDate(new Date());
            	}else {
            		manualChecksDistribution=new ManualChecksDistribution();
            		manualChecksDistribution.setCreatedDate(new Date());
            		Integer rowId = repositoryManualChecksDistribution.getCountOfTotaLocation();
    				Integer increment=0;
    				if(rowId!=0) {
    					increment= rowId+1;
    				}else {
    					increment=1;
    				}
    				manualChecksDistribution.setRowId(increment);
            		
            	}
            	Department department=null;
            	if(dtoManualChecksDistribution.getDtoDepartment()!=null &&dtoManualChecksDistribution.getDtoDepartment().getId()>0) {
            		department=repositoryDepartment.findOne(dtoManualChecksDistribution.getDtoDepartment().getId());
            	}
            	
            	Position position=null;
            	if(dtoManualChecksDistribution.getDtoPosition()!=null&& dtoManualChecksDistribution.getDtoPosition().getId()>0) {
            		position=repositoryPosition.findOne(dtoManualChecksDistribution.getDtoPosition().getId());
            	}
            	AccountsTableAccumulation accountsTableAccumulation=null;
            	if(dtoManualChecksDistribution.getDtoAccountsTableAccumulation()!=null && dtoManualChecksDistribution.getDtoAccountsTableAccumulation().getId()>0) {
            		accountsTableAccumulation=repositoryAccountsTableAccumulation.findOne(dtoManualChecksDistribution.getDtoAccountsTableAccumulation().getId());
            	}
            	ManualChecks manualChecks=null;
            	if(dtoManualChecksDistribution.getDtoManualChecks()!=null&& dtoManualChecksDistribution.getDtoManualChecks().getId()>0) {
            		manualChecks=repositoryManualChecks.findOne(dtoManualChecksDistribution.getDtoManualChecks().getId());
            	}
            	manualChecksDistribution.setManualChecks(manualChecks);
            	manualChecksDistribution.setPosition(position);
            	manualChecksDistribution.setDepartment(department);
            	manualChecksDistribution.setAccountsTableAccumulation(accountsTableAccumulation);
            	manualChecksDistribution.setAccountNumberSequence(dtoManualChecksDistribution.getAccountNumberSequence());
            	manualChecksDistribution.setCodeId(dtoManualChecksDistribution.getCodeId());
            	manualChecksDistribution.setTransactionType(dtoManualChecksDistribution.getTransactionType());
            	manualChecksDistribution.setDebitAmount(dtoManualChecksDistribution.getDebitAmount());
            	manualChecksDistribution.setCreditAmount(dtoManualChecksDistribution.getCreditAmount());
            	repositoryManualChecksDistribution.saveAndFlush(manualChecksDistribution);
            	
            	
            	
			} catch (Exception e) {
				log.error(e);
			}
		
		return dtoManualChecksDistribution;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAll(DtoSearch dtoSearch) {

		try {
			
			log.info("ManualChecksDistribution Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("codeId")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}
			
				 
				 dtoSearch.setTotalCount(this.repositoryManualChecksDistribution.predictiveManualChecksDistributionSearchTotalCount());
				 List<ManualChecksDistribution> manualChecksDistributionList=null;
				 
				 if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null) {
					 
					 if(dtoSearch.getSortBy().equals("")&& dtoSearch.getSortOn().equals("")) {
						 manualChecksDistributionList=this.repositoryManualChecksDistribution.predictiveManualChecksDistributionSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					 }
					 if(dtoSearch.getSortBy().equals("ASC")) {
						 manualChecksDistributionList=this.repositoryManualChecksDistribution.predictiveManualChecksDistributionSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					 }else if(dtoSearch.getSortBy().equals("DESC")) {
						 manualChecksDistributionList=this.repositoryManualChecksDistribution.predictiveManualChecksDistributionSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					 }
					 
				 }
				 
				 if(manualChecksDistributionList!=null && !manualChecksDistributionList.isEmpty()) {
					 List<DtoManualChecksDistribution> dtoManualChecksDistributions= new ArrayList<>();
					 DtoManualChecksDistribution dtoManualChecksDistribution=null;
					 for (ManualChecksDistribution manualChecksDistribution : manualChecksDistributionList) {
						 dtoManualChecksDistribution=new DtoManualChecksDistribution(manualChecksDistribution);
						 
						 
						 DtoManualChecks dtoManualChecks=new DtoManualChecks();
						 if(manualChecksDistribution.getManualChecks()!=null) {
							 dtoManualChecks.setId(manualChecksDistribution.getManualChecks().getId());
							 dtoManualChecks.setCheckDate(manualChecksDistribution.getManualChecks().getCheckDate());
							 dtoManualChecks.setCheckNumber(manualChecksDistribution.getManualChecks().getCheckNumber());
							 dtoManualChecks.setGrossAmount(manualChecksDistribution.getManualChecks().getGrossAmount());
							 dtoManualChecks.setCheckType(manualChecksDistribution.getManualChecks().getCheckType());
							 dtoManualChecks.setDescription(manualChecksDistribution.getManualChecks().getDescription());
							 dtoManualChecks.setNetAmount(manualChecksDistribution.getManualChecks().getNetAmount());
							 dtoManualChecks.setPostDate(manualChecksDistribution.getManualChecks().getPostDate());
						 }
						 
						 DtoAccountsTableAccumulation dtoAccountsTableAccumulation=new  DtoAccountsTableAccumulation();
						 if(manualChecksDistribution.getAccountsTableAccumulation()!=null) {
							 dtoAccountsTableAccumulation.setId(manualChecksDistribution.getManualChecks().getId());
							 dtoAccountsTableAccumulation.setAccountSegmant(manualChecksDistribution.getAccountsTableAccumulation().getAccountSegmant());
						 }
						
						 dtoManualChecksDistribution.setDtoManualChecks(dtoManualChecks);
						 dtoManualChecksDistribution.setAccountNumberSequence(manualChecksDistribution.getAccountNumberSequence());
						 dtoManualChecksDistribution.setId(manualChecksDistribution.getId());
						 dtoManualChecksDistribution.setCodeId(manualChecksDistribution.getCodeId());
						 dtoManualChecksDistribution.setCreditAmount(manualChecksDistribution.getCreditAmount());
						 dtoManualChecksDistribution.setDebitAmount(manualChecksDistribution.getDebitAmount());
						 dtoManualChecksDistribution.setTransactionType(manualChecksDistribution.getTransactionType());
						 dtoManualChecksDistributions.add(dtoManualChecksDistribution);
					 }
					 dtoSearch.setRecords(dtoManualChecksDistributions);
					}
				 log.debug("Search ManualChecksDistribution Size is:"+dtoSearch.getTotalCount());
				 }
		} catch (Exception e) {
			log.error(e);
		}
		
		
		return dtoSearch;
	}

}
