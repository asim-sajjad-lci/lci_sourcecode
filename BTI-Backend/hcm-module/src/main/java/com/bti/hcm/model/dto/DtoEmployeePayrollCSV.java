package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import com.bti.hcm.util.CommonUtils;


public class DtoEmployeePayrollCSV {
	private String topRow = "";
	ArrayList<String> records = new ArrayList<String>();
	
	public DtoEmployeePayrollCSV(){
		
	}
	
	public int getNumberOfRecords() {
		return records.size();
	}
	
	
	public void setTopRow(Object topRowArray[]) {
		for (int i=1; i < topRowArray.length; i++) {
			topRow += CommonUtils.castToString(topRowArray[i]) + ",";
		}
		
		if (topRow != null && topRow.toString().endsWith(",")) {
			topRow = topRow.substring(0, topRow.lastIndexOf(","));
		}
	
	}
	
	public String getTopRow() {
		return this.topRow;
	}
	
	
	
	public BigDecimal setRecordsAndCalculateTotal(Map<String, DtoEmployeePayrollDetails> map, boolean discardDecimal) {
//		Set<String> keys =  map.keySet();
		Object[] keys = map.keySet().toArray();
		Arrays.sort(keys);
		
		int sequence = 1;
		
		BigDecimal totalPayroll = new BigDecimal(0.0);
		
//		for (String key: keys) {
		for (int i=0; i<keys.length; i++) {
			String key = keys[i].toString();
			String rec = "";
			DtoEmployeePayrollDetails recObj = map.get(key);
			
			if (!recObj.isTotalPayPositive()) {
				continue;
			}
			
			rec = "1";
			rec += ",";
			rec += Integer.toString(sequence++);
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getEmployeeCode());
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getEmployeeName());
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getEmployeeIBAN());
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getOtherBank());
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getSwiftCode());
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getCountry());
			rec += ",";
			rec += (discardDecimal ? recObj.getNetPay().intValue() : recObj.getNetPay());
			totalPayroll = totalPayroll.add(recObj.getNetPay());
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getCurrency());
			rec += ",";
			rec += "1";
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getTransferType());
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getTransferDest());
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += ",";
			rec += CommonUtils.removeNull(recObj.getEmployeeIdNumber());
			rec += ",";
			rec += ",";
			
			rec += (discardDecimal ? recObj.getBasic().intValue() : recObj.getBasic());
			rec += ",";
			rec += (discardDecimal ? recObj.getHousing().intValue() : recObj.getHousing());
			rec += ",";
			rec += (discardDecimal ? recObj.getBenefitsForBankReport().intValue() : recObj.getBenefitsForBankReport());
			rec += ",";
			rec += (discardDecimal ? recObj.getTotalDeductions().intValue() : recObj.getTotalDeductions());
			records.add(rec);
		}
		
		return totalPayroll;
		
	}
	
	public String[] getDetailsArray () {
//		String csvPrint = topRow + "\n";
		
		
		String[] csvPrint = new String[records.size()];
		int i =0;
		for (String rec : records) {
			csvPrint[i++] = rec ;
		}
		
		return csvPrint;
	}
	
	
}
