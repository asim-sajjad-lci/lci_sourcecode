package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Position;
import com.bti.hcm.model.PositionPalnSetup;
import com.bti.hcm.model.dto.DtoPositionPlanSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositoryPositionPlanSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("servicePositionPalnSetup")
public class ServicePositionPalnSetup {

	/**
	 * @Description LOGGER use for put a logger in PositionPalnSetup Service
	 */
	static Logger log = Logger.getLogger(ServicePositionPalnSetup.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in PositionPalnSetup service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PositionPalnSetup service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PositionPalnSetup service
	 */
	@Autowired
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPosition Autowired here using annotation of spring for access of repositoryPosition method in Position service
	 * 				In short Access Position Query from Database using repositoryPosition.
	 */
	@Autowired
	RepositoryPositionPlanSetup repositoryPositionPlanSetup;
	
	@Autowired
	RepositoryPosition repositoryPosition;

	/**
	 * @param dtoPositionPlanSetup
	 * @return
	 */
	public DtoPositionPlanSetup saveOrUpdatePositionPalnSetup(DtoPositionPlanSetup dtoPositionPlanSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		PositionPalnSetup positionSetup = null;
		if (dtoPositionPlanSetup.getId() != null && dtoPositionPlanSetup.getId() > 0) {
			
			positionSetup = repositoryPositionPlanSetup.findByIdAndIsDeleted(dtoPositionPlanSetup.getId(), false);
			positionSetup.setUpdatedBy(loggedInUserId);
			positionSetup.setUpdatedDate(new Date());
		} else {
			positionSetup = new PositionPalnSetup();
			positionSetup.setCreatedDate(new Date());
			
			Integer rowId = repositoryPositionPlanSetup.getCountOfTotaPositionPlanSetup();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			positionSetup.setRowId(increment);
		}
		Position position =  repositoryPosition.findOne(Integer.valueOf(dtoPositionPlanSetup.getPositionId()));
		positionSetup.setPositionPlanStart(dtoPositionPlanSetup.getPositionPlanStart());
		positionSetup.setPositionPlanEnd(dtoPositionPlanSetup.getPositionPlanEnd());
		positionSetup.setPositionPlanId(dtoPositionPlanSetup.getPositionPlanId());
		positionSetup.setPosition(position);
		positionSetup.setInactive(dtoPositionPlanSetup.isInactive());
		positionSetup.setPositionPlanDesc(dtoPositionPlanSetup.getPositionPlanDesc());
		positionSetup.setPositionPlanArbicDesc(dtoPositionPlanSetup.getPositionPlanArbicDesc());
		positionSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryPositionPlanSetup.saveAndFlush(positionSetup);
		return dtoPositionPlanSetup;
	}
	
	public DtoPositionPlanSetup updateStatus(DtoPositionPlanSetup dtoPositionPlanSetup) {
		log.info("saveOrUpdatePositionPalnSetup Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		PositionPalnSetup positionSetup = null;
		if (dtoPositionPlanSetup.getId() != null && dtoPositionPlanSetup.getId() > 0) {
			
			positionSetup = repositoryPositionPlanSetup.findByIdAndIsDeleted(dtoPositionPlanSetup.getId(), false);
			positionSetup.setInactive(positionSetup.isInactive());
			positionSetup.setUpdatedBy(loggedInUserId);
			positionSetup.setUpdatedDate(new Date());
			repositoryPositionPlanSetup.saveAndFlush(positionSetup);
			
		} 
		return dtoPositionPlanSetup;
	}

	/**
	 * @param dtoPositionPlanSetup
	 * @return
	 */
	public DtoSearch getAllPostionPlanSteup(DtoPositionPlanSetup dtoPositionPlanSetup) {
		log.info("getAllPogetAllPostionPlanSteupstionSteup Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoPositionPlanSetup.getPageNumber());
		dtoSearch.setPageSize(dtoPositionPlanSetup.getPageSize());
		dtoSearch.setTotalCount(repositoryPositionPlanSetup.getCountOfTotalPosition());
		List<PositionPalnSetup> skillPositionList = null;
		if (dtoPositionPlanSetup.getPageNumber() != null && dtoPositionPlanSetup.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoPositionPlanSetup.getPageNumber(), dtoPositionPlanSetup.getPageSize(), Direction.DESC, "createdDate");
			skillPositionList = repositoryPositionPlanSetup.findByIsDeleted(false, pageable);
		} else {
			skillPositionList = repositoryPositionPlanSetup.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		
		List<DtoPositionPlanSetup> dtoPOsitionList=new ArrayList<>();
		if(skillPositionList!=null && !skillPositionList.isEmpty())
		{
			for (PositionPalnSetup position : skillPositionList) 
			{
				dtoPositionPlanSetup=new DtoPositionPlanSetup(position);
				dtoPositionPlanSetup.setPositionPlanDesc(position.getPositionPlanDesc());
				dtoPositionPlanSetup.setPositionPlanArbicDesc(position.getPositionPlanArbicDesc());
				dtoPositionPlanSetup.setPositionPlanStart(position.getPositionPlanStart());
				dtoPositionPlanSetup.setPositionPlanEnd(position.getPositionPlanEnd());
				dtoPositionPlanSetup.setPositionPlanId(position.getPositionPlanId());
				dtoPositionPlanSetup.setId(position.getId());
				dtoPOsitionList.add(dtoPositionPlanSetup);
			
			}
			dtoSearch.setRecords(dtoPOsitionList);
		}
		log.debug("All Postion plan Steup List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	
	public DtoSearch findByAllPosionPlanPositionId(DtoPositionPlanSetup dtoPositionPlanSetup) {
		log.info("getAllPogetAllPostionPlanSteupstionSteup Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoPositionPlanSetup.getPageNumber());
		dtoSearch.setPageSize(dtoPositionPlanSetup.getPageSize());
		
		List<PositionPalnSetup> skillPositionList = null;
		if (dtoPositionPlanSetup.getPageNumber() != null && dtoPositionPlanSetup.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoPositionPlanSetup.getPageNumber(), dtoPositionPlanSetup.getPageSize(), Direction.DESC, "createdDate");
			skillPositionList = repositoryPositionPlanSetup.findByAllPosionPlanPositionId(Integer.valueOf(dtoPositionPlanSetup.getPositionId()),pageable);
		} else {
			skillPositionList = repositoryPositionPlanSetup.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		dtoSearch.setTotalCount(skillPositionList.size());
		List<DtoPositionPlanSetup> dtoPOsitionList=new ArrayList<>();
		if(!skillPositionList.isEmpty())
		{
			for (PositionPalnSetup position : skillPositionList) 
			{
				dtoPositionPlanSetup=new DtoPositionPlanSetup(position);
				dtoPositionPlanSetup.setPositionPlanDesc(position.getPositionPlanDesc());
				dtoPositionPlanSetup.setPositionPlanArbicDesc(position.getPositionPlanArbicDesc());
				dtoPositionPlanSetup.setPositionPlanStart(position.getPositionPlanStart());
				dtoPositionPlanSetup.setPositionPlanId(position.getPositionPlanId());
				dtoPositionPlanSetup.setPositionPlanEnd(position.getPositionPlanEnd());
				dtoPositionPlanSetup.setInactive(position.isInactive());
				dtoPositionPlanSetup.setId(position.getId());
				dtoPOsitionList.add(dtoPositionPlanSetup);
			}
			dtoSearch.setRecords(dtoPOsitionList);
		}
		log.debug("All Postion plan Steup List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	

	/**
	 * @param ids
	 * @return
	 */
	public DtoPositionPlanSetup deletePositionPlanSteup(List<Integer> ids) {
		log.info("deletePositionPlanSteup Method");
		DtoPositionPlanSetup dtoPosition = new DtoPositionPlanSetup();
		dtoPosition
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("POSITION_PLAN_DELETED", false));
		dtoPosition.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("POSTION_ASSOCIATED", false));
		List<DtoPositionPlanSetup> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("POSITION_PLAN_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer positionId : ids) {
				
				PositionPalnSetup position = repositoryPositionPlanSetup.findOne(positionId);
				if(position.getPositionPayCode().isEmpty()){
					DtoPositionPlanSetup dtoskillSetup2 = new DtoPositionPlanSetup();
					dtoskillSetup2.setId(positionId);
					dtoskillSetup2.setPositionPlanDesc(position.getPositionPlanDesc());
					dtoskillSetup2.setPositionPlanArbicDesc(position.getPositionPlanArbicDesc());
					dtoskillSetup2.setPositionPlanId(position.getPositionPlanId());
					repositoryPositionPlanSetup.deleteSinglePosition(true, loggedInUserId, positionId);
					deletePosition.add(dtoskillSetup2);
				}else{
					inValidDelete = true;
					invlidDeleteMessage.append(position.getPosition().getPositionId()+","); 
				}

			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoPosition.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoPosition.setMessageType("");
				
			}
			
			dtoPosition.setDeletePositionSetup(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete position plan Setup :"+dtoPosition.getId());
		return dtoPosition;
	}

	/**
	 * @param id
	 * @return
	 */
	public DtoPositionPlanSetup getByPositionPlanSetupId(Integer id) {
		log.info("getByPositionPlanSetupId Method");
		DtoPositionPlanSetup dtoPosition = new DtoPositionPlanSetup();
		if (id > 0) {
			PositionPalnSetup position = repositoryPositionPlanSetup.findByIdAndIsDeleted(id, false);
			if (position != null) {
				dtoPosition = new DtoPositionPlanSetup(position);
				
				dtoPosition.setId(position.getId());
				dtoPosition.setPositionPlanEnd(position.getPositionPlanEnd());
				dtoPosition.setPositionPlanStart(position.getPositionPlanStart());
				dtoPosition.setPositionPlanId(position.getPositionPlanId());
				if(position.getPosition()!=null) {
					dtoPosition.setPositionDescription(position.getPosition().getDescription());
					dtoPosition.setPositionId(position.getPosition().getPositionId());
				}
				dtoPosition.setPositionPlanArbicDesc(position.getPositionPlanArbicDesc());
				dtoPosition.setPositionPlanDesc(position.getPositionPlanDesc());
			} else {
				dtoPosition.setMessageType("POSITION_PLAN_LIST_NOT_GETTING");

			}
		} else {
			dtoPosition.setMessageType("POSITION_PLAN_LIST_NOT_GETTING");

		}
		log.debug("PositionPlanSetup By Id is:"+dtoPosition.getId());
		return dtoPosition;
	}

	/**
	 * @param positionPlanId
	 * @return
	 */
	public DtoPositionPlanSetup repeatByPositionPlanId(String positionPlanId) {
		log.info("repeatByPositionPlanId Method");
		DtoPositionPlanSetup dtoPositioneup = new DtoPositionPlanSetup();
		try {
			List<PositionPalnSetup> skillsteup=repositoryPositionPlanSetup.searchBypositionPlanId(positionPlanId);
			if(skillsteup!=null && !(skillsteup.isEmpty())) {
				dtoPositioneup.setIsRepeat(true);
			}else {
				dtoPositioneup.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPositioneup;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchPositionPlan(DtoSearch dtoSearch) {
		log.info("searchPositionPlan Method");
			if(dtoSearch != null){
	            String searchWord=dtoSearch.getSearchKeyword();
	            Integer positionId = dtoSearch.getId();
	            String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("positionPlanId") || dtoSearch.getSortOn().equals("positionPlanDesc") || dtoSearch.getSortOn().equals("positionPlanArbicDesc") || dtoSearch.getSortOn().equals("positionPlanStart")
								|| dtoSearch.getSortOn().equals("positionPlanEnd")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
	            
				 dtoSearch.setTotalCount(this.repositoryPositionPlanSetup.predictivePositionPlanSearchTotalCount1("%"+searchWord+"%",positionId));
					List<PositionPalnSetup> positionPalnSetupList =null;
					if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
						
						if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
							positionPalnSetupList = this.repositoryPositionPlanSetup.predictivePositionPlanSearchWithPagination("%"+searchWord+"%",positionId,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
						}
						if(dtoSearch.getSortBy().equals("ASC")){
							positionPalnSetupList = this.repositoryPositionPlanSetup.predictivePositionPlanSearchWithPagination("%"+searchWord+"%",positionId,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
						}else if(dtoSearch.getSortBy().equals("DESC")){
							positionPalnSetupList = this.repositoryPositionPlanSetup.predictivePositionPlanSearchWithPagination("%"+searchWord+"%",positionId,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
						}
						
					}
			 if(positionPalnSetupList != null && !positionPalnSetupList.isEmpty()){
	                List<DtoPositionPlanSetup> dtoPositionPlanSetupList = new ArrayList<>();
	                DtoPositionPlanSetup dtoPositionPlanSetup=null;
	                for (PositionPalnSetup positionPalnSetup : positionPalnSetupList) {
	                	
	                  dtoPositionPlanSetup = new DtoPositionPlanSetup(positionPalnSetup);
	                  dtoPositionPlanSetup.setId(positionPalnSetup.getId());
	                  dtoPositionPlanSetup.setInactive(positionPalnSetup.isInactive());
	                  dtoPositionPlanSetup.setPositionPlanEnd(positionPalnSetup.getPositionPlanEnd());
	                  dtoPositionPlanSetup.setPositionPlanStart(positionPalnSetup.getPositionPlanStart());
	                  dtoPositionPlanSetup.setPositionPlanId(positionPalnSetup.getPositionPlanId());
	                  dtoPositionPlanSetup.setPositionDescription(positionPalnSetup.getPosition().getDescription());
	                  dtoPositionPlanSetup.setPositionId(positionPalnSetup.getPosition().getPositionId());
	                  dtoPositionPlanSetup.setPositionPlanArbicDesc(positionPalnSetup.getPositionPlanArbicDesc());
	                    dtoPositionPlanSetupList.add(dtoPositionPlanSetup);
	                }
	                dtoSearch.setRecords(dtoPositionPlanSetupList);
	            }
		}
		log.debug("Search positionPalnSetup Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	public DtoSearch searchPositionPlan1(DtoSearch dtoSearch) {
        log.info("searchPositionPlan Method");
        if(dtoSearch != null){
            String searchWord=dtoSearch.getSearchKeyword();
            String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("positionPlanId") || dtoSearch.getSortOn().equals("positionPlanDesc") || dtoSearch.getSortOn().equals("positionPlanArbicDesc") || dtoSearch.getSortOn().equals("positionPlanStart")
							|| dtoSearch.getSortOn().equals("positionPlanEnd")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
            
				List<PositionPalnSetup> positionPalnSetupList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						positionPalnSetupList = this.repositoryPositionPlanSetup.predictivePositionPlanSearchWithPagination1("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						positionPalnSetupList =this.repositoryPositionPlanSetup.predictivePositionPlanSearchWithPagination1("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						positionPalnSetupList = this.repositoryPositionPlanSetup.predictivePositionPlanSearchWithPagination1("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
		

            if(positionPalnSetupList != null && !positionPalnSetupList.isEmpty()){
                List<DtoPositionPlanSetup> dtoPositionPlanSetupList = new ArrayList<>();
                DtoPositionPlanSetup dtoPositionPlanSetup=null;
                for (PositionPalnSetup positionPalnSetup : positionPalnSetupList) {
                	
                  dtoPositionPlanSetup = new DtoPositionPlanSetup(positionPalnSetup);
                  dtoPositionPlanSetup.setId(positionPalnSetup.getId());
                  dtoPositionPlanSetup.setPositionPlanEnd(positionPalnSetup.getPositionPlanEnd());
                  dtoPositionPlanSetup.setPositionPlanStart(positionPalnSetup.getPositionPlanStart());
                  dtoPositionPlanSetup.setPositionPlanId(positionPalnSetup.getPositionPlanId());
                  dtoPositionPlanSetup.setPositionDescription(positionPalnSetup.getPosition().getDescription());
                  dtoPositionPlanSetup.setPositionId(positionPalnSetup.getPosition().getPositionId());
                  dtoPositionPlanSetup.setPositionPlanArbicDesc(positionPalnSetup.getPositionPlanArbicDesc());
                    dtoPositionPlanSetupList.add(dtoPositionPlanSetup);
                }
                dtoSearch.setRecords(dtoPositionPlanSetupList);
            }
        }
        log.debug("Search PositionPalnSetup Size is:"+dtoSearch.getTotalCount());
        return dtoSearch;
    } 
	
	
	public DtoPositionPlanSetup changeStatus(DtoPositionPlanSetup dtoPositionPlanSetup) {
        log.info("saveOrUpdatePositionPalnSetup Method");
        int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
        PositionPalnSetup positionSetup = null;
        if (dtoPositionPlanSetup.getId() != null && dtoPositionPlanSetup.getId() > 0) {

            positionSetup = repositoryPositionPlanSetup.changeStatus(dtoPositionPlanSetup.getIsActive(),dtoPositionPlanSetup.getId());
            positionSetup.setUpdatedBy(loggedInUserId);
            positionSetup.setUpdatedDate(new Date());
        }
        return dtoPositionPlanSetup;
    }

	public DtoSearch searchPositionPlanId(DtoSearch dtoSearch) {
		log.info("searchPositionPlanId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			
			List<DtoPositionPlanSetup> dtopositionplanIdList =new ArrayList<>();
			
			List<PositionPalnSetup> positionplanIdList = this.repositoryPositionPlanSetup.predictivePositionPlanIdSearchWithPagination("%"+searchWord+"%");
				if(!positionplanIdList.isEmpty()) {
					
					for (PositionPalnSetup positionPalnSetup : positionplanIdList) {
						DtoPositionPlanSetup dtoPositionPlanSetup = new DtoPositionPlanSetup();
						dtoPositionPlanSetup.setId(positionPalnSetup.getId());
						dtoPositionPlanSetup.setPositionPlanId(positionPalnSetup.getPositionPlanId());
						dtoPositionPlanSetup.setPositionId(positionPalnSetup.getPositionPlanId());
						dtoPositionPlanSetup.setPositionPlanDesc(positionPalnSetup.getPositionPlanDesc());
						dtoPositionPlanSetup.setPositionPlanArbicDesc(positionPalnSetup.getPositionPlanArbicDesc());
						dtopositionplanIdList.add(dtoPositionPlanSetup);
					}
					
					dtoSearch.setTotalCount(dtopositionplanIdList.size());
					dtoSearch.setRecords(dtopositionplanIdList);
				}
			
		}
		
		return dtoSearch;
	} 
	
	public List<DtoPositionPlanSetup> getAllPositionPlanSetupDropDownList() {
		log.info("getAllPositionPlanSetupDropDownList  Method");
		List<DtoPositionPlanSetup> dtoPositionPlanSetupList = new ArrayList<>();
		try {
			List<PositionPalnSetup> list = repositoryPositionPlanSetup.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (PositionPalnSetup positionPalnSetup : list) {
					DtoPositionPlanSetup dtoPositionPlanSetup = new DtoPositionPlanSetup();
					dtoPositionPlanSetup.setId(positionPalnSetup.getId());
					dtoPositionPlanSetup.setPositionPlanId(positionPalnSetup.getPositionPlanId());
					dtoPositionPlanSetup.setPositionDescription(positionPalnSetup.getPositionPlanDesc());
					dtoPositionPlanSetup.setPositionPlanArbicDesc(positionPalnSetup.getPositionPlanArbicDesc());
					dtoPositionPlanSetup.setPositionPlanStart(positionPalnSetup.getPositionPlanStart());
					dtoPositionPlanSetup.setPositionPlanEnd(positionPalnSetup.getPositionPlanEnd());
					dtoPositionPlanSetup.setInactive(positionPalnSetup.isInactive());
					dtoPositionPlanSetup.setPositionId(positionPalnSetup.getPosition().getPositionId());
					dtoPositionPlanSetup.setPositionDescription(positionPalnSetup.getPosition().getDescription());
					dtoPositionPlanSetupList.add(dtoPositionPlanSetup);
				}
			}
			log.debug("PositonPlanSetup is:"+dtoPositionPlanSetupList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPositionPlanSetupList;
	}
	
	public List<DtoPositionPlanSetup> getAllPositionPlanSetupInActiveList() {
		log.info("getAllPositionPlanSetupInActiveList  Method");
		List<DtoPositionPlanSetup> dtoPositionPlanSetupList = new ArrayList<>();
		try {
			List<PositionPalnSetup> list = repositoryPositionPlanSetup.findByIsDeletedAndInactive(false, false);
			if (list != null && !list.isEmpty()) {
				for (PositionPalnSetup positionPalnSetup : list) {
					DtoPositionPlanSetup dtoPositionPlanSetup = new DtoPositionPlanSetup();
					dtoPositionPlanSetup.setId(positionPalnSetup.getId());
					dtoPositionPlanSetup.setPositionPlanId(positionPalnSetup.getPositionPlanId());
					dtoPositionPlanSetup.setPositionDescription(positionPalnSetup.getPositionPlanDesc());
					dtoPositionPlanSetup.setPositionPlanArbicDesc(positionPalnSetup.getPositionPlanArbicDesc());
					dtoPositionPlanSetup.setPositionPlanStart(positionPalnSetup.getPositionPlanStart());
					dtoPositionPlanSetup.setPositionPlanEnd(positionPalnSetup.getPositionPlanEnd());
					dtoPositionPlanSetup.setInactive(positionPalnSetup.isInactive());
					dtoPositionPlanSetup.setPositionId(positionPalnSetup.getPosition().getPositionId());
					dtoPositionPlanSetup.setPositionDescription(positionPalnSetup.getPosition().getDescription());
					dtoPositionPlanSetupList.add(dtoPositionPlanSetup);
				}
			}
			log.debug("PositonPlanSetup is:"+dtoPositionPlanSetupList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPositionPlanSetupList;
	}
	
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public DtoPositionPlanSetup getPostionFromPostionPlanSetupId(Integer id) {
        log.info("getPostionFromPostionPlanSetupId Method");
        PositionPalnSetup positionSetup = null;
        DtoPositionPlanSetup dtoPositionPlanSetup = new DtoPositionPlanSetup();
        if (id != null && id > 0) {

            positionSetup = repositoryPositionPlanSetup.predictivePositionPlanByPosition(id);
            dtoPositionPlanSetup.setPositionPlanDesc(positionSetup.getPositionPlanDesc());
            dtoPositionPlanSetup.setPositionId(positionSetup.getPosition().getPositionId());
            dtoPositionPlanSetup.setPositionDescription(positionSetup.getPosition().getDescription());
            dtoPositionPlanSetup.setId(positionSetup.getPosition().getId());
        }
        return dtoPositionPlanSetup;
    }

}
