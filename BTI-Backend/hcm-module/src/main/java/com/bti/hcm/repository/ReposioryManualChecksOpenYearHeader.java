package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ManualChecksOpenYearHeader;

@Repository("reposioryManualChecksOpenYearHeader")
public interface ReposioryManualChecksOpenYearHeader extends JpaRepository<ManualChecksOpenYearHeader, Integer> {

	@Query("SELECT pto FROM ManualChecksOpenYearHeader pto WHERE YEAR(pto.createdDate) =:year and pto.isDeleted = false")
	List<ManualChecksOpenYearHeader> findByYear(@Param("year") Integer year);
}
