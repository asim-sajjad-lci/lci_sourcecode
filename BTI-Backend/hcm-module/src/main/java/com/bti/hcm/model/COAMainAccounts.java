package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "GL00101",indexes = {
        @Index(columnList = "ACTINDX")
})
public class COAMainAccounts extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACTINDX")
	private Integer id;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "coaMainAccounts")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<AccountsTableAccumulation> listAccountsTableAccumulation;

	@Column(name = "ACTNUMID", columnDefinition = "char(21)")
	private String mainAccNumber;

	@Column(name = "ACTDESCR", columnDefinition = "char(31)")
	private String mainAccDescription;

	@Column(name = "ACTDESCRA", columnDefinition = "char(61)")
	private String mainAccArabicDescription;

	@Column(name = "ACCTTYPE")
	private Integer accountType;

	@Column(name = "ACCATNUM")
	private Integer accountCategoryIndex;

	@Column(name = "ACTIVE")
	private Boolean active;

	@Column(name = "TPCLBLNC")
	private Short transactionAction;

	@Column(name = "ACTALIAS", columnDefinition = "char(21)")
	private String aliasAccount;

	@Column(name = "ACTALIASA", columnDefinition = "char(41)")
	private String aliasAccountArabic;

	@Column(name = "USERDEF1", columnDefinition = "char(21)")
	private String userDefine1;

	@Column(name = "USERDEF2", columnDefinition = "char(21)")
	private String userDefine2;

	@Column(name = "USERDEF3", columnDefinition = "char(21)")
	private String userDefine3;

	@Column(name = "USERDEF4", columnDefinition = "char(21)")
	private String userDefine4;

	@Column(name = "USERDEF5", columnDefinition = "char(21)")
	private String userDefine5;

	@Column(name = "ACCTENTR")
	private Boolean allowAccountTxEntry;

	@Column(name = "UNACCOUNT")
	private Boolean unitAccount;

	@Column(name = "FXALLOCACC")
	private Boolean fixedAllocationAccount;

	@Column(name = "IS_USED")
	private Integer isUsed;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMainAccNumber() {
		return mainAccNumber;
	}

	public void setMainAccNumber(String mainAccNumber) {
		this.mainAccNumber = mainAccNumber;
	}

	public String getMainAccDescription() {
		return mainAccDescription;
	}

	public void setMainAccDescription(String mainAccDescription) {
		this.mainAccDescription = mainAccDescription;
	}

	public String getMainAccArabicDescription() {
		return mainAccArabicDescription;
	}

	public void setMainAccArabicDescription(String mainAccArabicDescription) {
		this.mainAccArabicDescription = mainAccArabicDescription;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public Integer getAccountCategoryIndex() {
		return accountCategoryIndex;
	}

	public void setAccountCategoryIndex(Integer accountCategoryIndex) {
		this.accountCategoryIndex = accountCategoryIndex;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Short getTransactionAction() {
		return transactionAction;
	}

	public void setTransactionAction(Short transactionAction) {
		this.transactionAction = transactionAction;
	}

	public String getAliasAccount() {
		return aliasAccount;
	}

	public void setAliasAccount(String aliasAccount) {
		this.aliasAccount = aliasAccount;
	}

	public String getAliasAccountArabic() {
		return aliasAccountArabic;
	}

	public void setAliasAccountArabic(String aliasAccountArabic) {
		this.aliasAccountArabic = aliasAccountArabic;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public String getUserDefine4() {
		return userDefine4;
	}

	public void setUserDefine4(String userDefine4) {
		this.userDefine4 = userDefine4;
	}

	public String getUserDefine5() {
		return userDefine5;
	}

	public void setUserDefine5(String userDefine5) {
		this.userDefine5 = userDefine5;
	}

	public Boolean getAllowAccountTxEntry() {
		return allowAccountTxEntry;
	}

	public void setAllowAccountTxEntry(Boolean allowAccountTxEntry) {
		this.allowAccountTxEntry = allowAccountTxEntry;
	}

	public Boolean getUnitAccount() {
		return unitAccount;
	}

	public void setUnitAccount(Boolean unitAccount) {
		this.unitAccount = unitAccount;
	}

	public Boolean getFixedAllocationAccount() {
		return fixedAllocationAccount;
	}

	public void setFixedAllocationAccount(Boolean fixedAllocationAccount) {
		this.fixedAllocationAccount = fixedAllocationAccount;
	}

	public List<AccountsTableAccumulation> getListAccountsTableAccumulation() {
		return listAccountsTableAccumulation;
	}

	public void setListAccountsTableAccumulation(List<AccountsTableAccumulation> listAccountsTableAccumulation) {
		this.listAccountsTableAccumulation = listAccountsTableAccumulation;
	}

	public Integer getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
	}
	
	

}
