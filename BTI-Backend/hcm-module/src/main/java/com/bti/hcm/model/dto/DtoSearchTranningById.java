package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * Description: The DTO class for Search Operations 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoSearchTranningById extends DtoBase{
	private Integer id;
	private String description;
	private Short trainingClassStatus;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Short getTrainingClassStatus() {
		return trainingClassStatus;
	}
	public void setTrainingClassStatus(Short trainingClassStatus) {
		this.trainingClassStatus = trainingClassStatus;
	}

	

}
