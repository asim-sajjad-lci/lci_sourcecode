package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeDependents;
import com.bti.hcm.model.dto.DtoEmployeeDependents;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeDependents;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceEmployeeDependents")
public class ServiceEmployeeDependents {
	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceEmployeeDependents.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeDependents repositoryEmployeeDependents;


	
	

	public DtoEmployeeDependents saveOrUpdate(DtoEmployeeDependents dtoEmployeeDependents) {
		
		try {
			
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			EmployeeDependents employeeDependents=null;
			if (dtoEmployeeDependents.getId() != null && dtoEmployeeDependents.getId() > 0) {
				employeeDependents = repositoryEmployeeDependents.findByIdAndIsDeleted(dtoEmployeeDependents.getId(), false);
				employeeDependents.setUpdatedBy(loggedInUserId);
				employeeDependents.setUpdatedDate(new Date());
			} else {
				employeeDependents = new EmployeeDependents();
				employeeDependents.setCreatedDate(new Date());
				Integer rowId = repositoryEmployeeDependents.getCountOfTotalEmployeeDependents();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				employeeDependents.setRowId(increment);
			}
			employeeDependents.setEmpRelationshipId(dtoEmployeeDependents.getEmpRelationshipId());
	        employeeDependents.setDesc(dtoEmployeeDependents.getDesc());
	        employeeDependents.setArabicDesc(dtoEmployeeDependents.getArabicDesc());
	        employeeDependents.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
	        repositoryEmployeeDependents.saveAndFlush(employeeDependents);
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDependents;
	}


	public DtoEmployeeDependents delete(List<Integer> ids) {
		log.info("delete EmployeeDependents Method");
		DtoEmployeeDependents dtoEmployeeDependents = new DtoEmployeeDependents();
		dtoEmployeeDependents.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_DEPDNDENTS_DELETED", false));
		dtoEmployeeDependents.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_DEPDNDENTS_ASSOCIATED", false));
		List<DtoEmployeeDependents> deleteDtoEmployeeDependents = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPDNDENTS_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer planId : ids) {
				EmployeeDependents employeeDependents = repositoryEmployeeDependents.findOne(planId);
	            if(employeeDependents!=null) {
	            	if(employeeDependents.getListEmployeeDependent().isEmpty()) {
		            	DtoEmployeeDependents dtoEmployeeDependents2=new DtoEmployeeDependents();
		 				dtoEmployeeDependents.setId(employeeDependents.getId());
		 				repositoryEmployeeDependents.deleteSingleEmployeeDependents(true, loggedInUserId, planId);
		 				deleteDtoEmployeeDependents.add(dtoEmployeeDependents2);
		            	 
		             }	
	            }else {
	            	inValidDelete = true;
	            }
				
				
				}

			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoEmployeeDependents.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoEmployeeDependents.setMessageType("");
				
			}
			
				
			dtoEmployeeDependents.setDelete(deleteDtoEmployeeDependents);
		} 
		catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeeDependents"
				+ " :"+dtoEmployeeDependents.getId());
		return dtoEmployeeDependents;
	}

	
	

	
	public DtoEmployeeDependents getById(int id) {
		DtoEmployeeDependents dtoEmployeeDependents  = new DtoEmployeeDependents();
		try {
			
			if (id > 0) {
				EmployeeDependents employeeDependents = repositoryEmployeeDependents.findByIdAndIsDeleted(id, false);
				
					dtoEmployeeDependents = new DtoEmployeeDependents(employeeDependents);
					dtoEmployeeDependents.setEmpRelationshipId(employeeDependents.getEmpRelationshipId());
					dtoEmployeeDependents.setDesc(employeeDependents.getDesc());
					dtoEmployeeDependents.setArabicDesc(employeeDependents.getArabicDesc());			
				 
			} else {
				dtoEmployeeDependents.setMessageType("INVALID_ID");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDependents;
	}
	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchEmployeeDependents(DtoSearch dtoSearch) {

		try {

			log.info("searchEmployeeDependents Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
			  
				dtoSearch=searchByEmployeeDependents(dtoSearch);
				
				dtoSearch.setTotalCount(this.repositoryEmployeeDependents.predictiveEmployeeDependentsSearchTotalCount("%"+searchWord+"%"));
				List<EmployeeDependents> employeeDependentsList =searchByPageSizeAndNumber(dtoSearch);
				if(employeeDependentsList != null && !employeeDependentsList.isEmpty()){
					List<DtoEmployeeDependents> dtoEmployeeDependentsList = new ArrayList<>();
					DtoEmployeeDependents dtoEmployeeDependents=null;
					for (EmployeeDependents employeeDependents : employeeDependentsList) {
						dtoEmployeeDependents = new DtoEmployeeDependents(employeeDependents);
						dtoEmployeeDependents.setEmpRelationshipId(employeeDependents.getEmpRelationshipId());
						dtoEmployeeDependents.setDesc(employeeDependents.getDesc());
						dtoEmployeeDependents.setArabicDesc(employeeDependents.getArabicDesc());
						dtoEmployeeDependentsList.add(dtoEmployeeDependents);
						}
					dtoSearch.setRecords(dtoEmployeeDependentsList);
						}
				
			
			log.debug("Search OrientationSetup Size is:"+dtoSearch.getTotalCount());
			
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}

	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchByEmployeeDependents(DtoSearch dtoSearch) {
		String condition="";
		try {
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				
				if(dtoSearch.getSortOn().equals("empRelationshipId")|| dtoSearch.getSortOn().equals("desc") || dtoSearch.getSortOn().equals("arabicDesc")) {
					condition=dtoSearch.getSortOn();
				}else {
					condition="id";
				}
				
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}	
				dtoSearch.setCondition(condition);
		} catch (Exception e) {
			log.error(e);
		}

		 
		return dtoSearch;
	}
	

	public List<EmployeeDependents> searchByPageSizeAndNumber(DtoSearch dtoSearch) {
		List<EmployeeDependents> employeeDependentsList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
			
			if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
				employeeDependentsList = this.repositoryEmployeeDependents.predictiveEmployeeDependentsSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if(dtoSearch.getSortBy().equals("ASC")){
				employeeDependentsList = this.repositoryEmployeeDependents.predictiveEmployeeDependentsSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
			}else if(dtoSearch.getSortBy().equals("DESC")){
				employeeDependentsList = this.repositoryEmployeeDependents.predictiveEmployeeDependentsSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
			}
			
		}
		
		return employeeDependentsList;
	}
	public DtoSearch getAllDroupDown(DtoSearch dtoSearch) {
		try {

			log.info("getAllDroupDown Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				
				 List<EmployeeDependents> employeeDependentsList = this.repositoryEmployeeDependents.predictiveSearchgetAllDroupDownWithPagination("%"+searchWord+"%");
					if(!employeeDependentsList.isEmpty()) {

						if(!employeeDependentsList.isEmpty()){
							List<DtoEmployeeDependents> dtoEmployeeDependents = new ArrayList<>();
							DtoEmployeeDependents dtoEmployeeDependents1=null;
							for (EmployeeDependents employeeDependents : employeeDependentsList) {
								
								dtoEmployeeDependents1=new DtoEmployeeDependents(employeeDependents);
								dtoEmployeeDependents1.setId(employeeDependents.getId());
								dtoEmployeeDependents1.setDesc(employeeDependents.getDesc());
								dtoEmployeeDependents1.setArabicDesc(employeeDependents.getArabicDesc());
								dtoEmployeeDependents1.setEmpRelationshipId(employeeDependents.getEmpRelationshipId());
								dtoEmployeeDependents1.setDesc(employeeDependents.getEmpRelationshipId()+" | "+employeeDependents.getDesc());
						

								dtoEmployeeDependents.add(dtoEmployeeDependents1);
							}
							dtoSearch.setRecords(dtoEmployeeDependents);
						}

						dtoSearch.setTotalCount(employeeDependentsList.size());
					}
				
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public List<DtoEmployeeDependents> getAllemployeeDependentsDropDownList() {
		log.info("getAllEmployeeDependent InActive List  Method");
		List<DtoEmployeeDependents> dtoEmployeeDependentsList = new ArrayList<>();
		try {
			
			List<EmployeeDependents> list = repositoryEmployeeDependents.findByIsDeleted(false);
			
			if (list != null && !list.isEmpty()) {
				for (EmployeeDependents employeeDependents : list) {
					DtoEmployeeDependents dtoEmployeeDependents = new DtoEmployeeDependents();
					dtoEmployeeDependents.setEmpRelationshipId(employeeDependents.getEmpRelationshipId());
					dtoEmployeeDependents.setId(employeeDependents.getId());
					dtoEmployeeDependents.setDesc(employeeDependents.getDesc());
					dtoEmployeeDependents.setArabicDesc(employeeDependents.getArabicDesc());
					
					dtoEmployeeDependentsList.add(dtoEmployeeDependents);
				}
			}
			log.debug("Position is:"+dtoEmployeeDependentsList.size());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDependentsList;
	}
	

	
	
	
}
