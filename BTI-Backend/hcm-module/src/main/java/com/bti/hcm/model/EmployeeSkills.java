package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00109",indexes = {
        @Index(columnList = "EMPSKINDX")
})
public class EmployeeSkills extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPSKINDX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "SKLSTINDX")
	private SkillSetSetup skillSetSetup;
	
	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "employeeSkills")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeeSkillsDesc> listEmployeeSkillsDesc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SkillSetSetup getSkillSetSetup() {
		return skillSetSetup;
	}

	public void setSkillSetSetup(SkillSetSetup skillSetSetup) {
		this.skillSetSetup = skillSetSetup;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public List<EmployeeSkillsDesc> getListEmployeeSkillsDesc() {
		return listEmployeeSkillsDesc;
	}

	public void setListEmployeeSkillsDesc(List<EmployeeSkillsDesc> listEmployeeSkillsDesc) {
		this.listEmployeeSkillsDesc = listEmployeeSkillsDesc;
	}
	
	
}
