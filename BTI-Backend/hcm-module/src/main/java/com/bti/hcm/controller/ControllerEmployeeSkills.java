package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeSkills;
import com.bti.hcm.model.dto.DtoEmployeeSkillsDesc;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeSkills;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeSkills")
public class ControllerEmployeeSkills extends BaseController{

	@Autowired
	ServiceEmployeeSkills serviceEmployeeSkills;
	
	
	@Autowired
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeSkills.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoEmployeeSkills dtoEmployeeSkills) throws Exception{
		LOGGER.info("Create EmployeeSkills Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeSkills = serviceEmployeeSkills.saveOrUpdate(dtoEmployeeSkills);
			responseMessage=displayMessage(dtoEmployeeSkills, "EMPLOYEE_SKILL_CREATED", "EMPLOYEE_SKILL_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeeSkills Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeeSkills dtoTimeCode) throws Exception {
		LOGGER.info("Update EmployeeSkills Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTimeCode = serviceEmployeeSkills.saveOrUpdate(dtoTimeCode);
			responseMessage=displayMessage(dtoTimeCode, "EMPLOYEE_SKILL_UPDATED_SUCCESS", "EMPLOYEE_SKILL_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeeSkills Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByEmployeeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeSkills Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeSkills.searchByEmployeeId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.EMPLOYEE_SKILL_GET_DETAIL, MessageConstant.EMPLOYEE_SKILL_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search EmployeeSkills Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getByEmployeeId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getByEmployeeId(@RequestBody DtoEmployeeSkills dtoEmployeeSkills, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeSkills Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeSkills = this.serviceEmployeeSkills.getByEmployeeId(dtoEmployeeSkills);
			responseMessage=displayMessage(dtoEmployeeSkills, MessageConstant.EMPLOYEE_SKILL_GET_DETAIL, MessageConstant.EMPLOYEE_SKILL_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoEmployeeSkills!=null) {
			LOGGER.debug("Search EmployeeSkills Method:"+dtoEmployeeSkills.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeSkills Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeSkills.listSkills(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_SKILL_GET_DETAIL", "EMPLOYEE_SKILL_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search EmployeeSkills Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/deleteEmployeeSkill", method = RequestMethod.PUT)
	public ResponseMessage deleteEmployeeSkill(HttpServletRequest request, @RequestBody DtoEmployeeSkillsDesc dtoEmployeeSkillsDesc) throws Exception {
		LOGGER.info("Delete TimeCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeSkillsDesc.getIds() != null && !dtoEmployeeSkillsDesc.getIds().isEmpty()) {
				DtoEmployeeSkillsDesc dtoPosition1 = serviceEmployeeSkills.delete(dtoEmployeeSkillsDesc.getIds());
				if(dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_SKILL_DELETED", false), dtoPosition1);

				}else {
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_SKILL_NOT_DELETE_ID_MESSAGE", false), dtoPosition1);


				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete TimeCode Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoEmployeeSkills dtoEmployeeSkills) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeSkills dtoEmployeeSkillsObj = serviceEmployeeSkills.getById(dtoEmployeeSkills.getId());
			responseMessage=displayMessage(dtoEmployeeSkillsObj, "EMPLOYEE_SKILL_LIST_GET_DETAIL", "EMPLOYEE_SKILL_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoEmployeeSkills.getId());
		return responseMessage;
	}
	

	
	@RequestMapping(value = "/employeeIdandSkillIdcheck", method = RequestMethod.POST)
	public ResponseMessage helthInsurancecheck(HttpServletRequest request, @RequestBody DtoEmployeeSkills dtoEmployeeSkills)
			throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeSkills dtoEmployeeSkillsObj = serviceEmployeeSkills.helthInsurancecheck(dtoEmployeeSkills);
			
			 if (dtoEmployeeSkillsObj !=null && dtoEmployeeSkillsObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                		 serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_SKILL_LIST_GET_DETAIL_EXITS", false), dtoEmployeeSkillsObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
                		 serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_SKILL_LIST_GET_DETAIL_NOT_EXIT", false),
                         dtoEmployeeSkillsObj);
             }
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
