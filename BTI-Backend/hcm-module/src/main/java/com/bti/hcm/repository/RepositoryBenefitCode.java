/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.PayCode;

/**
 * Description: Interface for BenefitCode Name of Project: Hcm Version: 0.0.1
 */
@Repository("repositoryBenefitCode")
public interface RepositoryBenefitCode extends JpaRepository<BenefitCode, Integer> {

	/**
	 *
	 * @param id
	 * @param deleted
	 * @return
	 */
	public BenefitCode findByIdAndIsDeleted(int id, boolean deleted);

	/**
	 *
	 * @param deleted
	 * @return
	 */
	public List<BenefitCode> findByIsDeleted(Boolean deleted);

	/**
	 *
	 * @param deleted
	 * @param inActive
	 * @return
	 */
	public List<BenefitCode> findByIsDeletedAndInActive(Boolean deleted, Boolean inActive);

	@Query("select m from BenefitCode m where  m.startDate >= :startDate and m.endDate <= :endDate and m.inActive=false and m.isDeleted=false")
	List<BenefitCode> findByStartEndDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query("select m from BenefitCode m where  (m.startDate <= :startDate and m.endDate >= :endDate) and m.inActive=false and m.isDeleted=false")
	List<BenefitCode> findByStartEndDate2(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 *
	 * @return
	 */
	@Query("select count(*) from BenefitCode b where b.isDeleted=false")
	public Integer getCountOfTotalBenefitCode();

	/**
	 *
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<BenefitCode> findByIsDeleted(Boolean deleted, Pageable pageable);

	/**
	 *
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BenefitCode b set b.isDeleted =:deleted, b.updatedBy=:updateById where b.id IN (:idList)")
	public void deleteBenefitCode(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);

	/**
	 *
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BenefitCode b set b.isDeleted =:deleted ,b.updatedBy =:updateById where b.id =:id ")
	public void deleteSingleBenefitCode(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	/**
	 *
	 * @return
	 */
	public BenefitCode findTop1ByOrderByBenefitIdDesc();

	/**
	 *
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select b from BenefitCode b where ( b.benefitId like :searchKeyWord  or b.desc like :searchKeyWord or b.arbicDesc like :searchKeyWord) and b.isDeleted=false")
	public List<BenefitCode> predictiveBenefitCodeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 *
	 * @param deleted
	 * @return
	 */
	public List<BenefitCode> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	/**
	 *
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from BenefitCode b where (b.benefitId like :searchKeyWord  or b.desc like :searchKeyWord or b.arbicDesc like :searchKeyWord) and b.isDeleted=false")
	public Integer predictiveBenefitCodeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 *
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select b from BenefitCode b where (b.benefitId like :searchKeyWord  or b.desc like :searchKeyWord or b.arbicDesc like :searchKeyWord) and b.isDeleted=false")
	public List<BenefitCode> predictiveBenefitCodeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	/**
	 *
	 * @param benefitId
	 * @return
	 */

	@Query("select b from BenefitCode b where (b.benefitId =:benefitId) and b.isDeleted=false")
	public List<BenefitCode> findByBenefitId(@Param("benefitId") String benefitId);

	@Query("select d.benefitId from BenefitCode d where (d.benefitId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<String> predictiveBenefitIdSearchWithPagination(@Param("searchKeyWord") String string);

	@Query("select d from BenefitCode d where (d.benefitId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<BenefitCode> predictivesearchBenefitIdWithPagination(@Param("searchKeyWord") String benefitId);

	@Query("select count(*) from BenefitCode b ")
	public Integer getCountOfTotalAtteandaces();

	@Query("select d from BenefitCode d where d.isDeleted=false and d.allPaycode=true")
	public List<BenefitCode> findByIsDeletedAndAllPaycode();

	@Query("select d from BenefitCode d where d.id=:id")
	public BenefitCode findByBenefitId(@Param("id") Integer id);

	@Query("select m from BenefitCode m where  m.startDate <= :startDate and m.endDate <= :endDate and m.inActive=false and m.isDeleted=false")
	List<BenefitCode> findByStartEndDate1(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

//    select * from hcm_smartsoft.hr40903  where bencstdt <= benceddt AND (benceddt >= '2019-06-30'  OR benceddt BETWEEN '2019-06-01' AND '2019-06-30' ) AND bencstdt < '2019-06-30'  ;

	@Query("select d from BenefitCode d where d.startDate <= d.endDate and (d.endDate >= :endDate or d.endDate between :startDate and :endDate) and d.startDate < :endDate and d.inActive=false and d.isDeleted=false")
	List<BenefitCode> findByStartDateAfterAndEndDateBefore(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query("select d from BenefitCode d where d.id=:id and d.startDate <= d.endDate and (d.endDate >= :endDate or d.endDate between :startDate and :endDate) and d.startDate < :endDate and d.inActive=false and d.isDeleted=false")
	BenefitCode findByStartDateAfterAndEndDateBeforeAndBenefitId(@Param("id") Integer id,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	// For ESB Calc //ME
	@Query("select b from BenefitCode b where b.typeField in :ids and b.isDeleted=false") // unused
	List<PayCode> findByTypeFieldIdsAndIsDeleted(@Param("ids") List<Integer> ids);

	//
	@Query("select b.id from BenefitCode b where b.typeField in :ids and b.isDeleted=false") // THIS //ME //ESB
	List<Integer> findIdsByTypeFieldIdsAndIsDeleted(@Param("ids") List<Integer> ids);

}
