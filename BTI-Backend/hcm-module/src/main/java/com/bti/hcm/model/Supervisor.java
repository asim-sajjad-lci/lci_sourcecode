package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40105",indexes = {
        @Index(columnList = "SUPRINDX")
})
public class Supervisor extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SUPRINDX")
	private int id;

	@Column(name = "SUPRID", columnDefinition = "char(15)")
	private String superVisionCode;

	@Column(name = "SUPRDSCR", columnDefinition = "char(31)")
	private String description;

	@Column(name = "SUPRDSCRA", columnDefinition = "char(61)")
	private String arabicDescription;

	@Column(name = "EMPLYNMN", columnDefinition = "char(150)")
	private String employeeName;
	
	@OneToMany(mappedBy = "supervisor",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeePositionHistory> listEmployeePositionHistory;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "EMPLOYEE_ID")
	private EmployeeMaster employeeId;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "supervisor")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<EmployeeMaster> employeeMaster;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "supervisor")
	@Where(clause = "is_deleted = false")
	private List<Requisitions> requisitions;

	public EmployeeMaster getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(EmployeeMaster employeeId) {
		this.employeeId = employeeId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSuperVisionCode() {
		return superVisionCode;
	}

	public void setSuperVisionCode(String superVisionCode) {
		this.superVisionCode = superVisionCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public List<Requisitions> getRequisitions() {
		return requisitions;
	}

	public void setRequisitions(List<Requisitions> requisitions) {
		this.requisitions = requisitions;
	}

	public List<EmployeePositionHistory> getListEmployeePositionHistory() {
		return listEmployeePositionHistory;
	}

	public void setListEmployeePositionHistory(List<EmployeePositionHistory> listEmployeePositionHistory) {
		this.listEmployeePositionHistory = listEmployeePositionHistory;
	}

	/*
	 * @Column(name="EMPLYINDX") private int employeeId;
	 */

}
