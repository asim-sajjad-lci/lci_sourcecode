package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.TrainingBatchSignup;

@Repository("repositoryTrainingBatchSignup")
public interface RepositoryTrainingBatchSignup extends JpaRepository<TrainingBatchSignup, Integer>{

	TrainingBatchSignup saveAndFlush(TrainingBatchSignup trainingBatchSignup);

	TrainingBatchSignup findOne(Integer id);

	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TrainingBatchSignup t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	public void deleteSingleTrainingBatchSignup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	
	@Query("select count(*) from TrainingBatchSignup t where (t.trainingCourse.desc LIKE :searchKeyWord or t.trainingCourse.traningId LIKE :searchKeyWord or t.selectedEmployes LIKE :searchKeyWord or t.courseComments LIKE :searchKeyWord) and t.isDeleted=false")
	Integer predictiveTrainingBatchSignupSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);

	
	@Query("select t from TrainingBatchSignup t where (t.trainingCourse.desc LIKE :searchKeyWord or t.trainingCourse.desc LIKE :searchKeyWord or t.trainingCourse.traningId LIKE :searchKeyWord or t.selectedEmployes LIKE :searchKeyWord or t.courseComments LIKE :searchKeyWord) and t.isDeleted=false")
	List<TrainingBatchSignup> predictiveTrainingBatchSignupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	
	TrainingBatchSignup findByIdAndIsDeleted(int id, boolean b);
	
	@Query("select t from TrainingBatchSignup t where (t.trainingCourse.id =:id) and t.isDeleted = false")
	List<TrainingBatchSignup> predictiveTrainingBatchSignupSearchWithPaginationByTraningId(@Param("id") Integer id);

	@Query(value = "select employee_master_employindx from hr40811_employee_master t where t.training_batch_signup_trncclsrwid =:id",nativeQuery = true)
	List<Integer> getEmployeeByTrainingId(@Param("id")Integer id);

	
	@Query("FROM EmployeeMaster e WHERE e.employeeIndexId IN (:id)")
	List<EmployeeMaster> findEmployee(@Param("id")List<Integer> employeeMasters);
	

	List<TrainingBatchSignup> findByTrainingClassStatusAndIsDeleted(Short status, Boolean deleted);
	
	@Query("select t from TrainingBatchSignup t where (t.trainingCourse.id =:id) and t.isDeleted = false")
	List<TrainingBatchSignup> predictiveTrainingSignupSearchWithPaginationByTraningId(@Param("id") Integer id);

	@Query("select t from TrainingBatchSignup t where (t.trainingCourse.id =:id) and t.isDeleted = false")
	List<TrainingBatchSignup> findByTrainingClassByIdAndIsDeleted(@Param("id") Integer id);
	
	
	@Query("select t from TrainingBatchSignup t where (t.trainingCourse.id =:id) and t.isDeleted = false")
	List<TrainingBatchSignup> findByTrainingClassByIdAndTrainingClassStatusAndIsDeleted(@Param("id") Integer id);
	
	@Query(name = "select * from hr40811_hr40801  where training_course_detail_id =:id",nativeQuery= true)
	List<TrainingBatchSignup> findByTrainingCourseDetailsId(@Param("id") Integer id);

	@Query("select count(*) from TrainingBatchSignup t ")
	public Integer getCountOfTotalTrainingBatchSignup();



}
