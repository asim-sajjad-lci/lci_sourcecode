package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.CompnayInsurance;
import com.bti.hcm.model.RetirementEntranceDate;
import com.bti.hcm.model.RetirementFundSetup;
import com.bti.hcm.model.RetirementPlanSetup;
import com.bti.hcm.model.dto.DtoRetirementPlanSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryCompanyInsurance;
import com.bti.hcm.repository.RepositoryRetirementEntranceDate;
import com.bti.hcm.repository.RepositoryRetirementFundSetup;
import com.bti.hcm.repository.RepositoryRetirementPlanSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceRetirementPlanSetup")
public class ServiceRetirementPlanSetup {

	/**
	 * @Description LOGGER use for put a logger in RetirementPlanSetup Service
	 */	
static Logger log = Logger.getLogger(ServiceRetirementPlanSetup.class.getName());
	
/**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceSalaryMatrixRow service
 */	
@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
/**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceSalaryMatrixRow service
 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryRetirementPlanSetup repositoryRetirementPlanSetup;
	
	/**
	 *  @Description repositoryHelthInsurance Autowired here using annotation of spring for access of repositoryHelthInsurance method in HelthInsurance service
	 * 				In short Access CompanyInsurance Query from Database using repositoryHelthInsurance.
	 */
	@Autowired
	RepositoryCompanyInsurance repositoryHelthInsurance;
	
	/**
	 * @Description repositoryRetirementEntranceDate Autowired here using annotation of spring for access of repositoryRetirementEntranceDate method in RetirementEntranceDate service
	 * 				In short Access RetirementEntranceDate Query from Database using repositoryRetirementEntranceDate.
	 */
	@Autowired
	RepositoryRetirementEntranceDate repositoryRetirementEntranceDate;
	
	/**
	 * @Description repositoryRetirementFundSetup Autowired here using annotation of spring for access of repositoryRetirementFundSetup method in RetirementFundSetup service
	 * 				In short Access repositoryRetirementFundSetup Query from Database using repositoryRetirementFundSetup.
	 */
	@Autowired
	RepositoryRetirementFundSetup repositoryRetirementFundSetup;
	
	
	public DtoRetirementPlanSetup saveOrUpdateRetirementPlanSetup(DtoRetirementPlanSetup dtoRetirementPlanSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		RetirementPlanSetup retirementPlanSetup=null;
		if (dtoRetirementPlanSetup.getId() != null && dtoRetirementPlanSetup.getId() > 0) {
			retirementPlanSetup = repositoryRetirementPlanSetup.findByIdAndIsDeleted(dtoRetirementPlanSetup.getId(), false);
			retirementPlanSetup.setUpdatedBy(loggedInUserId);
			retirementPlanSetup.setUpdatedDate(new Date());
		} else {
			retirementPlanSetup = new RetirementPlanSetup();
			retirementPlanSetup.setCreatedDate(new Date());
			

			Integer rowId = repositoryRetirementPlanSetup.getCountOfTotaRetirementPlanSetup();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			retirementPlanSetup.setRowId(increment);
		}
		CompnayInsurance helthInsurance = null;
		if(dtoRetirementPlanSetup.getHelthInsuranceId()!=null && dtoRetirementPlanSetup.getHelthInsuranceId()>0) {
			helthInsurance = repositoryHelthInsurance.findOne(dtoRetirementPlanSetup.getHelthInsuranceId());	
		}
		
		RetirementEntranceDate retirementEntranceDate=null;
		if(dtoRetirementPlanSetup.getRetirementEntranceDateId()!=null && dtoRetirementPlanSetup.getRetirementEntranceDateId()>0) {
			retirementEntranceDate=repositoryRetirementEntranceDate.findOne(dtoRetirementPlanSetup.getRetirementEntranceDateId());
		}else {
			retirementEntranceDate= new RetirementEntranceDate();
			retirementEntranceDate.setDateSequence(dtoRetirementPlanSetup.getDateSequence());
			retirementEntranceDate.setPlanId(dtoRetirementPlanSetup.getPlanId());
			retirementEntranceDate.setEntranceDescription(dtoRetirementPlanSetup.getEntranceDescription());
			retirementEntranceDate=repositoryRetirementEntranceDate.saveAndFlush(retirementEntranceDate);
		}
		RetirementFundSetup retirementFundSetup=null;
		if(dtoRetirementPlanSetup.getRetirementFundSetupId()!=null&&dtoRetirementPlanSetup.getRetirementFundSetupId()>0) {
			retirementFundSetup=repositoryRetirementFundSetup.findOne(dtoRetirementPlanSetup.getRetirementFundSetupId());
		}else {
			retirementFundSetup = new RetirementFundSetup();
			retirementFundSetup.setFundId(dtoRetirementPlanSetup.getFundId());
			retirementFundSetup.setPlanDescription(dtoRetirementPlanSetup.getPlanDescription());
			retirementFundSetup.setFundSequence(dtoRetirementPlanSetup.getFundSequence());
			retirementFundSetup.setFundActive(dtoRetirementPlanSetup.getFundActive());
			retirementFundSetup=repositoryRetirementFundSetup.saveAndFlush(retirementFundSetup);
		}
		
		
		
		retirementPlanSetup.setHelthInsurance(helthInsurance);
		retirementPlanSetup.setRetirementEntranceDate(retirementEntranceDate);
		retirementPlanSetup.setRetirementFundSetup(retirementFundSetup);
		retirementPlanSetup.setRetirementPlanId(dtoRetirementPlanSetup.getRetirementPlanId());
		retirementPlanSetup.setRetirementPlanAccountNumber(dtoRetirementPlanSetup.getRetirementPlanAccountNumber());
		retirementPlanSetup.setRetirementPlanDescription(dtoRetirementPlanSetup.getRetirementPlanDescription());
		retirementPlanSetup.setRetirementPlanArbicDescription(dtoRetirementPlanSetup.getRetirementPlanArbicDescription());
		retirementPlanSetup.setRetirementPlanMatchPercent(dtoRetirementPlanSetup.getRetirementPlanMatchPercent());
		retirementPlanSetup.setRetirementPlanMaxPercent(dtoRetirementPlanSetup.getRetirementPlanMaxPercent());
		retirementPlanSetup.setRetirementPlanMaxAmount(dtoRetirementPlanSetup.getRetirementPlanMaxAmount());
		retirementPlanSetup.setLoans(dtoRetirementPlanSetup.getLoans());
		retirementPlanSetup.setBonus(dtoRetirementPlanSetup.getBonus());
		retirementPlanSetup.setRetirementAge(dtoRetirementPlanSetup.getRetirementAge());
		retirementPlanSetup.setWatingPeriods(dtoRetirementPlanSetup.getWatingPeriods());
		retirementPlanSetup.setRetirementContribution(dtoRetirementPlanSetup.getRetirementContribution());
		retirementPlanSetup.setWatingMethod(dtoRetirementPlanSetup.getWatingMethod());
		retirementPlanSetup.setRetirementPlanAmount(dtoRetirementPlanSetup.getRetirementPlanAmount());
		retirementPlanSetup.setRetirementPlanPercent(dtoRetirementPlanSetup.getRetirementPlanPercent());
		retirementPlanSetup.setRetirementFrequency(dtoRetirementPlanSetup.getRetirementFrequency());
			
		
		retirementPlanSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryRetirementPlanSetup.saveAndFlush(retirementPlanSetup);
		return dtoRetirementPlanSetup;
		
	}
	
	
	public DtoRetirementPlanSetup deleteRetirementPlanSetup(List<Integer> ids) {
		log.info("deleteRetirementPlanSetup Method");
		DtoRetirementPlanSetup dtoRetirementPlanSetup = new DtoRetirementPlanSetup();
		dtoRetirementPlanSetup.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("RETIREMENTPLANSETUP_DELETED", false));
		dtoRetirementPlanSetup.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("RETIREMENTPLANSETUP_ASSOCIATED", false));
		List<DtoRetirementPlanSetup> deleteRetirementPlanSetup = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer RetirementPlanId : ids) {
				RetirementPlanSetup retirementPlanSetup = repositoryRetirementPlanSetup.findOne(RetirementPlanId);
				
					DtoRetirementPlanSetup dtoRetirementPlanSetup2 = new DtoRetirementPlanSetup();
					dtoRetirementPlanSetup.setId(RetirementPlanId);
					dtoRetirementPlanSetup.setRetirementPlanAccountNumber(retirementPlanSetup.getRetirementPlanAccountNumber());

					dtoRetirementPlanSetup.setRetirementPlanDescription(retirementPlanSetup.getRetirementPlanDescription());
					dtoRetirementPlanSetup.setRetirementPlanArbicDescription(retirementPlanSetup.getRetirementPlanArbicDescription());
					dtoRetirementPlanSetup.setRetirementPlanMatchPercent(retirementPlanSetup.getRetirementPlanMatchPercent());
					dtoRetirementPlanSetup.setRetirementPlanMaxAmount(retirementPlanSetup.getRetirementPlanMaxAmount());
					dtoRetirementPlanSetup.setRetirementPlanMaxPercent(retirementPlanSetup.getRetirementPlanMaxPercent());
					dtoRetirementPlanSetup.setLoans(retirementPlanSetup.getLoans());
					dtoRetirementPlanSetup.setBonus(retirementPlanSetup .getBonus());
					dtoRetirementPlanSetup.setRetirementAge(retirementPlanSetup.getRetirementAge());
					dtoRetirementPlanSetup.setWatingPeriods(retirementPlanSetup.getWatingPeriods());
					dtoRetirementPlanSetup.setRetirementContribution(retirementPlanSetup.getRetirementContribution());
					dtoRetirementPlanSetup.setRetirementPlanAmount(retirementPlanSetup.getRetirementPlanPercent());
					dtoRetirementPlanSetup.setRetirementPlanPercent(retirementPlanSetup.getRetirementPlanPercent());
					dtoRetirementPlanSetup.setRetirementFrequency(retirementPlanSetup.getRetirementFrequency());
					
					repositoryRetirementPlanSetup.deleteSingleRetirementPlanSetup(true, loggedInUserId, RetirementPlanId);
					deleteRetirementPlanSetup.add(dtoRetirementPlanSetup2);	
				
			}
			dtoRetirementPlanSetup.setDelete(deleteRetirementPlanSetup);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete RETIREMENTPALNSETUP :"+dtoRetirementPlanSetup.getId());
		return dtoRetirementPlanSetup;
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchRetirementPlanSetup(DtoSearch dtoSearch) {
		log.info("searchRetirementPlanSetup Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("retirementPlanId") || dtoSearch.getSortOn().equals("retirementPlanDescription") || dtoSearch.getSortOn().equals("retirementPlanArbicDescription") || dtoSearch.getSortOn().equals("retirementPlanAccountNumber")
							|| dtoSearch.getSortOn().equals("retirementPlanMatchPercent") || dtoSearch.getSortOn().equals("retirementPlanMaxPercent") || dtoSearch.getSortOn().equals("retirementPlanMaxAmount") || dtoSearch.getSortOn().equals("loans")
							|| dtoSearch.getSortOn().equals("bonus")|| dtoSearch.getSortOn().equals("retirementAge")|| dtoSearch.getSortOn().equals("retirementContribution") 
							|| dtoSearch.getSortOn().equals("watingMethod") || dtoSearch.getSortOn().equals("retirementPlanAmount") || dtoSearch.getSortOn().equals("retirementPlanPercent")|| dtoSearch.getSortOn().equals("retirementFrequency")   ) {
						condition=dtoSearch.getSortOn();
					}else {
						condition = "id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
			
			dtoSearch.setTotalCount(this.repositoryRetirementPlanSetup.predictiveRetirementPlanSetupSearchTotalCount("%"+searchWord+"%"));
			List<RetirementPlanSetup> retirementPlanSetupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					retirementPlanSetupList = this.repositoryRetirementPlanSetup.predictiveRetirementPlanSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					retirementPlanSetupList = this.repositoryRetirementPlanSetup.predictiveRetirementPlanSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					retirementPlanSetupList = this.repositoryRetirementPlanSetup.predictiveRetirementPlanSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			
			
			
		
			if(retirementPlanSetupList != null && !retirementPlanSetupList.isEmpty()){
				List<DtoRetirementPlanSetup> dtoRetirementPlanSetupList = new ArrayList<>();
				DtoRetirementPlanSetup dtoRetirementPlanSetup=null;
				for (RetirementPlanSetup retirementPlanSetup : retirementPlanSetupList) {
					dtoRetirementPlanSetup = new DtoRetirementPlanSetup(retirementPlanSetup);
					dtoRetirementPlanSetup.setId(retirementPlanSetup.getId());
					dtoRetirementPlanSetup.setRetirementPlanAccountNumber(retirementPlanSetup.getRetirementPlanAccountNumber());
					if(retirementPlanSetup.getHelthInsurance()!=null) {
						dtoRetirementPlanSetup.setHelthInsuranceId(retirementPlanSetup.getHelthInsurance().getId());
						dtoRetirementPlanSetup.setInsCompanyId(retirementPlanSetup.getHelthInsurance().getInsCompanyId());
					}
					dtoRetirementPlanSetup.setRetirementPlanDescription(retirementPlanSetup.getRetirementPlanDescription());
					dtoRetirementPlanSetup.setRetirementPlanArbicDescription(retirementPlanSetup.getRetirementPlanArbicDescription());
					dtoRetirementPlanSetup.setRetirementPlanMatchPercent(retirementPlanSetup.getRetirementPlanMatchPercent());
					dtoRetirementPlanSetup.setRetirementPlanMaxAmount(retirementPlanSetup.getRetirementPlanMaxAmount());
					dtoRetirementPlanSetup.setRetirementPlanMaxPercent(retirementPlanSetup.getRetirementPlanMaxPercent());
					dtoRetirementPlanSetup.setWatingMethod(retirementPlanSetup.getWatingMethod());
					dtoRetirementPlanSetup.setLoans(retirementPlanSetup.getLoans());
					dtoRetirementPlanSetup.setBonus(retirementPlanSetup .getBonus());
					dtoRetirementPlanSetup.setRetirementAge(retirementPlanSetup.getRetirementAge());
					dtoRetirementPlanSetup.setWatingPeriods(retirementPlanSetup.getWatingPeriods());
					dtoRetirementPlanSetup.setRetirementContribution(retirementPlanSetup.getRetirementContribution());
					dtoRetirementPlanSetup.setRetirementPlanAmount(retirementPlanSetup.getRetirementPlanAmount());
					dtoRetirementPlanSetup.setRetirementPlanPercent(retirementPlanSetup.getRetirementPlanPercent());
					dtoRetirementPlanSetup.setRetirementFrequency(retirementPlanSetup.getRetirementFrequency());
					
		             dtoRetirementPlanSetup.setId(retirementPlanSetup.getId());
		             if(retirementPlanSetup.getRetirementEntranceDate()!=null) {
					dtoRetirementPlanSetup.setDateSequence(retirementPlanSetup.getRetirementEntranceDate().getDateSequence());
					dtoRetirementPlanSetup.setPlanId(retirementPlanSetup.getRetirementEntranceDate().getPlanId());
					dtoRetirementPlanSetup.setEntranceDescription(retirementPlanSetup.getRetirementEntranceDate().getEntranceDescription());
					dtoRetirementPlanSetup.setFundId(retirementPlanSetup.getRetirementFundSetup().getFundId());
					dtoRetirementPlanSetup.setFundSequence(retirementPlanSetup.getRetirementFundSetup().getFundSequence());
					dtoRetirementPlanSetup.setPlanDescription(retirementPlanSetup.getRetirementFundSetup().getPlanDescription());
					dtoRetirementPlanSetup.setFundActive(retirementPlanSetup.getRetirementFundSetup().getFundActive());
					
					
		             }
					
					dtoRetirementPlanSetupList.add(dtoRetirementPlanSetup);	
				}
				
				dtoSearch.setRecords(dtoRetirementPlanSetupList);
				}
			}

		
		log.debug("Search searchPosition Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
		}
	
	
	public DtoRetirementPlanSetup getRetirementPlanSetupById(int id) {
		DtoRetirementPlanSetup dtoRetirementPlanSetup  = new DtoRetirementPlanSetup();
		if (id > 0) {
			RetirementPlanSetup retirementPlanSetup = repositoryRetirementPlanSetup.findByIdAndIsDeleted(id, false);
			if (retirementPlanSetup != null) {
				dtoRetirementPlanSetup = new DtoRetirementPlanSetup(retirementPlanSetup);
			} else {
				dtoRetirementPlanSetup.setMessageType("RETIREMENTPLANSETUP_NOT_GETTING");

			}
		} else {
			dtoRetirementPlanSetup.setMessageType("INVALID_RETIREMENTPLAN_ID");

		}
		return dtoRetirementPlanSetup;
	}
	
	
	
	public DtoRetirementPlanSetup repeatByRetirementPlanId(String retirementPlanId) {
		log.info("repeatByRetirementPlanId Method");
		DtoRetirementPlanSetup dtoRetirementPlanSetup = new DtoRetirementPlanSetup();
		try {
			List<RetirementPlanSetup> retirementPlanSetup=repositoryRetirementPlanSetup.findByRetirementPlanId(retirementPlanId);
			if(retirementPlanSetup!=null && !retirementPlanSetup.isEmpty()) {
				dtoRetirementPlanSetup.setIsRepeat(true);
				dtoRetirementPlanSetup.setRetirementPlanId(retirementPlanId);
			}else {
				dtoRetirementPlanSetup.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoRetirementPlanSetup;
	}
	
	
}













