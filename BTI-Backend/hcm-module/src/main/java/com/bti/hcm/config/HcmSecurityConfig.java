/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Description: SecurityConfig
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */

@Configuration
@ComponentScan("com.bti.hcm")
@Async
@CacheConfig
//@ComponentScan("com.bti.model")
//@ComponentScan("com.bti.repository")
//@ComponentScan("com.bti.model.dto")
//@ComponentScan("com.bti.config.WebConfig")
//@ComponentScan("com.bti.constant")
//@ComponentScan("com.bti.authentication")


@EnableGlobalMethodSecurity(prePostEnabled = true)
public class HcmSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	Environment env;

	/*@Autowired(required=false)
	com.bti.service.ServiceAuthentication authenticationService;*/

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		
		
		 
		http
			.csrf().disable()
			.authorizeRequests()
			.antMatchers("/***","/login/**","/group/**","/user/**","/financialDimensions/**","/company/**","/workflow/**","/authSettings/**", "/ip/**","/plugUnplugServices/**","/language/**","/department/**","/division/**","/hrCity/**","/hrSkillSteup/**", "/hrPosition/**","/positionSetup/**","/positionPlanSetup/**","/hrPositionBudget/**","/positionPlanPayCode/**","/positionCourseToLink/**","/positionCourseDetail/**","/location/**","/supervisor/**","/positionClass/**","/positionAttachmentSetup/**","/positionBudget/**","/common/**","/hijriDate/**","/attedance/**","/atteandaceOption/**","/miscellaneousBenefits/**","/deductionCode/**","/helthCoverageType/**","/accurual/**", "/salaryMatrixSetup/**","/skillSetSteup/**","/skillSetDescDetail/**","/benefitCode/**","/helthInsurance/**","/hrCountry/**","/hrState/**","/hrcity/**","/orientationCheckListSetup/**","/orientationPredefinedCheckListItem/**","/lifeInsuranceSetup/**","/PayScheduleSetup/**","/PayScheduleSetupSchedulePeriods/**","/interviewTypeSetup/**","/interviewTypeSetupDetail/**", "/retirementPlanSetup/**" ,"/retirementEntranceDate/**"  ,"/retirementFundSetup/**", "/HelthInsuranceSetUp/**","/payCode/**","/timeCode/**","/terminationSetup/**","/accrualSetup/**","/accrualSchedule/**","/exitInterview/**","/predefinecheckList/**","/payCodeType/**", "/orientationSetup/**","/shiftCode/**","/benefitPreferences/**","/predefinedCheckListSetup/**","/accrualScheduleDetail/**","/accurualType/**","/activityLog/**","/requisition/**","/terminationSetupDetails/**","/payScheduleSetupEmployee/**","/payScheduleSetupDepartment/**","/payScheduleSetupLocation/**","/payScheduleSetupPosition/**","/accrualPeriod/**","/settingFamilyLeave/**", "/traningCourse/**", "/trainingCourseDetail/**", "/accrualPeriodSetting/**","/controllerTraningCourseClassSkill/**" ,"/employeeMaster/**","/trainingBatchSignup/**","/atteandaceOptionType/**" ,"/orientationSetupDetail/**" ,"/applicant/**" ,"/employeeContacts/**", "/employeeDependent/**","/employeeDependents/**","/controllerEmployeePositionReason/**" ,"/employeeBenefitMaintenance/**" ,"/employeeNationalities/**" ,"/employeeAddressMaster/**" ,"/employeePayCodeMaintenance/**", "/employeeDeductionMaintenance/**", "/employeeDirectDeposit/**", "/employeeSkills/**", "/healthInsuranceEnrollment/**", "/employeePositionHistory/**", "/employeeEducation/**", "/miscellaneousBenefirtEnrollment/**", "/employeePostDatedPayRates/**","/employeeQuickAssigments/**","/batches/**","/buildChecks/**" ,"/buildChecks/**" ,"/activateEmployeePostDatePayRate/**" ,"/buildPayrollCheckByPayCodes/**" ,"/buildPayrollCheckByDeductions/**" ,"/buildPayrollCheckByBenefits/**"  ,"/buildPayrollCheckByBatches/**","/transactionEntry/**" ,"/default/**","/checkbookMaintenance/**","/vatSetup/**","/payrollSetup/**","/coaMainAccount/**","/calculateChecks/**","/payrollAccounts/**","/manualChecksDistribution/**","/manualChecksDetails/**","/manualChecks/**","/controllerJasper/**","/payrollTransectionHistoryByEmployee/**","/CodeForDeductionBenefitPayCode/**",  "/payrollTransactionHistoryByDepartment/**","/routineUtilities/**","/employeeByTransactionType/**","/newTransactionEntry/**","/bank/**", "/workflowapis/**", "/miscellaneous/**", "/values/**", "/projectSetup/**", "/miscellaneousMaintenance/**", "/loanPayment/**", "/typeFieldForCodes/**", "/packageESB/**").permitAll()
			.anyRequest().authenticated()
				.and()
			.httpBasic(); //Basic authentication enabled
		
			
	}

	/*@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(authenticationService).passwordEncoder(passwordEncoder());
	}*/
}
