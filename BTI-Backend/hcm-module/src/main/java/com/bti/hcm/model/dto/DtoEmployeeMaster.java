/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import com.bti.hcm.model.EmployeeMaster;

/**
 * Description: DTO EmployeeMaster class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
public class DtoEmployeeMaster extends DtoBase{
	
	private int employeeIndexId;
	private String employeeId;
	private String employeeTitle;
	private String employeeTitleArabic;
	private String employeeLastName;
	private String employeeFirstName;
	private String employeeMiddleName;
	private String employeeLastNameArabic;
	private String employeeFirstNameArabic;
	private String employeeMiddleNameArabic;
	private String department_Id;
	private String position_Id;
	private String division_Id;
	private Date employeeHireDate;
	private Date employeeAdjustHireDate;
	private Date employeeLastWorkDate;
	private Date employeeInactiveDate;
	private String employeeInactiveReason;
	private short employeeType;
	private short employeeGender;
	private short employeeMaritalStatus;
	private Date employeeBirthDate;
	private int employeeUserIdInSystem;
	private int employeeWorkHourYearly;
	private boolean employeeCitizen;
	private Boolean employeeInactive;
	private boolean employeeSmoker;
	private String idNumber;
	private Date expire;
	private String expireHijri;
	private String passportNumber;
	private Date expires;
	private String expiresHijri;
	private String employeeBirthDateH;
	private boolean employeeImmigration;
	private int divisionId;
	private int departmentId;
	private int locationId;
	private int supervisorId;
	private int positionId;
	private int employeeNationalitiesId;
	private int employeeAddressIndexId;
	private byte[] employeeImage;
	private Integer $$index;
	private List<DtoEmployeeMaster> deleteEmployeeMaster;
	private DtoEmployeeAddressMaster dtoEmployeeAddressMaster;
	private DtoEmployeeNationalities dtoEmployeeNationalities;
	private DtoDepartment dtoDepartment;
	private DtoDivision dtoDivision;
	private DtoPosition dtoPosition;
	private DtoLocation dtoLocation;
	private DtoSupervisor dtoSupervisor;
	private String employeeNotes;
	private String employeeEmail;
	private String employeeBankAccountNumber;
	private int bankId;
	private DtoBank dtoBank;
	private DtoProjectSetup dtoProjectSetup;
	private Integer projectId;
	private String project_Id;
	private int valueId;
	private String valueDescription;
	private int valId;
	
	
	public int getEmployeeIndexId() {
		return employeeIndexId;
	}
	public void setEmployeeIndexId(int employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeTitle() {
		return employeeTitle;
	}
	public void setEmployeeTitle(String employeeTitle) {
		this.employeeTitle = employeeTitle;
	}
	public String getEmployeeTitleArabic() {
		return employeeTitleArabic;
	}
	public void setEmployeeTitleArabic(String employeeTitleArabic) {
		this.employeeTitleArabic = employeeTitleArabic;
	}
	public String getEmployeeLastName() {
		return employeeLastName;
	}
	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}
	public String getEmployeeMiddleName() {
		return employeeMiddleName;
	}
	public void setEmployeeMiddleName(String employeeMiddleName) {
		this.employeeMiddleName = employeeMiddleName;
	}
	public String getEmployeeLastNameArabic() {
		return employeeLastNameArabic;
	}
	public void setEmployeeLastNameArabic(String employeeLastNameArabic) {
		this.employeeLastNameArabic = employeeLastNameArabic;
	}
	public String getEmployeeFirstNameArabic() {
		return employeeFirstNameArabic;
	}
	public void setEmployeeFirstNameArabic(String employeeFirstNameArabic) {
		this.employeeFirstNameArabic = employeeFirstNameArabic;
	}
	public String getEmployeeMiddleNameArabic() {
		return employeeMiddleNameArabic;
	}
	
	public String getEmployeeFullName() {
		String employeeFullName = "";
		if (employeeFirstName != null && !employeeFirstName.isEmpty()) {
			employeeFullName = employeeFirstName;
		} 
		if (employeeMiddleName != null && !employeeMiddleName.isEmpty()) {
			employeeFullName += " " + employeeMiddleName;
		} 

		if (employeeLastName != null && !employeeLastName.isEmpty()) {
			employeeFullName += " " + employeeLastName;
		}
		return employeeFullName;
	}
	
	public String getEmployeeFullNameArabic() {
		String employeeFullNameArabic = "";
		if (employeeFirstNameArabic != null && !employeeFirstNameArabic.isEmpty()) {
			employeeFullNameArabic = employeeFirstNameArabic;
		} 
		if (employeeMiddleNameArabic != null && !employeeMiddleNameArabic.isEmpty()) {
			employeeFullNameArabic += " " + employeeMiddleNameArabic;
		} 

		if (employeeLastNameArabic != null && !employeeLastNameArabic.isEmpty()) {
			employeeFullNameArabic += " " + employeeLastNameArabic;
		}
		return employeeFullNameArabic;
	}

	public void setEmployeeMiddleNameArabic(String employeeMiddleNameArabic) {
		this.employeeMiddleNameArabic = employeeMiddleNameArabic;
	}
	public Date getEmployeeHireDate() {
		return employeeHireDate;
	}
	public void setEmployeeHireDate(Date employeeHireDate) {
		this.employeeHireDate = employeeHireDate;
	}
	public Date getEmployeeAdjustHireDate() {
		return employeeAdjustHireDate;
	}
	public void setEmployeeAdjustHireDate(Date employeeAdjustHireDate) {
		this.employeeAdjustHireDate = employeeAdjustHireDate;
	}
	public Date getEmployeeLastWorkDate() {
		return employeeLastWorkDate;
	}
	public void setEmployeeLastWorkDate(Date employeeLastWorkDate) {
		this.employeeLastWorkDate = employeeLastWorkDate;
	}
	public Date getEmployeeInactiveDate() {
		return employeeInactiveDate;
	}
	public void setEmployeeInactiveDate(Date employeeInactiveDate) {
		this.employeeInactiveDate = employeeInactiveDate;
	}
	public String getEmployeeInactiveReason() {
		return employeeInactiveReason;
	}
	public void setEmployeeInactiveReason(String employeeInactiveReason) {
		this.employeeInactiveReason = employeeInactiveReason;
	}
	public short getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(short employeeType) {
		this.employeeType = employeeType;
	}
	public short getEmployeeGender() {
		return employeeGender;
	}
	public void setEmployeeGender(short employeeGender) {
		this.employeeGender = employeeGender;
	}
	public short getEmployeeMaritalStatus() {
		return employeeMaritalStatus;
	}
	public void setEmployeeMaritalStatus(short employeeMaritalStatus) {
		this.employeeMaritalStatus = employeeMaritalStatus;
	}
	public Date getEmployeeBirthDate() {
		return employeeBirthDate;
	}
	public void setEmployeeBirthDate(Date employeeBirthDate) {
		this.employeeBirthDate = employeeBirthDate;
	}
	public int getEmployeeUserIdInSystem() {
		return employeeUserIdInSystem;
	}
	public void setEmployeeUserIdInSystem(int employeeUserIdInSystem) {
		this.employeeUserIdInSystem = employeeUserIdInSystem;
	}
	public int getEmployeeWorkHourYearly() {
		return employeeWorkHourYearly;
	}
	public void setEmployeeWorkHourYearly(int employeeWorkHourYearly) {
		this.employeeWorkHourYearly = employeeWorkHourYearly;
	}
	public boolean isEmployeeCitizen() {
		return employeeCitizen;
	}
	public void setEmployeeCitizen(boolean employeeCitizen) {
		this.employeeCitizen = employeeCitizen;
	}
	
	public Boolean getEmployeeInactive() {
		return employeeInactive;
	}
	public void setEmployeeInactive(Boolean employeeInactive) {
		this.employeeInactive = employeeInactive;
	}
	public boolean isEmployeeImmigration() {
		return employeeImmigration;
	}
	public void setEmployeeImmigration(boolean employeeImmigration) {
		this.employeeImmigration = employeeImmigration;
	}
	public int getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(int divisionId) {
		this.divisionId = divisionId;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getLocationId() {
		return locationId;
	}
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	public int getSupervisorId() {
		return supervisorId;
	}
	public void setSupervisorId(int supervisorId) {
		this.supervisorId = supervisorId;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public int getEmployeeNationalitiesId() {
		return employeeNationalitiesId;
	}
	public void setEmployeeNationalitiesId(int employeeNationalitiesId) {
		this.employeeNationalitiesId = employeeNationalitiesId;
	}
	
	public int getEmployeeAddressIndexId() {
		return employeeAddressIndexId;
	}
	public void setEmployeeAddressIndexId(int employeeAddressIndexId) {
		this.employeeAddressIndexId = employeeAddressIndexId;
	}
	public byte[] getEmployeeImage() {
		return employeeImage;
	}
	public void setEmployeeImage(byte[] employeeImage) {
		this.employeeImage = employeeImage;
	}
	
	public DtoEmployeeMaster() {
		
	}
	public DtoEmployeeMaster(EmployeeMaster employeeMaster) {
		
	}
	public List<DtoEmployeeMaster> getDeleteEmployeeMaster() {
		return deleteEmployeeMaster;
	}
	public void setDeleteEmployeeMaster(List<DtoEmployeeMaster> deleteEmployeeMaster) {
		this.deleteEmployeeMaster = deleteEmployeeMaster;
	}
	public String getDepartment_Id() {
		return department_Id;
	}
	public void setDepartment_Id(String department_Id) {
		this.department_Id = department_Id;
	}
	public String getPosition_Id() {
		return position_Id;
	}
	public void setPosition_Id(String position_Id) {
		this.position_Id = position_Id;
	}
	public String getDivision_Id() {
		return division_Id;
	}
	public void setDivision_Id(String division_Id) {
		this.division_Id = division_Id;
	}
	public boolean isEmployeeSmoker() {
		return employeeSmoker;
	}
	public void setEmployeeSmoker(boolean employeeSmoker) {
		this.employeeSmoker = employeeSmoker;
	}
	public Integer get$$index() {
		return $$index;
	}
	public void set$$index(Integer $$index) {
		this.$$index = $$index;
	}
	public DtoEmployeeAddressMaster getDtoEmployeeAddressMaster() {
		return dtoEmployeeAddressMaster;
	}
	public void setDtoEmployeeAddressMaster(DtoEmployeeAddressMaster dtoEmployeeAddressMaster) {
		this.dtoEmployeeAddressMaster = dtoEmployeeAddressMaster;
	}
	public DtoEmployeeNationalities getDtoEmployeeNationalities() {
		return dtoEmployeeNationalities;
	}
	public void setDtoEmployeeNationalities(DtoEmployeeNationalities dtoEmployeeNationalities) {
		this.dtoEmployeeNationalities = dtoEmployeeNationalities;
	}
	
	public DtoDepartment getDtoDepartment() {
		return dtoDepartment;
	}
	public void setDtoDepartment(DtoDepartment dtoDepartment) {
		this.dtoDepartment = dtoDepartment;
	}
	public DtoDivision getDtoDivision() {
		return dtoDivision;
	}
	public void setDtoDivision(DtoDivision dtoDivision) {
		this.dtoDivision = dtoDivision;
	}
	public DtoPosition getDtoPosition() {
		return dtoPosition;
	}
	public void setDtoPosition(DtoPosition dtoPosition) {
		this.dtoPosition = dtoPosition;
	}
	public DtoLocation getDtoLocation() {
		return dtoLocation;
	}
	public void setDtoLocation(DtoLocation dtoLocation) {
		this.dtoLocation = dtoLocation;
	}
	public DtoSupervisor getDtoSupervisor() {
		return dtoSupervisor;
	}
	public void setDtoSupervisor(DtoSupervisor dtoSupervisor) {
		this.dtoSupervisor = dtoSupervisor;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public Date getExpire() {
		return expire;
	}
	public void setExpire(Date expire) {
		this.expire = expire;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public Date getExpires() {
		return expires;
	}
	public void setExpires(Date expires) {
		this.expires = expires;
	}
	public String getEmployeeBirthDateH() {
		return employeeBirthDateH;
	}
	public void setEmployeeBirthDateH(String employeeBirthDateH) {
		this.employeeBirthDateH = employeeBirthDateH;
	}
	public String getExpireHijri() {
		return expireHijri;
	}
	public void setExpireHijri(String expireHijri) {
		this.expireHijri = expireHijri;
	}
	public String getExpiresHijri() {
		return expiresHijri;
	}
	public void setExpiresHijri(String expiresHijri) {
		this.expiresHijri = expiresHijri;
	}
	
	
	public String getEmployeeNotes() {
		return employeeNotes;
	}

	public void setEmployeeNotes(String employeeNotes) {
		this.employeeNotes = employeeNotes;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}
	
	public String getEmployeeBankAccountNumber() {
		return employeeBankAccountNumber;
	}
	public void setEmployeeBankAccountNumber(String employeeAccountNumber) {
		this.employeeBankAccountNumber = employeeAccountNumber;
	}
	
	public int getBankId() {
		return bankId;
	}
	
	public void setBankId(int bankId) {
		this.bankId = bankId;
	}
	
	public DtoBank getDtoBank() {
		return dtoBank;
	}
	public void setDtoBank(DtoBank dtoBank) {
		this.dtoBank = dtoBank;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public DtoProjectSetup getDtoProjectSetup() {
		return dtoProjectSetup;
	}
	public void setDtoProjectSetup(DtoProjectSetup dtoProjectSetup) {
		this.dtoProjectSetup = dtoProjectSetup;
	}
	public String getProject_Id() {
		return project_Id;
	}
	public void setProject_Id(String project_Id) {
		this.project_Id = project_Id;
	}
	public int getValueId() {
		return valueId;
	}
	public void setValueId(int valueId) {
		this.valueId = valueId;
	}
	public String getValueDescription() {
		return valueDescription;
	}
	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}
	public int getValId() {
		return valId;
	}
	public void setValId(int valId) {
		this.valId = valId;
	}
	

}
