package com.bti.hcm.model.dto;

import java.sql.Date;
import java.util.List;

import com.bti.hcm.model.TerminationSetup;
import com.bti.hcm.model.TerminationSetupDetails;
import com.bti.hcm.util.UtilRandomKey;

public class DtoTerminationSetupDetails extends DtoBase{
	private Integer id;
	private Integer terminationSetupId;
	private TerminationSetup terminationSetup;
	private List<DtoTerminationSetup> subItems;
	private String checklistitem;
	private Integer sequence;
	private String personResponsible;
	private Date completeDate;
	private Integer terminationSequence;
	private String terminationDesc;
	private String terminationArbicDesc;
	private Date terminationDate;
	private String terminationPersonName;
	private List<DtoTerminationSetupDetails> delete;
	private List<DtoTerminationSetupDetails> list;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTerminationSequence() {
		return terminationSequence;
	}

	public void setTerminationSequence(Integer terminationSequence) {
		this.terminationSequence = terminationSequence;
	}

	public String getTerminationDesc() {
		return terminationDesc;
	}

	public void setTerminationDesc(String terminationDesc) {
		this.terminationDesc = terminationDesc;
	}

	public String getTerminationArbicDesc() {
		return terminationArbicDesc;
	}

	public void setTerminationArbicDesc(String terminationArbicDesc) {
		this.terminationArbicDesc = terminationArbicDesc;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getTerminationPersonName() {
		return terminationPersonName;
	}

	public void setTerminationPersonName(String terminationPersonName) {
		this.terminationPersonName = terminationPersonName;
	}
	public List<DtoTerminationSetupDetails> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoTerminationSetupDetails> delete) {
		this.delete = delete;
	}

	public Integer getTerminationSetupId() {
		return terminationSetupId;
	}

	public void setTerminationSetupId(Integer terminationSetupId) {
		this.terminationSetupId = terminationSetupId;
	}

	public TerminationSetup getTerminationSetup() {
		return terminationSetup;
	}

	public void setTerminationSetup(TerminationSetup terminationSetup) {
		this.terminationSetup = terminationSetup;
	}

	public List<DtoTerminationSetup> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<DtoTerminationSetup> subItems) {
		this.subItems = subItems;
	}

	public String getChecklistitem() {
		return checklistitem;
	}

	public void setChecklistitem(String checklistitem) {
		this.checklistitem = checklistitem;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getPersonResponsible() {
		return personResponsible;
	}

	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public DtoTerminationSetupDetails(TerminationSetupDetails terminationSetupDetails) {
		this.id = terminationSetupDetails.getId();

		if (UtilRandomKey.isNotBlank(terminationSetupDetails.getTerminationDesc())) {
			this.terminationDesc = terminationSetupDetails.getTerminationDesc();
		} else {
			this.terminationDesc = "";
		}
		if (UtilRandomKey.isNotBlank(terminationSetupDetails.getTerminationArbicDesc())) {
			this.terminationArbicDesc = terminationSetupDetails.getTerminationArbicDesc();
		} else {
			this.terminationArbicDesc = "";
		}
		if (UtilRandomKey.isNotBlank(terminationSetupDetails.getTerminationPersonName())) {
			this.terminationPersonName = terminationSetupDetails.getTerminationPersonName();
		} else {
			this.terminationPersonName = "";
		}
	}

	public DtoTerminationSetupDetails() {
	}

	public List<DtoTerminationSetupDetails> getList() {
		return list;
	}

	public void setList(List<DtoTerminationSetupDetails> list) {
		this.list = list;
	}

}
