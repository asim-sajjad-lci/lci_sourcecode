/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoOrientationCheckListSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceOrientationCheckListSetup;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: Controller orientationCheckListSetup
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/predefinedCheckListSetup")
public class ControllerOrientationCheckListSetup extends BaseController{
	

	/**
	 * @Description LOGGER use for put a logger in orientationCheckListSetup Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerOrientationCheckListSetup.class);
	
	/**
	 * @Description serviceOrientationCheckListSetup Autowired here using annotation of spring for use of serviceOrientationCheckListSetup method in this controller
	 */
	@Autowired(required=true)
	ServiceOrientationCheckListSetup serviceOrientationCheckListSetup;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @description : Create orientationCheckListSetup
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request
	 		{
			  "orientationChecklistItemDescription":"description",
			  "orientationChecklistItemDescriptionArabic":"arabic Description",
			  "orientationPredefinedCheckListItemId":1
			}
			
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"orientationChecklistItemDescription": "description",
			"orientationChecklistItemDescriptionArabic": "arabic Description",
			"orientationPredefinedCheckListItemId": 1
			},
			"btiMessage": {
			"message": "OrientationCheckListSetup created successfully.",
			"messageShort": "ORIENTATION_CHECKLIST_SETUP_CREATED"
			}
		}
	*/
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createOrientationCheckListSetup(HttpServletRequest request, @RequestBody DtoOrientationCheckListSetup dtoOrientationCheckListSetup) throws Exception {
		LOGGER.info("Create orientationCheckListSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoOrientationCheckListSetup = serviceOrientationCheckListSetup.saveOrUpdateOrientationCheckListSetup(dtoOrientationCheckListSetup);
			responseMessage=displayMessage(dtoOrientationCheckListSetup, "ORIENTATION_CHECKLIST_SETUP_CREATED", "ORIENTATION_CHECKLIST_SETUP_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create orientationCheckListSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description update OrientationCheckListSetup
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request 
	 	{
		  "id":1,
		  "orientationChecklistItemDescription":"descriptions",
		  "orientationChecklistItemDescriptionArabic":"arabic Descriptions",
		  "orientationPredefinedCheckListItemId":1
		}
		
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": 1,
			"orientationChecklistItemDescription": "descriptions",
			"orientationChecklistItemDescriptionArabic": "arabic Descriptions",
			"orientationPredefinedCheckListItemId": 1
			},
			"btiMessage": {
			"message": "OrientationCheckListSetup updated successfully.",
			"messageShort": "ORIENTATION_CHECKLIST_SETUP_UPDATED_SUCCESS"
			}
		}
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateOrientationCheckListSetup(HttpServletRequest request, @RequestBody DtoOrientationCheckListSetup dtoOrientationCheckListSetup) throws Exception {
		LOGGER.info("Update OrientationCheckListSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoOrientationCheckListSetup = serviceOrientationCheckListSetup.saveOrUpdateOrientationCheckListSetup(dtoOrientationCheckListSetup);
			responseMessage=displayMessage(dtoOrientationCheckListSetup, "ORIENTATION_CHECKLIST_SETUP_UPDATED_SUCCESS", MessageConstant.ORIENTATION_CHECKLIST_SETUP_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update OrientationCheckListSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description delete OrientationCheckListSetup
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request
	 	{
		  "ids":[1]
		}
		
		@response
		
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"deleteMessage": "OrientationCheckListSetup deleted successfully.",
			"associateMessage": "N/A",
			"deleteDtoOrientationCheckListSetup": [
			  {
			"id": 1,
			"orientationChecklistItemDescription": "descriptions",
			"orientationChecklistItemDescriptionArabic": "arabic Descriptions"
			}
		],
	},
	"btiMessage": {
		"message": "OrientationCheckListSetup deleted successfully.",
		"messageShort": "ORIENTATION_CHECKLIST_SETUP_DELETED"
		}
	}
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteOrientationCheckListSetup(HttpServletRequest request, @RequestBody DtoOrientationCheckListSetup dtoOrientationCheckListSetup) throws Exception {
		LOGGER.info("Delete OrientationCheckListSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoOrientationCheckListSetup.getIds() != null && !dtoOrientationCheckListSetup.getIds().isEmpty()) {
				DtoOrientationCheckListSetup doOrientationCheckListSetup2 = serviceOrientationCheckListSetup.deleteOrientationCheckListSetup(dtoOrientationCheckListSetup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ORIENTATION_CHECKLIST_SETUP_DELETED", false), doOrientationCheckListSetup2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete OrientationCheckListSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	/**
	 * @description search OrientationCheckListSetup
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchOrientationCheckListSetup(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search OrientationCheckListSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceOrientationCheckListSetup.searchOrientationCheckListSetup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ORIENTATION_CHECKLIST_SETUP_GET_ALL", MessageConstant.ORIENTATION_CHECKLIST_SETUP_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search OrientationCheckListSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * @description get All OrientationCheckListSetup
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST)
	public ResponseMessage getAllOrientationCheckListSetup(HttpServletRequest request, @RequestBody  DtoSearch dtoSearch) throws Exception {
		LOGGER.info("Get All OrientationCheckListSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			 dtoSearch = serviceOrientationCheckListSetup.searchOrientationCheckListSetup(dtoSearch);
			 responseMessage=displayMessage(dtoSearch, "ORIENTATION_CHECKLIST_SETUP_GET_ALL", "ORIENTATION_CHECKLIST_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All OrientationCheckListSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @description get OrientationCheckListSetup By Id
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 * @request 
	 {
		  "id":1
		}
		
		@response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": 1,
			"orientationChecklistItemDescription": "descriptions",
			"orientationChecklistItemDescriptionArabic": "arabic Descriptions"
			},
			"btiMessage": {
			"message": "OrientationCheckListSetup list fetched successfully.",
			"messageShort": "ORIENTATION_CHECKLIST_SETUP_GET_DETAIL"
			}
	}
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getOrientationCheckListSetupById(HttpServletRequest request, @RequestBody DtoOrientationCheckListSetup dtoOrientationCheckListSetup) throws Exception {
		LOGGER.info("Get OrientationCheckListSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoOrientationCheckListSetup dtoOrientationCheckListSetupObj = serviceOrientationCheckListSetup.getDtoOrientationCheckListSetupById(dtoOrientationCheckListSetup.getId());
			responseMessage=displayMessage(dtoOrientationCheckListSetupObj, "ORIENTATION_CHECKLIST_SETUP_GET_DETAIL", "ORIENTATION_CHECKLIST_SETUP_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get OrientationCheckListSetup ById Method:"+dtoOrientationCheckListSetup.getId());
		return responseMessage;
	}
	/**
	 * @description Id is exiest or not
	 * @param request
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws Exception
	 *@request
		{
		  "id":1
		}
	 *@response
	  {
				"code": 302,
				"status": "FOUND",
				"result": {
				"isRepeat": true
				},
				"btiMessage": {
				"message": "OrientationCheckListSetup ID already exists.",
				"messageShort": "ORIENTATION_CHECKLIST_SETUP_RESULT"
				}
		}
	
	 */
	@RequestMapping(value = "/orientationCheckListSetupIdcheck", method = RequestMethod.POST)
	public ResponseMessage departmetnIdCheck(HttpServletRequest request, @RequestBody DtoOrientationCheckListSetup dtoOrientationCheckListSetup) throws Exception {
		LOGGER.info("DtoOrientationCheckListSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoOrientationCheckListSetup dtoOrientationCheckListSetupObj = serviceOrientationCheckListSetup.repeatById(dtoOrientationCheckListSetup.getId());
			if (dtoOrientationCheckListSetupObj != null) {
				if (dtoOrientationCheckListSetupObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("ORIENTATION_CHECKLIST_SETUP_RESULT", false), dtoOrientationCheckListSetupObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoOrientationCheckListSetupObj.getMessageType(), false),
							dtoOrientationCheckListSetupObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("ORIENTATION_CHECKLIST_SETUP_REPEAT_ID_NOT_FOUND", false), dtoOrientationCheckListSetupObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getOrientaionChekListSetupById", method = RequestMethod.POST)
	public ResponseMessage getOrientaionChekListSetupById(HttpServletRequest request, @RequestBody DtoOrientationCheckListSetup dtoOrientationCheckListSetup) throws Exception {
		LOGGER.info("Get getOrientaionChekListSetupById  Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			List<DtoOrientationCheckListSetup> dtoOrientationCheckListSetupObj =  serviceOrientationCheckListSetup.getOrientaionChekListSetupById(dtoOrientationCheckListSetup.getId());
			responseMessage=displayMessage(dtoOrientationCheckListSetupObj, "ORIENTATION_CHECKLIST_SETUP_GET_DETAIL", "ORIENTATION_CHECKLIST_SETUP_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get OrientationCheckListSetup ById Method:"+dtoOrientationCheckListSetup.getId());
		return responseMessage;
	}
	
	
}
