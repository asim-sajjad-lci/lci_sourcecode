package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoHrState;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHrState;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/hrState")
public class ControllerHrState {


	/**
	 * @Description serviceHrState Autowired here using annotation of spring for use of serviceHrState method in this controller
	 */
	@Autowired
	ServiceHrState serviceHrState;
	
	

	/**
	 * @Description sessionManager Autowired here using annotation of spring for use of sessionManager method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;
	
	@RequestMapping(value = "/getStateList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCityList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		List<DtoHrState> cityList = serviceHrState.getStateListWithLanguage();
		responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
				serviceResponse.getMessageByShortAndIsDeleted("CITY_GET_ALL", false), cityList);
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getStateByCountry", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getStateByCountry(HttpServletRequest request,@RequestBody DtoHrState dtoHrState) {
		ResponseMessage responseMessage = null;
		List<DtoHrState> cityList = serviceHrState.getStateListBaseOnCountry(dtoHrState);
		responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
				serviceResponse.getMessageByShortAndIsDeleted("STATE_GET_ALL", false), cityList);
		return responseMessage;
	}
	
	@RequestMapping(value = "/getStateByCountryByLangId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getStateByCountryByLangId(HttpServletRequest request,@RequestBody DtoHrState dtoHrState) {
		ResponseMessage responseMessage = null;
		List<DtoHrState> cityList = serviceHrState.getStateByCountryByLangId(dtoHrState);
		responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
				serviceResponse.getMessageByShortAndIsDeleted("STATE_GET_ALL", false), cityList);
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCityList(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		
			dtoSearch = this.serviceHrState.searchState(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted("LOCATION_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("LOCATION_LIST_NOT_GETTING", false),dtoSearch);
		} 
		return responseMessage;
	}
	
	@RequestMapping(value = "/getCityListByCountryAndLangId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getStateListBaseOnCountry(@RequestBody DtoHrState dtoHrState) {
		ResponseMessage responseMessage = null;
		DtoSearch dtoSearch = new DtoSearch();
		
		List<DtoHrState>	dtoHrState1 = this.serviceHrState.getStateListBaseOnCountry(dtoHrState);
		dtoSearch.setRecords(dtoHrState1);
			if (dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted("LOCATION_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("LOCATION_LIST_NOT_GETTING", false),dtoSearch);
		} 
		return responseMessage;
	}
}
