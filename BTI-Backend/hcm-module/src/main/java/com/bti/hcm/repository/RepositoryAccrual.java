package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Accrual;

@Repository("repositoryAccrual")
public interface RepositoryAccrual extends JpaRepository<Accrual, Integer>{

	Accrual findByIdAndIsDeleted(Integer id, boolean b);
	
	@Query("select count(*) from Accrual a where a.isDeleted=false")
	Integer getCountOfTotalAccrual();

	List<Accrual> findByIsDeleted(boolean b, Pageable pageable);

	List<Accrual> findByIsDeletedOrderByCreatedDateDesc(boolean b);



	@Query("select a  from Accrual a where (a.arbic like :searchKeyWord or a.desc like :searchKeyWord or a.accuralId like :searchKeyWord) and a.isDeleted=false")
	List<Accrual> predictiveAccrualSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select a from Accrual a where (a.arbic like :searchKeyWord or a.desc like :searchKeyWord or a.accuralId like :searchKeyWord) and a.isDeleted=false")
	List<Accrual> predictiveAccrualSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Accrual a set a.isDeleted =:deleted ,a.updatedBy =:updateById where a.id =:id ")
	void deleteSingleAccrualCode(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select a from Accrual a where (a.accuralId =:accuralId) and a.isDeleted=false")
	List<Accrual> findByAccruaCodeId(@Param("accuralId") String accrualId);

	@Query("select count(*) from Accrual a where (a.accuralId LIKE :searchKeyWord or a.desc LIKE :searchKeyWord or a.arbic LIKE :searchKeyWord) and a.isDeleted=false")
	Integer predictiveAccrualSearchTotalCount(@Param("searchKeyWord")  String searchKeyWord);

	@Query("select a from Accrual a where (a.accuralId =:accuralId) and a.isDeleted=false")
	List<Accrual> findByAccruaId(@Param("accuralId")String accuralId);

	@Query("select a from Accrual a where (a.arbic like :searchKeyWord or a.desc like :searchKeyWord) and a.isDeleted=false")
	List<Accrual> getAllAccrualSetupId(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select a  from Accrual a where (a.accuralId like :searchKeyWord) and a.isDeleted=false")
	List<Accrual> searchAccrualId(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select count(*) from Accrual a ")
	public Integer getCountOfTotalAccruals();

}
