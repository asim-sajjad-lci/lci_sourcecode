package com.bti.hcm.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.ManualChecks;
import com.bti.hcm.model.dto.DtoManualChecks;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryManualChecks;
import com.bti.hcm.repository.RepositoryManualChecksDetails;
import com.bti.hcm.repository.RepositoryManualChecksDistribution;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceManualChecks")
public class ServiceManualChecks {

	static Logger log = Logger.getLogger(ServiceLocation.class.getName());
	
	@Autowired
	RepositoryManualChecksDistribution repositoryManualChecksDistribution;
	
	@Autowired
	RepositoryManualChecks repositoryManualChecks;
			
	@Autowired
	RepositoryManualChecksDetails repositoryManualChecksDetails;  
	
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	public DtoManualChecks saveOrUpdate(DtoManualChecks dtoManualChecks) {
		
		try {
			  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			  
			  ManualChecks manualChecks=null;
			  if(dtoManualChecks.getId()!=null && dtoManualChecks.getId()>0) {
				  manualChecks=repositoryManualChecks.findByIdAndIsDeleted(dtoManualChecks.getId(),false);
				  manualChecks.setUpdatedBy(loggedInUserId);
				  manualChecks.setUpdatedDate(new Date());
			  }else {
				  manualChecks=new ManualChecks();
				  manualChecks.setCreatedDate(new Date());
				  Integer rowId = repositoryManualChecks.getCountOfTotaLocation();
    				Integer increment=0;
    				if(rowId!=0) {
    					increment= rowId+1;
    				}else {
    					increment=1;
    				}
    				manualChecks.setRowId(increment);
    				
    				EmployeeMaster employeeMaster=null;
    				if(dtoManualChecks.getDtoEmployeeMasterHcm()!=null && dtoManualChecks.getDtoEmployeeMasterHcm().getEmployeeIndexId()>0) {
    					employeeMaster=repositoryEmployeeMaster.findOne(dtoManualChecks.getDtoEmployeeMasterHcm().getEmployeeIndexId());
    				}
    				manualChecks.setEmployeeMaster(employeeMaster);
    				manualChecks.setCheckDate(dtoManualChecks.getCheckDate());
    				manualChecks.setCheckType(dtoManualChecks.getCheckType());
    				manualChecks.setDescription(dtoManualChecks.getDescription());
    				manualChecks.setCheckNumber(dtoManualChecks.getCheckNumber());
    				manualChecks.setPostDate(dtoManualChecks.getPostDate());
    				manualChecks.setGrossAmount(dtoManualChecks.getGrossAmount());
    				manualChecks.setNetAmount(dtoManualChecks.getNetAmount());
    				manualChecks.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
    				repositoryManualChecks.saveAndFlush(manualChecks);
			  }
			
		} catch (Exception e) {
			log.error(e);

		}
		
		return dtoManualChecks;
	}

	
}
