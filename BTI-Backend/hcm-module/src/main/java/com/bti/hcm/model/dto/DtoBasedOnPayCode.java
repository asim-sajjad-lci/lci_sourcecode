package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

public class DtoBasedOnPayCode {

	private Integer id;
	private Integer payCodeId;
	private Integer employeeId;
	private String baseOnPayCode;
	private BigDecimal amount;
	private BigDecimal payFactory;
	private BigDecimal payRate;
	private Integer totalCount;
	private Short unitOfPay;
	private Object records;
	private Integer basePayCodeId;
	private String employeeFirstName;
	private List<DtoPayCode> dtoPaycodeList;
	private DtoPayCode dtoPayCode;
	protected Boolean isRepeat;
	
	private List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenance;

	public Integer getPayCodeId() {
		return payCodeId;
	}

	public void setPayCodeId(Integer payCodeId) {
		this.payCodeId = payCodeId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getBaseOnPayCode() {
		return baseOnPayCode;
	}

	public void setBaseOnPayCode(String baseOnPayCode) {
		this.baseOnPayCode = baseOnPayCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPayFactory() {
		return payFactory;
	}

	public void setPayFactory(BigDecimal payFactory) {
		this.payFactory = payFactory;
	}

	public BigDecimal getPayRate() {
		return payRate;
	}

	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}

	public Short getUnitOfPay() {
		return unitOfPay;
	}

	public void setUnitOfPay(Short unitOfPay) {
		this.unitOfPay = unitOfPay;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<DtoEmployeePayCodeMaintenance> getDtoEmployeePayCodeMaintenance() {
		return dtoEmployeePayCodeMaintenance;
	}

	public void setDtoEmployeePayCodeMaintenance(List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenance) {
		this.dtoEmployeePayCodeMaintenance = dtoEmployeePayCodeMaintenance;
	}

	public Object getRecords() {
		return records;
	}

	public void setRecords(Object records) {
		this.records = records;
	}

	public Integer getBasePayCodeId() {
		return basePayCodeId;
	}

	public void setBasePayCodeId(Integer basePayCodeId) {
		this.basePayCodeId = basePayCodeId;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public List<DtoPayCode> getDtoPaycodeList() {
		return dtoPaycodeList;
	}

	public void setDtoPaycodeList(List<DtoPayCode> dtoPaycodeList) {
		this.dtoPaycodeList = dtoPaycodeList;
	}

	public DtoPayCode getDtoPayCode() {
		return dtoPayCode;
	}

	public void setDtoPayCode(DtoPayCode dtoPayCode) {
		this.dtoPayCode = dtoPayCode;
	}

	public Boolean getIsRepeat() {
		return isRepeat;
	}

	public void setIsRepeat(Boolean isRepeat) {
		this.isRepeat = isRepeat;
	}

}
