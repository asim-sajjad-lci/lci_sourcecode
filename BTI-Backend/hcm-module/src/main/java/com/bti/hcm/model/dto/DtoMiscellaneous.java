package com.bti.hcm.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoMiscellaneous extends DtoBase{
	
	
	private int id;
	private String codeId;
	private String codeName;
	private String codeArabicName;
	private boolean dimension;
	private List<DtoMiscellaneous> deleteMiscellaneous;
	private List<DtoValues> dtoValuesList;
	private int masterId;
	private int valueIndexId;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCodeId() {
		return codeId;
	}
	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getCodeArabicName() {
		return codeArabicName;
	}
	public void setCodeArabicName(String codeArabicName) {
		this.codeArabicName = codeArabicName;
	}
	public boolean isDimension() {
		return dimension;
	}
	public void setDimension(boolean dimension) {
		this.dimension = dimension;
	}
	public List<DtoMiscellaneous> getDeleteMiscellaneous() {
		return deleteMiscellaneous;
	}
	public void setDeleteMiscellaneous(List<DtoMiscellaneous> deleteMiscellaneous) {
		this.deleteMiscellaneous = deleteMiscellaneous;
	}
	public List<DtoValues> getDtoValuesList() {
		return dtoValuesList;
	}
	public void setDtoValuesList(List<DtoValues> dtoValues) {
		this.dtoValuesList = dtoValues;
	}
	public int getMasterId() {
		return masterId;
	}
	public void setMasterId(int masterId) {
		this.masterId = masterId;
	}
	public int getValueIndexId() {
		return valueIndexId;
	}
	public void setValueIndexId(int valueIndexId) {
		this.valueIndexId = valueIndexId;
	}

	
	

}
