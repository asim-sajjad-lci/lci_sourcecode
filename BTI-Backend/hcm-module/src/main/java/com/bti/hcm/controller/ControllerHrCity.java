/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoCity;
import com.bti.hcm.service.ServiceHrCity;
import com.bti.hcm.service.ServiceResponse;
/**
 * Description: ControllerHrCity
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/hrCity")
public class ControllerHrCity {
	

	/**
	 * @Description serviceHrCity Autowired here using annotation of spring for use of serviceHrCity method in this controller
	 */
	@Autowired
	ServiceHrCity serviceHrCity;
	
	
	/**
	 * @Description serviceDivision Autowired here using annotation of spring for use of serviceDivision method in this controller
	 */

	/**
	 * @Description sessionManager Autowired here using annotation of spring for use of sessionManager method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;
	
	
	/**
	 * @Description Get All CityName in Dropdown list
 	 * @param request
	 * @return
	 * @response{
				"code": 201,
				"status": "CREATED",
				"result": [
				  {
				"cityId": 1,
				"cityName": "Ahmedabad"
				},
				  {
				"cityId": 2,
				"cityName": "Ananad"
				},
				  {
				"cityId": 3,
				"cityName": "Surat"
				}
			],
			"btiMessage": {
				"message": "City list fetched successfully",
				"messageShort": "CITY_GET_ALL"
			}
	 */
	@RequestMapping(value = "/getCityList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCityList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		List<DtoCity> cityList = serviceHrCity.getCityListWithLanguage();
		responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
				serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.CITY_GET_ALL, false), cityList);
		return responseMessage;
	}
	
	@RequestMapping(value = "/getCityByState", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getStateByCountry(HttpServletRequest request,@RequestBody DtoCity dtoCity) {
		ResponseMessage responseMessage = null;
		List<DtoCity> cityList = serviceHrCity.getCityListBaseOnState(dtoCity);
		responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
				serviceResponse.getMessageByShortAndIsDeleted("CITY_GET_ALL", false), cityList);
		return responseMessage;
	}
	
	@RequestMapping(value = "/getCityByStateAndLangId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getCityByStateAndLangId(HttpServletRequest request,@RequestBody DtoCity dtoCity) {
		ResponseMessage responseMessage = null;
		List<DtoCity> cityList = serviceHrCity.getCityByStateAndLangId(dtoCity);
		responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
				serviceResponse.getMessageByShortAndIsDeleted("CITY_GET_ALL", false), cityList);
		return responseMessage;
	}

}
