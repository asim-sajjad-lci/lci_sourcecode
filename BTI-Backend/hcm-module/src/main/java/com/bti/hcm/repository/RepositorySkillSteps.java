package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.SkillsSetup;

/**
 * Description: Interface for SkillSteps 
 * Name of Project: Hcm
 * Version: 0.0.1
 * @author Gaurav
 *
 */
@Repository("RepositorySkillSteps")
public interface RepositorySkillSteps extends JpaRepository<SkillsSetup, Integer>{

	SkillsSetup findByIdAndIsDeleted(Integer id, boolean b);

	/**
	 * @return
	 */
	@Query("select count(*) from SkillsSetup s where s.isDeleted=false")
	Integer getCountOfTotalSkillSetup();

	/**
	 * @param b
	 * @param pageable
	 * @return
	 */
	List<SkillsSetup> findByIsDeleted(boolean b, Pageable pageable);

	/**
	 * @param b
	 * @return
	 */
	List<SkillsSetup> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	
	/**
	 * @param b
	 * @param loggedInUserId
	 * @param skillStepId
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update SkillsSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleSkillSteup(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer skillStepId);

	
	
	/**
	 * @param string
	 * @return
	 */
	@Query("select count(*) from SkillsSetup d where (d.skillId like :searchKeyWord  or d.skillDesc like :searchKeyWord  or d.arabicDesc like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveSkillSteupSearchTotalCount(@Param("searchKeyWord")String string);

	
	/**
	 * @param skillId
	 * @return
	 */
	@Query("select p from SkillsSetup p where (p.skillId =:skillId) and p.isDeleted=false")
	List<SkillsSetup> findByskillId(@Param("skillId")String skillId);

	
	/**
	 * @param string
	 * @param pageRequest 
	 * @param pageRequest
	 * @return
	 */	
	
	@Query("select d from SkillsSetup d where (d.skillId like :searchKeyWord  or d.skillDesc like  :searchKeyWord  or d.arabicDesc like :searchKeyWord) and d.isDeleted=false")
	List<SkillsSetup> predictiveSkillsSetupSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Query("select d from SkillsSetup d where (d.skillId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<SkillsSetup> predictiveSkillSetupIdSearchWithPagination(@Param("searchKeyWord")String string);

	List<SkillsSetup> findByIsDeleted(boolean b);
	
	
	@Query("select d from SkillsSetup d where (d.skillId like :searchKeyWord) and d.isDeleted=false")
	List<SkillsSetup> searchIds(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Query("select d from SkillsSetup d where  d.id in (:ids) and d.isDeleted=false")
	List<SkillsSetup> findByIds(@Param("ids")List<Integer> ids);

	@Query("select d from SkillsSetup d where (d.skillId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<SkillsSetup> predictiveSearchAllSkillIdWithPagination(@Param("searchKeyWord")String skillId);

	@Query("select count(*) from SkillsSetup s ")
	public Integer getCountOfTotalSkillSteps();
	
	
	
	@Query("select d from SkillsSetup d where d.id in (:list) and d.isDeleted=false")
	public List<SkillsSetup> findAllPayCodeListId(@Param("list") List<Integer> ls);
	
	
	


}
