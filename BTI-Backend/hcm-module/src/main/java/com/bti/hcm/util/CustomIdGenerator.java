package com.bti.hcm.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bti.hcm.repository.RepositoryDimensions;

@Component
public class CustomIdGenerator {

	@Autowired
	RepositoryDimensions repositoryDimension;

	public Integer generateCustomId(Integer id) {

		Integer generatedCode = null;
		Integer codeId = 5001;

		List<Integer> dimensionIds = repositoryDimension.findById2();

		for (Integer ids : dimensionIds) {
			if (ids >= 5001 && ids <= 6000) {
				generatedCode = ids + 1;
			} else {
				generatedCode = codeId;
			}
		}

		return generatedCode;

	}

}
