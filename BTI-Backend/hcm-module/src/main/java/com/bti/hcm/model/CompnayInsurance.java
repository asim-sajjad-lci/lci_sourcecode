package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40321",indexes = {
        @Index(columnList = "INSCOMINDX")
})
public class CompnayInsurance extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INSCOMINDX")
	private Integer id;
	
	@Column(name = "INSCOMID",columnDefinition = "char(15)")
	private String insCompanyId;
	
	@Column(name = "INSCOMDSCR",columnDefinition = "char(31)")
	private String desc;
	
	@Column(name = "INSCOMDSCRA",columnDefinition = "char(61)")
	private String arbicDesc;
	
	@Column(name = "INSCOMADRSS",columnDefinition = "char(61)")
	private String address;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "helthInsurance")
	//@LazyCollection (LazyCollectionOption.FALSE)
	private List<RetirementPlanSetup> listRetirementPlanSetup;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="compnayInsurance")
	private List<HelthInsurance>helthInsuranceList;
	
	@ManyToOne
	@JoinColumn(name="INSCOMCOUNTRY")
	private HrCountry country;
	
	@ManyToOne
	@JoinColumn(name="INSCOMSTATE")
	private HrState state;
	
	@ManyToOne
	@JoinColumn(name="INSCOMCITY")
	private HrCity city;
	
	@Column(name = "INSCOMPHBU",columnDefinition = "char(21)")
	private String phoneNo;
	
	@Column(name = "INSCOMFAX",columnDefinition = "char(21)")
	private String fax;
	
	@Column(name = "INSCOMCONT",columnDefinition = "char(91)")
	private String contactPerson;
	
	@Column(name = "INSCOMEMAL",columnDefinition = "char(225)")
	private String email;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInsCompanyId() {
		return insCompanyId;
	}

	public void setInsCompanyId(String insCompanyId) {
		this.insCompanyId = insCompanyId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	

	public HrCountry getCountry() {
		return country;
	}

	public void setCountry(HrCountry country) {
		this.country = country;
	}

	public HrState getState() {
		return state;
	}

	public void setState(HrState state) {
		this.state = state;
	}

	public HrCity getCity() {
		return city;
	}

	public void setCity(HrCity city) {
		this.city = city;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<RetirementPlanSetup> getListRetirementPlanSetup() {
		return listRetirementPlanSetup;
	}

	public void setListRetirementPlanSetup(List<RetirementPlanSetup> listRetirementPlanSetup) {
		this.listRetirementPlanSetup = listRetirementPlanSetup;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((arbicDesc == null) ? 0 : arbicDesc.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((contactPerson == null) ? 0 : contactPerson.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((insCompanyId == null) ? 0 : insCompanyId.hashCode());
		result = prime * result + ((listRetirementPlanSetup == null) ? 0 : listRetirementPlanSetup.hashCode());
		result = prime * result + ((phoneNo == null) ? 0 : phoneNo.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

	public List<HelthInsurance> getHelthInsuranceList() {
		return helthInsuranceList;
	}

	public void setHelthInsuranceList(List<HelthInsurance> helthInsuranceList) {
		this.helthInsuranceList = helthInsuranceList;
	}
}
