/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.AtteandaceOptionType;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO AtteandaceOptionType class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoAttedanceOptionType extends DtoBase{

	private Integer id;
	private Integer seqn;
	private Integer reasonIndx;
	private String reasonDesc;
	private Integer atteandanceTypeSeqn;
	private Integer atteandanceTypeIndx;
	private String atteandanceTypeDesc;
	private List<DtoAttedanceOptionType> delete;
	public Integer getSeqn() {
		return seqn;
	}
	public void setSeqn(Integer seqn) {
		this.seqn = seqn;
	}
	public Integer getReasonIndx() {
		return reasonIndx;
	}
	public void setReasonIndx(Integer reasonIndx) {
		this.reasonIndx = reasonIndx;
	}
	public String getReasonDesc() {
		return reasonDesc;
	}
	public void setReasonDesc(String reasonDesc) {
		this.reasonDesc = reasonDesc;
	}
	public Integer getAtteandanceTypeSeqn() {
		return atteandanceTypeSeqn;
	}
	public void setAtteandanceTypeSeqn(Integer atteandanceTypeSeqn) {
		this.atteandanceTypeSeqn = atteandanceTypeSeqn;
	}
	public Integer getAtteandanceTypeIndx() {
		return atteandanceTypeIndx;
	}
	public void setAtteandanceTypeIndx(Integer atteandanceTypeIndx) {
		this.atteandanceTypeIndx = atteandanceTypeIndx;
	}
	public String getAtteandanceTypeDesc() {
		return atteandanceTypeDesc;
	}
	public void setAtteandanceTypeDesc(String atteandanceTypeDesc) {
		this.atteandanceTypeDesc = atteandanceTypeDesc;
	}
	
	public List<DtoAttedanceOptionType> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoAttedanceOptionType> delete) {
		this.delete = delete;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public DtoAttedanceOptionType(AtteandaceOptionType atteandaceOptionType) {
		
		
	}
public DtoAttedanceOptionType() {
		
		
	}
	

}
