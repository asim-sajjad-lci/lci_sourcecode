package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoHealthInsuranceEnrollment;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceHealthInsuranceEnrollment;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/healthInsuranceEnrollment")
public class ControllerHealthInsuranceEnrollment extends BaseController{

	

	private static final Logger LOGGER = Logger.getLogger(ControllerHealthInsuranceEnrollment.class);
	
	@Autowired
	ServiceHealthInsuranceEnrollment serviceHealthInsuranceEnrollment;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment) throws Exception {
		LOGGER.info("Create HealthInsuranceEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHealthInsuranceEnrollment  = serviceHealthInsuranceEnrollment.saveOrUpdate(dtoHealthInsuranceEnrollment);
			responseMessage=displayMessage(dtoHealthInsuranceEnrollment, "HEALTH_INSURANCE_ENROLLMENT_CREATED", "HEALTH_INSURANCE_ENROLLMENT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create HealthInsuranceEnrollment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment) throws Exception {
		LOGGER.info("Update HealthInsuranceEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoHealthInsuranceEnrollment = serviceHealthInsuranceEnrollment.saveOrUpdate(dtoHealthInsuranceEnrollment);
			responseMessage=displayMessage(dtoHealthInsuranceEnrollment, "HEALTH_INSURANCE_ENROLLMENT_UPDATED", "HEALTH_INSURANCE_ENROLLMENT_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update HealthInsuranceEnrollment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment) throws Exception {
		LOGGER.info("Delete HealthInsuranceEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoHealthInsuranceEnrollment.getIds() != null && !dtoHealthInsuranceEnrollment.getIds().isEmpty()) {
				DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment2 = serviceHealthInsuranceEnrollment.delete(dtoHealthInsuranceEnrollment.getIds());
				if(dtoHealthInsuranceEnrollment2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("HEALTH_INSURANCE_ENROLLMENT_DELETED", false), dtoHealthInsuranceEnrollment2);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("HEALTH_INSURANCE_ENROLLMENT_NOT_DELETED", false), dtoHealthInsuranceEnrollment2);
					
				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("HEALTH_INSURANCE_ENROLLMENT_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete HealthInsuranceEnrollment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollmentObj = serviceHealthInsuranceEnrollment.getById(dtoHealthInsuranceEnrollment.getId());
			responseMessage=displayMessage(dtoHealthInsuranceEnrollmentObj, "HEALTH_INSURANCE_ENROLLMENT_LIST_GET_DETAIL", "HEALTH_INSURANCE_ENROLLMENT_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoHealthInsuranceEnrollment.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search HealthInsuranceEnrollment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceHealthInsuranceEnrollment.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "HEALTH_INSURANCE_ENROLLMENT_GET_ALL", "HEALTH_INSURANCE_ENROLLMENT_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
					
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search HealthInsuranceEnrollment Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	/*@RequestMapping(value = "/employeeIdcheck", method = RequestMethod.POST)
	public ResponseMessage employeeIDCheck(HttpServletRequest request, @RequestBody DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment) throws Exception {
		LOGGER.info("PayCodeIdCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollments = serviceHealthInsuranceEnrollment.repeatByEmployeeId(dtoHealthInsuranceEnrollment.getEmployeeMaster().getEmployeeIndexId());
			responseMessage=displayMessage(dtoHealthInsuranceEnrollments, "EMPLOYEES_ID_MAINTAIN_EXIST", "HEALTH_INSURANCE_ENROLLMENT_REPEAT_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}*/
	
	
	
	

	@RequestMapping(value = "/employeeIdHealthInsuranceIdcheck", method = RequestMethod.POST)
	public ResponseMessage helthInsurancecheck(HttpServletRequest request, @RequestBody DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollment)
			throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoHealthInsuranceEnrollment dtoHealthInsuranceEnrollmentObj = serviceHealthInsuranceEnrollment.repeatByHelthInsuranceId(dtoHealthInsuranceEnrollment);
			
			 if (dtoHealthInsuranceEnrollmentObj !=null && dtoHealthInsuranceEnrollmentObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                		 serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEES_ID_MAINTAIN_EXIST", false), dtoHealthInsuranceEnrollmentObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
                		 serviceResponse.getMessageByShortAndIsDeleted("HEALTH_INSURANCE_ENROLLMENT_REPEAT_NOT_FOUND", false),
                         dtoHealthInsuranceEnrollmentObj);
             }
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	
	
	
	
}
