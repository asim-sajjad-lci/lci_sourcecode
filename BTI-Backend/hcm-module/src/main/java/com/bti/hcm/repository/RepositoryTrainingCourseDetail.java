package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TrainingCourseDetail;
import com.bti.hcm.model.TraningCourse;


@Repository("repositoryTrainingCourseDetail")
public interface RepositoryTrainingCourseDetail extends JpaRepository<TrainingCourseDetail, Integer>{

	TrainingCourseDetail findByIdAndIsDeleted(int id, boolean b);

	
	@Query("select t from TrainingCourseDetail t where (t.classId =:classId) and t.isDeleted=false")
	List<TrainingCourseDetail> findByTrainingCourseDetailClassId(@Param("classId")String classId);

	
	
	
	@Query("select t from TrainingCourseDetail t where (t.classId LIKE :searchKeyWord or t.className LIKE :searchKeyWord  or t.instructorName LIKE :searchKeyWord or "
			+ " t.classLocation LIKE :searchKeyWord) and t.isDeleted=false")
	List<TraningCourse> predictiveTrainingCourseDetailSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	
	
	@Query("select count(*) from TrainingCourseDetail t where (t.classId LIKE :searchKeyWord or t.className LIKE :searchKeyWord or t.instructorName LIKE :searchKeyWord or "
			+ " t.classLocation Like :searchKeyWord) and t.isDeleted=false")
	Integer predictiveTrainingCourseDetailTotalCount(@Param("searchKeyWord")String searchKeyWord);

	

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TrainingCourseDetail t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	public void deleteSingleTraningCourseDetail(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	TrainingCourseDetail findOne(Integer id);
	
	
	List<TrainingCourseDetail> findByIsDeleted(boolean b);


	@Query("select t from TrainingCourseDetail t where (t.classId LIKE :searchKeyWord) and t.isDeleted=false")
	List<TrainingCourseDetail> getAll(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);


	@Query("select t from TrainingCourseDetail t where (t.trainingCourse.id =:id) and t.isDeleted=false")
	List<TrainingCourseDetail> findByTrainingCourseAndIsDeleted(@Param("id")Integer id);

	@Query("select count(*) from TrainingCourseDetail t ")
	public Integer getCountOfTotalTrainingCourseDetail();
	
}
