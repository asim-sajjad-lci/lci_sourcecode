package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.HelthCoverageType;
import com.bti.hcm.util.UtilRandomKey;

public class DtoHelthCoverageType extends DtoBase {

	private Integer id;
	private String helthCoverageId;
	private String desc;
	private String arbicDesc;
	private List<DtoHelthCoverageType> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHelthCoverageId() {
		return helthCoverageId;
	}

	public void setHelthCoverageId(String helthCoverageId) {
		this.helthCoverageId = helthCoverageId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public List<DtoHelthCoverageType> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoHelthCoverageType> delete) {
		this.delete = delete;
	}

	public DtoHelthCoverageType() {
	}

	public DtoHelthCoverageType(HelthCoverageType coverageType) {
		this.helthCoverageId = coverageType.getHelthCoverageId();

		if (UtilRandomKey.isNotBlank(coverageType.getDesc())) {
			this.desc = coverageType.getDesc();
		} else {
			this.desc = "";
		}

		if (UtilRandomKey.isNotBlank(coverageType.getArbicDesc())) {
			this.arbicDesc = coverageType.getArbicDesc();
		} else {
			this.arbicDesc = "";
		}
	}
}
