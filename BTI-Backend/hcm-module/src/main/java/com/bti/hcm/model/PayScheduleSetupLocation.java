/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the PayScheduleSetupLocation database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40915",indexes = {
        @Index(columnList = "PYSCHDINDXL")
})
@NamedQuery(name = "PayScheduleSetupLocation.findAll", query = "SELECT p FROM PayScheduleSetupLocation p")
public class PayScheduleSetupLocation extends HcmBaseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PYSCHDINDXL")
	private int payScheduleSetupLocationId;
	
	
	@Column(name = "LOCDNMA",columnDefinition="char(31)")
	private String locationName;
	
	
	@Column(name = "PYSCHDACT")
	private Boolean payScheduleStatus;
	
	
	@ManyToOne
	@JoinColumn(name="LOCINDX")
	private Location location;
	
	@ManyToOne
	@JoinColumn(name="PYSCHDINDX")
	private PayScheduleSetup payScheduleSetup;

	public int getPayScheduleSetupLocationId() {
		return payScheduleSetupLocationId;
	}

	public void setPayScheduleSetupLocationId(int payScheduleSetupLocationId) {
		this.payScheduleSetupLocationId = payScheduleSetupLocationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Boolean getPayScheduleStatus() {
		return payScheduleStatus;
	}

	public void setPayScheduleStatus(Boolean payScheduleStatus) {
		this.payScheduleStatus = payScheduleStatus;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public PayScheduleSetup getPayScheduleSetup() {
		return payScheduleSetup;
	}

	public void setPayScheduleSetup(PayScheduleSetup payScheduleSetup) {
		this.payScheduleSetup = payScheduleSetup;
	}
	
	
	
	

	


}
