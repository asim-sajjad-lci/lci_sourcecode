package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoHrCountry;
import com.bti.hcm.service.ServiceHrCountry;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/hrCountry")
public class ControllerHrCountry {

	/**
	 * @Description serviceHrCity Autowired here using annotation of spring for use of serviceHrCity method in this controller
	 */
	@Autowired
	ServiceHrCountry serviceHrCountry;
	
	


	/**
	 * @Description sessionManager Autowired here using annotation of spring for use of sessionManager method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;
	
	@RequestMapping(value = "/getCountryList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCityList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		List<DtoHrCountry> cityList = serviceHrCountry.getCountryListWithLanguage();
		responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
				serviceResponse.getMessageByShortAndIsDeleted("COUNTRY_GET_ALL", false), cityList);
		return responseMessage;
	}
}
