package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.OrientationSetup;


@Repository("repositoryOrientationSetup")
public interface RepositoryOrientationSetup extends JpaRepository<OrientationSetup, Integer>{

	
	
	
	OrientationSetup findByIdAndIsDeleted(Integer id, boolean b);

	OrientationSetup saveAndFlush(OrientationSetup orientationSetup);

	OrientationSetup findOne(Integer id);

	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update OrientationSetup o set o.isDeleted =:deleted ,o.updatedBy =:updateById where o.id =:id ")
	public void deleteSingleOrientationSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select O from OrientationSetup O where (O.orientationId =:orientationId) and O.isDeleted=false")
	List<OrientationSetup> repeatByorientationId(@Param("orientationId")String orientationId);

	
	
	@Query("select count(*) from OrientationSetup o where (o.orientationId LIKE :searchKeyWord or o.orientationDesc LIKE :searchKeyWord  or o.orientationArbicDesc LIKE :searchKeyWord) and o.isDeleted=false")
	Integer predictiveOrientationSetupSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);

	
	@Query("select o from OrientationSetup o where (o.orientationId LIKE :searchKeyWord or o.orientationDesc LIKE :searchKeyWord or o.orientationArbicDesc LIKE :searchKeyWord) and o.isDeleted=false")
	List<OrientationSetup> predictiveOrientationSetupSearchWithPagination(@Param("searchKeyWord")String searchKeyWord, Pageable pageable);

	@Query("select o.orientationId from OrientationSetup o where (o.orientationId like :searchKeyWord) and o.isDeleted=false order by o.id desc")
	List<String> predictiveSearchOrientationIdSearchWithPagination(@Param("searchKeyWord")String string);

	@Query("select count(*) from OrientationSetup o ")
	Integer getCountOfTotaOrientationSetup();

}
