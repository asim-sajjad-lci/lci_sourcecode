
package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoJoinData {

	int accountRowId;
	String tableName;
	int tableIndx;
	int indexId;
	int segmentNumber;
	String columnName;
	int code;
	String endDesc;
	String arabicDesc;

	public int getAccountRowId() {
		return accountRowId;
	}

	public void setAccountRowId(int accountRowId) {
		this.accountRowId = accountRowId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getTableIndx() {
		return tableIndx;
	}

	public void setTableIndx(int tableIndx) {
		this.tableIndx = tableIndx;
	}

	public int getIndexId() {
		return indexId;
	}

	public void setIndexId(int indexId) {
		this.indexId = indexId;
	}

	public int getSegmentNumber() {
		return segmentNumber;
	}

	public void setSegmentNumber(int segmentNumber) {
		this.segmentNumber = segmentNumber;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getEndDesc() {
		return endDesc;
	}

	public void setEndDesc(String endDesc) {
		this.endDesc = endDesc;
	}

	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

}
