package com.bti.hcm.model.dto;

import java.math.BigDecimal;

public class DtoPayrollTransactionHistoryByDepartment extends DtoBase{
	private Integer id;
	private Integer year;
	private Short codeType;
	private Integer codeIndexId;
	private BigDecimal totalAmountPeriod1;
	private BigDecimal totalAmountPeriod2;
	private BigDecimal totalAmountPeriod3;
	private BigDecimal totalAmountPeriod4;
	private BigDecimal totalAmountPeriod5;
	private BigDecimal totalAmountPeriod6;
	private BigDecimal totalAmountPeriod7;
	private BigDecimal totalAmountPeriod8;
	private BigDecimal totalAmountPeriod9;
	private BigDecimal totalAmountPeriod10;
	private BigDecimal totalAmountPeriod11;
	private BigDecimal totalAmountPeriod12;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Short getCodeType() {
		return codeType;
	}
	public void setCodeType(Short codeType) {
		this.codeType = codeType;
	}
	public Integer getCodeIndexId() {
		return codeIndexId;
	}
	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}
	public BigDecimal getTotalAmountPeriod1() {
		return totalAmountPeriod1;
	}
	public void setTotalAmountPeriod1(BigDecimal totalAmountPeriod1) {
		this.totalAmountPeriod1 = totalAmountPeriod1;
	}
	public BigDecimal getTotalAmountPeriod2() {
		return totalAmountPeriod2;
	}
	public void setTotalAmountPeriod2(BigDecimal totalAmountPeriod2) {
		this.totalAmountPeriod2 = totalAmountPeriod2;
	}
	public BigDecimal getTotalAmountPeriod3() {
		return totalAmountPeriod3;
	}
	public void setTotalAmountPeriod3(BigDecimal totalAmountPeriod3) {
		this.totalAmountPeriod3 = totalAmountPeriod3;
	}
	public BigDecimal getTotalAmountPeriod4() {
		return totalAmountPeriod4;
	}
	public void setTotalAmountPeriod4(BigDecimal totalAmountPeriod4) {
		this.totalAmountPeriod4 = totalAmountPeriod4;
	}
	public BigDecimal getTotalAmountPeriod5() {
		return totalAmountPeriod5;
	}
	public void setTotalAmountPeriod5(BigDecimal totalAmountPeriod5) {
		this.totalAmountPeriod5 = totalAmountPeriod5;
	}
	public BigDecimal getTotalAmountPeriod6() {
		return totalAmountPeriod6;
	}
	public void setTotalAmountPeriod6(BigDecimal totalAmountPeriod6) {
		this.totalAmountPeriod6 = totalAmountPeriod6;
	}
	public BigDecimal getTotalAmountPeriod7() {
		return totalAmountPeriod7;
	}
	public void setTotalAmountPeriod7(BigDecimal totalAmountPeriod7) {
		this.totalAmountPeriod7 = totalAmountPeriod7;
	}
	public BigDecimal getTotalAmountPeriod8() {
		return totalAmountPeriod8;
	}
	public void setTotalAmountPeriod8(BigDecimal totalAmountPeriod8) {
		this.totalAmountPeriod8 = totalAmountPeriod8;
	}
	public BigDecimal getTotalAmountPeriod9() {
		return totalAmountPeriod9;
	}
	public void setTotalAmountPeriod9(BigDecimal totalAmountPeriod9) {
		this.totalAmountPeriod9 = totalAmountPeriod9;
	}
	public BigDecimal getTotalAmountPeriod10() {
		return totalAmountPeriod10;
	}
	public void setTotalAmountPeriod10(BigDecimal totalAmountPeriod10) {
		this.totalAmountPeriod10 = totalAmountPeriod10;
	}
	public BigDecimal getTotalAmountPeriod11() {
		return totalAmountPeriod11;
	}
	public void setTotalAmountPeriod11(BigDecimal totalAmountPeriod11) {
		this.totalAmountPeriod11 = totalAmountPeriod11;
	}
	public BigDecimal getTotalAmountPeriod12() {
		return totalAmountPeriod12;
	}
	public void setTotalAmountPeriod12(BigDecimal totalAmountPeriod12) {
		this.totalAmountPeriod12 = totalAmountPeriod12;
	}


}
