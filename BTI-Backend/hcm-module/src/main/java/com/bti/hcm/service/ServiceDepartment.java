/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Department;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryActivityLog;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.util.UtilDateAndTimeHr;

/**
 * Description: Service Department Project: Hcm Version: 0.0.1
 */
@Service("serviceDepartment")
public class ServiceDepartment {

	/**
	 * @Description LOGGER use for put a logger in Department Service
	 */
	static Logger log = Logger.getLogger(ServiceDepartment.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in Department service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in Department service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in Department service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryDepartment Autowired here using annotation of spring
	 *              for access of repositoryDepartment method in Department service
	 *              In short Access Department Query from Database using
	 *              repositoryDepartment.
	 */
	@Autowired
	RepositoryDepartment repositoryDepartment;

	@Autowired
	RepositoryActivityLog repositoryActivityLog;

	@Autowired
	ServiceActivityLog serviceActivityLog;

	/**
	 * @Description: save and update department data
	 * @param dtoDepartment
	 * @return
	 * @throws ParseException
	 */
	public DtoDepartment saveOrUpdateDepartment(DtoDepartment dtoDepartment) {
		log.info("saveOrUpdateDepartment Method");
		Department department = null;
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if (dtoDepartment.getId() != null && dtoDepartment.getId() > 0) {
				department = repositoryDepartment.findByIdAndIsDeleted(dtoDepartment.getId(), false);
				department.setUpdatedBy(loggedInUserId);
				department.setUpdatedDate(new Date());
			} else {
				department = new Department();
				department.setCreatedDate(new Date());
				Integer rowId = repositoryDepartment.getCountOfTotalDepartments();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				department.setRowId(increment);
			}
			department.setDepartmentDescription(dtoDepartment.getDepartmentDescription());
			department.setDepartmentId(dtoDepartment.getDepartmentId());
			department.setArabicDepartmentDescription(dtoDepartment.getArabicDepartmentDescription());
			department.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryDepartment.saveAndFlush(department);
			log.debug("Department is:" + dtoDepartment.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDepartment;

	}

	/**
	 * @Description: get All Department
	 * @param dtoDepartment
	 * @return
	 */
	public DtoSearch getAllDepartment(DtoDepartment dtoDepartment) {
		log.info("getAllDepartment Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoDepartment.getPageNumber());
			dtoSearch.setPageSize(dtoDepartment.getPageSize());
			dtoSearch.setTotalCount(repositoryDepartment.getCountOfTotalDepartment());
			List<Department> departmentList = null;
			if (dtoDepartment.getPageNumber() != null && dtoDepartment.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoDepartment.getPageNumber(), dtoDepartment.getPageSize(),
						Direction.DESC, "createdDate");
				departmentList = repositoryDepartment.findByIsDeleted(false, pageable);
			} else {
				departmentList = repositoryDepartment.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			List<DtoDepartment> dtoDepartmentList = new ArrayList<>();
			if (departmentList != null && !departmentList.isEmpty()) {
				for (Department department : departmentList) {
					dtoDepartment = new DtoDepartment(department);
					dtoDepartment.setArabicDepartmentDescription(department.getArabicDepartmentDescription());
					dtoDepartment.setId(department.getId());
					dtoDepartmentList.add(dtoDepartment);
				}
				dtoSearch.setRecords(dtoDepartmentList);
			}
			log.debug("All Department List Size is:" + dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description: Search department data
	 * @param dtoSearch
	 * @return
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchDepartment(DtoSearch dtoSearch) {
		log.info("searchDepartment Method");
		try {
			if (dtoSearch != null) {

				String searchWord = dtoSearch.getSearchKeyword();
				dtoSearch = searchByDepartment(dtoSearch);
				dtoSearch.setTotalCount(
						this.repositoryDepartment.predictiveDepartmentSearchTotalCount("%" + searchWord + "%"));

				List<Department> departmentList = searchByPageSizeAndNumber(dtoSearch);

				if (departmentList != null && !departmentList.isEmpty()) {
					List<DtoDepartment> dtoDepartmentList = new ArrayList<>();
					DtoDepartment dtoDepartment = null;
					for (Department department : departmentList) {
						dtoDepartment = new DtoDepartment(department);
						dtoDepartment.setId(department.getId());
						dtoDepartment.setArabicDepartmentDescription(department.getArabicDepartmentDescription());
						dtoDepartmentList.add(dtoDepartment);
					}
					dtoSearch.setRecords(dtoDepartmentList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchByDepartment(DtoSearch dtoSearch) {

		String condition = "";

		if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
			switch (dtoSearch.getSortOn()) {
			case "departmentId":
				condition += dtoSearch.getSortOn();
				break;
			case "departmentDescription":
				condition += dtoSearch.getSortOn();
				break;
			case "arabicDepartmentDescription":
				condition += dtoSearch.getSortOn();
				break;
			case "gregorianDate":
				condition += dtoSearch.getSortOn();
				break;
			case "hijriDate":
				condition += dtoSearch.getSortOn();
				break;
			default:
				condition += "id";
			}
		} else {
			condition += "id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
		}
		dtoSearch.setCondition(condition);
		return dtoSearch;

	}

	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public List<Department> searchByPageSizeAndNumber(DtoSearch dtoSearch) {
		List<Department> departmentList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

			if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
				departmentList = this.repositoryDepartment.predictiveDepartmentSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%",
						new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if (dtoSearch.getSortBy().equals("ASC")) {
				departmentList = this.repositoryDepartment.predictiveDepartmentSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
			} else if (dtoSearch.getSortBy().equals("DESC")) {
				departmentList = this.repositoryDepartment.predictiveDepartmentSearchWithPagination(
						"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
			}

		}

		return departmentList;
	}

	/**
	 * @Description: get Department data by id
	 * @param departmentId
	 * @return
	 */
	public DtoDepartment getDepartmentByDepartmentId(int id) {
		log.info("getDepartmentByDepartmentId Method");
		DtoDepartment dtoDepartment = new DtoDepartment();
		try {
			if (id > 0) {
				Department department = repositoryDepartment.findByIdAndIsDeleted(id, false);
				if (department != null) {
					dtoDepartment = new DtoDepartment(department);
					dtoDepartment.setArabicDepartmentDescription(department.getArabicDepartmentDescription());
				} else {
					dtoDepartment.setMessageType("DEPARTMENT_NOT_GETTING");

				}
			} else {
				dtoDepartment.setMessageType("INVALID_DEPARTMENT_ID");

			}
			log.debug("Department By Id is:" + dtoDepartment.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDepartment;
	}

	/**
	 * @Description: delete department
	 * @param ids
	 * @return
	 */

	public DtoDepartment deleteDepartment(List<Integer> ids) {
		log.info("deleteDepartment Method");
		DtoDepartment dtoDepartment = new DtoDepartment();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			dtoDepartment
					.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DEPARTMENT_DELETED", false));
			dtoDepartment.setAssociateMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("DEPARTMENT_ASSOCIATED", false));
			boolean inValidDelete = false;
			StringBuilder invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse
					.getMessageByShortAndIsDeleted("DEPARTMENT_NOT_DELETE_ID_MESSAGE", false).getMessage());
			List<DtoDepartment> deleteDepartment = new ArrayList<>();

			for (Integer departmentId : ids) {
				Department department = repositoryDepartment.findOne(departmentId);
				if (department.getEmployeeMaster().isEmpty() && department.getListEmployeePositionHistory().isEmpty()
						&& department.getRequisitions().isEmpty()
						&& department.getPayScheduleSetupDepartment().isEmpty()) {
					DtoDepartment dtoDepartment2 = new DtoDepartment();
					dtoDepartment2.setId(departmentId);
					dtoDepartment2.setDepartmentDescription(department.getDepartmentDescription());
					repositoryDepartment.deleteSingleDepartment(true, loggedInUserId, departmentId);
					deleteDepartment.add(dtoDepartment2);
				} else {
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoDepartment.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoDepartment.setMessageType("");

			}
			dtoDepartment.setDeleteDepartment(deleteDepartment);

			log.debug("Delete Department :" + dtoDepartment.getId());
		} catch (Exception e) {
			log.error(e);

		}
		return dtoDepartment;
	}

	/**
	 * @Description: check Repeat DepartmentId
	 * @param departmetnId
	 * @return
	 */

	public DtoDepartment repeatByDepartmentId(String departmetnId) {
		log.info("repeatByDepartmentId Method");
		DtoDepartment dtoDepartment = new DtoDepartment();
		try {
			List<Department> department = repositoryDepartment.findByDepartmentId(departmetnId.trim());
			if (department != null && !department.isEmpty()) {
				dtoDepartment.setIsRepeat(true);
			} else {
				dtoDepartment.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDepartment;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchDepartmentId(DtoSearch dtoSearch) {
		log.info("searchDepartmentId Method");
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();

			List<String> departmentIdList = this.repositoryDepartment
					.predictiveDepartmentIdSearchWithPagination("%" + searchWord + "%");
			if (!departmentIdList.isEmpty()) {
				dtoSearch.setRecords(departmentIdList.size());
			}
			dtoSearch.setIds(departmentIdList);
		}
		return dtoSearch;
	}

	@Transactional
	@Async
	public List<DtoDepartment> getAllDepartmentDropDownList() {
		log.info("getAllDepartmentList  Method");
		List<DtoDepartment> dtoDepartmentList = new ArrayList<>();
		try {
			List<Department> list = repositoryDepartment.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (Department department : list) {
					DtoDepartment dtoDepartment = new DtoDepartment();
					dtoDepartment.setId(department.getId());
					dtoDepartment.setDepartmentId(department.getDepartmentId());
					dtoDepartment.setDepartmentDescription(department.getDepartmentDescription());
					dtoDepartment.setArabicDepartmentDescription(department.getArabicDepartmentDescription());
					dtoDepartmentList.add(dtoDepartment);
				}
			}
			log.debug("Department is:" + dtoDepartmentList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDepartmentList;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllDepartmentId(DtoSearch dtoSearch) {
		log.info("searchDepartment Method");
		try {
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				dtoSearch = searchByDepartment(dtoSearch);
				dtoSearch.setTotalCount(
						this.repositoryDepartment.predictiveDepartmentSearchTotalCount("%" + searchWord + "%"));

				List<Department> departmentList = searchByPageSizeAndNumber(dtoSearch);

				if (departmentList != null && !departmentList.isEmpty()) {
					List<DtoDepartment> dtoDepartmentList = new ArrayList<>();
					DtoDepartment dtoDepartment = null;
					for (Department department : departmentList) {
						dtoDepartment = new DtoDepartment(department);
						dtoDepartment.setId(department.getId());
						dtoDepartment.setArabicDepartmentDescription(department.getArabicDepartmentDescription());
						dtoDepartmentList.add(dtoDepartment);
					}
					dtoSearch.setRecords(dtoDepartmentList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		if (dtoSearch != null) {
			log.debug("Search Department Size is:" + dtoSearch.getTotalCount());
		}

		return dtoSearch;
	}

}
