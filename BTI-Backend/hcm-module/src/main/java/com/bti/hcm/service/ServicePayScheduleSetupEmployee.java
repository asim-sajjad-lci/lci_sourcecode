/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.PayScheduleSetup;
import com.bti.hcm.model.PayScheduleSetupEmployee;
import com.bti.hcm.model.dto.DtoPayScheduleSetupEmployee;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryPayScheduleSetup;
import com.bti.hcm.repository.RepositoryPayScheduleSetupEmployee;

/**
 * Description: Service PayScheduleSetupEmployee
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("servicePayScheduleSetupEmployee")
public class ServicePayScheduleSetupEmployee {
	
	

	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupEmployee Service
	 */
	static Logger log = Logger.getLogger(ServicePayScheduleSetupEmployee.class.getName());


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PayScheduleSetupEmployee service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PayScheduleSetupEmployee service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPayScheduleSetupEmployeet Autowired here using annotation of spring for access of repositoryPayScheduleSetupEmployeet method in PayScheduleSetupEmployee service
	 * 				In short Access PayScheduleSetupEmployee Query from Database using repositoryPayScheduleSetupEmployeet.
	 */
	@Autowired
	RepositoryPayScheduleSetupEmployee repositoryPayScheduleSetupEmployee;
	
	@Autowired
	RepositoryPayScheduleSetup repositoryPayScheduleSetup;
	
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	/**
	 * 
	 * @param dtoPayScheduleSetupEmployee
	 * @return
	 * @throws ParseException
	 */
	public DtoPayScheduleSetupEmployee saveOrUpdatePayScheduleSetupEmployee(DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee) throws ParseException {
		log.info("saveOrUpdatePayScheduleSetupEmployee Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		PayScheduleSetupEmployee payScheduleSetupEmployee = null;
		try {
			PayScheduleSetup payScheduleSetup = repositoryPayScheduleSetup.findOne(dtoPayScheduleSetupEmployee.getPayScheduleSetupId());
			EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOne(dtoPayScheduleSetupEmployee.getEmployeeId());
			if (dtoPayScheduleSetupEmployee.getPaySchedulEmployeeIndexId() > 0) {
				payScheduleSetupEmployee = repositoryPayScheduleSetupEmployee.findBypaySchedulEmployeeIndexIdAndIsDeleted(dtoPayScheduleSetupEmployee.getPaySchedulEmployeeIndexId(), false);
				payScheduleSetupEmployee.setUpdatedBy(loggedInUserId);
				payScheduleSetupEmployee.setUpdatedDate(new Date());
			} else {
				payScheduleSetupEmployee = new PayScheduleSetupEmployee();
				payScheduleSetupEmployee.setCreatedDate(new Date());
				
				Integer rowId = repositoryPayScheduleSetup.getCountOfTotaPayScheduleSetup();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				payScheduleSetupEmployee.setRowId(increment);
			}
			payScheduleSetupEmployee.setEmployeeFullName(dtoPayScheduleSetupEmployee.getEmployeeFullName());
			payScheduleSetupEmployee.setPayScheduleStatus(dtoPayScheduleSetupEmployee.getPayScheduleStatus());
			payScheduleSetupEmployee.setPayScheduleSetup(payScheduleSetup);
			payScheduleSetupEmployee.setEmployeeMaster(employeeMaster);
			log.debug("PayScheduleSetupEmployee is:"+dtoPayScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
		}
		return dtoPayScheduleSetupEmployee;
	}
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPayScheduleSetupEmployee(DtoSearch dtoSearch) {
		try {
			log.info("searchPayScheduleSetupEmployee Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "payScheduleSetup" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "employeeFullName" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "payScheduleStatus" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="paySchedulEmployeeIndexId";
					
				}
			}
				dtoSearch.setTotalCount(this.repositoryPayScheduleSetupEmployee.predictivePayScheduleSetupEmployeeSearchTotalCount("%"+searchWord+"%"));
				List<PayScheduleSetupEmployee> payScheduleSetupEmployeeList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						payScheduleSetupEmployeeList = this.repositoryPayScheduleSetupEmployee.predictivePayScheduleSetupEmployeeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						payScheduleSetupEmployeeList = this.repositoryPayScheduleSetupEmployee.predictivePayScheduleSetupEmployeeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						payScheduleSetupEmployeeList = this.repositoryPayScheduleSetupEmployee.predictivePayScheduleSetupEmployeeSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}
				if(payScheduleSetupEmployeeList != null && !payScheduleSetupEmployeeList.isEmpty()){
					List<DtoPayScheduleSetupEmployee> dtoPayScheduleSetupEmployeeList = new ArrayList<DtoPayScheduleSetupEmployee>();
					DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee=null;
					for (PayScheduleSetupEmployee payScheduleSetupEmployee : payScheduleSetupEmployeeList) {
						dtoPayScheduleSetupEmployee = new DtoPayScheduleSetupEmployee(payScheduleSetupEmployee);
						dtoPayScheduleSetupEmployee.setPaySchedulEmployeeIndexId(payScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
						dtoPayScheduleSetupEmployee.setPayScheduleSetupId(payScheduleSetupEmployee.getPayScheduleSetup().getPayScheduleIndexId());
						dtoPayScheduleSetupEmployee.setEmployeeFullName(payScheduleSetupEmployee.getEmployeeFullName());
						dtoPayScheduleSetupEmployee.setPayScheduleStatus(payScheduleSetupEmployee.getPayScheduleStatus());
						dtoPayScheduleSetupEmployee.setEmployeeId(payScheduleSetupEmployee.getEmployeeMaster().getEmployeeIndexId());
						dtoPayScheduleSetupEmployeeList.add(dtoPayScheduleSetupEmployee);
					}
					dtoSearch.setRecords(dtoPayScheduleSetupEmployeeList);
				}
			}
			log.debug("Search PayScheduleSetupEmployee Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public DtoPayScheduleSetupEmployee getPaySchedulEmployeeIndexId(int id) {
		log.info("getPaySchedulEmployeeIndexId Method");
		DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee = new DtoPayScheduleSetupEmployee();
		try {
			if (id > 0) {
				PayScheduleSetupEmployee payScheduleSetupEmployee = repositoryPayScheduleSetupEmployee.findBypaySchedulEmployeeIndexIdAndIsDeleted(id, false);
				if (payScheduleSetupEmployee != null) {
					dtoPayScheduleSetupEmployee = new DtoPayScheduleSetupEmployee(payScheduleSetupEmployee);
					dtoPayScheduleSetupEmployee.setPaySchedulEmployeeIndexId(payScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
					dtoPayScheduleSetupEmployee.setPayScheduleSetupId(payScheduleSetupEmployee.getPayScheduleSetup().getPayScheduleIndexId());
					dtoPayScheduleSetupEmployee.setPayScheduleStatus(payScheduleSetupEmployee.getPayScheduleStatus());
					dtoPayScheduleSetupEmployee.setEmployeeId(payScheduleSetupEmployee.getEmployeeMaster().getEmployeeIndexId());
					
				} else {
					dtoPayScheduleSetupEmployee.setMessageType("PAYSCHEDULE_SETUP_EMPLOYEE_NOT_GETTING");

				}
			} else {
				dtoPayScheduleSetupEmployee.setMessageType("INVALID_PAYSCHEDULE_SETUP_EMPLOYEE_INDEX_ID");

			}
			log.debug("getPaySchedulEmployeeIndexId By Id is:"+dtoPayScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayScheduleSetupEmployee;
	}
	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoPayScheduleSetupEmployee deletePayScheduleSetupEmployee(List<Integer> ids) {
		log.info("deletePayScheduleSetupEmployee Method");
		DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee = new DtoPayScheduleSetupEmployee();
		dtoPayScheduleSetupEmployee.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAYSCHEDULE_SETUP_EMPLOYEE_DELETED", false));
		dtoPayScheduleSetupEmployee.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("PAYSCHEDULE_SETUP_EMPLOYEE_ASSOCIATED", false));
		List<DtoPayScheduleSetupEmployee> deletePayScheduleSetupEmployee = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		try {
			for (Integer payScheduleEmployeeIndexId : ids) {
				PayScheduleSetupEmployee payScheduleSetupEmployee = repositoryPayScheduleSetupEmployee.findOne(payScheduleEmployeeIndexId);
				DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee2 = new DtoPayScheduleSetupEmployee();
				dtoPayScheduleSetupEmployee2.setPaySchedulEmployeeIndexId(payScheduleEmployeeIndexId);
				dtoPayScheduleSetupEmployee2.setEmployeeFullName(payScheduleSetupEmployee.getEmployeeFullName());
				repositoryPayScheduleSetupEmployee.deleteSinglPayScheduleSetupEmployee(true, loggedInUserId, payScheduleEmployeeIndexId);
				deletePayScheduleSetupEmployee.add(dtoPayScheduleSetupEmployee2);

			}
			dtoPayScheduleSetupEmployee.setDeletPayScheduleSetupEmployee(deletePayScheduleSetupEmployee);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete PayScheduleSetupEmployee :"+dtoPayScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
		return dtoPayScheduleSetupEmployee;
	}

}
