package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40300",indexes = {
        @Index(columnList = "MISCINDX")
})
public class MiscellaneousBenefits extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MISCINDX")
	private Integer id;

	@Column(name = "MISCID", columnDefinition = "char(15)")
	private String BenefitsId;

	@Column(name = "MISCDSCR", columnDefinition = "char(31)")
	private String desc;

	@Column(name = "MISCDSCRA", columnDefinition = "char(61)")
	private String arbicDesc;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "miscellaneousBenefits")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<MiscellaneousBenefirtEnrollment> listMiscellaneousBenefirtEnrollment;

	@Column(name = "MISCFREQ")
	private short frequency;

	@Column(name = "MISCSTRDT")
	private Date startDate;

	@Column(name = "MISCENDDT")
	private Date endDate;

	@Column(name = "MISCEMPL")
	private boolean inactive;

	@Column(name = "MISCMETOD")
	private short method;

	@Column(name = "MISCAMNT", precision = 13, scale = 3)
	private BigDecimal dudctionAmount;

	@Column(name = "MISCPRNT", precision = 13, scale = 3)
	private BigDecimal dudctionPercent;

	@Column(name = "MISCMAXAMTP", precision = 13, scale = 3)
	private BigDecimal monthlyAmount;

	@Column(name = "MISCMAXAMTC", precision = 13, scale = 3)
	private BigDecimal yearlyAmount;

	@Column(name = "MISCMAXAMTL", precision = 13, scale = 3)
	private BigDecimal lifetimeAmount;

	@Column(name = "MISCEMPLOR", precision = 13, scale = 3)
	private boolean empluyeeerinactive;

	@Column(name = "MISCMETODR")
	private short empluyeeermethod;

	@Column(name = "MISCAMNTR", precision = 13, scale = 3)
	private BigDecimal benefitAmount;

	@Column(name = "MISCPRNTR", precision = 13, scale = 3)
	private BigDecimal benefitPercent;

	@Column(name = "MISCMAXAMTRP", precision = 13, scale = 3)
	private BigDecimal employermonthlyAmount;

	@Column(name = "MISCMAXAMTRC", precision = 13, scale = 3)
	private BigDecimal employeryearlyAmount;

	@Column(name = "MISCMAXAMTRL", precision = 13, scale = 3)
	private BigDecimal employerlifetimeAmount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBenefitsId() {
		return BenefitsId;
	}

	public void setBenefitsId(String benefitsId) {
		BenefitsId = benefitsId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public short getFrequency() {
		return frequency;
	}

	public void setFrequency(short frequency) {
		this.frequency = frequency;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public short getMethod() {
		return method;
	}

	public void setMethod(short method) {
		this.method = method;
	}

	public boolean isEmpluyeeerinactive() {
		return empluyeeerinactive;
	}

	public void setEmpluyeeerinactive(boolean empluyeeerinactive) {
		this.empluyeeerinactive = empluyeeerinactive;
	}

	public short getEmpluyeeermethod() {
		return empluyeeermethod;
	}

	public void setEmpluyeeermethod(short empluyeeermethod) {
		this.empluyeeermethod = empluyeeermethod;
	}

	public BigDecimal getDudctionAmount() {
		return dudctionAmount;
	}

	public void setDudctionAmount(BigDecimal dudctionAmount) {
		this.dudctionAmount = dudctionAmount;
	}

	public BigDecimal getDudctionPercent() {
		return dudctionPercent;
	}

	public void setDudctionPercent(BigDecimal dudctionPercent) {
		this.dudctionPercent = dudctionPercent;
	}

	public BigDecimal getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(BigDecimal monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public BigDecimal getYearlyAmount() {
		return yearlyAmount;
	}

	public void setYearlyAmount(BigDecimal yearlyAmount) {
		this.yearlyAmount = yearlyAmount;
	}

	public BigDecimal getLifetimeAmount() {
		return lifetimeAmount;
	}

	public void setLifetimeAmount(BigDecimal lifetimeAmount) {
		this.lifetimeAmount = lifetimeAmount;
	}

	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public BigDecimal getBenefitPercent() {
		return benefitPercent;
	}

	public void setBenefitPercent(BigDecimal benefitPercent) {
		this.benefitPercent = benefitPercent;
	}

	public BigDecimal getEmployermonthlyAmount() {
		return employermonthlyAmount;
	}

	public void setEmployermonthlyAmount(BigDecimal employermonthlyAmount) {
		this.employermonthlyAmount = employermonthlyAmount;
	}

	public BigDecimal getEmployeryearlyAmount() {
		return employeryearlyAmount;
	}

	public void setEmployeryearlyAmount(BigDecimal employeryearlyAmount) {
		this.employeryearlyAmount = employeryearlyAmount;
	}

	public BigDecimal getEmployerlifetimeAmount() {
		return employerlifetimeAmount;
	}

	public void setEmployerlifetimeAmount(BigDecimal employerlifetimeAmount) {
		this.employerlifetimeAmount = employerlifetimeAmount;
	}

	public List<MiscellaneousBenefirtEnrollment> getListMiscellaneousBenefirtEnrollment() {
		return listMiscellaneousBenefirtEnrollment;
	}

	public void setListMiscellaneousBenefirtEnrollment(
			List<MiscellaneousBenefirtEnrollment> listMiscellaneousBenefirtEnrollment) {
		this.listMiscellaneousBenefirtEnrollment = listMiscellaneousBenefirtEnrollment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((BenefitsId == null) ? 0 : BenefitsId.hashCode());
		result = prime * result + ((arbicDesc == null) ? 0 : arbicDesc.hashCode());
		result = prime * result + ((benefitAmount == null) ? 0 : benefitAmount.hashCode());
		result = prime * result + ((benefitPercent == null) ? 0 : benefitPercent.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((dudctionAmount == null) ? 0 : dudctionAmount.hashCode());
		result = prime * result + ((dudctionPercent == null) ? 0 : dudctionPercent.hashCode());
		result = prime * result + ((employerlifetimeAmount == null) ? 0 : employerlifetimeAmount.hashCode());
		result = prime * result + ((employermonthlyAmount == null) ? 0 : employermonthlyAmount.hashCode());
		result = prime * result + ((employeryearlyAmount == null) ? 0 : employeryearlyAmount.hashCode());
		result = prime * result + (empluyeeerinactive ? 1231 : 1237);
		result = prime * result + empluyeeermethod;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + frequency;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (inactive ? 1231 : 1237);
		result = prime * result + ((lifetimeAmount == null) ? 0 : lifetimeAmount.hashCode());
		result = prime * result
				+ ((listMiscellaneousBenefirtEnrollment == null) ? 0 : listMiscellaneousBenefirtEnrollment.hashCode());
		result = prime * result + method;
		result = prime * result + ((monthlyAmount == null) ? 0 : monthlyAmount.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((yearlyAmount == null) ? 0 : yearlyAmount.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
