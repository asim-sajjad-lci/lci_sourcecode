/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPayScheduleSetupEmployee;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayScheduleSetupEmployee;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: Controller PayScheduleSetupEmployee
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/payScheduleSetupEmployee")
public class ControllerPayScheduleSetupEmployee extends BaseController{
	
	
	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupEmployee Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerPayScheduleSetupEmployee.class);
	
	/**
	 * @Description servicePayScheduleSetupEmployee Autowired here using annotation of spring for use of servicePayScheduleSetupEmployee method in this controller
	 */
	@Autowired(required=true)
	ServicePayScheduleSetupEmployee servicePayScheduleSetupEmployee;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee) throws Exception {
		LOGGER.info("Create PayScheduleSetupEmployee Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupEmployee = servicePayScheduleSetupEmployee.saveOrUpdatePayScheduleSetupEmployee(dtoPayScheduleSetupEmployee);
			responseMessage=displayMessage(dtoPayScheduleSetupEmployee, "PAY_SCHEDULE_SETUP_EMPLOYEE_CREATED", "PAY_SCHEDULE_SETUP_EMPLOYEE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PayScheduleSetupDepartment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupEmployee
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee) throws Exception {
		LOGGER.info("Update PayScheduleSetupEmployee Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupEmployee = servicePayScheduleSetupEmployee.saveOrUpdatePayScheduleSetupEmployee(dtoPayScheduleSetupEmployee);
			responseMessage=displayMessage(dtoPayScheduleSetupEmployee, "PAY_SCHEDULE_SETUP_EMPLOYEE_UPDATED_SUCCESS", "PAY_SCHEDULE_SETUP_EMPLOYEE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update PayScheduleSetupEmployee Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupEmployee
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee) throws Exception {
		LOGGER.info("Delete PayScheduleSetupEmployee Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayScheduleSetupEmployee.getIds() != null && !dtoPayScheduleSetupEmployee.getIds().isEmpty()) {
				DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee2 = servicePayScheduleSetupEmployee.deletePayScheduleSetupEmployee(dtoPayScheduleSetupEmployee.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_EMPLOYEE_LIST_DELETED", false), dtoPayScheduleSetupEmployee2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete PayScheduleSetupEmployee Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search PayScheduleSetupEmployee Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayScheduleSetupEmployee.searchPayScheduleSetupEmployee(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_SCHEDULE_SETUP_EMPLOYEE_GET_ALL", "PAY_SCHEDULE_SETUP_EMPLOYEE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PayScheduleSetupEmployee Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployee) throws Exception {
		LOGGER.info("Get  ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayScheduleSetupEmployee dtoPayScheduleSetupEmployeeObj = servicePayScheduleSetupEmployee.getPaySchedulEmployeeIndexId(dtoPayScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
			responseMessage=displayMessage(dtoPayScheduleSetupEmployeeObj, "PAY_SCHEDULE_SETUP_EMPLOYEE_GET_DETAIL", "PAY_SCHEDULE_SETUP_EMPLOYEE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoPayScheduleSetupEmployee.getPaySchedulEmployeeIndexId());
		return responseMessage;
	}

}
