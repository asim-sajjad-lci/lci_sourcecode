package com.bti.hcm.model;

import java.util.Date;
import java.util.List;

public class DtoProcess{
	private Short transactionType;
	private List<String> ids;
	private Integer pageNumber;
	private Integer pageSize;
	private Integer id;
	private Date postingDate;
	
	public Short getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(Short transactionType) {
		this.transactionType = transactionType;
	}
	public List<String> getIds() {
		return ids;
	}
	public void setIds(List<String> ids) {
		this.ids = ids;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getPostingDate() {
		return postingDate;
	}
	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}
	

}
