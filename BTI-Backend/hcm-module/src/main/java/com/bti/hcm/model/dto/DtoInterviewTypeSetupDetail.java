/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.InterviewTypeSetupDetail;
import com.bti.hcm.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO InterviewTypeSetupDetail class having getter and setter for
 * fields (POJO) Name Name of Project: Hcm Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoInterviewTypeSetupDetail extends DtoBase{

	private Integer id;
	private Integer interviewSetupId;
	private Integer interviewDetailId;
	private String interviewTypeId;
	private Integer interviewTypeSetupDetailCategoryRowSequance;
	private String interviewTypeSetupDetailDescription;
	private Integer interviewTypeSetupDetailCategorySequance;
	private Integer interviewTypeSetupDetailCategoryWeight;
	private Integer interviewTypeSetupId;
	private Integer interviewTypePrimaryId;
	private String interviewTypeDesc;
	private String interviewTypeArbicDesc;
	private List<DtoInterviewTypeSetupDetail> deleteInterviewTypeSetupDetail;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getInterviewTypeSetupDetailCategoryRowSequance() {
		return interviewTypeSetupDetailCategoryRowSequance;
	}

	public void setInterviewTypeSetupDetailCategoryRowSequance(Integer interviewTypeSetupDetailCategoryRowSequance) {
		this.interviewTypeSetupDetailCategoryRowSequance = interviewTypeSetupDetailCategoryRowSequance;
	}

	public String getInterviewTypeSetupDetailDescription() {
		return interviewTypeSetupDetailDescription;
	}

	public void setInterviewTypeSetupDetailDescription(String interviewTypeSetupDetailDescription) {
		this.interviewTypeSetupDetailDescription = interviewTypeSetupDetailDescription;
	}

	public Integer getInterviewTypeSetupDetailCategorySequance() {
		return interviewTypeSetupDetailCategorySequance;
	}

	public void setInterviewTypeSetupDetailCategorySequance(Integer interviewTypeSetupDetailCategorySequance) {
		this.interviewTypeSetupDetailCategorySequance = interviewTypeSetupDetailCategorySequance;
	}

	public Integer getInterviewTypeSetupDetailCategoryWeight() {
		return interviewTypeSetupDetailCategoryWeight;
	}

	public void setInterviewTypeSetupDetailCategoryWeight(Integer interviewTypeSetupDetailCategoryWeight) {
		this.interviewTypeSetupDetailCategoryWeight = interviewTypeSetupDetailCategoryWeight;
	}

	public List<DtoInterviewTypeSetupDetail> getDeleteInterviewTypeSetupDetail() {
		return deleteInterviewTypeSetupDetail;
	}

	public void setDeleteInterviewTypeSetupDetail(List<DtoInterviewTypeSetupDetail> deleteInterviewTypeSetupDetail) {
		this.deleteInterviewTypeSetupDetail = deleteInterviewTypeSetupDetail;
	}

	public Integer getInterviewTypeSetupId() {
		return interviewTypeSetupId;
	}

	public void setInterviewTypeSetupId(Integer interviewTypeSetupId) {
		this.interviewTypeSetupId = interviewTypeSetupId;
	}

	public DtoInterviewTypeSetupDetail() {

	}

	public Integer getInterviewTypePrimaryId() {
		return interviewTypePrimaryId;
	}

	public void setInterviewTypePrimaryId(Integer interviewTypePrimaryId) {
		this.interviewTypePrimaryId = interviewTypePrimaryId;
	}

	public String getInterviewTypeDesc() {
		return interviewTypeDesc;
	}

	public void setInterviewTypeDesc(String interviewTypeDesc) {
		this.interviewTypeDesc = interviewTypeDesc;
	}

	public String getInterviewTypeArbicDesc() {
		return interviewTypeArbicDesc;
	}

	public void setInterviewTypeArbicDesc(String interviewTypeArbicDesc) {
		this.interviewTypeArbicDesc = interviewTypeArbicDesc;
	}

	public Integer getInterviewDetailId() {
		return interviewDetailId;
	}

	public void setInterviewDetailId(Integer interviewDetailId) {
		this.interviewDetailId = interviewDetailId;
	}

	public String getInterviewTypeId() {
		return interviewTypeId;
	}

	public void setInterviewTypeId(String interviewTypeId) {
		this.interviewTypeId = interviewTypeId;
	}

	public DtoInterviewTypeSetupDetail(InterviewTypeSetupDetail interviewTypeSetupDetail) {
		if (UtilRandomKey.isNotBlank(interviewTypeSetupDetail.getInterviewTypeSetupDetailDescription())) {
			this.interviewTypeSetupDetailDescription = interviewTypeSetupDetail
					.getInterviewTypeSetupDetailDescription();
		} else {
			this.interviewTypeSetupDetailDescription = "";
		}
	}

	public Integer getInterviewSetupId() {
		return interviewSetupId;
	}

	public void setInterviewSetupId(Integer interviewSetupId) {
		this.interviewSetupId = interviewSetupId;
	}

}
