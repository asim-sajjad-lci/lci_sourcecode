package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.EmployeeDeductionMaintenance;

public class DtoEmployeeDeductionMaintenance extends DtoBase {
	private Integer id;
	private Date startDate;
	private Date endDate;
	private Boolean transactionRequired;
	private Short benefitMethod;
	private BigDecimal deductionAmount;
	private BigDecimal deductionPercent;
	private BigDecimal perPeriord;
	private BigDecimal perYear;
	private BigDecimal lifeTime;
	private Short frequency;
	private Boolean inactive;
	private List<DtoPayCode> dtoPayCode;

	private DtoEmployeeMaster employeeMaster;

	private DtoEmployeeDependent employeeDependent;
	private DtoDeductionCode deductionCode;

	private List<DtoEmployeeDeductionMaintenance> delete;
	private BigDecimal payFactor;

	private Integer noOfDays;
	private Integer endDateDays;

	public DtoEmployeeDeductionMaintenance(EmployeeDeductionMaintenance employeeDeductionMaintenance) {

	}

	public DtoEmployeeDeductionMaintenance() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getTransactionRequired() {
		return transactionRequired;
	}

	public void setTransactionRequired(Boolean transactionRequired) {
		this.transactionRequired = transactionRequired;
	}

	public Short getBenefitMethod() {
		return benefitMethod;
	}

	public void setBenefitMethod(Short benefitMethod) {
		this.benefitMethod = benefitMethod;
	}

	public BigDecimal getDeductionAmount() {
		return deductionAmount;
	}

	public void setDeductionAmount(BigDecimal deductionAmount) {
		this.deductionAmount = deductionAmount;
	}

	public BigDecimal getDeductionPercent() {
		return deductionPercent;
	}

	public void setDeductionPercent(BigDecimal deductionPercent) {
		this.deductionPercent = deductionPercent;
	}

	public BigDecimal getPerPeriord() {
		return perPeriord;
	}

	public void setPerPeriord(BigDecimal perPeriord) {
		this.perPeriord = perPeriord;
	}

	public BigDecimal getPerYear() {
		return perYear;
	}

	public void setPerYear(BigDecimal perYear) {
		this.perYear = perYear;
	}

	public BigDecimal getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(BigDecimal lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Short getFrequency() {
		return frequency;
	}

	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}

	public Boolean getInactive() {
		return inactive;
	}

	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}

	public List<DtoEmployeeDeductionMaintenance> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoEmployeeDeductionMaintenance> delete) {
		this.delete = delete;
	}

	public DtoEmployeeDependent getEmployeeDependent() {
		return employeeDependent;
	}

	public void setEmployeeDependent(DtoEmployeeDependent employeeDependent) {
		this.employeeDependent = employeeDependent;
	}

	public DtoDeductionCode getDeductionCode() {
		return deductionCode;
	}

	public void setDeductionCode(DtoDeductionCode deductionCode) {
		this.deductionCode = deductionCode;
	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getEndDateDays() {
		return endDateDays;
	}

	public void setEndDateDays(Integer endDateDays) {
		this.endDateDays = endDateDays;
	}

	public List<DtoPayCode> getDtoPayCode() {
		return dtoPayCode;
	}

	public void setDtoPayCode(List<DtoPayCode> dtoPayCode) {
		this.dtoPayCode = dtoPayCode;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

}
