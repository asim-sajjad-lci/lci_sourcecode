package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AccrualPeriodSetting;

@Repository("repositoryAccrualPeriodSetting")
public interface RepositoryAccrualPeriodSetting extends JpaRepository<AccrualPeriodSetting, Integer>{

	AccrualPeriodSetting findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update AccrualPeriodSetting d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleAccrualPeriodSetting(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	List<AccrualPeriodSetting> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select count(*) from AccrualPeriodSetting a where a.isDeleted=false")
	Integer predictiveAccrualPeriodSettingSearchTotalCount();

	@Query("select a from AccrualPeriod a where  a.isDeleted=false")
	List<AccrualPeriodSetting> predictiveAccrualPeriodSettingSearchWithPagination(Pageable pageRequest);

	@Query("select count(*) from AccrualPeriod a ")
	public Integer getCountOfTotalAccruals();
	
}
