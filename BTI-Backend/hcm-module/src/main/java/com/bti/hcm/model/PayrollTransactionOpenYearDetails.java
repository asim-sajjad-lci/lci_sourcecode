package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90200")
public class PayrollTransactionOpenYearDetails extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RWPRLINDX")
	private Integer id;

	@Column(name = "HCMTRXPYRL")
	private String auditTransactionNumber;

	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;
	
	@ManyToOne
	@JoinColumn(name = "DEPINDX")
	private Department department;  
	
	@ManyToOne
	@JoinColumn(name = "POTINDX")
	private Position position;
	
	@ManyToOne
	@JoinColumn(name = "LOCINDX")
	private Location location;
	
	
	@ManyToOne
	@JoinColumn(name = "HCMBATINDX")
	private Batches batches;
	
	@ManyToOne
	@JoinColumn(name = "DIMINXVALUE")
	private FinancialDimensionsValues financialDimensionsValues;  
	
	@Column(name = "HCMEMPSEQN")
	private Integer codeSequence;

	@Column(name = "HCMCDTYP")
	private Short codeType;

	@Column(name = "HCMCDINDX")
	private Integer codeIndexId;

	@Column(name = "HCMCDAMT", precision = 10, scale = 3)
	private BigDecimal totalAmount;

	@Column(name = "HCMCDAAMT", precision = 10, scale = 3)
	private BigDecimal totalActualAmount;

	@Column(name = "HCMCDPRNT", precision = 10, scale = 3)
	private BigDecimal totalPercentcode;

	@Column(name = "HCMCDHRS")
	private Integer totalHour;

	@Column(name = "HCMBSDRTE", precision = 10, scale = 3)
	private BigDecimal rateBaseOnPayCode;

	@Column(name = "HCMCDBSD")
	private Integer codeBaseOnCodeIndex;

	@Column(name = "HCMCDFDT")
	private Date codeFromPeriodDate;

	@Column(name = "HCMCDTDT")
	private Date codeToPeriodDate;

	@Column(name = "PRLPOSTDT")
	private Date payrollPostDate;

	@Column(name = "PRLPOSTURS")
	private String payrollPostByUserId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuditTransactionNumber() {
		return auditTransactionNumber;
	}

	public void setAuditTransactionNumber(String auditTransactionNumber) {
		this.auditTransactionNumber = auditTransactionNumber;
	}

	public Integer getCodeSequence() {
		return codeSequence;
	}

	public void setCodeSequence(Integer codeSequence) {
		this.codeSequence = codeSequence;
	}

	public Short getCodeType() {
		return codeType;
	}

	public void setCodeType(Short codeType) {
		this.codeType = codeType;
	}

	public Integer getCodeIndexId() {
		return codeIndexId;
	}

	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}

	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}

	public BigDecimal getTotalPercentcode() {
		return totalPercentcode;
	}

	public void setTotalPercentcode(BigDecimal totalPercentcode) {
		this.totalPercentcode = totalPercentcode;
	}

	public Integer getTotalHour() {
		return totalHour;
	}

	public void setTotalHour(Integer totalHour) {
		this.totalHour = totalHour;
	}

	public BigDecimal getRateBaseOnPayCode() {
		return rateBaseOnPayCode;
	}

	public void setRateBaseOnPayCode(BigDecimal rateBaseOnPayCode) {
		this.rateBaseOnPayCode = rateBaseOnPayCode;
	}

	public Integer getCodeBaseOnCodeIndex() {
		return codeBaseOnCodeIndex;
	}

	public void setCodeBaseOnCodeIndex(Integer codeBaseOnCodeIndex) {
		this.codeBaseOnCodeIndex = codeBaseOnCodeIndex;
	}

	public Date getCodeFromPeriodDate() {
		return codeFromPeriodDate;
	}

	public void setCodeFromPeriodDate(Date codeFromPeriodDate) {
		this.codeFromPeriodDate = codeFromPeriodDate;
	}

	public Date getCodeToPeriodDate() {
		return codeToPeriodDate;
	}

	public void setCodeToPeriodDate(Date codeToPeriodDate) {
		this.codeToPeriodDate = codeToPeriodDate;
	}

	public Date getPayrollPostDate() {
		return payrollPostDate;
	}

	public void setPayrollPostDate(Date payrollPostDate) {
		this.payrollPostDate = payrollPostDate;
	}

	public String getPayrollPostByUserId() {
		return payrollPostByUserId;
	}

	public void setPayrollPostByUserId(String payrollPostByUserId) {
		this.payrollPostByUserId = payrollPostByUserId;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Batches getBatches() {
		return batches;
	}

	public void setBatches(Batches batches) {
		this.batches = batches;
	}

	public FinancialDimensionsValues getFinancialDimensionsValues() {
		return financialDimensionsValues;
	}

	public void setFinancialDimensionsValues(FinancialDimensionsValues financialDimensionsValues) {
		this.financialDimensionsValues = financialDimensionsValues;
	}

}
