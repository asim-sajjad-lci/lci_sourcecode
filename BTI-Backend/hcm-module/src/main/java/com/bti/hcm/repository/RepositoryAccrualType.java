package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AccrualType;

@Repository("repositoryAccrualType")
public interface RepositoryAccrualType extends JpaRepository<AccrualType, Integer>{

	AccrualType findByIdAndIsDeleted(Integer id, boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update AccrualType d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleAccrual(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from AccrualType s where s.isDeleted=false")
	Integer getCountOfTotalAccrual();

	List<AccrualType> findByIsDeleted(boolean b, Pageable pageable);
	
	List<AccrualType> findByIsDeleted(boolean b);

	List<AccrualType> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select d from AccrualType d where ( d.desc like :searchKeyWord) and d.isDeleted=false")
	public List<AccrualType> predictiveAccrualSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	

	@Query("select count(*) from AccrualType d where ( d.desc like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveAccrualSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	
	@Query("select a from AccrualType a where (a.desc like :searchKeyWord) and a.isDeleted=false")
	List<AccrualType> getAllAccrualsTypesId(@Param("searchKeyWord")String searchKeyWord);

	
	@Query("select a from AccrualType a where (a.desc=:desc) and a.isDeleted=false")
	List<AccrualType>  checkedDuplicateByDescription(@Param("desc")String desc);

	@Query("select count(*) from AccrualType a ")
	public Integer getCountOfTotalAccrualType();

}
