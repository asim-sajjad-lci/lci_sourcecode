package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PositionClass;
import com.bti.hcm.model.PositionSetup;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoPositionClass extends DtoBase{

	
	private Integer id;
	private String positionClassId;
	private String arabicPositionClassDescription;
	private String positionClassDescription;
	private List<DtoPositionClass> deletePosition;
	private List<PositionSetup> listPositionSetup;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getPositionClassId() {
		return positionClassId;
	}
	public void setPositionClassId(String positionClassId) {
		this.positionClassId = positionClassId;
	}
	public String getArabicPositionClassDescription() {
		return arabicPositionClassDescription;
	}
	public void setArabicPositionClassDescription(String arabicPositionClassDescription) {
		this.arabicPositionClassDescription = arabicPositionClassDescription;
	}
	public String getPositionClassDescription() {
		return positionClassDescription;
	}
	public void setPositionClassDescription(String positionClassDescription) {
		this.positionClassDescription = positionClassDescription;
	}
	
	public List<DtoPositionClass> getDeletePosition() {
		return deletePosition;
	}
	public void setDeletePosition(List<DtoPositionClass> deletePosition) {
		this.deletePosition = deletePosition;
	}
	
	public List<PositionSetup> getListPositionSetup() {
		return listPositionSetup;
	}
	public void setListPositionSetup(List<PositionSetup> listPositionSetup) {
		this.listPositionSetup = listPositionSetup;
	}
	public DtoPositionClass() {
	
	}
	public DtoPositionClass(PositionClass positionClass) {
		this.positionClassId = positionClass.getPositionClassId();
	
	}
	
}
