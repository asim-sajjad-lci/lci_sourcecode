package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10310",indexes = {
        @Index(columnList = "HCMDEDINX")
})
public class BuildPayrollCheckByDeductions extends HcmBaseEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMDEDINX")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "HCMBULDINX")
	private BuildChecks buildChecks;

	@ManyToOne
	@JoinColumn(name = "DEDCINDX")
	@Where(clause = "DEDCINCTV = false and is_deleted = false")
	private DeductionCode deductionCode;

	
	
	/*@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="buildPayrollCheckByDeductions")
	@Where(clause = "is_deleted = false")
	private List<Distribution>listDistribution;
*/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BuildChecks getBuildChecks() {
		return buildChecks;
	}

	public void setBuildChecks(BuildChecks buildChecks) {
		this.buildChecks = buildChecks;
	}

	public DeductionCode getDeductionCode() {
		return deductionCode;
	}

	public void setDeductionCode(DeductionCode deductionCode) {
		this.deductionCode = deductionCode;
	}

}
