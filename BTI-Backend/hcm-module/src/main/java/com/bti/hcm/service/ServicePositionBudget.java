package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.PositionBudget;
import com.bti.hcm.model.PositionPalnSetup;
import com.bti.hcm.model.dto.DtoPositionBudget;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPositionBudget;
import com.bti.hcm.repository.RepositoryPositionPlanSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("servicePositionBudget")
public class ServicePositionBudget {

	/**
	 * @Description LOGGER use for put a logger in Department Service
	 */
	static Logger log = Logger.getLogger(ServicePositionBudget.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in PositionBudget service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PositionBudget service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PositionBudget service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired(required =  false)
	RepositoryPositionBudget repositoryPositionBudget;
	
	@Autowired(required =  false)
	RepositoryPositionPlanSetup repositoryPositionPlanSetup;

	public DtoPositionBudget saveOrUpdatePositionBudget(DtoPositionBudget dtoPositionBudget) {
		try {
			log.info("saveOrUpdatePosition Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			PositionBudget position = null;
			if (dtoPositionBudget.getId() != null && dtoPositionBudget.getId() > 0) {
				position = repositoryPositionBudget.findByIdAndIsDeleted(dtoPositionBudget.getId(), false);
				position.setUpdatedBy(loggedInUserId);
				position.setUpdatedDate(new Date());
			} else {
				position = new PositionBudget();
				position.setCreatedDate(new Date());

				Integer rowId = repositoryPositionBudget.getCountOfTotaPositionBudget();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				position.setRowId(increment);
			}
			PositionPalnSetup positionPlan = repositoryPositionPlanSetup.findOne(Integer.parseInt(dtoPositionBudget.getPositionPlanId()));
			position.setPositionPalnSetup(positionPlan);
			position.setBudgetArbicDesc(dtoPositionBudget.getBudgetArbicDesc());
			position.setBudgetScheduleSeqn(dtoPositionBudget.getBudgetScheduleSeqn());
			position.setBudgetScheduleStart(dtoPositionBudget.getBudgetScheduleStart());
			position.setBudgetScheduleEnd(dtoPositionBudget.getBudgetScheduleEnd());
			position.setBudgetDesc(dtoPositionBudget.getBudgetDesc());
			position.setBudgetAmount(dtoPositionBudget.getBudgetAmount());
			position.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryPositionBudget.saveAndFlush(position);
			log.debug("Position is:"+position.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPositionBudget;
	}

	public DtoSearch getAllPostion(DtoPositionBudget dtoPositionBudget) {
		log.info("getAllPostion Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoPositionBudget.getPageNumber());
			dtoSearch.setPageSize(dtoPositionBudget.getPageSize());
			dtoSearch.setTotalCount(repositoryPositionBudget.getCountOfTotalPosition());
			List<PositionBudget> skillPositionList = null;
			if (dtoPositionBudget.getPageNumber() != null && dtoPositionBudget.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoPositionBudget.getPageNumber(), dtoPositionBudget.getPageSize(), Direction.DESC, "createdDate");
				skillPositionList = repositoryPositionBudget.findByIsDeleted(false, pageable);
			} else {
				skillPositionList = repositoryPositionBudget.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			List<DtoPositionBudget> dtoPOsitionList=new ArrayList<>();
			if(skillPositionList!=null && !skillPositionList.isEmpty())
			{
				for (PositionBudget position : skillPositionList) 
				{
					dtoPositionBudget=new DtoPositionBudget(position);
					dtoPositionBudget.setBudgetDesc(position.getBudgetDesc());
					dtoPositionBudget.setBudgetScheduleStart(position.getBudgetScheduleStart());
					dtoPositionBudget.setBudgetScheduleEnd(position.getBudgetScheduleEnd());
					dtoPositionBudget.setBudgetAmount(position.getBudgetAmount());
					dtoPositionBudget.setBudgetArbicDesc(position.getBudgetArbicDesc());
					dtoPositionBudget.setId(position.getId());
					if(position.getPositionPalnSetup()!=null) {
						dtoPositionBudget.setPositionPlanId(position.getPositionPalnSetup().getPositionPlanId());
					}
					if(position.getPositionPalnSetup()!=null){
						dtoPositionBudget.setDescription(position.getPositionPalnSetup().getPositionPlanDesc());
						dtoPositionBudget.setPositionPlanDesc((position.getPositionPalnSetup().getPositionPlanDesc()));
						dtoPositionBudget.setPositionPlanPrimaryId(position.getPositionPalnSetup().getId());
					}
					
					dtoPOsitionList.add(dtoPositionBudget);
				}
				dtoSearch.setRecords(dtoPOsitionList);
			}
			log.debug("All Postion List Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoPositionBudget deletePositionBudget(List<Integer> ids) {
		log.info("deletePosition Method");
		DtoPositionBudget dtoPosition = new DtoPositionBudget();
		dtoPosition
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("POSITION_BUDGET_DELETED", false));
		dtoPosition.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("POSTION_ASSOCIATED", false));
		List<DtoPositionBudget> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer positionId : ids) {
				PositionBudget position = repositoryPositionBudget.findOne(positionId);
				DtoPositionBudget dtoskillSetup2 = new DtoPositionBudget();
				dtoskillSetup2.setId(positionId);
				dtoskillSetup2.setBudgetDesc(position.getBudgetDesc());
				repositoryPositionBudget.deleteSinglePositionBudget(true, loggedInUserId, positionId);
				deletePosition.add(dtoskillSetup2);

			}
			dtoPosition.setDeletePosition(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete SkillSteup :"+dtoPosition.getId());
		return dtoPosition;
	}

	public DtoPositionBudget getByPositionBudgetId(Integer id) {
		log.info("getByPositionBudgetId Method");
		DtoPositionBudget dtoPositionBudget = new DtoPositionBudget();
		try {
			if (id > 0) {
				PositionBudget position = repositoryPositionBudget.findByIdAndIsDeleted(id, false);
				if (position != null) {
					dtoPositionBudget = new DtoPositionBudget(position);
					dtoPositionBudget.setBudgetDesc(position.getBudgetDesc());
					dtoPositionBudget.setBudgetArbicDesc(position.getBudgetArbicDesc());
					dtoPositionBudget.setBudgetScheduleStart(position.getBudgetScheduleStart());
					dtoPositionBudget.setBudgetScheduleEnd(position.getBudgetScheduleEnd());
					dtoPositionBudget.setBudgetAmount(position.getBudgetAmount());
					dtoPositionBudget.setBudgetArbicDesc(position.getBudgetArbicDesc());
					dtoPositionBudget.setId(position.getId());
					if(position.getPositionPalnSetup()!=null){
						dtoPositionBudget.setDescription(position.getPositionPalnSetup().getPositionPlanDesc());
						dtoPositionBudget.setPositionPlanDesc((position.getPositionPalnSetup().getPositionPlanDesc()));
						dtoPositionBudget.setPositionPlanId(position.getPositionPalnSetup().getPositionPlanId());
						dtoPositionBudget.setPositionPlanPrimaryId(position.getPositionPalnSetup().getId());
					}
				} else {
					dtoPositionBudget.setMessageType("SKILL_STEUP_NOT_GETTING");

				}
			} else {
				dtoPositionBudget.setMessageType("INVALID_SKILL_STEUP_ID");

			}
			log.debug("getByPositionBudgetId By Id is:"+dtoPositionBudget.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPositionBudget;
	}

	public DtoSearch searchPositionBudget(DtoSearch dtoSearch) {
		log.info("searchPosition Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("positionPalnSetup") || dtoSearch.getSortOn().equals("budgetScheduleSeqn") || dtoSearch.getSortOn().equals("budgetScheduleStart") || dtoSearch.getSortOn().equals("budgetScheduleEnd")
								|| dtoSearch.getSortOn().equals("budgetDesc") || dtoSearch.getSortOn().equals("budgetAmount")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				
				dtoSearch.setTotalCount(this.repositoryPositionBudget.predictivePositionBudgetSearchTotalCount("%"+searchWord+"%"));
				List<PositionBudget> positonList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						positonList = this.repositoryPositionBudget.predictivePositionBudgetSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						positonList = this.repositoryPositionBudget.predictivePositionBudgetSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						positonList = this.repositoryPositionBudget.predictivePositionBudgetSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				if(positonList != null && !positonList.isEmpty()){
					List<DtoPositionBudget> dtoDepartmentList = new ArrayList<>();
					DtoPositionBudget dtoPositionBudget=null;
					for (PositionBudget position : positonList) {
						dtoPositionBudget = new DtoPositionBudget(position);
						dtoPositionBudget.setBudgetDesc(position.getBudgetDesc());
						dtoPositionBudget.setBudgetArbicDesc(position.getBudgetArbicDesc());
						dtoPositionBudget.setBudgetScheduleStart(position.getBudgetScheduleStart());
						dtoPositionBudget.setBudgetScheduleEnd(position.getBudgetScheduleEnd());
						dtoPositionBudget.setBudgetAmount(position.getBudgetAmount());
						dtoPositionBudget.setBudgetArbicDesc(position.getBudgetArbicDesc());
						dtoPositionBudget.setId(position.getId());
						
						
						if(position.getPositionPalnSetup()!=null){
							dtoPositionBudget.setDescription(position.getPositionPalnSetup().getPositionPlanDesc());
							dtoPositionBudget.setPositionPlanDesc((position.getPositionPalnSetup().getPositionPlanDesc()));
							dtoPositionBudget.setPositionPlanId(position.getPositionPalnSetup().getPositionPlanId());
							dtoPositionBudget.setPositionPlanPrimaryId(position.getPositionPalnSetup().getId());
						}
						
						dtoDepartmentList.add(dtoPositionBudget);
					}
					dtoSearch.setRecords(dtoDepartmentList);
				}
			}
			log.debug("Search searchPosition Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
}
