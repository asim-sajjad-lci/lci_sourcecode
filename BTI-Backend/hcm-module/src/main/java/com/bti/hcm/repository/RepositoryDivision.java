/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Division;

/**
 * Description: Interface for Divison 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryDivision")
public interface RepositoryDivision extends JpaRepository<Division, Integer> {

	/**
	 * @param divisionId
	 * @param deleted
	 * @return
	 */
	public Division findByIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * @param deleted
	 * @return
	 */
	public List<Division> findByIsDeleted(Boolean deleted);
	
	/**
	 * @return
	 */
	@Query("select count(*) from Division d where d.isDeleted=false")
	public Integer getCountOfTotalDivision();
	
	

	/**
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<Division> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Division d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteDivision(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Division d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleDivision(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);


	/**
	 * @return
	 */
	public Division findTop1ByOrderByDivisionIdDesc();
	
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select d from Division d where (d.divisionId LIKE :searchKeyWord or d.divisionDescription LIKE :searchKeyWord  or d.phoneNumber LIKE :searchKeyWord or  d.email LIKE :searchKeyWord) and d.isDeleted=false")
	public List<Division> predictiveDivisionSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	

	/**
	 * @param deleted
	 * @return
	 */
	public List<Division> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from Division d where (d.divisionId LIKE :searchKeyWord or d.divisionDescription LIKE :searchKeyWord or d.phoneNumber LIKE :searchKeyWord or  d.email LIKE :searchKeyWord) and d.isDeleted=false")
	public Integer predictiveDivisionSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param divisionId
	 * @return
	 */
	@Query("select p from Division p where (p.divisionId =:divisionId) and p.isDeleted=false")
	public List<Division> findByDivisionId(@Param("divisionId")String divisionId);

	@Query("select d.divisionId from Division d where (d.divisionId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	public List<String> predictiveDivisionIdSearchWithPagination(@Param("searchKeyWord")String string);

	
	@Query("select d from Division d where (d.divisionId like :searchKeyWord) and d.isDeleted=false")
	public List<Division> predictiveDivisionAllIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select count(*) from Division d ")
	public Integer getCountOfTotalDivisions();
}
