package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.HelthInsurance;

public class DtoHelthInsurance extends DtoBase{

	
	private Integer id;
	private String helthInsuranceId;
	private Integer helthCoveragePrimaryTypeId;
	private String helthCoverageTypeId;
	private String helthCoverageDesc;
	private String desc;
	private String arbicDesc;
	private short frequency;
	private Integer helthInsurancePrimaryId;
	private Integer compnayInsurancePrimaryId;
	private String compnayInsuranceId;
	private String compnayInsuranceDesc;
	private String groupNumber;
	private Integer maxAge;
	private Integer minAge;
	private BigDecimal employeePay;
	private BigDecimal employerPay;
	private BigDecimal expenses;
	private BigDecimal maxCoverage;
	private List<DtoHelthInsurance> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getHelthInsuranceId() {
		return helthInsuranceId;
	}
	public void setHelthInsuranceId(String helthInsuranceId) {
		this.helthInsuranceId = helthInsuranceId;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getArbicDesc() {
		return arbicDesc;
	}
	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}
	public short getFrequency() {
		return frequency;
	}
	public void setFrequency(short frequency) {
		this.frequency = frequency;
	}
	public String getGroupNumber() {
		return groupNumber;
	}
	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}
	public Integer getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}
	public Integer getMinAge() {
		return minAge;
	}
	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}
	public BigDecimal getEmployeePay() {
		return employeePay;
	}
	public void setEmployeePay(BigDecimal employeePay) {
		this.employeePay = employeePay;
	}
	public BigDecimal getEmployerPay() {
		return employerPay;
	}
	public void setEmployerPay(BigDecimal employerPay) {
		this.employerPay = employerPay;
	}
	public BigDecimal getExpenses() {
		return expenses;
	}
	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}
	public BigDecimal getMaxCoverage() {
		return maxCoverage;
	}
	public void setMaxCoverage(BigDecimal maxCoverage) {
		this.maxCoverage = maxCoverage;
	}
	public List<DtoHelthInsurance> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoHelthInsurance> delete) {
		this.delete = delete;
	}
	public Integer getHelthCoveragePrimaryTypeId() {
		return helthCoveragePrimaryTypeId;
	}
	public void setHelthCoveragePrimaryTypeId(Integer helthCoveragePrimaryTypeId) {
		this.helthCoveragePrimaryTypeId = helthCoveragePrimaryTypeId;
	}
	public Integer getHelthInsurancePrimaryId() {
		return helthInsurancePrimaryId;
	}
	public void setHelthInsurancePrimaryId(Integer helthInsurancePrimaryId) {
		this.helthInsurancePrimaryId = helthInsurancePrimaryId;
	}
	public DtoHelthInsurance() {
	}
	
	public String getHelthCoverageTypeId() {
		return helthCoverageTypeId;
	}
	public void setHelthCoverageTypeId(String helthCoverageTypeId) {
		this.helthCoverageTypeId = helthCoverageTypeId;
	}
	public String getCompnayInsuranceId() {
		return compnayInsuranceId;
	}
	public void setCompnayInsuranceId(String compnayInsuranceId) {
		this.compnayInsuranceId = compnayInsuranceId;
	}
	
	public Integer getCompnayInsurancePrimaryId() {
		return compnayInsurancePrimaryId;
	}
	public void setCompnayInsurancePrimaryId(Integer compnayInsurancePrimaryId) {
		this.compnayInsurancePrimaryId = compnayInsurancePrimaryId;
	}
	public String getCompnayInsuranceDesc() {
		return compnayInsuranceDesc;
	}
	public void setCompnayInsuranceDesc(String compnayInsuranceDesc) {
		this.compnayInsuranceDesc = compnayInsuranceDesc;
	}
	
	
	public String getHelthCoverageDesc() {
		return helthCoverageDesc;
	}
	public void setHelthCoverageDesc(String helthCoverageDesc) {
		this.helthCoverageDesc = helthCoverageDesc;
	}
	public DtoHelthInsurance(HelthInsurance helthInsurance) {
		
		this.helthInsuranceId = helthInsurance.getHelthInsuranceId();
	}
}
