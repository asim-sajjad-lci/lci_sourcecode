package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.bti.hcm.model.SettingFamilyLeave;

public class DtoSettingFamilyLeave extends DtoBase {

	private Integer id;
	private short method;
	private short isThisPeriod;
	private Date periodStart;
	private List<DtoSettingFamilyLeave> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public short getMethod() {
		return method;
	}

	public void setMethod(short method) {
		this.method = method;
	}

	public short getIsThisPeriod() {
		return isThisPeriod;
	}

	public void setIsThisPeriod(short isThisPeriod) {
		this.isThisPeriod = isThisPeriod;
	}

	public Date getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}

	public List<DtoSettingFamilyLeave> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoSettingFamilyLeave> delete) {
		this.delete = delete;
	}

	public DtoSettingFamilyLeave() {

	}

	public DtoSettingFamilyLeave(SettingFamilyLeave settingFamilyLeave) {

	}
}
