package com.bti.hcm.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.GroupLayout.Alignment;
import javax.xml.transform.TransformerConfigurationException;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PaperSize;
import org.apache.poi.ss.usermodel.PrintOrientation;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFPrintSetup;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.AccountsTableAccumulation;
import com.bti.hcm.model.AuditTrialCodes;
import com.bti.hcm.model.Batches;
import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.BuildChecks;
import com.bti.hcm.model.BuildPayrollCheckByBatches;
import com.bti.hcm.model.BuildPayrollCheckByBenefits;
import com.bti.hcm.model.BuildPayrollCheckByDeductions;
import com.bti.hcm.model.BuildPayrollCheckByPayCodes;
import com.bti.hcm.model.CalculateChecks;
import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.Default;
import com.bti.hcm.model.Department;
import com.bti.hcm.model.Distribution;
import com.bti.hcm.model.DtoProcess;
import com.bti.hcm.model.Employee;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.FinancialDimensions;
import com.bti.hcm.model.Location;
import com.bti.hcm.model.ManualChecks;
import com.bti.hcm.model.MiscellaneousDetails;
import com.bti.hcm.model.MiscellaneousEmployee;
import com.bti.hcm.model.MiscellaneousHeader;
import com.bti.hcm.model.ParollTransactionOpenYearDistribution;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.PayrollAccounts;
import com.bti.hcm.model.PayrollTransactionOpenYearDetails;
import com.bti.hcm.model.PayrollTransactionOpenYearHeader;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.ProjectSetup;
import com.bti.hcm.model.TransactionEntry;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoEmployeePayrollDetails;
import com.bti.hcm.model.dto.DtoAuditTrialCodes;
import com.bti.hcm.model.dto.DtoBank;
import com.bti.hcm.model.dto.DtoBenefitCode;
import com.bti.hcm.model.dto.DtoBuildChecks;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByBenefits;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByDeductions;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByPayCodes;
import com.bti.hcm.model.dto.DtoCOAMainAccounts;
import com.bti.hcm.model.dto.DtoCalculateChecks;
import com.bti.hcm.model.dto.DtoCalculateChecksDisplay;
import com.bti.hcm.model.dto.DtoCompanyInfo;
import com.bti.hcm.model.dto.DtoDeductionCode;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoDistribrution;
import com.bti.hcm.model.dto.DtoEmployeeBenefitMaintenance;
import com.bti.hcm.model.dto.DtoEmployeeMasterHcm;
import com.bti.hcm.model.dto.DtoEmployeePayrollCSV;
import com.bti.hcm.model.dto.DtoManualChecks;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoProjectSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoEmployeeCalculateChecckReport;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoStatement;
import com.bti.hcm.repository.RepositoryAccountsTableAccumulation;
import com.bti.hcm.repository.RepositoryAuditTrialCodes;
import com.bti.hcm.repository.RepositoryBatches;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByBatches;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByBenefits;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByDeductions;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByPayCodes;
import com.bti.hcm.repository.RepositoryCalculateChecks;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryDefault;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryDistribution;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryEntityManager;
import com.bti.hcm.repository.RepositoryFinancialDimensions;
import com.bti.hcm.repository.RepositoryFinancialDimensionsValues;
import com.bti.hcm.repository.RepositoryLocation;
import com.bti.hcm.repository.RepositoryManualChecks;
import com.bti.hcm.repository.RepositoryParollTransactionOpenYearDistribution;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryPayrollAccounts;
import com.bti.hcm.repository.RepositoryPayrollTransactionOpenYearDetails;
import com.bti.hcm.repository.RepositoryPayrollTransactionOpenYearHeader;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositoryProjectSetup;
import com.bti.hcm.repository.RepositoryTransactionEntry;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;
import com.bti.hcm.service.helper.PaySlipReportHelper;
import com.bti.hcm.util.CommonUtils;
import com.bti.hcm.util.PreparePrintingCSV;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opencsv.CSVWriter;

import net.sf.jasperreports.engine.type.PropertyEvaluationTimeEnum;


@Service("serviceCalculateChecks")
public class ServiceCalculateChecks {

	static Logger log = Logger.getLogger(ServiceCalculateChecks.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceEmployeeMaster serviceEmployeeMaster;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	@Autowired
	RepositoryPayrollAccounts repositoryPayrollAccounts;
	
	@Autowired
	RepositoryDefault repositoryDefault;
	
	
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired
	RepositoryDepartment repositoryDepartment;
	
	@Autowired
	RepositoryFinancialDimensions repositoryFinancialDimensions;
	
	@Autowired
	RepositoryPayCode repositoryPayCode;
	
	@Autowired
	RepositoryPosition repositoryPosition;
	
	@Autowired
	RepositoryBuildChecks repositoryBuildChecks;
	
	@Autowired
	RepositoryLocation repositoryLocation;
	
	@Autowired
	RepositoryCalculateChecks repositoryCalculateChecks;
	
	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;
	
	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;
	
	@Autowired
	RepositoryManualChecks  repositoryManualChecks;
	
	@Autowired
	RepositoryFinancialDimensionsValues repositoryFinancialDimensionsValues;
	
	@Autowired
	RepositoryAccountsTableAccumulation repositoryAccountsTableAccumulation;
	
	@Autowired
	RepositoryAuditTrialCodes repositoryAuditTrialCodes;
	
	@Autowired
	RepositoryDistribution repositoryDistribution;
	
	@Autowired
	RepositoryPayrollTransactionOpenYearDetails repositoryPayrollTransactionOpenYearDetails;
	
	@Autowired
	RepositoryParollTransactionOpenYearDistribution repositoryParollTransactionOpenYearDistribution;
	
	@Autowired
	RepositoryPayrollTransactionOpenYearHeader repositoryPayrollTransactionOpenYearHeader;
	
	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired
	RepositoryBatches repositoryBatches;
	
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;
	
	@Autowired
	RepositoryTransactionEntry repositoryTransactionEntry;
	
	@Autowired
	RepositoryBuildPayrollCheckByPayCodes repositoryBuildPayrollCheckByPayCodes;
	
	@Autowired
	RepositoryBuildPayrollCheckByBenefits repositoryBuildPayrollCheckByBenefits;
	
	@Autowired
	RepositoryBuildPayrollCheckByDeductions repositoryBuildPayrollCheckByDeductions;
	
	@Autowired
	RepositoryBuildPayrollCheckByBatches repositoryBuildPayrollCheckByBatches;
	
	@Autowired
	RepositoryEntityManager repositoryEntityManager;

	@Autowired
	ServiceEmailHandler serviceEmailHandler;
	
	@Autowired
	RepositoryProjectSetup repositoryProjectSetup;

	@Value("${spring.bar.discard_decimal}")
	boolean discardDecimal;

	@Value("${spring.bar.payslip_path}")
	String payslipFilesPath;

	
	public DtoCalculateChecks saveOrUpdate(DtoCalculateChecks dtoCalculateChecksList) {
		log.info("enter into save or update CalculateChecks");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		repositoryCalculateChecks.deleteAllRecord(true, loggedInUserId);
		
		for (DtoCalculateChecks dtoCalculateChecks : dtoCalculateChecksList.getList()) {
			
			CalculateChecks calculateChecks=null;
			try {
				if (dtoCalculateChecks.getId() != null && dtoCalculateChecks.getId() > 0) {
					calculateChecks = repositoryCalculateChecks.findByIdAndIsDeleted(dtoCalculateChecks.getId(), false);
					calculateChecks.setUpdatedBy(loggedInUserId);
					calculateChecks.setUpdatedDate(new Date());
					
				} else {
					
					calculateChecks = new CalculateChecks();
					calculateChecks.setCreatedDate(new Date());
					
					Integer rowId = repositoryCalculateChecks.findAll().size();
					Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					calculateChecks.setRowId(increment);
				}
				
				EmployeeMaster employeeMaster = null;
				FinancialDimensions financialDimensions = null;
				
				if(dtoCalculateChecks.getEmployeeMaster()!=null) {
					employeeMaster = repositoryEmployeeMaster.findOne(dtoCalculateChecks.getEmployeeMaster().getEmployeeIndexId());
					calculateChecks.setDepartment(employeeMaster.getDepartment());
					calculateChecks.setPosition(employeeMaster.getPosition());
					calculateChecks.setLocation(employeeMaster.getLocation());
				}
				
				if(dtoCalculateChecks.getDimensions()!=null) {
					financialDimensions = repositoryFinancialDimensions.findOne(dtoCalculateChecks.getDimensions().getId());
				}
				
				calculateChecks.setCodeType(dtoCalculateChecks.getCodeType());
				calculateChecks.setAuditTransNum(dtoCalculateChecks.getAuditTransNum());
				calculateChecks.setEmployeeMaster(employeeMaster);
				calculateChecks.sethCMCodeSequence(dtoCalculateChecks.gethCMCodeSequence());
				calculateChecks.setDimensions(financialDimensions);
				calculateChecks.setTotalAmount(dtoCalculateChecks.getTotalAmount());
				calculateChecks.setCodeIndexID(dtoCalculateChecks.getCodeIndexID());
				
				repositoryCalculateChecks.saveAndFlush(calculateChecks);
			}catch (Exception e) {
				log.error(e);
			}
		}
		return dtoCalculateChecksList;
	}

	/*public DtoCalculateChecks saveOrUpdateOld(DtoCalculateChecks dtoCalculateChecks) {
		log.info("enter into save or update CalculateChecks");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		CalculateChecks calculateChecks=null;
		try {
			if (dtoCalculateChecks.getId() != null && dtoCalculateChecks.getId() > 0) {
				calculateChecks = repositoryCalculateChecks.findByIdAndIsDeleted(dtoCalculateChecks.getId(), false);
				calculateChecks.setUpdatedBy(loggedInUserId);
				calculateChecks.setUpdatedDate(new Date());
				
			} else {
				
				calculateChecks = new CalculateChecks();
				calculateChecks.setCreatedDate(new Date());
				
				Integer rowId = repositoryCalculateChecks.findAll().size();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				calculateChecks.setRowId(increment);
			}
			
			Default defaultData = null;
			EmployeeMaster employeeMaster = null;
			FinancialDimensions financialDimensions = null;
			TransactionEntryDetail transactionEntryDetail = null;
			
			if(dtoCalculateChecks.getDefault_indx()!=null) {
				defaultData = repositoryDefault.findOne(dtoCalculateChecks.getDefault_indx().getId());
				for (BuildChecks buildChecks : defaultData.getListBuildChecks()) {
					calculateChecks.setBuildCheck(buildChecks);
				
					if(buildChecks.getListBuildPayrollCheckByBenefits()!=null) {
						for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildChecks.getListBuildPayrollCheckByBenefits()) {
							calculateChecks.setCodeType(Short.parseShort("3"));
							calculateChecks.setTotalAmount(buildPayrollCheckByBenefits.getBenefitCode().getAmount());
						}
					}
				
				
				}
			}
			
			if(dtoCalculateChecks.getEmployeeMaster()!=null) {
				employeeMaster = repositoryEmployeeMaster.findOne(dtoCalculateChecks.getEmployeeMaster().getEmployeeIndexId());
				calculateChecks.setDepartment(employeeMaster.getDepartment());
				calculateChecks.setPosition(employeeMaster.getPosition());
				calculateChecks.setLocation(employeeMaster.getLocation());
			}
			
			if(dtoCalculateChecks.getDimensions()!=null) {
				financialDimensions = repositoryFinancialDimensions.findOne(dtoCalculateChecks.getDimensions().getId());
				transactionEntryDetail = repositoryTransactionEntryDetail.getByDimensions(dtoCalculateChecks.getDimensions().getId());
			}
			
			calculateChecks.setCodeType(dtoCalculateChecks.getCodeType());
			calculateChecks.setDefaultIndx(defaultData);
			calculateChecks.setCheckNumForPayRoll(dtoCalculateChecks.getCheckNumForPayRoll());
			calculateChecks.setAuditTransNum(dtoCalculateChecks.getAuditTransNum());
			calculateChecks.setEmployeeMaster(employeeMaster);
			calculateChecks.sethCMCodeSequence(dtoCalculateChecks.gethCMCodeSequence());
			calculateChecks.setDimensions(financialDimensions);
			
			
			
			repositoryCalculateChecks.saveAndFlush(calculateChecks);
			
			
		}catch (Exception e) {
			log.error(e);
			
		}
		return dtoCalculateChecks;
	}*/

	public DtoCalculateChecks delete(List<Integer> ids) {
		log.info("delete CalculateChecks Method");
		DtoCalculateChecks dtoCalculateChecks = new DtoCalculateChecks();
		dtoCalculateChecks
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("CALCULATE_CHECK_DELETED", false));
		dtoCalculateChecks
				.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("CALCULATE_CHECK_DELETED", false));
		List<DtoCalculateChecks> deleteList = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));

		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("CALCULATE_CHECK_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer positionId : ids) {
					CalculateChecks calculateChecks = repositoryCalculateChecks.findOne(positionId);
					if(calculateChecks!=null) {
						DtoCalculateChecks dtoCalculateChecksObj = new DtoCalculateChecks();
						dtoCalculateChecksObj.setId(positionId);
						repositoryCalculateChecks.deleteSingleCalculateChecks(true, loggedInUserId, calculateChecks.getId());
						deleteList.add(dtoCalculateChecksObj);	
					}else {
						inValidDelete = true;
					}
					
			}
			if(inValidDelete){
				dtoCalculateChecks.setMessageType(invlidDeleteMessage.toString());
				
			}else {
				dtoCalculateChecks.setMessageType("");
			}

			dtoCalculateChecks.setDelete(deleteList);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.info("Delete CalculateChecks :" + dtoCalculateChecks.getId());
		return dtoCalculateChecks;
	}


	public DtoSearch getAll(DtoSearch dtoSearch) {
		if (dtoSearch != null) {
			String searchWord = dtoSearch.getSearchKeyword();
			String condition = "";

			if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
				if (dtoSearch.getSortOn().equals("payCodeId")) {
					condition = dtoSearch.getSortOn();
				} else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			} else {
				condition += "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}

			dtoSearch.setTotalCount(
					this.repositoryCalculateChecks.predictiveCalculateChecksSearchTotalCount("%" + searchWord + "%"));
			List<CalculateChecks> calculateChecksList = null;
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

				if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
					calculateChecksList = this.repositoryCalculateChecks.predictiveCalculateChecksSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, "id"));
				}
				if (dtoSearch.getSortBy().equals("ASC")) {
					calculateChecksList = this.repositoryCalculateChecks.predictiveCalculateChecksSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.ASC, condition));
				} else if (dtoSearch.getSortBy().equals("DESC")) {
					calculateChecksList = this.repositoryCalculateChecks.predictiveCalculateChecksSearchWithPagination(
							"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, condition));
				}
			}

			List<DtoCalculateChecksDisplay> dtoCalculateChecksList = new ArrayList<>();
			for (CalculateChecks calculateChecks : calculateChecksList) {
				 DtoCalculateChecksDisplay dtoCalculateChecks = getDtoCalculateChecksDisplay(calculateChecks);
				dtoCalculateChecksList.add(dtoCalculateChecks);
			}

			dtoSearch.setRecords(dtoCalculateChecksList);
		}

		return dtoSearch;
	}

	public DtoCalculateChecksDisplay getDtoCalculateChecksDisplay(CalculateChecks calculateChecks) {

		DtoCalculateChecksDisplay dtoCalculateChecks = new DtoCalculateChecksDisplay();
		dtoCalculateChecks.setId(calculateChecks.getId());
		dtoCalculateChecks.setCheckNumForPayRoll(calculateChecks.getCheckNumForPayRoll());
		dtoCalculateChecks.setAuditTransNum(calculateChecks.getAuditTransNum());
		dtoCalculateChecks.sethCMCodeSequence(calculateChecks.gethCMCodeSequence());
		dtoCalculateChecks.setCodeType(calculateChecks.getCodeType());
		dtoCalculateChecks.setCodeIndexID(calculateChecks.getCodeIndexID());
		dtoCalculateChecks.setTotalAmount(calculateChecks.getTotalAmount());
		dtoCalculateChecks.setTotalActualAmount(calculateChecks.getTotalActualAmount());
		dtoCalculateChecks.setTotalPercentCode(calculateChecks.getTotalPercentCode());
		dtoCalculateChecks.setTotalHours(calculateChecks.getTotalHours());
		dtoCalculateChecks.setRateOfBaseOnCode(calculateChecks.getRateOfBaseOnCode());
		dtoCalculateChecks.setCodefromPerioddate(calculateChecks.getCodefromPerioddate());
		dtoCalculateChecks.setCodeToPerioddate(calculateChecks.getCodeToPerioddate());
		dtoCalculateChecks.setRecordStatus(calculateChecks.getRecordStatus());
		if(calculateChecks.getDefaultIndx()!=null) {
			dtoCalculateChecks.setDefaultIndxPrimaryId(calculateChecks.getDefaultIndx().getId());
			dtoCalculateChecks.setDefaultIndx(calculateChecks.getDefaultIndx().getDefaultID());
		}
		if(calculateChecks.getEmployeeMaster()!=null) {
			dtoCalculateChecks.setEmployeePrimaryId(calculateChecks.getEmployeeMaster().getEmployeeIndexId());
			dtoCalculateChecks.setEmployeeID(calculateChecks.getEmployeeMaster().getEmployeeId());
			dtoCalculateChecks.setEmployeeName(calculateChecks.getEmployeeMaster().getEmployeeFirstName() + " " +calculateChecks.getEmployeeMaster().getEmployeeMiddleName() +" "+calculateChecks.getEmployeeMaster().getEmployeeLastName());
		}
		if(calculateChecks.getDepartment()!=null) {
			dtoCalculateChecks.setDepartmentPrimaryId(calculateChecks.getDepartment().getId());
			dtoCalculateChecks.setDepartmentId(calculateChecks.getDepartment().getDepartmentId());
		}
		if(calculateChecks.getDimensions()!=null) {
			dtoCalculateChecks.setProjectPrimaryId(calculateChecks.getDimensions().getId());
			dtoCalculateChecks.setProjectId(calculateChecks.getDimensions().getDimensionDescription());
		}
		return dtoCalculateChecks;
	}
	
	public List<DtoCalculateChecksDisplay> getAllByDefautIdForProcess(DtoSearch dtoSearch) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		List<DtoCalculateChecksDisplay> dtoCalculateChecksList = new ArrayList<>();	
		if (dtoSearch != null) {
				
			List<BuildChecks> buildChecks = this.repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());

			
			
			List<EmployeeMaster> employeeMasters = null;
			List<Integer> departmentList= new ArrayList<>();
			List<Integer> employeeList= new ArrayList<>();
			if(!buildChecks.isEmpty()) {
				departmentList = buildChecks.get(0).getListDepartment().stream().map(department ->department.getId()).collect(Collectors.toList());	
				employeeList = buildChecks.get(0).getListEmployee().stream().map(employee -> employee.getEmployeeIndexId()).collect(Collectors.toList());
			}
			
			
			if(buildChecks!=null && !buildChecks.isEmpty()) {
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom() == null &&  dtoSearch.getTo()==null && buildChecks.get(0).getFromEmployeeId()!=null && 
						buildChecks.get(0).getFromEmployeeId()>0 && buildChecks.get(0).getToEmployeeId()!=null && buildChecks.get(0).getToEmployeeId()>0) {
					employeeMasters = repositoryEmployeeMaster.findbyFromAndToId(buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
				}else if(dtoSearch.isAll()) {
					if(!departmentList.isEmpty()) {
						
						if(!employeeList.isEmpty()) {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentListAndEmployeeList(departmentList,employeeList);
						}else {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentList(departmentList);
						}
					
					}else {
						employeeMasters = new ArrayList<>();
					}
					
				}
				
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom()!=null && dtoSearch.getTo()!=null) {
					if(dtoSearch.getSortOn().equals("1")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForEmployee(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
					if(dtoSearch.getSortOn().equals("2")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForDeduction(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
				}
				
				
				for (EmployeeMaster employeeMaster : employeeMasters) {
					
					DtoCalculateChecksDisplay dtoCalculateChecks = new DtoCalculateChecksDisplay();
					
					if(buildChecks.get(0).getListBuildPayrollCheckByPayCodes()!=null && !buildChecks.get(0).getListBuildPayrollCheckByPayCodes().isEmpty()) {
						dtoCalculateChecks.setCodeType((short)1);
						for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecks.get(0).getListBuildPayrollCheckByPayCodes()) {
							if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
								dtoCalculateChecks.setCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
								dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByPayCodes.getPayCode().getId());
								dtoCalculateChecks.setTotalAmount(buildPayrollCheckByPayCodes.getPayCode().getBaseOnPayCodeAmount());
							}
						}
					}
					Date date = buildChecks.get(0).getCreatedDate();
					 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
				        String time = localDateFormat.format(date);
					dtoCalculateChecks.setBuildDate(date);
					dtoCalculateChecks.setBuildtime(time);
					
					if(buildChecks.get(0).getListBuildPayrollCheckByDeductions()!=null && !buildChecks.get(0).getListBuildPayrollCheckByDeductions().isEmpty()) {
						dtoCalculateChecks.setCodeType((short)2);
						for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildChecks.get(0).getListBuildPayrollCheckByDeductions()) {
							if(buildPayrollCheckByDeductions.getDeductionCode()!=null) {
								dtoCalculateChecks.setCodeId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
								dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByDeductions.getDeductionCode().getId());
								dtoCalculateChecks.setTotalAmount(buildPayrollCheckByDeductions.getDeductionCode().getAmount());
							}
						}
					}
					if(buildChecks.get(0).getListBuildPayrollCheckByBenefits()!=null && !buildChecks.get(0).getListBuildPayrollCheckByBenefits().isEmpty()) {
						dtoCalculateChecks.setCodeType((short)3);
						for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildChecks.get(0).getListBuildPayrollCheckByBenefits()) {
							if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
								dtoCalculateChecks.setCodeId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
								dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByBenefits.getBenefitCode().getId());
								dtoCalculateChecks.setTotalAmount(buildPayrollCheckByBenefits.getBenefitCode().getAmount());
							}
						}
					}
					
					dtoCalculateChecks.setEmployeePrimaryId(employeeMaster.getEmployeeIndexId());
					dtoCalculateChecks.setEmployeeID(employeeMaster.getEmployeeId());
					dtoCalculateChecks.setEmployeeId(employeeMaster.getEmployeeId());
					dtoCalculateChecks.setEmployeeName(employeeMaster.getEmployeeFirstName() + " " +employeeMaster.getEmployeeMiddleName() +" "+employeeMaster.getEmployeeLastName());
					dtoCalculateChecks.setProjectId(employeeMaster.getEmployeeId());
					dtoCalculateChecks.setUserId(loggedInUserId);
					if(employeeMaster.getDepartment()!=null) {
						dtoCalculateChecks.setDepartmentPrimaryId(employeeMaster.getDepartment().getId());
						dtoCalculateChecks.setDepartmentId(employeeMaster.getDepartment().getDepartmentId());
						
						
						List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
						dtoCalculateChecks.setStatus("fail");
						for (PayrollAccounts payrollAccounts : listAccount) {
							if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
								String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
								if(accountnum!=null) {
									dtoCalculateChecks.setStatus("success");
								}else {
									dtoCalculateChecks.setStatus("fail");
								}
							}
							
						}
					}
					dtoCalculateChecksList.add(dtoCalculateChecks);
				}
			
			
			}
			
			
			

		}

		return dtoCalculateChecksList;
	}
	
	
	public DtoSearch getAllByDefautId(DtoSearch dtoSearch) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		if (dtoSearch != null) {
				
			List<BuildChecks> buildChecks = this.repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());

			List<DtoCalculateChecksDisplay> dtoCalculateChecksList = new ArrayList<>();	
			
			List<EmployeeMaster> employeeMasters = null;
			List<Integer> departmentList= new ArrayList<>();
			List<Integer> employeeList= new ArrayList<>();
			if(!buildChecks.isEmpty()) {
				departmentList = buildChecks.get(0).getListDepartment().stream().map(department ->department.getId()).collect(Collectors.toList());	
				employeeList = buildChecks.get(0).getListEmployee().stream().map(employee -> employee.getEmployeeIndexId()).collect(Collectors.toList());
			}
			
			
			if(buildChecks!=null && !buildChecks.isEmpty()) {
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom() == null &&  dtoSearch.getTo()==null && buildChecks.get(0).getFromEmployeeId()!=null && 
						buildChecks.get(0).getFromEmployeeId()>0 && buildChecks.get(0).getToEmployeeId()!=null && buildChecks.get(0).getToEmployeeId()>0) {
					employeeMasters = repositoryEmployeeMaster.findbyFromAndToId(buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
				}else if(dtoSearch.isAll()) {
					if(!departmentList.isEmpty()) {
						
						if(!employeeList.isEmpty()) {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentListAndEmployeeList(departmentList,employeeList);
						}else {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentList(departmentList);
						}
					
					}else {
						employeeMasters = new ArrayList<>();
					}
					
				}
				
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom()!=null && dtoSearch.getTo()!=null) {
					if(dtoSearch.getSortOn().equals("1")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForEmployee(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
					if(dtoSearch.getSortOn().equals("2")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForDeduction(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
				}
				
				
				for (EmployeeMaster employeeMaster : employeeMasters) {
				
					
					
					Integer serialNum = dtoSearch.getPageNumber() * dtoSearch.getPageSize();
					
					List<TransactionEntryDetail> list = repositoryTransactionEntryDetail.searchByEmployeeId12(employeeMaster.getEmployeeIndexId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
							Sort.Direction.DESC, "id"));
					
					// dtoSearch.setTotalCount(repositoryTransactionEntryDetail.searchByEmployeeId(employeeMaster.getEmployeeIndexId()).size());
					for (TransactionEntryDetail transactionEntryDetail : list) {
						
						DtoCalculateChecksDisplay dtoCalculateChecks = new DtoCalculateChecksDisplay();
						
						if(buildChecks.get(0).getListBuildPayrollCheckByPayCodes()!=null && !buildChecks.get(0).getListBuildPayrollCheckByPayCodes().isEmpty()) {
							dtoCalculateChecks.setCodeType((short)1);
							for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecks.get(0).getListBuildPayrollCheckByPayCodes()) {
								if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
									dtoCalculateChecks.setCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
									dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByPayCodes.getPayCode().getId());
									dtoCalculateChecks.setTotalAmount(buildPayrollCheckByPayCodes.getPayCode().getBaseOnPayCodeAmount());
								}
							}
						}
						
						
						
						
						
						
						/*List<EmployeeBenefitMaintenance> list1=this.repositoryEmployeeBenefitMaintenance.findByIsDeletedAndInactive(false, false);
						List<DtoEmployeeBenefitMaintenance> dtoEmployeeBenefitMaintenances=new ArrayList<>();
						if(list1!=null && !list1.isEmpty()) {
							for (EmployeeBenefitMaintenance employeeBenefitMaintenance : list1) {
								if(employeeBenefitMaintenance.getBenefitCode()!=null && employeeBenefitMaintenance.getBenefitCode().getId()>0) {
									DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance=new  DtoEmployeeBenefitMaintenance();
									dtoEmployeeBenefitMaintenance.setBenefitAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
									dtoEmployeeBenefitMaintenance.setId(employeeBenefitMaintenance.getBenefitCode().getId());
									dtoEmployeeBenefitMaintenances.add(dtoEmployeeBenefitMaintenance);
									dtoCalculateChecks.setDtoEmployeeBenefitMaintenance(dtoEmployeeBenefitMaintenance);
									dtoCalculateChecks.setBenefitAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
									dtoCalculateChecks.setId(employeeBenefitMaintenance.getBenefitCode().getId());
									dtoCalculateChecks.setCodeId(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
									
									
								}
							}
						}*/
						
						
						Date date = buildChecks.get(0).getCreatedDate();
						 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
					        String time = localDateFormat.format(date);
						dtoCalculateChecks.setBuildDate(date);
						dtoCalculateChecks.setBuildtime(time);
						dtoCalculateChecks.setNumberId(++serialNum);
						
						
						if(transactionEntryDetail.getBenefitCode()!=null) {
							dtoCalculateChecks.setCodeType((short)3);
							dtoCalculateChecks.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
							dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getBenefitCode().getId());
							dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getBenefitCode().getAmount());
						
						}
						
						if(transactionEntryDetail.getDeductionCode()!=null) {
							dtoCalculateChecks.setCodeType((short)2);
							dtoCalculateChecks.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
							dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getDeductionCode().getId());
							dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getDeductionCode().getAmount());
						}
						
						if(transactionEntryDetail.getDimensions()!=null) {
							dtoCalculateChecks.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());
						}
						
						if(transactionEntryDetail.getCode()!=null) {
							dtoCalculateChecks.setCodeType((short)1);
							dtoCalculateChecks.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
							dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getCode().getId());
							//dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getBaseOnPayCodeAmount());
							dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getPayRate());
						}
						
						dtoCalculateChecks.setEmployeePrimaryId(employeeMaster.getEmployeeIndexId());
						dtoCalculateChecks.setEmployeeID(employeeMaster.getEmployeeId());
						dtoCalculateChecks.setEmployeeId(employeeMaster.getEmployeeId());
						dtoCalculateChecks.setEmployeeName(employeeMaster.getEmployeeFirstName() + " " +employeeMaster.getEmployeeMiddleName() +" "+employeeMaster.getEmployeeLastName());
						
						dtoCalculateChecks.setUserId(loggedInUserId);
						if(employeeMaster.getDepartment()!=null) {
							dtoCalculateChecks.setDepartmentPrimaryId(employeeMaster.getDepartment().getId());
							dtoCalculateChecks.setDepartmentId(employeeMaster.getDepartment().getDepartmentId());
							
							
							/*List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
							dtoCalculateChecks.setStatus("fail");
							for (PayrollAccounts payrollAccounts : listAccount) {
								if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
									String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
									if(accountnum!=null) {
										dtoCalculateChecks.setStatus("success");
									}else {
										dtoCalculateChecks.setStatus("fail");
									}
								}
								
							}*/
							
							
							List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
							//fail
							dtoCalculateChecks.setStatus("Success");
							for (PayrollAccounts payrollAccounts : listAccount) {
								/*if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
									String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
									if(accountnum!=null) {
										dtoCalculateChecks.setStatus("success");
									}else {
										dtoCalculateChecks.setStatus("fail");
									}
								}*/
								
								/*if(payrollAccounts.getFinancialDimensions().getDimensionDescription().equals(transactionEntryDetail.getDimensions().getDimensionDescription())) {
									dtoCalculateChecks.setStatus("success");
								}else {
									dtoCalculateChecks.setStatus("fail");
								}*/
								PayCode payCode = null;
								BenefitCode benefitCode = null;
								DeductionCode deductionCode = null;
								if(payrollAccounts.getAccountType()==1) {
									payCode = repositoryPayCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
								}
								if(payrollAccounts.getAccountType()==2) {
									deductionCode = repositoryDeductionCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
								}
								if(payrollAccounts.getAccountType()==3) {
									benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
								}
								
							if(payrollAccounts!=null && transactionEntryDetail!=null && payrollAccounts.getDepartment().getId() == transactionEntryDetail.getEmployeeMaster().getDepartment().getId()) {
								if(payCode!=null && transactionEntryDetail.getCode()!=null && payCode.getPayCodeId().equals(transactionEntryDetail.getCode().getPayCodeId())) {
									dtoCalculateChecks.setStatus("Success");
									break;
								}else if(benefitCode!=null && transactionEntryDetail.getBenefitCode()!=null &&  benefitCode.getBenefitId().equals(transactionEntryDetail.getBenefitCode().getBenefitId())) {
									dtoCalculateChecks.setStatus("Success");
									break;
								}else if(deductionCode!=null && transactionEntryDetail.getDeductionCode()!=null && deductionCode.getDiductionId().equals(transactionEntryDetail.getDeductionCode().getDiductionId())){
									dtoCalculateChecks.setStatus("Success");
									break;
								}else {
									//fail
									dtoCalculateChecks.setStatus("Success");
								}
							}else {
								//fail
								dtoCalculateChecks.setStatus("Success");
							}
								
								
								
							}
							
							
						}
						
						
						
						dtoCalculateChecksList.add(dtoCalculateChecks);
					}
					
					
				}
			
			
			}
			
			
			if(dtoSearch.getSortOn()!=null) {
				if(dtoSearch.getSortOn().equals("employee")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByEmployee sortbyEmployee = e.new SortByEmployee(); 
					Collections.sort(dtoCalculateChecksList, sortbyEmployee);
				}
				
				if(dtoSearch.getSortOn().equals("department")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByDepartment sortByDepartment = e.new SortByDepartment(); 
					Collections.sort(dtoCalculateChecksList, sortByDepartment);
				}
				
				if(dtoSearch.getSortOn().equals("codeId")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByCodeId sortByCodeId = e.new SortByCodeId(); 
					Collections.sort(dtoCalculateChecksList, sortByCodeId);
				}
				
				if(dtoSearch.getSortOn().equals("projectId")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByProjectId sortByProjectId = e.new SortByProjectId(); 
					Collections.sort(dtoCalculateChecksList, sortByProjectId);
				}
			}
			dtoSearch.setTotalCount(dtoCalculateChecksList.size());
			dtoSearch.setRecords(dtoCalculateChecksList);
		}
		log.info("Search TransactionEntry Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	
	//working code to get combine transcation by employee
	public DtoSearch getAllByDefautId_Working(DtoSearch dtoSearch) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		if (dtoSearch != null) {
				
			List<BuildChecks> buildChecks = this.repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());

			List<DtoCalculateChecksDisplay> dtoCalculateChecksList = new ArrayList<>();	
			
			List<EmployeeMaster> employeeMasters = null;
			List<Integer> departmentList= new ArrayList<>();
			List<Integer> employeeList= new ArrayList<>();
			if(!buildChecks.isEmpty()) {
				departmentList = buildChecks.get(0).getListDepartment().stream().map(department ->department.getId()).collect(Collectors.toList());	
				employeeList = buildChecks.get(0).getListEmployee().stream().map(employee -> employee.getEmployeeIndexId()).collect(Collectors.toList());
			}
			
			
			if(buildChecks!=null && !buildChecks.isEmpty()) {
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom() == null &&  dtoSearch.getTo()==null && buildChecks.get(0).getFromEmployeeId()!=null && 
						buildChecks.get(0).getFromEmployeeId()>0 && buildChecks.get(0).getToEmployeeId()!=null && buildChecks.get(0).getToEmployeeId()>0) {
					employeeMasters = repositoryEmployeeMaster.findbyFromAndToId(buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
				}else if(dtoSearch.isAll()) {
					if(!departmentList.isEmpty()) {
						
						if(!employeeList.isEmpty()) {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentListAndEmployeeList(departmentList,employeeList);
						}else {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentList(departmentList);
						}
					
					}else {
						employeeMasters = new ArrayList<>();
					}
					
				}
				
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom()!=null && dtoSearch.getTo()!=null) {
					if(dtoSearch.getSortOn().equals("1")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForEmployee(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
					if(dtoSearch.getSortOn().equals("2")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForDeduction(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
				}
				
				
				for (EmployeeMaster employeeMaster : employeeMasters) {
					
					
					
					
					
					DtoCalculateChecksDisplay dtoCalculateChecks = new DtoCalculateChecksDisplay();
					
					if(buildChecks.get(0).getListBuildPayrollCheckByPayCodes()!=null && !buildChecks.get(0).getListBuildPayrollCheckByPayCodes().isEmpty()) {
						dtoCalculateChecks.setCodeType((short)1);
						for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecks.get(0).getListBuildPayrollCheckByPayCodes()) {
							if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
								dtoCalculateChecks.setCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
								dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByPayCodes.getPayCode().getId());
								dtoCalculateChecks.setTotalAmount(buildPayrollCheckByPayCodes.getPayCode().getBaseOnPayCodeAmount());
							}
						}
					}
					Date date = buildChecks.get(0).getCreatedDate();
					 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
				        String time = localDateFormat.format(date);
					dtoCalculateChecks.setBuildDate(date);
					dtoCalculateChecks.setBuildtime(time);
					
					/*if(buildChecks.get(0).getListBuildPayrollCheckByDeductions()!=null && !buildChecks.get(0).getListBuildPayrollCheckByDeductions().isEmpty()) {
						dtoCalculateChecks.setCodeType((short)2);
						for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildChecks.get(0).getListBuildPayrollCheckByDeductions()) {
							if(buildPayrollCheckByDeductions.getDeductionCode()!=null) {
								dtoCalculateChecks.setCodeId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
								dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByDeductions.getDeductionCode().getId());
								dtoCalculateChecks.setTotalAmount(buildPayrollCheckByDeductions.getDeductionCode().getAmount());
							}
						}
					}
					if(buildChecks.get(0).getListBuildPayrollCheckByBenefits()!=null && !buildChecks.get(0).getListBuildPayrollCheckByBenefits().isEmpty()) {
						dtoCalculateChecks.setCodeType((short)3);
						for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildChecks.get(0).getListBuildPayrollCheckByBenefits()) {
							if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
								dtoCalculateChecks.setCodeId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
								dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByBenefits.getBenefitCode().getId());
								dtoCalculateChecks.setTotalAmount(buildPayrollCheckByBenefits.getBenefitCode().getAmount());
							}
						}
					}*/
					
					Integer credit = 0;
					Integer debit = 0;
					List<TransactionEntryDetail> list = repositoryTransactionEntryDetail.searchByEmployeeId(employeeMaster.getEmployeeIndexId());
					for (TransactionEntryDetail transactionEntryDetail : list) {
						if(transactionEntryDetail.getBenefitCode()!=null) {
							dtoCalculateChecks.setCodeType((short)3);
							//dtoCalculateChecks.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
							dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getBenefitCode().getId());
							//dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getAmount());
						
							credit+=transactionEntryDetail.getAmount().intValue();
						}
						
						if(transactionEntryDetail.getDeductionCode()!=null) {
							dtoCalculateChecks.setCodeType((short)2);
							//dtoCalculateChecks.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
							dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getDeductionCode().getId());
							//dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getAmount());
							debit+=transactionEntryDetail.getAmount().intValue();
						}
						
						if(transactionEntryDetail.getDimensions()!=null) {
							dtoCalculateChecks.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());
						}
						
						/*if(transactionEntryDetail.getCode()!=null) {
							dtoCalculateChecks.setCodeType((short)1);
							dtoCalculateChecks.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
							dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getCode().getId());
							dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getAmount());
						}*/
					}
					int total = credit - debit;
					dtoCalculateChecks.setTotalAmount(BigDecimal.valueOf(total));
					if(total>0) {
						dtoCalculateChecks.setCodeType((short)3);
					}else {
						dtoCalculateChecks.setCodeType((short)2);
					}
					
					dtoCalculateChecks.setEmployeePrimaryId(employeeMaster.getEmployeeIndexId());
					dtoCalculateChecks.setEmployeeID(employeeMaster.getEmployeeId());
					dtoCalculateChecks.setEmployeeId(employeeMaster.getEmployeeId());
					dtoCalculateChecks.setEmployeeName(employeeMaster.getEmployeeFirstName() + " " +employeeMaster.getEmployeeMiddleName() +" "+employeeMaster.getEmployeeLastName());
					
					dtoCalculateChecks.setUserId(loggedInUserId);
					if(employeeMaster.getDepartment()!=null) {
						dtoCalculateChecks.setDepartmentPrimaryId(employeeMaster.getDepartment().getId());
						dtoCalculateChecks.setDepartmentId(employeeMaster.getDepartment().getDepartmentId());
						
						
						List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
						dtoCalculateChecks.setStatus("fail");
						for (PayrollAccounts payrollAccounts : listAccount) {
							if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
								String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
								if(accountnum!=null) {
									dtoCalculateChecks.setStatus("success");
								}else {
									dtoCalculateChecks.setStatus("fail");
								}
							}
							
						}
					}
					dtoCalculateChecksList.add(dtoCalculateChecks);
				}
			
			
			}
			
			
			

			dtoSearch.setRecords(dtoCalculateChecksList);
		}

		return dtoSearch;
	}


	public DtoCalculateChecksDisplay getById(Integer id) {
		CalculateChecks calculateChecks = repositoryCalculateChecks.findByIdAndIsDeleted(id, false);
		DtoCalculateChecksDisplay dtoCalculateChecks = null;
		if(calculateChecks!=null) {
			dtoCalculateChecks = getDtoCalculateChecksDisplay(calculateChecks);
		}
		
		return dtoCalculateChecks;
	}

	public DtoSearch getDistribrution(DtoSearch dtoSearch) {
		try {
			 List<DtoDistribrution> listDistribrution = getListDtoDistribrution(dtoSearch);
			 dtoSearch.setRecords(listDistribrution);	
		} catch (Exception e) {
		}
		return dtoSearch;
	}
	
	public List<DtoDistribrution> getListDtoDistribrutionForProcess(DtoSearch dtoSearch){
		dtoSearch.setAll(true);
		List<DtoCalculateChecksDisplay> listEmployee = getAllByDefautIdForProcess(dtoSearch);
		List<String> empoyee = listEmployee.stream().map(m->m.getEmployeePrimaryId().toString()).collect(Collectors.toList());
		dtoSearch.setIds(empoyee);
		List<DtoDistribrution> listDistribrution = new ArrayList<>();
		for (String id : dtoSearch.getIds()) {
			List<TransactionEntryDetail> list = repositoryTransactionEntryDetail.searchByEmployeeId(Integer.parseInt(id), new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			for (TransactionEntryDetail transactionEntryDetail : list) {	
				DtoDistribrution distribrution = new DtoDistribrution();
				if(transactionEntryDetail.getTransactionEntry()!=null) {
					distribrution.setTranscationId(transactionEntryDetail.getTransactionEntry().getId());	
				}
				
				//List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = 	repositoryEmployeePayCodeMaintenance.findByEmployeeId(Integer.parseInt(id));
				/*List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = 	repositoryEmployeePayCodeMaintenance.findByEmployeeId(Integer.parseInt(id));
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
					DtoEmployeeMasterHcm dtoEmployeeMasterHcm=new DtoEmployeeMasterHcm();
					distribrution.setTotalAmont(employeePayCodeMaintenance2.getAmount());
					distribrution.setEmployeeId(Integer.parseInt(employeePayCodeMaintenance2.getEmployeeMaster().getEmployeeId()));
					distribrution.setProjectId(Integer.parseInt(employeePayCodeMaintenance2.getEmployeeMaster().getEmployeeId()));
					distribrution.setEmployeeFirstName(employeePayCodeMaintenance2.getEmployeeMaster().getEmployeeFirstName());
					distribrution.setCodeId(employeePayCodeMaintenance2.getPayCode().getId());
					distribrution.setTransactionType(dtoSearch.getType());
					
					distribrution.setDescription(employeePayCodeMaintenance2.getPayCode().getDescription());
					distribrution.setEmployeeMasterHcm(dtoEmployeeMasterHcm);
				}*/
				
				if(transactionEntryDetail.getTransactionEntry()!=null && transactionEntryDetail.getTransactionEntry().getBatches()!=null) {
					distribrution.setBatchPrimaryId(transactionEntryDetail.getTransactionEntry().getBatches().getId());
				}
				
				EmployeeMaster	employeePayCodeMaintenance2 = 	repositoryEmployeeMaster.findByEmployeeIndexIdAndIsDeleted(Integer.parseInt(id), false);
				
				DtoEmployeeMasterHcm dtoEmployeeMasterHcm=new DtoEmployeeMasterHcm();
				//distribrution.setTotalAmont(employeePayCodeMaintenance2.getAmount());
				distribrution.setEmployeeId(employeePayCodeMaintenance2.getEmployeeId());
				distribrution.setEmployeePrimaryId(employeePayCodeMaintenance2.getEmployeeIndexId());
				distribrution.setProjectId(employeePayCodeMaintenance2.getEmployeeId());
				distribrution.setEmployeeFirstName(employeePayCodeMaintenance2.getEmployeeFirstName());
				//distribrution.setCodeId(employeePayCodeMaintenance2.getPayCode().getId());
				distribrution.setTransactionType(dtoSearch.getType());
				
				//distribrution.setDescription(employeePayCodeMaintenance2.getPayCode().getDescription());
				distribrution.setEmployeeMasterHcm(dtoEmployeeMasterHcm);
				
				
				//List<BuildChecks> buildChecks =repositoryBuildChecks.findByDefauiltId(Integer.parseInt(id));
				List<BuildChecks> buildChecks =repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());
				for (BuildChecks buildChecks2 : buildChecks) {
					DtoBuildChecks  dtoBuildChecks=new DtoBuildChecks();
					distribrution.setDefaultId(buildChecks2.getDefault1().getDefaultID());
					distribrution.setDefaultPrimaryId(buildChecks2.getDefault1().getId());
					distribrution.setBuildId(buildChecks2.getId());
					distribrution.setDtoBuildChecks(dtoBuildChecks);
					if(buildChecks2.getListBuildPayrollCheckByPayCodes()!=null && !buildChecks2.getListBuildPayrollCheckByPayCodes().isEmpty()) {
					PayCode payCode =	buildChecks2.getListBuildPayrollCheckByPayCodes().stream().findFirst().map(pay->pay.getPayCode()).orElseGet(null);
					if(payCode!=null) {
						distribrution.setCodeId(payCode.getPayCodeId());
						distribrution.setCodePrimaryId(payCode.getId());
						distribrution.setDescription(payCode.getDescription());
					}
					
					}
				}
			
				List<AuditTrialCodes> auditTrialCodes=repositoryAuditTrialCodes.findBySerialId(Integer.parseInt(id));
				for (AuditTrialCodes auditTrialCodes2 : auditTrialCodes) {
					DtoAuditTrialCodes dtoAuditTrialCodes=new DtoAuditTrialCodes();
					distribrution.setAuditTransactionNo(auditTrialCodes2.getNextTransectionSourceNumber());
					distribrution.setAuditTrialCodes(dtoAuditTrialCodes);
				}
				Department dept =  transactionEntryDetail.getDepartment();
				if (dept == null) {
					dept = new Department();
				}
				//List<PayrollAccounts> payrollAccountss=repositoryPayrollAccounts.getstatusByDepartment(transactionEntryDetail.getDepartment().getId());
				List<PayrollAccounts> payrollAccountss=repositoryPayrollAccounts.getstatusByDepartment(dept.getId());
				for (PayrollAccounts payrollAccounts2 : payrollAccountss) {
					DtoDepartment department=new DtoDepartment();
					distribrution.setDepartmentId(payrollAccounts2.getDepartment().getDepartmentId());
					distribrution.setDepartmentDescription(payrollAccounts2.getDepartment().getDepartmentDescription());
					distribrution.setDtoDepartment(department);
				}
				List<ManualChecks>manualChecks=repositoryManualChecks.findByEmployeeId(Integer.parseInt(id));
				for (ManualChecks manualChecks2 : manualChecks) {
					DtoManualChecks dtoManualChecks=new  DtoManualChecks();
					distribrution.setCheckNumber(manualChecks2.getCheckNumber());
					distribrution.setDtoManualChecks(dtoManualChecks);
				}
				//List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(transactionEntryDetail.getDepartment().getId());
				List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(dept.getId());
				for (PayrollAccounts payrollAccounts : listAccount) {
					if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
							DtoCOAMainAccounts coaMainAccounts = new DtoCOAMainAccounts();
							coaMainAccounts.setMainAccNumber(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
							coaMainAccounts.setMainAccDescription(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccDescription());
							coaMainAccounts.setAccountType(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getAccountType());
							distribrution.setAccountPrimaryId(payrollAccounts.getAccountsTableAccumulation().getId());
							distribrution.setAccounts(coaMainAccounts);
					}
				}
				if(transactionEntryDetail.getDeductionCode()!=null&& transactionEntryDetail.getDeductionCode().getListEmployeeDeductionMaintenance()!=null) {
					for (EmployeeDeductionMaintenance employeeDeductionMaintenance : transactionEntryDetail.getDeductionCode().getListEmployeeDeductionMaintenance()) {
						distribrution.setDebitAmount(employeeDeductionMaintenance.getDeductionAmount());
						distribrution.setPostingTypes((short)2);
					}
				}
				
				if(transactionEntryDetail.getBenefitCode()!=null && transactionEntryDetail.getBenefitCode().getListEmployeeBenefitMaintenance()!=null) {
					for (EmployeeBenefitMaintenance employeeBenefitMaintenance : transactionEntryDetail.getBenefitCode().getListEmployeeBenefitMaintenance()) {
						distribrution.setCreditAmount(employeeBenefitMaintenance.getBenefitAmount());
						distribrution.setPostingTypes((short)3);
					}
				}
				
				/*if(transactionEntryDetail.getBenefitCode()!=null) {
					distribrution.setCreditAmount(transactionEntryDetail.getBenefitCode().getAmount());
					distribrution.setPostingTypes((short)3);
				}*/
				/*if(transactionEntryDetail.getDeductionCode()!=null){
					distribrution.setDebitAmount(transactionEntryDetail.getDeductionCode().getAmount());
					distribrution.setPostingTypes((short)2);
				}*/
				
				listDistribrution.add(distribrution);
			}
		}
		return listDistribrution;
	}
	
	
	public List<DtoDistribrution> getListDtoDistribrution(DtoSearch dtoSearch){
		List<DtoDistribrution> listDistribrution = new ArrayList<>();
		for (String id : dtoSearch.getIds()) {
			List<TransactionEntryDetail> list = repositoryTransactionEntryDetail.searchByEmployeeId(Integer.parseInt(id), new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			for (TransactionEntryDetail transactionEntryDetail : list) {	
				DtoDistribrution distribrution = new DtoDistribrution();
				
				//List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = 	repositoryEmployeePayCodeMaintenance.findByEmployeeId(Integer.parseInt(id));
				/*List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = 	repositoryEmployeePayCodeMaintenance.findByEmployeeId(Integer.parseInt(id));
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
					DtoEmployeeMasterHcm dtoEmployeeMasterHcm=new DtoEmployeeMasterHcm();
					distribrution.setTotalAmont(employeePayCodeMaintenance2.getAmount());
					distribrution.setEmployeeId(Integer.parseInt(employeePayCodeMaintenance2.getEmployeeMaster().getEmployeeId()));
					distribrution.setProjectId(Integer.parseInt(employeePayCodeMaintenance2.getEmployeeMaster().getEmployeeId()));
					distribrution.setEmployeeFirstName(employeePayCodeMaintenance2.getEmployeeMaster().getEmployeeFirstName());
					distribrution.setCodeId(employeePayCodeMaintenance2.getPayCode().getId());
					distribrution.setTransactionType(dtoSearch.getType());
					
					distribrution.setDescription(employeePayCodeMaintenance2.getPayCode().getDescription());
					distribrution.setEmployeeMasterHcm(dtoEmployeeMasterHcm);
				}*/
				
				if(transactionEntryDetail.getTransactionEntry()!=null && transactionEntryDetail.getTransactionEntry().getBatches()!=null) {
					distribrution.setBatchPrimaryId(transactionEntryDetail.getTransactionEntry().getBatches().getId());
				}
				
				EmployeeMaster	employeePayCodeMaintenance2 = 	repositoryEmployeeMaster.findByEmployeeIndexIdAndIsDeleted(Integer.parseInt(id), false);
				
				DtoEmployeeMasterHcm dtoEmployeeMasterHcm=new DtoEmployeeMasterHcm();
				//distribrution.setTotalAmont(employeePayCodeMaintenance2.getAmount());
				distribrution.setEmployeeId(employeePayCodeMaintenance2.getEmployeeId());
				distribrution.setEmployeePrimaryId(employeePayCodeMaintenance2.getEmployeeIndexId());
				distribrution.setProjectId(employeePayCodeMaintenance2.getEmployeeId());
				distribrution.setEmployeeFirstName(employeePayCodeMaintenance2.getEmployeeFirstName());
				//distribrution.setCodeId(employeePayCodeMaintenance2.getPayCode().getId());
				//distribrution.setTransactionType(dtoSearch.getType());
				
				//distribrution.setDescription(employeePayCodeMaintenance2.getPayCode().getDescription());
				distribrution.setEmployeeMasterHcm(dtoEmployeeMasterHcm);
				
				
				DtoDepartment department=new DtoDepartment();
				distribrution.setDepartmentId(employeePayCodeMaintenance2.getDepartment().getDepartmentId());
				distribrution.setDepartmentDescription(employeePayCodeMaintenance2.getDepartment().getDepartmentDescription());
				distribrution.setDtoDepartment(department);
				
				//List<BuildChecks> buildChecks =repositoryBuildChecks.findByDefauiltId(Integer.parseInt(id));
				List<BuildChecks> buildChecks =repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());
				for (BuildChecks buildChecks2 : buildChecks) {
					DtoBuildChecks  dtoBuildChecks=new DtoBuildChecks();
					distribrution.setDefaultId(buildChecks2.getDefault1().getDefaultID());
					distribrution.setDefaultPrimaryId(buildChecks2.getDefault1().getId());
					distribrution.setBuildId(buildChecks2.getId());
					distribrution.setDtoBuildChecks(dtoBuildChecks);
					if(buildChecks2.getListBuildPayrollCheckByPayCodes()!=null && !buildChecks2.getListBuildPayrollCheckByPayCodes().isEmpty()) {
					PayCode payCode =	buildChecks2.getListBuildPayrollCheckByPayCodes().stream().findFirst().map(m->m.getPayCode()).orElseGet(null);
					if(payCode!=null) {
						distribrution.setCodeId(payCode.getPayCodeId());
						distribrution.setCodePrimaryId(payCode.getId());
						distribrution.setDescription(payCode.getDescription());
					}
					
					}
				}
				List<AuditTrialCodes> auditTrialCodes=repositoryAuditTrialCodes.findByDefaultId(dtoSearch.getId());
				for (AuditTrialCodes auditTrialCodes2 : auditTrialCodes) {
					DtoAuditTrialCodes dtoAuditTrialCodes=new DtoAuditTrialCodes();
					distribrution.setAuditTransactionNo(auditTrialCodes2.getNextTransectionSourceNumber());
					distribrution.setAuditTransactionSourceNo(auditTrialCodes2.getSourceDocment());
					distribrution.setAuditTrialCodes(dtoAuditTrialCodes);
				}
				List<PayrollAccounts> payrollAccountss=repositoryPayrollAccounts.getstatusByDepartment(transactionEntryDetail.getDepartment().getId());
				/*for (PayrollAccounts payrollAccounts2 : payrollAccountss) {
					DtoDepartment department=new DtoDepartment();
					distribrution.setDepartmentId(payrollAccounts2.getDepartment().getDepartmentId());
					distribrution.setDepartmentDescription(payrollAccounts2.getDepartment().getDepartmentDescription());
					distribrution.setDtoDepartment(department);
				}*/
				List<ManualChecks>manualChecks=repositoryManualChecks.findByEmployeeId(Integer.parseInt(id));
				for (ManualChecks manualChecks2 : manualChecks) {
					DtoManualChecks dtoManualChecks=new  DtoManualChecks();
					distribrution.setCheckNumber(manualChecks2.getCheckNumber());
					distribrution.setDtoManualChecks(dtoManualChecks);
				}
				List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(transactionEntryDetail.getDepartment().getId());
				for (PayrollAccounts payrollAccounts : listAccount) {
					if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
							DtoCOAMainAccounts coaMainAccounts = new DtoCOAMainAccounts();
							coaMainAccounts.setMainAccNumber(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
							coaMainAccounts.setMainAccDescription(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccDescription());
							coaMainAccounts.setAccountType(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getAccountType());
							distribrution.setAccountPrimaryId(payrollAccounts.getAccountsTableAccumulation().getId());
							distribrution.setAccounts(coaMainAccounts);
					}
				}
				
				
				if(transactionEntryDetail.getDeductionCode()!=null&& transactionEntryDetail.getDeductionCode().getListEmployeeDeductionMaintenance()!=null) {
				for (EmployeeDeductionMaintenance employeeDeductionMaintenance : transactionEntryDetail.getDeductionCode().getListEmployeeDeductionMaintenance()) {
						distribrution.setDebitAmount(transactionEntryDetail.getAmount());
						distribrution.setPostingTypes((short)2);
						distribrution.setTransactionType((short)2);
						
						distribrution.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
						distribrution.setCodePrimaryId(transactionEntryDetail.getDeductionCode().getId());
						distribrution.setDescription(transactionEntryDetail.getDeductionCode().getDiscription());
					}
				}
				
				if(transactionEntryDetail.getBenefitCode()!=null &&transactionEntryDetail.getBenefitCode().getListEmployeeBenefitMaintenance()!=null) {
					for (EmployeeBenefitMaintenance employeeBenefitMaintenance : transactionEntryDetail.getBenefitCode().getListEmployeeBenefitMaintenance()){
						distribrution.setCreditAmount(transactionEntryDetail.getAmount());
						distribrution.setPostingTypes((short)3);
						distribrution.setTransactionType((short)3);
						
						distribrution.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
						distribrution.setCodePrimaryId(transactionEntryDetail.getBenefitCode().getId());
						distribrution.setDescription(transactionEntryDetail.getBenefitCode().getDesc());
						
					}
				}
				
				
				
				/*if(transactionEntryDetail.getBenefitCode()!=null) {
					distribrution.setCreditAmount(transactionEntryDetail.getBenefitCode().getAmount());
					distribrution.setPostingTypes((short)3);
				}*/
				/*if(transactionEntryDetail.getDeductionCode()!=null){
					distribrution.setDebitAmount(transactionEntryDetail.getDeductionCode().getAmount());
					distribrution.setPostingTypes((short)2);
				}*/
				listDistribrution.add(distribrution);
			}
		}
		return listDistribrution;
	}

	
	public List<DtoDistribrution> getListDtoDistribrutionNew(DtoSearch dtoSearch){
		List<DtoDistribrution> listDistribrution = new ArrayList<>();
		for (String id : dtoSearch.getIds()) {
		
		List<TransactionEntryDetail> list = repositoryTransactionEntryDetail.searchByEmployeeId(Integer.parseInt(id), new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
		EmployeeMaster employeeMaster = 	repositoryEmployeeMaster.findByEmployeeIndexIdAndIsDeleted(Integer.valueOf(id), false);
			
			List<String> accNum = new ArrayList<>();
			
			List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
			for (PayrollAccounts payrollAccounts : listAccount) {
				DtoDistribrution distribrution1 = new DtoDistribrution();
				
				if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
						
					if(!accNum.contains(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber())) {
						DtoCOAMainAccounts coaMainAccounts = new DtoCOAMainAccounts();
						coaMainAccounts.setMainAccNumber(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
						coaMainAccounts.setMainAccDescription(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccDescription());
						coaMainAccounts.setAccountType(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getAccountType());
						distribrution1.setAccountPrimaryId(payrollAccounts.getAccountsTableAccumulation().getId());
						distribrution1.setAccounts(coaMainAccounts);
						accNum.add(payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
						
						List<BuildChecks> buildChecks =repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());
						for (BuildChecks buildChecks2 : buildChecks) {
							DtoBuildChecks  dtoBuildChecks=new DtoBuildChecks();
							distribrution1.setDefaultId(buildChecks2.getDefault1().getDefaultID());
							distribrution1.setDefaultPrimaryId(buildChecks2.getDefault1().getId());
							distribrution1.setBuildId(buildChecks2.getId());
							distribrution1.setDtoBuildChecks(dtoBuildChecks);
							if(buildChecks2.getListBuildPayrollCheckByPayCodes()!=null && !buildChecks2.getListBuildPayrollCheckByPayCodes().isEmpty()) {
							PayCode payCode =	buildChecks2.getListBuildPayrollCheckByPayCodes().stream().findFirst().map(m->m.getPayCode()).orElse(null);
							if(payCode!=null) {
								distribrution1.setCodeId(payCode.getPayCodeId());
								distribrution1.setCodePrimaryId(payCode.getId());
								distribrution1.setDescription(payCode.getDescription());
							}
							
							}
						}
						
						DtoDepartment department=new DtoDepartment();
						distribrution1.setDepartmentId(payrollAccounts.getDepartment().getDepartmentId());
						distribrution1.setDepartmentDescription(payrollAccounts.getDepartment().getDepartmentDescription());
						distribrution1.setDtoDepartment(department);
						
						EmployeeMaster	employeePayCodeMaintenance2 = 	repositoryEmployeeMaster.findByEmployeeIndexIdAndIsDeleted(Integer.parseInt(id), false);
						DtoEmployeeMasterHcm dtoEmployeeMasterHcm=new DtoEmployeeMasterHcm();
						distribrution1.setEmployeeId(employeePayCodeMaintenance2.getEmployeeId());
						distribrution1.setEmployeePrimaryId(employeePayCodeMaintenance2.getEmployeeIndexId());
						distribrution1.setProjectId(employeePayCodeMaintenance2.getEmployeeId());
						distribrution1.setEmployeeFirstName(employeePayCodeMaintenance2.getEmployeeFirstName());
						distribrution1.setTransactionType(dtoSearch.getType());
						distribrution1.setEmployeeMasterHcm(dtoEmployeeMasterHcm);
					
						List<AuditTrialCodes> auditTrialCodes=repositoryAuditTrialCodes.findBySerialId(Integer.parseInt(id));
						for (AuditTrialCodes auditTrialCodes2 : auditTrialCodes) {
							DtoAuditTrialCodes dtoAuditTrialCodes=new DtoAuditTrialCodes();
							distribrution1.setAuditTransactionNo(auditTrialCodes2.getNextTransectionSourceNumber());
							distribrution1.setAuditTrialCodes(dtoAuditTrialCodes);
						}
						
						List<ManualChecks>manualChecks=repositoryManualChecks.findByEmployeeId(Integer.parseInt(id));
						for (ManualChecks manualChecks2 : manualChecks) {
							DtoManualChecks dtoManualChecks=new  DtoManualChecks();
							distribrution1.setCheckNumber(manualChecks2.getCheckNumber());
							distribrution1.setDtoManualChecks(dtoManualChecks);
						}
						
						BigDecimal credit = new BigDecimal("0.0");
						BigDecimal debit = new BigDecimal("0.0");
						
						List<TransactionEntryDetail> listTransactionEntryDetail = list.stream().filter(detail->detail.getEmployeeMaster().getDepartment().getDepartmentId().equals(payrollAccounts.getDepartment().getDepartmentId())).collect(Collectors.toList());
						for (TransactionEntryDetail transactionEntryDetail : listTransactionEntryDetail) {
							if(transactionEntryDetail.getDeductionCode()!=null) {
								BigDecimal b =transactionEntryDetail.getDeductionCode().getAmount();
								debit = debit.add(b);
							}
							if(transactionEntryDetail.getBenefitCode()!=null) {
								BigDecimal b =transactionEntryDetail.getBenefitCode().getAmount();
								credit = credit.add(b);
							}
						}
						distribrution1.setCreditAmount(credit);
						distribrution1.setDebitAmount(debit);
						listDistribrution.add(distribrution1);
					}	
				}	
			}
		}
		return listDistribrution;
	}
	
	public DtoProcess process(DtoProcess dtoProcess) {
		
         	DtoSearch dtoSearch = new DtoSearch();
         	dtoSearch.setIds(dtoProcess.getIds());
         	dtoSearch.setType(dtoSearch.getType());
         	dtoSearch.setPageNumber(dtoProcess.getPageNumber());
         	dtoSearch.setPageSize(dtoProcess.getPageSize());
         	dtoSearch.setId(dtoProcess.getId());
         	Date date = dtoProcess.getPostingDate();
         	log.info("Date is:--->"+date);
         	//dtoSearch = getDistribrution(dtoSearch);
         	List<DtoDistribrution> listDistribrution = getListDtoDistribrutionForProcess(dtoSearch);
         	for (DtoDistribrution dtoDistribrution : listDistribrution) {
         		Distribution distribution= new Distribution();
         		if(dtoDistribrution.getCheckNumber()!=null) {
         			distribution.setCheckNumber(dtoDistribrution.getCheckNumber().toString());	
         		}
         		distribution.setPostingDate(dtoDistribrution.getPostingDate());
                distribution.setCodeId(dtoDistribrution.getCodePrimaryId());
                distribution.setPostingType(dtoDistribrution.getPostingTypes());
                distribution.setDebit(dtoDistribrution.getDebitAmount());
                distribution.setCredit(dtoDistribrution.getCreditAmount());
                //distribution.setPostingDate(dtoDistribrution.getPostingDate());
                repositoryDistribution.saveAndFlush(distribution);
                distribution.setAccountNumberSeq(dtoDistribrution.getAccountNumberSeq());
                
                TransactionEntry transactionEntry = null;
                if(dtoDistribrution.getTranscationId()!=null) {
                	transactionEntry = repositoryTransactionEntry.findByIdAndIsDeleted(dtoDistribrution.getTranscationId(), false);	
                }
                
                distribution.setTransactionEntry(transactionEntry);
                
                Default default1=null;
                if(dtoDistribrution.getDefaultPrimaryId()!=null && dtoDistribrution.getDefaultPrimaryId()>0) {
                	default1=repositoryDefault.findOne(dtoDistribrution.getDefaultPrimaryId());
                }
                
                EmployeeMaster employeeMaster=null;
                if(dtoDistribrution.getEmployeePrimaryId()!=null  && dtoDistribrution.getEmployeePrimaryId()>0) {
                	employeeMaster=repositoryEmployeeMaster.findOne(dtoDistribrution.getEmployeePrimaryId());
                }
                
                BuildChecks buildChecks=null;
                if(dtoDistribrution.getBuildId()!=null && dtoDistribrution.getBuildId()>0) {
                	buildChecks=repositoryBuildChecks.findOne(dtoDistribrution.getBuildId());
                }
                
                AccountsTableAccumulation accountsTableAccumulation=null;
                if(dtoDistribrution.getAccountPrimaryId()!=null && dtoDistribrution.getAccountPrimaryId()>0) {
                	accountsTableAccumulation=repositoryAccountsTableAccumulation.findOne(dtoDistribrution.getAccountPrimaryId());
                	
                }
                AuditTrialCodes auditTrialCodes=null;
                if(dtoDistribrution.getAuditTransactionNo()!=null && dtoDistribrution.getAuditTransactionNo()>0) {
                	auditTrialCodes=repositoryAuditTrialCodes.findOne(dtoDistribrution.getAuditTransactionNo());
                }
                distribution.setDefault1(default1);
                distribution.setEmployeeMaster(employeeMaster);
                distribution.setBuildChecks(buildChecks);
                distribution.setAccountsTableAccumulation(accountsTableAccumulation); 
                distribution.setAuditTrialCodes(auditTrialCodes);
                distribution.setCreatedDate(new Date());
         		distribution.setPostingDate(date);
         		repositoryDistribution.saveAndFlush(distribution);

         		Department department = null;
         		if(employeeMaster!=null && employeeMaster.getDepartment()!=null) {
         			department = repositoryDepartment.findByIdAndIsDeleted(employeeMaster.getDepartment().getId(), false);	
         		}
         		Position position=null;
         		if(employeeMaster!=null && employeeMaster.getPosition()!=null) {
         			position=repositoryPosition.findByIdAndIsDeleted(employeeMaster.getPosition().getId(), false);
         		}
         		
         		Location location=null;
         		if(employeeMaster!=null && employeeMaster.getLocation()!=null) {
         			location=repositoryLocation.findByIdAndIsDeleted(employeeMaster.getLocation().getId(), false);
         		}
         		/*Batches batches=null;
         		if(batches!=null) {
         			batches=repositoryBatches.findByIdAndIsDeleted(batches.getId(), false);
         		}
         		FinancialDimensionsValues financialDimensionsValues=null;
         		if(financialDimensionsValues!=null) {
         			financialDimensionsValues=repositoryFinancialDimensionsValues.findByIdAndIsDeleted(financialDimensionsValues.getId(),false);
         		}*/
         		
         		PayrollTransactionOpenYearDetails payrollTransactionOpenYearDetails = new PayrollTransactionOpenYearDetails();
         		payrollTransactionOpenYearDetails.setEmployeeMaster(employeeMaster);
         		payrollTransactionOpenYearDetails.setDepartment(department);
         		payrollTransactionOpenYearDetails.setPosition(position);
         		payrollTransactionOpenYearDetails.setLocation(location);
         		/*payrollTransactionOpenYearDetails.setBatches(batches);
         		payrollTransactionOpenYearDetails.setFinancialDimensionsValues(financialDimensionsValues);*/
         		
         		
         		if(dtoDistribrution.getDebitAmount()!=null) {
         			payrollTransactionOpenYearDetails.setTotalActualAmount(dtoDistribrution.getDebitAmount());
         		}else {
         			payrollTransactionOpenYearDetails.setTotalActualAmount(dtoDistribrution.getCreditAmount());
         		}
         		payrollTransactionOpenYearDetails.setCodeIndexId(dtoDistribrution.getCodePrimaryId());
         		payrollTransactionOpenYearDetails.setCodeType(dtoDistribrution.getTransactionType());
         		
         		/*if(dtoDistribrution.getBatchPrimaryId()!=null) {
         			Batches batches = repositoryBatches.findOne(dtoDistribrution.getBatchPrimaryId());
         		
         			if(batches!=null) {
         				batches.setStatus((byte)2);
             			repositoryBatches.saveAndFlush(batches);	
         			}
         			
         		}*/
         		
         	if(buildChecks!=null) {
         		for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildChecks.getListBuildPayrollCheckByBatches()) {
         			Batches batches = buildPayrollCheckByBatches.getBatches();
         			if(batches!=null) {
         				batches.setStatus((byte)2);
             			repositoryBatches.saveAndFlush(batches);
         			}
				}
         	}
         		
         		repositoryPayrollTransactionOpenYearDetails.saveAndFlush(payrollTransactionOpenYearDetails);
         		
         		
         		ParollTransactionOpenYearDistribution parollTransactionOpenYearDistribution = new ParollTransactionOpenYearDistribution();
         		parollTransactionOpenYearDistribution.setDepartment(department);
         		parollTransactionOpenYearDistribution.setPosition(position);
         		//payrollTransactionOpenYearDetails.setFinancialDimensionsValues(financialDimensionsValues);
         		repositoryParollTransactionOpenYearDistribution.saveAndFlush(parollTransactionOpenYearDistribution);
         		
         		PayrollTransactionOpenYearHeader payrollTransactionOpenYearHeader=new PayrollTransactionOpenYearHeader();
         		payrollTransactionOpenYearHeader.setDefaults(default1);
         		repositoryPayrollTransactionOpenYearHeader.saveAndFlush(payrollTransactionOpenYearHeader);
			}
		
		return dtoProcess;
	}
	
	
	public DtoSearch getAllDisctrubution(DtoProcess dtoProcess) {
     	DtoSearch dtoSearch = new DtoSearch();
		List<Distribution> list = repositoryDistribution.getAllList(dtoProcess.getId());
     	List<DtoDistribrution> listDtoDistribrution = new ArrayList<>();
		for (Distribution distributionList : list) {
			DtoDistribrution distribrution = new DtoDistribrution();
			if(distributionList.getEmployeeMaster()!=null) {
				distribrution.setEmployeeId(distributionList.getEmployeeMaster().getEmployeeId());
				distribrution.setEmployeeFirstName(distributionList.getEmployeeMaster().getEmployeeFirstName()+" "+distributionList.getEmployeeMaster().getEmployeeLastName());
				distribrution.setProjectId(distributionList.getEmployeeMaster().getEmployeeId());
				if(distributionList.getEmployeeMaster().getDepartment()!=null) {
					distribrution.setDepartmentId(distributionList.getEmployeeMaster().getDepartment().getDepartmentId());
					distribrution.setDescription(distributionList.getEmployeeMaster().getDepartment().getDepartmentDescription());
				}
			
			}
			distribrution.setCodePrimaryId(distributionList.getCodeId());
			distribrution.setPostingTypes(distributionList.getPostingType());
			distribrution.setCreditAmount(distributionList.getCredit());
			distribrution.setDebitAmount(distributionList.getDebit());
			distribrution.setAccountNumberSeq(distributionList.getAccountNumberSeq());
			
			
			distribrution.setCheckNumberDisplay(distributionList.getCheckNumber());
			
			if(distributionList.getBuildChecks()!=null) {
				DtoBuildChecks buildChecks = new DtoBuildChecks();
				buildChecks.setCheckByUserId(buildChecks.getCheckByUserId());
				distribrution.setDtoBuildChecks(buildChecks);
			}
			
			if(distributionList.getDefault1()!=null) {
				distribrution.setDefaultId(distributionList.getDefault1().getDefaultID());
			}
			
			if(distributionList.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
				DtoCOAMainAccounts accounts = new DtoCOAMainAccounts();
				accounts.setMainAccNumber(distributionList.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
			}
			listDtoDistribrution.add(distribrution);
			
		}
		dtoSearch.setRecords(listDtoDistribrution);
	return dtoSearch;
	}
	
	
	public DtoSearch getAllDefaultBaseOnDisctrbution() {
     	DtoSearch dtoSearch = new DtoSearch();
		List<Distribution> list = repositoryDistribution.getAll();
     	List<DtoDistribrution> listDtoDistribrution = new ArrayList<>();
		for (Distribution distributionList : list) {
			DtoDistribrution distribrution = new DtoDistribrution();
			if(distributionList.getEmployeeMaster()!=null) {
				distribrution.setEmployeeId(distributionList.getEmployeeMaster().getEmployeeId());
				distribrution.setEmployeeFirstName(distributionList.getEmployeeMaster().getEmployeeFirstName()+" "+distributionList.getEmployeeMaster().getEmployeeLastName());
				distribrution.setProjectId(distributionList.getEmployeeMaster().getEmployeeId());
				if(distributionList.getEmployeeMaster().getDepartment()!=null) {
					distribrution.setDepartmentId(distributionList.getEmployeeMaster().getDepartment().getDepartmentId());
					distribrution.setDescription(distributionList.getEmployeeMaster().getDepartment().getDepartmentDescription());
				}
			
			}
			distribrution.setCodePrimaryId(distributionList.getCodeId());
			distribrution.setPostingTypes(distributionList.getPostingType());
			distribrution.setCreditAmount(distributionList.getCredit());
			distribrution.setDebitAmount(distributionList.getDebit());
			distribrution.setAccountNumberSeq(distributionList.getAccountNumberSeq());
			
			
			distribrution.setCheckNumberDisplay(distributionList.getCheckNumber());
			
			if(distributionList.getBuildChecks()!=null) {
				DtoBuildChecks buildChecks = new DtoBuildChecks();
				buildChecks.setCheckByUserId(buildChecks.getCheckByUserId());
				distribrution.setDtoBuildChecks(buildChecks);
			}
			
			if(distributionList.getDefault1()!=null) {
				distribrution.setDefaultId(distributionList.getDefault1().getDefaultID());
			}
			
			if(distributionList.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
				DtoCOAMainAccounts accounts = new DtoCOAMainAccounts();
				accounts.setMainAccNumber(distributionList.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
			}
			listDtoDistribrution.add(distribrution);
			
		}
		dtoSearch.setRecords(listDtoDistribrution);
	return dtoSearch;
	}
	
	public DtoSearch getAllDisctrubutionNew() {
     	DtoSearch dtoSearch = new DtoSearch();
		List<Distribution> list = repositoryDistribution.getAll();
     	List<DtoDistribrution> listDtoDistribrution = new ArrayList<>();
		for (Distribution distributionList : list) {
			DtoDistribrution distribrution = new DtoDistribrution();
			if(distributionList.getEmployeeMaster()!=null) {
				distribrution.setEmployeeId(distributionList.getEmployeeMaster().getEmployeeId());
				distribrution.setEmployeeFirstName(distributionList.getEmployeeMaster().getEmployeeFirstName()+" "+distributionList.getEmployeeMaster().getEmployeeLastName());
				distribrution.setProjectId(distributionList.getEmployeeMaster().getEmployeeId());
				if(distributionList.getEmployeeMaster().getDepartment()!=null) {
					distribrution.setDepartmentId(distributionList.getEmployeeMaster().getDepartment().getDepartmentId());
					distribrution.setDescription(distributionList.getEmployeeMaster().getDepartment().getDepartmentDescription());
				}
			
			}
			distribrution.setCodePrimaryId(distributionList.getCodeId());
			distribrution.setPostingTypes(distributionList.getPostingType());
			distribrution.setCreditAmount(distributionList.getCredit());
			distribrution.setDebitAmount(distributionList.getDebit());
			distribrution.setAccountNumberSeq(distributionList.getAccountNumberSeq());
			
			
			distribrution.setCheckNumberDisplay(distributionList.getCheckNumber());
			
			if(distributionList.getBuildChecks()!=null) {
				DtoBuildChecks buildChecks = new DtoBuildChecks();
				buildChecks.setCheckByUserId(buildChecks.getCheckByUserId());
				distribrution.setDtoBuildChecks(buildChecks);
			}
			
			if(distributionList.getDefault1()!=null) {
				distribrution.setDefaultId(distributionList.getDefault1().getDefaultID());
			}
			
			if(distributionList.getAccountsTableAccumulation()!=null && distributionList.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
				DtoCOAMainAccounts accounts = new DtoCOAMainAccounts();
				accounts.setMainAccNumber(distributionList.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
			}
			listDtoDistribrution.add(distribrution);
			
		}
		dtoSearch.setRecords(listDtoDistribrution);
	return dtoSearch;
	}

	public DtoSearch getAllPaySlipForReport() {
     	DtoSearch dtoSearch = new DtoSearch();
		List<Distribution> list = repositoryDistribution.getAll();
		List<DtoDistribrution> listDtoDistribrution = getDtoDistribrution(list);
		dtoSearch.setRecords(listDtoDistribrution);
		return dtoSearch;
	}
	
	public DtoSearch getAllPaySlipForReportByEmployeeId(Integer id) {
     	DtoSearch dtoSearch = new DtoSearch();
		List<Distribution> list = repositoryDistribution.getAllByEmployeeId(id);
		List<DtoDistribrution> listDtoDistribrution = getDtoDistribrution(list);
		dtoSearch.setRecords(listDtoDistribrution);
		return dtoSearch;
	}
	
	public List<DtoDistribrution> getDtoDistribrution(List<Distribution> list){
		List<DtoDistribrution> listDtoDistribrution = new ArrayList<>();
		for (Distribution distributionList : list) {
			DtoDistribrution distribrution = new DtoDistribrution();
			
			if(distributionList.getEmployeeMaster()!=null) {
				distribrution.setEmployeePrimaryId(distributionList.getEmployeeMaster().getEmployeeIndexId());
				distribrution.setEmployeeId(distributionList.getEmployeeMaster().getEmployeeId());
				distribrution.setEmployeeFirstName(distributionList.getEmployeeMaster().getEmployeeFirstName()+" "+distributionList.getEmployeeMaster().getEmployeeLastName());
				distribrution.setProjectId(distributionList.getEmployeeMaster().getEmployeeId());
				
				if(distributionList.getEmployeeMaster()!=null && distributionList.getEmployeeMaster().getDepartment()!=null) {
					distribrution.setDepartmentPrimaryId(distributionList.getEmployeeMaster().getDepartment().getId());
					distribrution.setDepartmentId(distributionList.getEmployeeMaster().getDepartment().getDepartmentId());
					distribrution.setDescription(distributionList.getEmployeeMaster().getDepartment().getDepartmentDescription());
				}
				if(distributionList.getEmployeeMaster()!=null&&distributionList.getEmployeeMaster().getPosition()!=null) {
					distribrution.setPositionPrimaryId(distributionList.getEmployeeMaster().getPosition().getId());
					distribrution.setPositionId(distributionList.getEmployeeMaster().getPosition().getPositionId());
					distribrution.setDescriptions(distributionList.getEmployeeMaster().getPosition().getDescription());
				}
				if(distributionList.getEmployeeMaster()!=null&&distributionList.getEmployeeMaster().getLocation()!=null) {
					distribrution.setLocationPrimaryId(distributionList.getEmployeeMaster().getLocation().getId());
					distribrution.setLocationId(distributionList.getEmployeeMaster().getLocation().getLocationId());
					distribrution.setDescriptionss(distributionList.getEmployeeMaster().getLocation().getDescription());
				}
				if(distributionList.getAccountsTableAccumulation()!=null&&distributionList.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
					distribrution.setAccountPrimaryId(distributionList.getAccountsTableAccumulation().getCoaMainAccounts().getId());
					distribrution.setBankAcountnumber(distributionList.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
					distribrution.setBankName(distributionList.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccDescription());
				}			
			}
			Map<String, BigDecimal> listOfDeduction = new HashMap<>();
			if(distributionList.getEmployeeMaster()!=null) {
				for (EmployeeDeductionMaintenance employeeDeductionMaintenance : distributionList.getEmployeeMaster().getListEmployeeDeductionMaintenance()) {
					if(employeeDeductionMaintenance.getDeductionCode()!=null) {
						listOfDeduction.put(employeeDeductionMaintenance.getDeductionCode().getDiductionId(), employeeDeductionMaintenance.getDeductionAmount());
					}
					
				}	
			}
			Map<String, BigDecimal>listOfBenefit=new HashMap<>();
			if(distributionList.getEmployeeMaster()!=null) {
				for (EmployeeBenefitMaintenance employeeBenefitMaintenance : distributionList.getEmployeeMaster().getListEmployeeBenefitMaintenance()) {
					if(employeeBenefitMaintenance.getBenefitCode()!=null) {
						listOfBenefit.put(employeeBenefitMaintenance.getBenefitCode().getBenefitId(), employeeBenefitMaintenance.getBenefitAmount());
					}
				}
			}
			BigDecimal totalEarning = listOfBenefit.entrySet().stream().map(Map.Entry :: getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
			BigDecimal totalDeduction = listOfDeduction.entrySet().stream().map(Map.Entry :: getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
			BigDecimal netPays=totalEarning.subtract(totalDeduction);

			distribrution.setNetPay(netPays);
			distribrution.setTotalEarnings(totalEarning);
			distribrution.setTotalDeductions(totalDeduction);
			distribrution.setListOfBenefit(listOfBenefit);
			distribrution.setListOfDeduction(listOfDeduction);
			distribrution.setCodePrimaryId(distributionList.getCodeId());
			distribrution.setPostingTypes(distributionList.getPostingType());
			distribrution.setTotalBenefit(distributionList.getCredit());
			distribrution.setTotalDeduction(distributionList.getDebit());
			distribrution.setCreditAmount(distributionList.getCredit());
			distribrution.setDebitAmount(distributionList.getDebit());
			distribrution.setAccountNumberSeq(distributionList.getAccountNumberSeq());			
			distribrution.setCheckNumberDisplay(distributionList.getCheckNumber());
			
			if(distributionList.getBuildChecks()!=null) {
				DtoBuildChecks buildChecks = new DtoBuildChecks();
				buildChecks.setCheckByUserId(buildChecks.getCheckByUserId());
				distribrution.setDtoBuildChecks(buildChecks);
			}
			
			if(distributionList.getDefault1()!=null) {
				distribrution.setDefaultId(distributionList.getDefault1().getDefaultID());
			}
			
			if(distributionList.getAccountsTableAccumulation()!=null && distributionList.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {

				DtoCOAMainAccounts accounts = new DtoCOAMainAccounts();
				accounts.setMainAccNumber(distributionList.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
			}
			listDtoDistribrution.add(distribrution);
			
		}
		return listDtoDistribrution;
	}
	
	
	
	
	public DtoSearch getAllDisctrubutionByDefaultId(DtoSearch dtoSearch) {
		List<Distribution> list = repositoryDistribution.getAllByDefaultId(dtoSearch.getId());
     	List<DtoDistribrution> listDtoDistribrution = new ArrayList<>();
		for (Distribution distributionList : list) {
			DtoDistribrution distribrution = new DtoDistribrution();
			if(distributionList.getEmployeeMaster()!=null) {
				distribrution.setEmployeeId(distributionList.getEmployeeMaster().getEmployeeId());
				distribrution.setEmployeeFirstName(distributionList.getEmployeeMaster().getEmployeeFirstName()+" "+distributionList.getEmployeeMaster().getEmployeeLastName());
				distribrution.setProjectId(distributionList.getEmployeeMaster().getEmployeeId());
				if(distributionList.getEmployeeMaster().getDepartment()!=null) {
					distribrution.setDepartmentId(distributionList.getEmployeeMaster().getDepartment().getDepartmentId());
					distribrution.setDescription(distributionList.getEmployeeMaster().getDepartment().getDepartmentDescription());
				}
			
			}
			distribrution.setCodePrimaryId(distributionList.getCodeId());
			distribrution.setPostingTypes(distributionList.getPostingType());
			distribrution.setCreditAmount(distributionList.getCredit());
			distribrution.setDebitAmount(distributionList.getDebit());
			distribrution.setAccountNumberSeq(distributionList.getAccountNumberSeq());
			
			
			distribrution.setCheckNumberDisplay(distributionList.getCheckNumber());
			
			if(distributionList.getBuildChecks()!=null) {
				DtoBuildChecks buildChecks = new DtoBuildChecks();
				buildChecks.setCheckByUserId(buildChecks.getCheckByUserId());
				distribrution.setDtoBuildChecks(buildChecks);
			}
			
			if(distributionList.getDefault1()!=null) {
				distribrution.setDefaultId(distributionList.getDefault1().getDefaultID());
			}
			
			if(distributionList.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
				DtoCOAMainAccounts accounts = new DtoCOAMainAccounts();
				accounts.setMainAccNumber(distributionList.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber());
			}
			listDtoDistribrution.add(distribrution);
			
		}
		dtoSearch.setRecords(listDtoDistribrution);
	return dtoSearch;
	}

	public DtoSearch getAllForPayRollStatement() {
		DtoSearch dtoSearch = new DtoSearch();
		List<EmployeeMaster> listEmployee = repositoryEmployeeMaster.findByIsDeleted(false);
		List<DtoStatement> listDtoStatement = new ArrayList<>();
		for (EmployeeMaster employeeMaster : listEmployee) {
			DtoStatement dtoStatement = new DtoStatement();
			dtoStatement.setEmployeeId(employeeMaster.getEmployeeId());
			dtoStatement.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
			dtoStatement.setEmployeePrimaryId(employeeMaster.getEmployeeIndexId());
			
			if(employeeMaster.getDepartment()!=null) {
				dtoStatement.setDepartmentPrimaryId(employeeMaster.getDepartment().getId());
				dtoStatement.setDepartmentId(employeeMaster.getDepartment().getDepartmentId());
				dtoStatement.setDescriptionss(employeeMaster.getDepartment().getDepartmentDescription());
			
			}
			
			if(employeeMaster.getPosition()!=null) {
				dtoStatement.setPositionPrimaryId(employeeMaster.getPosition().getId());
				dtoStatement.setPositionId(employeeMaster.getPosition().getPositionId());
				dtoStatement.setDescriptionss(employeeMaster.getPosition().getDescription());
			}
			
			if(employeeMaster.getLocation()!=null) {
				dtoStatement.setLocationPrimaryId(employeeMaster.getLocation().getId());
				dtoStatement.setLocationId(employeeMaster.getLocation().getLocationId());
				dtoStatement.setDescriptionss(employeeMaster.getLocation().getDescription());
				
			}
			if(employeeMaster.getListEmployeeBenefitMaintenance()!=null) {
				for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeMaster.getListEmployeeBenefitMaintenance()) {
					if(employeeBenefitMaintenance.getBenefitCode().getDesc().contains("salary") ||
						employeeBenefitMaintenance.getBenefitCode().getBenefitId().contains("salary")) {
						dtoStatement.setBasicSalary(employeeBenefitMaintenance.getBenefitCode().getAmount());
						
					}
					
					if(employeeBenefitMaintenance.getBenefitCode().getDesc().contains("transportation") ||
							employeeBenefitMaintenance.getBenefitCode().getBenefitId().contains("transportation")) {
						dtoStatement.setTransportation(employeeBenefitMaintenance.getBenefitCode().getAmount());
							
						}
					
					if(employeeBenefitMaintenance.getBenefitCode().getDesc().contains("housing") ||
							employeeBenefitMaintenance.getBenefitCode().getBenefitId().contains("housing")) {
						dtoStatement.setHousing(employeeBenefitMaintenance.getBenefitCode().getAmount());
							
						}
					if(employeeBenefitMaintenance.getBenefitCode().getDesc().contains("otherEarning") ||
							employeeBenefitMaintenance.getBenefitCode().getBenefitId().contains("otherEarning")) {
						dtoStatement.setOtherEarning(employeeBenefitMaintenance.getBenefitCode().getAmount());
					
					
				}
			}
			
		}
			listDtoStatement.add(dtoStatement);
			dtoSearch.setRecords(listDtoStatement);
			
			
	}
		return dtoSearch;
	}
	
	public DtoSearch getAllByDefautIdForDistribution(DtoSearch dtoSearch) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		if (dtoSearch != null) {
				
			List<BuildChecks> buildChecks = this.repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());

			List<DtoCalculateChecksDisplay> dtoCalculateChecksList = new ArrayList<>();	
			
			List<EmployeeMaster> employeeMasters = null;
			List<Integer> departmentList= new ArrayList<>();
			List<Integer> employeeList= new ArrayList<>();
			if(!buildChecks.isEmpty()) {
				departmentList = buildChecks.get(0).getListDepartment().stream().map(department ->department.getId()).collect(Collectors.toList());	
				employeeList = buildChecks.get(0).getListEmployee().stream().map(employee -> employee.getEmployeeIndexId()).collect(Collectors.toList());
			}
			
			
			if(buildChecks!=null && !buildChecks.isEmpty()) {
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom() == null &&  dtoSearch.getTo()==null && buildChecks.get(0).getFromEmployeeId()!=null && 
						buildChecks.get(0).getFromEmployeeId()>0 && buildChecks.get(0).getToEmployeeId()!=null && buildChecks.get(0).getToEmployeeId()>0) {
					employeeMasters = repositoryEmployeeMaster.findbyFromAndToId(buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
				}else if(dtoSearch.isAll()) {
					if(!departmentList.isEmpty()) {
						
						if(!employeeList.isEmpty()) {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentListAndEmployeeList(departmentList,employeeList);
						}else {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentList(departmentList);
						}
					
					}else {
						employeeMasters = new ArrayList<>();
					}
					
				}
				
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom()!=null && dtoSearch.getTo()!=null) {
					if(dtoSearch.getSortOn().equals("1")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForEmployee(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
					if(dtoSearch.getSortOn().equals("2")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForDeduction(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
				}
				
			List<Distribution> list = 	repositoryDistribution.getAllByDefaultId(dtoSearch.getId());
		Set<Integer> listEmployee= 	list.stream().map(m->m.getEmployeeMaster().getEmployeeIndexId()).collect(Collectors.toSet());
				
				for (EmployeeMaster employeeMaster : employeeMasters) {
					
					if(listEmployee.contains(employeeMaster.getEmployeeIndexId())) {
						DtoCalculateChecksDisplay dtoCalculateChecks = new DtoCalculateChecksDisplay();
						
						/*if(buildChecks.get(0).getListBuildPayrollCheckByPayCodes()!=null && !buildChecks.get(0).getListBuildPayrollCheckByPayCodes().isEmpty()) {
							dtoCalculateChecks.setCodeType((short)1);
							for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecks.get(0).getListBuildPayrollCheckByPayCodes()) {
								if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
									dtoCalculateChecks.setCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
									dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByPayCodes.getPayCode().getId());
									dtoCalculateChecks.setTotalAmount(buildPayrollCheckByPayCodes.getPayCode().getBaseOnPayCodeAmount());
								}
							}
						}
						*/
						
						Integer amount = 0;
						String transcationType = "";
						String codeId = "";
						BenefitCode benefitCode = null;
						DeductionCode deductionCode = null;
						PayCode payCode = null;
						
						for (Distribution distribution : list) {
							if(distribution.getCredit()!=null) {
								amount += distribution.getCredit().intValue();
							}
							
							if(distribution.getDebit()!=null) {
								amount -= distribution.getDebit().intValue();
							}
							
							if(distribution.getPostingType()==2) {
								benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(distribution.getCodeId(), false);
								if(benefitCode!=null && !transcationType.contains("Benefit")) {				
									transcationType += "Benefit,";
									codeId += benefitCode.getBenefitId() + ",";
								}
							}
							
							if(distribution.getPostingType()==3) {
								deductionCode = repositoryDeductionCode.findByIdAndIsDeleted(distribution.getCodeId(), false);
								if(deductionCode!=null && !transcationType.contains("Deduction")) {
									transcationType += "Deduction,";
									codeId += deductionCode.getDiductionId() + ",";
								}
							}
							
							if(distribution.getPostingType()==1) {
								payCode = repositoryPayCode.findByIdAndIsDeleted(distribution.getCodeId(), false);
								if(payCode!=null && !transcationType.contains("PayCode")) {
									transcationType += "PayCode,";
									codeId += payCode.getPayCodeId() + ",";
								}
							}
						}
						
						if(transcationType.length()>1) {
							transcationType = transcationType.substring(0, transcationType.length() - 1);	
						}
						if(codeId.length() > 1) {
							codeId = codeId.substring(0, codeId.length() - 1);
						}
						
						
						dtoCalculateChecks.setTransactionType(transcationType);
						dtoCalculateChecks.setCodeId(codeId);
						dtoCalculateChecks.setTotalAmount(BigDecimal.valueOf(amount));
						
						Date date = buildChecks.get(0).getCreatedDate();
						 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
					        String time = localDateFormat.format(date);
						dtoCalculateChecks.setBuildDate(date);
						dtoCalculateChecks.setBuildtime(time);
						
						/*if(buildChecks.get(0).getListBuildPayrollCheckByDeductions()!=null && !buildChecks.get(0).getListBuildPayrollCheckByDeductions().isEmpty()) {
							dtoCalculateChecks.setCodeType((short)2);
							for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildChecks.get(0).getListBuildPayrollCheckByDeductions()) {
								if(buildPayrollCheckByDeductions.getDeductionCode()!=null) {
									dtoCalculateChecks.setCodeId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
									dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByDeductions.getDeductionCode().getId());
									dtoCalculateChecks.setTotalAmount(buildPayrollCheckByDeductions.getDeductionCode().getAmount());
								}
							}
						}*/
						/*if(buildChecks.get(0).getListBuildPayrollCheckByBenefits()!=null && !buildChecks.get(0).getListBuildPayrollCheckByBenefits().isEmpty()) {
							dtoCalculateChecks.setCodeType((short)3);
							for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildChecks.get(0).getListBuildPayrollCheckByBenefits()) {
								if(buildPayrollCheckByBenefits.getBenefitCode()!=null) {
									dtoCalculateChecks.setCodeId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
									dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByBenefits.getBenefitCode().getId());
									dtoCalculateChecks.setTotalAmount(buildPayrollCheckByBenefits.getBenefitCode().getAmount());
								}
							}
						}*/
						
						dtoCalculateChecks.setEmployeePrimaryId(employeeMaster.getEmployeeIndexId());
						dtoCalculateChecks.setEmployeeID(employeeMaster.getEmployeeId());
						dtoCalculateChecks.setEmployeeId(employeeMaster.getEmployeeId());
						dtoCalculateChecks.setEmployeeName(employeeMaster.getEmployeeFirstName() + " " +employeeMaster.getEmployeeMiddleName() +" "+employeeMaster.getEmployeeLastName());
						
						
						List<TransactionEntryDetail> entry = repositoryTransactionEntryDetail.searchByEmployeeId(employeeMaster.getEmployeeIndexId());
						
						if(entry!=null && !entry.isEmpty()) {
							
							dtoCalculateChecks.setProjectId(entry.get(0).getDimensions().getDimensionDescription());
						}
						
						dtoCalculateChecks.setUserId(loggedInUserId);
						if(employeeMaster.getDepartment()!=null) {
							dtoCalculateChecks.setDepartmentPrimaryId(employeeMaster.getDepartment().getId());
							dtoCalculateChecks.setDepartmentId(employeeMaster.getDepartment().getDepartmentId());
							
							
							List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
							dtoCalculateChecks.setStatus("fail");
							for (PayrollAccounts payrollAccounts : listAccount) {
								if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
									String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
									if(accountnum!=null) {
										dtoCalculateChecks.setStatus("success");
									}else {
										dtoCalculateChecks.setStatus("fail");
									}
								}
								
							}
						}
						dtoCalculateChecksList.add(dtoCalculateChecks);
					}
					
					
				}
			
			
			}
			
			
			

			dtoSearch.setRecords(dtoCalculateChecksList);
		}

		return dtoSearch;
	}

	
	/*//process for Calaculate checks with Trasaction

	public DtoProcess processForBuildChecks(DtoProcess dtoProcess) {
         	DtoSearch dtoSearch = new DtoSearch();
         	dtoSearch.setIds(dtoProcess.getIds());
         	dtoSearch.setType(dtoSearch.getType());
         	dtoSearch.setPageNumber(dtoProcess.getPageNumber());
         	dtoSearch.setPageSize(dtoProcess.getPageSize());
         	dtoSearch.setId(dtoProcess.getId());
         	//dtoSearch = getDistribrution(dtoSearch);
         	List<DtoDistribrution> listDistribrution = getListDtoDistribrutionForProcess(dtoSearch);
         	for (DtoDistribrution dtoDistribrution : listDistribrution) {
         		Distribution distribution= new Distribution();
         		if(dtoDistribrution.getCheckNumber()!=null) {
         			distribution.setCheckNumber(dtoDistribrution.getCheckNumber().toString());	
         		}
                distribution.setCodeId(dtoDistribrution.getCodePrimaryId());
                distribution.setPostingType(dtoDistribrution.getPostingTypes());
                distribution.setDebit(dtoDistribrution.getDebitAmount());
                distribution.setCredit(dtoDistribrution.getCreditAmount());
                distribution.setAccountNumberSeq(dtoDistribrution.getAccountNumberSeq());
                
                TransactionEntry transactionEntry = null;
                if(dtoDistribrution.getTranscationId()!=null) {
                	transactionEntry = repositoryTransactionEntry.findByIdAndIsDeleted(dtoDistribrution.getTranscationId(), false);	
                }
                
                distribution.setTransactionEntry(transactionEntry);
                
                Default default1=null;
                if(dtoDistribrution.getDefaultPrimaryId()!=null && dtoDistribrution.getDefaultPrimaryId()>0) {
                	default1=repositoryDefault.findOne(dtoDistribrution.getDefaultPrimaryId());
                }
                
                EmployeeMaster employeeMaster=null;
                if(dtoDistribrution.getEmployeePrimaryId()!=null  && dtoDistribrution.getEmployeePrimaryId()>0) {
                	employeeMaster=repositoryEmployeeMaster.findOne(dtoDistribrution.getEmployeePrimaryId());
                }
                
                BuildChecks buildChecks=null;
                if(dtoDistribrution.getBuildId()!=null && dtoDistribrution.getBuildId()>0) {
                	buildChecks=repositoryBuildChecks.findOne(dtoDistribrution.getBuildId());
                }
                
                AccountsTableAccumulation accountsTableAccumulation=null;
                if(dtoDistribrution.getAccountPrimaryId()!=null && dtoDistribrution.getAccountPrimaryId()>0) {
                	accountsTableAccumulation=repositoryAccountsTableAccumulation.findOne(dtoDistribrution.getAccountPrimaryId());
                	
                }
                AuditTrialCodes auditTrialCodes=null;
                if(dtoDistribrution.getAuditTransactionNo()!=null && dtoDistribrution.getAuditTransactionNo()>0) {
                	auditTrialCodes=repositoryAuditTrialCodes.findOne(dtoDistribrution.getAuditTransactionNo());
                }
                distribution.setDefault1(default1);
                distribution.setEmployeeMaster(employeeMaster);
                distribution.setBuildChecks(buildChecks);
                distribution.setAccountsTableAccumulation(accountsTableAccumulation); 
                distribution.setAuditTrialCodes(auditTrialCodes);
                distribution.setCreatedDate(new Date());
         		repositoryDistribution.saveAndFlush(distribution);

         		Department department = null;
         		if(employeeMaster!=null && employeeMaster.getDepartment()!=null) {
         			department = repositoryDepartment.findByIdAndIsDeleted(employeeMaster.getDepartment().getId(), false);	
         		}
         		Position position=null;
         		if(employeeMaster!=null && employeeMaster.getPosition()!=null) {
         			position=repositoryPosition.findByIdAndIsDeleted(employeeMaster.getPosition().getId(), false);
         		}
         		
         		Location location=null;
         		if(employeeMaster!=null && employeeMaster.getLocation()!=null) {
         			location=repositoryLocation.findByIdAndIsDeleted(employeeMaster.getLocation().getId(), false);
         		}
         		Batches batches=null;
         		if(batches!=null) {
         			batches=repositoryBatches.findByIdAndIsDeleted(batches.getId(), false);
         		}
         		FinancialDimensionsValues financialDimensionsValues=null;
         		if(financialDimensionsValues!=null) {
         			financialDimensionsValues=repositoryFinancialDimensionsValues.findByIdAndIsDeleted(financialDimensionsValues.getId(),false);
         		}
         		
         		PayrollTransactionOpenYearDetails payrollTransactionOpenYearDetails = new PayrollTransactionOpenYearDetails();
         		payrollTransactionOpenYearDetails.setEmployeeMaster(employeeMaster);
         		payrollTransactionOpenYearDetails.setDepartment(department);
         		payrollTransactionOpenYearDetails.setPosition(position);
         		payrollTransactionOpenYearDetails.setLocation(location);
         		payrollTransactionOpenYearDetails.setBatches(batches);
         		payrollTransactionOpenYearDetails.setFinancialDimensionsValues(financialDimensionsValues);
         		
         		
         		if(dtoDistribrution.getDebitAmount()!=null) {
         			payrollTransactionOpenYearDetails.setTotalActualAmount(dtoDistribrution.getDebitAmount());
         		}else {
         			payrollTransactionOpenYearDetails.setTotalActualAmount(dtoDistribrution.getCreditAmount());
         		}employeeMasters
         		payrollTransactionOpenYearDetails.setCodeIndexId(dtoDistribrution.getCodePrimaryId());
         		payrollTransactionOpenYearDetails.setCodeType(dtoDistribrution.getTransactionType());
         		
         		if(dtoDistribrution.getBatchPrimaryId()!=null) {
         			Batches batches = repositoryBatches.findOne(dtoDistribrution.getBatchPrimaryId());
         		
         			if(batches!=null) {
         				batches.setStatus((byte)2);
             			repositoryBatches.saveAndFlush(batches);	
         			}
         			
         		}
         		
         	if(buildChecks!=null) {
         		for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildChecks.getListBuildPayrollCheckByBatches()) {
         			Batches batches = buildPayrollCheckByBatches.getBatches();
         			if(batches!=null) {
         				batches.setStatus((byte)2);
             			repositoryBatches.saveAndFlush(batches);
         			}
				}
         	}
         		
         		repositoryPayrollTransactionOpenYearDetails.saveAndFlush(payrollTransactionOpenYearDetails);
         		
         		
         		ParollTransactionOpenYearDistribution parollTransactionOpenYearDistribution = new ParollTransactionOpenYearDistribution();
         		parollTransactionOpenYearDistribution.setDepartment(department);
         		parollTransactionOpenYearDistribution.setPosition(position);
         		//payrollTransactionOpenYearDetails.setFinancialDimensionsValues(financialDimensionsValues);
         		repositoryParollTransactionOpenYearDistribution.saveAndFlush(parollTransactionOpenYearDistribution);
         		
         		PayrollTransactionOpenYearHeader payrollTransactionOpenYearHeader=new PayrollTransactionOpenYearHeader();
         		payrollTransactionOpenYearHeader.setDefaults(default1);
         		repositoryPayrollTransactionOpenYearHeader.saveAndFlush(payrollTransactionOpenYearHeader);
			}
		
		return dtoProcess;
	}
	*/
	
	public DtoSearch getAllByDefautId12(DtoSearch dtoSearch) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		if (dtoSearch != null) {
				
			List<BuildChecks> buildChecks = this.repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());

			List<DtoCalculateChecksDisplay> dtoCalculateChecksList = new ArrayList<>();	
			
			List<EmployeeMaster> employeeMasters = null;
			List<Integer> departmentList= new ArrayList<>();
			
			List<Integer> employeeList= new ArrayList<>();
			List<EmployeeMaster> employeeMasters2 = new ArrayList<>();
			if(!buildChecks.isEmpty()) {
				departmentList = buildChecks.get(0).getListDepartment().stream().map(department ->department.getId()).collect(Collectors.toList());	
				
				employeeList = buildChecks.get(0).getListEmployee().stream().map(employee -> employee.getEmployeeIndexId()).collect(Collectors.toList());
				//employeeList= repositoryEmployeeMaster.findByEmployeeByDepartmentListAndEmployeeList1(departmentList);
				
				if(!employeeList.isEmpty()) {
					employeeMasters2= repositoryEmployeeMaster.findAllEmployeeListByEmployeeIndexId(employeeList);
				}
				
			}
			
			
			if(buildChecks!=null && !buildChecks.isEmpty()) {
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom() == null &&  dtoSearch.getTo()==null && buildChecks.get(0).getFromEmployeeId()!=null && 
						buildChecks.get(0).getFromEmployeeId()>0 && buildChecks.get(0).getToEmployeeId()!=null && buildChecks.get(0).getToEmployeeId()>0) {
					employeeMasters = repositoryEmployeeMaster.findbyFromAndToId(buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
				}else if(dtoSearch.isAll()) {
					if(!departmentList.isEmpty()) {
						
						if(!employeeList.isEmpty()) {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentListAndEmployeeList(departmentList,employeeList);
						}else {
							employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentList(departmentList);
						}
					
					}else {
						employeeMasters = new ArrayList<>();
					}
					
				}
				
				
				if(!dtoSearch.isAll() && dtoSearch.getFrom()!=null && dtoSearch.getTo()!=null) {
					if(dtoSearch.getSortOn().equals("1")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForEmployee(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
					if(dtoSearch.getSortOn().equals("2")) {
						employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForDeduction(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
					}
				}
				
				if(!employeeMasters2.isEmpty()) {
					for (EmployeeMaster employeeMaster : employeeMasters2) {
						
						
						
						Integer serialNum = dtoSearch.getPageNumber() * dtoSearch.getPageSize();
						
						List<TransactionEntryDetail> list = repositoryTransactionEntryDetail.searchByEmployeeId12(employeeMaster.getEmployeeIndexId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
								Sort.Direction.DESC, "id"));
						if(!list.isEmpty()) {
							//For ByPass Output
							// dtoSearch.setTotalCount(repositoryTransactionEntryDetail.searchByEmployeeId(employeeMaster.getEmployeeIndexId()).size());
							for (TransactionEntryDetail transactionEntryDetail : list) {
								
								DtoCalculateChecksDisplay dtoCalculateChecks = new DtoCalculateChecksDisplay();
								
								if(buildChecks.get(0).getListBuildPayrollCheckByPayCodes()!=null && !buildChecks.get(0).getListBuildPayrollCheckByPayCodes().isEmpty()) {
									dtoCalculateChecks.setCodeType((short)1);
									for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecks.get(0).getListBuildPayrollCheckByPayCodes()) {
										if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
											dtoCalculateChecks.setCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
											dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByPayCodes.getPayCode().getId());
											dtoCalculateChecks.setTotalAmount(buildPayrollCheckByPayCodes.getPayCode().getBaseOnPayCodeAmount());
										}
									}
								}
								
								
								
								
								
								
								/*List<EmployeeBenefitMaintenance> list1=this.repositoryEmployeeBenefitMaintenance.findByIsDeletedAndInactive(false, false);
								List<DtoEmployeeBenefitMaintenance> dtoEmployeeBenefitMaintenances=new ArrayList<>();
								if(list1!=null && !list1.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : list1) {
										if(employeeBenefitMaintenance.getBenefitCode()!=null && employeeBenefitMaintenance.getBenefitCode().getId()>0) {
											DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance=new  DtoEmployeeBenefitMaintenance();
											dtoEmployeeBenefitMaintenance.setBenefitAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
											dtoEmployeeBenefitMaintenance.setId(employeeBenefitMaintenance.getBenefitCode().getId());
											dtoEmployeeBenefitMaintenances.add(dtoEmployeeBenefitMaintenance);
											dtoCalculateChecks.setDtoEmployeeBenefitMaintenance(dtoEmployeeBenefitMaintenance);
											dtoCalculateChecks.setBenefitAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
											dtoCalculateChecks.setId(employeeBenefitMaintenance.getBenefitCode().getId());
											dtoCalculateChecks.setCodeId(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
											
											
										}
									}
								}*/
								
								
								Date date = buildChecks.get(0).getCreatedDate();
								 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
							        String time = localDateFormat.format(date);
								dtoCalculateChecks.setBuildDate(date);
								dtoCalculateChecks.setBuildtime(time);
								dtoCalculateChecks.setNumberId(++serialNum);
								
								
								
									
									for (BuildChecks buildCheck : buildChecks) {
										if(!buildCheck.getListEmployee().isEmpty()) {
											for (EmployeeMaster employeeMaster2 : buildCheck.getListEmployee()) {
												List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildCheck.getId());
												List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList= repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
												if(!buildPayrollCheckByBenefitsList.isEmpty() && !employeeBenefitMaintenanceList.isEmpty()) {
													for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
														
														for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
															if (buildPayrollCheckByBenefits.getBenefitCode().getId()== employeeBenefitMaintenance.getBenefitCode().getId()) {
																dtoCalculateChecks.setCodeId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
																dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByBenefits.getBenefitCode().getId());
																dtoCalculateChecks.setTotalAmount(buildPayrollCheckByBenefits.getBenefitCode().getAmount());
																dtoCalculateChecks.setCodeType((short)3);
															}
														}
														
													}
												}	
											}
											
										}
										
										
										
									}
									/*dtoCalculateChecks.setCodeType((short)3);
									List<BuildPayrollCheckByBenefits> buildPayrollCheckByBenefitsList = repositoryBuildPayrollCheckByBenefits.findByBuildChecksId(buildChecks.get(0).getId());
									for (BuildPayrollCheckByBenefits buildPayrollCheckByBenefits : buildPayrollCheckByBenefitsList) {
										BuildChecks buildCheckForBenefit = repositoryBuildChecks.findByIdAndIsDeleted(buildPayrollCheckByBenefits.getBuildChecks().getId(), false);
										for (EmployeeMaster employeeMaster1 : buildCheckForBenefit.getListEmployee()) {
											List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList= repositoryEmployeeBenefitMaintenance.findByEmployeeId(employeeMaster1.getEmployeeIndexId());
											for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
												if(buildPayrollCheckByBenefits.getBenefitCode().getId()== employeeBenefitMaintenance.getBenefitCode().getId()) {
													dtoCalculateChecks.setCodeId(buildPayrollCheckByBenefits.getBenefitCode().getBenefitId());
													dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByBenefits.getBenefitCode().getId());
													dtoCalculateChecks.setTotalAmount(buildPayrollCheckByBenefits.getBenefitCode().getAmount());
												}
												
												
											}
											
										}
										
									}*/
									
									
									for (BuildChecks buildCheck : buildChecks) {
										if(!buildCheck.getListEmployee().isEmpty()) {
											for (EmployeeMaster employeeMaster2 : buildCheck.getListEmployee()) {
												List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductionsList = repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(buildCheck.getId());
												List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList= repositoryEmployeeDeductionMaintenance.findByEmployeeId(employeeMaster2.getEmployeeIndexId());
												if(!buildPayrollCheckByDeductionsList.isEmpty() && !employeeDeductionMaintenanceList.isEmpty()) {
													for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildPayrollCheckByDeductionsList) {
														
														for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenanceList) {
															if (buildPayrollCheckByDeductions.getDeductionCode().getId()== employeeDeductionMaintenance.getDeductionCode().getId()) {
																dtoCalculateChecks.setCodeId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
																dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByDeductions.getDeductionCode().getId());
																dtoCalculateChecks.setTotalAmount(buildPayrollCheckByDeductions.getDeductionCode().getAmount());
																dtoCalculateChecks.setCodeType((short)2);
															}
														}
														
													}
												}	
											}
											
										}
										
										
										
									}
								
								/*if(transactionEntryDetail.getDeductionCode()!=null) {
									dtoCalculateChecks.setCodeType((short)2);
									dtoCalculateChecks.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
									dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getDeductionCode().getId());
									dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getDeductionCode().getAmount());
								}*/
								
								if(transactionEntryDetail.getDimensions()!=null) {
									dtoCalculateChecks.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());
								}
								
								if(transactionEntryDetail.getCode()!=null) {
									dtoCalculateChecks.setCodeType((short)1);
									dtoCalculateChecks.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
									dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getCode().getId());
									//dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getBaseOnPayCodeAmount());
									dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getPayRate());
								}
								
								dtoCalculateChecks.setEmployeePrimaryId(employeeMaster.getEmployeeIndexId());
								dtoCalculateChecks.setEmployeeID(employeeMaster.getEmployeeId());
								dtoCalculateChecks.setEmployeeId(employeeMaster.getEmployeeId());
								dtoCalculateChecks.setEmployeeName(employeeMaster.getEmployeeFirstName() + " " +employeeMaster.getEmployeeMiddleName() +" "+employeeMaster.getEmployeeLastName());
								
								dtoCalculateChecks.setUserId(loggedInUserId);
								if(employeeMaster.getDepartment()!=null) {
									dtoCalculateChecks.setDepartmentPrimaryId(employeeMaster.getDepartment().getId());
									dtoCalculateChecks.setDepartmentId(employeeMaster.getDepartment().getDepartmentId());
									
									
									/*List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
									dtoCalculateChecks.setStatus("fail");
									for (PayrollAccounts payrollAccounts : listAccount) {
										if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
											String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
											if(accountnum!=null) {
												dtoCalculateChecks.setStatus("success");
											}else {
												dtoCalculateChecks.setStatus("fail");
											}
										}
										
									}*/
									
									
									List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
									//fail
									dtoCalculateChecks.setStatus("Success");
									for (PayrollAccounts payrollAccounts : listAccount) {
										/*if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
											String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
											if(accountnum!=null) {
												dtoCalculateChecks.setStatus("success");
											}else {
												dtoCalculateChecks.setStatus("fail");
											}
										}*/
										
										/*if(payrollAccounts.getFinancialDimensions().getDimensionDescription().equals(transactionEntryDetail.getDimensions().getDimensionDescription())) {
											dtoCalculateChecks.setStatus("success");
										}else {
											dtoCalculateChecks.setStatus("fail");
										}*/
										PayCode payCode = null;
										BenefitCode benefitCode = null;
										DeductionCode deductionCode = null;
										if(payrollAccounts.getAccountType()==1) {
											payCode = repositoryPayCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
										}
										if(payrollAccounts.getAccountType()==2) {
											deductionCode = repositoryDeductionCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
										}
										if(payrollAccounts.getAccountType()==3) {
											benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
										}
										
									if(payrollAccounts!=null && transactionEntryDetail!=null && payrollAccounts.getDepartment().getId() == transactionEntryDetail.getEmployeeMaster().getDepartment().getId()) {
										if(payCode!=null && transactionEntryDetail.getCode()!=null && payCode.getPayCodeId().equals(transactionEntryDetail.getCode().getPayCodeId())) {
											dtoCalculateChecks.setStatus("Success");
											break;
										}else if(benefitCode!=null && transactionEntryDetail.getBenefitCode()!=null &&  benefitCode.getBenefitId().equals(transactionEntryDetail.getBenefitCode().getBenefitId())) {
											dtoCalculateChecks.setStatus("Success");
											break;
										}else if(deductionCode!=null && transactionEntryDetail.getDeductionCode()!=null && deductionCode.getDiductionId().equals(transactionEntryDetail.getDeductionCode().getDiductionId())){
											dtoCalculateChecks.setStatus("Success");
											break;
										}else {
											//fail
											dtoCalculateChecks.setStatus("Success");
										}
									}else {
										//fail
										dtoCalculateChecks.setStatus("Success");
									}
										
										
										
									}
									
									
								}
								
								
								
								dtoCalculateChecksList.add(dtoCalculateChecks);
							}
						}else {
							//From transcation Entry
							
							
							List<TransactionEntryDetail> list1 = repositoryTransactionEntryDetail.searchByEmployeeId(employeeMaster.getEmployeeIndexId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
									Sort.Direction.DESC, "id"));
							

							// dtoSearch.setTotalCount(repositoryTransactionEntryDetail.searchByEmployeeId(employeeMaster.getEmployeeIndexId()).size());
							for (TransactionEntryDetail transactionEntryDetail : list1) {
								
								DtoCalculateChecksDisplay dtoCalculateChecks = new DtoCalculateChecksDisplay();
								
								if(buildChecks.get(0).getListBuildPayrollCheckByPayCodes()!=null && !buildChecks.get(0).getListBuildPayrollCheckByPayCodes().isEmpty()) {
									dtoCalculateChecks.setCodeType((short)1);
									for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecks.get(0).getListBuildPayrollCheckByPayCodes()) {
										if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
											dtoCalculateChecks.setCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
											dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByPayCodes.getPayCode().getId());
											dtoCalculateChecks.setTotalAmount(buildPayrollCheckByPayCodes.getPayCode().getBaseOnPayCodeAmount());
										}
									}
								}
								
								
								
								
								
								
								/*List<EmployeeBenefitMaintenance> list1=this.repositoryEmployeeBenefitMaintenance.findByIsDeletedAndInactive(false, false);
								List<DtoEmployeeBenefitMaintenance> dtoEmployeeBenefitMaintenances=new ArrayList<>();
								if(list1!=null && !list1.isEmpty()) {
									for (EmployeeBenefitMaintenance employeeBenefitMaintenance : list1) {
										if(employeeBenefitMaintenance.getBenefitCode()!=null && employeeBenefitMaintenance.getBenefitCode().getId()>0) {
											DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance=new  DtoEmployeeBenefitMaintenance();
											dtoEmployeeBenefitMaintenance.setBenefitAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
											dtoEmployeeBenefitMaintenance.setId(employeeBenefitMaintenance.getBenefitCode().getId());
											dtoEmployeeBenefitMaintenances.add(dtoEmployeeBenefitMaintenance);
											dtoCalculateChecks.setDtoEmployeeBenefitMaintenance(dtoEmployeeBenefitMaintenance);
											dtoCalculateChecks.setBenefitAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
											dtoCalculateChecks.setId(employeeBenefitMaintenance.getBenefitCode().getId());
											dtoCalculateChecks.setCodeId(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
											
											
										}
									}
								}*/
								
								
								Date date = buildChecks.get(0).getCreatedDate();
								 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
							        String time = localDateFormat.format(date);
								dtoCalculateChecks.setBuildDate(date);
								dtoCalculateChecks.setBuildtime(time);
								dtoCalculateChecks.setNumberId(++serialNum);
								
								
								if(transactionEntryDetail.getBenefitCode()!=null) {
									dtoCalculateChecks.setCodeType((short)3);
									dtoCalculateChecks.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
									dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getBenefitCode().getId());
									dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getBenefitCode().getAmount());
								
								}
								
								if(transactionEntryDetail.getDeductionCode()!=null) {
									dtoCalculateChecks.setCodeType((short)2);
									dtoCalculateChecks.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
									dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getDeductionCode().getId());
									dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getDeductionCode().getAmount());
								}
								
								if(transactionEntryDetail.getDimensions()!=null) {
									dtoCalculateChecks.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());
								}
								
								if(transactionEntryDetail.getCode()!=null) {
									dtoCalculateChecks.setCodeType((short)1);
									dtoCalculateChecks.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
									dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getCode().getId());
									//dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getBaseOnPayCodeAmount());
									dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getPayRate());
								}
								
								dtoCalculateChecks.setEmployeePrimaryId(employeeMaster.getEmployeeIndexId());
								dtoCalculateChecks.setEmployeeID(employeeMaster.getEmployeeId());
								dtoCalculateChecks.setEmployeeId(employeeMaster.getEmployeeId());
								dtoCalculateChecks.setEmployeeName(employeeMaster.getEmployeeFirstName() + " " +employeeMaster.getEmployeeMiddleName() +" "+employeeMaster.getEmployeeLastName());
								
								dtoCalculateChecks.setUserId(loggedInUserId);
								if(employeeMaster.getDepartment()!=null) {
									dtoCalculateChecks.setDepartmentPrimaryId(employeeMaster.getDepartment().getId());
									dtoCalculateChecks.setDepartmentId(employeeMaster.getDepartment().getDepartmentId());
									
									
									/*List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
									dtoCalculateChecks.setStatus("fail");
									for (PayrollAccounts payrollAccounts : listAccount) {
										if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
											String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
											if(accountnum!=null) {
												dtoCalculateChecks.setStatus("success");
											}else {
												dtoCalculateChecks.setStatus("fail");
											}
										}
										
									}*/
									
									
									List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
									//fail
									dtoCalculateChecks.setStatus("Success");
									for (PayrollAccounts payrollAccounts : listAccount) {
										/*if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
											String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
											if(accountnum!=null) {
												dtoCalculateChecks.setStatus("success");
											}else {
												dtoCalculateChecks.setStatus("fail");
											}
										}*/
										
										/*if(payrollAccounts.getFinancialDimensions().getDimensionDescription().equals(transactionEntryDetail.getDimensions().getDimensionDescription())) {
											dtoCalculateChecks.setStatus("success");
										}else {
											dtoCalculateChecks.setStatus("fail");
										}*/
										PayCode payCode = null;
										BenefitCode benefitCode = null;
										DeductionCode deductionCode = null;
										if(payrollAccounts.getAccountType()==1) {
											payCode = repositoryPayCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
										}
										if(payrollAccounts.getAccountType()==2) {
											deductionCode = repositoryDeductionCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
										}
										if(payrollAccounts.getAccountType()==3) {
											benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
										}
										
									if(payrollAccounts!=null && transactionEntryDetail!=null && payrollAccounts.getDepartment().getId() == transactionEntryDetail.getEmployeeMaster().getDepartment().getId()) {
										if(payCode!=null && transactionEntryDetail.getCode()!=null && payCode.getPayCodeId().equals(transactionEntryDetail.getCode().getPayCodeId())) {
											dtoCalculateChecks.setStatus("Success");
											break;
										}else if(benefitCode!=null && transactionEntryDetail.getBenefitCode()!=null &&  benefitCode.getBenefitId().equals(transactionEntryDetail.getBenefitCode().getBenefitId())) {
											dtoCalculateChecks.setStatus("Success");
											break;
										}else if(deductionCode!=null && transactionEntryDetail.getDeductionCode()!=null && deductionCode.getDiductionId().equals(transactionEntryDetail.getDeductionCode().getDiductionId())){
											dtoCalculateChecks.setStatus("Success");
											break;
										}else {
											//fail
											dtoCalculateChecks.setStatus("Success");
										}
									}else {
										//fail
										dtoCalculateChecks.setStatus("Success");
									}
										
										
										
									}
									
									
								}
								
								
								
								dtoCalculateChecksList.add(dtoCalculateChecks);
							}
						}
						
						
						
					}
				}
				
			
			
			}
			
			
			if(dtoSearch.getSortOn()!=null) {
				if(dtoSearch.getSortOn().equals("employee")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByEmployee sortbyEmployee = e.new SortByEmployee(); 
					Collections.sort(dtoCalculateChecksList, sortbyEmployee);
				}
				
				if(dtoSearch.getSortOn().equals("department")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByDepartment sortByDepartment = e.new SortByDepartment(); 
					Collections.sort(dtoCalculateChecksList, sortByDepartment);
				}
				
				if(dtoSearch.getSortOn().equals("codeId")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByCodeId sortByCodeId = e.new SortByCodeId(); 
					Collections.sort(dtoCalculateChecksList, sortByCodeId);
				}
				
				if(dtoSearch.getSortOn().equals("projectId")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByProjectId sortByProjectId = e.new SortByProjectId(); 
					Collections.sort(dtoCalculateChecksList, sortByProjectId);
				}
			}
			dtoSearch.setTotalCount(dtoCalculateChecksList.size());
			dtoSearch.setRecords(dtoCalculateChecksList);
		}
		log.info("Search TransactionEntry Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	
	
	public DtoSearch getAllByDefautId1(DtoSearch dtoSearch) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		if (dtoSearch != null) {
				
			List<DtoCalculateChecksDisplay> dtoCalculateChecksList = getDtoCalculateChecksList(dtoSearch,
					loggedInUserId);
			
			
			if(dtoSearch.getSortOn()!=null) {
				if(dtoSearch.getSortOn().equals("employee")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByEmployee sortbyEmployee = e.new SortByEmployee(); 
					Collections.sort(dtoCalculateChecksList, sortbyEmployee);
				}
				
				if(dtoSearch.getSortOn().equals("department")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByDepartment sortByDepartment = e.new SortByDepartment(); 
					Collections.sort(dtoCalculateChecksList, sortByDepartment);
				}
				
				if(dtoSearch.getSortOn().equals("codeId")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByCodeId sortByCodeId = e.new SortByCodeId(); 
					Collections.sort(dtoCalculateChecksList, sortByCodeId);
				}
				
				if(dtoSearch.getSortOn().equals("projectId")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByProjectId sortByProjectId = e.new SortByProjectId(); 
					Collections.sort(dtoCalculateChecksList, sortByProjectId);
				}
			}
			//dtoSearch.setTotalCount(dtoCalculateChecksList.size());
			int pageNumber = dtoSearch.getPageNumber();
			int pageSize = dtoSearch.getPageSize();
			 
			int index = pageNumber * pageSize;
			
			List<DtoCalculateChecksDisplay> listTobeReturned = new ArrayList<>();
			for(int i = index ; i < dtoCalculateChecksList.size() && i < pageSize * (pageNumber + 1); i++) {
				listTobeReturned.add(dtoCalculateChecksList.get(i));
			}
			
			
			dtoSearch.setRecords(listTobeReturned);
		}
		log.info("Search TransactionEntry Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public List<DtoCalculateChecksDisplay> getDtoCalculateChecksList(DtoSearch dtoSearch, int loggedInUserId) {
		List<BuildChecks> buildChecks = this.repositoryBuildChecks.findByDefauiltId(dtoSearch.getId());
		
		List<DtoCalculateChecksDisplay> dtoCalculateChecksList = new ArrayList<>();	
		
		List<EmployeeMaster> employeeMasters = null;
		List<Integer> departmentList= new ArrayList<>();
		List<Integer> employeeList= new ArrayList<>();
		if(!buildChecks.isEmpty()) {
			departmentList = buildChecks.get(0).getListDepartment().stream().map(department ->department.getId()).collect(Collectors.toList());	
			employeeList = buildChecks.get(0).getListEmployee().stream().map(employee -> employee.getEmployeeIndexId()).collect(Collectors.toList());
		}
		
		
		if(buildChecks!=null && !buildChecks.isEmpty()) {
			List<BuildPayrollCheckByBatches> buildCheckBatch = repositoryBuildPayrollCheckByBatches.findByBuildChecksId(buildChecks.get(0).getId());
			if(!dtoSearch.isAll() && dtoSearch.getFrom() == null &&  dtoSearch.getTo()==null && buildChecks.get(0).getFromEmployeeId()!=null && 
					buildChecks.get(0).getFromEmployeeId()>0 && buildChecks.get(0).getToEmployeeId()!=null && buildChecks.get(0).getToEmployeeId()>0) {
				employeeMasters = repositoryEmployeeMaster.findbyFromAndToId(buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
			}else if(dtoSearch.isAll()) {
				if(!departmentList.isEmpty()) {
					
					if(!employeeList.isEmpty()) {
						employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentListAndEmployeeList(departmentList,employeeList);
					}else {
						employeeMasters = repositoryEmployeeMaster.findByEmployeeByDepartmentList(departmentList);
					}
				
				}else {
					employeeMasters = new ArrayList<>();
				}
				
			}
			
			
			if(!dtoSearch.isAll() && dtoSearch.getFrom()!=null && dtoSearch.getTo()!=null) {
				if(dtoSearch.getSortOn().equals("1")) {
					employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForEmployee(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
				}
				if(dtoSearch.getSortOn().equals("2")) {
					employeeMasters = repositoryEmployeeMaster.findbyFromAndToIdForDeduction(dtoSearch.getFrom(),dtoSearch.getTo(),buildChecks.get(0).getFromEmployeeId(),buildChecks.get(0).getToEmployeeId());
				}
			}
			
			dtoSearch.setTotalCount(0);
			for (EmployeeMaster employeeMaster : employeeMasters) {
				//dtoSearch.setTotalCount(repositoryTransactionEntryDetail.getCountOfTotalTranscationEntryDetail(employeeMaster.getEmployeeIndexId()));
				
				
				//Integer serialNum = dtoSearch.getPageNumber() * dtoSearch.getPageSize();
				 dtoSearch.setTotalCount(dtoSearch.getTotalCount()+repositoryTransactionEntryDetail.getCountOfTotalTranscationEntryDetail(employeeMaster.getEmployeeIndexId(),buildChecks.get(0).getId()));
				List<TransactionEntryDetail> list = repositoryTransactionEntryDetail.searchByEmployeeId1(employeeMaster.getEmployeeIndexId(),buildChecks.get(0).getId());
				
				
				for (TransactionEntryDetail transactionEntryDetail : list) {
					
					DtoCalculateChecksDisplay dtoCalculateChecks = new DtoCalculateChecksDisplay();
					
					if(buildChecks.get(0).getListBuildPayrollCheckByPayCodes()!=null && !buildChecks.get(0).getListBuildPayrollCheckByPayCodes().isEmpty()) {
						dtoCalculateChecks.setCodeType((short)1);
						for (BuildPayrollCheckByPayCodes buildPayrollCheckByPayCodes : buildChecks.get(0).getListBuildPayrollCheckByPayCodes()) {
							if(buildPayrollCheckByPayCodes.getPayCode()!=null) {
								dtoCalculateChecks.setCodeId(buildPayrollCheckByPayCodes.getPayCode().getPayCodeId());
								dtoCalculateChecks.setCodeIndexID(buildPayrollCheckByPayCodes.getPayCode().getId());
								dtoCalculateChecks.setTotalAmount(buildPayrollCheckByPayCodes.getPayCode().getBaseOnPayCodeAmount());
							}
						}
					}
					
					
					
					
					
					
					/*List<EmployeeBenefitMaintenance> list1=this.repositoryEmployeeBenefitMaintenance.findByIsDeletedAndInactive(false, false);
					List<DtoEmployeeBenefitMaintenance> dtoEmployeeBenefitMaintenances=new ArrayList<>();
					if(list1!=null && !list1.isEmpty()) {
						for (EmployeeBenefitMaintenance employeeBenefitMaintenance : list1) {
							if(employeeBenefitMaintenance.getBenefitCode()!=null && employeeBenefitMaintenance.getBenefitCode().getId()>0) {
								DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance=new  DtoEmployeeBenefitMaintenance();
								dtoEmployeeBenefitMaintenance.setBenefitAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
								dtoEmployeeBenefitMaintenance.setId(employeeBenefitMaintenance.getBenefitCode().getId());
								dtoEmployeeBenefitMaintenances.add(dtoEmployeeBenefitMaintenance);
								dtoCalculateChecks.setDtoEmployeeBenefitMaintenance(dtoEmployeeBenefitMaintenance);
								dtoCalculateChecks.setBenefitAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
								dtoCalculateChecks.setId(employeeBenefitMaintenance.getBenefitCode().getId());
								dtoCalculateChecks.setCodeId(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
								
								
							}
						}
					}*/
					
					
					Date date = buildChecks.get(0).getCreatedDate();
					 SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
				        String time = localDateFormat.format(date);
					dtoCalculateChecks.setBuildDate(date);
					dtoCalculateChecks.setBuildtime(time);
					if (buildChecks != null && buildChecks.get(0) != null) {
						dtoCalculateChecks.setPeriodFromDate(buildChecks.get(0).getDateFrom());
						dtoCalculateChecks.setPeriodToDate(buildChecks.get(0).getDateTo());
					}
//					if (buildChecks.get(0).getListDistribution() != null && buildChecks.get(0).getListDistribution().size() > 0) {
//						dtoCalculateChecks.setPostingDate(buildChecks.get(0).getListDistribution().get(0).getPostingDate());
//					}

					//dtoCalculateChecks.setNumberId(++serialNum);
					if(transactionEntryDetail.getTransactionEntry().getBatches()!=null) {
						
					}
					
					if(transactionEntryDetail.getBenefitCode()!=null) {
						dtoCalculateChecks.setCodeType((short)3);
						dtoCalculateChecks.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
						dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getBenefitCode().getId());
						dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getPayRate());
					
					}
					
					if(transactionEntryDetail.getDeductionCode()!=null) {
						dtoCalculateChecks.setCodeType((short)2);
						dtoCalculateChecks.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
						dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getDeductionCode().getId());
						dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getPayRate());
					}
					
					if(transactionEntryDetail.getDimensions()!=null) {
						dtoCalculateChecks.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());
					}
					
					transactionEntryDetail.getEmployeeMaster().getMiscellaneousEmployee().stream()
					.filter(e -> e.getMiscellaneousHeader().getMiscellaneousIndex() == 7).collect(Collectors.toList()).stream().forEach(p -> {
						
						dtoCalculateChecks.setMiscellaneousId(Integer.parseInt(p.getMiscellaneousDetails().getValueId()));
						dtoCalculateChecks.setMiscellaneousDescrption(p.getMiscellaneousDetails().getValueDescription());
						dtoCalculateChecks.setMiscellaneousArabicDescrption(p.getMiscellaneousDetails().getValueArabicDescription());
					});
					
					/*.stream().forEach(e -> {
						e.getMiscellaneousHeader()
						e.getMiscellaneousDetails().stream().filter(p -> p.getMiscellaneousHeaders().getMiscellaneousIndex() == 7).collect(Collectors.toList())
						.stream().forEach(s -> {
							dtoCalculateChecks.setMiscellaneousId(s.getValueIndex());
							dtoCalculateChecks.setMiscellaneousDescrption(s.getValueDescription());
							dtoCalculateChecks.setMiscellaneousArabicDescrption(s.getValueArabicDescription());
						});
					});
					*/
					//.filter(e ->e.getMiscellaneousDetails().stream().filter(m -> m.getMiscellaneousEmployee()
					//		.stream().filter(s -> s.getMiscellaneousIndex() == 7)) ).count();
					//List<MiscellaneousDetails> misDetails = misemployee.stream().filte)
					//MiscellaneousEmployee empMis = transactionEntryDetail.getEmployeeMaster().getMiscellaneousEmployee().stream()
					//		.filter(e -> e.get)
					//if()
					if(transactionEntryDetail.getCode()!=null) {
						dtoCalculateChecks.setCodeType((short)1);
						dtoCalculateChecks.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
						dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getCode().getId());
						//dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getBaseOnPayCodeAmount());
						dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getPayRate());
					}
					
					/*if(!buildCheckBatch.isEmpty()) {
						if(transactionEntryDetail.getBatches()!=null) {
							for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildCheckBatch) {
								if(buildPayrollCheckByBatches.getBuildChecks().getId().equals(transactionEntryDetail.getBuildChecks().getId())) {
									if(buildPayrollCheckByBatches.getBatches().getId().equals(transactionEntryDetail.getBatches().getId())) {
										if(transactionEntryDetail.getBenefitCode()!=null) {
											dtoCalculateChecks.setCodeType((short)3);
											dtoCalculateChecks.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
											dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getBenefitCode().getId());
											dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getBenefitCode().getAmount());
										
										}
										
										if(transactionEntryDetail.getDeductionCode()!=null) {
											dtoCalculateChecks.setCodeType((short)2);
											dtoCalculateChecks.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
											dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getDeductionCode().getId());
											dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getDeductionCode().getAmount());
										}
										
										if(transactionEntryDetail.getDimensions()!=null) {
											dtoCalculateChecks.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());
										}
										
										if(transactionEntryDetail.getCode()!=null) {
											dtoCalculateChecks.setCodeType((short)1);
											dtoCalculateChecks.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
											dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getCode().getId());
											//dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getBaseOnPayCodeAmount());
											dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getPayRate());
										}
									}
								}
								
								
							}
						}
						
					}*/
					if(!buildCheckBatch.isEmpty()) {
						if(transactionEntryDetail.getTransactionEntry().getBatches()!=null) {
							for (BuildPayrollCheckByBatches buildPayrollCheckByBatches : buildCheckBatch) {
								if(transactionEntryDetail.getTransactionEntry().getBatches().getId().equals(buildPayrollCheckByBatches.getBatches().getId())) {
									if(transactionEntryDetail.getBenefitCode()!=null) {
										dtoCalculateChecks.setCodeType((short)3);
										dtoCalculateChecks.setCodeId(transactionEntryDetail.getBenefitCode().getBenefitId());
										dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getBenefitCode().getId());
										dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getPayRate());
									
									}
									
									if(transactionEntryDetail.getDeductionCode()!=null) {
										dtoCalculateChecks.setCodeType((short)2);
										dtoCalculateChecks.setCodeId(transactionEntryDetail.getDeductionCode().getDiductionId());
										dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getDeductionCode().getId());
										dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getPayRate());
									}
									
									if(transactionEntryDetail.getDimensions()!=null) {
										dtoCalculateChecks.setProjectId(transactionEntryDetail.getDimensions().getDimensionDescription());
									}
									
									if(transactionEntryDetail.getCode()!=null) {
										dtoCalculateChecks.setCodeType((short)1);
										dtoCalculateChecks.setCodeId(transactionEntryDetail.getCode().getPayCodeId());
										dtoCalculateChecks.setCodeIndexID(transactionEntryDetail.getCode().getId());
										//dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getCode().getBaseOnPayCodeAmount());
										dtoCalculateChecks.setTotalAmount(transactionEntryDetail.getPayRate());
									}
								}
							}
							
						}
					}
					
					dtoCalculateChecks.setEmployeePrimaryId(employeeMaster.getEmployeeIndexId());
					dtoCalculateChecks.setEmployeeID(employeeMaster.getEmployeeId());
					dtoCalculateChecks.setEmployeeId(employeeMaster.getEmployeeId());
					dtoCalculateChecks.setEmployeeName(employeeMaster.getEmployeeFirstName() + " " +employeeMaster.getEmployeeMiddleName() +" "+employeeMaster.getEmployeeLastName());
					
					//employeeMaster.getPro
					dtoCalculateChecks.setUserId(loggedInUserId);
					
					if(employeeMaster.getProjectSetup() != null) {
						dtoCalculateChecks.setProjectId(employeeMaster.getProjectSetup().getId()+"");
					}
					
					if(employeeMaster.getDepartment()!=null) {
						dtoCalculateChecks.setDepartmentPrimaryId(employeeMaster.getDepartment().getId());
						dtoCalculateChecks.setDepartmentId(employeeMaster.getDepartment().getDepartmentId());
						
						
						/*List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
						dtoCalculateChecks.setStatus("fail");
						for (PayrollAccounts payrollAccounts : listAccount) {
							if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
								String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
								if(accountnum!=null) {
									dtoCalculateChecks.setStatus("success");
								}else {
									dtoCalculateChecks.setStatus("fail");
								}
							}
							
						}*/
						
						
/*						List<PayrollAccounts> listAccount = repositoryPayrollAccounts.getstatusByDepartment(employeeMaster.getDepartment().getId());
						//fail
						dtoCalculateChecks.setStatus("Success");
						for (PayrollAccounts payrollAccounts : listAccount) {
							if(payrollAccounts.getAccountsTableAccumulation()!=null && payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts()!=null) {
								String accountnum = payrollAccounts.getAccountsTableAccumulation().getCoaMainAccounts().getMainAccNumber();
								if(accountnum!=null) {
									dtoCalculateChecks.setStatus("success");
								}else {
									dtoCalculateChecks.setStatus("fail");
								}
							}
							
							if(payrollAccounts.getFinancialDimensions().getDimensionDescription().equals(transactionEntryDetail.getDimensions().getDimensionDescription())) {
								dtoCalculateChecks.setStatus("success");
							}else {
								dtoCalculateChecks.setStatus("fail");
							}
							PayCode payCode = null;
							BenefitCode benefitCode = null;
							DeductionCode deductionCode = null;
							if(payrollAccounts.getAccountType()==1) {
								payCode = repositoryPayCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
							}
							if(payrollAccounts.getAccountType()==2) {
								deductionCode = repositoryDeductionCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
							}
							if(payrollAccounts.getAccountType()==3) {
								benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(payrollAccounts.getPayrollCode(), false);
							}
							
						if(payrollAccounts!=null && transactionEntryDetail!=null && payrollAccounts.getDepartment().getId() == transactionEntryDetail.getEmployeeMaster().getDepartment().getId()) {
							if(payCode!=null && transactionEntryDetail.getCode()!=null && payCode.getPayCodeId().equals(transactionEntryDetail.getCode().getPayCodeId())) {
								dtoCalculateChecks.setStatus("Success");
								break;
							}else if(benefitCode!=null && transactionEntryDetail.getBenefitCode()!=null &&  benefitCode.getBenefitId().equals(transactionEntryDetail.getBenefitCode().getBenefitId())) {
								dtoCalculateChecks.setStatus("Success");
								break;
							}else if(deductionCode!=null && transactionEntryDetail.getDeductionCode()!=null && deductionCode.getDiductionId().equals(transactionEntryDetail.getDeductionCode().getDiductionId())){
								dtoCalculateChecks.setStatus("Success");
								break;
							}else {
								//fail
								dtoCalculateChecks.setStatus("Success");
							}
						} else {
						*/	//fail
							dtoCalculateChecks.setStatus("Success");
//						}
							
							
							
//						}
						
						
					}
					
					
					
					dtoCalculateChecksList.add(dtoCalculateChecks);
				}
				
				
			}
		
		
		}
		return dtoCalculateChecksList;
	}

	
	private Map<String, DtoEmployeePayrollDetails> generateEmployeePayrollDataMap(DtoSearch dtoSearch, int loggedInUserId) {
		HashMap<String, DtoEmployeePayrollDetails> map = new HashMap<String, DtoEmployeePayrollDetails>();
		String employeeIds = "";

		Date periodFromDate = null;
		Date periodToDate = null;
		String strPeriodFromDate = "";
		String strPeriodToDate = "";

		List<DtoCalculateChecksDisplay> dtoCalculateChecksList = getDtoCalculateChecksList(dtoSearch,
				loggedInUserId);

		if (dtoCalculateChecksList != null && dtoCalculateChecksList.size() > 0) {
			
			periodFromDate = dtoCalculateChecksList.get(0).getPeriodFromDate();
			periodToDate = dtoCalculateChecksList.get(0).getPeriodToDate();
			

			// adjust date if not in same month (related to problem #EDNI-73
			if(periodFromDate.getMonth() != periodToDate.getMonth()) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(periodFromDate);
				cal.add(Calendar.DATE, 1);
				periodFromDate = cal.getTime();
				
				cal.setTime(periodToDate);
				cal.add(Calendar.DATE, 1);
				periodToDate = cal.getTime();
				
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
			if (periodFromDate != null) {
				strPeriodFromDate = sdf.format(periodFromDate);
			}
			if (periodToDate != null) {
				strPeriodToDate = sdf.format(periodToDate);
			}
		}
		
		for (DtoCalculateChecksDisplay dtoCalculateChecksDisplay: dtoCalculateChecksList) {

			DtoEmployeePayrollDetails dtoEmployeePayrollDetails = null;  
			String employeeId = dtoCalculateChecksDisplay.getEmployeeId();
			
			if (map.containsKey(employeeId)) {
				dtoEmployeePayrollDetails = map.get(employeeId);
			} else {
				
				DtoEmployeeMaster dtoEmployeeMaster = serviceEmployeeMaster.getEmployeeByCode(employeeId);
				dtoEmployeePayrollDetails = new DtoEmployeePayrollDetails();
				dtoEmployeePayrollDetails.setDtoEmployeeMaster(dtoEmployeeMaster);
				dtoEmployeePayrollDetails.setEmployeeCode(employeeId);
				dtoEmployeePayrollDetails.setEmployeeName(dtoCalculateChecksDisplay.getEmployeeName());
				dtoEmployeePayrollDetails.setStrPeriodFromDate(strPeriodFromDate);
				dtoEmployeePayrollDetails.setStrPeriodToDate(strPeriodToDate);
				
				
				DtoBank dtoBank = null;
				
				if (dtoEmployeeMaster != null) {
					dtoEmployeePayrollDetails.setEmployeeIdNumber(dtoEmployeeMaster.getIdNumber());
					dtoEmployeePayrollDetails.setEmployeeIBAN(dtoEmployeeMaster.getEmployeeBankAccountNumber());

					dtoBank = dtoEmployeeMaster.getDtoBank();
				}
				if (dtoBank != null) {
					
					String employeeOtherBank = "";
					
					if ("INMA".equalsIgnoreCase(dtoBank.getSwiftCode())) {
						employeeOtherBank = "NO";
					} else {
						employeeOtherBank = "Yes";
					}
					
					dtoEmployeePayrollDetails.setOtherBank(employeeOtherBank);
					dtoEmployeePayrollDetails.setSwiftCode(dtoBank.getSwiftCode());
					dtoEmployeePayrollDetails.setCountry("SA");
					dtoEmployeePayrollDetails.setCurrency("SAR");
					dtoEmployeePayrollDetails.setTransferType("/PAYROLL/");
					String transferDest = "";
					if ("INMA".equalsIgnoreCase(dtoBank.getSwiftCode())) {
						transferDest = "BANK ACCOUNT";
					} else {
						transferDest = "SARIE";
					}
					dtoEmployeePayrollDetails.setTransferDest(transferDest);
				}

				map.put(employeeId, dtoEmployeePayrollDetails);
			}
			
			String codeId = dtoCalculateChecksDisplay.getCodeId();
			int codeType = dtoCalculateChecksDisplay.getCodeType();
			BigDecimal totalAmount = dtoCalculateChecksDisplay.getTotalAmount();
			
				
			
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("BASIC")) dtoEmployeePayrollDetails.addToBasic(totalAmount);
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("HOUSE")) dtoEmployeePayrollDetails.addToHousing(totalAmount);
			if (CommonUtils.removeNull(codeId).toUpperCase().equalsIgnoreCase("TRANS")) dtoEmployeePayrollDetails.addToTransportation(totalAmount);
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("OTHE1")
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("OTHE2")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("FOOD")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("MOBI")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("COLI")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("TRANS1")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("TICK")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("CASH")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("CARS")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("CAL")	
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("CLEAN")
				|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("CFMA")) { 
					dtoEmployeePayrollDetails.addToOthersBenefits1(totalAmount);
			}
			
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("FIXED")) dtoEmployeePayrollDetails.addToFixed(totalAmount);
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("OVER")) dtoEmployeePayrollDetails.addToOvertime(totalAmount);
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("BONUS")) dtoEmployeePayrollDetails.addToBonus(totalAmount);
			
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("OTBE")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("MOAL")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("COMM")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("EOS")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("EOS-5")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("VAC-21")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("VAC-30")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("NVAC")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("GOSI-2")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("GOSI-12")) {
				dtoEmployeePayrollDetails.addToOthersBenefits2(totalAmount);
			}
			
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("LOAN")) dtoEmployeePayrollDetails.addToLoan(totalAmount);
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("ABSEN")) dtoEmployeePayrollDetails.addToAbsent(totalAmount);
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("SANED-1")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("GOSI-9") 
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("GOSI-10")) { 
				dtoEmployeePayrollDetails.addToGosi(totalAmount);
			}
			
			if (CommonUtils.removeNull(codeId).toUpperCase().startsWith("OTDE")
					|| CommonUtils.removeNull(codeId).toUpperCase().startsWith("TAI")) {
				dtoEmployeePayrollDetails.addToOtherDeductions(totalAmount);
			}
			
			log.info(dtoCalculateChecksDisplay);
			
			if (!employeeIds.contains(employeeId)) {
				employeeIds += "'" + employeeId + "',";
			}
			
		}
		
		
		log.info("employeeIds before: " + employeeIds);
		if (employeeIds != null && employeeIds.endsWith(",")) {
			employeeIds = employeeIds.substring(0, employeeIds.lastIndexOf(","));
		}
		log.info("employeeIds after: " + employeeIds);
		
		
//		repositoryEntityManager.populateEmployeeAccountsMapForPayroll(employeeIds, map);
		
		log.info("map: " + map);
		return map;
	}
	
	
	public String[] generateCSVFile(String id, int loggedInUserId, String strPostingDate) {
		
		Date buildDate = null;
		Date postingDate = null;
		String strBuildDate = "";
//		String strPostingDate = "";

		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setId(CommonUtils.parseInteger(id));
		dtoSearch.setAll(true);

		List<DtoCalculateChecksDisplay> dtoCalculateChecksList = getDtoCalculateChecksList(dtoSearch,
				loggedInUserId);

		if (dtoCalculateChecksList != null && dtoCalculateChecksList.size() > 0) {
			buildDate = dtoCalculateChecksList.get(0).getBuildDate();
			postingDate = dtoCalculateChecksList.get(0).getPostingDate();
			
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			if (buildDate != null) {
				strBuildDate = sdf.format(buildDate);
			}
//			if (postingDate != null) {
//				strPostingDate = sdf.format(postingDate);
//			}

		}

			String csvArray [] = null;
			
			Object[] topRow = repositoryEntityManager.getPayrollReportTopRow(CommonUtils.parseInteger(id));
			
			Map<String, DtoEmployeePayrollDetails> map = generateEmployeePayrollDataMap(dtoSearch, loggedInUserId);
			DtoEmployeePayrollCSV dtoCsv = new DtoEmployeePayrollCSV();
			
			BigDecimal totalPayroll = dtoCsv.setRecordsAndCalculateTotal(map, discardDecimal);
			
			topRow[5] = strBuildDate;
			topRow[6] = strPostingDate;
			topRow[8] = dtoCsv.getNumberOfRecords();
			topRow[9] = (discardDecimal ? totalPayroll.intValue() : totalPayroll.toString());
			
			dtoCsv.setTopRow(topRow);
			String[] csvDetailsArray = dtoCsv.getDetailsArray();
			log.info(csvDetailsArray);

			csvArray = prepareFinalArrays(dtoCsv.getTopRow(), csvDetailsArray);
			
			
		return csvArray;

	}
	
	public boolean generateAndEmailPaySlip(DtoSearch dtoSearch, int loggedInUserId) {
		
		boolean isSuccess = false;
		DtoCompanyInfo dtoCompanyInfo = serviceHcmHome.getDtoCompanyInfo();
		

		Date periodFromDate = null;
		Date periodToDate = null;
		String strPeriodFromDate = "";
		String strPeriodToDate = "";
		String strPrintedDate = "";
		String strPayPeriod = "";

		dtoSearch.setAll(true);
		List<DtoCalculateChecksDisplay> dtoCalculateChecksList = getDtoCalculateChecksList(dtoSearch,
				loggedInUserId);

		if (dtoCalculateChecksList != null && dtoCalculateChecksList.size() > 0) {
			
			periodFromDate = dtoCalculateChecksList.get(0).getPeriodFromDate();
			periodToDate = dtoCalculateChecksList.get(0).getPeriodToDate();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

			// adjust date if not in same month (related to problem #EDNI-73
			if(periodFromDate.getMonth() != periodToDate.getMonth()) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(periodFromDate);
				cal.add(Calendar.DATE, 1);
				periodFromDate = cal.getTime();
				
				cal.setTime(periodToDate);
				cal.add(Calendar.DATE, 1);
				periodToDate = cal.getTime();
				
			}
			
			sdf = new SimpleDateFormat("dd-MMM-yy");
			if (periodFromDate != null) {
				strPeriodFromDate = sdf.format(periodFromDate);
			}
			if (periodToDate != null) {
				strPeriodToDate = sdf.format(periodToDate);
			}
			sdf = new SimpleDateFormat("dd MMMM, yyyy");
			strPrintedDate = sdf.format(new Date());

			sdf = new SimpleDateFormat("MMMM-yyyy");
			strPayPeriod = sdf.format(periodToDate);
		}
		
		Map<String, DtoEmployeePayrollDetails> map = generateEmployeePayrollDataMap(dtoSearch, loggedInUserId); 
		
		for (Entry<String, DtoEmployeePayrollDetails> entry : map.entrySet()) {
			String empId = entry.getKey(); 
			
			DtoEmployeePayrollDetails dtoEmployeePayrollDetails = entry.getValue(); 
			
			dtoEmployeePayrollDetails.setPrintedDate(strPrintedDate);
			String fileName = "pay_slip_" + empId + "-" + strPayPeriod + ".pdf";

			String paySlipPDFFullPath = payslipFilesPath +  File.separator + fileName;
			
			log.info("paySlipPDFFullPath: " + paySlipPDFFullPath);

			PaySlipReportHelper payslipReport = new PaySlipReportHelper(paySlipPDFFullPath, dtoCompanyInfo);
			
			try {
				payslipReport.generatePaySlip(empId, dtoEmployeePayrollDetails);
				isSuccess = true;
			} catch (Exception ex) {
				log.error(ex);
				return false;
			}
		
			log.info("Payslip function completed: ");
			try {
				String empEmail = CommonUtils.removeNull(dtoEmployeePayrollDetails.getDtoEmployeeMaster().getEmployeeEmail());
				if (!empEmail.isEmpty()) {
					serviceEmailHandler.sendEmailWithAttachment(fileName, paySlipPDFFullPath, empEmail, dtoEmployeePayrollDetails.getEmployeeName(), strPayPeriod, strPeriodFromDate, strPeriodToDate);
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
				log.error(e.getStackTrace());
			}
		}
		
		return isSuccess;

	}
	
	
	
	String [] prepareFinalArrays(String header, String details[]) {
		String[] finalArray = new String[details.length + 1];
		
		finalArray[0] = header;
		for (int i=0; i < details.length; i++) {
			finalArray[i+1] = details[i];
		}
		
		return finalArray;
		
	}
	
	
	
	public DtoSearch prepareDateForExcel(DtoSearch dtoSearch) {
		List<DtoCalculateChecksDisplay> dtoCalculateChecksList = getDtoCalculateChecksList(dtoSearch,
				2);
		
		Date payrolDate = dtoCalculateChecksList.get(0).getBuildDate();
		List<DtoDepartment> list = new ArrayList<>();
		//dtoCalculateChecksList.stream().peek(e ->{ if(e.getProjectId() == null){e.setProjectId("0");}}).collect(Collectors.groupingBy(DtoCalculateChecksDisplay::getProjectId)).values().forEach(departmentLevel -> {
		//Collection<List<DtoCalculateChecksDisplay>> data = dtoCalculateChecksList.stream().collect(Collectors.groupingBy(DtoCalculateChecksDisplay::getMiscellaneousId)).values();
		dtoCalculateChecksList.stream().collect(Collectors.groupingBy(DtoCalculateChecksDisplay::getMiscellaneousId)).values().forEach(departmentLevel -> {
			/*
			 * ! important 
			 * the dtoDepartment Object is for department , BUT it should replaced by dtoMiscellanousHeader
			 * this because of time & important requirment 
			 */
			DtoDepartment dtoDepartment = new DtoDepartment();
			
			// Create DtoProject
			// find project by project ID
			//ProjectSetup projectSetup = null;
			//if(departmentLevel.get(0).getProjectId() != "0") {
			//	projectSetup = repositoryProjectSetup.findByIdAndIsDeleted(Integer.parseInt(departmentLevel.get(0).getProjectId()), false);	
			//}
			// setting attribute
			//if(projectSetup == null) {
			//	dtoDepartment.setProjectId("Managment");
			//	dtoDepartment.setProjectArabicName("ألإدارة");
			//	dtoDepartment.setProjectName(("001"));
			//}else {
			//	dtoDepartment.setProjectId(projectSetup.getProjectId());
			//	dtoDepartment.setProjectName(projectSetup.getProjectName());
			//	dtoDepartment.setProjectArabicName(projectSetup.getProjectArabicDescription());
			//}
			
			dtoDepartment.setMiscellaneousId(departmentLevel.get(0).getMiscellaneousId());
			dtoDepartment.setMiscellaneousCode(departmentLevel.get(0).getMiscellaneousDescrption());
			dtoDepartment.setMiscellaneousArabicCode(departmentLevel.get(0).getMiscellaneousArabicDescrption());
			
			dtoDepartment.setGregorianDate(payrolDate.toString());
			Department d = repositoryDepartment.findByDepartmentIdOne(departmentLevel.get(0).getDepartmentId());
			dtoDepartment.setDepartmentId(departmentLevel.get(0).getDepartmentId());
			dtoDepartment.setDepartmentName(d.getDepartmentDescription());
			dtoDepartment.setArabicDepartmentDescription(d.getArabicDepartmentDescription());
			List<DtoEmployeeCalculateChecckReport> employeeList = new ArrayList<>();
			departmentLevel.stream().collect(Collectors.groupingBy(DtoCalculateChecksDisplay::getEmployeeId)).values().forEach(employeeLevel -> {
				String langid = httpServletRequest.getHeader("langid");
				DtoEmployeeCalculateChecckReport employee = new DtoEmployeeCalculateChecckReport();
				EmployeeMaster emp = repositoryEmployeeMaster.findOneByEmployeeId(employeeLevel.get(0).getEmployeeID());
				dtoDepartment.setNumberOfEmployee(dtoDepartment.getNumberOfEmployee()+1);
				List<BigDecimal> payCode = new ArrayList<>(Collections.nCopies(5, BigDecimal.ZERO));
				List<BigDecimal> benefit = new ArrayList<>(Collections.nCopies(3, BigDecimal.ZERO));
				List<BigDecimal> deduction = new ArrayList<>(Collections.nCopies(4, BigDecimal.ZERO));
				
				
				
				employeeLevel.stream().collect(Collectors.groupingBy(DtoCalculateChecksDisplay::getCodeType)).values().forEach(employeeCodesLevels -> {
					AtomicInteger atomicInteger = new AtomicInteger(0);
					employeeCodesLevels.stream().forEach(amount -> {
						
						BigDecimal totalBasicD = BigDecimal.ZERO;
						BigDecimal totalHousingD = BigDecimal.ZERO;
						BigDecimal totalTransportationD = BigDecimal.ZERO;
						BigDecimal totalFixedD = BigDecimal.ZERO;
						BigDecimal totalOtherPaycodeD = BigDecimal.ZERO;
						
						BigDecimal totalBounsD = BigDecimal.ZERO;
						BigDecimal totalOvertimeD = BigDecimal.ZERO;
						BigDecimal totalOtherBenefitD = BigDecimal.ZERO;
						
						
						BigDecimal totalIncomesD= BigDecimal.ZERO;
						
						BigDecimal totalGOSID = BigDecimal.ZERO;
						BigDecimal totalLoanD = BigDecimal.ZERO;
						BigDecimal totalAbsentD = BigDecimal.ZERO;
						BigDecimal totalOtherDeductionD = BigDecimal.ZERO;
						BigDecimal totalDeductions = BigDecimal.ZERO;
						
						BigDecimal totalSummryD = BigDecimal.ZERO;
						
						if(langid.equals("2")) {
							employee.setName(emp.getEmployeeFirstNameArabic()+" "+ emp.getEmployeeMiddleNameArabic()+" "+ emp.getEmployeeLastNameArabic());
							employee.setStartSate(emp.getEmployeeHireDate().toString());
							employee.setLocation(emp.getLocation().getArabicDescription());
							employee.setEmployeeCode(amount.getEmployeeId());
						}else {
							employee.setName(amount.getEmployeeName());
							employee.setStartSate(emp.getEmployeeHireDate().toString());
							employee.setLocation(emp.getLocation().getDescription());
							employee.setEmployeeCode(amount.getEmployeeId());
						}
						
						
						//employee.setStartSate(amount.getDa);
						if(amount.getCodeType() == 1) { // paycode
							if(amount.getCodeId().equals("BASIC")) {
								payCode.set(0, amount.getTotalAmount());
								totalBasicD = totalBasicD.add(amount.getTotalAmount());
								totalIncomesD = totalIncomesD.add(amount.getTotalAmount());
							}else if(amount.getCodeId().equals("HOUSE")) {
								payCode.set(1, amount.getTotalAmount());
								totalHousingD = totalHousingD.add(amount.getTotalAmount());
								totalIncomesD = totalIncomesD.add(amount.getTotalAmount());
							}else if(amount.getCodeId().equals("TRANS")) {
								payCode.set(2, amount.getTotalAmount());
								totalTransportationD = totalTransportationD.add(amount.getTotalAmount());
								totalIncomesD = totalIncomesD.add(amount.getTotalAmount());
							}else if(amount.getCodeId().equals("FIXED")) {
								payCode.set(3, amount.getTotalAmount());
								totalFixedD = totalFixedD.add(amount.getTotalAmount());
								totalIncomesD = totalIncomesD.add(amount.getTotalAmount());
							}else {
								BigDecimal newValue = BigDecimal.ZERO;
								newValue = newValue.add(payCode.get(4));
								newValue = newValue.add(amount.getTotalAmount());
								payCode.set(4, newValue);
								totalOtherPaycodeD =totalOtherPaycodeD.add(amount.getTotalAmount());
								totalIncomesD = totalIncomesD.add(amount.getTotalAmount());
							}
							//payCode.set(atomicInteger.getAndIncrement(), amount.getTotalAmount());
							//payCode.add(amount.getTotalAmount());
							
						}else if(amount.getCodeType() == 3) { // Benefit 
							if(amount.getCodeId().equals("BONUS")) {
								BigDecimal newValue = BigDecimal.ZERO;
								newValue = newValue.add(benefit.get(0));
								newValue = newValue.add(amount.getTotalAmount());
								benefit.set(0, newValue);
								totalBounsD=totalBounsD.add(amount.getTotalAmount());
								totalIncomesD = totalIncomesD.add(amount.getTotalAmount());
							}else if(amount.getCodeId().equals("OVER") || 
									amount.getCodeId().equals("OVER1") ||
									amount.getCodeId().equals("OVER2") ||
									amount.getCodeId().equals("OVER3")) {
								BigDecimal newValue = BigDecimal.ZERO;
								newValue = newValue.add(benefit.get(1));
								newValue = newValue.add(amount.getTotalAmount());
								benefit.set(1, newValue);
								totalOvertimeD = totalOvertimeD.add(amount.getTotalAmount());
								totalIncomesD = totalIncomesD.add(amount.getTotalAmount());
							}else  {
								BigDecimal newValue = BigDecimal.ZERO;
								newValue = newValue.add(benefit.get(2));
								newValue = newValue.add(amount.getTotalAmount());
								benefit.set(2, newValue);
								totalOtherBenefitD = totalOtherBenefitD.add(amount.getTotalAmount());
								totalIncomesD = totalIncomesD.add(amount.getTotalAmount());

							}
								
							
							//benefit.set(atomicInteger.getAndIncrement(),amount.getTotalAmount());
							//benefit.add(amount.getTotalAmount());	
						}else if(amount.getCodeType() == 2) { // deduction
							if(amount.getCodeId().equals("GOSI-9") || 
									amount.getCodeId().equals("SANED-1") ||
									amount.getCodeId().equals("GOSI-10")) {
								BigDecimal newValue = BigDecimal.ZERO;
								newValue = newValue.add(deduction.get(0));
								newValue = newValue.add(amount.getTotalAmount());
								deduction.set(0, newValue);
								totalGOSID = totalGOSID.add(amount.getTotalAmount());
								totalDeductions = totalDeductions.add(amount.getTotalAmount());
							}else if(amount.getCodeId().equals("LOAN")) {
								BigDecimal newValue = BigDecimal.ZERO;
								newValue = newValue.add(deduction.get(1));
								newValue = newValue.add(amount.getTotalAmount());
								deduction.set(1, newValue);
								totalLoanD = totalLoanD.add(amount.getTotalAmount());
								totalDeductions = totalDeductions.add(amount.getTotalAmount());
							}else if(amount.getCodeId().equals("ABSEN")) {
								BigDecimal newValue = BigDecimal.ZERO;
								newValue = newValue.add(deduction.get(2));
								newValue = newValue.add(amount.getTotalAmount());
								deduction.set(2, newValue);
								totalAbsentD = totalAbsentD.add(amount.getTotalAmount());
								totalDeductions = totalDeductions.add(amount.getTotalAmount());
							}else {
								BigDecimal newValue = BigDecimal.ZERO;
								newValue = newValue.add(deduction.get(3));
								newValue = newValue.add(amount.getTotalAmount());
								deduction.set(3, newValue);
								totalOtherDeductionD = totalOtherBenefitD.add(amount.getTotalAmount());
								totalDeductions = totalDeductions.add(amount.getTotalAmount());
							}
							//deduction.set(atomicInteger.getAndIncrement(), amount.getTotalAmount());
							//deduction.add(amount.getTotalAmount());	
						}
						
						dtoDepartment.setTotalBasic(dtoDepartment.getTotalBasic().add(totalBasicD));
						dtoDepartment.setTotalHousing(dtoDepartment.getTotalHousing().add(totalHousingD));
						dtoDepartment.setTotalTransportation(dtoDepartment.getTotalTransportation().add(totalTransportationD));
						dtoDepartment.setTotalFixed(dtoDepartment.getTotalFixed().add(totalFixedD));
						dtoDepartment.setTotalOtherPaycode(dtoDepartment.getTotalOtherPaycode().add(totalOtherPaycodeD));
						
						
						dtoDepartment.setTotalBouns(dtoDepartment.getTotalBouns().add(totalBounsD));
						dtoDepartment.setTotalOvertime(dtoDepartment.getTotalOvertime().add(totalOvertimeD));
						dtoDepartment.setTotalOtherBenefit(dtoDepartment.getTotalOtherBenefit().add(totalOtherBenefitD));
						
						dtoDepartment.setTotalIncomes(dtoDepartment.getTotalIncomes().add(totalIncomesD));
						
						dtoDepartment.setTotalGOSI(dtoDepartment.getTotalGOSI().add(totalGOSID));
						dtoDepartment.setTotalLoan(dtoDepartment.getTotalLoan().add(totalLoanD));
						dtoDepartment.setTotalAbsent(dtoDepartment.getTotalAbsent().add(totalAbsentD));
						dtoDepartment.setTotalOtherDeduction(dtoDepartment.getTotalOtherDeduction().add(totalOtherDeductionD));
						dtoDepartment.setTotalDeductions(dtoDepartment.getTotalDeductions().add(totalDeductions));
						dtoDepartment.setTotalSummry(dtoDepartment.getTotalSummry().add(totalIncomesD).subtract(totalDeductions));
						
						
					});
					BigDecimal totalIncome = new BigDecimal(0);
					BigDecimal totalDeduction = new BigDecimal(0);
					BigDecimal totalSummry = new BigDecimal(0);
					for(BigDecimal number : payCode) {
						totalIncome = totalIncome.add(number);
					}
					for(BigDecimal number : benefit) {
						totalIncome = totalIncome.add(number);
					}
					for(BigDecimal number : deduction) {
						totalDeduction = totalDeduction.add(number);
					}
					totalSummry = totalSummry.add(totalIncome);
					totalSummry = totalSummry.subtract(totalDeduction);
					
					employee.setPayCodeList(payCode);
					employee.setBenefitList(benefit);
					employee.setDeductionList(deduction);
					
					employee.setTotalDeducion(totalDeduction);
					employee.setTotalIncome(totalIncome);
					employee.setTotalSummry(totalSummry);
					
					
					
				});
				employeeList.add(employee);
			});
			
			
			
			dtoDepartment.setEmployeeList(employeeList);
			list.add(dtoDepartment);
		});
		
		list.sort(Comparator.comparing(DtoDepartment::getDepartmentId));
		list.stream().forEach(dtoDepartment -> {
			dtoDepartment.getEmployeeList().sort(Comparator.comparing(DtoEmployeeCalculateChecckReport::getEmployeeCode));
		});
		dtoSearch.setRecords(list);
		return dtoSearch;
	}
	
public void generateExcelFileBeforeProcessing(List<DtoDepartment> departments , HttpServletRequest request, HttpServletResponse response ) {
	final ServletContext servletContext = request.getSession().getServletContext();
    MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
    final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
    String temperotyFilePath = tempDirectory.getAbsolutePath();
	XSSFWorkbook workbook = new XSSFWorkbook();
	
	String langid = httpServletRequest.getHeader("langid");
	String[] COLUMNs ;
	if(langid.equals("2")) {
		COLUMNs =new String[] {"رقم الموظف", "اسم الموظف", "الموقع", "تاريخ البداية",
					"الأساسي", "بدل السكن", "بدل نقل","بدل ثابت","بدلات آخرى", //paycode
					"مكافات", "عمل اضافي", "بدلات  اخرى", //benefit
					"مجمل الدخل",
					"التامين", "سلف", "غياب ","استقطاعات آخرى ", // deductions
					"اجمالي الاستقطاعات",
					"صافي المجموع"
					};
	}else {
		COLUMNs = new String[] {"Employee Number", "Employee Name", "Location", "Start Date",
				"Basic", "Housing", "Transportation","Fixed","Others", //paycode
				"Bonus", "Overtime", "Others", //benefit
				"Total Income",
				"GOSI", "Loan","Absent", "Others", // deductions
				"Total Deduction",
				"Net"
				};
	}
	
	try {
       // Write the workbook in file system
        String excelFileName = "MasterData.xlsx";
        String fileName = URLEncoder.encode(excelFileName, "UTF-8");
        fileName = URLDecoder.decode(fileName, "ISO8859_1");
        response.setHeader("Content-disposition", "attachment; filename=" + fileName);
        response.setContentType(mimetypesFileTypeMap.getContentType(excelFileName));

        FileOutputStream out = new FileOutputStream(new File(temperotyFilePath + "\\" + excelFileName));
        CreationHelper createHelper = workbook.getCreationHelper();
      	 
			Sheet sheet = workbook.createSheet("Report");
			//  setting up  defualt print option
			sheet.setFitToPage(true);
			sheet.getPrintSetup().setPaperSize(XSSFPrintSetup.A3_PAPERSIZE);
			sheet.getPrintSetup().setFitWidth( (short) 1);
			sheet.getPrintSetup().setFitHeight( (short) 0);
			sheet.getPrintSetup().setLandscape(true);
			sheet.setRepeatingRows(CellRangeAddress.valueOf("1:6"));
			if(langid.equals("2")) {
				sheet.setRightToLeft(true);
			}
			
			//Title data & Header report style
			String title;
			//Rport Header Info
			if(langid.equals("2")) {
				title  = "تقرير رواتب "; 
			}else {
				title  = "Payroll Report"; 
			}
			
			Date printDate = new Date();
			Date periodDate=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(departments.get(0).getGregorianDate());  
			
			String companyName= httpServletRequest.getHeader("tenantid");
				       			
			
			// creation and style for cell and row
			//Creation
			Row titlsRow = sheet.createRow(1);
			Row titlsRow2 = sheet.createRow(2);
			Cell titleCell = titlsRow.createCell(7);
			Cell titleCell1 = titlsRow.createCell(8);
			Cell titleCell2 = titlsRow.createCell(9);
			Cell titleCell3 = titlsRow2.createCell(7);
			Cell titleCell4 = titlsRow2.createCell(8);
			Cell titleCell5 = titlsRow2.createCell(9);
			XSSFFont titleFont = workbook.createFont();
			XSSFCellStyle titleStyle = workbook.createCellStyle();
			
			//Styling
			titleStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
			titleStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
			titleStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
			titleStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
			titleStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
			titleFont.setBold(true);
			titleFont.setFontHeightInPoints((short) 15);
			titleCell.setCellValue(title);
			titleStyle.setFont(titleFont);
			titleCell.setCellStyle(titleStyle);
			titleCell1.setCellStyle(titleStyle);
			titleCell2.setCellStyle(titleStyle);
			titleCell3.setCellStyle(titleStyle);
			titleCell4.setCellStyle(titleStyle);
			titleCell5.setCellStyle(titleStyle);
			
			sheet.addMergedRegion(new CellRangeAddress(1,2,7,9));
			
			//Date Cell (print date)
			Cell dateCell = titlsRow2.createCell(1);
			CellStyle cell1Style = workbook.createCellStyle();
			cell1Style.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			if(langid.equals("2")) {
				cell1Style.setAlignment(CellStyle.ALIGN_RIGHT);
			}else {
				cell1Style.setAlignment(CellStyle.ALIGN_LEFT);
			}
			
			dateCell.setCellValue(printDate);
			dateCell.setCellStyle(cell1Style);
		
			//period text
			String period;
			if(langid.equals("2")) {
				period  = "الفترة  "; 
			}else {
				period  = "Period"; 
			}
			
			//period Date
			Row periodTitle = sheet.createRow(3);
			Cell periodCellText = periodTitle.createCell(7);
			periodCellText.setCellValue(period);
			Cell periodCell = periodTitle.createCell(9);
			CellStyle periodStyle = workbook.createCellStyle();
			periodStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM-yyyy"));
			periodStyle.setAlignment(CellStyle.ALIGN_LEFT);
			periodCell.setCellValue(periodDate);
			periodCell.setCellStyle(periodStyle);
			periodCellText.setCellStyle(cell1Style);
			
			
			// Table Header
			Font headerFont = workbook.createFont();
			//headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
	 
			// Row for Header
			Row headerRow = sheet.createRow(5);
	 
			// Header
			for (int col = 1; col <= COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				
				XSSFFont tableHeader = workbook.createFont();
				XSSFCellStyle tableHeaderStyle = workbook.createCellStyle();
   			
				tableHeaderStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
				tableHeaderStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
				tableHeaderStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
				tableHeaderStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
				tableHeader.setBold(true);
				tableHeader.setFontHeightInPoints((short) 10);
				tableHeaderStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
				tableHeaderStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellValue(COLUMNs[col-1]);
				cell.setCellStyle(tableHeaderStyle);
				tableHeaderStyle.setFont(tableHeader);
				//cell.setCellStyle(tableHeaderStyle);
			}
	 
			// CellStyle for Age
			//CellStyle ageCellStyle = workbook.createCellStyle();
			//ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
			
			
			AtomicInteger rows = new AtomicInteger(6);
			AtomicInteger cols = new AtomicInteger(1);
			
			// totals styles
			XSSFFont totalsFonst = workbook.createFont();
			XSSFCellStyle totalsStyle = workbook.createCellStyle();
			totalsStyle.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			totalsStyle.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			totalsStyle.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
			totalsStyle.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			totalsStyle.setAlignment(CellStyle.ALIGN_RIGHT);
			
			//Department Styles
			XSSFCellStyle departmentStyle = workbook.createCellStyle();
			departmentStyle.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			departmentStyle.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			departmentStyle.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
			departmentStyle.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			
			if(langid.equals("2")) {
				departmentStyle.setAlignment(CellStyle.ALIGN_RIGHT);
			}else {
				departmentStyle.setAlignment(CellStyle.ALIGN_LEFT);
			}
			
			
			
			BigDecimal overAllTotalsEmployee = BigDecimal.ZERO;
			BigDecimal overAllTotalsBasic = BigDecimal.ZERO;
			BigDecimal overAllTotalsHousing = BigDecimal.ZERO;
			BigDecimal overAllTotalsTransportation = BigDecimal.ZERO;
			BigDecimal overAllTotalsFixed = BigDecimal.ZERO;
			BigDecimal overAllTotalsOtherPaycode = BigDecimal.ZERO;
			BigDecimal overAllTotalsBouns = BigDecimal.ZERO;
			BigDecimal overAllTotalsOvertime = BigDecimal.ZERO;
			BigDecimal overAllTotalsOtherBenufit = BigDecimal.ZERO;
			BigDecimal overAllTotalsIncome = BigDecimal.ZERO;
			BigDecimal overAllTotalsGOSI = BigDecimal.ZERO;
			BigDecimal overAllTotalsLoan = BigDecimal.ZERO;
			BigDecimal overAllTotalsAbsent = BigDecimal.ZERO;
			BigDecimal overAllTotalsOthersDeduction = BigDecimal.ZERO;
			BigDecimal overAllTotalsDeduction = BigDecimal.ZERO;
			BigDecimal overAllTotalsSumary = BigDecimal.ZERO;
			
			CellStyle employeeStyle = workbook.createCellStyle();
			
			employeeStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
			employeeStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
			employeeStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
			employeeStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
			
			if(langid.equals("2")) {
				employeeStyle.setAlignment(CellStyle.ALIGN_RIGHT);
			}else {
				employeeStyle.setAlignment(CellStyle.ALIGN_LEFT);
			}
			
			
			for (DtoDepartment dtoDepartment : departments) {
				
				Row rowDepartment = sheet.createRow(rows.getAndIncrement());
				Cell departmentNumber = rowDepartment.createCell(cols.getAndIncrement());
				departmentNumber.setCellValue(dtoDepartment.getMiscellaneousId());
				departmentNumber.setCellStyle(departmentStyle);
				Cell departmentName = rowDepartment.createCell(cols.getAndIncrement());
				if(langid.equals("2")) {
					departmentName.setCellValue(dtoDepartment.getArabicDepartmentDescription());
					departmentName.setCellValue(dtoDepartment.getMiscellaneousArabicCode());
				}else {
					departmentName.setCellValue(dtoDepartment.getDepartmentName());
					departmentName.setCellValue(dtoDepartment.getMiscellaneousCode());
				}
				
				departmentName.setCellStyle(departmentStyle);
				
				
				
				sheet.addMergedRegion(new CellRangeAddress(rows.get()-1, rows.get()-1, cols.get()-1, COLUMNs.length));
				for(int i = rows.get()-1 ; i<=(rows.get()-1);i++) {
					//ROW Level
					for(int j =cols.get(); j <= COLUMNs.length;j++ ) {
						//COLUMN Level
						rowDepartment.createCell(j).setCellStyle(departmentStyle);
					}
				}
				
				cols.set(1);
				dtoDepartment.getEmployeeList().stream().forEach(employee -> {
					
				Row row = sheet.createRow(rows.getAndIncrement());
   				Cell employeeCode = row.createCell(cols.getAndIncrement());
   				employeeCode.setCellValue((String) ""+employee.getEmployeeCode());
   				employeeCode.setCellStyle(employeeStyle);
   				Cell employeeName= row.createCell(cols.getAndIncrement());
   				employeeName.setCellValue((String) employee.getName());
   				employeeName.setCellStyle(employeeStyle);
   				Cell employeeLocation = row.createCell(cols.getAndIncrement());
   				employeeLocation.setCellValue((String) employee.getLocation());
   				employeeLocation.setCellStyle(employeeStyle);
   				Cell employeeState = row.createCell(cols.getAndIncrement());
   				String str = employee.getStartSate();
   				String[] datesplit = str.split(" ");
   				employeeState.setCellValue((String) datesplit[0]);
   				employeeState.setCellStyle(employeeStyle);
   				employee.getPayCodeList().stream().forEach(paucodes -> {
   					Cell paycodes=row.createCell(cols.getAndIncrement());
   					paycodes.setCellValue( paucodes.intValue());
   					paycodes.setCellStyle(totalsStyle);
   				});
   				employee.getBenefitList().stream().forEach(benefits -> {
   					Cell benefit =row.createCell(cols.getAndIncrement());
   					benefit.setCellValue( benefits.intValue());
   					benefit.setCellStyle(totalsStyle);
   				});
   				Cell totalIncome =row.createCell(cols.getAndIncrement());
   				totalIncome.setCellValue(employee.getTotalIncome().intValue());
   				totalIncome.setCellStyle(totalsStyle);
   				employee.getDeductionList().stream().forEach(deduction -> {
   					Cell deductions = row.createCell(cols.getAndIncrement());
   					deductions.setCellValue( deduction.intValue());
   					deductions.setCellStyle(totalsStyle);
   				});
   				Cell totalDeductions = row.createCell(cols.getAndIncrement());
   				totalDeductions.setCellValue(employee.getTotalDeducion().intValue());
   				totalDeductions.setCellStyle(totalsStyle);
   				Cell totalSummry = row.createCell(cols.getAndIncrement());
   				totalSummry.setCellValue(employee.getTotalSummry().intValue());
   				totalSummry.setCellStyle(totalsStyle);
   				cols.set(1);
				});
				
				
				
				Row row = sheet.createRow(rows.getAndIncrement());
				Cell cellText = row.createCell(cols.getAndIncrement());
				cellText.setCellStyle(departmentStyle);
				
				if(langid.equals("2")) {
					cellText.setCellValue("عدد الموظفين");
				}else {
					cellText.setCellValue("Number of Employees");
				}
				
				Cell cellValue = row.createCell(cols.getAndIncrement());
				cellValue.setCellStyle(totalsStyle);
				cellValue.setCellValue(dtoDepartment.getEmployeeList().size());
				cellValue.setCellStyle(departmentStyle);
				overAllTotalsEmployee = overAllTotalsEmployee.add(new BigDecimal(dtoDepartment.getEmployeeList().size()));
				
				sheet.addMergedRegion(new CellRangeAddress(rows.get()-1,rows.get()-1,cols.get()-1,cols.get()+1));
				
				row.createCell(cols.getAndIncrement()).setCellStyle(totalsStyle);
				row.createCell(cols.getAndIncrement()).setCellStyle(totalsStyle);
				Cell cellValueTotalBaisc = row.createCell(cols.getAndIncrement());
				cellValueTotalBaisc.setCellStyle(totalsStyle);
				cellValueTotalBaisc.setCellValue(""+dtoDepartment.getTotalBasic().intValue());
				overAllTotalsBasic = overAllTotalsBasic.add(dtoDepartment.getTotalBasic());
				
				Cell cellValueTotalHousing = row.createCell(cols.getAndIncrement());
				cellValueTotalHousing.setCellStyle(totalsStyle);
				cellValueTotalHousing.setCellValue(""+dtoDepartment.getTotalHousing().intValue());
				overAllTotalsHousing = overAllTotalsHousing.add(dtoDepartment.getTotalHousing());
				
				Cell cellValueTotalTransportation = row.createCell(cols.getAndIncrement());
				cellValueTotalTransportation.setCellStyle(totalsStyle);
				cellValueTotalTransportation.setCellValue(""+dtoDepartment.getTotalTransportation().intValue());
				overAllTotalsTransportation = overAllTotalsTransportation.add(dtoDepartment.getTotalTransportation());
				
				Cell cellValueTotalFixed = row.createCell(cols.getAndIncrement());
				cellValueTotalFixed.setCellStyle(totalsStyle);
				cellValueTotalFixed.setCellValue(""+dtoDepartment.getTotalFixed().intValue());
				overAllTotalsFixed = overAllTotalsFixed.add(dtoDepartment.getTotalFixed());
				
				Cell cellValueTotalOtherPaycode = row.createCell(cols.getAndIncrement());
				cellValueTotalOtherPaycode.setCellStyle(totalsStyle);
				cellValueTotalOtherPaycode.setCellValue(""+dtoDepartment.getTotalOtherPaycode().intValue());
				overAllTotalsOtherPaycode = overAllTotalsOtherPaycode.add(dtoDepartment.getTotalOtherPaycode());
				
				Cell cellValueTotalBouns = row.createCell(cols.getAndIncrement());
				cellValueTotalBouns.setCellStyle(totalsStyle);
				cellValueTotalBouns.setCellValue(""+dtoDepartment.getTotalBouns().intValue());
				overAllTotalsBouns = overAllTotalsBouns.add(dtoDepartment.getTotalBouns());
				
				Cell cellValueTotalOvertime = row.createCell(cols.getAndIncrement());
				cellValueTotalOvertime.setCellStyle(totalsStyle);
				cellValueTotalOvertime.setCellValue(""+dtoDepartment.getTotalOvertime().intValue());
				overAllTotalsOvertime = overAllTotalsOvertime.add(dtoDepartment.getTotalOvertime());
				
				Cell cellValueTotalOtherBenefit = row.createCell(cols.getAndIncrement());
				cellValueTotalOtherBenefit.setCellStyle(totalsStyle);
				cellValueTotalOtherBenefit.setCellValue(""+dtoDepartment.getTotalOtherBenefit().intValue());
				overAllTotalsOtherBenufit = overAllTotalsOtherBenufit.add(dtoDepartment.getTotalOtherBenefit());
				
				Cell cellValueTotalIncome = row.createCell(cols.getAndIncrement());
				cellValueTotalIncome.setCellStyle(totalsStyle);
				cellValueTotalIncome.setCellValue(""+dtoDepartment.getTotalIncomes().intValue());
				overAllTotalsIncome = overAllTotalsIncome.add(dtoDepartment.getTotalIncomes());
				
				Cell cellValueTotalGOSI = row.createCell(cols.getAndIncrement());
				cellValueTotalGOSI.setCellStyle(totalsStyle);
				cellValueTotalGOSI.setCellValue(""+dtoDepartment.getTotalGOSI().intValue());
				overAllTotalsGOSI = overAllTotalsGOSI.add(dtoDepartment.getTotalGOSI());
				
				Cell cellValueTotalLoan = row.createCell(cols.getAndIncrement());
				cellValueTotalLoan.setCellStyle(totalsStyle);
				cellValueTotalLoan.setCellValue(""+dtoDepartment.getTotalLoan().intValue());
				overAllTotalsLoan = overAllTotalsLoan.add(dtoDepartment.getTotalLoan());
				
				Cell cellValueTotalAbsent = row.createCell(cols.getAndIncrement());
				cellValueTotalAbsent.setCellStyle(totalsStyle);
				cellValueTotalAbsent.setCellValue(""+dtoDepartment.getTotalAbsent().intValue());
				overAllTotalsAbsent = overAllTotalsAbsent.add(dtoDepartment.getTotalAbsent());
				
				Cell cellValueTotalOther = row.createCell(cols.getAndIncrement());
				cellValueTotalOther.setCellStyle(totalsStyle);
				cellValueTotalOther.setCellValue(""+dtoDepartment.getTotalOtherDeduction().intValue());
				overAllTotalsOthersDeduction = overAllTotalsOthersDeduction.add(dtoDepartment.getTotalOtherDeduction());
				
				Cell cellValueTotalDeduction = row.createCell(cols.getAndIncrement());
				cellValueTotalDeduction.setCellStyle(totalsStyle);
				cellValueTotalDeduction.setCellValue(""+dtoDepartment.getTotalDeductions().intValue());
				overAllTotalsDeduction = overAllTotalsDeduction.add(dtoDepartment.getTotalDeductions());
				
				Cell cellValueTotalSummary = row.createCell(cols.getAndIncrement());
				cellValueTotalSummary.setCellStyle(totalsStyle);
				cellValueTotalSummary.setCellValue(""+dtoDepartment.getTotalSummry().intValue());
				overAllTotalsSumary = overAllTotalsSumary.add(dtoDepartment.getTotalSummry());
				
				cols.set(1);
			}
			
			rows.getAndIncrement();
	 
			Row summryRow = sheet.createRow(rows.getAndIncrement());
			Cell employeeSummryText = summryRow.createCell(1);
			Cell employeeSummry = summryRow.createCell(2);
			employeeSummry.setCellStyle(departmentStyle);
			employeeSummryText.setCellStyle(departmentStyle);
			employeeSummry.setCellValue(""+overAllTotalsEmployee.intValue());
			
			if(langid.equals("2")) {
				employeeSummryText.setCellValue("إجمالي الموظفين");
			}else {
				employeeSummryText.setCellValue("Employees Totals:");
			}
			
			
			summryRow.createCell(3).setCellStyle(totalsStyle);
			summryRow.createCell(4).setCellStyle(totalsStyle);
			
			
			Cell basicSummry = summryRow.createCell(5);
			basicSummry.setCellStyle(totalsStyle);
			basicSummry.setCellValue(""+overAllTotalsBasic.intValue());
			
			sheet.addMergedRegion(new CellRangeAddress(rows.get()-1,rows.get()-1,2,4));

			Cell houseSummry = summryRow.createCell(6);
			houseSummry.setCellStyle(totalsStyle);
			houseSummry.setCellValue(""+overAllTotalsHousing.intValue());
			

			Cell transportationSummry = summryRow.createCell(7);
			transportationSummry.setCellStyle(totalsStyle);
			transportationSummry.setCellValue(""+overAllTotalsTransportation.intValue());
			

			Cell fixedSummry = summryRow.createCell(8);
			fixedSummry.setCellStyle(totalsStyle);
			fixedSummry.setCellValue(""+overAllTotalsFixed.intValue());
			

			Cell othersPaycodeSummry = summryRow.createCell(9);
			othersPaycodeSummry.setCellStyle(totalsStyle);
			othersPaycodeSummry.setCellValue(""+overAllTotalsOtherPaycode.intValue());
			

			Cell bounsSummry = summryRow.createCell(10);
			bounsSummry.setCellStyle(totalsStyle);
			bounsSummry.setCellValue(""+overAllTotalsBouns.intValue());
			

			Cell overtimeSummry = summryRow.createCell(11);
			overtimeSummry.setCellStyle(totalsStyle);
			overtimeSummry.setCellValue(""+overAllTotalsOvertime.intValue());
			

			Cell otherBenefitSummry = summryRow.createCell(12);
			otherBenefitSummry.setCellStyle(totalsStyle);
			otherBenefitSummry.setCellValue(""+overAllTotalsOtherBenufit.intValue());
			
			Cell totalIncomesSummry = summryRow.createCell(13);
			totalIncomesSummry.setCellStyle(totalsStyle);
			totalIncomesSummry.setCellValue(""+overAllTotalsIncome.intValue());
			

			Cell GOSISummry = summryRow.createCell(14);
			GOSISummry.setCellStyle(totalsStyle);
			GOSISummry.setCellValue(""+overAllTotalsGOSI.intValue());
			
			Cell loanSummry = summryRow.createCell(15);
			loanSummry.setCellStyle(totalsStyle);
			loanSummry.setCellValue(""+overAllTotalsLoan.intValue());
			
			Cell absentSummry = summryRow.createCell(16);
			absentSummry.setCellStyle(totalsStyle);
			absentSummry.setCellValue(""+overAllTotalsAbsent.intValue());
			
			
			
			Cell othersDeductionsSummry = summryRow.createCell(17);
			othersDeductionsSummry.setCellStyle(totalsStyle);
			othersDeductionsSummry.setCellValue(""+overAllTotalsOthersDeduction.intValue());
			
			Cell totalDeductionsSummry = summryRow.createCell(18);
			totalDeductionsSummry.setCellStyle(totalsStyle);
			totalDeductionsSummry.setCellValue(""+overAllTotalsDeduction.intValue());
			
			Cell totalSummryOfSummry = summryRow.createCell(19);
			totalSummryOfSummry.setCellStyle(totalsStyle);
			totalSummryOfSummry.setCellValue(""+overAllTotalsSumary.intValue());
			
			for(int i =1 ; i<22 ; i++) {
				sheet.autoSizeColumn(i);
			}
			
			
			
           workbook.write(out);
           out.close();

           ByteArrayOutputStream baos = new ByteArrayOutputStream();
           baos = convertexcelToByteArrayOutputStream(temperotyFilePath + "\\" + excelFileName);
           OutputStream os = response.getOutputStream();
           baos.writeTo(os);
           os.flush();

    } catch (Exception e) {
    	//Logger.info(Arrays.toString(e.getStackTrace()));
    }

		
	}

private ByteArrayOutputStream convertexcelToByteArrayOutputStream(String fileName) {
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	try (InputStream inputStream = new FileInputStream(fileName)){
		byte[] buffer = new byte[1024];
		baos = new ByteArrayOutputStream();

		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			baos.write(buffer, 0, bytesRead);
		}

	} catch (Exception e) {
//		LOGGER.info(Arrays.toString(e.getStackTrace()));
	}   
	return baos;
}
	

public DtoSearch generateEcelFileBeforeProcessing(DtoSearch dtoSearch ) {
	

	List<DtoCalculateChecksDisplay> dtoCalculateChecksList = getDtoCalculateChecksList(dtoSearch,
			2);
	
	
	List<DtoDepartment> list = new ArrayList<>();
	dtoCalculateChecksList.stream().collect(Collectors.groupingBy(DtoCalculateChecksDisplay::getDepartmentId)).values().forEach(departmentLevel -> {
		log.info(departmentLevel);
		DtoDepartment dtoDepartment = new DtoDepartment();
		Department d = repositoryDepartment.findByDepartmentIdOne(departmentLevel.get(0).getDepartmentId());
		
		dtoDepartment.setDepartmentId(departmentLevel.get(0).getDepartmentId());
		dtoDepartment.setDepartmentName(d.getDepartmentDescription());
		List<DtoEmployeeCalculateChecckReport> employeeList = new ArrayList<>();
		departmentLevel.stream().collect(Collectors.groupingBy(DtoCalculateChecksDisplay::getEmployeeId)).values().forEach(employeeLevel -> {
			
			DtoEmployeeCalculateChecckReport employee = new DtoEmployeeCalculateChecckReport();
			EmployeeMaster emp = repositoryEmployeeMaster.findOneByEmployeeId(employeeLevel.get(0).getEmployeeID());
			dtoDepartment.setNumberOfEmployee(dtoDepartment.getNumberOfEmployee()+1);
			List<BigDecimal> payCode = new ArrayList<>(Collections.nCopies(5, BigDecimal.ZERO));
			List<BigDecimal> benefit = new ArrayList<>(Collections.nCopies(2, BigDecimal.ZERO));
			List<BigDecimal> deduction = new ArrayList<>(Collections.nCopies(6, BigDecimal.ZERO));
			
			employeeLevel.stream().collect(Collectors.groupingBy(DtoCalculateChecksDisplay::getCodeType)).values().forEach(employeeCodesLevels -> {
				AtomicInteger atomicInteger = new AtomicInteger(0);
				employeeCodesLevels.stream().forEach(amount -> {
					
					employee.setName(amount.getEmployeeName());
					employee.setStartSate(emp.getEmployeeHireDate().toString());
					employee.setLocation(emp.getLocation().getLocationAddress());
					employee.setEmployeeCode(amount.getEmployeeId());
					//employee.setStartSate(amount.getDa);
					if(amount.getCodeType() == 1) { // paycode
						//dtoCalculateChecksDisplay.setAmountBasic(amount.getTotalAmount());
						payCode.set(atomicInteger.getAndIncrement(), amount.getTotalAmount());
						//payCode.add(amount.getTotalAmount());
						
					}else if(amount.getCodeType() == 2) { // deduction
						benefit.set(atomicInteger.getAndIncrement(),amount.getTotalAmount());
						//benefit.add(amount.getTotalAmount());	
					}else if(amount.getCodeType() == 3) { // Benefit
						deduction.set(atomicInteger.getAndIncrement(), amount.getTotalAmount());
						//deduction.add(amount.getTotalAmount());	
					}
				});
				BigDecimal totalIncome = new BigDecimal(0);
				BigDecimal totalDeduction = new BigDecimal(0);
				BigDecimal totalSummry = new BigDecimal(0);
				for(BigDecimal number : payCode) {
					totalIncome = totalIncome.add(number);
				}
				for(BigDecimal number : benefit) {
					totalIncome = totalIncome.add(number);
				}
				for(BigDecimal number : deduction) {
					totalDeduction = totalDeduction.add(number);
				}
				totalSummry = totalSummry.add(totalIncome);
				totalSummry = totalSummry.subtract(totalDeduction);
				
				employee.setPayCodeList(payCode);
				employee.setBenefitList(benefit);
				employee.setDeductionList(deduction);
				
				employee.setTotalDeducion(totalDeduction);
				employee.setTotalIncome(totalIncome);
				employee.setTotalSummry(totalSummry);
			});
			employeeList.add(employee);
		});
		dtoDepartment.setEmployeeList(employeeList);
		list.add(dtoDepartment);
	});
	dtoSearch.setRecords(list);
	

	
	
	
	
	return dtoSearch;
}
	public DtoCalculateChecks getTotalNumbers(DtoCalculateChecks dtoCalculateChecks) {
		try {
			List<BuildChecks> buildCheckList = repositoryBuildChecks.getByDefaultId(dtoCalculateChecks.getId());
			if(buildCheckList!=null) {
				for (BuildChecks buildChecks : buildCheckList) {
					Integer employeeCount=  repositoryTransactionEntryDetail.predictivegetAllTransactionEntryDetailEmployeeCount(buildChecks.getId());
					log.info("Total Employee Count is:-->"+employeeCount);
				}
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoCalculateChecks;
	}

	
	public DtoSearch getAllByDefautIdWithoutPagination(DtoSearch dtoSearch) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		if (dtoSearch != null) {
				
			List<DtoCalculateChecksDisplay> dtoCalculateChecksList = getDtoCalculateChecksList(dtoSearch,
					loggedInUserId);
			
			
			if(dtoSearch.getSortOn()!=null) {
				if(dtoSearch.getSortOn().equals("employee")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByEmployee sortbyEmployee = e.new SortByEmployee(); 
					Collections.sort(dtoCalculateChecksList, sortbyEmployee);
				}
				
				if(dtoSearch.getSortOn().equals("department")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByDepartment sortByDepartment = e.new SortByDepartment(); 
					Collections.sort(dtoCalculateChecksList, sortByDepartment);
				}
				
				if(dtoSearch.getSortOn().equals("codeId")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByCodeId sortByCodeId = e.new SortByCodeId(); 
					Collections.sort(dtoCalculateChecksList, sortByCodeId);
				}
				
				if(dtoSearch.getSortOn().equals("projectId")) {
					DtoCalculateChecksDisplay e = new DtoCalculateChecksDisplay();
					DtoCalculateChecksDisplay.SortByProjectId sortByProjectId = e.new SortByProjectId(); 
					Collections.sort(dtoCalculateChecksList, sortByProjectId);
				}
			}
			//dtoSearch.setTotalCount(dtoCalculateChecksList.size());
		
			dtoSearch.setRecords(dtoCalculateChecksList);
		}
		log.debug("Search TransactionEntry Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
}