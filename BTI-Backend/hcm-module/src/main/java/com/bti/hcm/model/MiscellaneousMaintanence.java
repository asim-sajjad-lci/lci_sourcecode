
package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "HR40174")
@NamedQuery(name = "MiscellaneousMaintanence.findAll", query = "SELECT d FROM MiscellaneousMaintanence d")

public class MiscellaneousMaintanence extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -729948272239040800L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MISCMINDX")
	private int id;

	@ManyToOne
	@JoinColumn(name = "MISCINDX")
	private Miscellaneous miscellaneous;

	@ManyToOne
	@JoinColumn(name = "VALUEINDX")
	private Values values;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	@JoinColumn(name = "EMPLOYINDX", nullable = false)
	private EmployeeMaster employeeMaster;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public Values getValues() {
		return values;
	}

	public void setValues(Values values) {
		this.values = values;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

}
