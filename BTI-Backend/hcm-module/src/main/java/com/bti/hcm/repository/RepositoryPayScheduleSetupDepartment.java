/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayScheduleSetupDepartment;
/**
 * Description: Interface for PayScheduleSetupDepartment 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryPayScheduleSetupDepartment")
public interface RepositoryPayScheduleSetupDepartment extends JpaRepository<PayScheduleSetupDepartment, Integer>{

	
	/**
	 * 
	 * @param payScheduleDepartmentIndexId
	 * @param deleted
	 * @return
	 */
	public PayScheduleSetupDepartment findByPayScheduleDepartmentIndexIdAndIsDeleted(int payScheduleDepartmentIndexId, boolean deleted);

	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupDepartment> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupDepartment p where p.isDeleted=false")
	public Integer getCountOfTotalPayScheduleSetupDepartment();
	
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<PayScheduleSetupDepartment> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupDepartment d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deletePayScheduleSetupDepartment(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayScheduleSetupDepartment d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSinglPayScheduleSetupDepartment(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public PayScheduleSetupDepartment findTop1ByOrderByPayScheduleDepartmentIndexIdDesc();
	
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select p from PayScheduleSetupDepartment p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.department.departmentId like :searchKeyWord or p.departmentDescription like :searchKeyWord or p.arabicDepartmentDescription like :searchKeyWord ) and p.isDeleted=false")
	public List<PayScheduleSetupDepartment> predictivePayScheduleSetupDepartmentSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<PayScheduleSetupDepartment> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PayScheduleSetupDepartment p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.department.departmentId like :searchKeyWord or p.departmentDescription like :searchKeyWord or p.arabicDepartmentDescription like :searchKeyWord ) and p.isDeleted=false")
	public Integer predictivePayScheduleSetupDepartmentSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select p from PayScheduleSetupDepartment p where (p.payScheduleSetup.payScheduleId like :searchKeyWord  or p.department.departmentId like :searchKeyWord or p.departmentDescription like :searchKeyWord or p.arabicDepartmentDescription like :searchKeyWord ) and p.isDeleted=false")
	public List<PayScheduleSetupDepartment> predictivePayScheduleSetupDepartmentSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param payScheduleDepartmentIndexId
	 * @return
	 */
	@Query("select p from PayScheduleSetupDepartment p where (p.payScheduleDepartmentIndexId =:payScheduleDepartmentIndexId) and p.isDeleted=false")
	public List<PayScheduleSetupDepartment> findByPayScheduleDepartmentIndexId(@Param("payScheduleDepartmentIndexId")Integer payScheduleDepartmentIndexId);
	
	@Query("select p from PayScheduleSetupDepartment p where (p.payScheduleSetup.payScheduleIndexId =:payScheduleIndexId) and p.isDeleted=false")
	public List<PayScheduleSetupDepartment> findByPayScheduleSetup(@Param("payScheduleIndexId")Integer payScheduleIndexId);

	@Query("select count(*) from PayScheduleSetupDepartment p ")
	public Integer getCountOfTotaPayScheduleSetupDepartment();

}
