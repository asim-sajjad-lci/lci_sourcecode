/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceDepartment;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: ControllerDepartment Name of Project: Hcm Version: 0.0.1
 */
@RestController
@RequestMapping("/department")
public class ControllerDepartment extends BaseController{
	/**
	 * @Description LOGGER use for put a logger in Department Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerDepartment.class);

	/**
	 * @Description serviceDepartment Autowired here using annotation of spring for
	 *              use of serviceDepartment method in this controller
	 */
	@Autowired(required = true)
	ServiceDepartment serviceDepartment;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @description : Create Department
	 * @param request
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 * @request { "departmentDescription":"This is test Department",
	 *          "departmentId":"DEP0012", "arabicDepartmentDescription":"هذا هو قسم
	 *          الاختبار" } @response{ "code": 201, "status": "CREATED", "result": {
	 *          "id": 1, "departmentId": "DEP0012", "departmentDescription": "This
	 *          is test Department", "arabicDepartmentDescription": "ذا هو قسم
	 *          الاختبار" }, "btiMessage": { "message": "Department create
	 *          successfully", "messageShort": "DEPARTMENT_CREATED" } }
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createDepartment(HttpServletRequest request, @RequestBody DtoDepartment dtoDepartment)
			throws Exception {
		LOGGER.info("Create Department Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDepartment = serviceDepartment.saveOrUpdateDepartment(dtoDepartment);
			responseMessage=displayMessage(dtoDepartment, "DEPARTMENT_CREATED", "DEPARTMENT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create Department Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description update Department
	 * @param request
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 * @request { "id":"1", "departmentDescription":"This is test Departmentss",
	 *          "departmentId":"DEP001", "arbicDepartmentDescription":"هذا هو قسم
	 *          الاختبار" } @response{ "code": 201, "status": "CREATED", "result": {
	 *          "id": 1, "departmentId": "DEP001", "departmentDescription": "This is
	 *          test Departmentss" }, "btiMessage": { "message": "Department updated
	 *          successfully", "messageShort": "DEPARTMENT_UPDATED_SUCCESS" } }
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateDepartment(HttpServletRequest request, @RequestBody DtoDepartment dtoDepartment)
			throws Exception {
		LOGGER.info("Update Department Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoDepartment = serviceDepartment.saveOrUpdateDepartment(dtoDepartment);
			responseMessage=displayMessage(dtoDepartment, MessageConstant.DEPARTMENT_UPDATED_SUCCESS, MessageConstant.DEPARTMENT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update Department Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description delete Department
	 * @param request
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 * @request { "ids":[1] } @response{ "code": 201, "status": "CREATED", "result":
	 *          { "deleteMessage": "Department delete successfully",
	 *          "associateMessage": "N/A", "deleteDepartment": [ { "id": 1,
	 *          "departmentDescription": "This is test Departmentss" } ], },
	 *          "btiMessage": { "message": "Department delete successfully",
	 *          "messageShort": "DEPARTMENT_DELETED" } }
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteDepartment(HttpServletRequest request, @RequestBody DtoDepartment dtoDepartment)
			throws Exception {
		LOGGER.info("Delete Department Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoDepartment.getIds() != null && !dtoDepartment.getIds().isEmpty()) {
				DtoDepartment dtoDepartment2 = serviceDepartment.deleteDepartment(dtoDepartment.getIds());
				if (dtoDepartment2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_DELETED", false), dtoDepartment2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_NOT_DELETE_ID_MESSAGE", false),
							dtoDepartment2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Department Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description search Department
	 * @param dtoSearch
	 * @param request
	 * @return @request{ "searchKeyword":"t", "pageNumber" : 0, "pageSize" :2
	 * 
	 *         } @response{ "code": 200, "status": "OK", "result": {
	 *         "searchKeyword": "t", "pageNumber": 0, "pageSize": 2, "totalCount":
	 *         8, "records": [ { "departmentId": "DEP001", "departmentDescription":
	 *         "Test Department", "arabicDepartmentDescription": "This is Arabic
	 *         Description" }, { "departmentId": "DEP001", "departmentDescription":
	 *         "Test Department", "arabicDepartmentDescription": "This is Arabic
	 *         Description" } ], }, "btiMessage": { "message": "Department list
	 *         fetched successfully", "messageShort": "DEPARTMENT_GET_ALL" } }
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchDepartment(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDepartment.searchDepartment(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DEPARTMENT_GET_ALL, MessageConstant.DEPARTMENT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	/**
	 * @description get All Department
	 * @param request
	 * @param dtoDepartment
	 * @return @request{ "pageNumber": 0, "pageSize": 10 } @response{ "code": 201,
	 *         "status": "CREATED", "result": { "pageNumber": 0, "pageSize": 10,
	 *         "totalCount": 8, "records": [ { "id": 11, "departmentId": "DEP001",
	 *         "departmentDescription": "Test Department",
	 *         "arabicDepartmentDescription": "This is Arabic Description" }, {
	 *         "id": 10, "departmentId": "DEP001", "departmentDescription": "Test
	 *         Department", "arabicDepartmentDescription": "This is Arabic
	 *         Description" }, { "id": 7, "departmentId": "DEP0012",
	 *         "departmentDescription": "DEP 2", "arabicDepartmentDescription":
	 *         "Test" },
	 * 
	 *         ], }, "btiMessage": { "message": "Department list fetched
	 *         successfully", "messageShort": "DEPARTMENT_GET_ALL" } }
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAllDepartment(HttpServletRequest request, @RequestBody DtoDepartment dtoDepartment)
			throws Exception {
		LOGGER.info("Get All Department Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceDepartment.getAllDepartment(dtoDepartment);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DEPARTMENT_GET_ALL, MessageConstant.DEPARTMENT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get All Department Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @description get Department By Id
	 * @param request
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 * @request { "id":"3" } @response{ "code": 201, "status": "CREATED", "result":
	 *          { "departmentId": "DEP001", "departmentDescription": "Test Hr
	 *          Department" }, "btiMessage": { "message": "Department details
	 *          fetched successfully", "messageShort": "DEPARTMENT_GET_DETAIL" } }
	 */
	@RequestMapping(value = "/getDepartmentById", method = RequestMethod.POST)
	public ResponseMessage getDepartmentById(HttpServletRequest request, @RequestBody DtoDepartment dtoDepartment)
			throws Exception {
		LOGGER.info("Get Department ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoDepartment dtoDepartmentObj = serviceDepartment.getDepartmentByDepartmentId(dtoDepartment.getId());
			responseMessage=displayMessage(dtoDepartmentObj, "DEPARTMENT_GET_DETAIL", "DEPARTMENT_NOT_GETTING", serviceResponse);
			
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get Department ById Method:" + dtoDepartment.getId());
		return responseMessage;
	}

	/**
	 * @description DepartmentId is exiest or not
	 * @param request
	 * @param dtoDepartment
	 * @return
	 * @throws Exception
	 * @request { "departmentId":"DEP003"
	 * 
	 *          } @response{ "code": 302, "status": "FOUND", "result": { "isRepeat":
	 *          false }, "btiMessage": { "message": "Department ID already exists",
	 *          "messageShort": "DEPARTMENT_RESULT" } }
	 */
	@RequestMapping(value = "/departmentIdcheck", method = RequestMethod.POST)
	public ResponseMessage departmetnIdCheck(HttpServletRequest request, @RequestBody DtoDepartment dtoDepartment)
			throws Exception {
		LOGGER.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoDepartment dtoDepartmentObj = serviceDepartment.repeatByDepartmentId(dtoDepartment.getDepartmentId());
			
			 if (dtoDepartmentObj !=null && dtoDepartmentObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                         serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_RESULT", false), dtoDepartmentObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                         serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_REPEAT_DEPARTMENTID_NOT_FOUND", false),
                         dtoDepartmentObj);
             }
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	
	
	@RequestMapping(value = "/searchDepartmentId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchDepartmentId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("Search Department Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDepartment.searchDepartmentId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DEPARTMENT_GET_ALL, MessageConstant.DEPARTMENT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllDepartmentDropDown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllDepartmentDropDown(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoDepartment> departmentList = serviceDepartment.getAllDepartmentDropDownList();
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_GET_ALL", false), departmentList);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return responseMessage;
	}

	@RequestMapping(value = "/getAllDepartmentId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllDepartmentId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request)
			throws Exception {
		LOGGER.info("Search Department Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceDepartment.getAllDepartmentId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.DEPARTMENT_GET_ALL, MessageConstant.DEPARTMENT_LIST_NOT_GETTING, serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

}
