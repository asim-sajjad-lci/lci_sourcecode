package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSkillSetSteup;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceSkillSetSteup;

@RestController
@RequestMapping("/skillSetSteup")
public class ControllerSkillSetSteup extends BaseController{
private static final Logger LOGGER = Logger.getLogger(ControllerSkillSteup.class);
	
	@Autowired
	ServiceSkillSetSteup serviceSkillSetSteup;
	
	@Autowired
	ServiceResponse response;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	/**
	 * @param request{  
			"skillSetId":"1",
			 "skillSetDiscription" : "test",
			 "arabicDiscription" :"test",
  			"skillSetSeqn":1,
  "skillRequired":true,
  "comment":"This is sample",
  "SkillSetDescId":1,
  "skillSetIndexId":1
  
		}
	 * @param dtoSkillSetSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoSkillSetSteup dtoSkillSetSteup) throws Exception{
		LOGGER.info("Create Skill Set Steup Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSkillSetSteup = serviceSkillSetSteup.saveOrUpdateSkillSteup(dtoSkillSetSteup);
			responseMessage=displayMessage(dtoSkillSetSteup, "SKILL_SET_STEUP_CREATED", "SKILL_SET_STEUP_NOT_CREATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create Skill Set Steup Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAll(HttpServletRequest request, @RequestBody DtoSkillSetSteup dtoSkillSteup) throws Exception {
		LOGGER.info("Get All Skill Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = serviceSkillSetSteup.getAllSkillSteup(dtoSkillSteup);
			responseMessage=displayMessage(dtoSearch, MessageConstant.SKILL_SET_STEUP_GET_ALL, MessageConstant.SKILL_SET_STEUP_LIST_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get All Skill Steup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param request{  
		  	"id" : 2,
			"skillSetId":"12",
			 "skillSetDiscription" : "test12",
			 "arabicDiscription" :"test12"
		}
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoSkillSetSteup dtoSkillSteup) throws Exception {
		LOGGER.info("Update Skill Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSkillSteup = serviceSkillSetSteup.saveOrUpdateSkillSteup(dtoSkillSteup);
			responseMessage=displayMessage(dtoSkillSteup, "SKILL_SET_STEUP_UPDATED_SUCCESS", "SKILL_SET_STEUP_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update Skill Steup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param request{  
		 	"ids":[1]
		}
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteSkillSteup(HttpServletRequest request, @RequestBody  DtoSkillSetSteup dtoSkillSteup) throws Exception {
		LOGGER.info("Delete Skill Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSkillSteup.getIds() != null && !dtoSkillSteup.getIds().isEmpty()) {
				DtoSkillSetSteup dtoSkillSetSteup = serviceSkillSetSteup.deleteSkillSteup(dtoSkillSteup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,response.getMessageByShortAndIsDeleted("SKILL_SET_STEUP_DELETED", false), dtoSkillSetSteup);

				if(dtoSkillSetSteup.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("SKILL_SET_STEUP_DELETED", false), dtoSkillSetSteup);	
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoSkillSetSteup.getMessageType());
					btiMessageHcm.setMessageShort("SKILL_SET_SETUP_NOT_DELETE_ID_MESSAGE");
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							btiMessageHcm, dtoSkillSetSteup);
					 
				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Skill Set Steup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param dtoSearch
	 * @param request{  
		 	"searchKeyword":"t",
		  	"pageNumber" : 0,
		  	"pageSize" :2
	}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search Skill Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSetSteup.searchSkillSetSteup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.SKILL_SET_STEUP_GET_ALL, MessageConstant.SKILL_SET_STEUP_LIST_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null){
			LOGGER.debug("Search Skill Steup Methods:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	
	/**
	 * @param request{  
		  	"id" : 2 
		}
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoSkillSetSteup dtoSkillSteup) throws Exception {
		LOGGER.info("Get Skill Steup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSkillSetSteup dtoSkillSteupObj = serviceSkillSetSteup.getById(dtoSkillSteup.getId());
			responseMessage=displayMessage(dtoSkillSteupObj, "SKILL_SET_STEUP_GET_DETAIL", "SKILL_SET_STEUP_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get Skill Steup ById Method:"+dtoSkillSteup.getId());
		return responseMessage;
	}
	
	
	/**
	 * @param request{  
		  	"skillSetId" : 2 
		}
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/skillSteupIdcheck", method = RequestMethod.POST)
	public ResponseMessage skillSteupIdcheck(HttpServletRequest request, @RequestBody DtoSkillSetSteup dtoSkillSteup) throws Exception {
		LOGGER.info("skillSteupIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSkillSetSteup dtoSkillSteupObj = serviceSkillSetSteup.repeatBySkillSetSteupId(dtoSkillSteup.getSkillSetId());
			if (dtoSkillSteupObj != null) {
				if (dtoSkillSteupObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("SKILL_SET_STEUP_RESULT", false), dtoSkillSteupObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							response.getMessageByShortAndIsDeleted(dtoSkillSteupObj.getMessageType(), false),
							dtoSkillSteupObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("SKILL_SET_STEUP_REPEAT_SKILL_SET_STEUP_ID_NOT_FOUND", false), dtoSkillSteupObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchSkillSetId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchSkillSetId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search searchSkillSetIds Methods");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSetSteup.searchSkillSetId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.SKILL_SET_STEUP_GET_ALL, MessageConstant.SKILL_SET_STEUP_LIST_NOT_GETTING, response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Skills Steups Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search searchSkillSetId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSetSteup.getIds(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SKILL_SET_STEUP_GET_ALL", "SKILL_SET_STEUP_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Skilsl Steups Methods:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getById(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search searchSkillSetId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSetSteup.findById(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SKILL_SET_STEUP_GET_ALL", "SKILL_SET_STEUP_LIST_NOT_GETTING", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Skill Steup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/searchSkillSetSetupId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchSkillSetSetupId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("searchSkillSetSetupId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSkillSetSteup.searchSkillSetSetupId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SKILL_SET_SETUP_GET_ALL", "SKILL_SET_SETUP_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search Skill Steup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
}
