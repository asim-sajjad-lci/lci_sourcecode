package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.FinancialDimensionsValues;

@Repository("/repositoryFinancialDimensionsValues")
public interface RepositoryFinancialDimensionsValues extends JpaRepository<FinancialDimensionsValues, Integer>{

	public FinancialDimensionsValues findByIdAndIsDeleted(int id, boolean deleted);
	
	@Query("select b from FinancialDimensionsValues b where b.dimensionValue =:id and b.isDeleted=false")
	List<FinancialDimensionsValues> findBydimensionValue(@Param("id") String dimensionValue);
}
