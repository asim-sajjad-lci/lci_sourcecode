package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.HelthInsurance;


@Repository("repositoryHelthInsurances")
public interface RepositoryHelthInsurance extends JpaRepository<HelthInsurance, Integer>{

	
	HelthInsurance findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update HelthInsurance d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleHelthInsurance(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from HelthInsurance d where ( d.helthInsuranceId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord or d.compnayInsurance.insCompanyId like :searchKeyWord or d.groupNumber like :searchKeyWord or d.helthCoverageType.helthCoverageId like :searchKeyWord) and d.isDeleted=false")
	Integer predictiveHelthInsuranceSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from HelthInsurance d where ( d.helthInsuranceId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord or d.compnayInsurance.insCompanyId like :searchKeyWord or d.groupNumber like :searchKeyWord or d.helthCoverageType.helthCoverageId like :searchKeyWord) and d.isDeleted=false")
	List<HelthInsurance> predictiveHelthInsuranceSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select d from HelthInsurance d where (d.helthInsuranceId=:searchKeyWord) and d.isDeleted=false")
	List<HelthInsurance> findByHelthInsuranceId(@Param("searchKeyWord") String id);

	@Query("select d.helthInsuranceId from HelthInsurance d where (d.helthInsuranceId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<String> predictiveHelthInsuranceIdSearchWithPagination(@Param("searchKeyWord") String string);

	List<HelthInsurance> findByIsDeleted(Boolean deleted);

	@Query("select count(*) from HelthInsurance h ")
	public Integer getCountOfTotalHelthInsurance();
}
