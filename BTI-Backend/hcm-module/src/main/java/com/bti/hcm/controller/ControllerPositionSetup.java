package com.bti.hcm.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.PositionSetup;
import com.bti.hcm.model.dto.DtoPositionSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePositionSteup;
import com.bti.hcm.service.ServiceResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/positionAttachmentSetup")
public class ControllerPositionSetup extends BaseController{
	@Autowired
	ServicePositionSteup servicePositionSetup;
	
	@Autowired
	ServiceResponse response;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerPositionSetup.class);
	
	/**
	 * @param request{
  		"ducumentAttachmentSecq":1,
 	 	"position":{
       						"id":1
     					},
	  	"documentAttachmentName": "test1",
	  	"documentAttachmenDesc": "test2",
	  	"pageNumber": "0",
	  	"pageSize": "10"
	}
	 * @param response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": null,
			"ducumentAttachmentSecq": 1,
			"documentAttachmentName": "test1",
			"position": {
			"createdDate": null,
			"updatedDate": 1517479411941,
			"updatedBy": 0,
			"isDeleted": false,
			"updatedRow": null,
			"rowId": 0,
			"id": 1,
			"skillSetSetup": null,
			"positionId": null,
			"description": null,
			"arabicDescription": null,
			"positionClass": null,
			"reportToPostion": null,
			"postionLongDesc": null
			},
			"documentAttachmenDesc": "test2",
			"attachmentType": null,
			"attachmentDate": null,
			"attachmentTime": null,
			"pageNumber": 0,
			"pageSize": 10,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePositionSetup": null,
			"isRepeat": null
			},
			"btiMessage": {
			"message":Position Setup created successfully",
			"messageShort": "POSITIONSETUP_CREATED"
			}
			}
	 * @param dtoPositionSetup
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/createPosition", method = RequestMethod.POST,consumes = {"multipart/form-data"})
	public ResponseMessage createPosition(HttpServletRequest request,@RequestParam ("DtoPositionSetup")String   dtoPositionSetup, @RequestParam(name="file",required = false) MultipartFile file) throws Exception{
		LOGGER.info("Create Postion Info");
		
		ObjectMapper objectMapper = new ObjectMapper();
		DtoPositionSetup dtoPositionSetups = objectMapper.readValue(dtoPositionSetup, DtoPositionSetup.class);
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionSetups = servicePositionSetup.saveOrUpdatePositionSetup(dtoPositionSetups,file);
			responseMessage=displayMessage(dtoPositionSetups, "POSITION_ATTACHMENT_CREATED", "POSITION_ATTACHMENT_NOT_CREATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create Postion Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}

	
	/**
	 * @param request{
  		"ids" : [1]
	}
	 @param response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"totalCount": 1,
			"records": [
			  {
			"id": 1,
			"ducumentAttachmentSecq": null,
			"documentAttachmentName": "test1",
			"position": null,
			"documentAttachmenDesc": "test2",
			"attachmentType": null,
			"attachmentDate": null,
			"attachmentTime": null,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePositionSetup": null,
			"isRepeat": null
			}
			],
			},
			"btiMessage": {
			"message": "Skill Steup list fetched successfully",
			"messageShort": "POSITION_ATTACHMENT_GET_ALL"
			}
			}
	 * @param dtoPositionSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllAttachmentByPositionId", method = RequestMethod.PUT)
	public ResponseMessage getAllPostionSetup(HttpServletRequest request, @RequestBody DtoSearch dtoSearch1) throws Exception {
		LOGGER.info("Get All Postion Steup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePositionSetup.getAllPostionSteup(dtoSearch1);
			responseMessage=displayMessage(dtoSearch, "POSITION_ATTACHMENT_GET_ALL", "POSITION_ATTACHMENT_LIST_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get All Postion Steup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  	"id" : 1,
	  	"ducumentAttachmentSecq":1,
	 	 	"position":{
	       						"id":1
	     					},
		  	"documentAttachmentName": "test1",
		  	"documentAttachmenDesc": "testssssssssssssssss",
		  	"pageNumber": "0",
		  	"pageSize": "10"
		}
		 @param response
		{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": 1,
			"ducumentAttachmentSecq": 1,
			"documentAttachmentName": "test1",
			"position": {
			"createdDate": null,
			"updatedDate": 1517479757423,
			"updatedBy": 0,
			"isDeleted": false,
			"updatedRow": null,
			"rowId": 0,
			"id": 1,
			"skillSetSetup": null,
			"positionId": null,
			"description": null,
			"arabicDescription": null,
			"positionClass": null,
			"reportToPostion": null,
			"postionLongDesc": null
			},
			"documentAttachmenDesc": "testssssssssssssssss",
			"attachmentType": null,
			"attachmentDate": null,
			"attachmentTime": null,
			"pageNumber": 0,
			"pageSize": 10,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePositionSetup": null,
			"isRepeat": null
			},
			"btiMessage": {
			"message": "PositionSetup updated successfully",
			"messageShort": "POSITIONSETUP_UPDATED_SUCCESS"
			}
			}
	 * @param dtoPositionSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST,consumes = {"multipart/form-data"})
	public ResponseMessage updatePositionSetup(HttpServletRequest request,@RequestParam ("DtoPositionSetup")String dtoPositionSetup, @RequestParam("file") MultipartFile file) throws Exception {
		LOGGER.info("Update Position Method");
		ResponseMessage responseMessage = null;
		ObjectMapper objectMapper = new ObjectMapper();
		DtoPositionSetup dtoPositionSetups = objectMapper.readValue(dtoPositionSetup, DtoPositionSetup.class);
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionSetups = servicePositionSetup.saveOrUpdatePositionSetup(dtoPositionSetups,file);
			responseMessage=displayMessage(dtoPositionSetups, "POSITION_ATTACHMENT_UPDATED_SUCCESS", "POSITION_ATTACHMENT_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update Position Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
		  	"ids":[1]
		  	
			}
			@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"ducumentAttachmentSecq": null,
		"documentAttachmentName": null,
		"position": null,
		"documentAttachmenDesc": null,
		"attachmentType": null,
		"attachmentDate": null,
		"attachmentTime": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": "Skill Steup deleted successfully",
		"associateMessage": "N/A",
		"deletePositionSetup": [
		  {
		"id": 1,
		"ducumentAttachmentSecq": null,
		"documentAttachmentName": "test1",
		"position": null,
		"documentAttachmenDesc": "testssssssssssssssss",
		"attachmentType": null,
		"attachmentDate": null,
		"attachmentTime": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePositionSetup": null,
		"isRepeat": null
		}
		],
		"isRepeat": null
		},
		"btiMessage": {
		"message": "Skill Steup deleted successfully",
		"messageShort": "POSITION_ATTACHMENT_DELETED"
		}
		}
	 * @param dtoPositionSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePositionSetup(HttpServletRequest request, @RequestBody DtoPositionSetup dtoPositionSetup) throws Exception {
		LOGGER.info("Delete Position Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPositionSetup.getIds() != null && !dtoPositionSetup.getIds().isEmpty()) {
				DtoPositionSetup dtoPosition1 = servicePositionSetup.deletePositionSteup(dtoPositionSetup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						response.getMessageByShortAndIsDeleted("POSITION_PLAN_DELETED", false), dtoPosition1);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Position Setup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  	"id" : 1
  	
	}
	@param response
			{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": 1,
		"ducumentAttachmentSecq": null,
		"documentAttachmentName": "test1",
		"position": null,
		"documentAttachmenDesc": "testssssssssssssssss",
		"attachmentType": null,
		"attachmentDate": null,
		"attachmentTime": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePositionSetup": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "PositionSetup details fetched successfully",
		"messageShort": "POSITIONSETUP_GET_DETAIL"
		}
		}
	 * @param dtoPositionSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAttachmentById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoPositionSetup dtoPositionSetup) throws Exception {
		LOGGER.info("Get Position Steup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPositionSetup dtoPositionObj = servicePositionSetup.getByPositionSetupId(dtoPositionSetup.getId());
			responseMessage=displayMessage(dtoPositionObj, "POSITION_ATTACHMENT_GET_DETAIL", "POSITION_ATTACHMENT_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get Position by Id Method:"+dtoPositionSetup.getId());
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchPositionSetup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPositionPlan(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search searchPositionSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionSetup.searchPositionSetup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "POSITION_ATTACHMENT_GET_ALL", "POSITION_ATTACHMENT_LIST_NOT_GETTING", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search searchPositionSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
			
	@RequestMapping(value =  {"/attachment/{id:.+}" }, method = RequestMethod.GET)
	public HttpEntity<byte[]> profileId(@PathVariable int id,HttpServletResponse response)

			throws  IOException {
		
		PositionSetup position = servicePositionSetup.getAttachment(id);

		ByteArrayOutputStream bao = null;
		HttpHeaders headers = new HttpHeaders();
		 headers.setContentDispositionFormData("attachment; filename=", position.getDocumentAttachmentName());
		 headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);	 	    
		if(position.getAttachment()!=null){
			byte[] bytes = position.getAttachment();

			bao = new ByteArrayOutputStream();
			bao.write(bytes);
			
		    return new HttpEntity<byte[]>(bao.toByteArray(), headers);
		}	
		

		 return new HttpEntity<byte[]>(null, headers);
	}
	
}
