package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.SalaryMatrixRow;
import com.bti.hcm.model.SalaryMatrixSetup;
import com.bti.hcm.model.dto.DtoSalaryMatrixRow;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositorySalaryMatrixRow;
import com.bti.hcm.repository.RepositorySalaryMatrixSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceSalaryMatrixRow")
public class ServiceSalaryMatrixRow {

	/**
	 * @Description LOGGER use for put a logger in ServiceSalaryMatrixRow Service
	 */
	static Logger log = Logger.getLogger(ServiceSalaryMatrixRow.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in ServiceSalaryMatrixRow service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceSalaryMatrixRow service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in ServiceSalaryMatrixRow service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositorySalaryMatrixRow Autowired here using annotation of spring for access of repositorySalaryMatrixRow method in SalaryMatrixRow service
	 * 				In short Access SalaryMatrixRow Query from Database using repositorySalaryMatrixRow.
	 */
	@Autowired
	RepositorySalaryMatrixRow repositorySalaryMatrixRow;
	
	/**
	 * 
	 */
	@Autowired
	RepositorySalaryMatrixSetup repositorySalaryMatrixSetup;

	
	/**
	 * @param dtoSalaryMatrixRow
	 * @return
	 * @throws ParseException
	 */
	public DtoSalaryMatrixRow saveOrUpdate(DtoSalaryMatrixRow dtoSalaryMatrixRow)  {
		log.info("saveOrUpdate SalaryMatrixRow Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		SalaryMatrixRow salaryMatrixRow = null;
		if (dtoSalaryMatrixRow.getId() != null && dtoSalaryMatrixRow.getId() > 0) {
			salaryMatrixRow = repositorySalaryMatrixRow.findByIdAndIsDeleted(dtoSalaryMatrixRow.getId(), false);
			salaryMatrixRow.setUpdatedBy(loggedInUserId);
			salaryMatrixRow.setUpdatedDate(new Date());
		} else {
			salaryMatrixRow = new SalaryMatrixRow();
			salaryMatrixRow.setCreatedDate(new Date());
			Integer rowId = repositorySalaryMatrixRow.getCountOfTotaSalaryMatrixRow();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			salaryMatrixRow.setRowId(increment);
		}
		
		SalaryMatrixSetup salaryMatrixSetup = repositorySalaryMatrixSetup.findOne(Integer.parseInt(dtoSalaryMatrixRow.getSalaryMatrixSetupId()));
		salaryMatrixRow.setDesc(dtoSalaryMatrixRow.getDesc());
		salaryMatrixRow.setArbic(dtoSalaryMatrixRow.getArbic());
		salaryMatrixRow.setSalaryMatrixSetup(salaryMatrixSetup);
		salaryMatrixRow.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositorySalaryMatrixRow.saveAndFlush(salaryMatrixRow);
		log.debug("SalaryMatrixRow is:"+dtoSalaryMatrixRow.getId());
		return dtoSalaryMatrixRow;

	}

	
	/**
	 * @param dtoSalaryMatrixRow
	 * @return
	 */
	public DtoSearch getAll(DtoSalaryMatrixRow dtoSalaryMatrixRow) {
		log.info("getAllDeductionCode Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSalaryMatrixRow.getPageNumber());
		dtoSearch.setPageSize(dtoSalaryMatrixRow.getPageSize());
		dtoSearch.setTotalCount(repositorySalaryMatrixRow.getCountOfTotalSalaryMatrixRow());
		List<SalaryMatrixRow> salaryMatrixRowList = null;
		if (dtoSalaryMatrixRow.getPageNumber() != null && dtoSalaryMatrixRow.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSalaryMatrixRow.getPageNumber(), dtoSalaryMatrixRow.getPageSize(), Direction.DESC, "createdDate");
			salaryMatrixRowList = repositorySalaryMatrixRow.findByIsDeleted(false, pageable);
		} else {
			salaryMatrixRowList = repositorySalaryMatrixRow.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		
		List<DtoSalaryMatrixRow> salaryMatrixSetupList=new ArrayList<>();
		if(salaryMatrixRowList!=null && !salaryMatrixRowList.isEmpty())
		{
			for (SalaryMatrixRow salaryMatrixRow : salaryMatrixRowList) 
			{
				dtoSalaryMatrixRow=new DtoSalaryMatrixRow(salaryMatrixRow);
				dtoSalaryMatrixRow.setId(salaryMatrixRow.getId());
				
				salaryMatrixSetupList.add(dtoSalaryMatrixRow);
			}
			dtoSearch.setRecords(salaryMatrixSetupList);
		}
		log.debug("All DtoSalaryMatrixSetup List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search SalaryMatrixSetupRow Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			dtoSearch.setTotalCount(this.repositorySalaryMatrixRow.predictiveSalaryMatrixSetupSearchTotalCount("%"+searchWord+"%"));
			List<SalaryMatrixRow> salaryMatrixSetupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				salaryMatrixSetupList = this.repositorySalaryMatrixRow.predictiveSalaryMatrixSetupSearchWithPagination("%"+searchWord+"%");
			}
		
			if(salaryMatrixSetupList != null && !salaryMatrixSetupList.isEmpty()){
				List<DtoSalaryMatrixRow> dtoSalaryMatrixRowList = new ArrayList<>();
				DtoSalaryMatrixRow dtoSalaryMatrixSetup=null;
				for (SalaryMatrixRow salaryMatrixRow : salaryMatrixSetupList) {
					dtoSalaryMatrixSetup = new DtoSalaryMatrixRow(salaryMatrixRow);
					dtoSalaryMatrixSetup.setDesc(salaryMatrixRow.getDesc());
					dtoSalaryMatrixSetup.setArbic(salaryMatrixRow.getArbic());
					dtoSalaryMatrixSetup.setSalaryMatrixSetupId(salaryMatrixRow.getSalaryMatrixSetup().getSalaryMatrixId());
					dtoSalaryMatrixRowList.add(dtoSalaryMatrixSetup);
				}
				dtoSearch.setRecords(dtoSalaryMatrixRowList);
			}
		}
		if(dtoSearch!=null){
			log.debug("Search SalaryMatrixRow Size is:"+dtoSearch.getTotalCount());
		}
		
		return dtoSearch;
	}

	
	/**
	 * @param id
	 * @return
	 */
	public DtoSalaryMatrixRow getById(int id) {
		log.info("getById Method");
		DtoSalaryMatrixRow dtoSalaryMatrixSetup = new DtoSalaryMatrixRow();
		if (id > 0) {
			SalaryMatrixRow salaryMatrixSetup = repositorySalaryMatrixRow.findByIdAndIsDeleted(id, false);
			if (salaryMatrixSetup != null) {
				dtoSalaryMatrixSetup = new DtoSalaryMatrixRow(salaryMatrixSetup);
			} else {
				dtoSalaryMatrixSetup.setMessageType("SALARY_MATRIX_LIST_NOT_GETTING");

			}
		} else {
			dtoSalaryMatrixSetup.setMessageType("SALARY_MATRIX_LIST_NOT_GETTING");

		}
		log.debug("SalaryMatrixSetupRow By Id is:"+dtoSalaryMatrixSetup.getId());
		return dtoSalaryMatrixSetup;
	}


	
	
	public DtoSalaryMatrixRow delete(List<Integer> ids) {
		log.info("delete DtoSalaryMatrixRow Method");
		DtoSalaryMatrixRow dtoSalaryMatrixSetup = new DtoSalaryMatrixRow();
		dtoSalaryMatrixSetup
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SALARY_MATRIX_DELETED", false));
		dtoSalaryMatrixSetup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SALARY_MATRIX_DELETED", false));
		List<DtoSalaryMatrixRow> salaryMatrixRow = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer Id : ids) {
				SalaryMatrixRow salaryMatrixSetup = repositorySalaryMatrixRow.findOne(Id);
				if(salaryMatrixSetup.getSalaryMatrixRowValue().isEmpty()) {
					DtoSalaryMatrixRow dtoSalaryMatrixRow = new DtoSalaryMatrixRow();
					dtoSalaryMatrixRow.setId(salaryMatrixSetup.getId());
					repositorySalaryMatrixRow.deleteSingleSalaryMatrixSetupRow(true, loggedInUserId, Id);
					salaryMatrixRow.add(dtoSalaryMatrixRow);
				}
			}
			dtoSalaryMatrixSetup.setDelete(salaryMatrixRow);
		} catch (NumberFormatException e) {
			log.error("error in delete matrix row: ", e);
		}
		log.debug("Delete SalaryMatrixSetup :"+dtoSalaryMatrixSetup.getId());
		return dtoSalaryMatrixSetup;
	}
	
}
