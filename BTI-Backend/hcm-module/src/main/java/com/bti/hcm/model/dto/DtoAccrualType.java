package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.AccrualType;

public class DtoAccrualType extends DtoBase{
	private Integer id;
	private String desc;
	private short type;
	private List<DtoAccrualType> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public short getType() {
		return type;
	}
	public void setType(short type) {
		this.type = type;
	}
	public List<DtoAccrualType> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoAccrualType> delete) {
		this.delete = delete;
	}
	
	public DtoAccrualType() {
	}
	
	public DtoAccrualType(AccrualType accrual) {
	}
}
