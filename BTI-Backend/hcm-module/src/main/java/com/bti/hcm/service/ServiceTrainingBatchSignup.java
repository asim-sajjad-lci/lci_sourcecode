package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.TrainingBatchSignup;
import com.bti.hcm.model.TrainingCourseDetail;
import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchTranningById;
import com.bti.hcm.model.dto.DtoTrainingBatchSignup;
import com.bti.hcm.model.dto.DtoTrainingCourseDetail;
import com.bti.hcm.model.dto.DtoTraningCourse;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryTrainingBatchSignup;
import com.bti.hcm.repository.RepositoryTrainingCourseDetail;
import com.bti.hcm.repository.RepositoryTraningCourse;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceTrainingBatchSignup")
public class ServiceTrainingBatchSignup {
	
	
	static Logger log = Logger.getLogger(ServiceTrainingBatchSignup.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryTraningCourse repositoryTraningCourse;
	
	@Autowired
	RepositoryTrainingBatchSignup repositoryTrainingBatchSignup;
	
	@Autowired
	RepositoryTrainingCourseDetail repositoryTrainingCourseDetail;
	
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	public DtoTrainingBatchSignup saveOrUpdateTrainingBatchSignup(DtoTrainingBatchSignup dtoTrainingBatchSignup) {
		
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		TrainingBatchSignup trainingBatchSignup=null;
		if (dtoTrainingBatchSignup.getId() != null && dtoTrainingBatchSignup.getId() > 0) {
			trainingBatchSignup = repositoryTrainingBatchSignup.findByIdAndIsDeleted(dtoTrainingBatchSignup.getId(), false);
			trainingBatchSignup.setUpdatedBy(loggedInUserId);
			trainingBatchSignup.setUpdatedDate(new Date());
		} else {
			trainingBatchSignup = new TrainingBatchSignup();
			trainingBatchSignup.setCreatedDate(new Date());
			
			Integer rowId = repositoryTrainingBatchSignup.getCountOfTotalTrainingBatchSignup();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			trainingBatchSignup.setRowId(increment);
		}
		
		List<EmployeeMaster> list = new ArrayList<>();
		
		for (DtoEmployeeMaster iterable_element : dtoTrainingBatchSignup.getEmployeeMaster()) {
			EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOne(iterable_element.getEmployeeIndexId());
			list.add(employeeMaster);
		}
         
		List<TrainingCourseDetail> trainingCourseDetailList = new ArrayList<>();
		
		 for (DtoTrainingCourseDetail dtoTrainingCourseDetail : dtoTrainingBatchSignup.getDtoTrainingCourseDetail()) {
			 TrainingCourseDetail trainingCourseDetail = repositoryTrainingCourseDetail.findOne(dtoTrainingCourseDetail.getId());
			 trainingCourseDetailList.add(trainingCourseDetail);
			 trainingBatchSignup.setTrainingCourseDetails(trainingCourseDetailList);
		}
		 
		TraningCourse traningCourse = repositoryTraningCourse.findOne(dtoTrainingBatchSignup.getTraningCourse().getId());
					
		trainingBatchSignup.setCompleteDate(dtoTrainingBatchSignup.getCompleteDate());
		 trainingBatchSignup.setEmployeeMaster(list);
		 trainingBatchSignup.setTrainingCourse(traningCourse);
		 trainingBatchSignup.setId(dtoTrainingBatchSignup.getId());
		 if(!list.isEmpty()) {
			 trainingBatchSignup.setSelectedEmployes(list.get(0).getEmployeeFirstName());
		 }
		trainingBatchSignup.setCompleteDate(dtoTrainingBatchSignup.getCompleteDate());
		trainingBatchSignup.setCourseComments(dtoTrainingBatchSignup.getCourseComments());
		trainingBatchSignup.setTrainingClassStatus(dtoTrainingBatchSignup.getTrainingClassStatus());
		
		trainingBatchSignup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryTrainingBatchSignup.saveAndFlush(trainingBatchSignup);
		return dtoTrainingBatchSignup;
	}
	
	
public DtoTrainingBatchSignup changeStatus(DtoTrainingBatchSignup dtoTrainingBatchSignup) {
		
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		TrainingBatchSignup trainingBatchSignup=null;
		if (dtoTrainingBatchSignup.getId() != null && dtoTrainingBatchSignup.getId() > 0) {
			trainingBatchSignup = repositoryTrainingBatchSignup.findByIdAndIsDeleted(dtoTrainingBatchSignup.getId(), false);
			trainingBatchSignup.setUpdatedBy(loggedInUserId);
			trainingBatchSignup.setUpdatedDate(new Date());
			trainingBatchSignup.setTrainingClassStatus(dtoTrainingBatchSignup.getTrainingClassStatus());
			
			trainingBatchSignup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			trainingBatchSignup.setRowId(loggedInUserId);
			repositoryTrainingBatchSignup.saveAndFlush(trainingBatchSignup);
		}else {
			dtoTrainingBatchSignup = null;
			return dtoTrainingBatchSignup;
		}
		
		return dtoTrainingBatchSignup;
	}
	
	
	public DtoTrainingBatchSignup deleteTrainingBatchSignup(List<Integer> ids) {
		log.info("delete TrainingBatchSignup Method");
		DtoTrainingBatchSignup dtoTrainingBatchSignup = new DtoTrainingBatchSignup();
		dtoTrainingBatchSignup.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRANING_BATCH_SIGHNUP_DELETED", false));
		dtoTrainingBatchSignup.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TRANING_BATCH_SIGHNUP_ASSOCIATED", false));
		List<DtoTrainingBatchSignup> deleteDtoTrainingBatchSignup = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TRANING_BATCH_SIGHNUP__NOT_DELETE_ID_MESSAGE", false).getMessage());
		
		try {
			for (Integer id : ids) {
				TrainingBatchSignup trainingBatchSignup = repositoryTrainingBatchSignup.findOne(id);
				if(trainingBatchSignup!=null) {
					DtoTrainingBatchSignup dtoTrainingBatchSignup2 = new DtoTrainingBatchSignup();
					
					dtoTrainingBatchSignup2.setId(trainingBatchSignup.getId());
					dtoTrainingBatchSignup2.setSelectedEmployes(trainingBatchSignup.getSelectedEmployes());
					
							
					
					repositoryTrainingBatchSignup.deleteSingleTrainingBatchSignup(true, loggedInUserId,id);
					deleteDtoTrainingBatchSignup.add(dtoTrainingBatchSignup2);
				}else {
					inValidDelete = true;
				}
				

			}
			dtoTrainingBatchSignup.setDelete(deleteDtoTrainingBatchSignup);
			
			
			
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoTrainingBatchSignup.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoTrainingBatchSignup.setMessageType("");
				
			}
			
			dtoTrainingBatchSignup.setDelete(deleteDtoTrainingBatchSignup);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete TrainingBatchSignup :"+dtoTrainingBatchSignup.getId());
		return dtoTrainingBatchSignup;
	}
	

	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchTrainingBatchSignup(DtoSearch dtoSearch) {
		log.info("search TrainingBatchSignup Method");
		if(dtoSearch != null){
			String searchWordTraningBatchSingup=dtoSearch.getSearchKeyword();
			String conditionForTraningBatchSignup="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("selectedEmployes") || dtoSearch.getSortOn().equals("courseComments")) {
						conditionForTraningBatchSignup=dtoSearch.getSortOn();
					}else {
						conditionForTraningBatchSignup="id";
					}
					
				}else{
					conditionForTraningBatchSignup+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	  
	
			
			dtoSearch.setTotalCount(this.repositoryTrainingBatchSignup.predictiveTrainingBatchSignupSearchTotalCount("%"+searchWordTraningBatchSingup+"%"));
			List<TrainingBatchSignup> trainingBatchSignupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					trainingBatchSignupList = this.repositoryTrainingBatchSignup.predictiveTrainingBatchSignupSearchWithPagination("%"+searchWordTraningBatchSingup+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					trainingBatchSignupList = this.repositoryTrainingBatchSignup.predictiveTrainingBatchSignupSearchWithPagination("%"+searchWordTraningBatchSingup+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, conditionForTraningBatchSignup));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					trainingBatchSignupList = this.repositoryTrainingBatchSignup.predictiveTrainingBatchSignupSearchWithPagination("%"+searchWordTraningBatchSingup+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, conditionForTraningBatchSignup));
				}
				
			}
			if(trainingBatchSignupList != null && !trainingBatchSignupList.isEmpty()){
				List<DtoTrainingBatchSignup> dtoTrainingBatchSignupList = new ArrayList<>();
				DtoTrainingBatchSignup dtoTrainingBatchSignup=null;
				for (TrainingBatchSignup trainingBatchSignup : trainingBatchSignupList) {
					dtoTrainingBatchSignup = new DtoTrainingBatchSignup(trainingBatchSignup);
					
					
					dtoTrainingBatchSignup.setId(trainingBatchSignup.getId());
					dtoTrainingBatchSignup.setSelectedEmployes(trainingBatchSignup.getSelectedEmployes());
					dtoTrainingBatchSignup.setCompleteDate(trainingBatchSignup.getCompleteDate());
					dtoTrainingBatchSignup.setCourseComments(trainingBatchSignup.getCourseComments());
					dtoTrainingBatchSignup.setTrainingClassStatus(trainingBatchSignup.getTrainingClassStatus());
					
					if(dtoTrainingBatchSignup.getEmployeeMaster()==null) {
						List<DtoEmployeeMaster> dtoEmployeeMaster = new ArrayList<>();
						dtoTrainingBatchSignup.setEmployeeMaster(dtoEmployeeMaster);
						
						List<EmployeeMaster> employeeMastersList = new ArrayList<>();
						
						
						List<Integer> employeeMasters = 	repositoryTrainingBatchSignup.getEmployeeByTrainingId(trainingBatchSignup.getId());
						
						if(employeeMasters!=null && !employeeMasters.isEmpty()) {
							employeeMastersList = repositoryTrainingBatchSignup.findEmployee(employeeMasters);
						}
						
						
						for (EmployeeMaster employeeMaster : employeeMastersList) {
							DtoEmployeeMaster dtoEmployeeMaster2 = new DtoEmployeeMaster();
							dtoEmployeeMaster2.setEmployeeId(employeeMaster.getEmployeeId());
							dtoEmployeeMaster2.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
							dtoEmployeeMaster2.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
							dtoEmployeeMaster2.setEmployeeLastName(employeeMaster.getEmployeeLastName());
							dtoEmployeeMaster2.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
							dtoEmployeeMaster2.setEmployeeMiddleName(employeeMaster.getEmployeeMiddleName());
							dtoEmployeeMaster2.setEmployeeMiddleNameArabic(employeeMaster.getEmployeeMiddleNameArabic());
							if(employeeMaster.getDepartment()!=null) {
								dtoEmployeeMaster2.setDepartment_Id(employeeMaster.getDepartment().getDepartmentId());	
							}
							if(employeeMaster.getPosition()!=null) {
								dtoEmployeeMaster2.setPosition_Id(employeeMaster.getPosition().getPositionId());
							}
							if(employeeMaster.getDivision()!=null) {
								dtoEmployeeMaster2.setDivision_Id(employeeMaster.getDivision().getDivisionId());
							}
							dtoEmployeeMaster2.setIsActive(false);
							dtoEmployeeMaster.add(dtoEmployeeMaster2);
						}
						dtoTrainingBatchSignup.setEmployeeMaster(dtoEmployeeMaster);
					}
					
					if(dtoTrainingBatchSignup.getTrainingClass()==null) {
						DtoTrainingCourseDetail dtoTraningClass = new DtoTrainingCourseDetail();
						dtoTrainingBatchSignup.setTrainingClass(dtoTraningClass);
						
						
						List<DtoTrainingCourseDetail> listDtoTrainingCourseDetail = new ArrayList<>();
						
						for (TrainingCourseDetail trainingCourseDetail : trainingBatchSignup.getTrainingCourseDetails()) {
							DtoTrainingCourseDetail dtoTrainingCourseDetail = 	getDtoTrainingCourseDetail(trainingCourseDetail);
							listDtoTrainingCourseDetail.add(dtoTrainingCourseDetail);
						}
						
						dtoTrainingBatchSignup.setTrainingClassList(listDtoTrainingCourseDetail);
						
					}
					
					if(dtoTrainingBatchSignup.getTraningCourse()==null) {
						DtoTraningCourse dtoTraningCourseDetail = new DtoTraningCourse();
						dtoTrainingBatchSignup.setTraningCourse(dtoTraningCourseDetail);
						if(trainingBatchSignup.getTrainingCourse()!=null) {
							dtoTrainingBatchSignup.getTraningCourse().setId(trainingBatchSignup.getTrainingCourse().getId());
							dtoTrainingBatchSignup.getTraningCourse().setTraningId(trainingBatchSignup.getTrainingCourse().getTraningId());
							dtoTrainingBatchSignup.getTraningCourse().setDesc(trainingBatchSignup.getTrainingCourse().getDesc());
							
						}
						
					}
					
					
					dtoTrainingBatchSignupList.add(dtoTrainingBatchSignup);
					}
					
				
				dtoSearch.setRecords(dtoTrainingBatchSignupList);
				}
				
			}
		
		log.debug("Search TrainingBatchSignup Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
		}

	
	public DtoTrainingBatchSignup getTraningBatchSighUpById(int id) {
		DtoTrainingBatchSignup dtoTrainingBatchSignup  = new DtoTrainingBatchSignup();
		if (id > 0) {
			TrainingBatchSignup trainingBatchSignup = repositoryTrainingBatchSignup.findByIdAndIsDeleted(id, false);
			if (trainingBatchSignup != null) {
				dtoTrainingBatchSignup = new DtoTrainingBatchSignup(trainingBatchSignup);
				
				
				if(dtoTrainingBatchSignup.getEmployeeMaster()==null) {
					List<DtoEmployeeMaster> dtoEmployeeMaster = new ArrayList<>();
					dtoTrainingBatchSignup.setEmployeeMaster(dtoEmployeeMaster);
					
					for (EmployeeMaster employeeMaster : trainingBatchSignup.getEmployeeMaster()) {
						DtoEmployeeMaster dtoEmployeeMast = new DtoEmployeeMaster();
						
						dtoEmployeeMast.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
						dtoEmployeeMast.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
						dtoEmployeeMast.setEmployeeLastName(employeeMaster.getEmployeeLastName());
						dtoEmployeeMast.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
						dtoEmployeeMast.setEmployeeLastNameArabic(employeeMaster.getEmployeeLastNameArabic());
						dtoEmployeeMaster.add(dtoEmployeeMast);
					}
					dtoTrainingBatchSignup.setEmployeeMaster(dtoEmployeeMaster);
				}
				
				
				if(dtoTrainingBatchSignup.getTrainingClass()==null) {
					DtoTrainingCourseDetail dtoTraningClass = new DtoTrainingCourseDetail();
					dtoTrainingBatchSignup.setTrainingClass(dtoTraningClass);
					
				}
				
				if(dtoTrainingBatchSignup.getTraningCourse()==null) {
					DtoTraningCourse dtoTraningCourseDetail = new DtoTraningCourse();
					dtoTrainingBatchSignup.setTraningCourse(dtoTraningCourseDetail);
					if(trainingBatchSignup.getTrainingCourse()!=null) {
						dtoTrainingBatchSignup.getTraningCourse().setId(trainingBatchSignup.getTrainingCourse().getId());
						dtoTrainingBatchSignup.getTraningCourse().setTraningId(trainingBatchSignup.getTrainingCourse().getTraningId());
						
						
						
						dtoTrainingBatchSignup.getTraningCourse().setDesc(trainingBatchSignup.getTrainingCourse().getDesc());
						dtoTrainingBatchSignup.getTraningCourse().setArbicDesc(trainingBatchSignup.getTrainingCourse().getArbicDesc());
						dtoTrainingBatchSignup.getTraningCourse().setLocation(trainingBatchSignup.getTrainingCourse().getLocation());
						dtoTrainingBatchSignup.getTraningCourse().setInstructorCost(trainingBatchSignup.getTrainingCourse().getInstructorCost());
						dtoTrainingBatchSignup.getTraningCourse().setEmployeeCost(trainingBatchSignup.getTrainingCourse().getEmployeeCost());
						dtoTrainingBatchSignup.getTraningCourse().setEmployerCost(trainingBatchSignup.getTrainingCourse().getEmployerCost());
						dtoTrainingBatchSignup.getTraningCourse().setSupplierCost(trainingBatchSignup.getTrainingCourse().getSupplierCost());
						dtoTrainingBatchSignup.getTraningCourse().setEmployeeCost(trainingBatchSignup.getTrainingCourse().getEmployeeCost());
						dtoTrainingBatchSignup.getTraningCourse().setEmployerCost(trainingBatchSignup.getTrainingCourse().getEmployerCost());
						dtoTrainingBatchSignup.getTraningCourse().setPrerequisiteId(trainingBatchSignup.getTrainingCourse().getPrerequisiteId());
					    dtoTrainingBatchSignup.setTrainingClassStatus(trainingBatchSignup.getTrainingClassStatus());
						
						
					}
					
				}
			} else {
				dtoTrainingBatchSignup.setMessageType("TRANING_BATCH_SIGHNUP__NOT_GETTING");

			}
		} else {
			dtoTrainingBatchSignup.setMessageType("TRANING_BATCH_SIGHNUP_ID");

		}
		return dtoTrainingBatchSignup;
	}

	public DtoSearch getTraningBatchSighUpByIdTraningId(int id) {
		
		DtoSearch dtoSearchForSignUp = new DtoSearch();
		
		List<DtoTrainingBatchSignup> listdtoTrainingBatchSignup = new ArrayList<>();
		if (id > 0) {
			List<TrainingBatchSignup> trainingBatchSignup = repositoryTrainingBatchSignup
					.predictiveTrainingBatchSignupSearchWithPaginationByTraningId(id);
			
			
			
		
			
			if (trainingBatchSignup != null) {
				for (TrainingBatchSignup trainingBatchSignup1 : trainingBatchSignup) {
					DtoTrainingBatchSignup		dtoTrainingBatc = new DtoTrainingBatchSignup(trainingBatchSignup1);
					List<EmployeeMaster> employeeMastersList = new ArrayList<>();
					
					
					List<Integer> employeeMasters = 	repositoryTrainingBatchSignup.getEmployeeByTrainingId(trainingBatchSignup1.getId());
					
					if(employeeMasters!=null && !employeeMasters.isEmpty()) {
						employeeMastersList = repositoryTrainingBatchSignup.findEmployee(employeeMasters);
					}
					
					
					
					if (dtoTrainingBatc.getEmployeeMaster() == null) {
						List<DtoEmployeeMaster> dtoEmployeeMaster = new ArrayList<>();
						dtoTrainingBatc.setEmployeeMaster(dtoEmployeeMaster);

						for (EmployeeMaster employeeMaster : employeeMastersList) {
							DtoEmployeeMaster dtoEmployeeMasterBatch = new DtoEmployeeMaster();

							dtoEmployeeMasterBatch.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
							dtoEmployeeMasterBatch.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
							dtoEmployeeMasterBatch.setEmployeeLastName(employeeMaster.getEmployeeLastName());
							dtoEmployeeMasterBatch.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
							dtoEmployeeMasterBatch.setEmployeeLastNameArabic(employeeMaster.getEmployeeLastNameArabic());
							dtoEmployeeMaster.add(dtoEmployeeMasterBatch);
						}
						dtoTrainingBatc.setEmployeeMaster(dtoEmployeeMaster);
					}

					if (dtoTrainingBatc.getTrainingClass() == null) {
						DtoTrainingCourseDetail dtoTraningClass = new DtoTrainingCourseDetail();
						dtoTrainingBatc.setTrainingClass(dtoTraningClass);

					}

					if (dtoTrainingBatc.getTraningCourse() == null) {
						DtoTraningCourse dtoTraningCourseDetail = new DtoTraningCourse();
						dtoTrainingBatc.setTraningCourse(dtoTraningCourseDetail);
						if (trainingBatchSignup1.getTrainingCourse() != null) {
							dtoTrainingBatc.getTraningCourse()
									.setId(trainingBatchSignup1.getTrainingCourse().getId());
							dtoTrainingBatc.getTraningCourse()
									.setTraningId(trainingBatchSignup1.getTrainingCourse().getTraningId());
							dtoTrainingBatc.getTraningCourse().setDesc(trainingBatchSignup1.getTrainingCourse().getDesc());
							dtoTrainingBatc.getTraningCourse().setArbicDesc(trainingBatchSignup1.getTrainingCourse().getArbicDesc());
							dtoTrainingBatc.getTraningCourse().setLocation(trainingBatchSignup1.getTrainingCourse().getLocation());
							dtoTrainingBatc.getTraningCourse().setInstructorCost(trainingBatchSignup1.getTrainingCourse().getInstructorCost());
							dtoTrainingBatc.getTraningCourse().setEmployeeCost(trainingBatchSignup1.getTrainingCourse().getEmployeeCost());
							dtoTrainingBatc.getTraningCourse().setEmployerCost(trainingBatchSignup1.getTrainingCourse().getEmployerCost());
							dtoTrainingBatc.getTraningCourse().setSupplierCost(trainingBatchSignup1.getTrainingCourse().getSupplierCost());
							dtoTrainingBatc.getTraningCourse().setEmployeeCost(trainingBatchSignup1.getTrainingCourse().getEmployeeCost());
							dtoTrainingBatc.getTraningCourse().setEmployerCost(trainingBatchSignup1.getTrainingCourse().getEmployerCost());
							dtoTrainingBatc.getTraningCourse().setPrerequisiteId(trainingBatchSignup1.getTrainingCourse().getPrerequisiteId());
						
						}

					} else {
						dtoTrainingBatc.setMessageType("TRANING_BATCH_SIGHNUP__NOT_GETTING");

					}

					listdtoTrainingBatchSignup.add(dtoTrainingBatc);
					dtoSearchForSignUp.setRecords(listdtoTrainingBatchSignup);
					dtoSearchForSignUp.setTotalCount(listdtoTrainingBatchSignup.size());
				}

			}

		}
		return dtoSearchForSignUp;

	}

public DtoSearch getSighnUpFromTrainingId(int id) {
		
		DtoSearch dtoSearch = new DtoSearch();
		
		List<DtoTrainingBatchSignup> listdtoTrainingBatchSignup = new ArrayList<>();
		if (id > 0) {
			
			
			List<TrainingBatchSignup> list = new ArrayList<>();
			List<TrainingBatchSignup> ids = repositoryTrainingBatchSignup.findByTrainingCourseDetailsId(id);
			
			
		
			
			if (list != null) {
				for (TrainingBatchSignup trainingBatchSignup1 : ids) {
					DtoTrainingBatchSignup		dtoTrainingBatchSignup = new DtoTrainingBatchSignup(trainingBatchSignup1);
					List<EmployeeMaster> employeeMastersList = new ArrayList<>();
					dtoTrainingBatchSignup.setCompleteDate(trainingBatchSignup1.getCompleteDate());
					
					List<Integer> employeeMasters = 	repositoryTrainingBatchSignup.getEmployeeByTrainingId(trainingBatchSignup1.getId());
					
					if(employeeMasters!=null && !employeeMasters.isEmpty()) {
						employeeMastersList = repositoryTrainingBatchSignup.findEmployee(employeeMasters);
					}
					
					
					
					if (dtoTrainingBatchSignup.getEmployeeMaster() == null) {
						List<DtoEmployeeMaster> dtoEmployeeMaster = new ArrayList<>();
						dtoTrainingBatchSignup.setEmployeeMaster(dtoEmployeeMaster);

						for (EmployeeMaster employeeMaster : employeeMastersList) {
							DtoEmployeeMaster dtoEmployeeMaster2 = new DtoEmployeeMaster();

							dtoEmployeeMaster2.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
							dtoEmployeeMaster2.setEmployeeId(employeeMaster.getEmployeeId());
							dtoEmployeeMaster2.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
							dtoEmployeeMaster2.setEmployeeLastName(employeeMaster.getEmployeeLastName());
							dtoEmployeeMaster2.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
							dtoEmployeeMaster2.setEmployeeLastNameArabic(employeeMaster.getEmployeeLastNameArabic());
							dtoEmployeeMaster.add(dtoEmployeeMaster2);
						}
						dtoTrainingBatchSignup.setEmployeeMaster(dtoEmployeeMaster);
					}

			
						
						
						
						List<DtoTrainingCourseDetail> listDtoTrainingCourseDetail = new ArrayList<>();
						
						for (TrainingCourseDetail trainingCourseDetail : trainingBatchSignup1.getTrainingCourseDetails()) {
							DtoTrainingCourseDetail dtoTrainingCourseDetail = 	getDtoTrainingCourseDetail(trainingCourseDetail);
						}
						
						dtoTrainingBatchSignup.setTrainingClassList(listDtoTrainingCourseDetail);
						dtoTrainingBatchSignup.setTrainingClassStatus(trainingBatchSignup1.getTrainingClassStatus());

					if (dtoTrainingBatchSignup.getTraningCourse() == null) {
						DtoTraningCourse dtoTraningCourseDetail = new DtoTraningCourse();
						dtoTrainingBatchSignup.setTraningCourse(dtoTraningCourseDetail);
						if (trainingBatchSignup1.getTrainingCourse() != null) {
							dtoTrainingBatchSignup.getTraningCourse()
									.setId(trainingBatchSignup1.getTrainingCourse().getId());
							dtoTrainingBatchSignup.getTraningCourse()
									.setTraningId(trainingBatchSignup1.getTrainingCourse().getTraningId());
							dtoTrainingBatchSignup.getTraningCourse().setDesc(trainingBatchSignup1.getTrainingCourse().getDesc());
							dtoTrainingBatchSignup.getTraningCourse().setArbicDesc(trainingBatchSignup1.getTrainingCourse().getArbicDesc());
							dtoTrainingBatchSignup.getTraningCourse().setLocation(trainingBatchSignup1.getTrainingCourse().getLocation());
							dtoTrainingBatchSignup.getTraningCourse().setInstructorCost(trainingBatchSignup1.getTrainingCourse().getInstructorCost());
							dtoTrainingBatchSignup.getTraningCourse().setEmployeeCost(trainingBatchSignup1.getTrainingCourse().getEmployeeCost());
							dtoTrainingBatchSignup.getTraningCourse().setEmployerCost(trainingBatchSignup1.getTrainingCourse().getEmployerCost());
							dtoTrainingBatchSignup.getTraningCourse().setSupplierCost(trainingBatchSignup1.getTrainingCourse().getSupplierCost());
							dtoTrainingBatchSignup.getTraningCourse().setEmployeeCost(trainingBatchSignup1.getTrainingCourse().getEmployeeCost());
							dtoTrainingBatchSignup.getTraningCourse().setEmployerCost(trainingBatchSignup1.getTrainingCourse().getEmployerCost());
							dtoTrainingBatchSignup.getTraningCourse().setPrerequisiteId(trainingBatchSignup1.getTrainingCourse().getPrerequisiteId());
							
						
						}

					} else {
						dtoTrainingBatchSignup.setMessageType("TRANING_BATCH_SIGHNUP__NOT_GETTING");

					}

					listdtoTrainingBatchSignup.add(dtoTrainingBatchSignup);
					dtoSearch.setRecords(listdtoTrainingBatchSignup);
					dtoSearch.setTotalCount(listdtoTrainingBatchSignup.size());
				}

			}

		}
		return dtoSearch;

	}

	

	public DtoSearchTranningById getTraningIdByStatus(DtoSearchTranningById dtoSearch) {
		DtoTraningCourse dtoTraningCourseBatchTest  = new DtoTraningCourse();
		
		
		if(dtoSearch.getId()!=null  && dtoSearch.getTrainingClassStatus() == 0) {
			List<TraningCourse> traningCourseList = repositoryTraningCourse.predictivegetAllTraningCourseIdSearchWithPagination(dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize()));
				
				for (TraningCourse traningCourse : traningCourseList) {
					dtoTraningCourseBatchTest= new DtoTraningCourse();
					dtoTraningCourseBatchTest.setId(traningCourse.getId());
					dtoTraningCourseBatchTest.setTraningId(traningCourse.getTraningId());
					dtoTraningCourseBatchTest.setDesc(traningCourse.getDesc());
					dtoTraningCourseBatchTest.setArbicDesc(traningCourse.getArbicDesc());
					dtoTraningCourseBatchTest.setLocation(traningCourse.getLocation());
					dtoTraningCourseBatchTest.setPrerequisiteId(traningCourse.getPrerequisiteId());
					dtoTraningCourseBatchTest.setEmployeeCost(traningCourse.getEmployeeCost());
					dtoTraningCourseBatchTest.setEmployerCost(traningCourse.getEmployerCost());
					dtoTraningCourseBatchTest.setSupplierCost(traningCourse.getSupplierCost());
					dtoTraningCourseBatchTest.setInstructorCost(traningCourse.getInstructorCost());
					
					List<DtoTrainingCourseDetail> listDtoTrainingCourseDetail = new ArrayList<>();
					
					for (TrainingCourseDetail trainingCourseDetail : traningCourse.getListTrainingCourseDetail()) {
						DtoTrainingCourseDetail dtoTrainingCourseDetailBatch = 	getDtoTrainingCourseDetail(trainingCourseDetail);
						
						Set<EmployeeMaster> employeeMastersSignUp = new HashSet<>(); 
						String employeeIds = "";
						String employeeNames = "";
						
						for (TrainingBatchSignup trainingBatchSignup : traningCourse.getTrainingBatchSignupList()) {
							
							for (EmployeeMaster dtoEmployeeMaster : trainingBatchSignup.getEmployeeMaster()) {
								
								employeeMastersSignUp.add(dtoEmployeeMaster);
							}
						}
						
						for (EmployeeMaster employeeMaster : employeeMastersSignUp) {
							employeeIds += employeeMaster.getEmployeeId() + " ";
							employeeNames += employeeMaster.getEmployeeFirstName() + " ";
						}
						
						dtoTrainingCourseDetailBatch.setEmployeeId(employeeIds);
						dtoTrainingCourseDetailBatch.setEmployeeName(employeeNames);
						
						
						listDtoTrainingCourseDetail.add(dtoTrainingCourseDetailBatch);
					}
					dtoTraningCourseBatchTest.setSubItems(listDtoTrainingCourseDetail);
					
				}
				dtoSearch.setRecords(dtoTraningCourseBatchTest);	
			
		}else  if(dtoSearch.getTrainingClassStatus()==1){
			


			List<TrainingBatchSignup> list = repositoryTrainingBatchSignup.findByTrainingClassByIdAndTrainingClassStatusAndIsDeleted(dtoSearch.getId());
			if(list!=null) {
				List<DtoTrainingCourseDetail> listDtoTrainingCourse = new ArrayList<>();
				for (TrainingBatchSignup trainingBatchSign : list) {
					if(trainingBatchSign.getTrainingClassStatus()==1) {
						dtoTraningCourseBatchTest= new DtoTraningCourse();
						dtoTraningCourseBatchTest.setId(trainingBatchSign.getTrainingCourse().getId());
						dtoTraningCourseBatchTest.setTraningId(trainingBatchSign.getTrainingCourse().getTraningId());
						dtoTraningCourseBatchTest.setDesc(trainingBatchSign.getTrainingCourse().getDesc());
						dtoTraningCourseBatchTest.setArbicDesc(trainingBatchSign.getTrainingCourse().getArbicDesc());
						dtoTraningCourseBatchTest.setLocation(trainingBatchSign.getTrainingCourse().getLocation());
						dtoTraningCourseBatchTest.setPrerequisiteId(trainingBatchSign.getTrainingCourse().getPrerequisiteId());
						dtoTraningCourseBatchTest.setEmployeeCost(trainingBatchSign.getTrainingCourse().getEmployeeCost());
						dtoTraningCourseBatchTest.setEmployerCost(trainingBatchSign.getTrainingCourse().getEmployerCost());
						dtoTraningCourseBatchTest.setSupplierCost(trainingBatchSign.getTrainingCourse().getSupplierCost());
						dtoTraningCourseBatchTest.setInstructorCost(trainingBatchSign.getTrainingCourse().getInstructorCost());
						
						
						
						for (TrainingCourseDetail trainingCourseDetail : trainingBatchSign.getTrainingCourseDetails()) {
							DtoTrainingCourseDetail dtoTrainingCourseDetail = 	getDtoTrainingCourseDetail(trainingCourseDetail);
									
									if(trainingBatchSign.getTrainingCourse()!=null) {
										Set<EmployeeMaster> employeeMasters = new HashSet<>(); 
										String employeeId = "";
										String employeeName = "";
										
										for (TrainingBatchSignup trainingBatchSignup1 : trainingBatchSign.getTrainingCourse().getTrainingBatchSignupList()) {
											
											for (EmployeeMaster dtoEmployeeMaster : trainingBatchSignup1.getEmployeeMaster()) {
												
												employeeMasters.add(dtoEmployeeMaster);
											}
										}
										
										for (EmployeeMaster employeeMaster : employeeMasters) {
											employeeId += employeeMaster.getEmployeeId() + " ";
											employeeName += employeeMaster.getEmployeeFirstName() + " ";
										}
										
										dtoTrainingCourseDetail.setEmployeeId(employeeId);
										dtoTrainingCourseDetail.setEmployeeName(employeeName);
									}
									
									listDtoTrainingCourse.add(dtoTrainingCourseDetail);
								
							
						}
						
					}
					
					
					
					
				}
				dtoTraningCourseBatchTest.setSubItems(listDtoTrainingCourse);
				dtoSearch.setRecords(dtoTraningCourseBatchTest);
			}
		}else if (dtoSearch.getTrainingClassStatus()==2){
				List<TrainingBatchSignup> listTrainingBatchSignup = repositoryTrainingBatchSignup.findByTrainingClassByIdAndTrainingClassStatusAndIsDeleted(dtoSearch.getId());
				if(listTrainingBatchSignup!=null) {
					List<DtoTrainingCourseDetail> listDtoTrainingCourseDetail = new ArrayList<>();
					for (TrainingBatchSignup trainingBatchSignup : listTrainingBatchSignup) {
						if(trainingBatchSignup.getTrainingClassStatus()==2) {
							dtoTraningCourseBatchTest= new DtoTraningCourse();
							dtoTraningCourseBatchTest.setId(trainingBatchSignup.getTrainingCourse().getId());
							dtoTraningCourseBatchTest.setTraningId(trainingBatchSignup.getTrainingCourse().getTraningId());
							dtoTraningCourseBatchTest.setDesc(trainingBatchSignup.getTrainingCourse().getDesc());
							dtoTraningCourseBatchTest.setArbicDesc(trainingBatchSignup.getTrainingCourse().getArbicDesc());
							dtoTraningCourseBatchTest.setLocation(trainingBatchSignup.getTrainingCourse().getLocation());
							dtoTraningCourseBatchTest.setPrerequisiteId(trainingBatchSignup.getTrainingCourse().getPrerequisiteId());
							dtoTraningCourseBatchTest.setEmployeeCost(trainingBatchSignup.getTrainingCourse().getEmployeeCost());
							dtoTraningCourseBatchTest.setEmployerCost(trainingBatchSignup.getTrainingCourse().getEmployerCost());
							dtoTraningCourseBatchTest.setSupplierCost(trainingBatchSignup.getTrainingCourse().getSupplierCost());
							dtoTraningCourseBatchTest.setInstructorCost(trainingBatchSignup.getTrainingCourse().getInstructorCost());
							
							
							
							
							for (TrainingCourseDetail trainingCourseDetail : trainingBatchSignup.getTrainingCourseDetails()) {
										
								
								DtoTrainingCourseDetail dtoTrainingCourseDetail = 	getDtoTrainingCourseDetail(trainingCourseDetail);
								
									if(trainingBatchSignup.getTrainingCourse()!=null) {
										Set<EmployeeMaster> employeeMasters = new HashSet<>(); 
										String employeeId = "";
										String employeeName = "";
										
										for (TrainingBatchSignup trainingBatchSignup1 : trainingBatchSignup.getTrainingCourse().getTrainingBatchSignupList()) {
											
											for (EmployeeMaster dtoEmployeeMaster : trainingBatchSignup1.getEmployeeMaster()) {
												
												employeeMasters.add(dtoEmployeeMaster);
											}
										}
										
										for (EmployeeMaster employeeMaster : employeeMasters) {
											employeeId += employeeMaster.getEmployeeId() + " ";
											employeeName += employeeMaster.getEmployeeFirstName() + " ";
										}
										
										dtoTrainingCourseDetail.setEmployeeId(employeeId);
										dtoTrainingCourseDetail.setEmployeeName(employeeName);
									}
										
										listDtoTrainingCourseDetail.add(dtoTrainingCourseDetail);
									
								
							}
							dtoTraningCourseBatchTest.setSubItems(listDtoTrainingCourseDetail);
						}
						
						
						
						
					}
					dtoSearch.setRecords(dtoTraningCourseBatchTest);
				}
			
			}
		return dtoSearch;
	}
	
	
	public DtoTrainingCourseDetail getDtoTrainingCourseDetail(TrainingCourseDetail trainingCourseDetail ) {
		DtoTrainingCourseDetail dtoTrainingCourseDetail = new DtoTrainingCourseDetail();
		dtoTrainingCourseDetail.setId(trainingCourseDetail.getId());
		dtoTrainingCourseDetail.setClassId(trainingCourseDetail.getClassId());
		dtoTrainingCourseDetail.setClassName(trainingCourseDetail.getClassName());
		dtoTrainingCourseDetail.setStartDate(trainingCourseDetail.getStartDate());
		dtoTrainingCourseDetail.setEndDate(trainingCourseDetail.getEndDate());
		dtoTrainingCourseDetail.setStartTime(trainingCourseDetail.getStartTime());
		dtoTrainingCourseDetail.setEndTime(trainingCourseDetail.getEndTime());
		dtoTrainingCourseDetail.setInstructorName(trainingCourseDetail.getInstructorName());
		dtoTrainingCourseDetail.setEnrolled(trainingCourseDetail.getEnrolled());
		dtoTrainingCourseDetail.setMaximum(trainingCourseDetail.getMaximum());
		dtoTrainingCourseDetail.setClassLocation(trainingCourseDetail.getClassLocation());
		return dtoTrainingCourseDetail;
	}
	
	
	
}
