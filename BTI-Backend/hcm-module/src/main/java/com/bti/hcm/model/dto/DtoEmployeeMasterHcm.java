package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.EmployeeMaster;

public class DtoEmployeeMasterHcm {
	private int employeeIndexId;
	private String employeeId;
	private String employeeLastName;
	private String employeeFirstName;
	private String employeeMiddleName;
	private String employeeName;
	private List<DtoDeductionCode> dtoDeductionCode;
	
	private DtoDeductionCode dtoDeductionCodeObj;

	private DtoEmployeeMaster employeeMaster;
	private DtoPayCode payCode;

	public int getEmployeeIndexId() {
		return employeeIndexId;
	}

	public void setEmployeeIndexId(int employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeLastName() {
		return employeeLastName;
	}

	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public String getEmployeeMiddleName() {
		return employeeMiddleName;
	}

	public void setEmployeeMiddleName(String employeeMiddleName) {
		this.employeeMiddleName = employeeMiddleName;
	}

	public DtoEmployeeMasterHcm() {

	}

	public DtoEmployeeMasterHcm(EmployeeMaster employeeMaster) {

	}

	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public DtoPayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(DtoPayCode payCode) {
		this.payCode = payCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public List<DtoDeductionCode> getDtoDeductionCode() {
		return dtoDeductionCode;
	}

	public void setDtoDeductionCode(List<DtoDeductionCode> dtoDeductionCode) {
		this.dtoDeductionCode = dtoDeductionCode;
	}

	public DtoDeductionCode getDtoDeductionCodeObj() {
		return dtoDeductionCodeObj;
	}

	public void setDtoDeductionCodeObj(DtoDeductionCode dtoDeductionCodeObj) {
		this.dtoDeductionCodeObj = dtoDeductionCodeObj;
	}

}
