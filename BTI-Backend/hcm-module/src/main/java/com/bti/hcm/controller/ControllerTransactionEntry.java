package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBuildCheckBatchMarkOrUnMark;
import com.bti.hcm.model.dto.DtoBuildDefaultCheck;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTransactionEntry;
import com.bti.hcm.model.dto.DtoTransactionEntryDetail;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTransactionEntry;

@RestController
@RequestMapping("/transactionEntry")
public class ControllerTransactionEntry extends BaseController{

	private static final Logger log = Logger.getLogger(ControllerTypeAccurual.class);
	
	/**
	 * @Description serviceTransactionEntry Autowired here using annotation of spring for use of TransactionEntry method in this controller
	 */
	@Autowired(required=true)
	ServiceTransactionEntry serviceTransactionEntry;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoTransactionEntry dtoAccrual) throws Exception {
		log.info("Create TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrual = serviceTransactionEntry.saveOrUpdate(dtoAccrual);
			
			responseMessage = displayMessage(dtoAccrual,"TRANSCATION_ENTRY_CREATED","TRANSCATION_ENTRY_NOT_CREATED",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(responseMessage!=null) {
			log.debug("Create TransactionEntry Method:"+responseMessage.getMessage());
		}
	
		return responseMessage;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoTransactionEntry dtoAccrual) throws Exception {
		log.info("Create TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAccrual = serviceTransactionEntry.saveOrUpdate(dtoAccrual);
			responseMessage = displayMessage(dtoAccrual,MessageConstant.TRANSCATION_ENTRY_UPDATED,MessageConstant.TRANSCATION_ENTRY_NOT_UPDATED,serviceResponse);
		} else {
			unauthorizedMsg(serviceResponse);
		}
		if(responseMessage!=null) {
			log.debug("Create TransactionEntry Method:"+responseMessage.getMessage());
		}
		
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoTransactionEntryDetail dtoTransactionEntry) throws Exception {
		log.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTransactionEntryDetail dtoTransactionEntryObj = serviceTransactionEntry.getById(dtoTransactionEntry.getId());
			responseMessage=displayMessage(dtoTransactionEntryObj, "TRANSCATION_ENTRY_GET_DETAIL_FOR_ID", "TRANSCATION_ENTRY_NOT_GETING_DETAIL_FOR_ID", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get ById Method:"+dtoTransactionEntry.getId());
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/searchByBatchId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchByBatchId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search TransactionEntryss Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTransactionEntry.searchByBatchId(dtoSearch);
			responseMessage = displayMessage(dtoSearch,MessageConstant.BUILD_CHECK_BATCHES_GET_ALL,MessageConstant.BUILD_CHECK_BATCHES_NOT_GETTING,serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			log.debug("Search TransactionEntryes Method:"+dtoSearch.getTotalCount());	
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTransactionEntry.getAll(dtoSearch);
			responseMessage = displayMessage(dtoSearch,MessageConstant.BUILD_CHECK_BATCHES_GET_ALL,MessageConstant.BUILD_CHECK_BATCHES_NOT_GETTING,serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			log.debug("Search TransactionEntry Method:"+dtoSearch.getTotalCount());	
		}
		
		return responseMessage;
	}
	
	
	/*@RequestMapping(value = "/getAllForCalculateCheck", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllForCalculateCheck(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTransactionEntry.getAllCalculateCheck(dtoSearch);
			responseMessage = displayMessage(dtoSearch,"BUILD_CHECK_BATCHES_GET_ALL","BUILD_CHECK_BATCHES_NOT_GETTING",serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			log.debug("Search TransactionEntry Method:"+dtoSearch.getTotalCount());	
		}
		
		return responseMessage;
	}*/
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.PUT)
	public ResponseMessage deleteBatch(HttpServletRequest request, @RequestBody DtoTransactionEntry dtoTransactionEntry) throws Exception {
		log.info("Delete TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTransactionEntry.getIds() != null) {
				DtoTransactionEntry dtoTraningCourse2 = serviceTransactionEntry.deleteBatch(dtoTransactionEntry.getIds());
				
				if(dtoTraningCourse2.getMessageType().equals("")) {
					
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("TRANSCATION_ENTRY_NOT_DELETED", false), dtoTraningCourse2);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("TRANSCATION_ENTRY_DELETED", false), dtoTraningCourse2);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(responseMessage!=null) {
			log.debug("Delete TransactionEntry Method:"+responseMessage.getMessage());
		}
		
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/deleteBatchRow", method = RequestMethod.PUT)
	public ResponseMessage deleteBatchRow(HttpServletRequest request, @RequestBody DtoTransactionEntryDetail dtoTransactionEntryDetail) throws Exception {
		log.info("Delete TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTransactionEntryDetail.getIds() != null && !dtoTransactionEntryDetail.getIds().isEmpty()) {
				DtoTransactionEntryDetail dtoTraningCourse2 = serviceTransactionEntry.deleteBatchRow(dtoTransactionEntryDetail.getIds());
				
				if(dtoTraningCourse2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("TRANSCATION_ENTRY_DELETED", false), dtoTraningCourse2);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("TRANSCATION_ENTRY_NOT_DELETED", false), dtoTraningCourse2);
				}
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(responseMessage!=null) {
			log.debug("Delete TransactionEntry Method:"+responseMessage.getMessage());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllForFromDateToDate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllForFromDateToDate(@RequestBody DtoTransactionEntry  dtoTransactionEntry, HttpServletRequest request) throws Exception {
		log.info("Search BuildPayrollCheckByBatches Method");
		DtoSearch dtoSearch = null;
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTransactionEntry.getAllForFromDateToDate(dtoTransactionEntry);
			responseMessage = displayMessage(dtoSearch,"BUILD_CHECK_BATCHES_GET_ALL","BUILD_CHECK_BATCHES_NOT_GETTING",serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/batcheIdcheckForTransactionEntry", method = RequestMethod.POST)
	public ResponseMessage batcheIdcheckForTransactionEntry(HttpServletRequest request, @RequestBody DtoTransactionEntry dtoTransactionEntry) throws Exception {
		log.info("batcheIdcheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTransactionEntry dtoTransactionEntryObj = serviceTransactionEntry.repeatByBatchesIdForTransaction(dtoTransactionEntry);
			 if (dtoTransactionEntryObj !=null && dtoTransactionEntryObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                         serviceResponse.getMessageByShortAndIsDeleted("BATCHES_GET_DETAILS_FOR_TRANSACTION", false), dtoTransactionEntryObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                         serviceResponse.getMessageByShortAndIsDeleted("BATCHES_NOT_GETTING_FOR_TRANSACTION", false),
                         dtoTransactionEntryObj);
             }
			
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/buildcheckBatchMarkUnMark", method = RequestMethod.POST)
	public ResponseMessage buildcheckBatchMarkUnMark(HttpServletRequest request, @RequestBody DtoBuildCheckBatchMarkOrUnMark dtoBuildCheckBatchMarkOrUnMark) throws Exception {
		log.info("Create TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBuildCheckBatchMarkOrUnMark = serviceTransactionEntry.checkmarkOrUnMark(dtoBuildCheckBatchMarkOrUnMark);
			
			responseMessage = displayMessage(dtoBuildCheckBatchMarkOrUnMark,"BATCH_STATUS_CHANGED","BATCH_STATUS_NOT_CHANGED",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(responseMessage!=null) {
			log.debug("Create TransactionEntry Method:"+responseMessage.getMessage());
		}
	
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/checkAndUnCheckedFromBatchBuildCheck", method = RequestMethod.POST)
	public ResponseMessage checkAndUnCheckedFromBatchBuildCheck(HttpServletRequest request, @RequestBody DtoBuildDefaultCheck dtoBuildDefaultCheck) throws Exception {
		log.info("CheckAndUnCheckedFromBatchBuildCheck TransactionEntry Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBuildDefaultCheck = serviceTransactionEntry.checkAndUnCheckedFromBatchBuildCheck(dtoBuildDefaultCheck);
			
			responseMessage = displayMessage(dtoBuildDefaultCheck,"BATCH_STATUS_CHANGED","BATCH_STATUS_NOT_CHANGED",serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(responseMessage!=null) {
			log.debug("CheckAndUnCheckedFromBatchBuildCheck  Method:"+responseMessage.getMessage());
		}
	
		return responseMessage;
	}
}
