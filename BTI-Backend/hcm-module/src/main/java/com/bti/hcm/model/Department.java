/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the department database table. Name of
 * Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40102",indexes = {
        @Index(columnList = "DEPINDX")
})
@NamedQuery(name = "Department.findAll", query = "SELECT d FROM Department d")
public class Department extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DEPINDX")
	private int id;

	@Column(name = "DEPID", columnDefinition = "char(15)")
	private String departmentId;

	@Column(name = "DEPDSCR", columnDefinition = "char(31)")
	private String departmentDescription;

	@Column(name = "DEPDSCRA", columnDefinition = "char(61)")
	private String arabicDepartmentDescription;

	@Column(name = "GREGORIANDATE")
	private Date gregorianDate;

	@Column(name = "HIJRIDATE", columnDefinition = "char(61)")
	private String hijriDate;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "department")
	@Where(clause = "is_deleted = false")
	private List<PayScheduleSetupDepartment> payScheduleSetupDepartment;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "department")
	@Where(clause = "is_deleted = false")
	private List<Requisitions> requisitions;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "department")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<EmployeeMaster> employeeMaster;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "department")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<PayrollTransactionOpenYearDetails> payrollTransactionOpenYearDetails;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "department")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<ParollTransactionOpenYearDistribution> parollTransactionOpenYearDistribution;
	
	
	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "department")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeePositionHistory> listEmployeePositionHistory;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "department")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<PayrollAccounts> listPayrollAccounts;
	
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="department")
	//@LazyCollection(LazyCollectionOption.FALSE)
    private List<ManualChecksDistribution> listManualChecksDistribution;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="department")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksOpenYearDistribution>listManualChecksOpenYearDistribution;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="department")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksHistoryYearDistributions>listManualChecksHistoryYearDistributions;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getArabicDepartmentDescription() {
		return arabicDepartmentDescription;
	}

	public void setArabicDepartmentDescription(String arabicDepartmentDescription) {
		this.arabicDepartmentDescription = arabicDepartmentDescription;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDepartmentDescription() {
		return departmentDescription;
	}

	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}

	public Date getGregorianDate() {
		return gregorianDate;
	}

	public void setGregorianDate(Date gregorianDate) {
		this.gregorianDate = gregorianDate;
	}

	public String getHijriDate() {
		return hijriDate;
	}

	public void setHijriDate(String hijriDate) {
		this.hijriDate = hijriDate;
	}

	public List<PayScheduleSetupDepartment> getPayScheduleSetupDepartment() {
		return payScheduleSetupDepartment;
	}

	public void setPayScheduleSetupDepartment(List<PayScheduleSetupDepartment> payScheduleSetupDepartment) {
		this.payScheduleSetupDepartment = payScheduleSetupDepartment;
	}

	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public List<Requisitions> getRequisitions() {
		return requisitions;
	}

	public void setRequisitions(List<Requisitions> requisitions) {
		this.requisitions = requisitions;
	}

	public List<EmployeePositionHistory> getListEmployeePositionHistory() {
		return listEmployeePositionHistory;
	}

	public void setListEmployeePositionHistory(List<EmployeePositionHistory> listEmployeePositionHistory) {
		this.listEmployeePositionHistory = listEmployeePositionHistory;
	}

	public List<PayrollTransactionOpenYearDetails> getPayrollTransactionOpenYearDetails() {
		return payrollTransactionOpenYearDetails;
	}

	public void setPayrollTransactionOpenYearDetails(
			List<PayrollTransactionOpenYearDetails> payrollTransactionOpenYearDetails) {
		this.payrollTransactionOpenYearDetails = payrollTransactionOpenYearDetails;
	}

	public List<ManualChecksDistribution> getListManualChecksDistribution() {
		return listManualChecksDistribution;
	}

	public void setListManualChecksDistribution(List<ManualChecksDistribution> listManualChecksDistribution) {
		this.listManualChecksDistribution = listManualChecksDistribution;
	}

	public List<ManualChecksHistoryYearDistributions> getListManualChecksHistoryYearDistributions() {
		return listManualChecksHistoryYearDistributions;
	}

	public void setListManualChecksHistoryYearDistributions(
			List<ManualChecksHistoryYearDistributions> listManualChecksHistoryYearDistributions) {
		this.listManualChecksHistoryYearDistributions = listManualChecksHistoryYearDistributions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((arabicDepartmentDescription == null) ? 0 : arabicDepartmentDescription.hashCode());
		result = prime * result + ((departmentDescription == null) ? 0 : departmentDescription.hashCode());
		result = prime * result + ((departmentId == null) ? 0 : departmentId.hashCode());
		result = prime * result + ((employeeMaster == null) ? 0 : employeeMaster.hashCode());
		result = prime * result + ((gregorianDate == null) ? 0 : gregorianDate.hashCode());
		result = prime * result + ((hijriDate == null) ? 0 : hijriDate.hashCode());
		result = prime * result + id;
		result = prime * result + ((listEmployeePositionHistory == null) ? 0 : listEmployeePositionHistory.hashCode());
		result = prime * result + ((payScheduleSetupDepartment == null) ? 0 : payScheduleSetupDepartment.hashCode());
		result = prime * result + ((requisitions == null) ? 0 : requisitions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (arabicDepartmentDescription == null) {
			if (other.arabicDepartmentDescription != null)
				return false;
		} else if (!arabicDepartmentDescription.equals(other.arabicDepartmentDescription)) {
			return false;
		}
		if (departmentDescription == null) {
			if (other.departmentDescription != null)
				return false;
		} else if (!departmentDescription.equals(other.departmentDescription)) {
			return false;
		}
		if (departmentId == null) {
			if (other.departmentId != null)
				return false;
		} else if (!departmentId.equals(other.departmentId)) {
			return false;
		}
		if (employeeMaster == null) {
			if (other.employeeMaster != null)
				return false;
		} else if (!employeeMaster.equals(other.employeeMaster)) {
			return false;
		}
		if (gregorianDate == null) {
			if (other.gregorianDate != null)
				return false;
		} else if (!gregorianDate.equals(other.gregorianDate)) {
			return false;
		}
		if (hijriDate == null) {
			if (other.hijriDate != null)
				return false;
		} else if (!hijriDate.equals(other.hijriDate)) {
			return false;
		}
		if (id != other.id)
			return false;
		if (listEmployeePositionHistory == null) {
			if (other.listEmployeePositionHistory != null)
				return false;
		} else if (!listEmployeePositionHistory.equals(other.listEmployeePositionHistory)) {
			return false;
		}
		if (payScheduleSetupDepartment == null) {
			if (other.payScheduleSetupDepartment != null)
				return false;
		} else if (!payScheduleSetupDepartment.equals(other.payScheduleSetupDepartment)) {
			return false;
		}
		if (requisitions == null) {
			if (other.requisitions != null)
				return false;
		} else if (!requisitions.equals(other.requisitions)) {
			return false;
		}
		return true;
	}

	public List<PayrollAccounts> getListPayrollAccounts() {
		return listPayrollAccounts;
	}

	public void setListPayrollAccounts(List<PayrollAccounts> listPayrollAccounts) {
		this.listPayrollAccounts = listPayrollAccounts;
	}

	public List<ParollTransactionOpenYearDistribution> getParollTransactionOpenYearDistribution() {
		return parollTransactionOpenYearDistribution;
	}

	public void setParollTransactionOpenYearDistribution(
			List<ParollTransactionOpenYearDistribution> parollTransactionOpenYearDistribution) {
		this.parollTransactionOpenYearDistribution = parollTransactionOpenYearDistribution;
	}

	public List<ManualChecksOpenYearDistribution> getListManualChecksOpenYearDistribution() {
		return listManualChecksOpenYearDistribution;
	}

	public void setListManualChecksOpenYearDistribution(
			List<ManualChecksOpenYearDistribution> listManualChecksOpenYearDistribution) {
		this.listManualChecksOpenYearDistribution = listManualChecksOpenYearDistribution;
	}

}
