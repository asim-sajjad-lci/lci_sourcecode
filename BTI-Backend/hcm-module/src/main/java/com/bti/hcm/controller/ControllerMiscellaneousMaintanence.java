package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoMiscellaneousMaintanence;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceMiscellaneousMaintenance;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/miscellaneousMaintenance")
public class ControllerMiscellaneousMaintanence extends BaseController {
	private Logger log = Logger.getLogger(ControllerMiscellaneousMaintanence.class);

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	@Autowired
	ServiceMiscellaneousMaintenance serviceMiscellaneousMaintanence;

	@PostMapping("/create")
	public ResponseMessage createMiscellaneousEntry(HttpServletRequest request,
			@RequestBody List<DtoMiscellaneousMaintanence> dtoMiscellaneousMaintenance) throws Exception {
		log.info("Create Miscellaneous Maintenance Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoMiscellaneousMaintenance = serviceMiscellaneousMaintanence.saveOrUpdate(dtoMiscellaneousMaintenance);
			responseMessage = displayMessage(dtoMiscellaneousMaintenance, "MISCELLANEOUS_MAINTENANCE_ENTRY_CREATED",
					"MISCELLANEOUS_MAINTENANCE_ENTRY_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create Miscellaneous Maintenance Entry:" + responseMessage.getMessage());
		return responseMessage;
	}

	@PostMapping("/getAllMiscellaneousForEmployeeWithId")
	public ResponseMessage getAllMiscellaneousForEmployee(HttpServletRequest request,
			@RequestBody DtoSearchActivity dtoSearchActivity) throws Exception {
		log.info("Search Miscellaneous for Employee Entry Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearchActivity = this.serviceMiscellaneousMaintanence
					.searchMiscellaneousEntryWithValues(dtoSearchActivity);
			responseMessage = displayMessage(dtoSearchActivity, "MISCELLANEOUS_ENTRY_ALL_RECORDS_FOUND",
					"MISCELLANEOUS_ENTRY_RECORDS_NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
