/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

/**
 * Name of Project: Hcm
 * Version: 0.0.1
 */
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.HrCity;

@Repository("repositoryHrCity")
public interface RepositoryHrCity extends JpaRepository<HrCity, Integer> {

	
	@Query("select c from HrCity c where (c.cityName like %:searchKeyWord% ) and c.isDeleted=false")
	public List<HrCity> predictiveCitySearch(@Param("searchKeyWord") String searchKeyWord);
	
	List<HrCity> findByIsDeletedAndLanguageLanguageId(boolean deleted, int langId);

	@Query("select p from HrCity p where (p.state.stateId =:state) and p.isDeleted=false")
	public List<HrCity> findByStateId(@Param("state") Integer stateId);

	@Query("select p from HrCity p where (p.state.stateId =:state) and p.language.id =:langId and p.isDeleted=false")
	public List<HrCity> getByStateIdAndLangId(@Param("state")Integer stateId, @Param("langId")Integer valueOf);
	
}
