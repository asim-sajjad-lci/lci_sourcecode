
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.SalaryMatrixRow;
import com.bti.hcm.model.SalaryMatrixRowValue;
import com.bti.hcm.model.dto.DtoSalaryMatrixRowValue;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositorySalaryMatrixRow;
import com.bti.hcm.repository.RepositorySalaryMatrixRowValue;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceSalaryMatrixRowValue")
public class ServiceSalaryMatrixRowValue {
	/**
	 * @Description LOGGER use for put a logger in ServiceSalaryMatrixRowValue Service
	 */
	static Logger log = Logger.getLogger(ServiceSalaryMatrixRowValue.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in ServiceSalaryMatrixRowValue service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceSalaryMatrixRowValue service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in ServiceSalaryMatrixRowValue service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositorySalaryMatrixRowValue Autowired here using annotation of spring for access of repositorySalaryMatrixRowValue method in SalaryMatrixRow service
	 * 				In short Access SalaryMatrixRowValue Query from Database using repositorySalaryMatrixRowValue.
	 */
	@Autowired
	RepositorySalaryMatrixRowValue repositorySalaryMatrixRowValue;
	
	/**
	 * 
	 */
	@Autowired
	RepositorySalaryMatrixRow repositorySalaryMatrixRow;

	
	/**
	 * @param dtoSalaryMatrixRowValue
	 * @return
	 * @throws ParseException
	 */
	public DtoSalaryMatrixRowValue saveOrUpdate(DtoSalaryMatrixRowValue dtoSalaryMatrixRowValue)  {
		log.info("saveOrUpdate SalaryMatrixRowValue Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		SalaryMatrixRowValue salaryMatrixRowValue = null;
		if (dtoSalaryMatrixRowValue.getId() != null && dtoSalaryMatrixRowValue.getId() > 0) {
			salaryMatrixRowValue = repositorySalaryMatrixRowValue.findByIdAndIsDeleted(dtoSalaryMatrixRowValue.getId(), false);
			salaryMatrixRowValue.setUpdatedBy(loggedInUserId);
			salaryMatrixRowValue.setUpdatedDate(new Date());
		} else {
			salaryMatrixRowValue = new SalaryMatrixRowValue();
			salaryMatrixRowValue.setCreatedDate(new Date());
			Integer rowId = repositorySalaryMatrixRowValue.getCountOfTotaSalaryMatrixRowValue();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			salaryMatrixRowValue.setRowId(increment);
		}
		
		SalaryMatrixRow salaryMatrixRow = repositorySalaryMatrixRow.findOne(dtoSalaryMatrixRowValue.getSalaryMatrixRowId());
		salaryMatrixRowValue.setSalaryMatrixRow(salaryMatrixRow);
		salaryMatrixRowValue.setSequence(dtoSalaryMatrixRowValue.getSequence());
		salaryMatrixRowValue.setAmount(dtoSalaryMatrixRowValue.getAmount());
		
		salaryMatrixRowValue.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositorySalaryMatrixRowValue.saveAndFlush(salaryMatrixRowValue);
		log.debug("SalaryMatrixRow is:"+dtoSalaryMatrixRowValue.getId());
		return dtoSalaryMatrixRowValue;

	}

	
	/**
	 * @param dtoSalaryMatrixRowValue
	 * @return
	 */
	public DtoSearch getAll(DtoSalaryMatrixRowValue dtoSalaryMatrixRowValue) {
		log.info("getAllDeductionCode Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSalaryMatrixRowValue.getPageNumber());
		dtoSearch.setPageSize(dtoSalaryMatrixRowValue.getPageSize());
		dtoSearch.setTotalCount(repositorySalaryMatrixRowValue.getCountOfTotalSalaryMatrixRow());
		List<SalaryMatrixRowValue> salaryMatrixRowList = null;
		
		if (dtoSalaryMatrixRowValue.getPageNumber() != null && dtoSalaryMatrixRowValue.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSalaryMatrixRowValue.getPageNumber(), dtoSalaryMatrixRowValue.getPageSize(), Direction.DESC, "createdDate");
			salaryMatrixRowList = repositorySalaryMatrixRowValue.findByIsDeleted(false, pageable);
		} else {
			salaryMatrixRowList = repositorySalaryMatrixRowValue.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		
		List<DtoSalaryMatrixRowValue> salaryMatrixSetupList=new ArrayList<>();
		if(salaryMatrixRowList!=null && !salaryMatrixRowList.isEmpty())
		{
			for (SalaryMatrixRowValue salaryMatrixRow : salaryMatrixRowList) 
			{
				dtoSalaryMatrixRowValue=new DtoSalaryMatrixRowValue(salaryMatrixRow);
				dtoSalaryMatrixRowValue.setId(salaryMatrixRow.getId());
				
				salaryMatrixSetupList.add(dtoSalaryMatrixRowValue);
			}
			dtoSearch.setRecords(salaryMatrixSetupList);
		}
		log.debug("All DtoSalaryMatrixRowValue List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search SalaryMatrixSetupRowValue Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			dtoSearch.setTotalCount(this.repositorySalaryMatrixRowValue.predictiveSalaryMatrixSetupSearchTotalCount("%"+searchWord+"%"));
			List<SalaryMatrixRowValue> salaryMatrixSetupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				salaryMatrixSetupList = this.repositorySalaryMatrixRowValue.predictiveSalaryMatrixRowValueSearchWithPagination("%"+searchWord+"%");
			}
		
			if(salaryMatrixSetupList != null && !salaryMatrixSetupList.isEmpty()){
				List<DtoSalaryMatrixRowValue> matrixRowValueList = new ArrayList<>();
				DtoSalaryMatrixRowValue dtoSalaryMatrixSetup=null;
				for (SalaryMatrixRowValue salaryMatrixSetup : salaryMatrixSetupList) {
					dtoSalaryMatrixSetup = new DtoSalaryMatrixRowValue(salaryMatrixSetup);
					dtoSalaryMatrixSetup.setSequence(salaryMatrixSetup.getSequence());
					dtoSalaryMatrixSetup.setAmount(salaryMatrixSetup.getAmount());
					dtoSalaryMatrixSetup.setId(salaryMatrixSetup.getId());
					matrixRowValueList.add(dtoSalaryMatrixSetup);
				}
				dtoSearch.setRecords(matrixRowValueList);
			}
		}
		if(dtoSearch!=null){
			log.debug("Search SalaryMatrixRowValue Size is:"+dtoSearch.getTotalCount());
		}
		
		return dtoSearch;
	}

	
	/**
	 * @param id
	 * @return
	 */
	public DtoSalaryMatrixRowValue getById(int id) {
		log.info("getById Method");
		DtoSalaryMatrixRowValue dtoSalaryMatrixRowValue = new DtoSalaryMatrixRowValue();
		if (id > 0) {
			SalaryMatrixRowValue salaryMatrixSetup = repositorySalaryMatrixRowValue.findByIdAndIsDeleted(id, false);
			if (salaryMatrixSetup != null) {
				dtoSalaryMatrixRowValue = new DtoSalaryMatrixRowValue(salaryMatrixSetup);
			} else {
				dtoSalaryMatrixRowValue.setMessageType("SALARY_MATRIX_LIST_NOT_GETTING");

			}
		} else {
			dtoSalaryMatrixRowValue.setMessageType("SALARY_MATRIX_LIST_NOT_GETTING");

		}
		log.debug("SalaryMatrixSetupRow By Id is:"+dtoSalaryMatrixRowValue.getId());
		return dtoSalaryMatrixRowValue;
	}


	
	
	public DtoSalaryMatrixRowValue delete(List<Integer> ids) {
		log.info("delete DtoSalaryMatrixRowValue Method");
		DtoSalaryMatrixRowValue dtoSalaryMatrixRowValue = new DtoSalaryMatrixRowValue();
		dtoSalaryMatrixRowValue
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SALARY_MATRIX_DELETED", false));
		dtoSalaryMatrixRowValue.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SALARY_MATRIX_DELETED", false));
		List<DtoSalaryMatrixRowValue> dtoSalaryMatrixRowValue1 = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer Id : ids) {
				SalaryMatrixRowValue salaryMatrixSetup = repositorySalaryMatrixRowValue.findOne(Id);
				DtoSalaryMatrixRowValue dtoSalaryMatrixRow = new DtoSalaryMatrixRowValue();
				dtoSalaryMatrixRow.setId(salaryMatrixSetup.getId());
				repositorySalaryMatrixRowValue.deleteSingleSalaryMatrixSetupRowValue(true, loggedInUserId, Id);
				dtoSalaryMatrixRowValue1.add(dtoSalaryMatrixRow);

			}
			dtoSalaryMatrixRowValue.setDelete(dtoSalaryMatrixRowValue1);
		} catch (NumberFormatException e) {
			log.error("error in delete matrix row: ", e);
		}
		log.debug("Delete SalaryMatrixRowValue :"+dtoSalaryMatrixRowValue.getId());
		return dtoSalaryMatrixRowValue;
	}
}
