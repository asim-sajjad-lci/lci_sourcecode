package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "SY03600_h",indexes = {
        @Index(columnList = "ID")
})
public class VATSetup extends HcmBaseEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "TAXSCHDID",columnDefinition = "char(15)")
	private String vatScheduleId;
	
	@Column(name = "DSCRIPTN",columnDefinition = "char(31)")
	private String vatDescription;
	
	@Column(name = "DSCRIPTNA",columnDefinition = "char(61)")
	private String vatDescriptionArabic;
	
	@Column(name = "VATYP")
	private Integer vatSeriesType;
	
	@Column(name = "VATIDNUM")
	private Integer vatIdNumber;
	
	@Column(name = "ACTROWID")
	private Long accountTableRowIndex;
	
	@Column(name = "BASDID")
	private Integer vatBasedOn;
	
	@Column(name = "BASPERCT",precision = 10,scale = 2)
	private BigDecimal percentageBasedOn;
	
	@Column(name = "MINVATAMT",precision = 10,scale = 2)
	private BigDecimal minimumVatAmt;
	
	@Column(name = "MAXVATAMT",precision = 10,scale = 2)
	private BigDecimal maximumVatAmt;
	
	@Column(name = "YTDTOTSAL",precision = 10,scale = 2)
	private BigDecimal ytdTotalSales;
	
	@Column(name = "YTDTOTVAT",precision = 10,scale = 2)
	private BigDecimal ytdTotalTaxableSales;
	
	@Column(name = "YTDTOTTAX",precision = 10,scale = 2)
	private BigDecimal ytdTotalSalesTaxes;
	
	@Column(name = "LSTTOTSAL",precision = 10,scale = 2)
	private BigDecimal lastYearTotalSales;
	
	@Column(name = "LSTTOTVAT",precision = 10,scale = 2)
	private BigDecimal lastYearTotalTaxableSales;
	
	@Column(name = "LSTTOTTAX",precision = 10,scale = 2)
	private BigDecimal lastYearTotalSalesTaxes;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVatScheduleId() {
		return vatScheduleId;
	}

	public void setVatScheduleId(String vatScheduleId) {
		this.vatScheduleId = vatScheduleId;
	}

	public String getVatDescription() {
		return vatDescription;
	}

	public void setVatDescription(String vatDescription) {
		this.vatDescription = vatDescription;
	}

	public String getVatDescriptionArabic() {
		return vatDescriptionArabic;
	}

	public void setVatDescriptionArabic(String vatDescriptionArabic) {
		this.vatDescriptionArabic = vatDescriptionArabic;
	}

	public Integer getVatSeriesType() {
		return vatSeriesType;
	}

	public void setVatSeriesType(Integer vatSeriesType) {
		this.vatSeriesType = vatSeriesType;
	}

	public Integer getVatIdNumber() {
		return vatIdNumber;
	}

	public void setVatIdNumber(Integer vatIdNumber) {
		this.vatIdNumber = vatIdNumber;
	}

	public Long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(Long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Integer getVatBasedOn() {
		return vatBasedOn;
	}

	public void setVatBasedOn(Integer vatBasedOn) {
		this.vatBasedOn = vatBasedOn;
	}

	public BigDecimal getPercentageBasedOn() {
		return percentageBasedOn;
	}

	public void setPercentageBasedOn(BigDecimal percentageBasedOn) {
		this.percentageBasedOn = percentageBasedOn;
	}

	public BigDecimal getMinimumVatAmt() {
		return minimumVatAmt;
	}

	public void setMinimumVatAmt(BigDecimal minimumVatAmt) {
		this.minimumVatAmt = minimumVatAmt;
	}

	public BigDecimal getMaximumVatAmt() {
		return maximumVatAmt;
	}

	public void setMaximumVatAmt(BigDecimal maximumVatAmt) {
		this.maximumVatAmt = maximumVatAmt;
	}

	public BigDecimal getYtdTotalSales() {
		return ytdTotalSales;
	}

	public void setYtdTotalSales(BigDecimal ytdTotalSales) {
		this.ytdTotalSales = ytdTotalSales;
	}

	public BigDecimal getYtdTotalTaxableSales() {
		return ytdTotalTaxableSales;
	}

	public void setYtdTotalTaxableSales(BigDecimal ytdTotalTaxableSales) {
		this.ytdTotalTaxableSales = ytdTotalTaxableSales;
	}

	public BigDecimal getYtdTotalSalesTaxes() {
		return ytdTotalSalesTaxes;
	}

	public void setYtdTotalSalesTaxes(BigDecimal ytdTotalSalesTaxes) {
		this.ytdTotalSalesTaxes = ytdTotalSalesTaxes;
	}

	public BigDecimal getLastYearTotalSales() {
		return lastYearTotalSales;
	}

	public void setLastYearTotalSales(BigDecimal lastYearTotalSales) {
		this.lastYearTotalSales = lastYearTotalSales;
	}

	public BigDecimal getLastYearTotalTaxableSales() {
		return lastYearTotalTaxableSales;
	}

	public void setLastYearTotalTaxableSales(BigDecimal lastYearTotalTaxableSales) {
		this.lastYearTotalTaxableSales = lastYearTotalTaxableSales;
	}

	public BigDecimal getLastYearTotalSalesTaxes() {
		return lastYearTotalSalesTaxes;
	}

	public void setLastYearTotalSalesTaxes(BigDecimal lastYearTotalSalesTaxes) {
		this.lastYearTotalSalesTaxes = lastYearTotalSalesTaxes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accountTableRowIndex == null) ? 0 : accountTableRowIndex.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastYearTotalSales == null) ? 0 : lastYearTotalSales.hashCode());
		result = prime * result + ((lastYearTotalSalesTaxes == null) ? 0 : lastYearTotalSalesTaxes.hashCode());
		result = prime * result + ((lastYearTotalTaxableSales == null) ? 0 : lastYearTotalTaxableSales.hashCode());
		result = prime * result + ((maximumVatAmt == null) ? 0 : maximumVatAmt.hashCode());
		result = prime * result + ((minimumVatAmt == null) ? 0 : minimumVatAmt.hashCode());
		result = prime * result + ((percentageBasedOn == null) ? 0 : percentageBasedOn.hashCode());
		result = prime * result + ((vatBasedOn == null) ? 0 : vatBasedOn.hashCode());
		result = prime * result + ((vatDescription == null) ? 0 : vatDescription.hashCode());
		result = prime * result + ((vatDescriptionArabic == null) ? 0 : vatDescriptionArabic.hashCode());
		result = prime * result + ((vatIdNumber == null) ? 0 : vatIdNumber.hashCode());
		result = prime * result + ((vatScheduleId == null) ? 0 : vatScheduleId.hashCode());
		result = prime * result + ((vatSeriesType == null) ? 0 : vatSeriesType.hashCode());
		result = prime * result + ((ytdTotalSales == null) ? 0 : ytdTotalSales.hashCode());
		result = prime * result + ((ytdTotalSalesTaxes == null) ? 0 : ytdTotalSalesTaxes.hashCode());
		result = prime * result + ((ytdTotalTaxableSales == null) ? 0 : ytdTotalTaxableSales.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

	
	
	
}
