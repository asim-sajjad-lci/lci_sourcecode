package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceCompany;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/company")
public class ControllerCompany extends BaseController{
	
	private static final Logger LOGGER = Logger.getLogger(ControllerCompany.class);

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	@Autowired
	ServiceCompany serviceCompany;
	
	@RequestMapping(value = "/searchCompanyId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchCompanyId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search CompanyId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceCompany.searchCompanyId(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "HELTH_INSURANCE_COMPANY_GET_ALL", "HELTH_INSURANCE_COMAPNY_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search CompanyId Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
}
