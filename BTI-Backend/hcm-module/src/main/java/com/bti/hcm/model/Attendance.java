package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40200",indexes = {
        @Index(columnList = "ATDROWID")
})
public class Attendance extends HcmBaseEntity implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ATDROWID")
	private Integer id;
	
	@Column(name = "ATACCTYPE")
	private short accrueType;
	
	@Column(name = "ALLWENTATTN")
	private boolean attendance;
	
	@Column(name = "CURRYEAR")
	private Integer currentyear;
	
	@Column(name = "LSTACCRDY")
	private Date latDayAcuurate;
	
	@Column(name = "NMOFDYWEK")
	private Integer numberOfDayInWeek;
	
	@Column(name = "NMOFHRDY")
	private Integer numberOfHourInDay;
	
	@Column(name = "NXTTRXID")
	private Integer nextTranscationNum;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public short getAccrueType() {
		return accrueType;
	}

	public void setAccrueType(short accrueType) {
		this.accrueType = accrueType;
	}

	public boolean isAttendance() {
		return attendance;
	}

	public void setAttendance(boolean attendance) {
		this.attendance = attendance;
	}

	public Integer getCurrentyear() {
		return currentyear;
	}

	public void setCurrentyear(Integer currentyear) {
		this.currentyear = currentyear;
	}

	public Date getLatDayAcuurate() {
		return latDayAcuurate;
	}

	public void setLatDayAcuurate(Date latDayAcuurate) {
		this.latDayAcuurate = latDayAcuurate;
	}

	public Integer getNumberOfDayInWeek() {
		return numberOfDayInWeek;
	}

	public void setNumberOfDayInWeek(Integer numberOfDayInWeek) {
		this.numberOfDayInWeek = numberOfDayInWeek;
	}

	public Integer getNumberOfHourInDay() {
		return numberOfHourInDay;
	}

	public void setNumberOfHourInDay(Integer numberOfHourInDay) {
		this.numberOfHourInDay = numberOfHourInDay;
	}

	public Integer getNextTranscationNum() {
		return nextTranscationNum;
	}

	public void setNextTranscationNum(Integer nextTranscationNum) {
		this.nextTranscationNum = nextTranscationNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + accrueType;
		result = prime * result + (attendance ? 1231 : 1237);
		result = prime * result + ((currentyear == null) ? 0 : currentyear.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((latDayAcuurate == null) ? 0 : latDayAcuurate.hashCode());
		result = prime * result + ((nextTranscationNum == null) ? 0 : nextTranscationNum.hashCode());
		result = prime * result + ((numberOfDayInWeek == null) ? 0 : numberOfDayInWeek.hashCode());
		result = prime * result + ((numberOfHourInDay == null) ? 0 : numberOfHourInDay.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}
	
	
}
