/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the InterviewTypeSetupDetail database table.
 * Name of Project: Hcm 
 * Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40401",indexes = {
        @Index(columnList = "INTRVINDXD")
})
@NamedQuery(name = "InterviewTypeSetupDetail.findAll", query = "SELECT i FROM InterviewTypeSetupDetail i")
public class InterviewTypeSetupDetail extends HcmBaseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INTRVINDXD")
	private int id;

	
	@ManyToOne
	@JoinColumn(name="INTRVINDX")
	private InterviewTypeSetup interviewTypeSetup;
	
	@Column(name = "INTRCATSEQ")
	private int interviewTypeSetupDetailCategoryRowSequance;
	
	@Column(name = "INTRCACTDSC",columnDefinition="char(150)")
	private String interviewTypeSetupDetailDescription;

	@Column(name = "INTRCACTSQ")
	private int interviewTypeSetupDetailCategorySequance;
	
	@Column(name = "INTRCACTWEGT")
	private int interviewTypeSetupDetailCategoryWeight;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public InterviewTypeSetup getInterviewTypeSetup() {
		return interviewTypeSetup;
	}

	public void setInterviewTypeSetup(InterviewTypeSetup interviewTypeSetup) {
		this.interviewTypeSetup = interviewTypeSetup;
	}

	public int getInterviewTypeSetupDetailCategoryRowSequance() {
		return interviewTypeSetupDetailCategoryRowSequance;
	}

	public void setInterviewTypeSetupDetailCategoryRowSequance(int interviewTypeSetupDetailCategoryRowSequance) {
		this.interviewTypeSetupDetailCategoryRowSequance = interviewTypeSetupDetailCategoryRowSequance;
	}

	public String getInterviewTypeSetupDetailDescription() {
		return interviewTypeSetupDetailDescription;
	}

	public void setInterviewTypeSetupDetailDescription(String interviewTypeSetupDetailDescription) {
		this.interviewTypeSetupDetailDescription = interviewTypeSetupDetailDescription;
	}

	public int getInterviewTypeSetupDetailCategorySequance() {
		return interviewTypeSetupDetailCategorySequance;
	}

	public void setInterviewTypeSetupDetailCategorySequance(int interviewTypeSetupDetailCategorySequance) {
		this.interviewTypeSetupDetailCategorySequance = interviewTypeSetupDetailCategorySequance;
	}

	public int getInterviewTypeSetupDetailCategoryWeight() {
		return interviewTypeSetupDetailCategoryWeight;
	}

	public void setInterviewTypeSetupDetailCategoryWeight(int interviewTypeSetupDetailCategoryWeight) {
		this.interviewTypeSetupDetailCategoryWeight = interviewTypeSetupDetailCategoryWeight;
	}
	
	
	
}
