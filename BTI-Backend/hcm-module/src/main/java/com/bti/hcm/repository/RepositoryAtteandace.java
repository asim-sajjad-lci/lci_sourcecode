package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Attendance;

@Repository("repositoryAtteandace")
public interface RepositoryAtteandace extends JpaRepository<Attendance, Integer>{

	Attendance findByIdAndIsDeleted(Integer id, boolean b);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Attendance d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleAtteandace(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);
	
	@Query("select count(*) from Attendance s where s.isDeleted=false")
	Integer getCountOfTotalAtteandace();

	List<Attendance> findByIsDeleted(boolean b, Pageable pageable);

	List<Attendance> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	@Query("select count(*) from Attendance a ")
	public Integer getCountOfTotalAtteandaces();

}
