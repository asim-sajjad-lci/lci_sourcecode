package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10400", indexes = { @Index(columnList = "HCMPYMTINDX") })
public class ManualChecks extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMPYMTINDX")
	private Integer id;

	@Column(name = "HCMPYDSCR", columnDefinition = "char(60)")
	private String description;

	@Column(name = "HCMMLTYP")
	private Short checkType;

	@Column(name = "CHEKNMBR")
	private Integer checkNumber;

	@Column(name = "CHEKDTA")
	private Date checkDate;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "manualChecks")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksDistribution> listManualChecksDistribution;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "manualChecks")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksDetails> listmanualChecksDetails;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="manualChecks")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksHistoryYearDetails>listManualChecksHistoryYearDetails;

	@Column(name = "CHEKPSTDT")
	private Date postDate;

	@Column(name = "TOTLAMT", precision = 10, scale = 3)
	private BigDecimal grossAmount;

	@Column(name = "TOTLNETAMT", precision = 10, scale = 3)
	private BigDecimal netAmount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Short getCheckType() {
		return checkType;
	}

	public void setCheckType(Short checkType) {
		this.checkType = checkType;
	}

	public Integer getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public BigDecimal getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public List<ManualChecksDistribution> getListManualChecksDistribution() {
		return listManualChecksDistribution;
	}

	public void setListManualChecksDistribution(List<ManualChecksDistribution> listManualChecksDistribution) {
		this.listManualChecksDistribution = listManualChecksDistribution;
	}

	public List<ManualChecksDetails> getListmanualChecksDetails() {
		return listmanualChecksDetails;
	}

	public void setListmanualChecksDetails(List<ManualChecksDetails> listmanualChecksDetails) {
		this.listmanualChecksDetails = listmanualChecksDetails;
	}

	public List<ManualChecksHistoryYearDetails> getListManualChecksHistoryYearDetails() {
		return listManualChecksHistoryYearDetails;
	}

	public void setListManualChecksHistoryYearDetails(
			List<ManualChecksHistoryYearDetails> listManualChecksHistoryYearDetails) {
		this.listManualChecksHistoryYearDetails = listManualChecksHistoryYearDetails;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((checkDate == null) ? 0 : checkDate.hashCode());
		result = prime * result + ((checkNumber == null) ? 0 : checkNumber.hashCode());
		result = prime * result + ((checkType == null) ? 0 : checkType.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((grossAmount == null) ? 0 : grossAmount.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((netAmount == null) ? 0 : netAmount.hashCode());
		result = prime * result + ((postDate == null) ? 0 : postDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
