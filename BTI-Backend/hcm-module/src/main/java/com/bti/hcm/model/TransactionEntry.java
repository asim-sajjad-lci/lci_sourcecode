package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10100",indexes = {
        @Index(columnList = "HCMTRXINX")
})
public class TransactionEntry extends HcmBaseEntity implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMTRXINX")
	private Integer id;
	
	@Column(name = "HCMTRXID")
	private Integer entryNumber;
	
	@Column(name = "HCMTRXDT")
	private Date entryDate;
	
	@Column(name = "HCMTRXDSCR")
	private String description;
	
	@Column(name = "HCMTRXDSCRA")
	private String arabicDescription;
	
	@ManyToOne
	@JoinColumn(name = "HCMBATINDX")
	private Batches batches;
	
	@Column(name = "HCMTRXFDT")
	private Date fromDate;
	
	@Column(name = "HCMTRXTDT")
	private Date toDate;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "transactionEntry")
	@Where(clause = "is_deleted = false")
	private List<Distribution> listDistribution;

	@Column(name = "HCMTRXFROMBUILDs")
	private Boolean fromBuild;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "transactionEntry")
	@Where(clause = "is_deleted = false")
	private List<TransactionEntryDetail> transDetailList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEntryNumber() {
		return entryNumber;
	}

	public void setEntryNumber(Integer entryNumber) {
		this.entryNumber = entryNumber;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public Batches getBatches() {
		return batches;
	}

	public void setBatches(Batches batches) {
		this.batches = batches;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<Distribution> getListDistribution() {
		return listDistribution;
	}

	public void setListDistribution(List<Distribution> listDistribution) {
		this.listDistribution = listDistribution;
	}

	public Boolean getFromBuild() {
		return fromBuild;
	}

	public void setFromBuild(Boolean fromBuild) {
		this.fromBuild = fromBuild;
	}

	public List<TransactionEntryDetail> getTransDetailList() {
		return transDetailList;
	}

	public void setTransDetailList(List<TransactionEntryDetail> transDetailList) {
		this.transDetailList = transDetailList;
	}
	
	
	
	
}
