package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40800",indexes = {
        @Index(columnList = "TRNCINDX")
})
public class TraningCourse extends HcmBaseEntity implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRNCINDX")
	private Integer id;
	
	/*@OneToMany(mappedBy = "trainingCoursedetail")
	@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<TrainingBatchSignup> listTrainingBatchSignup;*/
	
	@Column(name = "TRNCID")
	private String traningId;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "trainingCourse")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<TrainingBatchSignup> trainingBatchSignupList;
	
	@Column(name = "TRNCNME")
	private String desc;
	
	@Column(name = "TRNCNMEA")
	private String arbicDesc;
	
	@Column(name = "TRNCLOCN")
	private String location;
	
	@Column(name = "TRNCPREQ")
	private String prerequisiteId;
	
	public List<TrainingCourseDetail> getListTrainingCourseDetail() {
		return listTrainingCourseDetail;
	}

	public void setListTrainingCourseDetail(List<TrainingCourseDetail> listTrainingCourseDetail) {
		this.listTrainingCourseDetail = listTrainingCourseDetail;
	}

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "trainingCourse")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<TrainingCourseDetail> listTrainingCourseDetail;
	
	@Column(name = "TRNCINCO")
	private boolean courseInCompany;
	
	@Column(name = "TRNCEMPAMT",precision = 10,scale = 3)
	private BigDecimal employeeCost;
	
	@Column(name = "TRNCEMAMT",precision = 10,scale = 3)
	private BigDecimal employerCost;
	
	@Column(name = "TRNCSUPLAMT",precision = 10,scale = 3)
	private BigDecimal supplierCost;
	
	@Column(name = "TRNCINSRAMT",precision = 10,scale = 3)
	private BigDecimal instructorCost;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTraningId() {
		return traningId;
	}

	public void setTraningId(String traningId) {
		this.traningId = traningId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPrerequisiteId() {
		return prerequisiteId;
	}

	public void setPrerequisiteId(String prerequisiteId) {
		this.prerequisiteId = prerequisiteId;
	}

	public boolean isCourseInCompany() {
		return courseInCompany;
	}

	public void setCourseInCompany(boolean courseInCompany) {
		this.courseInCompany = courseInCompany;
	}

	public BigDecimal getEmployeeCost() {
		return employeeCost;
	}

	public void setEmployeeCost(BigDecimal employeeCost) {
		this.employeeCost = employeeCost;
	}

	public BigDecimal getEmployerCost() {
		return employerCost;
	}

	public void setEmployerCost(BigDecimal employerCost) {
		this.employerCost = employerCost;
	}

	public BigDecimal getSupplierCost() {
		return supplierCost;
	}

	public void setSupplierCost(BigDecimal supplierCost) {
		this.supplierCost = supplierCost;
	}

	public BigDecimal getInstructorCost() {
		return instructorCost;
	}

	public void setInstructorCost(BigDecimal instructorCost) {
		this.instructorCost = instructorCost;
	}

	public List<TrainingBatchSignup> getTrainingBatchSignupList() {
		return trainingBatchSignupList;
	}

	public void setTrainingBatchSignupList(List<TrainingBatchSignup> trainingBatchSignupList) {
		this.trainingBatchSignupList = trainingBatchSignupList;
	}

	/*public List<TrainingBatchSignup> getListTrainingBatchSignup() {
		return listTrainingBatchSignup;
	}

	public void setListTrainingBatchSignup(List<TrainingBatchSignup> listTrainingBatchSignup) {
		this.listTrainingBatchSignup = listTrainingBatchSignup;
	}*/

	
	
	
	
}
