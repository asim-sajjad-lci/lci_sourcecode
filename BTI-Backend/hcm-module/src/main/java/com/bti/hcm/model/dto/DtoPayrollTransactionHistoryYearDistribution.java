package com.bti.hcm.model.dto;

import java.math.BigDecimal;

public class DtoPayrollTransactionHistoryYearDistribution extends DtoBase {
	private Integer id;
	private Integer year;
	private String auditTransactionNumber;
	private Integer employeeId;
	private Long accountTableRowIndex;
	private Short postingTypes;
	private Integer codeIndexId;
	private BigDecimal debitAmount;
	private BigDecimal creditAmount;
	private Integer accountSequence;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getAuditTransactionNumber() {
		return auditTransactionNumber;
	}

	public void setAuditTransactionNumber(String auditTransactionNumber) {
		this.auditTransactionNumber = auditTransactionNumber;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(Long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Short getPostingTypes() {
		return postingTypes;
	}

	public void setPostingTypes(Short postingTypes) {
		this.postingTypes = postingTypes;
	}

	public Integer getCodeIndexId() {
		return codeIndexId;
	}

	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Integer getAccountSequence() {
		return accountSequence;
	}

	public void setAccountSequence(Integer accountSequence) {
		this.accountSequence = accountSequence;
	}

}
