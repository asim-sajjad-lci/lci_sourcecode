package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.OrientationCheckListSetup;
import com.bti.hcm.model.OrientationPredefinedCheckListItem;
import com.bti.hcm.model.OrientationSetup;
import com.bti.hcm.model.OrientationSetupDetail;
import com.bti.hcm.model.dto.DtoOrientationSetup;
import com.bti.hcm.model.dto.DtoOrientationSetupDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryOrientationCheckListSetup;
import com.bti.hcm.repository.RepositoryOrientationPredefinedCheckListItem;
import com.bti.hcm.repository.RepositoryOrientationSetup;
import com.bti.hcm.repository.RepositoryOrientationSetupDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceOrientationSetup")
public class ServiceOrientationSetup {
	
static Logger log = Logger.getLogger(ServiceOrientationSetup.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryOrientationSetup repositoryOrientationSetup;
	
	@Autowired
	RepositoryOrientationPredefinedCheckListItem repositoryOrientationPredefinedCheckListItem;
	
	
	@Autowired
	RepositoryOrientationCheckListSetup repositoryOrientationCheckListSetup;
	
	@Autowired
	RepositoryOrientationSetupDetail repositoryOrientationSetupDetail;
	
	
	public DtoOrientationSetup saveOrUpdateOrientationSetup(DtoOrientationSetup dtoOrientationSetup) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			OrientationSetup orientationSetup=null;
			if (dtoOrientationSetup.getId() != null && dtoOrientationSetup.getId() > 0) {
				orientationSetup = repositoryOrientationSetup.findByIdAndIsDeleted(dtoOrientationSetup.getId(), false);
				orientationSetup.setUpdatedBy(loggedInUserId);
				orientationSetup.setUpdatedDate(new Date());
			} else {
				orientationSetup = new OrientationSetup();
				orientationSetup.setCreatedDate(new Date());
				
				Integer rowId = repositoryOrientationSetup.getCountOfTotaOrientationSetup();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				orientationSetup.setRowId(increment);
			}
			
			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem=null;
			if(dtoOrientationSetup.getOrientationPredefinedCheckListItemId()!=null &&dtoOrientationSetup.getOrientationPredefinedCheckListItemId()>0) {
				orientationPredefinedCheckListItem=repositoryOrientationPredefinedCheckListItem.findOne(dtoOrientationSetup.getOrientationPredefinedCheckListItemId());
			}
			OrientationCheckListSetup orientationCheckListSetup=null;
			if(dtoOrientationSetup.getOrientationCheckListSetupId()!=null&& dtoOrientationSetup.getOrientationCheckListSetupId()>0) {
				orientationCheckListSetup=repositoryOrientationCheckListSetup.findOne(dtoOrientationSetup.getOrientationCheckListSetupId());
			}
			orientationSetup.setOrientationCheckListSetup(orientationCheckListSetup);
			orientationSetup.setOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
			orientationSetup.setOrientationId(dtoOrientationSetup.getOrientationId());
			orientationSetup.setOrientationDesc(dtoOrientationSetup.getOrientationDesc());
			orientationSetup.setOrientationArbicDesc(dtoOrientationSetup.getOrientationArbicDesc());
			orientationSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			orientationSetup = repositoryOrientationSetup.saveAndFlush(orientationSetup);
			if(dtoOrientationSetup.getSubItems()!=null && !dtoOrientationSetup.getSubItems().isEmpty()) {
				for(DtoOrientationSetupDetail dtoOrientationSetupDetail : dtoOrientationSetup.getSubItems()) {
					
					OrientationSetupDetail orientationSetupDetail=null;
					if(dtoOrientationSetupDetail.getId()!=null && dtoOrientationSetupDetail.getId()>0) {
						orientationSetupDetail   = repositoryOrientationSetupDetail.findOne(dtoOrientationSetupDetail.getId());
					}else {
						orientationSetupDetail = new OrientationSetupDetail();
					}
					orientationSetupDetail.setOrientationSetups(orientationSetup);
					orientationSetupDetail.setSequence(dtoOrientationSetupDetail.getSequence());
					orientationSetupDetail.setStartDate(dtoOrientationSetupDetail.getStartdate());
					orientationSetupDetail.setEnddate(dtoOrientationSetupDetail.getEnddate());
					orientationSetupDetail.setStarttime(dtoOrientationSetupDetail.getStarttime());
					orientationSetupDetail.setEndtime(dtoOrientationSetupDetail.getEndtime());
					orientationSetupDetail.setChecklistitem(dtoOrientationSetupDetail.getChecklistitem());
					orientationSetupDetail.setArabicDescription(dtoOrientationSetupDetail.getArabicDescription());
					orientationSetupDetail.setPersonResponsible(dtoOrientationSetupDetail.getPersonResponsible());
					repositoryOrientationSetupDetail.saveAndFlush(orientationSetupDetail);
				}
			}
			else {
				for(DtoOrientationSetupDetail  dtoOrientationSetupDetails:dtoOrientationSetup.getSubItems()) {
					OrientationSetupDetail orientationSetupDetailUpdate=repositoryOrientationSetupDetail.findOne(dtoOrientationSetupDetails.getId());
					
					orientationSetupDetailUpdate.setSequence(dtoOrientationSetupDetails.getSequence());
					orientationSetupDetailUpdate.setStartDate(dtoOrientationSetupDetails.getStartdate());
					orientationSetupDetailUpdate.setEnddate(dtoOrientationSetupDetails.getEnddate());
					orientationSetupDetailUpdate.setStarttime(dtoOrientationSetupDetails.getStarttime());
					orientationSetupDetailUpdate.setEndtime(dtoOrientationSetupDetails.getEndtime());
					orientationSetupDetailUpdate.setChecklistitem(dtoOrientationSetupDetails.getChecklistitem());
					orientationSetupDetailUpdate.setArabicDescription(dtoOrientationSetupDetails.getArabicDescription());
					orientationSetupDetailUpdate.setPersonResponsible(dtoOrientationSetupDetails.getPersonResponsible());
					repositoryOrientationSetupDetail.saveAndFlush(orientationSetupDetailUpdate);
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoOrientationSetup;
	}
	public DtoOrientationSetup deleteOrientationSetup(List<Integer> ids) {
		log.info("delete OrientationSetup Method");
		DtoOrientationSetup dtoOrientationSetup = new DtoOrientationSetup();
		dtoOrientationSetup.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ORIENTATIONSETUP_DELETED", false));
		dtoOrientationSetup.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ORIENTATIONSETUP_ASSOCIATED", false));
		List<DtoOrientationSetup> deleteDtoOrientationSetup = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer id : ids) {
				OrientationSetup orientationSetup = repositoryOrientationSetup.findOne(id);
				if(orientationSetup.getListOrientationSetupDetail().isEmpty()) {
					DtoOrientationSetup dtoOrientationSetup2 = new DtoOrientationSetup();
					dtoOrientationSetup2.setId(orientationSetup.getId());
					dtoOrientationSetup2.setOrientationDesc(orientationSetup.getOrientationDesc());
					dtoOrientationSetup2.setOrientationArbicDesc(orientationSetup.getOrientationArbicDesc());
					repositoryOrientationSetup.deleteSingleOrientationSetup(true, loggedInUserId,id);
					deleteDtoOrientationSetup.add(dtoOrientationSetup2);
				}
				
			}
			dtoOrientationSetup.setDelete(deleteDtoOrientationSetup);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Location :"+dtoOrientationSetup.getId());
		return dtoOrientationSetup;
	}
	
	
	public DtoOrientationSetup getOrientationSetupById(int id) {
		DtoOrientationSetup dtoOrientationSetup  = new DtoOrientationSetup();
		try {
			if (id > 0) {
				OrientationSetup orientationSetup = repositoryOrientationSetup.findByIdAndIsDeleted(id, false);
				if (orientationSetup != null) {
					dtoOrientationSetup = new DtoOrientationSetup(orientationSetup);
				} else {
					dtoOrientationSetup.setMessageType("ORIENTATIONSETUP_NOT_GETTING");

				}
			} else {
				dtoOrientationSetup.setMessageType("INVALID_ORIENTATION_ID");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoOrientationSetup;
	}
	public DtoOrientationSetup repeatByorientationId(String orientationId) {
		log.info("repeatByorientationId Method");
		DtoOrientationSetup dtoOrientationSetup = new DtoOrientationSetup();
		try {
			List<OrientationSetup> orientationSetup=repositoryOrientationSetup.repeatByorientationId(orientationId);
			if(orientationSetup!=null && !orientationSetup.isEmpty()) {
				dtoOrientationSetup.setIsRepeat(true);
			}else {
				dtoOrientationSetup.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoOrientationSetup;
	}


	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchOrientationSetup(DtoSearch dtoSearch) {
		try {
			log.info("search OrientationSetup Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						if(dtoSearch.getSortOn().equals("orientationId") || dtoSearch.getSortOn().equals("orientationDesc")|| dtoSearch.getSortOn().equals("orientationArbicDesc")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
				dtoSearch.setTotalCount(this.repositoryOrientationSetup.predictiveOrientationSetupSearchTotalCount("%"+searchWord+"%"));
				List<OrientationSetup> orientationSetupList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						orientationSetupList = this.repositoryOrientationSetup.predictiveOrientationSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						orientationSetupList = this.repositoryOrientationSetup.predictiveOrientationSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						orientationSetupList = this.repositoryOrientationSetup.predictiveOrientationSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}
				if(orientationSetupList != null && !orientationSetupList.isEmpty()){
					List<DtoOrientationSetup> dtoOrientationSetupList = new ArrayList<>();
					DtoOrientationSetup dtoOrientationSetup=null;
					for (OrientationSetup orientationSetup : orientationSetupList) {
						dtoOrientationSetup = new DtoOrientationSetup(orientationSetup);
						List<OrientationSetupDetail> list=orientationSetup.getListOrientationSetupDetail();
						dtoOrientationSetup.setId(orientationSetup.getId());
						dtoOrientationSetup.setOrientationId(orientationSetup.getOrientationId());
						dtoOrientationSetup.setOrientationDesc(orientationSetup.getOrientationDesc());
						dtoOrientationSetup.setOrientationArbicDesc(orientationSetup.getOrientationArbicDesc());
						dtoOrientationSetup.setOrientationPredefinedCheckListItemId(orientationSetup.getOrientationPredefinedCheckListItem().getId());
						List<DtoOrientationSetupDetail> details = new ArrayList<>(); 
						for (OrientationSetupDetail orientationSetupDetail : orientationSetup.getListOrientationSetupDetail()) {
							DtoOrientationSetupDetail dtoOrientationSetupDetail = new DtoOrientationSetupDetail();
							dtoOrientationSetupDetail.setId(orientationSetupDetail.getId());
							dtoOrientationSetupDetail.setSequence(orientationSetupDetail.getSequence());
							dtoOrientationSetupDetail.setStartdate(orientationSetupDetail.getStartDate());
							dtoOrientationSetupDetail.setEnddate(orientationSetupDetail.getEnddate());
							dtoOrientationSetupDetail.setStarttime(orientationSetupDetail.getStarttime());
							dtoOrientationSetupDetail.setEndtime(orientationSetupDetail.getEndtime());
							dtoOrientationSetupDetail.setChecklistitem(orientationSetupDetail.getChecklistitem());
							dtoOrientationSetupDetail.setArabicDescription(orientationSetupDetail.getArabicDescription());
							dtoOrientationSetupDetail.setPersonResponsible(orientationSetupDetail.getPersonResponsible());
							details.add(dtoOrientationSetupDetail);
							for(OrientationSetupDetail orientationSetupDetail2:list) {
								if(dtoOrientationSetup.getSubItems()==null) {
									List<DtoOrientationSetupDetail> listDtoOrientationSetupDetail = new ArrayList<>();
									dtoOrientationSetup.setSubItems(listDtoOrientationSetupDetail);
								}
								if(orientationSetupDetail2.getIsDeleted()==false) {
									dtoOrientationSetupDetail.setId(orientationSetupDetail.getId());
									dtoOrientationSetupDetail.setSequence(orientationSetupDetail.getSequence());
									dtoOrientationSetupDetail.setStartdate(orientationSetupDetail.getStartDate());
									dtoOrientationSetupDetail.setEnddate(orientationSetupDetail.getEnddate());
									dtoOrientationSetupDetail.setStarttime(orientationSetupDetail.getStarttime());
									dtoOrientationSetupDetail.setEndtime(orientationSetupDetail.getEndtime());
									dtoOrientationSetupDetail.setChecklistitem(orientationSetupDetail.getChecklistitem());
									dtoOrientationSetupDetail.setArabicDescription(orientationSetupDetail.getArabicDescription());
									dtoOrientationSetupDetail.setPersonResponsible(orientationSetupDetail.getPersonResponsible());
									dtoOrientationSetup.getSubItems().add(dtoOrientationSetupDetail);
								}
							}	
						}
						dtoOrientationSetup.setSubItems(details);
						dtoOrientationSetupList.add(dtoOrientationSetup);
						}
					dtoSearch.setRecords(dtoOrientationSetupList);
					}
					
				}
			
			log.debug("Search OrientationSetup Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
		}
	public DtoSearch searchOrientationId(DtoSearch dtoSearch) {
		try {
			log.info("searchOrientationId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				List<String> orientationIdList =new ArrayList<>();
					orientationIdList = this.repositoryOrientationSetup.predictiveSearchOrientationIdSearchWithPagination("%"+searchWord+"%");
					if(!orientationIdList.isEmpty()) {
						dtoSearch.setTotalCount(orientationIdList.size());
						dtoSearch.setRecords(orientationIdList.size());
					}
				dtoSearch.setIds(orientationIdList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
}
