package com.bti.hcm.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40109",indexes = {
        @Index(columnList = "PROINDEX")
})
@NamedQuery(name = "ProjectSetup.findAll", query = "SELECT p FROM ProjectSetup p")
public class ProjectSetup extends HcmBaseEntity implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PROINDEX")
	private int id;
	
	@Column(name = "PROID", columnDefinition = "char(15)")
	private String projectId;
	
	@Column(name = "PRONAME", columnDefinition = "char(61)")
	private String projectName;
	
	
	@Column(name = "PROARNAME", columnDefinition = "char(61)")
	private String projectArabicName;
	
	@Column(name = "PRODESC", columnDefinition = "char(120)")
	private String projectDescription;
	
	@Column(name = "PROARDESC", columnDefinition = "char(120)")
	private String projectArabicDescription;
	
	@OneToMany
//	@JoinColumn(name = "EMPLOYINDX")
	private List<EmployeeMaster> employeeMaster;

	@Column(name = "STARTDATE")
	private Date startDate;
	

	@Column(name = "ENDDATE")
	private Date endDate;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getProjectId() {
		return projectId;
	}


	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public String getProjectArabicName() {
		return projectArabicName;
	}


	public void setProjectArabicName(String projectArabicName) {
		this.projectArabicName = projectArabicName;
	}


	public String getProjectDescription() {
		return projectDescription;
	}


	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}


	public String getProjectArabicDescription() {
		return projectArabicDescription;
	}


	public void setProjectArabicDescription(String projectArabicDescription) {
		this.projectArabicDescription = projectArabicDescription;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}


	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	
	
	
	
}
