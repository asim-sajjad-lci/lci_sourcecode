package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoBenefitCode;
import com.bti.hcm.model.dto.DtoEmployeeBenefitMaintenance;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeMasterHcm;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("/serviceEmployeeBenefitMaintenance ")
public class ServiceEmployeeBenefitMaintenance {

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletResponse method in
	 *              ServiceRetirementFundSetup service
	 */

	static Logger log = Logger.getLogger(ServiceEmployeeBenefitMaintenance.class.getName());

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in
	 *              ServiceRetirementFundSetup service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * /**
	 * 
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletResponse method in
	 *              ServiceRetirementFundSetup service
	 */

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;

	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;

	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;

	@Autowired(required = false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;

	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	public DtoEmployeeBenefitMaintenance saveOrUpdate(DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			EmployeeBenefitMaintenance employeeBenefitMaintenance = null;
			if (dtoEmployeeBenefitMaintenance.getId() != null && dtoEmployeeBenefitMaintenance.getId() > 0) {
				employeeBenefitMaintenance = repositoryEmployeeBenefitMaintenance
						.findByIdAndIsDeleted(dtoEmployeeBenefitMaintenance.getId(), false);
				employeeBenefitMaintenance.setUpdatedBy(loggedInUserId);
				employeeBenefitMaintenance.setUpdatedDate(new Date());
			} else {
				employeeBenefitMaintenance = new EmployeeBenefitMaintenance();
				employeeBenefitMaintenance.setCreatedDate(new Date());
				Integer rowId = repositoryEmployeeBenefitMaintenance.getCountOfTotalEmployeeBenefitMaintenance();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				employeeBenefitMaintenance.setRowId(increment);
			}

			EmployeeMaster employeeMaster = null;
			if (dtoEmployeeBenefitMaintenance.getEmployeeMaster() != null
					&& dtoEmployeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId() > 0) {
				employeeMaster = repositoryEmployeeMaster
						.findOne(dtoEmployeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId());
			}
			BenefitCode benefitCode = new BenefitCode();
			if (dtoEmployeeBenefitMaintenance.getBenefitCode() != null
					&& dtoEmployeeBenefitMaintenance.getBenefitCode().getId() > 0) {
				benefitCode = repositoryBenefitCode.findOne(dtoEmployeeBenefitMaintenance.getBenefitCode().getId());
			}
			employeeBenefitMaintenance.setBenefitCode(benefitCode);
			employeeBenefitMaintenance.setEmployeeMaster(employeeMaster);
			employeeBenefitMaintenance.setStartDate(dtoEmployeeBenefitMaintenance.getStartDate());
			if (benefitCode.getNoOfDays() != null && benefitCode.getEndDateDays() != null
					&& employeeBenefitMaintenance != null && employeeMaster != null) {
				employeeBenefitMaintenance.setStartDate(
						UtilDateAndTimeHr.startDate(benefitCode.getNoOfDays(), employeeMaster.getEmployeeHireDate()));
				employeeBenefitMaintenance.setEndDate(UtilDateAndTimeHr.endDate(benefitCode.getNoOfDays(),
						benefitCode.getEndDateDays(), employeeMaster.getEmployeeHireDate()));
			} else {
				employeeBenefitMaintenance.setStartDate(dtoEmployeeBenefitMaintenance.getStartDate());
				employeeBenefitMaintenance.setEndDate(dtoEmployeeBenefitMaintenance.getEndDate());
			}
			employeeBenefitMaintenance.setPayFactor(dtoEmployeeBenefitMaintenance.getPayFactor());
			employeeBenefitMaintenance.setTransactionRequired(dtoEmployeeBenefitMaintenance.getTransactionRequired());
			employeeBenefitMaintenance.setBenefitMethod(dtoEmployeeBenefitMaintenance.getBenefitMethod());
			employeeBenefitMaintenance.setBenefitAmount(dtoEmployeeBenefitMaintenance.getBenefitAmount());
			employeeBenefitMaintenance.setBenefitPercent(dtoEmployeeBenefitMaintenance.getBenefitPercent());
			employeeBenefitMaintenance.setPerPeriord(dtoEmployeeBenefitMaintenance.getPerPeriord());
			employeeBenefitMaintenance.setEndDateDays(dtoEmployeeBenefitMaintenance.getEndDateDays());
			employeeBenefitMaintenance.setNoOfDays(dtoEmployeeBenefitMaintenance.getNoOfDays());
			employeeBenefitMaintenance.setPerYear(dtoEmployeeBenefitMaintenance.getPerYear());
			employeeBenefitMaintenance.setLifeTime(dtoEmployeeBenefitMaintenance.getLifeTime());
			employeeBenefitMaintenance.setFrequency(dtoEmployeeBenefitMaintenance.getFrequency());
			employeeBenefitMaintenance.setInactive(dtoEmployeeBenefitMaintenance.getInactive());

			employeeBenefitMaintenance.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryEmployeeBenefitMaintenance.saveAndFlush(employeeBenefitMaintenance);
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeBenefitMaintenance;
	}

	public DtoEmployeeBenefitMaintenance delete(List<Integer> ids) {
		log.info("delete EmployeeBenefitMaintenance Method");
		DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance = new DtoEmployeeBenefitMaintenance();
		dtoEmployeeBenefitMaintenance.setDeleteMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTANENANCE_DELETED", false));
		dtoEmployeeBenefitMaintenance.setAssociateMessage(serviceResponse
				.getStringMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTANENANCE_ASSOCIATED", false));
		List<DtoEmployeeBenefitMaintenance> deletedtoEmployeeBenefitMaintenance = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		List<TransactionEntryDetail> transactionEntryDetailList = new ArrayList<>();
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse
				.getMessageByShortAndIsDeleted("EMPLOYEE_BENEFIT_MAINTANENANCE_NOT_DELETE_ID_MESSAGE", false)
				.getMessage());
		try {
			for (Integer planId : ids) {
				EmployeeBenefitMaintenance employeeBenefitMaintenance = repositoryEmployeeBenefitMaintenance
						.findOne(planId);
				if (employeeBenefitMaintenance != null) {
					// if(employeeBenefitMaintenance.getBenefitCode().getListBuildPayrollCheckByBenefits().isEmpty())
					// {
					transactionEntryDetailList = repositoryTransactionEntryDetail.findByTransactionEntry3(
							employeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId(),
							employeeBenefitMaintenance.getBenefitCode().getId());
					if (transactionEntryDetailList.isEmpty()) {
						DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance2 = new DtoEmployeeBenefitMaintenance();
						dtoEmployeeBenefitMaintenance.setId(employeeBenefitMaintenance.getId());

						repositoryEmployeeBenefitMaintenance.deleteSingleEmployeeBenefitMaintenance(true,
								loggedInUserId, planId);
						deletedtoEmployeeBenefitMaintenance.add(dtoEmployeeBenefitMaintenance2);

					} else {
						inValidDelete = true;
					}

					/*
					 * }else { inValidDelete = true; }
					 */

				} else {
					inValidDelete = true;
				}

			}

			if (inValidDelete) {
				invlidDeleteMessage.replace(invlidDeleteMessage.length() - 1, invlidDeleteMessage.length(), "");
				dtoEmployeeBenefitMaintenance.setMessageType(invlidDeleteMessage.toString());

			}
			if (!inValidDelete) {
				dtoEmployeeBenefitMaintenance.setMessageType("");

			}

			dtoEmployeeBenefitMaintenance.setDelete(deletedtoEmployeeBenefitMaintenance);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeePayCodeMaintenance" + " :" + dtoEmployeeBenefitMaintenance.getId());
		return dtoEmployeeBenefitMaintenance;
	}

	public DtoEmployeeBenefitMaintenance getById(int id) {
		DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance = new DtoEmployeeBenefitMaintenance();
		try {
			if (id > 0) {
				EmployeeBenefitMaintenance employeeBenefitMaintenance = repositoryEmployeeBenefitMaintenance
						.findByIdAndIsDeleted(id, false);
				if (employeeBenefitMaintenance != null) {
					dtoEmployeeBenefitMaintenance = new DtoEmployeeBenefitMaintenance(employeeBenefitMaintenance);
					dtoEmployeeBenefitMaintenance.setId(employeeBenefitMaintenance.getId());
					dtoEmployeeBenefitMaintenance.setStartDate(employeeBenefitMaintenance.getStartDate());
					dtoEmployeeBenefitMaintenance.setEndDate(employeeBenefitMaintenance.getEndDate());
					dtoEmployeeBenefitMaintenance
							.setTransactionRequired(dtoEmployeeBenefitMaintenance.getTransactionRequired());
					dtoEmployeeBenefitMaintenance.setBenefitMethod(employeeBenefitMaintenance.getBenefitMethod());
					dtoEmployeeBenefitMaintenance.setBenefitAmount(employeeBenefitMaintenance.getBenefitAmount());
					dtoEmployeeBenefitMaintenance.setPerPeriord(employeeBenefitMaintenance.getPerPeriord());
					dtoEmployeeBenefitMaintenance.setPerYear(employeeBenefitMaintenance.getPerYear());
					dtoEmployeeBenefitMaintenance.setPayFactor(employeeBenefitMaintenance.getPayFactor());
					dtoEmployeeBenefitMaintenance.setLifeTime(employeeBenefitMaintenance.getLifeTime());
					dtoEmployeeBenefitMaintenance.setEndDateDays(employeeBenefitMaintenance.getEndDateDays());
					dtoEmployeeBenefitMaintenance.setNoOfDays(employeeBenefitMaintenance.getNoOfDays());
					dtoEmployeeBenefitMaintenance.setFrequency(employeeBenefitMaintenance.getFrequency());
					dtoEmployeeBenefitMaintenance.setInactive(employeeBenefitMaintenance.getInactive());

					List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
							.findByEmployeeId(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId());
					if (employeeBenefitMaintenance.getBenefitCode().getPayCodeList() != null
							&& !employeeBenefitMaintenance.getBenefitCode().getPayCodeList().isEmpty()) {
						List<Integer> listId = new ArrayList<>();
						for (PayCode payCode : employeeBenefitMaintenance.getBenefitCode().getPayCodeList()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance1 : employeePayCodeMaintenance) {
								if (payCode.getId() == employeePayCodeMaintenance1.getPayCode().getId()) {
									listId.add(payCode.getId());
								}

							}

						}
						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCode2.setAmount(payCode1.getBaseOnPayCodeAmount());
								payCode2.setPayRate(payCode1.getPayRate());
								payCode2.setPayFactor(payCode1.getPayFactor());
								payCodeList.add(payCode2);
							}
							dtoEmployeeBenefitMaintenance.setDtoPayCode(payCodeList);
						}

					}

					DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
					if (employeeBenefitMaintenance.getEmployeeMaster() != null) {
						employeeMaster.setEmployeeIndexId(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId());
						employeeMaster.setEmployeeId(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeId());
						employeeMaster.setEmployeeBirthDate(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeBirthDate());
						employeeMaster
								.setEmployeeCitizen(employeeBenefitMaintenance.getEmployeeMaster().isEmployeeCitizen());
						employeeMaster.setEmployeeFirstName(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeFirstName());
						employeeMaster.setEmployeeFirstNameArabic(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeFirstNameArabic());
						employeeMaster
								.setEmployeeGender(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeGender());
						employeeMaster.setEmployeeHireDate(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeHireDate());
						employeeMaster.setEmployeeImmigration(
								employeeBenefitMaintenance.getEmployeeMaster().isEmployeeImmigration());
						employeeMaster.setEmployeeInactive(
								employeeBenefitMaintenance.getEmployeeMaster().isEmployeeInactive());
						employeeMaster.setEmployeeInactiveDate(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactiveDate());
						employeeMaster.setEmployeeInactiveReason(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactiveReason());
						employeeMaster.setEmployeeLastName(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeLastName());
						employeeMaster.setEmployeeLastNameArabic(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeLastNameArabic());
						employeeMaster.setEmployeeLastWorkDate(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeLastWorkDate());
						employeeMaster.setEmployeeMaritalStatus(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeMaritalStatus());
						employeeMaster.setEmployeeMiddleName(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeMiddleName());
						employeeMaster.setEmployeeMiddleNameArabic(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeMiddleNameArabic());
						employeeMaster
								.setEmployeeTitle(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeTitle());
						employeeMaster.setEmployeeTitleArabic(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeTitleArabic());
						employeeMaster
								.setEmployeeType(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeType());
						employeeMaster.setEmployeeUserIdInSystem(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeUserIdInSystem());
						employeeMaster.setEmployeeWorkHourYearly(
								employeeBenefitMaintenance.getEmployeeMaster().getEmployeeWorkHourYearly());
						dtoEmployeeBenefitMaintenance.setEmployeeMasters(employeeMaster);

					}

				}
			} else {
				dtoEmployeeBenefitMaintenance.setMessageType("INVALID_ID");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeBenefitMaintenance;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchEmployeeBenefitMaintenance(DtoSearch dtoSearch) {

		try {
			log.info("searchEmployeeBenefitMaintenance Method");
			if (dtoSearch != null) {
				String condition = "";
				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

					if (dtoSearch.getSortOn().equals("endDate") || dtoSearch.getSortOn().equals("startDate")) {
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
					}

				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}

				List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						employeeBenefitMaintenanceList = this.repositoryEmployeeBenefitMaintenance
								.findByIsDeleted(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						employeeBenefitMaintenanceList = this.repositoryEmployeeBenefitMaintenance
								.findByIsDeleted(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						employeeBenefitMaintenanceList = this.repositoryEmployeeBenefitMaintenance
								.findByIsDeleted(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
										Sort.Direction.DESC, condition));
					}

				}
				if (employeeBenefitMaintenanceList != null && !employeeBenefitMaintenanceList.isEmpty()) {
					List<DtoEmployeeBenefitMaintenance> dtoEmployeeBenefitMaintenanceList = new ArrayList<>();
					DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance = null;
					for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
						dtoEmployeeBenefitMaintenance = new DtoEmployeeBenefitMaintenance(employeeBenefitMaintenance);

						DtoEmployeeMasterHcm employeeMaster = new DtoEmployeeMasterHcm();
						if (employeeBenefitMaintenance.getEmployeeMaster() != null) {
							employeeMaster.setEmployeeIndexId(
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId());
							employeeMaster
									.setEmployeeId(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeId());
							employeeMaster.setEmployeeFirstName(
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeFirstName());
							employeeMaster.setEmployeeMiddleName(
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeMiddleName());
							employeeMaster.setEmployeeLastName(
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeLastName());
							dtoEmployeeBenefitMaintenance.setEmployeeMaster(employeeMaster);

						}

						DtoBenefitCode benefitCode = new DtoBenefitCode();
						if (employeeBenefitMaintenance.getBenefitCode() != null) {
							benefitCode.setId(employeeBenefitMaintenance.getBenefitCode().getId());

							benefitCode.setBenefitId(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
							benefitCode.setDesc(employeeBenefitMaintenance.getBenefitCode().getDesc());
							benefitCode.setArbicDesc(employeeBenefitMaintenance.getBenefitCode().getArbicDesc());
							benefitCode.setStartDate(employeeBenefitMaintenance.getBenefitCode().getStartDate());
							benefitCode.setEndDate(employeeBenefitMaintenance.getBenefitCode().getEndDate());
							benefitCode.setTransction(employeeBenefitMaintenance.getBenefitCode().isTransction());
							benefitCode.setMethod(employeeBenefitMaintenance.getBenefitCode().getMethod());
							benefitCode.setAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
							benefitCode.setPercent(employeeBenefitMaintenance.getBenefitCode().getPercent());
							benefitCode.setPerPeriod(employeeBenefitMaintenance.getBenefitCode().getPerPeriod());
							benefitCode.setPerYear(employeeBenefitMaintenance.getBenefitCode().getPerYear());
							benefitCode.setLifeTime(employeeBenefitMaintenance.getBenefitCode().getLifeTime());
							benefitCode.setFrequency(employeeBenefitMaintenance.getBenefitCode().getFrequency());
							benefitCode.setInActive(employeeBenefitMaintenance.getBenefitCode().isInActive());
							benefitCode.setCustomDate(employeeBenefitMaintenance.getBenefitCode().isCustomDate());
							dtoEmployeeBenefitMaintenance.setBenefitCode(benefitCode);
						}

						dtoEmployeeBenefitMaintenance.setId(employeeBenefitMaintenance.getId());
						dtoEmployeeBenefitMaintenance.setStartDate(employeeBenefitMaintenance.getStartDate());
						dtoEmployeeBenefitMaintenance.setEndDate(employeeBenefitMaintenance.getEndDate());
						dtoEmployeeBenefitMaintenance
								.setTransactionRequired(employeeBenefitMaintenance.isTransactionRequired());
						dtoEmployeeBenefitMaintenance.setBenefitMethod(employeeBenefitMaintenance.getBenefitMethod());
						dtoEmployeeBenefitMaintenance.setBenefitAmount(employeeBenefitMaintenance.getBenefitAmount());
						dtoEmployeeBenefitMaintenance.setBenefitPercent(employeeBenefitMaintenance.getBenefitPercent());
						dtoEmployeeBenefitMaintenance.setPerPeriord(employeeBenefitMaintenance.getPerPeriord());
						dtoEmployeeBenefitMaintenance.setPerYear(employeeBenefitMaintenance.getPerYear());
						dtoEmployeeBenefitMaintenance.setLifeTime(employeeBenefitMaintenance.getLifeTime());
						dtoEmployeeBenefitMaintenance.setFrequency(employeeBenefitMaintenance.getFrequency());
						dtoEmployeeBenefitMaintenance.setInactive(employeeBenefitMaintenance.getInactive());
						dtoEmployeeBenefitMaintenance.setNoOfDays(employeeBenefitMaintenance.getNoOfDays());
						dtoEmployeeBenefitMaintenance.setEndDateDays(employeeBenefitMaintenance.getEndDateDays());

						dtoEmployeeBenefitMaintenanceList.add(dtoEmployeeBenefitMaintenance);
					}
					dtoSearch.setRecords(dtoEmployeeBenefitMaintenanceList);
				}

				log.debug("Search OrientationSetup Size is:" + dtoSearch.getTotalCount());

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllData(DtoSearch dtoSearch) {
		try {
			log.info("searchEmployeeBenefitMaintenance Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";
				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
					if (dtoSearch.getSortOn().equals("endDate") || dtoSearch.getSortOn().equals("startDate")) {
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
					}
				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}

				dtoSearch.setTotalCount(this.repositoryEmployeeBenefitMaintenance
						.predictiveEmployeeBenefitMaintenanceSearchTotalCount("%" + searchWord + "%"));
				List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						employeeBenefitMaintenanceList = this.repositoryEmployeeBenefitMaintenance
								.predictiveEmployeeBenefitMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.DESC, "id"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						employeeBenefitMaintenanceList = this.repositoryEmployeeBenefitMaintenance
								.predictiveEmployeeBenefitMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						employeeBenefitMaintenanceList = this.repositoryEmployeeBenefitMaintenance
								.predictiveEmployeeBenefitMaintenanceSearchWithPagination("%" + searchWord + "%",
										new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
												Sort.Direction.DESC, condition));
					}
				}
				if (employeeBenefitMaintenanceList != null && !employeeBenefitMaintenanceList.isEmpty()) {
					List<DtoEmployeeBenefitMaintenance> dtoEmployeeBenefitMaintenanceList = new ArrayList<>();
					DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance = null;
					for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
						dtoEmployeeBenefitMaintenance = new DtoEmployeeBenefitMaintenance(employeeBenefitMaintenance);
						DtoEmployeeMasterHcm employeeMaster = new DtoEmployeeMasterHcm();
						if (employeeBenefitMaintenance.getEmployeeMaster() != null) {
							employeeMaster.setEmployeeIndexId(
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId());
							employeeMaster
									.setEmployeeId(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeId());
							employeeMaster.setEmployeeFirstName(
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeFirstName());
							employeeMaster.setEmployeeMiddleName(
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeMiddleName());
							employeeMaster.setEmployeeLastName(
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeLastName());
							dtoEmployeeBenefitMaintenance.setEmployeeMaster(employeeMaster);
						}
						DtoBenefitCode benefitCode = new DtoBenefitCode();
						if (employeeBenefitMaintenance.getBenefitCode() != null) {
							benefitCode.setId(employeeBenefitMaintenance.getBenefitCode().getId());
							benefitCode.setBenefitId(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
							benefitCode.setDesc(employeeBenefitMaintenance.getBenefitCode().getDesc());
							benefitCode.setArbicDesc(employeeBenefitMaintenance.getBenefitCode().getArbicDesc());
							benefitCode.setStartDate(employeeBenefitMaintenance.getBenefitCode().getStartDate());
							benefitCode.setEndDate(employeeBenefitMaintenance.getBenefitCode().getEndDate());
							benefitCode.setTransction(employeeBenefitMaintenance.getBenefitCode().isTransction());
							benefitCode.setMethod(employeeBenefitMaintenance.getBenefitCode().getMethod());
							benefitCode.setAmount(employeeBenefitMaintenance.getBenefitCode().getAmount());
							benefitCode.setPercent(employeeBenefitMaintenance.getBenefitCode().getPercent());
							benefitCode.setPerPeriod(employeeBenefitMaintenance.getBenefitCode().getPerPeriod());
							benefitCode.setPerYear(employeeBenefitMaintenance.getBenefitCode().getPerYear());
							benefitCode.setLifeTime(employeeBenefitMaintenance.getBenefitCode().getLifeTime());
							benefitCode.setFrequency(employeeBenefitMaintenance.getBenefitCode().getFrequency());
							benefitCode.setInActive(employeeBenefitMaintenance.getBenefitCode().isInActive());
							benefitCode.setCustomDate(employeeBenefitMaintenance.getBenefitCode().isCustomDate());

							benefitCode.setRoundOf(employeeBenefitMaintenance.getBenefitCode().getRoundOf()); // ME
							benefitCode.setBenefitTypeId(employeeBenefitMaintenance.getBenefitCode().getTypeField()); // ME

							dtoEmployeeBenefitMaintenance.setBenefitCode(benefitCode);
						}
						dtoEmployeeBenefitMaintenance.setId(employeeBenefitMaintenance.getId());

						if (employeeBenefitMaintenance.getEmployeeMaster() != null
								&& employeeBenefitMaintenance.getBenefitCode().getNoOfDays() != null
								&& employeeBenefitMaintenance.getBenefitCode().getEndDateDays() != null) {

							dtoEmployeeBenefitMaintenance.setStartDate(UtilDateAndTimeHr.startDate(
									employeeBenefitMaintenance.getBenefitCode().getNoOfDays(),
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeHireDate()));
							dtoEmployeeBenefitMaintenance.setEndDate(
									UtilDateAndTimeHr.endDate(employeeBenefitMaintenance.getBenefitCode().getNoOfDays(),
											employeeBenefitMaintenance.getBenefitCode().getEndDateDays(),
											employeeBenefitMaintenance.getEmployeeMaster().getEmployeeHireDate()));
						} else {
							dtoEmployeeBenefitMaintenance.setStartDate(employeeBenefitMaintenance.getStartDate());
							dtoEmployeeBenefitMaintenance.setEndDate(employeeBenefitMaintenance.getEndDate());
						}

//						dtoEmployeeBenefitMaintenance.setStartDate(employeeBenefitMaintenance.getStartDate());
//						dtoEmployeeBenefitMaintenance.setEndDate(employeeBenefitMaintenance.getEndDate());
						dtoEmployeeBenefitMaintenance
								.setTransactionRequired(employeeBenefitMaintenance.isTransactionRequired());
						dtoEmployeeBenefitMaintenance.setBenefitMethod(employeeBenefitMaintenance.getBenefitMethod());
						dtoEmployeeBenefitMaintenance.setBenefitAmount(employeeBenefitMaintenance.getBenefitAmount());
						dtoEmployeeBenefitMaintenance.setBenefitPercent(employeeBenefitMaintenance.getBenefitPercent());
						dtoEmployeeBenefitMaintenance.setPerPeriord(employeeBenefitMaintenance.getPerPeriord());
						dtoEmployeeBenefitMaintenance.setPerYear(employeeBenefitMaintenance.getPerYear());
						dtoEmployeeBenefitMaintenance.setLifeTime(employeeBenefitMaintenance.getLifeTime());
						dtoEmployeeBenefitMaintenance.setFrequency(employeeBenefitMaintenance.getFrequency());
						dtoEmployeeBenefitMaintenance.setInactive(employeeBenefitMaintenance.getInactive());

						dtoEmployeeBenefitMaintenanceList.add(dtoEmployeeBenefitMaintenance);
					}
					dtoSearch.setRecords(dtoEmployeeBenefitMaintenanceList);
				}
				log.debug("Search OrientationSetup Size is:" + dtoSearch.getTotalCount());

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoEmployeeBenefitMaintenance validate(Integer employeeId, Integer benefitCode) {
		log.info("repeatByEmployeeId Method");
		DtoEmployeeBenefitMaintenance dto = new DtoEmployeeBenefitMaintenance();
		try {
			List<EmployeeBenefitMaintenance> list = repositoryEmployeeBenefitMaintenance.validate(employeeId,
					benefitCode);
			if (list != null && !list.isEmpty()) {
				dto.setIsRepeat(true);
			} else {
				dto.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dto;
	}
}
