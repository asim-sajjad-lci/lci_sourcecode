/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.PayScheduleSetup;
import com.bti.hcm.model.PayScheduleSetupSchedulePeriods;
import com.bti.hcm.model.dto.DtoPayScheduleSetupSchedulePeriods;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPayScheduleSetup;
import com.bti.hcm.repository.RepositoryPayScheduleSetupSchedulePeriods;

/**
 * Description: Service PayScheduleSetupSchedulePeriods
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("servicePayScheduleSetupSchedulePeriods")
public class ServicePayScheduleSetupSchedulePeriods {
	
	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetup Service
	 */
	static Logger log = Logger.getLogger(ServicePayScheduleSetupSchedulePeriods.class.getName());


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PayScheduleSetupSchedulePeriods service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PayScheduleSetupSchedulePeriods service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPayScheduleSetupSchedulePeriods Autowired here using annotation of spring for access of repositoryPayScheduleSetupSchedulePeriods method in PayScheduleSetupSchedulePeriods service
	 * 				In short Access PayScheduleSetupSchedulePeriods Query from Database using repositoryPayScheduleSetupSchedulePeriods.
	 */
	@Autowired
	RepositoryPayScheduleSetupSchedulePeriods  repositoryPayScheduleSetupSchedulePeriods;
	
	
	/**
	 * @Description repositoryPayScheduleSetup Autowired here using annotation of spring for access of repositoryPayScheduleSetup method in PayScheduleSetup service
	 * 				In short Access PayScheduleSetup Query from Database using repositoryPayScheduleSetup.
	 */
	@Autowired
	RepositoryPayScheduleSetup repositoryPayScheduleSetup;
	
	
	/**
	 * 
	 * @param dtoPayScheduleSetupSchedulePeriods
	 * @return
	 * @throws ParseException
	 */
	public DtoPayScheduleSetupSchedulePeriods saveOrUpdatePayScheduleSetupSchedulePeriod(DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods) throws ParseException {
		log.info("saveOrUpdatePayScheduleSetupSchedulePeriod Method");
		PayScheduleSetupSchedulePeriods payScheduleSetupSchedulePeriods = null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
			PayScheduleSetup payScheduleSetup= repositoryPayScheduleSetup.findOne(dtoPayScheduleSetupSchedulePeriods.getPayScheduleSetupId());
			if (dtoPayScheduleSetupSchedulePeriods.getPayScheduleIndexId() > 0) {
				payScheduleSetupSchedulePeriods = repositoryPayScheduleSetupSchedulePeriods.findByPaySchedulePeriodIndexIdAndIsDeleted(dtoPayScheduleSetupSchedulePeriods.getPayScheduleIndexId(), false);
				payScheduleSetupSchedulePeriods.setUpdatedBy(loggedInUserId);
				payScheduleSetupSchedulePeriods.setUpdatedDate(new Date());
			} else {
				payScheduleSetupSchedulePeriods = new PayScheduleSetupSchedulePeriods();
				payScheduleSetupSchedulePeriods.setCreatedDate(new Date());
				
				Integer rowId = repositoryPayScheduleSetupSchedulePeriods.getCountOfTotaPayScheduleSetupSchedulePeriods();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				payScheduleSetupSchedulePeriods.setRowId(increment);
			}
			payScheduleSetupSchedulePeriods.setPeriodId(dtoPayScheduleSetupSchedulePeriods.getPeriodId());
			payScheduleSetupSchedulePeriods.setYear(dtoPayScheduleSetupSchedulePeriods.getYear());
			payScheduleSetupSchedulePeriods.setPeriodName(dtoPayScheduleSetupSchedulePeriods.getPeriodName());
			payScheduleSetupSchedulePeriods.setPeriodStartDate(dtoPayScheduleSetupSchedulePeriods.getPeriodStartDate());
			payScheduleSetupSchedulePeriods.setPeriodEndDate(dtoPayScheduleSetupSchedulePeriods.getPeriodEndDate());
			payScheduleSetupSchedulePeriods.setPayScheduleSetup(payScheduleSetup);
			payScheduleSetupSchedulePeriods = repositoryPayScheduleSetupSchedulePeriods.saveAndFlush(payScheduleSetupSchedulePeriods);
			log.debug("PayScheduleSetupSchedulePeriod is:"+dtoPayScheduleSetupSchedulePeriods.getPayScheduleIndexId());
		} catch (Exception e) {
			
			log.error(e);
		}
		return dtoPayScheduleSetupSchedulePeriods;

	}
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPayScheduleSetupSchedulePeriods(DtoSearch dtoSearch) {
		log.info("searchPayScheduleSetupSchedulePeriods Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "periodName" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="paySchedulePeriodIndexId";
					
				}
			}else{
				condition+="paySchedulePeriodIndexId";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositoryPayScheduleSetupSchedulePeriods.predictivePayScheduleSetupSchedulePeriodsSearchTotalCount("%"+searchWord+"%"));
				List<PayScheduleSetupSchedulePeriods> payScheduleSetupSchedulePeriodsList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						payScheduleSetupSchedulePeriodsList = this.repositoryPayScheduleSetupSchedulePeriods.predictivePayScheduleSetupSchedulePeriodsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						payScheduleSetupSchedulePeriodsList = this.repositoryPayScheduleSetupSchedulePeriods.predictivePayScheduleSetupSchedulePeriodsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						payScheduleSetupSchedulePeriodsList = this.repositoryPayScheduleSetupSchedulePeriods.predictivePayScheduleSetupSchedulePeriodsSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}
				if(payScheduleSetupSchedulePeriodsList != null && !payScheduleSetupSchedulePeriodsList.isEmpty()){
					List<DtoPayScheduleSetupSchedulePeriods> dtoPayScheduleSetupSchedulePeriodsList = new ArrayList<DtoPayScheduleSetupSchedulePeriods>();
					DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods=null;
					for (PayScheduleSetupSchedulePeriods payScheduleSetupSchedulePeriods : payScheduleSetupSchedulePeriodsList) {
						PayScheduleSetup payScheduleSetup= repositoryPayScheduleSetup.findOne(payScheduleSetupSchedulePeriods.getPaySchedulePeriodIndexId());
						dtoPayScheduleSetupSchedulePeriods = new DtoPayScheduleSetupSchedulePeriods(payScheduleSetupSchedulePeriods);
						dtoPayScheduleSetupSchedulePeriods.setPeriodId(payScheduleSetupSchedulePeriods.getPeriodId());
						dtoPayScheduleSetupSchedulePeriods.setYear(payScheduleSetupSchedulePeriods.getYear());
						dtoPayScheduleSetupSchedulePeriods.setPeriodName(payScheduleSetupSchedulePeriods.getPeriodName());
						dtoPayScheduleSetupSchedulePeriods.setPeriodStartDate(payScheduleSetupSchedulePeriods.getPeriodStartDate());
						dtoPayScheduleSetupSchedulePeriods.setPeriodEndDate(payScheduleSetupSchedulePeriods.getPeriodEndDate());
						payScheduleSetupSchedulePeriods.setPayScheduleSetup(payScheduleSetup);
						dtoPayScheduleSetupSchedulePeriodsList.add(dtoPayScheduleSetupSchedulePeriods);
						
					}
					dtoSearch.setRecords(dtoPayScheduleSetupSchedulePeriodsList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public DtoPayScheduleSetupSchedulePeriods getPayScheduleSetupSchedulePeriodsId(int id) {
		log.info("getPayScheduleSetupSchedulePeriodsId Method");
		DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods = new DtoPayScheduleSetupSchedulePeriods();
		try {
			if (id > 0) {
				PayScheduleSetupSchedulePeriods payScheduleSetupSchedulePeriods = repositoryPayScheduleSetupSchedulePeriods.findByPaySchedulePeriodIndexIdAndIsDeleted(id, false);
				if (payScheduleSetupSchedulePeriods != null) {
					dtoPayScheduleSetupSchedulePeriods = new DtoPayScheduleSetupSchedulePeriods(payScheduleSetupSchedulePeriods);
				} else {
					dtoPayScheduleSetupSchedulePeriods.setMessageType("PAYSCHEDULE_SETUP_SCHEDULE_PERIOD_NOT_GETTING");

				}
			} else {
				dtoPayScheduleSetupSchedulePeriods.setMessageType("INVALID_PAYSCHEDULE_SETUP_SCHEDULE_PERIOD_INDEX_ID");

			}
			log.debug("PayScheduleSetup By Id is:"+dtoPayScheduleSetupSchedulePeriods.getPayScheduleIndexId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayScheduleSetupSchedulePeriods;
	}
	
	
	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoPayScheduleSetupSchedulePeriods deletePayScheduleSetupSchedulePeriods(List<Integer> ids) {
		log.info("deletePayScheduleSetupSchedulePeriods Method");
		DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods = new DtoPayScheduleSetupSchedulePeriods();
		dtoPayScheduleSetupSchedulePeriods.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAYSCHEDULE_SETUP_SCHEDULE_PERIOD_DELETED", false));
		dtoPayScheduleSetupSchedulePeriods.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("PAYSCHEDULE_SETUP_SCHEDULE_PERIOD_ASSOCIATED", false));
		List<DtoPayScheduleSetupSchedulePeriods> deleteDtoPayScheduleSetupSchedulePeriods = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		try {
			for (Integer payScheduleIndexId : ids) {
				PayScheduleSetupSchedulePeriods payScheduleSetupSchedulePeriods = repositoryPayScheduleSetupSchedulePeriods.findOne(payScheduleIndexId);
				DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods2 = new DtoPayScheduleSetupSchedulePeriods();
				dtoPayScheduleSetupSchedulePeriods2.setPayScheduleIndexId(payScheduleIndexId);
				repositoryPayScheduleSetupSchedulePeriods.deleteSinglePayScheduleSetupSchedulePeriods(true, loggedInUserId, payScheduleIndexId);
				deleteDtoPayScheduleSetupSchedulePeriods.add(dtoPayScheduleSetupSchedulePeriods2);

			}
			dtoPayScheduleSetupSchedulePeriods.setDeletePayScheduleSetupSchedulePeriods(deleteDtoPayScheduleSetupSchedulePeriods);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete PayScheduleSetupSchedulePeriods :"+dtoPayScheduleSetupSchedulePeriods.getPayScheduleIndexId());
		return dtoPayScheduleSetupSchedulePeriods;
	}
	
	
	/**
	 * 
	 * @param payScheduleIndexId
	 * @return
	 */
	public DtoPayScheduleSetupSchedulePeriods repeatByPayScheduleSetupSchedulePeriodsId(int payScheduleIndexId) {
		log.info("repeatByPayScheduleSetupSchedulePeriodsId Method");
		DtoPayScheduleSetupSchedulePeriods dtoPayScheduleSetupSchedulePeriods = new DtoPayScheduleSetupSchedulePeriods();
		try {
			List<PayScheduleSetupSchedulePeriods> payScheduleSetupSchedulePeriods=repositoryPayScheduleSetupSchedulePeriods.findByPayScheduleSetupSchedulePeriods(payScheduleIndexId);
			if(payScheduleSetupSchedulePeriods!=null && !payScheduleSetupSchedulePeriods.isEmpty()) {
				dtoPayScheduleSetupSchedulePeriods.setIsRepeat(true);
			}else {
				dtoPayScheduleSetupSchedulePeriods.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoPayScheduleSetupSchedulePeriods;
	}

}
