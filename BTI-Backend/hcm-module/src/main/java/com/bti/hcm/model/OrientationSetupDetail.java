package com.bti.hcm.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40531",indexes = {
        @Index(columnList = "ORNTINDXD")
})
public class OrientationSetupDetail extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORNTINDXD")
	private Integer id;

	@Column(name = "ORNTCHKSEQN")
	private Integer sequence;

	@ManyToOne
	@JoinColumn(name = "ORNTINDX")
	private OrientationSetup orientationSetups; 	
	
	@Column(name = "ORNTCHKDSCR", columnDefinition = "char(150)")
	private String checklistitem;

	@Column(name = "ORNTCHKDSCRA", columnDefinition = "char(150)")
	private String arabicDescription;

	@Column(name = "ORNTCHKSTDT")
	private Date startDate;

	@Column(name = "ORNTCHKENDT")
	private Date enddate;

	@Column(name = "ORNTCHKSTMT")
	private Date starttime;

	@Column(name = "ORNTCHKENMT")
	private Date endtime;

	@Column(name = "ORNTCHKRESP", columnDefinition = "char(15)")
	private String personResponsible;
	
	
	
	
	
	

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getChecklistitem() {
		return checklistitem;
	}

	public void setChecklistitem(String checklistitem) {
		this.checklistitem = checklistitem;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}


	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	public String getPersonResponsible() {
		return personResponsible;
	}

	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}

	public OrientationSetup getOrientationSetups() {
		return orientationSetups;
	}

	public void setOrientationSetups(OrientationSetup orientationSetups) {
		this.orientationSetups = orientationSetups;
	}

	

	

}
