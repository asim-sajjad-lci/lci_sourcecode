package com.bti.hcm.model.dto;

import java.util.List;

public class DtoFinancialDimensions extends DtoBase {

	private Integer id;
	private String dimensionDescription;
	private String dimensionArbicDescription;
	private String dimensionColumnName;
	private List<DtoFinancialDimensions> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDimensionDescription() {
		return dimensionDescription;
	}

	public void setDimensionDescription(String dimensionDescription) {
		this.dimensionDescription = dimensionDescription;
	}

	public String getDimensionArbicDescription() {
		return dimensionArbicDescription;
	}

	public void setDimensionArbicDescription(String dimensionArbicDescription) {
		this.dimensionArbicDescription = dimensionArbicDescription;
	}

	public String getDimensionColumnName() {
		return dimensionColumnName;
	}

	public void setDimensionColumnName(String dimensionColumnName) {
		this.dimensionColumnName = dimensionColumnName;
	}

	public List<DtoFinancialDimensions> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoFinancialDimensions> delete) {
		this.delete = delete;
	}

}
