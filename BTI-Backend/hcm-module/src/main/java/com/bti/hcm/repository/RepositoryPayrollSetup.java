package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollSetup;

@Repository("repositoryPayrollSetup")
public interface RepositoryPayrollSetup extends JpaRepository<PayrollSetup,Integer >{
 
	public List<PayrollSetup> findByIsDeleted(Boolean deleted);
	
	public PayrollSetup findByIdAndIsDeleted(Integer id,Boolean deleted);

	@Query("select count(*) from PayrollSetup d where (d.vatSetup.vatScheduleId like :searchKeyWord) and d.isDeleted=false")
	public Integer predictivePayrollSearchTotalCount(@Param("searchKeyWord")String string);

	@Query("select d from PayrollSetup d where (d.vatSetup.vatScheduleId like :searchKeyWord) and d.isDeleted=false")
	public List<PayrollSetup> predictivePayrollSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayrollSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSinglePayrollSetup(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);
}
