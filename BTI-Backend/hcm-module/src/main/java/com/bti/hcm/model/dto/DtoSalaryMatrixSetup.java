/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.SalaryMatrixSetup;
import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * Description: DTO SalaryMatrixSetup class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoSalaryMatrixSetup extends DtoBase{
	
	private Integer id;
	private List<DtoSalaryMatrixSetup> deleteSalaryMatrixSetup;
	private List<DtoSalaryMatrixRow> listSalaryMatrixRow;
	private List<DtoSalaryMatrixColSetup> listSalaryMatrixColSetup;
	private String salaryMatrixId;
	private String description;
	private String arabicSalaryMatrixDescription;
	private Short payUnit;
	private int totalRow;
	private int totalColumns;
	private int totalRowValues;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<DtoSalaryMatrixSetup> getDeleteSalaryMatrixSetup() {
		return deleteSalaryMatrixSetup;
	}
	public void setDeleteSalaryMatrixSetup(List<DtoSalaryMatrixSetup> deleteSalaryMatrixSetup) {
		this.deleteSalaryMatrixSetup = deleteSalaryMatrixSetup;
	}
	public String getSalaryMatrixId() {
		return salaryMatrixId;
	}
	public void setSalaryMatrixId(String salaryMatrixId) {
		this.salaryMatrixId = salaryMatrixId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getArabicSalaryMatrixDescription() {
		return arabicSalaryMatrixDescription;
	}
	public void setArabicSalaryMatrixDescription(String arabicSalaryMatrixDescription) {
		this.arabicSalaryMatrixDescription = arabicSalaryMatrixDescription;
	}
	public Short getPayUnit() {
		return payUnit;
	}
	public void setPayUnit(Short payUnit) {
		this.payUnit = payUnit;
	}
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public int getTotalColumns() {
		return totalColumns;
	}
	public void setTotalColumns(int totalColumns) {
		this.totalColumns = totalColumns;
	}
	public int getTotalRowValues() {
		return totalRowValues;
	}
	public void setTotalRowValues(int totalRowValues) {
		this.totalRowValues = totalRowValues;
	}
	
	public DtoSalaryMatrixSetup() {
		
	}
	public DtoSalaryMatrixSetup(SalaryMatrixSetup department) {
	}
	public List<DtoSalaryMatrixRow> getListSalaryMatrixRow() {
		return listSalaryMatrixRow;
	}
	public void setListSalaryMatrixRow(List<DtoSalaryMatrixRow> listSalaryMatrixRow) {
		this.listSalaryMatrixRow = listSalaryMatrixRow;
	}
	public List<DtoSalaryMatrixColSetup> getListSalaryMatrixColSetup() {
		return listSalaryMatrixColSetup;
	}
	public void setListSalaryMatrixColSetup(List<DtoSalaryMatrixColSetup> listSalaryMatrixColSetup) {
		this.listSalaryMatrixColSetup = listSalaryMatrixColSetup;
	}
	
	

}
