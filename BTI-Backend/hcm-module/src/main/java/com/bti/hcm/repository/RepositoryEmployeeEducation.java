package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeEducation;


@Repository("repositoryEmployeeEducation")

   public interface RepositoryEmployeeEducation extends JpaRepository<EmployeeEducation, Integer>{

	EmployeeEducation findByIdAndIsDeleted(Integer id, boolean b);


	EmployeeEducation findOne(Integer employeeEducationId);
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeEducation e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.id =:id ") 
	public void deleteSinglEmployeeEducation(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from EmployeeEducation e where (e.schoolName LIKE :searchKeyWord or e.major LIKE :searchKeyWord or e.comments LIKE :searchKeyWord) and e.isDeleted=false")
	Integer predictiveEmployeeEducationSearchTotalCount(@Param("searchKeyWord")  String searchKeyWord);

	@Query("select e from EmployeeEducation e where (e.schoolName LIKE :searchKeyWord or e.major LIKE :searchKeyWord or e.comments LIKE :searchKeyWord) and e.isDeleted=false order by e.id desc")
	List<EmployeeEducation> predictiveEmployeeEducationSearchWithPagination(@Param("searchKeyWord")String string, Pageable pageRequest);

	
	@Query("select e from EmployeeEducation e where e.employeeMaster.employeeIndexId =:id and e.isDeleted=false")
	List<EmployeeEducation> getByEmployeeId(@Param("id") Integer id);

	@Query("select count(*) from EmployeeEducation e ")
	public Integer getCountOfTotalEmployeeEducation();
	
}
