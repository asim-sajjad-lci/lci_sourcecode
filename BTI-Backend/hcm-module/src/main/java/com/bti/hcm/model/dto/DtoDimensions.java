package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoDimensions extends DtoBase {
	private int id;
	private String dimensionDesc;
	private String dimensionArabicDesc;
	private String moduleCode;
	private String tableName;
	private Integer tableIndx;
	private boolean isStatic;
	private boolean hasUpdate;
	private String valueId;
	private String valueDescription;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDimensionDesc() {
		return dimensionDesc;
	}

	public void setDimensionDesc(String dimensionDesc) {
		this.dimensionDesc = dimensionDesc;
	}

	public String getDimensionArabicDesc() {
		return dimensionArabicDesc;
	}

	public void setDimensionArabicDesc(String dimensionArabicDesc) {
		this.dimensionArabicDesc = dimensionArabicDesc;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Integer getTableIndx() {
		return tableIndx;
	}

	public void setTableIndx(Integer tableIndx) {
		this.tableIndx = tableIndx;
	}

	public boolean isStatic() {
		return isStatic;
	}

	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}

	public boolean isHasUpdate() {
		return hasUpdate;
	}

	public void setHasUpdate(boolean hasUpdate) {
		this.hasUpdate = hasUpdate;
	}

	public String getValueId() {
		return valueId;
	}

	public void setValueId(String valueId) {
		this.valueId = valueId;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	
}
