/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.OrientationCheckListSetup;
import com.bti.hcm.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO OrientationCheckListSetup class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoOrientationCheckListSetup extends DtoBase{
	
	private Integer id;
	private String itemDesc;
	private String itemDescArabic;
	private boolean deletedSubitem;
	private boolean isdeletedItems;
	public boolean isDeletedSubitem() {
		return deletedSubitem;
	}
	public void setDeletedSubitem(boolean deletedSubitem) {
		this.deletedSubitem = deletedSubitem;
	}
	private String orientationChecklistItemDescription;
	private String orientationChecklistItemDescriptionArabic;
	private Integer orientationPredefinedCheckListItemId;
	private List<DtoOrientationCheckListSetup> deleteDtoOrientationCheckListSetup;
	private List<DtoOrientationCheckListSetup> list;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOrientationChecklistItemDescription() {
		return orientationChecklistItemDescription;
	}
	public void setOrientationChecklistItemDescription(String orientationChecklistItemDescription) {
		this.orientationChecklistItemDescription = orientationChecklistItemDescription;
	}
	public String getOrientationChecklistItemDescriptionArabic() {
		return orientationChecklistItemDescriptionArabic;
	}
	public void setOrientationChecklistItemDescriptionArabic(String orientationChecklistItemDescriptionArabic) {
		this.orientationChecklistItemDescriptionArabic = orientationChecklistItemDescriptionArabic;
	}
	
	public Integer getOrientationPredefinedCheckListItemId() {
		return orientationPredefinedCheckListItemId;
	}
	public void setOrientationPredefinedCheckListItemId(Integer orientationPredefinedCheckListItemId) {
		this.orientationPredefinedCheckListItemId = orientationPredefinedCheckListItemId;
	}
	
	public List<DtoOrientationCheckListSetup> getDeleteDtoOrientationCheckListSetup() {
		return deleteDtoOrientationCheckListSetup;
	}
	public void setDeleteDtoOrientationCheckListSetup(
			List<DtoOrientationCheckListSetup> deleteDtoOrientationCheckListSetup) {
		this.deleteDtoOrientationCheckListSetup = deleteDtoOrientationCheckListSetup;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getItemDescArabic() {
		return itemDescArabic;
	}
	public void setItemDescArabic(String itemDescArabic) {
		this.itemDescArabic = itemDescArabic;
	}

	public DtoOrientationCheckListSetup() {
		
	}
public boolean isIsdeletedItems() {
		return isdeletedItems;
	}
	public void setIsdeletedItems(boolean isdeletedItems) {
		this.isdeletedItems = isdeletedItems;
	}
public DtoOrientationCheckListSetup(OrientationCheckListSetup orientationCheckListSetup) {
		
		this.id = orientationCheckListSetup.getId();


		if (UtilRandomKey.isNotBlank(orientationCheckListSetup.getOrientationChecklistItemDescription())) {
			this.orientationChecklistItemDescription = orientationCheckListSetup.getOrientationChecklistItemDescription();
		} else {
			this.orientationChecklistItemDescription = "";
		}
		
		if (UtilRandomKey.isNotBlank(orientationCheckListSetup.getOrientationChecklistItemDescriptionArabic())) {
			this.orientationChecklistItemDescriptionArabic = orientationCheckListSetup.getOrientationChecklistItemDescriptionArabic();
		} else {
			this.orientationChecklistItemDescriptionArabic = "";
		}
	}
public List<DtoOrientationCheckListSetup> getList() {
	return list;
}
public void setList(List<DtoOrientationCheckListSetup> list) {
	this.list = list;
}


}
