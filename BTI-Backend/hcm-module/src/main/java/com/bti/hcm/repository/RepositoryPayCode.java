package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayCode;

@Repository("repositoryPayCode")
public interface RepositoryPayCode extends JpaRepository<PayCode, Integer>{

	PayCode findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update PayCode d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSinglePayCode(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from PayCode d where (d.payType.desc like :searchKeyWord or d.payCodeId like :searchKeyWord or d.description like :searchKeyWord or d.arbicDescription like :searchKeyWord ) and d.isDeleted=false")
	Integer predictivePayCodeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from PayCode d where (d.payType.desc like :searchKeyWord or d.payCodeId like :searchKeyWord or d.description like :searchKeyWord or d.arbicDescription like :searchKeyWord) and d.isDeleted=false")
	List<PayCode> predictivePayCodeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select d from PayCode d where (d.payCodeId=:searchKeyWord) and d.isDeleted=false")
	List<PayCode> findByPayCodeId(@Param("searchKeyWord") String id);

	@Query("select d from PayCode d where (d.payCodeId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<PayCode> predictivePositionClassIdSearchWithPagination(@Param("searchKeyWord") String string);

	@Query("select d from PayCode d where  d.payCodeId like :searchKeyWord and d.isDeleted=false")
	List<PayCode> predictiveAllPayCodeIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);
	
	public List<PayCode> findByIsDeleted(Boolean deleted);
	
	public List<PayCode> findByIsDeletedAndInActive(Boolean deleted, Boolean inActive);

	@Query("select d from PayCode d where (d.payCodeId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<PayCode> predictiveSearchAllPayCodeIdWithPagination(@Param("searchKeyWord")String payCodeId);

	@Query("select count(*) from PayCode p ")
	public Integer getCountOfTotaPayCode();

	//public List<PayCode> findByIsDeletedAndInActive(Boolean deleted, Boolean inactive);
	
	@Query("select d from PayCode d where d.id in (:list) and d.isDeleted=false")
	public List<PayCode> findAllPayCodeListId(@Param("list") List<Integer> ls);
	
	@Query("select d from PayCode d where d.id =:id and d.isDeleted=false")
	public List<PayCode> findAllPayCodeListId1(@Param("id") Integer id);
	
	@Query("select p from PayCode p where p.id=:id and p.isDeleted=false")
	public PayCode findByIdPayCodeId(@Param("id")Integer id);
	
//	@Query("select d from DeductionCode d where d.id in (:list) and d.isDeleted=false")
//	public List<DeductionCode> findAllEmployeeListId(@Param("list") List<Integer> list);

	@Query("select p from PayCode p where p.typeField in :ids and p.isDeleted=false")
	List<PayCode> findByTypeFieldIdsAndIsDeleted(@Param("ids") List<Integer> ids);
	
	@Query("select p.id from PayCode p where p.typeField in :ids and p.isDeleted=false")
	List<Integer> findIdsByTypeFieldIdsAndIsDeleted(@Param("ids") List<Integer> ids);
	
	
	
}
