package com.bti.hcm.model.dto;

public class DtoWorkFlowAssign {

	private Integer id;
	private DtoWorkFlow workFlowUploadFile;
	private DtoCompany company;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DtoWorkFlow getWorkFlowUploadFile() {
		return workFlowUploadFile;
	}

	public void setWorkFlowUploadFile(DtoWorkFlow workFlowUploadFile) {
		this.workFlowUploadFile = workFlowUploadFile;
	}

	public DtoCompany getCompany() {
		return company;
	}

	public void setCompany(DtoCompany company) {
		this.company = company;
	}

}
