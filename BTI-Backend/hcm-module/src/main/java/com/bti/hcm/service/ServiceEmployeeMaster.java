/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bti.hcm.model.Bank;
import com.bti.hcm.model.Department;
import com.bti.hcm.model.Division;
import com.bti.hcm.model.EmployeeAddressMaster;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeeNationalities;
import com.bti.hcm.model.Location;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.ProjectSetup;
import com.bti.hcm.model.Supervisor;
import com.bti.hcm.model.dto.DtoBank;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoDivision;
import com.bti.hcm.model.dto.DtoEmployeeAddressMaster;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeMasterHcm;
import com.bti.hcm.model.dto.DtoEmployeeNationalities;
import com.bti.hcm.model.dto.DtoLocation;
import com.bti.hcm.model.dto.DtoPosition;
import com.bti.hcm.model.dto.DtoProjectSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSupervisor;
import com.bti.hcm.repository.RepositoryBank;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryDivision;
import com.bti.hcm.repository.RepositoryEmployeeAddressMaster;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeeNationalities;
import com.bti.hcm.repository.RepositoryLocation;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositoryProjectSetup;
import com.bti.hcm.repository.RepositorySupervisor;
import com.bti.hcm.util.CommonUtils;

/**
 * Description: Service EmployeeMaster
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("serviceEmployeeMaster")
@CacheConfig(cacheNames={"TransactionEntryDetail"})
public class ServiceEmployeeMaster {
	
	/**
	 * @Description LOGGER use for put a logger in EmployeeMaster Service
	 */
	static Logger log = Logger.getLogger(ServiceEmployeeMaster.class.getName());

	
	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in EmployeeMaster service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in EmployeeMaster service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryEmployeeMaster Autowired here using annotation of spring for access of repositoryEmployeeMaster method in EmployeeMaster service
	 * 				In short Access Department Query from Database using repositoryEmployeeMaster.
	 */
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired
	RepositoryDepartment repositoryDepartment;
	
	@Autowired
	RepositoryPosition repositoryPosition;
	
	@Autowired
	RepositoryDivision repositoryDivision;
	
	@Autowired
	RepositoryLocation repositoryLocation;
	
	@Autowired
	RepositoryBank repositoryBank;

	
	@Autowired
	RepositoryEmployeeAddressMaster repositoryEmployeeAddressMaster;
	
	@Autowired
	RepositoryEmployeeNationalities repositoryEmployeeNationalities;
	
	@Autowired
	RepositorySupervisor repositorySupervisor;
	
	@Autowired
	RepositoryProjectSetup repositoryProjectSetup;
	
	/**
	 * 
	 * @param dtoEmployeeMaster
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public DtoEmployeeMaster saveOrUpdate(DtoEmployeeMaster dtoEmployeeMaster,MultipartFile file) throws IOException {
		log.info("saveOrUpdateEmployeeMaster Method");
		EmployeeMaster employeeMaster = null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Department department = repositoryDepartment.findOne(dtoEmployeeMaster.getDepartmentId());
			Division division = repositoryDivision.findOne(dtoEmployeeMaster.getDivisionId());
			Position position = repositoryPosition.findOne(dtoEmployeeMaster.getPositionId());
			Location location = repositoryLocation.findOne(dtoEmployeeMaster.getLocationId());
			Bank bank = repositoryBank.findOne(dtoEmployeeMaster.getBankId());
			Supervisor supervisor = repositorySupervisor.findOne(dtoEmployeeMaster.getSupervisorId());
			ProjectSetup projectSetup = repositoryProjectSetup.findOne(dtoEmployeeMaster.getProjectId());
			EmployeeAddressMaster employeeAddressMaster = repositoryEmployeeAddressMaster
.findOne(dtoEmployeeMaster.getEmployeeAddressIndexId());
			EmployeeNationalities employeeNationalities = repositoryEmployeeNationalities
.findOne(dtoEmployeeMaster.getEmployeeNationalitiesId());
			if (dtoEmployeeMaster.getEmployeeIndexId() > 0) {
				
				employeeMaster = repositoryEmployeeMaster
.findByEmployeeIndexIdAndIsDeleted(dtoEmployeeMaster.getEmployeeIndexId(), false);
				employeeMaster.setUpdatedBy(loggedInUserId);
				employeeMaster.setUpdatedDate(new Date());
			} else {
				employeeMaster = new EmployeeMaster();
				employeeMaster.setCreatedDate(new Date());
				employeeMaster.setUpdatedDate(new Date());
				Integer rowId = repositoryEmployeeMaster.getCountOfTotalEmployeeMasters();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				employeeMaster.setRowId(increment);
			}
			employeeMaster.setDepartment(department);
			employeeMaster.setDivision(division);
			employeeMaster.setPosition(position);
			employeeMaster.setLocation(location);
			employeeMaster.setSupervisor(supervisor);
			employeeMaster.setProjectSetup(projectSetup);
			employeeMaster.setEmployeeAdjustHireDate(dtoEmployeeMaster.getEmployeeAdjustHireDate());
			employeeMaster.setEmployeeAddressMaster(employeeAddressMaster);
			employeeMaster.setEmployeeNationalities(employeeNationalities);
			employeeMaster.setEmployeeId(dtoEmployeeMaster.getEmployeeId());
			employeeMaster.setEmployeeBirthDate(dtoEmployeeMaster.getEmployeeBirthDate());
			employeeMaster.setEmployeeCitizen(dtoEmployeeMaster.isEmployeeCitizen());
			employeeMaster.setEmployeeFirstName(dtoEmployeeMaster.getEmployeeFirstName());
			employeeMaster.setEmployeeFirstNameArabic(dtoEmployeeMaster.getEmployeeFirstNameArabic());
			employeeMaster.setEmployeeGender(dtoEmployeeMaster.getEmployeeGender());
			employeeMaster.setEmployeeHireDate(dtoEmployeeMaster.getEmployeeHireDate());
			employeeMaster.setExpireHijri(dtoEmployeeMaster.getExpireHijri());
			if(file!=null) {
				employeeMaster.setEmployeeImage(file.getBytes());
			} else {
				employeeMaster.setEmployeeImage(null);
			}
			if(dtoEmployeeMaster.getEmployeeBirthDateH() != null) {
				employeeMaster.setEmployeeBirthDateHijri(dtoEmployeeMaster.getEmployeeBirthDateH());
			}else {
				employeeMaster.setEmployeeBirthDateHijri(null);
			}
			if(dtoEmployeeMaster.getExpireHijri() != null) {
				employeeMaster.setExpireHijri(dtoEmployeeMaster.getExpireHijri());
			}else {
				employeeMaster.setExpireHijri(null);
			}
			if(dtoEmployeeMaster.getExpiresHijri() != null) {
				employeeMaster.setExpiresHijri(dtoEmployeeMaster.getExpiresHijri());
			}else {
				employeeMaster.setExpiresHijri(null);
			}
			employeeMaster.setEmployeeImmigration(dtoEmployeeMaster.isEmployeeImmigration());
			employeeMaster.setEmployeeInactive(dtoEmployeeMaster.getEmployeeInactive());
			employeeMaster.setEmployeeInactiveDate(dtoEmployeeMaster.getEmployeeInactiveDate());
			employeeMaster.setEmployeeInactiveReason(dtoEmployeeMaster.getEmployeeInactiveReason());
			employeeMaster.setEmployeeLastName(dtoEmployeeMaster.getEmployeeLastName());
			employeeMaster.setEmployeeLastNameArabic(dtoEmployeeMaster.getEmployeeLastNameArabic());
			employeeMaster.setEmployeeLastWorkDate(dtoEmployeeMaster.getEmployeeLastWorkDate());
			employeeMaster.setEmployeeMaritalStatus(dtoEmployeeMaster.getEmployeeMaritalStatus());
			employeeMaster.setEmployeeMiddleName(dtoEmployeeMaster.getEmployeeMiddleName());
			employeeMaster.setEmployeeMiddleNameArabic(dtoEmployeeMaster.getEmployeeMiddleNameArabic());
			employeeMaster.setEmployeeTitle(dtoEmployeeMaster.getEmployeeTitle());
			employeeMaster.setEmployeeTitleArabic(dtoEmployeeMaster.getEmployeeTitleArabic());
			employeeMaster.setEmployeeType(dtoEmployeeMaster.getEmployeeType());
			employeeMaster.setEmployeeUserIdInSystem(dtoEmployeeMaster.getEmployeeUserIdInSystem());
			employeeMaster.setEmployeeWorkHourYearly(dtoEmployeeMaster.getEmployeeWorkHourYearly());
			employeeMaster.setEmployeeSmoker(dtoEmployeeMaster.isEmployeeSmoker());
			employeeMaster.setIdNumber(dtoEmployeeMaster.getIdNumber());
			employeeMaster.setExpire(dtoEmployeeMaster.getExpire());
			employeeMaster.setExpires(dtoEmployeeMaster.getExpires());
			employeeMaster.setPassportNumber(dtoEmployeeMaster.getPassportNumber());
	
			employeeMaster.setEmployeeNotes(dtoEmployeeMaster.getEmployeeNotes());
			employeeMaster.setEmployeeEmail(dtoEmployeeMaster.getEmployeeEmail());
			employeeMaster.setEmployeeBankAccountNumber(dtoEmployeeMaster.getEmployeeBankAccountNumber());
			employeeMaster.setBank(bank);
			
			employeeMaster = repositoryEmployeeMaster.saveAndFlush(employeeMaster);
			log.debug("Employee Master is:"+employeeMaster.getEmployeeIndexId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeMaster;
	}

	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoEmployeeMaster deleteEmployeeMaster(List<Integer> ids) {
		log.info("deleteEmployeeMaster Method");
		DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
		dtoEmployeeMaster
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_DELETED", false));
		dtoEmployeeMaster.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_ASSOCIATED", false));
		List<DtoEmployeeMaster> deleteEmployee = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_MASTER_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer employeeId : ids) {
				EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOne(employeeId);
				if(employeeMaster.getListEmployeeBenefitMaintenance().isEmpty() && employeeMaster.getListEmployeeDeductionMaintenance().isEmpty() 
						&& employeeMaster.getListEmployeeDirectDeposit().isEmpty() && employeeMaster.getListEmployeePayCodeMaintenance().isEmpty()
						&& employeeMaster.getListHealthInsuranceEnrollment().isEmpty()&& employeeMaster.getListEmployeeEducation().isEmpty()
						&& employeeMaster.getListMiscellaneousBenefirtEnrollment().isEmpty()&&employeeMaster.getListEmployeeDependent().isEmpty()
						&& employeeMaster.getApplicantHire().isEmpty() &&employeeMaster.getListActivateEmployeePostDatePayRate().isEmpty()) {
					DtoEmployeeMaster dtoEmployeeMaster2 = new DtoEmployeeMaster();
					dtoEmployeeMaster2.setEmployeeIndexId(employeeId);
					repositoryEmployeeMaster.deleteSingleEmployeeMaster(true, loggedInUserId, employeeId);
					deleteEmployee.add(dtoEmployeeMaster2);	
				}else {
					inValidDelete = true;
				}
				
			}
			if(inValidDelete){
				dtoEmployeeMaster.setMessageType(invlidDeleteMessage.toString());
				
			}
			
			dtoEmployeeMaster.setDeleteEmployeeMaster(deleteEmployee);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Employee :"+dtoEmployeeMaster.getEmployeeIndexId());
		return dtoEmployeeMaster;
	}
	
	public List<DtoEmployeeMaster> getAllEmployeeMasterDropDownList() {
		log.info("getAllEmployeeMasterDropDownList  Method");
		List<DtoEmployeeMaster> dtoEmployeeMasterList = new ArrayList<>();
		List<EmployeeMaster> list = repositoryEmployeeMaster.findByIsDeleted(false);
		
		if (list != null && !list.isEmpty()) {
			for (EmployeeMaster employeeMaster : list) {
				DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
				
				dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
				dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
				dtoEmployeeMaster.setEmployeeMiddleName(employeeMaster.getEmployeeMiddleName());
				dtoEmployeeMaster.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
				dtoEmployeeMaster.setEmployeeMiddleNameArabic(employeeMaster.getEmployeeMiddleNameArabic());
				dtoEmployeeMaster.setEmployeeLastName(employeeMaster.getEmployeeLastName());
				dtoEmployeeMasterList.add(dtoEmployeeMaster);
			}
		}
		log.debug("Position is:"+dtoEmployeeMasterList.size());
		return dtoEmployeeMasterList;
	}

	public List<DtoEmployeeMaster> getAllEmployeeMasterInActiveList() {
		log.info("getAllEmployeeMaster InActive List  Method");
		List<DtoEmployeeMaster> dtoEmployeeMasterList = new ArrayList<>();
		List<EmployeeMaster> list = repositoryEmployeeMaster.findByIsDeletedAndEmployeeInactive(false, false);
		
		if (list != null && !list.isEmpty()) {
			for (EmployeeMaster employeeMaster : list) {
				DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
				dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
				dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
				dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
				dtoEmployeeMaster.setEmployeeMiddleName(employeeMaster.getEmployeeMiddleName());
				dtoEmployeeMaster.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
				dtoEmployeeMaster.setEmployeeMiddleNameArabic(employeeMaster.getEmployeeMiddleNameArabic());
				dtoEmployeeMaster.setIdNumber(employeeMaster.getIdNumber());
				dtoEmployeeMaster.setExpire(employeeMaster.getExpire());
				dtoEmployeeMaster.setExpires(employeeMaster.getExpires());
				dtoEmployeeMaster.setPassportNumber(employeeMaster.getPassportNumber());
				dtoEmployeeMaster.setEmployeeLastName(employeeMaster.getEmployeeLastName());
				dtoEmployeeMaster.setIsActive(employeeMaster.isEmployeeInactive());
				dtoEmployeeMaster.setEmployeeHireDate(employeeMaster.getEmployeeHireDate());
				dtoEmployeeMaster.setEmployeeAdjustHireDate(employeeMaster.getEmployeeAdjustHireDate());
				dtoEmployeeMaster.setDivision_Id(employeeMaster.getDivision().getDivisionId());
				dtoEmployeeMaster.setPosition_Id(employeeMaster.getPosition().getPositionId());
				dtoEmployeeMaster.setDepartment_Id(employeeMaster.getDepartment().getDepartmentId());
				dtoEmployeeMasterList.add(dtoEmployeeMaster);
			}
		}
		log.debug("Position is:"+dtoEmployeeMasterList.size());
		return dtoEmployeeMasterList;
	}
	
	public List<DtoEmployeeMasterHcm> employeeMasterInActiveList() {
		log.info("getAllEmployeeMaster InActive List  Method");
		List<DtoEmployeeMasterHcm> dtoEmployeeMasterList = new ArrayList<>();
		List<EmployeeMaster> list = repositoryEmployeeMaster.findByIsDeletedAndEmployeeInactive(false, false);
		
		if (list != null && !list.isEmpty()) {
			for (EmployeeMaster employeeMaster : list) {
				DtoEmployeeMasterHcm dtoEmployeeMaster = new DtoEmployeeMasterHcm();
				dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
				dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
				dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
				dtoEmployeeMaster.setEmployeeMiddleName(employeeMaster.getEmployeeMiddleName());
				dtoEmployeeMaster.setEmployeeLastName(employeeMaster.getEmployeeLastName());
				dtoEmployeeMasterList.add(dtoEmployeeMaster);
			}
		}
		log.debug("Position is:"+dtoEmployeeMasterList.size());
		return dtoEmployeeMasterList;
	}
	
	public DtoEmployeeMaster repeatByEmployeeId(String employeeId) {
		log.info("repeatByEmployeeId Method");
		DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
		try {
			List<EmployeeMaster> employeeMaster = repositoryEmployeeMaster.findByEmployeeId(employeeId);
			if (employeeMaster != null && !employeeMaster.isEmpty()) {
				dtoEmployeeMaster.setIsRepeat(true);
			} else {
				dtoEmployeeMaster.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeMaster;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchEmployee(DtoSearch dtoSearch) {
		log.info("searchEmployee Method");
		try {
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				String condition = "";
				
				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
					switch (dtoSearch.getSortOn()) {
					case "employeeId":
						condition += dtoSearch.getSortOn();
						break;
					case "employeeTitle":
						condition += dtoSearch.getSortOn();
						break;
					case "employeeTitleArabic":
						condition += dtoSearch.getSortOn();
						break;
					case "employeeLastName":
						condition += dtoSearch.getSortOn();
						break;
					case "employeeFirstName":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeMiddleName":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeLastNameArabic":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeFirstNameArabic":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeHireDate":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeLastWorkDate":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeAdjustHireDate":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeInactiveDate":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeType":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeGender":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeMaritalStatus":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeBirthDate":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeInactiveReason":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeUserIdInSystem":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeWorkHourYearly":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeCitizen":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeInactive":
						condition += dtoSearch.getSortOn();
						break;
						
					case "employeeImmigration":
						condition += dtoSearch.getSortOn();
						break;
					default:
						condition += "employeeIndexId";
					}
				} else {
					condition += "employeeIndexId";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
				dtoSearch.setTotalCount(
						this.repositoryEmployeeMaster.predictiveEmployeeMasterSearchTotalCount("%" + searchWord + "%"));
				List<EmployeeMaster> employeeMasterList = null;
				if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

					if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
						employeeMasterList = this.repositoryEmployeeMaster.predictiveEmployeeMasterSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, "employeeIndexId"));
					}
					if (dtoSearch.getSortBy().equals("ASC")) {
						employeeMasterList = this.repositoryEmployeeMaster.predictiveEmployeeMasterSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					} else if (dtoSearch.getSortBy().equals("DESC")) {
						employeeMasterList = this.repositoryEmployeeMaster.predictiveEmployeeMasterSearchWithPagination(
								"%" + searchWord + "%", new PageRequest(dtoSearch.getPageNumber(),
										dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}

				}
				if (employeeMasterList != null && !employeeMasterList.isEmpty()) {
					List<DtoEmployeeMaster> dtoEmployeeMasteList = new ArrayList<>();
					DtoEmployeeMaster dtoEmployeeMaster = null;
					for (EmployeeMaster employeeMaster : employeeMasterList) {
						dtoEmployeeMaster = new DtoEmployeeMaster(employeeMaster);
						dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
						dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
						dtoEmployeeMaster.setEmployeeTitle(employeeMaster.getEmployeeTitle());
						dtoEmployeeMaster.setEmployeeTitleArabic(employeeMaster.getEmployeeTitleArabic());
						dtoEmployeeMaster.setEmployeeLastNameArabic(employeeMaster.getEmployeeLastNameArabic());
						dtoEmployeeMaster.setEmployeeHireDate(employeeMaster.getEmployeeHireDate());
						dtoEmployeeMaster.setEmployeeAdjustHireDate(employeeMaster.getEmployeeAdjustHireDate());
						dtoEmployeeMaster.setEmployeeLastWorkDate(employeeMaster.getEmployeeLastWorkDate());
						dtoEmployeeMaster.setEmployeeInactiveDate(employeeMaster.getEmployeeInactiveDate());
						dtoEmployeeMaster.setEmployeeInactiveReason(employeeMaster.getEmployeeInactiveReason());
						dtoEmployeeMaster.setEmployeeType(employeeMaster.getEmployeeType());
						dtoEmployeeMaster.setEmployeeGender(employeeMaster.getEmployeeGender());
						dtoEmployeeMaster.setEmployeeMaritalStatus(employeeMaster.getEmployeeMaritalStatus());
						dtoEmployeeMaster.setEmployeeBirthDate(employeeMaster.getEmployeeBirthDate());
						dtoEmployeeMaster.setEmployeeCitizen(employeeMaster.isEmployeeCitizen());
						dtoEmployeeMaster.setEmployeeSmoker(employeeMaster.isEmployeeSmoker());
						dtoEmployeeMaster.setIdNumber(employeeMaster.getIdNumber());
						dtoEmployeeMaster.setExpire(employeeMaster.getExpire());
						dtoEmployeeMaster.setExpires(employeeMaster.getExpires());
						dtoEmployeeMaster.setPassportNumber(employeeMaster.getPassportNumber());
						dtoEmployeeMaster.setEmployeeUserIdInSystem(employeeMaster.getEmployeeUserIdInSystem());
						dtoEmployeeMaster.setEmployeeWorkHourYearly(employeeMaster.getEmployeeWorkHourYearly());
						dtoEmployeeMaster.setDivisionId(employeeMaster.getDivision().getId());
						dtoEmployeeMaster.setDepartmentId(employeeMaster.getDepartment().getId());
						if (employeeMaster.getProjectSetup() != null) {
							dtoEmployeeMaster.setProjectId(employeeMaster.getProjectSetup().getId());
							dtoEmployeeMaster.setProject_Id(employeeMaster.getProjectSetup().getProjectId());
							
						}
						dtoEmployeeMaster.setEmployeeImmigration(employeeMaster.isEmployeeImmigration());
						dtoEmployeeMaster.setLocationId(employeeMaster.getLocation().getId());
						dtoEmployeeMaster.setSupervisorId(employeeMaster.getSupervisor().getId());
						dtoEmployeeMaster.setPositionId(employeeMaster.getPosition().getId());
						dtoEmployeeMaster.setEmployeeNationalitiesId(
employeeMaster.getEmployeeNationalities().getEmployeeNationalityIndexId());
						dtoEmployeeMaster.setEmployeeAddressIndexId(
employeeMaster.getEmployeeAddressMaster().getEmployeeAddressIndexId());
						EmployeeAddressMaster employeeAddressMaster=repositoryEmployeeAddressMaster
.findOne(employeeMaster.getEmployeeAddressMaster().getEmployeeAddressIndexId());
						DtoEmployeeAddressMaster dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster(
employeeAddressMaster);
						dtoEmployeeAddressMaster
.setEmployeeAddressIndexId(employeeAddressMaster.getEmployeeAddressIndexId());
						dtoEmployeeAddressMaster.setAddressId(employeeAddressMaster.getAddressId());
						dtoEmployeeAddressMaster.setAddress1(employeeAddressMaster.getAddress1());
						dtoEmployeeAddressMaster.setAddress2(employeeAddressMaster.getAddress2());
						dtoEmployeeAddressMaster.setAddressArabic1(employeeAddressMaster.getAddressArabic1());
						dtoEmployeeAddressMaster.setAddressArabic2(employeeAddressMaster.getAddressArabic2());
						dtoEmployeeAddressMaster.setPersonalEmail(employeeAddressMaster.getPersonalEmail());
						dtoEmployeeAddressMaster.setBusinessEmail(employeeAddressMaster.getBusinessEmail());
						dtoEmployeeAddressMaster.setpOBox(employeeAddressMaster.getpOBox());
						dtoEmployeeAddressMaster.setPostCode(employeeAddressMaster.getPostCode());
						dtoEmployeeAddressMaster.setPersonalPhone(employeeAddressMaster.getPersonalPhone());
						dtoEmployeeAddressMaster.setBusinessPhone(employeeAddressMaster.getBusinessPhone());
						dtoEmployeeAddressMaster.setOtherPhone(employeeAddressMaster.getOtherPhone());
						dtoEmployeeAddressMaster.setExt(employeeAddressMaster.getExt());
						dtoEmployeeAddressMaster
.setLocationLinkGoogleMap(employeeAddressMaster.getLocationLinkGoogleMap());
						dtoEmployeeAddressMaster.setStateId(employeeAddressMaster.getState().getStateId());
						dtoEmployeeAddressMaster.setStateName(employeeAddressMaster.getState().getStateName());
						
						dtoEmployeeAddressMaster.setCountryId(employeeAddressMaster.getCountry().getCountryId());
						dtoEmployeeAddressMaster.setCountryName(employeeAddressMaster.getCountry().getCountryName());
						
						dtoEmployeeMaster.setExpireHijri(employeeMaster.getExpireHijri());
						dtoEmployeeMaster.setEmployeeBirthDateH(employeeMaster.getEmployeeBirthDateHijri());
						dtoEmployeeMaster.setExpiresHijri(employeeMaster.getExpiresHijri());
						dtoEmployeeAddressMaster.setCityId(employeeAddressMaster.getCity().getCityId());
						dtoEmployeeAddressMaster.setCityName(employeeAddressMaster.getCity().getCityName());
						
						dtoEmployeeMaster.setDtoEmployeeAddressMaster(dtoEmployeeAddressMaster);
						EmployeeNationalities employeeNationalities = repositoryEmployeeNationalities
.findOne(employeeMaster.getEmployeeNationalities().getEmployeeNationalityIndexId());
						DtoEmployeeNationalities dtoEmployeeNationalities = new DtoEmployeeNationalities(
employeeNationalities);
						dtoEmployeeNationalities
.setEmployeeNationalityIndexId(employeeNationalities.getEmployeeNationalityIndexId());
						dtoEmployeeNationalities.setEmployeeNationalityDescriptionArabic(
employeeNationalities.getEmployeeNationalityDescriptionArabic());
						dtoEmployeeMaster.setDtoEmployeeNationalities(dtoEmployeeNationalities);
						
						if(employeeMaster.getDepartment()!=null) {
							Department department = repositoryDepartment
.findOne(employeeMaster.getDepartment().getId());
							DtoDepartment dtoDepartment = new DtoDepartment();
							dtoDepartment.setId(department.getId());
							dtoDepartment.setArabicDepartmentDescription(department.getArabicDepartmentDescription());
							dtoDepartment.setDepartmentId(department.getDepartmentId());
							dtoDepartment.setDepartmentDescription(department.getDepartmentDescription());
							dtoEmployeeMaster.setDtoDepartment(dtoDepartment);
						}
						
						if(employeeMaster.getLocation()!=null) {
							Location location = repositoryLocation.findOne(employeeMaster.getLocation().getId());
							DtoLocation dtoLocation = new DtoLocation();
							dtoLocation.setDescription(location.getDescription());
							dtoLocation.setArabicDescription(location.getArabicDescription());
							dtoLocation.setContactName(location.getContactName());
							dtoLocation.setId(location.getId());
							dtoLocation.setFax(location.getFax());
							dtoLocation.setLocationAddress(location.getLocationAddress());
							dtoLocation.setLocationId(location.getLocationId());
							dtoEmployeeMaster.setDtoLocation(dtoLocation);
						}
						
						if(employeeMaster.getDivision()!=null) {
							Division division = repositoryDivision.findOne(employeeMaster.getDivision().getId());
							DtoDivision division2 = new DtoDivision();
							division2.setDivisionAddress(division.getDivisionAddress());
							division2.setArabicDivisionDescription(division.getArabicDivisionDescription());
							division2.setDivisionDescription(division.getDivisionDescription());
							division2.setId(division.getId());
							division2.setDivisionId(division.getDivisionId());
							dtoEmployeeMaster.setDtoDivision(division2);
						}
						
						if(employeeMaster.getSupervisor()!=null) {
							Supervisor supervisor =	repositorySupervisor
.findOne(employeeMaster.getSupervisor().getId());
							DtoSupervisor dtoSupervisor = new DtoSupervisor();
							dtoSupervisor.setDescription(supervisor.getDescription());
							dtoSupervisor.setArabicDescription(supervisor.getArabicDescription());
							dtoSupervisor.setSuperVisionCode(supervisor.getSuperVisionCode());
							dtoSupervisor.setId(supervisor.getId());
							dtoEmployeeMaster.setDtoSupervisor(dtoSupervisor);
						}
						
						if(employeeMaster.getPosition()!=null) {
							Position position = repositoryPosition.findOne(employeeMaster.getPosition().getId());
							DtoPosition dtoPosition = new DtoPosition();
							
							dtoPosition.setReportToPostion(position.getReportToPostion());
							dtoPosition.setDescription(position.getDescription());
							dtoPosition.setReportToPostion(position.getReportToPostion());
							dtoPosition.setPositionId(position.getPositionId());
							dtoPosition.setArabicDescription(position.getArabicDescription());
							dtoPosition.setId(position.getId());
							dtoEmployeeMaster.setDtoPosition(dtoPosition);
						}
					
						if (employeeMaster.getBank() != null) {
							
							Bank bank = repositoryBank.findOne(employeeMaster.getBank().getId());
							DtoBank dtoBank = new DtoBank();
							
							dtoBank.setId(bank.getId());
							dtoBank.setBankName(bank.getBankName());
							dtoBank.setBankDescription(bank.getBankDescription());
							dtoBank.setBankDescriptionArabic(bank.getBankDescriptionArabic());
							dtoBank.setAccountNumber(bank.getAccountNumber());
							dtoBank.setSwiftCode(bank.getSwiftCode());
							dtoEmployeeMaster.setBankId(dtoBank.getId());
							dtoEmployeeMaster.setDtoBank(dtoBank);
						}
						if (employeeMaster.getProjectSetup() != null) {
							ProjectSetup projectSetup = repositoryProjectSetup
									.findOne(employeeMaster.getProjectSetup().getId());
							DtoProjectSetup dtoProjectSetup = new DtoProjectSetup();
							dtoProjectSetup.setProjectId(projectSetup.getProjectId());
							dtoProjectSetup.setProjectName(projectSetup.getProjectName());
							dtoProjectSetup.setProjectArabicName(projectSetup.getProjectArabicName());
							dtoProjectSetup.setProjectDescription(projectSetup.getProjectDescription());
							dtoProjectSetup.setProjectArabicDescription(projectSetup.getProjectArabicDescription());
							dtoEmployeeMaster.setDtoProjectSetup(dtoProjectSetup);
						}
				
						dtoEmployeeMaster.setEmployeeImage(employeeMaster.getEmployeeImage());
						dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
						dtoEmployeeMaster.setEmployeeMiddleName(employeeMaster.getEmployeeMiddleName());
						dtoEmployeeMaster.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
						dtoEmployeeMaster.setEmployeeMiddleNameArabic(employeeMaster.getEmployeeMiddleNameArabic());
						dtoEmployeeMaster.setEmployeeLastName(employeeMaster.getEmployeeLastName());
						dtoEmployeeMaster.setEmployeeInactive(employeeMaster.getEmployeeInactive());
						dtoEmployeeMaster.setDivision_Id(employeeMaster.getDivision().getDivisionId());
						dtoEmployeeMaster.setPosition_Id(employeeMaster.getPosition().getPositionId());
						dtoEmployeeMaster.setDepartment_Id(employeeMaster.getDepartment().getDepartmentId());
						
						dtoEmployeeMaster.setEmployeeNotes(employeeMaster.getEmployeeNotes());
						dtoEmployeeMaster.setEmployeeEmail(employeeMaster.getEmployeeEmail());
						dtoEmployeeMaster.setEmployeeBankAccountNumber(employeeMaster.getEmployeeBankAccountNumber());

						dtoEmployeeMasteList.add(dtoEmployeeMaster);
					}
					dtoSearch.setRecords(dtoEmployeeMasteList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		log.debug("Search EmployeeMaster Size is:" + dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllEmployeeId(DtoSearch dtoSearch) {

		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
					String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("employeeId") || dtoSearch.getSortOn().equals("employeeTitle") 
|| dtoSearch.getSortOn().equals("employeeTitleArabic") 
|| dtoSearch.getSortOn().equals("employeeInactiveReason") 
|| dtoSearch.getSortOn().equals("employeeMiddleNameArabic")
|| dtoSearch.getSortOn().equals("employeeFirstNameArabic") 
|| dtoSearch.getSortOn().equals("employeeLastNameArabic")
|| dtoSearch.getSortOn().equals("employeeLastName")) {
						
						     if(dtoSearch.getSortOn().equals("cityName")) {
						    	 dtoSearch.setSortOn("city");
						     }
						    	 
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	
				
				dtoSearch.setTotalCount(
this.repositoryEmployeeMaster.predictiveEmployeeIdSearchTotalCount("%"+searchWord+"%"));
				List<EmployeeMaster> employeeMasterList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeeMasterList =this.repositoryEmployeeMaster.predictiveEmployeeIdSearchWithPagination(
"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), 
dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeeMasterList = this.repositoryEmployeeMaster.predictiveEmployeeIdSearchWithPagination(
"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), 
dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeeMasterList = this.repositoryEmployeeMaster.predictiveEmployeeIdSearchWithPagination(
"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), 
dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
				if(employeeMasterList != null && !employeeMasterList.isEmpty()){
					List<DtoEmployeeMasterHcm> dtoEmployeeMasterList = new ArrayList<>();
					for (EmployeeMaster employeeMaster : employeeMasterList) {
						DtoEmployeeMasterHcm dtoEmployeeMaster = new DtoEmployeeMasterHcm();
						if(employeeMaster.getEmployeeInactive()==false) {
						dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
						dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
						dtoEmployeeMaster.setEmployeeFirstName(
employeeMaster.getEmployeeFirstName() + " " +employeeMaster.getEmployeeMiddleName()
 + " " +employeeMaster.getEmployeeLastName());
						dtoEmployeeMasterList.add(dtoEmployeeMaster);
					}
					}
					dtoSearch.setRecords(dtoEmployeeMasterList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllEmployeeName(DtoSearch dtoSearch) {

		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
					String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("employeeId") || dtoSearch.getSortOn().equals("employeeTitle") 
|| dtoSearch.getSortOn().equals("employeeTitleArabic") 
							|| dtoSearch.getSortOn().equals("employeeInactiveReason") 
|| dtoSearch.getSortOn().equals("employeeMiddleNameArabic") 
|| dtoSearch.getSortOn().equals("employeeFirstNameArabic") 
|| dtoSearch.getSortOn().equals("employeeLastNameArabic")
|| dtoSearch.getSortOn().equals("employeeLastName")) {
						
						     if(dtoSearch.getSortOn().equals("cityName")) {
						    	 dtoSearch.setSortOn("city");
						     }
						    	 
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	
				
				dtoSearch.setTotalCount(
						this.repositoryEmployeeMaster.predictiveEmployeeNameSearchTotalCount("%" + searchWord + "%"));
				List<EmployeeMaster> employeeMasterList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeeMasterList =this.repositoryEmployeeMaster.predictiveEmployeeNameSearchWithPagination(
"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(),
 dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeeMasterList = this.repositoryEmployeeMaster.predictiveEmployeeNameSearchWithPagination(
"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), 
dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeeMasterList = this.repositoryEmployeeMaster.predictiveEmployeeNameSearchWithPagination(
"%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(),
 dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
				if(employeeMasterList != null && !employeeMasterList.isEmpty()){
					List<DtoEmployeeMasterHcm> dtoEmployeeMasterList = new ArrayList<>();
					for (EmployeeMaster employeeMaster : employeeMasterList) {
						DtoEmployeeMasterHcm dtoEmployeeMaster = new DtoEmployeeMasterHcm();
						dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
						dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
						
						dtoEmployeeMasterList.add(dtoEmployeeMaster);
					}
					dtoSearch.setRecords(dtoEmployeeMasterList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	@Transactional
	@Async
	public List<DtoEmployeeMasterHcm> getAllByEmployeeForDepartmentList(DtoDepartment dtoDepartment) {
		
		List<DtoEmployeeMasterHcm> list = new ArrayList<>();
		List<EmployeeMaster> employeeMaster2=repositoryEmployeeMaster
.findByEmployeeByDepartmentList(dtoDepartment.getIds());
		for (EmployeeMaster employeeMaster : employeeMaster2) {
			if(employeeMaster.getEmployeeInactive()==false) {
			DtoEmployeeMasterHcm dtoEmployeeMasterHcm = new DtoEmployeeMasterHcm();
			dtoEmployeeMasterHcm.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
			dtoEmployeeMasterHcm.setEmployeeId(employeeMaster.getEmployeeId());
			dtoEmployeeMasterHcm.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
			dtoEmployeeMasterHcm.setEmployeeLastName(employeeMaster.getEmployeeLastName());
				list.add(dtoEmployeeMasterHcm);
			}
			
		}
		return list;
	}
	
	public DtoEmployeeMaster getEmployeeById(DtoEmployeeMaster dtoEmployeeMaster) {
		try {
			EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOne(dtoEmployeeMaster.getEmployeeIndexId());
			if(employeeMaster!=null) {

				dtoEmployeeMaster = new DtoEmployeeMaster(employeeMaster);
				dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
				dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
				dtoEmployeeMaster.setEmployeeTitle(employeeMaster.getEmployeeTitle());
				dtoEmployeeMaster.setEmployeeTitleArabic(employeeMaster.getEmployeeTitleArabic());
				dtoEmployeeMaster.setEmployeeLastNameArabic(employeeMaster.getEmployeeLastNameArabic());
				dtoEmployeeMaster.setEmployeeHireDate(employeeMaster.getEmployeeHireDate());
				dtoEmployeeMaster.setEmployeeAdjustHireDate(employeeMaster.getEmployeeAdjustHireDate());
				dtoEmployeeMaster.setEmployeeLastWorkDate(employeeMaster.getEmployeeLastWorkDate());
				dtoEmployeeMaster.setEmployeeInactiveDate(employeeMaster.getEmployeeInactiveDate());
				dtoEmployeeMaster.setEmployeeInactiveReason(employeeMaster.getEmployeeInactiveReason());
				dtoEmployeeMaster.setEmployeeType(employeeMaster.getEmployeeType());
				dtoEmployeeMaster.setEmployeeGender(employeeMaster.getEmployeeGender());
				dtoEmployeeMaster.setEmployeeMaritalStatus(employeeMaster.getEmployeeMaritalStatus());
				dtoEmployeeMaster.setEmployeeBirthDate(employeeMaster.getEmployeeBirthDate());
				dtoEmployeeMaster.setEmployeeCitizen(employeeMaster.isEmployeeCitizen());
				dtoEmployeeMaster.setEmployeeSmoker(employeeMaster.isEmployeeSmoker());
				dtoEmployeeMaster.setIdNumber(employeeMaster.getIdNumber());
				dtoEmployeeMaster.setExpire(employeeMaster.getExpire());
				dtoEmployeeMaster.setExpires(employeeMaster.getExpires());
				dtoEmployeeMaster.setPassportNumber(employeeMaster.getPassportNumber());
				dtoEmployeeMaster.setEmployeeUserIdInSystem(employeeMaster.getEmployeeUserIdInSystem());
				dtoEmployeeMaster.setEmployeeWorkHourYearly(employeeMaster.getEmployeeWorkHourYearly());
				dtoEmployeeMaster.setDivisionId(employeeMaster.getDivision().getId());
				dtoEmployeeMaster.setDepartmentId(employeeMaster.getDepartment().getId());
				if (employeeMaster.getProjectSetup() != null) {
					dtoEmployeeMaster.setProjectId(employeeMaster.getProjectSetup().getId());
					dtoEmployeeMaster.setProject_Id(employeeMaster.getProjectSetup().getProjectId());
					
				}
				dtoEmployeeMaster.setEmployeeImmigration(employeeMaster.isEmployeeImmigration());
				dtoEmployeeMaster.setLocationId(employeeMaster.getLocation().getId());
				dtoEmployeeMaster.setSupervisorId(employeeMaster.getSupervisor().getId());
				dtoEmployeeMaster.setPositionId(employeeMaster.getPosition().getId());
				dtoEmployeeMaster.setEmployeeNationalitiesId(employeeMaster.getEmployeeNationalities().getEmployeeNationalityIndexId());
				dtoEmployeeMaster.setEmployeeAddressIndexId(employeeMaster.getEmployeeAddressMaster().getEmployeeAddressIndexId());
				EmployeeAddressMaster employeeAddressMaster=repositoryEmployeeAddressMaster.findOne(employeeMaster.getEmployeeAddressMaster().getEmployeeAddressIndexId());
				DtoEmployeeAddressMaster dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster(employeeAddressMaster);
				dtoEmployeeAddressMaster.setEmployeeAddressIndexId(employeeAddressMaster.getEmployeeAddressIndexId());
				dtoEmployeeAddressMaster.setAddressId(employeeAddressMaster.getAddressId());
				dtoEmployeeAddressMaster.setAddress1(employeeAddressMaster.getAddress1());
				dtoEmployeeAddressMaster.setAddress2(employeeAddressMaster.getAddress2());
				dtoEmployeeAddressMaster.setAddressArabic1(employeeAddressMaster.getAddressArabic1());
				dtoEmployeeAddressMaster.setAddressArabic2(employeeAddressMaster.getAddressArabic2());
				dtoEmployeeAddressMaster.setPersonalEmail(employeeAddressMaster.getPersonalEmail());
				dtoEmployeeAddressMaster.setBusinessEmail(employeeAddressMaster.getBusinessEmail());
				dtoEmployeeAddressMaster.setpOBox(employeeAddressMaster.getpOBox());
				dtoEmployeeAddressMaster.setPostCode(employeeAddressMaster.getPostCode());
				dtoEmployeeAddressMaster.setPersonalPhone(employeeAddressMaster.getPersonalPhone());
				dtoEmployeeAddressMaster.setBusinessPhone(employeeAddressMaster.getBusinessPhone());
				dtoEmployeeAddressMaster.setOtherPhone(employeeAddressMaster.getOtherPhone());
				dtoEmployeeAddressMaster.setExt(employeeAddressMaster.getExt());
				dtoEmployeeAddressMaster.setLocationLinkGoogleMap(employeeAddressMaster.getLocationLinkGoogleMap());
				dtoEmployeeAddressMaster.setStateId(employeeAddressMaster.getState().getStateId());
				dtoEmployeeAddressMaster.setStateName(employeeAddressMaster.getState().getStateName());
				
				dtoEmployeeAddressMaster.setCountryId(employeeAddressMaster.getCountry().getCountryId());
				dtoEmployeeAddressMaster.setCountryName(employeeAddressMaster.getCountry().getCountryName());
				
				dtoEmployeeAddressMaster.setCityId(employeeAddressMaster.getCity().getCityId());
				dtoEmployeeAddressMaster.setCityName(employeeAddressMaster.getCity().getCityName());
				
				dtoEmployeeMaster.setDtoEmployeeAddressMaster(dtoEmployeeAddressMaster);
				EmployeeNationalities employeeNationalities = repositoryEmployeeNationalities
.findOne(employeeMaster.getEmployeeNationalities().getEmployeeNationalityIndexId());
				DtoEmployeeNationalities dtoEmployeeNationalities = new DtoEmployeeNationalities(employeeNationalities);
				dtoEmployeeNationalities
.setEmployeeNationalityIndexId(employeeNationalities.getEmployeeNationalityIndexId());
				dtoEmployeeNationalities.setEmployeeNationalityDescriptionArabic(
employeeNationalities.getEmployeeNationalityDescriptionArabic());
				dtoEmployeeMaster.setDtoEmployeeNationalities(dtoEmployeeNationalities);
				
				if(employeeMaster.getDepartment()!=null) {
					Department department = repositoryDepartment.findOne(employeeMaster.getDepartment().getId());
					DtoDepartment dtoDepartment = new DtoDepartment();
					dtoDepartment.setId(department.getId());
					dtoDepartment.setArabicDepartmentDescription(department.getArabicDepartmentDescription());
					dtoDepartment.setDepartmentId(department.getDepartmentId());
					dtoDepartment.setDepartmentDescription(department.getDepartmentDescription());
					dtoEmployeeMaster.setDtoDepartment(dtoDepartment);
				}
				
				if(employeeMaster.getLocation()!=null) {
					Location location = repositoryLocation.findOne(employeeMaster.getLocation().getId());
					DtoLocation dtoLocation = new DtoLocation();
					dtoLocation.setDescription(location.getDescription());
					dtoLocation.setArabicDescription(location.getArabicDescription());
					dtoLocation.setContactName(location.getContactName());
					dtoLocation.setId(location.getId());
					dtoLocation.setFax(location.getFax());
					dtoLocation.setLocationAddress(location.getLocationAddress());
					dtoLocation.setLocationId(location.getLocationId());
					dtoEmployeeMaster.setDtoLocation(dtoLocation);
				}
				
				if(employeeMaster.getDivision()!=null) {
					Division division = repositoryDivision.findOne(employeeMaster.getDivision().getId());
					DtoDivision division2 = new DtoDivision();
					division2.setDivisionAddress(division.getDivisionAddress());
					division2.setArabicDivisionDescription(division.getArabicDivisionDescription());
					division2.setDivisionDescription(division.getDivisionDescription());
					division2.setId(division.getId());
					division2.setDivisionId(division.getDivisionId());
					dtoEmployeeMaster.setDtoDivision(division2);
				}
				
				if(employeeMaster.getSupervisor()!=null) {
					Supervisor supervisor =	repositorySupervisor.findOne(employeeMaster.getSupervisor().getId());
					DtoSupervisor dtoSupervisor = new DtoSupervisor();
					dtoSupervisor.setDescription(supervisor.getDescription());
					dtoSupervisor.setArabicDescription(supervisor.getArabicDescription());
					dtoSupervisor.setSuperVisionCode(supervisor.getSuperVisionCode());
					dtoSupervisor.setId(supervisor.getId());
					dtoEmployeeMaster.setDtoSupervisor(dtoSupervisor);
				}
				
				if(employeeMaster.getPosition()!=null) {
					Position position = repositoryPosition.findOne(employeeMaster.getPosition().getId());
					DtoPosition dtoPosition = new DtoPosition();
					
					dtoPosition.setReportToPostion(position.getReportToPostion());
					dtoPosition.setDescription(position.getDescription());
					dtoPosition.setReportToPostion(position.getReportToPostion());
					dtoPosition.setPositionId(position.getPositionId());
					dtoPosition.setArabicDescription(position.getArabicDescription());
					dtoPosition.setId(position.getId());
					dtoEmployeeMaster.setDtoPosition(dtoPosition);
				}
			
				if (employeeMaster.getBank() != null) {
					
					Bank bank = repositoryBank.findOne(employeeMaster.getBank().getId());
					DtoBank dtoBank = new DtoBank();
					
					dtoBank.setId(bank.getId());
					dtoBank.setBankName(bank.getBankName());
					dtoBank.setBankDescription(bank.getBankDescription());
					dtoBank.setBankDescriptionArabic(bank.getBankDescriptionArabic());
					dtoBank.setAccountNumber(bank.getAccountNumber());
					dtoBank.setSwiftCode(bank.getSwiftCode());
					dtoEmployeeMaster.setBankId(dtoBank.getId());
					dtoEmployeeMaster.setDtoBank(dtoBank);
				}
		
				if (employeeMaster.getProjectSetup() != null) {
					ProjectSetup projectSetup = repositoryProjectSetup
							.findOne(employeeMaster.getProjectSetup().getId());
					DtoProjectSetup dtoProjectSetup = new DtoProjectSetup();
					dtoProjectSetup.setProjectId(projectSetup.getProjectId());
					dtoProjectSetup.setProjectName(projectSetup.getProjectName());
					dtoProjectSetup.setProjectArabicName(projectSetup.getProjectArabicName());
					dtoProjectSetup.setProjectDescription(projectSetup.getProjectDescription());
					dtoProjectSetup.setProjectArabicDescription(projectSetup.getProjectArabicDescription());
					dtoEmployeeMaster.setDtoProjectSetup(dtoProjectSetup);

				}
		
				dtoEmployeeMaster.setEmployeeImage(employeeMaster.getEmployeeImage());
				dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
				dtoEmployeeMaster.setEmployeeMiddleName(employeeMaster.getEmployeeMiddleName());
				dtoEmployeeMaster.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
				dtoEmployeeMaster.setEmployeeMiddleNameArabic(employeeMaster.getEmployeeMiddleNameArabic());
				dtoEmployeeMaster.setEmployeeLastName(employeeMaster.getEmployeeLastName());
				dtoEmployeeMaster.setEmployeeInactive(employeeMaster.getEmployeeInactive());
				dtoEmployeeMaster.setDivision_Id(employeeMaster.getDivision().getDivisionId());
				dtoEmployeeMaster.setPosition_Id(employeeMaster.getPosition().getPositionId());
				dtoEmployeeMaster.setDepartment_Id(employeeMaster.getDepartment().getDepartmentId());
				
				dtoEmployeeMaster.setEmployeeNotes(employeeMaster.getEmployeeNotes());
				dtoEmployeeMaster.setEmployeeEmail(employeeMaster.getEmployeeEmail());
				dtoEmployeeMaster.setEmployeeBankAccountNumber(employeeMaster.getEmployeeBankAccountNumber());
			
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		 
		
		
		return dtoEmployeeMaster;
		
	}
	
	public DtoEmployeeMaster getEmployeeByCode(String employeeCode) {
		DtoEmployeeMaster dtoEmployeeMaster = null;
		try {
			EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOneByEmployeeId(employeeCode);
			if(employeeMaster!=null) {

				dtoEmployeeMaster = new DtoEmployeeMaster(employeeMaster);
				dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
				dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
				dtoEmployeeMaster.setEmployeeTitle(employeeMaster.getEmployeeTitle());
				dtoEmployeeMaster.setEmployeeTitleArabic(employeeMaster.getEmployeeTitleArabic());
				dtoEmployeeMaster.setEmployeeLastNameArabic(employeeMaster.getEmployeeLastNameArabic());
				dtoEmployeeMaster.setEmployeeHireDate(employeeMaster.getEmployeeHireDate());
				dtoEmployeeMaster.setEmployeeAdjustHireDate(employeeMaster.getEmployeeAdjustHireDate());
				dtoEmployeeMaster.setEmployeeLastWorkDate(employeeMaster.getEmployeeLastWorkDate());
				dtoEmployeeMaster.setEmployeeInactiveDate(employeeMaster.getEmployeeInactiveDate());
				dtoEmployeeMaster.setEmployeeInactiveReason(employeeMaster.getEmployeeInactiveReason());
				dtoEmployeeMaster.setEmployeeType(employeeMaster.getEmployeeType());
				dtoEmployeeMaster.setEmployeeGender(employeeMaster.getEmployeeGender());
				dtoEmployeeMaster.setEmployeeMaritalStatus(employeeMaster.getEmployeeMaritalStatus());
				dtoEmployeeMaster.setEmployeeBirthDate(employeeMaster.getEmployeeBirthDate());
				dtoEmployeeMaster.setEmployeeCitizen(employeeMaster.isEmployeeCitizen());
				dtoEmployeeMaster.setEmployeeSmoker(employeeMaster.isEmployeeSmoker());
				dtoEmployeeMaster.setIdNumber(employeeMaster.getIdNumber());
				dtoEmployeeMaster.setExpire(employeeMaster.getExpire());
				dtoEmployeeMaster.setExpires(employeeMaster.getExpires());
				dtoEmployeeMaster.setPassportNumber(employeeMaster.getPassportNumber());
				dtoEmployeeMaster.setEmployeeUserIdInSystem(employeeMaster.getEmployeeUserIdInSystem());
				dtoEmployeeMaster.setEmployeeWorkHourYearly(employeeMaster.getEmployeeWorkHourYearly());
				dtoEmployeeMaster.setDivisionId(employeeMaster.getDivision().getId());
				dtoEmployeeMaster.setDepartmentId(employeeMaster.getDepartment().getId());
				dtoEmployeeMaster.setEmployeeImmigration(employeeMaster.isEmployeeImmigration());
				dtoEmployeeMaster.setLocationId(employeeMaster.getLocation().getId());
				dtoEmployeeMaster.setSupervisorId(employeeMaster.getSupervisor().getId());
				dtoEmployeeMaster.setPositionId(employeeMaster.getPosition().getId());
				dtoEmployeeMaster.setEmployeeNationalitiesId(employeeMaster.getEmployeeNationalities().getEmployeeNationalityIndexId());
				dtoEmployeeMaster.setEmployeeAddressIndexId(employeeMaster.getEmployeeAddressMaster().getEmployeeAddressIndexId());
				EmployeeAddressMaster employeeAddressMaster=repositoryEmployeeAddressMaster.findOne(employeeMaster.getEmployeeAddressMaster().getEmployeeAddressIndexId());
				DtoEmployeeAddressMaster dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster(employeeAddressMaster);
				dtoEmployeeAddressMaster.setEmployeeAddressIndexId(employeeAddressMaster.getEmployeeAddressIndexId());
				dtoEmployeeAddressMaster.setAddressId(employeeAddressMaster.getAddressId());
				dtoEmployeeAddressMaster.setAddress1(employeeAddressMaster.getAddress1());
				dtoEmployeeAddressMaster.setAddress2(employeeAddressMaster.getAddress2());
				dtoEmployeeAddressMaster.setAddressArabic1(employeeAddressMaster.getAddressArabic1());
				dtoEmployeeAddressMaster.setAddressArabic2(employeeAddressMaster.getAddressArabic2());
				dtoEmployeeAddressMaster.setPersonalEmail(employeeAddressMaster.getPersonalEmail());
				dtoEmployeeAddressMaster.setBusinessEmail(employeeAddressMaster.getBusinessEmail());
				dtoEmployeeAddressMaster.setpOBox(employeeAddressMaster.getpOBox());
				dtoEmployeeAddressMaster.setPostCode(employeeAddressMaster.getPostCode());
				dtoEmployeeAddressMaster.setPersonalPhone(employeeAddressMaster.getPersonalPhone());
				dtoEmployeeAddressMaster.setBusinessPhone(employeeAddressMaster.getBusinessPhone());
				dtoEmployeeAddressMaster.setOtherPhone(employeeAddressMaster.getOtherPhone());
				dtoEmployeeAddressMaster.setExt(employeeAddressMaster.getExt());
				dtoEmployeeAddressMaster.setLocationLinkGoogleMap(employeeAddressMaster.getLocationLinkGoogleMap());
				dtoEmployeeAddressMaster.setStateId(employeeAddressMaster.getState().getStateId());
				dtoEmployeeAddressMaster.setStateName(employeeAddressMaster.getState().getStateName());
				
				dtoEmployeeAddressMaster.setCountryId(employeeAddressMaster.getCountry().getCountryId());
				dtoEmployeeAddressMaster.setCountryName(employeeAddressMaster.getCountry().getCountryName());
				
				
				dtoEmployeeAddressMaster.setCityId(employeeAddressMaster.getCity().getCityId());
				dtoEmployeeAddressMaster.setCityName(employeeAddressMaster.getCity().getCityName());
				
				dtoEmployeeMaster.setDtoEmployeeAddressMaster(dtoEmployeeAddressMaster);
				EmployeeNationalities employeeNationalities = repositoryEmployeeNationalities.findOne(employeeMaster.getEmployeeNationalities().getEmployeeNationalityIndexId());
				DtoEmployeeNationalities dtoEmployeeNationalities = new DtoEmployeeNationalities(employeeNationalities);
				dtoEmployeeNationalities.setEmployeeNationalityIndexId(employeeNationalities.getEmployeeNationalityIndexId());
				dtoEmployeeNationalities.setEmployeeNationalityDescriptionArabic(employeeNationalities.getEmployeeNationalityDescriptionArabic());
				dtoEmployeeMaster.setDtoEmployeeNationalities(dtoEmployeeNationalities);
				
				if(employeeMaster.getDepartment()!=null) {
					Department department = repositoryDepartment.findOne(employeeMaster.getDepartment().getId());
					DtoDepartment dtoDepartment = new DtoDepartment();
					dtoDepartment.setId(department.getId());
					dtoDepartment.setArabicDepartmentDescription(department.getArabicDepartmentDescription());
					dtoDepartment.setDepartmentId(department.getDepartmentId());
					dtoDepartment.setDepartmentDescription(department.getDepartmentDescription());
					dtoEmployeeMaster.setDtoDepartment(dtoDepartment);
				}
				
				if(employeeMaster.getLocation()!=null) {
					Location location = repositoryLocation.findOne(employeeMaster.getLocation().getId());
					DtoLocation dtoLocation = new DtoLocation();
					dtoLocation.setDescription(location.getDescription());
					dtoLocation.setArabicDescription(location.getArabicDescription());
					dtoLocation.setContactName(location.getContactName());
					dtoLocation.setId(location.getId());
					dtoLocation.setFax(location.getFax());
					dtoLocation.setLocationAddress(location.getLocationAddress());
					dtoLocation.setLocationId(location.getLocationId());
					dtoEmployeeMaster.setDtoLocation(dtoLocation);
				}
				
				if(employeeMaster.getDivision()!=null) {
					Division division = repositoryDivision.findOne(employeeMaster.getDivision().getId());
					DtoDivision division2 = new DtoDivision();
					division2.setDivisionAddress(division.getDivisionAddress());
					division2.setArabicDivisionDescription(division.getArabicDivisionDescription());
					division2.setDivisionDescription(division.getDivisionDescription());
					division2.setId(division.getId());
					division2.setDivisionId(division.getDivisionId());
					dtoEmployeeMaster.setDtoDivision(division2);
				}
				
				if(employeeMaster.getSupervisor()!=null) {
					Supervisor supervisor =	repositorySupervisor.findOne(employeeMaster.getSupervisor().getId());
					DtoSupervisor dtoSupervisor = new DtoSupervisor();
					dtoSupervisor.setDescription(supervisor.getDescription());
					dtoSupervisor.setArabicDescription(supervisor.getArabicDescription());
					dtoSupervisor.setSuperVisionCode(supervisor.getSuperVisionCode());
					dtoSupervisor.setId(supervisor.getId());
					dtoEmployeeMaster.setDtoSupervisor(dtoSupervisor);
				}
				
		
				if(employeeMaster.getPosition()!=null) {
					Position position = repositoryPosition.findOne(employeeMaster.getPosition().getId());
					DtoPosition dtoPosition = new DtoPosition();
					
					dtoPosition.setReportToPostion(position.getReportToPostion());
					dtoPosition.setDescription(position.getDescription());
					dtoPosition.setReportToPostion(position.getReportToPostion());
					dtoPosition.setPositionId(position.getPositionId());
					dtoPosition.setArabicDescription(position.getArabicDescription());
					dtoPosition.setId(position.getId());
					dtoEmployeeMaster.setDtoPosition(dtoPosition);
				}
			
				
				if (employeeMaster.getBank() != null) {
					
					Bank bank = repositoryBank.findOne(employeeMaster.getBank().getId());
					DtoBank dtoBank = new DtoBank();
					
					dtoBank.setId(bank.getId());
					dtoBank.setBankName(bank.getBankName());
					dtoBank.setBankDescription(bank.getBankDescription());
					dtoBank.setBankDescriptionArabic(bank.getBankDescriptionArabic());
					dtoBank.setAccountNumber(bank.getAccountNumber());
					dtoBank.setSwiftCode(bank.getSwiftCode());
					dtoEmployeeMaster.setBankId(dtoBank.getId());
					dtoEmployeeMaster.setDtoBank(dtoBank);
				}
		
		
				dtoEmployeeMaster.setEmployeeImage(employeeMaster.getEmployeeImage());
				dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
				dtoEmployeeMaster.setEmployeeMiddleName(employeeMaster.getEmployeeMiddleName());
				dtoEmployeeMaster.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
				dtoEmployeeMaster.setEmployeeMiddleNameArabic(employeeMaster.getEmployeeMiddleNameArabic());
				dtoEmployeeMaster.setEmployeeLastName(employeeMaster.getEmployeeLastName());
				dtoEmployeeMaster.setEmployeeInactive(employeeMaster.getEmployeeInactive());
				dtoEmployeeMaster.setDivision_Id(employeeMaster.getDivision().getDivisionId());
				dtoEmployeeMaster.setPosition_Id(employeeMaster.getPosition().getPositionId());
				dtoEmployeeMaster.setDepartment_Id(employeeMaster.getDepartment().getDepartmentId());
				
				dtoEmployeeMaster.setEmployeeNotes(employeeMaster.getEmployeeNotes());
				dtoEmployeeMaster.setEmployeeEmail(employeeMaster.getEmployeeEmail());
				dtoEmployeeMaster.setEmployeeBankAccountNumber(employeeMaster.getEmployeeBankAccountNumber());
			
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		 
		return dtoEmployeeMaster;
		
	}

}
