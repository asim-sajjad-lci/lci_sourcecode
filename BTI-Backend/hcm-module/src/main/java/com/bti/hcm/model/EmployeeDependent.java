package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00930",indexes = {
        @Index(columnList = "EMPLOYINDX")
})
public class EmployeeDependent extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPLOYINDX")
	private int id;

	@Column(name = "EMPDEPSEQN")
	private int sequence;

	@ManyToOne
	@JoinColumn(name = "EMPRELSHINDX")
	private EmployeeDependents employeeDependents;

	@ManyToOne
	@JoinColumn(name = "EMPLOYINDXM")
	private EmployeeMaster employeeMaster;

	@ManyToOne
	@JoinColumn(name = "LOCCOUNTY")
	private HrCountry country;

	@ManyToOne
	@JoinColumn(name = "LOCSTATE")
	private HrState state;

	@ManyToOne
	@JoinColumn(name = "LOCCITY")
	private HrCity city;

	@Column(name = "FRSTNME", columnDefinition = "char(31)")
	private String empFistName;

	@Column(name = "MIDDLNME", columnDefinition = "char(31)")
	private String empMidleName;

	@Column(name = "LSTNAME", columnDefinition = "char(31)")
	private String empLastName;

	@Column(name = "COMMTS", columnDefinition = "char(120)")
	private String comments;

	@Column(name = "GENDR")
	private short gender;

	@Column(name = "DOBDT")
	private Date dateOfBirth;

	@Column(name = "AOG")
	private Integer age;

	@Column(name = "PHONE", columnDefinition = "char(21)")
	private String phoneNumber;

	@Column(name = "PHONEW", columnDefinition = "char(21)")
	private String workNumber;

	@Column(name = "ADDRESS", columnDefinition = "char(120)")
	private String address;

	/*
	 * @Column(name = "CITY", columnDefinition = "char(21)") private String city;
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getEmpFistName() {
		return empFistName;
	}

	public void setEmpFistName(String empFistName) {
		this.empFistName = empFistName;
	}

	public String getEmpMidleName() {
		return empMidleName;
	}

	public void setEmpMidleName(String empMidleName) {
		this.empMidleName = empMidleName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public short getGender() {
		return gender;
	}

	public void setGender(short gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getWorkNumber() {
		return workNumber;
	}

	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/*
	 * public String getCity() { return city; }
	 * 
	 * public void setCity(String city) { this.city = city; }
	 */

	public EmployeeDependents getEmployeeDependents() {
		return employeeDependents;
	}

	public void setEmployeeDependents(EmployeeDependents employeeDependents) {
		this.employeeDependents = employeeDependents;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public HrCountry getCountry() {
		return country;
	}

	public void setCountry(HrCountry country) {
		this.country = country;
	}

	public HrCity getCity() {
		return city;
	}

	public void setCity(HrCity city) {
		this.city = city;
	}

	public HrState getState() {
		return state;
	}

	public void setState(HrState state) {
		this.state = state;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
