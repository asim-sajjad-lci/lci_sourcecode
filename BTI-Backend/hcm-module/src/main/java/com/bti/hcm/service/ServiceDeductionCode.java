package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.dto.DtoDeductionCode;
import com.bti.hcm.model.dto.DtoEmployeePayCodeMaintenance;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceDeductionCode")
public class ServiceDeductionCode {

	/**
	 * @Description LOGGER use for put a logger in DeductionCode Service
	 */
	static Logger log = Logger.getLogger(ServiceDeductionCode.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in DeductionCode service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in DeductionCode service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryDeductionCode Autowired here using annotation of
	 *              spring for access of repositoryDeductionCode method in
	 *              DeductionCode service In short Access DeductionCode Query from
	 *              Database using repositoryDeductionCode.
	 */
	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;

	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	@Autowired
	RepositoryDeductionCode deductionCode;

	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;

	/**
	 * @Description: save and update DeductionCode data
	 * @param DtoDeductionCode
	 * @return
	 * @throws ParseException
	 */
	/*
	 * public DtoDeductionCode saveOrUpdate(DtoDeductionCode dtoDeductionCode) {
	 * 
	 * try { if(dtoDeductionCode.getDtoPayCode()!=null &&
	 * !dtoDeductionCode.getDtoPayCode().isEmpty()) { for (DtoPayCode dtoPayCode :
	 * dtoDeductionCode.getDtoPayCode()) {
	 * log.info("saveOrUpdate DeductionCode Method"); int loggedInUserId =
	 * Integer.parseInt(httpServletRequest.getHeader("userid")); DeductionCode
	 * deductionCode = null; if (dtoDeductionCode.getId() != null &&
	 * dtoDeductionCode.getId() > 0) { deductionCode =
	 * repositoryDeductionCode.findByIdAndIsDeleted(dtoDeductionCode.getId(),
	 * false); deductionCode.setUpdatedBy(loggedInUserId);
	 * deductionCode.setUpdatedDate(new Date()); } else { deductionCode = new
	 * DeductionCode(); deductionCode.setCreatedDate(new Date()); Integer rowId =
	 * repositoryDeductionCode.getCountOfTotalDeductionCodes(); Integer increment =
	 * 0; if (rowId != 0) { increment = rowId + 1; } else { increment = 1; }
	 * deductionCode.setRowId(increment); }
	 * 
	 * PayCode payCode=new PayCode(); if(dtoPayCode.getId()!=null &&
	 * dtoPayCode.getId()>0) {
	 * payCode=repositoryPayCode.findOne(dtoPayCode.getId()); }
	 * deductionCode.setPayCode(payCode);
	 * deductionCode.setDiductionId(dtoDeductionCode.getDiductionId());
	 * deductionCode.setDiscription(dtoDeductionCode.getDiscription());
	 * deductionCode.setArbicDiscription(dtoDeductionCode.getArbicDiscription());
	 * deductionCode.setStartDate(dtoDeductionCode.getStartDate());
	 * deductionCode.setEndDate(dtoDeductionCode.getEndDate());
	 * deductionCode.setTransction(dtoDeductionCode.isTransction());
	 * deductionCode.setMethod(dtoDeductionCode.getMethod());
	 * deductionCode.setAmount(dtoDeductionCode.getAmount());
	 * deductionCode.setPercent(dtoDeductionCode.getPercent());
	 * deductionCode.setPerPeriod(dtoDeductionCode.getPerPeriod());
	 * deductionCode.setPerYear(dtoDeductionCode.getPerYear());
	 * deductionCode.setLifeTime(dtoDeductionCode.getLifeTime());
	 * 
	 * deductionCode.setFrequency(dtoDeductionCode.getFrequency());
	 * deductionCode.setInActive(dtoDeductionCode.isInActive());
	 * 
	 * deductionCode.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
	 * 
	 * repositoryDeductionCode.saveAndFlush(deductionCode);
	 * log.debug("DeductionCode is:" + dtoDeductionCode.getId()); }
	 * 
	 * }
	 * 
	 * 
	 * } catch (Exception e) { log.error(e); } return dtoDeductionCode;
	 * 
	 * }
	 */

	/**
	 * @Description: get All DeductionCode
	 * @param DtoDeductionCode
	 * @return
	 */
	public DtoSearch getAllDeductionCode(DtoDeductionCode dtoDeductionCode) {
		log.info("getAllDeductionCode Method");
		DtoSearch dtoSearch = new DtoSearch();

		try {

			dtoSearch.setPageNumber(dtoDeductionCode.getPageNumber());
			dtoSearch.setPageSize(dtoDeductionCode.getPageSize());
			dtoSearch.setTotalCount(repositoryDeductionCode.getCountOfTotalDeductionCode());
			List<DeductionCode> deductionCodeList = null;
			if (dtoDeductionCode.getPageNumber() != null && dtoDeductionCode.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoDeductionCode.getPageNumber(), dtoDeductionCode.getPageSize(),
						Direction.DESC, "createdDate");
				deductionCodeList = repositoryDeductionCode.findByIsDeleted(false, pageable);
			} else {
				deductionCodeList = repositoryDeductionCode.findByIsDeletedOrderByCreatedDateDesc(false);
			}

			List<DtoDeductionCode> dtoDeductionCodeList = new ArrayList<>();
			if (deductionCodeList != null && !deductionCodeList.isEmpty()) {
				for (DeductionCode deductionCode : deductionCodeList) {
					dtoDeductionCode = getAndSetDeductionCode(deductionCode);
					dtoDeductionCodeList.add(dtoDeductionCode);
				}
				dtoSearch.setRecords(dtoDeductionCodeList);
			}
			log.debug("All DtoDeductionCode List Size is:" + dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	/**
	 * @Description: Search DeductionCode data
	 * @param dtoSearch
	 * @return
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("search DeductionCode Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();
				dtoSearch = searchByDeductionCode(dtoSearch);
				dtoSearch.setTotalCount(
						this.repositoryDeductionCode.predictiveDeductionCodeSearchTotalCount("%" + searchWord + "%"));
				List<DeductionCode> deductionList = searchByPageSizeAndNumberByDeductionCode(dtoSearch);
				if (deductionList != null && !deductionList.isEmpty()) {
					List<DtoDeductionCode> dtoDeductionCodeListes = new ArrayList<>();
					DtoDeductionCode dtoDeductionCode = null;
					for (DeductionCode deductionCode : deductionList) {
						dtoDeductionCode = getAndSetDeductionCode(deductionCode);
						dtoDeductionCodeListes.add(dtoDeductionCode);
					}
					dtoSearch.setRecords(dtoDeductionCodeListes);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoSearch searchByDeductionCode(DtoSearch dtoSearch) {
		String condition = "";

		try {
			if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {

				if (dtoSearch.getSortOn().equals("diductionId") || dtoSearch.getSortOn().equals("discription")
						|| dtoSearch.getSortOn().equals("arbicDiscription") || dtoSearch.getSortOn().equals("startDate")
						|| dtoSearch.getSortOn().equals("endDate") || dtoSearch.getSortOn().equals("transction")
						|| dtoSearch.getSortOn().equals("method") || dtoSearch.getSortOn().equals("amount")
						|| dtoSearch.getSortOn().equals("percent") || dtoSearch.getSortOn().equals("frequency")) {
					condition = dtoSearch.getSortOn();
				} else {
					condition = "id";
				}

			} else {
				condition += "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
			dtoSearch.setCondition(condition);
		} catch (Exception e) {
			log.error(e);
		}

		return dtoSearch;
	}

	public List<DeductionCode> searchByPageSizeAndNumberByDeductionCode(DtoSearch dtoSearch) {
		List<DeductionCode> deductionList = null;
		try {
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {

				if (dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")) {
					deductionList = this.repositoryDeductionCode.predictiveDeductionCodeSearchWithPagination(
							"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
									dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if (dtoSearch.getSortBy().equals("ASC")) {
					deductionList = this.repositoryDeductionCode.predictiveDeductionCodeSearchWithPagination(
							"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
									dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
				} else if (dtoSearch.getSortBy().equals("DESC")) {
					deductionList = this.repositoryDeductionCode.predictiveDeductionCodeSearchWithPagination(
							"%" + dtoSearch.getSearchKeyword() + "%", new PageRequest(dtoSearch.getPageNumber(),
									dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
				}

			}
		} catch (Exception e) {
			log.error(e);
		}

		return deductionList;
	}

	/**
	 * @Description: get Department data by id
	 * @param departmentId
	 * @return
	 */
	public DtoDeductionCode getById(int id) {
		log.info("getById Method");
		DtoDeductionCode dtoDeductionCode = new DtoDeductionCode();

		try {
			if (id > 0) {
				DeductionCode department = repositoryDeductionCode.findByIdAndIsDeleted(id, false);
				if (department != null) {
					dtoDeductionCode = new DtoDeductionCode(department);

					if (department.getPayCodeList() != null && !department.getPayCodeList().isEmpty()) {
						List<Integer> listId = new ArrayList<>();
						for (PayCode payCode : department.getPayCodeList()) {
							listId.add(payCode.getId());
						}
						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCode2.setPayFactor(payCode1.getPayFactor());
								payCode2.setPayRate(payCode1.getPayRate());
								payCode2.setAmount(payCode1.getBaseOnPayCodeAmount());
								payCodeList.add(payCode2);
							}
							dtoDeductionCode.setDtoPayCode(payCodeList);
						}

					}

					dtoDeductionCode.setId(department.getId());
					dtoDeductionCode.setAmount(department.getAmount());
					dtoDeductionCode.setArbicDiscription(department.getArbicDiscription());
					dtoDeductionCode.setDiductionId(department.getDiductionId());
					dtoDeductionCode.setEndDate(department.getEndDate());
					dtoDeductionCode.setStartDate(department.getStartDate());
					dtoDeductionCode.setFrequency(department.getFrequency());
					dtoDeductionCode.setLifeTime(department.getLifeTime());
					dtoDeductionCode.setPercent(department.getPercent());
					dtoDeductionCode.setPayFactor(department.getPayFactor());
					dtoDeductionCode.setPerPeriod(department.getPerPeriod());
					dtoDeductionCode.setCustomDate(department.isCustomeDate());
					dtoDeductionCode.setPerYear(department.getPerYear());
					dtoDeductionCode.setTransction(department.isTransction());
					dtoDeductionCode.setInActive(department.isInActive());
					dtoDeductionCode.setNoOfDays(department.getNoOfDays());
					dtoDeductionCode.setEndDateDays(department.getEndDateDays());
					dtoDeductionCode.setMethod(department.getMethod());
					dtoDeductionCode.setDiscription(department.getDiscription());
					dtoDeductionCode.setDeductionTypeId(department.getTypeField());	// ME
					dtoDeductionCode.setRoundOf(department.getRoundOf());			// ME

				} else {
					dtoDeductionCode.setMessageType("DEDUCTION_CODE_NOT_GETTING");

				}
			} else {
				dtoDeductionCode.setMessageType("INVALID_DEDUCTION_CODE_ID");

			}
			log.debug("getById is:" + dtoDeductionCode.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDeductionCode;
	}

	/**
	 * @Description: delete department
	 * @param ids
	 * @return
	 */

	public DtoDeductionCode delete(List<Integer> ids) {
		log.info("delete Method");
		DtoDeductionCode dtoDeductionCode = new DtoDeductionCode();
		dtoDeductionCode
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DEDUCTION_CODE_DELETED", false));
		dtoDeductionCode.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("DtoDeductionCode_ASSOCIATED", false));
		List<DtoDeductionCode> deleteDepartment = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse
				.getMessageByShortAndIsDeleted("DEDUCTION_CODE_NOT_DELETE_ID_ALREADY_IN USE", false).getMessage());

		try {
			for (Integer departmentId : ids) {
				DeductionCode deductionCode = repositoryDeductionCode.findOne(departmentId);
				if (deductionCode.getListEmployeeDeductionMaintenance().isEmpty()
						&& deductionCode.getListBuildPayrollCheckByDeductions().isEmpty()) {
					DtoDeductionCode deductionCode2 = new DtoDeductionCode();
					deductionCode2.setId(departmentId);
					repositoryDeductionCode.deleteSingleDeductionCode(true, loggedInUserId, departmentId);
					deleteDepartment.add(deductionCode2);

				} else {
					inValidDelete = true;
				}
			}

			if (inValidDelete) {
				dtoDeductionCode.setMessageType(invlidDeleteMessage.toString());

			}
			if (!inValidDelete) {
				dtoDeductionCode.setMessageType("");
			}

			dtoDeductionCode.setDelete(deleteDepartment);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete delete :" + dtoDeductionCode.getId());
		return dtoDeductionCode;
	}

	/**
	 * @Description: check Repeat DepartmentId
	 * @param departmetnId
	 * @return
	 */

	public DtoDeductionCode repeatByDeductionCodeId(String deductionCodeId) {
		log.info("repeatByDeductionCodeId Method");
		DtoDeductionCode dtoDeductionCode = new DtoDeductionCode();
		try {
			List<DeductionCode> deductionCode = repositoryDeductionCode.findByDeductionCodeId(deductionCodeId);
			if (deductionCode != null && !deductionCode.isEmpty()) {
				dtoDeductionCode.setIsRepeat(true);
			} else {
				dtoDeductionCode.setIsRepeat(false);
			}

		} catch (Exception e) {
			log.error(e);
		}

		return dtoDeductionCode;
	}

	public List<DtoDeductionCode> getAllDtoDeductionCodeDropDownList() {
		log.info("getAllDtoDeductionCodeDropDownList  Method");
		List<DtoDeductionCode> dtoDeductionCodeList = new ArrayList<>();
		try {
			List<DeductionCode> list = repositoryDeductionCode.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (DeductionCode deductionCode : list) {
					DtoDeductionCode dtoDeductionCode = new DtoDeductionCode();
					dtoDeductionCode.setId(deductionCode.getId());
					dtoDeductionCode.setDiductionId(deductionCode.getDiductionId());
					dtoDeductionCode.setDiscription(deductionCode.getDiscription());
					dtoDeductionCodeList.add(dtoDeductionCode);
				}
			}
			log.debug("DeducaitonCode is:" + dtoDeductionCodeList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDeductionCodeList;
	}

	public List<DtoDeductionCode> getAllDtoDeductionCodeInActiveList() {
		log.info("getAllDtoDeductionCodeDropDownList  Method");
		List<DtoDeductionCode> dtoDeductionCodeList = new ArrayList<>();
		try {
			List<DeductionCode> list = repositoryDeductionCode.findByIsDeletedAndInActive(false, false);
			if (list != null && !list.isEmpty()) {
				for (DeductionCode deductionCode : list) {
					DtoDeductionCode dtoDeductionCode = new DtoDeductionCode();

					if (deductionCode.getPayCodeList() != null && !deductionCode.getPayCodeList().isEmpty()) {
						List<Integer> listId = new ArrayList<>();
						for (PayCode payCode : deductionCode.getPayCodeList()) {
							listId.add(payCode.getId());
						}
						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCodeList.add(payCode2);
							}
							dtoDeductionCode.setDtoPayCode(payCodeList);
						}

					}

					dtoDeductionCode.setId(deductionCode.getId());
					dtoDeductionCode.setDiductionId(deductionCode.getDiductionId());
					dtoDeductionCode.setDiscription(deductionCode.getDiscription());
					dtoDeductionCode.setArbicDiscription(deductionCode.getArbicDiscription());
					dtoDeductionCode.setMethod(deductionCode.getMethod());
					dtoDeductionCode.setPercent(deductionCode.getPercent());
					dtoDeductionCode.setPayFactor(deductionCode.getPayFactor());
					dtoDeductionCode.setAmount(deductionCode.getAmount());
					dtoDeductionCode.setPerYear(deductionCode.getPerYear());
					dtoDeductionCode.setPerPeriod(deductionCode.getPerPeriod());
					dtoDeductionCode.setLifeTime(deductionCode.getLifeTime());
					dtoDeductionCode.setTransction(deductionCode.isTransction());
					dtoDeductionCode.setStartDate(deductionCode.getStartDate());
					dtoDeductionCode.setEndDate(deductionCode.getEndDate());
					dtoDeductionCode.setInActive(deductionCode.isInActive());
					dtoDeductionCode.setEndDateDays(deductionCode.getEndDateDays());
					dtoDeductionCode.setNoOfDays(deductionCode.getNoOfDays());
					dtoDeductionCode.setCustomDate(deductionCode.isCustomeDate());
					dtoDeductionCode.setFrequency(deductionCode.getFrequency());
					
					dtoDeductionCode.setRoundOf(deductionCode.getRoundOf());			//ME
					dtoDeductionCode.setDeductionTypeId(deductionCode.getTypeField());	//ME
					
					dtoDeductionCodeList.add(dtoDeductionCode);
				}
			}
			log.debug("DeducaitonCode is:" + dtoDeductionCodeList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDeductionCodeList;
	}

	public DtoSearch searchdiductionId(DtoSearch dtoSearch) {

		try {
			log.info("searchdiductionId Method");
			if (dtoSearch != null) {
				String searchWord = dtoSearch.getSearchKeyword();

				List<DeductionCode> deductionCodeList = this.repositoryDeductionCode
						.predictiveSearchDiductionIdWithPagination("%" + searchWord + "%");
				if (!deductionCodeList.isEmpty()) {

					if (!deductionCodeList.isEmpty()) {
						List<DtoDeductionCode> dtoDeductionCode = new ArrayList<>();
						DtoDeductionCode dtoDeductionCode1 = null;
						for (DeductionCode deductionCode : deductionCodeList) {

							dtoDeductionCode1 = new DtoDeductionCode(deductionCode);

							dtoDeductionCode1.setId(deductionCode.getId());
							dtoDeductionCode1.setDiductionId(deductionCode.getDiductionId());
							dtoDeductionCode1.setDiscription(
									deductionCode.getDiductionId() + "  | " + deductionCode.getDiscription());
							dtoDeductionCode1.setAmount(deductionCode.getAmount());

							dtoDeductionCode.add(dtoDeductionCode1);
						}
						dtoSearch.setRecords(dtoDeductionCode);
					}

					dtoSearch.setTotalCount(deductionCodeList.size());
				}

			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public DtoDeductionCode getAndSetDeductionCode(DeductionCode deductionCode) {
		DtoDeductionCode dtoDeductionCode = new DtoDeductionCode();
		try {
			dtoDeductionCode.setDiductionId(deductionCode.getDiductionId());
			dtoDeductionCode.setId(deductionCode.getId());

			if (deductionCode.getPayCodeList() != null && !deductionCode.getPayCodeList().isEmpty()) {
				List<Integer> listId = new ArrayList<>();
				for (PayCode payCode : deductionCode.getPayCodeList()) {
					listId.add(payCode.getId());
				}
				List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
				if (!payCode.isEmpty()) {
					
					List<DtoPayCode> payCodeList = new ArrayList<>();
					for (PayCode payCode1 : payCode) {
						DtoPayCode payCode2 = new DtoPayCode();
						payCode2.setId(payCode1.getId());
						payCode2.setPayCodeId(payCode1.getPayCodeId());
						payCodeList.add(payCode2);
					}
					dtoDeductionCode.setId(deductionCode.getId());
					dtoDeductionCode.setDiductionId(deductionCode.getDiductionId());
					dtoDeductionCode.setDiscription(deductionCode.getDiscription());
					dtoDeductionCode.setArbicDiscription(deductionCode.getArbicDiscription());
					dtoDeductionCode.setMethod(deductionCode.getMethod());
					dtoDeductionCode.setPercent(deductionCode.getPercent());
					dtoDeductionCode.setPayFactor(deductionCode.getPayFactor());
					dtoDeductionCode.setAmount(deductionCode.getAmount());
					dtoDeductionCode.setPerYear(deductionCode.getPerYear());
					dtoDeductionCode.setPerPeriod(deductionCode.getPerPeriod());
					dtoDeductionCode.setLifeTime(deductionCode.getLifeTime());
					dtoDeductionCode.setTransction(deductionCode.isTransction());
					dtoDeductionCode.setStartDate(deductionCode.getStartDate());
					dtoDeductionCode.setEndDate(deductionCode.getEndDate());
					dtoDeductionCode.setEndDateDays(deductionCode.getEndDateDays());
					dtoDeductionCode.setNoOfDays(deductionCode.getNoOfDays());

					dtoDeductionCode.setCustomDate(deductionCode.isCustomeDate());
					dtoDeductionCode.setInActive(deductionCode.isInActive());
					dtoDeductionCode.setFrequency(deductionCode.getFrequency());
					
					dtoDeductionCode.setRoundOf(deductionCode.getRoundOf());			//ME
					dtoDeductionCode.setDeductionTypeId(deductionCode.getTypeField());	//ME
					
					dtoDeductionCode.setDtoPayCode(payCodeList);
				}

			}
			dtoDeductionCode.setDiscription(deductionCode.getDiscription());
			dtoDeductionCode.setArbicDiscription(deductionCode.getArbicDiscription());
			dtoDeductionCode.setStartDate(deductionCode.getStartDate());
			dtoDeductionCode.setEndDate(deductionCode.getEndDate());
			dtoDeductionCode.setTransction(deductionCode.isTransction());
			dtoDeductionCode.setMethod(deductionCode.getMethod());
			dtoDeductionCode.setAmount(deductionCode.getAmount());
			dtoDeductionCode.setPercent(deductionCode.getPercent());
			dtoDeductionCode.setPayFactor(deductionCode.getPayFactor());
			dtoDeductionCode.setPerPeriod(deductionCode.getPerPeriod());
			dtoDeductionCode.setPerYear(deductionCode.getPerYear());
			dtoDeductionCode.setLifeTime(deductionCode.getLifeTime());
			dtoDeductionCode.setCustomDate(deductionCode.isCustomeDate());
			dtoDeductionCode.setFrequency(deductionCode.getFrequency());
			dtoDeductionCode.setInActive(deductionCode.isInActive());
			
			dtoDeductionCode.setRoundOf(deductionCode.getRoundOf());			//ME
			dtoDeductionCode.setDeductionTypeId(deductionCode.getTypeField());	//ME
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoDeductionCode;
	}

	public DtoDeductionCode saveOrUpdate(DtoDeductionCode dtoDeductionCode) {

		try {
			log.info("saveOrUpdate DeductionCode Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			DeductionCode deductionCode = null;
			if (dtoDeductionCode.getId() != null && dtoDeductionCode.getId() > 0) {
				deductionCode = repositoryDeductionCode.findByIdAndIsDeleted(dtoDeductionCode.getId(), false);
				deductionCode.setUpdatedBy(loggedInUserId);
				deductionCode.setUpdatedDate(new Date());
				if (dtoDeductionCode.getStartDate() != null && dtoDeductionCode.getEndDate() != null) {
					List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = repositoryEmployeeDeductionMaintenance
							.findByBatchId(dtoDeductionCode.getId());
					if (employeeDeductionMaintenanceList != null) {
						for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenanceList) {
							repositoryEmployeeDeductionMaintenance.deleteSingleEmployeeDeductionMaintenance1(
									dtoDeductionCode.getStartDate(), dtoDeductionCode.getEndDate(),
									dtoDeductionCode.getInActive(), employeeDeductionMaintenance.getId());
						}
					}
				}

				if (dtoDeductionCode.getInActive() != null) {

					List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = repositoryEmployeeDeductionMaintenance
							.findByBatchId(dtoDeductionCode.getId());
					if (employeeDeductionMaintenanceList != null) {
						for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenanceList) {
							repositoryEmployeeDeductionMaintenance.deleteSingleEmployeeDeductionMaintenance2(
									dtoDeductionCode.getInActive(), employeeDeductionMaintenance.getId());

						}
					}

				}
				if (dtoDeductionCode.getNoOfDays() != null) {

					List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = repositoryEmployeeDeductionMaintenance
							.findByBatchId(dtoDeductionCode.getId());
					if (employeeDeductionMaintenanceList != null) {
						for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenanceList) {
							repositoryEmployeeDeductionMaintenance.deleteSingleEmployeeDeductionMaintenance3(
									dtoDeductionCode.getNoOfDays(), employeeDeductionMaintenance.getId());

						}
					}

				}
				if (dtoDeductionCode.getEndDateDays() != null) {

					List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = repositoryEmployeeDeductionMaintenance
							.findByBatchId(dtoDeductionCode.getId());
					if (employeeDeductionMaintenanceList != null) {
						for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenanceList) {
							repositoryEmployeeDeductionMaintenance.deleteSingleEmployeeDeductionMaintenance4(
									dtoDeductionCode.getEndDateDays(), employeeDeductionMaintenance.getId());
						}
					}

				}
				if (dtoDeductionCode.getTransction() != null) {
					List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = repositoryEmployeeDeductionMaintenance
							.findByBatchId(dtoDeductionCode.getId());
					if (employeeDeductionMaintenanceList != null) {
						for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenanceList) {
							repositoryEmployeeDeductionMaintenance.deleteSingleEmployeeDeductionMaintenance5(
									dtoDeductionCode.getTransction(), employeeDeductionMaintenance.getId());
						}
					}
				}
			} else {
				deductionCode = new DeductionCode();
				deductionCode.setCreatedDate(new Date());
				Integer rowId = repositoryDeductionCode.getCountOfTotalDeductionCodes();
				Integer increment = 0;
				if (rowId != 0) {
					increment = rowId + 1;
				} else {
					increment = 1;
				}
				deductionCode.setRowId(increment);
			}

			if (!dtoDeductionCode.getDtoPayCode().isEmpty()) {
				ArrayList<PayCode> paycodeList = new ArrayList<>();
				log.info("paycodeList:" + paycodeList);
				for (DtoPayCode dtoPayCode : dtoDeductionCode.getDtoPayCode()) {
					PayCode payCode = repositoryPayCode.findByIdAndIsDeleted(dtoPayCode.getId(), false);
					log.info("payCode:" + payCode);
					paycodeList.add(payCode);
					log.info("paycodeList:" + paycodeList);
				}
				log.info("deductionCode1:" + deductionCode);
				deductionCode.setPayCodeList(paycodeList);
				log.info("deductionCode2:" + deductionCode);
			}
			
			log.info("deductionCode:" + deductionCode);
			deductionCode.setAllPaycode(true);
			deductionCode.setAllPaycode(dtoDeductionCode.isAllPaycode());
			deductionCode.setDiductionId(dtoDeductionCode.getDiductionId());
			deductionCode.setDiscription(dtoDeductionCode.getDiscription());
			deductionCode.setArbicDiscription(dtoDeductionCode.getArbicDiscription());

			deductionCode.setStartDate(dtoDeductionCode.getStartDate());
			deductionCode.setEndDate(dtoDeductionCode.getEndDate());

			deductionCode.setTransction(dtoDeductionCode.getTransction());
			deductionCode.setMethod(dtoDeductionCode.getMethod());
			deductionCode.setAmount(dtoDeductionCode.getAmount());
			deductionCode.setPercent(dtoDeductionCode.getPercent());
			deductionCode.setPayFactor(dtoDeductionCode.getPayFactor());
			deductionCode.setPerPeriod(dtoDeductionCode.getPerPeriod());
			deductionCode.setCustomeDate(dtoDeductionCode.isCustomDate());
			deductionCode.setPerYear(dtoDeductionCode.getPerYear());
			deductionCode.setLifeTime(dtoDeductionCode.getLifeTime());
			deductionCode.setNoOfDays(dtoDeductionCode.getNoOfDays());
			deductionCode.setEndDateDays(dtoDeductionCode.getEndDateDays());
			deductionCode.setFrequency(dtoDeductionCode.getFrequency());
			deductionCode.setInActive(dtoDeductionCode.getInActive());

			deductionCode.setTypeField(dtoDeductionCode.getDeductionTypeId()); // ME
			deductionCode.setRoundOf(dtoDeductionCode.getRoundOf()); 		   // ME

			deductionCode.setUpdatedRow(UtilDateAndTimeHr.localToUTC());

			repositoryDeductionCode.saveAndFlush(deductionCode);
			log.debug("DeductionCode is:" + dtoDeductionCode.getId());

		} catch (Exception e) {
			log.error(e);
		}
		return dtoDeductionCode;

	}

	public DtoPayCode deducationCodeByPaycode(DtoPayCode dtoDeducationByPaycode) {
		List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenanceList = new ArrayList<>();
		try {

			List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance
					.findByEmployeeIds(dtoDeducationByPaycode.getEmployeeId());
			for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
				if (employeePayCodeMaintenance != null) {
					DeductionCode deductionCode = repositoryDeductionCode
							.findByDeductionId(dtoDeducationByPaycode.getDeductionId());
					if (deductionCode.getPayCodeList() != null) {
						for (PayCode payCode : deductionCode.getPayCodeList()) {
							if (payCode.getId() == employeePayCodeMaintenance.getPayCode().getId()) {
								DtoPayCode dtoPayCode = new DtoPayCode();
								DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance();
								dtoPayCode.setId(payCode.getId());
								dtoPayCode.setPayCodeId(payCode.getPayCodeId());
								dtoPayCode.setPayFactor(payCode.getPayFactor());
								dtoPayCode.setAmount(payCode.getBaseOnPayCodeAmount());
								dtoPayCode.setPayRate(payCode.getPayRate());
								dtoEmployeePayCodeMaintenance.setPayRate(employeePayCodeMaintenance.getPayRate());
								dtoEmployeePayCodeMaintenance.setId(employeePayCodeMaintenance.getId());
								dtoEmployeePayCodeMaintenance.setPayCode(dtoPayCode);
								dtoEmployeePayCodeMaintenanceList.add(dtoEmployeePayCodeMaintenance);
								dtoDeducationByPaycode.setRecords(dtoEmployeePayCodeMaintenanceList);
							}

						}
					}
				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoDeducationByPaycode;
	}

	// code wise Data for method
	public DtoPayCode deducationCodeByPaycode123(DtoPayCode dtoDeducationByPaycode) {
		List<DtoEmployeePayCodeMaintenance> dtoEmployeePayCodeMaintenanceList = new ArrayList<>();
		try {
			if (dtoDeducationByPaycode.getMethod() == 5) {
				List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = repositoryEmployeePayCodeMaintenance
						.findByEmployeeIds(dtoDeducationByPaycode.getEmployeeId());
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
					if (employeePayCodeMaintenance != null) {
						DeductionCode deductionCode = repositoryDeductionCode
								.findByDeductionId(dtoDeducationByPaycode.getDeductionId());
						if (deductionCode.getPayCodeList() != null) {
							for (PayCode payCode : deductionCode.getPayCodeList()) {
								if (payCode.getId() == employeePayCodeMaintenance.getPayCode().getId()) {
									DtoPayCode dtoPayCode = new DtoPayCode();
									DtoEmployeePayCodeMaintenance dtoEmployeePayCodeMaintenance = new DtoEmployeePayCodeMaintenance();
									dtoPayCode.setId(payCode.getId());
									dtoPayCode.setPayCodeId(payCode.getPayCodeId());
									dtoPayCode.setPayFactor(payCode.getPayFactor());
									dtoPayCode.setAmount(payCode.getBaseOnPayCodeAmount());
									dtoPayCode.setPayRate(payCode.getPayRate());
									dtoEmployeePayCodeMaintenance.setPayRate(employeePayCodeMaintenance.getPayRate());
									dtoEmployeePayCodeMaintenance.setId(employeePayCodeMaintenance.getId());
									dtoEmployeePayCodeMaintenance.setPayCode(dtoPayCode);
									dtoEmployeePayCodeMaintenanceList.add(dtoEmployeePayCodeMaintenance);
									dtoDeducationByPaycode.setRecords(dtoEmployeePayCodeMaintenanceList);
								}

							}
						}
					}
				}

			}
			if (dtoDeducationByPaycode.getMethod() == 1 || dtoDeducationByPaycode.getMethod() == 2
					|| dtoDeducationByPaycode.getMethod() == 3 || dtoDeducationByPaycode.getMethod() == 4) {
				DeductionCode deductionCode = repositoryDeductionCode
						.findByDeductionId(dtoDeducationByPaycode.getDeductionId());
				if (deductionCode != null) {
					DtoDeductionCode dtoDeductionCode = new DtoDeductionCode();
					dtoDeductionCode.setDiscription(deductionCode.getDiscription());
					dtoDeductionCode.setArbicDiscription(deductionCode.getArbicDiscription());
					dtoDeductionCode.setStartDate(deductionCode.getStartDate());
					dtoDeductionCode.setEndDate(deductionCode.getEndDate());
					dtoDeductionCode.setTransction(deductionCode.isTransction());
					dtoDeductionCode.setMethod(deductionCode.getMethod());
					dtoDeductionCode.setAmount(deductionCode.getAmount());
					dtoDeductionCode.setPercent(deductionCode.getPercent());
					dtoDeductionCode.setPayFactor(deductionCode.getPayFactor());
					dtoDeductionCode.setPerPeriod(deductionCode.getPerPeriod());
					dtoDeductionCode.setPerYear(deductionCode.getPerYear());
					dtoDeductionCode.setLifeTime(deductionCode.getLifeTime());
					dtoDeductionCode.setCustomDate(deductionCode.isCustomeDate());
					dtoDeductionCode.setFrequency(deductionCode.getFrequency());
					dtoDeductionCode.setInActive(deductionCode.isInActive());
					dtoDeducationByPaycode.setRecords(dtoDeductionCode);
				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		return dtoDeducationByPaycode;
	}

}
