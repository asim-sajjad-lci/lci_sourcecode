package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeePositionHistory;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeePositionHistory;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeePositionHistory")
public class ControllerEmployeePositionHistory extends BaseController{

	@Autowired
	ServiceEmployeePositionHistory serviceEmployeePositionHistory;
	
	
	@Autowired
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeePositionHistory.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoEmployeePositionHistory dtoEmployeePositionHistory) throws Exception{
		LOGGER.info("Create EmployeePositionHistory Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeePositionHistory = serviceEmployeePositionHistory.saveOrUpdate(dtoEmployeePositionHistory);
			responseMessage=displayMessage(dtoEmployeePositionHistory, "EMPLOYEE_POSITION_HISTORY_CREATED", "EMPLOYEE_POSITION_HISTORY_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeePositionHistory Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeePositionHistory dtoEmployeePositionHistory) throws Exception {
		LOGGER.info("Update EmployeePositionHistory Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeePositionHistory = serviceEmployeePositionHistory.saveOrUpdate(dtoEmployeePositionHistory);
			responseMessage=displayMessage(dtoEmployeePositionHistory, "EMPLOYEE_POSITION_HISTORY_UPDATED_SUCCESS", "EMPLOYEE_POSITION_HISTORY_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeePositionHistory Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllByEmployeeId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeePositionHistory Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePositionHistory.getAll(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_POSITION_HISTORY_GET_DETAIL", "EMPLOYEE_POSITION_HISTORY_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search EmployeePositionHistory Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/findById", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage findById(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeePositionHistory Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeePositionHistory.findById(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_POSITION_HISTORY_GET_DETAIL", "EMPLOYEE_POSITION_HISTORY_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search EmployeePositionHistory Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteEmployeeSkill(HttpServletRequest request, @RequestBody DtoEmployeePositionHistory dtoEmployeePositionHistory) throws Exception {
		LOGGER.info("Delete EmployeePositionHistory Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeePositionHistory.getIds() != null && !dtoEmployeePositionHistory.getIds().isEmpty()) {
				DtoEmployeePositionHistory dtoEmployeePositionHistory1 = serviceEmployeePositionHistory.delete(dtoEmployeePositionHistory.getIds());
				if(dtoEmployeePositionHistory1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_POSITION_HISTORY_DELETED", false), dtoEmployeePositionHistory1);

				}else {
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_POSITION_HISTORY_NOT_DELETE_ID_MESSAGE", false), dtoEmployeePositionHistory1);


				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeePositionHistory Method:"+responseMessage.getMessage());
		return responseMessage;
	}
}
