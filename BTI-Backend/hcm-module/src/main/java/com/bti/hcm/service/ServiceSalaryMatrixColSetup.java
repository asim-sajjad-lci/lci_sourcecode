package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.SalaryMatrixColSetup;
import com.bti.hcm.model.dto.DtoSalaryMatrixColSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositorySalaryMatrixColSetup;
import com.bti.hcm.repository.RepositorySalaryMatrixSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceSalaryMatrixColSetup")
public class ServiceSalaryMatrixColSetup {
	
	
	/**
	 * @Description LOGGER use for put a logger in ServiceSalaryMatrixColSetup Service
	 */
	static Logger log = Logger.getLogger(ServiceSalaryMatrixColSetup.class.getName());

	

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceSalaryMatrixRow service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in ServiceSalaryMatrixRow service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositorySalaryMatrixSetup Autowired here using annotation of spring for access of repositorySalaryMatrixSetup method in SalaryMatrixSetup service
	 * 				In short Access SalaryMatrixSetup Query from Database using repositorySalaryMatrixSetup.
	 */
	@Autowired
	RepositorySalaryMatrixSetup repositorySalaryMatrixSetup;
	
	@Autowired
	RepositorySalaryMatrixColSetup repositorySalaryMatrixColSetup;
	
	
	/**
	 * 
	 * @param dtoSalaryMatrixColSetup
	 * @return
	 * @throws ParseException
	 */
	public DtoSalaryMatrixColSetup saveOrUpdateSalaryMatrixColSetup(DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup) {
		log.info("saveOrUpdateSalaryMatrixColSetup Method");
		SalaryMatrixColSetup salaryMatrixColSetup = null;
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			if (dtoSalaryMatrixColSetup.getColSalaryMatrixIndexId() != null && dtoSalaryMatrixColSetup.getColSalaryMatrixIndexId() > 0) {
				salaryMatrixColSetup = repositorySalaryMatrixColSetup.findByColSalaryMatrixIndexIdAndIsDeleted(dtoSalaryMatrixColSetup.getColSalaryMatrixIndexId(), false);
				salaryMatrixColSetup.setUpdatedBy(loggedInUserId);
				salaryMatrixColSetup.setUpdatedDate(new Date());
				
			} else {
				salaryMatrixColSetup = new SalaryMatrixColSetup();
				salaryMatrixColSetup.setCreatedDate(new Date());
				
				Integer rowId = repositorySalaryMatrixColSetup.getCountOfTotaSalaryMatrixColSetup();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				salaryMatrixColSetup.setRowId(increment);
			}
			salaryMatrixColSetup.setColSalaryMatrixDescription(dtoSalaryMatrixColSetup.getColSalaryMatrixDescription());
			salaryMatrixColSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositorySalaryMatrixColSetup.saveAndFlush(salaryMatrixColSetup);
			log.debug("SalaryMatrixColSetup is:"+dtoSalaryMatrixColSetup.getColSalaryMatrixIndexId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSalaryMatrixColSetup;

	}

	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchSalaryMatrixColSetup(DtoSearch dtoSearch) {
		log.info("searchSalaryMatrixColSetup Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "arabicColSalaryMatrixDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "colSalaryMatrixDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "salaryMatrixSetup" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="colSalaryMatrixIndexId";
				}
			}else{
				condition+="colSalaryMatrixIndexId";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositorySalaryMatrixColSetup.predictiveSalaryMatrixColSetupSearchTotalCount("%"+searchWord+"%"));
				List<SalaryMatrixColSetup> salaryMatrixColSetupList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						salaryMatrixColSetupList = this.repositorySalaryMatrixColSetup.predictiveSalaryMatrixColSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						salaryMatrixColSetupList = this.repositorySalaryMatrixColSetup.predictiveSalaryMatrixColSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						salaryMatrixColSetupList = this.repositorySalaryMatrixColSetup.predictiveSalaryMatrixColSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(salaryMatrixColSetupList != null && !salaryMatrixColSetupList.isEmpty()){
					List<DtoSalaryMatrixColSetup> dtoSalaryMatrixColSetupList = new ArrayList<>();
					DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup=null;
					for (SalaryMatrixColSetup salaryMatrixColSetup : salaryMatrixColSetupList) {
						dtoSalaryMatrixColSetup = new DtoSalaryMatrixColSetup(salaryMatrixColSetup);
						dtoSalaryMatrixColSetup.setColSalaryMatrixIndexId(salaryMatrixColSetup.getColSalaryMatrixIndexId());
						dtoSalaryMatrixColSetup.setColSalaryMatrixDescription(salaryMatrixColSetup.getColSalaryMatrixDescription());
						dtoSalaryMatrixColSetup.setArabicColSalaryMatrixDescription(salaryMatrixColSetup.getArabicColSalaryMatrixDescription());
						dtoSalaryMatrixColSetupList.add(dtoSalaryMatrixColSetup);
					}
					dtoSearch.setRecords(salaryMatrixColSetupList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		log.debug("Search SalaryMatrixColSetup Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoSalaryMatrixColSetup deleteSalaryMatrixColSetup(List<Integer> ids) {
		log.info("deleteSalaryMatrixColSetup Method");
		DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup = new DtoSalaryMatrixColSetup();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
				try {
					dtoSalaryMatrixColSetup
					.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SALARY_MATRIX_COL_SETUP_DELETED", false));
					dtoSalaryMatrixColSetup.setAssociateMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("SALARY_MATRIX_COL_SETUP_DELETED", false));
			List<DtoSalaryMatrixColSetup> deleteSalaryMatrixColSetup = new ArrayList<>();
					try {
						for (Integer colSalaryMatrixIndexId : ids) {
							SalaryMatrixColSetup salaryMatrixColSetup = repositorySalaryMatrixColSetup.findOne(colSalaryMatrixIndexId);
							DtoSalaryMatrixColSetup dtoSalaryMatrixColSetup2 = new DtoSalaryMatrixColSetup();
							dtoSalaryMatrixColSetup2.setColSalaryMatrixIndexId(salaryMatrixColSetup.getColSalaryMatrixIndexId());
							dtoSalaryMatrixColSetup2.setColSalaryMatrixDescription(salaryMatrixColSetup.getColSalaryMatrixDescription());
							dtoSalaryMatrixColSetup2.setArabicColSalaryMatrixDescription(salaryMatrixColSetup.getArabicColSalaryMatrixDescription());
							repositorySalaryMatrixColSetup.deleteSingleSalaryMatrixColSetup(true, loggedInUserId, colSalaryMatrixIndexId);
							deleteSalaryMatrixColSetup.add(dtoSalaryMatrixColSetup2);
						}
						dtoSalaryMatrixColSetup.setDeleteSalaryMatrixColSetup(deleteSalaryMatrixColSetup);
					} catch (NumberFormatException e) {
						log.error(e);
					}
				log.debug("Delete SalaryMatrixColSetup :"+dtoSalaryMatrixColSetup.getColSalaryMatrixIndexId());
				} catch (Exception e) {
					log.error(e);
				}
		return dtoSalaryMatrixColSetup;
	}
	

}
