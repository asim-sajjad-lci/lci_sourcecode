package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBatches;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceBatches;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/batches")
public class ControllerBatches extends BaseController {

	/**
	 * 
	 */
	@Autowired
	ServiceBatches serviceBatches;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerBatches.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * @param request
	 * @param dtoBatches
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoBatches dtoBatches) throws Exception{
		LOGGER.info("Create Batches Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBatches = serviceBatches.saveOrUpdate(dtoBatches);
			responseMessage=displayMessage(dtoBatches, "BATCHES_CREATED", "BATCHES_NOT_CREATED", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		LOGGER.debug("Create Batches Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoBatches dtoBatches) throws Exception {
		LOGGER.info("Update Batches Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBatches = serviceBatches.saveOrUpdate(dtoBatches);
			responseMessage=displayMessage(dtoBatches, "BATCHES_UPDATED_SUCCESS", "BATCHES_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update Batches Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoBatches dtoBatches) throws Exception {
		LOGGER.info("Delete PayCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoBatches.getIds() != null && !dtoBatches.getIds().isEmpty()) {
				DtoBatches dtoBatchesObj = serviceBatches.deleteBatches(dtoBatches.getIds());
				if(dtoBatchesObj.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("BATCHES_DELETED", false), dtoBatchesObj);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							response.getMessageByShortAndIsDeleted("BATCHES_NOT_DELETE_ID_MESSAGE", false), dtoBatchesObj);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("DEFAULT_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete Default Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoBatches dtoBatches) throws Exception {
		LOGGER.info("Get payCode ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoBatchesObj = serviceBatches.getById(dtoBatches.getId());
			responseMessage=displayMessage(dtoBatchesObj, MessageConstant.BATCHES_GET_DETAIL, MessageConstant.BATCHES_NOT_GETTING, response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		LOGGER.debug("Get pay Code by Id Method:"+dtoBatches.getId());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/batcheIdcheck", method = RequestMethod.POST)
	public ResponseMessage batcheIdcheck(HttpServletRequest request, @RequestBody DtoBatches dtoBatches) throws Exception {
		LOGGER.info("batcheIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoBatches dtoPayCodeObj = serviceBatches.repeatByBatchesId(dtoBatches.getBatchId());
			responseMessage=displayMessage(dtoPayCodeObj, MessageConstant.BATCHES_GET_DETAILS, MessageConstant.BATCHES_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}
	

	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search batche Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBatches.getAll(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.BATCHES_GET_DETAIL, MessageConstant.BATCHES_NOT_GETTING, response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllForBuild", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllForBuild(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search batche Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBatches.getAllBuildCheck(dtoSearch);
			responseMessage=displayMessage(dtoSearch, MessageConstant.BATCHES_GET_DETAIL, MessageConstant.BATCHES_NOT_GETTING, response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		return responseMessage;
	}
	
	
		
		@RequestMapping(value = "/getAllBatcheDropDown", method = RequestMethod.POST)
		@ResponseBody
		public ResponseMessage getAllBatcheDropDown(HttpServletRequest request,@RequestBody DtoSearch dtoSearch) {
			ResponseMessage responseMessage = null;
			try {
				boolean flag =serviceHcmHome.checkValidCompanyAccess();
				if (flag) {
					List<DtoBatches> dtoBatchesList = serviceBatches.getAllBatchesCodeDropDownList(dtoSearch);
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted(MessageConstant.BATCHES_GET_DETAIL, false), dtoBatchesList);
				}else {
					responseMessage = unauthorizedMsg(response);
				}
				
			} catch (Exception e) {
				LOGGER.error(e);
			}
			
			return responseMessage;
		}
		
		
		
		
		
		@RequestMapping(value = "/getAllForTransectionEntry", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
		public ResponseMessage getAllForTransectionEntry(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
			LOGGER.info("Search batche Method");
			ResponseMessage responseMessage = null;
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtoSearch = this.serviceBatches.getAllForTransectionEntry(dtoSearch);
				responseMessage=displayMessage(dtoSearch, MessageConstant.BATCHES_GET_DETAIL, MessageConstant.BATCHES_NOT_GETTING, response);
			} else {
				responseMessage =unauthorizedMsg(response);
			}
			return responseMessage;
		}
		
}
