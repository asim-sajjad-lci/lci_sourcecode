package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.PredefinecheckList;
import com.bti.hcm.model.dto.DtoPredefinecheckList;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPredefinecheckList;

@Service("servicePredefinecheckList")
public class ServicePredefinecheckList {
	
	static Logger log = Logger.getLogger(ServicePredefinecheckList.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryPredefinecheckList repositoryPredefinecheckList;
	
	
	public DtoSearch getAllIds(DtoSearch dtoSearch) {
		DtoPredefinecheckList dtoCompnayInsurance;
		
		List<PredefinecheckList> compnayInsuranceList = null;
		compnayInsuranceList = repositoryPredefinecheckList.findAll();
		List<DtoPredefinecheckList> dtoCompnayInsuranceList=new ArrayList<>();
		if(compnayInsuranceList!=null && !compnayInsuranceList.isEmpty())
		{
			for (PredefinecheckList compnayInsurance : compnayInsuranceList) 
			{
				dtoCompnayInsurance=new DtoPredefinecheckList(compnayInsurance);
				dtoCompnayInsurance.setId(compnayInsurance.getId());
				dtoCompnayInsurance.setChecklistDesc(compnayInsurance.getChecklistDesc());
				dtoCompnayInsurance.setChecklistType(compnayInsurance.getChecklistType());
				dtoCompnayInsurance.setChecklistArbicDesc(compnayInsurance.getChecklistArbicDesc());
				dtoCompnayInsuranceList.add(dtoCompnayInsurance);
			}
			dtoSearch.setRecords(dtoCompnayInsuranceList);
		}
		return dtoSearch;
	}
}
