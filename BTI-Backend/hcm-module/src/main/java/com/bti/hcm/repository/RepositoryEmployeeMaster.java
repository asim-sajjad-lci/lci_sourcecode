/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Department;
import com.bti.hcm.model.EmployeeMaster;

/**
 * Description: Interface for EmployeeMaster 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryEmployeeMaster")
public interface RepositoryEmployeeMaster extends JpaRepository<EmployeeMaster, Integer>{
	
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public EmployeeMaster findByEmployeeIndexIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<EmployeeMaster> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @param inActive
	 * @return
	 */
	public List<EmployeeMaster> findByIsDeletedAndEmployeeInactive(Boolean deleted, Boolean inActive);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from EmployeeMaster e where e.isDeleted=false")
	public Integer getCountOfTotalEmployeeMaster();

	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<EmployeeMaster> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeMaster e set e.isDeleted =:deleted, e.updatedBy=:updateById where e.employeeIndexId IN (:idList)")
	public void deleteEmployeeMaster(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeMaster e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.employeeIndexId =:id ")
	public void deleteSingleEmployeeMaster(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	/**
	 * 
	 * @return
	 */
	public EmployeeMaster findTop1ByOrderByEmployeeIndexIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select e from EmployeeMaster e where (e.employeeId like :searchKeyWord  or e.employeeTitle like :searchKeyWord or e.employeeTitleArabic like :searchKeyWord or e.employeeLastName like :searchKeyWord or e.employeeFirstName like :searchKeyWord or e.employeeMiddleName like :searchKeyWord or e.employeeLastNameArabic like :searchKeyWord or e.employeeFirstNameArabic like :searchKeyWord or e.employeeMiddleNameArabic like :searchKeyWord) and e.isDeleted=false")
	public List<EmployeeMaster> predictiveEmployeeMasterSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<EmployeeMaster> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from EmployeeMaster e where (e.employeeId like :searchKeyWord  or e.employeeTitle like :searchKeyWord or e.employeeTitleArabic like :searchKeyWord or e.employeeLastName like :searchKeyWord or e.employeeFirstName like :searchKeyWord or e.employeeMiddleName like :searchKeyWord or e.employeeLastNameArabic like :searchKeyWord or e.employeeFirstNameArabic like :searchKeyWord or e.employeeMiddleNameArabic like :searchKeyWord) and e.isDeleted=false")
	public Integer predictiveEmployeeMasterSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select e from EmployeeMaster e where (e.employeeId like :searchKeyWord  or e.employeeTitle like :searchKeyWord or e.employeeTitleArabic like :searchKeyWord or e.employeeLastName like :searchKeyWord or e.employeeFirstName like :searchKeyWord or e.employeeMiddleName like :searchKeyWord or e.employeeLastNameArabic like :searchKeyWord or e.employeeFirstNameArabic like :searchKeyWord or e.employeeMiddleNameArabic like :searchKeyWord) and e.isDeleted=false")
	public List<EmployeeMaster> predictiveEmployeeMasterSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param employeeId
	 * @return
	 */
	@Query("select e from EmployeeMaster e where (e.employeeId =:employeeId) and e.isDeleted=false")
	public List<EmployeeMaster> findByEmployeeId(@Param("employeeId")String employeeId);
	
	/**
	 * 
	 * @param employeeId
	 * @return
	 */
	@Query("select e from EmployeeMaster e where (e.employeeId =:employeeId) and e.isDeleted=false")
	public EmployeeMaster findOneByEmployeeId(@Param("employeeId")String employeeId);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select e.employeeId from EmployeeMaster e where (e.employeeId like :searchKeyWord) and e.isDeleted=false order by e.employeeIndexId desc")
	public List<String> predictiveEmployeeIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	@Query("select count(*) from EmployeeMaster e where (e.employeeId LIKE :searchKeyWord) and e.isDeleted=false")
	public Integer predictiveEmployeeIdSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select e from EmployeeMaster e where (e.employeeId like :searchKeyWord) and e.isDeleted=false")
	public List<EmployeeMaster> predictiveEmployeeIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select count(*) from EmployeeMaster e where (e.employeeFirstName LIKE :searchKeyWord) and e.isDeleted=false")
	public Integer predictiveEmployeeNameSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select e from EmployeeMaster e where (e.employeeFirstName like :searchKeyWord) and e.isDeleted=false")
	public List<EmployeeMaster> predictiveEmployeeNameSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	@Query("select count(*) from EmployeeMaster e ")
	public Integer getCountOfTotalEmployeeMasters();
	
	@Query("select e from EmployeeMaster e where (e.employeeIndexId BETWEEN  :fromEmployeeId and :toEmployeeId) and e.isDeleted=false order by e.employeeIndexId ASC")
	public List<EmployeeMaster> findbyFromAndToId(@Param("fromEmployeeId")Integer fromEmployeeId, @Param("toEmployeeId")Integer toEmployeeId);

	@Query("select e from EmployeeMaster e where e.employeeIndexId in (select e.employeeIndexId from  EmployeeMaster e where (e.employeeIndexId BETWEEN  :fromEmployeeId and :toEmployeeId) and e.isDeleted=false order by e.employeeIndexId ASC) and e.department.id BETWEEN  :from and :to")
	public List<EmployeeMaster> findbyFromAndToIdForDeduction(@Param("from")Integer from, @Param("to")Integer to, @Param("fromEmployeeId")Integer fromEmployeeId, @Param("toEmployeeId")Integer toEmployeeId);

	@Query("select e from EmployeeMaster e where e.employeeIndexId in (select e.employeeIndexId from EmployeeMaster e where (e.employeeIndexId BETWEEN  :fromEmployeeId and :toEmployeeId) and e.isDeleted=false order by e.employeeIndexId ASC) and e.employeeIndexId BETWEEN  :from and :to")
	public List<EmployeeMaster> findbyFromAndToIdForEmployee(@Param("from")Integer from, @Param("to")Integer to, @Param("fromEmployeeId")Integer fromEmployeeId, @Param("toEmployeeId")Integer toEmployeeId);
	
	// @Cacheable("EmployeeMaster")
	@Query("select d from EmployeeMaster d where id in(:list) and d.isDeleted=false")
	public List<EmployeeMaster> findAllEmployeeListId(@Param("list") List<Integer> list);
	
	
   // @Cacheable("employeeMaster")
	@Query("select d from EmployeeMaster d where d.department.id in(:list) and d.isDeleted=false")
	public List<EmployeeMaster> findByEmployeeByDepartmentList(@Param("list") List<Integer> list);
	
	@Query("select d from EmployeeMaster d where d.department.id in(:list) and d.isDeleted=false")
	public List<EmployeeMaster> findByEmployeeByDepartmentList1(@Param("list") List<Department> list);
	
	 //@Cacheable("EmployeeMaster")
	@Query("select d from EmployeeMaster d where d.department.id =:id and d.isDeleted=false")
	public List<EmployeeMaster> findByEmployeeByDepartmentList2(@Param("id") Integer id);
	
	
	@Query("select d from EmployeeMaster d where (d.department.id in(:departmentList) and d.employeeIndexId not in(:employeeList)) and d.isDeleted=false")
	public List<EmployeeMaster> findByEmployeeByDepartmentListAndEmployeeList(@Param("departmentList") List<Integer> departmentList,@Param("employeeList") List<Integer> employeeList);

	@Query("select d from EmployeeMaster d where d.department.id in(:departmentList) and d.isDeleted=false")
	public List<Integer> findByEmployeeByDepartmentListAndEmployeeList1(@Param("departmentList") List<Integer> departmentList);

	
	@Query("select d from EmployeeMaster d where d.employeeIndexId in(:list) and d.isDeleted=false")
	public List<EmployeeMaster> findAllEmployeeListByEmployeeIndexId(@Param("list") List<Integer> list);
	
	@Query("select e.location.id from EmployeeMaster e where e.employeeIndexId =:eid and e.isDeleted =false")
	public Integer getLocationByEmpId(@Param("eid") Integer eid);	//ME

	@Query("select e.position.id from EmployeeMaster e where e.employeeIndexId =:eid and e.isDeleted =false")
	public Integer getPositionByEmpId(@Param("eid") Integer eid);	//ME
	
	
}
