package com.bti.hcm.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90401")
public class ManualChecksOpenYearDetails extends HcmBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMPYMTINDXD")
	private Integer id;

	@Column(name = "HCMPYMTSEQ")
	private Integer paymentSequence;

	@Column(name = "HCMPYMTYP")
	private Short paymentCodeType;

	@Column(name = "HCMCODINX")
	private Integer codeId;

	@Column(name = "HCMFDTA")
	private Date fromPeriodDate;

	@Column(name = "HCMTDTA")
	private Date toPeriodDate;

	@ManyToOne
	@JoinColumn(name = "HCMPYMTINDX")
	@Where(clause = "is_deleted = false")
	private ManualChecksOpenYearHeader manualChecksOpenYearHeader;

	@Column(name = "HCMAMNT",precision = 10, scale = 3)
	private BigDecimal totalAmount;

	@Column(name = "HCMHRS")
	private Integer totalHours;

	@Column(name = "HCMDYS")
	private Integer weaksWorked;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPaymentSequence() {
		return paymentSequence;
	}

	public void setPaymentSequence(Integer paymentSequence) {
		this.paymentSequence = paymentSequence;
	}

	public Short getPaymentCodeType() {
		return paymentCodeType;
	}

	public void setPaymentCodeType(Short paymentCodeType) {
		this.paymentCodeType = paymentCodeType;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public Date getFromPeriodDate() {
		return fromPeriodDate;
	}

	public void setFromPeriodDate(Date fromPeriodDate) {
		this.fromPeriodDate = fromPeriodDate;
	}

	public Date getToPeriodDate() {
		return toPeriodDate;
	}

	public void setToPeriodDate(Date toPeriodDate) {
		this.toPeriodDate = toPeriodDate;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public Integer getWeaksWorked() {
		return weaksWorked;
	}

	public void setWeaksWorked(Integer weaksWorked) {
		this.weaksWorked = weaksWorked;
	}

	public ManualChecksOpenYearHeader getManualChecksOpenYearHeader() {
		return manualChecksOpenYearHeader;
	}

	public void setManualChecksOpenYearHeader(ManualChecksOpenYearHeader manualChecksOpenYearHeader) {
		this.manualChecksOpenYearHeader = manualChecksOpenYearHeader;
	}

}
