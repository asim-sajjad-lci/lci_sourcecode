package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.MiscellaneousMaintanence;

@Repository("repositoryMiscellaneousMaintenance")
public interface RepositoryMiscellneousMaintenance extends JpaRepository<MiscellaneousMaintanence, Integer> {

	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	public MiscellaneousMaintanence findByIdAndIsDeleted(int id, boolean deleted);

	/**
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update MiscellaneousMaintanence d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleMiscellaneousMaintenanceEntry(@Param("deleted") Boolean deleted,
			@Param("updateById") Integer updateById, @Param("id") Integer id);

	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update MiscellaneousMaintanence d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteAllMiscellaneousMaintenanceEntries(@Param("deleted") Boolean deleted,
			@Param("idList") List<Integer> idList, @Param("updateById") Integer updateById);

	/**
	 * @param deleted
	 * @return
	 */
	public List<MiscellaneousMaintanence> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);

	@Query("select count(*) from MiscellaneousMaintanence d where d.isDeleted=false")
	public Integer getCountOfTotalMiscellaneousEntries();

	@Query("select d from MiscellaneousMaintanence d where (d.employeeMaster.id=:employeeId) and d.isDeleted=false")
	public List<MiscellaneousMaintanence> predictiveMiscellaneousAllIdSearchWithPagination(
			@Param("employeeId") int employeeId, Pageable pageable);

	@Query("select count(*) from MiscellaneousMaintanence d where (d.employeeMaster.id=:employeeId) and d.isDeleted=false")
	public Integer predictiveMiscellaneousCodeSearchTotalCount(@Param("employeeId") int employeeId);

	@Query("select d from MiscellaneousMaintanence d where (d.employeeMaster.id=:employeeId) and d.isDeleted=false")
	public List<MiscellaneousMaintanence> findByEmployeeId(@Param("employeeId") Integer employeeId);

	// ME
	@Query("select d from MiscellaneousMaintanence d where (d.employeeMaster.id=:employeeId) and d.isDeleted=false")	//ME
	public List<MiscellaneousMaintanence> findByEmployeeIdPageable(@Param("employeeId") Integer employeeId, Pageable pageable);	//ME

	
	//ME
	@Query("select count(*) from MiscellaneousMaintanence d where d.isDeleted=false and d.employeeMaster.id =:employeeId ")	//ME
	public Integer countByEmployeeId(@Param("employeeId") Integer employeeId);	//ME
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update MiscellaneousMaintanence e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.employeeMaster.employeeIndexId =:id")
	public void deleteByEmployeeId(@Param("deleted") boolean b, @Param("updateById") int loggedInUserId,
			@Param("id") int employeeId);
}
