package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.TimeCode;

public class DtoTimeCode extends DtoBase{

	private Integer id;
	private String timeCodeId;
	private String desc;
	private String arbicDesc;
	private boolean inActive;
	private String payCodeId;
	private Integer payPrimaryId;
	private Integer scheduleId;
	private String scheduleIds;
	private Integer schedulePrimaryId;
	private Short accrualPeriod;
	private Short timeType;
	private List<DtoTimeCode> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTimeCodeId() {
		return timeCodeId;
	}

	public void setTimeCodeId(String timeCodeId) {
		this.timeCodeId = timeCodeId;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public Short getAccrualPeriod() {
		return accrualPeriod;
	}

	public void setAccrualPeriod(Short accrualPeriod) {
		this.accrualPeriod = accrualPeriod;
	}

	public Short getTimeType() {
		return timeType;
	}

	public void setTimeType(Short timeType) {
		this.timeType = timeType;
	}
	
	public List<DtoTimeCode> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoTimeCode> delete) {
		this.delete = delete;
	}

	public String getPayCodeId() {
		return payCodeId;
	}

	public void setPayCodeId(String payCodeId) {
		this.payCodeId = payCodeId;
	}

	public DtoTimeCode() {
	}

	public DtoTimeCode(TimeCode timeCode) {

		this.timeCodeId = timeCode.getTimeCodeId();
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Integer getSchedulePrimaryId() {
		return schedulePrimaryId;
	}

	public void setSchedulePrimaryId(Integer schedulePrimaryId) {
		this.schedulePrimaryId = schedulePrimaryId;
	}

	public Integer getPayPrimaryId() {
		return payPrimaryId;
	}

	public void setPayPrimaryId(Integer payPrimaryId) {
		this.payPrimaryId = payPrimaryId;
	}

	public String getScheduleIds() {
		return scheduleIds;
	}

	public void setScheduleIds(String scheduleIds) {
		this.scheduleIds = scheduleIds;
	}

}
