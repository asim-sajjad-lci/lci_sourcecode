
package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * Description: DTO Department class having getter and setter for fields (POJO) Name
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoBank extends DtoBase{
	
	private int id;
	private String bankName;
	private String bankDescription;
	private String bankDescriptionArabic;
	private String swiftCode;
	private String accountNumber;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankDescription() {
		return bankDescription;
	}

	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}

	public String getBankDescriptionArabic() {
		return bankDescriptionArabic;
	}

	public void setBankDescriptionArabic(String bankDescriptionArabic) {
		this.bankDescriptionArabic = bankDescriptionArabic;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	

}
