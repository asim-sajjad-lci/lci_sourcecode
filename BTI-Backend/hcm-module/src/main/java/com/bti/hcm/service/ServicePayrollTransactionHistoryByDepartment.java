package com.bti.hcm.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.PayrollTransactionHistoryByDepartment;
import com.bti.hcm.model.dto.DtoPayrollTransectionHistoryByEmployee;
import com.bti.hcm.repository.RepositoryPayrollTransactionHistoryByDepartment;

@Service("/payrollTransactionHistoryByDepartment")
public class ServicePayrollTransactionHistoryByDepartment {

	

	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServicePayrollTransactionHistoryByDepartment.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryPayrollTransactionHistoryByDepartment repositoryPayrollTransactionHistoryByDepartment;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	public DtoPayrollTransectionHistoryByEmployee saveOrUpdate(DtoPayrollTransectionHistoryByEmployee dtoPayrollTransectionHistoryByEmployee) {

		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		PayrollTransactionHistoryByDepartment payrollTransactionHistoryByDepartment=null;
		if(dtoPayrollTransectionHistoryByEmployee.getId()!=null && dtoPayrollTransectionHistoryByEmployee.getId()>0) {
			payrollTransactionHistoryByDepartment=repositoryPayrollTransactionHistoryByDepartment.findByIdAndIsDeleted(dtoPayrollTransectionHistoryByEmployee.getId(),false);
			payrollTransactionHistoryByDepartment.setUpdatedBy(loggedInUserId);
			payrollTransactionHistoryByDepartment.setUpdatedDate(new Date());
			
		}else {
			payrollTransactionHistoryByDepartment = new PayrollTransactionHistoryByDepartment();
			payrollTransactionHistoryByDepartment.setCreatedDate(new Date());
			Integer rowId = repositoryPayrollTransactionHistoryByDepartment.findAll().size();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			payrollTransactionHistoryByDepartment.setRowId(increment);
		}
		payrollTransactionHistoryByDepartment.setCodeIndexId(dtoPayrollTransectionHistoryByEmployee.getCodeIndexId());
		payrollTransactionHistoryByDepartment.setCodeType(dtoPayrollTransectionHistoryByEmployee.getCodeType());
		payrollTransactionHistoryByDepartment.setYear(dtoPayrollTransectionHistoryByEmployee.getYear());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod1(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod1());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod2(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod2());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod3(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod3());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod4(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod4());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod5(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod5());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod6(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod6());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod7(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod7());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod8(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod8());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod9(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod9());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod10(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod10());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod11(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod11());
		payrollTransactionHistoryByDepartment.setTotalAmountPeriod12(dtoPayrollTransectionHistoryByEmployee.getTotalAmountPeriod12());
		repositoryPayrollTransactionHistoryByDepartment.saveAndFlush(payrollTransactionHistoryByDepartment);
		return dtoPayrollTransectionHistoryByEmployee;
	}
	
	
}
