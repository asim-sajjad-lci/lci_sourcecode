package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoEmployeeQuickAssigments;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeQuickAssigments;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeQuickAssigments")
public class ControllerEmployeeQuickAssigments extends BaseController{

	private static final Logger LOGGER = Logger.getLogger(ControllerAccrual.class);
	@Autowired
	ServiceEmployeeQuickAssigments serviceEmployeeQuickAssigments;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createAccrual(HttpServletRequest request, @RequestBody DtoEmployeeQuickAssigments dtoEmployeeQuickAssigments) throws Exception {
		LOGGER.info("Create EmployeeQuickAssigments Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeQuickAssigments  = serviceEmployeeQuickAssigments.saveOrUpdate(dtoEmployeeQuickAssigments);
			responseMessage=displayMessage(dtoEmployeeQuickAssigments, "QUICK_ASSIGMENT_CREATED", "QUICK_ASSIGMENT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeeQuickAssigments Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoEmployeeQuickAssigments dtoEmployeeQuickAssigments, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeQuickAssigments Method");
		DtoSearch dtoSearch = null;
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeQuickAssigments.getAllByCodeType(dtoEmployeeQuickAssigments);
			responseMessage=displayMessage(dtoSearch, "QUICK_ASSIGMENT_GET_ALL", "QUICK_ASSIGMENT_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search EmployeeQuickAssigments Method:"+dtoSearch.getTotalCount());
		}
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/updateQuickAssignment", method = RequestMethod.POST)
	public ResponseMessage updateOnly(HttpServletRequest request, @RequestBody DtoEmployeeQuickAssigments dtoEmployeeQuickAssigments) throws Exception {
		LOGGER.info("Create EmployeeQuickAssigments Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeQuickAssigments  = serviceEmployeeQuickAssigments.updateOnly(dtoEmployeeQuickAssigments);
			responseMessage=displayMessage(dtoEmployeeQuickAssigments, "QUICK_ASSIGMENT_CREATED", "QUICK_ASSIGMENT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeeQuickAssigments Method:"+responseMessage.getMessage());
		return responseMessage;
	}
}
