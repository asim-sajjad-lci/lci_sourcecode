package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.ActivityLog;
import com.bti.hcm.model.dto.DtoActivityLog;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.repository.RepositoryActivityLog;

@Service("serviceActivityLog")
public class ServiceActivityLog {
	
	/**
	 * @Description LOGGER use for put a logger in ActivityLog Service
	 */
	static Logger log = Logger.getLogger(ServiceActivityLog.class.getName());


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ActivityLog service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in ActivityLog service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryActivityLog Autowired here using annotation of spring for access of repositoryActivityLog method in Atteandace service
	 * 				In short Access ActivityLog Query from Database using repositoryActivityLog.
	 */
	@Autowired
	RepositoryActivityLog repositoryActivityLog;
	
	static String s ="E:\\activityLog.sql";
	private static final String FILENAME = s;
	
	/**
	 * 
	 * @param dtoActivityLog
	 * @return
	 * @throws ParseException
	 */
	public DtoActivityLog saveOrActivityLog(DtoActivityLog dtoActivityLog){
		try {
			log.info("saveOrActivityLog Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			ActivityLog activityLog = null;
			if (dtoActivityLog.getId() != null && dtoActivityLog.getId() > 0) {
				activityLog = repositoryActivityLog.findById(dtoActivityLog.getId());
				activityLog.setUpdatedDate(new Date());
			} else {
				activityLog = new ActivityLog();
				activityLog.setCreatedDate(new Date());
			}
			activityLog.setUserId(loggedInUserId);
			repositoryActivityLog.saveAndFlush(activityLog);
			log.debug("ActivityLog is:"+dtoActivityLog.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoActivityLog;

	}
	
	/**
	 * @param userId
	 * @param serviceNmae
	 * @param methodName
	 * @return
	 */
	public ActivityLog activityLogs(Integer userId,String serviceName,String methodName,String moduleName,String activity,String companyName,String screenName) {
		
		ActivityLog activityLog = new ActivityLog();
		try {
			activityLog.setCreatedDate(new Date());
			activityLog.setMethod(methodName);
			activityLog.setUserId(userId);
			activityLog.setServices(serviceName);
			activityLog.setModuleName(moduleName);
			activityLog.setActivity(activity);
			activityLog.setCompanyName(companyName);
			activityLog.setScreenName(screenName);
			activityLog=repositoryActivityLog.saveAndFlush(activityLog);
			
		} catch (Exception e) {
			log.error(e);
		}
		return activityLog;
		
	}
		public ActivityLog activityLogs(Integer userId,String serviceNmae,String methodName,String moduleName,String activity) {
				
				ActivityLog activityLog = new ActivityLog();
				try {
					activityLog.setCreatedDate(new Date());
					activityLog.setMethod(methodName);
					activityLog.setUserId(userId);
					activityLog.setServices(serviceNmae);
					activityLog.setModuleName(moduleName);
					activityLog.setActivity(activity);
					activityLog=repositoryActivityLog.saveAndFlush(activityLog);
				} catch (Exception e) {
					log.error(e);
				}
				return activityLog;
				
			}
	
		public ActivityLog activityLogs(Integer userId,String serviceNmae,String methodName,String moduleName,String activity,String screenName,String companyName,String type) {
			
			ActivityLog activityLog = new ActivityLog();
			activityLog.setCreatedDate(new Date());
			activityLog.setMethod(methodName);
			activityLog.setUserId(userId);
			activityLog.setServices(serviceNmae);
			activityLog.setModuleName(moduleName);
			activityLog.setActivity(activity);
			activityLog.setScreenName(screenName);
			activityLog.setCompanyName(companyName);
			activityLog.setType(type);
			activityLog=repositoryActivityLog.saveAndFlush(activityLog);
			
			return activityLog;
			
		}
		
		
		public ActivityLog activityLogs(Integer userId,String serviceName,String methodName,String moduleName,String activity,String companyName,String screenName,String userName,String type) {
			
			ActivityLog activityLog = new ActivityLog();
			activityLog.setCreatedDate(new Date());
			activityLog.setMethod(methodName);
			activityLog.setUserId(userId);
			activityLog.setServices(serviceName);
			activityLog.setModuleName(moduleName);
			activityLog.setActivity(activity);
			activityLog.setCompanyName(companyName);
			activityLog.setScreenName(screenName);
			activityLog.setUserName(userName);
			activityLog.setType(type);
			activityLog=repositoryActivityLog.saveAndFlush(activityLog);
			return activityLog;
			
		}
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchActivityLog(DtoSearch dtoSearch) {
		log.info("Search & GetAll ActiveLog Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "services" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "method" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "userId" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
				dtoSearch.setTotalCount(this.repositoryActivityLog.predictiveActivityLogSearchTotalCount("%"+searchWord+"%"));
				List<ActivityLog> activeLogList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						activeLogList = this.repositoryActivityLog.predictiveActivityLogSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						activeLogList = this.repositoryActivityLog.predictiveActivityLogSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						activeLogList = this.repositoryActivityLog.predictiveActivityLogSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(activeLogList != null && !activeLogList.isEmpty()){
					List<DtoActivityLog> dtoActiveLogList = new ArrayList<>();
					DtoActivityLog dtoActivityLog=null;
					for (ActivityLog activityLog : activeLogList) {
						dtoActivityLog = new DtoActivityLog(activityLog);
						dtoActivityLog.setId(activityLog.getId());
						dtoActivityLog.setMethod(activityLog.getMethod());
						dtoActivityLog.setServices(activityLog.getServices());
						dtoActivityLog.setUserId(activityLog.getUserId());
						dtoActivityLog.setModuleName(activityLog.getModuleName());
						dtoActivityLog.setActivity(activityLog.getActivity());
						dtoActivityLog.setScreenName(activityLog.getScreenName());
						dtoActivityLog.setCompanyName(activityLog.getCompanyName());
						dtoActivityLog.setCreatedDate(activityLog.getCreatedDate());
						dtoActivityLog.setUpdatedDate(activityLog.getUpdatedDate());
						dtoActivityLog.setType(activityLog.getType());
						dtoActivityLog.setUserName(activityLog.getUserName());
						dtoActiveLogList.add(dtoActivityLog);
					}
					dtoSearch.setRecords(dtoActiveLogList);
				}
			}
			log.debug("Search & getAll ActiveLog Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	

	
	
	
			
			
			@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
			public DtoSearchActivity searchByStartDateAndEndDate(DtoSearchActivity dtoSearchActivity) {
				log.info("searchByStartDateAndEndDate");
				try {
					List<ActivityLog> activeLogList =null;
					if(dtoSearchActivity != null){
						dtoSearchActivity.setTotalCount(this.repositoryActivityLog.predictiveActivityLogSearchTotalCountByStartDateAndEndDate(dtoSearchActivity.getStartDate(),dtoSearchActivity.getEndDate()));
						
						if(dtoSearchActivity.getPageNumber()!=null && dtoSearchActivity.getPageSize()!=null){
								activeLogList = this.repositoryActivityLog.predictiveActivityLogSearchWithByStartDateAndEndDateAndWithPagination(dtoSearchActivity.getStartDate(),dtoSearchActivity.getEndDate(),new PageRequest(dtoSearchActivity.getPageNumber(), dtoSearchActivity.getPageSize(), Sort.Direction.DESC, "id"));
						}
						if(activeLogList != null && !activeLogList.isEmpty()){
							List<DtoActivityLog> dtoActiveLogList = new ArrayList<>();
							DtoActivityLog dtoActivityLog=null;
							for (ActivityLog activityLog : activeLogList) {
								dtoActivityLog = new DtoActivityLog(activityLog);
								dtoActivityLog.setId(activityLog.getId());
								dtoActivityLog.setMethod(activityLog.getMethod());
								dtoActivityLog.setServices(activityLog.getServices());
								dtoActivityLog.setUserId(activityLog.getUserId());
								dtoActivityLog.setModuleName(activityLog.getModuleName());
								dtoActivityLog.setActivity(activityLog.getActivity());
								dtoActivityLog.setScreenName(activityLog.getScreenName());
								dtoActivityLog.setCompanyName(activityLog.getCompanyName());
								dtoActivityLog.setCreatedDate(activityLog.getCreatedDate());
								dtoActivityLog.setUpdatedDate(activityLog.getUpdatedDate());
								dtoActivityLog.setType(activityLog.getType());
								dtoActivityLog.setUserName(activityLog.getUserName());
								dtoActiveLogList.add(dtoActivityLog);
							}
							dtoSearchActivity.setRecords(dtoActiveLogList);
						}
					}
				} catch (Exception e) {
					log.error(e);
				}
				return dtoSearchActivity;
			}
}	
