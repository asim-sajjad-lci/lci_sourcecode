package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.hcm.model.ApplicantCost;

public class DtoApplicantCost extends DtoBase{

	private Integer id;
	private DtoRequisitions dtoRequisitions;
	private DtoApplicant  dtoApplicant;
	private Integer sequence;
	private BigDecimal travelCost;
	private BigDecimal lodgingCost;
	private BigDecimal movingCost;
	private BigDecimal otherCost;
	private BigDecimal totalCost;
	private List<DtoApplicantCost> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	
	public BigDecimal getTravelCost() {
		return travelCost;
	}
	public void setTravelCost(BigDecimal travelCost) {
		this.travelCost = travelCost;
	}
	public BigDecimal getLodgingCost() {
		return lodgingCost;
	}
	public void setLodgingCost(BigDecimal lodgingCost) {
		this.lodgingCost = lodgingCost;
	}
	public BigDecimal getMovingCost() {
		return movingCost;
	}
	public void setMovingCost(BigDecimal movingCost) {
		this.movingCost = movingCost;
	}
	public BigDecimal getOtherCost() {
		return otherCost;
	}
	public void setOtherCost(BigDecimal otherCost) {
		this.otherCost = otherCost;
	}
	public BigDecimal getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}
	public List<DtoApplicantCost> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoApplicantCost> delete) {
		this.delete = delete;
	}
	public DtoRequisitions getDtoRequisitions() {
		return dtoRequisitions;
	}
	public void setDtoRequisitions(DtoRequisitions dtoRequisitions) {
		this.dtoRequisitions = dtoRequisitions;
	}
	public DtoApplicant getDtoApplicant() {
		return dtoApplicant;
	}
	public void setDtoApplicant(DtoApplicant dtoApplicant) {
		this.dtoApplicant = dtoApplicant;
	}
	
	public DtoApplicantCost() {
	}
	
	public DtoApplicantCost(ApplicantCost applicantCost) {
	}
	
}
