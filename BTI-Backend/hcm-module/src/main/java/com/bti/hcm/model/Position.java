package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the Position database table. Name of
 * Project: Hcm Version: 0.0.1
 * 
 * @author Gaurav
 *
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40103",indexes = {
        @Index(columnList = "POTINDX")
})
public class Position extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POTINDX")
	private int id;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "SKLSTINDX")
	private SkillSetSetup skillSetSetup;

	@Column(name = "POTID", columnDefinition = "char(15)")
	private String positionId;

	@OneToMany(mappedBy = "position",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "POTPLINCT = false and is_deleted = false")
	private List<PositionPalnSetup> listPositionPlan;

	@OneToMany(mappedBy = "position",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<PositionSetup> listPositionAttachment;

	@Column(name = "POTDSCR", columnDefinition = "char(31)")
	private String description;

	@Column(name = "POTDSCRA", columnDefinition = "char(61)")
	private String arabicDescription;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "POTCLSINDX")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private PositionClass positionClass;

	@Column(name = "RPTTOPOT", columnDefinition = "char(15)")
	private String reportToPostion;

	@Column(name = "POTLNGDSCR", columnDefinition = "char(255)")
	private String postionLongDesc;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "position")
	@Where(clause = "is_deleted = false")
	private List<PayScheduleSetupPosition> payScheduleSetupPosition;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "position")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<EmployeeMaster> employeeMaster;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "position")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<PayrollTransactionOpenYearDetails> payrollTransactionOpenYearDetails;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "position")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<ParollTransactionOpenYearDistribution> parollTransactionOpenYearDistribution;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "position")
	private List<Requisitions> requisitions;

	@OneToMany(mappedBy = "position",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeePositionHistory> listEmployeePositionHistory;

	@OneToMany(mappedBy = "position",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<PayrollAccounts> listPayrollAccounts;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="position")
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<ManualChecksDistribution> listManualChecksDistribution;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="position")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksOpenYearDistribution> listManualChecksOpenYearDistribution;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="position")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksHistoryYearDistributions>listManualChecksHistoryYearDistributions;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SkillSetSetup getSkillSetSetup() {
		return skillSetSetup;
	}

	public void setSkillSetSetup(SkillSetSetup skillSetSetup) {
		this.skillSetSetup = skillSetSetup;
	}

	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArabicDescription() {
		return arabicDescription;
	}

	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}

	public PositionClass getPositionClass() {
		return positionClass;
	}

	public void setPositionClass(PositionClass positionClass) {
		this.positionClass = positionClass;
	}

	public String getReportToPostion() {
		return reportToPostion;
	}

	public void setReportToPostion(String reportToPostion) {
		this.reportToPostion = reportToPostion;
	}

	public String getPostionLongDesc() {
		return postionLongDesc;
	}

	public void setPostionLongDesc(String postionLongDesc) {
		this.postionLongDesc = postionLongDesc;
	}

	public java.util.List<PositionPalnSetup> getListPositionPlan() {
		return listPositionPlan;
	}

	public void setListPositionPlan(java.util.List<PositionPalnSetup> listPositionPlan) {
		this.listPositionPlan = listPositionPlan;
	}

	public java.util.List<PositionSetup> getListPositionAttachment() {
		return listPositionAttachment;
	}

	public void setListPositionAttachment(java.util.List<PositionSetup> listPositionAttachment) {
		this.listPositionAttachment = listPositionAttachment;
	}

	public List<PayScheduleSetupPosition> getPayScheduleSetupPosition() {
		return payScheduleSetupPosition;
	}

	public void setPayScheduleSetupPosition(List<PayScheduleSetupPosition> payScheduleSetupPosition) {
		this.payScheduleSetupPosition = payScheduleSetupPosition;
	}

	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public List<Requisitions> getRequisitions() {
		return requisitions;
	}

	public void setRequisitions(List<Requisitions> requisitions) {
		this.requisitions = requisitions;
	}

	public List<EmployeePositionHistory> getListEmployeePositionHistory() {
		return listEmployeePositionHistory;
	}

	public void setListEmployeePositionHistory(List<EmployeePositionHistory> listEmployeePositionHistory) {
		this.listEmployeePositionHistory = listEmployeePositionHistory;
	}

	public List<PayrollAccounts> getListPayrollAccounts() {
		return listPayrollAccounts;
	}

	public void setListPayrollAccounts(List<PayrollAccounts> listPayrollAccounts) {
		this.listPayrollAccounts = listPayrollAccounts;
	}

	public List<PayrollTransactionOpenYearDetails> getPayrollTransactionOpenYearDetails() {
		return payrollTransactionOpenYearDetails;
	}

	public void setPayrollTransactionOpenYearDetails(
			List<PayrollTransactionOpenYearDetails> payrollTransactionOpenYearDetails) {
		this.payrollTransactionOpenYearDetails = payrollTransactionOpenYearDetails;
	}

	public List<ParollTransactionOpenYearDistribution> getParollTransactionOpenYearDistribution() {
		return parollTransactionOpenYearDistribution;
	}

	public void setParollTransactionOpenYearDistribution(
			List<ParollTransactionOpenYearDistribution> parollTransactionOpenYearDistribution) {
		this.parollTransactionOpenYearDistribution = parollTransactionOpenYearDistribution;
	}

	public List<ManualChecksDistribution> getListManualChecksDistribution() {
		return listManualChecksDistribution;
	}

	public void setListManualChecksDistribution(List<ManualChecksDistribution> listManualChecksDistribution) {
		this.listManualChecksDistribution = listManualChecksDistribution;
	}

	public List<ManualChecksOpenYearDistribution> getListManualChecksOpenYearDistribution() {
		return listManualChecksOpenYearDistribution;
	}

	public void setListManualChecksOpenYearDistribution(
			List<ManualChecksOpenYearDistribution> listManualChecksOpenYearDistribution) {
		this.listManualChecksOpenYearDistribution = listManualChecksOpenYearDistribution;
	}

	public List<ManualChecksHistoryYearDistributions> getListManualChecksHistoryYearDistributions() {
		return listManualChecksHistoryYearDistributions;
	}

	public void setListManualChecksHistoryYearDistributions(
			List<ManualChecksHistoryYearDistributions> listManualChecksHistoryYearDistributions) {
		this.listManualChecksHistoryYearDistributions = listManualChecksHistoryYearDistributions;
	}

}
