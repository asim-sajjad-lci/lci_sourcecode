package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoApplicantCost;
import com.bti.hcm.service.ServiceApplicantCost;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/applicantCost")
public class ControllerApplicantCost extends BaseController{

	private Logger log = Logger.getLogger(ControllerApplicantCost.class);
	
	@Autowired
	ServiceApplicantCost serviceApplicantCost;
	
	@Autowired
	ServiceResponse response;
	
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoApplicantCost dtoApplicantCost) throws Exception{
		log.info("Create ApplicantCost Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoApplicantCost = serviceApplicantCost.saveOrUpdate(dtoApplicantCost);
			responseMessage=displayMessage(dtoApplicantCost, "TIME_CODE_CREATED", "TIME_CODE_NOT_CREATED", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		log.debug("Create ApplicantCost Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoApplicantCost applicantCost) throws Exception {
		log.info("Update ApplicantCost Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			applicantCost = serviceApplicantCost.saveOrUpdate(applicantCost);
			responseMessage=displayMessage(applicantCost, "TIME_CODE_UPDATED_SUCCESS", "TIME_CODE_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Update ApplicantCost Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoApplicantCost dtoApplicantCost) throws Exception {
		log.info("Delete ApplicantCost Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoApplicantCost.getIds() != null && !dtoApplicantCost.getIds().isEmpty()) {
				DtoApplicantCost dtoApplicantCost1 = serviceApplicantCost.delete(dtoApplicantCost.getIds());
				if(dtoApplicantCost1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("TIME_CODE_DELETED", false), dtoApplicantCost1);

				}else {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false), dtoApplicantCost1);


				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete ApplicantCost Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoApplicantCost dtoApplicantCost) throws Exception {
		log.info("Get ApplicantCost ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoApplicantCost dtoApplicantCostNew = serviceApplicantCost.getById(dtoApplicantCost.getId());
			responseMessage=displayMessage(dtoApplicantCostNew, "TIME_CODE_GET_DETAIL", "TIME_CODE_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Get ApplicantCost by Id Method:"+dtoApplicantCost.getId());
		return responseMessage;
	}
}
