package com.bti.hcm.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.PayCodeType;
import com.bti.hcm.model.dto.DtoEmployeeCodeType;
import com.bti.hcm.model.dto.DtoEmployeeQuickAssigments;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoQuickAssiegmentDisplay;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryPayCodeType;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceEmployeeQuickAssigments")
public class ServiceEmployeeQuickAssigments {

	static Logger log = Logger.getLogger(ServiceEmployeeQuickAssigments.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;

	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;

	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryPayCodeType repositoryPayCodeType;

	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;

	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;

	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Transactional
	public DtoEmployeeQuickAssigments saveOrUpdate(DtoEmployeeQuickAssigments dtoEmployeeQuickAssigments) {
		log.info("saveOrUpdate EmployeeSkills Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		EmployeeMaster employeeMaster = null;

		if (dtoEmployeeQuickAssigments.getEmployeeId() != null && dtoEmployeeQuickAssigments.getEmployeeId() > 0) {
			employeeMaster = repositoryEmployeeMaster
					.findByEmployeeIndexIdAndIsDeleted(dtoEmployeeQuickAssigments.getEmployeeId(), false);
		}

		// 1 for Employee Deduction
		if (dtoEmployeeQuickAssigments.getCodeType().equals(1)) {

			repositoryEmployeeDeductionMaintenance.deleteByEmployeeIdAndCodeId(true, loggedInUserId,
					dtoEmployeeQuickAssigments.getEmployeeId());

			for (DtoEmployeeCodeType dtoEmployeeCodeType : dtoEmployeeQuickAssigments.getListCodeTypes()) {

				DeductionCode deductionCode = repositoryDeductionCode
						.findByIdAndIsDeleted(dtoEmployeeCodeType.getCodeId(), false);

				EmployeeDeductionMaintenance deductionMaintenance = null;
				if (dtoEmployeeCodeType.getId() != null && dtoEmployeeCodeType.getId() > 0) {
					deductionMaintenance = repositoryEmployeeDeductionMaintenance
							.findByIdAndIsDeleted(dtoEmployeeCodeType.getId(), false);
				} else {
					deductionMaintenance = new EmployeeDeductionMaintenance();
				}

				deductionMaintenance.setDeductionCode(deductionCode);
				deductionMaintenance.setEmployeeMaster(employeeMaster);
				deductionMaintenance.setStartDate(dtoEmployeeCodeType.getStartDate());
				deductionMaintenance.setEndDate(dtoEmployeeCodeType.getEndDate());
				deductionMaintenance.setTransactionRequired(deductionCode.isTransction());
				deductionMaintenance.setBenefitMethod(deductionCode.getMethod());
				deductionMaintenance.setDeductionAmount(dtoEmployeeCodeType.getAmount());

				deductionMaintenance.setDeductionPercent(dtoEmployeeCodeType.getPercent());
				deductionMaintenance.setPerPeriord(deductionCode.getPerPeriod());
				deductionMaintenance.setPerYear(deductionCode.getPerYear());
				deductionMaintenance.setLifeTime(deductionCode.getLifeTime());
				deductionMaintenance.setFrequency(deductionCode.getFrequency());
				deductionMaintenance.setInactive(dtoEmployeeCodeType.isInActive());

				repositoryEmployeeDeductionMaintenance.saveAndFlush(deductionMaintenance);
			}
		}

		// for payCode
		if (dtoEmployeeQuickAssigments.getCodeType().equals(2)) {

			System.out.println("hfPayline.Inside.PayCode");

//			repositoryEmployeePayCodeMaintenance.deleteByEmployeeId(true, loggedInUserId,
//					dtoEmployeeQuickAssigments.getEmployeeId());

			Integer employeeId = dtoEmployeeQuickAssigments.getEmployeeId();

			repositoryEmployeePayCodeMaintenance.deleteByEmployeeIdAndCodeId(true, loggedInUserId, employeeId); // ME
			System.out.println("hfPayline.0");
			int i = 1;
			for (DtoEmployeeCodeType dtoEmployeeCodeType : dtoEmployeeQuickAssigments.getListCodeTypes()) {
				System.out.println("hfPayline.1-" + i);
				PayCode payCode = repositoryPayCode.findByIdAndIsDeleted(dtoEmployeeCodeType.getCodeId(), false);
				int ii = payCode.getId();

				System.out.println("hfPayline.2-" + i + " ; PayCode : " + ii + " ; " + payCode);
				EmployeePayCodeMaintenance employeePayCodeMaintenanceForBaseOnPayCode = null;
				System.out.println("hfPayline.3-" + i);
				if (payCode.getBaseOnPayCodeId() != null && payCode.getBaseOnPayCodeId() > 0) {
					List<EmployeePayCodeMaintenance> listOfEmployee = repositoryEmployeePayCodeMaintenance
							.findByEmployeeIdAndCodeIdes(employeeId, payCode.getBaseOnPayCodeId());
					employeePayCodeMaintenanceForBaseOnPayCode = !CollectionUtils.isEmpty(listOfEmployee)
							? listOfEmployee.get(0)
							: new EmployeePayCodeMaintenance();
				}
				System.out.println("hfPayline.4-" + i);
				EmployeePayCodeMaintenance employeePayCodeMaintenance = null;
				if (dtoEmployeeCodeType.getEmpolyeeMaintananceId() != null) {
					employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
							.findByIdAndIsDeleted(dtoEmployeeCodeType.getEmpolyeeMaintananceId(), false);
				} else {
					employeePayCodeMaintenance = new EmployeePayCodeMaintenance();
				}
				System.out.println("hfPayline.5-" + i);
				// int ii = payCode.getId();
				employeePayCodeMaintenance.setPayCode(payCode);

				if (dtoEmployeeCodeType.getBaseOnPayCode() != null
						&& !dtoEmployeeCodeType.getBaseOnPayCode().isEmpty()) {
					System.out.println("hfPayline.6a" + i);
					employeePayCodeMaintenance.setBaseOnPayCode(payCode.getBaseOnPayCode());
					employeePayCodeMaintenance.setBaseOnPayCodeId(payCode.getBaseOnPayCodeId());
					employeePayCodeMaintenance.setAmount(dtoEmployeeCodeType.getAmount());
					float employeePayRate = employeePayCodeMaintenanceForBaseOnPayCode != null
							&& employeePayCodeMaintenanceForBaseOnPayCode.getPayRate() != null
									? employeePayCodeMaintenanceForBaseOnPayCode.getPayRate().floatValue()
									: 0f;
					float payCodePayFactor = payCode.getPayFactor() != null ? payCode.getPayFactor().floatValue() : 0f;
					float result = employeePayRate * payCodePayFactor;
					employeePayCodeMaintenance.setPayRate(new BigDecimal(result));

				} else {
					System.out.println("hfPayline.6b" + i);
					employeePayCodeMaintenance.setBaseOnPayCode("");
					employeePayCodeMaintenance.setPayRate(dtoEmployeeCodeType.getPayRate());
					employeePayCodeMaintenance.setAmount(dtoEmployeeCodeType.getAmount());

				}
//				BigDecimal payFactorCheck = new BigDecimal(0);
//				if (dtoEmployeeCodeType.getPayFactory() != payFactorCheck) {
//					employeePayCodeMaintenance.setPayFactory(payCode.getPayFactor());
//				}

				employeePayCodeMaintenance.setUnitOfPay(payCode.getUnitofPay());
				// employeePayCodeMaintenance.setPayRate(dtoEmployeeCodeType.getPayRate());
				System.out.println("hfPayline.7" + i);
				PayCodeType codeType = repositoryPayCodeType.findOne(1);
				employeePayCodeMaintenance.setPayCodeType(codeType);
				employeePayCodeMaintenance.setPayPeriod(payCode.getPayperiod());
				employeePayCodeMaintenance.setEmployeeMaster(employeeMaster);
				employeePayCodeMaintenance.setInactive(dtoEmployeeCodeType.isInActive());
				repositoryEmployeePayCodeMaintenance.saveAndFlush(employeePayCodeMaintenance);
			}
		}

		// for benefitCode
		if (dtoEmployeeQuickAssigments.getCodeType().equals(3)) {

			repositoryEmployeeBenefitMaintenance.deleteByEmployeeId(true, loggedInUserId,
					dtoEmployeeQuickAssigments.getEmployeeId());

			for (DtoEmployeeCodeType dtoEmployeeCodeType : dtoEmployeeQuickAssigments.getListCodeTypes()) {
				BenefitCode benefitCode = repositoryBenefitCode.findByIdAndIsDeleted(dtoEmployeeCodeType.getCodeId(),
						false);
				EmployeeBenefitMaintenance employeeBenefitMaintenance = null;
				if (dtoEmployeeCodeType.getId() != null && dtoEmployeeCodeType.getId() > 0) {
					employeeBenefitMaintenance = repositoryEmployeeBenefitMaintenance
							.findByIdAndIsDeleted(dtoEmployeeCodeType.getId(), false);
				} else {
					employeeBenefitMaintenance = new EmployeeBenefitMaintenance();
				}
				employeeBenefitMaintenance.setBenefitPercent(dtoEmployeeCodeType.getPercent());
				employeeBenefitMaintenance.setBenefitMethod(dtoEmployeeCodeType.getMethod());
				employeeBenefitMaintenance.setBenefitCode(benefitCode);
				employeeBenefitMaintenance.setEmployeeMaster(employeeMaster);
				employeeBenefitMaintenance.setBenefitAmount(dtoEmployeeCodeType.getAmount());
				employeeBenefitMaintenance.setInactive(dtoEmployeeCodeType.isInActive());
				employeeBenefitMaintenance.setStartDate(dtoEmployeeCodeType.getStartDate());
				employeeBenefitMaintenance.setEndDate(dtoEmployeeCodeType.getEndDate());
				employeeBenefitMaintenance.setTransactionRequired(benefitCode.isTransction());
				employeeBenefitMaintenance.setBenefitMethod(benefitCode.getMethod());
				employeeBenefitMaintenance.setPerPeriord(benefitCode.getPerPeriod());
				employeeBenefitMaintenance.setPerYear(benefitCode.getPerYear());
				employeeBenefitMaintenance.setLifeTime(benefitCode.getLifeTime());
				employeeBenefitMaintenance.setFrequency(benefitCode.getFrequency());

				repositoryEmployeeBenefitMaintenance.saveAndFlush(employeeBenefitMaintenance);

			}

		}

		return dtoEmployeeQuickAssigments;
	}

	@Transactional
	public DtoEmployeeQuickAssigments updateOnly(DtoEmployeeQuickAssigments dtoEmployeeQuickAssigments) {
		log.info("saveOrUpdate EmployeeSkills Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		EmployeeMaster employeeMaster = null;
		if (dtoEmployeeQuickAssigments.getEmployeeId() != null && dtoEmployeeQuickAssigments.getEmployeeId() > 0) {
			employeeMaster = repositoryEmployeeMaster
					.findByEmployeeIndexIdAndIsDeleted(dtoEmployeeQuickAssigments.getEmployeeId(), false);
			employeeMaster.setUpdatedBy(loggedInUserId);
		}

		// 1 for Employee Deduction
		if (dtoEmployeeQuickAssigments.getCodeType().equals(1)) {

			for (DtoEmployeeCodeType dtoEmployeeCodeType : dtoEmployeeQuickAssigments.getListCodeTypes()) {

				if (dtoEmployeeCodeType.getEmpolyeeMaintananceId() != null) {

					EmployeeDeductionMaintenance deductionMaintenance = repositoryEmployeeDeductionMaintenance
							.findByIdAndIsDeleted(dtoEmployeeCodeType.getEmpolyeeMaintananceId(), false);

					if (deductionMaintenance != null) {
						DeductionCode deductionCode = repositoryDeductionCode
								.findByIdAndIsDeleted(dtoEmployeeCodeType.getEmpolyeeMaintananceId(), false);

						if (dtoEmployeeCodeType.getEmpolyeeMaintananceId() != null
								&& dtoEmployeeCodeType.getEmpolyeeMaintananceId() > 0) {
							deductionMaintenance = repositoryEmployeeDeductionMaintenance
									.findByIdAndIsDeleted(dtoEmployeeCodeType.getId(), false);
						} else {
							deductionMaintenance = new EmployeeDeductionMaintenance();
						}

						deductionMaintenance.setDeductionCode(deductionCode);
						deductionMaintenance.setEmployeeMaster(employeeMaster);
						deductionMaintenance.setStartDate(dtoEmployeeCodeType.getStartDate());
						deductionMaintenance.setEndDate(dtoEmployeeCodeType.getEndDate());
						deductionMaintenance.setTransactionRequired(deductionCode.isTransction());
						deductionMaintenance.setBenefitMethod(deductionCode.getMethod());
						deductionMaintenance.setDeductionAmount(dtoEmployeeCodeType.getAmount());
						deductionMaintenance.setDeductionPercent(dtoEmployeeCodeType.getPercent());
						deductionMaintenance.setPerPeriord(deductionCode.getPerPeriod());
						deductionMaintenance.setPerYear(deductionCode.getPerYear());
						deductionMaintenance.setLifeTime(deductionCode.getLifeTime());
						deductionMaintenance.setFrequency(deductionCode.getFrequency());
						deductionMaintenance.setInactive(dtoEmployeeCodeType.isInActive());

						repositoryEmployeeDeductionMaintenance.saveAndFlush(deductionMaintenance);
					}

				}
			}
		}
		// for payCode
		if (dtoEmployeeQuickAssigments.getCodeType().equals(2)) {

			// repositoryEmployeePayCodeMaintenance.deleteByEmployeeId(true,
			// loggedInUserId,dtoEmployeeQuickAssigments.getEmployeeId());
			// if(dtoEmployeeQuickAssigments.getEmpolyeeMaintananceId()!=null)
			// EmployeePayCodeMaintenance employeePayCodeMaintenance =
			// repositoryEmployeePayCodeMaintenance.findByIdAndIsDeleted(dtoEmployeeQuickAssigments.getEmpolyeeMaintananceId(),
			// false);

			// if(employeePayCodeMaintenance!=null)

			for (DtoEmployeeCodeType dtoEmployeeCodeType : dtoEmployeeQuickAssigments.getListCodeTypes()) {
				if (dtoEmployeeCodeType.getEmpolyeeMaintananceId() != null) {
					EmployeePayCodeMaintenance employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
							.findByIdAndIsDeleted(dtoEmployeeCodeType.getEmpolyeeMaintananceId(), false);
					if (employeePayCodeMaintenance != null) {
						PayCode payCode = repositoryPayCode.findByIdAndIsDeleted(dtoEmployeeCodeType.getCodeId(),
								false);

						if (dtoEmployeeCodeType.getEmpolyeeMaintananceId() != null
								&& dtoEmployeeCodeType.getEmpolyeeMaintananceId() > 0) {
							employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
									.findByIdAndIsDeleted(dtoEmployeeCodeType.getEmpolyeeMaintananceId(), false);
						} else {
							employeePayCodeMaintenance = new EmployeePayCodeMaintenance();
						}
						employeePayCodeMaintenance.setPayCode(payCode);
						employeePayCodeMaintenance.setUnitOfPay(payCode.getUnitofPay());
						employeePayCodeMaintenance.setPayRate(dtoEmployeeCodeType.getPayRate());

						PayCodeType codeType = repositoryPayCodeType.findOne(1);
						employeePayCodeMaintenance.setPayCodeType(codeType);
						employeePayCodeMaintenance.setPayPeriod(payCode.getPayperiod());
						employeePayCodeMaintenance.setEmployeeMaster(employeeMaster);
						employeePayCodeMaintenance.setAmount(employeePayCodeMaintenance.getPayRate());
						employeePayCodeMaintenance.setInactive(dtoEmployeeCodeType.isInActive());
						repositoryEmployeePayCodeMaintenance.saveAndFlush(employeePayCodeMaintenance);
					}

				}

			}

		}

		// for benefitCode
		if (dtoEmployeeQuickAssigments.getCodeType().equals(3)) {

			for (DtoEmployeeCodeType dtoEmployeeCodeType : dtoEmployeeQuickAssigments.getListCodeTypes()) {
				if (dtoEmployeeCodeType.getEmpolyeeMaintananceId() != null) {
					EmployeeBenefitMaintenance employeeBenefitMaintenance = repositoryEmployeeBenefitMaintenance
							.findByIdAndIsDeleted(dtoEmployeeCodeType.getEmpolyeeMaintananceId(), false);
					if (employeeBenefitMaintenance != null) {
						BenefitCode benefitCode = repositoryBenefitCode
								.findByIdAndIsDeleted(dtoEmployeeCodeType.getCodeId(), false);
						if (dtoEmployeeCodeType.getEmpolyeeMaintananceId() != null
								&& dtoEmployeeCodeType.getEmpolyeeMaintananceId() > 0) {
							employeeBenefitMaintenance = repositoryEmployeeBenefitMaintenance
									.findByIdAndIsDeleted(dtoEmployeeCodeType.getEmpolyeeMaintananceId(), false);
						} else {
							employeeBenefitMaintenance = new EmployeeBenefitMaintenance();
						}
						employeeBenefitMaintenance.setBenefitPercent(dtoEmployeeCodeType.getPercent());
						employeeBenefitMaintenance.setBenefitMethod(dtoEmployeeCodeType.getMethod());
						employeeBenefitMaintenance.setBenefitCode(benefitCode);
						employeeBenefitMaintenance.setEmployeeMaster(employeeMaster);
						employeeBenefitMaintenance.setBenefitAmount(dtoEmployeeCodeType.getAmount());
						employeeBenefitMaintenance.setInactive(dtoEmployeeCodeType.isInActive());
						employeeBenefitMaintenance.setStartDate(dtoEmployeeCodeType.getStartDate());
						employeeBenefitMaintenance.setEndDate(dtoEmployeeCodeType.getEndDate());
						employeeBenefitMaintenance.setTransactionRequired(benefitCode.isTransction());
						employeeBenefitMaintenance.setBenefitMethod(benefitCode.getMethod());
						employeeBenefitMaintenance.setPerPeriord(benefitCode.getPerPeriod());
						employeeBenefitMaintenance.setPerYear(benefitCode.getPerYear());
						employeeBenefitMaintenance.setLifeTime(benefitCode.getLifeTime());
						employeeBenefitMaintenance.setFrequency(benefitCode.getFrequency());

						repositoryEmployeeBenefitMaintenance.saveAndFlush(employeeBenefitMaintenance);
					}

				}

			}

		}

		return dtoEmployeeQuickAssigments;
	}

	public DtoSearch getAllByCodeType(DtoEmployeeQuickAssigments dtoEmployeeQuickAssigments) {
		DtoSearch dtoSearch = new DtoSearch();
		List<DtoQuickAssiegmentDisplay> listResponse = new ArrayList<>();

		// PayCode
		if (dtoEmployeeQuickAssigments.getCodeType().equals(2)) {

			List<DtoQuickAssiegmentDisplay> listDtoEmployeeQuickAssigments = new ArrayList<>();

			if (dtoEmployeeQuickAssigments.isAllCompanyCode()) {

				List<PayCode> payCodeList = repositoryPayCode.findByIsDeleted(false);

				for (PayCode payCode : payCodeList) {

					long count = listDtoEmployeeQuickAssigments.stream()
							.filter(d -> d.getCodeId().equals(payCode.getId())).count();
					if (count == 0) {
						DtoQuickAssiegmentDisplay dtoQuickAssiegmentDisplay = new DtoQuickAssiegmentDisplay();
						dtoQuickAssiegmentDisplay.setCodeId(payCode.getId());
						dtoQuickAssiegmentDisplay.setCode(payCode.getPayCodeId());
						dtoQuickAssiegmentDisplay.setDesc(payCode.getDescription());
						dtoQuickAssiegmentDisplay.setArabicDesc(payCode.getArbicDescription());
						dtoQuickAssiegmentDisplay.setActive(payCode.isInActive());
						dtoQuickAssiegmentDisplay.setAmount(payCode.getBaseOnPayCodeAmount());
						dtoQuickAssiegmentDisplay.setMasterId(payCode.getId()); // id
						dtoQuickAssiegmentDisplay.setEmpolyeeMaintananceId(payCode.getId()); // same q? id

						dtoQuickAssiegmentDisplay.setPayFactor(payCode.getPayFactor());
						dtoQuickAssiegmentDisplay.setPayRate(payCode.getPayRate());

						dtoQuickAssiegmentDisplay.setBaseOnPayCode(payCode.getBaseOnPayCode());

						if (payCode.getBaseOnPayCodeId() != null) {

							PayCode paycodeP = repositoryPayCode.findByIdAndIsDeleted(payCode.getBaseOnPayCodeId(),
									false);

							if (paycodeP != null) {
								System.out.println("Parent PayCode.id : " + paycodeP.getId());
								dtoQuickAssiegmentDisplay.setMasterPayCodeId(paycodeP.getId());
								dtoQuickAssiegmentDisplay.setParentPayFactor(paycodeP.getPayFactor()); //
								// dtoQuickAssiegmentDisplay.setParentPayRate(paycodeM.getPayRate());
								EmployeePayCodeMaintenance employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
										.findByPayCodeIdAndIsDeletedAndEmployeeMasterEmployeeIndexIdAndEmployeeMasterIsDeleted(
												paycodeP.getId(), false,
												dtoEmployeeQuickAssigments.getEmployeeId(), false);
								if (employeePayCodeMaintenance != null) {
									dtoQuickAssiegmentDisplay.setParentPayRate(employeePayCodeMaintenance.getPayRate()); // this
									System.out.println("AllCompanyCode: false & empId: "
											+ dtoEmployeeQuickAssigments.getEmployeeId() + "RayRate: "
											+ employeePayCodeMaintenance.getPayRate());
								} else
									dtoQuickAssiegmentDisplay.setParentPayRate(paycodeP.getPayRate()); // just extra
							
							}
						}
						listDtoEmployeeQuickAssigments.add(dtoQuickAssiegmentDisplay);
					}
				}

			} else { // HF dtd 7Jul

				List<EmployeePayCodeMaintenance> employeePayCodeMaintenanceList = new ArrayList<>();
				employeePayCodeMaintenanceList = this.repositoryEmployeePayCodeMaintenance
						.findByEmployeeId(dtoEmployeeQuickAssigments.getEmployeeId());

				for (EmployeePayCodeMaintenance employeePayCodeMaintenance : employeePayCodeMaintenanceList) {
					DtoQuickAssiegmentDisplay dtoQuickAssiegmentDisplay = new DtoQuickAssiegmentDisplay();
					if (employeePayCodeMaintenance.getEmployeeMaster() != null) {
						dtoQuickAssiegmentDisplay
								.setEmployeeId(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId());
					}

					dtoQuickAssiegmentDisplay.setMasterId(employeePayCodeMaintenance.getId()); // id
					dtoQuickAssiegmentDisplay.setEmpolyeeMaintananceId(employeePayCodeMaintenance.getId()); // same q?
					dtoQuickAssiegmentDisplay.setActive(employeePayCodeMaintenance.getInactive());
					dtoQuickAssiegmentDisplay.setAmount(employeePayCodeMaintenance.getAmount());
					dtoQuickAssiegmentDisplay.setStartDate(new Date());
					dtoQuickAssiegmentDisplay.setEndDate(new Date());
					dtoQuickAssiegmentDisplay.setPayFactor(employeePayCodeMaintenance.getPayFactory());
					dtoQuickAssiegmentDisplay.setPayRate(employeePayCodeMaintenance.getPayRate());
					dtoQuickAssiegmentDisplay
							.setBaseOnPayCode(employeePayCodeMaintenance.getPayCode().getBaseOnPayCode());

					if (employeePayCodeMaintenance.getPayCode() != null) {
						dtoQuickAssiegmentDisplay.setCodeId(employeePayCodeMaintenance.getPayCode().getId());
						dtoQuickAssiegmentDisplay.setCode(employeePayCodeMaintenance.getPayCode().getPayCodeId());
						dtoQuickAssiegmentDisplay.setDesc(employeePayCodeMaintenance.getPayCode().getDescription());
						dtoQuickAssiegmentDisplay
								.setArabicDesc(employeePayCodeMaintenance.getPayCode().getArbicDescription());

						if (employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeId() != null) {

							EmployeePayCodeMaintenance employeePayCodeMaintenance2 = repositoryEmployeePayCodeMaintenance
									.findByPayCodeIdAndIsDeletedAndEmployeeMasterEmployeeIndexIdAndEmployeeMasterIsDeleted(
											employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeId(), false,
											dtoEmployeeQuickAssigments.getEmployeeId(), false); // Based on payCode id,
																								// get another PayCode

							System.out.println(
									"for AllCompanyCode: false & empId: " + dtoEmployeeQuickAssigments.getEmployeeId()
											+ "RayRate: " + employeePayCodeMaintenance2.getPayRate());

							if (employeePayCodeMaintenance2.getPayRate() != null)
								dtoQuickAssiegmentDisplay.setParentPayRate(employeePayCodeMaintenance2.getPayRate()); // this
							else
								dtoQuickAssiegmentDisplay
										.setParentPayRate(employeePayCodeMaintenance.getPayCode().getPayRate()); // just
																													// //
																													// extra

							System.out.println("Reached Here!  its basedOnPayCode...!");

							// base on pay code already set previously
							PayCode paycodeP = repositoryPayCode.findByIdAndIsDeleted(
									employeePayCodeMaintenance.getPayCode().getBaseOnPayCodeId(), false);
							System.out.println("Parent PayCode.id : " + paycodeP.getId());
							dtoQuickAssiegmentDisplay.setMasterPayCodeId(paycodeP.getId());
							// dtoQuickAssiegmentDisplay.setParentPayRate(paycodeM.getPayRate());
							dtoQuickAssiegmentDisplay.setParentPayFactor(paycodeP.getPayFactor());
						} // else { //if not based on pay code then - DoNothing

					} else {
						System.out.println(" employeePayCodeMaintenance.PayCode == null ! ");
					}

					listDtoEmployeeQuickAssigments.add(dtoQuickAssiegmentDisplay);
				}

			}

			dtoSearch.setTotalCount(listDtoEmployeeQuickAssigments.size());

			if (dtoEmployeeQuickAssigments.getFrom() != null && dtoEmployeeQuickAssigments.getToIndex() != null
					&& dtoEmployeeQuickAssigments.getFrom() <= listDtoEmployeeQuickAssigments.size()
					&& dtoEmployeeQuickAssigments.getToIndex() <= listDtoEmployeeQuickAssigments.size()) {
				listResponse = listDtoEmployeeQuickAssigments.subList(dtoEmployeeQuickAssigments.getFrom(),
						dtoEmployeeQuickAssigments.getToIndex());
				dtoSearch.setRecords(listResponse);
			} else {
				if (dtoEmployeeQuickAssigments.getFrom() == null && dtoEmployeeQuickAssigments.getToIndex() == null) {
					dtoSearch.setRecords(listDtoEmployeeQuickAssigments);
				} else {
					if (listResponse.isEmpty()
							&& dtoEmployeeQuickAssigments.getFrom() <= listDtoEmployeeQuickAssigments.size()) {
						listDtoEmployeeQuickAssigments.subList(0, dtoEmployeeQuickAssigments.getFrom()).clear();
						dtoSearch.setRecords(listDtoEmployeeQuickAssigments);
					} else {
						dtoSearch.setRecords(listResponse);
					}
				}
			}

		}

		// DeductionCode
		if (dtoEmployeeQuickAssigments.getCodeType().equals(1)) {

			List<DtoQuickAssiegmentDisplay> listDtoEmployeeQuickAssigments = new ArrayList<>();

			List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = new ArrayList<>();
			employeeDeductionMaintenanceList = this.repositoryEmployeeDeductionMaintenance
					.getByEmployeeId(dtoEmployeeQuickAssigments.getEmployeeId());

			List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
					.findByEmployeeId(dtoEmployeeQuickAssigments.getEmployeeId());

			for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeDeductionMaintenanceList) {
				DtoQuickAssiegmentDisplay dtoQuickAssiegmentDisplay = new DtoQuickAssiegmentDisplay();

				if (employeeDeductionMaintenance.getEmployeeMaster() != null) {
					dtoQuickAssiegmentDisplay
							.setEmployeeId(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId());
				}
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeeDeductionMaintenance
						.getEmployeeMaster().getListEmployeePayCodeMaintenance()) {
					if (!employeePayCodeMaintenance2.getInactive()
							&& employeePayCodeMaintenance2.getBaseOnPayCode() != null && employeePayCodeMaintenance2
									.getBaseOnPayCode().equals(employeePayCodeMaintenance2.getBaseOnPayCode())) {
						dtoQuickAssiegmentDisplay.setPayRate(employeePayCodeMaintenance2.getPayRate());
					}
				}
				if (employeeDeductionMaintenance.getDeductionPercent() != null) {
					dtoQuickAssiegmentDisplay.setPercent(employeeDeductionMaintenance.getDeductionPercent());
				}
				if (employeeDeductionMaintenance.getBenefitMethod() != null) {
					dtoQuickAssiegmentDisplay.setMethod(employeeDeductionMaintenance.getBenefitMethod());
				}
				dtoQuickAssiegmentDisplay.setMasterId(employeeDeductionMaintenance.getId());
				dtoQuickAssiegmentDisplay.setActive(employeeDeductionMaintenance.getInactive());
				dtoQuickAssiegmentDisplay.setAmount(employeeDeductionMaintenance.getDeductionAmount());
				if (employeeDeductionMaintenance.getEmployeeMaster() != null
						&& employeeDeductionMaintenance.getDeductionCode().getNoOfDays() != null
						&& employeeDeductionMaintenance.getDeductionCode().getEndDateDays() != null) {

					dtoQuickAssiegmentDisplay.setStartDate(
							UtilDateAndTimeHr.startDate(employeeDeductionMaintenance.getDeductionCode().getNoOfDays(),
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeHireDate()));
					dtoQuickAssiegmentDisplay.setEndDate(
							UtilDateAndTimeHr.endDate(employeeDeductionMaintenance.getDeductionCode().getNoOfDays(),
									employeeDeductionMaintenance.getDeductionCode().getEndDateDays(),
									employeeDeductionMaintenance.getEmployeeMaster().getEmployeeHireDate()));
				} else {
					dtoQuickAssiegmentDisplay.setStartDate(employeeDeductionMaintenance.getStartDate());
					dtoQuickAssiegmentDisplay.setEndDate(employeeDeductionMaintenance.getEndDate());
				}
				dtoQuickAssiegmentDisplay.setEmpolyeeMaintananceId(employeeDeductionMaintenance.getId());

				for (EmployeePayCodeMaintenance employeePayCodeMaintenance2 : employeePayCodeMaintenance) {
					if (employeePayCodeMaintenance2.getBaseOnPayCode() != null && employeePayCodeMaintenance2
							.getBaseOnPayCode().equals(employeePayCodeMaintenance2.getBaseOnPayCode())) {
						dtoQuickAssiegmentDisplay.setPayRate(employeePayCodeMaintenance2.getPayRate());
					}
				}

				if (employeeDeductionMaintenance.getDeductionCode() != null) {
					dtoQuickAssiegmentDisplay.setCodeId(employeeDeductionMaintenance.getDeductionCode().getId());
					dtoQuickAssiegmentDisplay.setCode(employeeDeductionMaintenance.getDeductionCode().getDiductionId());
					dtoQuickAssiegmentDisplay.setDesc(employeeDeductionMaintenance.getDeductionCode().getDiscription());

					if (employeeDeductionMaintenance.getDeductionPercent() != null) {
						dtoQuickAssiegmentDisplay.setPercent(employeeDeductionMaintenance.getDeductionPercent());
					}
					if (employeeDeductionMaintenance.getBenefitMethod() != null) {
						dtoQuickAssiegmentDisplay.setMethod(employeeDeductionMaintenance.getBenefitMethod());
					}

					dtoQuickAssiegmentDisplay
							.setArabicDesc(employeeDeductionMaintenance.getDeductionCode().getArbicDiscription());
					if (employeeDeductionMaintenance.getDeductionCode().getPayCodeList() != null
							&& !employeeDeductionMaintenance.getDeductionCode().getPayCodeList().isEmpty()) {

						List<Integer> listId = new ArrayList<>();

						for (PayCode payCode : employeeDeductionMaintenance.getDeductionCode().getPayCodeList()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance1 : employeePayCodeMaintenance) {

								if (payCode.getId() == employeePayCodeMaintenance1.getPayCode().getId()) {
									listId.add(payCode.getId());
									dtoQuickAssiegmentDisplay.setPayRate(employeePayCodeMaintenance1.getPayRate());
								}

							}
						}

						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCode2.setAmount(payCode1.getBaseOnPayCodeAmount());
								payCode2.setPayFactor(payCode1.getPayFactor());
								for (EmployeePayCodeMaintenance employeePayCode : payCode1
										.getListEmployeePayCodeMaintenance()) {
									if ((employeePayCode.getPayCode().getId().equals(payCode1.getId()))
											&& (employeePayCode.getEmployeeMaster()
													.getEmployeeIndexId() == dtoEmployeeQuickAssigments
															.getEmployeeId())) {
										payCode2.setEmployeeId(
												employeePayCode.getEmployeeMaster().getEmployeeIndexId());
										payCode2.setPayRate(employeePayCode.getPayRate());

									}
								}
								payCodeList.add(payCode2);
							}
							dtoQuickAssiegmentDisplay.setDtoPayCode(payCodeList);
						}
					}
					listDtoEmployeeQuickAssigments.add(dtoQuickAssiegmentDisplay);
				}

			}

			if (dtoEmployeeQuickAssigments.isAllCompanyCode()) {
				List<DeductionCode> deductionCode = repositoryDeductionCode.findByIsDeleted(false);
				for (DeductionCode deductionCode2 : deductionCode) {

					long count = listDtoEmployeeQuickAssigments.stream()
							.filter(d -> d.getCodeId().equals(deductionCode2.getId())).count();

					if (count == 0) {
						DtoQuickAssiegmentDisplay dtoQuickAssiegmentDisplay = new DtoQuickAssiegmentDisplay();

						dtoQuickAssiegmentDisplay.setStartDate(deductionCode2.getStartDate());
						dtoQuickAssiegmentDisplay.setEndDate(deductionCode2.getEndDate());
						if (deductionCode2.getMethod() != null) {
							dtoQuickAssiegmentDisplay.setMethod(deductionCode2.getMethod());
						}
						if (deductionCode2.getPercent() != null) {
							dtoQuickAssiegmentDisplay.setPercent(deductionCode2.getPercent());
						}

						dtoQuickAssiegmentDisplay.setPayFactor(deductionCode2.getPayFactor());

						if (deductionCode2.getPayCodeList() != null && !deductionCode2.getPayCodeList().isEmpty()) {

							List<Integer> listId = new ArrayList<>();

							for (PayCode payCode : deductionCode2.getPayCodeList()) {
								for (EmployeePayCodeMaintenance employeePayCodeMaintenance1 : employeePayCodeMaintenance) {
									if (payCode.getId() == employeePayCodeMaintenance1.getPayCode().getId()) {
										listId.add(payCode.getId());
										dtoQuickAssiegmentDisplay.setPayRate(employeePayCodeMaintenance1.getPayRate());

										dtoQuickAssiegmentDisplay.setPayFactor(payCode.getPayFactor());

									}
								}
							}
							List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
							if (!payCode.isEmpty()) {
								List<DtoPayCode> payCodeList = new ArrayList<>();
								for (PayCode payCode1 : payCode) {
									DtoPayCode payCode2 = new DtoPayCode();
									payCode2.setId(payCode1.getId());
									payCode2.setPayCodeId(payCode1.getPayCodeId());
									payCode2.setAmount(payCode1.getBaseOnPayCodeAmount());
									payCode2.setPayFactor(payCode1.getPayFactor());
									payCode2.setEmployeeId(dtoEmployeeQuickAssigments.getEmployeeId());
									;
									for (EmployeePayCodeMaintenance employeePayCode : payCode1
											.getListEmployeePayCodeMaintenance()) {
										if (employeePayCode.getPayCode().getId().equals(payCode1.getId())
												&& employeePayCode.getEmployeeMaster()
														.getEmployeeIndexId() == dtoEmployeeQuickAssigments
																.getEmployeeId()) {

											payCode2.setEmployeeId(
													employeePayCode.getEmployeeMaster().getEmployeeIndexId());
											payCode2.setPayRate(employeePayCode.getPayRate());
											payCode2.setPayFactor(employeePayCode.getPayFactory());
										}
									}
									payCodeList.add(payCode2);
								}
								dtoQuickAssiegmentDisplay.setDtoPayCode(payCodeList);
							}
						}
						dtoQuickAssiegmentDisplay.setCodeId(deductionCode2.getId());
						dtoQuickAssiegmentDisplay.setCode(deductionCode2.getDiductionId());
						dtoQuickAssiegmentDisplay.setDesc(deductionCode2.getDiscription());
						dtoQuickAssiegmentDisplay.setArabicDesc(deductionCode2.getArbicDiscription());
						dtoQuickAssiegmentDisplay.setActive(deductionCode2.isInActive());
						dtoQuickAssiegmentDisplay.setAmount(deductionCode2.getAmount());
						listDtoEmployeeQuickAssigments.add(dtoQuickAssiegmentDisplay);
					}

				}
			}

			dtoSearch.setTotalCount(listDtoEmployeeQuickAssigments.size());
			if (dtoEmployeeQuickAssigments.getFrom() != null && dtoEmployeeQuickAssigments.getToIndex() != null
					&& dtoEmployeeQuickAssigments.getFrom() <= listDtoEmployeeQuickAssigments.size()
					&& dtoEmployeeQuickAssigments.getToIndex() <= listDtoEmployeeQuickAssigments.size()) {
				listResponse = listDtoEmployeeQuickAssigments.subList(dtoEmployeeQuickAssigments.getFrom(),
						dtoEmployeeQuickAssigments.getToIndex());
				dtoSearch.setRecords(listResponse);
			} else {
				if (dtoEmployeeQuickAssigments.getFrom() == null && dtoEmployeeQuickAssigments.getToIndex() == null) {
					dtoSearch.setRecords(listDtoEmployeeQuickAssigments);
				} else {

					if (listResponse.isEmpty()
							&& dtoEmployeeQuickAssigments.getFrom() <= listDtoEmployeeQuickAssigments.size()) {
						listDtoEmployeeQuickAssigments.subList(0, dtoEmployeeQuickAssigments.getFrom()).clear();
						dtoSearch.setRecords(listDtoEmployeeQuickAssigments);
					} else {
						dtoSearch.setRecords(listResponse);
					}

				}

			}

			// dtoSearch.setRecords(listDtoEmployeeQuickAssigments);
		}

		// BenefitCode
		if (dtoEmployeeQuickAssigments.getCodeType().equals(3)) {

			List<DtoQuickAssiegmentDisplay> listDtoEmployeeQuickAssigments = new ArrayList<>();

			List<EmployeeBenefitMaintenance> employeeBenefitMaintenanceList = new ArrayList<>();
			employeeBenefitMaintenanceList = this.repositoryEmployeeBenefitMaintenance
					.findByEmployeeId(dtoEmployeeQuickAssigments.getEmployeeId());

			List<EmployeePayCodeMaintenance> employeePayCodeMaintenance = repositoryEmployeePayCodeMaintenance
					.findByIsDeleted(false);
			for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeBenefitMaintenanceList) {
				DtoQuickAssiegmentDisplay dtoQuickAssiegmentDisplay = new DtoQuickAssiegmentDisplay();

				if (employeeBenefitMaintenance.getEmployeeMaster() != null) {
					dtoQuickAssiegmentDisplay
							.setEmployeeId(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId());
				}
				if (employeeBenefitMaintenance.getBenefitPercent() != null) {
					dtoQuickAssiegmentDisplay.setPercent(employeeBenefitMaintenance.getBenefitPercent());
				}
				if (employeeBenefitMaintenance.getBenefitMethod() != null) {
					dtoQuickAssiegmentDisplay.setMethod(employeeBenefitMaintenance.getBenefitMethod());
				}
				if (employeeBenefitMaintenance.getBenefitMethod() != null) {
					dtoQuickAssiegmentDisplay.setMethod(employeeBenefitMaintenance.getBenefitMethod());
				}
				if (employeeBenefitMaintenance.getBenefitPercent() != null) {
					dtoQuickAssiegmentDisplay.setPercent(employeeBenefitMaintenance.getBenefitPercent());
				}
				dtoQuickAssiegmentDisplay.setMasterId(employeeBenefitMaintenance.getId());
				dtoQuickAssiegmentDisplay.setActive(employeeBenefitMaintenance.getInactive());
				dtoQuickAssiegmentDisplay.setAmount(employeeBenefitMaintenance.getBenefitAmount());
				if (employeeBenefitMaintenance.getEmployeeMaster() != null
						&& employeeBenefitMaintenance.getBenefitCode().getNoOfDays() != null
						&& employeeBenefitMaintenance.getBenefitCode().getEndDateDays() != null) {

					dtoQuickAssiegmentDisplay.setStartDate(
							UtilDateAndTimeHr.startDate(employeeBenefitMaintenance.getBenefitCode().getNoOfDays(),
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeHireDate()));
					dtoQuickAssiegmentDisplay.setEndDate(
							UtilDateAndTimeHr.endDate(employeeBenefitMaintenance.getBenefitCode().getNoOfDays(),
									employeeBenefitMaintenance.getBenefitCode().getEndDateDays(),
									employeeBenefitMaintenance.getEmployeeMaster().getEmployeeHireDate()));
				} else {
					dtoQuickAssiegmentDisplay.setStartDate(employeeBenefitMaintenance.getStartDate());
					dtoQuickAssiegmentDisplay.setEndDate(employeeBenefitMaintenance.getEndDate());
				}
				if (employeeBenefitMaintenance.getBenefitCode() != null) {

					if (employeeBenefitMaintenance.getBenefitPercent() != null) {
						dtoQuickAssiegmentDisplay.setPercent(employeeBenefitMaintenance.getBenefitPercent());
					}
					if (employeeBenefitMaintenance.getBenefitMethod() != null) {
						dtoQuickAssiegmentDisplay.setMethod(employeeBenefitMaintenance.getBenefitMethod());
					}

					dtoQuickAssiegmentDisplay.setCodeId(employeeBenefitMaintenance.getBenefitCode().getId());
					dtoQuickAssiegmentDisplay.setCode(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
					dtoQuickAssiegmentDisplay.setDesc(employeeBenefitMaintenance.getBenefitCode().getDesc());
					dtoQuickAssiegmentDisplay.setArabicDesc(employeeBenefitMaintenance.getBenefitCode().getArbicDesc());
				}
				if (employeeBenefitMaintenance.getBenefitCode().getPayCodeList() != null
						&& !employeeBenefitMaintenance.getBenefitCode().getPayCodeList().isEmpty()) {

					List<Integer> listId = new ArrayList<>();

					for (PayCode payCode : employeeBenefitMaintenance.getBenefitCode().getPayCodeList()) {
						for (EmployeePayCodeMaintenance employeePayCodeMaintenance1 : employeePayCodeMaintenance) {
							if (payCode.getId() == employeePayCodeMaintenance1.getPayCode().getId()) {
								listId.add(payCode.getId());
								dtoQuickAssiegmentDisplay.setPayRate(employeePayCodeMaintenance1.getPayRate());
							}
						}
					}
					List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
					if (!payCode.isEmpty()) {
						List<DtoPayCode> payCodeList = new ArrayList<>();
						for (PayCode payCode1 : payCode) {
							DtoPayCode payCode2 = new DtoPayCode();
							payCode2.setId(payCode1.getId());
							payCode2.setPayCodeId(payCode1.getPayCodeId());
							payCode2.setAmount(payCode1.getBaseOnPayCodeAmount());
							payCode2.setPayFactor(payCode1.getPayFactor());
							for (EmployeePayCodeMaintenance employeePayCode : payCode1
									.getListEmployeePayCodeMaintenance()) {
								if (employeePayCode.getPayCode().getId().equals(payCode1.getId())
										&& employeePayCode.getEmployeeMaster()
												.getEmployeeIndexId() == dtoEmployeeQuickAssigments.getEmployeeId()) {
									payCode2.setEmployeeId(employeePayCode.getEmployeeMaster().getEmployeeIndexId());

									payCode2.setPayRate(employeePayCode.getPayRate());
								}
							}
							payCodeList.add(payCode2);
						}
						dtoQuickAssiegmentDisplay.setDtoPayCode(payCodeList);
					}
				}

				listDtoEmployeeQuickAssigments.add(dtoQuickAssiegmentDisplay);
			}

			if (dtoEmployeeQuickAssigments.isAllCompanyCode()) {
				List<BenefitCode> benefitCode = repositoryBenefitCode.findByIsDeleted(false);

				for (BenefitCode benefitCode1 : benefitCode) {
					long count = listDtoEmployeeQuickAssigments.stream()
							.filter(d -> d.getCodeId().equals(benefitCode1.getId())).count();
					DtoQuickAssiegmentDisplay dtoQuickAssiegmentDisplay = new DtoQuickAssiegmentDisplay();

					if (count == 0) {

						for (EmployeeBenefitMaintenance employeeMaster2 : benefitCode1
								.getListEmployeeBenefitMaintenance()) {
							if (employeeMaster2.getEmployeeMaster().getEmployeeIndexId() == dtoEmployeeQuickAssigments
									.getEmployeeId() && benefitCode1.getNoOfDays() != null
									&& employeeMaster2.getEmployeeMaster().getEmployeeHireDate() != null) {
								dtoQuickAssiegmentDisplay
										.setStartDate(UtilDateAndTimeHr.startDate(benefitCode1.getNoOfDays(),
												employeeMaster2.getEmployeeMaster().getEmployeeHireDate()));
								dtoQuickAssiegmentDisplay.setEndDate(UtilDateAndTimeHr.endDate(
										benefitCode1.getNoOfDays(), benefitCode1.getEndDateDays(),
										employeeMaster2.getEmployeeMaster().getEmployeeHireDate()));
							}
						}

					} else {
						dtoQuickAssiegmentDisplay.setStartDate(benefitCode1.getStartDate());
						dtoQuickAssiegmentDisplay.setEndDate(benefitCode1.getEndDate());
					}

					dtoQuickAssiegmentDisplay.setCodeId(benefitCode1.getId());
					dtoQuickAssiegmentDisplay.setCode(benefitCode1.getBenefitId());
					if (benefitCode1.getPercent() != null) {
						dtoQuickAssiegmentDisplay.setPercent(benefitCode1.getPercent());
					}
					if (benefitCode1.getMethod() != null) {
						dtoQuickAssiegmentDisplay.setMethod(benefitCode1.getMethod());
					}

					dtoQuickAssiegmentDisplay.setPayFactor(benefitCode1.getPayFactor());

					if (benefitCode1.getPayCodeList() != null && !benefitCode1.getPayCodeList().isEmpty()) {

						List<Integer> listId = new ArrayList<>();

						for (PayCode payCode : benefitCode1.getPayCodeList()) {
							for (EmployeePayCodeMaintenance employeePayCodeMaintenance1 : employeePayCodeMaintenance) {
								if (payCode.getId() == employeePayCodeMaintenance1.getPayCode().getId()) {
									listId.add(payCode.getId());
									dtoQuickAssiegmentDisplay.setPayRate(employeePayCodeMaintenance1.getPayRate());
								}
							}
						}
						List<PayCode> payCode = repositoryPayCode.findAllPayCodeListId(listId);
						if (!payCode.isEmpty()) {
							List<DtoPayCode> payCodeList = new ArrayList<>();
							for (PayCode payCode1 : payCode) {
								DtoPayCode payCode2 = new DtoPayCode();
								payCode2.setId(payCode1.getId());
								payCode2.setPayCodeId(payCode1.getPayCodeId());
								payCode2.setAmount(payCode1.getBaseOnPayCodeAmount());
								// payCode2.setPayRate(payCode1.getPayRate());
								payCode2.setPayFactor(payCode1.getPayFactor());
								for (EmployeePayCodeMaintenance employeePayCode : payCode1
										.getListEmployeePayCodeMaintenance()) {
									if (employeePayCode.getPayCode().getId().equals(payCode1.getId()) && employeePayCode
											.getEmployeeMaster()
											.getEmployeeIndexId() == dtoEmployeeQuickAssigments.getEmployeeId()) {
										payCode2.setEmployeeId(
												employeePayCode.getEmployeeMaster().getEmployeeIndexId());
										// payCode2.setPayFactor(payCode1.getPayFactor());

										payCode2.setPayRate(employeePayCode.getPayRate());
									}
								}
								payCodeList.add(payCode2);
							}
							dtoQuickAssiegmentDisplay.setDtoPayCode(payCodeList);
						}
					}

					dtoQuickAssiegmentDisplay.setDesc(benefitCode1.getDesc());
					dtoQuickAssiegmentDisplay.setArabicDesc(benefitCode1.getArbicDesc());
					dtoQuickAssiegmentDisplay.setActive(benefitCode1.isInActive());
					dtoQuickAssiegmentDisplay.setAmount(benefitCode1.getAmount());
					listDtoEmployeeQuickAssigments.add(dtoQuickAssiegmentDisplay);
				}

			}

			dtoSearch.setTotalCount(listDtoEmployeeQuickAssigments.size());
			if (dtoEmployeeQuickAssigments.getFrom() != null && dtoEmployeeQuickAssigments.getToIndex() != null
					&& dtoEmployeeQuickAssigments.getFrom() <= listDtoEmployeeQuickAssigments.size()
					&& dtoEmployeeQuickAssigments.getToIndex() <= listDtoEmployeeQuickAssigments.size()) {
				listResponse = listDtoEmployeeQuickAssigments.subList(dtoEmployeeQuickAssigments.getFrom(),
						dtoEmployeeQuickAssigments.getToIndex());
				dtoSearch.setRecords(listResponse);
			} else {
				if (dtoEmployeeQuickAssigments.getFrom() == null && dtoEmployeeQuickAssigments.getToIndex() == null) {
					dtoSearch.setRecords(listDtoEmployeeQuickAssigments);
				} else {
					if (listResponse.isEmpty()
							&& dtoEmployeeQuickAssigments.getFrom() <= listDtoEmployeeQuickAssigments.size()) {
						listDtoEmployeeQuickAssigments.subList(0, dtoEmployeeQuickAssigments.getFrom()).clear();
						dtoSearch.setRecords(listDtoEmployeeQuickAssigments);
					} else {
						dtoSearch.setRecords(listResponse);
					}
				}
			}
			// dtoSearch.setRecords(listDtoEmployeeQuickAssigments);
		}

		return dtoSearch;

	}

}
