package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.AccrualType;
import com.bti.hcm.model.dto.DtoAccrualType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAccrualType;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceAccurualType")
public class ServiceAccurualType {
	
	
	static Logger log = Logger.getLogger(ServiceAccurualType.class.getName());

	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryAccrual Autowired here using annotation of spring for access of repositoryAccrual method in Accrual service
	 * 				In short Access Accrual Query from Database using repositoryAccrual.
	 */
	@Autowired
	RepositoryAccrualType repositoryAccrual;

	public DtoAccrualType saveOrUpdate(DtoAccrualType dtoAccrual) {
		try {
			log.info("saveOrUpdate Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			AccrualType accrual = null;
			if (dtoAccrual.getId() != null && dtoAccrual.getId() > 0) {
				accrual = repositoryAccrual.findByIdAndIsDeleted(dtoAccrual.getId(), false);
				accrual.setUpdatedBy(loggedInUserId);
				accrual.setUpdatedDate(new Date());
			} else {
				accrual = new AccrualType();
				accrual.setCreatedDate(new Date());
				Integer rowId = repositoryAccrual.getCountOfTotalAccrualType();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				accrual.setRowId(increment);
			}
			
			accrual.setType(dtoAccrual.getType());
			accrual.setDesc(dtoAccrual.getDesc());
			accrual.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			repositoryAccrual.saveAndFlush(accrual);
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrual;
	}
	
	public DtoAccrualType delete(List<Integer> ids) {
		log.info("delete AccrualType Method");
		DtoAccrualType dtoAccrual = new DtoAccrualType();
		dtoAccrual.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUALTYPE_DELETED", false));
		dtoAccrual.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUEALTYPE_ASSOCIATED", false));
		List<DtoAccrualType> deleteAccrualType = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append("AccrualType already in use,you are not able to  Delete id : ");
		
		try {
			for (Integer deAccrualId : ids) {
				AccrualType accrualType = repositoryAccrual.findOne(deAccrualId);
				
				if(accrualType.getListAccrual().isEmpty() && accrualType.getListAccrualScheduleDetail().isEmpty()) {
				
				DtoAccrualType dtoAccrual2 = new DtoAccrualType();
				dtoAccrual2.setId(accrualType.getId());
				repositoryAccrual.deleteSingleAccrual(true, loggedInUserId, deAccrualId);
				deleteAccrualType.add(dtoAccrual2);
				}
				
				else{
					inValidDelete = true;
					invlidDeleteMessage.append(accrualType.getId()+","); 
				}
			}
			
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoAccrual.setMessageType(invlidDeleteMessage.toString());
				
			}
			
			
			if(!inValidDelete){
				dtoAccrual.setMessageType("AccrualType is Deleted Sucessfully");
				
			}
			
			dtoAccrual.setDelete(deleteAccrualType);
		} 
		catch (NumberFormatException e) {
			log.error(e);
		}
		
		
		log.debug("Delete Attandance :"+dtoAccrual.getId());
		return dtoAccrual;
	}

	
	
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("search Attandance Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("desc") || dtoSearch.getSortOn().equals("type")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
				
				dtoSearch.setTotalCount(this.repositoryAccrual.predictiveAccrualSearchTotalCount("%"+searchWord+"%"));
				List<AccrualType> accrualList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						accrualList = this.repositoryAccrual.predictiveAccrualSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						accrualList =  this.repositoryAccrual.predictiveAccrualSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						accrualList =  this.repositoryAccrual.predictiveAccrualSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
			
				if(accrualList != null && !accrualList.isEmpty()){
					List<DtoAccrualType> dtoAccrualList = new ArrayList<>();
					DtoAccrualType dtoAccrual=null;
					for (AccrualType accrual : accrualList) {
						dtoAccrual = new DtoAccrualType(accrual);
						dtoAccrual.setId(accrual.getId());
						dtoAccrual.setDesc(accrual.getDesc());
						dtoAccrual.setType(accrual.getType());
						dtoAccrualList.add(dtoAccrual);
					}
					dtoSearch.setRecords(dtoAccrualList);
				}
			}
			log.debug("Search Accrual Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public DtoAccrualType getByAccrualId(Integer id) {
		log.info("getByAccrualId Method");
		DtoAccrualType dtoAccrual = new DtoAccrualType();
		try {
			if (id > 0) {
				AccrualType position = repositoryAccrual.findByIdAndIsDeleted(id, false);
				if (position != null) {
					dtoAccrual = new DtoAccrualType(position);
				} else {
					dtoAccrual.setMessageType("ACCRUAL_NOT_GETTING");

				}
			} else {
				dtoAccrual.setMessageType("INVALID_DEPARTMENT_ID");

			}
			log.debug("Atteandace By Id is:"+dtoAccrual.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrual;
	}
	
	public DtoSearch getAll(DtoAccrualType dtoAccrual) {
		log.info("getAll Accrual Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoAccrual.getPageNumber());
			dtoSearch.setPageSize(dtoAccrual.getPageSize());
			dtoSearch.setTotalCount(repositoryAccrual.getCountOfTotalAccrual());
			List<AccrualType> skillAccrualList = null;
			if (dtoAccrual.getPageNumber() != null && dtoAccrual.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoAccrual.getPageNumber(), dtoAccrual.getPageSize(), Direction.DESC, "createdDate");
				skillAccrualList = repositoryAccrual.findByIsDeleted(false, pageable);
			} else {
				skillAccrualList = repositoryAccrual.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoAccrualType> dtoPOsitionList=new ArrayList<>();
			if(skillAccrualList!=null && !skillAccrualList.isEmpty())
			{
				for (AccrualType accrual : skillAccrualList) 
				{
					dtoAccrual=new DtoAccrualType(accrual);
					dtoAccrual.setId(accrual.getId());
					dtoAccrual.setDesc(accrual.getDesc());
					dtoAccrual.setType(accrual.getType());
					
					dtoPOsitionList.add(dtoAccrual);
				}
				dtoSearch.setRecords(dtoPOsitionList);
			}
			log.debug("All Accrual List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public DtoSearch getAllAccrualsTypesId(DtoSearch dtoSearch) {

		try {
			log.info("get AllAccrualsTypesId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				List<AccrualType> accrualTypeIdList = this.repositoryAccrual.getAllAccrualsTypesId("%"+searchWord+"%");
					if(accrualTypeIdList != null && !accrualTypeIdList.isEmpty()){
						List<DtoAccrualType> dtoAccrualTypeList = new ArrayList<>();
						DtoAccrualType dtoAccrualType=null;
						for (AccrualType accrualType : accrualTypeIdList) {
							
							String desc="";
							
							dtoAccrualType = new DtoAccrualType(accrualType);
							dtoAccrualType.setDesc(accrualType.getDesc());
							dtoAccrualType.setType(accrualType.getType());
							dtoAccrualType.setId(accrualType.getId());
							
							
							
							if(accrualType.getType() == 1) {
								desc = "One Time";
							}else if(accrualType.getType() == 2) {
								desc = "Hourly";  
							}else if(accrualType.getType() == 3) {
								desc = "Period";
							}else if(accrualType.getType() == 4) {
								desc = "Interval";
							}

							
							dtoAccrualType.setDesc(desc+" - "+accrualType.getDesc());
							
							dtoAccrualTypeList.add(dtoAccrualType);
						
						}
						dtoSearch.setRecords(dtoAccrualTypeList);
					}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	public DtoAccrualType repeatByAccrualDesc(String desc) {
		log.info("repeatByAccrualDesc Method");
		DtoAccrualType dtoaccrualType = new DtoAccrualType();
		try {
			List<AccrualType> listAccrual=repositoryAccrual.checkedDuplicateByDescription(desc);
			if(listAccrual!=null && !listAccrual.isEmpty()) {
				dtoaccrualType.setIsRepeat(true);
			}else {
				dtoaccrualType.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoaccrualType;
	}
	
	
	
}
