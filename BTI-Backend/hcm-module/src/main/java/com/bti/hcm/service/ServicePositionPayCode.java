package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.PositionPalnSetup;
import com.bti.hcm.model.PositionPayCode;
import com.bti.hcm.model.dto.DtoPositionPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryPositionPayCode;
import com.bti.hcm.repository.RepositoryPositionPlanSetup;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("servicePositionPayCode")
public class ServicePositionPayCode {

	/**
	 * @Description LOGGER use for put a logger in PositionPayCode Service
	 */
	static Logger log = Logger.getLogger(ServicePositionPayCode.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in PositionPayCode service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PositionPayCode service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PositionPayCode service
	 */
	@Autowired
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPositionPayCode Autowired here using annotation of spring for access of repositoryPositionPayCode method in PositionPayCode service
	 * 				In short Access PositionPayCode Query from Database using repositoryPositionPayCode.
	 */
	@Autowired
	RepositoryPositionPayCode repositoryPositionPayCode;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;
	
	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryPositionPlanSetup repositoryPositionPlanSetup;
	/**
	 * @param dtoPositionPayCode
	 * @return
	 */
	public DtoPositionPayCode saveOrUpdatePositionPalnSetup(DtoPositionPayCode dtoPositionPayCode) {
		log.info("saveOrUpdatePositionPalnSetup Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		PositionPayCode positionSetup = null;
		PositionPalnSetup palnSetup = repositoryPositionPlanSetup.findByIdAndIsDeleted(dtoPositionPayCode.getPositionplanId(), false);
		if (dtoPositionPayCode.getId() != null && dtoPositionPayCode.getId() > 0) {
			
			positionSetup = repositoryPositionPayCode.findByIdAndIsDeleted(dtoPositionPayCode.getId(), false);
			positionSetup.setUpdatedBy(loggedInUserId);
			positionSetup.setUpdatedDate(new Date());
		} else {
			positionSetup = new PositionPayCode();
			positionSetup.setCreatedDate(new Date());
		}
		BenefitCode benefitCode=null;
		if(dtoPositionPayCode.getBenefitMainId()!=null && dtoPositionPayCode.getBenefitMainId() > 0) {
			 benefitCode = repositoryBenefitCode.findOne(dtoPositionPayCode.getBenefitMainId());
			 positionSetup.setBenefitCode(benefitCode);
		}
		
		DeductionCode deductionCode= null;
		if(dtoPositionPayCode.getDeductionMainId()!=null && dtoPositionPayCode.getDeductionMainId() >0) {
			deductionCode= repositoryDeductionCode.findOne(dtoPositionPayCode.getDeductionMainId());
			positionSetup.setDeductionCode(deductionCode);
		}
		PayCode payCode = null;
		if(dtoPositionPayCode.getPayCodeMainId()!=null && dtoPositionPayCode.getPayCodeMainId()>0) {
			payCode= repositoryPayCode.findOne(dtoPositionPayCode.getPayCodeMainId());
			positionSetup.setPayCode(payCode);
		}
		
		positionSetup.setRatePay(dtoPositionPayCode.getPayRate());
		
		
		
		positionSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		positionSetup.setRowId(loggedInUserId);
		positionSetup.setPositionPalnSetup(palnSetup);
		positionSetup = repositoryPositionPayCode.saveAndFlush(positionSetup);
		log.debug("Position is:"+positionSetup.getId());
		return dtoPositionPayCode;
	
	
		
	
	
	
	}

	/**
	 * @param dtoPositionPayCode
	 * @return
	 */
	public DtoSearch getAllPostionPayCode(DtoPositionPayCode dtoPositionPayCode) {
		log.info("getAllPostionPayCode  Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoPositionPayCode.getPageNumber());
		dtoSearch.setPageSize(dtoPositionPayCode.getPageSize());
		dtoSearch.setTotalCount(repositoryPositionPayCode.getCountOfTotalPosition());
		List<PositionPayCode> skillPositionList = null;
		if (dtoPositionPayCode.getPageNumber() != null && dtoPositionPayCode.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoPositionPayCode.getPageNumber(), dtoPositionPayCode.getPageSize(), Direction.DESC, "createdDate");
			skillPositionList = repositoryPositionPayCode.findByIsDeleted(false, pageable);
		} else {
			skillPositionList = repositoryPositionPayCode.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		
		List<DtoPositionPayCode> dtoPOsitionList=new ArrayList<>();
		if(skillPositionList!=null && !skillPositionList.isEmpty())
		{
			for (PositionPayCode position : skillPositionList) 
			{
				dtoPositionPayCode=new DtoPositionPayCode(position);
				dtoPositionPayCode.setPositionplandescription(position.getPositionPalnSetup().getPositionPlanDesc());
				dtoPositionPayCode.setCodeType(position.getCodeType());
				dtoPositionPayCode.setPositionplanId(position.getPositionPalnSetup().getId());
				dtoPositionPayCode.setPositionPlan((position.getPositionPalnSetup().getPositionPlanId()));
				dtoPositionPayCode.setPayRate(position.getRatePay());
				dtoPositionPayCode.setId(position.getId());
				dtoPositionPayCode.setPositionPlanDesc(position.getPositionPalnSetup().getPositionPlanDesc());
				dtoPositionPayCode.setPositionId(position.getPositionPalnSetup().getPosition().getPositionId());
				dtoPositionPayCode.setPositiondescription(position.getPositionPalnSetup().getPosition().getDescription());
				dtoPositionPayCode.setPayCodeId(position.getPayCode().getPayCodeId());
				dtoPOsitionList.add(dtoPositionPayCode);
			}
			dtoSearch.setRecords(dtoPOsitionList);
		}
		log.debug("All getAllPostionPayCode List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	/**
	 * @param ids
	 * @return
	 */
	public DtoPositionPayCode deletePositionPayCode(List<Integer> ids) {
		log.info("deletePositionPayCode Method");
		DtoPositionPayCode dtoPosition = new DtoPositionPayCode();
		dtoPosition
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAY_CODE_DELETED", false));
		dtoPosition.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("POSTION_ASSOCIATED", false));
		List<DtoPositionPayCode> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer positionId : ids) {
				PositionPayCode positionPayCode = repositoryPositionPayCode.findOne(positionId);
				DtoPositionPayCode dtoPositionPayCode2 = new DtoPositionPayCode();
				dtoPositionPayCode2.setId(positionPayCode.getId());
				
				repositoryPositionPayCode.deleteSinglePosition(true, loggedInUserId, positionId);
				deletePosition.add(dtoPositionPayCode2);

			}
			dtoPosition.setDeletePosition(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete deletePositionPayCode Setup :"+dtoPosition.getId());
		return dtoPosition;
	}

	/**
	 * @param id
	 * @return
	 */
	public DtoPositionPayCode getByPositionPaycodeId(Integer id) {
		log.info("getByPositionPlanSetupId Method");
		DtoPositionPayCode dtoPosition = new DtoPositionPayCode();
		if (id > 0) {
			PositionPayCode position = repositoryPositionPayCode.findByIdAndIsDeleted(id, false);
			if (position != null) {
				dtoPosition = new DtoPositionPayCode(position);
			} else {
				dtoPosition.setMessageType("PAY_CODE_NOT_GETTING");

			}
		} else {
			dtoPosition.setMessageType("PAY_CODE_NOT_GETTING");

		}
		log.debug("position By Id is:"+dtoPosition.getId());
		return dtoPosition;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchPositionPayCode(DtoSearch dtoSearch) {
		log.info("searchPositionPlan Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
			
             if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				
				if(dtoSearch.getSortOn().equals("positionPlan") || dtoSearch.getSortOn().equals("positionPaycodeSeqn") || dtoSearch.getSortOn().equals("payCodeId") || dtoSearch.getSortOn().equals("codeType")
						|| dtoSearch.getSortOn().equals("deductionCode") || dtoSearch.getSortOn().equals("positionPlanDesc")  || dtoSearch.getSortOn().equals("ratePay")) {
					
					
					if(dtoSearch.getSortOn().equals("positionPlan")) {
						dtoSearch.setSortOn("positionPalnSetup");
					}
					
					if(dtoSearch.getSortOn().equals("payCodeId")) {
						dtoSearch.setSortOn("positionCode");
					}
					if(dtoSearch.getSortOn().equals("positionPlanDesc")) {
						dtoSearch.setSortOn("positionPalnSetup.positionPlanDesc");
					}
					if(dtoSearch.getSortOn().equals("codeType")) {
						dtoSearch.setSortOn("payCode");
					}
					
					condition=dtoSearch.getSortOn();
				}else {
					condition="id";
				}
				
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}	  
			
		
	
			
			
			dtoSearch.setTotalCount(this.repositoryPositionPayCode.predictivePositionPayRoleSearchTotalCount("%"+searchWord+"%"));
			List<PositionPayCode> positonList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					positonList = this.repositoryPositionPayCode.predictivePositionPayRoleSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					positonList =  this.repositoryPositionPayCode.predictivePositionPayRoleSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					positonList =  this.repositoryPositionPayCode.predictivePositionPayRoleSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			
			
			
			
			
			if(positonList != null && !positonList.isEmpty()){
				List<DtoPositionPayCode> dtoPositionList = new ArrayList<>();
				DtoPositionPayCode dtoPositionPayCode=null;
				for (PositionPayCode positionPayCode : positonList) {
					dtoPositionPayCode=new DtoPositionPayCode(positionPayCode);
					dtoPositionPayCode.setId(positionPayCode.getId());
					dtoPositionPayCode.setPositionplandescription(positionPayCode.getPositionPalnSetup().getPositionPlanDesc());
					dtoPositionPayCode.setCodeType(positionPayCode.getCodeType());
					dtoPositionPayCode.setPositionplanId(positionPayCode.getPositionPalnSetup().getId());
					dtoPositionPayCode.setPositionPlan((positionPayCode.getPositionPalnSetup().getPositionPlanId()));
					dtoPositionPayCode.setPayRate(positionPayCode.getRatePay());
					dtoPositionPayCode.setId(positionPayCode.getId());
					dtoPositionPayCode.setPositionPlanDesc(positionPayCode.getPositionPalnSetup().getPositionPlanDesc());
					dtoPositionPayCode.setPositionId(positionPayCode.getPositionPalnSetup().getPosition().getPositionId());
					dtoPositionPayCode.setPositiondescription(positionPayCode.getPositionPalnSetup().getPosition().getDescription());
					if(positionPayCode.getPayCode()!=null ) {
						dtoPositionPayCode.setPayCodeId(positionPayCode.getPayCode().getPayCodeId());
					}
					
					
					
					dtoPositionList.add(dtoPositionPayCode);
					
					
				}
				dtoSearch.setRecords(dtoPositionList);
			}
		}
		log.debug("Search PositionPayCode Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchPositionPayCodeId(DtoSearch dtoSearch) {
		log.info("searchPositionClassId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			List<String> positionPayCodeIdList = null;
			
				positionPayCodeIdList = this.repositoryPositionPayCode.predictivePositionPayCodeIdSearchWithPagination("%"+searchWord+"%");
				if(!positionPayCodeIdList.isEmpty()) {
					dtoSearch.setTotalCount(positionPayCodeIdList.size());
					dtoSearch.setRecords(positionPayCodeIdList.size());
				}
			dtoSearch.setIds(positionPayCodeIdList);
		}
		
		return dtoSearch;
	}
}
