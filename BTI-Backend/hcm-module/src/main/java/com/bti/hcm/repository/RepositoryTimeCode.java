package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TimeCode;

@Repository("repositoryTimeCode")
public interface RepositoryTimeCode extends JpaRepository<TimeCode, Integer>{
	
	TimeCode findByIdAndIsDeleted(Integer id, boolean b);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TimeCode d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleTimeCode(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId, @Param("id")Integer positionId);

	@Query("select count(*) from TimeCode d where ( d.timeCodeId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord ) and d.isDeleted=false")
	Integer predictiveTimeCodeSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select d from TimeCode d where ( d.timeCodeId like :searchKeyWord or d.desc like :searchKeyWord or d.arbicDesc like :searchKeyWord) and d.isDeleted=false")
	List<TimeCode> predictiveTimeCodeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageRequest);

	@Query("select d from TimeCode d where (d.timeCodeId=:searchKeyWord) and d.isDeleted=false")
	List<TimeCode> findByTimeCodeId(@Param("searchKeyWord") String id);

	@Query("select d.timeCodeId from TimeCode d where (d.timeCodeId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<String> predictiveTimeCodeSearchWithPagination(@Param("searchKeyWord")String string);
	
	public List<TimeCode> findByIsDeletedAndInActive(Boolean deleted, Boolean inActive);

	
	 @Query("select d from TimeCode d where (d.timeCodeId like :searchKeyWord) and d.isDeleted=false order by d.id desc")
	List<TimeCode> predictivesearchAllTimeCodeIdWithPagination(@Param("searchKeyWord")String timeCodeId);

	 @Query("select count(*) from TimeCode t ")
	public Integer getCountOfTotalTimeCode();
}
