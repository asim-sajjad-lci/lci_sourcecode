package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.TypeFieldForCodes;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTypeFieldForCodes;
import com.bti.hcm.repository.RepositoryTypeFieldForCodes;

/**
 * 
 * @author HAMID
 *
 */
@Service("/serviceTypeFieldForCodes")
public class ServiceTypeFieldCodes {

	static Logger log = Logger.getLogger(ServiceTypeFieldCodes.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryTypeFieldForCodes repositoryTypeFieldForCodes;

	public DtoSearch getIdsListByCodeTypeId(DtoSearch dtoSearch) {
		List<DtoTypeFieldForCodes> dtoTypeFieldForCodesList = new ArrayList<DtoTypeFieldForCodes>();
		//DtoSearch dtoSearch = new DtoSearch();
		if (dtoSearch.getId() == null || dtoSearch.getId() == 0)
			return null;

		List<TypeFieldForCodes> typeList = repositoryTypeFieldForCodes.findByTypeIdAndIsDeleted(dtoSearch.getId());
		
		for (TypeFieldForCodes type : typeList) {
			DtoTypeFieldForCodes dtoCode = new DtoTypeFieldForCodes();
			if (typeList != null && !typeList.isEmpty()) {
				dtoCode.setId(type.getIndxid());
				dtoCode.setDesc(type.getDesc());
				dtoTypeFieldForCodesList.add(dtoCode);
			}
		}
		Integer totalCount = repositoryTypeFieldForCodes.countByTypeIdAndIsDeleted(dtoSearch.getId());
		dtoSearch.setRecords(dtoTypeFieldForCodesList);
		dtoSearch.setTotalCount(totalCount);
		
		return dtoSearch;
	}

}
