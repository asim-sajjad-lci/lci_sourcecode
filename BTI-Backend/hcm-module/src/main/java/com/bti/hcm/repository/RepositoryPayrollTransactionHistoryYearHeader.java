package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollTransactionHistoryYearHeader;

@Repository("payrollTransactionHistoryYearHeader")
public interface RepositoryPayrollTransactionHistoryYearHeader extends JpaRepository<PayrollTransactionHistoryYearHeader, Integer>{

}
