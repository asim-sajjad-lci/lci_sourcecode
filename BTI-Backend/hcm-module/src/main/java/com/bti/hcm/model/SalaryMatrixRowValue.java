package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40924",indexes = {
        @Index(columnList = "SMTXRINDXR")
})
public class SalaryMatrixRowValue extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SMTXRINDXR")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "SMTXRINDX")
	private SalaryMatrixRow salaryMatrixRow;

	@Column(name = "SMTXRSEQNR")
	private Integer sequence;

	@Column(name = "SMTXRAMT")
	private BigDecimal amount;

	@Column(name = "SMTRRDSCR",columnDefinition = "char(31)")
	private String desc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SalaryMatrixRow getSalaryMatrixRow() {
		return salaryMatrixRow;
	}

	public void setSalaryMatrixRow(SalaryMatrixRow salaryMatrixRow) {
		this.salaryMatrixRow = salaryMatrixRow;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
