package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoEmployeeDeductionMaintenance;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceEmployeeDeductionMaintenance;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/employeeDeductionMaintenance")
public class ControllerEmployeeDeductionMaintenance extends BaseController{
	
	
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployeeDeductionMaintenance.class);
	
	@Autowired
	ServiceEmployeeDeductionMaintenance serviceEmployeeDeductionMaintenance;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;

	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance) throws Exception {
		LOGGER.info("Create EmployeeDeductionMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeDeductionMaintenance  = serviceEmployeeDeductionMaintenance.saveOrUpdate(dtoEmployeeDeductionMaintenance);
			responseMessage=displayMessage(dtoEmployeeDeductionMaintenance, "EMPLOYEE_DEDUCTION_MAINTENANCE_CREATED", "EMPLOYEE_DEDUCTION_MAINTENANCE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create EmployeeDeductionMaintenance Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance) throws Exception {
		LOGGER.info("Update EmployeeDeductionMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoEmployeeDeductionMaintenance = serviceEmployeeDeductionMaintenance.saveOrUpdate(dtoEmployeeDeductionMaintenance);
			responseMessage=displayMessage(dtoEmployeeDeductionMaintenance, "EMPLOYEE_DEDUCTION_MAINTENANCE_UPDATED", "EMPLOYEE_DEDUCTION_MAINTENANCE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update EmployeeDeductionMaintenance Method:"+responseMessage.getMessage());
		return responseMessage;
	}	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance) throws Exception {
		LOGGER.info("Delete EmployeeDeductionMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoEmployeeDeductionMaintenance.getIds() != null && !dtoEmployeeDeductionMaintenance.getIds().isEmpty()) {
				DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance2 = serviceEmployeeDeductionMaintenance.delete(dtoEmployeeDeductionMaintenance.getIds());
				if(dtoEmployeeDeductionMaintenance2.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEDUCTION_MAINTENANCE_DELETED", false), dtoEmployeeDeductionMaintenance2);	
				}else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEDUCTION_MAINTENANCE_NOT_DELETED", false), dtoEmployeeDeductionMaintenance2);
				}
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEDUCTION_MAINTENANCE_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete EmployeeDeductionMaintenance Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenanceObj = serviceEmployeeDeductionMaintenance.getById(dtoEmployeeDeductionMaintenance.getId());
			responseMessage=displayMessage(dtoEmployeeDeductionMaintenanceObj, "EMPLOYEE_DEDUCTION_MAINTENANCE_LIST_GET_DETAIL", "EMPLOYEE_DEDUCTION_MAINTENANCE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoEmployeeDeductionMaintenance.getId());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeDeductionMaintenance(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search EmployeeDeductionMaintenance Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceEmployeeDeductionMaintenance.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_DEDUCTION_MAINTENANCE_GET_ALL", "EMPLOYEE_DEDUCTION_MAINTENANCE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/checkForDel", method = RequestMethod.GET)
	public ResponseMessage checkCodeInTransactionEntry(HttpServletRequest request, Integer employeeId, Integer codeType, Integer codeId) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			boolean isEmpty = serviceEmployeeDeductionMaintenance.checkCodeInTransactionEntry(employeeId, codeType, codeId);
			responseMessage=displayMessage(isEmpty, "EMPLOYEE_DEDUCTION_MAINTENANCE_LIST_GET_DETAIL", "EMPLOYEE_DEDUCTION_MAINTENANCE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		//LOGGER.debug("Get ById Method:" + dtoEmployeeDeductionMaintenance.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getByEmployeeId", method = RequestMethod.POST)
	public ResponseMessage getByEmployeeId(HttpServletRequest request, @RequestBody DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		DtoSearch dtoSearch = null;
		if (flag) {
			dtoSearch = serviceEmployeeDeductionMaintenance.getByEmployeeId(dtoEmployeeDeductionMaintenance.getId());
			responseMessage=displayMessage(dtoSearch, "EMPLOYEE_EDUCATION_LIST_GET_DETAIL", "EMPLOYEE_EDUCATION_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoEmployeeDeductionMaintenance.getId());
		return responseMessage;
	}
	
	@PostMapping("/validate")
	public ResponseMessage validate(@RequestBody DtoEmployeeDeductionMaintenance dto, HttpServletRequest request) throws Exception {
		LOGGER.info("benefit code with employee id Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoEmployeeDeductionMaintenance dtoEmployeeDeductionMaintenance = serviceEmployeeDeductionMaintenance.validate(dto.getEmployeeMaster().getEmployeeIndexId(),dto.getDeductionCode().getId());
			responseMessage=displayMessage(dtoEmployeeDeductionMaintenance, "EMPLOYEE_DEDUCATION_MAINTENANCE_RESULT", "NOT_FOUND", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
}
