package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPositionBudget;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePositionBudget;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/hrPositionBudget")
public class ControllerPositionBudget extends BaseController{

	@Autowired
	ServicePositionBudget servicePositionBudget;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerPositionBudget.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @param request{
	  	 "budgetScheduleSeqn":1,
	 	 	"positionPalnSetup":{
	       						"id":1
	     					},
		  	"description": "test1",
		  	"budgetDesc": "test2",
		  	"budgetAmount": 45,
		  	
		  	"pageNumber": "0",
		  	"pageSize": "10"
  	
	}
	 @param request{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": null,
			"positionPalnSetup": {
			"createdDate": null,
			"updatedDate": 1517556972651,
			"updatedBy": 0,
			"isDeleted": false,
			"updatedRow": null,
			"rowId": 0,
			"id": 1,
			"position": null,
			"positionPlanId": null,
			"positionPlanDesc": null,
			"positionPlanArbicDesc": null,
			"positionPlanStart": null,
			"positionPlanEnd": null,
			"inactive": false
			},
			"budgetScheduleSeqn": 1,
			"budgetScheduleStart": null,
			"budgetScheduleEnd": null,
			"budgetDesc": "test2",
			"budgetAmount": 45,
			"pageNumber": 0,
			"pageSize": 10,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePosition": null,
			"isRepeat": null
			},
			"btiMessage": {
			"message": "Position budget created successfully",
			
			}
			}
	 
	 * @param dtoPositionBudget
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/createPositionBudget", method = RequestMethod.POST)
	public ResponseMessage createPositionBudget(HttpServletRequest request,@RequestBody DtoPositionBudget dtoPositionBudget) throws Exception{
		LOGGER.info("Create createPositionBudget Info");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionBudget = servicePositionBudget.saveOrUpdatePositionBudget(dtoPositionBudget);
			responseMessage = displayMessage(dtoPositionBudget,"POSITION_BUDGET_CREATED","POSITION_BUDGET_NOT_CREATED",response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Create createPositionBudget Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	/**
	 * @param request
	 * {
	  	"pageNumber": "0",
	  	"pageSize": "10"
  	
		}
		@param response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"pageNumber": 0,
			"pageSize": 10,
			"totalCount": 1,
			"records": [
			  {
			"id": 1,
			"positionPalnSetup": {
			"createdDate": 1517486019000,
			"updatedDate": 1517486484000,
			"updatedBy": 2,
			"isDeleted": false,
			"updatedRow": 1517466683000,
			"rowId": 2,
			"id": 1,
			"position": {
			"createdDate": 1517470967000,
			"updatedDate": 1517471719000,
			"updatedBy": 2,
			"isDeleted": true,
			"updatedRow": 1517451934000,
			"rowId": 2,
			"id": 1,
			"skillSetSetup": {
			"createdDate": 1517466495000,
			"updatedDate": 1517466502000,
			"updatedBy": 0,
			"isDeleted": false,
			"updatedRow": 1517466502000,
			"rowId": 0,
			"id": 1,
			"skillSetId": "1",
			"skillSetDiscription": "sample Skill",
			"arabicDiscription": "sample skill"
			},
			"positionId": "1",
			"description": "test3",
			"arabicDescription": "test3",
			"positionClass": {
			"createdDate": 1517470685000,
			"updatedDate": 1517470685000,
			"updatedBy": 2,
			"isDeleted": false,
			"updatedRow": 1517450884000,
			"rowId": 2,
			"id": 1,
			"positionClassId": 123,
			"description": "POSITION ",
			"arabicDescription": "بوسيتيون "
			},
			"reportToPostion": null,
			"postionLongDesc": "test3"
			},
			"positionPlanId": null,
			"positionPlanDesc": "test1",
			"positionPlanArbicDesc": "test55555555555",
			"positionPlanStart": null,
			"positionPlanEnd": null,
			"inactive": false
			},
			"budgetScheduleSeqn": null,
			"budgetScheduleStart": null,
			"budgetScheduleEnd": null,
			"budgetDesc": "test2ssssssssss",
			"budgetAmount": 45,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePosition": null,
			"isRepeat": null
			}
			],
			},
			"btiMessage": {
			"message": "position list fetched successfully",
		
			}
			}
	 * @param dtoPositionBudget
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAllPostionBudget(HttpServletRequest request, @RequestBody DtoPositionBudget dtoPositionBudget) throws Exception {
		LOGGER.info("Get All PostionBudget Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePositionBudget.getAllPostion(dtoPositionBudget);
			responseMessage = displayMessage(dtoSearch,MessageConstant.POSITION_BUDGET_GET_ALL,MessageConstant.POSITION_BUDGET_LIST_NOT_GETTING,response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get All PostionBudget Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
  	"id":1,
  	 "budgetScheduleSeqn":1,
 	 	"positionPalnSetup":{
       						"id":1
     					},
	  	"description": "test1",
	  	"budgetDesc": "test2ssssssssss",
	  	"budgetAmount": 45,
	  	
	  	"pageNumber": "0",
	  	"pageSize": "10"
  	
		}@param response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": 1,
			"positionPalnSetup": {
			"createdDate": null,
			"updatedDate": 1517557256487,
			"updatedBy": 0,
			"isDeleted": false,
			"updatedRow": null,
			"rowId": 0,
			"id": 1,
			"position": null,
			"positionPlanId": null,
			"positionPlanDesc": null,
			"positionPlanArbicDesc": null,
			"positionPlanStart": null,
			"positionPlanEnd": null,
			"inactive": false
			},
			"budgetScheduleSeqn": 1,
			"budgetScheduleStart": null,
			"budgetScheduleEnd": null,
			"budgetDesc": "test2ssssssssss",
			"budgetAmount": 45,
			"pageNumber": 0,
			"pageSize": 10,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePosition": null,
			"isRepeat": null
			},
			"btiMessage": {
			"message": "position updated successfully",
			
			}
			}
	 * @param dtoPositionBudget
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePositionBudget(HttpServletRequest request, @RequestBody DtoPositionBudget dtoPositionBudget) throws Exception {
		LOGGER.info("Update updatePositionBudget Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPositionBudget = servicePositionBudget.saveOrUpdatePositionBudget(dtoPositionBudget);
			responseMessage = displayMessage(dtoPositionBudget,"POSITION_BUDGET_UPDATED_SUCCESS","POSITION_BUDGET_NOT_UPDATED",response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update updatePositionBudget Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{
	  	"ids": [1]
	}
	@param response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": null,
			"positionPalnSetup": null,
			"budgetScheduleSeqn": null,
			"budgetScheduleStart": null,
			"budgetScheduleEnd": null,
			"budgetDesc": null,
			"budgetAmount": null,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": "Skill Steup deleted successfully",
			"associateMessage": "N/A",
			"deletePosition": [
			  {
			"id": 1,
			"positionPalnSetup": null,
			"budgetScheduleSeqn": null,
			"budgetScheduleStart": null,
			"budgetScheduleEnd": null,
			"budgetDesc": "test2ssssssssss",
			"budgetAmount": null,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"deletePosition": null,
			"isRepeat": null
			}
			],
			"isRepeat": null
			},
			"btiMessage": {
			"message": "Skill Steup deleted successfully",
			"messageShort": "POSITION_BUDGET_DELETED"
			}
			}
	 * @param dtoPositionBudget
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePositionBudget(HttpServletRequest request, @RequestBody DtoPositionBudget dtoPositionBudget) throws Exception {
		LOGGER.info("Delete deletePositionBudget Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPositionBudget.getIds() != null && !dtoPositionBudget.getIds().isEmpty()) {
				DtoPositionBudget dtoPosition1 = servicePositionBudget.deletePositionBudget(dtoPositionBudget.getIds());
				responseMessage = displayMessage(dtoPosition1,"POSITION_BUDGET_DELETED","LIST_IS_EMPTY",response);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}
			
			

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Delete deletePositionBudget Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request{ 	
  			"id" :2
  	
		}
		@param response{
		"code": 201,
		"status": "CREATED",
		"result": {
		"id": null,
		"positionPalnSetup": null,
		"budgetScheduleSeqn": null,
		"budgetScheduleStart": null,
		"budgetScheduleEnd": null,
		"budgetDesc": "test2",
		"budgetAmount": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePosition": null,
		"isRepeat": null
		},
		"btiMessage": {
		"message": "position budget details fetched successfully",
		
		}
		}
	 * @param dtoPositionBudget
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPositionBudgetId", method = RequestMethod.POST)
	public ResponseMessage getPositionBudgetId(HttpServletRequest request, @RequestBody DtoPositionBudget dtoPositionBudget) throws Exception {
		LOGGER.info("Get Position ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			DtoPositionBudget dtoPositionObj = servicePositionBudget.getByPositionBudgetId(dtoPositionBudget.getId());
			responseMessage = displayMessage(dtoPositionObj,"POSITION_BUDGET_GET_ALL","POSITION_BUDGET_LIST_NOT_GETTING",response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get Position by Id Method:"+dtoPositionBudget.getId());
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @param request{ 	
  		"searchKeyword":"t",
		  "pageNumber" : 0,
		  "pageSize" :2
  	
		} @param response{
		"code": 200,
		"status": "OK",
		"result": {
		"searchKeyword": "t",
		"pageNumber": 0,
		"pageSize": 2,
		"totalCount": 1,
		"records": [
		  {
		"id": null,
		"positionPalnSetup": null,
		"budgetScheduleSeqn": null,
		"budgetScheduleStart": null,
		"budgetScheduleEnd": null,
		"budgetDesc": "test2",
		"budgetAmount": null,
		"pageNumber": null,
		"pageSize": null,
		"ids": null,
		"isActive": null,
		"messageType": null,
		"message": null,
		"deleteMessage": null,
		"associateMessage": null,
		"deletePosition": null,
		"isRepeat": null
		}
		],
		},
		"btiMessage": {
		"message": "position budget fetched successfully",
		}
		}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPosition(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search searchPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePositionBudget.searchPositionBudget(dtoSearch);
			responseMessage = displayMessage(dtoSearch,"POSITION_BUDGET_GET_ALL","POSITION_BUDGET_LIST_NOT_GETTING",response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search searchPosition Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
}
