package com.bti.hcm.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="hr40111")
public class MiscellaneousDetails extends HcmBaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="valueindx")
	private int valueindx;
	
	@Column(name="valuedescr")
	private String valueDescription;
	
	@Column(name="valueid")
	private String valueId;
	
	@Column(name="valueardescr")
	private String valueArabicDescription;
	
	@ManyToOne
	@JoinColumn(name="miscindx")
	private MiscellaneousHeader miscellaneousHeaders;
	
	@OneToMany
	@JoinColumn(name="miscindx")
	private List<MiscellaneousEmployee> miscellaneousEmployee;


	public int getValueindx() {
		return valueindx;
	}

	public void setValueindx(int valueindx) {
		this.valueindx = valueindx;
	}

	public List<MiscellaneousEmployee> getMiscellaneousEmployee() {
		return miscellaneousEmployee;
	}

	public void setMiscellaneousEmployee(List<MiscellaneousEmployee> miscellaneousEmployee) {
		this.miscellaneousEmployee = miscellaneousEmployee;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	public String getValueId() {
		return valueId;
	}

	public void setValueId(String valueId) {
		this.valueId = valueId;
	}

	public String getValueArabicDescription() {
		return valueArabicDescription;
	}

	public void setValueArabicDescription(String valueArabicDescription) {
		this.valueArabicDescription = valueArabicDescription;
	}

	public MiscellaneousHeader getMiscellaneousHeaders() {
		return miscellaneousHeaders;
	}

	public void setMiscellaneousHeaders(MiscellaneousHeader miscellaneousHeaders) {
		this.miscellaneousHeaders = miscellaneousHeaders;
	}

	
	
	
}
