package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.Accrual;
import com.bti.hcm.model.AccrualSchedule;
import com.bti.hcm.model.AccrualScheduleDetail;
import com.bti.hcm.model.AccrualType;
import com.bti.hcm.util.UtilRandomKey;

public class DtoAccrualScheduleDetail extends DtoBase{
	private Integer id;
	private Integer accrualScheduleId;
	private AccrualSchedule accrualSchedule;
	
	private List<DtoAccrualSchedule> subItems;

	private Integer accrualSetupId;
	private Accrual accrual;

	private Integer accrualTypeId;
	
	
	private String accrualsTypeId;
	
	private AccrualType accrualType;

	private Integer scheduleSequence;
	private Integer scheduleSeniority;
	private String description;
	private Integer hours;
	
	private List<DtoAccrualScheduleDetail> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getScheduleSequence() {
		return scheduleSequence;
	}

	public void setScheduleSequence(Integer scheduleSequence) {
		this.scheduleSequence = scheduleSequence;
	}

	public Integer getScheduleSeniority() {
		return scheduleSeniority;
	}

	public void setScheduleSeniority(Integer scheduleSeniority) {
		this.scheduleSeniority = scheduleSeniority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	

	public List<DtoAccrualScheduleDetail> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoAccrualScheduleDetail> delete) {
		this.delete = delete;
	}

	
	public Integer getAccrualScheduleId() {
		return accrualScheduleId;
	}

	public void setAccrualScheduleId(Integer accrualScheduleId) {
		this.accrualScheduleId = accrualScheduleId;
	}

	public AccrualSchedule getAccrualSchedule() {
		return accrualSchedule;
	}

	public void setAccrualSchedule(AccrualSchedule accrualSchedule) {
		this.accrualSchedule = accrualSchedule;
	}

	public Integer getAccrualSetupId() {
		return accrualSetupId;
	}

	public void setAccrualSetupId(Integer accrualSetupId) {
		this.accrualSetupId = accrualSetupId;
	}

	public Accrual getAccrual() {
		return accrual;
	}

	public void setAccrual(Accrual accrual) {
		this.accrual = accrual;
	}

	public Integer getAccrualTypeId() {
		return accrualTypeId;
	}

	public void setAccrualTypeId(Integer accrualTypeId) {
		this.accrualTypeId = accrualTypeId;
	}

	public AccrualType getAccrualType() {
		return accrualType;
	}

	public void setAccrualType(AccrualType accrualType) {
		this.accrualType = accrualType;
	}

	public List<DtoAccrualSchedule> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<DtoAccrualSchedule> subItems) {
		this.subItems = subItems;
	}

	public String getAccrualsTypeId() {
		return accrualsTypeId;
	}

	public void setAccrualsTypeId(String accrualsTypeId) {
		this.accrualsTypeId = accrualsTypeId;
	}

	public DtoAccrualScheduleDetail(AccrualScheduleDetail accrualScheduleDetail) {
		this.description = accrualScheduleDetail.getDescription();

		if (UtilRandomKey.isNotBlank(accrualScheduleDetail.getDescription())) {
			this.description = accrualScheduleDetail.getDescription();
		} else {
			this.description = "";
		}

	}

	public DtoAccrualScheduleDetail() {

	}

}