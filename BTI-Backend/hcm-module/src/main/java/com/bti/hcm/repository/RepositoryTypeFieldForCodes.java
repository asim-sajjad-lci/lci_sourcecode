package com.bti.hcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TypeFieldForCodes;

@Repository("repositoryTypeFieldForCodes")
public interface RepositoryTypeFieldForCodes extends JpaRepository<TypeFieldForCodes, Integer> {


	// @Query("select t.id from TypeFieldForCodes t where t.typeFieldCodeId =:typeId
	// and t.isDeleted = false")
	// List<Integer> findIdsByTypeFieldCodeIdAndIsDeleted(@Param("typeId") Integer
	// typeId);
	
	
	@Query("select t from TypeFieldForCodes t where t.typeId =:typeId and t.isDeleted = false ")
	List<TypeFieldForCodes> findByTypeIdAndIsDeleted(@Param("typeId") Integer typeId); // typeFieldCodeId

	@Query("select count(*) from TypeFieldForCodes t where t.typeId =:typeId and t.isDeleted = false ")
	Integer countByTypeIdAndIsDeleted(@Param("typeId") Integer typeId);

	TypeFieldForCodes findByIndxidAndIsDeleted(Integer id, boolean b);  // Id --> indxid

//	@Query("select count(*) from TypeFieldForCodes t where t.isDeleted = false")
//	public Integer getCountOfTotalTypeFieldForCodes(); // unused

	
	
	
	
}

