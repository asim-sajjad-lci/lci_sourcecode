package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "GL49999_h",indexes = {
        @Index(columnList = "ACTROWID")
})
public class AccountsTableAccumulation extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACTROWID")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "ACTINDX")
	private COAMainAccounts coaMainAccounts;

//	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accountsTableAccumulation")
//	@Where(clause = "is_deleted = false")
//	private List<PayrollAccounts> listPayrollAccounts;

	@Column(name = "SGMTNUMB")
	private Integer accountSegmant;
	
	
//	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accountsTableAccumulation")
//	@Where(clause = "is_deleted = false")
//	private List<Distribution> listDistribution;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="accountsTableAccumulation")
	@Where(clause = "is_deleted = false")
	private List<ManualChecksDistribution> listanualChecksDistribution;

	@Column(name = "TRXTBLID")
	private Integer transactionType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAccountSegmant() {
		return accountSegmant;
	}

	public void setAccountSegmant(Integer accountSegmant) {
		this.accountSegmant = accountSegmant;
	}

	public Integer getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}

	public COAMainAccounts getCoaMainAccounts() {
		return coaMainAccounts;
	}

	public void setCoaMainAccounts(COAMainAccounts coaMainAccounts) {
		this.coaMainAccounts = coaMainAccounts;
	}

	public List<PayrollAccounts> getListPayrollAccounts() {
//		return listPayrollAccounts;
		return null;
	}

	public void setListPayrollAccounts(List<PayrollAccounts> listPayrollAccounts) {
//		this.listPayrollAccounts = listPayrollAccounts;
	}

	public List<Distribution> getListDistribution() {
//		return listDistribution;
		return null;
	}

	public void setListDistribution(List<Distribution> listDistribution) {
//		this.listDistribution = listDistribution;
	}

	public List<ManualChecksDistribution> getListanualChecksDistribution() {
		return listanualChecksDistribution;
	}

	public void setListanualChecksDistribution(List<ManualChecksDistribution> listanualChecksDistribution) {
		this.listanualChecksDistribution = listanualChecksDistribution;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accountSegmant == null) ? 0 : accountSegmant.hashCode());
		result = prime * result + ((coaMainAccounts == null) ? 0 : coaMainAccounts.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
//		result = prime * result + ((listPayrollAccounts == null) ? 0 : listPayrollAccounts.hashCode());
		result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

	
}
