package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.PayrollTransactionHistoryByDepartment;

@Repository("payrollTransactionHistoryByDepartment")
public interface RepositoryPayrollTransactionHistoryByDepartment extends JpaRepository<PayrollTransactionHistoryByDepartment, Integer>{

	PayrollTransactionHistoryByDepartment findByIdAndIsDeleted(Integer id, boolean b);

}
