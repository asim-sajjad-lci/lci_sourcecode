package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.TerminationSetup;

@Repository("repositoryTerminationSetup")
public interface RepositoryTerminationSetup extends JpaRepository<TerminationSetup, Integer>{

	TerminationSetup findByIdAndIsDeleted(Integer id, boolean b);

	TerminationSetup saveAndFlush(TerminationSetup terminationSetup);
	
	public List<TerminationSetup> findByIsDeleted(Boolean deleted);


	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update TerminationSetup t set t.isDeleted =:deleted ,t.updatedBy =:updateById where t.id =:id ")
	public void deleteSingleTeminationSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer terminationId);

	TerminationSetup findOne(Integer terminationId);
	

	@Query("select count(*) from TerminationSetup t where (t.terminationId LIKE :searchKeyWord or t.terminationDesc LIKE :searchKeyWord or t.terminationArbicDesc LIKE :searchKeyWord) and t.isDeleted=false")
	Integer predictiveTerminationSetupSearchTotalCount(@Param("searchKeyWord")String searchKeyWord);

	@Query("select t from TerminationSetup t where (t.terminationId LIKE :searchKeyWord or t.terminationDesc LIKE :searchKeyWord or t.terminationArbicDesc LIKE :searchKeyWord) and t.isDeleted=false")
	List<TerminationSetup> predictiveTerminationSetupSearchWithPagination(@Param("searchKeyWord")String searchKeyWord, Pageable pageable);

	@Query("select t from TerminationSetup t where (t.terminationId =:terminationId) and t.isDeleted=false")
	List<TerminationSetup> findByByTerminationId(@Param("terminationId")String terminationId);
}
