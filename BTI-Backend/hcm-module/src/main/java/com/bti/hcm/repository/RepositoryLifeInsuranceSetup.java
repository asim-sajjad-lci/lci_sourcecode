/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.LifeInsuranceSetup;

/**
 * Description: Interface for LifeInsuranceSetup 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryLifeInsuranceSetup")
public interface RepositoryLifeInsuranceSetup extends JpaRepository<LifeInsuranceSetup, Integer>{
	
	
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public LifeInsuranceSetup findByIdAndIsDeleted(int id, boolean deleted);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<LifeInsuranceSetup> findByIsDeleted(Boolean deleted);
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from LifeInsuranceSetup l where l.isDeleted=false")
	public Integer getCountOfTotalLifeInsuranceSetup();
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<LifeInsuranceSetup> findByIsDeleted(Boolean deleted, Pageable pageable);
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update LifeInsuranceSetup l set l.isDeleted =:deleted, l.updatedBy=:updateById where l.id IN (:idList)")
	public void deleteLifeInsuranceSetup(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update LifeInsuranceSetup l set l.isDeleted =:deleted ,l.updatedBy =:updateById where l.id =:id ")
	public void deleteSingleLifeInsuranceSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public LifeInsuranceSetup findTop1ByOrderByLifeInsuranceSetupIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select l from LifeInsuranceSetup l where (l.lifeInsuranceSetupId like :searchKeyWord  or l.lifeInsuraceDescription like :searchKeyWord or l.lifeInsuraceDescriptionArabic like :searchKeyWord or l.healthInsuranceGroupNumber like :searchKeyWord or l.amount like :searchKeyWord  or l.spouseAmount like :searchKeyWord or l.childAmount like :searchKeyWord or l.coverageTotalAmount like :searchKeyWord or l.employeePay like :searchKeyWord) and l.isDeleted=false")
	public List<LifeInsuranceSetup> predictiveLifeInsuranceSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);

	/***
	 * 
	 * @param deleted
	 * @return
	 */
	public List<LifeInsuranceSetup> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from LifeInsuranceSetup l where (l.lifeInsuranceSetupId like :searchKeyWord  or l.lifeInsuraceDescription like :searchKeyWord or l.lifeInsuraceDescriptionArabic like :searchKeyWord or l.healthInsuranceGroupNumber like :searchKeyWord or l.amount like :searchKeyWord  or l.spouseAmount like :searchKeyWord or l.childAmount like :searchKeyWord or l.coverageTotalAmount like :searchKeyWord or l.employeePay like :searchKeyWord) and l.isDeleted=false")
	public Integer predictiveLifeInsuranceSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select l from LifeInsuranceSetup l where (l.lifeInsuranceSetupId like :searchKeyWord  or l.lifeInsuraceDescription like :searchKeyWord or l.lifeInsuraceDescriptionArabic like :searchKeyWord or l.healthInsuranceGroupNumber like :searchKeyWord ) and l.isDeleted=false")
	public List<LifeInsuranceSetup> predictiveLifeInsuranceSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param lifeInsuraceId
	 * @return
	 */
	@Query("select l from LifeInsuranceSetup l where (l.lifeInsuranceSetupId =:lifeInsuranceSetupId) and l.isDeleted=false")
	public List<LifeInsuranceSetup> findByLifeInsuraceId(@Param("lifeInsuranceSetupId")String lifeInsuranceSetupId);


	@Query("select l from LifeInsuranceSetup l where (l.lifeInsuranceSetupId like :searchKeyWord ) and l.isDeleted=false")
	public List<LifeInsuranceSetup> searchIds(@Param("searchKeyWord") String searchKeyWord,Pageable pageRequest);
	
	@Query("select count(*) from LifeInsuranceSetup l ")
	public Integer getCountOfTotalLifeInsuranceSetups();
}
