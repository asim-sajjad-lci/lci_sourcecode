package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40902", indexes = { @Index(columnList = "DEDCINDX") })
public class DeductionCode extends HcmBaseEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DEDCINDX")
	private Integer id;

//	@ManyToOne(targetEntity=EmployeeDeductionMaintenance.class)
//	@JoinColumn(name ="DEDCAMT")
//	private BigDecimal deductionAmount;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "deductionCode")
	// @LazyCollection(LazyCollectionOption.FALSE)
	private List<EmployeeDeductionMaintenance> listEmployeeDeductionMaintenance;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "deductionCode")
	// @LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<BuildPayrollCheckByDeductions> listBuildPayrollCheckByDeductions;

	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="PYCDINDX") private PayCode payCode;
	 */

	@Column(name = "DEDCID")
	private String diductionId;

	@Column(name = "DEDCDSCR")
	private String discription;

	@Column(name = "DEDCDSCRA")
	private String arbicDiscription;

	@Column(name = "DEDCSTDT")
	private Date startDate;

	@Column(name = "DEDCEDDT")
	private Date endDate;

	@Column(name = "DEDCTRXRQ")
	private boolean transction;

	@Column(name = "DEDCMETHD")
	private Short method;

	@Column(name = "DEDCAMT", precision = 13, scale = 3)
	private BigDecimal amount;

	@Column(name = "DEDCPERT", precision = 13, scale = 3)
	private BigDecimal percent;

	@Column(name = "PYCDFACTR", precision = 15, scale = 6)
	private BigDecimal payFactor;

	@Column(name = "DEDCMXPERD", precision = 13, scale = 3)
	private BigDecimal perPeriod;

	@Column(name = "DEDCMXYERL", precision = 13, scale = 3)
	private BigDecimal perYear;

	@Column(name = "DEDCMXLIFE", precision = 13, scale = 3)
	private BigDecimal lifeTime;

	@Column(name = "DEDCFREQN")
	private Short frequency;

	@Column(name = "DEDCINCTV")
	private boolean inActive;

	@Column(name = "DEDCNOOFDAYS")
	private Integer noOfDays;

	@Column(name = "DEDCENDDAYS")
	private Integer endDateDays;

	@ManyToMany(cascade = { CascadeType.ALL })
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	@JoinTable(name = "Deducation_PayCode", joinColumns = { @JoinColumn(name = "DEDCINDX") }, inverseJoinColumns = {
			@JoinColumn(name = "PYCDINDX") })
	private List<PayCode> payCodeList;

	@Column(name = "ALLPAYCODE")
	private boolean allPaycode;

	@Column(name = "DEDCCUSTDATE")
	private boolean isCustomDate;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "deductionCode")
	private List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductions;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "deductionCode")
	private List<TransactionEntryDetail> transactionEntryDetail;

	@Column(name = "type_field") // ME dtd 04Sep
	private Integer typeField; // for new "Type" field binding

	// ME dtd 03Sep
	@Column(name = "roundOf") // ME dtd 04Sep
	private Integer roundOf; // for new "Type" field binding

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTypeField() {
		return typeField;
	}

	public void setTypeField(Integer typeField) {
		this.typeField = typeField;
	}

	public Integer getRoundOf() {
		return roundOf;
	}

	public void setRoundOf(Integer roundOf) {
		this.roundOf = roundOf;
	}

	public String getDiductionId() {
		return diductionId;
	}

	public void setDiductionId(String diductionId) {
		this.diductionId = diductionId;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public String getArbicDiscription() {
		return arbicDiscription;
	}

	public void setArbicDiscription(String arbicDiscription) {
		this.arbicDiscription = arbicDiscription;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isTransction() {
		return transction;
	}

	public void setTransction(boolean transction) {
		this.transction = transction;
	}

	public Short getMethod() {
		return method;
	}

	public void setMethod(Short method) {
		this.method = method;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	public BigDecimal getPerPeriod() {
		return perPeriod;
	}

	public void setPerPeriod(BigDecimal perPeriod) {
		this.perPeriod = perPeriod;
	}

	public BigDecimal getPerYear() {
		return perYear;
	}

	public void setPerYear(BigDecimal perYear) {
		this.perYear = perYear;
	}

	public BigDecimal getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(BigDecimal lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Short getFrequency() {
		return frequency;
	}

	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public List<EmployeeDeductionMaintenance> getListEmployeeDeductionMaintenance() {
		return listEmployeeDeductionMaintenance;
	}

	public void setListEmployeeDeductionMaintenance(
			List<EmployeeDeductionMaintenance> listEmployeeDeductionMaintenance) {
		this.listEmployeeDeductionMaintenance = listEmployeeDeductionMaintenance;
	}

	public List<BuildPayrollCheckByDeductions> getListBuildPayrollCheckByDeductions() {
		return listBuildPayrollCheckByDeductions;
	}

	public void setListBuildPayrollCheckByDeductions(
			List<BuildPayrollCheckByDeductions> listBuildPayrollCheckByDeductions) {
		this.listBuildPayrollCheckByDeductions = listBuildPayrollCheckByDeductions;
	}

	/*
	 * public PayCode getPayCode() { return payCode; }
	 * 
	 * public void setPayCode(PayCode payCode) { this.payCode = payCode; }
	 */

	/*
	 * public BigDecimal getDeductionAmount() { return deductionAmount; }
	 * 
	 * public void setDeductionAmount(BigDecimal deductionAmount) {
	 * this.deductionAmount = deductionAmount; }
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((arbicDiscription == null) ? 0 : arbicDiscription.hashCode());
		result = prime * result + ((diductionId == null) ? 0 : diductionId.hashCode());
		result = prime * result + ((discription == null) ? 0 : discription.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (inActive ? 1231 : 1237);
		result = prime * result + ((lifeTime == null) ? 0 : lifeTime.hashCode());
		result = prime * result
				+ ((listBuildPayrollCheckByDeductions == null) ? 0 : listBuildPayrollCheckByDeductions.hashCode());
		result = prime * result
				+ ((listEmployeeDeductionMaintenance == null) ? 0 : listEmployeeDeductionMaintenance.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((perPeriod == null) ? 0 : perPeriod.hashCode());
		result = prime * result + ((perYear == null) ? 0 : perYear.hashCode());
		result = prime * result + ((percent == null) ? 0 : percent.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + (transction ? 1231 : 1237);
		return result;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

	public List<PayCode> getPayCodeList() {
		return payCodeList;
	}

	public void setPayCodeList(List<PayCode> payCodeList) {
		this.payCodeList = payCodeList;
	}

	public boolean isAllPaycode() {
		return allPaycode;
	}

	public void setAllPaycode(boolean allPaycode) {
		this.allPaycode = allPaycode;
	}

	public boolean isCustomeDate() {
		return isCustomDate;
	}

	public void setCustomeDate(boolean isCustomeDate) {
		this.isCustomDate = isCustomeDate;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getEndDateDays() {
		return endDateDays;
	}

	public void setEndDateDays(Integer endDateDays) {
		this.endDateDays = endDateDays;
	}

	public boolean isCustomDate() {
		return isCustomDate;
	}

	public void setCustomDate(boolean isCustomDate) {
		this.isCustomDate = isCustomDate;
	}

	public List<BuildPayrollCheckByDeductions> getBuildPayrollCheckByDeductions() {
		return buildPayrollCheckByDeductions;
	}

	public void setBuildPayrollCheckByDeductions(List<BuildPayrollCheckByDeductions> buildPayrollCheckByDeductions) {
		this.buildPayrollCheckByDeductions = buildPayrollCheckByDeductions;
	}

	public List<TransactionEntryDetail> getTransactionEntryDetail() {
		return transactionEntryDetail;
	}

	public void setTransactionEntryDetail(List<TransactionEntryDetail> transactionEntryDetail) {
		this.transactionEntryDetail = transactionEntryDetail;
	}

}
