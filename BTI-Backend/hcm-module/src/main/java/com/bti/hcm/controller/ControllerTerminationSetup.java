package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoTerminationSetup;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceTerminationSetup;

@RestController
@RequestMapping("/terminationSetup")
public class ControllerTerminationSetup {

	private static final Logger LOGGER = Logger.getLogger(ControllerTerminationSetup.class);

	@Autowired(required = true)
	ServiceTerminationSetup serviceTerminationSetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;

	/**
	 * @param request{

		  "terminationId":"1",
		  "terminationDesc":"test",
		  "terminationArbicDesc":"test",
  "terminationDetailSequence":1,
  "terminationDetailDesc":"Detail Description",
  	"terminationDetailArbicDesc":"Detail Arabic Description",
  "terminationDetailDate":1245123145412,
  "terminationDetailPersonName":"pk",
  "orientationPredefinedCheckListItemId":1
		}
		
		
		@response{
			"code": 201,
			"status": "CREATED",
			"result": {
			"id": null,
			"orientationPredefinedCheckListItemId": 1,
			"orientationPredefinedCheckListItem": null,
			"terminationId": "1",
			"terminationDesc": "test",
			"terminationArbicDesc": "test",
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"delete": null,
			"isRepeat": null,
			"terminationDetailSequence": 1,
			"terminationDetailDesc": "Detail Description",
			"terminationDetailArbicDesc": "Detail Arabic Description",
			"terminationDetailDate": "2009-06-16",
			"terminationDetailPersonName": "pk",
			"terminationDetailId": null
			},
			"btiMessage": {
			"message": "Termination Created Successfully.",
			"messageShort": "TERMINATIONSETUP_CREATED"
			}
}
	 * @param dtoTerminationSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoTerminationSetup dtoTerminationSetup)
			throws Exception {
		LOGGER.info("Create TerminationSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTerminationSetup = serviceTerminationSetup.saveOrUpdateTerminationSetup(dtoTerminationSetup);
			if (dtoTerminationSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("TERMINATION_SETUP_CREATED", false),
						dtoTerminationSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("TERMINATION_SETUP_NOT_CREATED", false),
						dtoTerminationSetup);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Create TerminationSetup Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * @param request{
   
	  "id":1,
	 "terminationId":"1",
	  "terminationDesc":"test",
	  "terminationArbicDesc":"test"
	
	   }
	
	
	response{
	    
	{
	"code": 201,
	"status": "CREATED",
	"result": {
	"id": 1,
	"terminationId": "1",
	"terminationDesc": "test",
	"terminationArbicDesc": "test",
	"ids": null,
	"isActive": null,
	"messageType": null,
	"message": null,
	"deleteMessage": null,
	"associateMessage": null,
	"delete": null,
	"isRepeat": null
	}
	
	}
	 * @param dtoTerminationSetup
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoTerminationSetup dtoTerminationSetup)
			throws Exception {
		LOGGER.info("Update TerminationSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoTerminationSetup = serviceTerminationSetup.saveOrUpdateTerminationSetup(dtoTerminationSetup);
			if (dtoTerminationSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted("TERMINATION_SETUP_UPDATED_SUCCESS", false),
						dtoTerminationSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("TERMINATION_SETUP_NOT_UPDATED", false),
						dtoTerminationSetup);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Update TerminationSetup Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoTerminationSetup dtoTerminationSetup)
			throws Exception {
		LOGGER.info("Delete TerminationSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoTerminationSetup.getIds() != null && !dtoTerminationSetup.getIds().isEmpty()) {
				DtoTerminationSetup dtoTerminationSetup2 = serviceTerminationSetup
						.deleteTerminationSetup(dtoTerminationSetup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("TERMINATION_SETUP_DELETED", false),
						dtoTerminationSetup2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete RetirementFundSetup Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search TerminationSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceTerminationSetup.searchTerminationSetup(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.TERMINATION_SETUP_GET_DETAIL, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("TERMINATION_LIST_NOT_GETTING", false),
						dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search TerminationSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getTerminationId", method = RequestMethod.POST)
	public ResponseMessage getTerminationId(HttpServletRequest request,
			@RequestBody DtoTerminationSetup dtoTerminationSetup) throws Exception {
		LOGGER.info("Get TerminationSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTerminationSetup dtoTerminationSetupObj = serviceTerminationSetup
					.getTerminationSetupById(dtoTerminationSetup.getId());
			if (dtoTerminationSetupObj != null) {
				if (dtoTerminationSetupObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("TERMINATION_SETUP_GET_DETAIL", false),
							dtoTerminationSetupObj);
				} else {
					responseMessage = new ResponseMessage(
							HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, serviceResponse
									.getMessageByShortAndIsDeleted(dtoTerminationSetupObj.getMessageType(), false),
							dtoTerminationSetupObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("TERMINATIONSETUP_NOT_GETTING", false),
						dtoTerminationSetupObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Get TerminationSetup by Id Method:" + dtoTerminationSetup.getId());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/terminationIdcheck", method = RequestMethod.POST)
	public ResponseMessage terminationIdcheck(HttpServletRequest request, @RequestBody DtoTerminationSetup dtoTerminationSetup1) throws Exception {
		LOGGER.info("terminationIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoTerminationSetup dtoTerminationSetup = serviceTerminationSetup.repeatByTerminationId(dtoTerminationSetup1.getTerminationId());
			if (dtoTerminationSetup != null) {
				if (dtoTerminationSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("TERMINATION_SETUP_GET_DETAIL", false), dtoTerminationSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoTerminationSetup.getMessageType(), false),
							dtoTerminationSetup);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("TERMINATIONSETUP_NOT_GETTING", false), dtoTerminationSetup);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}

}
