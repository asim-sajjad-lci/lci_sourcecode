/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.OrientationCheckListSetup;
import com.bti.hcm.model.OrientationPredefinedCheckListItem;
import com.bti.hcm.model.dto.DtoOrientationCheckListSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryOrientationCheckListSetup;
import com.bti.hcm.repository.RepositoryOrientationPredefinedCheckListItem;

/**
 * Description: Service OrientationCheckListSetup
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("ServiceOrientationCheckListSetup")
public class ServiceOrientationCheckListSetup {
	
	
	/**
	 * @Description LOGGER use for put a logger in OrientationCheckListSetup Service
	 */
	static Logger log = Logger.getLogger(ServiceOrientationCheckListSetup.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in OrientationCheckListSetup service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in OrientationCheckListSetup service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in OrientationCheckListSetup service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryOrientationCheckListSetup Autowired here using annotation of spring for access of repositoryOrientationCheckListSetup method in OrientationCheckListSetup service
	 * 				In short Access OrientationCheckListSetup Query from Database using repositoryOrientationCheckListSetup.
	 */
	@Autowired
	RepositoryOrientationCheckListSetup repositoryOrientationCheckListSetup;
	
	@Autowired
	RepositoryOrientationPredefinedCheckListItem repositoryOrientationPredefinedCheckListItem;
	
	/**
	 * @Description: save and update OrientationCheckListSetup data
	 * @param dtoOrientationCheckListSetup
	 * @return
	 * @throws ParseException
	 */
	public DtoOrientationCheckListSetup saveOrUpdateOrientationCheckListSetup(DtoOrientationCheckListSetup dtoOrientationCheckListSetup) throws ParseException {
		log.info("saveOrUpdateOrientationCheckListSetup Method");
		OrientationCheckListSetup orientationCheckListSetup = null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			OrientationPredefinedCheckListItem orientationPredefinedCheckListItem= repositoryOrientationPredefinedCheckListItem.findOne(dtoOrientationCheckListSetup.getOrientationPredefinedCheckListItemId());
			if (dtoOrientationCheckListSetup.getId() != null && dtoOrientationCheckListSetup.getId()>0) {
				orientationCheckListSetup = repositoryOrientationCheckListSetup.findByIdAndIsDeleted(dtoOrientationCheckListSetup.getId(), false);
				orientationCheckListSetup.setUpdatedBy(loggedInUserId);
				orientationCheckListSetup.setUpdatedDate(new Date());
			} else {
				orientationCheckListSetup = new OrientationCheckListSetup();
				orientationCheckListSetup.setCreatedDate(new Date());
				Integer rowId = repositoryOrientationCheckListSetup.getCountOfTotaOrientationCheckListSetup();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				orientationCheckListSetup.setRowId(increment);
			}
			orientationCheckListSetup.setOrientationChecklistItemDescription(dtoOrientationCheckListSetup.getOrientationChecklistItemDescription());
			orientationCheckListSetup.setOrientationChecklistItemDescriptionArabic(dtoOrientationCheckListSetup.getOrientationChecklistItemDescriptionArabic());
			orientationCheckListSetup.setOrientationPredefinedCheckListItem(orientationPredefinedCheckListItem);
			repositoryOrientationCheckListSetup.saveAndFlush(orientationCheckListSetup);
			log.debug("OrientationCheckListSetup is:"+dtoOrientationCheckListSetup.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoOrientationCheckListSetup;

	}
	/**
	 * @Description: get All OrientationCheckListSetup
	 * @param dtoOrientationCheckListSetup
	 * @return
	 */
	public DtoSearch getAllOrientationCheckListSetup(DtoOrientationCheckListSetup dtoOrientationCheckListSetup) {
		log.info("getAllOrientationCheckListSetup Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoOrientationCheckListSetup.getPageNumber());
			dtoSearch.setPageSize(dtoOrientationCheckListSetup.getPageSize());
			dtoSearch.setTotalCount(repositoryOrientationCheckListSetup.getCountOfTotalOrientationCheckListSetup());
			List<OrientationCheckListSetup> orientationCheckListSetupList = null;
			if (dtoOrientationCheckListSetup.getPageNumber() != null && dtoOrientationCheckListSetup.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoOrientationCheckListSetup.getPageNumber(), dtoOrientationCheckListSetup.getPageSize(), Direction.DESC, "createdDate");
				orientationCheckListSetupList = repositoryOrientationCheckListSetup.findByIsDeleted(false, pageable);
			} else {
				orientationCheckListSetupList = repositoryOrientationCheckListSetup.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoOrientationCheckListSetup> dtoOrientationCheckListSetupList=new ArrayList<>();
			if(orientationCheckListSetupList!=null && orientationCheckListSetupList.isEmpty())
			{
				for (OrientationCheckListSetup orientationCheckListSetup : orientationCheckListSetupList) 
				{
					dtoOrientationCheckListSetup=new DtoOrientationCheckListSetup(orientationCheckListSetup);
					dtoOrientationCheckListSetup.setOrientationChecklistItemDescription(orientationCheckListSetup.getOrientationChecklistItemDescription());
					dtoOrientationCheckListSetup.setOrientationChecklistItemDescriptionArabic(orientationCheckListSetup.getOrientationChecklistItemDescriptionArabic());
					dtoOrientationCheckListSetup.setId(orientationCheckListSetup.getId());
					dtoOrientationCheckListSetupList.add(dtoOrientationCheckListSetup);
				}
				dtoSearch.setRecords(dtoOrientationCheckListSetup);
			}
			log.debug("All OrientationCheckListSetup List Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	/**
	 * @Description: Search OrientationCheckListSetup data
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchOrientationCheckListSetup(DtoSearch dtoSearch) {
		try {
			log.info("searchOrientationCheckListSetup Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";

				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "orientationChecklistItemDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "orientationPredefinedCheckListItem" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
					
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
				
			}
				dtoSearch.setTotalCount(this.repositoryOrientationCheckListSetup.predictiveOrientationCheckListSetupSearchTotalCount("%"+searchWord+"%"));
				List<OrientationCheckListSetup> orientationCheckListSetupList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){

					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						orientationCheckListSetupList = this.repositoryOrientationCheckListSetup.predictiveOrientationCheckListSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						orientationCheckListSetupList = this.repositoryOrientationCheckListSetup.predictiveOrientationCheckListSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						orientationCheckListSetupList = this.repositoryOrientationCheckListSetup.predictiveOrientationCheckListSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				
				}
			
				if(orientationCheckListSetupList != null && !orientationCheckListSetupList.isEmpty()){
					List<DtoOrientationCheckListSetup> dtoOrientationCheckListSetupList = new ArrayList<>();
					DtoOrientationCheckListSetup dtoOrientationCheckListSetup=null;
					for (OrientationCheckListSetup orientationCheckListSetup : orientationCheckListSetupList) {
						dtoOrientationCheckListSetup=new DtoOrientationCheckListSetup(orientationCheckListSetup);
						dtoOrientationCheckListSetup.setOrientationChecklistItemDescription(orientationCheckListSetup.getOrientationChecklistItemDescription());
						dtoOrientationCheckListSetup.setOrientationChecklistItemDescriptionArabic(orientationCheckListSetup.getOrientationChecklistItemDescriptionArabic());
						dtoOrientationCheckListSetup.setId(orientationCheckListSetup.getId());
						dtoOrientationCheckListSetupList.add(dtoOrientationCheckListSetup);
					}
					dtoSearch.setRecords(dtoOrientationCheckListSetupList);
				}
			}
			log.debug("Search OrientationCheckListSetup Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	/**
	 * @Description: get OrientationCheckListSetup data by id
	 * @param id
	 * @return
	 */
	public DtoOrientationCheckListSetup getDtoOrientationCheckListSetupById(int id) {
		log.info("getDtoOrientationCheckListSetupById Method");
		DtoOrientationCheckListSetup dtoOrientationCheckListSetup = new DtoOrientationCheckListSetup();
		try {
			if (id > 0) {
				OrientationCheckListSetup orientationCheckListSetup = repositoryOrientationCheckListSetup.findByIdAndIsDeleted(id, false);
				if (orientationCheckListSetup != null) {
					dtoOrientationCheckListSetup = new DtoOrientationCheckListSetup(orientationCheckListSetup);
				} else {
					dtoOrientationCheckListSetup.setMessageType("ORIENTATION_CHECKLIST_SETUP_NOT_GETTING");

				}
			} else {
				dtoOrientationCheckListSetup.setMessageType("INVALID_ORIENTATION_CHECKLIST_SETUP_ID");

			}
			log.debug("OrientationCheckListSetup By Id is:"+dtoOrientationCheckListSetup.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoOrientationCheckListSetup;
	}
	
	/**
	 * @Description: delete OrientationCheckListSetup
	 * @param ids
	 * @return
	 */
	
	public DtoOrientationCheckListSetup deleteOrientationCheckListSetup(List<Integer> ids) {
		log.info("deleteOrientationCheckListSetup Method");
		DtoOrientationCheckListSetup dtoOrientationCheckListSetup = new DtoOrientationCheckListSetup();
		dtoOrientationCheckListSetup
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ORIENTATION_CHECKLIST_SETUP_DELETED_USE", false));
		dtoOrientationCheckListSetup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("ORIENTATION_CHECKLIST_SETUP_ASSOCIATED", false));
		List<DtoOrientationCheckListSetup> deleteOrientationCheckListSetupList = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		List<DtoOrientationCheckListSetup> actualList = new ArrayList<>();
		try {
			for (Integer id : ids) {
				OrientationCheckListSetup orientationCheckListSetup = repositoryOrientationCheckListSetup.findOne(id);
				if(!orientationCheckListSetup.getOrientationPredefinedCheckListItem().getListOrientationCheckListSetup().isEmpty() && orientationCheckListSetup.getOrientationPredefinedCheckListItem().getListOrientationCheckListSetup()!=null) {
					for(OrientationCheckListSetup checkListSetup :orientationCheckListSetup.getOrientationPredefinedCheckListItem().getListOrientationCheckListSetup()) {
						if(checkListSetup.getIsDeleted()==false) {
							if(id!=checkListSetup.getId()) {
								DtoOrientationCheckListSetup dtoOrientationCheckListSetup3 = new DtoOrientationCheckListSetup();
								dtoOrientationCheckListSetup3.setItemDesc(checkListSetup.getOrientationChecklistItemDescription());
								dtoOrientationCheckListSetup3.setItemDescArabic(checkListSetup.getOrientationChecklistItemDescriptionArabic());
								dtoOrientationCheckListSetup3.setId(checkListSetup.getId());
								actualList.add(dtoOrientationCheckListSetup3);
							}
						}
					}
				}
				DtoOrientationCheckListSetup dtoOrientationCheckListSetup2 = new DtoOrientationCheckListSetup();
				dtoOrientationCheckListSetup2.setOrientationChecklistItemDescription(orientationCheckListSetup.getOrientationChecklistItemDescription());
				dtoOrientationCheckListSetup2.setOrientationChecklistItemDescriptionArabic(orientationCheckListSetup.getOrientationChecklistItemDescriptionArabic());
				dtoOrientationCheckListSetup2.setId(orientationCheckListSetup.getId());
				dtoOrientationCheckListSetup2.setList(actualList);
				repositoryOrientationCheckListSetup.deleteSingleOrientationCheckListSetup(true, loggedInUserId, id);
				
 				deleteOrientationCheckListSetupList.add(dtoOrientationCheckListSetup2);
			}
			dtoOrientationCheckListSetup.setDeleteDtoOrientationCheckListSetup(deleteOrientationCheckListSetupList);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete OrientationCheckListSetup :"+dtoOrientationCheckListSetup.getId());
		return dtoOrientationCheckListSetup;
	}
	
	/**
	 * @Description: check Repeat id
	 * @param id
	 * @return
	 */
	public DtoOrientationCheckListSetup repeatById(int id) {
		log.info("repeatBId Method");
		DtoOrientationCheckListSetup dtoOrientationCheckListSetup = new DtoOrientationCheckListSetup();
		try {
			List<OrientationCheckListSetup> orientationCheckListSetupList=repositoryOrientationCheckListSetup.findById(id);
			if(orientationCheckListSetupList!=null && !orientationCheckListSetupList.isEmpty()) {
				dtoOrientationCheckListSetup.setIsRepeat(true);
			}else {
				dtoOrientationCheckListSetup.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoOrientationCheckListSetup;
	}
	
	
	public List<DtoOrientationCheckListSetup> getOrientaionChekListSetupById(Integer id){
		
	
	List<DtoOrientationCheckListSetup> dtoOrientationCheckListSetupList = new ArrayList<>();
	List<OrientationCheckListSetup> orientationCheckListSetup = repositoryOrientationCheckListSetup.findByOrientationCheckListSetup(id);
	if(orientationCheckListSetup!=null && !orientationCheckListSetup.isEmpty()) {
		DtoOrientationCheckListSetup dtoOrientationCheckListSetup=null;
		for(OrientationCheckListSetup orientationCheckListSetup1 : orientationCheckListSetup) { 
			if(orientationCheckListSetup1.getIsDeleted()==false) {
				
				dtoOrientationCheckListSetup=new DtoOrientationCheckListSetup(orientationCheckListSetup1);
				
				dtoOrientationCheckListSetup.setItemDesc(orientationCheckListSetup1.getOrientationChecklistItemDescription());
				dtoOrientationCheckListSetup.setItemDescArabic(orientationCheckListSetup1.getOrientationChecklistItemDescriptionArabic());
				dtoOrientationCheckListSetupList.add(dtoOrientationCheckListSetup);
			}
			
		}
	}
	return dtoOrientationCheckListSetupList;
	
	
}
	

}
