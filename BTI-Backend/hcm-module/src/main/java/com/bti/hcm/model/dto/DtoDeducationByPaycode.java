package com.bti.hcm.model.dto;

import java.util.List;

public class DtoDeducationByPaycode extends DtoBase{

	private Integer employeeId;
	private Integer deductionId;
	List<DtoDeductionCode> dtoDeductionCode;
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public List<DtoDeductionCode> getDtoDeductionCode() {
		return dtoDeductionCode;
	}
	public void setDtoDeductionCode(List<DtoDeductionCode> dtoDeductionCode) {
		this.dtoDeductionCode = dtoDeductionCode;
	}
	public Integer getDeductionId() {
		return deductionId;
	}
	public void setDeductionId(Integer deductionId) {
		this.deductionId = deductionId;
	}
	
	
}
