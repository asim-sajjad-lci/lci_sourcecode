package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoBuildChecks;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByBatches;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceBenefitCode;
import com.bti.hcm.service.ServiceBuildPayrollCheckByBatches;
import com.bti.hcm.service.ServiceBuildPayrollCheckByBenefits;
import com.bti.hcm.service.ServiceBuildPayrollCheckByPayCodes;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/buildPayrollCheckByBatches")
public class ControllerBuildPayrollCheckByBatches extends BaseController{
	

	/**
	 * @Description LOGGER use for put a logger in BenefitCode Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerBuildPayrollCheckByBatches.class);

	/**
	 * @Description serviceBenefitCode Autowired here using annotation of spring for
	 *              use of serviceBenefitCode method in this controller
	 */
	@Autowired(required = true)
	ServiceBenefitCode serviceBenefitCode;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              use of serviceResponse method in this controller
	 */
	@Autowired(required = true)
	ServiceResponse serviceResponse;

	@Autowired(required = true)
	ServiceHcmHome serviceHcmHome;

	@Autowired
	ServiceBuildPayrollCheckByPayCodes serviceBuildPayrollCheckByPayCodes;
	
	@Autowired(required = true)
	ServiceBuildPayrollCheckByBenefits serviceBuildPayrollCheckByBenefits;
	
	@Autowired(required = true)
	ServiceBuildPayrollCheckByBatches serviceBuildPayrollCheckByBatches;
	
	
	/**
	 * @description Create BeneditCode
	 * @param request
	 * @param dtoBenefitCode
	 * @return
	 * @throws Exception
	 */


	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoBuildPayrollCheckByBatches  dtoBuildPayrollCheckByBatches) throws Exception {
		LOGGER.info("Create BuildPayrollCheckByBatches Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBuildPayrollCheckByBatches  = serviceBuildPayrollCheckByBatches.saveOrUpdateFinalVersion(dtoBuildPayrollCheckByBatches);
			responseMessage=displayMessage(dtoBuildPayrollCheckByBatches, "BUILD_CHECK_BATCHES_CREATED", "BUILD_CHECK_BATCHES_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create BuildPayrollCheckByBatches Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoBuildPayrollCheckByBatches dtoBuildPayrollCheckByBatches, HttpServletRequest request) throws Exception {
		LOGGER.info("Search BuildPayrollCheckByBatches Method");
		DtoSearch dtoSearch = null;
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBuildPayrollCheckByBatches.getAllByCodeType(dtoBuildPayrollCheckByBatches);
			responseMessage=displayMessage(dtoSearch, "BUILD_CHECK_BATCHES_GET_ALL", "BUILD_CHECK_BATCHES_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllForFromDateToDate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllForFromDateToDate(@RequestBody DtoBuildChecks dtoBuildChecks, HttpServletRequest request) throws Exception {
		LOGGER.info("Search BuildPayrollCheckByBatches Method");
		DtoSearch dtoSearch = null;
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBuildPayrollCheckByBatches.getAllForFromDateToDate(dtoBuildChecks);
			responseMessage=displayMessage(dtoSearch, "BUILD_CHECK_BATCHES_GET_ALL", "BUILD_CHECK_BATCHES_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
}
