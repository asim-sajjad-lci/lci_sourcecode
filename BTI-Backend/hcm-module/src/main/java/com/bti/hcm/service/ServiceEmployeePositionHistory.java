package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Department;
import com.bti.hcm.model.Division;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeePositionHistory;
import com.bti.hcm.model.EmployeePositionReason;
import com.bti.hcm.model.Location;
import com.bti.hcm.model.Position;
import com.bti.hcm.model.Supervisor;
import com.bti.hcm.model.dto.DtoDepartment;
import com.bti.hcm.model.dto.DtoDivision;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeePositionHistory;
import com.bti.hcm.model.dto.DtoEmployeePositionReason;
import com.bti.hcm.model.dto.DtoLocation;
import com.bti.hcm.model.dto.DtoPosition;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSupervisor;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryDivision;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePositionHistory;
import com.bti.hcm.repository.RepositoryEmployeePositionReason;
import com.bti.hcm.repository.RepositoryLocation;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositorySupervisor;

@Service("serviceEmployeePositionHistory")
public class ServiceEmployeePositionHistory {

	static Logger log = Logger.getLogger(ServiceEmployeePositionHistory.class.getName());

	

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired(required=false)
	RepositoryEmployeePositionHistory repositoryEmployeePositionHistory;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired(required=false)
	RepositoryDivision repositoryDivision;
	
	@Autowired(required=false)
	RepositoryDepartment repositoryDepartment;
	
	@Autowired(required=false)
	RepositoryPosition repositoryPosition;
	
	@Autowired(required=false)
	RepositoryLocation repositoryLocation;

	@Autowired(required=false)
	RepositorySupervisor repositorySupervisor;
	
	@Autowired(required=false)
	RepositoryEmployeePositionReason repositoryEmployeePositionReason;
	
	
	
	public DtoEmployeePositionHistory saveOrUpdate(DtoEmployeePositionHistory dtoEmployeePositionHistory) {
		
		try {

			log.info("saveOrUpdate EmployeePositionHistory Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			EmployeePositionHistory employeePositionHistory = null;
			if (dtoEmployeePositionHistory.getId() != null && dtoEmployeePositionHistory.getId() > 0) {
				
				employeePositionHistory = repositoryEmployeePositionHistory.findByIdAndIsDeleted(dtoEmployeePositionHistory.getId(), false);
				employeePositionHistory.setUpdatedBy(loggedInUserId);
				employeePositionHistory.setUpdatedDate(new Date());
			} else {
				employeePositionHistory = new EmployeePositionHistory();
				employeePositionHistory.setCreatedDate(new Date());
				Integer rowId = repositoryEmployeePositionHistory.getCountOfTotalEmployeePositionHistory();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				employeePositionHistory.setRowId(increment);
			}
			
			EmployeeMaster employeeMaster =null;
			Division division = null;
			Department department =null;
			Position position = null;
			Location location = null;
			Supervisor supervisor = null;
			EmployeePositionReason employeePositionReason = null;
			
			if(dtoEmployeePositionHistory.getEmployeeMaster()!=null && dtoEmployeePositionHistory.getEmployeeMaster().getEmployeeIndexId()>0){
				employeeMaster = repositoryEmployeeMaster.findOne(dtoEmployeePositionHistory.getEmployeeMaster().getEmployeeIndexId());
			
			}
			
			if(dtoEmployeePositionHistory.getDivision()!=null && dtoEmployeePositionHistory.getDivision().getId()!=null && dtoEmployeePositionHistory.getDivision().getId()>0){
				division = repositoryDivision.findOne(dtoEmployeePositionHistory.getDivision().getId());
			
			}
			
			if(dtoEmployeePositionHistory.getDepartment()!=null && dtoEmployeePositionHistory.getDepartment().getId()!=null && dtoEmployeePositionHistory.getDepartment().getId()>0){
				department = repositoryDepartment.findOne(dtoEmployeePositionHistory.getDepartment().getId());
			
			}
			
			if(dtoEmployeePositionHistory.getPosition()!=null && dtoEmployeePositionHistory.getPosition().getId()!=null && dtoEmployeePositionHistory.getPosition().getId()>0){
				position = repositoryPosition.findOne(dtoEmployeePositionHistory.getPosition().getId());
			
			}
			
			if(dtoEmployeePositionHistory.getLocation()!=null && dtoEmployeePositionHistory.getLocation().getId()!=null && dtoEmployeePositionHistory.getLocation().getId()>0){
				location = repositoryLocation.findOne(dtoEmployeePositionHistory.getLocation().getId());
			
			}
			
			
			if(dtoEmployeePositionHistory.getDtoEmployeePositionReason()!=null && dtoEmployeePositionHistory.getDtoEmployeePositionReason().getId()!=null && dtoEmployeePositionHistory.getDtoEmployeePositionReason().getId()>0){
				employeePositionReason = repositoryEmployeePositionReason.findOne(dtoEmployeePositionHistory.getDtoEmployeePositionReason().getId());
			
			}
			
			if(dtoEmployeePositionHistory.getSupervisor()!=null && dtoEmployeePositionHistory.getSupervisor().getId()!=null && dtoEmployeePositionHistory.getSupervisor().getId()>0){
				supervisor = repositorySupervisor.findOne(dtoEmployeePositionHistory.getSupervisor().getId());
			
			}
			employeePositionHistory.setEmployeeType(dtoEmployeePositionHistory.getEmployeeType());
			employeePositionHistory.setEmployeeMaster(employeeMaster);
			employeePositionHistory.setPositionEffectiveDate(dtoEmployeePositionHistory.getPositionEffectiveDate());
			employeePositionHistory.setDivision(division);
			employeePositionHistory.setDepartment(department);
			employeePositionHistory.setPosition(position);
			employeePositionHistory.setLocation(location);
			employeePositionHistory.setSupervisor(supervisor);
			employeePositionHistory.setEmployeePositionReason(employeePositionReason);
			employeePositionHistory = repositoryEmployeePositionHistory.saveAndFlush(employeePositionHistory);
			
			log.debug("Time EmployeePositionHistory is:"+employeePositionHistory.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoEmployeePositionHistory;
	
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAll(DtoSearch dtoSearch) {
		try {

			log.info("search EmployeePositionHistory Method");
			String searchWord=dtoSearch.getSearchKeyword();
			if(dtoSearch != null){
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("id")
							) {
						condition = dtoSearch.getSortOn();
					
				}else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
				dtoSearch.setTotalCount(this.repositoryEmployeePositionHistory.predictiveEmployeePositionHistorySearchTotalCount("%"+searchWord+"%"));
				List<EmployeePositionHistory> employeePositionHistoryList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeePositionHistoryList = this.repositoryEmployeePositionHistory.predictiveEmployeePositionHistorySearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeePositionHistoryList = this.repositoryEmployeePositionHistory.predictiveEmployeePositionHistorySearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeePositionHistoryList = this.repositoryEmployeePositionHistory.predictiveEmployeePositionHistorySearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
				
				if(employeePositionHistoryList != null && !employeePositionHistoryList.isEmpty()){
					List<DtoEmployeePositionHistory> dtoEmployeePositionHistoryList = new ArrayList<>();
					DtoEmployeePositionHistory dtoEmployeePositionHistory=null;
					for (EmployeePositionHistory employeePositionHistory : employeePositionHistoryList) {
						dtoEmployeePositionHistory = new DtoEmployeePositionHistory(employeePositionHistory);
						
						dtoEmployeePositionHistory.setPositionEffectiveDate(employeePositionHistory.getPositionEffectiveDate());
						
						if(employeePositionHistory.getPosition()!=null) {
							DtoPosition dtoPosition = new DtoPosition();
							dtoPosition.setId(employeePositionHistory.getPosition().getId());
							dtoPosition.setPositionId(employeePositionHistory.getPosition().getPositionId());
							dtoPosition.setDescription(employeePositionHistory.getPosition().getDescription());
							dtoPosition.setArabicDescription(employeePositionHistory.getPosition().getArabicDescription());
							dtoEmployeePositionHistory.setPosition(dtoPosition);
						}
						
						if(employeePositionHistory.getEmployeeMaster()!=null) {
							DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
							dtoEmployeeMaster.setEmployeeIndexId(employeePositionHistory.getEmployeeMaster().getEmployeeIndexId());
							dtoEmployeeMaster.setEmployeeId(employeePositionHistory.getEmployeeMaster().getEmployeeId());
							dtoEmployeeMaster.setEmployeeFirstName(employeePositionHistory.getEmployeeMaster().getEmployeeFirstName());
							dtoEmployeeMaster.setEmployeeLastName(employeePositionHistory.getEmployeeMaster().getEmployeeLastName());
							dtoEmployeePositionHistory.setEmployeeMaster(dtoEmployeeMaster);
						}
						
						if(employeePositionHistory.getDivision()!=null) {
							DtoDivision dtoDivision = new DtoDivision();
							dtoDivision.setId(employeePositionHistory.getDivision().getId());
							dtoDivision.setDivisionId(employeePositionHistory.getDivision().getDivisionId());
							dtoDivision.setDivisionDescription(employeePositionHistory.getDivision().getDivisionDescription());
							dtoEmployeePositionHistory.setDivision(dtoDivision);
						}
						
						if(employeePositionHistory.getDepartment()!=null) {
							DtoDepartment dtoDepartment = new DtoDepartment();
							dtoDepartment.setId(employeePositionHistory.getDepartment().getId());
							dtoDepartment.setDepartmentId(employeePositionHistory.getDepartment().getDepartmentId());
							dtoDepartment.setDepartmentDescription(employeePositionHistory.getDepartment().getDepartmentDescription());
							dtoEmployeePositionHistory.setDepartment(dtoDepartment);
						}
						
						if(employeePositionHistory.getLocation()!=null) {
							DtoLocation dtoLocation = new DtoLocation();
							dtoLocation.setId(employeePositionHistory.getLocation().getId());
							dtoLocation.setLocationId(employeePositionHistory.getLocation().getLocationId());
							dtoLocation.setDescription(employeePositionHistory.getLocation().getDescription());
							dtoEmployeePositionHistory.setLocation(dtoLocation);
						}
						
						if(employeePositionHistory.getSupervisor()!=null) {
							DtoSupervisor dtoSupervisor = new DtoSupervisor();
							dtoSupervisor.setId(employeePositionHistory.getSupervisor().getId());
							dtoSupervisor.setSuperVisionCode(employeePositionHistory.getSupervisor().getSuperVisionCode());
							dtoSupervisor.setDescription(employeePositionHistory.getSupervisor().getDescription());
							dtoEmployeePositionHistory.setSupervisor(dtoSupervisor);
						}
						
						if(employeePositionHistory.getEmployeePositionReason()!=null) {
							DtoEmployeePositionReason dtoEmployeePositionReason = new DtoEmployeePositionReason();
							dtoEmployeePositionReason.setId(employeePositionHistory.getEmployeePositionReason().getId());
							dtoEmployeePositionReason.setPositionReason(employeePositionHistory.getEmployeePositionReason().getPositionReason());
							dtoEmployeePositionHistory.setDtoEmployeePositionReason(dtoEmployeePositionReason);
						}
						dtoEmployeePositionHistory.setPositionEffectiveDate(employeePositionHistory.getPositionEffectiveDate());
						dtoEmployeePositionHistory.setId(employeePositionHistory.getId());
						dtoEmployeePositionHistory.setEmployeeType(employeePositionHistory.getEmployeeType());		
						dtoEmployeePositionHistoryList.add(dtoEmployeePositionHistory);
					}
					dtoSearch.setRecords(dtoEmployeePositionHistoryList);
				}
			}
			log.debug("Search EmployeePositionHistory Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	
	public DtoEmployeePositionHistory delete(List<Integer> ids) {
		log.info("delete EmployeePositionHistory Method");
		DtoEmployeePositionHistory dtoEmployeePositionHistory = new DtoEmployeePositionHistory();
		dtoEmployeePositionHistory
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_DELETED", false));
		dtoEmployeePositionHistory.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_ASSOCIATED", false));
		List<DtoEmployeePositionHistory> delete = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer positionHistoryId : ids) {
				EmployeePositionHistory employeePositionHistory = repositoryEmployeePositionHistory.findOne(positionHistoryId);
				if(employeePositionHistory!=null) {
					DtoEmployeePositionHistory dtoEmployeePositionHistory1 = new DtoEmployeePositionHistory();
					dtoEmployeePositionHistory1.setId(employeePositionHistory.getId());
					repositoryEmployeePositionHistory.deleteSingleEmployeePositionHistory(true, loggedInUserId, positionHistoryId);
					delete.add(dtoEmployeePositionHistory1);
		
				}
				else {
					inValidDelete = true;
				}
			}
			if(inValidDelete){
				dtoEmployeePositionHistory.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoEmployeePositionHistory.setMessageType("");
				
			}
			
			dtoEmployeePositionHistory.setDelete(delete);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeePositionHistory :"+dtoEmployeePositionHistory.getId());
		return dtoEmployeePositionHistory;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch findById(DtoSearch dtoSearch) {
		
		try {

			log.info("search EmployeePositionHistory Method");
			
			EmployeePositionHistory employeePositionHistory = repositoryEmployeePositionHistory.findByIdAndIsDeleted(dtoSearch.getId(), false);
				
				
					DtoEmployeePositionHistory	dtoEmployeePositionHistory = new DtoEmployeePositionHistory();
						
						dtoEmployeePositionHistory.setPositionEffectiveDate(employeePositionHistory.getPositionEffectiveDate());
						
						if(employeePositionHistory.getPosition()!=null) {
							DtoPosition dtoPosition = new DtoPosition();
							dtoPosition.setId(employeePositionHistory.getPosition().getId());
							dtoPosition.setPositionId(employeePositionHistory.getPosition().getPositionId());
							
							dtoEmployeePositionHistory.setPosition(dtoPosition);
						}
						
						if(employeePositionHistory.getDivision()!=null) {
							DtoDivision dtoDivision = new DtoDivision();
							dtoDivision.setId(employeePositionHistory.getPosition().getId());
							dtoDivision.setDivisionId(employeePositionHistory.getDivision().getDivisionId());
							
							dtoEmployeePositionHistory.setDivision(dtoDivision);
						}
						if(employeePositionHistory.getEmployeeMaster()!=null) {
							DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
							dtoEmployeeMaster.setEmployeeIndexId(employeePositionHistory.getEmployeeMaster().getEmployeeIndexId());
							dtoEmployeeMaster.setEmployeeId(employeePositionHistory.getEmployeeMaster().getEmployeeId());
							dtoEmployeeMaster.setEmployeeFirstName(employeePositionHistory.getEmployeeMaster().getEmployeeFirstName());
							dtoEmployeeMaster.setEmployeeLastName(employeePositionHistory.getEmployeeMaster().getEmployeeLastName());
							dtoEmployeePositionHistory.setEmployeeMaster(dtoEmployeeMaster);
						}
						
						if(employeePositionHistory.getDepartment()!=null) {
							DtoDepartment dtoDepartment = new DtoDepartment();
							dtoDepartment.setId(employeePositionHistory.getDepartment().getId());
							dtoDepartment.setDepartmentId(employeePositionHistory.getDepartment().getDepartmentId());
							
							dtoEmployeePositionHistory.setDepartment(dtoDepartment);
						}
						
						if(employeePositionHistory.getLocation()!=null) {
							DtoLocation dtoLocation = new DtoLocation();
							dtoLocation.setId(employeePositionHistory.getLocation().getId());
							dtoLocation.setLocationId(employeePositionHistory.getLocation().getLocationId());
							
							dtoEmployeePositionHistory.setLocation(dtoLocation);
						}
						
						if(employeePositionHistory.getSupervisor()!=null) {
							DtoSupervisor dtoSupervisor = new DtoSupervisor();
							dtoSupervisor.setId(employeePositionHistory.getSupervisor().getId());
							dtoSupervisor.setSuperVisionCode(employeePositionHistory.getSupervisor().getSuperVisionCode());
							
							dtoEmployeePositionHistory.setSupervisor(dtoSupervisor);
						}
						
						if(employeePositionHistory.getEmployeePositionReason()!=null) {
							DtoEmployeePositionReason dtoEmployeePositionReason = new DtoEmployeePositionReason();
							dtoEmployeePositionReason.setId(employeePositionHistory.getEmployeePositionReason().getId());
							dtoEmployeePositionReason.setPositionReason(employeePositionHistory.getEmployeePositionReason().getPositionReason());
							dtoEmployeePositionHistory.setDtoEmployeePositionReason(dtoEmployeePositionReason);
						}
						dtoEmployeePositionHistory.setEmployeeType(employeePositionHistory.getEmployeeType());		
						dtoEmployeePositionHistory.setId(employeePositionHistory.getId());
						dtoEmployeePositionHistory.setPositionEffectiveDate(employeePositionHistory.getPositionEffectiveDate());
					dtoSearch.setRecords(dtoEmployeePositionHistory);
				
			
			log.debug("Search EmployeePositionHistory Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
}
