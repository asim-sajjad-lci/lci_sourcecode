package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00202",indexes = {
        @Index(columnList = "EMPDINDX")
})
public class EmployeeDeductionMaintenance extends HcmBaseEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPDINDX")
	private Integer id;
	
	
	/*@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeDependent employeeDependent;
	
*/

	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private EmployeeMaster employeeMaster;
	
	
	@ManyToOne
	@JoinColumn(name = "DEDCINDX")
	@Where(clause = "DEDCINCTV = false and is_deleted = false")
	private DeductionCode deductionCode;

	@Column(name = "DEDCSTDT")
	private Date startDate;

	@Column(name = "DEDCEDDT")
	private Date endDate;

	@Column(name = "DEDCTRXRQ")
	private boolean transactionRequired;

	@Column(name = "DEDCMETHD")
	private Short benefitMethod;

	@Column(name = "DEDCAMT", precision = 10, scale = 3)
	private BigDecimal deductionAmount;

	@Column(name = "DEDCPERT", precision = 10, scale = 5)
	private BigDecimal deductionPercent;

	@Column(name = "DEDCMXPERD", precision = 10, scale = 3)
	private BigDecimal perPeriord;

	@Column(name = "DEDCMXYERL", precision = 10, scale = 3)
	private BigDecimal perYear;

	@Column(name = "DEDCMXLIFE", precision = 10, scale = 3)
	private BigDecimal lifeTime;

	@Column(name = "DEDCFREQN")
	private Short frequency;

	@Column(name = "DEDCINCTV")
	private Boolean inactive;
	
	@Column(name = "PYCDFACTR", precision = 15, scale = 6)
	private BigDecimal payFactor;

	@Column(name="DEDCNOOFDAYS")
	private Integer noOfDays;
	
	@Column(name="DEDCENDDAYS")
	private Integer endDateDays;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isTransactionRequired() {
		return transactionRequired;
	}

	public Short getBenefitMethod() {
		return benefitMethod;
	}

	public void setBenefitMethod(Short benefitMethod) {
		this.benefitMethod = benefitMethod;
	}

	public BigDecimal getDeductionAmount() {
		return deductionAmount;
	}

	public void setDeductionAmount(BigDecimal deductionAmount) {
		this.deductionAmount = deductionAmount;
	}

	public BigDecimal getDeductionPercent() {
		return deductionPercent;
	}

	public void setDeductionPercent(BigDecimal deductionPercent) {
		this.deductionPercent = deductionPercent;
	}

	public BigDecimal getPerPeriord() {
		return perPeriord;
	}

	public void setPerPeriord(BigDecimal perPeriord) {
		this.perPeriord = perPeriord;
	}

	public BigDecimal getPerYear() {
		return perYear;
	}

	public void setPerYear(BigDecimal perYear) {
		this.perYear = perYear;
	}

	public BigDecimal getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(BigDecimal lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Short getFrequency() {
		return frequency;
	}

	public void setFrequency(Short frequency) {
		this.frequency = frequency;
	}

	public Boolean getInactive() {
		return inactive;
	}

	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public DeductionCode getDeductionCode() {
		return deductionCode;
	}

	public void setDeductionCode(DeductionCode deductionCode) {
		this.deductionCode = deductionCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((benefitMethod == null) ? 0 : benefitMethod.hashCode());
		result = prime * result + ((deductionAmount == null) ? 0 : deductionAmount.hashCode());
		result = prime * result + ((deductionCode == null) ? 0 : deductionCode.hashCode());
		result = prime * result + ((deductionPercent == null) ? 0 : deductionPercent.hashCode());
		result = prime * result + ((employeeMaster == null) ? 0 : employeeMaster.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((inactive == null) ? 0 : inactive.hashCode());
		result = prime * result + ((lifeTime == null) ? 0 : lifeTime.hashCode());
		result = prime * result + ((perPeriord == null) ? 0 : perPeriord.hashCode());
		result = prime * result + ((perYear == null) ? 0 : perYear.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

	public void setTransactionRequired(boolean transactionRequired) {
		this.transactionRequired = transactionRequired;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getEndDateDays() {
		return endDateDays;
	}

	public void setEndDateDays(Integer endDateDays) {
		this.endDateDays = endDateDays;
	}
	

}
