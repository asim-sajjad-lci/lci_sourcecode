package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoDefaulCheck extends DtoBase{
	
	 Integer defaultId;
	 List<Integer> deductionId;
	 List<Integer> benefitId;
	 List<Integer> paycodeId;
	 Date startDate;
	 Date endDate;
	public Integer getDefaultId() {
		return defaultId;
	}
	public void setDefaultId(Integer defaultId) {
		this.defaultId = defaultId;
	}
	public List<Integer> getDeductionId() {
		return deductionId;
	}
	public void setDeductionId(List<Integer> deductionId) {
		this.deductionId = deductionId;
	}
	public List<Integer> getBenefitId() {
		return benefitId;
	}
	public void setBenefitId(List<Integer> benefitId) {
		this.benefitId = benefitId;
	}
	public List<Integer> getPaycodeId() {
		return paycodeId;
	}
	public void setPaycodeId(List<Integer> paycodeId) {
		this.paycodeId = paycodeId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
