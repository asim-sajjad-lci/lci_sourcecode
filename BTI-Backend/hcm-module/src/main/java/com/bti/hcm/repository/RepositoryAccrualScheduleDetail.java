package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.AccrualScheduleDetail;
@Repository("repositoryAccrualScheduleDetail")
public interface RepositoryAccrualScheduleDetail extends JpaRepository<AccrualScheduleDetail, Integer> {

	@Query("select count(*) from AccrualScheduleDetail b where b.isDeleted=false")
	public Integer getCountOfTotalAccrualScheduleDetail();

	public List<AccrualScheduleDetail> findByIsDeleted(boolean b, Pageable pageable);

	public List<AccrualScheduleDetail> findByIsDeletedOrderByCreatedDateDesc(boolean b);

	public AccrualScheduleDetail findByIdAndIsDeleted(Integer id, boolean b);
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update AccrualScheduleDetail a set a.isDeleted =:deleted ,a.updatedBy =:updateById where a.id =:id ")
	public void deleteSingleAccrualScheduleDetail(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,@Param("id") Integer id);

	public AccrualScheduleDetail findById(Integer id);

	@Query("select count(*) from AccrualScheduleDetail a where (a.description LIKE :searchKeyWord or a.scheduleSeniority LIKE :searchKeyWord) and a.isDeleted=false")
	public Integer predictiveAccrualSheduleDetailSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select a from AccrualScheduleDetail a where (a.description LIKE :searchKeyWord or a.scheduleSeniority LIKE :searchKeyWord) and a.isDeleted=false")
	public List<AccrualScheduleDetail> predictiveAccrualSheduleDetailSearchWithPagination(@Param("searchKeyWord")String searchKeyWord, Pageable pageable);

	@Query("select count(*) from AccrualScheduleDetail a ")
	public Integer getCountOfTotalAccruals();

}
