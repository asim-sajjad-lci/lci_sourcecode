package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.Position;
import com.bti.hcm.model.PositionCourseDetail;
import com.bti.hcm.model.PositionCourseToLink;
import com.bti.hcm.model.TraningCourse;

public class DtoPositionCourseToLink extends DtoBase {

	private Integer id;
	private Integer sequence;
	private String positionId;
	private Integer traningCourseId;
	private String traningCourseIdName;
	private String traningClassName;
	private String courseDesc;
	private String positionTrainingDesc;
	private String positionTrainingArbicDesc;
	private Integer traningClassId;
	private String traningClassIdName;
	private TraningCourse traningCourse;
	private Position position;
	private String className;
	private Integer classId;
	private List<PositionCourseDetail> listPositionCourseDetail;
	private List<DtoPositionCourseToLink> delete;

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

	public Integer getTraningCourseId() {
		return traningCourseId;
	}

	public void setTraningCourseId(Integer traningCourseId) {
		this.traningCourseId = traningCourseId;
	}

	public TraningCourse getTraningCourse() {
		return traningCourse;
	}

	public void setTraningCourse(TraningCourse traningCourse) {
		this.traningCourse = traningCourse;
	}

	public List<DtoPositionCourseToLink> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoPositionCourseToLink> delete) {
		this.delete = delete;
	}

	public List<PositionCourseDetail> getListPositionCourseDetail() {
		return listPositionCourseDetail;
	}

	public void setListPositionCourseDetail(List<PositionCourseDetail> listPositionCourseDetail) {
		this.listPositionCourseDetail = listPositionCourseDetail;
	}

	public Integer getTraningClassId() {
		return traningClassId;
	}

	public void setTraningClassId(Integer traningClassId) {
		this.traningClassId = traningClassId;
	}

	public DtoPositionCourseToLink() {
	}

	public String getCourseDesc() {
		return courseDesc;
	}

	public void setCourseDesc(String courseDesc) {
		this.courseDesc = courseDesc;
	}

	public String getPositionTrainingDesc() {
		return positionTrainingDesc;
	}

	public void setPositionTrainingDesc(String positionTrainingDesc) {
		this.positionTrainingDesc = positionTrainingDesc;
	}

	public String getPositionTrainingArbicDesc() {
		return positionTrainingArbicDesc;
	}

	public void setPositionTrainingArbicDesc(String positionTrainingArbicDesc) {
		this.positionTrainingArbicDesc = positionTrainingArbicDesc;
	}

	public DtoPositionCourseToLink(PositionCourseToLink positionCourseToLink) {

		this.traningCourseId = positionCourseToLink.getTraningCourseId();
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public Integer getClassId() {
		return classId;
	}

	public String getTraningClassIdName() {
		return traningClassIdName;
	}

	public void setTraningClassIdName(String traningClassIdName) {
		this.traningClassIdName = traningClassIdName;
	}

	public String getTraningCourseIdName() {
		return traningCourseIdName;
	}

	public void setTraningCourseIdName(String traningCourseIdName) {
		this.traningCourseIdName = traningCourseIdName;
	}

	public String getTraningClassName() {
		return traningClassName;
	}

	public void setTraningClassName(String traningClassName) {
		this.traningClassName = traningClassName;
	}

}
