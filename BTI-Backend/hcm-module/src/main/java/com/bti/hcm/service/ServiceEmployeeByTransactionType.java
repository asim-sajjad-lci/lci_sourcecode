package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.EmployeeBenefitMaintenance;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.EmployeePayCodeMaintenance;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearchActivity;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;

/*
 * @author Safwan
 * 
 * */

@Service("serviceEmployeeByTransactionType")
public class ServiceEmployeeByTransactionType {
	static Logger log = Logger.getLogger(ServiceEmployeeByTransactionType.class.getName());

	@Autowired(required = false)
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;

	@Autowired
	RepositoryEmployeePayCodeMaintenance repositoryEmployeePayCodeMaintenance;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;

	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;

	public DtoSearchActivity findEmployeeByTransactionId(DtoSearchActivity dtoSearchActivity) {
		log.info("find all codes by Transaction Id Method");
		List<DtoPayCode> dtoPaycode = new ArrayList<>();

		try {
			if (dtoSearchActivity.getType().shortValue() == 1) {
				List<PayCode> payCodes = repositoryPayCode.findAll();
				for (PayCode payCode : payCodes) {
					DtoPayCode code = new DtoPayCode();
					if (!payCode.isInActive()) {
						code.setId(payCode.getId());
						code.setPayCodeId(payCode.getPayCodeId());
						code.setPayCodeTypeDesc(payCode.getDescription());
						code.setPayRate(payCode.getPayRate());
					}
					dtoPaycode.add(code);
				}
			}

			if (dtoSearchActivity.getType().shortValue() == 2) {

				List<DeductionCode> deductionCodes = repositoryDeductionCode.findAll();
				for (DeductionCode deductionCode : deductionCodes) {
					DtoPayCode code = new DtoPayCode();

					if ((deductionCode != null && !deductionCode.isInActive()
							&& deductionCode.isTransction() == true)) {
						code.setId(deductionCode.getId());
						code.setPayCodeId(deductionCode.getDiductionId());
						code.setPayCodeTypeDesc(deductionCode.getDiscription());
						code.setAmount(deductionCode.getAmount());
						dtoPaycode.add(code);

					} else {
						log.info("code inactive or not in range");
					}
				}
			}

			if (dtoSearchActivity.getType().shortValue() == 3) {

				List<BenefitCode> benefitCodes = repositoryBenefitCode.findAll();
				for (BenefitCode benefitCode : benefitCodes) {
					DtoPayCode code = new DtoPayCode();

					if ((benefitCode != null && !benefitCode.isInActive() && benefitCode.isTransction() == true)) {

						code.setId(benefitCode.getId());
						code.setPayCodeId(benefitCode.getBenefitId());
						code.setPayCodeTypeDesc(benefitCode.getDesc());
						code.setAmount(benefitCode.getAmount());

						dtoPaycode.add(code);

					} else {
						log.info("code inactive or not in range");

					}
				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		dtoSearchActivity.setRecords(dtoPaycode);
		return dtoSearchActivity;
	}

	public DtoSearchActivity findEmployeeByPayCodeId(DtoSearchActivity dtoSearchActivity) {
		log.info("find by payCodeId Method");
		List<DtoPayCode> dtoPaycode = new ArrayList<>();
		try {
			PayCode payCode = repositoryPayCode.findByIdPayCodeId(dtoSearchActivity.getId());
			List<EmployeePayCodeMaintenance> data = payCode.getListEmployeePayCodeMaintenance();

			if (data != null) {
				for (EmployeePayCodeMaintenance employeePayCodeMaintenance : data) {
					DtoPayCode code = new DtoPayCode();

					if (!payCode.isInActive() && !employeePayCodeMaintenance.getEmployeeMaster().getEmployeeInactive()
							&& employeePayCodeMaintenance.getBaseOnPayCodeId().equals(payCode.getBaseOnPayCodeId())) {

						code.setId(payCode.getId());
						code.setPayCodeId(payCode.getPayCodeId());
						code.setPayCodeTypeDesc(payCode.getDescription());
						code.setAmount(payCode.getBaseOnPayCodeAmount());
						code.setEmployeeFirstName(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeFirstName()
								+ " " + employeePayCodeMaintenance.getEmployeeMaster().getEmployeeLastName());
						code.setEmployeeIndexId(
								Integer.valueOf(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeIndexId()));
						code.setEmployeeIdInString(employeePayCodeMaintenance.getEmployeeMaster().getEmployeeId());

						dtoPaycode.add(code);
					}

				}

			}

		} catch (Exception e) {

			log.error(e);
		}

		dtoSearchActivity.setRecords(dtoPaycode);

		return dtoSearchActivity;
	}

	public DtoSearchActivity findEmployeeByDeductionCodeId(DtoSearchActivity dtoSearchActivity) {
		log.info("find by deductionCodeId Method");
		List<DtoPayCode> dtoPaycode = new ArrayList<>();

		try {

			List<EmployeeDeductionMaintenance> employeeByDeductionCode = repositoryEmployeeDeductionMaintenance
					.findByStartDateAfterAndEndDateBeforeAndDeductionId(dtoSearchActivity.getId(),
							setTime(dtoSearchActivity.getStartDate()), setTime(dtoSearchActivity.getEndDate()));

			if (employeeByDeductionCode != null) {
				for (EmployeeDeductionMaintenance employeeDeductionMaintenance : employeeByDeductionCode) {
					DtoPayCode code = new DtoPayCode();
					if (employeeDeductionMaintenance.getDeductionCode() != null
							&& !employeeDeductionMaintenance.getDeductionCode().isInActive()
							&& !employeeDeductionMaintenance.getEmployeeMaster().getEmployeeInactive()
							&& employeeDeductionMaintenance.getDeductionCode().getId().equals(dtoSearchActivity.getId())
							&& employeeDeductionMaintenance.getIsDeleted() == false) {
						code.setId(employeeDeductionMaintenance.getDeductionCode().getId());
						code.setPayCodeId(employeeDeductionMaintenance.getDeductionCode().getDiductionId());
						code.setAmount(employeeDeductionMaintenance.getDeductionAmount());
						code.setEmployeeFirstName(
								employeeDeductionMaintenance.getEmployeeMaster().getEmployeeFirstName() + " "
										+ employeeDeductionMaintenance.getEmployeeMaster().getEmployeeLastName());
						code.setEmployeeIndexId(
								Integer.valueOf(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeIndexId()));
						code.setEmployeeIdInString(employeeDeductionMaintenance.getEmployeeMaster().getEmployeeId());

						dtoPaycode.add(code);

					} else {
						log.info("Either deduction code or employee inactive or records not in date range.");
					}

				}
			}

		} catch (Exception e) {

			log.error(e);
		}

		dtoSearchActivity.setRecords(dtoPaycode);

		return dtoSearchActivity;
	}

	public DtoSearchActivity findEmployeeByBenefitCodeId(DtoSearchActivity dtoSearchActivity) {
		log.info("find by benefitCodeId Method");
		List<DtoPayCode> dtoPaycode = new ArrayList<>();
		try {

			List<EmployeeBenefitMaintenance> employeeByBenefitCode = repositoryEmployeeBenefitMaintenance
					.findByStartDateAfterAndEndDateBeforeAndBenefitId(dtoSearchActivity.getId(),
							setTime(dtoSearchActivity.getStartDate()), setTime(dtoSearchActivity.getEndDate()));

			if (employeeByBenefitCode != null) {
				for (EmployeeBenefitMaintenance employeeBenefitMaintenance : employeeByBenefitCode) {
					DtoPayCode code = new DtoPayCode();
					if (employeeByBenefitCode != null
							&& !employeeBenefitMaintenance.getEmployeeMaster().isEmployeeInactive()
							&& !employeeBenefitMaintenance.getEmployeeMaster().getEmployeeInactive()
							&& employeeBenefitMaintenance.getBenefitCode().getId().equals(dtoSearchActivity.getId())
							&& employeeBenefitMaintenance.getIsDeleted() == false) {
						code.setId(employeeBenefitMaintenance.getBenefitCode().getId());
						code.setPayCodeId(employeeBenefitMaintenance.getBenefitCode().getBenefitId());
						code.setAmount(employeeBenefitMaintenance.getBenefitAmount());

						code.setEmployeeFirstName(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeFirstName()
								+ " " + employeeBenefitMaintenance.getEmployeeMaster().getEmployeeLastName());
						code.setEmployeeIndexId(
								Integer.valueOf(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeIndexId()));
						code.setEmployeeIdInString(employeeBenefitMaintenance.getEmployeeMaster().getEmployeeId());

						dtoPaycode.add(code);

					} else {
						log.info("Either benefit code or employee inactive or records not in date range.");
					}

				}
			}

		} catch (Exception e) {

			log.error(e);
		}

		dtoSearchActivity.setRecords(dtoPaycode);

		return dtoSearchActivity;
	}

	public Date setTime(Date date) {
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);

		return date;
	}

}