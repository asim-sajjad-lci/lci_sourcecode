package com.bti.hcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.WorkFlowAssign;


@Repository("repositoryWorkFlowAssign")
public interface RepositoryWorkFlowAssign  extends JpaRepository<WorkFlowAssign, Integer>{

	WorkFlowAssign findByIdAndIsDeleted(Integer id, boolean b);

}
