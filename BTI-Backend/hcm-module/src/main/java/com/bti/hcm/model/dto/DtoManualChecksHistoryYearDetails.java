package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DtoManualChecksHistoryYearDetails {

	private Integer id;
	private Integer year;
	private Integer paymentSequence;
	private Short paymentCodeType;
	private Integer codeId;
	private Date fromPeriodDate;
	private Date toPeriodDate;
	private BigDecimal totalAmount;
	private Integer totalHours;
	private Integer daysWorked;
	private Integer weeksWorked;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPaymentSequence() {
		return paymentSequence;
	}

	public void setPaymentSequence(Integer paymentSequence) {
		this.paymentSequence = paymentSequence;
	}

	public Short getPaymentCodeType() {
		return paymentCodeType;
	}

	public void setPaymentCodeType(Short paymentCodeType) {
		this.paymentCodeType = paymentCodeType;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public Date getFromPeriodDate() {
		return fromPeriodDate;
	}

	public void setFromPeriodDate(Date fromPeriodDate) {
		this.fromPeriodDate = fromPeriodDate;
	}

	public Date getToPeriodDate() {
		return toPeriodDate;
	}

	public void setToPeriodDate(Date toPeriodDate) {
		this.toPeriodDate = toPeriodDate;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public Integer getDaysWorked() {
		return daysWorked;
	}

	public void setDaysWorked(Integer daysWorked) {
		this.daysWorked = daysWorked;
	}

	public Integer getWeeksWorked() {
		return weeksWorked;
	}

	public void setWeeksWorked(Integer weeksWorked) {
		this.weeksWorked = weeksWorked;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

}
