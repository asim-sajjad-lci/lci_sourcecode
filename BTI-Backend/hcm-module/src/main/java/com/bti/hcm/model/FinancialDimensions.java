package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "GL00102_h",indexes = {
        @Index(columnList = "DIMINXD")
})
public class FinancialDimensions extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DIMINXD")
	private Integer id;

	@Column(name = "DIMNAME", columnDefinition = "char(31)")
	private String dimensionDescription;

	@Column(name = "DIMNAMEA", columnDefinition = "char(61)")
	private String dimensionArbicDescription;

	@Column(name = "DIMCOLNAME", columnDefinition = "char(10)")
	private String dimensionColumnName;

	@Column(name = "DIMMASK", columnDefinition = "char(10)")
	private String dimensionMask;
	
//	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "financialDimensions")
//	@Where(clause = "is_deleted = false")
//	private List<FinancialDimensionsValues> listFinancialDimensionsValues;
	
	
//	@OneToMany(cascade = CascadeType.ALL,mappedBy = "financialDimensions", fetch = FetchType.LAZY)
//	@Where(clause = "is_deleted = false")
//	private List<PayrollAccounts> listPayrollAccounts;

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDimensionDescription() {
		return dimensionDescription;
	}

	public void setDimensionDescription(String dimensionDescription) {
		this.dimensionDescription = dimensionDescription;
	}

	public String getDimensionArbicDescription() {
		return dimensionArbicDescription;
	}

	public void setDimensionArbicDescription(String dimensionArbicDescription) {
		this.dimensionArbicDescription = dimensionArbicDescription;
	}

	public String getDimensionColumnName() {
		return dimensionColumnName;
	}

	public void setDimensionColumnName(String dimensionColumnName) {
		this.dimensionColumnName = dimensionColumnName;
	}

	public List<FinancialDimensionsValues> getListFinancialDimensionsValues() {
//		return listFinancialDimensionsValues;
		return null;
	}

	public void setListFinancialDimensionsValues(List<FinancialDimensionsValues> listFinancialDimensionsValues) {
//		this.listFinancialDimensionsValues = listFinancialDimensionsValues;
	}

	public String getDimensionMask() {
		return dimensionMask;
	}

	public void setDimensionMask(String dimensionMask) {
		this.dimensionMask = dimensionMask;
	}
	
}
