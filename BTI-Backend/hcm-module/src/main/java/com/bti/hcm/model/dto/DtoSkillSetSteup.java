package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.SkillSetSetup;

public class DtoSkillSetSteup extends DtoBase{
	
	private Integer id;
	private Integer skillId;
	private String skillsIds;
	private int sequence;
	private String skillSetId;
	private Boolean skillRequire;
	private String skillSetDiscription;
	private List<DtoSkillSetDesc> listDtoSkillSetDesc;
	private String arabicDiscription;
	private String skillIdParam;
	private List<DtoSkillSetSteup> deleteSkillSet;
	private int skillSetSeqn;
	private boolean  skillRequired;
	private String  comment;
	private int skillSetDescId;
	private int skillSetIndexId;
	
	private List<DtoSkillSteup> listSkillSteup;
	
	private List<DtoSkillSteup> skillIds;
	
	 
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSkillSetId() {
		return skillSetId;
	}

	public void setSkillSetId(String skillSetId) {
		this.skillSetId = skillSetId;
	}

	public String getSkillSetDiscription() {
		return skillSetDiscription;
	}

	public void setSkillSetDiscription(String skillSetDiscription) {
		this.skillSetDiscription = skillSetDiscription;
	}

	public String getArabicDiscription() {
		return arabicDiscription;
	}

	public void setArabicDiscription(String arabicDiscription) {
		this.arabicDiscription = arabicDiscription;
	}

	
	public List<DtoSkillSetSteup> getDeleteSkillSet() {
		return deleteSkillSet;
	}

	public void setDeleteSkillSet(List<DtoSkillSetSteup> deleteSkillSet) {
		this.deleteSkillSet = deleteSkillSet;
	}

	public Integer getSkillId() {
		return skillId;
	}

	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}


	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	
	public DtoSkillSetSteup() {
	
	}

	public Boolean getSkillRequire() {
		return skillRequire;
	}

	public void setSkillRequire(Boolean skillRequire) {
		this.skillRequire = skillRequire;
	}
	
	
	
	public int getSkillSetSeqn() {
		return skillSetSeqn;
	}

	public void setSkillSetSeqn(int skillSetSeqn) {
		this.skillSetSeqn = skillSetSeqn;
	}

	public boolean isSkillRequired() {
		return skillRequired;
	}
	public void setSkillRequired(boolean skillRequired) {
		this.skillRequired = skillRequired;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

	public int getSkillSetDescId() {
		return skillSetDescId;
	}

	public void setSkillSetDescId(int skillSetDescId) {
		this.skillSetDescId = skillSetDescId;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getSkillSetIndexId() {
		return skillSetIndexId;
	}
	public void setSkillSetIndexId(int skillSetIndexId) {
		this.skillSetIndexId = skillSetIndexId;
	}
	
	
	public List<DtoSkillSetDesc> getListDtoSkillSetDesc() {
		return listDtoSkillSetDesc;
	}

	public void setListDtoSkillSetDesc(List<DtoSkillSetDesc> listDtoSkillSetDesc) {
		this.listDtoSkillSetDesc = listDtoSkillSetDesc;
	}

	public String getSkillIdParam() {
		return skillIdParam;
	}

	public void setSkillIdParam(String skillIdParam) {
		this.skillIdParam = skillIdParam;
	}

	
	
	public List<DtoSkillSteup> getSkillIds() {
		return skillIds;
	}

	public void setSkillIds(List<DtoSkillSteup> skillIds) {
		this.skillIds = skillIds;
	}

	public DtoSkillSetSteup(SkillSetSetup skillSetSetup) {
		this.skillSetId = skillSetSetup.getSkillSetId();
		
	}

	public List<DtoSkillSteup> getListSkillSteup() {
		return listSkillSteup;
	}

	public void setListSkillSteup(List<DtoSkillSteup> listSkillSteup) {
		this.listSkillSteup = listSkillSteup;
	}

	public String getSkillsIds() {
		return skillsIds;
	}

	public void setSkillsIds(String skillsIds) {
		this.skillsIds = skillsIds;
	}
	
	
	

}
