package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.PositionCourseDetail;
import com.bti.hcm.model.PositionCourseToLink;
import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.model.dto.DtoPositionCourseDetail;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPositionCourseDetail;
import com.bti.hcm.repository.RepositoryPositionCourseToLink;
import com.bti.hcm.repository.RepositoryTraningCourse;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("servicePositionCourseDetail")
public class ServicePositionCourseDetail {

	
	/**
	 * @Description LOGGER use for put a logger in PositionCourseDetail Service
	 */
	static Logger log = Logger.getLogger(ServicePositionCourseDetail.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in PositionCourseDetail service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PositionCourseDetail service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PositionCourseDetail service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired(required =  false)
	RepositoryPositionCourseDetail repositoryPositionCourseDetail;
	
	@Autowired(required =  false)
	RepositoryPositionCourseToLink repositoryPositionCourseToLink;
	
	@Autowired(required =  false)
	RepositoryTraningCourse repositoryTraningCourse;

	public DtoPositionCourseDetail saveOrUpdateCourseDetail(DtoPositionCourseDetail dtoPositionCourseDetail) {
		log.info("saveOrUpdatePosition Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		PositionCourseDetail position = null;
		if (dtoPositionCourseDetail.getId() != null && dtoPositionCourseDetail.getId() > 0) {
			
			position = repositoryPositionCourseDetail.findByIdAndIsDeleted(dtoPositionCourseDetail.getId(), false);
			position.setUpdatedBy(loggedInUserId);
			position.setUpdatedDate(new Date());
		} else {
			position = new PositionCourseDetail();
			position.setCreatedDate(new Date());
			
			Integer rowId = repositoryPositionCourseDetail.getCountOfTotaPositionCourseDetail();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			position.setRowId(increment);
		}
		PositionCourseToLink  positionCourseToLink=  repositoryPositionCourseToLink.findOne(dtoPositionCourseDetail.getPositionCourseId());
		TraningCourse  traningCourse=  repositoryTraningCourse.findOne(dtoPositionCourseDetail.getTraningCourseId());
		
		
		position.setPositionCourse(positionCourseToLink);
		position.setTraningCourse(traningCourse);
		position.setSequence(dtoPositionCourseDetail.getSequence());
		position.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositoryPositionCourseDetail.saveAndFlush(position);
		log.debug("Position is:"+position.getId());
		return dtoPositionCourseDetail;
	}

	public DtoSearch getAll(DtoPositionCourseDetail dtoPositionCourseDetail) {
		log.info("getAllPostion Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoPositionCourseDetail.getPageNumber());
		dtoSearch.setPageSize(dtoPositionCourseDetail.getPageSize());
		dtoSearch.setTotalCount(repositoryPositionCourseDetail.getCountOfTotalPosition());
		List<PositionCourseDetail> skillPositionList = null;
		if (dtoPositionCourseDetail.getPageNumber() != null && dtoPositionCourseDetail.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoPositionCourseDetail.getPageNumber(), dtoPositionCourseDetail.getPageSize(), Direction.DESC, "createdDate");
			skillPositionList = repositoryPositionCourseDetail.findByIsDeleted(false, pageable);
		} else {
			skillPositionList = repositoryPositionCourseDetail.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		
		List<DtoPositionCourseDetail> dtoPOsitionList=new ArrayList<>();
		if(skillPositionList!=null && !skillPositionList.isEmpty())
		{
			for (PositionCourseDetail position : skillPositionList) 
			{
				dtoPositionCourseDetail=new DtoPositionCourseDetail(position);
				dtoPositionCourseDetail.setId(position.getId());
				dtoPOsitionList.add(dtoPositionCourseDetail);
			}
			dtoSearch.setRecords(dtoPOsitionList);
		}
		log.debug("All PositionCourseDetail List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoPositionCourseDetail delete(List<Integer> ids) {
		log.info("delete DtoPositionCourseDetail Method");
		DtoPositionCourseDetail dtoPosition = new DtoPositionCourseDetail();
		dtoPosition
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("COURSE_DETAL_DELETED", false));
		dtoPosition.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("POSTION_ASSOCIATED", false));
		List<DtoPositionCourseDetail> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer positionId : ids) {
				PositionCourseDetail position = repositoryPositionCourseDetail.findOne(positionId);
				DtoPositionCourseDetail dtoskillSetup2 = new DtoPositionCourseDetail();
				dtoskillSetup2.setId(position.getId());
				repositoryPositionCourseDetail.deleteSinglePosition(true, loggedInUserId, positionId);
				deletePosition.add(dtoskillSetup2);

			}
			dtoPosition.setDeletePosition(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete PositionCourseDetail :"+dtoPosition.getId());
		return dtoPosition;
	}

	public DtoPositionCourseDetail getByPositionId(Integer id) {
		log.info("getByPositionId Method");
		DtoPositionCourseDetail dtoPosition = new DtoPositionCourseDetail();
		if (id > 0) {
			PositionCourseDetail position = repositoryPositionCourseDetail.findByIdAndIsDeleted(id, false);
			if (position != null) {
				dtoPosition = new DtoPositionCourseDetail(position);
			} else {
				dtoPosition.setMessageType("COURSE_DETAL_LIST_NOT_GETTING");

			}
		} else {
			dtoPosition.setMessageType("COURSE_DETAL_LIST_NOT_GETTING");

		}
		log.debug("PositionCourseDetail By Id is:"+dtoPosition.getId());
		return dtoPosition;
	}

	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("search Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			dtoSearch.setTotalCount(this.repositoryPositionCourseDetail.predictivePositionSearchTotalCount("%"+searchWord+"%"));
			List<PositionCourseDetail> positionCourseDetailList =null;
			/*if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				positionCourseDetailList = this.repositoryPositionCourseDetail.predictivePositionSearchWithPagination("%"+searchWord+"%");
			}else {*/
				positionCourseDetailList = this.repositoryPositionCourseDetail.predictivePositionSearchWithPagination("%"+searchWord+"%");
			/*}*/
		
			if(positionCourseDetailList != null && !positionCourseDetailList.isEmpty()){
				List<DtoPositionCourseDetail> dtoPositionCourseDetailList = new ArrayList<>();
				DtoPositionCourseDetail dtoPositionCourseDetail=null;
				for (PositionCourseDetail positionCourseDetail : positionCourseDetailList) {
					dtoPositionCourseDetail = new DtoPositionCourseDetail(positionCourseDetail);
					dtoPositionCourseDetailList.add(dtoPositionCourseDetail);
				}
				dtoSearch.setRecords(dtoPositionCourseDetailList);
			}
		}
		return dtoSearch;
	}
}
