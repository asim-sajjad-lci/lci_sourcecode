package com.bti.hcm.model.dto;

public class DtoAuditTrialCodes extends DtoBase {

	private Integer id;
	private Integer serialId;
	private Integer sequenceNumber;
	private String transectionSourceDesc;
	private String transectionSourceCode;
	private Integer nextTransectionSourceNumber;
	private String sourceDocment;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSerialId() {
		return serialId;
	}

	public void setSerialId(Integer serialId) {
		this.serialId = serialId;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getTransectionSourceDesc() {
		return transectionSourceDesc;
	}

	public void setTransectionSourceDesc(String transectionSourceDesc) {
		this.transectionSourceDesc = transectionSourceDesc;
	}

	public String getTransectionSourceCode() {
		return transectionSourceCode;
	}

	public void setTransectionSourceCode(String transectionSourceCode) {
		this.transectionSourceCode = transectionSourceCode;
	}

	public Integer getNextTransectionSourceNumber() {
		return nextTransectionSourceNumber;
	}

	public void setNextTransectionSourceNumber(Integer nextTransectionSourceNumber) {
		this.nextTransectionSourceNumber = nextTransectionSourceNumber;
	}

	public String getSourceDocment() {
		return sourceDocment;
	}

	public void setSourceDocment(String sourceDocment) {
		this.sourceDocment = sourceDocment;
	}

}
