/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the division database table. Name of
 * Project: Hrm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40101",indexes = {
        @Index(columnList = "DIVINDX")
})
@NamedQuery(name = "Division.findAll", query = "SELECT d FROM Department d")
public class Division extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DIVINDX")
	private int id;

	@Column(name = "DIVIID", columnDefinition = "char(15)")
	private String divisionId;

	@Column(name = "DIDSCR", columnDefinition = "char(31)")
	private String divisionDescription;

	@Column(name = "DIDSCRA", columnDefinition = "char(61)")
	private String arabicDivisionDescription;

	@Column(name = "DIDADDR", columnDefinition = "char(61)")
	private String divisionAddress;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DIDCOUNTRY")
	private HrCountry country;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DIDSTATE")
	private HrState state;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DIDCITY")
	private HrCity city;

	@Column(name = "DIDPHONE", columnDefinition = "char(21)")
	private String phoneNumber;

	@Column(name = "DIDFAX", columnDefinition = "char(21)")
	private String fax;

	@Column(name = "DIDEMAL", columnDefinition = "char(150)")
	private String email;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "division")
	@Where(clause = "EMPACTIV = false and is_deleted = false")
	private List<EmployeeMaster> employeeMaster;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "division")
	@Where(clause = "is_deleted = false")
	private List<Requisitions> requisitions;
	
	@OneToMany(mappedBy = "division",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@LazyCollection(LazyCollectionOption.FALSE)
	@Where(clause = "is_deleted = false")
	private List<EmployeePositionHistory> listEmployeePositionHistory;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}

	public String getArabicDivisionDescription() {
		return arabicDivisionDescription;
	}

	public void setArabicDivisionDescription(String arabicDivisionDescription) {
		this.arabicDivisionDescription = arabicDivisionDescription;
	}

	public String getDivisionAddress() {
		return divisionAddress;
	}

	public void setDivisionAddress(String divisionAddress) {
		this.divisionAddress = divisionAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDivisionDescription() {
		return divisionDescription;
	}

	public void setDivisionDescription(String divisionDescription) {
		this.divisionDescription = divisionDescription;
	}

	public HrCountry getCountry() {
		return country;
	}

	public void setCountry(HrCountry country) {
		this.country = country;
	}

	public HrState getState() {
		return state;
	}

	public void setState(HrState state) {
		this.state = state;
	}

	public HrCity getCity() {
		return city;
	}

	public void setCity(HrCity city) {
		this.city = city;
	}

	public List<EmployeeMaster> getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(List<EmployeeMaster> employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public List<Requisitions> getRequisitions() {
		return requisitions;
	}

	public void setRequisitions(List<Requisitions> requisitions) {
		this.requisitions = requisitions;
	}

	public List<EmployeePositionHistory> getListEmployeePositionHistory() {
		return listEmployeePositionHistory;
	}

	public void setListEmployeePositionHistory(List<EmployeePositionHistory> listEmployeePositionHistory) {
		this.listEmployeePositionHistory = listEmployeePositionHistory;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((arabicDivisionDescription == null) ? 0 : arabicDivisionDescription.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((divisionAddress == null) ? 0 : divisionAddress.hashCode());
		result = prime * result + ((divisionDescription == null) ? 0 : divisionDescription.hashCode());
		result = prime * result + ((divisionId == null) ? 0 : divisionId.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((employeeMaster == null) ? 0 : employeeMaster.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result + id;
		result = prime * result + ((listEmployeePositionHistory == null) ? 0 : listEmployeePositionHistory.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((requisitions == null) ? 0 : requisitions.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Division other = (Division) obj;
		if (arabicDivisionDescription == null) {
			if (other.arabicDivisionDescription != null)
				return false;
		} else if (!arabicDivisionDescription.equals(other.arabicDivisionDescription)) {
			return false;
		}
			
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city)) {
			return false;
		}
			
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country)) {
			return false;
		}
			
		if (divisionAddress == null) {
			if (other.divisionAddress != null)
				return false;
		} else if (!divisionAddress.equals(other.divisionAddress)) {
			return false;
		}
			
		if (divisionDescription == null) {
			if (other.divisionDescription != null)
				return false;
		} else if (!divisionDescription.equals(other.divisionDescription)) {
			return false;
		}
			
		if (divisionId == null) {
			if (other.divisionId != null)
				return false;
		} else if (!divisionId.equals(other.divisionId)) {
			return false;
		}
			
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email)) {
			return false;
		}
			
		if (employeeMaster == null) {
			if (other.employeeMaster != null)
				return false;
		} else if (!employeeMaster.equals(other.employeeMaster)) {
			return false;
		}
			
		if (fax == null) {
			if (other.fax != null)
				return false;
		} else if (!fax.equals(other.fax)) {
			return false;
		}
			
		if (id != other.id)
			return false;
		if (listEmployeePositionHistory == null) {
			if (other.listEmployeePositionHistory != null)
				return false;
		} else if (!listEmployeePositionHistory.equals(other.listEmployeePositionHistory)) {
			return false;
		}
			
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (requisitions == null) {
			if (other.requisitions != null)
				return false;
		} else if (!requisitions.equals(other.requisitions))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}
	
	

}
