/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Description: The persistent class for the OrientationPredefinedCheckListItem
 * database table. Name of Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40521",indexes = {
        @Index(columnList = "PRCHKINDX")
})
@NamedQuery(name = "OrientationPredefinedCheckListItem.findAll", query = "SELECT o FROM OrientationPredefinedCheckListItem o")
public class OrientationPredefinedCheckListItem extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRCHKINDX")
	private int id;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "orientationPredefinedCheckListItem")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<TerminationSetup> listTerminationSetup;

	@Column(name = "PRCHKTYP")
	private short preDefineCheckListType;

	@Column(name = "PRCHKDSCR", columnDefinition = "char(150)")
	private String preDefineCheckListItemDescription;

	@Column(name = "PRCHKDSCRA", columnDefinition = "char(150)")
	private String preDefineCheckListItemDescriptionArabic;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "orientationPredefinedCheckListItem")
	private List<OrientationCheckListSetup> listOrientationCheckListSetup;
	
	
	

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "orientationPredefinedCheckListItem")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<OrientationSetup> listOrientationSetup;
	
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getPreDefineCheckListType() {
		return preDefineCheckListType;
	}

	public void setPreDefineCheckListType(short preDefineCheckListType) {
		this.preDefineCheckListType = preDefineCheckListType;
	}

	public String getPreDefineCheckListItemDescription() {
		return preDefineCheckListItemDescription;
	}

	public void setPreDefineCheckListItemDescription(String preDefineCheckListItemDescription) {
		this.preDefineCheckListItemDescription = preDefineCheckListItemDescription;
	}

	public String getPreDefineCheckListItemDescriptionArabic() {
		return preDefineCheckListItemDescriptionArabic;
	}

	public void setPreDefineCheckListItemDescriptionArabic(String preDefineCheckListItemDescriptionArabic) {
		this.preDefineCheckListItemDescriptionArabic = preDefineCheckListItemDescriptionArabic;
	}

	public List<OrientationCheckListSetup> getListOrientationCheckListSetup() {
		return listOrientationCheckListSetup;
	}

	public void setListOrientationCheckListSetup(List<OrientationCheckListSetup> listOrientationCheckListSetup) {
		this.listOrientationCheckListSetup = listOrientationCheckListSetup;
	}

	public List<TerminationSetup> getListTerminationSetup() {
		return listTerminationSetup;
	}

	public void setListTerminationSetup(List<TerminationSetup> listTerminationSetup) {
		this.listTerminationSetup = listTerminationSetup;
	}

	public List<OrientationSetup> getListOrientationSetup() {
		return listOrientationSetup;
	}

	public void setListOrientationSetup(List<OrientationSetup> listOrientationSetup) {
		this.listOrientationSetup = listOrientationSetup;
	}

}
