package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bti.hcm.model.Default;

public class DtoDistribrution {

	private String defaultId;
	private Integer defaultPrimaryId;
	private Integer buildId;
	private Integer auditTransactionNo;
	private String auditTransactionSourceNo;
	private Integer transcationId;
	
	private List<Integer> listDepartmentId;
	private List<Integer> listEmployeeId;
	
	private Map<String, BigDecimal> listOfDeduction;
	private Map<String, BigDecimal>listOfBenefit;
	
	private BigDecimal totalEarnings;
	private BigDecimal totalDeductions;
	private BigDecimal netPay;
	
	private BigDecimal totalAmont;
	private String employeeId;
	private Integer employeePrimaryId;
	private String projectId;
	private String employeeFirstName; 
	
	private Integer departmentPrimaryId;
	private String departmentId;
	private String departmentDescription;
	
	private String codeId;
	private Integer codePrimaryId;
	
	private Short transactionType;
	private String transectionSourceCode;
	private Integer nextTransectionSourceNumber;
	private String description;
	
	private Integer accountPrimaryId;
	private String dimensionValue;
	private Integer accountNumberSeq;
	private Integer checkNumber;
	private String checkNumberDisplay;
	private DtoCOAMainAccounts accounts;
	private DtoEmployeeMasterHcm employeeMasterHcm;
	private DtoPayCode payCode;
	private Default defaults;
	private DtoBuildChecks dtoBuildChecks;
	private DtoDepartment dtoDepartment;
	private DtoManualChecks dtoManualChecks;
	private DtoManualChecksDetails dtoManualChecksDetails;
    private DtoAccountsTableAccumulation dtoAccountsTableAccumulation;
    private DtoAuditTrialCodes auditTrialCodes;
    
    private List<DtoBuildPayrollCheckByBenefits> listDtoBuildPayrollCheckByBenefits;
	private List<DtoBuildPayrollCheckByDeductions> listDtoBuildPayrollCheckByDeductions;
    
	
	private short postingTypes;
	private BigDecimal debitAmount;
	private BigDecimal creditAmount;
	private Integer sequence;

	private Integer batchPrimaryId;
	
	
	private Integer positionPrimaryId;
	private String positionId;
	private String descriptions;
	
	private Integer locationPrimaryId;
	private String locationId;
	private String descriptionss;
	
	private Integer bankAcountPrimaryId;
	private String bankAcountnumber;
	private String bankName;
	
	private BigDecimal totalDeduction;
	private BigDecimal totalBenefit;
	
	private Date postingDate;
	
	public String getDefaultId() {
		return defaultId;
	}

	public void setDefaultId(String defaultId) {
		this.defaultId = defaultId;
	}

	

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getEmployeePrimaryId() {
		return employeePrimaryId;
	}

	public void setEmployeePrimaryId(Integer employeePrimaryId) {
		this.employeePrimaryId = employeePrimaryId;
	}

	
	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public Integer getCodePrimaryId() {
		return codePrimaryId;
	}

	public void setCodePrimaryId(Integer codePrimaryId) {
		this.codePrimaryId = codePrimaryId;
	}

	public DtoCOAMainAccounts getAccounts() {
		return accounts;
	}

	public void setAccounts(DtoCOAMainAccounts accounts) {
		this.accounts = accounts;
	}

	public short getPostingTypes() {
		return postingTypes;
	}

	public void setPostingTypes(short postingTypes) {
		this.postingTypes = postingTypes;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public BigDecimal getTotalAmont() {
		return totalAmont;
	}

	public void setTotalAmont(BigDecimal totalAmont) {
		this.totalAmont = totalAmont;
	}

	public DtoEmployeeMasterHcm getEmployeeMasterHcm() {
		return employeeMasterHcm;
	}

	public void setEmployeeMasterHcm(DtoEmployeeMasterHcm employeeMasterHcm) {
		this.employeeMasterHcm = employeeMasterHcm;
	}

	public DtoPayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(DtoPayCode payCode) {
		this.payCode = payCode;
	}

	public Default getDefaults() {
		return defaults;
	}

	public void setDefaults(Default defaults) {
		this.defaults = defaults;
	}

	public DtoBuildChecks getDtoBuildChecks() {
		return dtoBuildChecks;
	}

	public void setDtoBuildChecks(DtoBuildChecks dtoBuildChecks) {
		this.dtoBuildChecks = dtoBuildChecks;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public DtoDepartment getDtoDepartment() {
		return dtoDepartment;
	}

	public void setDtoDepartment(DtoDepartment dtoDepartment) {
		this.dtoDepartment = dtoDepartment;
	}

	public Integer getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(Integer checkNumber) {
		this.checkNumber = checkNumber;
	}

	public DtoManualChecks getDtoManualChecks() {
		return dtoManualChecks;
	}

	public void setDtoManualChecks(DtoManualChecks dtoManualChecks) {
		this.dtoManualChecks = dtoManualChecks;
	}

	

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getDepartmentDescription() {
		return departmentDescription;
	}

	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public DtoManualChecksDetails getDtoManualChecksDetails() {
		return dtoManualChecksDetails;
	}

	public void setDtoManualChecksDetails(DtoManualChecksDetails dtoManualChecksDetails) {
		this.dtoManualChecksDetails = dtoManualChecksDetails;
	}

	public Short getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Short transactionType) {
		this.transactionType = transactionType;
	}

	

	public Integer getAccountNumberSeq() {
		return accountNumberSeq;
	}

	public void setAccountNumberSeq(Integer accountNumberSeq) {
		this.accountNumberSeq = accountNumberSeq;
	}

	public DtoAccountsTableAccumulation getDtoAccountsTableAccumulation() {
		return dtoAccountsTableAccumulation;
	}

	public void setDtoAccountsTableAccumulation(DtoAccountsTableAccumulation dtoAccountsTableAccumulation) {
		this.dtoAccountsTableAccumulation = dtoAccountsTableAccumulation;
	}

	public DtoAuditTrialCodes getAuditTrialCodes() {
		return auditTrialCodes;
	}

	public void setAuditTrialCodes(DtoAuditTrialCodes auditTrialCodes) {
		this.auditTrialCodes = auditTrialCodes;
	}

	public String getTransectionSourceCode() {
		return transectionSourceCode;
	}

	public void setTransectionSourceCode(String transectionSourceCode) {
		this.transectionSourceCode = transectionSourceCode;
	}

	public Integer getNextTransectionSourceNumber() {
		return nextTransectionSourceNumber;
	}

	public void setNextTransectionSourceNumber(Integer nextTransectionSourceNumber) {
		this.nextTransectionSourceNumber = nextTransectionSourceNumber;
	}

	public Integer getAccountPrimaryId() {
		return accountPrimaryId;
	}

	public void setAccountPrimaryId(Integer accountPrimaryId) {
		this.accountPrimaryId = accountPrimaryId;
	}

	public Integer getBuildId() {
		return buildId;
	}

	public void setBuildId(Integer buildId) {
		this.buildId = buildId;
	}

	public Integer getAuditTransactionNo() {
		return auditTransactionNo;
	}

	public void setAuditTransactionNo(Integer auditTransactionNo) {
		this.auditTransactionNo = auditTransactionNo;
	}

	public Integer getDefaultPrimaryId() {
		return defaultPrimaryId;
	}

	public void setDefaultPrimaryId(Integer defaultPrimaryId) {
		this.defaultPrimaryId = defaultPrimaryId;
	}

	public String getCheckNumberDisplay() {
		return checkNumberDisplay;
	}

	public void setCheckNumberDisplay(String checkNumberDisplay) {
		this.checkNumberDisplay = checkNumberDisplay;
	}

	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getDescriptionss() {
		return descriptionss;
	}

	public void setDescriptionss(String descriptionss) {
		this.descriptionss = descriptionss;
	}

	public String getBankAcountnumber() {
		return bankAcountnumber;
	}

	public void setBankAcountnumber(String bankAcountnumber) {
		this.bankAcountnumber = bankAcountnumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Integer getDepartmentPrimaryId() {
		return departmentPrimaryId;
	}

	public void setDepartmentPrimaryId(Integer departmentPrimaryId) {
		this.departmentPrimaryId = departmentPrimaryId;
	}

	public Integer getPositionPrimaryId() {
		return positionPrimaryId;
	}

	public void setPositionPrimaryId(Integer positionPrimaryId) {
		this.positionPrimaryId = positionPrimaryId;
	}

	public Integer getLocationPrimaryId() {
		return locationPrimaryId;
	}

	public void setLocationPrimaryId(Integer locationPrimaryId) {
		this.locationPrimaryId = locationPrimaryId;
	}

	public Integer getBankAcountPrimaryId() {
		return bankAcountPrimaryId;
	}

	public void setBankAcountPrimaryId(Integer bankAcountPrimaryId) {
		this.bankAcountPrimaryId = bankAcountPrimaryId;
	}

	public BigDecimal getTotalDeduction() {
		return totalDeduction;
	}

	public void setTotalDeduction(BigDecimal totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	public BigDecimal getTotalBenefit() {
		return totalBenefit;
	}

	public void setTotalBenefit(BigDecimal totalBenefit) {
		this.totalBenefit = totalBenefit;
	}

	public Integer getBatchPrimaryId() {
		return batchPrimaryId;
	}

	public void setBatchPrimaryId(Integer batchPrimaryId) {
		this.batchPrimaryId = batchPrimaryId;
	}

	public String getAuditTransactionSourceNo() {
		return auditTransactionSourceNo;
	}

	public void setAuditTransactionSourceNo(String auditTransactionSourceNo) {
		this.auditTransactionSourceNo = auditTransactionSourceNo;
	}

	public Map<String, BigDecimal> getListOfDeduction() {
		return listOfDeduction;
	}

	public void setListOfDeduction(Map<String, BigDecimal> listOfDeduction) {
		this.listOfDeduction = listOfDeduction;
	}

	public Map<String, BigDecimal> getListOfBenefit() {
		return listOfBenefit;
	}

	public void setListOfBenefit(Map<String, BigDecimal> listOfBenefit) {
		this.listOfBenefit = listOfBenefit;
	}

	public BigDecimal getTotalEarnings() {
		return totalEarnings;
	}

	public void setTotalEarnings(BigDecimal totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

	public BigDecimal getTotalDeductions() {
		return totalDeductions;
	}

	public void setTotalDeductions(BigDecimal totalDeductions) {
		this.totalDeductions = totalDeductions;
	}

	public BigDecimal getNetPay() {
		return netPay;
	}

	public void setNetPay(BigDecimal netPay) {
		this.netPay = netPay;
	}

	public Integer getTranscationId() {
		return transcationId;
	}

	public void setTranscationId(Integer transcationId) {
		this.transcationId = transcationId;
	}

	public List<Integer> getListDepartmentId() {
		return listDepartmentId;
	}

	public void setListDepartmentId(List<Integer> listDepartmentId) {
		this.listDepartmentId = listDepartmentId;
	}

	public List<Integer> getListEmployeeId() {
		return listEmployeeId;
	}

	public void setListEmployeeId(List<Integer> listEmployeeId) {
		this.listEmployeeId = listEmployeeId;
	}

	public List<DtoBuildPayrollCheckByBenefits> getListDtoBuildPayrollCheckByBenefits() {
		return listDtoBuildPayrollCheckByBenefits;
	}

	public void setListDtoBuildPayrollCheckByBenefits(
			List<DtoBuildPayrollCheckByBenefits> listDtoBuildPayrollCheckByBenefits) {
		this.listDtoBuildPayrollCheckByBenefits = listDtoBuildPayrollCheckByBenefits;
	}

	public List<DtoBuildPayrollCheckByDeductions> getListDtoBuildPayrollCheckByDeductions() {
		return listDtoBuildPayrollCheckByDeductions;
	}

	public void setListDtoBuildPayrollCheckByDeductions(
			List<DtoBuildPayrollCheckByDeductions> listDtoBuildPayrollCheckByDeductions) {
		this.listDtoBuildPayrollCheckByDeductions = listDtoBuildPayrollCheckByDeductions;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}
	
}
