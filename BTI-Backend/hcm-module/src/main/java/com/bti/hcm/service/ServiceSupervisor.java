package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.Supervisor;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSupervisor;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositorySupervisor;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceSupervisor")
public class ServiceSupervisor {

/**
 *@Description LOGGER use for put a logger in Supervisor Service 
 */
static Logger log = Logger.getLogger(ServiceSupervisor.class.getName());
	
	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in Supervisor service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in Supervisor service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in repositorySupervisor service
	 */
	
	@Autowired
	RepositorySupervisor repositorySupervisor;
	
	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster ;
	
	
	
	public DtoSupervisor saveOrUpdateSupervisor(DtoSupervisor dtoSupervisor) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		Supervisor supervisor=null;
		if (dtoSupervisor.getId() != null && dtoSupervisor.getId() > 0) {
			supervisor = repositorySupervisor.findByIdAndIsDeleted(dtoSupervisor.getId(), false);
			supervisor.setUpdatedBy(loggedInUserId);
			supervisor.setUpdatedDate(new Date());
		} else {
			supervisor = new Supervisor();
			supervisor.setCreatedDate(new Date());
			

			Integer rowId = repositorySupervisor.getCountOfTotalSupervisors();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			supervisor.setRowId(increment);
		}
		EmployeeMaster employeeMaste=null;
		if(dtoSupervisor.getEmployeeId()!=null) {
			employeeMaste=repositoryEmployeeMaster.findOne(dtoSupervisor.getEmployeeId());
			 supervisor.setEmployeeId(employeeMaste);
		}else {
			supervisor.setEmployeeId(null);
		}
		/*EmployeeMaster employeeMaster=null;
		if(dtoSupervisor.getEmployeeMaster()!=null && dtoSupervisor.getEmployeeMaster().getEmployeeIndexId()>0) {
			employeeMaster=repositoryEmployeeMaster.findOne(dtoSupervisor.getEmployeeMaster().getEmployeeIndexId());
		}
		else {
			supervisor.setEmployeeId(employeeMaster);
		}*/
	//	EmployeeMaster employeeMaster  =repositoryEmployeeMaster.findOne(dtoSupervisor.getEmployeeId());
		
		supervisor.setDescription(dtoSupervisor.getDescription());
		supervisor.setArabicDescription(dtoSupervisor.getArabicDescription());
		supervisor.setSuperVisionCode(dtoSupervisor.getSuperVisionCode());
		supervisor.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		repositorySupervisor.saveAndFlush(supervisor);
		return dtoSupervisor;
	}
	
	public DtoSearch getAllSupervisor(DtoSupervisor dtoSupervisor) {
		

		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSupervisor.getPageNumber());
		
		
		
		
		dtoSearch.setPageSize(dtoSupervisor.getPageSize());
		dtoSearch.setTotalCount(repositorySupervisor.getCountOfTotalSupervisor());
		List<Supervisor> supervisorList = null;
		if (dtoSupervisor.getPageNumber() != null && dtoSupervisor.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSupervisor.getPageNumber(), dtoSupervisor.getPageSize(), Direction.DESC, "createdDate");
			supervisorList = repositorySupervisor.findByIsDeleted(false, pageable);
		} else {
			supervisorList = repositorySupervisor.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		
		List<DtoSupervisor> dtoSupervisorList=new ArrayList<>();
		if(supervisorList!=null && !supervisorList.isEmpty())
		{
			for (Supervisor supervisor : supervisorList) 
			{
				dtoSupervisor=new DtoSupervisor(supervisor);
				dtoSupervisor.setDescription(supervisor.getDescription());
				dtoSupervisor.setArabicDescription(supervisor.getArabicDescription());
				dtoSupervisor.setSuperVisionCode(supervisor.getSuperVisionCode());
				
				if(supervisor.getEmployeeId()!=null){
					dtoSupervisor.setEmployeeId(supervisor.getEmployeeId().getEmployeeIndexId());
					dtoSupervisor.setEmployeeName(supervisor.getEmployeeId().getEmployeeFirstName());
				}
				
				dtoSupervisor.setId(supervisor.getId());
				dtoSupervisorList.add(dtoSupervisor);
			}
			dtoSearch.setRecords(dtoSupervisorList);
		}
		return dtoSearch;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchSupervisor(DtoSearch dtoSearch) {
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			String condition="";
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("superVisionCode")|| dtoSearch.getSortOn().equals("description") || dtoSearch.getSortOn().equals("arabicDescription")|| dtoSearch.getSortOn().equals("employeeName") ||
							dtoSearch.getSortOn().equals("employeeMaster.employeeId")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	
			
			
			
			dtoSearch.setTotalCount(this.repositorySupervisor.predictiveSupervisorSearchTotalCount("%"+searchWord+"%"));
			
			List<Supervisor> supervisorList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					supervisorList = this.repositorySupervisor.predictiveSupervisorSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					supervisorList = this.repositorySupervisor.predictiveSupervisorSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					supervisorList = this.repositorySupervisor.predictiveSupervisorSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			
			if(supervisorList != null && !supervisorList.isEmpty()){
				List<DtoSupervisor> dtoSupervisorList = new ArrayList<>();
				for (Supervisor supervisor : supervisorList) {
					DtoSupervisor dtoSupervisor = new DtoSupervisor(supervisor);
					dtoSupervisor.setDescription(supervisor.getDescription());
					dtoSupervisor.setId(supervisor.getId());
					dtoSupervisor.setArabicDescription(supervisor.getArabicDescription());
					dtoSupervisor.setSuperVisionCode(supervisor.getSuperVisionCode());
					if(supervisor.getEmployeeId()!=null){
						dtoSupervisor.setEmployeeId(supervisor.getEmployeeId().getEmployeeIndexId());
						dtoSupervisor.setEmployeeName(supervisor.getEmployeeId().getEmployeeFirstName());
						dtoSupervisor.setEmployeeIds(supervisor.getEmployeeId().getEmployeeId());
					}
					if (dtoSupervisor.getListDtoEmployeeMaster() == null && dtoSupervisor.getListDtoEmployeeMaster()!=null && dtoSupervisor.getListDtoEmployeeMaster().isEmpty() && !dtoSupervisor.getListDtoEmployeeMaster().isEmpty()) {
						List<DtoEmployeeMaster> listDtoEmployeeMaster = new ArrayList<>();
						dtoSupervisor.setListDtoEmployeeMaster(listDtoEmployeeMaster);
					}
					
					if(supervisor.getEmployeeId()!=null && supervisor.getEmployeeId()==null) {
						DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
						dtoEmployeeMaster.setEmployeeId(supervisor.getEmployeeId().getEmployeeId());
						dtoEmployeeMaster.setDepartmentId(supervisor.getEmployeeId().getDepartment().getId());
						dtoEmployeeMaster.setDivisionId(supervisor.getEmployeeId().getDivision().getId());
						dtoEmployeeMaster.setEmployeeFirstName(supervisor.getEmployeeId().getEmployeeFirstName());
						dtoEmployeeMaster.setEmployeeLastName(supervisor.getEmployeeId().getEmployeeLastName());
						dtoEmployeeMaster.setEmployeeFirstNameArabic(supervisor.getEmployeeId().getEmployeeFirstNameArabic());
						dtoEmployeeMaster.setEmployeeLastNameArabic(supervisor.getEmployeeId().getEmployeeLastNameArabic());
						dtoEmployeeMaster.setEmployeeGender(supervisor.getEmployeeId().getEmployeeGender());
						dtoEmployeeMaster.setEmployeeMaritalStatus(supervisor.getEmployeeId().getEmployeeMaritalStatus());
						dtoEmployeeMaster.setEmployeeMiddleName(supervisor.getEmployeeId().getEmployeeMiddleName());
						dtoEmployeeMaster.setEmployeeMiddleNameArabic(supervisor.getEmployeeId().getEmployeeMiddleNameArabic());
						dtoEmployeeMaster.setEmployeeBirthDate(supervisor.getEmployeeId().getEmployeeBirthDate());
						dtoEmployeeMaster.setEmployeeTitle(supervisor.getEmployeeId().getEmployeeTitle());
						dtoSupervisor.getListDtoEmployeeMaster().add(dtoEmployeeMaster);
					}
					
					
					
					supervisor.getEmployeeId();
					
					dtoSupervisor.setId(supervisor.getId());
					dtoSupervisorList.add(dtoSupervisor);
				}
				dtoSearch.setRecords(dtoSupervisorList);
			}
		}
		return dtoSearch;
	}
	
	public DtoSupervisor getSuperVisorByCode(String superVisorCode) {
		DtoSupervisor dtoSupervisor  = new DtoSupervisor();
		if (superVisorCode!=null) {
			Supervisor supervisor = repositorySupervisor.findSuperVisorByCode(superVisorCode);
			if (supervisor != null) {
				dtoSupervisor = new DtoSupervisor(supervisor);
			} else {
				dtoSupervisor.setMessageType("SUPERVISOR_NOT_GETTING");

			}
		} else {
			dtoSupervisor.setMessageType("INVALID_SUPERVISOR_ID");

		}
		return dtoSupervisor;
	}
	
	
	
	public DtoSupervisor deleteSupervisor(List<Integer> ids) {
		log.info("deleteSupervisor Method");
		DtoSupervisor dtoSupervisor = new DtoSupervisor();
		dtoSupervisor.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SUPERVISOR_DELETED", false));
		dtoSupervisor.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SUPERVISOR_ASSOCIATED", false));
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage
				.append(serviceResponse.getMessageByShortAndIsDeleted("SUPERVISOR_NOT_DELETE_ID_MESSAGE", false).getMessage());
		List<DtoSupervisor> deleteSupervisor = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer supervisorId : ids) {
				Supervisor supervisor = repositorySupervisor.findOne(supervisorId);
				if (supervisor.getEmployeeMaster().isEmpty() && supervisor.getListEmployeePositionHistory().isEmpty() &&supervisor.getRequisitions().isEmpty()) {
					DtoSupervisor dtoSupervisor2 = new DtoSupervisor();
					dtoSupervisor2.setId(supervisorId);
					dtoSupervisor2.setDescription(supervisor.getDescription());
					dtoSupervisor2.setArabicDescription(supervisor.getArabicDescription());
					dtoSupervisor2.setSuperVisionCode(supervisor.getSuperVisionCode());
					if(supervisor.getEmployeeId()!=null){
						dtoSupervisor.setEmployeeId(supervisor.getEmployeeId().getEmployeeIndexId());
						dtoSupervisor.setEmployeeName(supervisor.getEmployeeId().getEmployeeFirstName());
					}
					repositorySupervisor.deleteSingleSupervisor(true, loggedInUserId, supervisorId);
					deleteSupervisor.add(dtoSupervisor2);
				} else {
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoSupervisor.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoSupervisor.setMessageType("");
			}
			dtoSupervisor.setDeletesupervisor(deleteSupervisor);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Supervisor :"+dtoSupervisor.getId());
		return dtoSupervisor;
	}
	
	
	public DtoSupervisor repeatBySuperVisorCode(String superVisionCode) {
		log.info("repeatBySupervisorId Method");
		DtoSupervisor dtoSupervisor = new DtoSupervisor();
		try {
			List<Supervisor> supervisor=repositorySupervisor.findBySuperVisionCode(superVisionCode);
			if(supervisor!=null && !supervisor.isEmpty()) {
				dtoSupervisor.setIsRepeat(true);
				dtoSupervisor.setSuperVisionCode(superVisionCode);
			}else {
				dtoSupervisor.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSupervisor;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchEmployeeById(DtoSearch dtoSearch) {
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			dtoSearch.setTotalCount(this.repositorySupervisor.predictiveEmployeeCount(Integer.parseInt(searchWord)));
			EmployeeMaster employee = this.repositorySupervisor.predictiveEmployeeIdSearch(Integer.parseInt(searchWord));
			if(employee!=null) {
			DtoEmployeeMaster dtoEmployee=new DtoEmployeeMaster();
			dtoEmployee.setEmployeeIndexId(employee.getEmployeeIndexId());
			dtoEmployee.setEmployeeFirstName(employee.getEmployeeFirstName());
			dtoEmployee.setEmployeeLastName(employee.getEmployeeLastName());
			dtoSearch.setRecords(dtoEmployee);
			}
		}
		
		return dtoSearch;
	}

	public DtoSearch searchSupervisorId(DtoSearch dtoSearch) {
		log.info("searchSupervisorId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
			List<String> supervisorIdList =null;
			
				supervisorIdList = this.repositorySupervisor.predictiveSupervisorIdSearchWithPagination("%"+searchWord+"%");
				if(!supervisorIdList.isEmpty()) {
					dtoSearch.setTotalCount(supervisorIdList.size());
					dtoSearch.setRecords(supervisorIdList.size());
				}
			dtoSearch.setIds(supervisorIdList);
		}
		
		return dtoSearch;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<DtoSupervisor> getAllSupervisorDropDownList() {
		log.info("getAllSupervisorDropDownList  Method");
		List<DtoSupervisor> dtoSupervisorList = new ArrayList<>();
		List<Supervisor> list = repositorySupervisor.findByIsDeleted(false);
		
		if (list != null && !list.isEmpty()) {
			for (Supervisor supervisor : list) {
				DtoSupervisor dtoSupervisor = new DtoSupervisor();
				
				dtoSupervisor.setId(supervisor.getId());
				dtoSupervisor.setSuperVisionCode(supervisor.getSuperVisionCode());
				dtoSupervisor.setDescription(supervisor.getDescription());
				dtoSupervisorList.add(dtoSupervisor);
			}
		}
		log.debug("Supervisor is:"+dtoSupervisorList.size());
		return dtoSupervisorList;
	}
	
	
	
	
	
	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllSuperVisorId(DtoSearch dtoSearch) {
		log.info("getAllSuperVisorId Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "superVisionCode" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "description" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "arabicDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "employeeName" : 
					condition+=dtoSearch.getSortOn();
					break;
				default:
					condition+="id";
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
			
			dtoSearch.setTotalCount(this.repositorySupervisor.predictiveSupervisorSearchTotalCount("%"+searchWord+"%"));
			List<Supervisor> supervisorList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					supervisorList =this.repositorySupervisor.predictivegetAllSuperVisorIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					supervisorList = this.repositorySupervisor.predictivegetAllSuperVisorIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					supervisorList = this.repositorySupervisor.predictivegetAllSuperVisorIdSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			if(supervisorList != null && !supervisorList.isEmpty()){
				List<DtoSupervisor> dtoSupervisorList = new ArrayList<>();
				for (Supervisor supervisor : supervisorList) {
					DtoSupervisor dtoSupervisor = new DtoSupervisor(supervisor);
					
					dtoSupervisor.setDescription(supervisor.getDescription());
					dtoSupervisor.setArabicDescription(supervisor.getArabicDescription());
					dtoSupervisor.setSuperVisionCode(supervisor.getSuperVisionCode());
					
					if(supervisor.getEmployeeId()!=null){
						dtoSupervisor.setEmployeeId(supervisor.getEmployeeId().getEmployeeIndexId());
						dtoSupervisor.setEmployeeName(supervisor.getEmployeeId().getEmployeeFirstName());
						dtoSupervisor.setEmployeeIds(supervisor.getEmployeeId().getEmployeeId());
					}
					dtoSupervisor.setId(supervisor.getId());
					
					dtoSupervisorList.add(dtoSupervisor);
				}
				dtoSearch.setRecords(dtoSupervisorList);
			}
			}
		} catch (Exception e) {
			log.error(e);
		}
		log.debug("Search getAllSuperVisorId Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoSupervisor getById(DtoSupervisor dtoSupervisor1) {

		Supervisor supervisor=repositorySupervisor.findByIdAndIsDeleted(dtoSupervisor1.getId(), false);
		
		if(supervisor!=null) {
			dtoSupervisor1.setDescription(supervisor.getDescription());
			dtoSupervisor1.setId(supervisor.getId());
			dtoSupervisor1.setArabicDescription(supervisor.getArabicDescription());
			dtoSupervisor1.setSuperVisionCode(supervisor.getSuperVisionCode());
			dtoSupervisor1.setDescription(supervisor.getDescription());
			dtoSupervisor1.setEmployeeName(supervisor.getEmployeeName());
			if(supervisor.getEmployeeId()!=null){
				dtoSupervisor1.setEmployeeId(supervisor.getEmployeeId().getEmployeeIndexId());
				dtoSupervisor1.setEmployeeName(supervisor.getEmployeeId().getEmployeeFirstName());
				dtoSupervisor1.setEmployeeIds(supervisor.getEmployeeId().getEmployeeId());
			}
			if (dtoSupervisor1.getListDtoEmployeeMaster() == null) {
				List<DtoEmployeeMaster> listDtoEmployeeMaster = new ArrayList<>();
				dtoSupervisor1.setListDtoEmployeeMaster(listDtoEmployeeMaster);
			}
			DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
			if(supervisor.getEmployeeId()!=null) {
				dtoEmployeeMaster.setEmployeeId(supervisor.getEmployeeId().getEmployeeId());
				dtoEmployeeMaster.setDepartmentId(supervisor.getEmployeeId().getDepartment().getId());
				dtoEmployeeMaster.setDivisionId(supervisor.getEmployeeId().getDivision().getId());
				dtoEmployeeMaster.setEmployeeFirstName(supervisor.getEmployeeId().getEmployeeFirstName());
				dtoEmployeeMaster.setEmployeeLastName(supervisor.getEmployeeId().getEmployeeLastName());
				dtoEmployeeMaster.setEmployeeFirstNameArabic(supervisor.getEmployeeId().getEmployeeFirstNameArabic());
				dtoEmployeeMaster.setEmployeeLastNameArabic(supervisor.getEmployeeId().getEmployeeLastNameArabic());
				dtoEmployeeMaster.setEmployeeGender(supervisor.getEmployeeId().getEmployeeGender());
				dtoEmployeeMaster.setEmployeeMaritalStatus(supervisor.getEmployeeId().getEmployeeMaritalStatus());
				dtoEmployeeMaster.setEmployeeMiddleName(supervisor.getEmployeeId().getEmployeeMiddleName());
				dtoEmployeeMaster.setEmployeeMiddleNameArabic(supervisor.getEmployeeId().getEmployeeMiddleNameArabic());
				dtoEmployeeMaster.setEmployeeBirthDate(supervisor.getEmployeeId().getEmployeeBirthDate());
				dtoEmployeeMaster.setEmployeeTitle(supervisor.getEmployeeId().getEmployeeTitle());
				dtoSupervisor1.setEmployeeMaster(dtoEmployeeMaster);
		}
		
	}
		return dtoSupervisor1;
	
	}
	
}
