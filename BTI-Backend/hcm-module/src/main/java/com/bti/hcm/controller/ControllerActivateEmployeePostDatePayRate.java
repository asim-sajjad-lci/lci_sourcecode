package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoActivateEmployeePostDatePayRate;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceActivateEmployeePostDatePayRate;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/activateEmployeePostDatePayRate")
public class ControllerActivateEmployeePostDatePayRate extends BaseController{
	
	private static final Logger LOGGER = Logger.getLogger(ControllerActivateEmployeePostDatePayRate.class);
	
	
	@Autowired
	ServiceActivateEmployeePostDatePayRate  serviceActivateEmployeePostDatePayRate;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	

	
	/**
	 * 
	 * @param request
	 * @param dtoActivateEmployeePostDatePayRate
	 * @return
	 * @throws Exception
	 * request{
	"activePostDatedSeq":12,
	"effectiveDate":2121212121,
	"currentRate":1,
	"newRate":2,
	"active":1,
	
	"employeeMaster":{
		"employeeIndexId":1
	},
	"payCode":{
		"id":1
	}
}


response{
    "code": 201,
    "status": "CREATED",
    "result": {
        "id": null,
        "activePostDatedSeq": 12,
        "effectiveDate": 2121212121,
        "currentRate": 1,
        "newRate": 2,
        "active": true,
        "employeeMaster": {
            "employeeIndexId": 1,
            "employeeId": null,
            "employeeTitle": null,
            "employeeTitleArabic": null,
            "employeeLastName": null,
            "employeeFirstName": null,
            "employeeMiddleName": null,
            "employeeLastNameArabic": null,
            "employeeFirstNameArabic": null,
            "employeeMiddleNameArabic": null,
            "department_Id": null,
            "position_Id": null,
            "division_Id": null,
            "employeeHireDate": null,
            "employeeAdjustHireDate": null,
            "employeeLastWorkDate": null,
            "employeeInactiveDate": null,
            "employeeInactiveReason": null,
            "employeeType": 0,
            "employeeGender": 0,
            "employeeMaritalStatus": 0,
            "employeeBirthDate": null,
            "employeeUserIdInSystem": 0,
            "employeeWorkHourYearly": 0,
            "employeeCitizen": false,
            "employeeInactive": null,
            "employeeSmoker": false,
            "employeeImmigration": false,
            "divisionId": 0,
            "departmentId": 0,
            "locationId": 0,
            "supervisorId": 0,
            "positionId": 0,
            "employeeNationalitiesId": 0,
            "employeeAddressIndexId": 0,
            "employeeImage": null,
            "pageNumber": null,
            "pageSize": null,
            "ids": null,
            "isActive": null,
            "messageType": null,
            "message": null,
            "deleteMessage": null,
            "associateMessage": null,
            "$$index": null,
            "deleteEmployeeMaster": null,
            "dtoEmployeeAddressMaster": null,
            "dtoEmployeeNationalities": null,
            "dtoDepartment": null,
            "dtoDivision": null,
            "dtoPosition": null,
            "dtoLocation": null,
            "dtoSupervisor": null,
            "isRepeat": null
        },
        "payCode": {
            "id": 1,
            "payCodeTypeId": null,
            "payCodeTypeDesc": null,
            "payCodeTypeArabicDesc": null,
            "payCodeId": null,
            "description": null,
            "arbicDescription": null,
            "payType": null,
            "baseOnPayCode": null,
            "baseOnPayCodeAmount": null,
            "payFactor": null,
            "payRate": null,
            "unitofPay": null,
            "payperiod": null,
            "inActive": false,
            "pageNumber": null,
            "pageSize": null,
            "ids": null,
            "isActive": null,
            "messageType": null,
            "message": null,
            "deleteMessage": null,
            "associateMessage": null,
            "delete": null,
            "listTimeCode": null,
            "isRepeat": null
        },
        "pageNumber": null,
        "pageSize": null,
        "ids": null,
        "isActive": null,
        "messageType": null,
        "message": null,
        "deleteMessage": null,
        "associateMessage": null,
        "delete": null,
        "isRepeat": null
    },
    "btiMessage": {
        "message": "N/A",
        "messageShort": "N/A"
    }
}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate) throws Exception {
		LOGGER.info("Create ActivateEmployeePostDatePayRate Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoActivateEmployeePostDatePayRate  = serviceActivateEmployeePostDatePayRate.saveOrUpdate(dtoActivateEmployeePostDatePayRate);
			responseMessage=displayMessage(dtoActivateEmployeePostDatePayRate, "ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_CREATED", "ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create ActivateEmployeePostDatePayRate Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate) throws Exception {
		LOGGER.info("Update ActivateEmployeePostDatePayRate Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoActivateEmployeePostDatePayRate = serviceActivateEmployeePostDatePayRate.saveOrUpdate(dtoActivateEmployeePostDatePayRate);
			responseMessage=displayMessage(dtoActivateEmployeePostDatePayRate, "ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_UPDATED", "ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update ActivateEmployeePostDatePayRate Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate) throws Exception {
		LOGGER.info("Delete ActivateEmployeePostDatePayRate Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoActivateEmployeePostDatePayRate.getIds() != null && !dtoActivateEmployeePostDatePayRate.getIds().isEmpty()) {
				DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate2 = serviceActivateEmployeePostDatePayRate.delete(dtoActivateEmployeePostDatePayRate.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_DELETED", false), dtoActivateEmployeePostDatePayRate2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete ActivateEmployeePostDatePayRate Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate) throws Exception {
		LOGGER.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRateObj = serviceActivateEmployeePostDatePayRate.getById(dtoActivateEmployeePostDatePayRate.getId());
			responseMessage=displayMessage(dtoActivateEmployeePostDatePayRateObj, "ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_LIST_GET_DETAIL", "ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoActivateEmployeePostDatePayRate.getId());
		return responseMessage;
	}
	

	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search ActivateEmployeePostDatePayRate Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceActivateEmployeePostDatePayRate.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_GET_ALL", "ACTIVAT_EMPLOYEE_POST_DATE_PAY_RATE_LIST_NOT_GETTING", serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	

}
