/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.InterviewTypeSetup;

/**
 * Description: Interface for InterviewTypeSetup 
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@Repository("repositoryInterviewTypeSetup")
public interface RepositoryInterviewTypeSetup extends JpaRepository<InterviewTypeSetup, Integer>{
	
	
	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public InterviewTypeSetup findByIdAndIsDeleted(int id, boolean deleted);
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<InterviewTypeSetup> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from InterviewTypeSetup i where i.isDeleted=false")
	public Integer getCountOfTotalInterviewTypeSetup();
	
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<InterviewTypeSetup> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update InterviewTypeSetup d set d.isDeleted =:deleted, d.updatedBy=:updateById where d.id IN (:idList)")
	public void deleteInterviewTypeSetup(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update InterviewTypeSetup d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleInterviewTypeSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	/**
	 * 
	 * @return
	 */
	public InterviewTypeSetup findTop1ByOrderByInterviewTypeIdDesc();
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select i from InterviewTypeSetup i where (i.interviewTypeId like :searchKeyWord  or i.interviewTypeDescription like :searchKeyWord or i.interviewTypeDescriptionArabic like :searchKeyWord) and i.isDeleted=false")
	public List<InterviewTypeSetup> predictiveInterviewTypeSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<InterviewTypeSetup> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from InterviewTypeSetup i where (i.interviewTypeId like :searchKeyWord  or i.interviewTypeDescription like :searchKeyWord or i.interviewTypeDescriptionArabic like :searchKeyWord) and i.isDeleted=false")
	public Integer predictiveInterviewTypeSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select i from InterviewTypeSetup i where (i.interviewTypeId like :searchKeyWord  or i.interviewTypeDescription like :searchKeyWord or i.interviewTypeDescriptionArabic like :searchKeyWord) and i.isDeleted=false")
	public List<InterviewTypeSetup> predictiveInterviewTypeSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * 
	 * @param interviewTypeId
	 * @return
	 */
	@Query("select i from InterviewTypeSetup i where (i.interviewTypeId =:interviewTypeId) and i.isDeleted=false")
	public List<InterviewTypeSetup> findByInterviewTypeId(@Param("interviewTypeId")String interviewTypeId);
	
	@Query("select count(*) from InterviewTypeSetup i ")
	public Integer getCountOfTotalInterviewTypeSetups();
}
