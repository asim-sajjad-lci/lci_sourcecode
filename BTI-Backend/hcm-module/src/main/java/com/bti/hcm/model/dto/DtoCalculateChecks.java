package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.CalculateChecks;

public class DtoCalculateChecks extends DtoBase{

	private Integer id;
	private DtoDefault default_indx;
	private DtoBuildChecks buildCheck;
	private DtoFinancialDimensions dimensions;
	private String checkNumForPayRoll;
	private String auditTransNum;
	private Integer hCMCodeSequence;
	private DtoEmployeeMasterHcm employeeMaster;
	private DtoDepartment department;
	private DtoPosition position;
	private DtoLocation location;
	private short codeType;
	private Integer codeIndexID;
	private BigDecimal totalAmount;
	private BigDecimal totalActualAmount;
	private BigDecimal totalPercentCode;
	private Integer totalHours;
	private BigDecimal rateOfBaseOnCode;
	private Integer codeBaseOnCodeIndex;
	private Date codefromPerioddate;
	private Date codeToPerioddate;
	private short recordStatus;
	
	private List<DtoCalculateChecks> list;
	
	
	private List<DtoCalculateChecks> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCheckNumForPayRoll() {
		return checkNumForPayRoll;
	}
	public void setCheckNumForPayRoll(String checkNumForPayRoll) {
		this.checkNumForPayRoll = checkNumForPayRoll;
	}
	public String getAuditTransNum() {
		return auditTransNum;
	}
	public void setAuditTransNum(String auditTransNum) {
		this.auditTransNum = auditTransNum;
	}
	public Integer gethCMCodeSequence() {
		return hCMCodeSequence;
	}
	public void sethCMCodeSequence(Integer hCMCodeSequence) {
		this.hCMCodeSequence = hCMCodeSequence;
	}
	public DtoEmployeeMasterHcm getEmployeeMaster() {
		return employeeMaster;
	}
	public void setEmployeeMaster(DtoEmployeeMasterHcm employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	
	public DtoDepartment getDepartment() {
		return department;
	}
	public void setDepartment(DtoDepartment department) {
		this.department = department;
	}
	public DtoPosition getPosition() {
		return position;
	}
	public void setPosition(DtoPosition position) {
		this.position = position;
	}
	public DtoLocation getLocation() {
		return location;
	}
	public void setLocation(DtoLocation location) {
		this.location = location;
	}
	public short getCodeType() {
		return codeType;
	}
	public void setCodeType(short codeType) {
		this.codeType = codeType;
	}
	public Integer getCodeIndexID() {
		return codeIndexID;
	}
	public void setCodeIndexID(Integer codeIndexID) {
		this.codeIndexID = codeIndexID;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}
	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}
	public BigDecimal getTotalPercentCode() {
		return totalPercentCode;
	}
	public void setTotalPercentCode(BigDecimal totalPercentCode) {
		this.totalPercentCode = totalPercentCode;
	}
	public Integer getTotalHours() {
		return totalHours;
	}
	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}
	public BigDecimal getRateOfBaseOnCode() {
		return rateOfBaseOnCode;
	}
	public void setRateOfBaseOnCode(BigDecimal rateOfBaseOnCode) {
		this.rateOfBaseOnCode = rateOfBaseOnCode;
	}
	public Integer getCodeBaseOnCodeIndex() {
		return codeBaseOnCodeIndex;
	}
	public void setCodeBaseOnCodeIndex(Integer codeBaseOnCodeIndex) {
		this.codeBaseOnCodeIndex = codeBaseOnCodeIndex;
	}
	public Date getCodefromPerioddate() {
		return codefromPerioddate;
	}
	public void setCodefromPerioddate(Date codefromPerioddate) {
		this.codefromPerioddate = codefromPerioddate;
	}
	public Date getCodeToPerioddate() {
		return codeToPerioddate;
	}
	public void setCodeToPerioddate(Date codeToPerioddate) {
		this.codeToPerioddate = codeToPerioddate;
	}
	public short getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(short recordStatus) {
		this.recordStatus = recordStatus;
	}
	public List<DtoCalculateChecks> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoCalculateChecks> delete) {
		this.delete = delete;
	}
	
	public DtoDefault getDefault_indx() {
		return default_indx;
	}
	public void setDefault_indx(DtoDefault default_indx) {
		this.default_indx = default_indx;
	}
	public DtoBuildChecks getBuildCheck() {
		return buildCheck;
	}
	public void setBuildCheck(DtoBuildChecks buildCheck) {
		this.buildCheck = buildCheck;
	}
	public DtoFinancialDimensions getDimensions() {
		return dimensions;
	}
	public void setDimensions(DtoFinancialDimensions dimensions) {
		this.dimensions = dimensions;
	}
	
	
	public List<DtoCalculateChecks> getList() {
		return list;
	}
	public void setList(List<DtoCalculateChecks> list) {
		this.list = list;
	}
	public DtoCalculateChecks() {
		// TODO Auto-generated constructor stub
	}
	
	public DtoCalculateChecks(CalculateChecks calculateChecks) {
		// TODO Auto-generated constructor stub
	}
}
