package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeDependent;
import com.bti.hcm.model.EmployeeDependents;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.model.dto.DtoEmployeeDependent;
import com.bti.hcm.model.dto.DtoEmployeeDependents;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeDependents;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryHrCity;
import com.bti.hcm.repository.RepositoryHrCountry;
import com.bti.hcm.repository.RepositoryHrState;

@Service("/serviceEmployeeDependent")
public class ServiceEmployeeDependent {
		
	
	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceEmployeeDependent.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeDependents repositoryEmployeeDependents;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired
	RepositoryHrCountry repositoryHrCountry;
	
	@Autowired
	RepositoryHrState repositoryHrState;
	
	@Autowired
	RepositoryHrCity repositoryHrCity;


	
		
		public DtoEmployeeDependent saveOrUpdate(DtoEmployeeDependent dtoEmployeeDependent1) {
			try {
				log.info("saveOrUpdate EmployeeDpenedent Method");
				int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			if(dtoEmployeeDependent1.getListEmployeeDependent().get(0)!=null &&	dtoEmployeeDependent1.getListEmployeeDependent().get(0).getEmployeeMaster().getEmployeeIndexId()>0)
			{
				repositoryEmployeeDependent.deleteByEmployeeId(true, loggedInUserId,dtoEmployeeDependent1.getListEmployeeDependent().get(0).getEmployeeMaster().getEmployeeIndexId());
			}
				for (DtoEmployeeDependent dtoEmployeeDependent : dtoEmployeeDependent1.getListEmployeeDependent()) {
					EmployeeDependent employeeDependent = null;
					if (dtoEmployeeDependent.getId() != null && dtoEmployeeDependent.getId() > 0) {
						employeeDependent = repositoryEmployeeDependent.findByIdAndIsDeleted(dtoEmployeeDependent.getId(), false);
						employeeDependent.setUpdatedBy(loggedInUserId);
						employeeDependent.setUpdatedDate(new Date());
					} else {
						employeeDependent = new EmployeeDependent();
						employeeDependent.setCreatedDate(new Date());
						Integer rowId = repositoryEmployeeDependent.getCountOfTotalEmployeeDependent();
						Integer increment=0;
						if(rowId!=0) {
							increment= rowId+1;
						}else {
							increment=1;
						}
						employeeDependent.setRowId(increment);
					}
					employeeDependent = getEmployeeDependent(employeeDependent,dtoEmployeeDependent);
					
					employeeDependent = repositoryEmployeeDependent.saveAndFlush(employeeDependent);
					dtoEmployeeDependent.setId(employeeDependent.getId());
					log.debug("EmployeeDirectDeposit is:"+employeeDependent.getId());
				}
				
				
			} catch (Exception e) {
				log.error(e);
				
			}
			
			return dtoEmployeeDependent1;
		
		}
		
		public EmployeeDependent getEmployeeDependent(EmployeeDependent employeeDependent,DtoEmployeeDependent dtoEmployeeDependent) {
			EmployeeMaster employeeMaster=null;
			if(dtoEmployeeDependent.getEmployeeMaster()!=null && dtoEmployeeDependent.getEmployeeMaster().getEmployeeIndexId()>0) {
				employeeMaster=repositoryEmployeeMaster.findOne(dtoEmployeeDependent.getEmployeeMaster().getEmployeeIndexId());
			}
			
			EmployeeDependents employeeDependents=null;
			if(dtoEmployeeDependent.getEmployeeDependents()!=null && dtoEmployeeDependent.getEmployeeDependents().getId()>0) {
				employeeDependents=repositoryEmployeeDependents.findOne(dtoEmployeeDependent.getEmployeeDependents().getId());
			}
			HrCountry hrCountry = repositoryHrCountry.findOne(dtoEmployeeDependent.getCountryId());
			HrState hrState = repositoryHrState.findOne(dtoEmployeeDependent.getStateId());
			HrCity hrCity = repositoryHrCity.findOne(dtoEmployeeDependent.getCityId());
			employeeDependent.setEmployeeMaster(employeeMaster);
			employeeDependent.setEmployeeDependents(employeeDependents);
			employeeDependent.setSequence(dtoEmployeeDependent.getSequence());
			employeeDependent.setEmpFistName(dtoEmployeeDependent.getEmpFistName());
			employeeDependent.setEmpMidleName(dtoEmployeeDependent.getEmpMidleName());
			employeeDependent.setEmpLastName(dtoEmployeeDependent.getEmpLastName());
			employeeDependent.setComments(dtoEmployeeDependent.getComments());
			employeeDependent.setAge(dtoEmployeeDependent.getAge());
			employeeDependent.setGender(dtoEmployeeDependent.getGender());
			employeeDependent.setDateOfBirth(dtoEmployeeDependent.getDateOfBirth());
			employeeDependent.setPhoneNumber(dtoEmployeeDependent.getPhoneNumber());
			employeeDependent.setWorkNumber(dtoEmployeeDependent.getWorkNumber());
			employeeDependent.setAddress(dtoEmployeeDependent.getAddress());
			employeeDependent.setCity(hrCity);
			employeeDependent.setCountry(hrCountry);
			employeeDependent.setState(hrState);
			return employeeDependent;
		}
		
		
		
		
  //update 
		
		
		public DtoEmployeeDependent update(DtoEmployeeDependent dtoEmployeeDependent1) {
				
				try {
					int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
					for (DtoEmployeeDependent dtoEmployeeDependent : dtoEmployeeDependent1.getListEmployeeDependent()) {
						
						EmployeeDependent employeeDependent = null;
						
						if(dtoEmployeeDependent.getId()!=null && dtoEmployeeDependent.getId()>0) {
							employeeDependent = repositoryEmployeeDependent.findOne(dtoEmployeeDependent.getId());
						}else{
							employeeDependent = new EmployeeDependent();
							employeeDependent.setUpdatedBy(loggedInUserId);
							employeeDependent.setUpdatedDate(new Date());
						}
						
						
						employeeDependent = getEmployeeDependent(employeeDependent,dtoEmployeeDependent);
						
						employeeDependent = repositoryEmployeeDependent.saveAndFlush(employeeDependent);
						dtoEmployeeDependent.setId(employeeDependent.getId());
						log.debug("EmployeeDirectDeposit is:"+employeeDependent.getId());
					}
					
					
				} catch (Exception e) {
					log.error(e);
				}
				
				return dtoEmployeeDependent1;
			
			}


	public DtoEmployeeDependent deleteEmployeeDependent(List<Integer> ids) {
		log.info("delete EmployeeDependent Method");
		DtoEmployeeDependent dtoEmployeeDependent = new DtoEmployeeDependent();
		try {

			dtoEmployeeDependent.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_DELETED", false));
			dtoEmployeeDependent.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_ASSOCIATED", false));
			List<DtoEmployeeDependent> deleteEmployeeDependent = new ArrayList<>();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			
			boolean inValidDelete = false;
			StringBuilder  invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_DEPENDENT_NOT_DELETE_ID_ALREADY_IN USE", false).getMessage());
			
				for (Integer planId : ids) {
				EmployeeDependent employeeDependent = repositoryEmployeeDependent.findOne(planId);
				if(employeeDependent!=null && employeeDependent.getEmployeeMaster()==null) {
					DtoEmployeeDependent dtoEmployeeDependent2=new DtoEmployeeDependent();
					dtoEmployeeDependent.setId(employeeDependent.getId());
					dtoEmployeeDependent.setEmpFistName(employeeDependent.getEmpFistName());
					repositoryEmployeeDependent.deleteSingleEmployeeDependent(true, loggedInUserId, planId);
					deleteEmployeeDependent.add(dtoEmployeeDependent2);				
				}else {
					inValidDelete = true;
				}
			
		
		}

				if(inValidDelete){
					invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
					dtoEmployeeDependent.setMessageType(invlidDeleteMessage.toString());
					
				}
				if(!inValidDelete){
					dtoEmployeeDependent.setMessageType("");
					
				}
				
					
				dtoEmployeeDependent.setDelete(deleteEmployeeDependent);
			
			log.debug("Delete EmployeeDependent"
					+ " :"+dtoEmployeeDependent.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDependent;
	}





	public DtoEmployeeDependent getById(int id) {
		DtoEmployeeDependent dtoEmployeeDependent  = new DtoEmployeeDependent();
		try {

			
			if (id > 0) {
				EmployeeDependent employeeDependent = repositoryEmployeeDependent.findByIdAndIsDeleted(id, false);
				if (employeeDependent != null) {
					dtoEmployeeDependent = new DtoEmployeeDependent(employeeDependent);
					dtoEmployeeDependent = getDtoEmployeeDependent(employeeDependent,dtoEmployeeDependent);
				} 
			} else {
				dtoEmployeeDependent.setMessageType("INVALID_ID");

			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDependent;
	}	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchEmployeeDependent(DtoSearch dtoSearch) {
		
		try {

			log.info("searchEmployeeDependent Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("empFistName")|| dtoSearch.getSortOn().equals("empMidleName")|| dtoSearch.getSortOn().equals("empLastName")|| dtoSearch.getSortOn().equals("comments")
								|| dtoSearch.getSortOn().equals("phoneNumber")|| dtoSearch.getSortOn().equals("workNumber")|| dtoSearch.getSortOn().equals("address")|| dtoSearch.getSortOn().equals("city")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				
				dtoSearch.setTotalCount(this.repositoryEmployeeDependent.predictiveEmployeeDependentSearchTotalCount("%"+searchWord+"%"));
				List<EmployeeDependent> employeeDependentList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeeDependentList = this.repositoryEmployeeDependent.predictiveEmployeeDependentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeeDependentList = this.repositoryEmployeeDependent.predictiveEmployeeDependentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeeDependentList = this.repositoryEmployeeDependent.predictiveEmployeeDependentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(employeeDependentList != null && !employeeDependentList.isEmpty()){
					List<DtoEmployeeDependent> dtoEmployeeDependentList = new ArrayList<>();
					DtoEmployeeDependent dtoEmployeeDependent=null;
					for (EmployeeDependent employeeDependent : employeeDependentList) {
						dtoEmployeeDependent = new DtoEmployeeDependent(employeeDependent);
						
						DtoEmployeeDependents dependent = new DtoEmployeeDependents();
						if(employeeDependent.getEmployeeDependents()!=null) {
							dependent.setId(employeeDependent.getEmployeeDependents().getId());
							dependent.setEmpRelationshipId(employeeDependent.getEmployeeDependents().getEmpRelationshipId());
							dependent.setDesc(employeeDependent.getEmployeeDependents().getDesc());
							dependent.setArabicDesc(employeeDependent.getEmployeeDependents().getArabicDesc());
							dtoEmployeeDependent.setEmployeeDependents(dependent);
						}
						
						if(employeeDependent.getEmployeeMaster()!=null) {
							EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOne(employeeDependent.getEmployeeMaster().getEmployeeIndexId());
						DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
						
							dtoEmployeeMaster.setEmployeeIndexId(employeeMaster.getEmployeeIndexId());
							dtoEmployeeMaster.setEmployeeId(employeeMaster.getEmployeeId());
							dtoEmployeeMaster.setEmployeeBirthDate(employeeMaster.getEmployeeBirthDate());
							dtoEmployeeMaster.setEmployeeCitizen(employeeMaster.isEmployeeCitizen());
							dtoEmployeeMaster.setEmployeeFirstName(employeeMaster.getEmployeeFirstName());
							dtoEmployeeMaster.setEmployeeFirstNameArabic(employeeMaster.getEmployeeFirstNameArabic());
							dtoEmployeeMaster.setEmployeeGender(employeeMaster.getEmployeeGender());
							dtoEmployeeMaster.setEmployeeHireDate(employeeMaster.getEmployeeHireDate());
							dtoEmployeeMaster.setEmployeeImmigration(employeeMaster.isEmployeeImmigration());
							dtoEmployeeMaster.setEmployeeInactive(employeeMaster.isEmployeeInactive());
							dtoEmployeeMaster.setEmployeeInactiveDate(employeeMaster.getEmployeeInactiveDate());
							dtoEmployeeMaster.setEmployeeInactiveReason(employeeMaster.getEmployeeInactiveReason());
							dtoEmployeeMaster.setEmployeeLastName(employeeMaster.getEmployeeLastName());
							dtoEmployeeMaster.setEmployeeLastNameArabic(employeeMaster.getEmployeeLastNameArabic());
							dtoEmployeeMaster.setEmployeeLastWorkDate(employeeMaster.getEmployeeLastWorkDate());
							dtoEmployeeMaster.setEmployeeMaritalStatus(employeeMaster.getEmployeeMaritalStatus());
							dtoEmployeeMaster.setEmployeeMiddleName(employeeMaster.getEmployeeMiddleName());
							dtoEmployeeMaster.setEmployeeMiddleNameArabic(employeeMaster.getEmployeeMiddleNameArabic());
							dtoEmployeeMaster.setEmployeeTitle(employeeMaster.getEmployeeTitle());
							dtoEmployeeMaster.setEmployeeTitleArabic(employeeMaster.getEmployeeTitleArabic());
							dtoEmployeeMaster.setEmployeeType(employeeMaster.getEmployeeType());
							dtoEmployeeMaster.setEmployeeUserIdInSystem(employeeMaster.getEmployeeUserIdInSystem());
							dtoEmployeeMaster.setEmployeeWorkHourYearly(employeeMaster.getEmployeeWorkHourYearly());
							dtoEmployeeDependent.setEmployeeMaster(dtoEmployeeMaster);
					
						}
						
						dtoEmployeeDependent.setEmployeeDependents(dependent);
						dtoEmployeeDependent.setId(employeeDependent.getId());
						
						if(employeeDependent.getCity()!=null){
							dtoEmployeeDependent.setCityId(employeeDependent.getCity().getCityId());
							dtoEmployeeDependent.setCityName(employeeDependent.getCity().getCityName());
						}
						if(employeeDependent.getCountry()!=null){
							dtoEmployeeDependent.setCountryId(employeeDependent.getCountry().getCountryId());
							dtoEmployeeDependent.setCountryName(employeeDependent.getCountry().getCountryName());
						}
						
						if(employeeDependent.getState()!=null){
							dtoEmployeeDependent.setStateName(employeeDependent.getState().getStateName());
							dtoEmployeeDependent.setStateId(employeeDependent.getState().getStateId());
						}
						
						dtoEmployeeDependent.setSequence(employeeDependent.getSequence());
						dtoEmployeeDependent.setEmpFistName(employeeDependent.getEmpFistName());
						dtoEmployeeDependent.setEmpMidleName(employeeDependent.getEmpMidleName());
						dtoEmployeeDependent.setEmpLastName(employeeDependent.getEmpLastName());
						dtoEmployeeDependent.setComments(employeeDependent.getComments());
						dtoEmployeeDependent.setGender(employeeDependent.getGender());
						dtoEmployeeDependent.setDateOfBirth(employeeDependent.getDateOfBirth());
						dtoEmployeeDependent.setAge(employeeDependent.getAge());
						dtoEmployeeDependent.setPhoneNumber(employeeDependent.getPhoneNumber());
						dtoEmployeeDependent.setWorkNumber(employeeDependent.getWorkNumber());
						dtoEmployeeDependent.setAddress(employeeDependent.getAddress());
						dtoEmployeeDependentList.add(dtoEmployeeDependent);
						}
					dtoSearch.setRecords(dtoEmployeeDependentList);
						}
				
			
			
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	public List<DtoEmployeeDependent> employeeDependentsDropDownList() {
		log.info("getAllEmployeeDependent InActive List  Method");
		List<DtoEmployeeDependent> dtoEmployeeDependentList = new ArrayList<>();
		try {

			
			List<EmployeeDependent> list = repositoryEmployeeDependent.findByIsDeleted(false);
			
			if (list != null && !list.isEmpty()) {
				for (EmployeeDependent employeeDependent : list) {
					DtoEmployeeDependent dtoEmployeeDependent = new DtoEmployeeDependent();
					dtoEmployeeDependent = getDtoEmployeeDependent(employeeDependent,dtoEmployeeDependent);
					dtoEmployeeDependentList.add(dtoEmployeeDependent);
				}
			}
			log.debug("Position is:"+dtoEmployeeDependentList.size());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeDependentList;
	}

	public DtoEmployeeDependent getDtoEmployeeDependent(EmployeeDependent employeeDependent,DtoEmployeeDependent dtoEmployeeDependent) {
		
		dtoEmployeeDependent.setId(employeeDependent.getId());
		dtoEmployeeDependent.setSequence(employeeDependent.getSequence());
		dtoEmployeeDependent.setEmpFistName(employeeDependent.getEmpFistName());
		dtoEmployeeDependent.setEmpMidleName(employeeDependent.getEmpMidleName());
		dtoEmployeeDependent.setEmpLastName(employeeDependent.getEmpLastName());
		dtoEmployeeDependent.setComments(employeeDependent.getComments());
		dtoEmployeeDependent.setGender(employeeDependent.getGender());
		dtoEmployeeDependent.setDateOfBirth(employeeDependent.getDateOfBirth());
		dtoEmployeeDependent.setPhoneNumber(employeeDependent.getPhoneNumber());
		dtoEmployeeDependent.setWorkNumber(employeeDependent.getWorkNumber());
		dtoEmployeeDependent.setAddress(employeeDependent.getAddress());
		return dtoEmployeeDependent;
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getDependentByEmployeeId(DtoSearch dtoSearch) {
		log.info("search EmployeeDependent Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
			
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				if(dtoSearch.getSortOn().equals("employeeMaster") ||
						dtoSearch.getSortOn().equals("sequence") || 
						dtoSearch.getSortOn().equals("employeeDependents") ||
						dtoSearch.getSortOn().equals("empFistName") ||
						dtoSearch.getSortOn().equals("empMidleName") ||
						dtoSearch.getSortOn().equals("empLastName") ||
						dtoSearch.getSortOn().equals("comments") ||
						dtoSearch.getSortOn().equals("gender") ||
						dtoSearch.getSortOn().equals("phoneNumber") ||
						dtoSearch.getSortOn().equals("workNumber") ||
						dtoSearch.getSortOn().equals("address")
						) {
					condition = dtoSearch.getSortOn();
				
			}else {
				condition = "id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
			
		}
			dtoSearch.setTotalCount(this.repositoryEmployeeDependent.getDependentByEmployeeIdCount("%"+searchWord+"%",dtoSearch.getId()));
			List<EmployeeDependent> employeeDependentList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					employeeDependentList = this.repositoryEmployeeDependent.getDependentByEmployeeIdCount("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					employeeDependentList = this.repositoryEmployeeDependent.getDependentByEmployeeIdCount("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					employeeDependentList = this.repositoryEmployeeDependent.getDependentByEmployeeIdCount("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
		
			if(employeeDependentList != null && !employeeDependentList.isEmpty()){
				List<DtoEmployeeDependent> dtoEmployeeDependentList = new ArrayList<>();
				DtoEmployeeDependent dtoEmployeeDependent=null;
				for (EmployeeDependent employeeDependent : employeeDependentList) {
					dtoEmployeeDependent = new DtoEmployeeDependent(employeeDependent);
					if(employeeDependent.getEmployeeMaster()!=null) {
						
						DtoEmployeeMaster dtoEmployeeMaster = new DtoEmployeeMaster();
						dtoEmployeeDependent.setEmployeeMaster(dtoEmployeeMaster);
						dtoEmployeeDependent.getEmployeeMaster().setEmployeeIndexId(employeeDependent.getEmployeeMaster().getEmployeeIndexId());
						dtoEmployeeDependent.getEmployeeMaster().setEmployeeFirstName(employeeDependent.getEmployeeMaster().getEmployeeFirstName());
						dtoEmployeeDependent.getEmployeeMaster().setEmployeeFirstNameArabic(employeeDependent.getEmployeeMaster().getEmployeeFirstNameArabic());
						dtoEmployeeDependent.getEmployeeMaster().setEmployeeLastName(employeeDependent.getEmployeeMaster().getEmployeeLastName());
						dtoEmployeeDependent.getEmployeeMaster().setEmployeeLastNameArabic(employeeDependent.getEmployeeMaster().getEmployeeLastNameArabic());
			
					}
					if(employeeDependent.getEmployeeDependents()!=null) {
						DtoEmployeeDependents dtoEmployeeDependents = new DtoEmployeeDependents();
						dtoEmployeeDependent.setEmployeeDependents(dtoEmployeeDependents);
						dtoEmployeeDependent.getEmployeeDependents().setEmpRelationshipId(employeeDependent.getEmployeeDependents().getEmpRelationshipId());
						dtoEmployeeDependent.getEmployeeDependents().setDesc(employeeDependent.getEmployeeDependents().getDesc());
						dtoEmployeeDependent.getEmployeeDependents().setArabicDesc(employeeDependent.getEmployeeDependents().getArabicDesc());
						dtoEmployeeDependent.getEmployeeDependents().setId(employeeDependent.getEmployeeDependents().getId());
			
					}
					
					if(employeeDependent.getCity()!=null){
						dtoEmployeeDependent.setCityId(employeeDependent.getCity().getCityId());
						dtoEmployeeDependent.setCityName(employeeDependent.getCity().getCityName());
					}
					if(employeeDependent.getCountry()!=null){
						dtoEmployeeDependent.setCountryId(employeeDependent.getCountry().getCountryId());
						dtoEmployeeDependent.setCountryName(employeeDependent.getCountry().getCountryName());
					}
					
					if(employeeDependent.getState()!=null){
						dtoEmployeeDependent.setStateName(employeeDependent.getState().getStateName());
						dtoEmployeeDependent.setStateId(employeeDependent.getState().getStateId());
					}
					
					dtoEmployeeDependent.setSequence(employeeDependent.getSequence());
					dtoEmployeeDependent.setEmpFistName(employeeDependent.getEmpFistName());
					dtoEmployeeDependent.setEmpMidleName(employeeDependent.getEmpMidleName());
					dtoEmployeeDependent.setEmpLastName(employeeDependent.getEmpLastName());
					dtoEmployeeDependent.setComments(employeeDependent.getComments());
					dtoEmployeeDependent.setGender(employeeDependent.getGender());
					dtoEmployeeDependent.setDateOfBirth(employeeDependent.getDateOfBirth());
					dtoEmployeeDependent.setPhoneNumber(employeeDependent.getPhoneNumber());
					dtoEmployeeDependent.setWorkNumber(employeeDependent.getWorkNumber());
				    dtoEmployeeDependent.setCompleteName(employeeDependent.getEmpFistName()+" "+employeeDependent.getEmpMidleName()+" "+employeeDependent.getEmpLastName());
				    dtoEmployeeDependent.setAge(employeeDependent.getAge());
					
					dtoEmployeeDependent.setAddress(employeeDependent.getAddress());
					dtoEmployeeDependentList.add(dtoEmployeeDependent);
				}
				dtoSearch.setRecords(dtoEmployeeDependentList);
			}
		}
		return dtoSearch;
	}
	
	
	
	
}
