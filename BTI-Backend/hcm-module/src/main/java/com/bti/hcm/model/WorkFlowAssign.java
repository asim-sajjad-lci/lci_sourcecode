package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "WorkFlow_Assign")
public class WorkFlowAssign extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "work_flow_assign_id")
	private Integer id;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "work_flow_id")
	private WorkFlowUploadFile workFlowUploadFile;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id")
	private Company company;

	/*@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "work_flow_assign_join_user_table", joinColumns = {
			@JoinColumn(name = "work_flow_assign_id") }, inverseJoinColumns = { @JoinColumn(name = "user_id") })
	private List<User> userList;
*/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public WorkFlowUploadFile getWorkFlowUploadFile() {
		return workFlowUploadFile;
	}

	public void setWorkFlowUploadFile(WorkFlowUploadFile workFlowUploadFile) {
		this.workFlowUploadFile = workFlowUploadFile;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
