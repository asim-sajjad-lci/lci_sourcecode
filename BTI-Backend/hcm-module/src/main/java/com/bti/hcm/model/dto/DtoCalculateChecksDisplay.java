package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;



/**
 * @author Sheikh Nasir Saeed
 *
 */
public class DtoCalculateChecksDisplay extends DtoBase{

	private Integer id;
	private String defaultIndx;
	private String transactionType;
	private Integer defaultIndxPrimaryId;
	private String buildCheck;
	private Integer buildCheckPrimaryId;
	private String projectId;
	private int miscellaneousId;
	private String miscellaneousDescrption;
	private String miscellaneousArabicDescrption;
	private int miscellaneousEmployeeId;
	private Integer projectPrimaryId;
	private String checkNumForPayRoll;
	private String auditTransNum;
	private Integer hCMCodeSequence;
	private String employeeID;
	private String employeeId;
	private Integer employeePrimaryId;
	private String employeeName;
	private String departmentId;
	private String codeId;
	private Integer departmentPrimaryId;
	private short codeType;
	private Integer codeIndexID;
	private BigDecimal totalAmount;
	private BigDecimal totalActualAmount;
	private BigDecimal totalPercentCode;
	private Integer totalHours;
	private BigDecimal rateOfBaseOnCode;
	private Integer codeBaseOnCodeIndex;
	private Date codefromPerioddate;
	private Date codeToPerioddate;
	private short recordStatus;
	private Date buildDate;
	private Date postingDate;
	private Date periodFromDate;
	private Date periodToDate;
	private String buildtime;
	private String status;
	private Integer numberId;
	private BigDecimal benefitAmount;
	List<DtoBenefitCode> dtobenfitCode;
	List<DtoDeductionCode> dtoDeductionCodes;
	
	private BigDecimal baseOnPayCodeAmount;
	
	
	
	private DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance; 
	
	
	private Integer userId;

	private List<DtoCalculateChecks> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDefaultIndx() {
		return defaultIndx;
	}

	public void setDefaultIndx(String defaultIndx) {
		this.defaultIndx = defaultIndx;
	}

	public Integer getDefaultIndxPrimaryId() {
		return defaultIndxPrimaryId;
	}

	public void setDefaultIndxPrimaryId(Integer defaultIndxPrimaryId) {
		this.defaultIndxPrimaryId = defaultIndxPrimaryId;
	}

	public String getBuildCheck() {
		return buildCheck;
	}

	public void setBuildCheck(String buildCheck) {
		this.buildCheck = buildCheck;
	}

	public Integer getBuildCheckPrimaryId() {
		return buildCheckPrimaryId;
	}

	public void setBuildCheckPrimaryId(Integer buildCheckPrimaryId) {
		this.buildCheckPrimaryId = buildCheckPrimaryId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectPrimaryId() {
		return projectPrimaryId;
	}

	public void setProjectPrimaryId(Integer projectPrimaryId) {
		this.projectPrimaryId = projectPrimaryId;
	}

	public String getCheckNumForPayRoll() {
		return checkNumForPayRoll;
	}

	public void setCheckNumForPayRoll(String checkNumForPayRoll) {
		this.checkNumForPayRoll = checkNumForPayRoll;
	}

	public String getAuditTransNum() {
		return auditTransNum;
	}

	public void setAuditTransNum(String auditTransNum) {
		this.auditTransNum = auditTransNum;
	}

	public Integer gethCMCodeSequence() {
		return hCMCodeSequence;
	}

	public void sethCMCodeSequence(Integer hCMCodeSequence) {
		this.hCMCodeSequence = hCMCodeSequence;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public Integer getEmployeePrimaryId() {
		return employeePrimaryId;
	}

	public void setEmployeePrimaryId(Integer employeePrimaryId) {
		this.employeePrimaryId = employeePrimaryId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public Integer getDepartmentPrimaryId() {
		return departmentPrimaryId;
	}

	public void setDepartmentPrimaryId(Integer departmentPrimaryId) {
		this.departmentPrimaryId = departmentPrimaryId;
	}

	public short getCodeType() {
		return codeType;
	}

	public void setCodeType(short codeType) {
		this.codeType = codeType;
	}

	public Integer getCodeIndexID() {
		return codeIndexID;
	}

	public void setCodeIndexID(Integer codeIndexID) {
		this.codeIndexID = codeIndexID;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}

	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}

	public BigDecimal getTotalPercentCode() {
		return totalPercentCode;
	}

	public void setTotalPercentCode(BigDecimal totalPercentCode) {
		this.totalPercentCode = totalPercentCode;
	}

	public Integer getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public BigDecimal getRateOfBaseOnCode() {
		return rateOfBaseOnCode;
	}

	public void setRateOfBaseOnCode(BigDecimal rateOfBaseOnCode) {
		this.rateOfBaseOnCode = rateOfBaseOnCode;
	}

	public Integer getCodeBaseOnCodeIndex() {
		return codeBaseOnCodeIndex;
	}

	public void setCodeBaseOnCodeIndex(Integer codeBaseOnCodeIndex) {
		this.codeBaseOnCodeIndex = codeBaseOnCodeIndex;
	}

	public Date getCodefromPerioddate() {
		return codefromPerioddate;
	}

	public void setCodefromPerioddate(Date codefromPerioddate) {
		this.codefromPerioddate = codefromPerioddate;
	}

	public Date getCodeToPerioddate() {
		return codeToPerioddate;
	}

	public void setCodeToPerioddate(Date codeToPerioddate) {
		this.codeToPerioddate = codeToPerioddate;
	}

	public short getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(short recordStatus) {
		this.recordStatus = recordStatus;
	}


	public List<DtoCalculateChecks> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoCalculateChecks> delete) {
		this.delete = delete;
	}

	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}

	public String getBuildtime() {
		return buildtime;
	}

	public void setBuildtime(String buildtime) {
		this.buildtime = buildtime;
	}

	
	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public Date getPeriodFromDate() {
		return periodFromDate;
	}

	public void setPeriodFromDate(Date periodFromDate) {
		this.periodFromDate = periodFromDate;
	}

	public Date getPeriodToDate() {
		return periodToDate;
	}

	public void setPeriodToDate(Date periodToDate) {
		this.periodToDate = periodToDate;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public Integer getNumberId() {
		return numberId;
	}

	public void setNumberId(Integer numberId) {
		this.numberId = numberId;
	}



	public class SortByEmployee implements Comparator<DtoCalculateChecksDisplay>{
		@Override
		public int compare(DtoCalculateChecksDisplay o1, DtoCalculateChecksDisplay o2) {
			return o1.getEmployeeId().compareTo(o2.getEmployeeId());
		}
		
	}
	
	public class SortByDepartment implements Comparator<DtoCalculateChecksDisplay>{
		@Override
		public int compare(DtoCalculateChecksDisplay o1, DtoCalculateChecksDisplay o2) {
			return o1.getDepartmentId().compareTo(o2.getDepartmentId());
		}
		
	}
	
	public class SortByCodeId implements Comparator<DtoCalculateChecksDisplay>{
		@Override
		public int compare(DtoCalculateChecksDisplay o1, DtoCalculateChecksDisplay o2) {
			return o1.getCodeId().compareTo(o2.getCodeId());
		}
		
	}
	
	public class SortByProjectId implements Comparator<DtoCalculateChecksDisplay>{
		@Override
		public int compare(DtoCalculateChecksDisplay o1, DtoCalculateChecksDisplay o2) {
			return o1.getProjectId().compareTo(o2.getProjectId());
		}
		
	} 


	public DtoEmployeeBenefitMaintenance getDtoEmployeeBenefitMaintenance() {
		return dtoEmployeeBenefitMaintenance;
	}

	public void setDtoEmployeeBenefitMaintenance(DtoEmployeeBenefitMaintenance dtoEmployeeBenefitMaintenance) {
		this.dtoEmployeeBenefitMaintenance = dtoEmployeeBenefitMaintenance;
	}

	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public BigDecimal getBaseOnPayCodeAmount() {
		return baseOnPayCodeAmount;
	}

	public void setBaseOnPayCodeAmount(BigDecimal baseOnPayCodeAmount) {
		this.baseOnPayCodeAmount = baseOnPayCodeAmount;
	}

	public List<DtoBenefitCode> getDtobenfitCode() {
		return dtobenfitCode;
	}

	public void setDtobenfitCode(List<DtoBenefitCode> dtobenfitCode) {
		this.dtobenfitCode = dtobenfitCode;
	}

	public List<DtoDeductionCode> getDtoDeductionCodes() {
		return dtoDeductionCodes;
	}

	public void setDtoDeductionCodes(List<DtoDeductionCode> dtoDeductionCodes) {
		this.dtoDeductionCodes = dtoDeductionCodes;
	}

	public int getMiscellaneousEmployeeId() {
		return miscellaneousEmployeeId;
	}

	public void setMiscellaneousEmployeeId(int miscellaneousEmployeeId) {
		this.miscellaneousEmployeeId = miscellaneousEmployeeId;
	}

	public int getMiscellaneousId() {
		return miscellaneousId;
	}

	public void setMiscellaneousId(int miscellaneousId) {
		this.miscellaneousId = miscellaneousId;
	}

	public String getMiscellaneousDescrption() {
		return miscellaneousDescrption;
	}

	public void setMiscellaneousDescrption(String miscellaneousDescrption) {
		this.miscellaneousDescrption = miscellaneousDescrption;
	}

	public String getMiscellaneousArabicDescrption() {
		return miscellaneousArabicDescrption;
	}

	public void setMiscellaneousArabicDescrption(String miscellaneousArabicDescrption) {
		this.miscellaneousArabicDescrption = miscellaneousArabicDescrption;
	}
	
	
	
}


