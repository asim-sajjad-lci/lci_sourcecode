package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.AtteandaceOption;
import com.bti.hcm.util.UtilRandomKey;

public class DtoAtteandaceOption extends DtoBase{
	private Integer id;
	private Integer seqn;
	private Integer reasonIndx;
	private String reasonDesc;
	private Integer atteandanceTypeSeqn;
	private Integer atteandanceTypeIndx;
	private String atteandanceTypeDesc;
	private List<DtoAtteandaceOption> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSeqn() {
		return seqn;
	}
	public void setSeqn(Integer seqn) {
		this.seqn = seqn;
	}
	public Integer getReasonIndx() {
		return reasonIndx;
	}
	public void setReasonIndx(Integer reasonIndx) {
		this.reasonIndx = reasonIndx;
	}
	public String getReasonDesc() {
		return reasonDesc;
	}
	public void setReasonDesc(String reasonDesc) {
		this.reasonDesc = reasonDesc;
	}
	public Integer getAtteandanceTypeSeqn() {
		return atteandanceTypeSeqn;
	}
	public void setAtteandanceTypeSeqn(Integer atteandanceTypeSeqn) {
		this.atteandanceTypeSeqn = atteandanceTypeSeqn;
	}
	public Integer getAtteandanceTypeIndx() {
		return atteandanceTypeIndx;
	}
	public void setAtteandanceTypeIndx(Integer atteandanceTypeIndx) {
		this.atteandanceTypeIndx = atteandanceTypeIndx;
	}
	public String getAtteandanceTypeDesc() {
		return atteandanceTypeDesc;
	}
	public void setAtteandanceTypeDesc(String atteandanceTypeDesc) {
		this.atteandanceTypeDesc = atteandanceTypeDesc;
	}
	public List<DtoAtteandaceOption> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoAtteandaceOption> delete) {
		this.delete = delete;
	}
	
	public DtoAtteandaceOption() {
	}
	
	public DtoAtteandaceOption(AtteandaceOption atteandaceOption) {
		if (UtilRandomKey.isNotBlank(atteandaceOption.getReasonDesc())) {
			this.reasonDesc = atteandaceOption.getReasonDesc();
		} else {
			this.reasonDesc = "";
		}
		
		if (UtilRandomKey.isNotBlank(atteandaceOption.getAtteandanceTypeDesc())) {
			this.atteandanceTypeDesc = atteandaceOption.getAtteandanceTypeDesc();
		} else {
			this.atteandanceTypeDesc = "";
		}
	}
}
