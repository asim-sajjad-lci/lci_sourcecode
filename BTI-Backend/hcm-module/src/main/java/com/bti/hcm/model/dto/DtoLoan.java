package com.bti.hcm.model.dto;

import java.util.Date;
import java.util.List;

/**
 * @author HAMID
 */
public class DtoLoan extends DtoBase {

	private Integer loanId; // 1
	private String refId; // LN1001

	private Float loanAmt; // amountRequested // (float) //me
	private Float deductionPerMonthDefault; // 500 e.g.
	private Short numOfMonths; // short int

	private Date startDate;
	private Date endDate; // tobe calculated

	private long workflowReqId; // unknown Field , unclear yet

	private Date transactionDate;

	private Boolean isCompleted;
	private Boolean isPostpone;

	List<DtoLoanDetails> loanDetailsList;

	private Integer employeeIdx; // 1

	// private String employeeId; // String MAC1001

	/* ******* GetAll (EmployeeMaster Ref Fields) ******** */

	// refId autoGen (disabled in UI)
	// currentESB Unused now
	private String employeeName; // from EmployeeMaster
	private Date hiringDate; // from EmployeeMaster
	// position
	// site
	private String totalPackage;
	private String previousLoan;
	private String totalDeduction;
	private String totalRemainingPrevious;

	
	private float remainingAmt;
	// Current ESB
	private int pageNo;

	private int countPerPage;

	private int totalPages;

	public Integer getLoanId() {
		return loanId;
	}

	public Boolean getIsPostpone() {
		return isPostpone;
	}

	public void setIsPostpone(Boolean isPostpone) {
		this.isPostpone = isPostpone;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public Float getLoanAmt() {
		return loanAmt;
	}

	public void setLoanAmt(Float loanAmt) {
		this.loanAmt = loanAmt;
	}

	public Short getNumOfMonths() {
		return numOfMonths;
	}

	public void setNumOfMonths(Short numOfMonths) {
		this.numOfMonths = numOfMonths;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public long getWorkflowReqId() {
		return workflowReqId;
	}

	public void setWorkflowReqId(long workflowReqId) {
		this.workflowReqId = workflowReqId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Boolean getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	/*
	 * public String getEmployeeId() { return employeeId; }
	 * 
	 * public void setEmployeeId(String employeeId) { this.employeeId = employeeId;
	 * }
	 */
	public Integer getEmployeeIdx() {
		return employeeIdx;
	}

	public void setEmployeeIdx(Integer employeeIdx) {
		this.employeeIdx = employeeIdx;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Date getHiringDate() {
		return hiringDate;
	}

	public void setHiringDate(Date hiringDate) {
		this.hiringDate = hiringDate;
	}

	public String getTotalPackage() {
		return totalPackage;
	}

	public void setTotalPackage(String totalPackage) {
		this.totalPackage = totalPackage;
	}

	public String getPreviousLoan() {
		return previousLoan;
	}

	public void setPreviousLoan(String previousLoan) {
		this.previousLoan = previousLoan;
	}

	public String getTotalDeduction() {
		return totalDeduction;
	}

	public void setTotalDeduction(String totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	public String getTotalRemainingPrevious() {
		return totalRemainingPrevious;
	}

	public void setTotalRemainingPrevious(String totalRemainingPrevious) {
		this.totalRemainingPrevious = totalRemainingPrevious;
	}

	public float getRemainingAmt() {
		return remainingAmt;
	}

	public void setRemainingAmt(float remainingAmt) {
		this.remainingAmt = remainingAmt;
	}

	public Float getDeductionPerMonthDefault() {
		return deductionPerMonthDefault;
	}

	public void setDeductionPerMonthDefault(Float deductionPerMonthDefault) {
		this.deductionPerMonthDefault = deductionPerMonthDefault;
	}

	public List<DtoLoanDetails> getLoanDetailsList() {
		return loanDetailsList;
	}

	public void setLoanDetailsList(List<DtoLoanDetails> loanDetailsList) {
		this.loanDetailsList = loanDetailsList;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getCountPerPage() {
		return countPerPage;
	}

	public void setCountPerPage(int countPerPage) {
		this.countPerPage = countPerPage;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

}
