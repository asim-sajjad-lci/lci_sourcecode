package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeDependents;

@Repository("repositoryEmployeeDependents")
public interface RepositoryEmployeeDependents extends JpaRepository<EmployeeDependents, Integer>{

	EmployeeDependents findByIdAndIsDeleted(Integer id, boolean b);

	@Query("select e from EmployeeDependents e where (e.empRelationshipId like :searchKeyWord) and e.isDeleted=false order by e.id desc")
	List<EmployeeDependents> predictiveSearchgetAllDroupDownWithPagination(@Param("searchKeyWord")String empRelationshipId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeDependents e set e.isDeleted =:deleted ,e.updatedBy =:updateById where e.id =:id ")
	public void deleteSingleEmployeeDependents(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	@Query("select count(*) from EmployeeDependents e where (e.empRelationshipId LIKE :searchKeyWord or e.desc LIKE :searchKeyWord or e.arabicDesc LIKE :searchKeyWord) and e.isDeleted=false")
	Integer predictiveEmployeeDependentsSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	@Query("select e from EmployeeDependents e where (e.empRelationshipId LIKE :searchKeyWord or e.desc LIKE :searchKeyWord or e.arabicDesc LIKE :searchKeyWord) and e.isDeleted=false")
	List<EmployeeDependents> predictiveEmployeeDependentsSearchWithPagination(@Param("searchKeyWord")String searchKeyWord,Pageable pageable);

	List<EmployeeDependents> findByIsDeleted(boolean b);

	@Query("select count(*) from EmployeeDependents e ")
	Integer getCountOfTotalEmployeeDependents();

}
