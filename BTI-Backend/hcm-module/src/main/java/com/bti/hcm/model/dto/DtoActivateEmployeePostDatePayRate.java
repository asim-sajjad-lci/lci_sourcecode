package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bti.hcm.model.ActivateEmployeePostDatePayRate;

public class DtoActivateEmployeePostDatePayRate extends DtoBase{

	private Integer id;
	private Integer activePostDatedSeq;
	private Date effectiveDate;
	private BigDecimal currentRate;
	private BigDecimal newRate;
	private Boolean active;

	private String ranges;

	private DtoEmployeeMaster employeeMaster;
	private DtoPayCode payCode;


	private DtoEmployeeMasterHcm employeeMasterHcm;

	private List<DtoActivateEmployeePostDatePayRate> listActivateEmployeePostDatePayRate;

	
	private Date fromDate;
	private Date toDate;

	private Integer fromemployeeIndexId;
	private Integer toemployeeIndexId;

	private Integer fromPayCodeId;
	private Integer toPayCodeId;

	private Integer fromReasion;
	private Integer toReasion;

	private List<DtoActivateEmployeePostDatePayRate> delete;

	public DtoActivateEmployeePostDatePayRate(ActivateEmployeePostDatePayRate activateEmployeePostDatePayRate) {

	}

	public DtoActivateEmployeePostDatePayRate() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActivePostDatedSeq() {
		return activePostDatedSeq;
	}

	public void setActivePostDatedSeq(Integer activePostDatedSeq) {
		this.activePostDatedSeq = activePostDatedSeq;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public BigDecimal getCurrentRate() {
		return currentRate;
	}

	public void setCurrentRate(BigDecimal currentRate) {
		this.currentRate = currentRate;
	}

	public BigDecimal getNewRate() {
		return newRate;
	}

	public void setNewRate(BigDecimal newRate) {
		this.newRate = newRate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}


	public List<DtoActivateEmployeePostDatePayRate> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoActivateEmployeePostDatePayRate> delete) {
		this.delete = delete;
	}


	public DtoEmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(DtoEmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public DtoPayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(DtoPayCode payCode) {
		this.payCode = payCode;
	}

	public String getRanges() {
		return ranges;
	}

	public void setRanges(String ranges) {
		this.ranges = ranges;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public DtoEmployeeMasterHcm getEmployeeMasterHcm() {
		return employeeMasterHcm;
	}

	public void setEmployeeMasterHcm(DtoEmployeeMasterHcm employeeMasterHcm) {
		this.employeeMasterHcm = employeeMasterHcm;
	}

	public Integer getFromemployeeIndexId() {
		return fromemployeeIndexId;
	}

	public void setFromemployeeIndexId(Integer fromemployeeIndexId) {
		this.fromemployeeIndexId = fromemployeeIndexId;
	}

	public Integer getToemployeeIndexId() {
		return toemployeeIndexId;
	}

	public void setToemployeeIndexId(Integer toemployeeIndexId) {
		this.toemployeeIndexId = toemployeeIndexId;
	}

	public Integer getFromPayCodeId() {
		return fromPayCodeId;
	}

	public void setFromPayCodeId(Integer fromPayCodeId) {
		this.fromPayCodeId = fromPayCodeId;
	}

	public Integer getToPayCodeId() {
		return toPayCodeId;
	}

	public void setToPayCodeId(Integer toPayCodeId) {
		this.toPayCodeId = toPayCodeId;
	}

	public Integer getFromReasion() {
		return fromReasion;
	}

	public void setFromReasion(Integer fromReasion) {
		this.fromReasion = fromReasion;
	}

	public Integer getToReasion() {
		return toReasion;
	}

	public void setToReasion(Integer toReasion) {
		this.toReasion = toReasion;
	}

	public List<DtoActivateEmployeePostDatePayRate> getListActivateEmployeePostDatePayRate() {
		return listActivateEmployeePostDatePayRate;
	}

	public void setListActivateEmployeePostDatePayRate(
			List<DtoActivateEmployeePostDatePayRate> listActivateEmployeePostDatePayRate) {
		this.listActivateEmployeePostDatePayRate = listActivateEmployeePostDatePayRate;
	}


}
