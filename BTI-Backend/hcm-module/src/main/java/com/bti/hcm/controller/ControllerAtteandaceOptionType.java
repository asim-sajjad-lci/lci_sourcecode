/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
/**
 * Description: Controller AtteandaceOptionType
 * Name of Project: Hcm
 * Version: 0.0.1
 */
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoAttedanceOptionType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceAtteandaceOptionType;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/atteandaceOptionType")
public class ControllerAtteandaceOptionType extends BaseController{
	
	
	/**
	 * @Description LOGGER use for put a logger in AtteandaceOptionType Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerAtteandaceOptionType.class);
	
	/**
	 * @Description serviceAtteandaceOptionType Autowired here using annotation of spring for use of serviceAtteandaceOptionType method in this controller
	 */
	@Autowired(required=true)
	ServiceAtteandaceOptionType serviceAtteandaceOptionType;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * 
	 * @param request
	 * @param dtoAttedanceOptionType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createAtteandaceOptionType(HttpServletRequest request, @RequestBody DtoAttedanceOptionType dtoAttedanceOptionType) throws Exception {
		LOGGER.info("Create AtteandaceOptionType Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAttedanceOptionType = serviceAtteandaceOptionType.saveOrUpdate(dtoAttedanceOptionType);
			responseMessage=displayMessage(dtoAttedanceOptionType, "ATTEDANCE_OPTION_TYPE_CREATED", "ATTEDANCE_OPTION_TYPE_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create AtteandaceOptionType Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoAttedanceOptionType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoAttedanceOptionType dtoAttedanceOptionType) throws Exception {
		LOGGER.info("Update AtteandaceOptionType Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoAttedanceOptionType = serviceAtteandaceOptionType.saveOrUpdate(dtoAttedanceOptionType);
			responseMessage=displayMessage(dtoAttedanceOptionType, "ATTEDANCE_OPTION_TYPE_UPDATED_SUCCESS", "ATTEDANCE_OPTION_TYPE_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update AtteandaceOptionType Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * 
	 * @param request
	 * @param dtoAttedanceOptionType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoAttedanceOptionType dtoAttedanceOptionType) throws Exception {
		LOGGER.info("Delete AtteandaceOptionType Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoAttedanceOptionType.getIds() != null && !dtoAttedanceOptionType.getIds().isEmpty()) {
				DtoAttedanceOptionType dtoAttedanceOptionType2 = serviceAtteandaceOptionType.delete(dtoAttedanceOptionType.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("ATTEDANCE_OPTION_TYPE_DELETED", false), dtoAttedanceOptionType2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete AtteandaceOptionType Method:"+responseMessage.getMessage());
		return responseMessage;
	}

	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search AtteandaceOptionType Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceAtteandaceOptionType.searchAtteandaceOptionType(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "ATTEDANCE_OPTION_TYPE_GET_ALL", "ATTEDANCE_OPTION_TYPE_NOT_GETTING", serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAttedanceOptionTypeById", method = RequestMethod.POST)
	public ResponseMessage getAttedanceOptionTypeById(HttpServletRequest request, @RequestBody DtoAttedanceOptionType dtoAttedanceOptionType) throws Exception {
		LOGGER.info("Get AtteandaceOptionType ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoAttedanceOptionType dtoAttedanceOptionTypeObj = serviceAtteandaceOptionType.getDtoAttedanceOptionTypeById(dtoAttedanceOptionType.getId());
			responseMessage=displayMessage(dtoAttedanceOptionTypeObj, "ATTEDANCE_OPTION_TYPE_GET_ALL", "ATTEDANCE_OPTION_TYPE_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get AttedanceOptionType ById Method:"+dtoAttedanceOptionType.getId());
		return responseMessage;
	}
}
