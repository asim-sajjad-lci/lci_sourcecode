/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.BtiMessageHcm;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.repository.RepositoryException;


/**
 * Description: Service Response
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

@Service("ServiceResponse")
public class ServiceResponse {

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceResponse service
	 */
	static Logger log = Logger.getLogger(ServiceResponse.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceResponse service
	 */
	@Autowired(required = false)
	RepositoryException repositoryException;

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceResponse service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @param message
	 * @param b
	 * @return
	 */
	public DtoBtiMessageHcm getMessageByShortAndIsDeleted(String message, boolean b) {
		String langId = httpServletRequest.getHeader("langId");
		if (langId == null || langId.isEmpty()) {
			langId = "1";
		}
		BtiMessageHcm exceptionMessage = repositoryException.findByMessageShortAndIsDeletedAndLanguageLanguageId(message, false,Integer.parseInt(langId));
		return new DtoBtiMessageHcm(exceptionMessage, langId);
	}

	/**
	 * @param message
	 * @param b
	 * @return
	 */
	public String getStringMessageByShortAndIsDeleted(String message, boolean b) 
	{
		String responseMessage = "";
		String langId = httpServletRequest.getHeader("langId");
		BtiMessageHcm exceptionMessage = repositoryException.findByMessageShortAndIsDeletedAndLanguageLanguageId(message, false,Integer.parseInt(langId));
		if(exceptionMessage!=null)
		{
			responseMessage = exceptionMessage.getMessage();
		}
		else
		{
			responseMessage="N/A";
		}
		return responseMessage;
	}
	

}
