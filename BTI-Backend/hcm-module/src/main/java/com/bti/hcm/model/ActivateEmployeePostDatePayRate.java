package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10500",indexes = {
        @Index(columnList = "HCMACTINDX")
})
public class ActivateEmployeePostDatePayRate extends HcmBaseEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMACTINDX")
	private Integer id;

	@Column(name = "HCMACTSEQN")
	private Integer activePostDatedSeq;

	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;

	@ManyToOne
	@JoinColumn(name = "PYCDINDX")
	@Where(clause = "PYCDINCTV = false and is_deleted = false")
	private PayCode payCode;

	@Column(name = "ACTEFFTDT")
	private Date effectiveDate;

	@Column(name = "PYCDNCUR", precision = 10, scale = 3)
	private BigDecimal currentRate;

	@Column(name = "PYCDNRAT", precision = 10, scale = 3)
	private BigDecimal newRate;

	@Column(name = "PYCDNACT")
	private Boolean active;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActivePostDatedSeq() {
		return activePostDatedSeq;
	}

	public void setActivePostDatedSeq(Integer activePostDatedSeq) {
		this.activePostDatedSeq = activePostDatedSeq;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public BigDecimal getCurrentRate() {
		return currentRate;
	}

	public void setCurrentRate(BigDecimal currentRate) {
		this.currentRate = currentRate;
	}

	public BigDecimal getNewRate() {
		return newRate;
	}

	public void setNewRate(BigDecimal newRate) {
		this.newRate = newRate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public PayCode getPayCode() {
		return payCode;
	}

	public void setPayCode(PayCode payCode) {
		this.payCode = payCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((activePostDatedSeq == null) ? 0 : activePostDatedSeq.hashCode());
		result = prime * result + ((currentRate == null) ? 0 : currentRate.hashCode());
		result = prime * result + ((effectiveDate == null) ? 0 : effectiveDate.hashCode());
		result = prime * result + ((employeeMaster == null) ? 0 : employeeMaster.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((newRate == null) ? 0 : newRate.hashCode());
		result = prime * result + ((payCode == null) ? 0 : payCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
