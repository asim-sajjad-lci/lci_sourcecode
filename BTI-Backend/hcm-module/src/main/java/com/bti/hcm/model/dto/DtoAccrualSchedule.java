package com.bti.hcm.model.dto;

import java.sql.Date;
import java.util.List;

import com.bti.hcm.model.AccrualSchedule;
import com.bti.hcm.util.UtilRandomKey;

public class DtoAccrualSchedule extends DtoBase{

	private String descriptions;

	private Integer seniority;
	private String description;
	private Integer amounts;
	private Integer accrualTyepId;
	private Integer accrualSetupId;
	private List<DtoAccrualScheduleDetail> subItems;
	private List<DtoTimeCode> timeCodeList;
	private Integer id;
	private String scheduleId;
	private String desc;
	private String arbicDesc;
	private Date startDate;
	private Date endDate;
	private Integer scheduleSequence;
	private Integer scheduleSeniority;
	private Integer hours;
	private Integer accrualScheduleDetailId;
	private List<DtoAccrualSchedule> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArbicDesc() {
		return arbicDesc;
	}

	public void setArbicDesc(String arbicDesc) {
		this.arbicDesc = arbicDesc;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	
	public List<DtoAccrualSchedule> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoAccrualSchedule> delete) {
		this.delete = delete;
	}


	public Integer getScheduleSequence() {
		return scheduleSequence;
	}

	public void setScheduleSequence(Integer scheduleSequence) {
		this.scheduleSequence = scheduleSequence;
	}

	public Integer getScheduleSeniority() {
		return scheduleSeniority;
	}

	public void setScheduleSeniority(Integer scheduleSeniority) {
		this.scheduleSeniority = scheduleSeniority;
	}

	public Integer getSeniority() {
		return seniority;
	}

	public void setSeniority(Integer seniority) {
		this.seniority = seniority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public Integer getAccrualScheduleDetailId() {
		return accrualScheduleDetailId;
	}

	public void setAccrualScheduleDetailId(Integer accrualScheduleDetailId) {
		this.accrualScheduleDetailId = accrualScheduleDetailId;
	}

	public Integer getAmounts() {
		return amounts;
	}

	public void setAmounts(Integer amounts) {
		this.amounts = amounts;
	}

	public Integer getAccrualTyepId() {
		return accrualTyepId;
	}

	public void setAccrualTyepId(Integer accrualTyepId) {
		this.accrualTyepId = accrualTyepId;
	}

	public Integer getAccrualSetupId() {
		return accrualSetupId;
	}

	public void setAccrualSetupId(Integer accrualSetupId) {
		this.accrualSetupId = accrualSetupId;
	}

	public List<DtoAccrualScheduleDetail> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<DtoAccrualScheduleDetail> subItems) {
		this.subItems = subItems;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	public DtoAccrualSchedule(AccrualSchedule accrualSchedule) {
		this.scheduleId = accrualSchedule.getScheduleId();

		if (UtilRandomKey.isNotBlank(accrualSchedule.getDesc())) {
			this.desc = accrualSchedule.getDesc();
		} else {
			this.desc = "";
		}

		if (UtilRandomKey.isNotBlank(accrualSchedule.getArbicDesc())) {
			this.arbicDesc = accrualSchedule.getArbicDesc();
		} else {
			this.arbicDesc = "";
		}

	}

	public DtoAccrualSchedule() {
	}

	public List<DtoTimeCode> getTimeCodeList() {
		return timeCodeList;
	}

	public void setTimeCodeList(List<DtoTimeCode> timeCodeList) {
		this.timeCodeList = timeCodeList;
	}

}
