package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.RetirementEntranceDate;

public class DtoRetirementEntranceDate extends DtoBase{
	private Integer id;
	private Integer dateSequence;
	private Integer planId;
	private String entranceDescription;
	private List<DtoRetirementEntranceDate> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDateSequence() {
		return dateSequence;
	}

	public void setDateSequence(Integer dateSequence) {
		this.dateSequence = dateSequence;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getEntranceDescription() {
		return entranceDescription;
	}

	public void setEntranceDescription(String entranceDescription) {
		this.entranceDescription = entranceDescription;
	}
	public List<DtoRetirementEntranceDate> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoRetirementEntranceDate> delete) {
		this.delete = delete;
	}
	public DtoRetirementEntranceDate(RetirementEntranceDate retirementEntranceDate) {
		this.entranceDescription = retirementEntranceDate.getEntranceDescription();

	}

	public DtoRetirementEntranceDate() {

	}

}
