package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Hijri year class having getter and setter for fields (POJO) Name
 * Name of Project:Hcm 
 * Version: 0.0.1 
 * Created on: February 12, 2018
 * 
 * @author Bansi
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoHijriYear extends DtoHijri{

	private Integer year;


	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}


}
