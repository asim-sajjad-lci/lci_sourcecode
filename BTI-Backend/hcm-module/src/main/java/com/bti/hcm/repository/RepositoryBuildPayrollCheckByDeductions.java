package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.BuildPayrollCheckByDeductions;

@Repository("/repositoryBuildPayrollCheckByDeductions")
public interface RepositoryBuildPayrollCheckByDeductions extends JpaRepository<BuildPayrollCheckByDeductions, Integer>{

	@Query("select m from BuildPayrollCheckByDeductions m where m.buildChecks.id =:id and m.isDeleted=false")
	List<BuildPayrollCheckByDeductions> findByBuildChecksId(@Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BuildPayrollCheckByDeductions m set m.isDeleted =:deleted ,m.updatedBy =:updateById where m.buildChecks.id =:id ")
	public void deleteByBuildCheckId(@Param("deleted")boolean b, @Param("updateById")int loggedInUserId,@Param("id")Integer id);

}
