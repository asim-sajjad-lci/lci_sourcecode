package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.ProjectSetup;

@Repository("repositoryProjectSetup")

public interface RepositoryProjectSetup extends JpaRepository<ProjectSetup, Integer>{

	public ProjectSetup findByIdAndIsDeleted(int id, boolean deleted);
	
	public ProjectSetup findByProjectIdAndIsDeleted(String id, boolean deleted);
	
	public List<ProjectSetup> findByIsDeleted(Boolean deleted);
	
	
	@Query("select count(*) from ProjectSetup p where p.isDeleted=false")
	public Integer getCountOfTotalProjectSetup();
	
	public List<ProjectSetup> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ProjectSetup p set p.isDeleted =:deleted, p.updatedBy=:updateById where p.id IN (:idList)")
	public void deleteProjectSetup(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ProjectSetup p set p.isDeleted =:deleted ,p.updatedBy =:updateById where p.id =:id ")
	public void deleteSingleProjectSetup(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
	
	public ProjectSetup findTop1ByOrderByProjectIdDesc();
	
	
	
	@Query("select p from ProjectSetup p where (p.projectId like :searchKeyWord  or p.projectName like :searchKeyWord or p.projectArabicName like :searchKeyWord or p.projectDescription like :searchKeyWord or p.projectArabicDescription like :searchKeyWord) and p.isDeleted=false")
	public List<ProjectSetup> predictiveProjectSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	
	
	public List<ProjectSetup> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	
	
	@Query("select count(*) from ProjectSetup p where (p.projectId like :searchKeyWord  or p.projectName like :searchKeyWord or p.projectArabicName like :searchKeyWord or p.projectDescription like :searchKeyWord or p.projectArabicDescription like :searchKeyWord) and p.isDeleted=false")
	public Integer predictiveProjectSetupSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query("select p from ProjectSetup p where (p.projectId like :searchKeyWord  or p.projectName like :searchKeyWord or p.projectArabicName like :searchKeyWord or p.projectDescription like :searchKeyWord or p.projectArabicDescription like :searchKeyWord) and p.isDeleted=false")
	public List<ProjectSetup> predictiveProjectSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query("select p from ProjectSetup p where (p.projectId =:projectId) and p.isDeleted=false")
	public List<ProjectSetup> findByProjectId(@Param("projectId")String projectId);
	
	
	@Query("select p.projectId from ProjectSetup p where (p.projectId like :searchKeyWord) and p.isDeleted=false order by p.id desc")
	public List<String> predictiveProjectIdSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query("select p from ProjectSetup p where (p.projectId like :searchKeyWord) and p.isDeleted=false")
	public List<ProjectSetup> predictiveProjectSetupAllIdsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,Pageable pageable);
	
	
	@Query("select count(*) from ProjectSetup d ")
	public Integer getCountOfTotalProjectSetups();
	
	
	@Query("select count(*) from Department d")
	public Integer getCountOfTotalProjectSetupWithOutDelete();
	
	@Query("select p from ProjectSetup p where id in(:list) and p.isDeleted=false")
	public List<ProjectSetup> findAllProjectSetupListId(@Param("list") List<Integer> ls);
}
