package com.bti.hcm.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.dto.DtoEmployeePayrollDetails;
import com.bti.hcm.util.CommonUtils;

@Repository("repositoryEntityManager")
public class RepositoryEntityManager {

	@Autowired
	EntityManager entityManager;
	
//	public DtoTrailBalanceReport getTrialBalanceReport(Date startDate, Date endDate) {
//		DtoTrailBalanceReport dtoTrailBalanceReport = new DtoTrailBalanceReport();
//		List<DtoTrailBalanceReport> dtoTrailBalanceReportList = new ArrayList<>();
//		Query q = entityManager.createNativeQuery("select account_number as ACCOUNT_NUMBER, \r\n" + 
//				"		account_type as ACCOUNT_TYPE,\r\n" + 
//				"		main_account_description as ACCOUNT_DESCRIPTION,\r\n" + 
//				"		main_account_description_arabic as ACCOUNT_DESCRIPTION_ARABIC,\r\n" + 
//				"		prev_bal as PREVIOUS_BALANCE, \r\n" + 
//				"		debit as DEBIT_AMOUNT, \r\n" + 
//				"		credit as CREDIT_AMOUNT, \r\n" + 
//				"		net_charge as NET_CHARGE, \r\n" + 
//				"		(prev_bal + net_charge)  as ENDING_BALANCE\r\n" + 
//				"from (\r\n" + 
//				"select \r\n" + 
//				"account_number, account_type, main_account_description, main_account_description_arabic,\r\n" + 
//				"sum( (case when (transaction_date < ?) then (debit_amount - credit_amount) else 0 end)) AS prev_bal,\r\n" + 
//				"sum( (case when (transaction_date between ? and ?) then (debit_amount) else 0 end)) as debit, \r\n" + 
//				"sum( (case when (transaction_date between ? and ?) then (credit_amount) else 0 end)) as credit,\r\n" + 
//				"sum( (case when (transaction_date between ? and ?) then (debit_amount - credit_amount) else 0 end )) as net_charge\r\n" + 
//				"from trl_blnc_n_act_stmt_rpts_v \r\n" + 
//				"group by account_number, account_type\r\n" + 
//				") as a");
//		q.setParameter(1, startDate);
//		q.setParameter(2, startDate);
//		q.setParameter(3, endDate);
//		q.setParameter(4, startDate);
//		q.setParameter(5, endDate);
//		q.setParameter(6, startDate);
//		q.setParameter(7, endDate);
//		
//		List<Object[]> list =  q.getResultList();
//		 
//		int totalAccounts=0;
//
//		if (list != null && !list.isEmpty()) 
//			{
//		    	totalAccounts=list.size();
////				for(TrialBalanceView trialBalanceView : list){
//		    	for (int i=0; i < list.size(); i++) {
//					DtoTrailBalanceReport dtoReport = new DtoTrailBalanceReport();
//					Object[] result = list.get(i);
//					
//					dtoReport.setAccountNumber(result[0].toString());
//					dtoReport.setAccountDescription(result[2].toString());
//					dtoReport.setAccountDescriptionArabic(result[3].toString());
//
//					dtoReport.setBeginningBalance(Double.valueOf(result[4].toString()));
//					dtoReport.setDebit(Double.valueOf(result[5].toString()));
//					dtoReport.setCredit(Double.valueOf(result[6].toString()));
//					dtoReport.setNetChange(Double.valueOf(result[7].toString()));
//					dtoReport.setEndingBalance(Double.valueOf(result[8].toString()));
//					dtoTrailBalanceReportList.add(dtoReport);
//				}
//			}
//
//		q = entityManager.createNativeQuery("select \r\n" + 
//				"sum(prev_bal) as PREVIOUS_BALANCE, \r\n" + 
//				"sum(debit) as TOTAL_DEBIT_AMOUNT, \r\n" + 
//				"sum(credit) as TOTAL_CREDIT_AMOUNT, \r\n" + 
//				"sum(net_charge) as TOTAL_NET_CHARGE, \r\n" + 
//				"sum(ending_balance) as TOTAL_ENDING_BALANCE\r\n" + 
//				"from (\r\n" + 
//				"select accountnumber, acttyp, prev_bal, debit, credit, net_charge, \r\n" + 
//				"(prev_bal + net_charge) as ending_balance\r\n" + 
//				"from (\r\n" + 
//				"select \r\n" + 
//				"accountnumber, acttyp,\r\n" + 
//				"sum( (case when (trxddt < ?) then (debitamt-crdtamt) else 0 end)) AS Prev_Bal,\r\n" + 
//				"sum( (case when (trxddt between ? and ?) then (debitamt) else 0 end)) as Debit, \r\n" + 
//				"sum( (case when (trxddt between ? and ?) then (crdtamt) else 0 end)) as Credit,\r\n" + 
//				"sum( (case when (trxddt between ? and ?) then (debitamt - crdtamt) else 0 end)) as Net_Charge\r\n" + 
//				"from accountsdetailsview\r\n" + 
//				"group by accountnumber, acttyp\r\n" + 
//				") as a\r\n" + 
//				") as b");
//		q.setParameter(1, startDate);
//		q.setParameter(2, startDate);
//		q.setParameter(3, endDate);
//		q.setParameter(4, startDate);
//		q.setParameter(5, endDate);
//		q.setParameter(6, startDate);
//		q.setParameter(7, endDate);
//		
//		Object[] result =  (Object[]) q.getSingleResult();
//
//		dtoTrailBalanceReport.setTrailBalanceReportList(dtoTrailBalanceReportList);
//		dtoTrailBalanceReport.setTotalAccounts(totalAccounts);
//		dtoTrailBalanceReport.setTotalBeginningBalance(Double.valueOf(result[0].toString()));
//		dtoTrailBalanceReport.setTotalDebit(Double.valueOf(result[1].toString()));
//		dtoTrailBalanceReport.setTotalCredit(Double.valueOf(result[2].toString()));
//		dtoTrailBalanceReport.setTotalNetChange(Double.valueOf(result[3].toString()));
//		dtoTrailBalanceReport.setTotalEndingBalance(Double.valueOf(result[4].toString()));
//
//		return dtoTrailBalanceReport;	
//
//	}

	public Object[] getPayrollReportTopRow(int payRollId){

		Query q = entityManager.createNativeQuery("select *\r\n" + 
				"from hr40106\r\n" + 
				"where pyrl_id = ?");
		payRollId = 1; // hard coded for test run
		q.setParameter(1, payRollId);
		
		List<Object[]> list =  q.getResultList();
		
		if (list != null && !list.isEmpty()) {
			return list.get(0);
		} else {
			return new Object[0];
		}
	}
	
	
	public void populateEmployeeAccountsMapForPayroll(String employeeIds, HashMap<String, DtoEmployeePayrollDetails> map) {
		
		Query query = entityManager.createNativeQuery("select emp.employid as `emp_id`, \r\n" + 
				"CONCAT(emp.empfrtnm, ' ', emp.emplstnm) as `EMP_NAME`,\r\n" + 
				"eb.emp_iban as `emp_iban`,\r\n" + 
				"(case when bnk.swft_code = 'INMA' then 'NO' else 'Yes' end)  as `other_bank`,\r\n" + 
				"bnk.swft_code as 'swift_code',\r\n" + 
				"'SA' as `country`,\r\n" + 
				"'SAR' as `currency`,\r\n" + 
				"'/PAYROLL/' as `transfer_type`,\r\n" + 
				"(case when bnk.swft_code = 'INMA' then 'BANK ACCOUNT' else 'SARIE' end)  as `transfer_dest`,\r\n" + 
				"emp.empidnmbr as `emp_id_no`\r\n" + 
				"from hr00101 emp\r\n" + 
				"left join emp_bank eb on (emp.employindx = eb.emp_id)\r\n" + 
				"join hr40107 bnk on (eb.bnk_id = bnk.id)\r\n" + 
				"where emp.employid in (" + employeeIds +")");
		
//		query.setParameter(1, employeeIds);
		
		List<Object[]> list =  query.getResultList();
		
		for (int i=0; i < list.size(); i++) {
			Object[] result = list.get(i);
			
			String employeeId = CommonUtils.castToString(result[0]);
			String employeeName = CommonUtils.castToString(result[1]);
			String employeeIBAN = CommonUtils.castToString(result[2]);
			String employeeOtherBank = CommonUtils.castToString(result[3]);
			String employeeSwiftCode = CommonUtils.castToString(result[4]);
			String employeeCountry = CommonUtils.castToString(result[5]);
			String employeeCurrency = CommonUtils.castToString(result[6]);
			String transferType = CommonUtils.castToString(result[7]);
			String transferDest = CommonUtils.castToString(result[8]);
			String empIdNumber = CommonUtils.castToString(result[9]);
			
			DtoEmployeePayrollDetails obj = map.get(employeeId);
			obj.setEmployeeIBAN(employeeIBAN);
			obj.setOtherBank(employeeOtherBank);
			obj.setSwiftCode(employeeSwiftCode);
			obj.setCountry(employeeCountry);
			obj.setCurrency(employeeCurrency);
			obj.setTransferType(transferType);
			obj.setTransferDest(transferDest);
			obj.setEmployeeIdNumber(empIdNumber);
			
		}
		
		
		
	}
	
//	public List<Object[]> getPeriod(DtoProfitAndLossReport dtoProfitAndLossReport) {
//		List<Object[]> data = entityManager.createNamedStoredProcedureQuery("rptPL2").setParameter("periodPARAM", dtoProfitAndLossReport.getPeriod()).getResultList();
//		return data;
//	}
//	
//	public List<Object[]> getBalanceSheetReport() {
//		List<Object[]> data = entityManager.createNamedStoredProcedureQuery("RPTBS").getResultList();
//		return data;
//	}

}
