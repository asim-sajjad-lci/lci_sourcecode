/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.CompnayInsurance;
import com.bti.hcm.model.LifeInsuranceSetup;
import com.bti.hcm.model.dto.DtoLifeInsuranceSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryCompanyInsurance;
import com.bti.hcm.repository.RepositoryLifeInsuranceSetup;

/**
 * Description: Service LifeInsuranceSetup
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("serviceLifeInsuranceSetup")
public class ServiceLifeInsuranceSetup {
	
	
	/**
	 * @Description LOGGER use for put a logger in LifeInsuranceSetup Service
	 */
	static Logger log = Logger.getLogger(ServiceLifeInsuranceSetup.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in LifeInsuranceSetup service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in LifeInsuranceSetup service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in LifeInsuranceSetup service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryLifeInsuranceSetup Autowired here using annotation of spring for access of repositoryLifeInsuranceSetup method in LifeInsuranceSetup service
	 * 				In short Access LifeInsuranceSetup Query from Database using repositoryLifeInsuranceSetup.
	 */
	@Autowired
	RepositoryLifeInsuranceSetup repositoryLifeInsuranceSetup;
	
	@Autowired
	RepositoryCompanyInsurance repositoryHelthInsurance;
	
	/**
	 * @Description: save and update LifeInsuranceSetup data
	 * @param dtoLifeInsuranceSetup
	 * @return
	 * @throws ParseException
	 */
	public DtoLifeInsuranceSetup saveOrUpdateLifeInsuranceSetup(DtoLifeInsuranceSetup dtoLifeInsuranceSetup) throws ParseException {
		log.info("saveOrUpdateLifeInsuranceSetup Method");
		LifeInsuranceSetup lifeInsuranceSetup = null;
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			
			CompnayInsurance helthInsurance= repositoryHelthInsurance.findOne(dtoLifeInsuranceSetup.getHealthInsuranceId());
			if (dtoLifeInsuranceSetup.getId() != null && dtoLifeInsuranceSetup.getId() > 0) {
				lifeInsuranceSetup = repositoryLifeInsuranceSetup.findByIdAndIsDeleted(dtoLifeInsuranceSetup.getId(), false);
				lifeInsuranceSetup.setUpdatedBy(loggedInUserId);
				lifeInsuranceSetup.setUpdatedDate(new Date());
			} else {
				lifeInsuranceSetup = new LifeInsuranceSetup();
				lifeInsuranceSetup.setCreatedDate(new Date());
				Integer rowId = repositoryLifeInsuranceSetup.getCountOfTotalLifeInsuranceSetups();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				lifeInsuranceSetup.setRowId(increment);
			}
			lifeInsuranceSetup.setLifeInsuranceSetupId(dtoLifeInsuranceSetup.getLifeInsuraceId());
			lifeInsuranceSetup.setLifeInsuraceDescription(dtoLifeInsuranceSetup.getLifeInsuraceDescription());
			lifeInsuranceSetup.setLifeInsuraceDescriptionArabic(dtoLifeInsuranceSetup.getLifeInsuraceDescriptionArabic());
			lifeInsuranceSetup.setHealthInsuranceGroupNumber(dtoLifeInsuranceSetup.getHealthInsuranceGroupNumber());
			lifeInsuranceSetup.setLifeInsuranceFrequency(dtoLifeInsuranceSetup.getLifeInsuranceFrequency());
			lifeInsuranceSetup.setAmount(dtoLifeInsuranceSetup.getAmount());
			lifeInsuranceSetup.setSpouseAmount(dtoLifeInsuranceSetup.getSpouseAmount());
			lifeInsuranceSetup.setChildAmount(dtoLifeInsuranceSetup.getChildAmount());
			lifeInsuranceSetup.setCoverageTotalAmount(dtoLifeInsuranceSetup.getCoverageTotalAmount());
			lifeInsuranceSetup.setEmployeePay(dtoLifeInsuranceSetup.getEmployeePay());
			lifeInsuranceSetup.setStartDate(dtoLifeInsuranceSetup.getStartDate());
			lifeInsuranceSetup.setEndDate(dtoLifeInsuranceSetup.getEndDate());
			lifeInsuranceSetup.setInsuranceType(dtoLifeInsuranceSetup.getInsuranceType());
			lifeInsuranceSetup.setHelthInsurance(helthInsurance);
			repositoryLifeInsuranceSetup.saveAndFlush(lifeInsuranceSetup);
			log.debug("LifeInsuranceSetup is:"+dtoLifeInsuranceSetup.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoLifeInsuranceSetup;

	}
	
	/**
	 * @Description: get All LifeInsuranceSetup
	 * @param dtoLifeInsuranceSetup
	 * @return
	 */
	public DtoSearch getAllLifeInsuranceSetup(DtoLifeInsuranceSetup dtoLifeInsuranceSetup) {
		log.info("getAllLifeInsuranceSetup Method");
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoLifeInsuranceSetup.getPageNumber());
			dtoSearch.setPageSize(dtoLifeInsuranceSetup.getPageSize());
			dtoSearch.setTotalCount(repositoryLifeInsuranceSetup.getCountOfTotalLifeInsuranceSetup());
			List<LifeInsuranceSetup> lifeInsuranceSetupList = null;
			if (dtoLifeInsuranceSetup.getPageNumber() != null && dtoLifeInsuranceSetup.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoLifeInsuranceSetup.getPageNumber(), dtoLifeInsuranceSetup.getPageSize(), Direction.DESC, "createdDate");
				lifeInsuranceSetupList = repositoryLifeInsuranceSetup.findByIsDeleted(false, pageable);
			} else {
				lifeInsuranceSetupList = repositoryLifeInsuranceSetup.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoLifeInsuranceSetup> dtoLifeInsuranceSetupList=new ArrayList<>();
			if(lifeInsuranceSetupList!=null && lifeInsuranceSetupList.isEmpty())
			{
				for (LifeInsuranceSetup lifeInsuranceSetup : lifeInsuranceSetupList) 
				{
					dtoLifeInsuranceSetup=new DtoLifeInsuranceSetup(lifeInsuranceSetup);
					dtoLifeInsuranceSetup.setLifeInsuraceDescription(dtoLifeInsuranceSetup.getLifeInsuraceDescription());
					dtoLifeInsuranceSetup.setLifeInsuraceDescriptionArabic(dtoLifeInsuranceSetup.getLifeInsuraceDescriptionArabic());
					dtoLifeInsuranceSetup.setHealthInsuranceGroupNumber(dtoLifeInsuranceSetup.getHealthInsuranceGroupNumber());
					dtoLifeInsuranceSetup.setLifeInsuranceFrequency(dtoLifeInsuranceSetup.getLifeInsuranceFrequency());
					dtoLifeInsuranceSetup.setAmount(dtoLifeInsuranceSetup.getAmount());
					dtoLifeInsuranceSetup.setSpouseAmount(dtoLifeInsuranceSetup.getSpouseAmount());
					dtoLifeInsuranceSetup.setChildAmount(dtoLifeInsuranceSetup.getChildAmount());
					dtoLifeInsuranceSetup.setCoverageTotalAmount(dtoLifeInsuranceSetup.getCoverageTotalAmount());
					dtoLifeInsuranceSetup.setEmployeePay(dtoLifeInsuranceSetup.getEmployeePay());
					dtoLifeInsuranceSetup.setStartDate(dtoLifeInsuranceSetup.getStartDate());
					dtoLifeInsuranceSetup.setEndDate(dtoLifeInsuranceSetup.getEndDate());
					dtoLifeInsuranceSetup.setInsuranceType(dtoLifeInsuranceSetup.getInsuranceType());
					dtoLifeInsuranceSetup.setLifeInsuraceId(dtoLifeInsuranceSetup.getLifeInsuraceId());
					dtoLifeInsuranceSetupList.add(dtoLifeInsuranceSetup);
				}
				dtoSearch.setRecords(dtoLifeInsuranceSetupList);
			}
			log.debug("All LifeInsuranceSetup List Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	/**
	 * @Description: Search LifeInsuranceSetup data
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchLifeInsuranceSetup(DtoSearch dtoSearch) {
		try {

			log.info("searchLifeInsuranceSetup Method");
			
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				dtoSearch = getDtosearch(dtoSearch);
				
				dtoSearch.setTotalCount(this.repositoryLifeInsuranceSetup.predictiveLifeInsuranceSetupSearchTotalCount("%"+searchWord+"%"));
				List<LifeInsuranceSetup> lifeInsuranceSetupList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						lifeInsuranceSetupList = this.repositoryLifeInsuranceSetup.predictiveLifeInsuranceSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						lifeInsuranceSetupList = this.repositoryLifeInsuranceSetup.predictiveLifeInsuranceSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						lifeInsuranceSetupList = this.repositoryLifeInsuranceSetup.predictiveLifeInsuranceSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
					}
					
				}
				if(lifeInsuranceSetupList != null && !lifeInsuranceSetupList.isEmpty()){
					List<DtoLifeInsuranceSetup> dtoLifeInsuranceSetupList = new ArrayList<>();
					DtoLifeInsuranceSetup dtoLifeInsuranceSetup=null;
					for (LifeInsuranceSetup lifeInsuranceSetup : lifeInsuranceSetupList) {
						dtoLifeInsuranceSetup = new DtoLifeInsuranceSetup(lifeInsuranceSetup);
						dtoLifeInsuranceSetup.setId(lifeInsuranceSetup.getId());
						dtoLifeInsuranceSetup.setLifeInsuraceId(lifeInsuranceSetup.getLifeInsuranceSetupId());
						dtoLifeInsuranceSetup.setLifeInsuraceDescription(lifeInsuranceSetup.getLifeInsuraceDescription());
						dtoLifeInsuranceSetup.setLifeInsuraceDescriptionArabic(lifeInsuranceSetup.getLifeInsuraceDescriptionArabic());
						dtoLifeInsuranceSetup.setHealthInsuranceGroupNumber(lifeInsuranceSetup.getHealthInsuranceGroupNumber());
						dtoLifeInsuranceSetup.setLifeInsuranceFrequency(lifeInsuranceSetup.getLifeInsuranceFrequency());
						dtoLifeInsuranceSetup.setAmount(lifeInsuranceSetup.getAmount());
						dtoLifeInsuranceSetup.setSpouseAmount(lifeInsuranceSetup.getSpouseAmount());
						dtoLifeInsuranceSetup.setChildAmount(lifeInsuranceSetup.getChildAmount());
						dtoLifeInsuranceSetup.setCoverageTotalAmount(lifeInsuranceSetup.getCoverageTotalAmount());
						dtoLifeInsuranceSetup.setEmployeePay(lifeInsuranceSetup.getEmployeePay());
						dtoLifeInsuranceSetup.setStartDate(lifeInsuranceSetup.getStartDate());
						dtoLifeInsuranceSetup.setEndDate(lifeInsuranceSetup.getEndDate());
						dtoLifeInsuranceSetup.setInsuranceType(lifeInsuranceSetup.getInsuranceType());
						if(lifeInsuranceSetup.getHelthInsurance().getId()!=null) {
							dtoLifeInsuranceSetup.setHealthInsuranceId(lifeInsuranceSetup.getHelthInsurance().getId());
						}
						
						dtoLifeInsuranceSetupList.add(dtoLifeInsuranceSetup);
					}
					dtoSearch.setRecords(dtoLifeInsuranceSetupList);
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}

	
	public DtoSearch getDtosearch(DtoSearch dtoSearch) {
		String condition="";

		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
		switch(dtoSearch.getSortOn()){
		case "lifeInsuranceSetupId" : 
			condition+=dtoSearch.getSortOn();
			break;
		case "lifeInsuraceDescription" : 
			condition+=dtoSearch.getSortOn();
			break;
		case "lifeInsuraceDescriptionArabic" : 
			condition+=dtoSearch.getSortOn();
			break;
		case "healthInsuranceGroupNumber" : 
			condition+=dtoSearch.getSortOn();
			break;
		case "amount" : 
			condition+=dtoSearch.getSortOn();
			break;
		case "spouseAmount" : 
			condition+=dtoSearch.getSortOn();
			break;
		case "childAmount" : 
			condition+=dtoSearch.getSortOn();
			break;
		case "coverageTotalAmount" : 
			condition+=dtoSearch.getSortOn();
			break;
		case "employeePay" : 
			condition+=dtoSearch.getSortOn();
			break;
		default:
			condition+="id";
			
		}
	}else{
		condition+="id";
		dtoSearch.setSortOn("");
		dtoSearch.setSortBy("");
		
		
	}
		dtoSearch.setCondition(condition);
		return dtoSearch;
	}
	/**
	 *  @Description: get LifeInsuranceSetup data by id
	 * @param id
	 * @return
	 */
	public DtoLifeInsuranceSetup getLifeInsuranceSetupByLifeInsuranceSetupId(int id) {
		log.info("getLifeInsuranceSetupByLifeInsuranceSetupId Method");
		DtoLifeInsuranceSetup dtoLifeInsuranceSetup = new DtoLifeInsuranceSetup();
		try {
			if (id > 0) {
				LifeInsuranceSetup lifeInsuranceSetup = repositoryLifeInsuranceSetup.findByIdAndIsDeleted(id, false);
				if (lifeInsuranceSetup != null) {
					dtoLifeInsuranceSetup = new DtoLifeInsuranceSetup(lifeInsuranceSetup);
				} else {
					dtoLifeInsuranceSetup.setMessageType("LIFE_INSURANCE_SETUP_NOT_GETTING");

				}
			} else {
				dtoLifeInsuranceSetup.setMessageType("INVALID_LIFE_INSURANCE_SETUP_ID");

			}
			log.debug("LifeInsuranceSetup By Id is:"+dtoLifeInsuranceSetup.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoLifeInsuranceSetup;
	}
	
	
	/**
	 * @Description: delete LifeInsuranceSetup
	 * @param ids
	 * @return
	 */
	public DtoLifeInsuranceSetup deleteLifeInsuranceSetup(List<Integer> ids) {
		log.info("deleteLifeInsuranceSetup Method");
		DtoLifeInsuranceSetup dtoLifeInsuranceSetup = new DtoLifeInsuranceSetup();
		dtoLifeInsuranceSetup
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("LIFE_INSURANCE_SETUP_DELETED", false));
		dtoLifeInsuranceSetup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("LIFE_INSURANCE_SETUPT_ASSOCIATED", false));
		List<DtoLifeInsuranceSetup> deleteDtoLifeInsuranceSetup = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer lifeInsuranceSetupId : ids) {
				LifeInsuranceSetup lifeInsuranceSetup = repositoryLifeInsuranceSetup.findOne(lifeInsuranceSetupId);
				DtoLifeInsuranceSetup dtoLifeInsuranceSetup2 = new DtoLifeInsuranceSetup();
				dtoLifeInsuranceSetup2.setId(lifeInsuranceSetupId);
				dtoLifeInsuranceSetup2.setLifeInsuraceDescription(lifeInsuranceSetup.getLifeInsuraceDescription());
				dtoLifeInsuranceSetup2.setLifeInsuraceDescriptionArabic(lifeInsuranceSetup.getLifeInsuraceDescriptionArabic());
				dtoLifeInsuranceSetup2.setHealthInsuranceGroupNumber(lifeInsuranceSetup.getHealthInsuranceGroupNumber());
				dtoLifeInsuranceSetup2.setLifeInsuranceFrequency(lifeInsuranceSetup.getLifeInsuranceFrequency());
				dtoLifeInsuranceSetup2.setAmount(lifeInsuranceSetup.getAmount());
				dtoLifeInsuranceSetup2.setSpouseAmount(lifeInsuranceSetup.getSpouseAmount());
				dtoLifeInsuranceSetup2.setChildAmount(lifeInsuranceSetup.getChildAmount());
				dtoLifeInsuranceSetup2.setCoverageTotalAmount(lifeInsuranceSetup.getCoverageTotalAmount());
				dtoLifeInsuranceSetup2.setEmployeePay(lifeInsuranceSetup.getEmployeePay());
				dtoLifeInsuranceSetup2.setStartDate(lifeInsuranceSetup.getStartDate());
				dtoLifeInsuranceSetup2.setEndDate(lifeInsuranceSetup.getEndDate());
				dtoLifeInsuranceSetup2.setInsuranceType(lifeInsuranceSetup.getInsuranceType());
				repositoryLifeInsuranceSetup.deleteSingleLifeInsuranceSetup(true, loggedInUserId, lifeInsuranceSetupId);
				deleteDtoLifeInsuranceSetup.add(dtoLifeInsuranceSetup2);

			}
			dtoLifeInsuranceSetup.setDeleteLifeInsuranceSetup(deleteDtoLifeInsuranceSetup);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete LifeInsuranceSetup :"+dtoLifeInsuranceSetup.getId());
		return dtoLifeInsuranceSetup;
	}
	
	
	/**
	 * @Description: check Repeat lifeInsuranceSetupId
	 * @param lifeInsuranceSetupId
	 * @return
	 */
	public DtoLifeInsuranceSetup repeatByLifeInsuranceSetupId(String lifeInsuranceSetupId) {
		log.info("repeatByLifeInsuranceSetupId Method");
		DtoLifeInsuranceSetup dtoLifeInsuranceSetup = new DtoLifeInsuranceSetup();
		try {
			List<LifeInsuranceSetup> lifeInsuranceSetup=repositoryLifeInsuranceSetup.findByLifeInsuraceId(lifeInsuranceSetupId);
			if(lifeInsuranceSetup!=null && !lifeInsuranceSetup.isEmpty()) {
				dtoLifeInsuranceSetup.setIsRepeat(true);
			}else {
				dtoLifeInsuranceSetup.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoLifeInsuranceSetup;
	}

	public DtoSearch searchIds(DtoSearch dtoSearch) {
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				dtoSearch = getDtosearch(dtoSearch);
				List<LifeInsuranceSetup> lifeInsuranceSetupList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						lifeInsuranceSetupList = this.repositoryLifeInsuranceSetup.searchIds("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						lifeInsuranceSetupList = this.repositoryLifeInsuranceSetup.searchIds("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						lifeInsuranceSetupList = this.repositoryLifeInsuranceSetup.searchIds("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
					}
					
				}
				if(lifeInsuranceSetupList != null && !lifeInsuranceSetupList.isEmpty()){
					List<DtoLifeInsuranceSetup> dtoLifeInsuranceSetupList = new ArrayList<>();
					DtoLifeInsuranceSetup dtoLifeInsuranceSetup=null;
					for (LifeInsuranceSetup lifeInsuranceSetup : lifeInsuranceSetupList) {
						dtoLifeInsuranceSetup = new DtoLifeInsuranceSetup(lifeInsuranceSetup);
						dtoLifeInsuranceSetup.setId(lifeInsuranceSetup.getId());
						dtoLifeInsuranceSetup.setLifeInsuraceId(lifeInsuranceSetup.getLifeInsuranceSetupId());
						
						dtoLifeInsuranceSetupList.add(dtoLifeInsuranceSetup);
					}
					dtoSearch.setRecords(dtoLifeInsuranceSetupList);
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	
}
