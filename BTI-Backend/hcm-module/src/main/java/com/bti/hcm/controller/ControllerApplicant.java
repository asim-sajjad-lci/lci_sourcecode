package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoApplicant;
import com.bti.hcm.service.ServiceApplicant;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/applicant")
public class ControllerApplicant extends BaseController{
	
	
	private Logger log = Logger.getLogger(ControllerApplicant.class);
	
	
	@Autowired
	ServiceApplicant serviceApplicant;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	/**
	 * @param request
	 * @param dtoApplicant
	 * @return
	 * @throws Exception
	 */
	
	
	
	
	/**
	 * @param request
	 * @param dtoApplicant
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createApplicant(HttpServletRequest request, @RequestBody DtoApplicant dtoApplicant) throws Exception {
		log.info("Create Applicant Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoApplicant  = serviceApplicant.saveOrUpdate(dtoApplicant);
			responseMessage=displayMessage(dtoApplicant, "APPLICANT_CREATED", "APPLICANT NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create Applicant Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoApplicant
	 * @return
	 * @throws Exception
	 */
	
	
	/**
	 * @param request
	 * @param dtoApplicant
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoApplicant dtoApplicant) throws Exception {
		log.info("Update Applicant Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoApplicant = serviceApplicant.saveOrUpdate(dtoApplicant);
			responseMessage=displayMessage(dtoApplicant, "APPLICANT_UPDATED", "APPLICANT_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update Applicant Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	/**
	 * @param request
	 * @param dtoApplicant
	 * @return
	 * @throws Exception
	 */
	
	
	
	/**
	 * @param request
	 * @param dtoApplicant
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoApplicant dtoApplicant) throws Exception {
		log.info("Delete Applicant Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoApplicant.getIds() != null && !dtoApplicant.getIds().isEmpty()) {
				DtoApplicant dtoApplicant2 = serviceApplicant.deleteApplicant(dtoApplicant.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("APPLICANT_DELETED", false), dtoApplicant2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete Applicant Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param request
	 * @param dtoApplicant
	 * @return
	 * @throws Exception
	 */
	
	
	
	/**
	 * @param request
	 * @param dtoApplicant
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getApplicantById", method = RequestMethod.POST)
	public ResponseMessage getApplicantById(HttpServletRequest request, @RequestBody DtoApplicant dtoApplicant) throws Exception {
		log.info("Get Applicant ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoApplicant dtoApplicantObj = serviceApplicant.getById(dtoApplicant.getId());
			responseMessage=displayMessage(dtoApplicantObj, "APPLICANT_LIST_GET_DETAIL", "APPLICANT_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get Applicant by Id Method:"+dtoApplicant.getId());
		return responseMessage;
	}
	
	
	

}
