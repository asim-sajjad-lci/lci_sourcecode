/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.Date;

/**
 * Description: DTO EmployeeMaster class having getter and setter for fields
 * (POJO) Name Name of Project: Hcm Version: 0.0.1
 */
public class DtoEmployeeMasterForLoan extends DtoBase {

	private String empFullName;
	private String locationName; // site
	private String positionName;
	private Double totalPackage; // 0 for now
	private Double previousLoan;
	private Double totalDeduction;
	private Double totalRemaining;
	private Double currentESB; // 0 for now

	private int employeeIndexId;
	private String employeeId;

	private String employeeHireDateString;
	private Date employeeHireDate; // Format(dd/mm/yyyy)

	public String getEmpFullName() {
		return empFullName;
	}

	public void setEmpFullName(String empFullName) {
		this.empFullName = empFullName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public Double getPreviousLoan() {
		return previousLoan;
	}

	public void setPreviousLoan(Double previousLoan) {
		this.previousLoan = previousLoan;
	}

	public Double getTotalDeduction() {
		return totalDeduction;
	}

	public void setTotalDeduction(Double totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	public Double getTotalRemaining() {
		return totalRemaining;
	}

	public void setTotalRemaining(Double totalRemaining) {
		this.totalRemaining = totalRemaining;
	}

	public int getEmployeeIndexId() {
		return employeeIndexId;
	}

	public void setEmployeeIndexId(int employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeHireDateString() {
		return employeeHireDateString;
	}

	public void setEmployeeHireDateString(String employeeHireDateString) {
		this.employeeHireDateString = employeeHireDateString;
	}

	public Date getEmployeeHireDate() {
		return employeeHireDate;
	}

	public void setEmployeeHireDate(Date employeeHireDate) {
		this.employeeHireDate = employeeHireDate;
	}

	public Double getTotalPackage() {
		return totalPackage;
	}

	public void setTotalPackage(Double totalPackage) {
		this.totalPackage = totalPackage;
	}

	public Double getCurrentESB() {
		return currentESB;
	}

	public void setCurrentESB(Double currentESB) {
		this.currentESB = currentESB;
	}

}
