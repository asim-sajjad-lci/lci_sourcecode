package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DtoPayrollTransactionHistoryYearDetail extends DtoBase {
	private Integer id;
	private Integer year;
	private String auditTransactionNumber;
	private Integer employeeCodeSequence;
	private Short codeType;
	private Integer codeIndexId;
	private BigDecimal totalAmount;
	private BigDecimal totalActualAmount;
	private BigDecimal totalPercentCode;
	private Integer totalHours;
	private BigDecimal rateofBaseOnCode;
	private Integer codeBaseOnCodeIndex;
	private Date codeFromPeriodDate;
	private Date codeToPeriodDate;
	private Date payrollPostDate;
	private String payrollPostByUserId;
	private Integer employeeId;
	private Integer employeeIndexId;
	private Long accountTableRowIndex;
	private Short postingTypes;
	private BigDecimal debitAmount;
	private BigDecimal creaditAmount;
	private Integer accountSequence;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getAuditTransactionNumber() {
		return auditTransactionNumber;
	}

	public void setAuditTransactionNumber(String auditTransactionNumber) {
		this.auditTransactionNumber = auditTransactionNumber;
	}

	public Integer getEmployeeCodeSequence() {
		return employeeCodeSequence;
	}

	public void setEmployeeCodeSequence(Integer employeeCodeSequence) {
		this.employeeCodeSequence = employeeCodeSequence;
	}

	public Short getCodeType() {
		return codeType;
	}

	public void setCodeType(Short codeType) {
		this.codeType = codeType;
	}

	public Integer getCodeIndexId() {
		return codeIndexId;
	}

	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}

	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}

	public BigDecimal getTotalPercentCode() {
		return totalPercentCode;
	}

	public void setTotalPercentCode(BigDecimal totalPercentCode) {
		this.totalPercentCode = totalPercentCode;
	}

	public Integer getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public BigDecimal getRateofBaseOnCode() {
		return rateofBaseOnCode;
	}

	public void setRateofBaseOnCode(BigDecimal rateofBaseOnCode) {
		this.rateofBaseOnCode = rateofBaseOnCode;
	}

	public Integer getCodeBaseOnCodeIndex() {
		return codeBaseOnCodeIndex;
	}

	public void setCodeBaseOnCodeIndex(Integer codeBaseOnCodeIndex) {
		this.codeBaseOnCodeIndex = codeBaseOnCodeIndex;
	}

	public Date getCodeFromPeriodDate() {
		return codeFromPeriodDate;
	}

	public void setCodeFromPeriodDate(Date codeFromPeriodDate) {
		this.codeFromPeriodDate = codeFromPeriodDate;
	}

	public Date getCodeToPeriodDate() {
		return codeToPeriodDate;
	}

	public void setCodeToPeriodDate(Date codeToPeriodDate) {
		this.codeToPeriodDate = codeToPeriodDate;
	}

	public Date getPayrollPostDate() {
		return payrollPostDate;
	}

	public void setPayrollPostDate(Date payrollPostDate) {
		this.payrollPostDate = payrollPostDate;
	}

	public String getPayrollPostByUserId() {
		return payrollPostByUserId;
	}

	public void setPayrollPostByUserId(String payrollPostByUserId) {
		this.payrollPostByUserId = payrollPostByUserId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getEmployeeIndexId() {
		return employeeIndexId;
	}

	public void setEmployeeIndexId(Integer employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}

	public Long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(Long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Short getPostingTypes() {
		return postingTypes;
	}

	public void setPostingTypes(Short postingTypes) {
		this.postingTypes = postingTypes;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreaditAmount() {
		return creaditAmount;
	}

	public void setCreaditAmount(BigDecimal creaditAmount) {
		this.creaditAmount = creaditAmount;
	}

	public Integer getAccountSequence() {
		return accountSequence;
	}

	public void setAccountSequence(Integer accountSequence) {
		this.accountSequence = accountSequence;
	}

}
