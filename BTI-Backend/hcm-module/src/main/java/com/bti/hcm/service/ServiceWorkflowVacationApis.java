package com.bti.hcm.service;

import java.util.HashMap;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.expression.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.EmployeeLoanEntry;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.EmployeeMonthlyInstallment;
import com.bti.hcm.model.EmployeeVacationEntry;
import com.bti.hcm.model.dto.DtoEmployeeInfo;
import com.bti.hcm.model.dto.DtoEmployeeLoanEntry;
import com.bti.hcm.model.dto.DtoEmployeeLoanInfo;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeVacation;
import com.bti.hcm.model.dto.DtoEmployeeVacation.DtoEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeeVacationEntry;
import com.bti.hcm.model.dto.DtoEmployeeVacationEntry;

@Service("serviceWorkflowVacationApis")
public class ServiceWorkflowVacationApis {

	static Logger log = LoggerFactory.getLogger(ServiceWorkflowVacationApis.class.getName());

	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceEmployeeMaster serviceEmployeeMaster; 
	
	@Autowired
	private RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private RepositoryEmployeeVacationEntry repositoryEmployeeVacationEntry;
	
	
	public DtoEmployeeVacation getEmployeeVacationDetails(String employeeCode) {
		
		DtoEmployeeVacation dtoEmployeeVacation = null;

		if (employeeCode != null && !employeeCode.isEmpty()) {
			
			DtoEmployeeMaster dtoEmployeeMaster = serviceEmployeeMaster.getEmployeeByCode(employeeCode);

			if (dtoEmployeeMaster != null) {
				dtoEmployeeVacation = new DtoEmployeeVacation();
				
				dtoEmployeeVacation.setEmployeeCode(employeeCode);
				dtoEmployeeVacation.setEmployeeName(dtoEmployeeMaster.getEmployeeFullName());
				dtoEmployeeVacation.setEmployeeNameArabic(dtoEmployeeMaster.getEmployeeFullNameArabic());
				
				if (dtoEmployeeMaster.getDtoPosition() != null) {
					dtoEmployeeVacation.setPosition(dtoEmployeeMaster.getDtoPosition().getDescription());
					dtoEmployeeVacation.setPositionArabic(dtoEmployeeMaster.getDtoPosition().getArabicDescription());
				}
				
				if (dtoEmployeeMaster.getDtoLocation() != null) {
					dtoEmployeeVacation.setSite(dtoEmployeeMaster.getDtoLocation().getDescription());
					dtoEmployeeVacation.setSiteArabic(dtoEmployeeMaster.getDtoLocation().getArabicDescription());
				}
				
				dtoEmployeeVacation.setJoiningDate(dtoEmployeeMaster.getEmployeeHireDate()); 
				// TODO it may be replaced with Adjusted Hire Date Later
				
				dtoEmployeeVacation.setNationality(dtoEmployeeMaster.getDtoEmployeeNationalities().getEmployeeNationalityDescription());
				
				dtoEmployeeVacation.setCitizen(dtoEmployeeMaster.isEmployeeCitizen());
				
				dtoEmployeeVacation.setDepartment(dtoEmployeeMaster.getDtoDepartment().getDepartmentDescription());
				
				dtoEmployeeVacation.setEmployeeMaritalStatus(dtoEmployeeMaster.getEmployeeMaritalStatus());
				
				dtoEmployeeVacation.setEmployeeId(dtoEmployeeMaster.getEmployeeId());
			}
		}
		

		return (dtoEmployeeVacation);
	}
	
	
	public String postEmployeeVacationEntry(DtoEmployeeVacationEntry dtoEmployeeVacationEntry) {
		String result = MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_FAILURE;
		try {
			
			EmployeeVacationEntry employeeVacationEntry = convertToModel(dtoEmployeeVacationEntry);
			
			String employeeCode = dtoEmployeeVacationEntry.getEmployeeCode();
			
			EmployeeMaster employeeMaster = repositoryEmployeeMaster.findOneByEmployeeId(employeeCode);
			
			if (employeeMaster == null) {
				return MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_EMPLOYEE_NOT_FOUND;
			}
			
			employeeVacationEntry.setEmployeeMaster(employeeMaster);
			
//			for (EmployeeMonthlyInstallment emi: employeeVacationEntry.get) {
//				emi.setEmployeeLoanEntry(employeeVacationEntry);
//			}
			
			employeeVacationEntry = repositoryEmployeeVacationEntry.save(employeeVacationEntry);
			
			if (employeeVacationEntry.getIdx() > 0) {
				result = MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_SUCCESS;
			} 

		} catch (DataIntegrityViolationException divEx) {
			result = MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_DUPLICATE_REQUEST;
			log.error(divEx.getMessage());
			divEx.printStackTrace();
		} catch (Exception ex) {
			result = MessageConstant.POST_EMPLOYEE_VACATION_ENTRY_FAILURE;
			log.error(ex.getMessage());
			ex.printStackTrace();
		}
		return result;
	}
	
	
	
	private DtoEmployeeVacationEntry convertToDto(EmployeeVacationEntry employeeVacationEntry) {
		DtoEmployeeVacationEntry dtoEmployeeLoanEntry = modelMapper.map(employeeVacationEntry, DtoEmployeeVacationEntry.class);
	    return dtoEmployeeLoanEntry;
	} 
	
	private EmployeeVacationEntry convertToModel(DtoEmployeeVacationEntry dtoEmployeeVacationEntry) throws ParseException {
		EmployeeVacationEntry employeeVacationEntry = modelMapper.map(dtoEmployeeVacationEntry, EmployeeVacationEntry.class);
	    return employeeVacationEntry;
	}
	

}
