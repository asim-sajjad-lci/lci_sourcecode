package com.bti.hcm.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoWorkFlow {

	private Integer worFlowId;
	private byte[] workFlowFile;
	private String fileDesc;

	

	public Integer getWorFlowId() {
		return worFlowId;
	}

	public void setWorFlowId(Integer worFlowId) {
		this.worFlowId = worFlowId;
	}

	public byte[] getWorkFlowFile() {
		return workFlowFile;
	}

	public void setWorkFlowFile(byte[] workFlowFile) {
		this.workFlowFile = workFlowFile;
	}

	public String getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}

}
