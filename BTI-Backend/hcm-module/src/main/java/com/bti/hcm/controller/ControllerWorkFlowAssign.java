/*package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.model.dto.DtoWorkFlowAssign;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceWorkFlow;
import com.bti.hcm.service.ServiceWorkFlowAssign;


@RestController
@RequestMapping("/workFlowAssign")
public class ControllerWorkFlowAssign extends BaseConroller{
	
	private static final Logger logger = Logger.getLogger(ControllerWorkFlowAssign.class);
	
	@Autowired
	ServiceWorkFlowAssign serviceWorkFlowAssign;

	@Autowired
	ServiceWorkFlow serviceWorkFlow;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createReportMaster(HttpServletRequest request,@RequestBody DtoWorkFlowAssign dtoWorkFlowAssign) {
		logger.info("In CreateWorkFlowAssignr Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoWorkFlowAssign = serviceWorkFlowAssign.saveOrUpdate(dtoWorkFlowAssign);
			if (dtoWorkFlowAssign != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("WORK_FLOW_ASSIGN_CREATED_SUCCESSFULLY", false),
						dtoWorkFlowAssign);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("WORK_FLOW_ASSIGN_NOT_CREATED", false),
						dtoWorkFlowAssign);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Message" + responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoWorkFlowAssign dtoWorkFlowAssign) throws Exception {
		logger.info("Create WorkFlowAssign Method");
		ResponseMessage responseMessage = null;
			boolean flag = serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				dtoWorkFlowAssign  = serviceWorkFlowAssign.saveOrUpdate(dtoWorkFlowAssign);
				responseMessage = displayMessage(dtoWorkFlowAssign,"WORK_FLOW_ASSIGN_CREATED_SUCCESSFULLY","WORK_FLOW_ASSIGN_NOT_CREATED",serviceResponse);
			} else {
				responseMessage = unauthorizedMsg(serviceResponse);
			}
			logger.debug("Create WorkFlowAssign Method:"+responseMessage.getMessage());
		    return responseMessage;
	}
	
}
*/