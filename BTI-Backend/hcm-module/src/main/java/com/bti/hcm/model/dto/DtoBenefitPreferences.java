package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.BenefitPreferences;

public class DtoBenefitPreferences extends DtoBase{

    private Integer id;
    private byte postEstimatedDate;
    private byte postPaymentDates;
    private int workingdaysWeek;
    private int workingHoursDay;
    private int eligibilityDate;
    private int paperworkDeadlines;
    private boolean inActive;
    private List<DtoBenefitPreferences> delete;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public byte getPostEstimatedDate() {
		return postEstimatedDate;
	}
	public void setPostEstimatedDate(byte postEstimatedDate) {
		this.postEstimatedDate = postEstimatedDate;
	}
	public byte getPostPaymentDates() {
		return postPaymentDates;
	}
	public void setPostPaymentDates(byte postPaymentDates) {
		this.postPaymentDates = postPaymentDates;
	}
	public int getWorkingdaysWeek() {
		return workingdaysWeek;
	}
	public void setWorkingdaysWeek(int workingdaysWeek) {
		this.workingdaysWeek = workingdaysWeek;
	}
	public int getWorkingHoursDay() {
		return workingHoursDay;
	}
	public void setWorkingHoursDay(int workingHoursDay) {
		this.workingHoursDay = workingHoursDay;
	}
	public int getEligibilityDate() {
		return eligibilityDate;
	}
	public void setEligibilityDate(int eligibilityDate) {
		this.eligibilityDate = eligibilityDate;
	}
	public int getPaperworkDeadlines() {
		return paperworkDeadlines;
	}
	public void setPaperworkDeadlines(int paperworkDeadlines) {
		this.paperworkDeadlines = paperworkDeadlines;
	}
	public boolean isInActive() {
		return inActive;
	}
	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}
	public List<DtoBenefitPreferences> getDelete() {
		return delete;
	}
	public void setDelete(List<DtoBenefitPreferences> delete) {
		this.delete = delete;
	}
    
    public DtoBenefitPreferences() {
	}
    
    public DtoBenefitPreferences(BenefitPreferences benefitPreferences) {
   	}
    
}
