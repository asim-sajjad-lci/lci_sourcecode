/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Description: Service AtteandaceOptionType
 * Project: Hcm 
 * Version: 0.0.1
 */
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.AtteandaceOptionType;
import com.bti.hcm.model.dto.DtoAttedanceOptionType;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAtteandaceOptionType;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceAtteandaceOptionType")
public class ServiceAtteandaceOptionType {
	
	
	static Logger log = Logger.getLogger(ServiceAtteandaceOptionType.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryAtteandaceOptionType repositoryAtteandaceOptionType;
	
	
	/**
	 * 
	 * @param dtoAttedanceOptionType
	 * @return
	 */
	public DtoAttedanceOptionType saveOrUpdate(DtoAttedanceOptionType dtoAttedanceOptionType) {

		try {
			log.info("saveOrUpdate Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			AtteandaceOptionType attaeandace = null;
			if (dtoAttedanceOptionType.getId() != null && dtoAttedanceOptionType.getId() > 0) {
				attaeandace = repositoryAtteandaceOptionType.findByIdAndIsDeleted(dtoAttedanceOptionType.getId(), false);
				attaeandace.setUpdatedBy(loggedInUserId);
				attaeandace.setUpdatedDate(new Date());
			} else {
				attaeandace = new AtteandaceOptionType();
				attaeandace.setCreatedDate(new Date());
				Integer rowId = repositoryAtteandaceOptionType.getCountOfTotalAtteandaces();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				attaeandace.setRowId(increment);
			}
			attaeandace.setSeqn(dtoAttedanceOptionType.getSeqn());
			attaeandace.setReasonIndx(dtoAttedanceOptionType.getReasonIndx());
			attaeandace.setReasonDesc(dtoAttedanceOptionType.getReasonDesc());
			attaeandace.setAtteandanceTypeDesc(dtoAttedanceOptionType.getAtteandanceTypeDesc());
			attaeandace.setAtteandanceTypeIndx(dtoAttedanceOptionType.getAtteandanceTypeIndx());
			attaeandace.setAtteandanceTypeSeqn(dtoAttedanceOptionType.getAtteandanceTypeSeqn());
			attaeandace.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			 repositoryAtteandaceOptionType.saveAndFlush(attaeandace);
			log.debug("AtteandaceOptionType is:"+dtoAttedanceOptionType.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAttedanceOptionType;
	}
	
	
	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoAttedanceOptionType delete(List<Integer> ids) {
		log.info("deleteAtteandaceOptionType Method");
		DtoAttedanceOptionType dtoAttedanceOptionType = new DtoAttedanceOptionType();
		dtoAttedanceOptionType
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ATTEDANCE_OPTION_TYPE_DELETED", false));
		dtoAttedanceOptionType.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("ATTEDANCE_OPTION_TYPE_ASSOCIATED", false));
		List<DtoAttedanceOptionType> deleteAtteandaceOptionType = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer attedanceOptionTypeId : ids) {
				AtteandaceOptionType atteandaceOptionType = repositoryAtteandaceOptionType.findOne(attedanceOptionTypeId);
				DtoAttedanceOptionType dtoAtteandaceOptionType2 = new DtoAttedanceOptionType();
				dtoAtteandaceOptionType2.setId(atteandaceOptionType.getId());
				repositoryAtteandaceOptionType.deleteSingleAtteandaceOptionType(true, loggedInUserId, attedanceOptionTypeId);
				deleteAtteandaceOptionType.add(dtoAtteandaceOptionType2);
			}
			dtoAttedanceOptionType.setDelete(deleteAtteandaceOptionType);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete AtteandaceOptionType :"+dtoAttedanceOptionType.getId());
		return dtoAttedanceOptionType;
	}

	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchAtteandaceOptionType(DtoSearch dtoSearch) {
		log.info("searchAtteandaceOptionType Method");
		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				dtoSearch=searchByAtteandaceOptionType(dtoSearch);
				dtoSearch.setTotalCount(this.repositoryAtteandaceOptionType.predictiveAtteandaceOptionTypeSearchTotalCount("%"+searchWord+"%"));
				List<AtteandaceOptionType> atteandaceOptionTypeList =searchByAtteandaceOptionTypePageSizeAndNumber(dtoSearch);
				if(atteandaceOptionTypeList != null && !atteandaceOptionTypeList.isEmpty()){
					List<DtoAttedanceOptionType> dtoAttedanceOptionTypeList = new ArrayList<>();
					DtoAttedanceOptionType dtoAttedanceOptionType=null;
					for (AtteandaceOptionType atteandaceOptionType : atteandaceOptionTypeList) {
						dtoAttedanceOptionType = new DtoAttedanceOptionType(atteandaceOptionType);
						dtoAttedanceOptionType.setId(atteandaceOptionType.getId());
						dtoAttedanceOptionType.setSeqn(atteandaceOptionType.getSeqn());
						dtoAttedanceOptionType.setReasonIndx(atteandaceOptionType.getReasonIndx());
						dtoAttedanceOptionType.setReasonDesc(atteandaceOptionType.getReasonDesc());
						dtoAttedanceOptionType.setAtteandanceTypeSeqn(atteandaceOptionType.getAtteandanceTypeSeqn());
						dtoAttedanceOptionType.setAtteandanceTypeIndx(atteandaceOptionType.getAtteandanceTypeIndx());
						dtoAttedanceOptionType.setAtteandanceTypeDesc(atteandaceOptionType.getAtteandanceTypeDesc());
						dtoAttedanceOptionTypeList.add(dtoAttedanceOptionType);
					}
					dtoSearch.setRecords(dtoAttedanceOptionTypeList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchByAtteandaceOptionType(DtoSearch dtoSearch) {
		String condition="";
		if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
			switch(dtoSearch.getSortOn()){
			case "reasonIndx" : 
				condition+=dtoSearch.getSortOn();
				break;
			case "reasonDesc" : 
				condition+=dtoSearch.getSortOn();
				break;
			case "atteandanceTypeSeqn" : 
				condition+=dtoSearch.getSortOn();
				break;
			case "atteandanceTypeIndx" : 
				condition+=dtoSearch.getSortOn();
				break;
			case "atteandanceTypeDesc" : 
				condition+=dtoSearch.getSortOn();
				break;
			default:
				condition+="id";
			}
		}else{
			condition+="id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
		}
		dtoSearch.setCondition(condition);
		return dtoSearch;
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public List<AtteandaceOptionType> searchByAtteandaceOptionTypePageSizeAndNumber(DtoSearch dtoSearch) {
		List<AtteandaceOptionType> atteandaceOptionTypeList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
			
			if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
				atteandaceOptionTypeList = this.repositoryAtteandaceOptionType.predictiveAtteandaceOptionTypeSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if(dtoSearch.getSortBy().equals("ASC")){
				atteandaceOptionTypeList = this.repositoryAtteandaceOptionType.predictiveAtteandaceOptionTypeSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, dtoSearch.getCondition()));
			}else if(dtoSearch.getSortBy().equals("DESC")){
				atteandaceOptionTypeList = this.repositoryAtteandaceOptionType.predictiveAtteandaceOptionTypeSearchWithPagination("%"+dtoSearch.getSearchKeyword()+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, dtoSearch.getCondition()));
			}
			
		}
		
		return atteandaceOptionTypeList;
	}
	public DtoAttedanceOptionType getDtoAttedanceOptionTypeById(int id) {
		log.info("getDtoAttedanceOptionTypeById Method");
		DtoAttedanceOptionType dtoAttedanceOptionType = new DtoAttedanceOptionType();
		try {
			if (id > 0) {
				AtteandaceOptionType attedanceOptionType = repositoryAtteandaceOptionType.findByIdAndIsDeleted(id, false);
				if (attedanceOptionType != null) {
					dtoAttedanceOptionType = new DtoAttedanceOptionType(attedanceOptionType);
					dtoAttedanceOptionType.setId(attedanceOptionType.getId());
					dtoAttedanceOptionType.setSeqn(attedanceOptionType.getSeqn());
					dtoAttedanceOptionType.setReasonIndx(attedanceOptionType.getReasonIndx());
					dtoAttedanceOptionType.setReasonDesc(attedanceOptionType.getReasonDesc());
					dtoAttedanceOptionType.setAtteandanceTypeSeqn(attedanceOptionType.getAtteandanceTypeSeqn());
					dtoAttedanceOptionType.setAtteandanceTypeIndx(attedanceOptionType.getAtteandanceTypeIndx());
					dtoAttedanceOptionType.setAtteandanceTypeDesc(attedanceOptionType.getAtteandanceTypeDesc());
				} else {
					dtoAttedanceOptionType.setMessageType("ATTEDANCE_OPTION_TYPE_NOT_GETTING");
				}
			} else {
				dtoAttedanceOptionType.setMessageType("INVALID_ATTEDANCE_OPTION_TYPE_ID");
			}
			log.debug("AttedanceOptionType By Id is:"+dtoAttedanceOptionType.getId());
		} catch (Exception e) {
			
			log.error(e);
		}
		return dtoAttedanceOptionType;
	}
}
