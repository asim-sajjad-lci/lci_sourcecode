package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Position;
import com.bti.hcm.model.PositionCourseToLink;
import com.bti.hcm.model.TrainingCourseDetail;
import com.bti.hcm.model.TraningCourse;
import com.bti.hcm.model.dto.DtoPositionCourseToLink;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryPosition;
import com.bti.hcm.repository.RepositoryPositionCourseToLink;
import com.bti.hcm.repository.RepositoryTrainingCourseDetail;
import com.bti.hcm.repository.RepositoryTraningCourse;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("servicePositionCourseToLink")
public class ServicePositionCourseToLink {
	/**
	 * @Description LOGGER use for put a logger in PositionCourseToLink Service
	 */
	static Logger log = Logger.getLogger(ServicePositionCourseToLink.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in PositionCourseToLink service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PositionCourseToLink service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PositionCourseToLink service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired(required =  false)
	RepositoryPositionCourseToLink repositoryPositionCourseToLink;

	@Autowired(required =  false)
	RepositoryTraningCourse repositoryTraningCourse;
	
	@Autowired(required =  false)
	RepositoryTrainingCourseDetail repositoryTrainingClass;
	
	/**
	 * 
	 */
	@Autowired(required =  false)
	RepositoryPosition repositoryPosition ;
	
	/**
	 * @param dtoPositionCourseToLink
	 * @return
	 */
	public DtoPositionCourseToLink saveOrUpdate(DtoPositionCourseToLink dtoPositionCourseToLink) {
		log.info("PositionCourseToLink Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		PositionCourseToLink position = null;
		if (dtoPositionCourseToLink.getId() != null && dtoPositionCourseToLink.getId() > 0) {
			
			position = repositoryPositionCourseToLink.findByIdAndIsDeleted(dtoPositionCourseToLink.getId(), false);
			position.setUpdatedBy(loggedInUserId);
			position.setUpdatedDate(new Date());
		} else {
			position = new PositionCourseToLink();
			position.setCreatedDate(new Date());
			
			Integer rowId = repositoryPositionCourseToLink.getCountOfTotaPositionCourseToLink();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			position.setRowId(increment);
		}
		TraningCourse traningCourse =  repositoryTraningCourse.findOne(dtoPositionCourseToLink.getTraningCourseId());
		
		Position position2 = repositoryPosition.findOne(Integer.parseInt(dtoPositionCourseToLink.getPositionId()));
		
		TrainingCourseDetail courseDetail =	repositoryTrainingClass.findOne(dtoPositionCourseToLink.getTraningClassId());
		
		
		position.setTrainingClass(courseDetail);
		position.setPosition(position2);
		position.setTraningCourse(traningCourse);
		position.setSequence(dtoPositionCourseToLink.getSequence());
		position.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		position = repositoryPositionCourseToLink.saveAndFlush(position);
		log.debug("Position is:"+position.getId());
		return dtoPositionCourseToLink;
	}

	public DtoSearch getAll(DtoPositionCourseToLink dtoPositionCourseToLink) {
		log.info("getAllPostion Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoPositionCourseToLink.getPageNumber());
		dtoSearch.setPageSize(dtoPositionCourseToLink.getPageSize());
		dtoSearch.setTotalCount(repositoryPositionCourseToLink.getCountOfTotalPosition());
		List<PositionCourseToLink> positionCourseToLinkList = null;
		if (dtoPositionCourseToLink.getPageNumber() != null && dtoPositionCourseToLink.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoPositionCourseToLink.getPageNumber(), dtoPositionCourseToLink.getPageSize(), Direction.DESC, "createdDate");
			positionCourseToLinkList = repositoryPositionCourseToLink.findByIsDeleted(false, pageable);
		} else {
			positionCourseToLinkList = repositoryPositionCourseToLink.findByIsDeletedOrderByCreatedDateDesc(false);
		}
		
		List<DtoPositionCourseToLink> dtoPOsitionList=new ArrayList<>();
		if(positionCourseToLinkList!=null && !positionCourseToLinkList.isEmpty())
		{
			for (PositionCourseToLink position : positionCourseToLinkList) 
			{
				
				dtoPositionCourseToLink=new DtoPositionCourseToLink(position);
				if(position.getPosition()!=null && position.getTraningCourse()!=null){
					dtoPositionCourseToLink.setPositionId(position.getPosition().getPositionId());
					dtoPositionCourseToLink.setPositionTrainingDesc(position.getPosition().getDescription());
					dtoPositionCourseToLink.setPositionTrainingArbicDesc(position.getPosition().getArabicDescription());
					dtoPositionCourseToLink.setCourseDesc(position.getTraningCourse().getDesc());
					dtoPositionCourseToLink.setTraningCourseId(position.getTraningCourse().getId());
					if(position.getTrainingClass()!=null) {
						dtoPositionCourseToLink.setTraningClassIdName(position.getTrainingClass().getClassId());
						dtoPositionCourseToLink.setTraningClassId(position.getTrainingClass().getId());
					}
					
				}
				
				
				dtoPositionCourseToLink.setId(position.getId());
				dtoPOsitionList.add(dtoPositionCourseToLink);
			}
			dtoSearch.setRecords(dtoPOsitionList);
		}
		log.debug("All PositionCourseDetail List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoPositionCourseToLink delete(List<Integer> ids) {
		log.info("delete DtoPositionCourseDetail Method");
		DtoPositionCourseToLink dtoPosition = new DtoPositionCourseToLink();
		dtoPosition
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("POSITION_COURSE_LINK_DELETED", false));
		dtoPosition.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("POSTION_ASSOCIATED", false));
		List<DtoPositionCourseToLink> deletePosition = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("POSITION_COURSE_TO_LINK_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer positionId : ids) {
				
				PositionCourseToLink position = repositoryPositionCourseToLink.findOne(positionId);
				if(position.getListPositionCourseDetail().isEmpty()){
					DtoPositionCourseToLink dtoskillSetup2 = new DtoPositionCourseToLink();
					dtoskillSetup2.setId(positionId);
					repositoryPositionCourseToLink.deleteSinglePosition(true, loggedInUserId, positionId);
					deletePosition.add(dtoskillSetup2);
				}else{
					inValidDelete = true;
					invlidDeleteMessage.append(position.getId()+","); 
					
					
					
				}
				

			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoPosition.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoPosition.setMessageType("");
			}
			dtoPosition.setDelete(deletePosition);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete PositionCourseDetail :"+dtoPosition.getId());
		return dtoPosition;
	}

	public DtoPositionCourseToLink getByPositionId(Integer id) {
		log.info("getByPositionId Method");
		DtoPositionCourseToLink dtoPosition = new DtoPositionCourseToLink();
		if (id > 0) {
			PositionCourseToLink position = repositoryPositionCourseToLink.findByIdAndIsDeleted(id, false);
			if (position != null) {
				dtoPosition = new DtoPositionCourseToLink(position);
				
				
				dtoPosition.setId(position.getTraningCourseId());
				dtoPosition.setPositionId(position.getPosition().getPositionId());
				dtoPosition.setPositionTrainingDesc(position.getPosition().getDescription());
				dtoPosition.setPositionTrainingArbicDesc(position.getPosition().getArabicDescription());
				dtoPosition.setCourseDesc(position.getTraningCourse().getDesc());
				dtoPosition.setTraningCourseId(position.getTraningCourse().getId());
				dtoPosition.setId(position.getId());
				if(position.getTrainingClass()!=null) {
					dtoPosition.setTraningClassIdName(position.getTrainingClass().getClassId());
					dtoPosition.setTraningClassId(position.getTrainingClass().getId());
				}
			} else {
				dtoPosition.setMessageType("POSITION_COURSE_LINK_LIST_NOT_GETTING");

			}
		} else {
			dtoPosition.setMessageType("POSITION_COURSE_LINK_LIST_NOT_GETTING");

		}
		log.debug("position By Id is:"+dtoPosition.getId());
		return dtoPosition;
	}

	public DtoSearch search(DtoSearch dtoSearch) {
		log.info("searchPosition Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			String condition="";
					
			 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("positionId") || dtoSearch.getSortOn().equals("positionTrainingDesc") || dtoSearch.getSortOn().equals("positionTrainingArbicDesc") || dtoSearch.getSortOn().equals("traningCourseId")
							|| dtoSearch.getSortOn().equals("courseDesc")) {
						
						if(dtoSearch.getSortOn().equals("positionId")) {
							
							dtoSearch.setSortOn("position.positionId"); 
						}
					

						if(dtoSearch.getSortOn().equals("positionTrainingDesc")) {
							
							dtoSearch.setSortOn("position.description"); 
						}
						

						if(dtoSearch.getSortOn().equals("positionTrainingArbicDesc")) {
							
							dtoSearch.setSortOn("position.arabicDescription"); 
						}
						
						
                        if(dtoSearch.getSortOn().equals("traningCourseId")) {
							
							dtoSearch.setSortOn("position.traningId"); 
						}
                        

                        if(dtoSearch.getSortOn().equals("courseDesc")) {
							
							dtoSearch.setSortOn("position.desc"); 
						}
						
							
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			
			
			dtoSearch.setTotalCount(this.repositoryPositionCourseToLink.predictivePositionSearchTotalCount("%"+searchWord+"%",dtoSearch.getId()));
			List<PositionCourseToLink> positionCourseList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					positionCourseList = this.repositoryPositionCourseToLink.predictivePositionSearchWithPagination("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					positionCourseList = this.repositoryPositionCourseToLink.predictivePositionSearchWithPagination("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					positionCourseList =this.repositoryPositionCourseToLink.predictivePositionSearchWithPagination("%"+searchWord+"%",dtoSearch.getId(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			
			
			
			if(positionCourseList != null && !positionCourseList.isEmpty()){
				List<DtoPositionCourseToLink> dtoPositionCourseToLinkList = new ArrayList<>();
				DtoPositionCourseToLink doPositionCourseToLink=null;
				for (PositionCourseToLink positionCourseToLink : positionCourseList) {
					doPositionCourseToLink = new DtoPositionCourseToLink(positionCourseToLink);
					doPositionCourseToLink.setId(positionCourseToLink.getTraningCourseId());
					doPositionCourseToLink.setPositionId(positionCourseToLink.getPosition().getPositionId());
					doPositionCourseToLink.setPositionTrainingDesc(positionCourseToLink.getPosition().getDescription());
					doPositionCourseToLink.setPositionTrainingArbicDesc(positionCourseToLink.getPosition().getArabicDescription());
					
					if(positionCourseToLink.getTraningCourse()!=null) {
						doPositionCourseToLink.setCourseDesc(positionCourseToLink.getTraningCourse().getDesc());
						doPositionCourseToLink.setTraningCourseId(positionCourseToLink.getTraningCourse().getId());
						doPositionCourseToLink.setTraningCourseIdName(positionCourseToLink.getTraningCourse().getTraningId());
					}
					
					doPositionCourseToLink.setId(positionCourseToLink.getId());
					if(positionCourseToLink.getTrainingClass()!=null) {
						doPositionCourseToLink.setTraningClassIdName(positionCourseToLink.getTrainingClass().getClassId());
						doPositionCourseToLink.setTraningClassId(positionCourseToLink.getTrainingClass().getId());
						doPositionCourseToLink.setTraningClassName(positionCourseToLink.getTrainingClass().getClassName());
					}
					
					dtoPositionCourseToLinkList.add(doPositionCourseToLink);
				}
				dtoSearch.setRecords(dtoPositionCourseToLinkList);
			}
		}
		log.debug("Search PositionCourseToLin Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchTraningId(DtoSearch dtoSearch) {
		log.info("searchTraningId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			List<String> positionIdList = this.repositoryPositionCourseToLink.predictiveTraningIdSearchWithPagination("%"+searchWord+"%");
				if(!positionIdList.isEmpty()) {
					dtoSearch.setTotalCount(positionIdList.size());
					dtoSearch.setRecords(positionIdList.size());
				}
			dtoSearch.setIds(positionIdList);
		}
		
		return dtoSearch;
	}
}
