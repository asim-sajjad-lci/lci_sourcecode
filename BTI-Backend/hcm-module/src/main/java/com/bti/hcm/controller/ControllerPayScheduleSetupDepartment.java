/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoPayScheduleSetupDepartment;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePayScheduleSetupDepartment;
import com.bti.hcm.service.ServiceResponse;

/**
 * Description: Controller PayScheduleSetupDepartment
 * Name of Project: Hcm
 * Version: 0.0.1
 */
@RestController
@RequestMapping("/payScheduleSetupDepartment")
public class ControllerPayScheduleSetupDepartment extends BaseController{
	
	
	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupDepartment Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerPayScheduleSetupDepartment.class);
	
	/**
	 * @Description servicePayScheduleSetupDepartment Autowired here using annotation of spring for use of servicePayScheduleSetupDepartment method in this controller
	 */
	@Autowired(required=true)
	ServicePayScheduleSetupDepartment servicePayScheduleSetupDepartment;


	/**
	 * @Description serviceResponse Autowired here using annotation of spring for use of serviceResponse method in this controller
	 */
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupDepartment
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment) throws Exception {
		LOGGER.info("Create PayScheduleSetupDepartment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupDepartment = servicePayScheduleSetupDepartment.saveOrUpdatePayScheduleSetupDepartment(dtoPayScheduleSetupDepartment);
		responseMessage=displayMessage(dtoPayScheduleSetupDepartment, "PAY_SCHEDULE_SETUP_DEPARTMENT_CREATED", "PAY_SCHEDULE_SETUP_DEPARTMENT_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create PayScheduleSetupDepartment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupDepartment
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment) throws Exception {
		LOGGER.info("Update PayScheduleSetupDepartment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPayScheduleSetupDepartment = servicePayScheduleSetupDepartment.saveOrUpdatePayScheduleSetupDepartment(dtoPayScheduleSetupDepartment);
			responseMessage=displayMessage(dtoPayScheduleSetupDepartment, "PAY_SCHEDULE_SETUP_DEPARTMENT_UPDATED_SUCCESS", "PAY_SCHEDULE_SETUP_DEPARTMENT_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update PayScheduleSetupDepartment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupDepartment
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment) throws Exception {
		LOGGER.info("Delete PayScheduleSetupDepartment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPayScheduleSetupDepartment.getIds() != null && !dtoPayScheduleSetupDepartment.getIds().isEmpty()) {
				DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment2 = servicePayScheduleSetupDepartment.deletePayScheduleSetupDepartment(dtoPayScheduleSetupDepartment.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("PAY_SCHEDULE_SETUP_DEPARTMENT_LIST_DELETED", false), dtoPayScheduleSetupDepartment2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete PayScheduleSetupDepartment Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search PayScheduleSetupDepartment Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePayScheduleSetupDepartment.searchPayScheduleSetupDepartment(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "PAY_SCHEDULE_SETUP_DEPARTMENT_GET_ALL", "PAY_SCHEDULE_SETUP_DEPARTMENT_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search PayScheduleSetupDepartment Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	/**
	 * 
	 * @param request
	 * @param dtoPayScheduleSetupDepartment
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment) throws Exception {
		LOGGER.info("Get  ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartmentObj = servicePayScheduleSetupDepartment.getPayScheduleDepartmentIndexId(dtoPayScheduleSetupDepartment.getPayScheduleDepartmentIndexId());
			responseMessage=displayMessage(dtoPayScheduleSetupDepartmentObj, "PAY_SCHEDULE_SETUP_DEPARTMENT_GET_DETAIL", "PAY_SCHEDULE_SETUP_DEPARTMENT_NOT_GETTING", serviceResponse);

		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get ById Method:"+dtoPayScheduleSetupDepartment.getPayScheduleDepartmentIndexId());
		return responseMessage;
	}
}
