package com.bti.hcm.model.dto;

import java.math.BigInteger;
import java.util.List;

import com.bti.hcm.model.RetirementFundSetup;
import com.bti.hcm.util.UtilRandomKey;

public class DtoRetirementFundSetup extends DtoBase{
	private Integer id;
	private Integer planId;
	private String fundId;
	private Integer fundSequence;
	private String planDescription;
	private BigInteger fundActive;
	private List<DtoRetirementFundSetup> delete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getFundId() {
		return fundId;
	}

	public void setFundId(String fundId) {
		this.fundId = fundId;
	}

	public Integer getFundSequence() {
		return fundSequence;
	}

	public void setFundSequence(Integer fundSequence) {
		this.fundSequence = fundSequence;
	}

	public String getPlanDescription() {
		return planDescription;
	}

	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}

	public BigInteger getFundActive() {
		return fundActive;
	}

	public void setFundActive(BigInteger fundActive) {
		this.fundActive = fundActive;
	}

	public List<DtoRetirementFundSetup> getDelete() {
		return delete;
	}

	public void setDelete(List<DtoRetirementFundSetup> delete) {
		this.delete = delete;
	}

	public DtoRetirementFundSetup(RetirementFundSetup retirementFundSetup) {
		this.id = retirementFundSetup.getId();

		if (UtilRandomKey.isNotBlank(retirementFundSetup.getFundId())) {
			this.fundId = retirementFundSetup.getFundId();
		} else {
			this.fundId = "";
		}
		if (UtilRandomKey.isNotBlank(retirementFundSetup.getPlanDescription())) {
			this.planDescription = retirementFundSetup.getPlanDescription();
		} else {
			this.planDescription = "";
		}
	}

	public DtoRetirementFundSetup() {

	}

}
