package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR10340",indexes = {
        @Index(columnList = "HCMBULDINXD")
})
public class CalculateChecks extends HcmBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HCMBULDINXD")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "HCMDEFINX")
	private Default defaultIndx;

	@ManyToOne
	@JoinColumn(name = "HCMBULDINX")
	private BuildChecks buildCheck;

	@Column(name = "CHEKNMBR")
	private String checkNumForPayRoll;

	@Column(name = "HCMTRXPYRL")
	private String auditTransNum;

	@Column(name = "HCMEMPSEQN")
	private Integer hCMCodeSequence;

	@ManyToOne
	@JoinColumn(name = "EMPLOYINDX")
	private EmployeeMaster employeeMaster;

	@ManyToOne
	@JoinColumn(name = "DEPINDX")
	private Department department;

	@ManyToOne
	@JoinColumn(name = "POTINDX")
	private Position position;

	@ManyToOne
	@JoinColumn(name = "LOCINDX")
	private Location location;

	@Column(name = "HCMCDTYP")
	private short codeType;

	@ManyToOne
	@JoinColumn(name = "DIMINXVALUE")
	private FinancialDimensions dimensions;

	@Column(name = "HCMCDINDX")
	private Integer codeIndexID;

	@Column(name = "HCMCDAMT", precision = 13, scale = 10)
	private BigDecimal totalAmount;

	@Column(name = "HCMCDAAMT", precision = 13, scale = 10)
	private BigDecimal totalActualAmount;

	@Column(name = "HCMCDPRNT", precision = 13, scale = 10)
	private BigDecimal totalPercentCode;

	@Column(name = "HCMCDHRS")
	private Integer totalHours;

	@Column(name = "HCMBSDRTE", precision = 13, scale = 10)
	private BigDecimal rateOfBaseOnCode;

	@Column(name = "HCMCDBSD")
	private Integer codeBaseOnCodeIndex;

	@Column(name = "HCMCDFDT")
	private Date codefromPerioddate;

	@Column(name = "HCMCDTDT")
	private Date codeToPerioddate;

	@Column(name = "HCMRDSTS")
	private short recordStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCheckNumForPayRoll() {
		return checkNumForPayRoll;
	}

	public void setCheckNumForPayRoll(String checkNumForPayRoll) {
		this.checkNumForPayRoll = checkNumForPayRoll;
	}

	public String getAuditTransNum() {
		return auditTransNum;
	}

	public void setAuditTransNum(String auditTransNum) {
		this.auditTransNum = auditTransNum;
	}

	public Integer gethCMCodeSequence() {
		return hCMCodeSequence;
	}

	public void sethCMCodeSequence(Integer hCMCodeSequence) {
		this.hCMCodeSequence = hCMCodeSequence;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public short getCodeType() {
		return codeType;
	}

	public void setCodeType(short codeType) {
		this.codeType = codeType;
	}

	public Integer getCodeIndexID() {
		return codeIndexID;
	}

	public void setCodeIndexID(Integer codeIndexID) {
		this.codeIndexID = codeIndexID;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}

	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}

	public BigDecimal getTotalPercentCode() {
		return totalPercentCode;
	}

	public void setTotalPercentCode(BigDecimal totalPercentCode) {
		this.totalPercentCode = totalPercentCode;
	}

	public Integer getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public BigDecimal getRateOfBaseOnCode() {
		return rateOfBaseOnCode;
	}

	public void setRateOfBaseOnCode(BigDecimal rateOfBaseOnCode) {
		this.rateOfBaseOnCode = rateOfBaseOnCode;
	}

	public Integer getCodeBaseOnCodeIndex() {
		return codeBaseOnCodeIndex;
	}

	public void setCodeBaseOnCodeIndex(Integer codeBaseOnCodeIndex) {
		this.codeBaseOnCodeIndex = codeBaseOnCodeIndex;
	}

	public Date getCodefromPerioddate() {
		return codefromPerioddate;
	}

	public void setCodefromPerioddate(Date codefromPerioddate) {
		this.codefromPerioddate = codefromPerioddate;
	}

	public Date getCodeToPerioddate() {
		return codeToPerioddate;
	}

	public void setCodeToPerioddate(Date codeToPerioddate) {
		this.codeToPerioddate = codeToPerioddate;
	}

	public short getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(short recordStatus) {
		this.recordStatus = recordStatus;
	}

	public Default getDefaultIndx() {
		return defaultIndx;
	}

	public void setDefaultIndx(Default defaultIndx) {
		this.defaultIndx = defaultIndx;
	}

	public BuildChecks getBuildCheck() {
		return buildCheck;
	}

	public void setBuildCheck(BuildChecks buildCheck) {
		this.buildCheck = buildCheck;
	}

	public FinancialDimensions getDimensions() {
		return dimensions;
	}

	public void setDimensions(FinancialDimensions dimensions) {
		this.dimensions = dimensions;
	}

}
