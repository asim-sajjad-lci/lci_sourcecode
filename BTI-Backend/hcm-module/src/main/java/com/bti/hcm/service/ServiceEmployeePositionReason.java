package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeePositionReason;
import com.bti.hcm.model.dto.DtoEmployeePositionReason;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryEmployeePositionReason;

@Service("serviceEmployeePositionReason")
public class ServiceEmployeePositionReason {

	/**
	 * @Description LOGGER use for put a logger in EmployeePositionReason Service
	 */
	static Logger log = Logger.getLogger(ServiceEmployeePositionReason.class.getName());

	
	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in EmployeeMaster service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in EmployeeMaster service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryEmployeePositionReason Autowired here using annotation of spring for access of repositoryEmployeePositionReason method in EmployeePositionReason service
	 * 				In short Access EmployeePositionReason Query from Database using repositoryEmployeePositionReason.
	 */
	@Autowired
	RepositoryEmployeePositionReason repositoryEmployeePositionReason;
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {

			log.info("search EmployeePositionReason Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("positionReason")) {
						condition = dtoSearch.getSortOn();
					
				}else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
				
				List<EmployeePositionReason> employeePositionReasonList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeePositionReasonList = this.repositoryEmployeePositionReason.predictiveEmployeePositionReasonSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeePositionReasonList = this.repositoryEmployeePositionReason.predictiveEmployeePositionReasonSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeePositionReasonList = this.repositoryEmployeePositionReason.predictiveEmployeePositionReasonSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				if(employeePositionReasonList != null && !employeePositionReasonList.isEmpty()){
					List<DtoEmployeePositionReason> dtoEmployeeContactsList = new ArrayList<>();
					DtoEmployeePositionReason dtoEmployeePositionReason=null;
					for (EmployeePositionReason employeePositionReason : employeePositionReasonList) {
						dtoEmployeePositionReason = new DtoEmployeePositionReason(employeePositionReason);
						dtoEmployeePositionReason.setId(employeePositionReason.getId());
						dtoEmployeePositionReason.setPositionReason(employeePositionReason.getPositionReason());
						
						dtoEmployeeContactsList.add(dtoEmployeePositionReason);
					}
					dtoSearch.setRecords(dtoEmployeeContactsList);
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllForDroupDown(DtoSearch dtoSearch) {
		try {
			log.info("search EmployeePositionReason Method");
				List<EmployeePositionReason> employeePositionReasonList =null;
				employeePositionReasonList = this.repositoryEmployeePositionReason.findByIsDeleted(false);
				if(employeePositionReasonList != null && !employeePositionReasonList.isEmpty()){
					List<DtoEmployeePositionReason> dtoEmployeeContactsList = new ArrayList<>();
					DtoEmployeePositionReason dtoEmployeePositionReason=null;
					for (EmployeePositionReason employeePositionReason : employeePositionReasonList) {
						dtoEmployeePositionReason = new DtoEmployeePositionReason(employeePositionReason);
						dtoEmployeePositionReason.setId(employeePositionReason.getId());
						dtoEmployeePositionReason.setPositionReason(employeePositionReason.getPositionReason());
						dtoEmployeeContactsList.add(dtoEmployeePositionReason);
					}
					dtoSearch.setRecords(dtoEmployeeContactsList);
				}
			
			log.debug("Search EmployeePositionReason Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllPositionReason(DtoSearch dtoSearch) {

		try {
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				
					String condition="";
				
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("employeeId")) {
						
						     if(dtoSearch.getSortOn().equals("cityName")) {
						    	 dtoSearch.setSortOn("city");
						     }
						    	 
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}	
				
				
				
				dtoSearch.setTotalCount(this.repositoryEmployeePositionReason.predictiveGetAllPositionReasonSearchTotalCount("%"+searchWord+"%"));
				List<EmployeePositionReason> employeePositionReasonList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeePositionReasonList =this.repositoryEmployeePositionReason.predictiveEmployeeGetAllPositionReasonSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeePositionReasonList = this.repositoryEmployeePositionReason.predictiveEmployeeGetAllPositionReasonSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeePositionReasonList = this.repositoryEmployeePositionReason.predictiveEmployeeGetAllPositionReasonSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
				
				
				if(employeePositionReasonList != null && !employeePositionReasonList.isEmpty()){
					List<DtoEmployeePositionReason> dtoEmployeePositionReasonList = new ArrayList<>();
					for (EmployeePositionReason employeePositionReason : employeePositionReasonList) {
						DtoEmployeePositionReason dtoEmployeePositionReason = new DtoEmployeePositionReason();
						dtoEmployeePositionReason.setId(employeePositionReason.getId());
						dtoEmployeePositionReason.setPositionReason(employeePositionReason.getPositionReason());
						dtoEmployeePositionReasonList.add(dtoEmployeePositionReason);
					}
					dtoSearch.setRecords(dtoEmployeePositionReasonList);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}

	
	
	
	
}
