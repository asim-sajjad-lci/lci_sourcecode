package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.BuildChecks;
import com.bti.hcm.model.BuildPayrollCheckByDeductions;
import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.EmployeeDeductionMaintenance;
import com.bti.hcm.model.dto.DtoBuildPayrollCheckByDeductions;
import com.bti.hcm.model.dto.DtoDeductionCode;
import com.bti.hcm.model.dto.DtoDeductionCodeDisplay;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByDeductions;
import com.bti.hcm.repository.RepositoryBuildPayrollCheckByPayCodes;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryPayCode;

@Service("/serviceBuildPayrollCheckByDeductions")
public class ServiceBuildPayrollCheckByDeductions {
	
static Logger log = Logger.getLogger(ServiceBuildPayrollCheckByDeductions.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired(required = false)
	RepositoryBuildPayrollCheckByPayCodes repositoryBuildPayrollCheckByPayCodes;
	
	@Autowired(required = false)
	RepositoryPayCode repositoryPayCode;
	
	@Autowired(required = false)
	RepositoryBuildChecks repositoryBuildChecks;
	
	@Autowired(required = false)
	RepositoryBuildPayrollCheckByDeductions repositoryBuildPayrollCheckByDeductions;

	@Autowired(required = false)
	RepositoryDeductionCode repositoryDeductionCode;
	
	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;
	
	public DtoBuildPayrollCheckByDeductions saveOrUpdate(DtoBuildPayrollCheckByDeductions dtoBuildPayrollCheckByDeductions) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate EmployeeContacts Method");
			
			BuildChecks buildChecks = new BuildChecks();
			if(dtoBuildPayrollCheckByDeductions.getListBuildChecks() != null && dtoBuildPayrollCheckByDeductions.getListBuildChecks().get(0).getBuildChecks().getId()!= null) {
				buildChecks = 	repositoryBuildChecks.findOne(dtoBuildPayrollCheckByDeductions.getListBuildChecks().get(0).getBuildChecks().getId());	
			}
			
			
			if(dtoBuildPayrollCheckByDeductions.getListBuildChecks().get(0)!=null && dtoBuildPayrollCheckByDeductions.getListBuildChecks().get(0).getBuildChecks().getId()>0) {
				repositoryBuildPayrollCheckByDeductions.deleteByBuildCheckId(true,loggedInUserId,dtoBuildPayrollCheckByDeductions.getListBuildChecks().get(0).getBuildChecks().getId());
				}
			
				
				for (DtoDeductionCode dtodeductionCode : dtoBuildPayrollCheckByDeductions.getDeductionCode()) {
					DeductionCode deductionCode= repositoryDeductionCode.findByIdAndIsDeleted(dtodeductionCode.getId(), false);
					
					BuildPayrollCheckByDeductions buildPayrollCheckByDeductions=new BuildPayrollCheckByDeductions();
					buildPayrollCheckByDeductions.setCreatedDate(new Date());
					
					Integer rowId = repositoryBuildPayrollCheckByDeductions.findAll().size();
					Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					
					buildPayrollCheckByDeductions.setRowId(increment);
					buildPayrollCheckByDeductions.setBuildChecks(buildChecks);
					buildPayrollCheckByDeductions.setDeductionCode(deductionCode);
					repositoryBuildPayrollCheckByDeductions.saveAndFlush(buildPayrollCheckByDeductions);
					
					
				}
			
			
		} catch (Exception e) {
			log.error(e);
			
		}
		return dtoBuildPayrollCheckByDeductions;
	}

	public DtoSearch getAllByCodeType(DtoBuildPayrollCheckByDeductions dtoBuildPayrollCheckByDeductions) {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoDeductionCodeDisplay> listResponse=new ArrayList<>();
		List<BuildPayrollCheckByDeductions> buildcheckList=this.repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(dtoBuildPayrollCheckByDeductions.getId());
		for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildcheckList) {
			DtoDeductionCodeDisplay dtoBuildPayrollCheckByDeductionss=new DtoDeductionCodeDisplay();
			if(buildPayrollCheckByDeductions.getDeductionCode()!=null) {
				dtoBuildPayrollCheckByDeductionss.setDeductionCodeParimaryId(buildPayrollCheckByDeductions.getDeductionCode().getId());
				dtoBuildPayrollCheckByDeductionss.setDiductionId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
			}
			if(buildPayrollCheckByDeductions.getBuildChecks()!=null) {
				dtoBuildPayrollCheckByDeductionss.setBuildChecksId(buildPayrollCheckByDeductions.getBuildChecks().getId());
			}
			listResponse.add(dtoBuildPayrollCheckByDeductionss);
			
		}
		List<DeductionCode>list=this.repositoryDeductionCode.findByIsDeletedAndInActive(false,false);
		if(list!=null && !list.isEmpty()) {
			for (DeductionCode deductionCode : list) {
				if(deductionCode.isTransction()==false) {
					long count =listResponse.stream().filter(d-> d.getDeductionCodeParimaryId().equals(deductionCode.getId())).count();
					if(count == 0) {
						DtoDeductionCodeDisplay dtoBuildPayrollCheckByDeductionss=new DtoDeductionCodeDisplay();
						dtoBuildPayrollCheckByDeductionss.setDeductionCodeParimaryId(deductionCode.getId());
						dtoBuildPayrollCheckByDeductionss.setDiductionId(deductionCode.getDiductionId());
						listResponse.add(dtoBuildPayrollCheckByDeductionss);
				}
				}
			}
		}
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	}
	
	public DtoSearch getAllByCodeTypeHm(DtoBuildPayrollCheckByDeductions dtoBuildPayrollCheckByDeductions) {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoDeductionCodeDisplay> listResponse=new ArrayList<>();
		List<BuildPayrollCheckByDeductions> buildcheckList=this.repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(dtoBuildPayrollCheckByDeductions.getId());
		for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildcheckList) {
			DtoDeductionCodeDisplay dtoDeductionCodeDisplay=new DtoDeductionCodeDisplay();
			if(buildPayrollCheckByDeductions.getDeductionCode()!=null) {
				dtoDeductionCodeDisplay.setDeductionCodeParimaryId(buildPayrollCheckByDeductions.getDeductionCode().getId());
				dtoDeductionCodeDisplay.setDiductionId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
						
			}
			if(buildPayrollCheckByDeductions.getBuildChecks()!=null) {
				dtoDeductionCodeDisplay.setBuildChecksId(buildPayrollCheckByDeductions.getBuildChecks().getId());
			}
			listResponse.add(dtoDeductionCodeDisplay);
			
		}
		//List<DeductionCode>list=this.repositoryDeductionCode.findByIsDeletedAndInActive(false,false);
		List<DeductionCode>list=this.repositoryEmployeeDeductionMaintenance.findByStartEndDate1(dtoBuildPayrollCheckByDeductions.getStartDate(),dtoBuildPayrollCheckByDeductions.getEndDate());
		if(list!=null && !list.isEmpty()) {
			for (DeductionCode deductionCode : list) {
				//List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = repositoryEmployeeDeductionMaintenance.findByBatchId(deductionCode.getId());
				if(deductionCode.isTransction()==false) {
					if(deductionCode.isInActive()==false) {
						long count =listResponse.stream().filter(d-> d.getDeductionCodeParimaryId().equals(deductionCode.getId())).count();
						if(count == 0) {
							DtoDeductionCodeDisplay dtoBuildPayrollCheckByDeductionss=new DtoDeductionCodeDisplay();
							dtoBuildPayrollCheckByDeductionss.setDeductionCodeParimaryId(deductionCode.getId());
							dtoBuildPayrollCheckByDeductionss.setDiductionId(deductionCode.getDiductionId());
							//dtoBuildPayrollCheckByDeductionss.setEmployeeDeductionMaintenanceList(employeeDeductionMaintenanceList);
							listResponse.add(dtoBuildPayrollCheckByDeductionss);
					}
					}
					
				}
			}
		}
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	}
	
	public DtoSearch getAllByCodeTypeHm1(DtoBuildPayrollCheckByDeductions dtoBuildPayrollCheckByDeductions) {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoDeductionCodeDisplay> listResponse=new ArrayList<>();
		List<BuildPayrollCheckByDeductions> buildcheckList=this.repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(dtoBuildPayrollCheckByDeductions.getId());
		for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildcheckList) {
			DtoDeductionCodeDisplay dtoDeductionCodeDisplay=new DtoDeductionCodeDisplay();
			if(buildPayrollCheckByDeductions.getDeductionCode()!=null) {
				dtoDeductionCodeDisplay.setDeductionCodeParimaryId(buildPayrollCheckByDeductions.getDeductionCode().getId());
				dtoDeductionCodeDisplay.setDiductionId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
						
			}
			if(buildPayrollCheckByDeductions.getBuildChecks()!=null) {
				dtoDeductionCodeDisplay.setBuildChecksId(buildPayrollCheckByDeductions.getBuildChecks().getId());
			}
			listResponse.add(dtoDeductionCodeDisplay);
			
		}
		//List<DeductionCode>list=this.repositoryDeductionCode.findByIsDeletedAndInActive(false,false);
		
		//List<DeductionCode>list=this.repositoryEmployeeDeductionMaintenance.findByStartEndDate(dtoBuildPayrollCheckByDeductions.getStartDate(),dtoBuildPayrollCheckByDeductions.getEndDate());
		List<DeductionCode>list=this.repositoryEmployeeDeductionMaintenance.findByStartEndDate2(dtoBuildPayrollCheckByDeductions.getStartDate(),dtoBuildPayrollCheckByDeductions.getEndDate());
		if(list!=null && !list.isEmpty()) {
			
			for (DeductionCode deductionCode : list) {
				for (EmployeeDeductionMaintenance  employeeDeductionMaintenance : deductionCode.getListEmployeeDeductionMaintenance()) {
					if(deductionCode.isTransction()==false) {
						if(deductionCode.isInActive()==false) {
								long count =listResponse.stream().filter(d-> d.getDeductionCodeParimaryId().equals(employeeDeductionMaintenance.getDeductionCode().getId())).count();
								if(count == 0) {
									DtoDeductionCodeDisplay dtoBuildPayrollCheckByDeductionss=new DtoDeductionCodeDisplay();
									dtoBuildPayrollCheckByDeductionss.setDeductionCodeParimaryId(deductionCode.getId());
									dtoBuildPayrollCheckByDeductionss.setDiductionId(deductionCode.getDiductionId());
									//dtoBuildPayrollCheckByDeductionss.setEmployeeDeductionMaintenanceList(employeeDeductionMaintenanceList);
									listResponse.add(dtoBuildPayrollCheckByDeductionss);
								}
									
								
							
						}
					}
				}
			}
			
		/*	for (DeductionCode deductionCode : list) {
				//List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = repositoryEmployeeDeductionMaintenance.findByBatchId(deductionCode.getId());
				if(deductionCode.isTransction()==false) {
					if(deductionCode.isInActive()==false) {
						if(deductionCode.getStartDate()!=null && deductionCode.getEndDate()!=null) {

								if(dtoBuildPayrollCheckByDeductions.getEndDate().getYear()+1900<=deductionCode.getEndDate().getYear()+1900) {
									long count =listResponse.stream().filter(d-> d.getDeductionCodeParimaryId().equals(deductionCode.getId())).count();
									if(count == 0) {
										DtoDeductionCodeDisplay dtoBuildPayrollCheckByDeductionss=new DtoDeductionCodeDisplay();
										dtoBuildPayrollCheckByDeductionss.setDeductionCodeParimaryId(deductionCode.getId());
										dtoBuildPayrollCheckByDeductionss.setDiductionId(deductionCode.getDiductionId());
										//dtoBuildPayrollCheckByDeductionss.setEmployeeDeductionMaintenanceList(employeeDeductionMaintenanceList);
										listResponse.add(dtoBuildPayrollCheckByDeductionss);
									}
								}
											
										
											
											
											
							
								
						}
						
					}
					
				}
			}*/
		}
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	}
	public DtoSearch getAllByCodeTypeHm2(DtoBuildPayrollCheckByDeductions dtoBuildPayrollCheckByDeductions) {
		DtoSearch dtoSearch=new DtoSearch();
		List<DtoDeductionCodeDisplay> listResponse=new ArrayList<>();
		List<BuildPayrollCheckByDeductions> buildcheckList=this.repositoryBuildPayrollCheckByDeductions.findByBuildChecksId(dtoBuildPayrollCheckByDeductions.getId());
		for (BuildPayrollCheckByDeductions buildPayrollCheckByDeductions : buildcheckList) {
			DtoDeductionCodeDisplay dtoDeductionCodeDisplay=new DtoDeductionCodeDisplay();
			if(buildPayrollCheckByDeductions.getDeductionCode()!=null) {
				dtoDeductionCodeDisplay.setDeductionCodeParimaryId(buildPayrollCheckByDeductions.getDeductionCode().getId());
				dtoDeductionCodeDisplay.setDiductionId(buildPayrollCheckByDeductions.getDeductionCode().getDiductionId());
						
			}
			if(buildPayrollCheckByDeductions.getBuildChecks()!=null) {
				dtoDeductionCodeDisplay.setBuildChecksId(buildPayrollCheckByDeductions.getBuildChecks().getId());
			}
			listResponse.add(dtoDeductionCodeDisplay);
			
		}
		//List<DeductionCode>list=this.repositoryDeductionCode.findByIsDeletedAndInActive(false,false);
		
		List<DeductionCode>list=this.repositoryEmployeeDeductionMaintenance.findByStartEndDate4(dtoBuildPayrollCheckByDeductions.getStartDate(),dtoBuildPayrollCheckByDeductions.getEndDate());
		//List<DeductionCode>list=this.repositoryEmployeeDeductionMaintenance.findByDeductionCode1();
		if(list!=null && !list.isEmpty()) {
			for (DeductionCode deductionCode : list) {
				//List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList = repositoryEmployeeDeductionMaintenance.findByBatchId(deductionCode.getId());
				if(deductionCode.isTransction()==false) {
					if(deductionCode.isInActive()==false) {
						
						long count =listResponse.stream().filter(d-> d.getDeductionCodeParimaryId().equals(deductionCode.getId())).count();
						if(count == 0) {
							DtoDeductionCodeDisplay dtoBuildPayrollCheckByDeductionss=new DtoDeductionCodeDisplay();
							dtoBuildPayrollCheckByDeductionss.setDeductionCodeParimaryId(deductionCode.getId());
							dtoBuildPayrollCheckByDeductionss.setDiductionId(deductionCode.getDiductionId());
							//dtoBuildPayrollCheckByDeductionss.setEmployeeDeductionMaintenanceList(employeeDeductionMaintenanceList);
							listResponse.add(dtoBuildPayrollCheckByDeductionss);
						}
					}
					
				}
			}
		}
		dtoSearch.setRecords(listResponse);
		return dtoSearch;
	}
	
	

}
