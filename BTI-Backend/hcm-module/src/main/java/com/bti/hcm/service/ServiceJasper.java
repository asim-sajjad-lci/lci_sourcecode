package com.bti.hcm.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Distribution;
import com.bti.hcm.model.dto.DtoDistribrution;
import com.bti.hcm.model.dto.DtoJasperReport;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryDistribution;
import com.bti.hcm.repository.RepositoryEmployeeMaster;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service("serviceJasper")
public class ServiceJasper {

	static Logger log = Logger.getLogger(ServiceJasper.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired(required = false)
	ServiceCalculateChecks serviceCalculateChecks;

	@Autowired(required = false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired(required = false)
	RepositoryDistribution repositoryDistribution;

	public DtoSearch pdfFromXmlFile(DtoSearch dtoSearch,HttpServletRequest request) throws JRException, IOException {

		String empId = "";
		JasperReport jasperReport = JasperCompileManager
				.compileReport(System.getProperty("user.dir")+File.separator+"data-source"+File.separator+"jasper"+File.separator+"jrxml-config"+File.separator+"payslip.jrxml");
		List<Distribution> list = repositoryDistribution.getAllByEmployeeIdAndMonthAndYear(dtoSearch.getId(),dtoSearch.getFrom(),dtoSearch.getTo());
		List<DtoDistribrution> listDtoDistribrution = serviceCalculateChecks.getDtoDistribrution(list);
		List<DtoJasperReport> listDtoJasperReport = new ArrayList<>();
		if(!listDtoDistribrution.isEmpty()) {
			DtoDistribrution dtoDistribrution = new DtoDistribrution();
			dtoDistribrution = listDtoDistribrution.get(0);
			DtoJasperReport dtoJasperReport = new DtoJasperReport();
			dtoJasperReport.setEmployeeId(dtoDistribrution.getEmployeeId());
			dtoJasperReport.setEmployeeFirstName(dtoDistribrution.getEmployeeFirstName());
			dtoJasperReport.setDepartment(dtoDistribrution.getDescription());
			dtoJasperReport.setPosition(dtoDistribrution.getPositionId());
			dtoJasperReport.setLocation(dtoDistribrution.getLocationId());
			empId = dtoDistribrution.getEmployeeId();
			for (Entry<String, BigDecimal> entry : dtoDistribrution.getListOfBenefit().entrySet()) {
				if (entry.getKey().contains("overtime") || entry.getKey().contains("Overtime")) {
					dtoJasperReport.setOvertime(entry.getValue());
				}
				if (entry.getKey().contains("basic") ||entry.getKey().contains("Basic")) {
					dtoJasperReport.setBasicSalary(entry.getValue());
				}
				if (entry.getKey().contains("Transportation")) {
					dtoJasperReport.setTransportation(entry.getValue());
				}
				if (entry.getKey().contains("housing") || entry.getKey().contains("Housing")) {
					dtoJasperReport.setHousing(entry.getValue());
				}
				if (entry.getKey().contains("earning") || entry.getKey().contains("Earning")) {
					dtoJasperReport.setOtherEarning(entry.getValue());
				}
			}
			
			for (Entry<String, BigDecimal> entry : dtoDistribrution.getListOfDeduction().entrySet()) {
				if (entry.getKey().contains("personal") || entry.getKey().contains("Personal")) {
					dtoJasperReport.setPersonalLoan(entry.getValue());
				}
				if (entry.getKey().contains("Housing")) {
					dtoJasperReport.setHousingLoan(entry.getValue());
				}
				if (entry.getKey().contains("Violation")) {
					dtoJasperReport.setViolation(entry.getValue());
				}
				if (entry.getKey().contains("GOZI")) {
					dtoJasperReport.setGozi(entry.getValue());
				}
				if (entry.getKey().contains("deductions") || entry.getKey().contains("Deduction")) {
					dtoJasperReport.setOtherDeduction(entry.getValue());
				}
			}
			dtoJasperReport.setTotalEarning(dtoDistribrution.getTotalEarnings());
			dtoJasperReport.setTotalDeduction(dtoDistribrution.getTotalDeductions());
			dtoJasperReport.setBankname(dtoDistribrution.getBankName());
		
				dtoJasperReport.setAccountNo(dtoDistribrution.getBankAcountnumber());	
		
			dtoJasperReport.setNetPay(dtoDistribrution.getNetPay());
			listDtoJasperReport.add(dtoJasperReport);
		
		JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(listDtoJasperReport, false);
		Map parameters = new HashMap();

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
		

		File outDir = new File(System.getProperty("user.dir")+File.separator+"data-source");
		if(!outDir.exists()) {
			outDir.mkdirs();
		}
		JasperExportManager.exportReportToHtmlFile(jasperPrint,System.getProperty("user.dir")+File.separator+"data-source"+File.separator+"jasper"+File.separator+"reports"+File.separator+empId+".html");
		JasperExportManager.exportReportToPdfFile(jasperPrint,System.getProperty("user.dir")+File.separator+"data-source"+File.separator+"jasper"+File.separator+"reports"+File.separator+empId+".pdf");
		String location = System.getProperty("user.dir")+File.separator+"data-source"+File.separator+"jasper"+File.separator+"reports"+File.separator+empId+".html";
		
		
		BufferedReader reader = new BufferedReader(new FileReader(location));
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;
		String ls = System.getProperty("line.separator");
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}
		stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		reader.close();

		String content = stringBuilder.toString();
		dtoSearch.setFileLocation(content);

		}else {
			
		}
		return dtoSearch;
	}
}
