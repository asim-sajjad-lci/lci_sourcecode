/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.PayScheduleSetupEmployee;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO PayScheduleSetupEmployee class having getter and setter for fields (POJO)
 * Name of Project: Hrm Created on: December 08, 2017
 * Version: 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoPayScheduleSetupEmployee extends DtoBase{
	
	private Integer paySchedulEmployeeIndexId;
	private int employeeIndexId;
	private Integer payScheduleSetup;
	private String employeeFullName;
	private Boolean payScheduleStatus;
	private Integer employeeMasterId;
	private Integer payScheduleSetupId;
	private List<DtoPayScheduleSetupEmployee> deletPayScheduleSetupEmployee;
	private Integer employeeId;
	
	public Integer getPaySchedulEmployeeIndexId() {
		return paySchedulEmployeeIndexId;
	}
	public void setPaySchedulEmployeeIndexId(Integer paySchedulEmployeeIndexId) {
		this.paySchedulEmployeeIndexId = paySchedulEmployeeIndexId;
	}
	public Integer getPayScheduleSetup() {
		return payScheduleSetup;
	}
	public void setPayScheduleSetup(Integer payScheduleSetup) {
		this.payScheduleSetup = payScheduleSetup;
	}
	public String getEmployeeFullName() {
		return employeeFullName;
	}
	public void setEmployeeFullName(String employeeFullName) {
		this.employeeFullName = employeeFullName;
	}
	public Boolean getPayScheduleStatus() {
		return payScheduleStatus;
	}
	public void setPayScheduleStatus(Boolean payScheduleStatus) {
		this.payScheduleStatus = payScheduleStatus;
	}
	public Integer getEmployeeMasterId() {
		return employeeMasterId;
	}
	public void setEmployeeMasterId(Integer employeeMasterId) {
		this.employeeMasterId = employeeMasterId;
	}
	
	
	public Integer getPayScheduleSetupId() {
		return payScheduleSetupId;
	}
	public void setPayScheduleSetupId(Integer payScheduleSetupId) {
		this.payScheduleSetupId = payScheduleSetupId;
	}
	public List<DtoPayScheduleSetupEmployee> getDeletPayScheduleSetupEmployee() {
		return deletPayScheduleSetupEmployee;
	}
	public void setDeletPayScheduleSetupEmployee(List<DtoPayScheduleSetupEmployee> deletPayScheduleSetupEmployee) {
		this.deletPayScheduleSetupEmployee = deletPayScheduleSetupEmployee;
	}
	public DtoPayScheduleSetupEmployee() {
	}
	
	public DtoPayScheduleSetupEmployee(PayScheduleSetupEmployee payScheduleSetupEmployee) {}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public int getEmployeeIndexId() {
		return employeeIndexId;
	}
	public void setEmployeeIndexId(int employeeIndexId) {
		this.employeeIndexId = employeeIndexId;
	}
	
	
}
