package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoManualChecksDetails;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceManualChecksDetails;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/manualChecksDetails")
public class ControllerManualChecksDetails extends BaseController{
	
	

	private static final Logger LOGGER = Logger.getLogger(ControllerManualChecksDetails.class);
	
	@Autowired
	ServiceManualChecksDetails serviceManualChecksDetails;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoManualChecksDetails dtoManualChecksDetails) throws Exception {
		LOGGER.info("Create manualChecksDistribution Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoManualChecksDetails  = serviceManualChecksDetails.saveOrUpdate(dtoManualChecksDetails);
			if (dtoManualChecksDetails != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("MANUAL_CHECKSLIST_DETAILS", false), dtoManualChecksDetails);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("MANUAL_CHECKSLIST_DETAILS_NOT_CREATED", false), dtoManualChecksDetails);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Create manualChecksDistribution Method:"+responseMessage.getMessage());
		return responseMessage;
	}
}
