package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.AccrualPeriodSetting;
import com.bti.hcm.model.dto.DtoAccrualPeriodSetting;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryAccrualPeriodSetting;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceAccrualPeriodSetting")
public class ServiceAccrualPeriodSetting {

    static Logger log = Logger.getLogger(ServiceAccrualPeriodSetting.class.getName());

	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryAccrualPeriodSetting Autowired here using annotation of spring for access of repositoryAccrualPeriodSetting method in AccrualPeriodSetting service
	 * 				In short Access AccrualPeriodSetting Query from Database using repositoryAccrualPeriodSetting.
	 */
	@Autowired
	RepositoryAccrualPeriodSetting repositoryAccrualPeriodSetting;
	
	
	public DtoAccrualPeriodSetting saveOrUpdate(DtoAccrualPeriodSetting dtoAccrualPeriodSetting) {
	        try {
	        	log.info("saveOrUpdate Method");
	    		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
	    		AccrualPeriodSetting accrualPeriodSetting = null;
	    		if (dtoAccrualPeriodSetting.getId() != null && dtoAccrualPeriodSetting.getId() > 0) {
	    			accrualPeriodSetting = repositoryAccrualPeriodSetting.findByIdAndIsDeleted(dtoAccrualPeriodSetting.getId(), false);
	    			accrualPeriodSetting.setUpdatedBy(loggedInUserId);
	    			accrualPeriodSetting.setUpdatedDate(new Date());
	    		} else {
	    			accrualPeriodSetting = new AccrualPeriodSetting();
	    			accrualPeriodSetting.setCreatedDate(new Date());
	    			Integer rowId = repositoryAccrualPeriodSetting.getCountOfTotalAccruals();
	    			Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					accrualPeriodSetting.setRowId(increment);
	    		}
	    		
	    		accrualPeriodSetting.setYear(dtoAccrualPeriodSetting.getYear());
	    		accrualPeriodSetting.setPeriodType(dtoAccrualPeriodSetting.getPeriodType());
	    		accrualPeriodSetting.setSequence(dtoAccrualPeriodSetting.getSequence());
	    		accrualPeriodSetting.setStartDate(dtoAccrualPeriodSetting.getStartDate());
	    		accrualPeriodSetting.setEndData(dtoAccrualPeriodSetting.getEndDate());
	    		accrualPeriodSetting.setAccrued(dtoAccrualPeriodSetting.isAccrued());
	    		accrualPeriodSetting.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
	    		accrualPeriodSetting.setRowId(loggedInUserId);
	    		repositoryAccrualPeriodSetting.saveAndFlush(accrualPeriodSetting);
	    		log.debug("AccrualPeriod is:"+dtoAccrualPeriodSetting.getId());
			} catch (Exception e) {
				log.error(e);
			}
		return dtoAccrualPeriodSetting;
	}
	
	public DtoAccrualPeriodSetting delete(List<Integer> ids) {
		log.info("delete DtoAccrualPeriod Method");
		DtoAccrualPeriodSetting dtoAccrualPeriodSetting = new DtoAccrualPeriodSetting();
		dtoAccrualPeriodSetting.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted(MessageConstant.ACCRUAL_PERIOED_DELETED, false));
		dtoAccrualPeriodSetting.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("ACCRUAL_PERIOED_DELETED", false));
		List<DtoAccrualPeriodSetting> deleteAccrualPeriod = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("ACCRUAL_PERIOED_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer deAccrualId : ids) {
				AccrualPeriodSetting accrualPeriod = repositoryAccrualPeriodSetting.findOne(deAccrualId);
				
				if(accrualPeriod!=null) {
					DtoAccrualPeriodSetting accrualPeriodSetting = new DtoAccrualPeriodSetting();
					accrualPeriodSetting.setId(accrualPeriod.getId());
					repositoryAccrualPeriodSetting.deleteSingleAccrualPeriodSetting(true, loggedInUserId, deAccrualId);
					deleteAccrualPeriod.add(accrualPeriodSetting);
				}
				else{
					inValidDelete = true;
				}
			}
			
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoAccrualPeriodSetting.setMessageType(invlidDeleteMessage.toString());
				
			}
			
			
			if(!inValidDelete){
				dtoAccrualPeriodSetting.setMessageType("ACCRUAL_PERIOED_DELETED");
				
			}
			
			dtoAccrualPeriodSetting.setDelete(deleteAccrualPeriod);
		} 
		catch (NumberFormatException e) {
			log.error(e);
		}
		
		
		log.debug("Delete AccrualPeriod :"+dtoAccrualPeriodSetting.getId());
		return dtoAccrualPeriodSetting;
	}

	
	
	public DtoSearch search(DtoSearch dtoSearch) {
 
		try {
			log.info("search AccrualPeriod Method");
			if(dtoSearch != null){
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					
					if(dtoSearch.getSortOn().equals("year") || dtoSearch.getSortOn().equals("periodType")
							|| dtoSearch.getSortOn().equals("sequence")
							|| dtoSearch.getSortOn().equals("startData")|| dtoSearch.getSortOn().equals("endData")
							|| dtoSearch.getSortOn().equals("accrued")) {
						condition=dtoSearch.getSortOn();
					}else {
						condition="id";
					}
					
				}else{
					condition+="id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
				
				dtoSearch.setTotalCount(this.repositoryAccrualPeriodSetting.predictiveAccrualPeriodSettingSearchTotalCount());
				List<AccrualPeriodSetting> accrualPeriodList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						accrualPeriodList = this.repositoryAccrualPeriodSetting.predictiveAccrualPeriodSettingSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						accrualPeriodList =  this.repositoryAccrualPeriodSetting.predictiveAccrualPeriodSettingSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						accrualPeriodList =  this.repositoryAccrualPeriodSetting.predictiveAccrualPeriodSettingSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
			
				if(accrualPeriodList != null && !accrualPeriodList.isEmpty()){
					List<DtoAccrualPeriodSetting> dtoAccrualPeriodList = new ArrayList<>();
					DtoAccrualPeriodSetting dtoAccrualPeriod=null;
					for (AccrualPeriodSetting accrualPeriodSetting : accrualPeriodList) {
						dtoAccrualPeriod = new DtoAccrualPeriodSetting(accrualPeriodSetting);
						dtoAccrualPeriod = getDtoAccrualPeriodSetting(dtoAccrualPeriod,accrualPeriodSetting);
						dtoAccrualPeriodList.add(dtoAccrualPeriod);
					}
					dtoSearch.setRecords(dtoAccrualPeriodList);
				}
			}
			log.debug("Search AccrualPeriod Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public DtoAccrualPeriodSetting getDtoAccrualPeriodSetting(DtoAccrualPeriodSetting dtoAccrualPeriod,AccrualPeriodSetting accrualPeriodSetting) {
		dtoAccrualPeriod.setId(accrualPeriodSetting.getId());
		dtoAccrualPeriod.setYear(accrualPeriodSetting.getYear());
		dtoAccrualPeriod.setPeriodType(accrualPeriodSetting.getPeriodType());
		dtoAccrualPeriod.setSequence(accrualPeriodSetting.getSequence());
		dtoAccrualPeriod.setStartDate(accrualPeriodSetting.getStartDate());
		dtoAccrualPeriod.setEndDate(accrualPeriodSetting.getEndData());
		dtoAccrualPeriod.setAccrued(accrualPeriodSetting.isAccrued());
		return dtoAccrualPeriod;
	}
	
	
	
	
	public DtoAccrualPeriodSetting getByAccrualPeriodId(Integer id) {
		log.info("getByAccrualPeriodId Method");
		DtoAccrualPeriodSetting dtoAccrualPeriod = new DtoAccrualPeriodSetting();
		try {
		
			if (id > 0) {
				AccrualPeriodSetting accrualPeriodSetting = repositoryAccrualPeriodSetting.findByIdAndIsDeleted(id, false);
				if (accrualPeriodSetting != null) {
					dtoAccrualPeriod = new DtoAccrualPeriodSetting(accrualPeriodSetting);
					dtoAccrualPeriod = getDtoAccrualPeriodSetting(dtoAccrualPeriod,accrualPeriodSetting);
				} else {
					dtoAccrualPeriod.setMessageType(MessageConstant.ACCRUAL_PERIOED_NOT_GETTING);

				}
			} else {
				dtoAccrualPeriod.setMessageType(MessageConstant.ACCRUAL_PERIOED_NOT_GETTING);

			}
			log.debug("AccrualPeriod By Id is:"+dtoAccrualPeriod.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrualPeriod;
	}
	
	public DtoSearch getAll() {
  		log.info("getAll AccrualPeriod Method");
		DtoSearch dtoSearch = new DtoSearch();
          try {

    		List<AccrualPeriodSetting> accrualPeriodList = null;
    		accrualPeriodList = repositoryAccrualPeriodSetting.findByIsDeletedOrderByCreatedDateDesc(false);
    		List<DtoAccrualPeriodSetting> dtoAccrualPeriodList=new ArrayList<>();
    		if(accrualPeriodList!=null && !accrualPeriodList.isEmpty())
    		{
    			for (AccrualPeriodSetting accrualPeriodSetting : accrualPeriodList) 
    			{
    				DtoAccrualPeriodSetting dtoAccrualPeriod=new DtoAccrualPeriodSetting(accrualPeriodSetting);
    				dtoAccrualPeriod = getDtoAccrualPeriodSetting(dtoAccrualPeriod,accrualPeriodSetting);
    				
    				dtoAccrualPeriodList.add(dtoAccrualPeriod);
    			}
    			dtoSearch.setRecords(dtoAccrualPeriodList);
    		}
    		log.debug("All AccrualPeriod List Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public DtoAccrualPeriodSetting getById(int id) {
		DtoAccrualPeriodSetting dtoAccrualPeriod  = new DtoAccrualPeriodSetting();

		try {
			if (id > 0) {
				AccrualPeriodSetting accrualPeriodSetting = repositoryAccrualPeriodSetting.findByIdAndIsDeleted(id, false);
				if (accrualPeriodSetting != null) {
					dtoAccrualPeriod = new DtoAccrualPeriodSetting(accrualPeriodSetting);
					dtoAccrualPeriod = getDtoAccrualPeriodSetting(dtoAccrualPeriod,accrualPeriodSetting);
				} else {
					dtoAccrualPeriod.setMessageType("ACCRUAL_PERIOED_NOT_GETTING");

				}
			} else {
				dtoAccrualPeriod.setMessageType("ACCRUAL_PERIOED_NOT_GETTING");

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoAccrualPeriod;
	}
	
}
