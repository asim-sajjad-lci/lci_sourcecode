package com.bti.hcm.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Applicant;
import com.bti.hcm.model.ApplicantCost;
import com.bti.hcm.model.Requisitions;
import com.bti.hcm.model.dto.DtoApplicant;
import com.bti.hcm.model.dto.DtoApplicantCost;
import com.bti.hcm.model.dto.DtoRequisitions;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryApplicant;
import com.bti.hcm.repository.RepositoryApplicantCost;
import com.bti.hcm.repository.RepositoryRequisitions;

@Service("serviceApplicantCost")
public class ServiceApplicantCost {

	/**
	 * @Description LOGGER use for put a logger in ApplicantCost Service
	 */
	static Logger log = Logger.getLogger(ServiceApplicantCost.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in TimeCode service
	 */
	@Autowired
	ServiceResponse response;
	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in TimeCode service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;
	
	
	/**
	 * @Description repositoryApplicantCost Autowired here using annotation of spring for access of repositoryApplicantCost method in ApplicantCost service
	 */
	@Autowired(required=false)
	RepositoryApplicantCost repositoryApplicantCost;
	
	@Autowired(required=false)
	RepositoryRequisitions repositoryRequisitions;
	
	
	@Autowired(required=false)
	RepositoryApplicant repositoryApplicant;
	
	
	/**
	 * @param dtoApplicantCost
	 * @return
	 */
	public DtoApplicantCost saveOrUpdate(DtoApplicantCost dtoApplicantCost) {

		try {
			log.info("saveOrUpdate ApplicantCost Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			ApplicantCost applicantCost = null;
			if (dtoApplicantCost.getId() != null && dtoApplicantCost.getId() > 0) {
				
				applicantCost = repositoryApplicantCost.findByIdAndIsDeleted(dtoApplicantCost.getId(), false);
				applicantCost.setUpdatedBy(loggedInUserId);
				applicantCost.setUpdatedDate(new Date());
			} else {
				applicantCost = new ApplicantCost();
				applicantCost.setCreatedDate(new Date());
				Integer rowId = repositoryApplicantCost.getCountOfTotalApplicantCost();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				applicantCost.setRowId(increment);
			}
			Requisitions requisitions =null;
			if(dtoApplicantCost.getDtoRequisitions()!=null && dtoApplicantCost.getDtoRequisitions().getId()!=null) {
				 requisitions = repositoryRequisitions.findOne(dtoApplicantCost.getDtoRequisitions().getId());
			}
			
			Applicant applicant = null;
			if(dtoApplicantCost.getDtoApplicant()!=null && dtoApplicantCost.getDtoApplicant().getId()!=null) {
				applicant = repositoryApplicant.findOne(dtoApplicantCost.getDtoApplicant().getId());
			}
			
			
			applicantCost.setApplicant(applicant);
			applicantCost.setRequisitions(requisitions);
			applicantCost.setSequence(dtoApplicantCost.getSequence());
			applicantCost.setTravelCost(dtoApplicantCost.getTravelCost());
			applicantCost.setLodgingCost(dtoApplicantCost.getLodgingCost());
			applicantCost.setMovingCost(dtoApplicantCost.getMovingCost());
			applicantCost.setOtherCost(dtoApplicantCost.getOtherCost());
			applicantCost.setTotalCost(BigDecimal.valueOf(dtoApplicantCost.getTravelCost().intValue() + dtoApplicantCost.getLodgingCost().intValue() + dtoApplicantCost.getMovingCost().intValue()+(long)dtoApplicantCost.getOtherCost().intValue()));		
			applicantCost = repositoryApplicantCost.saveAndFlush(applicantCost);
			log.debug("ApplicantCost is:"+applicantCost.getId());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoApplicantCost;
	
	}
	
	/**
	 * @param ids
	 * @return
	 */
	public DtoApplicantCost delete(List<Integer> ids) {
		log.info("delete ApplicantCost Method");
		DtoApplicantCost dtoApplicantCost = new DtoApplicantCost();
		dtoApplicantCost
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_DELETED", false));
		dtoApplicantCost.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_ASSOCIATED", false));
		List<DtoApplicantCost> deleteApplicantCost = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(response.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer payCodeId : ids) {
				ApplicantCost payCode = repositoryApplicantCost.findOne(payCodeId);
				if(payCode!=null) {
					DtoApplicantCost dtoTimeCode1 = new DtoApplicantCost();
					dtoTimeCode1.setId(payCode.getId());
					repositoryApplicantCost.deleteSingleApplicantCost(true, loggedInUserId, payCodeId);
					deleteApplicantCost.add(dtoTimeCode1);
		
				}else {
					inValidDelete= true;
				}
			
			}
			if(inValidDelete){
				dtoApplicantCost.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoApplicantCost.setMessageType("");
				
			}
			
			dtoApplicantCost.setDelete(deleteApplicantCost);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete ApplicantCost :"+dtoApplicantCost.getId());
		return dtoApplicantCost;
	}
	
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {

		try {
			log.info("search ApplicantCost Method");
			if(dtoSearch != null){
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("travelCost") || dtoSearch.getSortOn().equals("lodgingCost") || dtoSearch.getSortOn().equals("movingCost") 
							|| dtoSearch.getSortOn().equals("otherCost")|| dtoSearch.getSortOn().equals("totalCost")
							) {
						condition = dtoSearch.getSortOn();
					
				}else {
					condition = "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");
				}
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
				dtoSearch.setTotalCount(this.repositoryApplicantCost.findByIsDeleted(false).size());
				List<ApplicantCost> applicantCostList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						applicantCostList = this.repositoryApplicantCost.findByIsDeleted(false,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						applicantCostList = this.repositoryApplicantCost.findByIsDeleted(false,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						applicantCostList = this.repositoryApplicantCost.findByIsDeleted(false,new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				if(applicantCostList != null && !applicantCostList.isEmpty()){
					List<DtoApplicantCost> dtoTimeCodeList = new ArrayList<>();
					DtoApplicantCost dtoApplicantCost=null;
					for (ApplicantCost applicantCost : applicantCostList) {
						dtoApplicantCost = new DtoApplicantCost(applicantCost);
						dtoApplicantCost.setId(applicantCost.getId());
						if(applicantCost.getRequisitions()!=null) {
							DtoRequisitions dtoRequisitions = new DtoRequisitions();
							dtoApplicantCost.setDtoRequisitions(dtoRequisitions);
							dtoApplicantCost.getDtoRequisitions().setId(applicantCost.getRequisitions().getId());
							dtoApplicantCost.getDtoRequisitions().setRecruiterName(applicantCost.getRequisitions().getRecruiterName());
						
						}
						
						if(applicantCost.getApplicant()!=null) {
							DtoApplicant applicant = new DtoApplicant();
							dtoApplicantCost.setDtoApplicant(applicant);
							dtoApplicantCost.getDtoApplicant().setId(applicantCost.getApplicant().getId());
						}
						
						dtoApplicantCost.setSequence(applicantCost.getSequence());
						dtoApplicantCost.setTravelCost(applicantCost.getTravelCost());
						dtoApplicantCost.setLodgingCost(applicantCost.getLodgingCost());
						dtoApplicantCost.setMovingCost(applicantCost.getMovingCost());
						dtoApplicantCost.setOtherCost(applicantCost.getOtherCost());
						dtoApplicantCost.setTotalCost(applicantCost.getTotalCost());
						dtoTimeCodeList.add(dtoApplicantCost);
					}
					dtoSearch.setRecords(dtoTimeCodeList);
				}
			}
			log.debug("Search ApplicantCost Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	public DtoApplicantCost getById(int id) {
		log.info("getById Method");
		DtoApplicantCost dtoApplicantCost = new DtoApplicantCost();
		try {

			if (id > 0) {
				ApplicantCost applicantCost = repositoryApplicantCost.findByIdAndIsDeleted(id, false);
				if (applicantCost != null) {
					dtoApplicantCost = new DtoApplicantCost(applicantCost);
					
					dtoApplicantCost.setId(applicantCost.getId());
					if(applicantCost.getRequisitions()!=null) {
						DtoRequisitions dtoRequisitions = new DtoRequisitions();
						dtoApplicantCost.setDtoRequisitions(dtoRequisitions);
						dtoApplicantCost.getDtoRequisitions().setId(applicantCost.getRequisitions().getId());
						dtoApplicantCost.getDtoRequisitions().setRecruiterName(applicantCost.getRequisitions().getRecruiterName());
					
					}
					
					if(applicantCost.getApplicant()!=null) {
						DtoApplicant applicant = new DtoApplicant();
						dtoApplicantCost.setDtoApplicant(applicant);
						dtoApplicantCost.getDtoApplicant().setId(applicantCost.getApplicant().getId());
					}
					
					dtoApplicantCost.setSequence(applicantCost.getSequence());
					dtoApplicantCost.setTravelCost(applicantCost.getTravelCost());
					dtoApplicantCost.setLodgingCost(applicantCost.getLodgingCost());
					dtoApplicantCost.setMovingCost(applicantCost.getMovingCost());
					dtoApplicantCost.setOtherCost(applicantCost.getOtherCost());
					dtoApplicantCost.setTotalCost(applicantCost.getTotalCost());
				} else {
					dtoApplicantCost.setMessageType("TIME_CODE_NOT_GETTING");

				}
			} else {
				dtoApplicantCost.setMessageType("INVALID_TIME_CODE_ID");

			}
			log.debug("payCode By Id is:"+dtoApplicantCost.getId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoApplicantCost;
	}
}
