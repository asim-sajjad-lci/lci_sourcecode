package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40521",indexes = {
        @Index(columnList = "PRCHKINDX")
})
public class PredefinecheckList extends HcmBaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRCHKINDX")
	private Integer id;

	@Column(name = "PRCHKTYP")
	private short checklistType;

	@Column(name = "PRCHKDSCR", columnDefinition = "char(81)")
	private String checklistDesc;

	@Column(name = "PRCHKDSCRA", columnDefinition = "char(141)")
	private String checklistArbicDesc;

	/*@OneToMany(mappedBy = "predefinecheckList")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<OrientationSetup> listOrientationSetup;*/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public short getChecklistType() {
		return checklistType;
	}

	public void setChecklistType(short checklistType) {
		this.checklistType = checklistType;
	}

	public String getChecklistDesc() {
		return checklistDesc;
	}

	public void setChecklistDesc(String checklistDesc) {
		this.checklistDesc = checklistDesc;
	}

	public String getChecklistArbicDesc() {
		return checklistArbicDesc;
	}

	public void setChecklistArbicDesc(String checklistArbicDesc) {
		this.checklistArbicDesc = checklistArbicDesc;
	}

	/*public List<OrientationSetup> getListOrientationSetup() {
		return listOrientationSetup;
	}

	public void setListOrientationSetup(List<OrientationSetup> listOrientationSetup) {
		this.listOrientationSetup = listOrientationSetup;
	}*/

}
