package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeEducation;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.dto.DtoEmployeeEducation;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDependent;
import com.bti.hcm.repository.RepositoryEmployeeEducation;
import com.bti.hcm.repository.RepositoryEmployeeMaster;

@Service("/serviceEmployeeEducation")
public class ServiceEmployeeEducation {
	
	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceEmployeeEducation.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;
	
	@Autowired
	RepositoryEmployeeDependent repositoryEmployeeDependent;
	
	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;
	
	@Autowired(required=false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired(required=false)
	RepositoryEmployeeEducation repositoryEmployeeEducation;
	
	

	
	public DtoEmployeeEducation saveOrUpdate(DtoEmployeeEducation dtoEmployeeEducation1) {
		try {

			log.info("saveOrUpdate EmployeeEducation Method");
			
			
			for (DtoEmployeeEducation dtoEmployeeEducation : dtoEmployeeEducation1.getListEmployeeEducation()) {
				
				int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
				EmployeeEducation employeeEducation = null;
				if (dtoEmployeeEducation.getId() != null && dtoEmployeeEducation.getId() > 0) {
					
					employeeEducation = repositoryEmployeeEducation.findByIdAndIsDeleted(dtoEmployeeEducation.getId(), false);
					employeeEducation.setUpdatedBy(loggedInUserId);
					employeeEducation.setUpdatedDate(new Date());
				} else {
					employeeEducation = new EmployeeEducation();
					employeeEducation.setCreatedDate(new Date());
					Integer rowId = repositoryEmployeeEducation.getCountOfTotalEmployeeEducation();
					Integer increment=0;
					if(rowId!=0) {
						increment= rowId+1;
					}else {
						increment=1;
					}
					employeeEducation.setRowId(increment);
				}

				EmployeeMaster employeeMaster=null;
				if(dtoEmployeeEducation.getEmployeeMaster()!=null && dtoEmployeeEducation.getEmployeeMaster().getEmployeeIndexId()>0) {
					employeeMaster=repositoryEmployeeMaster.findOne(dtoEmployeeEducation.getEmployeeMaster().getEmployeeIndexId());
				}
				
				employeeEducation.setEmployeeMaster(employeeMaster);
				employeeEducation.setSchoolName(dtoEmployeeEducation.getSchoolName());
				employeeEducation.setMajor(dtoEmployeeEducation.getMajor());
				employeeEducation.setEndYear(dtoEmployeeEducation.getEndYear());
				employeeEducation.setStartYear(dtoEmployeeEducation.getStartYear());
				employeeEducation.setComments(dtoEmployeeEducation.getComments());
				employeeEducation.setGpa(dtoEmployeeEducation.getGpa());
				employeeEducation.setDegree(dtoEmployeeEducation.getDegree());
				employeeEducation.setEducationSequence(dtoEmployeeEducation.getEducationSequence());
				
				employeeEducation = repositoryEmployeeEducation.saveAndFlush(employeeEducation);
				dtoEmployeeEducation.setId(employeeEducation.getId());
				log.debug("EmployeeEducation is:"+employeeEducation.getId());
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoEmployeeEducation1;
	
	}
	
	public DtoEmployeeEducation delete(List<Integer> ids) {
		log.info("delete EmployeeEducation Method");
		DtoEmployeeEducation dtoEmployeeEducation = new DtoEmployeeEducation();
		dtoEmployeeEducation.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_DELETED", false));
		dtoEmployeeEducation.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("TIME_CODE_ASSOCIATED", false));
		List<DtoEmployeeEducation> delete = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("TIME_CODE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer employeeEducationId : ids) {
				EmployeeEducation employeeEducation = repositoryEmployeeEducation.findOne(employeeEducationId);
				if(employeeEducation!=null && employeeEducation.getEmployeeMaster()==null) {
					DtoEmployeeEducation dtoEmployeeEducation2 = new DtoEmployeeEducation();
					dtoEmployeeEducation2.setId(employeeEducation.getId());
						repositoryEmployeeEducation.deleteSinglEmployeeEducation(true, loggedInUserId, employeeEducationId);
						delete.add(dtoEmployeeEducation2);
		
				}else {
					inValidDelete = true;
				}
			
			}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoEmployeeEducation.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoEmployeeEducation.setMessageType("");
				
			}
			
			dtoEmployeeEducation.setDelete(delete);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeeEducation :"+dtoEmployeeEducation.getId());
		return dtoEmployeeEducation;
	}

	
	public DtoEmployeeEducation getById(int id) {
		DtoEmployeeEducation dtoEmployeeEducation  = new DtoEmployeeEducation();
		try {
			if (id > 0) {
				EmployeeEducation employeeEducation = repositoryEmployeeEducation.findByIdAndIsDeleted(id, false);
				if (employeeEducation != null) {
					dtoEmployeeEducation = new DtoEmployeeEducation(employeeEducation);
					dtoEmployeeEducation.setId(employeeEducation.getId());
					
					dtoEmployeeEducation.setSchoolName(employeeEducation.getSchoolName());
					dtoEmployeeEducation.setMajor(employeeEducation.getMajor());
					dtoEmployeeEducation.setEndYear(employeeEducation.getEndYear());
					dtoEmployeeEducation.setStartYear(employeeEducation.getStartYear());
					dtoEmployeeEducation.setComments(employeeEducation.getComments());
					dtoEmployeeEducation.setGpa(employeeEducation.getGpa());
					dtoEmployeeEducation.setDegree(employeeEducation.getDegree());
					dtoEmployeeEducation.setEducationSequence(employeeEducation.getEducationSequence());
						
					
					
				} 
			} else {
				dtoEmployeeEducation.setMessageType("INVALID_ID");

			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeEducation;
	}



	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("EmployeeEducation Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("schoolName")|| dtoSearch.getSortOn().equals("major")|| dtoSearch.getSortOn().equals("comments")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
				dtoSearch.setTotalCount(this.repositoryEmployeeEducation.predictiveEmployeeEducationSearchTotalCount("%"+searchWord+"%"));
				List<EmployeeEducation> employeeEducationList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeeEducationList = this.repositoryEmployeeEducation.predictiveEmployeeEducationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeeEducationList = this.repositoryEmployeeEducation.predictiveEmployeeEducationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeeEducationList = this.repositoryEmployeeEducation.predictiveEmployeeEducationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}
				if(employeeEducationList != null && !employeeEducationList.isEmpty()){
					List<DtoEmployeeEducation> dtoEmployeeEducationList = new ArrayList<>();
					DtoEmployeeEducation dtoEmployeeEducation=null;
					for (EmployeeEducation employeeEducation : employeeEducationList) {
						dtoEmployeeEducation = new DtoEmployeeEducation(employeeEducation);
						DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
						if(employeeEducation.getEmployeeMaster()!=null) {
							employeeMaster.setEmployeeIndexId(employeeEducation.getEmployeeMaster().getEmployeeIndexId());
							employeeMaster.setEmployeeId(employeeEducation.getEmployeeMaster().getEmployeeId());
							employeeMaster.setEmployeeBirthDate(employeeEducation.getEmployeeMaster().getEmployeeBirthDate());
							employeeMaster.setEmployeeCitizen(employeeEducation.getEmployeeMaster().isEmployeeCitizen());
							employeeMaster.setEmployeeFirstName(employeeEducation.getEmployeeMaster().getEmployeeFirstName());
							employeeMaster.setEmployeeFirstNameArabic(employeeEducation.getEmployeeMaster().getEmployeeFirstNameArabic());
							employeeMaster.setEmployeeGender(employeeEducation.getEmployeeMaster().getEmployeeGender());
							employeeMaster.setEmployeeHireDate(employeeEducation.getEmployeeMaster().getEmployeeHireDate());
							employeeMaster.setEmployeeImmigration(employeeEducation.getEmployeeMaster().isEmployeeImmigration());
							employeeMaster.setEmployeeInactive(employeeEducation.getEmployeeMaster().isEmployeeInactive());
							employeeMaster.setEmployeeInactiveDate(employeeEducation.getEmployeeMaster().getEmployeeInactiveDate());
							employeeMaster.setEmployeeInactiveReason(employeeEducation.getEmployeeMaster().getEmployeeInactiveReason());
							employeeMaster.setEmployeeLastName(employeeEducation.getEmployeeMaster().getEmployeeLastName());
							employeeMaster.setEmployeeLastNameArabic(employeeEducation.getEmployeeMaster().getEmployeeLastNameArabic());
							employeeMaster.setEmployeeLastWorkDate(employeeEducation.getEmployeeMaster().getEmployeeLastWorkDate());
							employeeMaster.setEmployeeMaritalStatus(employeeEducation.getEmployeeMaster().getEmployeeMaritalStatus());
							employeeMaster.setEmployeeMiddleName(employeeEducation.getEmployeeMaster().getEmployeeMiddleName());
							employeeMaster.setEmployeeMiddleNameArabic(employeeEducation.getEmployeeMaster().getEmployeeMiddleNameArabic());
							employeeMaster.setEmployeeTitle(employeeEducation.getEmployeeMaster().getEmployeeTitle());
							employeeMaster.setEmployeeTitleArabic(employeeEducation.getEmployeeMaster().getEmployeeTitleArabic());
							employeeMaster.setEmployeeType(employeeEducation.getEmployeeMaster().getEmployeeType());
							employeeMaster.setEmployeeUserIdInSystem(employeeEducation.getEmployeeMaster().getEmployeeUserIdInSystem());
							employeeMaster.setEmployeeWorkHourYearly(employeeEducation.getEmployeeMaster().getEmployeeWorkHourYearly());
							dtoEmployeeEducation.setEmployeeMaster(employeeMaster);
						}
						dtoEmployeeEducation.setId(employeeEducation.getId());
						dtoEmployeeEducation.setSchoolName(employeeEducation.getSchoolName());
						dtoEmployeeEducation.setMajor(employeeEducation.getMajor());
						dtoEmployeeEducation.setEndYear(employeeEducation.getEndYear());
						dtoEmployeeEducation.setStartYear(employeeEducation.getStartYear());
						dtoEmployeeEducation.setComments(employeeEducation.getComments());
						dtoEmployeeEducation.setGpa(employeeEducation.getGpa());
						dtoEmployeeEducation.setDegree(employeeEducation.getDegree());
						dtoEmployeeEducation.setEducationSequence(employeeEducation.getEducationSequence());
						
						dtoEmployeeEducationList.add(dtoEmployeeEducation);
						}
					dtoSearch.setRecords(dtoEmployeeEducationList);
						}
			log.debug("Search EmployeeEducation Size is:"+dtoSearch.getTotalCount());
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}


	public DtoSearch getByEmployeeId(int id) {
		DtoSearch dtoSearch = new DtoSearch();
		try {
			List<DtoEmployeeEducation> dtoEmployeeEducation  = new ArrayList<>();
			if (id > 0) {
				List<EmployeeEducation> employeeEducation = repositoryEmployeeEducation.getByEmployeeId(id);
					for (EmployeeEducation employeeEducation2 : employeeEducation) {
						DtoEmployeeEducation dtoEmployeeEdu = new DtoEmployeeEducation(employeeEducation2);
						DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
						if(employeeEducation2.getEmployeeMaster()!=null) {
							employeeMaster.setEmployeeIndexId(employeeEducation2.getEmployeeMaster().getEmployeeIndexId());
							employeeMaster.setEmployeeId(employeeEducation2.getEmployeeMaster().getEmployeeId());
							employeeMaster.setEmployeeBirthDate(employeeEducation2.getEmployeeMaster().getEmployeeBirthDate());
							employeeMaster.setEmployeeCitizen(employeeEducation2.getEmployeeMaster().isEmployeeCitizen());
							employeeMaster.setEmployeeFirstName(employeeEducation2.getEmployeeMaster().getEmployeeFirstName());
							employeeMaster.setEmployeeFirstNameArabic(employeeEducation2.getEmployeeMaster().getEmployeeFirstNameArabic());
							employeeMaster.setEmployeeGender(employeeEducation2.getEmployeeMaster().getEmployeeGender());
							employeeMaster.setEmployeeHireDate(employeeEducation2.getEmployeeMaster().getEmployeeHireDate());
							employeeMaster.setEmployeeImmigration(employeeEducation2.getEmployeeMaster().isEmployeeImmigration());
							employeeMaster.setEmployeeInactive(employeeEducation2.getEmployeeMaster().isEmployeeInactive());
							employeeMaster.setEmployeeInactiveDate(employeeEducation2.getEmployeeMaster().getEmployeeInactiveDate());
							employeeMaster.setEmployeeInactiveReason(employeeEducation2.getEmployeeMaster().getEmployeeInactiveReason());
							employeeMaster.setEmployeeLastName(employeeEducation2.getEmployeeMaster().getEmployeeLastName());
							employeeMaster.setEmployeeLastNameArabic(employeeEducation2.getEmployeeMaster().getEmployeeLastNameArabic());
							employeeMaster.setEmployeeLastWorkDate(employeeEducation2.getEmployeeMaster().getEmployeeLastWorkDate());
							employeeMaster.setEmployeeMaritalStatus(employeeEducation2.getEmployeeMaster().getEmployeeMaritalStatus());
							employeeMaster.setEmployeeMiddleName(employeeEducation2.getEmployeeMaster().getEmployeeMiddleName());
							employeeMaster.setEmployeeMiddleNameArabic(employeeEducation2.getEmployeeMaster().getEmployeeMiddleNameArabic());
							employeeMaster.setEmployeeTitle(employeeEducation2.getEmployeeMaster().getEmployeeTitle());
							employeeMaster.setEmployeeTitleArabic(employeeEducation2.getEmployeeMaster().getEmployeeTitleArabic());
							employeeMaster.setEmployeeType(employeeEducation2.getEmployeeMaster().getEmployeeType());
							employeeMaster.setEmployeeUserIdInSystem(employeeEducation2.getEmployeeMaster().getEmployeeUserIdInSystem());
							employeeMaster.setEmployeeWorkHourYearly(employeeEducation2.getEmployeeMaster().getEmployeeWorkHourYearly());
							dtoEmployeeEdu.setEmployeeMaster(employeeMaster);
						}
						dtoEmployeeEdu.setId(employeeEducation2.getId());
						dtoEmployeeEdu.setSchoolName(employeeEducation2.getSchoolName());
						dtoEmployeeEdu.setMajor(employeeEducation2.getMajor());
						dtoEmployeeEdu.setEndYear(employeeEducation2.getEndYear());
						dtoEmployeeEdu.setStartYear(employeeEducation2.getStartYear());
						dtoEmployeeEdu.setComments(employeeEducation2.getComments());
						dtoEmployeeEdu.setGpa(employeeEducation2.getGpa());
						dtoEmployeeEdu.setDegree(employeeEducation2.getDegree());
						dtoEmployeeEdu.setEducationSequence(employeeEducation2.getEducationSequence());
						dtoEmployeeEducation.add(dtoEmployeeEdu);
					}
			}
			dtoSearch.setRecords(dtoEmployeeEducation);
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
}
