package com.bti.hcm.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.FinancialDimensionsValues;
import com.bti.hcm.model.ManualChecks;
import com.bti.hcm.model.ManualChecksDetails;
import com.bti.hcm.model.dto.DtoManualChecksDetails;
import com.bti.hcm.repository.RepositoryAccountsTableAccumulation;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryFinancialDimensionsValues;
import com.bti.hcm.repository.RepositoryManualChecks;
import com.bti.hcm.repository.RepositoryManualChecksDetails;
import com.bti.hcm.repository.RepositoryManualChecksDistribution;
import com.bti.hcm.repository.RepositoryPosition;

@Service("sServiceManualChecksDetails")
public class ServiceManualChecksDetails {
	

	static Logger log = Logger.getLogger(ServiceLocation.class.getName());
	
	@Autowired
	RepositoryManualChecksDistribution repositoryManualChecksDistribution;
	
	@Autowired
	RepositoryManualChecks repositoryManualChecks;
	
	@Autowired
	RepositoryDepartment repositoryDepartment; 
	
	@Autowired
	RepositoryPosition  repositoryPosition;
	
	@Autowired
	RepositoryAccountsTableAccumulation repositoryAccountsTableAccumulation;
	
	@Autowired
	RepositoryFinancialDimensionsValues repositoryFinancialDimensionsValues;
		
	@Autowired
	RepositoryManualChecksDetails repositoryManualChecksDetails;  
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	

	public DtoManualChecksDetails saveOrUpdate(DtoManualChecksDetails dtoManualChecksDetails) {
              try {
            	  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
            	  ManualChecksDetails manualChecksDetails=null;
            	  if(dtoManualChecksDetails.getId()!=null && dtoManualChecksDetails.getId()>0) {
            		  manualChecksDetails=repositoryManualChecksDetails.findByIdAndIsDeleted(dtoManualChecksDetails.getId(),false);
            		  manualChecksDetails.setUpdatedBy(loggedInUserId);
            		  manualChecksDetails.setUpdatedDate(new Date());
            	  }else {
            		  manualChecksDetails=new ManualChecksDetails();
            		  manualChecksDetails.setCreatedDate(new Date());
            		  Integer rowId = repositoryManualChecksDetails.getCountOfTotaLocation();
      				Integer increment=0;
      				if(rowId!=0) {
      					increment= rowId+1;
      				}else {
      					increment=1;
      				}
      				manualChecksDetails.setRowId(increment);
              	}
            	 FinancialDimensionsValues financialDimensionsValues=null;
            	  if(dtoManualChecksDetails.getDtoFinancialDimensionsValues()!=null && dtoManualChecksDetails.getDtoFinancialDimensionsValues().getId()>0) {
            		  financialDimensionsValues=repositoryFinancialDimensionsValues.findOne(dtoManualChecksDetails.getDtoFinancialDimensionsValues().getId());
            	  }
            	  ManualChecks manualChecks=null;
            	  if(dtoManualChecksDetails.getManualChecks()!=null && dtoManualChecksDetails.getManualChecks().getId()>0) {
            		  manualChecks=repositoryManualChecks.findOne(dtoManualChecksDetails.getManualChecks().getId());
            	  }
            	  manualChecksDetails.setManualChecks(manualChecks);
            	  manualChecksDetails.setFinancialDimensionsValues(financialDimensionsValues);
            	  manualChecksDetails.setCodeId(dtoManualChecksDetails.getCodeId());
            	  manualChecksDetails.setTransaction(dtoManualChecksDetails.getTransaction());
            	  manualChecksDetails.setTransactiotype(dtoManualChecksDetails.getTransactiotype());
            	  manualChecksDetails.setDateFrom(dtoManualChecksDetails.getDateFrom());
            	  manualChecksDetails.setDateTo(dtoManualChecksDetails.getDateTo());
            	  manualChecksDetails.setTotalAmount(dtoManualChecksDetails.getTotalAmount());
            	  manualChecksDetails.setTotalPercent(dtoManualChecksDetails.getTotalPercent());
            	  manualChecksDetails.setHours(dtoManualChecksDetails.getHours());
            	  manualChecksDetails.setDaysWorks(dtoManualChecksDetails.getDaysWorks());
            	  manualChecksDetails.setWeeksWorks(dtoManualChecksDetails.getWeeksWorks());
            	  repositoryManualChecksDetails.saveAndFlush(manualChecksDetails);
			} catch (Exception e) {
                   log.error(e);
			}
		return dtoManualChecksDetails;
	}

}
