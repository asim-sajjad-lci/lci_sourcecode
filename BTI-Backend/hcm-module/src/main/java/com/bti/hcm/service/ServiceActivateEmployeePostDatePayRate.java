package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.ActivateEmployeePostDatePayRate;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.dto.DtoActivateEmployeePostDatePayRate;
import com.bti.hcm.model.dto.DtoEmployeeMaster;
import com.bti.hcm.model.dto.DtoPayCode;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryActivateEmployeePostDatePayRate;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryPayCode;

@Service("/serviceActivateEmployeePostDatePayRate")
public class ServiceActivateEmployeePostDatePayRate {
	


	/**
	 * /**
 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
 */
	
	static Logger log = Logger.getLogger(ServiceActivateEmployeePostDatePayRate.class.getName());
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in ServiceRetirementFundSetup service
	 */
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * /**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletResponse method in ServiceRetirementFundSetup service
	 */
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	/**
	 * 
	 */
	@Autowired(required = false)
	RepositoryActivateEmployeePostDatePayRate repositoryActivateEmployeePostDatePayRate;

	@Autowired(required = false)
	RepositoryEmployeeMaster repositoryEmployeeMaster;
	
	@Autowired(required = false)
	RepositoryPayCode repositoryPayCode;
	
	
	public DtoActivateEmployeePostDatePayRate saveOrUpdate(DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			log.info("saveOrUpdate ActiveEmployeePostDatedPayRates Method");
			if(dtoActivateEmployeePostDatePayRate.getListActivateEmployeePostDatePayRate().get(0)!=null && dtoActivateEmployeePostDatePayRate.getListActivateEmployeePostDatePayRate().get(0).getEmployeeMaster().getEmployeeIndexId()>0) {
				repositoryActivateEmployeePostDatePayRate.deleteByEmployeeId(true,loggedInUserId,dtoActivateEmployeePostDatePayRate.getListActivateEmployeePostDatePayRate().get(0).getEmployeeMaster().getEmployeeIndexId());
				}
			if(dtoActivateEmployeePostDatePayRate.getListActivateEmployeePostDatePayRate().get(0)!=null && dtoActivateEmployeePostDatePayRate.getListActivateEmployeePostDatePayRate().get(0).getPayCode().getId()>0) {
				repositoryActivateEmployeePostDatePayRate.deleteByPayCodeId(true,loggedInUserId,dtoActivateEmployeePostDatePayRate.getListActivateEmployeePostDatePayRate().get(0).getPayCode().getId());
				}
			
			for (DtoActivateEmployeePostDatePayRate dtoActivateEmployeePayRate : dtoActivateEmployeePostDatePayRate.getListActivateEmployeePostDatePayRate()) {
				ActivateEmployeePostDatePayRate activateEmployeePostDatePayRate=new ActivateEmployeePostDatePayRate();
				activateEmployeePostDatePayRate.setCreatedDate(new Date());
				Integer rowId = repositoryActivateEmployeePostDatePayRate.findAll().size();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				
				EmployeeMaster employeeMaster = null;
				
				if(dtoActivateEmployeePayRate.getEmployeeMaster()!=null) {
					employeeMaster = repositoryEmployeeMaster.findOne(dtoActivateEmployeePayRate.getEmployeeMaster().getEmployeeIndexId());
				}
		
				PayCode  payCode=null;
				if(dtoActivateEmployeePayRate.getPayCode()!=null) {
					payCode=repositoryPayCode.findOne(dtoActivateEmployeePayRate.getPayCode().getId());
				}
				activateEmployeePostDatePayRate.setRowId(increment);
				activateEmployeePostDatePayRate.setEmployeeMaster(employeeMaster);
				activateEmployeePostDatePayRate.setPayCode(payCode);
				activateEmployeePostDatePayRate.setEffectiveDate(dtoActivateEmployeePayRate.getEffectiveDate());
				activateEmployeePostDatePayRate.setCurrentRate(dtoActivateEmployeePayRate.getCurrentRate());
				activateEmployeePostDatePayRate.setNewRate(dtoActivateEmployeePayRate.getNewRate());
				activateEmployeePostDatePayRate.setActive(dtoActivateEmployeePayRate.getIsActive());
				repositoryActivateEmployeePostDatePayRate.saveAndFlush(activateEmployeePostDatePayRate);
				
			}
			
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoActivateEmployeePostDatePayRate;
	}
	
	
	
	
	public DtoActivateEmployeePostDatePayRate delete(List<Integer> ids) {
		log.info("delete ActivateEmployeePostDatePayRate Method");
		DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate = new DtoActivateEmployeePostDatePayRate();
		dtoActivateEmployeePostDatePayRate.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ACTIVATE_EMPLOYEE_POST_DATE_PAY_RATE_DELETED", false));
		dtoActivateEmployeePostDatePayRate.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("ACTIVATE_EMPLOYEE_POST_DATE_PAY_RATE_ASSOCIATED", false));
		List<DtoActivateEmployeePostDatePayRate> deleteDtoActivateEmployeePostDatePayRate = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("ACTIVATE_EMPLOYEE_POST_DATE_PAY_RATE_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer planId : ids) {
				ActivateEmployeePostDatePayRate activateEmployeePostDatePayRate = repositoryActivateEmployeePostDatePayRate.findOne(planId);
	if(activateEmployeePostDatePayRate!=null) {
		DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate2=new DtoActivateEmployeePostDatePayRate();
		dtoActivateEmployeePostDatePayRate.setId(activateEmployeePostDatePayRate.getId());
		     
			repositoryActivateEmployeePostDatePayRate.deleteSingleActivateEmployeePostDatePayRate(true, loggedInUserId, planId);
			deleteDtoActivateEmployeePostDatePayRate.add(dtoActivateEmployeePostDatePayRate2);	
		
	}else {
		inValidDelete = true;
	}
					
				}
			if(inValidDelete){
				invlidDeleteMessage.replace(invlidDeleteMessage.length()-1, invlidDeleteMessage.length(), "");
				dtoActivateEmployeePostDatePayRate.setMessageType(invlidDeleteMessage.toString());
			}
			if(!inValidDelete){
				dtoActivateEmployeePostDatePayRate.setMessageType("");
			}
			dtoActivateEmployeePostDatePayRate.setDelete(deleteDtoActivateEmployeePostDatePayRate);
		} 
		catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete ActivateEmployeePostDatePayRate"+ " :"+dtoActivateEmployeePostDatePayRate.getId());
		return dtoActivateEmployeePostDatePayRate;
	}
	
	
	
	public DtoActivateEmployeePostDatePayRate getById(int id) {
		DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate  = new DtoActivateEmployeePostDatePayRate();
		try {
			if (id > 0) {
				ActivateEmployeePostDatePayRate activateEmployeePostDatePayRate = repositoryActivateEmployeePostDatePayRate.findByIdAndIsDeleted(id, false);
					dtoActivateEmployeePostDatePayRate = new DtoActivateEmployeePostDatePayRate(activateEmployeePostDatePayRate);
					
					DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
					if(activateEmployeePostDatePayRate.getEmployeeMaster()!=null) {
						employeeMaster.setEmployeeIndexId(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeIndexId());
						employeeMaster.setEmployeeId(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeId());
						employeeMaster.setEmployeeBirthDate(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeBirthDate());
						employeeMaster.setEmployeeCitizen(activateEmployeePostDatePayRate.getEmployeeMaster().isEmployeeCitizen());
						employeeMaster.setEmployeeFirstName(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeFirstName());
						employeeMaster.setEmployeeFirstNameArabic(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeFirstNameArabic());
						employeeMaster.setEmployeeGender(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeGender());
						employeeMaster.setEmployeeHireDate(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeHireDate());
						employeeMaster.setEmployeeImmigration(activateEmployeePostDatePayRate.getEmployeeMaster().isEmployeeImmigration());
						employeeMaster.setEmployeeInactive(activateEmployeePostDatePayRate.getEmployeeMaster().isEmployeeInactive());
						employeeMaster.setEmployeeInactiveDate(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeInactiveDate());
						employeeMaster.setEmployeeInactiveReason(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeInactiveReason());
						employeeMaster.setEmployeeLastName(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeLastName());
						employeeMaster.setEmployeeLastNameArabic(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeLastNameArabic());
						employeeMaster.setEmployeeLastWorkDate(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeLastWorkDate());
						employeeMaster.setEmployeeMaritalStatus(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeMaritalStatus());
						employeeMaster.setEmployeeMiddleName(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeMiddleName());
						employeeMaster.setEmployeeMiddleNameArabic(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeMiddleNameArabic());
						employeeMaster.setEmployeeTitle(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeTitle());
						employeeMaster.setEmployeeTitleArabic(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeTitleArabic());
						employeeMaster.setEmployeeType(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeType());
						employeeMaster.setEmployeeUserIdInSystem(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeUserIdInSystem());
						employeeMaster.setEmployeeWorkHourYearly(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeWorkHourYearly());
						dtoActivateEmployeePostDatePayRate.setEmployeeMaster(employeeMaster);
					}
					DtoPayCode payCode=new DtoPayCode();
					if(activateEmployeePostDatePayRate.getPayCode()!=null) {
						payCode.setId(activateEmployeePostDatePayRate.getPayCode().getId());
						payCode.setDescription(activateEmployeePostDatePayRate.getPayCode().getDescription());
						payCode.setArbicDescription(activateEmployeePostDatePayRate.getPayCode().getArbicDescription());
						dtoActivateEmployeePostDatePayRate.setPayCode(payCode);
						
					}
					
					
					dtoActivateEmployeePostDatePayRate.setPayCode(payCode);
					dtoActivateEmployeePostDatePayRate.setEmployeeMaster(employeeMaster);
					dtoActivateEmployeePostDatePayRate.setId(activateEmployeePostDatePayRate.getId());
					dtoActivateEmployeePostDatePayRate.setActivePostDatedSeq(activateEmployeePostDatePayRate.getActivePostDatedSeq());
					dtoActivateEmployeePostDatePayRate.setEffectiveDate(activateEmployeePostDatePayRate.getEffectiveDate());
					dtoActivateEmployeePostDatePayRate.setCurrentRate(activateEmployeePostDatePayRate.getCurrentRate());
					dtoActivateEmployeePostDatePayRate.setNewRate(activateEmployeePostDatePayRate .getNewRate());
					dtoActivateEmployeePostDatePayRate.setActive(activateEmployeePostDatePayRate.getActive());
					
			} else {
				dtoActivateEmployeePostDatePayRate.setMessageType("INVALID_ID");

			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoActivateEmployeePostDatePayRate;
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		
		try {

			log.info("build checks Method");
			if(dtoSearch != null){
				String condition="";
				 if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
						
						if(dtoSearch.getSortOn().equals("effectiveDate")) {
							condition=dtoSearch.getSortOn();
						}else {
							condition="id";
						}
						
					}else{
						condition+="id";
						dtoSearch.setSortOn("");
						dtoSearch.setSortBy("");
					}	  
		
				
				List<ActivateEmployeePostDatePayRate> activateEmployeePostDatePayRateList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						activateEmployeePostDatePayRateList = this.repositoryActivateEmployeePostDatePayRate.predictiveActivatePayRateSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						activateEmployeePostDatePayRateList = this.repositoryActivateEmployeePostDatePayRate.predictiveActivatePayRateSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						activateEmployeePostDatePayRateList = this.repositoryActivateEmployeePostDatePayRate.predictiveActivatePayRateSearchWithPagination(new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				if(activateEmployeePostDatePayRateList != null && !activateEmployeePostDatePayRateList.isEmpty()){
					List<DtoActivateEmployeePostDatePayRate> dtoActivateEmployeePostDatePayRateList = new ArrayList<>();
					DtoActivateEmployeePostDatePayRate dtoActivateEmployeePostDatePayRate=null;
					for (ActivateEmployeePostDatePayRate activateEmployeePostDatePayRate : activateEmployeePostDatePayRateList) {
						dtoActivateEmployeePostDatePayRate = new DtoActivateEmployeePostDatePayRate(activateEmployeePostDatePayRate);
						
						
						DtoEmployeeMaster employeeMaster = new DtoEmployeeMaster();
						if(activateEmployeePostDatePayRate.getEmployeeMaster()!=null) {
							employeeMaster.setEmployeeIndexId(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeIndexId());
							employeeMaster.setEmployeeId(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeId());
							employeeMaster.setEmployeeBirthDate(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeBirthDate());
							employeeMaster.setEmployeeCitizen(activateEmployeePostDatePayRate.getEmployeeMaster().isEmployeeCitizen());
							employeeMaster.setEmployeeFirstName(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeFirstName());
							employeeMaster.setEmployeeFirstNameArabic(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeFirstNameArabic());
							employeeMaster.setEmployeeGender(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeGender());
							employeeMaster.setEmployeeHireDate(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeHireDate());
							employeeMaster.setEmployeeImmigration(activateEmployeePostDatePayRate.getEmployeeMaster().isEmployeeImmigration());
							employeeMaster.setEmployeeInactive(activateEmployeePostDatePayRate.getEmployeeMaster().isEmployeeInactive());
							employeeMaster.setEmployeeInactiveDate(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeInactiveDate());
							employeeMaster.setEmployeeInactiveReason(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeInactiveReason());
							employeeMaster.setEmployeeLastName(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeLastName());
							employeeMaster.setEmployeeLastNameArabic(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeLastNameArabic());
							employeeMaster.setEmployeeLastWorkDate(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeLastWorkDate());
							employeeMaster.setEmployeeMaritalStatus(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeMaritalStatus());
							employeeMaster.setEmployeeMiddleName(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeMiddleName());
							employeeMaster.setEmployeeMiddleNameArabic(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeMiddleNameArabic());
							employeeMaster.setEmployeeTitle(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeTitle());
							employeeMaster.setEmployeeTitleArabic(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeTitleArabic());
							employeeMaster.setEmployeeType(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeType());
							employeeMaster.setEmployeeUserIdInSystem(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeUserIdInSystem());
							employeeMaster.setEmployeeWorkHourYearly(activateEmployeePostDatePayRate.getEmployeeMaster().getEmployeeWorkHourYearly());
							dtoActivateEmployeePostDatePayRate.setEmployeeMaster(employeeMaster);
					
						}
						
						DtoPayCode payCode=new DtoPayCode();
						if(activateEmployeePostDatePayRate.getPayCode()!=null) {
							payCode.setId(activateEmployeePostDatePayRate.getPayCode().getId());
							payCode.setDescription(activateEmployeePostDatePayRate.getPayCode().getDescription());
							payCode.setArbicDescription(activateEmployeePostDatePayRate.getPayCode().getArbicDescription());
							dtoActivateEmployeePostDatePayRate.setPayCode(payCode);
						}
						
				
						dtoActivateEmployeePostDatePayRate.setId(activateEmployeePostDatePayRate.getId());
						dtoActivateEmployeePostDatePayRate.setActivePostDatedSeq(activateEmployeePostDatePayRate.getActivePostDatedSeq());
						dtoActivateEmployeePostDatePayRate.setEffectiveDate(activateEmployeePostDatePayRate.getEffectiveDate());
						dtoActivateEmployeePostDatePayRate.setCurrentRate(activateEmployeePostDatePayRate.getCurrentRate());
						dtoActivateEmployeePostDatePayRate.setNewRate(activateEmployeePostDatePayRate .getNewRate());
						dtoActivateEmployeePostDatePayRate.setActive(activateEmployeePostDatePayRate.getActive());
						dtoActivateEmployeePostDatePayRateList.add(dtoActivateEmployeePostDatePayRate);
						}
					dtoSearch.setRecords(dtoActivateEmployeePostDatePayRateList);
						}
			log.debug("Search BuildCheck Size is:"+dtoSearch.getTotalCount());
			
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}

	
	
}
