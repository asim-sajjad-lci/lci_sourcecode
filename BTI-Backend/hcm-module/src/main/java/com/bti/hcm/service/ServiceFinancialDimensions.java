package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.FinancialDimensions;
import com.bti.hcm.model.dto.DtoFinancialDimensions;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryFinancialDimensions;

@Service("/financialDimensions")
public class ServiceFinancialDimensions {
	
	static Logger log = Logger.getLogger(ServiceFinancialDimensions.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryBuildChecks repositoryBuildChecks;
	 
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryFinancialDimensions repositoryFinancialDimensions;

	
	public List<DtoFinancialDimensions> getAllFinancialDimensionsDropDown() {
		List<DtoFinancialDimensions>dtoFinancialDimensionsList=new  ArrayList<>();
		List<FinancialDimensions>financialDimensionsList=repositoryFinancialDimensions.findAll();
		if(!financialDimensionsList.isEmpty()) {
			for (FinancialDimensions financialDimensions : financialDimensionsList) {
				DtoFinancialDimensions dtoFinancialDimensions=new DtoFinancialDimensions();
				dtoFinancialDimensions.setId(financialDimensions.getId());
				dtoFinancialDimensions.setDimensionDescription(financialDimensions.getDimensionDescription());
				dtoFinancialDimensions.setDimensionArbicDescription(financialDimensions.getDimensionArbicDescription());
				dtoFinancialDimensions.setDimensionColumnName(financialDimensions.getDimensionColumnName());
				dtoFinancialDimensionsList.add(dtoFinancialDimensions);
			}
		}
		return dtoFinancialDimensionsList;
	}

}
