package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.EmployeeSkillsDesc;

@Repository("repositoryEmployeeSkillsDesc")
public interface RepositoryEmployeeSkillsDesc extends JpaRepository<EmployeeSkillsDesc, Integer>{

	public EmployeeSkillsDesc findByIdAndIsDeleted(int id, boolean deleted);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeSkillsDesc d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	void deleteSingleEmployeeSkillsDesc(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,@Param("id") Integer id);

	@Query("select d from EmployeeSkillsDesc d where d.employeeSkills.isDeleted=false and d.isDeleted=false")
	public List<EmployeeSkillsDesc> getList();
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update EmployeeSkillsDesc d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.employeeSkills.id =:id")
	public void deleteBySkillId(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,@Param("id")Integer id);

	@Query("select d from EmployeeSkillsDesc d where ((d.employeeSkills.employeeMaster.employeeId like :searchKeyWord or d.skillsSetup.skillDesc like :searchKeyWord or d.skillsSetup.skillId like :searchKeyWord) or d.employeeSkills.isDeleted=false and d.isDeleted=false)")
	public List<EmployeeSkillsDesc> getAllList(@Param("searchKeyWord") String deleted,Pageable pageRequest);

	@Query("select count(*) from EmployeeSkillsDesc d where ((d.employeeSkills.employeeMaster.employeeId like :searchKeyWord or d.skillsSetup.skillDesc like :searchKeyWord or d.skillsSetup.skillId like :searchKeyWord) or d.employeeSkills.isDeleted=false and d.isDeleted=false)")
	public Integer getAllListCount(@Param("searchKeyWord") String searchKeyWord);
	
	
	/*@Query("selcet e from EmployeeSkillsDesc e where e.id=:id and e.isDeleted=false")
	public List<EmployeeSkillsDesc> getEmployeeSkillsDescById(@Param("id")Integer  id);*/
}