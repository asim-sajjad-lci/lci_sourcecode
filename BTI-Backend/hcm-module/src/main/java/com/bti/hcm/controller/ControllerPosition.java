package com.bti.hcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoPosition;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServicePosition;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/positionSetup")
public class ControllerPosition extends BaseController {
	
	@Autowired
	ServicePosition servicePosition;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger log = Logger.getLogger(ControllerSkillSteup.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@RequestMapping(value = "/createPosition", method = RequestMethod.POST)
	public ResponseMessage createPosition(HttpServletRequest request,@RequestBody DtoPosition dtoPosition) throws Exception{
		log.info("Create Postion Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			
			dtoPosition = servicePosition.saveOrUpdatePosition(dtoPosition);
			if(dtoPosition.getMessage()!=null) {
				responseMessage = displayMessage(dtoPosition,"POSITION_UPDATED","POSITION_NOT_UPDATED",response);
			}else {
			responseMessage = displayMessage(dtoPosition,"POSITION_CREATED","POSITION_NOT_CREATED",response);
			}
			
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Create Postion Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updatePosition(HttpServletRequest request,@RequestBody DtoPosition dtoPosition) throws Exception{
		log.info("Create Postion Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoPosition = servicePosition.saveOrUpdatePosition(dtoPosition);
			responseMessage = displayMessage(dtoPosition,"POSITION_UPDATED","POSITION_NOT_UPDATED",response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("update Postion Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	@RequestMapping(value = "/getAllOld", method = RequestMethod.PUT)
	public ResponseMessage getAllPostion(HttpServletRequest request, @RequestBody DtoPosition dtoPosition) throws Exception {
		log.info("Get All Postion Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSearch dtoSearch = servicePosition.getAllPostion(dtoPosition);
			responseMessage = displayMessage(dtoSearch,MessageConstant.POSITION_GET_ALL,MessageConstant.POSITION_LIST_NOT_GETTING,response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Get All Postion Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deletePosition(HttpServletRequest request, @RequestBody DtoPosition dtoPosition) throws Exception {
		log.info("Delete Position Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoPosition.getIds() != null && !dtoPosition.getIds().isEmpty()) {
				DtoPosition dtoPosition1 = servicePosition.deletePosition(dtoPosition.getIds());
				if(dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("POSITION_DELETED", false), dtoPosition1);	
				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoPosition1.getMessageType());
					btiMessageHcm.setMessageShort("POSTION_NOT_DELETE_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoPosition1);
				}
				
				

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Delete Position Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	@RequestMapping(value = "/getPositionId", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoPosition dtoPosition) throws Exception {
		log.info("Get Position ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPosition dtoPositionObj = servicePosition.getByPositionId(dtoPosition.getId());
			responseMessage = displayMessage(dtoPositionObj,MessageConstant.POSITION_GET_ALL,MessageConstant.POSITION_LIST_NOT_GETTING,response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		log.debug("Get Position by Id Method:"+dtoPosition.getId());
		return responseMessage;
	}
	
	/*@RequestMapping(value = "/positionIdcheck", method = RequestMethod.POST)
	public ResponseMessage positionIdcheck(HttpServletRequest request, @RequestBody DtoPosition dtoPosition) throws Exception {
		log.info("positionIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPosition dtopPositionObj = servicePosition.repeatBySkillSteupId(dtoPosition.getPositionId());
			responseMessage = displayMessage(dtopPositionObj,MessageConstant.DEPARTMENT_RESULT,MessageConstant.POSITION_LIST_NOT_GETTING,response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}*/
	
	
	
	@RequestMapping(value = "/positionIdcheck", method = RequestMethod.POST)
	public ResponseMessage departmetnIdCheck(HttpServletRequest request, @RequestBody DtoPosition dtoPosition)
			throws Exception {
		log.info("departmetnIdCheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoPosition dtopPositionObj = servicePosition.repeatBySkillSteupId(dtoPosition.getPositionId());
			
			 if (dtopPositionObj !=null && dtopPositionObj.getIsRepeat()) {
                 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                		 response.getMessageByShortAndIsDeleted("POSIONT_ID_RESULT", false), dtopPositionObj);
             } else {
                 responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
                		 response.getMessageByShortAndIsDeleted("POSITION_REPEAT_DEPARTMENTID_NOT_FOUND", false),
                         dtopPositionObj);
             }
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		return responseMessage;
	}

	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPosition(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search searchPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePosition.searchPosition(dtoSearch);
			responseMessage = displayMessage(dtoSearch,MessageConstant.POSITION_GET_ALL,MessageConstant.POSITION_LIST_NOT_GETTING,response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search searchPosition Methods:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchPosition1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPosition1(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search searchPosition Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
				dtoSearch = this.servicePosition.searchPosition1(dtoSearch);
			responseMessage = displayMessage(dtoSearch,MessageConstant.POSITION_GET_ALL,MessageConstant.POSITION_LIST_NOT_GETTING,response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search searchPositions Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchPositionId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchPositionId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search Position Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePosition.searchPositionId(dtoSearch);
			responseMessage = displayMessage(dtoSearch,MessageConstant.POSITION_GET_ALL,MessageConstant.POSITION_LIST_NOT_GETTING,response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search searchPositions Methods:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllPostionDropDownList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllPostionDropDownList(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		try {
			boolean flag =serviceHcmHome.checkValidCompanyAccess();
			if (flag) {
				List<DtoPosition> positionList = servicePosition.getAllPostionDropDownList();
				responseMessage = displayMessage(positionList,"POSITION_GET_ALL","POSITION__LIST_NOT_GETTING",response);
			}else {
				responseMessage = unauthorizedMsg(response);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/getAllPositionId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllPositionId(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info(" getAllPositionId Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePosition.getAllPositionId(dtoSearch);
			responseMessage = displayMessage(dtoSearch,"POSITION_GET_ALL","POSITION__LIST_NOT_GETTING",response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search searchPosition Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	

	@RequestMapping(value = "/searchAllPostionIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchAllPostionIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("searchAllPostionIds Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.servicePosition.searchAllPostionIds(dtoSearch);
			responseMessage = displayMessage(dtoSearch,"POSITION_SETUP_GET_ALL","POSITION_SETUP_LIST_NOT_GETTING",response);

		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			log.debug("Search searchPosition Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}

	
	
	
	
}
