/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

/**
 * Description: The persistent class for the Applicant database table. Name of
 * Project: Hcm Version: 0.0.1
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR00901",indexes = {
        @Index(columnList = "APPLICINDX")
})
public class Applicant extends HcmBaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "APPLICINDX")
	private int id;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "applicant")
	@Where(clause = "is_deleted = false")
	private List<ApplicantHire> applicantHire;

	@Column(name = "FRSTNAM", columnDefinition = "char(31)")
	private String firstName;

	@Column(name = "MIDLNAM", columnDefinition = "char(31)")
	private String middleName;

	@Column(name = "LSTNAME", columnDefinition = "char(31)")
	private String lastName;

	@Column(name = "ADDRSS", columnDefinition = "char(60)")
	private String address;

	@Column(name = "CITY", columnDefinition = "char(21)")
	private String city;

	@Column(name = "COUNTRY", columnDefinition = "char(31)")
	private String country;

	@Column(name = "PHONE1", columnDefinition = "char(21)")
	private String phone1;

	@Column(name = "PHONE2", columnDefinition = "char(21)")
	private String phone2;

	@Column(name = "EMAIL", columnDefinition = "char(255)")
	private String email;

	@Column(name = "GENDR")
	private short gender;

	@Column(name = "AGE")
	private short age;

	@Column(name = "APPLDT")
	private Date applicantDate;

	@Column(name = "STATS")
	private short status;

	@Column(name = "REJCRES")
	private short rejectReason;

	@Column(name = "REJCOMNS", columnDefinition = "char(90)")
	private String rejectedComents;

	@Column(name = "REFSOURC")
	private short referalSource;

	@Column(name = "REFDSCRP", columnDefinition = "char(90)")
	private String referalDesc;

	@Column(name = "COLORCOD")
	private int color;

	@Column(name = "MANGRNAM", columnDefinition = "char(150)")
	private String managerName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public short getGender() {
		return gender;
	}

	public void setGender(short gender) {
		this.gender = gender;
	}

	public short getAge() {
		return age;
	}

	public void setAge(short age) {
		this.age = age;
	}

	public Date getApplicantDate() {
		return applicantDate;
	}

	public void setApplicantDate(Date applicantDate) {
		this.applicantDate = applicantDate;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(short rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getRejectedComents() {
		return rejectedComents;
	}

	public void setRejectedComents(String rejectedComents) {
		this.rejectedComents = rejectedComents;
	}

	public short getReferalSource() {
		return referalSource;
	}

	public void setReferalSource(short referalSource) {
		this.referalSource = referalSource;
	}

	public String getReferalDesc() {
		return referalDesc;
	}

	public void setReferalDesc(String referalDesc) {
		this.referalDesc = referalDesc;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + age;
		result = prime * result + ((applicantDate == null) ? 0 : applicantDate.hashCode());
		result = prime * result + ((applicantHire == null) ? 0 : applicantHire.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + color;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + gender;
		result = prime * result + id;
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((managerName == null) ? 0 : managerName.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((phone1 == null) ? 0 : phone1.hashCode());
		result = prime * result + ((phone2 == null) ? 0 : phone2.hashCode());
		result = prime * result + ((referalDesc == null) ? 0 : referalDesc.hashCode());
		result = prime * result + referalSource;
		result = prime * result + rejectReason;
		result = prime * result + ((rejectedComents == null) ? 0 : rejectedComents.hashCode());
		result = prime * result + status;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return true;
	}

}
