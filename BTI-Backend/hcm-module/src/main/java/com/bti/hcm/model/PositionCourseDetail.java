package com.bti.hcm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR40173",indexes = {
        @Index(columnList = "POTCRSDINDX")
})
public class PositionCourseDetail extends HcmBaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POTCRSDINDX")
	private Integer id;
	
	@Column(name = "POTCRSDSEQN")
	private Integer sequence;
	
	
	/*@Transient
	private Integer positionCourseId;
	
	
	@Transient
	private Integer traningCourseId;*/
	
	@ManyToOne
	@JoinColumn(name = "POTCRSINDX")
	private PositionCourseToLink positionCourse;
	
	@ManyToOne
	@JoinColumn(name = "TRNCCLSINDX")
	private TraningCourse traningCourse;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	/*public Integer getPositionCourseId() {
		return positionCourseId;
	}

	public void setPositionCourseId(Integer positionCourseId) {
		this.positionCourseId = positionCourseId;
	}

	public Integer getTraningCourseId() {
		return traningCourseId;
	}

	public void setTraningCourseId(Integer traningCourseId) {
		this.traningCourseId = traningCourseId;
	}*/

	public PositionCourseToLink getPositionCourse() {
		return positionCourse;
	}

	public void setPositionCourse(PositionCourseToLink positionCourse) {
		this.positionCourse = positionCourse;
	}

	public TraningCourse getTraningCourse() {
		return traningCourse;
	}

	public void setTraningCourse(TraningCourse traningCourse) {
		this.traningCourse = traningCourse;
	}

}
