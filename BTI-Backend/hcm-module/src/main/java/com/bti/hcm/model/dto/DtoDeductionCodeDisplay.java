package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.EmployeeDeductionMaintenance;

public class DtoDeductionCodeDisplay {
	private Integer deductionCodeParimaryId;
	private String diductionId;
	private Integer buildChecksId;
	private List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList;

	public Integer getDeductionCodeParimaryId() {
		return deductionCodeParimaryId;
	}

	public void setDeductionCodeParimaryId(Integer deductionCodeParimaryId) {
		this.deductionCodeParimaryId = deductionCodeParimaryId;
	}

	public String getDiductionId() {
		return diductionId;
	}

	public void setDiductionId(String diductionId) {
		this.diductionId = diductionId;
	}

	public Integer getBuildChecksId() {
		return buildChecksId;
	}

	public void setBuildChecksId(Integer buildChecksId) {
		this.buildChecksId = buildChecksId;
	}

	public List<EmployeeDeductionMaintenance> getEmployeeDeductionMaintenanceList() {
		return employeeDeductionMaintenanceList;
	}

	public void setEmployeeDeductionMaintenanceList(List<EmployeeDeductionMaintenance> employeeDeductionMaintenanceList) {
		this.employeeDeductionMaintenanceList = employeeDeductionMaintenanceList;
	}

}
