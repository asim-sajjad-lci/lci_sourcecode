package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBtiMessageHcm;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSettingFamilyLeave;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceSettingFamilyLeave;

@RestController
@RequestMapping("/settingFamilyLeave")
public class ControllerSettingFamilyLeave extends BaseController{
	@Autowired
	ServiceSettingFamilyLeave serviceSettingFamilyLeave;
	
	@Autowired
	ServiceResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(ControllerSettingFamilyLeave.class);
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	
	
	/**
	 * @param request{ 
	"method":2,
	"isThisPeriod":2,
 	"periodStart":"2018-08-08"
 
            }response{
"code": 201,
"status": "CREATED",
"result": {
"id": null,
"method": 2,
"isThisPeriod": 2,
"periodStart": 1533686400000,
"pageNumber": null,
"pageSize": null,
"ids": null,
"isActive": null,
"messageType": null,
"message": null,
"deleteMessage": null,
"associateMessage": null,
"delete": null,
"isRepeat": null
},
"btiMessage": {
"message": "Setting Family Leave created Successfully!",
"messageShort": "SETTING_FAMILY_LEAVE_CREATED"
}
}
	 * @param dtoSettingFamilyLeave
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request,@RequestBody DtoSettingFamilyLeave dtoSettingFamilyLeave) throws Exception{
		LOGGER.info("Create SettingFamilyLeave Info");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSettingFamilyLeave = serviceSettingFamilyLeave.saveOrUpdate(dtoSettingFamilyLeave);
			responseMessage=displayMessage(dtoSettingFamilyLeave, "SETTING_FAMILY_LEAVE_CREATED", "SETTING_FAMILY_LEAVE_NOT_CREATED", response);
		} else {
			responseMessage =unauthorizedMsg(response);
		}
		LOGGER.debug("Create SettingFamilyLeave Method:"+responseMessage.getMessage());
		return responseMessage;
		
	}
	
	
	
	
	/**
	 * @param requestrequest
{"id":2,
  "method":3,
 "isThisPeriod":3,
 "periodStart":"2018-08-08"
 
            }
response{
"code": 201,
"status": "CREATED",
"result": {
"id": 2,
"method": 3,
"isThisPeriod": 3,
"periodStart": 1533686400000,
"pageNumber": null,
"pageSize": null,
"ids": null,
"isActive": null,
"messageType": null,
"message": null,
"deleteMessage": null,
"associateMessage": null,
"delete": null,
"isRepeat": null
},
"btiMessage": {
"message": "Setting Family Leave updated successfully.",
"messageShort": "SETTING_FAMILY_LEAVE_UPDATED_SUCCESS"
}
}
	 * @param dtoSettingFamilyLeave
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoSettingFamilyLeave dtoSettingFamilyLeave) throws Exception {
		LOGGER.info("Update SettingFamilyLeave Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSettingFamilyLeave = serviceSettingFamilyLeave.saveOrUpdate(dtoSettingFamilyLeave);
			responseMessage=displayMessage(dtoSettingFamilyLeave, "SETTING_FAMILY_LEAVE_UPDATED_SUCCESS", "SETTING_FAMILY_LEAVE_NOT_UPDATED", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Update SettingFamilyLeave Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param requestrequest
{"ids":[1]
            }
response{
"code": 201,
"status": "CREATED",
"result": {
"id": null,
"method": 0,
"isThisPeriod": 0,
"periodStart": null,
"pageNumber": null,
"pageSize": null,
"ids": null,
"isActive": null,
"messageType": "",
"message": null,
"deleteMessage": "Shift Code deleted successfully.",
"associateMessage": "Shift Code deleted successfully.",
"delete": [
  {
"id": 1,
"method": 0,
"isThisPeriod": 0,
"periodStart": null,
"pageNumber": null,
"pageSize": null,
"ids": null,
"isActive": null,
"messageType": null,
"message": null,
"deleteMessage": null,
"associateMessage": null,
"delete": null,
"isRepeat": null
}
],
"isRepeat": null
},
"btiMessage": {
"message": "Setting Family Leave deleted successfully.",
"messageShort": "SETTING_FAMILY_LEAVE_DELETED"
}
}
	 * @param dtoSettingFamilyLeave
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoSettingFamilyLeave dtoSettingFamilyLeave) throws Exception {
		LOGGER.info("Delete SettingFamilyLeave Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoSettingFamilyLeave.getIds() != null && !dtoSettingFamilyLeave.getIds().isEmpty()) {
				DtoSettingFamilyLeave dtoPosition1 = serviceSettingFamilyLeave.delete(dtoSettingFamilyLeave.getIds());
				if(dtoPosition1.getMessageType().equals("")) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							response.getMessageByShortAndIsDeleted("SETTING_FAMILY_LEAVE_DELETED", false), dtoPosition1);

				}else {
					DtoBtiMessageHcm btiMessageHcm =	new DtoBtiMessageHcm(null,"");
					btiMessageHcm.setMessage(dtoPosition1.getMessageType());
					btiMessageHcm.setMessageShort("SETTING_FAMILY_LEAVE_NOT_DELETE_ID_MESSAGE");
					
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							btiMessageHcm, dtoPosition1);

				}
				
				
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						response.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					response.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete SettingFamilyLeave Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	/**
	 * @param request
	 * {"id" : 2
            }
            response{
"code": 201,
"status": "CREATED",
"result": {
"id": 2,
"method": 3,
"isThisPeriod": 3,
"periodStart": 1533686400000,
"pageNumber": null,
"pageSize": null,
"ids": null,
"isActive": null,
"messageType": null,
"message": null,
"deleteMessage": null,
"associateMessage": null,
"delete": null,
"isRepeat": null
},
"btiMessage": {
"message": "Setting Family Leave details fetched successfully.",
"messageShort": "SETTING_FAMILY_LEAVE_GET_ALL"
}
}
	 * @param dtoSettingFamilyLeave
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getPositionById(HttpServletRequest request, @RequestBody DtoSettingFamilyLeave dtoSettingFamilyLeave) throws Exception {
		LOGGER.info("Get SettingFamilyLeave ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoSettingFamilyLeave dtoSettingFamilyLeave1 = serviceSettingFamilyLeave.getById(dtoSettingFamilyLeave.getId());
			responseMessage=displayMessage(dtoSettingFamilyLeave1, MessageConstant.SETTING_FAMILY_LEAVE_GET_ALL, MessageConstant.SETTING_FAMILY_LEAVE_NOT_GETTING, response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		LOGGER.debug("Get SettingFamilyLeave by Id Method:"+dtoSettingFamilyLeave.getId());
		return responseMessage;
	}
	
	
	
	/**
//	 * @param dtoSearch
	 * @param request{
		  "sortOn" : "",
		  "sortBy" : "",
		 "searchKeyword" : "",
		"pageNumber":"0",
		"pageSize":"10"
		}
		 @param response{
			"code": 200,
			"status": "OK",
			"result": {
			"searchKeyword": "",
			"pageNumber": 0,
			"pageSize": 10,
			"sortOn": "",
			"sortBy": "",
			"totalCount": 1,
			"records": [
			  {
			"id": 1,
			"shiftCodeId": "sh1test",
			"desc": "shiftCode test",
			"arabicDesc": "Shift test Arabic",
			"inActive": true,
			"shitPremium": 1,
			"amount": 26,
			"percent": 52,
			"pageNumber": null,
			"pageSize": null,
			"ids": null,
			"isActive": null,
			"messageType": null,
			"message": null,
			"deleteMessage": null,
			"associateMessage": null,
			"delete": null,
			"isRepeat": null
			}
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage search(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search  SettingFamilyLeave Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSettingFamilyLeave.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SETTING_FAMILY_LEAVE_GET_ALL", "SETTING_FAMILY_LEAVE_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search SettingFamilyLeave Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	
	
	/**
	 * @param dtoSearch
	 * @param requestrequest
{"searchKeyword" : ""
            }
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getIds(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search ShiftCode Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceSettingFamilyLeave.getIds(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "SETTING_FAMILY_LEAVE_GET_ALL", "SETTING_FAMILY_LEAVE_NOT_GETTING", response);
		} else {
			responseMessage = unauthorizedMsg(response);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search SettingFamilyLeave Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
}
