package com.bti.hcm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.hcm.model.Dimensions;

@Repository("repositoryDimensions")
public interface RepositoryDimensions extends JpaRepository<Dimensions, Integer> {

	@Query("select d from Dimensions d where d.isStatic=true and d.isDeleted=false")
	List<Dimensions> findByIsStatic();

	@Query("select count(*) from Dimensions d where d.isDeleted=false")
	public Integer getCountOfTotalDimensionsEntries();

	@Query("select d.moduleCode from Dimensions d where d.isDeleted=false")
	public List<Dimensions> findByModuleCodeId();

	@Query("select d from Dimensions d where d.isStatic=false and d.isDeleted=false")
	List<Dimensions> findByNonStatic();

	@Query("select d.id from Dimensions d where d.isDeleted=false")
	List<Integer> findById2();

//	@Query("select d.id from Dimensions d where d.isDeleted=false")
//	List<Dimensions> findById2();

	@Query("select d.id from Dimensions d where d.id=id and d.isDeleted=false")
	List<Dimensions> findByDimensionId(@Param("id") int id);
	
	@Query("select d from Dimensions d where d.id=:id and d.isDeleted=false")
	Dimensions findById(@Param("id")int id);

	List<Dimensions> findByIsDeleted(boolean b);

	@Modifying(clearAutomatically = true)	//ME
	@Transactional
	@Query("update Dimensions d set d.isDeleted =:b , d.updatedBy =:updatedBy where d.tableIndx =:tid ")
	void deleteDimensionByTableIndx(@Param("b") Boolean b, @Param("updatedBy") Integer updatedBy, @Param("tid") Integer tid);
	

	@Query("select d from Dimensions d where d.isDeleted=false and d.tableIndx =:id and d.id>5000 and d.id<6001")
	Dimensions findByTableIdxAnd5kRange(@Param("id") int id);	//ME
	
}
