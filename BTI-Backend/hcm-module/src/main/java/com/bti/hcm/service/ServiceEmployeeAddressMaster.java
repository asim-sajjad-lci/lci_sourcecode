package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.EmployeeAddressMaster;
import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.model.dto.DtoCity;
import com.bti.hcm.model.dto.DtoEmployeeAddressMaster;
import com.bti.hcm.model.dto.DtoHrCountry;
import com.bti.hcm.model.dto.DtoHrState;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryEmployeeAddressMaster;
import com.bti.hcm.repository.RepositoryHrCity;
import com.bti.hcm.repository.RepositoryHrCountry;
import com.bti.hcm.repository.RepositoryHrState;

@Service("/employeeAddressMaster")
public class ServiceEmployeeAddressMaster {



	/**
	 * @Description LOGGER use for put a logger in Department Service
	 */
	static Logger log = Logger.getLogger(ServiceEmployeeAddressMaster.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for
	 *              access of codeGenerator method in Department service
	 */

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in Department service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in Department service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryDepartment Autowired here using annotation of spring
	 *              for access of repositoryDepartment method in Department service
	 *              In short Access Department Query from Database using
	 *              repositoryDepartment.
	 */
	@Autowired
	RepositoryEmployeeAddressMaster repositoryEmployeeAddressMaster;
	
	@Autowired
	RepositoryHrCountry repositoryHrCountry;
	
	@Autowired
	RepositoryHrState repositoryHrState;
	
	@Autowired
	RepositoryHrCity repositoryHrCity;
	
	
	public DtoEmployeeAddressMaster saveOrUpdate(DtoEmployeeAddressMaster dtoEmployeeAddressMaster) {
		try {
			log.info("saveOrUpdate Employee Address Method");
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			EmployeeAddressMaster employeeAddressMaster = null;
			if (dtoEmployeeAddressMaster.getEmployeeAddressIndexId() != null && dtoEmployeeAddressMaster.getEmployeeAddressIndexId() > 0) {
				
				employeeAddressMaster = repositoryEmployeeAddressMaster.findByEmployeeAddressIndexIdAndIsDeleted(dtoEmployeeAddressMaster.getEmployeeAddressIndexId(), false);
				employeeAddressMaster.setUpdatedBy(loggedInUserId);
				employeeAddressMaster.setUpdatedDate(new Date());
			} else {
				employeeAddressMaster = new EmployeeAddressMaster();
				employeeAddressMaster.setCreatedDate(new Date());
				Integer rowId = repositoryEmployeeAddressMaster.getCountOfTotalEmployeeAddressMaster();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				employeeAddressMaster.setRowId(increment);
			}
			
			HrCountry hrCountry = repositoryHrCountry.findOne(dtoEmployeeAddressMaster.getCountryId());
			HrState hrState = repositoryHrState.findOne(dtoEmployeeAddressMaster.getStateId());
			HrCity hrCity = repositoryHrCity.findOne(dtoEmployeeAddressMaster.getCityId());
			
			employeeAddressMaster.setAddressId(dtoEmployeeAddressMaster.getAddressId());
			employeeAddressMaster.setAddress1(dtoEmployeeAddressMaster.getAddress1());
			employeeAddressMaster.setAddress2(dtoEmployeeAddressMaster.getAddress2());
			employeeAddressMaster.setAddressArabic1(dtoEmployeeAddressMaster.getAddressArabic1());
			employeeAddressMaster.setAddressArabic2(dtoEmployeeAddressMaster.getAddressArabic2());
			employeeAddressMaster.setPersonalEmail(dtoEmployeeAddressMaster.getPersonalEmail());
			employeeAddressMaster.setBusinessEmail(dtoEmployeeAddressMaster.getBusinessEmail());
			employeeAddressMaster.setpOBox(dtoEmployeeAddressMaster.getpOBox());
			employeeAddressMaster.setPostCode(dtoEmployeeAddressMaster.getPostCode());
			employeeAddressMaster.setPersonalPhone(dtoEmployeeAddressMaster.getPersonalPhone());
			employeeAddressMaster.setBusinessPhone(dtoEmployeeAddressMaster.getBusinessPhone());
			employeeAddressMaster.setOtherPhone(dtoEmployeeAddressMaster.getOtherPhone());
			employeeAddressMaster.setExt(dtoEmployeeAddressMaster.getExt());
			employeeAddressMaster.setLocationLinkGoogleMap(dtoEmployeeAddressMaster.getLocationLinkGoogleMap());
			employeeAddressMaster.setCity(hrCity);
			employeeAddressMaster.setCountry(hrCountry);
			employeeAddressMaster.setState(hrState);
			
			
			employeeAddressMaster = repositoryEmployeeAddressMaster.saveAndFlush(employeeAddressMaster);
			log.debug("Employee Address is:"+employeeAddressMaster.getEmployeeAddressIndexId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeAddressMaster;
	
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch search(DtoSearch dtoSearch) {
		try {
			log.info("search Employee Address Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
				
				if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
					if(dtoSearch.getSortOn().equals("addressId") || dtoSearch.getSortOn().equals("address1") ||
							dtoSearch.getSortOn().equals("businessEmail") || 
							dtoSearch.getSortOn().equals("addressArabic1") || 
							dtoSearch.getSortOn().equals("addressArabic2") || 
							dtoSearch.getSortOn().equals("pOBox") || 
							dtoSearch.getSortOn().equals("postCode") || 
							dtoSearch.getSortOn().equals("personalPhone") || 
							dtoSearch.getSortOn().equals("businessPhone") || 
							dtoSearch.getSortOn().equals("otherPhone") || 
							dtoSearch.getSortOn().equals("ext") || 
							dtoSearch.getSortOn().equals("locationLinkGoogleMap") || 
							dtoSearch.getSortOn().equals("address2") || 
							dtoSearch.getSortOn().equals("personalEmail")
							) {
						condition = dtoSearch.getSortOn();
					
				}else {
					condition = "employeeAddressIndexId";
					
				}
			}else{
				condition+="employeeAddressIndexId";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
				
			}
				dtoSearch.setTotalCount(this.repositoryEmployeeAddressMaster.predictiveEmployeeAddressMasterSearchTotalCount("%"+searchWord+"%"));
				List<EmployeeAddressMaster> employeeAddressMasterList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						employeeAddressMasterList = this.repositoryEmployeeAddressMaster.predictiveEmployeeAddressMasterSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						employeeAddressMasterList = this.repositoryEmployeeAddressMaster.predictiveEmployeeAddressMasterSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						employeeAddressMasterList = this.repositoryEmployeeAddressMaster.predictiveEmployeeAddressMasterSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
			
				if(employeeAddressMasterList != null && !employeeAddressMasterList.isEmpty()){
					List<DtoEmployeeAddressMaster> dtoTimeCodeList = new ArrayList<>();
					DtoEmployeeAddressMaster dtoEmployeeAddressMaster=null;
					for (EmployeeAddressMaster employeeAddressMaster : employeeAddressMasterList) {
						dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster(employeeAddressMaster);
						dtoEmployeeAddressMaster.setEmployeeAddressIndexId(employeeAddressMaster.getEmployeeAddressIndexId());
						
						if(employeeAddressMaster.getCity()!=null){
							dtoEmployeeAddressMaster.setCityId(employeeAddressMaster.getCity().getCityId());
							dtoEmployeeAddressMaster.setCityName(employeeAddressMaster.getCity().getCityName());
						}
						if(employeeAddressMaster.getCountry()!=null){
							dtoEmployeeAddressMaster.setCountryId(employeeAddressMaster.getCountry().getCountryId());
							dtoEmployeeAddressMaster.setCountryName(employeeAddressMaster.getCountry().getCountryName());
						}
						
						if(employeeAddressMaster.getState()!=null){
							dtoEmployeeAddressMaster.setStateName(employeeAddressMaster.getState().getStateName());
							dtoEmployeeAddressMaster.setStateId(employeeAddressMaster.getState().getStateId());
						}
						
						DtoCity dtoCity =new DtoCity();
						DtoHrState dtoHrState=new DtoHrState();
						DtoHrCountry dtoHrCountry=new DtoHrCountry();
						if(employeeAddressMaster.getCountry()!=null && employeeAddressMaster.getState()!=null&& employeeAddressMaster.getCity()!=null) {
							
								dtoHrCountry.setCountryId(employeeAddressMaster.getCountry().getCountryId());
								dtoHrCountry.setCountryName(employeeAddressMaster.getCountry().getCountryName());
								dtoHrCountry.setCountryCode(employeeAddressMaster.getCountry().getCountryCode());

								dtoHrState.setStateId(employeeAddressMaster.getState().getStateId());
							    dtoHrState.setStateName(employeeAddressMaster.getState().getStateName());
							    dtoHrState.setStateCode(employeeAddressMaster.getState().getStateCode());
							
							    dtoCity.setCityId(employeeAddressMaster.getCity().getCityId());
							    dtoCity.setCityName(employeeAddressMaster.getCity().getCityName());
							    
						}
						
						
						
						dtoEmployeeAddressMaster.setAddressId(employeeAddressMaster.getAddressId());
						dtoEmployeeAddressMaster.setAddress1(employeeAddressMaster.getAddress1());
						dtoEmployeeAddressMaster.setAddress2(employeeAddressMaster.getAddress2());
						dtoEmployeeAddressMaster.setAddressArabic1(employeeAddressMaster.getAddressArabic1());
						dtoEmployeeAddressMaster.setAddressArabic2(employeeAddressMaster.getAddressArabic2());
						dtoEmployeeAddressMaster.setPersonalEmail(employeeAddressMaster.getPersonalEmail());
						dtoEmployeeAddressMaster.setBusinessEmail(employeeAddressMaster.getBusinessEmail());
						dtoEmployeeAddressMaster.setpOBox(employeeAddressMaster.getpOBox());
						dtoEmployeeAddressMaster.setPostCode(employeeAddressMaster.getPostCode());
						dtoEmployeeAddressMaster.setPersonalPhone(employeeAddressMaster.getPersonalPhone());
						dtoEmployeeAddressMaster.setBusinessPhone(employeeAddressMaster.getBusinessPhone());
						dtoEmployeeAddressMaster.setOtherPhone(employeeAddressMaster.getOtherPhone());
						dtoEmployeeAddressMaster.setExt(employeeAddressMaster.getExt());
						dtoEmployeeAddressMaster.setLocationLinkGoogleMap(employeeAddressMaster.getLocationLinkGoogleMap());
						dtoEmployeeAddressMaster.setDtoHrCountry(dtoHrCountry);
						dtoEmployeeAddressMaster.setDtoHrState(dtoHrState);
						dtoEmployeeAddressMaster.setDtoCity(dtoCity);
						dtoTimeCodeList.add(dtoEmployeeAddressMaster);
					}
					dtoSearch.setRecords(dtoTimeCodeList);
				}
			}
			log.debug("Search Employee Address Size is:"+dtoSearch.getTotalCount());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	
	public DtoEmployeeAddressMaster repeatByAddressId(String timeCodeId) {
		log.info("repeatByAddressId Method");
		DtoEmployeeAddressMaster dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster();
		try {
			List<EmployeeAddressMaster> employeeAddressMaster=repositoryEmployeeAddressMaster.findByAddressId(timeCodeId);
			if(employeeAddressMaster!=null && !employeeAddressMaster.isEmpty()) {
				dtoEmployeeAddressMaster.setIsRepeat(true);
			}else {
				dtoEmployeeAddressMaster.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoEmployeeAddressMaster;
	}
	
	public DtoEmployeeAddressMaster getById(int id) {
		log.info("getById Method");
		DtoEmployeeAddressMaster dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster();
		try {
			if (id > 0) {
				EmployeeAddressMaster employeeAddressMaster = repositoryEmployeeAddressMaster.findByEmployeeAddressIndexIdAndIsDeleted(id, false);
				if (employeeAddressMaster != null) {
					dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster(employeeAddressMaster);
					dtoEmployeeAddressMaster.setEmployeeAddressIndexId(employeeAddressMaster.getEmployeeAddressIndexId());
					dtoEmployeeAddressMaster.setAddressId(employeeAddressMaster.getAddressId());
					dtoEmployeeAddressMaster.setAddress1(employeeAddressMaster.getAddress1());
					dtoEmployeeAddressMaster.setAddress2(employeeAddressMaster.getAddress2());
					dtoEmployeeAddressMaster.setAddressArabic1(employeeAddressMaster.getAddressArabic1());
					dtoEmployeeAddressMaster.setAddressArabic2(employeeAddressMaster.getAddressArabic2());
					dtoEmployeeAddressMaster.setPersonalEmail(employeeAddressMaster.getPersonalEmail());
					dtoEmployeeAddressMaster.setBusinessEmail(employeeAddressMaster.getBusinessEmail());
					dtoEmployeeAddressMaster.setpOBox(employeeAddressMaster.getpOBox());
					dtoEmployeeAddressMaster.setPostCode(employeeAddressMaster.getPostCode());
					dtoEmployeeAddressMaster.setPersonalPhone(employeeAddressMaster.getPersonalPhone());
					dtoEmployeeAddressMaster.setBusinessPhone(employeeAddressMaster.getBusinessPhone());
					dtoEmployeeAddressMaster.setOtherPhone(employeeAddressMaster.getOtherPhone());
					dtoEmployeeAddressMaster.setExt(employeeAddressMaster.getExt());
					dtoEmployeeAddressMaster.setLocationLinkGoogleMap(employeeAddressMaster.getLocationLinkGoogleMap());
					
				} else {
					dtoEmployeeAddressMaster.setMessageType("EMPLOYEE_ADDRESS_MASTER_GET_NOT_GETTING");

				}
			} else {
				dtoEmployeeAddressMaster.setMessageType("EMPLOYEE_ADDRESS_MASTER_GET_NOT_GETTING");

			}
			log.debug("EmployeeAddressIndexId is:"+dtoEmployeeAddressMaster.getEmployeeAddressIndexId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeAddressMaster;
	}
	
	
	
	public List<DtoEmployeeAddressMaster> getAllEmployeeAddressMasterDropDown() {
		log.info("getAllEmployeeAddressMasterDropDown  Method");
		List<DtoEmployeeAddressMaster> dtoEmployeeAddressMasterList = new ArrayList<>();
		try {
			List<EmployeeAddressMaster> list = repositoryEmployeeAddressMaster.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (EmployeeAddressMaster employeeAddressMaster : list) {
					
					DtoEmployeeAddressMaster dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster();
					dtoEmployeeAddressMaster.setEmployeeAddressIndexId(employeeAddressMaster.getEmployeeAddressIndexId());
					dtoEmployeeAddressMaster.setAddressId(employeeAddressMaster.getAddressId());
					dtoEmployeeAddressMaster.setAddress1(employeeAddressMaster.getAddress1());
					dtoEmployeeAddressMaster.setAddress2(employeeAddressMaster.getAddress2());
					dtoEmployeeAddressMaster.setPersonalEmail(employeeAddressMaster.getPersonalEmail());
					dtoEmployeeAddressMaster.setBusinessEmail(employeeAddressMaster.getBusinessEmail());
					dtoEmployeeAddressMaster.setAddressArabic1(employeeAddressMaster.getAddressArabic1());
					dtoEmployeeAddressMaster.setAddressArabic2(employeeAddressMaster.getAddressArabic2());
					dtoEmployeeAddressMaster.setpOBox(employeeAddressMaster.getpOBox());
					dtoEmployeeAddressMaster.setPostCode(employeeAddressMaster.getPostCode());
					dtoEmployeeAddressMaster.setPersonalPhone(employeeAddressMaster.getPersonalPhone());
					dtoEmployeeAddressMaster.setBusinessPhone(employeeAddressMaster.getBusinessPhone());
					dtoEmployeeAddressMaster.setOtherPhone(employeeAddressMaster.getOtherPhone());
					dtoEmployeeAddressMaster.setExt(employeeAddressMaster.getExt());
					dtoEmployeeAddressMaster.setLocationLinkGoogleMap(employeeAddressMaster.getLocationLinkGoogleMap());
					dtoEmployeeAddressMaster.setStateId(employeeAddressMaster.getState().getStateId());
					dtoEmployeeAddressMaster.setStateName(employeeAddressMaster.getState().getStateName());
					
					dtoEmployeeAddressMaster.setCountryId(employeeAddressMaster.getCountry().getCountryId());
					dtoEmployeeAddressMaster.setCountryName(employeeAddressMaster.getCountry().getCountryName());
					
					
					dtoEmployeeAddressMaster.setCityId(employeeAddressMaster.getCity().getCityId());
					dtoEmployeeAddressMaster.setCityName(employeeAddressMaster.getCity().getCityName());
					dtoEmployeeAddressMasterList.add(dtoEmployeeAddressMaster);
				}
			}
			log.debug("getAllEmployeeAddressMasterDropDown is:" + dtoEmployeeAddressMasterList.size());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoEmployeeAddressMasterList;
	}
	
	
	public DtoEmployeeAddressMaster delete(List<Integer> ids) {
		log.info("delete EmployeeAddressMaster Method");
		DtoEmployeeAddressMaster dtoEmployeeAddressMaster = new DtoEmployeeAddressMaster();
		dtoEmployeeAddressMaster
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_ADDRESS_MASTER_GET_DELETED", false));
		dtoEmployeeAddressMaster.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("EMPLOYEE_ADDRESS_MASTER_GET_DELETED", false));
		List<DtoEmployeeAddressMaster> deleteEmployeeAddressMaster = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("EMPLOYEE_ADDRESS_MASTER_GET_NOT_DELETE_ID_MESSAGE", false).getMessage());
		try {
			for (Integer payCodeId : ids) {
				EmployeeAddressMaster employeeAddressMaster2 = repositoryEmployeeAddressMaster.findOne(payCodeId);
				if(employeeAddressMaster2.getEmployeeMaster().isEmpty()) {
					DtoEmployeeAddressMaster dtoTimeCode1 = new DtoEmployeeAddressMaster();
					dtoTimeCode1.setEmployeeAddressIndexId(employeeAddressMaster2.getEmployeeAddressIndexId());
					repositoryEmployeeAddressMaster.deleteEmployeeAddressMaster(true, loggedInUserId, payCodeId);
					deleteEmployeeAddressMaster.add(dtoTimeCode1);
				}else {
					inValidDelete = true;
				}
			}
			if(inValidDelete){
				dtoEmployeeAddressMaster.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoEmployeeAddressMaster.setMessageType("");
				
			}
			
			dtoEmployeeAddressMaster.setDelete(deleteEmployeeAddressMaster);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete EmployeeAddressMaster :"+dtoEmployeeAddressMaster.getEmployeeAddressIndexId());
		return dtoEmployeeAddressMaster;
	}

}
