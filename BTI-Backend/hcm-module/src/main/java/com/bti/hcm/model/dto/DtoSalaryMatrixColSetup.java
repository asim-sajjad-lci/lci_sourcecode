package com.bti.hcm.model.dto;

import java.util.List;

import com.bti.hcm.model.SalaryMatrixColSetup;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoSalaryMatrixColSetup extends DtoBase{

	private Integer colSalaryMatrixIndexId;
	private Integer salaryMatrixId;
	private String colSalaryMatrixDescription;
	private String arabicColSalaryMatrixDescription;
	private List<DtoSalaryMatrixColSetup> deleteSalaryMatrixColSetup;
	
	public Integer getColSalaryMatrixIndexId() {
		return colSalaryMatrixIndexId;
	}
	public void setColSalaryMatrixIndexId(Integer colSalaryMatrixIndexId) {
		this.colSalaryMatrixIndexId = colSalaryMatrixIndexId;
	}
	public Integer getSalaryMatrixId() {
		return salaryMatrixId;
	}
	public void setSalaryMatrixId(Integer salaryMatrixId) {
		this.salaryMatrixId = salaryMatrixId;
	}
	public String getColSalaryMatrixDescription() {
		return colSalaryMatrixDescription;
	}
	public void setColSalaryMatrixDescription(String colSalaryMatrixDescription) {
		this.colSalaryMatrixDescription = colSalaryMatrixDescription;
	}
	public String getArabicColSalaryMatrixDescription() {
		return arabicColSalaryMatrixDescription;
	}
	public void setArabicColSalaryMatrixDescription(String arabicColSalaryMatrixDescription) {
		this.arabicColSalaryMatrixDescription = arabicColSalaryMatrixDescription;
	}
	public List<DtoSalaryMatrixColSetup> getDeleteSalaryMatrixColSetup() {
		return deleteSalaryMatrixColSetup;
	}
	public void setDeleteSalaryMatrixColSetup(List<DtoSalaryMatrixColSetup> deleteSalaryMatrixColSetup) {
		this.deleteSalaryMatrixColSetup = deleteSalaryMatrixColSetup;
	}
	public DtoSalaryMatrixColSetup() {

	}
	
	public DtoSalaryMatrixColSetup(SalaryMatrixColSetup salaryMatrixColSetup) {
		
	}


}
