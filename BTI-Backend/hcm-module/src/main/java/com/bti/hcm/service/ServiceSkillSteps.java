package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.SkillsSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.model.dto.DtoSkillSteup;
import com.bti.hcm.repository.RepositorySkillSteps;
import com.bti.hcm.util.UtilDateAndTimeHr;

@Service("serviceSkillSteps")
public class ServiceSkillSteps {

	/**
	 * @Description LOGGER use for put a logger in SkillSteps Service
	 */
	static Logger log = Logger.getLogger(ServiceSkillSteps.class.getName());

	/**
	 * @Description codeGenerator Autowired here using annotation of spring for access of codeGenerator method in SkillSteps service
	 */


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in SkillSteps service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in SkillSteps service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description RepositorySkillSteps Autowired here using annotation of spring for access of RepositorySkillSteps method in SkillSteps service
	 * 				In short Access SkillSteps Query from Database using RepositorySkillSteps.
	 */
	@Autowired
	RepositorySkillSteps repositorySkillSteps;

	/**
	 * @param dtoSkillSteup
	 * @return
	 */
	public DtoSkillSteup saveOrUpdateSkillSteup(DtoSkillSteup dtoSkillSteup) {
		log.info("saveOrUpdateSkillSteup Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		SkillsSetup skillsSetup = null;
		if (dtoSkillSteup.getId() != null && dtoSkillSteup.getId() > 0) {
			skillsSetup = repositorySkillSteps.findByIdAndIsDeleted(dtoSkillSteup.getId(), false);
			skillsSetup.setUpdatedBy(loggedInUserId);
			skillsSetup.setUpdatedDate(new Date());
		} else {
			skillsSetup = new SkillsSetup();
			skillsSetup.setCreatedDate(new Date());
			
			Integer rowId = repositorySkillSteps.getCountOfTotalSkillSteps();
			Integer increment=0;
			if(rowId!=0) {
				increment= rowId+1;
			}else {
				increment=1;
			}
			skillsSetup.setRowId(increment);
		}
		skillsSetup.setCompensation(dtoSkillSteup.getCompensation());
		skillsSetup.setFrequency(dtoSkillSteup.getFrequency());
		skillsSetup.setSkillId(dtoSkillSteup.getSkillId());
		skillsSetup.setSkillDesc(dtoSkillSteup.getSkillDesc());
		skillsSetup.setId(dtoSkillSteup.getId());
		skillsSetup.setArabicDesc(dtoSkillSteup.getArabicDesc());
		skillsSetup.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
		skillsSetup = repositorySkillSteps.saveAndFlush(skillsSetup);
		log.debug("SkillSteup is:"+skillsSetup.getId());
		return dtoSkillSteup;
	}

	/**
	 * @param dtoSkillSteup
	 * @return
	 */
	public DtoSearch getAllSkillSteup() {
		log.info("getAllSkillSteup Method");
		DtoSearch dtoSearch = new DtoSearch();
		DtoSkillSteup dtoSkillSteup = null;
		dtoSearch.setTotalCount(repositorySkillSteps.getCountOfTotalSkillSetup());
		List<SkillsSetup> skillSteupList = null;
		
			skillSteupList = repositorySkillSteps.findByIsDeleted(false);
		
		
		List<DtoSkillSteup> dtoSkillSteupList=new ArrayList<>();
		if(skillSteupList!=null && !skillSteupList.isEmpty())
		{
			for (SkillsSetup skillsSetup : skillSteupList) 
			{
				dtoSkillSteup=new DtoSkillSteup(skillsSetup);
				dtoSkillSteup.setSkillId(skillsSetup.getSkillId());
				dtoSkillSteup.setId(skillsSetup.getId());
				dtoSkillSteupList.add(dtoSkillSteup);
			}
			dtoSearch.setRecords(dtoSkillSteupList);
		}
		log.debug("All Skill Steup List Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoSkillSteup deleteSkillSteup(List<Integer> ids) {
		
		log.info("deleteSkillSteup Method");
		DtoSkillSteup dtoSkillSteup = new DtoSkillSteup();
		dtoSkillSteup
				.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("SKILL_STEUP_DELETED", false));
		dtoSkillSteup.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("SKILL_STEUP_DELETED", false));
		List<DtoSkillSteup> deleteSkillSetup = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		boolean inValidDelete = false;
		StringBuilder  invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse.getMessageByShortAndIsDeleted("SKILL_SETUP_UNABLE_DELETED_ID_MESSAGE", false).getMessage());
		try {
			for (Integer skillStepId : ids) {
				SkillsSetup skillsteup = repositorySkillSteps.findOne(skillStepId);
				if(skillsteup.getListSkillsetDesc().isEmpty() && skillsteup.getListDesc().isEmpty()) {
					DtoSkillSteup dtoskillSetup2 = new DtoSkillSteup();
					dtoskillSetup2.setId(skillStepId);
					dtoskillSetup2.setSkillDesc(skillsteup.getSkillDesc());
					repositorySkillSteps.deleteSingleSkillSteup(true, loggedInUserId, skillStepId);
					deleteSkillSetup.add(dtoskillSetup2);
				}else {
					inValidDelete = true;
				}
				

			}
			if(inValidDelete){
				dtoSkillSteup.setMessageType(invlidDeleteMessage.toString());
				
			}
			if(!inValidDelete){
				dtoSkillSteup.setMessageType("");
				
			}
			dtoSkillSteup.setDeleteSkillSteup(deleteSkillSetup);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete SkillSteup :"+dtoSkillSteup.getId());
		return dtoSkillSteup;
	}

	public DtoSkillSteup getBySkillSteupId(Integer id) {
		log.info("getBySkillSteupId Method");
		DtoSkillSteup dtoSkillSteup = new DtoSkillSteup();
		if (id > 0) {
			SkillsSetup skillsSetup = repositorySkillSteps.findByIdAndIsDeleted(id, false);
			if (skillsSetup != null) {
				dtoSkillSteup = new DtoSkillSteup(skillsSetup);
				dtoSkillSteup.setSkillId(skillsSetup.getSkillId());
				dtoSkillSteup.setSkillDesc(skillsSetup.getSkillDesc());
				dtoSkillSteup.setFrequency(skillsSetup.getFrequency());
				dtoSkillSteup.setCompensation(skillsSetup.getCompensation());
				dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
				dtoSkillSteup.setId(skillsSetup.getId());
			} else {
				dtoSkillSteup.setMessageType("SKILL_STEUP_NOT_GETTING");

			}
		} else {
			dtoSkillSteup.setMessageType("SKILL_STEUP_NOT_GETTING");

		}
		log.debug("SkillSteup By Id is:"+dtoSkillSteup.getId());
		return dtoSkillSteup;
	}

	/**
	 * @param skillId
	 * @return
	 */
	public DtoSkillSteup repeatBySkillSteupId(String skillId) {
		
		log.info("repeatBySkillSteupId Method");
		DtoSkillSteup dtoSkillsteup = new DtoSkillSteup();
		try {
			List<SkillsSetup> skillsteup=repositorySkillSteps.findByskillId(skillId);
			if(skillsteup!=null && !skillsteup.isEmpty()) {
				dtoSkillsteup.setIsRepeat(true);
			}else {
				dtoSkillsteup.setIsRepeat(false);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSkillsteup;
	}

	public DtoSearch searchSkillSteup(DtoSearch dtoSearch) {
		log.info("searchSkillSteup Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
            String condition="";
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				
				if(dtoSearch.getSortOn().equals("skillId") || dtoSearch.getSortOn().equals("skillDesc") || dtoSearch.getSortOn().equals("arabicDesc") || dtoSearch.getSortOn().equals("compensation") || dtoSearch.getSortOn().equals("frequency")) {
					condition=dtoSearch.getSortOn();
				}else {
					condition="id";
				}
				
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
			
			
			dtoSearch.setTotalCount(this.repositorySkillSteps.predictiveSkillSteupSearchTotalCount("%"+searchWord+"%"));
			List<SkillsSetup> skillSteupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					skillSteupList = this.repositorySkillSteps.predictiveSkillsSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					skillSteupList =  this.repositorySkillSteps.predictiveSkillsSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					skillSteupList =  this.repositorySkillSteps.predictiveSkillsSetupSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			
			
		
			if(skillSteupList != null && !skillSteupList.isEmpty()){
				List<DtoSkillSteup> dtoSkillSteupList = new ArrayList<>();
				DtoSkillSteup dtoSkillSteup=null;
				for (SkillsSetup skillsSetup : skillSteupList) {
					dtoSkillSteup = new DtoSkillSteup(skillsSetup);
					
					dtoSkillSteup.setSkillId(skillsSetup.getSkillId());
					dtoSkillSteup.setSkillDesc(skillsSetup.getSkillDesc());
					dtoSkillSteup.setFrequency(skillsSetup.getFrequency());
					dtoSkillSteup.setCompensation(skillsSetup.getCompensation());
					dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
					dtoSkillSteup.setId(skillsSetup.getId());
					
					dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
					dtoSkillSteupList.add(dtoSkillSteup);
				}
				dtoSearch.setRecords(dtoSkillSteupList);
			}
		}
		log.debug("Search SkillSteup Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoSearch searchSkillSetupId(DtoSearch dtoSearch) {
		log.info("searchSkillSetupId Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			List<DtoSkillSteup> dtoskillsetIdList =new ArrayList<>();
			List<SkillsSetup> skillsetIdList =null;
			
				skillsetIdList = this.repositorySkillSteps.predictiveSkillSetupIdSearchWithPagination("%"+searchWord+"%");
				for (SkillsSetup skillsSetup : skillsetIdList) {
					DtoSkillSteup dtoSkillSteup = new DtoSkillSteup();
					dtoSkillSteup.setId(skillsSetup.getId());
					dtoSkillSteup.setSkillId(skillsSetup.getSkillId());
					dtoSkillSteup.setSkillDesc(skillsSetup.getSkillDesc());
					dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
					
					dtoskillsetIdList.add(dtoSkillSteup);
				}
				dtoSearch.setTotalCount(dtoskillsetIdList.size());
				dtoSearch.setRecords(dtoskillsetIdList);
		}
		
		return dtoSearch;
	}
	
	
	public DtoSearch searchIds(DtoSearch dtoSearch) {
		log.info("searchSkillSteup Method");
		if(dtoSearch != null){
			String searchWord=dtoSearch.getSearchKeyword();
			
            String condition="";
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				
				if(dtoSearch.getSortOn().equals("skillId") || dtoSearch.getSortOn().equals("skillDesc") || dtoSearch.getSortOn().equals("arabicDesc") || dtoSearch.getSortOn().equals("compensation") || dtoSearch.getSortOn().equals("frequency")) {
					condition=dtoSearch.getSortOn();
				}else {
					condition="id";
				}
				
			}else{
				condition+="id";
				dtoSearch.setSortOn("");
				dtoSearch.setSortBy("");
			}
			
			
			List<SkillsSetup> skillSteupList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				
				if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
					skillSteupList = this.repositorySkillSteps.searchIds("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
				}
				if(dtoSearch.getSortBy().equals("ASC")){
					skillSteupList =  this.repositorySkillSteps.searchIds("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
				}else if(dtoSearch.getSortBy().equals("DESC")){
					skillSteupList =  this.repositorySkillSteps.searchIds("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
				}
				
			}
			
			
			
		
			if(skillSteupList != null && !skillSteupList.isEmpty()){
				List<DtoSkillSteup> dtoSkillSteupList = new ArrayList<>();
				DtoSkillSteup dtoSkillSteup=null;
				for (SkillsSetup skillsSetup : skillSteupList) {
					dtoSkillSteup = new DtoSkillSteup(skillsSetup);
					
					dtoSkillSteup.setSkillId(skillsSetup.getSkillId());
					dtoSkillSteup.setSkillDesc(skillsSetup.getSkillDesc());
					dtoSkillSteup.setFrequency(skillsSetup.getFrequency());
					dtoSkillSteup.setCompensation(skillsSetup.getCompensation());
					dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
					dtoSkillSteup.setId(skillsSetup.getId());
					
					dtoSkillSteup.setArabicDesc(skillsSetup.getArabicDesc());
					dtoSkillSteupList.add(dtoSkillSteup);
				}
				dtoSearch.setRecords(dtoSkillSteupList);
			}
		}
		log.debug("Search SkillSteup Size is:"+dtoSearch.getTotalCount());
		return dtoSearch;
	}

	
public DtoSearch searchAllSkillId(DtoSearch dtoSearch) {
	log.info("searchAllSkillId Method");
	if(dtoSearch != null){
		String searchWord=dtoSearch.getSearchKeyword();
		
		List<SkillsSetup> skillsSetupList =null;
		
			skillsSetupList = this.repositorySkillSteps.predictiveSearchAllSkillIdWithPagination("%"+searchWord+"%");
			if(!skillsSetupList.isEmpty()) {

				if(!skillsSetupList.isEmpty()){
					List<DtoSkillSteup> dtoSkillSteup = new ArrayList<>();
					DtoSkillSteup dtoSkillSteup1=null;
					for (SkillsSetup skillsSetup : skillsSetupList) {
						
						dtoSkillSteup1=new DtoSkillSteup(skillsSetup);
						dtoSkillSteup1.setId(skillsSetup.getId());
						dtoSkillSteup1.setSkillId(skillsSetup.getSkillId());
						dtoSkillSteup1.setSkillDesc(skillsSetup.getSkillId()+" | "+skillsSetup.getSkillDesc());
						
						
						dtoSkillSteup.add(dtoSkillSteup1);
					}
					dtoSearch.setRecords(dtoSkillSteup);
				}

				dtoSearch.setTotalCount(skillsSetupList.size());
			}
		
	}
	
	return dtoSearch;
}

	
	
	
}
