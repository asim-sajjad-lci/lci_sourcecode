package com.bti.hcm.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.hcm.model.Batches;
import com.bti.hcm.model.BenefitCode;
import com.bti.hcm.model.DeductionCode;
import com.bti.hcm.model.EmployeeMaster;
import com.bti.hcm.model.FinancialDimensions;
import com.bti.hcm.model.PayCode;
import com.bti.hcm.model.TransactionEntry;
import com.bti.hcm.model.TransactionEntryDetail;
import com.bti.hcm.model.dto.DtoTransactionEntry;
import com.bti.hcm.repository.RepositoryBatches;
import com.bti.hcm.repository.RepositoryBenefitCode;
import com.bti.hcm.repository.RepositoryBuildChecks;
import com.bti.hcm.repository.RepositoryCalculateChecks;
import com.bti.hcm.repository.RepositoryDeductionCode;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryDistribution;
import com.bti.hcm.repository.RepositoryEmployeeBenefitMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeDeductionMaintenance;
import com.bti.hcm.repository.RepositoryEmployeeMaster;
import com.bti.hcm.repository.RepositoryEmployeePayCodeMaintenance;
import com.bti.hcm.repository.RepositoryFinancialDimensions;
import com.bti.hcm.repository.RepositoryPayCode;
import com.bti.hcm.repository.RepositoryPayrollAccounts;
import com.bti.hcm.repository.RepositoryTransactionEntry;
import com.bti.hcm.repository.RepositoryTransactionEntryDetail;

@Service("ServiceNewTransactionEntry")
public class ServiceNewTransactionEntry {

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryTransactionEntry repositoryTransactionEntry;

	@Autowired
	RepositoryTransactionEntryDetail repositoryTransactionEntryDetail;

	@Autowired
	RepositoryBatches repositoryBatches;

	@Autowired
	RepositoryEmployeeMaster repositoryEmployeeMaster;

	@Autowired
	RepositoryDepartment repositoryDepartment;

	@Autowired
	RepositoryDeductionCode repositoryDeductionCode;

	@Autowired
	RepositoryBenefitCode repositoryBenefitCode;

	@Autowired
	RepositoryFinancialDimensions repositoryFinancialDimensions;

	@Autowired
	RepositoryPayCode repositoryPayCode;

	@Autowired
	RepositoryPayrollAccounts repositoryPayrollAccounts;

	@Autowired
	RepositoryCalculateChecks repositoryCalculateChecks;

	@Autowired
	RepositoryEmployeePayCodeMaintenance employeePayCodeMaintenance;

	@Autowired
	RepositoryBuildChecks repositoryBuildChecks;

	@Autowired
	RepositoryDistribution repositoryDistribution;

	@Autowired
	RepositoryEmployeeDeductionMaintenance repositoryEmployeeDeductionMaintenance;

	@Autowired
	RepositoryEmployeeBenefitMaintenance repositoryEmployeeBenefitMaintenance;

	static Logger log = Logger.getLogger(ServiceNewTransactionEntry.class.getName());

	public List<DtoTransactionEntry> saveOrUpdate(List<DtoTransactionEntry> dtoTransactionEntrys) {
		log.info("enter into save or update TransactionEntry");
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));

			for (Iterator<DtoTransactionEntry> iterator = dtoTransactionEntrys.iterator(); iterator.hasNext();) {
				TransactionEntry transactionEntry = null;

				DtoTransactionEntry dtoTransactionEntry = iterator.next();
				if (dtoTransactionEntry.getId() != null && dtoTransactionEntry.getId() > 0) {
					transactionEntry = repositoryTransactionEntry
							.findByIdAndIsDeletedAndFromBuild(dtoTransactionEntry.getId(), false, false);
					transactionEntry.setUpdatedBy(loggedInUserId);
					transactionEntry.setUpdatedDate(new Date());
					transactionEntry.setCreatedDate(new Date());

					try {
						repositoryTransactionEntryDetail.deleteByTransactionEntry(true, loggedInUserId,
								transactionEntry.getId());
					} catch (Exception e) {
						log.error(e);
					}

				} else {

					transactionEntry = new TransactionEntry();
					transactionEntry.setCreatedDate(new Date());
					TransactionEntry transactionEntry2 = repositoryTransactionEntry.findTopByOrderByIdDesc();
					// Integer rowId = repositoryTransactionEntry.findAll().size();
					Integer increment = 0;
					if (transactionEntry2 != null) {
						if (transactionEntry2.getRowId() != 0) {
							increment = transactionEntry2.getRowId() + 1;
						} else {
							increment = 1;
						}
						transactionEntry.setRowId(increment);
					} else {
						increment = 1;
					}

				}
				Batches batches = null;

				if (dtoTransactionEntry.getBatches() != null) {
					batches = repositoryBatches.findByIdAndIsDeleted(dtoTransactionEntry.getBatches().getId(), false);
				}
				transactionEntry.setUpdatedRow(new Date());
				transactionEntry.setBatches(batches);
				transactionEntry.setEntryNumber(repositoryTransactionEntry.findAll().size() + 1);
				transactionEntry.setEntryDate(new Date());
				transactionEntry.setFromDate(dtoTransactionEntry.getFromDate());
				transactionEntry.setToDate(dtoTransactionEntry.getToDate());
				transactionEntry.setDescription(dtoTransactionEntry.getDescription());
				transactionEntry.setArabicDescription(dtoTransactionEntry.getArabicDescription());
				transactionEntry.setFromBuild(false);
				transactionEntry = repositoryTransactionEntry.saveAndFlush(transactionEntry);
				TransactionEntryDetail transactionEntryDetailLastRecord = repositoryTransactionEntryDetail
						.findTopByOrderByIdDesc();

				EmployeeMaster employeeMaster = null;
				PayCode payCode = null;
				DeductionCode deductionCode = null;
				BenefitCode benefitCode = null;
				FinancialDimensions financialDimensions = null;

				if (dtoTransactionEntry.getTransactionEntryDetail().getTransactionType() == 1) {
					payCode = repositoryPayCode
							.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
				} else if (dtoTransactionEntry.getTransactionEntryDetail().getTransactionType() == 2) {
					deductionCode = repositoryDeductionCode
							.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
				} else {
					benefitCode = repositoryBenefitCode
							.findByIdAndIsDeleted(dtoTransactionEntry.getTransactionEntryDetail().getCodeId(), false);
				}

				if (dtoTransactionEntry.getTransactionEntryDetail().getEmployeeMaster() != null) {
					employeeMaster = repositoryEmployeeMaster.findOne(
							dtoTransactionEntry.getTransactionEntryDetail().getEmployeeMaster().getEmployeeIndexId());
				}

				if (dtoTransactionEntry.getDimensions() != null && dtoTransactionEntry.getDimensions().getId() > 0) {
					financialDimensions = repositoryFinancialDimensions
							.findOne(dtoTransactionEntry.getDimensions().getId());
				}
				TransactionEntryDetail transactionEntryDetail = null;
				if (dtoTransactionEntry.getTransactionEntryDetail() != null
						&& dtoTransactionEntry.getTransactionEntryDetail().getId() != null
						&& dtoTransactionEntry.getTransactionEntryDetail().getId() > 0) {
					transactionEntryDetail = repositoryTransactionEntryDetail
							.findOne(dtoTransactionEntry.getTransactionEntryDetail().getId());
				} else {
					transactionEntryDetail = new TransactionEntryDetail();
				}
				transactionEntryDetail.setDimensions(financialDimensions);
				transactionEntryDetail.setTransactionEntry(transactionEntry);
				transactionEntryDetail
						.setTransactionType(dtoTransactionEntry.getTransactionEntryDetail().getTransactionType());
				if (transactionEntryDetailLastRecord != null) {
					transactionEntryDetail.setEntryNumber(transactionEntryDetailLastRecord.getId() + 1);
				} else {
					transactionEntryDetail.setEntryNumber(1);
				}
				if (transactionEntryDetailLastRecord != null) {
					transactionEntryDetail.setDetailsSequence(transactionEntryDetailLastRecord.getId() + 1);
				} else {
					transactionEntryDetail.setDetailsSequence(1);
				}

				transactionEntryDetail.setCode(payCode);
				transactionEntryDetail.setBenefitCode(benefitCode);
				transactionEntryDetail.setDeductionCode(deductionCode);
				transactionEntryDetail.setEmployeeMaster(employeeMaster);
				if (employeeMaster != null) {
					transactionEntryDetail.setDepartment(employeeMaster.getDepartment());
				}
				transactionEntryDetail.setCreatedDate(new Date());
				if (transactionEntryDetailLastRecord != null) {
					transactionEntryDetail.setRowId(transactionEntryDetailLastRecord.getId() + 1);
				} else {
					transactionEntryDetail.setRowId(1);
				}

				transactionEntryDetail.setUpdatedRow(new Date());
				transactionEntryDetail.setUpdatedBy(loggedInUserId);
				transactionEntryDetail.setAmount(dtoTransactionEntry.getTransactionEntryDetail().getAmount());
				transactionEntryDetail.setFromTranscationEntry(true);

				transactionEntryDetail.setPayRate(dtoTransactionEntry.getTransactionEntryDetail().getPayRate());
				transactionEntryDetail.setFromDate(transactionEntry.getFromDate());
				transactionEntryDetail.setToDate(transactionEntry.getToDate());
				/*
				 * if(transactionEntryDetail.getTransactionType()==1) {
				 * transactionEntryDetail.setFromBuild(false); }else {
				 * transactionEntryDetail.setFromBuild(true); }
				 */
				transactionEntryDetail.setFromBuild(true);
//				transactionEntryDetail.setBatches(batches);
				repositoryTransactionEntryDetail.saveAndFlush(transactionEntryDetail);

			}
		}

		catch (Exception e) {
			log.error(e);
		}

		return dtoTransactionEntrys;

	}

}
