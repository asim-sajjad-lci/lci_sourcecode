/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.hcm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.HrCity;
import com.bti.hcm.model.HrCountry;
import com.bti.hcm.model.HrState;
import com.bti.hcm.model.Location;
import com.bti.hcm.model.dto.DtoLocation;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryHrCity;
import com.bti.hcm.repository.RepositoryHrCountry;
import com.bti.hcm.repository.RepositoryHrState;
import com.bti.hcm.repository.RepositoryLocation;
import com.bti.hcm.util.UtilDateAndTimeHr;
/**
 * Description: Service Location
 * Project: Hcm 
 * Version: 0.0.1
 */

@Service("serviceLocation")
public class ServiceLocation {
	
	static Logger log = Logger.getLogger(ServiceLocation.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryLocation repositoryLocation;
	
	@Autowired
	RepositoryHrCountry repositoryHrCountry;
	
	@Autowired
	RepositoryHrState repositoryHrState;
	
	@Autowired
	RepositoryHrCity repositoryHrCity;
	
	public DtoLocation saveOrUpdateLocation(DtoLocation dtoLocation) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
			Location location=null;
			if (dtoLocation.getId() != null && dtoLocation.getId() > 0) {
				location = repositoryLocation.findByIdAndIsDeleted(dtoLocation.getId(), false);
				location.setUpdatedBy(loggedInUserId);
				location.setUpdatedDate(new Date());
			} else {
				location = new Location();
				location.setCreatedDate(new Date());
				Integer rowId = repositoryLocation.getCountOfTotaLocation();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				location.setRowId(increment);
			}
			HrCountry hrCountry = repositoryHrCountry.findOne(dtoLocation.getCountryId());
			HrState hrState = repositoryHrState.findOne(dtoLocation.getStateId());
			HrCity hrCity = repositoryHrCity.findOne(dtoLocation.getCityId());
			location.setDescription(dtoLocation.getDescription());
			location.setArabicDescription(dtoLocation.getArabicDescription());
			location.setContactName(dtoLocation.getContactName());
			location.setFax(dtoLocation.getFax());
			location.setLocationAddress(dtoLocation.getLocationAddress());
			location.setLocationId(dtoLocation.getLocationId());
			location.setCountry(hrCountry);
			location.setCity(hrCity);
			location.setState(hrState);
			location.setPhone(dtoLocation.getPhone());
			location.setUpdatedRow(UtilDateAndTimeHr.localToUTC());
			 repositoryLocation.saveAndFlush(location);
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoLocation;
	}
	
	public DtoSearch getAllLocation(DtoLocation dtoLocation) {
		DtoSearch dtoSearch = new DtoSearch();
		try {
			dtoSearch.setPageNumber(dtoLocation.getPageNumber());
			dtoSearch.setPageSize(dtoLocation.getPageSize());
			dtoSearch.setTotalCount(repositoryLocation.getCountOfTotalLocation());
			List<Location> locationList = null;
			if (dtoLocation.getPageNumber() != null && dtoLocation.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoLocation.getPageNumber(), dtoLocation.getPageSize(), Direction.DESC, "createdDate");
				locationList = repositoryLocation.findByIsDeleted(false, pageable);
			} else {
				locationList = repositoryLocation.findByIsDeletedOrderByCreatedDateDesc(false);
			}
			
			List<DtoLocation> dtoLocationList=new ArrayList<>();
			if(locationList!=null && locationList.isEmpty())
			{
				for (Location location : locationList) 
				{
					dtoLocation=new DtoLocation(location);
					dtoLocation.setDescription(location.getDescription());
					dtoLocation.setArabicDescription(location.getArabicDescription());
					dtoLocation.setContactName(location.getContactName());
					dtoLocation.setCountry(location.getCountry());
					dtoLocation.setFax(location.getFax());
					dtoLocation.setLocationAddress(location.getLocationAddress());
					dtoLocation.setLocationId(location.getLocationId());
					if(location.getCity()!=null){
						dtoLocation.setCityId(location.getCity().getCityId());
						dtoLocation.setCityName(location.getCity().getCityName());
						dtoLocation.setCity(location.getCity());
					}
					
					if(location.getCountry()!=null){
						dtoLocation.setCountryId(location.getCountry().getCountryId());
						dtoLocation.setCountryName(location.getCountry().getCountryName());
						dtoLocation.setCountry(location.getCountry());
					}
					
					if(location.getState()!=null){
						dtoLocation.setStateName(location.getState().getStateName());
						dtoLocation.setStateId(location.getState().getStateId());
						dtoLocation.setState(location.getState());
					}
					
					dtoLocation.setPhone(location.getPhone());
					dtoLocation.setId(location.getId());
					dtoLocationList.add(dtoLocation);
				}
				dtoSearch.setRecords(dtoLocationList);
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchLocation(DtoSearch dtoSearch) {
		try {

			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition = "";
				if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
					if (dtoSearch.getSortOn().equals("locationId") || dtoSearch.getSortOn().equals("description")
							|| dtoSearch.getSortOn().equals("arabicDescription") || dtoSearch.getSortOn().equals("contactName")
							|| dtoSearch.getSortOn().equals("locationAddress") || dtoSearch.getSortOn().equals("phone")
							|| dtoSearch.getSortOn().equals("fax")|| dtoSearch.getSortOn().equals("city")) {
						
						if(dtoSearch.getSortOn().equals("cityName")) {
							dtoSearch.setSortOn("city");
						}
						condition = dtoSearch.getSortOn();
					} else {
						condition = "id";
					}

				} else {
					condition += "id";
					dtoSearch.setSortOn("");
					dtoSearch.setSortBy("");

				}
				dtoSearch.setTotalCount(this.repositoryLocation.predictiveLocationSearchTotalCount("%"+searchWord+"%"));
				List<Location> locationList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						locationList = this.repositoryLocation.predictiveLocationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						locationList =  this.repositoryLocation.predictiveLocationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						locationList =  this.repositoryLocation.predictiveLocationSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
					
				}
				
				if(locationList != null && !locationList.isEmpty()){
					List<DtoLocation> dtoLocationList = new ArrayList<>();
					for (Location location : locationList) {
						DtoLocation dtoLocation = new DtoLocation(location);
						dtoLocation.setDescription(location.getDescription());
						dtoLocation.setArabicDescription(location.getArabicDescription());
						dtoLocation.setContactName(location.getContactName());
						dtoLocation.setFax(location.getFax());
						dtoLocation.setId(location.getId());
						dtoLocation.setLocationAddress(location.getLocationAddress());
						dtoLocation.setLocationId(location.getLocationId());
						
						if(location.getCity()!=null){
							dtoLocation.setCityId(location.getCity().getCityId());
							dtoLocation.setCityName(location.getCity().getCityName());
						}
			
						if(location.getCountry()!=null){
							dtoLocation.setCountryId(location.getCountry().getCountryId());
							dtoLocation.setCountryName(location.getCountry().getCountryName());
						}
						
						if(location.getState()!=null){
							dtoLocation.setStateName(location.getState().getStateName());
							dtoLocation.setStateId(location.getState().getStateId());
						}
						dtoLocation.setPhone(location.getPhone());
						dtoLocation.setId(location.getId());
						dtoLocationList.add(dtoLocation);
					}
					dtoSearch.setRecords(dtoLocationList);
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	public DtoLocation getLocationById(int id) {
		DtoLocation dtoLocation  = new DtoLocation();
		try {
			if (id > 0) {
				Location location = repositoryLocation.findByIdAndIsDeleted(id, false);
				if (location != null) {
					dtoLocation = new DtoLocation(location);
					dtoLocation.setDescription(location.getDescription());
					dtoLocation.setArabicDescription(location.getArabicDescription());
					dtoLocation.setContactName(location.getContactName());
					dtoLocation.setFax(location.getFax());
					dtoLocation.setId(location.getId());
					dtoLocation.setLocationAddress(location.getLocationAddress());
					dtoLocation.setLocationId(location.getLocationId());
					
					if(location.getCity()!=null){
						dtoLocation.setCityId(location.getCity().getCityId());
						dtoLocation.setCityName(location.getCity().getCityName());
					}
		
					if(location.getCountry()!=null){
						dtoLocation.setCountryId(location.getCountry().getCountryId());
						dtoLocation.setCountryName(location.getCountry().getCountryName());
					}
					
					if(location.getState()!=null){
						dtoLocation.setStateName(location.getState().getStateName());
						dtoLocation.setStateId(location.getState().getStateId());
					}
					dtoLocation.setPhone(location.getPhone());
					dtoLocation.setId(location.getId());
					
				} else {
					dtoLocation.setMessageType("DIVISION_NOT_GETTING");

				}
			} else {
				dtoLocation.setMessageType("INVALID_DIVISION_ID");

			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoLocation;
	}
	public DtoLocation deleteLocation(List<Integer> ids) {
		log.info("deleteLocation Method");
		DtoLocation dtoLocation = new DtoLocation();
		dtoLocation.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DIVISION_DELETED", false));
		dtoLocation.setAssociateMessage(serviceResponse.getStringMessageByShortAndIsDeleted("DIVISION_ASSOCIATED", false));
		boolean inValidDelete = false;
		StringBuilder invlidDeleteMessage = new StringBuilder();
		invlidDeleteMessage.append(serviceResponse
				.getMessageByShortAndIsDeleted("LOCATION_NOT_DELETE_ID_MESSAGE", false).getMessage());
		List<DtoLocation> deleteLocation = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		try {
			for (Integer locationId : ids) {
				Location location = repositoryLocation.findOne(locationId);
				if (location.getEmployeeMaster().isEmpty() && location.getListEmployeePositionHistory().isEmpty() && location.getPayScheduleSetupLocation().isEmpty()) {
					DtoLocation dtoLocation2 = new DtoLocation();
					dtoLocation2.setId(locationId);
					dtoLocation2.setDescription(location.getDescription());
					dtoLocation2.setArabicDescription(location.getArabicDescription());
					dtoLocation2.setContactName(location.getContactName());
					dtoLocation2.setFax(location.getFax());
					dtoLocation2.setLocationAddress(location.getLocationAddress());
					dtoLocation2.setLocationId(location.getLocationId());
					dtoLocation2.setPhone(location.getPhone());
					repositoryLocation.deleteSingleLocation(true, loggedInUserId, locationId);
					deleteLocation.add(dtoLocation2);
				} else {
					inValidDelete = true;
				}
			}
			if (inValidDelete) {
				dtoLocation.setMessageType(invlidDeleteMessage.toString());
			}
			if (!inValidDelete) {
				dtoLocation.setMessageType("");
			}
			dtoLocation.setDeleteLocation(deleteLocation);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete Location :"+dtoLocation.getId());
		return dtoLocation;
	}
	
	
	public DtoLocation repeatByLocationId(String locationId) {
		log.info("repeatByLocationId Method");
		DtoLocation dtoLocation = new DtoLocation();
		try {
			List<Location> location=repositoryLocation.findByLocationId(locationId.trim());
			if(location!=null && !location.isEmpty()) {
				dtoLocation.setIsRepeat(true);
				dtoLocation.setLocationId(locationId);
			}else {
				dtoLocation.setIsRepeat(false);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dtoLocation;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchLocationId(DtoSearch dtoSearch) {
		try {
			log.info("searchDivisiontId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				List<String> locationIdList =new ArrayList<>();
				
					locationIdList = this.repositoryLocation.predictiveLocationIdSearchWithPagination("%"+searchWord+"%");
				if(!locationIdList.isEmpty()) {
					dtoSearch.setRecords(locationIdList.size());
				}
					
				dtoSearch.setIds(locationIdList);
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<DtoLocation> getAllLocationDropDownList() {
		log.info("getAllLocationDropDownList  Method");
		List<DtoLocation> dtoLocationList = new ArrayList<>();
		try {
			List<Location> list = repositoryLocation.findByIsDeleted(false);
			
			if (list != null && !list.isEmpty()) {
				for (Location location : list) {
					DtoLocation dtoLocation = new DtoLocation();
					dtoLocation.setId(location.getId());
					dtoLocation.setLocationId(location.getLocationId());
					dtoLocation.setDescription(location.getDescription());
					dtoLocation.setArabicDescription(location.getArabicDescription());
					dtoLocationList.add(dtoLocation);
				}
			}
			log.debug("Location is:"+dtoLocationList.size());
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoLocationList;
	}

	public DtoSearch searchAllLocationId(DtoSearch dtoSearch) {
		try {

			log.info("searchAllLocationId Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				
				List<Location> locationList =new ArrayList<>();
				
					locationList = this.repositoryLocation.predictiveSearchAllLocationIdWithPagination("%"+searchWord+"%");
					if(!locationList.isEmpty()) {

						if(locationList != null && !locationList.isEmpty()){
							List<DtoLocation> dtoLocation = new ArrayList<>();
							DtoLocation dtoLocation1=null;
							for (Location location : locationList) {
								
								dtoLocation1=new DtoLocation(location);
								dtoLocation1.setId(location.getId());
								dtoLocation1.setLocationId(location.getLocationId());
								dtoLocation1.setDescription(location.getLocationId()+" | "+location.getDescription());
								dtoLocation.add(dtoLocation1);
							}
							dtoSearch.setRecords(dtoLocation);
						}

						dtoSearch.setTotalCount(locationList.size());
					}
				
			}
			
			
		} catch (Exception e) {
			log.error(e);
		}
		return dtoSearch;
	}
}
