package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoBuildChecks;
import com.bti.hcm.model.dto.DtoDefaulCheck;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryDefault;
import com.bti.hcm.service.ServiceBuildChecks;
import com.bti.hcm.service.ServiceDefault;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;

@RestController
@RequestMapping("/buildChecks")
public class ControllerBuildChecks extends BaseController{
	

	private Logger log = Logger.getLogger(ControllerBuildChecks.class);
	@Autowired
	ServiceBuildChecks  serviceBuildChecks;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	@Autowired
	ServiceDefault  serviceDefault;
	
	@Autowired
	RepositoryDefault repositoryDefault;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoBuildChecks dtoBuildChecks) throws Exception {
		log.info("Create BuildCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag = serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBuildChecks  = serviceBuildChecks.saveOrUpdateFinalVersion1(dtoBuildChecks);
			responseMessage=displayMessage(dtoBuildChecks, "BUILD_CHECK_CREATED", "BUILD_CHECK_NOT_CREATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Create BuildCheck Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoBuildChecks dtoBuildChecks) throws Exception {
		log.info("Update BuildCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoBuildChecks  = serviceBuildChecks.saveOrUpdateFinalVersion1(dtoBuildChecks);
			responseMessage=displayMessage(dtoBuildChecks, "BUILD_CHECK_UPDATED", "BUILD_CHECK_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Update BuildCheck Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoBuildChecks dtoBuildChecks) throws Exception {
		log.info("Delete BuildCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoBuildChecks.getIds() != null && !dtoBuildChecks.getIds().isEmpty()) {
				DtoBuildChecks dtoBuildChecks2 = serviceBuildChecks.delete(dtoBuildChecks.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("BUILD_CHECK_DELETED", false), dtoBuildChecks2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("BUILD_CHECK_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		log.debug("Delete BuildCheck Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	

	
	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public ResponseMessage getById(HttpServletRequest request, @RequestBody DtoBuildChecks dtoBuildChecks) throws Exception {
		log.info("Get ByIds Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoBuildChecks dtoBuildChecksObj = serviceBuildChecks.getById(dtoBuildChecks.getId());
			responseMessage=displayMessage(dtoBuildChecksObj, "BUILD_CHECK_LIST_GET_DETAIL", "BUILD_CHECK_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		log.debug("Get ById Method:"+dtoBuildChecks.getId());
		return responseMessage;
	}

	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchEmployeeDeductionMaintenance(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		log.info("Search BuildCheck Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceBuildChecks.search(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "BUILD_CHECK_GET_ALL", "BUILD_CHECK_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =  unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	
	@RequestMapping(value = "/getByDefaultId", method = RequestMethod.POST)
	public ResponseMessage getByDefaultId(HttpServletRequest request, @RequestBody DtoBuildChecks dtoBuildChecks) throws Exception {
		log.info("Get ByIdes Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		DtoSearch dtoSearch = null;
		if (flag) {
			dtoSearch = serviceBuildChecks.getByDefaultId(dtoBuildChecks.getId());
			responseMessage=displayMessage(dtoSearch, "BUILD_CHECK__LIST_GET_DETAIL", "BUILD_CHECK__NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/checkDuplicate", method = RequestMethod.POST)
	public ResponseMessage defaulIdCheck(HttpServletRequest request, @RequestBody DtoBuildChecks dtoBuildChecks) throws Exception {
		log.info("DefaultId checks Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag ) {
			DtoBuildChecks dtoBuildCheckss = serviceBuildChecks.repeatByDefaultId(dtoBuildChecks.getDefault1().getId());
			responseMessage=displayMessage(dtoBuildCheckss, "DEFAULT_SETUP_EXIST", "BUILD_CHECKS_NOT_EXITS", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	

	@RequestMapping(value = "/deleteBuild", method = RequestMethod.PUT)
	public ResponseMessage deleteBuild(HttpServletRequest request, @RequestBody DtoBuildChecks dtoBuildChecks) throws Exception {
		log.info("Delete Default Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoBuildChecks.getIds() != null && !dtoBuildChecks.getIds().isEmpty()) {
				DtoBuildChecks  dtoBuildChecks2 = serviceBuildChecks.deleteBuild(dtoBuildChecks.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_DELETED", false), dtoBuildChecks2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("DEFAULT_LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		return responseMessage;
	}
	

	@RequestMapping(value = "/getByDefaultIdWithAllData", method = RequestMethod.POST)
	public ResponseMessage getByDefaultIdWithAllData(HttpServletRequest request, @RequestBody DtoBuildChecks dtoBuildChecks) throws Exception {
		log.info("Get ByIdes Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		DtoSearch dtoSearch = null;
		if (flag) {
			dtoSearch = serviceBuildChecks.getByDefaultIdWithAllData(dtoBuildChecks.getId());
			responseMessage=displayMessage(dtoSearch, "BUILD_CHECK__LIST_GET_DETAIL", "BUILD_CHECK__NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/changeFlagBenefit", method = RequestMethod.POST)
	public ResponseMessage changeFlagBenefit(HttpServletRequest request, @RequestBody DtoDefaulCheck dtoDefaulCheck) throws Exception {
		log.info("Get ByIds Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		DtoDefaulCheck dtoDefaulCheck1 = null;
		if (flag) {
			dtoDefaulCheck1 = serviceBuildChecks.deleteFromTranscationByBenefit(dtoDefaulCheck);
			responseMessage=displayMessage(dtoDefaulCheck1, "BUILD_CHECK__LIST_GET_DETAIL", "BUILD_CHECK__NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/changeFlagDeduction", method = RequestMethod.POST)
	public ResponseMessage changeFlagDeduction(HttpServletRequest request, @RequestBody DtoDefaulCheck dtoDefaulCheck) throws Exception {
		log.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		DtoDefaulCheck dtoDefaulCheck1 = null;
		if (flag) {
			dtoDefaulCheck1 = serviceBuildChecks.deleteFromTranscationByDeduction(dtoDefaulCheck);
			responseMessage=displayMessage(dtoDefaulCheck1, "BUILD_CHECK__LIST_GET_DETAIL", "BUILD_CHECK__NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/changeFlagPaycode", method = RequestMethod.POST)
	public ResponseMessage changeFlagPaycode(HttpServletRequest request, @RequestBody DtoDefaulCheck dtoDefaulCheck) throws Exception {
		log.info("Get ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		DtoDefaulCheck dtoDefaulCheck1 = null;
		if (flag) {
			dtoDefaulCheck1 = serviceBuildChecks.deleteFromTranscationByPayCode(dtoDefaulCheck);
			responseMessage=displayMessage(dtoDefaulCheck1, "BUILD_CHECK__LIST_GET_DETAIL", "BUILD_CHECK__NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}

}
