/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.hcm.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.hcm.model.Department;
import com.bti.hcm.model.PayScheduleSetup;
import com.bti.hcm.model.PayScheduleSetupDepartment;
import com.bti.hcm.model.dto.DtoPayScheduleSetupDepartment;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.repository.RepositoryDepartment;
import com.bti.hcm.repository.RepositoryPayScheduleSetup;
import com.bti.hcm.repository.RepositoryPayScheduleSetupDepartment;

/**
 * Description: Service PayScheduleSetupDepartment
 * Project: Hcm 
 * Version: 0.0.1
 */
@Service("servicePayScheduleSetupDepartment")
public class ServicePayScheduleSetupDepartment {
	
	
	
	/**
	 * @Description LOGGER use for put a logger in PayScheduleSetupDepartment Service
	 */
	static Logger log = Logger.getLogger(ServicePayScheduleSetupDepartment.class.getName());


	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in PayScheduleSetupDepartment service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in PayScheduleSetupDepartment service
	 */
	@Autowired(required=false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryPayScheduleSetupDepartment Autowired here using annotation of spring for access of repositoryPayScheduleSetupDepartment method in PayScheduleSetupDepartment service
	 * 				In short Access PayScheduleSetupDepartment Query from Database using repositoryPayScheduleSetupDepartment.
	 */
	@Autowired
	RepositoryPayScheduleSetupDepartment repositoryPayScheduleSetupDepartment;
	
	@Autowired
	RepositoryPayScheduleSetup repositoryPayScheduleSetup;
	
	@Autowired
	RepositoryDepartment repositoryDepartment;
	
	
	/**
	 * 
	 * @param dtoPayScheduleSetupDepartment
	 * @return
	 * @throws ParseException
	 */
	public DtoPayScheduleSetupDepartment saveOrUpdatePayScheduleSetupDepartment(DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment) throws ParseException {
		log.info("saveOrUpdatePayScheduleSetupDepartment Method");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		PayScheduleSetupDepartment payScheduleSetupDepartment = null;
		try {
			Department department = repositoryDepartment.findOne(dtoPayScheduleSetupDepartment.getDepartmentId());
			PayScheduleSetup payScheduleSetup = repositoryPayScheduleSetup.findOne(dtoPayScheduleSetupDepartment.getPayScheduleSetupId());
			if (dtoPayScheduleSetupDepartment.getPayScheduleDepartmentIndexId() > 0) {
				payScheduleSetupDepartment = repositoryPayScheduleSetupDepartment.findByPayScheduleDepartmentIndexIdAndIsDeleted(dtoPayScheduleSetupDepartment.getPayScheduleDepartmentIndexId(), false);
			} else {
				payScheduleSetupDepartment = new PayScheduleSetupDepartment();
				payScheduleSetupDepartment.setCreatedDate(new Date());
				
				Integer rowId = repositoryPayScheduleSetupDepartment.getCountOfTotaPayScheduleSetupDepartment();
				Integer increment=0;
				if(rowId!=0) {
					increment= rowId+1;
				}else {
					increment=1;
				}
				payScheduleSetupDepartment.setRowId(increment);
				
			}
			payScheduleSetupDepartment.setPayScheduleDepartmentIndexId(dtoPayScheduleSetupDepartment.getPayScheduleDepartmentIndexId());
			payScheduleSetupDepartment.setDepartmentDescription(dtoPayScheduleSetupDepartment.getDepartmentDescription());
			payScheduleSetupDepartment.setArabicDepartmentDescription(dtoPayScheduleSetupDepartment.getArabicDepartmentDescription());
			payScheduleSetupDepartment.setPayScheduleStatus(dtoPayScheduleSetupDepartment.getPayScheduleStatus());
			payScheduleSetupDepartment.setDepartment(department);
			payScheduleSetupDepartment.setPayScheduleSetup(payScheduleSetup);
			log.debug("PayScheduleSetupDepartment is:"+dtoPayScheduleSetupDepartment.getPayScheduleDepartmentIndexId());
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
		}
		return dtoPayScheduleSetupDepartment;

	}
	
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchPayScheduleSetupDepartment(DtoSearch dtoSearch) {
		try {
			log.info("searchPayScheduleSetupDepartment Method");
			if(dtoSearch != null){
				String searchWord=dtoSearch.getSearchKeyword();
				String condition="";
			if(dtoSearch.getSortOn()!=null || dtoSearch.getSortBy()!=null){
				switch(dtoSearch.getSortOn()){
				case "payScheduleSetup" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "departmentDescription" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "department" : 
					condition+=dtoSearch.getSortOn();
					break;
				case "arabicDepartmentDescription" : 
					condition+=dtoSearch.getSortOn();
					break;	
				case "payScheduleStatus" : 
					condition+=dtoSearch.getSortOn();
					break;	
				default:
					condition+="payScheduleDepartmentIndexId";
					
				}
			}
				dtoSearch.setTotalCount(this.repositoryPayScheduleSetupDepartment.predictivePayScheduleSetupDepartmentSearchTotalCount("%"+searchWord+"%"));
				List<PayScheduleSetupDepartment> payScheduleSetupDepartmentList =null;
				if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
					
					if(dtoSearch.getSortBy().equals("") && dtoSearch.getSortOn().equals("")){
						payScheduleSetupDepartmentList = this.repositoryPayScheduleSetupDepartment.predictivePayScheduleSetupDepartmentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, "id"));
					}
					if(dtoSearch.getSortBy().equals("ASC")){
						payScheduleSetupDepartmentList = this.repositoryPayScheduleSetupDepartment.predictivePayScheduleSetupDepartmentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.ASC, condition));
					}else if(dtoSearch.getSortBy().equals("DESC")){
						payScheduleSetupDepartmentList = this.repositoryPayScheduleSetupDepartment.predictivePayScheduleSetupDepartmentSearchWithPagination("%"+searchWord+"%",new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Sort.Direction.DESC, condition));
					}
				}
				if(payScheduleSetupDepartmentList != null && !payScheduleSetupDepartmentList.isEmpty()){
					List<DtoPayScheduleSetupDepartment> dtoPayScheduleSetupDepartmentList = new ArrayList<DtoPayScheduleSetupDepartment>();
					DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment=null;
					for (PayScheduleSetupDepartment payScheduleSetupDepartment : payScheduleSetupDepartmentList) {
						dtoPayScheduleSetupDepartment = new DtoPayScheduleSetupDepartment(payScheduleSetupDepartment);
						dtoPayScheduleSetupDepartment.setArabicDepartmentDescription(payScheduleSetupDepartment.getArabicDepartmentDescription());
						dtoPayScheduleSetupDepartment.setDepartmentDescription(payScheduleSetupDepartment.getArabicDepartmentDescription());
						dtoPayScheduleSetupDepartment.setPayScheduleStatus(payScheduleSetupDepartment.getPayScheduleStatus());
						dtoPayScheduleSetupDepartment.setDepartmentId(payScheduleSetupDepartment.getDepartment().getId());
						dtoPayScheduleSetupDepartment.setPayScheduleSetupId(payScheduleSetupDepartment.getPayScheduleSetup().getPayScheduleIndexId());
						dtoPayScheduleSetupDepartmentList.add(dtoPayScheduleSetupDepartment);
					}
					dtoSearch.setRecords(dtoPayScheduleSetupDepartmentList);
				}
			}
			log.debug("Search PayScheduleSetupDepartment Size is:"+dtoSearch.getTotalCount());
			
		} catch (Exception e) {
			log.error(e);
		}
		
		return dtoSearch;
	}
	
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public DtoPayScheduleSetupDepartment getPayScheduleDepartmentIndexId(int id) {
		log.info("getPayScheduleDepartmentIndexId Method");
		DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment = new DtoPayScheduleSetupDepartment();
		try {
			if (id > 0) {
				PayScheduleSetupDepartment payScheduleSetupDepartment = repositoryPayScheduleSetupDepartment.findByPayScheduleDepartmentIndexIdAndIsDeleted(id, false);
				if (payScheduleSetupDepartment != null) {
					dtoPayScheduleSetupDepartment = new DtoPayScheduleSetupDepartment(payScheduleSetupDepartment);
					
					dtoPayScheduleSetupDepartment.setPayScheduleDepartmentIndexId(payScheduleSetupDepartment.getPayScheduleDepartmentIndexId());
					dtoPayScheduleSetupDepartment.setArabicDepartmentDescription(payScheduleSetupDepartment.getArabicDepartmentDescription());
					dtoPayScheduleSetupDepartment.setDepartmentDescription(payScheduleSetupDepartment.getDepartmentDescription());
					dtoPayScheduleSetupDepartment.setPayScheduleStatus(payScheduleSetupDepartment.getPayScheduleStatus());
					dtoPayScheduleSetupDepartment.setPayScheduleSetupId(payScheduleSetupDepartment.getPayScheduleSetup().getPayScheduleIndexId());
					dtoPayScheduleSetupDepartment.setDepartmentId(payScheduleSetupDepartment.getDepartment().getId());
					
				} else {
					dtoPayScheduleSetupDepartment.setMessageType("PAYSCHEDULE_SETUP_DEPARTMENT_NOT_GETTING");

				}
			} else {
				dtoPayScheduleSetupDepartment.setMessageType("INVALID_PAYSCHEDULE_SETUP_DEPARTMENT_INDEX_ID");

			}
			log.debug("GetPayScheduleDepartmentIndexId By Id is:"+dtoPayScheduleSetupDepartment.getPayScheduleDepartmentIndexId());
		} catch (Exception e) {
			log.error(e);
		}
		return dtoPayScheduleSetupDepartment;
	}
	
	
	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoPayScheduleSetupDepartment deletePayScheduleSetupDepartment(List<Integer> ids) {
		log.info("deletePayScheduleSetupDepartment Method");
		DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment = new DtoPayScheduleSetupDepartment();
		dtoPayScheduleSetupDepartment.setDeleteMessage(serviceResponse.getStringMessageByShortAndIsDeleted("PAYSCHEDULE_SETUP_DEPARTMENT_DELETED", false));
		dtoPayScheduleSetupDepartment.setAssociateMessage(
				serviceResponse.getStringMessageByShortAndIsDeleted("PAYSCHEDULE_SETUP_DEPARTMENT_ASSOCIATED", false));
		List<DtoPayScheduleSetupDepartment> deletePayScheduleSetupDepartment = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		try {
			for (Integer payScheduleDepartmentIndexId : ids) {
				PayScheduleSetupDepartment payScheduleSetupDepartment = repositoryPayScheduleSetupDepartment.findOne(payScheduleDepartmentIndexId);
				DtoPayScheduleSetupDepartment dtoPayScheduleSetupDepartment2 = new DtoPayScheduleSetupDepartment();
				dtoPayScheduleSetupDepartment2.setPayScheduleDepartmentIndexId(payScheduleDepartmentIndexId);
				repositoryPayScheduleSetupDepartment.deleteSinglPayScheduleSetupDepartment(true, loggedInUserId, payScheduleDepartmentIndexId);
				deletePayScheduleSetupDepartment.add(dtoPayScheduleSetupDepartment2);

			}
			dtoPayScheduleSetupDepartment.setDeletePayScheduleSetupDepartment(deletePayScheduleSetupDepartment);
		} catch (NumberFormatException e) {
			log.error(e);
		}
		log.debug("Delete PayScheduleSetupDepartment :"+dtoPayScheduleSetupDepartment.getPayScheduleDepartmentIndexId());
		return dtoPayScheduleSetupDepartment;
	}

}
