package com.bti.hcm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.hcm.config.ResponseMessage;
import com.bti.hcm.constant.MessageConstant;
import com.bti.hcm.model.dto.DtoRetirementPlanSetup;
import com.bti.hcm.model.dto.DtoSearch;
import com.bti.hcm.service.ServiceHcmHome;
import com.bti.hcm.service.ServiceResponse;
import com.bti.hcm.service.ServiceRetirementPlanSetup;

@RestController
@RequestMapping("/retirementPlanSetup")
public class ControllerRetirementPlanSetup extends BaseController{

	private static final Logger LOGGER = Logger.getLogger(ControllerRetirementPlanSetup.class);
	
	
	@Autowired(required=true)
	ServiceRetirementPlanSetup serviceRetirementPlanSetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHcmHome serviceHcmHome;
	
	/**
	 * @description : Create RetirementEntranceDate
	 * @param request
	 * @param dtoRetirementEntranceDate
	 * @return
	 * @throws Exception
	 * @request
	*

	/*	"retirementPlanId":1233232,
		"retirementPlanDescription": "welcome23232323hello",
		"retirementPlanArbicDescription": "hello121213",
		"retirementPlanAccountNumber": "1212345",
		"loans":"12345667657",
		"pageNumber": "0",
		"pageSize": "10"
	*/
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage create(HttpServletRequest request, @RequestBody DtoRetirementPlanSetup dtoRetirementPlanSetup) throws Exception {
		LOGGER.info("Create RetirementPlanSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoRetirementPlanSetup = serviceRetirementPlanSetup.saveOrUpdateRetirementPlanSetup(dtoRetirementPlanSetup);
			responseMessage=displayMessage(dtoRetirementPlanSetup, "RETIREMENT_PLAN_SETUP_CREATED", "RETIREMENT_PLAN_SETUP_NOT_CREATED", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Create RetirementPlanSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	/**
	 * @description : Create RetirementEntranceDate
	 * @param request
	 * @param dtoRetirementEntranceDate
	 * @return
	 * @throws Exception
	 * @request
	*		"retirementPlanId":1233232,
		"retirementPlanDescription": "welcome23232323hello",
		"retirementPlanArbicDescription": "hello121213",
		"retirementPlanAccountNumber": "1212345",
		"loans":"12345667657",
		"pageNumber": "0",
		"pageSize": "10"
	*/
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request, @RequestBody DtoRetirementPlanSetup dtoRetirementPlanSetup) throws Exception {
		LOGGER.info("Update RetirementPlanSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoRetirementPlanSetup = serviceRetirementPlanSetup.saveOrUpdateRetirementPlanSetup(dtoRetirementPlanSetup);
			responseMessage=displayMessage(dtoRetirementPlanSetup, "RETIREMENT_PLAN_SETUP_UPDATED_SUCCESS", "RETIREMENT_PLAN_SETUP_NOT_UPDATED", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Update RetirementPlanSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage delete(HttpServletRequest request, @RequestBody DtoRetirementPlanSetup dtoRetirementPlanSetup) throws Exception {
		LOGGER.info("Delete RetirementPlanSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			if (dtoRetirementPlanSetup.getIds() != null && !dtoRetirementPlanSetup.getIds().isEmpty()) {
				DtoRetirementPlanSetup dtoRetirementPlanSetup2 = serviceRetirementPlanSetup.deleteRetirementPlanSetup(dtoRetirementPlanSetup.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("RETIREMENT_PLAN_SETUP_DELETED", false), dtoRetirementPlanSetup2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageConstant.SESSION_EXPIRED, false));
		}
		LOGGER.debug("Delete RetirementPlanSetup Method:"+responseMessage.getMessage());
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAll(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) throws Exception {
		LOGGER.info("Search RetirementPlanSetup Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			dtoSearch = this.serviceRetirementPlanSetup.searchRetirementPlanSetup(dtoSearch);
			responseMessage=displayMessage(dtoSearch, "RETIREMENT_PLAN_SETUP_GET_DETAIL", "RETIREMENT_PLAN_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage =unauthorizedMsg(serviceResponse);
		}
		if(dtoSearch!=null) {
			LOGGER.debug("Search RetirementPlanSetup Method:"+dtoSearch.getTotalCount());
		}
		return responseMessage;
	}
	
	/**
	 * @param request{
	  "id" : 1
	}
	 * @param dtoSkillSteup
	 * @return
	 * @throws Exception
	 */
	
	
	
	@RequestMapping(value = "/getretirementPlanId", method = RequestMethod.POST)
	public ResponseMessage getretirementPlanId(HttpServletRequest request, @RequestBody DtoRetirementPlanSetup dtoRetirementPlanSetup) throws Exception {
		LOGGER.info("Get getRetirementPlanSetup ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoRetirementPlanSetup dtoRetirementPlanSetupObj = serviceRetirementPlanSetup.getRetirementPlanSetupById(dtoRetirementPlanSetup.getId());
			responseMessage=displayMessage(dtoRetirementPlanSetupObj, "RETIREMENT_PLAN_SETUP_GET_DETAIL", "RETIREMENT_PLAN_SETUP_LIST_NOT_GETTING", serviceResponse);
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		LOGGER.debug("Get RetirementPlanSetup by Id Method:"+dtoRetirementPlanSetup.getId());
		return responseMessage;
	}
	
	@RequestMapping(value = "/retirementPlanIdcheck", method = RequestMethod.POST)
	public ResponseMessage retirementPlanIdcheck(HttpServletRequest request, @RequestBody DtoRetirementPlanSetup dtoRetirementPlanSetups) throws Exception {
		LOGGER.info("timeCodeIdcheck ById Method");
		ResponseMessage responseMessage = null;
		boolean flag =serviceHcmHome.checkValidCompanyAccess();
		if (flag) {
			DtoRetirementPlanSetup dtoRetirementPlanSetup = serviceRetirementPlanSetup.repeatByRetirementPlanId(dtoRetirementPlanSetups.getRetirementPlanId());
			if (dtoRetirementPlanSetup != null) {
				if (dtoRetirementPlanSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("RETIREMENT_PLAN_SETUP_REPEAT_FOUND", false), dtoRetirementPlanSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoRetirementPlanSetup.getMessageType(), false),
							dtoRetirementPlanSetup);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("RETIREMENT_PLAN_SETUP_NOT_FOUND", false), dtoRetirementPlanSetup);
			}
		} else {
			responseMessage = unauthorizedMsg(serviceResponse);
		}
		return responseMessage;
	}
	
	
}
