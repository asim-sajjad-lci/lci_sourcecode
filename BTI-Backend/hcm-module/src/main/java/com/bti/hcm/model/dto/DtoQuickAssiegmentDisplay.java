	package com.bti.hcm.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class DtoQuickAssiegmentDisplay {

	private Integer masterId;
	private Integer employeeId;
	private short method;
	private BigDecimal percent;
	private String code;
	private Integer codeId;
	private String desc;
	private String arabicDesc;
	private BigDecimal amount;
	private BigDecimal payRate;
	private Date startDate;
	private Date endDate;
	private boolean isActive;
	private Integer empolyeeMaintananceId;
	private BigDecimal payFactor;
	private List<DtoPayCode> dtoPayCode;
	private String baseOnPayCode;

	private Integer MasterPayCodeId; // ME
	private BigDecimal parentPayRate; // ME
	private BigDecimal parentPayFactor; // ME

	public Integer getMasterId() {
		return masterId;
	}

	public void setMasterId(Integer masterId) {
		this.masterId = masterId;
	}
	
	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public short getMethod() {
		return method;
	}

	public void setMethod(short method) {
		this.method = method;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getArabicDesc() {
		return arabicDesc;
	}

	public void setArabicDesc(String arabicDesc) {
		this.arabicDesc = arabicDesc;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPayRate() {
		return payRate;
	}

	public void setPayRate(BigDecimal payRate) {
		this.payRate = payRate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getEmpolyeeMaintananceId() {
		return empolyeeMaintananceId;
	}

	public void setEmpolyeeMaintananceId(Integer empolyeeMaintananceId) {
		this.empolyeeMaintananceId = empolyeeMaintananceId;
	}

	public BigDecimal getPayFactor() {
		return payFactor;
	}

	public void setPayFactor(BigDecimal payFactor) {
		this.payFactor = payFactor;
	}

	public List<DtoPayCode> getDtoPayCode() {
		return dtoPayCode;
	}

	public void setDtoPayCode(List<DtoPayCode> dtoPayCode) {
		this.dtoPayCode = dtoPayCode;
	}

	public String getBaseOnPayCode() {
		return baseOnPayCode;
	}

	public void setBaseOnPayCode(String baseOnPayCode) {
		this.baseOnPayCode = baseOnPayCode;
	}

	public Integer getMasterPayCodeId() {
		return MasterPayCodeId;
	}

	public void setMasterPayCodeId(Integer masterPayCodeId) {
		MasterPayCodeId = masterPayCodeId;
	}

	public BigDecimal getParentPayRate() {
		return parentPayRate;
	}

	public void setParentPayRate(BigDecimal parentPayRate) {
		this.parentPayRate = parentPayRate;
	}

	public BigDecimal getParentPayFactor() {
		return parentPayFactor;
	}

	public void setParentPayFactor(BigDecimal parentPayFactor) {
		this.parentPayFactor = parentPayFactor;
	}
	
}
