package com.bti.hcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "HR90301")
public class PayrollTransactionHistoryYearDistribution  extends HcmBaseEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RWPRLINDX")
	private Integer id;
	
	@Column(name = "YEAR1")
	private Integer year;
	
	@Column(name = "HCMTRXPYRL")
	private String auditTransactionNumber;

	@Column(name = "EMPLOYINDX")
	private Integer employeeId;
	
	@Column(name = "ACTROWID")
	private Long accountTableRowIndex;
	
	@Column(name = "DITPOTTYP")
	private Short postingTypes;
	
	@Column(name = "HCMCODINX")
	private Integer codeIndexId;
	
	@Column(name = "DEBTAMT", precision = 10, scale = 3)
	private BigDecimal debitAmount;

	@Column(name = "CRDTAMT", precision = 10, scale = 3)
	private BigDecimal creditAmount;
	
	@Column(name = "ACCTSEQN")
	private Integer accountSequence;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getAuditTransactionNumber() {
		return auditTransactionNumber;
	}

	public void setAuditTransactionNumber(String auditTransactionNumber) {
		this.auditTransactionNumber = auditTransactionNumber;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Long getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(Long accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Short getPostingTypes() {
		return postingTypes;
	}

	public void setPostingTypes(Short postingTypes) {
		this.postingTypes = postingTypes;
	}

	public Integer getCodeIndexId() {
		return codeIndexId;
	}

	public void setCodeIndexId(Integer codeIndexId) {
		this.codeIndexId = codeIndexId;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Integer getAccountSequence() {
		return accountSequence;
	}

	public void setAccountSequence(Integer accountSequence) {
		this.accountSequence = accountSequence;
	}
	
}
