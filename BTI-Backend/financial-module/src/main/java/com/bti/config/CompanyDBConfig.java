package com.bti.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.MultiTenancyStrategy;
import org.hibernate.cfg.Environment;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "companyEntityManagerFactory", 
        transactionManagerRef = "companyTransactionManager",
        basePackages = { "com.bti.repository" })
public class CompanyDBConfig {
	
	 @Autowired
	  private JpaProperties jpaProperties;

	  @Bean
	  public JpaVendorAdapter jpaVendorAdapter() {
	    return new HibernateJpaVendorAdapter();
	  }
	
	
	@Bean(name = "companyDataSource")
	@Primary
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource companyDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "companyEntityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
        MultiTenantConnectionProvider multiTenantConnectionProviderImpl,
        CurrentTenantIdentifierResolver currentTenantIdentifierResolverImpl) {
      Map<String, Object> properties = new HashMap<>();
      properties.putAll(jpaProperties.getHibernateProperties(dataSource));
      properties.put(Environment.MULTI_TENANT, MultiTenancyStrategy.DATABASE);
      properties.put(Environment.MULTI_TENANT_CONNECTION_PROVIDER, multiTenantConnectionProviderImpl);
      properties.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, currentTenantIdentifierResolverImpl);

      //Property for hibernate pool
      //properties.put(Environment.AUTO_CLOSE_SESSION, true);
      //properties.put(Environment.C3P0_ACQUIRE_INCREMENT,1);
      properties.put(Environment.C3P0_MAX_SIZE,300);
      properties.put(Environment.C3P0_MIN_SIZE,100);
      properties.put(Environment.C3P0_MAX_STATEMENTS,500);
      properties.put(Environment.C3P0_TIMEOUT,500000);
    //  properties.put(Environment.C3P0_IDLE_TEST_PERIOD,30000);
   // properties.put(Environment.RELEASE_CONNECTIONS,"after_transaction");
      
      
       
      LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
      em.setDataSource(dataSource);
      em.setPackagesToScan("com.bti.model");
      em.setJpaVendorAdapter(jpaVendorAdapter());
      em.setJpaPropertyMap(properties);
      return em;
    }

    @Bean(name = "companyTransactionManager")
    @Primary
    public PlatformTransactionManager companyTransactionManager(
            @Qualifier("companyEntityManagerFactory") EntityManagerFactory companyEntityManagerFactory) {
        return new JpaTransactionManager(companyEntityManagerFactory);
    }
}
