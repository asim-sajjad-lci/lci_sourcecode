/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>BTIRoles</code> Contains the type of Application User Role.
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum ARDistributionType {

	CASH(1),DISCT(2),RECV(3),WHMT(4),GSTS(5),OTHR(6),ROUN(7),
	SALS(8),TRAD(9),FRGH(10),MISC(11),TAXS(12),COGS(13),FNCG(14),
	RETN(15),DMEM(16),CMEM(17),SRVC(18),WARE(19),WARS(20),INVT(21),FRGH_TAX(22),MISC_TAX(23);
	
	private int index;
	/**
	 * @param index
	 */
	private ARDistributionType(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static ARDistributionType getById(int id) {
	    for(ARDistributionType e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}