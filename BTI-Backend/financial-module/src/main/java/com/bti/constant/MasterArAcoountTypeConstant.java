/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>ARTransactionTypeConstant</code> 
 * Name of Project: BTI
 * Created on: Dec 29, 2017
 * Modified on:  Dec 29, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum MasterArAcoountTypeConstant {

	CASH(1),ACCOUNT_RECV(2),SALES(3),
	COST_OF_SALES(4),INVENTORY(5),FINANCE_CHARGE(6),
	SALES_ORDER_RETURNS(7),TRADE_DISCOUNT(8),DEBIT_MEMO(9),
	SERVICE_REPAIR(10),WARRANTY(11),CREDIT_MEMO(12),FREIGHT(13),MISCELLANEOUS(14),WARRANTY_EXPENSE(15);
	
	private int index;
	/**
	 * @param index
	 */
	private MasterArAcoountTypeConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static MasterArAcoountTypeConstant getById(int id) {
	    for(MasterArAcoountTypeConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}