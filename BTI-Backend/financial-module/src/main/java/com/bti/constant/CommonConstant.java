package com.bti.constant;

public class CommonConstant {
	private CommonConstant() {
	}
	public static final String SAVE = "SAVE";
	public static final String UPDATE = "UPDATE";
	public static final String USER_ID = "userid";
	public static final String ACCOUNT_NUMBER = "accountNumber";
	public static final String ACCOUNT_DESCRIPTION = "accountDescription";
	public static final String DEBIT_AMOUNT = "debitAmount";
	public static final String DEBIT_ACCOUNT_TABLE_ROW_INDEX = "debitAccountTableRowIndex";
	public static final String VAT_AMOUNT = "vatAmount";
	public static final String VAT_ACCOUNT_TABLE_ROW_INDEX = "vatAccountTableRowIndex";
	public static final String CREDIT_AMOUNT = "creditAmount";
	public static final String CREDIT_ACCOUNT_TABLE_ROW_INDEX = "creditAccountTableRowIndex";
	public static final String LANG_ID = "langId";
	public static final String SESSION = "session";
	public static final String TENANT_ID = "tenantid";
	public static final int FROM = 1;
	public static final int TO = 2;
	public static final String CONTENT_TYPE = "Content-Type";
	public static final Object APPLICATION_JSON = "application/json";
	public static final int STANDARD = 1;
	public static final int REVERSING = 2;
	
	
}
