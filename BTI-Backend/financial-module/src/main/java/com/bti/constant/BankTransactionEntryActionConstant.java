/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>BTIRoles</code> Contains the type of Application User Role.
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum BankTransactionEntryActionConstant {

	CHECK(1),WITHDRAWAL(2),INCREASE_BALANCE(3),DECREASE_BALANCE(4);
	
	private int index;
	/**
	 * @param index
	 */
	private BankTransactionEntryActionConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static BankTransactionEntryActionConstant getById(int id) {
	    for(BankTransactionEntryActionConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	}
	
}