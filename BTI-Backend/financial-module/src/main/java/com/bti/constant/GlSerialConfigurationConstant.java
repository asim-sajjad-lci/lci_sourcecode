/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>APDistributionTypeConstant</code> Contains the Distribution Types
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum GlSerialConfigurationConstant {
	CASH_RECEIPT("REPT");
	
	private String value;
	/**
	 * @param index
	 */
	private GlSerialConfigurationConstant(String value) {
		this.value = value;
	}

	public String getVaue() {
		return value;
	}
}