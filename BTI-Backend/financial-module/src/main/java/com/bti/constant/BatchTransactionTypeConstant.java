/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>BTIRoles</code> Contains the type of Application User Role.
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum BatchTransactionTypeConstant {

	JOURNAL_ENTRY(1),CLEARING_ENTRY(2),QUICK_JOURNAL_ENTRY(3),BUDGET_ENTRY(4),
	CASH_RECEIPT_ENTRY(5),BANK_TRANSFER(6),BANK_TRANSACTION(7),AR_TRANSACTION_ENTRY(8),
	AR_CASH_RECEIPT(9),AP_TRANSACTION_ENTRY(10),AP_CASH_RECEIPT(11),PAYMENT_VOUCHER(12);
	
	private int index;
	/**
	 * @param index
	 */
	private BatchTransactionTypeConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static BatchTransactionTypeConstant getById(int id) {
	    for(BatchTransactionTypeConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	}
}