/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>APDistributionTypeConstant</code> Contains the Distribution Types
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum ARMasterDocumentTypeConstant {

	SALES(1),DEBIT_MEMO(2),FINANCE_CHARGE(3),SERVICE(4),PURCHASE(5);
	
	
	private int index;
	/**
	 * @param index
	 */
	private ARMasterDocumentTypeConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static ARMasterDocumentTypeConstant getById(int id) {
	    for(ARMasterDocumentTypeConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}