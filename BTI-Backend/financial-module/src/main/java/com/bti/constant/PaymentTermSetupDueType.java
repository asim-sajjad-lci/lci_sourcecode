/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>BTIRoles</code> Contains the type of Application User Role.
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum PaymentTermSetupDueType {

	TRANSACTION_DATE(1),DISCOUNT_DATE(2);
	
	private int index;

	/**
	 * @param index
	 */
	private PaymentTermSetupDueType(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static PaymentTermSetupDueType getById(int id) {
	    for(PaymentTermSetupDueType e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}