/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>Week Day Constant</code> Contains the days name.
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum RateCalculationMethod {

	MULTIPLY("*"), DIVIDE("/");
	
	private String method;

	/**
	 * @param index
	 */
	private RateCalculationMethod(String method) {

		this.method = method;
	}
	
	public String getMethod() {
		return method;
	}
	
	
	public static RateCalculationMethod getByName(String name) {
	    for(RateCalculationMethod e : values()) {
	        if(e.name().equalsIgnoreCase(name)) return e;
	    }
	    return null;
	}
}