/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>ARTransactionTypeConstant</code> 
 * Name of Project: BTI
 * Created on: Dec 29, 2017
 * Modified on:  Dec 29, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum APTransactionTypeConstant {

	PAYMENTS(0),PURCHASE(1),DEBIT_MEMO(2),FINANCE_CHARGE(3),
	SERVICE_REPAIR(4),WARRANTY(5),CREDIT_MEMO(6),
	RETURNS(7),PAYABLE(8);
	
	private int index;
	/**
	 * @param index
	 */
	private APTransactionTypeConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static APTransactionTypeConstant getById(int id) {
	    for(APTransactionTypeConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}