/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>APDistributionTypeConstant</code> Contains the Distribution Types
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum ReceiptTypeConstant {

	AR(1),AP(2),GL(3);
	
	private int index;
	/**
	 * @param index
	 */
	private ReceiptTypeConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static ReceiptTypeConstant getById(int id) {
	    for(ReceiptTypeConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}