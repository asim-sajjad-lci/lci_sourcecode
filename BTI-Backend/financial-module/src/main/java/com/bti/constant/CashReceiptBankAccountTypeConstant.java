/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>ARTransactionTypeConstant</code> 
 * Name of Project: BTI
 * Created on: Dec 29, 2017
 * Modified on:  Dec 29, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum CashReceiptBankAccountTypeConstant {

	CASH(1),TAX(2),OTHER(3);
	
	private int index;
	/**
	 * @param index
	 */
	private CashReceiptBankAccountTypeConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static CashReceiptBankAccountTypeConstant getById(int id) {
	    for(CashReceiptBankAccountTypeConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}