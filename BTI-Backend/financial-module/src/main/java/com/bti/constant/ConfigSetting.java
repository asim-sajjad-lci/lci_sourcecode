/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 
package com.bti.constant;

/**
 * The <code>ConfigSetting</code> enum contains all Config Setting Codes
 * Constants Name of Project: BTI APP Created on: May 12, 2017 Modified on: May
 * 12, 2017 11:50:38 AM
 * 
 * @author seasia Version:
 */

public enum ConfigSetting {
	SMS_AUTHENTICATION("SMS_AUTHENTICATION"), PRIMARY("1"), SECONDARY("2"), SOLR_RUNNING("SOLR_RUNNING");

	private final String value;

	private ConfigSetting(String value) {
		this.value = value;
	}

	/**
	 * Return the reason phrase of this status code.
	 */
	public String getValue() {
		return value;
	}

}
