/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>BTIRoles</code> Contains the type of Application User Role.
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum PaymentTermSetupDiscountTypes {

	DAYS(1),DATE(2),NEXT_MONTH(3),MONTHS(4),ANNUAL(5);
	
	private int index;
	/**
	 * @param index
	 */
	private PaymentTermSetupDiscountTypes(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static PaymentTermSetupDiscountTypes getById(int id) {
	    for(PaymentTermSetupDiscountTypes e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}