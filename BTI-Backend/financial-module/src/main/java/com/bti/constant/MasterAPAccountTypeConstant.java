/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>ARTransactionTypeConstant</code> 
 * Name of Project: BTI
 * Created on: Dec 29, 2017
 * Modified on:  Dec 29, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum MasterAPAccountTypeConstant {

	CASH(1),ACCOUNT_PAYABLE(2),FINANCE_CHARGE(3),PURCHASE(4),
	TRADE_DISCOUNT(5),MISCELLANEOUS(6),FREIGHT(7),ACCRURED_PURCHASE(8),
	PURCHASE_PRICE_VAR(9),SERVICE_REPAIR(10),WARRANTY(11),CREDIT_MEMO(12),
	DEBIT_MEMO(13),WARRANTY_EXPENSE(14),RETURN(15);
	
	private int index;
	/**
	 * @param index
	 */
	private MasterAPAccountTypeConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static MasterAPAccountTypeConstant getById(int id) {
	    for(MasterAPAccountTypeConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}