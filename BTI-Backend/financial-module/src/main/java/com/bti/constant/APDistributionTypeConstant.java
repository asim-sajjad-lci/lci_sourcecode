/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>APDistributionTypeConstant</code> Contains the Distribution Types
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum APDistributionTypeConstant {

	CASH(1),PAYP(2),AVAIL(3),TAKN(4),FNCG(5),PURH(6),TRAD(7),
	MISC(8),FRGH_AMT(9),TAXS(10),OTHR(11),WHMT(12),
	ROUD(13),DMEM(14),CMEM(15),SRVC(16),RETN(17),
	WARS(18),WARE(19),FRGH_TAX(20),MISC_TAX(21);
	
	
	private int index;
	/**
	 * @param index
	 */
	private APDistributionTypeConstant(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
	public static APDistributionTypeConstant getById(int id) {
	    for(APDistributionTypeConstant e : values()) {
	        if(e.index==id) return e;
	    }
	    return null;
	 }
}