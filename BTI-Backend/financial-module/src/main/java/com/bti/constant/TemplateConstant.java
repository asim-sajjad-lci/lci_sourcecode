/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;

/**
 * The <code>TemplateConstant</code> enum contains all template Codes Constants
 * Name of Project: BTI APP Created on: May 12, 2017 Modified on: May 12, 2017
 * 11:50:38 AM
 * 
 * @author seasia Version:
 */

public enum TemplateConstant {
	FORGOT_PASSWORD_EMAIL("Forgot_Password"), CHANGE_USER_PASSWORD_EMAIL("Change_User_Password"), USER_REGISTRATION(
			"User_Registration"), RESET_PASSWORD("Reset_Password");

	private final String value;

	private TemplateConstant(String value) {
		this.value = value;
	}

	/**
	 * Return the reason phrase of this status code.
	 */
	public String getValue() {
		return value;
	}
}
