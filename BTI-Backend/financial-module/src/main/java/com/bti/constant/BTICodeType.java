/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */ 

package com.bti.constant;
/**
 * The <code>BTICodeType</code> Contains the type of Application Code Type.
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public enum BTICodeType {

	ROLE(1), ROLEGROUP(2), USERGROUP(3), EMPLOYEECODE(4), COMPANYCODE(5), CREDITCARD(6);

	private int index;

	/**
	 * @param index
	 */
	private BTICodeType(int index) {

		this.index = index;
	}

	public int getIndex() {
		return index;
	}
	
}