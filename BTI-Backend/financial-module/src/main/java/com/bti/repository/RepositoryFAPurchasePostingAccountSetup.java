/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAPurchasePostingAccountSetup;

/**
 * Description: RepositoryFAInsuranceMaintenance
 * Name of Project: BTI
 * Created on: Sep 01, 2017
 * Modified on: Sep 01, 2017 11:08:38 AM
 * @author seasia
 * Version: 
 */

@Repository("repositoryFAPurchasePostingAccountSetup")
public interface RepositoryFAPurchasePostingAccountSetup extends JpaRepository<FAPurchasePostingAccountSetup, Integer> {

	/**
	 * @param accountTableRowIndex
	 * @param classId
	 * @param deleted
	 * @return
	 */
	public FAPurchasePostingAccountSetup findByGlAccountsTableAccumulationAccountTableRowIndexAndFaClassSetupClassIdAndIsDeleted(String accountTableRowIndex, String classId, boolean deleted);

	/**
	 * @param classId
	 * @param deleted
	 * @return
	 */
	public FAPurchasePostingAccountSetup findByFaClassSetupClassIdAndIsDeleted(String classId, boolean deleted);
	
	/**
	 * @return
	 */
	@Query("select count(*) from FAPurchasePostingAccountSetup")
	public int getCountAll();
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select fppas from FAPurchasePostingAccountSetup fppas where fppas.isDeleted=false and fppas.glAccountsTableAccumulation.accountDescription like %:searchKeyWord% "
			+ " or fppas.faClassSetup.classSetupDescription like %:searchKeyWord%   ")
	public List<FAPurchasePostingAccountSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select fppas from FAPurchasePostingAccountSetup fppas where fppas.isDeleted=false and fppas.glAccountsTableAccumulation.accountDescription like %:searchKeyWord% "
			+ " or fppas.faClassSetup.classSetupDescription like %:searchKeyWord%   ")
	public List<FAPurchasePostingAccountSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);
}
