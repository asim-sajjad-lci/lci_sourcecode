/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAPhysicalLocationSetup;

@Repository("repositoryFAPhysicalLocationSetup")
public interface RepositoryFAPhysicalLocationSetup extends JpaRepository<FAPhysicalLocationSetup, String>{

	/**
	 * @param locationId
	 * @return
	 */
	FAPhysicalLocationSetup findByPhysicalLocationIdAndIsDeleted(String locationId, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select pls from FAPhysicalLocationSetup pls where pls.isDeleted=false and pls.physicalLocationId like %:searchKeyWord% or pls.physicalLocationDescription like %:searchKeyWord% or pls.physicalLocationDescriptionArabic like %:searchKeyWord%")
	List<FAPhysicalLocationSetup> predictiveFAPhysicalLocationSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select pls from FAPhysicalLocationSetup pls where pls.isDeleted=false and pls.physicalLocationId like %:searchKeyWord% or pls.physicalLocationDescription like %:searchKeyWord% or pls.physicalLocationDescriptionArabic like %:searchKeyWord%")
	List<FAPhysicalLocationSetup> predictiveFAPhysicalLocationSearch(@Param("searchKeyWord") String searchKeyWord);

}
