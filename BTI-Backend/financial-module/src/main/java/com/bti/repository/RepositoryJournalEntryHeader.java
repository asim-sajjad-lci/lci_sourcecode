/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.JournalEntryHeader;

/**
 * Description: Interface for JournalEntryHeader 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:43:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryJournalEntryHeader")
public interface RepositoryJournalEntryHeader extends JpaRepository<JournalEntryHeader,Integer>{
	
	/**
	 * @param journalId
	 * @param deleted
	 * @return
	 */
	JournalEntryHeader findByJournalIDAndIsDeleted(String journalId, boolean deleted);
	
	JournalEntryHeader findByJournalIDAndIsDeleted(int journalId, boolean deleted);
	
	/**
	 * @return
	 */
	@Query(" from JournalEntryHeader jeh where jeh.isDeleted=false")
	List<JournalEntryHeader> getJournalIdListForUpdateJvEntry();
	
	/**
	 * @return
	 */
	@Query("select DISTINCT jeh.journalID from JournalEntryHeader jeh where jeh.isDeleted=false")
	List<String> getDistnictJournalIdListForCopyJournalEntry();
	
	@Query("select count(*) ,COALESCE(SUM(jeh.totalJournalEntryDebit), 0) from JournalEntryHeader jeh where jeh.glBatches.batchID=:batchId and jeh.isDeleted=false")
	List<Object[]> getBatchTotalTrasactionsByTransactionType(@Param("batchId") String batchId);
	
	List<JournalEntryHeader> findByGlBatchesBatchIDAndIsDeleted(String batchId, boolean deleted);

	List<JournalEntryHeader> findByJournalID(String journalID);
	
	@Query("select j from JournalEntryHeader j where (j.journalID like %:searchKeyWord% or j.glBatches.batchDescription like %:searchKeyWord% or j.journalDescription like %:searchKeyWord% or j.journalDescriptionArabic like %:searchKeyWord%  ) and j.isDeleted=false")
	public List<JournalEntryHeader> predictiveJournalSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query("select j from JournalEntryHeader j where (j.journalID like %:searchKeyWord% or j.glBatches.batchDescription like %:searchKeyWord% or j.journalDescription like %:searchKeyWord% or j.journalDescriptionArabic like %:searchKeyWord%  ) and j.isDeleted=false")
	public List<JournalEntryHeader> predictiveJournalSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from JournalEntryHeader j where (j.journalID like %:searchKeyWord% or j.glBatches.batchDescription like %:searchKeyWord% or j.journalDescription like %:searchKeyWord% or j.journalDescriptionArabic like %:searchKeyWord%  ) and j.isDeleted=false")
	public Integer predictiveJournalHeaderSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query("select j from JournalEntryHeader j where (j.journalID like %:searchKeyWord% or j.glBatches.batchDescription like %:searchKeyWord% or j.journalDescription like %:searchKeyWord% or j.journalDescriptionArabic like %:searchKeyWord%  ) and j.isDeleted=false")
	public List<JournalEntryHeader> predictiveCompanySearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	
	public JournalEntryHeader findByJournalIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(String journalID, int seriesIndex, boolean isDeleted);
	
}
