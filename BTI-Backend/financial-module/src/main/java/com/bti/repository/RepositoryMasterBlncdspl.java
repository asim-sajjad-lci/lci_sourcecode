/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterBlncdspl;

@Repository("repositoryMasterBlncdspl")
public interface RepositoryMasterBlncdspl extends JpaRepository<MasterBlncdspl, Integer>{

	
	/**
	 * @param displayNCorPB
	 * @param deleted
	 * @return
	 */
	MasterBlncdspl findByTypeIdAndIsDeleted(short displayNCorPB,boolean deleted);

	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	MasterBlncdspl findByTypeIdAndLanguageLanguageIdAndIsDeleted(short typeId, int langId,boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterBlncdspl> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterBlncdspl> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);

	
}
