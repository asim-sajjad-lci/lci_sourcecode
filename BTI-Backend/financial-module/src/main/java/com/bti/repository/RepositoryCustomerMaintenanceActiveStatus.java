/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.CustomerMaintenanceActiveStatus;

/**
 * Description: Interface for RepositoryCustomerMaintenance 
 * Name of Project: BTI
 * Created on: Aug 08, 2017
 * Modified on: Aug 08, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCustomerMaintenanceActiveStatus")
public interface RepositoryCustomerMaintenanceActiveStatus extends JpaRepository<CustomerMaintenanceActiveStatus, Integer> {

	
	/**
	 * @param typeId
	 * @param langid
	 * @param deleted
	 * @return
	 */
	CustomerMaintenanceActiveStatus findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId,int langid,boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<CustomerMaintenanceActiveStatus> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);
	 
	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<CustomerMaintenanceActiveStatus> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
	 
}


