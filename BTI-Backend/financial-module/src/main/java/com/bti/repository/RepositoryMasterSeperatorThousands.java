/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterSeperatorThousands;

/**
 * Description: Interface for MasterSeperatorThousands 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMasterSeperatorThousands")
public interface RepositoryMasterSeperatorThousands extends JpaRepository<MasterSeperatorThousands, Integer> {
	
	
	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterSeperatorThousands> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param seperatorThousandId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	MasterSeperatorThousands findBySeperatorThousandIdAndLanguageLanguageIdAndIsDeleted(int seperatorThousandId, int langId,boolean deleted);
	
	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterSeperatorThousands> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
	
}
