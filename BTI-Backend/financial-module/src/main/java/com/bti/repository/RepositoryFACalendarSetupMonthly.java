/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FACalendarSetupMonthly;

/**
 * Description: Interface for RepositoryShipmentMethodSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFACalendarSetupMonthly")
public interface RepositoryFACalendarSetupMonthly extends JpaRepository<FACalendarSetupMonthly, Integer> {

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(distinct fscs.faCalendarSetup.calendarIndex) from FACalendarSetupMonthly fscs where fscs.isDeleted=false and fscs.faCalendarSetup.calendarDescription like %:searchKeyWord% OR fscs.faCalendarSetup.calendarDescriptionArabic like %:searchKeyWord%  ")
	public int predictiveFACalendarSetupSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
}
