/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.ReceiptType;

/**
 * Description: Interface for RepositoryReceiptType 
 * Name of Project: BTI
 * Created on: Aug 17, 2017
 * Modified on: Aug 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryReceiptType")
public interface RepositoryReceiptType extends JpaRepository<ReceiptType, Integer> {

	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	ReceiptType findByTypeIdAndLanguageLanguageIdAndIsDeleted(Integer typeId, int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<ReceiptType> findByIsDeletedAndLanguageLanguageId(boolean b, int i);
	
}


