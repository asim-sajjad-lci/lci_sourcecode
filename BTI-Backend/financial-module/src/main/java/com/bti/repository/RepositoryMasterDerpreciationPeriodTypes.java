/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterDepreciationPeriodTypes;

@Repository("repositoryMasterDerpreciationPeriodTypes")
public interface RepositoryMasterDerpreciationPeriodTypes extends JpaRepository<MasterDepreciationPeriodTypes, Integer>{

	
	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterDepreciationPeriodTypes> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	MasterDepreciationPeriodTypes findByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId, int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterDepreciationPeriodTypes> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);

}
