/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CustomerMaintenanceOptions;

/**
 * Description: Interface for CustomerMaintenanceOptions 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCustomerMaintenanceOptions")
public interface RepositoryCustomerMaintenanceOptions extends JpaRepository<CustomerMaintenanceOptions, Integer> {
	
	
	/**
	 * @param custNumber
	 * @param deleted
	 * @return
	 */
	CustomerMaintenanceOptions findByCustomerMaintenanceCustnumberAndIsDeleted(String custNumber,boolean deleted);

	/**
	 * @param paymentTermId
	 * @return
	 */
	@Query("select count(*) from CustomerMaintenanceOptions c where c.isDeleted=false and c.paymentTermsSetup.paymentTermId=:paymentTermId ")
	public Integer getCountByPaymentTermsSetupPaymentTermId(@Param("paymentTermId") String paymentTermId);
	
	/**
	 * @param cardId
	 * @return
	 */
	@Query("select count(*) from CustomerMaintenanceOptions c where c.isDeleted=false and c.creditCardsSetup.cardId=:cardId ")
	public Integer getCountByCreditCardSetupCardId(@Param("cardId") String cardId);
	
	/**
	 * @param shipmentMethodId
	 * @return
	 */
	@Query("select count(*) from CustomerMaintenanceOptions c where c.isDeleted=false and c.shipmentMethodSetup.shipmentMethodId=:shipmentMethodId ")
	public Integer getCountByShipmentMethodSetup(@Param("shipmentMethodId") String shipmentMethodId);

}
