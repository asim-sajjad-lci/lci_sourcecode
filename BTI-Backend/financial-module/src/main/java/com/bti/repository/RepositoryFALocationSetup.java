/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FALocationSetup;

@Repository("repositoryFALocationSetup")
public interface RepositoryFALocationSetup extends JpaRepository<FALocationSetup, Integer>{

	/**
	 * @param locationId
	 * @return
	 */
	FALocationSetup findByLocationIdAndIsDeleted(String locationId, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select ls from FALocationSetup ls where ls.isDeleted=false and ls.locationId like %:searchKeyWord% or ls.cityMaster.cityName like %:searchKeyWord% or ls.countryMaster.countryName like %:searchKeyWord% or ls.stateMaster.stateName like %:searchKeyWord% " )
	List<FALocationSetup> predictiveFALocationSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select ls from FALocationSetup ls where ls.isDeleted=false and ls.locationId like %:searchKeyWord% or ls.cityMaster.cityName like %:searchKeyWord% or ls.countryMaster.countryName like %:searchKeyWord% or ls.stateMaster.stateName like %:searchKeyWord% ")
	List<FALocationSetup> predictiveFALocationSearch(@Param("searchKeyWord") String searchKeyWord);

}
