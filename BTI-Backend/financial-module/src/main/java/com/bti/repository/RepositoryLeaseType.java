/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.LeaseType;

@Repository("repositoryLeaseType")
public interface RepositoryLeaseType extends JpaRepository<LeaseType, Integer> {

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<LeaseType> findByIsDeletedAndLanguageLanguageId(boolean b, int i);
	
	LeaseType findByLeaseTypeIdAndIsDeletedAndLanguageLanguageId(int leaseTypeId,boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);

}
