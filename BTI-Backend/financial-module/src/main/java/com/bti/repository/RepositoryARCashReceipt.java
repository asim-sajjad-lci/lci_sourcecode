/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ARCashReceipt;

@Repository("repositoryARCashReceipt")
public interface RepositoryARCashReceipt extends JpaRepository<ARCashReceipt, Integer>{

	/**
	 * @param receiptNumber
	 * @return
	 */
	ARCashReceipt findByCashReceiptNumber(String receiptNumber);
	
	List<ARCashReceipt> findByArBatchesBatchID(String batchId);
	
	@Query("select count(*),COALESCE(SUM(cr.cashReceiptAmount), 0) from ARCashReceipt cr where cr.arBatches.batchID=:batchId ")
	List<Object[]> getTotalAmountAndTotalCountByBatchId(@Param("batchId") String batchId);

}
