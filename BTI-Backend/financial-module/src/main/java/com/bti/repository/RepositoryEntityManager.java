package com.bti.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bti.model.dto.DtoAccountStatementMasterRecord;
import com.bti.model.dto.DtoProfitAndLossReport;
import com.bti.model.dto.DtoTrailBalanceReport;
import com.bti.util.CommonUtils;

@Repository("repositoryEntityManager")
public class RepositoryEntityManager {

	@Autowired
	EntityManager entityManager;
	
	public DtoTrailBalanceReport getTrialBalanceReport(Date startDate, Date endDate) {
		DtoTrailBalanceReport dtoTrailBalanceReport = new DtoTrailBalanceReport();
		List<DtoTrailBalanceReport> dtoTrailBalanceReportList = new ArrayList<>();
		Query q = entityManager.createNativeQuery("select account_number as ACCOUNT_NUMBER, \r\n" + 
				"		account_type as ACCOUNT_TYPE,\r\n" + 
				"		main_account_description as ACCOUNT_DESCRIPTION,\r\n" + 
				"		main_account_description_arabic as ACCOUNT_DESCRIPTION_ARABIC,\r\n" + 
				"		prev_bal as PREVIOUS_BALANCE, \r\n" + 
				"		debit as DEBIT_AMOUNT, \r\n" + 
				"		credit as CREDIT_AMOUNT, \r\n" + 
				"		net_charge as NET_CHARGE, \r\n" + 
				"		(prev_bal + net_charge)  as ENDING_BALANCE\r\n" + 
				"from (\r\n" + 
				"select \r\n" + 
				"account_number, account_type, main_account_description, main_account_description_arabic,\r\n" + 
				"sum( (case when (transaction_date < ?) then (debit_amount - credit_amount) else 0 end)) AS prev_bal,\r\n" + 
				"sum( (case when (transaction_date between ? and ?) then (debit_amount) else 0 end)) as debit, \r\n" + 
				"sum( (case when (transaction_date between ? and ?) then (credit_amount) else 0 end)) as credit,\r\n" + 
				"sum( (case when (transaction_date between ? and ?) then (debit_amount - credit_amount) else 0 end )) as net_charge\r\n" + 
				"from trl_blnc_n_act_stmt_rpts_v \r\n" + 
				"group by account_number, account_type\r\n" + 
				") as a");
		q.setParameter(1, startDate);
		q.setParameter(2, startDate);
		q.setParameter(3, endDate);
		q.setParameter(4, startDate);
		q.setParameter(5, endDate);
		q.setParameter(6, startDate);
		q.setParameter(7, endDate);
		
		List<Object[]> list =  q.getResultList();
		 
		int totalAccounts=0;

		if (list != null && !list.isEmpty()) 
			{
		    	totalAccounts=list.size();
//				for(TrialBalanceView trialBalanceView : list){
		    	for (int i=0; i < list.size(); i++) {
					DtoTrailBalanceReport dtoReport = new DtoTrailBalanceReport();
					Object[] result = list.get(i);
					
					dtoReport.setAccountNumber(result[0].toString());
					dtoReport.setAccountDescription(result[2].toString());
					dtoReport.setAccountDescriptionArabic(result[3].toString());

					dtoReport.setBeginningBalance(Double.valueOf(result[4].toString()));
					dtoReport.setDebit(Double.valueOf(result[5].toString()));
					dtoReport.setCredit(Double.valueOf(result[6].toString()));
					dtoReport.setNetChange(Double.valueOf(result[7].toString()));
					dtoReport.setEndingBalance(Double.valueOf(result[8].toString()));
					dtoTrailBalanceReportList.add(dtoReport);
				}
			}

		q = entityManager.createNativeQuery("select \r\n" + 
				"sum(prev_bal) as PREVIOUS_BALANCE, \r\n" + 
				"sum(debit) as TOTAL_DEBIT_AMOUNT, \r\n" + 
				"sum(credit) as TOTAL_CREDIT_AMOUNT, \r\n" + 
				"sum(net_charge) as TOTAL_NET_CHARGE, \r\n" + 
				"sum(ending_balance) as TOTAL_ENDING_BALANCE\r\n" + 
				"from (\r\n" + 
				"select accountnumber, acttyp, prev_bal, debit, credit, net_charge, \r\n" + 
				"(prev_bal + net_charge) as ending_balance\r\n" + 
				"from (\r\n" + 
				"select \r\n" + 
				"accountnumber, acttyp,\r\n" + 
				"sum( (case when (trxddt < ?) then (debitamt-crdtamt) else 0 end)) AS Prev_Bal,\r\n" + 
				"sum( (case when (trxddt between ? and ?) then (debitamt) else 0 end)) as Debit, \r\n" + 
				"sum( (case when (trxddt between ? and ?) then (crdtamt) else 0 end)) as Credit,\r\n" + 
				"sum( (case when (trxddt between ? and ?) then (debitamt - crdtamt) else 0 end)) as Net_Charge\r\n" + 
				"from accountsdetailsview\r\n" + 
				"group by accountnumber, acttyp\r\n" + 
				") as a\r\n" + 
				") as b");
		q.setParameter(1, startDate);
		q.setParameter(2, startDate);
		q.setParameter(3, endDate);
		q.setParameter(4, startDate);
		q.setParameter(5, endDate);
		q.setParameter(6, startDate);
		q.setParameter(7, endDate);
		
		Object[] result =  (Object[]) q.getSingleResult();

		dtoTrailBalanceReport.setTrailBalanceReportList(dtoTrailBalanceReportList);
		dtoTrailBalanceReport.setTotalAccounts(totalAccounts);
		dtoTrailBalanceReport.setTotalBeginningBalance(Double.valueOf(result[0].toString()));
		dtoTrailBalanceReport.setTotalDebit(Double.valueOf(result[1].toString()));
		dtoTrailBalanceReport.setTotalCredit(Double.valueOf(result[2].toString()));
		dtoTrailBalanceReport.setTotalNetChange(Double.valueOf(result[3].toString()));
		dtoTrailBalanceReport.setTotalEndingBalance(Double.valueOf(result[4].toString()));

		return dtoTrailBalanceReport;	

	}

	
	public HashMap<String, DtoAccountStatementMasterRecord> getAllAccountsWithPreviousBalance(String startDate){
		HashMap<String, DtoAccountStatementMasterRecord> map = new HashMap<String, DtoAccountStatementMasterRecord>();

		Query q = entityManager.createNativeQuery("select account_number, \r\n" + 
				"main_account_description, main_account_description_arabic, account_category_description,\r\n" + 
				"sum( (case when (transaction_date < ?) then (debit_amount - credit_amount) else 0 end)) AS prev_bal\r\n" + 
				"from trl_blnc_n_act_stmt_rpts_v \r\n" + 
				"group by account_number, main_account_description, main_account_description_arabic, account_category_description ");
		q.setParameter(1, startDate);

		List<Object[]> list =  q.getResultList();
		 
		int totalAccounts=0;

		if (list != null && !list.isEmpty()) 
			{
		    	for (int i=0; i < list.size(); i++) {
					Object[] row = list.get(i);

					String accountNumber = CommonUtils.castToString(row[0]); 
					String accountDescription = CommonUtils.castToString(row[1]);
					String accountDescriptionArabic = CommonUtils.castToString(row[2]);
					String accountCategoryDescription = CommonUtils.castToString(row[3]);
					Double previousBalance = CommonUtils.castToDouble(row[4]);
					
					DtoAccountStatementMasterRecord dtoAccountStatementMasterRecord = new DtoAccountStatementMasterRecord();
					dtoAccountStatementMasterRecord.setAccountNumber(accountNumber);
					dtoAccountStatementMasterRecord.setDescription(accountDescription + " - " + accountDescriptionArabic);
					dtoAccountStatementMasterRecord.setAccountCategory(accountCategoryDescription);
					dtoAccountStatementMasterRecord.setBeginningBalance(previousBalance);
					
					map.put(accountNumber, dtoAccountStatementMasterRecord);
				}
			}
		
		return map;

	}

//	public List<Object[]> getAllAccountNumbers(){
//		HashMap<String, Double> map = new HashMap<String, Double>();
//
//		Query q = entityManager.createNativeQuery("select distinct a.account_number, a.main_account_description, a.main_account_description_arabic, a.account_category_description \r\n" + 
//				"   			from trl_blnc_n_act_stmt_rpts_v");
//
//		List<Object[]> list =  q.getResultList();
//		 
//		return list;
//	}
	
	

	public String getMonthSequence(String motheFilter , int sourceDoc) {
		Query q = entityManager.createNativeQuery("SELECT max(gltransactions.JORNID) FROM gltransactions WHERE gltransactions.JORNID LIKE ? and gltransactions.SOURDOC = ? ORDER BY gltransactions.JORNID DESC ");
		q.setParameter(1, motheFilter+"%");
		q.setParameter(2, sourceDoc);
		//System.out.println(q.);
		Object obj = q.getSingleResult();
		String result = (String) String.valueOf(obj);
		return result;
	}
	

	public List<Object[]> getPeriod(DtoProfitAndLossReport dtoProfitAndLossReport) {
		List<Object[]> data = entityManager.createNamedStoredProcedureQuery("rptPL2").setParameter("periodPARAM", dtoProfitAndLossReport.getPeriod()).setParameter("yearParam", dtoProfitAndLossReport.getYear()).getResultList();
		return data;
	}
	
	public List<Object[]> getBalanceSheetReport() {
		List<Object[]> data = entityManager.createNamedStoredProcedureQuery("RPTBS").getResultList();
		return data;
	}

	
	public List<Object[]> getAllEmployee() {
		List<Object[]> data = entityManager.createNativeQuery("SELECT * FROM hr00101").getResultList();;
		return data;
	}
	
	public List<Object[]> getEmployeeByCode(String code) {
		List<Object[]> data = entityManager.createNativeQuery("SELECT * FROM HR00101 WHERE HR00101.empnts = '"+ code+"'").getResultList();
		return data;
	}
	
	public List<Object[]> getEmployeeByCodeNew(String code) {
		List<Object[]> data = entityManager.createNativeQuery("SELECT * FROM HR00101 WHERE HR00101.employid = '"+ code+"'").getResultList();
		return data;
	}
	
	
	public List<Object[]> getGlTransactionByJORNIDAndSerialCodeId(String JORNID, int sourceDocumentId) {
		List<Object[]> data = entityManager.createNativeQuery("select * from gltransactions where JORNID = '"+JORNID+"' and SOURDOC = "+sourceDocumentId).getResultList();
		return data;
	}
	
	public List<Object[]> getGlTransactionsByJORNID(String JORNID){
		List<Object[]> data = entityManager.createNativeQuery("select * from  gltransactions where JORNID = '"+JORNID+"'").getResultList();
		return data;
	}
 	// 1- first name 
	// 2- mdle name
	// 3- 
	public void insertEmployee(String[] employee, int hcmEmployeeCode , String employeeFCode) {
		entityManager.createNativeQuery("INSERT INTO "
				+ "`hr00101`"
				+ "( `creatddt`, `is_deleted`, `dex_row_id`, `changeby`, `modifdt`,"
				+ "`dex_row_ts`, `empahirdt`, `empbor`, `empborh`, `empcitzn`, `empfrtnm`, `empfrtnma`,"
				+ "`empgend`, `emphirdt`, `employid`, `empimg`, `empimagr`, `empactiv`, `empdcdt`, `empresn`,"
				+ "`emplstnm`, `emplstnma`, `emplstdt`, `empmast`, `empmdlnm`, `empmdlnma`, `empsmok`, `emptitl`,"
				+ "`emptitla`, `empltyp`, `empuserid`, `empwrkhr`, `empidexpr`, `empidexprh`, `emppasexpr`, `emppasexprh`,"
				+ "`empidnmbr`, `emppasnmr`, `depindx`, `divindx`, `empaddindx`, `empnaindx`, `locindx`, `potindx`, `suprindx`,"
				+ "`empnts`) "
				+ "VALUES "
				+ "( '2019-01-28 08:16:14', 0, 0, 1, '2019-01-28 08:16:14',"
				+ "'2019-01-28 08:16:14', '2019-03-13', '2019-03-13 00:00:00', '19/07/1440', b'1', '"+employee[0]+"',"
				+ "'هيثم', 1, '2009-04-01 00:00:00', 'MAC0001', NULL, b'0', b'0', NULL, NULL, 'ALGOHANI', 'الجهني', NULL, 2,"
				+ "'BAKHEET', 'بخيت سليم', b'0', 'Mr.', 'السيد', 1, 1, 2112, NULL, '', '2019-01-28 08:16:14', '25/04/1440',"
				+ "'1045514195', '', 1, 1, 47, 188, 50, 1048, 77, 'BT0901');\r\n" + 
				"");
	}
	
	public List<Object[]> getAllCustomer() {
		List<Object[]> data = entityManager.createNativeQuery("SELECT  CUSTNMBR,concat( CUSTNAME,':' ,CUSTNAMEA) FROM ar00101").getResultList();;
		return data;
	}
	
	public List<Object[]> getAllVendor() {
		List<Object[]> data = entityManager.createNativeQuery("SELECT VENDORID, VENDNAME, VENDNAMEA FROM ap00101").getResultList();;
		return data;
	}
	
	public List<Object[]> getAllFixedAsset() {
		List<Object[]> data = entityManager.createNativeQuery("SELECT ASSETID, DSCRPTN, DSCRPTNA FROM fa00101").getResultList();;
		return data;
	}
	
	public List<Object[]> getAllSalesman() {
		List<Object[]> data = entityManager.createNativeQuery("SELECT SALSPERID, concat(FRSTNAME,' ', MIDNAME,' ', LASTNAME"
				+ ",':',FRSTNAMEA,' ',MIDNAMEA,' ',LASTNAMEA)  FROM ar00104").getResultList();;
		return data;
	}
	
	public List<Object[]> getAllAccountDetails(){
		return  entityManager.createNativeQuery("SELECT gl49999.ACTROWID, actnum, actdesc, actdesca    FROM gl49999 INNER JOIN   gl99999 g  ON gl49999.ACTROWID = g.actrowid AND g.segmtnum =1 INNER JOIN gl00101 g1 ON g.indxid = g1.ACTINDX where  g1.ACTIVE = 1  AND  g1.ACCTENTR =1 AND g.is_deleted = 0  ORDER  BY gl49999.actnum").getResultList();
	}


}
