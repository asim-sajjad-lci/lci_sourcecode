/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.BankSetup;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryBankSetup")
public interface RepositoryBankSetup extends JpaRepository<BankSetup, Integer> {
	
	/**
	 * @param bankId
	 * @return
	 */
	BankSetup findByBankIdAndIsDeleted(String bankId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from BankSetup bs where bs.isDeleted=false and bs.bankId like %:searchKeyWord% or bs.bankDescription like %:searchKeyWord% or bs.bankDescriptionArabic like %:searchKeyWord%  ")
	public int predictiveBankSetupSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select bs from BankSetup bs where bs.isDeleted=false and bs.bankId like %:searchKeyWord% or bs.bankDescription like %:searchKeyWord% or bs.bankDescriptionArabic like %:searchKeyWord% ")
	public List<BankSetup> predictiveBankSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select bs from BankSetup bs where bs.isDeleted=false and bs.bankId like %:searchKeyWord% or bs.bankDescription like %:searchKeyWord% or bs.bankDescriptionArabic like %:searchKeyWord% ")
	public List<BankSetup> predictiveBankSetupSearch(@Param("searchKeyWord") String searchKeyWord);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update BankSetup d set d.isDeleted =:deleted, d.changeBy=:changeBy where d.bankId IN (:bankIdList)")
	void deleteMultipleBankSetup(@Param("bankIdList") List<String> bankIdList, @Param("deleted") boolean deleted,
			@Param("changeBy") Integer changeBy);

}
