/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bti.model.MasterCustomerClassMinimumCharge;


/**
 * Description: Interface for MasterCustomerClassMinimumCharge 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMasterCustomerClassMinimumCharge")
public interface RepositoryMasterCustomerClassMinimumCharge extends JpaRepository<MasterCustomerClassMinimumCharge, Integer> {
	
	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	MasterCustomerClassMinimumCharge findByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId,int langId,boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterCustomerClassMinimumCharge> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);
	
	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterCustomerClassMinimumCharge> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
	
}
