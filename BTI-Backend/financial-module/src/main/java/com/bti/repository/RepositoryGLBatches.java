/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bti.model.GLBatches;
import com.bti.model.GLConfigurationAuditTrialCodes;


/**
 * Description: Interface for GLBatches 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLBatches")
public interface RepositoryGLBatches extends JpaRepository<GLBatches,String>{
	
	/**
	 * @param batchId
	 * @param deleted
	 * @return
	 */
	//GLBatches findByBatchIDAndIsDeleted(String batchId, boolean deleted);
	

	/**
	 * @param batchId
	 * @param typeId
	 * @param deleted
	 * @return
	 */
	GLBatches findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(String batchId,int typeId,boolean deleted);
	

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from GLBatches glb where glb.isDeleted=false and "
			+ " ( glb.batchID like %:searchKeyWord% or glb.batchDescription like %:searchKeyWord% "
			+ " or glb.gLConfigurationAuditTrialCodes.sourceDocument like %:searchKeyWord% ) ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);

	

	/*
	 * 
	 *
	 * 
	 */
	GLBatches findByBatchIDAndIsDeleted(String batchId, boolean deleted);
	
	
	/*
	 * 
	 * 
	 */

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select glb from GLBatches glb where glb.isDeleted=false and "
			+ " ( glb.batchID like %:searchKeyWord% or glb.batchDescription like %:searchKeyWord% "
			+ " or glb.gLConfigurationAuditTrialCodes.sourceDocument like %:searchKeyWord% ) ")
	public List<GLBatches> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	
	
//	/**
//	 * @param searchKeyWord
//	 * @return
//	 */
//	@Query("select count(*) from GLBatches glb where glb.isDeleted=false and "
//			+ " ( glb.batchID like %:searchKeyWord% or glb.batchDescription like %:searchKeyWord% "
//			+ " ) ")
//	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select glb from GLBatches glb where glb.isDeleted=false and "
			+ " ( glb.batchID like %:searchKeyWord% or glb.batchDescription like %:searchKeyWord% "

			+ "   or glb.gLConfigurationAuditTrialCodes.sourceDocument like %:searchKeyWord% ) ")
	List<GLBatches> predictiveSearch(@Param("searchKeyWord") String searchKeyWord );
	
	//List<GLBatches> findByBatchTransactionTypeTypeIdAndIsDeleted(int typeId,boolean deleted);



	
//	/**
//	 * @param searchKeyWord
//	 * @param pageable
//	 * @return
//	 */
//	@Query("select glb from GLBatches glb where glb.isDeleted=false and "
//			+ " ( glb.batchID like %:searchKeyWord% or glb.batchDescription like %:searchKeyWord% "
//			+ " or glb.batchTransactionType.transactionType like %:searchKeyWord% ) ")
//	public List<GLBatches> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
//			Pageable pageable);

	
//	/**
//	 * @param searchKeyWord
//	 * @return
//	 */
//	@Query("select glb from GLBatches glb where glb.isDeleted=false and "
//			+ " ( glb.batchID like %:searchKeyWord% or glb.batchDescription like %:searchKeyWord% "
//			+ "   or glb.batchTransactionType.transactionType like %:searchKeyWord% ) ")
//	List<GLBatches> predictiveSearch(@Param("searchKeyWord") String searchKeyWord );
	
	List<GLBatches> findByBatchTransactionTypeTypeIdAndIsDeleted(int typeId,boolean deleted);
	
	List<GLBatches> findByGLConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(int seriesID,boolean deleted );

	
}
