/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.model.GLYTDHistoryTransactions;

/**
 * Description: Interface for RepositoryGlytdHistoryTransaction 
 * Name of Project: BTI
 * Created on: Dec 6, 2017
 * Modified on: Dec 6, 2017 2:50:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGlytdHistoryTransaction")
public interface RepositoryGlytdHistoryTransaction extends JpaRepository<GLYTDHistoryTransactions,Integer>{
	
	/**
	 * @param journalId
	 * @param deleted
	 * @return
	 */
	List<GLYTDHistoryTransactions> findByJournalEntryIDAndIsDeleted(String journalId, boolean deleted);
	
	/**
	 * @return
	 */
	@Query("select DISTINCT oht.journalEntryID FROM GLYTDHistoryTransactions oht WHERE oht.isDeleted=false and oht.transactionSource='JV'")
	List<String> getDistnictJournalIdForCopyJVEntry();
	
}
