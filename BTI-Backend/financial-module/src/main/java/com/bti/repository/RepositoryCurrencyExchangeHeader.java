/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bti.model.CurrencyExchangeHeader;

/**
 * Description: Interface for RepositoryCountryMaster 
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCurrencyExchangeHeader")
public interface RepositoryCurrencyExchangeHeader extends JpaRepository<CurrencyExchangeHeader, Integer> {

	/**
	 * @param exchangeId
	 * @return
	 */
	CurrencyExchangeHeader findByExchangeIdAndIsDeleted(String exchangeId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from CurrencyExchangeHeader ceh where ceh.isDeleted=false and "
			+ "(ceh.exchangeId like %:searchKeyWord% or ceh.exchangeDescription like %:searchKeyWord% or "
			+ "ceh.exchangeDescriptionArabic like %:searchKeyWord% or ceh.currencySetup.currencyId like %:searchKeyWord%) ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select ceh from CurrencyExchangeHeader ceh where ceh.isDeleted=false and (ceh.exchangeId like %:searchKeyWord% "
			+ "or ceh.exchangeDescription like %:searchKeyWord% or ceh.exchangeDescriptionArabic like %:searchKeyWord% or "
			+ "ceh.currencySetup.currencyId like %:searchKeyWord% )")
	public List<CurrencyExchangeHeader> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageable);
	
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select ceh from CurrencyExchangeHeader ceh where ceh.isDeleted=false and ceh.exchangeId like %:searchKeyWord% or ceh.exchangeDescription like %:searchKeyWord% or ceh.exchangeDescriptionArabic like %:searchKeyWord% or ceh.currencySetup.currencyId like %:searchKeyWord% ")
	public List<CurrencyExchangeHeader> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param currencyId
	 * @param deleted
	 * @return
	 */
	public List<CurrencyExchangeHeader> findByCurrencySetupCurrencyIdAndIsDeleted(String currencyId, boolean deleted);

	/**
	 * @param currencyId
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	@Query("select ceh from CurrencyExchangeHeader ceh where ceh.isDeleted =:deleted and ceh.currencySetup.currencyId=:currencyId")
	public List<CurrencyExchangeHeader> findByCurrencySetupCurrencyIdAndIsDeleted(@Param("currencyId") String currencyId,  @Param("deleted") boolean deleted, Pageable pageable);
	
	/**
	 * @param currencyId
	 * @return
	 */
	@Query("select count(*) from CurrencyExchangeHeader ceh where ceh.isDeleted=false and ceh.currencySetup.currencyId=:currencyId")
	public Integer getCountByCurrencySetupCurrencyIdAndIsDeleted(@Param("currencyId") String currencyId);
	
	/**
	 * @param exchangeId
	 * @return
	 */
	CurrencyExchangeHeader findByExchangeIndexAndIsDeleted(int exchangeIndex,boolean deleted);
}


