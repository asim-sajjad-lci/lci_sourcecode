/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.PMSetupFormatType;

/**
 * Description: Interface for RepositoryPMSetup 
 * Name of Project: BTI
 * Created on: Aug 28, 2017
 * Modified on: Aug 28, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryPMSetupFormatType")
public interface RepositoryPMSetupFormatType extends JpaRepository<PMSetupFormatType, Integer> {
	
	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	PMSetupFormatType findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId,int langId, boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<PMSetupFormatType> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<PMSetupFormatType> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);

	
}
