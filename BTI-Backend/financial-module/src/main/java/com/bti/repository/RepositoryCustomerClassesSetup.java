/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CustomerClassesSetup;

/**
 * Description: Interface for RepositoryCustomerClassesSetup 
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCustomerClassesSetup")
public interface RepositoryCustomerClassesSetup extends JpaRepository<CustomerClassesSetup, Integer> {

	/**
	 * @param customerClassId
	 * @return
	 */
	CustomerClassesSetup findByCustomerClassIdAndIsDeleted(String customerClassId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*)  from CustomerClassesSetup ccs where ccs.isDeleted=false and ccs.customerClassId like %:searchKeyWord% or ccs.customerClassDescription like %:searchKeyWord% or ccs.customeClassDescriptionArabic like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select ccs from CustomerClassesSetup ccs where ccs.isDeleted=false and ccs.customerClassId like %:searchKeyWord% or ccs.customerClassDescription like %:searchKeyWord% or ccs.customeClassDescriptionArabic like %:searchKeyWord% ")
	public List<CustomerClassesSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select ccs from CustomerClassesSetup ccs where ccs.isDeleted=false and ccs.customerClassId like %:searchKeyWord% or ccs.customerClassDescription like %:searchKeyWord% or ccs.customeClassDescriptionArabic like %:searchKeyWord% ")
	public List<CustomerClassesSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param paymentTermId
	 * @return
	 */
	@Query("select count(*) from CustomerClassesSetup ccs where ccs.isDeleted=false and ccs.paymentTermsSetup.paymentTermId=:paymentTermId ")
	public Integer getCountByPaymentTermsSetupPaymentTermId(@Param("paymentTermId") String paymentTermId);

	/**
	 * @param shipmentMethodId
	 * @return
	 */
	@Query("select count(*) from CustomerClassesSetup ccs where ccs.isDeleted=false and ccs.shipmentMethodSetup.shipmentMethodId=:shipmentMethodId ")
	public Integer getCountByShipmentMethodSetup(@Param("shipmentMethodId") String shipmentMethodId);
	
	/**
	 * @param vatScheduleId
	 * @return
	 */
	@Query("select count(*) from CustomerClassesSetup ccs where ccs.isDeleted=false and ccs.vatSetup.vatScheduleId=:vatScheduleId ")
	public Integer getCountByVatSetup(@Param("vatScheduleId") String vatScheduleId);
	

}


