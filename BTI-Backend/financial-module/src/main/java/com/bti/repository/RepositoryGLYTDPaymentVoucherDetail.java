package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bti.model.GLYTDPaymentVoucherDetail;

public interface RepositoryGLYTDPaymentVoucherDetail extends JpaRepository<GLYTDPaymentVoucherDetail,Integer>{

}
