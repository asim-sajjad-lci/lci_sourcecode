/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.model.RMDocumentsTypeSetup;

/**
 * Description: Interface for RMPeriodSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryRMDocumentType")
public interface RepositoryRMDocumentType extends JpaRepository<RMDocumentsTypeSetup, Integer> {
	
	/**
	 * @param docTypeId
	 * @param deleted
	 * @return
	 */
	RMDocumentsTypeSetup findByArDocumentTypeIdAndIsDeleted(int docTypeId,boolean deleted);
	
	/**
	 * @return
	 */
	@Query("select count(*) from RMDocumentsTypeSetup rm where rm.isDeleted=false ")
	public int countAll();
	
	/**
	 * @param pageable
	 * @return
	 */
	@Query("select rm from RMDocumentsTypeSetup rm where rm.isDeleted=false ")
	public List<RMDocumentsTypeSetup> getAllWithPagination(Pageable pageable);
	
	List<RMDocumentsTypeSetup> findByIsDeleted(boolean deleted);
	
	RMDocumentsTypeSetup findByDocumentCodeAndIsDeleted(String docCode,boolean deleted);
	
}
