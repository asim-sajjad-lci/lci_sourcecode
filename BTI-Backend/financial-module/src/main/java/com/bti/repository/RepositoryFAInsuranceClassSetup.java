/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAInsuranceClassSetup;

/**
 * Description: Repository FAInsurance Class Setup
 * Name of Project: BTI
 * Created on: Sep 01, 2017
 * Modified on: Sep 01, 2017 09:08:38 AM
 * @author seasia
 * Version: 
 */

@Repository("repositoryFAInsuranceClassSetup")
public interface RepositoryFAInsuranceClassSetup extends JpaRepository<FAInsuranceClassSetup, Integer> {

	/**
	 * @param insuranceIndexId
	 * @return
	 */
	FAInsuranceClassSetup findByInsuranceClassIndexAndIsDeleted(int insuranceIndexId, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FAInsuranceClassSetup ic where ic.isDeleted=false and ic.insuranceDescription like %:searchKeyWord%  or ic.insuranceDescriptionArabic like %:searchKeyWord%")
	Integer predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select ic from FAInsuranceClassSetup ic where ic.isDeleted=false and ic.insuranceDescription like %:searchKeyWord%  or ic.insuranceDescriptionArabic like %:searchKeyWord%")
	List<FAInsuranceClassSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select ic from FAInsuranceClassSetup ic where ic.isDeleted=false and ic.insuranceDescription like %:searchKeyWord%  or ic.insuranceDescriptionArabic like %:searchKeyWord%")
	List<FAInsuranceClassSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param insuranceClassId
	 * @return
	 */
	FAInsuranceClassSetup findByInsuranceClassId(String insuranceClassId);
	
	/**
	 * @param insuranceClassIndex
	 * @param insuranceClassId
	 * @return
	 */
	@Query("select ic from FAInsuranceClassSetup ic where ic.isDeleted=false and ic.insuranceClassIndex !=:insuranceClassIndex and ic.insuranceClassId =:insuranceClassId ")
	FAInsuranceClassSetup checkInsuranceClassIdButSkipCurrentIndecClassId(@Param("insuranceClassIndex") int insuranceClassIndex , @Param("insuranceClassId") String insuranceClassId);
	
}
