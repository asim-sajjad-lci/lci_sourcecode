/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.AccountNumberView;

@Repository("RepositoryAccountNumberView")
public interface RepositoryAccountNumberView extends JpaRepository<AccountNumberView, Integer>{

	/**
	 * @param accountnumber
	 * @return
	 */
	AccountNumberView findByAccountnumber(String accountnumber);
	
	List<AccountNumberView> findByAccountcategory(String accountCategory);
	
}
