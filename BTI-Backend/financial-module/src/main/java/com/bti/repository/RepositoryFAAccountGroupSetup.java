/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAAccountGroupsSetup;

/**
 * Description: Interface for FA Account Groups Setup
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFAAccountGroupSetup")
public interface RepositoryFAAccountGroupSetup extends JpaRepository<FAAccountGroupsSetup, Integer> {
	
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FAAccountGroupsSetup faags where faags.isDeleted=false and faags.faAccountGroupId like %:searchKeyWord% or faags.faAccountGroupDescription like %:searchKeyWord% or faags.faAccountGroupDescriptionArabic like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select faags from FAAccountGroupsSetup faags where faags.isDeleted=false and faags.faAccountGroupId like %:searchKeyWord% or faags.faAccountGroupDescription like %:searchKeyWord% or faags.faAccountGroupDescriptionArabic like %:searchKeyWord% ")
	public List<FAAccountGroupsSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select faags from FAAccountGroupsSetup faags where faags.isDeleted=false and faags.faAccountGroupId like %:searchKeyWord% or faags.faAccountGroupDescription like %:searchKeyWord% or faags.faAccountGroupDescriptionArabic like %:searchKeyWord% ")
	public List<FAAccountGroupsSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);
	
	
	/**
	 * @param faAccountGroupIndex
	 * @param deleted
	 * @return
	 */
	FAAccountGroupsSetup findByFaAccountGroupIndexAndIsDeleted(int faAccountGroupIndex, boolean deleted);
	
	/**
	 * @param accountGroupId
	 * @param deleted
	 * @return
	 */
	FAAccountGroupsSetup findByfaAccountGroupId(String accountGroupId);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update FAAccountGroupsSetup d set d.isDeleted =:deleted where d.faAccountGroupId=:id")
	void deleteRecordById(@Param("id") String id, @Param("deleted") Boolean deleted);

	public FAAccountGroupsSetup findByfaAccountGroupIdAndIsDeleted(String accountGroupId, boolean b);
	
	
	
}
