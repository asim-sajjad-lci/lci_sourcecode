/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.MainAccountTypes;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: Aug 10, 2017
 * Modified on: May 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMainAccountTypes")
public interface RepositoryMainAccountTypes extends JpaRepository<MainAccountTypes, Integer> {

	
	/**
	 * @param accountTypeId
	 * @param deleted
	 * @return
	 */
	public MainAccountTypes findByAccountTypeIdAndIsDeleted(Integer accountTypeId, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from MainAccountTypes mat where mat.isDeleted=false and  mat.accountTypeName like %:searchKeyWord% or mat.accountTypeNameArabic like %:searchKeyWord%  ")
	public int predictiveMainAccountTypeSearchCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select mat from MainAccountTypes mat where mat.isDeleted=false and mat.accountTypeName like %:searchKeyWord% or mat.accountTypeNameArabic like %:searchKeyWord% ")
	List<MainAccountTypes> predictiveMainAccountTypeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageable);
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select mat from MainAccountTypes mat where mat.isDeleted=false and mat.accountTypeName like %:searchKeyWord% or mat.accountTypeNameArabic like %:searchKeyWord% ")
	List<MainAccountTypes> predictiveMainAccountTypeSearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param accountTypeName
	 * @param deleted
	 * @return
	 */
	public MainAccountTypes findByAccountTypeNameAndIsDeleted(String accountTypeName, boolean deleted);

}
