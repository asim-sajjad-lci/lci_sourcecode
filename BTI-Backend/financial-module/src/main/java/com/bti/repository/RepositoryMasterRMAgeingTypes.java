/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterRMAgeingTypes;

@Repository("repositoryMasterRMAgeingTypes")
public interface RepositoryMasterRMAgeingTypes extends JpaRepository<MasterRMAgeingTypes, Integer>{
	
	/**
	 * @param typeId
	 * @param langid
	 * @param deleted
	 * @return
	 */
	MasterRMAgeingTypes findByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId,int langid,boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterRMAgeingTypes> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterRMAgeingTypes> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
}
