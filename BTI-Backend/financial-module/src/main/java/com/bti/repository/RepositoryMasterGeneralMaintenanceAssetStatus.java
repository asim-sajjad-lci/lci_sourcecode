/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterGeneralMaintenanceAssetStatus;


/**
 * Description: Interface for MasterGeneralMaintenanceAssetStatus 
 * Name of Project: BTI
 * Created on: August 30, 2017
 * Modified on: August 30, 2017 05:04:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMasterGeneralMaintenanceAssetStatus")
public interface RepositoryMasterGeneralMaintenanceAssetStatus extends JpaRepository<MasterGeneralMaintenanceAssetStatus, Integer> {
	
	

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterGeneralMaintenanceAssetStatus> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	MasterGeneralMaintenanceAssetStatus findByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId, int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterGeneralMaintenanceAssetStatus> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
	
}
