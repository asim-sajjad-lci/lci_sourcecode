/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bti.model.PaymentTermsSetup;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryPaymentTermSetup")
public interface RepositoryPaymentTermSetup extends JpaRepository<PaymentTermsSetup, Integer> {

	/**
	 * @param paymentTermId
	 * @return
	 */
	PaymentTermsSetup findByPaymentTermIdAndIsDeleted(String paymentTermId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select pts from PaymentTermsSetup pts where pts.isDeleted=false and pts.paymentTermId like %:searchKeyWord% or pts.paymentDescription like %:searchKeyWord% or pts.paymentDescriptionArabic like %:searchKeyWord% or pts.masterDueTypes.dueType like %:searchKeyWord% ")
	public List<PaymentTermsSetup> predictivePaymentTermSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select pts from PaymentTermsSetup pts where pts.isDeleted=false and pts.paymentTermId like %:searchKeyWord% or pts.paymentDescription like %:searchKeyWord% or pts.paymentDescriptionArabic like %:searchKeyWord% or pts.masterDueTypes.dueType like %:searchKeyWord% ")
	public List<PaymentTermsSetup> predictivePaymentTermSearch(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PaymentTermsSetup pts where pts.isDeleted=false and pts.paymentTermId like %:searchKeyWord% or pts.paymentDescription like %:searchKeyWord% or pts.paymentDescriptionArabic like %:searchKeyWord% or pts.masterDueTypes.dueType like %:searchKeyWord% ")
	public int predictivePaymentTermSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param paymentTermIdList
	 * @param deleted
	 * @param changeBy
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update PaymentTermsSetup d set d.isDeleted =:deleted, d.changeBy=:changeBy where d.paymentTermId IN (:paymentTermIdList)")
	void deleteMultiplePaymentTermSetup(@Param("paymentTermIdList") List<String> paymentTermIdList, @Param("deleted") boolean deleted,
			@Param("changeBy") Integer changeBy);
	
}
