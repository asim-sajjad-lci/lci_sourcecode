/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationDefinePeriodsDetails;

/**
 * Description: repository GL Configuration Define Periods Details
 * Name of Project: BTI
 * Created on: Aug 23, 2017
 * Modified on: Aug 23, 2017 02:35:38 AM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLConfigurationDefinePeriodsDetails")
public interface RepositoryGLConfigurationDefinePeriodsDetails extends JpaRepository<GLConfigurationDefinePeriodsDetails, Integer>{

	/**
	 * @param glConfigurationDefinePeriodsHeader
	 * @return
	 */
	List<GLConfigurationDefinePeriodsDetails> findByGlConfigurationDefinePeriodsHeaderYearAndIsDeleted(
			int headerYear,boolean isDeleted);
	
	/**
	 * @param glConfigurationDefinePeriodsHeader
	 * @param periodName
	 * @return
	 */
	GLConfigurationDefinePeriodsDetails findByGlConfigurationDefinePeriodsHeaderYearAndPeriodNameAndIsDeleted(
			Integer year, String periodName, boolean deleted);

	/**
	 * @param glConfigurationDefinePeriodsHeader
	 * @param startPeriodId
	 * @param endPeriodId
	 * @return
	 */
	List<GLConfigurationDefinePeriodsDetails> findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodIdBetween(
			int headerYear, boolean deleted ,  Integer startPeriodId,
			Integer endPeriodId);

	
	GLConfigurationDefinePeriodsDetails findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodId(
			int headerYear, boolean deleted ,  Integer periodId);
	
	@Query("select d from GLConfigurationDefinePeriodsDetails d where d.glConfigurationDefinePeriodsHeader.year=:year and "
			+ "d.periodSeriesFinancial=:financialAccess and  "
			+ "( DATE(:transactionDate) between d.startPeriodDate and d.endPeriodDate ) ")
	List<GLConfigurationDefinePeriodsDetails> getByTransactionDateAndYearAndPeriodSeriesFinancial(
			@Param("transactionDate") Date transactionDate, @Param("year") int year, @Param("financialAccess") boolean financialAccess);
	
	@Query("select d from GLConfigurationDefinePeriodsDetails d where d.glConfigurationDefinePeriodsHeader.year=:year and "
			+ "d.periodSeriesSales=:salesAccess and  "
			+ "( DATE(:transactionDate) between d.startPeriodDate and d.endPeriodDate ) ")
	List<GLConfigurationDefinePeriodsDetails> getByTransactionDateAndYearAndPeriodSeriesSales(
			@Param("transactionDate") Date transactionDate, @Param("year") int year, @Param("salesAccess") boolean salesAccess);
	
	@Query("select d from GLConfigurationDefinePeriodsDetails d where d.glConfigurationDefinePeriodsHeader.year=:year and "
			+ " d.periodSeriesPurchase=:purchaseAccess and "
			+ " ( DATE(:transactionDate) between d.startPeriodDate and d.endPeriodDate ) ")
	List<GLConfigurationDefinePeriodsDetails> getByTransactionDateAndYearAndPeriodSeriesPurchase(
			@Param("transactionDate") Date transactionDate, @Param("year") int year, @Param("purchaseAccess") boolean purchaseAccess);

}
