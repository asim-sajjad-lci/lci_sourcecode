/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterAccountType;

/**
 * Description: Interface for MasterAccountType 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMasterAccountType")
public interface RepositoryMasterAccountType extends JpaRepository<MasterAccountType, Integer> {
	
	/**
	 * @param typeId
	 * @param languageId
	 * @param deleted
	 * @return
	 */
	MasterAccountType findByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId , int languageId,boolean deleted);
	
	/**
	 * @param languageId
	 * @param deleted
	 * @return
	 */
	List<MasterAccountType> findByLanguageLanguageIdAndIsDeleted(int languageId,boolean deleted);
	
	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterAccountType> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
	
	
}
