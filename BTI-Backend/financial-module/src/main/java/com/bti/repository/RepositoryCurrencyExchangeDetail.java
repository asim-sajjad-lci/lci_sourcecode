/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CurrencyExchangeDetail;

/**
 * Description: Interface for Currency exchange detail entity 
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCurrencyExchangeDetail")
public interface RepositoryCurrencyExchangeDetail extends JpaRepository<CurrencyExchangeDetail, Integer> {

	/**
	 * @param exchangeId
	 * @return
	 */
	CurrencyExchangeDetail findByCurrencyExchangeHeaderExchangeIdAndIsDeleted(String exchangeId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from CurrencyExchangeDetail ced where ced.isDeleted=false and ced.currencyExchangeHeader.exchangeId like %:searchKeyWord% or ced.time like %:searchKeyWord% or ced.exchangeRate like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select ced from CurrencyExchangeDetail ced where ced.isDeleted=false and ced.currencyExchangeHeader.exchangeId like %:searchKeyWord% or ced.time like %:searchKeyWord% or ced.exchangeRate like %:searchKeyWord%  ")
	public List<CurrencyExchangeDetail> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select ced from CurrencyExchangeDetail ced where ced.isDeleted=false and ced.currencyExchangeHeader.exchangeId like %:searchKeyWord% or ced.time like %:searchKeyWord% or ced.exchangeRate like %:searchKeyWord% ")
	public List<CurrencyExchangeDetail> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
}


