/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLBankTransferHeader;

@Repository("repositoryGLBankTransferHeader")
public interface RepositoryGLBankTransferHeader extends JpaRepository<GLBankTransferHeader, Integer>{

	GLBankTransferHeader findByBankTransferNumber(int bankTransferNumber);
    
	@Query("select count(*),COALESCE(SUM(bth.transferAmountFrom), 0) from GLBankTransferHeader bth where bth.glBatches.batchID=:batchID ")
	List<Object[]> getTotaldebitAmountAndTotaCountByBatchId(@Param("batchID") String batchID );

	List<GLBankTransferHeader> findByGlBatchesBatchID(String batchId); 
	
}
