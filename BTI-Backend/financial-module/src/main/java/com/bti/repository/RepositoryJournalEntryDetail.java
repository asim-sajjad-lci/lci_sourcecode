/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.JournalEntryDetails;

/**
 * Description: Interface for JournalEntryDetails 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:45:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryJournalEntryDetail")
public interface RepositoryJournalEntryDetail extends JpaRepository<JournalEntryDetails,Integer>{
	
	/**
	 * @param journalId
	 * @param deleted
	 * @return
	 */
	List<JournalEntryDetails> findByJournalEntryHeaderJournalIDAndIsDeleted(String journalId, boolean deleted);
	
	/**
	 * @param journalId
	 * @param deleted
	 * @return
	 */
	List<JournalEntryDetails> findByJournalEntryHeaderJournalIDAndJournalEntryHeaderGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(String journalId,int sourceDocument, boolean deleted);
	
	/**
	 * @param ID
	 * @param deleted
	 * @return
	 */
	List<JournalEntryDetails> findByJournalEntryHeaderIdAndIsDeleted(int Id, boolean deleted);
	
	@Query("select j from JournalEntryDetails j where j.journalEntryHeader.id=:journalId  and j.isDeleted=:isDelete")
	List<JournalEntryDetails> findByJournalEntryHeaderId(@Param("journalId")int journalId, @Param("isDelete")boolean deleted);
	/**
	 * @param JournalHeaderId
	 */
	@Modifying
	@Transactional
	@Query("delete from JournalEntryDetails jed where jed.journalEntryHeader.id=:JournalHeaderId")
	public void hardDeleteByJournalHeaderId(@Param("JournalHeaderId") Integer JournalHeaderId);
}
