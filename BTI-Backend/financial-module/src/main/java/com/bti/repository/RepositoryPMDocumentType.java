/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.model.PMDocumentsTypeSetup;

/**
 * Description: Interface for PMDocumentsTypeSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryPMDocumentType")
public interface RepositoryPMDocumentType extends JpaRepository<PMDocumentsTypeSetup, Integer> {
	
	/**
	 * @param docTypeId
	 * @param deleted
	 * @return
	 */
	PMDocumentsTypeSetup findByApDocumentTypeIdAndIsDeleted(int docTypeId,boolean deleted);
	/**
	 * @return
	 */
	@Query("select count(*) from PMDocumentsTypeSetup pm where pm.isDeleted=false ")
	public int countAll();
	
	/**
	 * @param pageable
	 * @return
	 */
	@Query("select pm from PMDocumentsTypeSetup pm where pm.isDeleted=false ")
	public List<PMDocumentsTypeSetup> getAllWithPagination(Pageable pageable);
	
	List<PMDocumentsTypeSetup> findByIsDeleted(boolean deleted);
	
	PMDocumentsTypeSetup findByDocumentCodeAndIsDeleted(String docCode,boolean deleted);
}
