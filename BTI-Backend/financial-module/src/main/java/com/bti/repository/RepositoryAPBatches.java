/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.APBatches;

/**
 * Description: Interface for APBatches 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryAPBatches")
public interface RepositoryAPBatches extends JpaRepository<APBatches,String>{
	

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from APBatches apb where apb.isDeleted=false and "
			+ "apb.batchID like %:searchKeyWord% or apb.batchDescription like %:searchKeyWord% "
			+ "or apb.batchTransactionType.transactionType like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);

	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select apb from APBatches apb where apb.isDeleted=false and "
			+ "apb.batchID like %:searchKeyWord% or apb.batchDescription like %:searchKeyWord% "
			+ "or apb.batchTransactionType.transactionType like %:searchKeyWord% ")
	public List<APBatches> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select apb from APBatches apb where apb.isDeleted=false and "
			+ "apb.batchID like %:searchKeyWord% or apb.batchDescription like %:searchKeyWord% "
			+ "or apb.batchTransactionType.transactionType like %:searchKeyWord% ")
	List<APBatches> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

	APBatches findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(String batchId, Integer transactionTypeId,
			boolean b);

	List<APBatches> findByBatchTransactionTypeTypeIdAndIsDeleted(Integer transactionTypeId, boolean b);

	
}
