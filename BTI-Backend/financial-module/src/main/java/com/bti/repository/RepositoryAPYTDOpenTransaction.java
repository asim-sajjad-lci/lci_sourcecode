/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.APYTDOpenTransaction;
import com.bti.model.APYTDOpenTransactionKey;
import com.bti.model.ARYTDOpenTransactions;

/**
 * Description: Interface for APYTDOpenTransaction 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryAPYTDOpenTransaction")
public interface RepositoryAPYTDOpenTransaction extends JpaRepository<APYTDOpenTransaction,APYTDOpenTransactionKey>{

	List<APYTDOpenTransaction> findByVendorID(String vendorID);
	
	List<APYTDOpenTransaction> findByVendorID(String vendorID,Pageable pageable);
	
	@Query("select count(*) from APYTDOpenTransaction apytd where apytd.vendorID=:vendorID")
	public int getCountByVendorId(@Param("vendorID") String vendorID);
	
	@Query("select count(*) from APYTDOpenTransaction apytd where DATE(apytd.postingDate) = :postingDate and apytd.vendorID=:vendorID")
	public int getCountByPostingDateAndVendorId(@Param("postingDate")  Date postingDate, @Param("vendorID") String vendorID);
	
	
	@Query("select apytd from APYTDOpenTransaction apytd where DATE(apytd.postingDate) = :postingDate and apytd.vendorID=:vendorID")
	List<APYTDOpenTransaction> findByPostingDateAndVendorId(@Param("postingDate") Date postingDate, @Param("vendorID") String vendorID);

	@Query("select apytd from APYTDOpenTransaction apytd where DATE(apytd.postingDate) = :postingDate and apytd.vendorID=:vendorID")
	List<APYTDOpenTransaction> findByPostingDateAndVendorId(@Param("postingDate") Date postingDate, @Param("vendorID") String vendorID, 
			Pageable pageable);
	
	APYTDOpenTransaction findByCashReceiptNumber(String cashReceiptNumber);

	List<APYTDOpenTransaction> findByVendorIDAndIsPaymentAndPurchasesAmountGreaterThan(
			String vendorID, boolean isPayment, double greterThan);
	
	@Query("select apytd.apTransactionNumber from APYTDOpenTransaction apytd where apytd.apTransactionNumber like '%INV%' and "
			+ " apytd.apTransactionDate between DATE(:startDate) and DATE(:endDate) ")
	List<String> findByApTransactionDateBetween(@Param("startDate") Date startDate,@Param("endDate") Date endDate);
}
