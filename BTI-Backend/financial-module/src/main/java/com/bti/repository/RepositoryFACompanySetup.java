/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FACompanySetup;

/**
 * Description: Interface for FA Company Setup
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFACompanySetup")
public interface RepositoryFACompanySetup extends JpaRepository<FACompanySetup, Integer> {
	
	/**
	 * @param bookId
	 * @return
	 */
	FACompanySetup findByBookSetupBookIdAndIsDeleted(String bookId, boolean deleted);
	
	/**
	 * @param bookIndexId
	 * @return
	 */
	FACompanySetup findByBookSetupBookInxdAndIsDeleted(Integer bookIndexId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FACompanySetup cs where cs.isDeleted=false and cs.bookSetup.bookId like %:searchKeyWord%  ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select cs from FACompanySetup cs where cs.isDeleted=false and cs.bookSetup.bookId like %:searchKeyWord% ")
	public List<FACompanySetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select cs from FACompanySetup cs where cs.isDeleted=false and cs.bookSetup.bookId like %:searchKeyWord% ")
	public List<FACompanySetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

}
