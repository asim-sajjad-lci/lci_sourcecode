package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bti.model.GLYTDPaymentVoucherHeader;

public interface RepositoryGLYTDPaymentVoucherHeader extends JpaRepository<GLYTDPaymentVoucherHeader,Integer> {

}
