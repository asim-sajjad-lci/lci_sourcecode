package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.GLPaymentVoucherDetail;
import com.bti.model.GLPaymentVoucherHeader;

@Repository("repositoryPaymentVoucherDetail")
public interface RepositoryGLPaymentVoucherDetail extends JpaRepository<GLPaymentVoucherDetail, Integer> {

	public List<GLPaymentVoucherDetail> findByGlPaymentVoucherHeader(GLPaymentVoucherHeader glPaymentVoucherHeader);
}
