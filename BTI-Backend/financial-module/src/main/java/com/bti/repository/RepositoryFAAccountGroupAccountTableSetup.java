/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAAccountGroupAccountTableSetup;

/**
 * Description: Interface for FA Account Groups Setup
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFAAccountGroupAccountTableSetup")
public interface RepositoryFAAccountGroupAccountTableSetup extends JpaRepository<FAAccountGroupAccountTableSetup, Integer> {
	
	
	/**
	 * @param rowIndex
	 * @param deleted
	 * @return
	 */
	FAAccountGroupAccountTableSetup findByfaAccountTableRowIndexAndIsDeleted(String rowIndex , boolean deleted);
	
	/**
	 * @param faAccountGroupIndex
	 * @param deleted
	 * @return
	 */
	List<FAAccountGroupAccountTableSetup> findByfaAccountGroupsSetupFaAccountGroupIndexAndIsDeleted(int faAccountGroupIndex, boolean deleted);
	
	/**
	 * @param faAccountGroupIndex
	 * @param typeId
	 * @param deleted
	 * @return
	 */
	FAAccountGroupAccountTableSetup findByfaAccountGroupsSetupFaAccountGroupIndexAndAccountTypeTypeIdAndIsDeleted(int faAccountGroupIndex,int typeId, boolean deleted);
	
	/**
	 * @param faAccountGroupIndex
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("delete from FAAccountGroupAccountTableSetup ags where  ags.faAccountGroupsSetup.faAccountGroupIndex=:faAccountGroupIndex")
	public void deleteByAccountGroupIndex(@Param("faAccountGroupIndex") int faAccountGroupIndex);

}
