/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.VendorMaintenanceOptions;

@Repository("repositoryVendorMaintenanceOptions")
public interface RepositoryVendorMaintenanceOptions extends JpaRepository<VendorMaintenanceOptions, String> {

 /**
 * @param vendorId
 * @param deleted
 * @return
 */
 public VendorMaintenanceOptions findByVendorMaintenanceVendoridAndIsDeleted(String vendorId,boolean deleted);
 
 /**
 * @param deleted
 * @return
 */
 public List<VendorMaintenanceOptions> findByIsDeleted(boolean deleted);
 
 /**
 * @param paymentTermId
 * @return
 */
 @Query("select count(*) from VendorMaintenanceOptions c where c.isDeleted=false and c.paymentTermsSetup.paymentTermId=:paymentTermId ")
 public Integer getCountByPaymentTermsSetupPaymentTermId(@Param("paymentTermId") String paymentTermId);

 /**
 * @param shipmentMethodId
 * @return
 */
 @Query("select count(*) from VendorMaintenanceOptions c where c.isDeleted=false and c.shipmentMethodSetup.shipmentMethodId=:shipmentMethodId ")
 public Integer getCountByShipmentMethodSetup(@Param("shipmentMethodId") String shipmentMethodId);

}
