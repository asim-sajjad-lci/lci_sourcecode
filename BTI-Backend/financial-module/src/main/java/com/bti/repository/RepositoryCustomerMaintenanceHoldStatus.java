/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.CustomerMaintenanceHoldStatus;

/**
 * Description: Interface for RepositoryCustomerMaintenance 
 * Name of Project: BTI
 * Created on: Aug 08, 2017
 * Modified on: Aug 08, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCustomerMaintenanceHoldStatus")
public interface RepositoryCustomerMaintenanceHoldStatus extends JpaRepository<CustomerMaintenanceHoldStatus, Integer> {

	
	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	CustomerMaintenanceHoldStatus findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId,int langId,boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<CustomerMaintenanceHoldStatus> findByLanguageLanguageIdAndIsDeleted(int langId, boolean deleted);
	 
	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<CustomerMaintenanceHoldStatus> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
	 
}


