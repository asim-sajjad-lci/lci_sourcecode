/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationSetup;

@Repository("repositoryGLConfigurationSetup")
public interface RepositoryGLConfigurationSetup extends JpaRepository<GLConfigurationSetup, Integer>{

	/**
	 * @return
	 */
	GLConfigurationSetup findTop1ByOrderByIdDesc();
	
}
