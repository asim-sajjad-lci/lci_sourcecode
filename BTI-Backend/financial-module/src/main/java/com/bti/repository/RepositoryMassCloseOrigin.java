/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.AccountCurrencyAccess;
import com.bti.model.MasterMassCloseOrigin;

/**
 * Description: Interface for RepositoryCurrencySetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMassCloseOrigin")
public interface RepositoryMassCloseOrigin extends JpaRepository<MasterMassCloseOrigin,Integer>{
	
	List<MasterMassCloseOrigin> findBySeriesTypeTypeIdAndLanguageLanguageId(int serieTypeId,int langId);
	
	//List<MasterMassCloseOrigin> findBySeriesTypeTypePrimaryAndLanguageLanguageId(String seriesName,int langId);
	
	MasterMassCloseOrigin findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(int originTypeId,int langId,int seriesTypeId);
	
}
