/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterNegativeSymbolTypes;

/**
 * Description: Interface for MasterNegativeSymbolTypes 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMasterNegativeSymbolTypes")
public interface RepositoryMasterNegativeSymbolTypes extends JpaRepository<MasterNegativeSymbolTypes, Integer> {
	
	

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterNegativeSymbolTypes> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	MasterNegativeSymbolTypes findByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId, int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterNegativeSymbolTypes> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
	
}
