/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterAPManualPaymentType;

/**
 * Description: Interface for RepositoryMasterAPManualPaymentType 
 * Name of Project: BTI
 * Created on: Dec 11, 2017
 * Modified on: Dec 11, 2017 4:19:38 PM
 * @author Dev
 * Version: 
 */
@Repository("repositoryMasterAPManualPaymentType")
public interface RepositoryMasterAPManualPaymentType extends JpaRepository<MasterAPManualPaymentType, Integer> {

	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	MasterAPManualPaymentType findByTypeIdAndLanguageLanguageIdAndIsDeleted(Integer typeId, int langId,boolean deleted);

	/**
	 * @param b
	 * @param langId
	 * @return
	 */
	List<MasterAPManualPaymentType> findByIsDeletedAndLanguageLanguageId(boolean b, int langId);
	
}


