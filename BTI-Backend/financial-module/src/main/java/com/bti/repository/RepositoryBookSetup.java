/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.BookSetup;

/**
 * Description: Interface for Book Setup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryBookSetup")
public interface RepositoryBookSetup extends JpaRepository<BookSetup, Integer> {
	
	/**
	 * @param bookId
	 * @return
	 */
	BookSetup findByBookIdAndIsDeleted(String bookId,boolean deleted);
	
	/**
	 * @param bookId
	 * @param bookIndexId
	 * @return
	 */
	@Query("select bs from BookSetup bs where bs.isDeleted=false and bs.bookId=:bookId and bs.bookInxd!=:bookIndexId ")
	BookSetup findByBookIdAndBookInxdNotEqual(@Param("bookId") String bookId, @Param("bookIndexId") Integer bookIndexId);
	
	/**
	 * @param bookIndexId
	 * @return
	 */
	BookSetup findByBookInxdAndIsDeleted(Integer bookIndexId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from BookSetup bs where bs.isDeleted=false and bs.bookId like %:searchKeyWord% or bs.bookDescription like %:searchKeyWord% or bs.bookDescriptionArabic like %:searchKeyWord% or bs.masterDepreciationPeriodTypes.depreciationPeriodType like %:searchKeyWord% or bs.currentYear like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select bs from BookSetup bs where bs.isDeleted=false and bs.bookId like %:searchKeyWord% or bs.bookDescription like %:searchKeyWord% or bs.bookDescriptionArabic like %:searchKeyWord% or bs.masterDepreciationPeriodTypes.depreciationPeriodType like %:searchKeyWord% or bs.currentYear like %:searchKeyWord% ")
	public List<BookSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select bs from BookSetup bs where bs.isDeleted=false and bs.bookId like %:searchKeyWord% or bs.bookDescription like %:searchKeyWord% or bs.bookDescriptionArabic like %:searchKeyWord% or bs.masterDepreciationPeriodTypes.depreciationPeriodType like %:searchKeyWord% or bs.currentYear like %:searchKeyWord% ")
	public List<BookSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

}
