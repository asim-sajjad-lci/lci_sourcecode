/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CheckbookMaintenance;

/**
 * Description: Interface for RepositoryCheckBookMaintenance 
 * Name of Project: BTI
 * Created on: August 24, 2017
 * Modified on: August 24, 2017 10:36:38 AM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCheckBookMaintenance")
public interface RepositoryCheckBookMaintenance extends JpaRepository<CheckbookMaintenance, Integer> {
	
	/**
	 * @param checkBookId
	 * @return
	 */
	CheckbookMaintenance findByCheckBookIdAndIsDeleted(String checkBookId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from CheckbookMaintenance ch where ch.isDeleted=false and ch.checkBookId like %:searchKeyWord% or ch.checkbookDescription like %:searchKeyWord% or ch.checkbookDescriptionArabic like %:searchKeyWord% or ch.currencySetup.currencyId like %:searchKeyWord% or ch.bankSetup.bankId like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select ch from CheckbookMaintenance ch where ch.isDeleted=false and ch.checkBookId like %:searchKeyWord% or ch.checkbookDescription like %:searchKeyWord% or ch.checkbookDescriptionArabic like %:searchKeyWord% or ch.currencySetup.currencyId like %:searchKeyWord% or ch.bankSetup.bankId like %:searchKeyWord% ")
	public List<CheckbookMaintenance> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select ch from CheckbookMaintenance ch where ch.isDeleted=false and ch.checkBookId like %:searchKeyWord% or ch.checkbookDescription like %:searchKeyWord% or ch.checkbookDescriptionArabic like %:searchKeyWord% or ch.currencySetup.currencyId like %:searchKeyWord% or ch.bankSetup.bankId like %:searchKeyWord% ")
	public List<CheckbookMaintenance> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

	
	@Query("select count(*) from CheckbookMaintenance c where c.isDeleted=false and c.bankSetup.bankId=:bankId ")
	public Integer getCountByBankSetupBankId(@Param("bankId") String bankId);
	
	@Query("select count(*) from CheckbookMaintenance ch where ch.isDeleted=false and"
			+ " ch.inactive=false and ( ch.checkBookId like %:searchKeyWord% or ch.checkbookDescription "
			+ " like %:searchKeyWord% or ch.checkbookDescriptionArabic "
			+ " like %:searchKeyWord% or ch.currencySetup.currencyId "
			+ " like %:searchKeyWord% or ch.bankSetup.bankId like %:searchKeyWord% )")
	public int predictiveActiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select ch from CheckbookMaintenance ch where ch.isDeleted=false "
			+ "and ch.inactive=false and (ch.checkBookId like %:searchKeyWord% or "
			+ "ch.checkbookDescription like %:searchKeyWord% or "
			+ "ch.checkbookDescriptionArabic like %:searchKeyWord% or "
			+ "ch.currencySetup.currencyId like %:searchKeyWord% or "
			+ "ch.bankSetup.bankId like %:searchKeyWord% )")
	public List<CheckbookMaintenance> predictiveActiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	@Query("select ch from CheckbookMaintenance ch where ch.isDeleted=false and "
			+ " ch.inactive=false and (ch.checkBookId like %:searchKeyWord% or "
			+ "ch.checkbookDescription like %:searchKeyWord% or "
			+ "ch.checkbookDescriptionArabic like %:searchKeyWord% or "
			+ "ch.currencySetup.currencyId like %:searchKeyWord% or "
			+ "ch.bankSetup.bankId like %:searchKeyWord% ) ")
	public List<CheckbookMaintenance> predictiveActiveSearch(@Param("searchKeyWord") String searchKeyWord);

}
