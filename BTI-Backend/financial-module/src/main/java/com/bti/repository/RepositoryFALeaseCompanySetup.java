/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.bti.model.FALeaseCompanySetup;

@Repository("repositoryFALeaseCompanySetup")
public interface RepositoryFALeaseCompanySetup extends JpaRepository<FALeaseCompanySetup, Integer>{

	/**
	 * @return
	 */
	@Query("select count(*) from  FALeaseCompanySetup falcs where falcs.isDeleted=false")
	Integer countAll();
}
