/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CustomerMaintenance;

/**
 * Description: Interface for RepositoryCustomerMaintenance 
 * Name of Project: BTI
 * Created on: Aug 08, 2017
 * Modified on: Aug 08, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCustomerMaintenance")
public interface RepositoryCustomerMaintenance extends JpaRepository<CustomerMaintenance, Integer> {

	/**
	 * @param customerId
	 * @return
	 */
	CustomerMaintenance findByCustnumberAndIsDeleted(String customerId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select cust from CustomerMaintenance cust where cust.isDeleted=false and cust.custnumber like %:searchKeyWord% or cust.customerName like %:searchKeyWord% or cust.customerNameArabic like %:searchKeyWord% or cust.customerClassesSetup.customerClassId like %:searchKeyWord% or cust.customerPriority like %:searchKeyWord% ")
	List<CustomerMaintenance> predictiveMaintenanceSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from CustomerMaintenance cust where cust.isDeleted=false and cust.custnumber like %:searchKeyWord% or cust.customerName like %:searchKeyWord% or cust.customerNameArabic like %:searchKeyWord% or cust.customerClassesSetup.customerClassId like %:searchKeyWord% or cust.customerPriority like %:searchKeyWord% ")
	public int predictiveMaintenanceSearchCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select cust from CustomerMaintenance cust where cust.isDeleted=false and cust.custnumber like %:searchKeyWord% or cust.customerName like %:searchKeyWord% or cust.customerNameArabic like %:searchKeyWord% or cust.customerClassesSetup.customerClassId like %:searchKeyWord% or cust.customerPriority like %:searchKeyWord% ")
	List<CustomerMaintenance> predictiveMaintenanceSearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param custnumber
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("delete CustomerMaintenance cust where cust.custnumber = :custnumber)")
	void deleteQuery(@Param("custnumber") String custnumber);
	 
	
	@Query("select count(*) from CustomerMaintenance cust where cust.isDeleted=false and "
			+ "customerActiveStatus=1 and customerHoldStatus=0 and "
			+ "( cust.custnumber like %:searchKeyWord% or "
			+ "cust.customerName like %:searchKeyWord% or "
			+ "cust.customerNameArabic like %:searchKeyWord% or "
			+ "cust.customerClassesSetup.customerClassId like %:searchKeyWord% or "
			+ "cust.customerPriority like %:searchKeyWord% ) ")
	public int predictiveActiveMaintenanceSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select cust from CustomerMaintenance cust where "
			+ " cust.isDeleted=false and customerActiveStatus=1 and customerHoldStatus=0 and "
			+ " ( cust.custnumber like %:searchKeyWord% or "
			+ "cust.customerName like %:searchKeyWord% or "
			+ "cust.customerNameArabic like %:searchKeyWord% or "
			+ "cust.customerClassesSetup.customerClassId like %:searchKeyWord% or "
			+ "cust.customerPriority like %:searchKeyWord% )")
	List<CustomerMaintenance> predictiveActiveMaintenanceSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	@Query("select cust from CustomerMaintenance cust where "
			+ "cust.isDeleted=false and customerActiveStatus=1 and customerHoldStatus=0 and "
			+ " (cust.custnumber like %:searchKeyWord% or "
			+ "cust.customerName like %:searchKeyWord% or "
			+ "cust.customerNameArabic like %:searchKeyWord% or "
			+ "cust.customerClassesSetup.customerClassId like %:searchKeyWord% or "
			+ "cust.customerPriority like %:searchKeyWord% ) ")
	List<CustomerMaintenance> predictiveActiveMaintenanceSearch(@Param("searchKeyWord") String searchKeyWord);
	
	 
}


