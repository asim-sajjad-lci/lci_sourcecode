/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.PMSetup;

/**
 * Description: Interface for RepositoryPMSetup 
 * Name of Project: BTI
 * Created on: Aug 28, 2017
 * Modified on: Aug 28, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryPMSetup")
public interface RepositoryPMSetup extends JpaRepository<PMSetup, Integer> {
	
	
	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	PMSetup findByIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from PMSetup pms where pms.isDeleted=false and pms.userDefine1 like %:searchKeyWord%  ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select pms from PMSetup pms where pms.isDeleted=false and pms.userDefine1 like %:searchKeyWord%  ")
	public List<PMSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select pms from PMSetup pms where pms.isDeleted=false and pms.userDefine1 like %:searchKeyWord%  ")
	public List<PMSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

	
}
