/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CustomerClassAccountTableSetup;

/**
 * Description: Interface for Customer Class Account Table Setup 
 * Name of Project: BTI
 * Created on: August 29 , 2017
 * Modified on: August 29 , 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCustomerClassAccountTableSetup")
public interface RepositoryCustomerClassAccountTableSetup extends JpaRepository<CustomerClassAccountTableSetup, Integer> {
	
	/**
	 * @param accountType
	 * @param customerClassId
	 * @param deleted
	 * @return
	 */
	CustomerClassAccountTableSetup findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(Integer accountType,String customerClassId);
	
	/**
	 * @param customerClassId
	 * @param deleted
	 * @return
	 */
	List<CustomerClassAccountTableSetup> findByCustomerClassesSetupCustomerClassIdAndIsDeleted(String customerClassId, boolean deleted);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update CustomerClassAccountTableSetup d set d.isDeleted =:deleted where d.accountTableArRowIndex IN (:ids)")
	void deleteRecordById(@Param("ids") List<Integer> ids, @Param("deleted") Boolean deleted);

	@Query("select d.accountTableArRowIndex from CustomerClassAccountTableSetup d where d.customerClassesSetup.customerClassId =:id")
	List<Integer> getByCustomerClassId(@Param("id") String customerClassId);
}
