/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAAccountTableSetup;

/**
 * Description: Interface for repositoryFAAccountTableSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFAAccountTableSetup")
public interface RepositoryFAAccountTableSetup extends JpaRepository<FAAccountTableSetup, Integer> {

	/**
	 * @param accountType
	 * @param id
	 * @return
	 */
	FAAccountTableSetup findByFaAccountTableAccountTypeTypeIdAndGlAccountsTableAccumulationIdAndIsDeleted(int accountType, int id,boolean deleted);
	
	/**
	 * @param accountType
	 * @return
	 */
	FAAccountTableSetup findByFaAccountTableAccountTypeTypeIdAndFaAccountGroupsSetupFaAccountGroupIndexAndFaGeneralMaintenanceAssetId(int accountType,int accountGroupIndex,String assetId);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update FAAccountTableSetup d set d.isDeleted =:deleted where d.faAccountGroupsSetup.faAccountGroupIndex=:id and d.faGeneralMaintenance.assetId=:assetId")
	void deleteRecordById(@Param("id") int id,@Param("assetId") String assetId, @Param("deleted") Boolean deleted);


	
}
