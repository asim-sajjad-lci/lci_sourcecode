/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bti.model.FAGeneralMaintenance;

/**
 * Description: Interface for FA General Maintenance 
 * Name of Project: BTI
 * Created on: August 30, 2017
 * Modified on: August 30, 2017 03:36:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFAGeneralMaintenance")
public interface RepositoryFAGeneralMaintenance extends JpaRepository<FAGeneralMaintenance, Integer> {
	
	/**
	 * @param assetId
	 * @return
	 */
	FAGeneralMaintenance findByAssetIdAndIsDeleted(String assetId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FAGeneralMaintenance gm where gm.isDeleted=false "
			+ " and ( gm.assetId  like  %:searchKeyWord%  or gm.FADescription "
			+ " like %:searchKeyWord% or gm.FADescriptionArabic like %:searchKeyWord% or "
			+ "gm.assetExtendedDescription like %:searchKeyWord% )")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select gm from FAGeneralMaintenance gm where gm.isDeleted=false and gm.assetId like %:searchKeyWord% or gm.FADescription like %:searchKeyWord% or gm.FADescriptionArabic like %:searchKeyWord% or gm.assetExtendedDescription like %:searchKeyWord% ")
	public List<FAGeneralMaintenance> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select gm from FAGeneralMaintenance gm where gm.isDeleted=false and gm.assetId like %:searchKeyWord% or gm.FADescription like %:searchKeyWord% or gm.FADescriptionArabic like %:searchKeyWord% or gm.assetExtendedDescription like %:searchKeyWord% ")
	public List<FAGeneralMaintenance> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

	public List<FAGeneralMaintenance> findByMasterGeneralMaintenanceAssetStatusTypeIdAndIsDeleted(int assetStatusTypeId, boolean deleted,Pageable pageable);
	
	public List<FAGeneralMaintenance> findByMasterGeneralMaintenanceAssetStatusTypeIdAndIsDeleted(int assetStatusTypeId, boolean deleted);

	@Query("select count(*) from FAGeneralMaintenance gm where gm.isDeleted=false and gm.masterGeneralMaintenanceAssetStatus.typeId=1")
	public int activeFaAssetCount();
}
