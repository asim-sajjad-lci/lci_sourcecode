/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLClearingDetails;

/**
 * Description: Interface for GLClearingDetails 
 * Name of Project: BTI
 * Created on: Dec 7, 2017
 * Modified on: Dec 7, 2017 5:30:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLClearingDetail")
public interface RepositoryGLClearingDetail extends JpaRepository<GLClearingDetails,Integer>{
	
	
	/**
	 * @param journalID
	 * @param isDeleted
	 * @return
	 */
	public List<GLClearingDetails> findByJournalIDAndIsDeleted(String journalID, boolean isDeleted);
	
	@Query("select cd.glAccountsTableAccumulation.accountTableRowIndex from GLClearingDetails cd where "
			+ " cd.isDeleted=false and  cd.journalID IN "
			+ " ( select gch.journalID from  GLClearingHeader gch where gch.isDeleted=false "
			+ " and gch.glBatches.batchID=:batchID ) ")
	List<String> getAccountNumberListByBatchId(@Param("batchID") String batchID);
	
	@Query("select cd.glAccountsTableAccumulationOffset.accountTableRowIndex from GLClearingDetails cd where "
			+ " cd.isDeleted=false and  cd.journalID IN "
			+ " ( select gch.journalID from  GLClearingHeader gch where gch.isDeleted=false "
			+ " and gch.glBatches.batchID=:batchID  ) ")
	List<String> getAccountNumberOffsetListByBatchId(@Param("batchID") String batchID);
	
}
