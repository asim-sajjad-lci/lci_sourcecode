/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CreditCardsSetup;

/**
 * Description: Interface for RepositoryCreditCardsSetup 
 * Name of Project: BTI
 * Created on: Aug 17, 2017
 * Modified on: Aug 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCreditCardsSetup")
public interface RepositoryCreditCardsSetup extends JpaRepository<CreditCardsSetup, Integer> {

	
	/**
	 * @param cardIndex
	 * @param deleted
	 * @return
	 */
	CreditCardsSetup findByCardIndxAndIsDeleted(int cardIndex, boolean isDeleted);
	
	/**
	 * @param cardId
	 * @param deleted
	 * @return
	 */
	CreditCardsSetup findByCardIdAndIsDeleted(String cardId, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from CreditCardsSetup ccs where ccs.isDeleted=false and ccs.cardId like %:searchKeyWord% or ccs.creditCardNameArabic like %:searchKeyWord% or ccs.checkBookIdBank like %:searchKeyWord%  ")
	public int predictiveCreditCardSetupSearchCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select ccs from CreditCardsSetup ccs where ccs.isDeleted=false and ccs.cardId like %:searchKeyWord% or ccs.creditCardNameArabic like %:searchKeyWord% or ccs.checkBookIdBank like %:searchKeyWord%  ")
	public List<CreditCardsSetup> predictiveCreditCardSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select ccs from CreditCardsSetup ccs where ccs.isDeleted=false and ccs.cardId like %:searchKeyWord%  or ccs.creditCardNameArabic like %:searchKeyWord% or ccs.checkBookIdBank like %:searchKeyWord%  ")
	public List<CreditCardsSetup> predictiveCreditCardSetupSearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @return
	 */
	CreditCardsSetup findTop1ByOrderByCardIdDesc();
	
	/**
	 * @param cardName
	 * @param isDeleted
	 * @return
	 */
	CreditCardsSetup findByCreditCardNameAndIsDeleted(String cardName, boolean isDeleted);
	
	/**
	 * @param paymentTermIdList
	 * @param deleted
	 * @param changeBy
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update CreditCardsSetup d set d.isDeleted =:deleted, d.changeBy=:changeBy where d.cardId IN (:cardIdList)")
	void deleteMultipleCreditCardsSetup(@Param("cardIdList") List<String> cardIdList, @Param("deleted") boolean deleted,
			@Param("changeBy") Integer changeBy);
	
}


