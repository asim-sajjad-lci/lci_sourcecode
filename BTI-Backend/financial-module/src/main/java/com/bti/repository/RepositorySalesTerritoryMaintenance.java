/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.SalesTerritoryMaintenance;

/**
 * Description: Interface for RepositorySalesTerritoryMaintenance 
 * Name of Project: BTI
 * Created on: Aug 24, 2017
 * Modified on: Aug 24, 2017 9:37:38 AM
 * @author seasia
 * Version: 
 */
@Repository("repositorySalesTerritoryMaintenance")
public interface RepositorySalesTerritoryMaintenance extends JpaRepository<SalesTerritoryMaintenance, String>{

	
	/**
	 * @param salesTerritoryId
	 * @param deleted
	 * @return
	 */
	SalesTerritoryMaintenance findBySalesTerritoryIdAndIsDeleted(String salesTerritoryId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select stm from SalesTerritoryMaintenance stm where stm.isDeleted=false and stm.salesTerritoryId like %:searchKeyWord% or stm.territoryDescription like %:searchKeyWord% or stm.territoryDescriptionArabic like %:searchKeyWord%")
	List<SalesTerritoryMaintenance> predictiveSalesTerritoryMaintenanceSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select stm from SalesTerritoryMaintenance stm where stm.isDeleted=false and stm.salesTerritoryId like %:searchKeyWord% or stm.territoryDescription like %:searchKeyWord% or stm.territoryDescriptionArabic like %:searchKeyWord%")
	List<SalesTerritoryMaintenance> predictiveSalesTerritoryMaintenanceSearch(@Param("searchKeyWord") String searchKeyWord);

	List<SalesTerritoryMaintenance> findByIsDeleted(boolean deleted);
}
