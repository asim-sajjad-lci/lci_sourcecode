/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FALeaseMaintenance;

/**
 * Description: ControllerFixedAssets
 * Name of Project: BTI
 * Created on: Sep 01, 2017
 * Modified on: Sep 01, 2017 11:08:38 AM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFALeaseMaintenance")
public interface RepositoryFALeaseMaintenance extends JpaRepository<FALeaseMaintenance, Integer> {

	
	/**
	 * @param leaseCompanyIndex
	 * @param deleted
	 * @return
	 */
	FALeaseMaintenance findByFaLeaseCompanySetupLeaseCompanyIndexAndIsDeleted(int leaseCompanyIndex , boolean deleted);
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FALeaseMaintenance gm where gm.isDeleted=false and gm.assetId like %:searchKeyWord% or  gm.leaseCompanyId like %:searchKeyWord% or gm.leaseContractNumber like %:searchKeyWord% or gm.leaseType.leaseTypeId like %:searchKeyWord% ")
	Integer predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select gm from FALeaseMaintenance gm where gm.isDeleted=false and gm.assetId like %:searchKeyWord% or  gm.leaseCompanyId like %:searchKeyWord% or gm.leaseContractNumber like %:searchKeyWord% or gm.leaseType.leaseTypeId like %:searchKeyWord% ")
	List<FALeaseMaintenance> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select gm from FALeaseMaintenance gm where gm.isDeleted=false and gm.assetId like %:searchKeyWord% or  gm.leaseCompanyId like %:searchKeyWord% or gm.leaseContractNumber like %:searchKeyWord% or gm.leaseType.leaseTypeId like %:searchKeyWord% ")
	List<FALeaseMaintenance> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

}
