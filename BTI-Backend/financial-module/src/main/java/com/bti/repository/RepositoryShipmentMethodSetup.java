/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ShipmentMethodSetup;

/**
 * Description: Interface for RepositoryShipmentMethodSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryShipmentMethodSetup")
public interface RepositoryShipmentMethodSetup extends JpaRepository<ShipmentMethodSetup, Integer> {
	
	/**
	 * @param shipmentMethodId
	 * @param deleted
	 * @return
	 */
	ShipmentMethodSetup findByShipmentMethodIdAndIsDeleted(String shipmentMethodId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from ShipmentMethodSetup sms where sms.isDeleted=false and sms.shipmentMethodId like %:searchKeyWord% or sms.shipmentDescription like %:searchKeyWord% or sms.shipmentDescriptionArabic like %:searchKeyWord% or sms.shipmentCarrier like %:searchKeyWord% ")
	public int predictiveShipmentMethodSetupSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select sms from ShipmentMethodSetup sms where sms.isDeleted=false and sms.shipmentMethodId like %:searchKeyWord% or sms.shipmentDescription like %:searchKeyWord% or sms.shipmentDescriptionArabic like %:searchKeyWord% or sms.shipmentCarrier like %:searchKeyWord% ")
	public List<ShipmentMethodSetup> predictiveShipmentMethodSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select sms from ShipmentMethodSetup sms where sms.isDeleted=false and sms.shipmentMethodId like %:searchKeyWord% or sms.shipmentDescription like %:searchKeyWord% or sms.shipmentDescriptionArabic like %:searchKeyWord% or sms.shipmentCarrier like %:searchKeyWord% ")
	public List<ShipmentMethodSetup> predictiveShipmentMethodSetupSearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param shipmentMethodIdList
	 * @param deleted
	 * @param changeBy
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update ShipmentMethodSetup d set d.isDeleted =:deleted, d.changeBy=:changeBy where d.shipmentMethodId IN (:shipmentMethodIdList)")
	void deleteMultipleByShipMentMethodId(@Param("shipmentMethodIdList") List<String> shipmentMethodIdList, @Param("deleted") boolean deleted,
			@Param("changeBy") Integer changeBy);
	
}
