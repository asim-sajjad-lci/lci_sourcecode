/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationDefinePeriodsHeader;

/**
 * Description: repository GL Configuration Define Periods Header
 * Name of Project: BTI
 * Created on: Aug 23, 2017
 * Modified on: Aug 23, 2017 02:35:38 AM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLConfigurationDefinePeriodsHeader")
public interface RepositoryGLConfigurationDefinePeriodsHeader extends JpaRepository<GLConfigurationDefinePeriodsHeader, Integer>{

	
	/**
	 * @param year
	 * @param deleted
	 * @return
	 */
	GLConfigurationDefinePeriodsHeader findByYearAndIsDeleted(int year, boolean deleted);

	/**
	 * @return
	 */
	@Query("select glcdph.year from GLConfigurationDefinePeriodsHeader glcdph where glcdph.isDeleted=false ")
	List<Integer> findByAllYears();

}
