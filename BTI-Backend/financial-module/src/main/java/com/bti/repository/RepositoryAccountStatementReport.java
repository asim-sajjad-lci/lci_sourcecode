/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.trl_blnc_n_act_stmt_rpts_v;

@Repository("repositoryAccountStatementReport")
public interface RepositoryAccountStatementReport extends JpaRepository<trl_blnc_n_act_stmt_rpts_v, Long> {
	
/*
	@Query("select a.Journal_ID, a.Account_Number, a.Account_Description, a.Account_Categrory, a.Journal_Date, a.Distribution_Reference, a.DB, a.CB, a.History \r\n" + 
    		" from rptaccountstatement_lcl a \r\n" + 
    		" where a.Journal_Date between :startDate and :endDate \r\n" + 
    		" order by a.Journal_Date, Journal_ID")
   	List<Object[]> getAccountStatementsByDateRange(@Param("startDate") String startDate,
   			@Param("endDate") String endDate);
*/

   	
//	@Query("select a.JORNID, a.accountNumber, a.MainAccountDescription, a.AccountCategoryDescription, a.TRXDDT, a.MainAccountDescriptionArabic, a.DEBITAMT, a.CRDTAMT \r\n" + 
//    		" from rptaccountstatement_new a \r\n" + 
//    		" where a.TRXDDT between :startDate and :endDate \r\n" + 
//    		" order by a.TRXDDT, JORNID")
	@Query("select a.journal_id, a.account_number, a.main_account_description, a.account_category_description, a.transaction_date, a.distribution_reference, a.debit_amount, a.credit_amount \r\n" + 
    		" from trl_blnc_n_act_stmt_rpts_v a \r\n" + 
    		" where a.transaction_date between :startDate and :endDate \r\n" + 
    		" order by a.transaction_date, journal_id")
	List<Object[]> getAccountStatementsByDateRange(@Param("startDate") String startDate,
   			@Param("endDate") String endDate);
   	
  /*
  	@Query("select a.Journal_ID, a.Account_Number, a.Account_Description, a.Account_Categrory, a.Journal_Date, a.Distribution_Reference, a.DB, a.CB, a.History \r\n" + 
      		" from rptaccountstatement_lcl a \r\n" + 
      		" where date(a.Journal_Date) >= :startDate and date(a.Journal_Date) <= :endDate \r\n" + 
      		" order by a.Journal_Date, Journal_ID")
     	List<Object[]> getAccountStatementsByDateRange(@Param("startDate") Date startDate,
     			@Param("endDate") Date endDate);
  */
   	
   	
	/*@Query("select a.Account_Number, a.Account_Description, a.Account_Categrory\r\n" + 
    		" from rptaccountstatement_lcl a \r\n" + 
    		" where a.Journal_Date between :startDate and :endDate \r\n" + 
    		" order by a.Journal_Date, Journal_ID")*/
//	@Query("select a.journal_id, a.account_number, a.main_account_description, a.account_category_description, a.transaction_date, a.main_account_description_arabic, a.debit_amount, a.credit_amount \r\n" + 
//    		" from trl_blnc_n_act_stmt_rpts_v a \r\n" + 
//    		" where a.transaction_date between :startDate and :endDate \r\n" + 
//    		" order by a.transaction_date, journal_id")
//   	List<Object[]> getAccountNumberByDateRange(@Param("startDate") String startDate,
//   			@Param("endDate") String endDate);
   	
//   	@Query("select a.account_number, sum( (case when (transaction_date < :startDate) then (debit_amount - credit_amount) else 0 end)) AS prev_bal \r\n" + 
//   			"from trl_blnc_n_act_stmt_rpts_v a\r\n" + 
//   			"group by a.account_number")
//   	List<Object[]> getPreviousBalanceForAccountNumberBeforeDate(@Param("startDate") String startDate);
   	
/*   
    @Query("select a.Journal_ID, a.Account_Number, a.Account_Description, a.Account_Categrory, a.Journal_Date, a.Distribution_Reference, a.DB, a.CB, a.History \r\n" + 
    		" from rptaccountstatement_lcl a \r\n" + 
    		" where a.Journal_Date between :startDate and :endDate \r\n" + 
    		" order by a.Journal_Date, Journal_ID")
	public List<rptaccountstatement_lcl> getAccountStatementsByDateRange(@Param("startDate") String startDate,
   			@Param("endDate") String endDate);
  */
   	
   	/*
   	    @Query("select a.Journal_ID, a.Account_Number, a.Account_Description, a.Account_Categrory, a.Journal_Date, a.Distribution_Reference, a.DB, a.CB, a.History \r\n" + 
    		" from rptaccountstatement_lcl a \r\n" + 
    		" where a.journal_date between :startDate and :endDate \r\n" + 
    		" order by a.Journal_date, a.Journal_id")
   	List<Object[]> getAccountStatementsByDateRange(@Param("startDate") Date startDate,
   			@Param("endDate") Date endDate);


   	*/
}
