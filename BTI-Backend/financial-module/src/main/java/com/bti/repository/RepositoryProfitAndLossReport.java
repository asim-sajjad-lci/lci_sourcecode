package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bti.model.ReportProfitAndLoss;

public interface RepositoryProfitAndLossReport extends JpaRepository<ReportProfitAndLoss, Integer> {

	@Query("SELECT RPAL FROM ReportProfitAndLoss RPAL WHERE RPAL.AccountCategoryIndex IN :index and RPAL.period <= :period")
	public List<ReportProfitAndLoss> findByAccountCategoryIndex(@Param("index") List<Integer> ids ,@Param("period") int period );
	
}
