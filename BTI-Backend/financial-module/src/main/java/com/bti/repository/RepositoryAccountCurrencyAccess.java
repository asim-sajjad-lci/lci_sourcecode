/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.AccountCurrencyAccess;

/**
 * Description: Interface for RepositoryCurrencySetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryAccountCurrencyAccess")
public interface RepositoryAccountCurrencyAccess extends JpaRepository<AccountCurrencyAccess,String>{
	
	/**
	 * @param actIndx
	 * @return
	 */
	@Query("select aca.currencySetup.currencyId from AccountCurrencyAccess aca where aca.isDeleted=false and aca.coaMainAccounts.actIndx = :actIndx ")
	public List<String> getCurrecyIdByMainAccountActIndex(@Param("actIndx") Long actIndx);

 
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("delete AccountCurrencyAccess aca where aca.isDeleted=false and aca.coaMainAccounts.actIndx = :accountIndex")
	void deleteQuery(@Param("accountIndex") Integer accountIndex);

	public List<AccountCurrencyAccess> findByCoaMainAccountsActIndx(Long mainAccountActIndex);

}
