/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.TrialBalanceView;

@Repository("repositoryTrialBalanceReportNew")
public interface RepositoryTrialBalanceReportNew extends JpaRepository<TrialBalanceView, Long> {
	
	@Query("select v \r\n" + 
			"from TrialBalanceView v\r\n" + 
			"where v.trxDdt between :startDate and :endDate")
   	List<TrialBalanceView> getTrialBalanceReportByDateRange(@Param("startDate") Date startDate,
   			@Param("endDate") Date endDate);
}
