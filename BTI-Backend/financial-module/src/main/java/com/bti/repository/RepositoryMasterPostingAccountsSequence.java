/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterPostingAccountsSequence;

@Repository("repositoryMasterPostingAccountsSequence")
public interface RepositoryMasterPostingAccountsSequence extends JpaRepository<MasterPostingAccountsSequence, Integer> {

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from MasterPostingAccountsSequence mpas where mpas.isDeleted=false and mpas.postingAccountDescription like %:searchKeyWord% or mpas.postingAccountDescriptionArabic like %:searchKeyWord%")
	Integer predictivePostingAccountSearchCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select mpas from MasterPostingAccountsSequence mpas where mpas.isDeleted=false and mpas.postingAccountDescription like %:searchKeyWord% or mpas.postingAccountDescriptionArabic like %:searchKeyWord%")
	List<MasterPostingAccountsSequence> predictivePostingAccountSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select mpas from MasterPostingAccountsSequence mpas where mpas.isDeleted=false and mpas.postingAccountDescription like %:searchKeyWord% or mpas.postingAccountDescriptionArabic like %:searchKeyWord%")
	List<MasterPostingAccountsSequence> predictivePostingAccountSearch(@Param("searchKeyWord") String searchKeyWord);

	@Query("select count(*) from MasterPostingAccountsSequence c where c.isDeleted=false and c.series.moduleSeriesNumber=:moduleSeriesNumber ")
	public Integer getCountByModulesConfiguration(@Param("moduleSeriesNumber") Integer moduleSeriesNumber);

}
