/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.VATSetup;

/**
 * Description: Interface for RepositoryShipmentMethodSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryVATSetup")
public interface RepositoryVATSetup extends JpaRepository<VATSetup, Integer> {
	
	
	/**
	 * @param vatScheduleId
	 * @param deleted
	 * @return
	 */
	VATSetup findByVatScheduleIdAndIsDeleted(String vatScheduleId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from VATSetup vs where vs.isDeleted=false and vs.vatScheduleId like %:searchKeyWord% or vs.vatDescription like %:searchKeyWord% or vs.vatDescriptionArabic like %:searchKeyWord% or vs.vatType.typePrimary like %:searchKeyWord% ")
	public int predictiveVatSetupSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select vs from VATSetup vs where vs.isDeleted=false and vs.vatScheduleId like %:searchKeyWord% or vs.vatDescription like %:searchKeyWord% or vs.vatDescriptionArabic like %:searchKeyWord% or vs.vatType.typePrimary like %:searchKeyWord% ")
	public List<VATSetup> predictiveVatSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select vs from VATSetup vs where vs.isDeleted=false and vs.vatScheduleId like %:searchKeyWord% or vs.vatDescription like %:searchKeyWord% or vs.vatDescriptionArabic like %:searchKeyWord% or vs.vatType.typePrimary like %:searchKeyWord% ")
	public List<VATSetup> predictiveVatSetupSearch(@Param("searchKeyWord") String searchKeyWord);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update VATSetup d set d.isDeleted =:deleted, d.changeBy=:changeBy where d.vatScheduleId IN (:vatScheduleIdList)")
	void deleteMultipleByVATSetup(@Param("vatScheduleIdList") List<String> vatScheduleIdList, @Param("deleted") boolean deleted,
			@Param("changeBy") Integer changeBy);
	
}
