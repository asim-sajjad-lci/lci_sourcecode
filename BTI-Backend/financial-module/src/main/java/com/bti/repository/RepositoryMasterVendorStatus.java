/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterVendorStatus;

@Repository("repositoryMasterVendorStatus")
public interface RepositoryMasterVendorStatus extends JpaRepository<MasterVendorStatus, Integer>{

	
	/**
	 * @param deleted
	 * @return
	 */
	List<MasterVendorStatus> findByIsDeleted(boolean deleted);
	/**
	 * @param venderStatus
	 * @param langId
	 * @param deleted
	 * @return
	 */
	MasterVendorStatus findByStatusIdAndLanguageLanguageIdAndIsDeleted(short venderStatus,int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterVendorStatus> findByIsDeletedAndLanguageLanguageId(boolean b, int i);
	
	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);

}
