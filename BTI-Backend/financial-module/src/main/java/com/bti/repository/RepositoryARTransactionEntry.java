/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ARTransactionEntry;

/**
 * Description: Interface for ARTransactionEntry 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryARTransactionEntry")
public interface RepositoryARTransactionEntry extends JpaRepository<ARTransactionEntry,String>{

	ARTransactionEntry findByArTransactionNumber(String arTransactionNumber); 
	
	List<ARTransactionEntry> findByTransactionTypeArDocumentTypeId(int typeId);

	List<ARTransactionEntry> findByArBatchesBatchID(String batchId);

	@Query("select count(*),COALESCE(SUM(art.arTransactionTotalAmount), 0) from ARTransactionEntry art where art.arBatches.batchID=:batchId ")
	List<Object[]> getTotalAmountAndTotalCountByBatchId(@Param("batchId") String batchId);
	
}
