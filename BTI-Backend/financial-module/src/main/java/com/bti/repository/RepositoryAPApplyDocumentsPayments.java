/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ApplyDocumentsPayments;

/**
 * Description: Interface for ApplyDocumentsPayments 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryAPApplyDocumentsPayments")
public interface RepositoryAPApplyDocumentsPayments extends JpaRepository<ApplyDocumentsPayments,String>{
	
	
	ApplyDocumentsPayments findByApplyDocumentNumberPaymentNumberAndApTransactionNumber(String paymentNumber, String apTransactionNumber);

	@Query("select COALESCE(SUM(apdp.applyAmount), 0.0) from  ApplyDocumentsPayments apdp where apdp.apTransactionNumber=:apTransactionNumber")
	public double getApplyAmountByApTransactionNumber(@Param("apTransactionNumber") String apTransactionNumber);
	
	@Query("select COALESCE(SUM(apdp.applyAmount), 0.0) from  ApplyDocumentsPayments apdp where apdp.apTransactionNumber IN (:apTransactionNumberList) ")
	public Double getApplyAmountByApTransactionNumber(@Param("apTransactionNumberList") List<String> apTransactionNumberList);
}
