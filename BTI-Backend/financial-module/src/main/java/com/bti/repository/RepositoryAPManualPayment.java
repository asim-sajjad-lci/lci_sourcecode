/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.APManualPayment;

@Repository("repositoryAPManualPayment")
public interface RepositoryAPManualPayment extends JpaRepository<APManualPayment, Integer>{

	/**
	 * @param manualPaymentNumber
	 * @return
	 */
	APManualPayment findByManualPaymentNumber(String manualPaymentNumber);
	
	List<APManualPayment> findByApBatchesBatchID(String batchId);

	@Query("select count(*),COALESCE(SUM(mp.manualPaymentAmount), 0) from APManualPayment mp where mp.apBatches.batchID=:batchId ")
	List<Object[]> getTotalAmountAndTotalCountByBatchId(@Param("batchId") String batchId);

	

}
