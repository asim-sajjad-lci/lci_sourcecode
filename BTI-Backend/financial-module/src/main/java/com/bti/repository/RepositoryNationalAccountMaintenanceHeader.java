/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.NationalAccountMaintenanceHeader;

@Repository("repositoryNationalAccountMaintenanceHeader")
public interface RepositoryNationalAccountMaintenanceHeader extends JpaRepository<NationalAccountMaintenanceHeader, Integer>{

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select nam from NationalAccountMaintenanceHeader nam where nam.isDeleted=false and nam.customerName like %:searchKeyWord% or nam.customerNameArabic like %:searchKeyWord%  ")
	List<NationalAccountMaintenanceHeader> predictiveNationalAccountMaintenanceSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select nam from NationalAccountMaintenanceHeader nam where nam.isDeleted=false and nam.customerName like %:searchKeyWord% or nam.customerNameArabic like %:searchKeyWord% ")
	List<NationalAccountMaintenanceHeader> predictiveNationalAccountMaintenanceSearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @return
	 */
	@Query("select count(*) from NationalAccountMaintenanceHeader nam where nam.isDeleted=false")
	public int getAllNationalAccount();

	
	/**
	 * @param custNumber
	 * @param deleted
	 * @return
	 */
	NationalAccountMaintenanceHeader findByCustomerMaintenanceCustnumberAndIsDeleted(String custNumber, boolean deleted);

}
