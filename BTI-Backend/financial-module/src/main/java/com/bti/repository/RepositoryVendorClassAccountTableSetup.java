/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.VendorClassAccountTableSetup;

/**
 * Description: Interface for RepositoryVendorClassesSetup 
 * Name of Project: BTI
 * Created on: Aug 28, 2017
 * Modified on: Aug 28, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryVendorClassAccountTableSetup")
public interface RepositoryVendorClassAccountTableSetup extends JpaRepository<VendorClassAccountTableSetup, Integer> {
	

	/**
	 * @param accTableId
	 * @param deleted
	 * @return
	 */
	VendorClassAccountTableSetup findByAccTableIdAndIsDeleted(int accTableId,boolean deleted);

	/**
	 * @param accountType
	 * @param actRowId
	 * @param deleted
	 * @return
	 */
	VendorClassAccountTableSetup findByMasterAccountTypeTypeIdAndGlAccountsTableAccumulationIdAndIsDeleted(Integer accountType, int actRowId,boolean deleted);
	
	/**
	 * @param accountType
	 * @param deleted
	 * @return
	 */
	VendorClassAccountTableSetup findByMasterAccountTypeTypeIdAndIsDeleted(Integer accountType,boolean deleted);
	

	/**
	 * @param accountType
	 * @param vendorClassId
	 * @param deleted
	 * @return
	 */
	VendorClassAccountTableSetup findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(Integer accountType, String vendorClassId);
	
	/**
	 * @param vendorClassId
	 * @param deleted
	 * @return
	 */
	List<VendorClassAccountTableSetup> findByVendorClassesSetupVendorClassIdAndIsDeleted( String vendorClassId,boolean deleted);
	
	/**
	 * @param faAccountGroupIndex
	 */
	@Modifying
	@Transactional
	@Query("delete from FAAccountGroupAccountTableSetup ags where ags.isDeleted=false and ags.faAccountGroupsSetup.faAccountGroupIndex=:faAccountGroupIndex")
	public void deleteByAccountGroupIndex(@Param("faAccountGroupIndex") int faAccountGroupIndex);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update VendorClassAccountTableSetup d set d.isDeleted =:deleted where d.vendorClassesSetup.vendorClassId=:id")
	void deleteRecordById(@Param("id") String id, @Param("deleted") Boolean deleted);
 
	
	
	
}
