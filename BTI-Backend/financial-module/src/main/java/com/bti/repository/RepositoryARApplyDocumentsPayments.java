/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ARApplyDocumentsPayments;

/**
 * Description: Interface for ARApplyDocumentsPayments 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryARApplyDocumentsPayments")
public interface RepositoryARApplyDocumentsPayments extends JpaRepository<ARApplyDocumentsPayments,String>{
	
	//ARApplyDocumentsPayments findByCustomerNumberAndArTransactionNumber(String customerNumber, String transactionNumber);
	
    ARApplyDocumentsPayments findByApplyDocumentNumberPaymentNumberAndArTransactionNumber(String paymentNumber, String arTransactionNumber);
 
	@Query("select COALESCE(SUM(apdc.applyAmount), 0.0) from  ARApplyDocumentsPayments apdc where apdc.arTransactionNumber=:arTransactionNumber")
	public double getApplyAmountByArTransactionNumber(@Param("arTransactionNumber") String arTransactionNumber);
	
	@Query("select COALESCE(SUM(apdc.applyAmount),0.0) from  ARApplyDocumentsPayments apdc where apdc.arTransactionNumber IN (:arTransactionNumberList)")
	Double getApplyAmountofTransactionNumbers(@Param("arTransactionNumberList") List<String> arTransactionNumberList);
	 
}
