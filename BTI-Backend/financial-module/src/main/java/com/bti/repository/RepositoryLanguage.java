/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.Language;

/**
 * Description: Interface for Language 
 * Name of Project: BTI
 * Created on: Nov 20, 2017
 * Modified on: Nov 20, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */
@Repository("repositoryLanguage")
public interface RepositoryLanguage extends JpaRepository<Language, Integer> {
	
	/**
	 * @param languageName
	 * @param b
	 * @return
	 */
	Language findByLanguageNameAndIsDeleted(String languageName, boolean b);
	
	public Language findByLanguageIdAndIsDeleted(int langId, boolean deleted);
	 

}
