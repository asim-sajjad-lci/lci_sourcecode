package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.bti.model.GLPaymentVoucherHeader;

@Repository("repositoryPaymentVoucherHeader")
public interface RepositoryGLPaymentVoucherHeader extends JpaRepository<GLPaymentVoucherHeader, Integer>{

	//Find By PaymentVoucher ID
	@Query("SELECT  GLPV from GLPaymentVoucherHeader GLPV where GLPV.paymentVoucherId =:paymentVoucherId and isDeleted=false")
	public GLPaymentVoucherHeader getByPaymentVoucherIdAndIsDeleted(@Param("paymentVoucherId") int paymentVoucherId);
	
	
	
	@Query("from GLPaymentVoucherHeader PVH where PVH.isDeleted=false")
	public List<GLPaymentVoucherHeader> getId();
	
	@Query("SELECT coalesce(max(GLPV.paymentVoucherId), 0)+1 FROM GLPaymentVoucherHeader GLPV")
	public int getMaxId();
	
	@Query("SELECT GLPV.paymentVoucherId from GLPaymentVoucherHeader GLPV where GLPV.isDeleted=false and GLPV.isPosted=false")
	public List<Integer> getPaymentVoucherId();
	
	//Get Unposted Payment Voucher by id 
	@Query("SELECT GLPV FROM GLPaymentVoucherHeader GLPV WHERE GLPV.paymentVoucherId=:paymentVoucherId AND isDeleted=false AND isPosted=false ")
	public GLPaymentVoucherHeader findByPaymentVoucherId(@Param("paymentVoucherId") int paymentVoucherId);
}
