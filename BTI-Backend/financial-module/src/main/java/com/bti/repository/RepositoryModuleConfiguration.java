/**
' * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ModulesConfiguration;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: Aug 04, 2017
 * Modified on: Aug 04, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryModuleConfiguration")
public interface RepositoryModuleConfiguration extends JpaRepository<ModulesConfiguration, Integer>{

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select m from ModulesConfiguration m where  m.isDeleted=false and (m.moduleDescription like %:searchKeyWord% or m.moduleDescriptionArabic like %:searchKeyWord% or m.moduleSeriesNumber like %:searchKeyWord% or m.originTransactionDescription like %:searchKeyWord% or m.originTransactionDescriptionArabic like %:searchKeyWord%)")
	public List<ModulesConfiguration> predictiveModulesConfigurationSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select m from ModulesConfiguration m where  m.isDeleted=false and (m.moduleDescription like %:searchKeyWord% or m.moduleDescriptionArabic like %:searchKeyWord% or m.moduleSeriesNumber like %:searchKeyWord% or m.originTransactionDescription like %:searchKeyWord% or m.originTransactionDescriptionArabic like %:searchKeyWord%)")
	public List<ModulesConfiguration> predictiveModulesConfigurationSearch(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select m from ModulesConfiguration m where m.isDeleted=false and (m.moduleDescription = :series or m.moduleDescriptionArabic = :series )")
	public List<ModulesConfiguration> findByModuleDescriptionOrModuleDescriptionArabic(@Param("series") String series);

	/**
	 * @param seriesId
	 * @param deleted
	 * @return
	 */
	public ModulesConfiguration findByModuleSeriesNumberAndIsDeleted(Integer seriesId, boolean deleted);
	
	/**
	 * @param moduleSeriesNumberList
	 * @param deleted
	 * @param changeBy
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update ModulesConfiguration d set d.isDeleted =:deleted, d.changeBy=:changeBy where d.moduleSeriesNumber IN (:moduleSeriesNumberList)")
	void deleteMultipleByModuleSeriesNumber(@Param("moduleSeriesNumberList") List<Integer> moduleSeriesNumberList, @Param("deleted") boolean deleted,
			@Param("changeBy") Integer changeBy);
	
}
