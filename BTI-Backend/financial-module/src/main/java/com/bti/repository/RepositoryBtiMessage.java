/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.BtiMessage;

/**
 * Description: Interface for RepositoryBtiMessage
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryBtiMessage")
public interface RepositoryBtiMessage extends JpaRepository<BtiMessage, Integer> {
 
	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<BtiMessage> findByIsDeletedAndLanguageLanguageId(boolean b, int i);
	
	BtiMessage findByMessageShortAndIsDeletedAndLanguageLanguageId(String messageShort,boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
}


