/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.APTransactionEntry;

/**
 * Description: Interface for APTransactionEntry 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryAPTransactionEntry")
public interface RepositoryAPTransactionEntry extends JpaRepository<APTransactionEntry,String>{

	APTransactionEntry findByApTransactionNumber(String arTransactionNumber); 
	
	List<APTransactionEntry> findByTransactionTypeApDocumentTypeId(int typeId);

	List<APTransactionEntry> findByApBatchesBatchID(String batchId, boolean b);

	List<APTransactionEntry> findByApBatchesBatchID(String batchId);

	@Query("select count(*),COALESCE(SUM(apt.apTransactionTotalAmount), 0) from APTransactionEntry apt where apt.apBatches.batchID=:batchId")
	List<Object[]> getTotalAmountAndTotalCountByBatchId(@Param("batchId") String batchId);

	
}
