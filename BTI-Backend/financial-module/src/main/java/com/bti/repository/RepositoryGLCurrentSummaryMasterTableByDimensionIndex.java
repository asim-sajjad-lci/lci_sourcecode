/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLCurrentSummaryMasterTableByDimensions;
import com.bti.model.GLCurrentSummaryMasterTableByDimensionsKey;

@Repository("repositoryGLCurrentSummaryMasterTableByDimensionIndex")
public interface RepositoryGLCurrentSummaryMasterTableByDimensionIndex extends JpaRepository<GLCurrentSummaryMasterTableByDimensions, GLCurrentSummaryMasterTableByDimensionsKey> {
  
	@Transactional
    @Modifying(clearAutomatically = true)
   	@Query("update GLCurrentSummaryMasterTableByDimensions g set g.debitAmount=0.0,g.creditAmount=0.0,g.periodBalance=0.0 "
   			+ " where g.mainAccountIndex=:mainAccountIndex "
    		+ "and g.year=:year "
    		+ "and g.periodID between 1 and :endPeriod ")
	void setDebitCreditAmountZeroYTD(@Param("mainAccountIndex") int mainAccountIndex,
	   			@Param("year") int year,@Param("endPeriod") int endPeriod);
}
