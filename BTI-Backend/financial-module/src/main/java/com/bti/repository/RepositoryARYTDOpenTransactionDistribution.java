/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ARYTDOpenTransactionsDistribution;

/**
 * Description: Interface for ARYTDOpenTransactionsDistribution 
 * Name of Project: BTI
 * Created on: Aug 24, 2017
 * Modified on: Aug 24, 2017 9:37:38 AM
 * @author seasia
 * Version: 
 */
@Repository("repositoryARYTDOpenTransactionDistribution")
public interface RepositoryARYTDOpenTransactionDistribution extends JpaRepository<ARYTDOpenTransactionsDistribution, Integer>{
	
	List<ARYTDOpenTransactionsDistribution> findByArTransactionNumber(String transNumber);
	
	@Query("select COALESCE(SUM(d.creditAmount),0.0) from ARYTDOpenTransactionsDistribution d where d.accountTableRowIndex=:accountTableRowIndex "
			+ " and d.transactionType=:transactionType and d.arTransactionNumber IN (:arTransactionNumberList) ")
	public Double getTotalCreditAmountByAccountNumberAndTransactionType(
			@Param("accountTableRowIndex") int accountTableRowIndex,@Param("transactionType") int transactionType,
			@Param("arTransactionNumberList") List<String> arTransactionNumberList);
	
	@Query("select d.arTransactionNumber from ARYTDOpenTransactionsDistribution d where d.accountTableRowIndex=:accountTableRowIndex "
			+ " and (d.transactionType=:transactionType or d.transactionType=:transactionType2 ) and d.arTransactionNumber IN (:arTransactionNumberList) ")
	public List<String> getTransactionNumberByAccountNumberAndTransactionType(
			@Param("accountTableRowIndex") int accountTableRowIndex,@Param("transactionType") int transactionType,
			@Param("transactionType2") int transactionType2,@Param("arTransactionNumberList") List<String> arTransactionNumberList);
	

}
