/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationAccountCategories;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLConfigurationAccountCategories")
public interface RepositoryGLConfigurationAccountCategories extends JpaRepository<GLConfigurationAccountCategories, Integer> {

	
	/**
	 * @param accountCategoryId
	 * @param deleted
	 * @return
	 */
	GLConfigurationAccountCategories findByAccountCategoryIdAndIsDeleted(Integer accountCategoryId, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from GLConfigurationAccountCategories glcac where glcac.isDeleted=false and glcac.accountCategoryDescription like %:searchKeyWord% or glcac.accountCategoryDescriptionArabic like %:searchKeyWord% ")
	public int predictiveAccountCategorySearchCount(@Param("searchKeyWord") String searchKeyWord);

	 
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select glcac from GLConfigurationAccountCategories glcac where glcac.isDeleted=false and glcac.accountCategoryDescription like %:searchKeyWord% or glcac.accountCategoryDescriptionArabic like %:searchKeyWord% ")
	public List<GLConfigurationAccountCategories> predictiveAccountCategorySearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select glcac from GLConfigurationAccountCategories glcac where glcac.isDeleted=false and glcac.accountCategoryDescription like %:searchKeyWord% or glcac.accountCategoryDescriptionArabic like %:searchKeyWord% ")
	List<GLConfigurationAccountCategories> predictiveAccountCategorySearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param accountCategoryId
	 * @param deleted
	 * @return
	 */
	GLConfigurationAccountCategories findByAccountCategoryDescriptionAndIsDeleted(String accountCategoryName, boolean deleted);

}
