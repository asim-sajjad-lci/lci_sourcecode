/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bti.model.CurrencySetup;

/**
 * Description: Interface for RepositoryCurrencySetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCurrencySetup")
public interface RepositoryCurrencySetup extends JpaRepository<CurrencySetup,String>{
	
	/**
	 * @param currencyId
	 * @return
	 */
	public CurrencySetup findByCurrencyIdAndIsDeleted(String currencyId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query( "select count(*) from CurrencySetup cs where cs.isDeleted=false and "
			+ " ( cs.currencyId like %:searchKeyWord% or cs.currencyDescription like %:searchKeyWord% or"
			+ " cs.currencyDescriptionArabic like %:searchKeyWord% or "
			+ " cs.currencySymbol like %:searchKeyWord% ) ")
	public int predictiveCurrencySetupSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query( "select count(*) from CurrencySetup cs where cs.isDeleted=false and "
			+ " ( cs.currencyId like %:searchKeyWord% or cs.currencyDescription like %:searchKeyWord% or"
			+ " cs.currencyDescriptionArabic like %:searchKeyWord% or "
			+ " cs.currencySymbol like %:searchKeyWord% ) and  "
			+ " cs.currencyId IN (select ced.currencySetup.currencyId from CurrencyExchangeDetail ced where "
			+ "  DATE(ced.expirationExchangeDate) >=:currentDate ) ")
	public int predictiveCurrencySetupSearchCountByExpireDate(@Param("searchKeyWord") String searchKeyWord , @Param("currentDate") Date currentDate );
	
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select cs from CurrencySetup cs where cs.isDeleted=false and "
			+ " ( cs.currencyId like %:searchKeyWord% or cs.currencyDescription like %:searchKeyWord% "
			+ " or cs.currencyDescriptionArabic like %:searchKeyWord% or "
			+ " cs.currencySymbol like %:searchKeyWord% ) ")
	public List<CurrencySetup> predictiveCurrencySetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord, Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select cs from CurrencySetup cs where cs.isDeleted=false and "
			+ " ( cs.currencyId like %:searchKeyWord% or cs.currencyDescription like %:searchKeyWord% or "
			+ " cs.currencyDescriptionArabic like %:searchKeyWord% or "
			+ " cs.currencySymbol like %:searchKeyWord% ) ")
	public List<CurrencySetup> predictiveCurrencySetupSearch(@Param("searchKeyWord") String searchKeyWord);

	@Query("select cs from CurrencySetup cs where cs.isDeleted=false and "
			+ " (cs.currencyId like %:searchKeyWord% or cs.currencyDescription like %:searchKeyWord% "
			+ " or cs.currencyDescriptionArabic like %:searchKeyWord% or "
			+ " cs.currencySymbol like %:searchKeyWord% ) and "
			+ " cs.currencyId IN (select ced.currencySetup.currencyId from CurrencyExchangeDetail ced where "
			+ "  DATE(ced.expirationExchangeDate) >=:currentDate ) ")
	public List<CurrencySetup> predictiveCurrencySetupSearchWithPaginationAndByExpireDate(@Param("searchKeyWord") String searchKeyWord, @Param("currentDate") Date currentDate, Pageable pageable);
	
	
	@Query("select cs from CurrencySetup cs where cs.isDeleted=false and "
			+ " ( cs.currencyId like %:searchKeyWord% or cs.currencyDescription like %:searchKeyWord% or "
			+ " cs.currencyDescriptionArabic like %:searchKeyWord% or "
			+ " cs.currencySymbol like %:searchKeyWord% ) and "
			+ " cs.currencyId IN (select ced.currencySetup.currencyId from CurrencyExchangeDetail ced where "
			+ "  DATE(ced.expirationExchangeDate) >=:currentDate ) ")
	public List<CurrencySetup> predictiveCurrencySetupSearchAndByExpireDate(@Param("searchKeyWord") String searchKeyWord , @Param("currentDate") Date currentDate);
	
}
