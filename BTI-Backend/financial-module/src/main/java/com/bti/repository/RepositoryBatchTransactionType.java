/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.BatchTransactionType;

/**
 * Description: Interface for BatchTransactionType 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryBatchTransactionType")
public interface RepositoryBatchTransactionType extends JpaRepository<BatchTransactionType, Integer> {

	/**
	 * @param transactionTypeId
	 * @param langId
	 * @param b
	 * @return
	 */
	BatchTransactionType findByTypeIdAndLanguageLanguageIdAndIsDeleted(Integer transactionTypeId, int langId,
			boolean b);

	/**
	 * @param string
	 * @param langId
	 * @param b
	 * @return
	 */
	List<BatchTransactionType> findByTransactionModuleAndLanguageLanguageIdAndIsDeleted(String string, int langId,
			boolean b);
	

}
