/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.VendorAccountTableSetup;

/**
 * Description: Interface for VendorAccountTableSetup 
 * Name of Project: BTI
 * Created on: Aug 28, 2017
 * Modified on: Aug 28, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryVendorAccountTableSetup")
public interface RepositoryVendorAccountTableSetup extends JpaRepository<VendorAccountTableSetup, Integer> {
	
	
	/**
	 * @param accTableIndex
	 * @param deleted
	 * @return
	 */
	VendorAccountTableSetup findByAccountTableIndexAndIsDeleted(int accTableIndex,boolean deleted);
	
	/**
	 * @param accountTypeid
	 * @param actRowId
	 * @param deleted
	 * @return
	 */
	VendorAccountTableSetup findByMasterAccountTypeTypeIdAndGlAccountsTableAccumulationIdAndIsDeleted(int accountTypeid,int actRowId,boolean deleted);
	
	/**
	 * @param accountTypeid
	 * @param deleted
	 * @return
	 */
	VendorAccountTableSetup findByMasterAccountTypeTypeIdAndIsDeleted(int accountTypeid,boolean deleted);
	
	/**
	 * @param accountTypeid
	 * @param vendorId
	 * @param deleted
	 * @return
	 */
	VendorAccountTableSetup findByMasterAccountTypeTypeIdAndVendorMaintenanceVendorid(int accountTypeid, String vendorId);
	
	/**
	 * @param vendorId
	 * @param deleted
	 * @return
	 */
	List<VendorAccountTableSetup> findByVendorMaintenanceVendoridAndIsDeleted(String vendorId,boolean deleted);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update VendorAccountTableSetup d set d.isDeleted =:deleted where d.vendorMaintenance.vendorid=:id")
	void deleteRecordById(@Param("id") String id, @Param("deleted") Boolean deleted);

	VendorAccountTableSetup findByMasterAccountTypeTypeIdAndVendorMaintenanceVendoridAndIsDeleted(int i, String vendorID,
			boolean b);
	
}
