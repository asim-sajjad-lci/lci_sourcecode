/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.CountryMaster;

/**
 * Description: Interface for RepositoryCountryMaster 
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCountryMaster")
public interface RepositoryCountryMaster extends JpaRepository<CountryMaster, Integer> {

	
	/**
	 * @param deleted
	 * @param isActive
	 * @param langId
	 * @return
	 */
	List<CountryMaster> findByIsDeletedAndIsActiveAndLanguageLanguageId(boolean deleted, boolean isActive,int langId);

	/**
	 * @param countryId
	 * @param deleted
	 * @param isActive
	 * @param langId
	 * @return
	 */
	CountryMaster findByCountryIdAndIsDeletedAndIsActiveAndLanguageLanguageId(int countryId, boolean deleted, boolean isActive,int langId);
	
	
	/**
	 * @param countryName
	 * @param langid
	 * @param deleted
	 * @return
	 */
	CountryMaster findByCountryNameAndLanguageLanguageIdAndIsDeleted(String countryName, int langid,boolean deleted);
	
	/**
	 * @param countryName
	 * @param deleted
	 * @param active
	 * @return
	 */
	CountryMaster findByCountryNameAndIsDeletedAndIsActive(String countryName,boolean deleted,boolean active);
	
	/**
	 * @param countryId
	 * @param deleted
	 * @param isActive
	 * @param langId
	 * @return
	 */
	CountryMaster findByCountryCodeAndIsDeletedAndIsActiveAndLanguageLanguageId(String countryId, boolean deleted, boolean isActive,int langId);

	/**
	 * @param countryId
	 * @param deleted
	 * @param active
	 * @return
	 */
	CountryMaster findByCountryIdAndIsDeletedAndIsActive(int countryId, boolean deleted, boolean active);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<CountryMaster> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	/**
	 * @param i
	 * @param b
	 * @return
	 */
	CountryMaster findByCountryIdAndIsDeleted(int i, boolean b);
	
	CountryMaster findByShortNameAndIsDeletedAndLanguageLanguageId(String shortName, boolean deleted,int langId);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
}


