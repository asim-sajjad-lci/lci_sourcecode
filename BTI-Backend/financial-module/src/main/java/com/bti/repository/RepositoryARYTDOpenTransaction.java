/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ARYTDOpenTransactions;

/**
 * Description: Interface for APBatches 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryARYTDOpenTransaction")
public interface RepositoryARYTDOpenTransaction extends JpaRepository<ARYTDOpenTransactions,String>{

	List<ARYTDOpenTransactions> findByCustomerID(String customerID,Pageable pageable);
	
	List<ARYTDOpenTransactions> findByCustomerID(String customerID);
	
	ARYTDOpenTransactions findByCashReceiptNumber(String paymentNumber);
	
	@Query("select arytd.arTransactionNumber from ARYTDOpenTransactions arytd where arytd.arTransactionNumber like '%SLS%' and "
			+ " arytd.arTransactionDate between DATE(:startDate) and DATE(:endDate) ")
	List<String> findByArTransactionDateBetween(@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	@Query("select count(*) from ARYTDOpenTransactions arytd where arytd.customerID=:customerID")
	public int getCountByCustomerId(@Param("customerID") String customerID);
	
	@Query("select count(*) from ARYTDOpenTransactions arytd where DATE(arytd.postingDate) = :postingDate and arytd.customerID=:customerID")
	public int getCountByCustomerIdAndPostingDate(@Param("postingDate")  Date postingDate, @Param("customerID") String customerID);
	
	@Query("select arytd from ARYTDOpenTransactions arytd where DATE(arytd.postingDate) = :postingDate and arytd.customerID=:customerID")
	List<ARYTDOpenTransactions> findByPostingDateAndCustomerId(@Param("postingDate")  Date postingDate, @Param("customerID") String customerID);

	@Query("select arytd from ARYTDOpenTransactions arytd where DATE(arytd.postingDate) = :postingDate and arytd.customerID=:customerID")
	List<ARYTDOpenTransactions> findByPostingDateAndCustomerId(@Param("postingDate")  Date postingDate, @Param("customerID") String customerID, 
			Pageable pageable);

	List<ARYTDOpenTransactions> findByCustomerIDAndArTransactionSalesAmountIsNotNullAndIsPaymentAndArTransactionSalesAmountGreaterThan(
			String customerId,boolean isPayment, double greaterthan);
	
	List<ARYTDOpenTransactions> findByCustomerIDAndArTransactionDebitMemoAmountIsNotNullAndIsPaymentAndArTransactionDebitMemoAmountGreaterThan(
			String customerId,boolean isPayment, double greaterthan);
	
	List<ARYTDOpenTransactions> findByCustomerIDAndArTransactionFinanceChargeAmountIsNotNullAndIsPaymentAndArTransactionFinanceChargeAmountGreaterThan(
			String customerId,boolean isPayment, double greaterthan);
	
	List<ARYTDOpenTransactions> findByCustomerIDAndArServiceRepairAmountIsNotNullAndIsPaymentAndArServiceRepairAmountGreaterThan(
			String customerId,boolean isPayment, double greaterthan);

}
