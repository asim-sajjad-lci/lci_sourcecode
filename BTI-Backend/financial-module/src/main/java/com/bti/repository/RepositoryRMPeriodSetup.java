/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.RMPeriodSetup;

/**
 * Description: Interface for RMPeriodSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryRMPeriodSetup")
public interface RepositoryRMPeriodSetup extends JpaRepository<RMPeriodSetup, Integer> {
	
	
	/**
	 * @param periodIndex
	 * @param deleted
	 * @return
	 */
	RMPeriodSetup findByPeriodIndexAndIsDeleted(String periodIndex,boolean deleted);
	
	
	/**
	 * @param rmSetupId
	 * @param deleted
	 * @return
	 */
	List<RMPeriodSetup> findByRmSetupIdAndIsDeleted(int rmSetupId,boolean deleted);
	
}
