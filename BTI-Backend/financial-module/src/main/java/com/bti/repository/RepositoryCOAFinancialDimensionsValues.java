/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.COAFinancialDimensionsValues;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCOAFinancialDimensionsValues")
public interface RepositoryCOAFinancialDimensionsValues extends JpaRepository<COAFinancialDimensionsValues, Integer> {
	
	/**
	 * @param dimInxValue
	 * @return
	 */
	COAFinancialDimensionsValues findByDimInxValueAndIsDeleted(Long dimInxValue , boolean deleted);
	
	/**
	 * @param dimIndexId
	 * @return
	 */
	List<COAFinancialDimensionsValues> findByCoaFinancialDimensionsDimInxdAndIsDeleted(int dimIndexId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from COAFinancialDimensionsValues fdv where fdv.isDeleted=false and fdv.dimensionValue like %:searchKeyWord% or fdv.dimensionDescription like %:searchKeyWord% or fdv.dimensionDescriptionArabic like %:searchKeyWord% or fdv.coaFinancialDimensions.dimensioncolumnname like %:searchKeyWord% ")
	public int predictiveCOAFinancialDimensionsValueSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select fdv from COAFinancialDimensionsValues fdv where fdv.isDeleted=false and fdv.dimensionValue like %:searchKeyWord% or fdv.dimensionDescription like %:searchKeyWord% or fdv.dimensionDescriptionArabic like %:searchKeyWord% or fdv.coaFinancialDimensions.dimensioncolumnname like %:searchKeyWord% ")
	public List<COAFinancialDimensionsValues> predictiveCOAFinancialDimensionsValueSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select fdv from COAFinancialDimensionsValues fdv where fdv.isDeleted=false and  fdv.dimensionValue like %:searchKeyWord% or fdv.dimensionDescription like %:searchKeyWord% or fdv.dimensionDescriptionArabic like %:searchKeyWord% or fdv.coaFinancialDimensions.dimensioncolumnname like %:searchKeyWord% ")
	public List<COAFinancialDimensionsValues> predictiveCOAFinancialDimensionsValueSearch(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param dimensionValue
	 * @return
	 */
	List<COAFinancialDimensionsValues> findByDimensionValueAndIsDeleted(String  dimensionValue, boolean deleted);
	
	/**
	 * @param dimensionValue
	 * @param finanacialIndexId
	 * @return
	 */
	COAFinancialDimensionsValues findByDimensionValueAndCoaFinancialDimensionsDimInxdAndIsDeleted(String  dimensionValue,int finanacialIndexId , boolean deleted);
	
	/**
	 * @param dimensionValue
	 * @param dimensionIndexId
	 * @param dimensionValueIndexId
	 * @return
	 */
	@Query("select fdv from COAFinancialDimensionsValues fdv where fdv.isDeleted=false and fdv.dimensionValue=:dimensionValue and fdv.coaFinancialDimensions.dimInxd=:dimensionIndexId and fdv.dimInxValue!=:dimensionValueIndexId")
	COAFinancialDimensionsValues findByDimensionValueAndCoaFinancialDimensionsDimInxdAndCoaDimensionValueIndexNotEqual(@Param("dimensionValue") String dimensionValue , @Param("dimensionIndexId") Integer dimensionIndexId , @Param("dimensionValueIndexId") Long dimensionValueIndexId);
	
}
