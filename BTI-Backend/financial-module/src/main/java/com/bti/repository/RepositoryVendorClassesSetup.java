/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.VendorClassesSetup;

/**
 * Description: Interface for RepositoryVendorClassesSetup 
 * Name of Project: BTI
 * Created on: Aug 28, 2017
 * Modified on: Aug 28, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryVendorClassesSetup")
public interface RepositoryVendorClassesSetup extends JpaRepository<VendorClassesSetup, Integer> {
	
	
	/**
	 * @param vendorClassId
	 * @param deleted
	 * @return
	 */
	VendorClassesSetup findByVendorClassIdAndIsDeleted(String vendorClassId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from VendorClassesSetup vcs where vcs.isDeleted=false and vcs.vendorClassId like %:searchKeyWord% or vcs.classDescription like %:searchKeyWord% or vcs.classDescriptionArabic like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select vcs from VendorClassesSetup vcs where vcs.isDeleted=false and vcs.vendorClassId like %:searchKeyWord% or vcs.classDescription like %:searchKeyWord% or vcs.classDescriptionArabic like %:searchKeyWord% ")
	public List<VendorClassesSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select vcs from VendorClassesSetup vcs where vcs.isDeleted=false and vcs.vendorClassId like %:searchKeyWord% or vcs.classDescription like %:searchKeyWord% or vcs.classDescriptionArabic like %:searchKeyWord% ")
	public List<VendorClassesSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param paymentTermId
	 * @return
	 */
	@Query("select count(*) from VendorClassesSetup c where c.isDeleted=false and c.paymentTermsSetup.paymentTermId=:paymentTermId ")
	public Integer getCountByPaymentTermsSetupPaymentTermId(@Param("paymentTermId") String paymentTermId);
	
	/**
	 * @param shipmentMethodId
	 * @return
	 */
	@Query("select count(*) from VendorClassesSetup c where c.isDeleted=false and c.shipmentMethodSetup.shipmentMethodId=:shipmentMethodId ")
	public Integer getCountByShipmentMethodSetup(@Param("shipmentMethodId") String shipmentMethodId);
	
}
