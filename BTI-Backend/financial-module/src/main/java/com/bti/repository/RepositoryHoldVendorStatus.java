/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.HoldVendorStatus;

@Repository("repositoryHoldVendorStatus")
public interface RepositoryHoldVendorStatus extends JpaRepository<HoldVendorStatus, Integer>{

	
	/**
	 * @param vendorHold
	 * @param langid
	 * @param deleted
	 * @return
	 */
	HoldVendorStatus findByHoldStatusAndLanguageLanguageIdAndIsDeleted(int vendorHold,int langid,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<HoldVendorStatus> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);

}
