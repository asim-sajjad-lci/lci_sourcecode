/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLClearingHeader;

/**
 * Description: Interface for GLClearingHeader 
 * Name of Project: BTI
 * Created on: Dec 7, 2017
 * Modified on: Dec 7, 2017 5:20:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLClearingHeader")
public interface RepositoryGLClearingHeader extends JpaRepository<GLClearingHeader,Integer>{
	
	/**
	 * @param journalId
	 * @param deleted
	 * @return
	 */
	GLClearingHeader findByJournalIDAndIsDeleted(String journalId, boolean deleted);
	
	@Query(" select DISTINCT ch.journalID from  GLClearingHeader ch where isDeleted=false ")
	List<String> getDistnictjournalId();
	
	List<GLClearingHeader> findByGlBatchesBatchIDAndIsDeleted(String batchId, boolean deleted);
	
	@Query("select count(*) from GLClearingHeader jeh where jeh.glBatches.batchID=:batchId and jeh.isDeleted=false")
	Long getTotalQuantityByBatchId(@Param("batchId") String batchId);

	List<GLClearingHeader> findByJournalID(String journalID);
	
	
}
