/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bti.model.MasterCustomerClassSetupCreditLimit;

/**
 * Description: Interface for MasterCustomerClassSetupCreditLimit
 * Name of Project: BTI
 * Created on: August 29 , 2017
 * Modified on: August 29 , 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMasterCustomerClassSetupCreditLimit")
public interface RepositoryMasterCustomerClassSetupCreditLimit extends JpaRepository<MasterCustomerClassSetupCreditLimit, Integer> {
	
	/**
	 * @param typeId
	 * @param langid
	 * @param deleted
	 * @return
	 */
	MasterCustomerClassSetupCreditLimit findByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId,int langid,boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterCustomerClassSetupCreditLimit> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterCustomerClassSetupCreditLimit> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);

}
