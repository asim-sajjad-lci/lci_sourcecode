/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.FABookMaintenanceSwitchOverType;

/**
 * Description: Interface for RepositoryFABookMaintenanceSwitchOverType 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFABookMaintenanceSwitchOverType")
public interface RepositoryFABookMaintenanceSwitchOverType extends JpaRepository<FABookMaintenanceSwitchOverType, Integer> {
	
	
	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	FABookMaintenanceSwitchOverType findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId,int langId,boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<FABookMaintenanceSwitchOverType> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<FABookMaintenanceSwitchOverType> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);

	
}
