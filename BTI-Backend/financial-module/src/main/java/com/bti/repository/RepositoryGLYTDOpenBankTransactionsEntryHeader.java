/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.GLYTDOpenBankTransactionsEntryHeader;

/**
 * Description: Interface for RepositoryGLYTDOpenBankTransactionsEntryHeader
 * Name of Project: BTI
 * Created on: Dec 11, 2017
 * Modified on: Dec 11, 2017 4:19:38 PM
 * @author Dev
 * Version: 
 */
@Repository("repositoryGLYTDOpenBankTransactionsEntryHeader")
public interface RepositoryGLYTDOpenBankTransactionsEntryHeader extends JpaRepository<GLYTDOpenBankTransactionsEntryHeader, Integer> {
 
	
}


