/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FABookClassSetup;

/**
 * Description: Interface for RepositoryFABookMaintenance 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFABookClassSetup")
public interface RepositoryFABookClassSetup extends JpaRepository<FABookClassSetup, Integer> {
	
	/**
	 * @param bookIndex
	 * @return
	 */
	FABookClassSetup findByBookIndexAndIsDeleted(int bookIndex, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FABookClassSetup fabcs where fabcs.isDeleted=false and  fabcs.bookSetup.bookId like %:searchKeyWord% or fabcs.faClassSetup.classId like %:searchKeyWord% or fabcs.faDepreciationMethods.depreciationMethodId like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select fabcs from FABookClassSetup fabcs where fabcs.isDeleted=false and fabcs.bookSetup.bookId like %:searchKeyWord%  or fabcs.faClassSetup.classId like %:searchKeyWord% or fabcs.faDepreciationMethods.depreciationMethodId like %:searchKeyWord% ")
	public List<FABookClassSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select fabcs from FABookClassSetup fabcs where fabcs.isDeleted=false and fabcs.bookSetup.bookId like %:searchKeyWord% or fabcs.faClassSetup.classId like %:searchKeyWord% or fabcs.faDepreciationMethods.depreciationMethodId like %:searchKeyWord% ")
	public List<FABookClassSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

	FABookClassSetup findByBookSetupBookIdAndIsDeleted(String bookId, boolean deleted);

	
}
