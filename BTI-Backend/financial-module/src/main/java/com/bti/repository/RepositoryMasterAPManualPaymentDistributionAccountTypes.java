/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterAPManualPaymentDistributionAccountTypes;

/**
 * Description: Interface for MasterARCashDistributionAccountTypes 
 * Name of Project: BTI
 * Created on: Dec 27, 2017
 * Modified on: Dec 27, 2017 3:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMasterAPManualPaymentDistributionAccountTypes")
public interface RepositoryMasterAPManualPaymentDistributionAccountTypes extends JpaRepository<MasterAPManualPaymentDistributionAccountTypes,String>{
	
	List<MasterAPManualPaymentDistributionAccountTypes> findByLanguageLanguageIdAndIsDeleted(int langId, boolean deleted);
	
	MasterAPManualPaymentDistributionAccountTypes findByLanguageLanguageIdAndIsDeletedAndTypeId(int langId,boolean deleted,int typeId);
	
}
