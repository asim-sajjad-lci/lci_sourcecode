/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAInsuranceMaintenance;

/**
 * Description: RepositoryFAInsuranceMaintenance
 * Name of Project: BTI
 * Created on: Sep 01, 2017
 * Modified on: Sep 01, 2017 11:08:38 AM
 * @author seasia
 * Version: 
 */

@Repository("repositoryFAInsuranceMaintenance")
public interface RepositoryFAInsuranceMaintenance extends JpaRepository<FAInsuranceMaintenance, Integer> {

	/**
	 * @param insClassId
	 * @return
	 */
	
	FAInsuranceMaintenance findByFaInsuranceClassSetupInsuranceClassIdAndIsDeleted(String insClassId, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FAInsuranceMaintenance im where im.isDeleted=false and im.faInsuranceClassSetup.insuranceClassId like %:searchKeyWord% or im.insuranceValue like %:searchKeyWord% or im.insuranceYears like %:searchKeyWord% or im.faGeneralMaintenance.assetId like %:searchKeyWord% ")
	Integer predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select im from FAInsuranceMaintenance im where im.isDeleted=false and im.faInsuranceClassSetup.insuranceClassId like %:searchKeyWord% or im.insuranceValue like %:searchKeyWord% or im.insuranceYears like %:searchKeyWord% or im.faGeneralMaintenance.assetId like %:searchKeyWord% ")
	List<FAInsuranceMaintenance> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select im from FAInsuranceMaintenance im where im.isDeleted=false and im.faInsuranceClassSetup.insuranceClassId like %:searchKeyWord% or im.insuranceValue like %:searchKeyWord% or im.insuranceYears like %:searchKeyWord% or im.faGeneralMaintenance.assetId like %:searchKeyWord% ")
	List<FAInsuranceMaintenance> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);
	
}
