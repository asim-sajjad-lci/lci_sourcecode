/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLCashReceiptBank;

@Repository("repositoryGLCashReceiptBank")
public interface RepositoryGLCashReceiptBank extends JpaRepository<GLCashReceiptBank, Integer>{

	/**
	 * @param receiptNumber
	 * @return
	 */
	GLCashReceiptBank findByReceiptNumber(String receiptNumber);
	
	List<GLCashReceiptBank> findByGlBatchesBatchID(String batchId);

	@Query("select count(*) ,(COALESCE(SUM(jeh.receiptAmount),0)+COALESCE(SUM(jeh.vatAmount), 0)) from GLCashReceiptBank jeh where jeh.glBatches.batchID=:batchId ")
	List<Object[]> getBatchTotalTrasactionsByTransactionType(@Param("batchId") String batchId);
	
	

	

}
