/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.COAMainAccounts;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCOAMainAccounts")
public interface RepositoryCOAMainAccounts extends JpaRepository<COAMainAccounts, Integer> {
	
	/**
	 * @param mainAccountNumber
	 * @return
	 */
	COAMainAccounts findByMainAccountNumberAndIsDeleted(String mainAccountNumber, boolean deleted);
	
	/**
	 * @param actIndex
	 * @return
	 */
	COAMainAccounts findByActIndxAndIsDeleted(Long actIndex, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from COAMainAccounts fds where fds.isDeleted=false and fds.mainAccountNumber like %:searchKeyWord% or fds.mainAccountDescription like %:searchKeyWord% or fds.mainAccountDescriptionArabic like %:searchKeyWord% or fds.mainAccountTypes.accountTypeName like %:searchKeyWord% or fds.glConfigurationAccountCategories.accountCategoryDescription like %:searchKeyWord% ")
	public int predictiveCOAMainAccountsSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select fds from COAMainAccounts fds where fds.isDeleted=false and fds.mainAccountNumber like %:searchKeyWord% or fds.mainAccountDescription like %:searchKeyWord% or fds.mainAccountDescriptionArabic like %:searchKeyWord% or fds.mainAccountTypes.accountTypeName like %:searchKeyWord% or fds.glConfigurationAccountCategories.accountCategoryDescription like %:searchKeyWord% ")
	public List<COAMainAccounts> predictiveCOAMainAccountsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select fds from COAMainAccounts fds where fds.isDeleted=false and fds.mainAccountNumber like %:searchKeyWord% or fds.mainAccountDescription like %:searchKeyWord% or fds.mainAccountDescriptionArabic like %:searchKeyWord% or fds.mainAccountTypes.accountTypeName like %:searchKeyWord% or fds.glConfigurationAccountCategories.accountCategoryDescription like %:searchKeyWord% ")
	public List<COAMainAccounts> predictiveCOAMainAccountsSearch(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query("select count(*) from COAMainAccounts fds where fds.isDeleted=false and active=true and "
			+ "( fds.mainAccountNumber like %:searchKeyWord% or "
			+ "fds.mainAccountDescription like %:searchKeyWord% or "
			+ "fds.mainAccountDescriptionArabic like %:searchKeyWord% or "
			+ "fds.mainAccountTypes.accountTypeName like %:searchKeyWord% or "
			+ "fds.glConfigurationAccountCategories.accountCategoryDescription like %:searchKeyWord% ) ")
	public int predictiveActiveCOAMainAccountsSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select fds from COAMainAccounts fds where fds.isDeleted=false and active=true and "
			+ "( fds.mainAccountNumber like %:searchKeyWord% or "
			+ "fds.mainAccountDescription like %:searchKeyWord% or "
			+ "fds.mainAccountDescriptionArabic like %:searchKeyWord% or "
			+ "fds.mainAccountTypes.accountTypeName like %:searchKeyWord% or "
			+ "fds.glConfigurationAccountCategories.accountCategoryDescription like %:searchKeyWord% ) ")
	public List<COAMainAccounts> predictiveActiveCOAMainAccountsSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	@Query("select fds from COAMainAccounts fds where fds.isDeleted=false and active=true and "
			+ "( fds.mainAccountNumber like %:searchKeyWord% or "
			+ "fds.mainAccountDescription like %:searchKeyWord% or "
			+ "fds.mainAccountDescriptionArabic like %:searchKeyWord% or "
			+ "fds.mainAccountTypes.accountTypeName like %:searchKeyWord% or "
			+ "fds.glConfigurationAccountCategories.accountCategoryDescription like %:searchKeyWord% ) ")
	public List<COAMainAccounts> predictiveActiveCOAMainAccountsSearch(@Param("searchKeyWord") String searchKeyWord);
	
	COAMainAccounts findByMainAccountNumberAndIsDeletedAndActive(String mainAccountNumber, boolean deleted, boolean active);
	
	List<COAMainAccounts> findByActiveAndAllowAccountTransactionEntryAndIsDeleted(boolean isActive,boolean AllowAccountTransactionEntry ,boolean isDeleted);
}
