/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FixedAllocationAccountIndexDetails;

/**
 * Description: Interface for RepositoryCurrencySetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFixedAllocationAccountIndexDetail")
public interface RepositoryFixedAllocationAccountIndexDetail extends JpaRepository<FixedAllocationAccountIndexDetails,String>{
	
	/**
	 * @param accountFixId
	 * @return
	 */
	public List<FixedAllocationAccountIndexDetails> findByFixedAllocationAccountIndexHeaderActFixIndexAndIsDeleted(Integer accountFixIndex, boolean deleted);
	
	/**
	 * @param accountnumberId
	 * @param accountFixId
	 * @return
	 */
	public FixedAllocationAccountIndexDetails findByGlAccountsTableAccumulationIdAndFixedAllocationAccountIndexHeaderActFixIndexAndIsDeleted(int accountnumberId , int accountFixId , boolean deleted);
	
	/**
	 * @param actFixIndex
	 */
	@Modifying
	@Transactional
	@Query("delete from FixedAllocationAccountIndexDetails dd where  dd.fixedAllocationAccountIndexHeader.actFixIndex=:actFixIndex ")
	public void deleteByHeaderId(@Param("actFixIndex") Integer actFixIndex);
}
