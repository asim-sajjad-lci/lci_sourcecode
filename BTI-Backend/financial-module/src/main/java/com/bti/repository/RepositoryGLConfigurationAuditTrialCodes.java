/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationAuditTrialCodes;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: Aug 11, 2017
 * Modified on: Aug 11, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLConfigurationAuditTrialCodes")
public interface RepositoryGLConfigurationAuditTrialCodes extends JpaRepository<GLConfigurationAuditTrialCodes, Integer> {

	/**
	 * @param series
	 * @return
	 */
	GLConfigurationAuditTrialCodes findBySeriesIndexAndIsDeleted(Integer series, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from GLConfigurationAuditTrialCodes glcatc where glcatc.isDeleted=false and glcatc.sourceDocument like %:searchKeyWord% or glcatc.sequenceNumber like %:searchKeyWord% or glcatc.transactionSourceCode like %:searchKeyWord% ")
	public int predictiveAuditTrialCodeSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select glcatc from GLConfigurationAuditTrialCodes glcatc where glcatc.isDeleted=false and glcatc.sourceDocument like %:searchKeyWord% or glcatc.sequenceNumber like %:searchKeyWord% or glcatc.transactionSourceCode like %:searchKeyWord% ")
	public List<GLConfigurationAuditTrialCodes> predictiveAuditTrialCodeSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select glcatc from GLConfigurationAuditTrialCodes glcatc where glcatc.isDeleted=false and glcatc.sourceDocument like %:searchKeyWord% or glcatc.sequenceNumber like %:searchKeyWord% or glcatc.transactionSourceCode like %:searchKeyWord% ")
	List<GLConfigurationAuditTrialCodes> predictiveAuditTrialCodeSearch(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param seriesId
	 * @param deleted
	 * @return
	 */
	GLConfigurationAuditTrialCodes findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(int seriesId, boolean deleted);

	/**
	 * @param moduleSeriesNumber
	 * @return
	 */
	@Query("select count(*) from GLConfigurationAuditTrialCodes c where c.isDeleted=false and c.glConfigurationAuditTrialSeriesType.typeId=:moduleSeriesNumber ")
	public Integer getCountByModulesConfiguration(@Param("moduleSeriesNumber") Integer moduleSeriesNumber);

	GLConfigurationAuditTrialCodes findFirstByGlConfigurationAuditTrialSeriesTypeTypeIdAndGlConfigurationAuditTrialSeriesTypeLanguageLanguageIdAndSourceDocumentContainingIgnoreCase(int seriesTypeTypeId, int langId,String sourceDocument);

	
	GLConfigurationAuditTrialCodes findBySeriesIndexAndIsDeleted(int indeies, boolean isDeleted);
	
	
}
