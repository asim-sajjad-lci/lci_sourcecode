/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.APYTDOpenTransactionDistribution;

/**
 * Description: Interface for APYTDOpenTransactionDistribution 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryAPYTDOpenTransactionDistribution")
public interface RepositoryAPYTDOpenTransactionDistribution extends JpaRepository<APYTDOpenTransactionDistribution,String>{

	List<APYTDOpenTransactionDistribution> findByApTransactionNumber(String apTransactionNumber);
	
	@Query("select COALESCE(SUM(d.debitAmount), 0.0) from APYTDOpenTransactionDistribution d where d.accountTableRowIndex=:accountTableRowIndex "
			+ "and d.transactionType=:transactionType and d.apTransactionNumber IN (:apTransactionNumberList) ")
	public Double getTotalDebitAmountByAccountNumberAndTransactionType(@Param("accountTableRowIndex") int accountTableRowIndex, @Param("transactionType") int transactionType,
			@Param("apTransactionNumberList") List<String> apTransactionNumberList);
	
	@Query("select d.apTransactionNumber from APYTDOpenTransactionDistribution d where d.accountTableRowIndex=:accountTableRowIndex "
			+ "and ( d.transactionType=:transactionType or d.transactionType=:transactionType2 ) and d.apTransactionNumber IN (:apTransactionNumberList) ")
	public List<String> getTransactionNumberByAccountNumberAndTransactionType(@Param("accountTableRowIndex") int accountTableRowIndex, @Param("transactionType") int transactionType,
			@Param("transactionType2") int transactionType2,@Param("apTransactionNumberList") List<String> apTransactionNumberList);
}
