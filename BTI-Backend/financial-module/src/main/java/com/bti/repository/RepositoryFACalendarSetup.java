/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FACalendarSetup;

/**
 * Description: Interface for RepositoryShipmentMethodSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFACalendarSetup")
public interface RepositoryFACalendarSetup extends JpaRepository<FACalendarSetup, Integer> {
	
	/**
	 * @param calendarIndex
	 * @return
	 */
	FACalendarSetup findByCalendarIndexAndIsDeleted(int calendarIndex,boolean deleted);
	/**
	 * @param calendarId
	 * @return
	 */
	FACalendarSetup findByCalendarIdAndIsDeleted(String calendarId,boolean deleted);
	/**
	 * @param calendarId
	 * @param dataType
	 * @return
	 */
	FACalendarSetup findByCalendarIdAndDataTypeAndIsDeleted(String calendarId,String dataType,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @param dataType
	 * @return
	 */
	@Query("select count(*) from FACalendarSetup fscs where fscs.isDeleted=false and fscs.dataType = :dataType AND (fscs.calendarDescription like %:searchKeyWord% OR fscs.calendarDescriptionArabic like %:searchKeyWord% ) ")
	public int predictiveFACalendarSetupSearchCount(@Param("searchKeyWord") String searchKeyWord,@Param("dataType") String dataType);
	
	/**
	 * @param searchKeyWord
	 * @param dataType
	 * @param pageable
	 * @return
	 */
	@Query("select fscs from FACalendarSetup fscs where fscs.isDeleted=false and fscs.dataType = :dataType AND (fscs.calendarDescription like %:searchKeyWord% OR fscs.calendarDescriptionArabic like %:searchKeyWord% )  ")
	public List<FACalendarSetup> predictiveFACalendarSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,@Param("dataType") String dataType,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @param dataType
	 * @return
	 */
	@Query("select fscs from FACalendarSetup fscs where fscs.isDeleted=false and fscs.dataType = :dataType AND (fscs.calendarDescription like %:searchKeyWord% OR fscs.calendarDescriptionArabic like %:searchKeyWord% ) ")
	public List<FACalendarSetup> predictiveFACalendarSetupSearch(@Param("searchKeyWord") String searchKeyWord,@Param("dataType") String dataType);
	
	/**
	 * @param string
	 * @return
	 */
	FACalendarSetup findByDataTypeAndIsDeleted(String string, boolean deleted);


	
}
