/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.PMPeriodSetup;

/**
 * Description: Interface for PMPeriodSetup 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryPMPeriodSetup")
public interface RepositoryPMPeriodSetup extends JpaRepository<PMPeriodSetup, Integer> {
	
	
	/**
	 * @param rmSetupId
	 * @param deleted
	 * @return
	 */
	List<PMPeriodSetup> findByPmSetupIdAndIsDeleted(int rmSetupId,boolean deleted);
	
}
