/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.COAFinancialDimensions;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCOAFinancialDimensions")
public interface RepositoryCOAFinancialDimensions extends JpaRepository<COAFinancialDimensions, Integer> {
	
	/**
	 * @param dimIndex
	 * @return
	 */
	COAFinancialDimensions findByDimInxdAndIsDeleted(int dimIndex,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from COAFinancialDimensions fds where fds.isDeleted=false and fds.dimensioncolumnname like %:searchKeyWord% or fds.dimensionMask like %:searchKeyWord% ")
	public int predictiveCOAFinancialDimensionsSetupSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select fds from COAFinancialDimensions fds where fds.isDeleted=false and fds.dimensioncolumnname like %:searchKeyWord% or fds.dimensionMask like %:searchKeyWord% ")
	public List<COAFinancialDimensions> predictiveCOAFinancialDimensionsSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select fds from COAFinancialDimensions fds where fds.isDeleted=false and fds.dimensioncolumnname like %:searchKeyWord% or fds.dimensionMask like %:searchKeyWord% ")
	public List<COAFinancialDimensions> predictiveCOAFinancialDimensionsSetupSearch(@Param("searchKeyWord") String searchKeyWord);
	
}
