/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationRelationBetweenDimensionsAndMainAccount;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount")
public interface RepositoryGLConfigurationRelationBetweenDimensionsAndMainAccount extends JpaRepository<GLConfigurationRelationBetweenDimensionsAndMainAccount, Integer> {
	
	/**
	 * @param id
	 * @return
	 */
	GLConfigurationRelationBetweenDimensionsAndMainAccount findByIdAndIsDeleted(int id,boolean deleted);
	
	GLConfigurationRelationBetweenDimensionsAndMainAccount findTopByIsDeletedAndIsMainAccount(boolean deleted, int isMainAccount);

}
