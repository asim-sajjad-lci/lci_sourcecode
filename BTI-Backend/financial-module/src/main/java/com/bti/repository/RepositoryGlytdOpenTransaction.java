/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.JournalEntryHeader;

/**
 * Description: Interface for RepositoryGlytdOpenTransaction 
 * Name of Project: BTI
 * Created on: Dec 5, 2017
 * Modified on: Dec 5, 2017 12:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGlytdOpenTransaction")
public interface RepositoryGlytdOpenTransaction extends JpaRepository<GLYTDOpenTransactions,Integer>{
	
	/**
	 * @param journalId
	 * @param deleted
	 * @return
	 */
	List<GLYTDOpenTransactions> findByJournalEntryIDAndIsDeleted(String journalId, boolean deleted);
	
	/**
	 * @param journalId
	 * @param deleted
	 * @param Audit Trial
	 * @return
	 */
	List<GLYTDOpenTransactions> findByJournalEntryIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(String journalId,int seriesIndex, boolean deleted);
	
	/**
	 * @return
	 */
	@Query("select DISTINCT opt.journalEntryID FROM GLYTDOpenTransactions opt WHERE opt.isDeleted=false and opt.transactionReversingDate=null and opt.transactionSource='JV'")
	List<String> getDistnictJournalIdForCopyJVEntryAndNotReversingForJV();
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update GLYTDOpenTransactions opt set opt.transactionReversingDate =:reversingDate where opt.journalEntryID=:journalEntryID")
	void setReversingDateByJournalIdForJvEntry(@Param("reversingDate") Date reversingDate, @Param("journalEntryID") String journalEntryID);
	
	@Query("select DISTINCT opt.originalJournalEntryID FROM GLYTDOpenTransactions opt WHERE opt.isDeleted=false and opt.transactionType=2 and opt.transactionSource='JV'")
	List<String> getDistnictOriginalJournalIdReversingEntry();
	
	@Query("select DISTINCT opt.journalEntryID FROM GLYTDOpenTransactions opt WHERE "
			+ " opt.isDeleted=false and opt.transactionType=1 and opt.transactionSource='JV' and "
			+ " opt.journalEntryID NOT IN (:originalJournalIds) ")
	List<String> getDistnictJournalIdForCorrectJVEntryAndNotReversingForJV(@Param("originalJournalIds") List<String> originalJournalIds);
	
	
	@Query("select DISTINCT opt.journalEntryID FROM GLYTDOpenTransactions opt WHERE "
			+ "opt.isDeleted=false and opt.transactionType=1 and opt.transactionSource='JV'")
	List<String> getDistnictJournalIdForCorrectJVEntryForJV();
	
	
	@Query("select DISTINCT j from GLYTDOpenTransactions j where ( j.journalEntryID like %:searchKeyWord% or j.journalDescription like %:searchKeyWord% or j.journalDescriptionArabic like %:searchKeyWord%   ) and j.isDeleted=false group by j.journalEntryID, j.glConfigurationAuditTrialCodes.seriesIndex")
	public List<GLYTDOpenTransactions> predictiveJournalSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	@Query("select DISTINCT j  from GLYTDOpenTransactions j where (j.journalEntryID like %:searchKeyWord% or j.journalDescription like %:searchKeyWord% or j.journalDescriptionArabic like %:searchKeyWord%  ) and j.isDeleted=false group by j.journalEntryID  ")
	public List<GLYTDOpenTransactions> predictiveJournalSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count( DISTINCT j.journalEntryID ) from GLYTDOpenTransactions j where (j.journalEntryID like %:searchKeyWord% or j.journalDescription like %:searchKeyWord% or j.journalDescriptionArabic like %:searchKeyWord%  ) and j.isDeleted=false group by j.journalEntryID, j.glConfigurationAuditTrialCodes.seriesIndex")
	public List<Integer> predictiveJournalHeaderSearchTotalCount(@Param("searchKeyWord")String searchkeyWord);
	
	@Query("SELECT PJE FROM GLYTDOpenTransactions PJE WHERE PJE.journalEntryID =:journalID AND PJE.isDeleted =:isDeleted")
	public List<GLYTDOpenTransactions> getPostedJournalByID(@Param("journalID") String journalID ,@Param("isDeleted") Boolean isDeleted );
	
	
}
