/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLCurrentSummaryMasterTableByAccountNumber;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumberKey;

@Repository("repositoryGLCurrentSummaryMasterTableByAccountNumber")
public interface RepositoryGLCurrentSummaryMasterTableByAccountNumber extends JpaRepository<GLCurrentSummaryMasterTableByAccountNumber, GLCurrentSummaryMasterTableByAccountNumberKey> {

    @Query(" select DISTINCT acc.accountTableRowIndex from  GLCurrentSummaryMasterTableByAccountNumber acc where isDeleted=false ")
	List<Integer> getDistnictAccountNumbers();
    
    @Query("select Sum(g.debitAmount), sum(g.creditAmount) "
    		+ "from GLCurrentSummaryMasterTableByAccountNumber g  "
    		+ "where g.accountTableRowIndex=:accountTableRowIndex "
    		+ "and g.year=:year "
    		+ "and g.periodID BETWEEN 1 and :endPeriod ")
   	List<Object[]> getTotalDebitCreditAmoutByYTD(@Param("accountTableRowIndex") int accountTableRowIndex,
   			@Param("year") int year,@Param("endPeriod") int endPeriod);
    
    @Query("select Sum(g.debitAmount), sum(g.creditAmount) "
    		+ "from GLCurrentSummaryMasterTableByAccountNumber g  "
    		+ "where g.accountTableRowIndex=:accountTableRowIndex "  
    		+ "and g.year=:year "
    		+ "and g.periodID=:endPeriod ")
   	List<Object[]> getTotalDebitCreditAmoutByPeriod(@Param("accountTableRowIndex") int accountTableRowIndex,
   			@Param("year") int year,@Param("endPeriod") int endPeriod);
   	
   	@Transactional
    @Modifying(clearAutomatically = true)
   	@Query("update GLCurrentSummaryMasterTableByAccountNumber g set g.debitAmount=0.0, g.creditAmount=0.0,g.periodBalance=0.0 "
   			+ " where g.accountTableRowIndex=:accountTableRowIndex "
    		+ "and g.year=:year "
    		+ "and g.periodID between 1 and :endPeriod ")
	void setDebitCreditAmountZeroYTD(@Param("accountTableRowIndex") int accountTableRowIndex,
	   			@Param("year") int year,@Param("endPeriod") int endPeriod);
   	
   	
   	@Query("select COALESCE(SUM(g.debitAmount), 0.0) from GLCurrentSummaryMasterTableByAccountNumber g where "
   			+ " (g.accountTableRowIndex IN (:accountTableRowIndex))")
	public double getAmountByAccountNumbersList(@Param("accountTableRowIndex") List<Integer> accountTableRowIndex);

    @Query("select g "
    		+ "from GLCurrentSummaryMasterTableByAccountNumber g  "
    		+ "where g.year BETWEEN :startYear and :endYear "
    		+ "and g.periodID BETWEEN :startPeriod and :endPeriod ")
	List<GLCurrentSummaryMasterTableByAccountNumber> getReportByYearBetweenAndPeriodBetween(@Param("startYear") int startYear,
			@Param("endYear") int endYear, @Param("startPeriod") int startPeriod, @Param("endPeriod") int endPeriod);
   	
   	
   	
}
