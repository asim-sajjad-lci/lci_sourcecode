/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAClassSetup;

/**
 * Description: Interface for FA Class Setup
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFAClassSetup")
public interface RepositoryFAClassSetup extends JpaRepository<FAClassSetup, Integer> 
{
	
	/**
	 * @param classId
	 * @return
	 */
	FAClassSetup findByClassIdAndIsDeleted(String classId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FAClassSetup fasc where fasc.isDeleted=false and fasc.classId like %:searchKeyWord% or fasc.classSetupDescription like %:searchKeyWord% or fasc.classSetupDescriptionArabic like %:searchKeyWord% or fasc.faAccountGroupsSetup.faAccountGroupId like %:searchKeyWord% or fasc.faInsuranceClassSetup.insuranceClassIndex like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select fasc from FAClassSetup fasc where fasc.isDeleted=false and fasc.classId like %:searchKeyWord% or fasc.classSetupDescription like %:searchKeyWord% or fasc.classSetupDescriptionArabic like %:searchKeyWord% or fasc.faAccountGroupsSetup.faAccountGroupId like %:searchKeyWord% or fasc.faInsuranceClassSetup.insuranceClassIndex like %:searchKeyWord% ")
	public List<FAClassSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select fasc from FAClassSetup fasc where fasc.isDeleted=false and fasc.classId like %:searchKeyWord% or fasc.classSetupDescription like %:searchKeyWord% or fasc.classSetupDescriptionArabic like %:searchKeyWord% or fasc.faAccountGroupsSetup.faAccountGroupId like %:searchKeyWord% or fasc.faInsuranceClassSetup.insuranceClassIndex like %:searchKeyWord% ")
	public List<FAClassSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);
}
