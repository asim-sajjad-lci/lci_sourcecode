/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FARetirementSetup;

/**
 * Description: Interface for Fixed assets retirement setup 
 * Name of Project: BTI
 * Created on: Aug 22, 2017
 * Modified on: Aug 22, 2017 3:50:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFARetirementSetup")
public interface RepositoryFARetirementSetup extends JpaRepository<FARetirementSetup, Integer>{

	/**
	 * @param faRetirementId
	 * @return
	 */
	FARetirementSetup findByRetirementIdAndIsDeleted(String faRetirementId, boolean deleted);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FARetirementSetup rs where rs.isDeleted=false and rs.retirementDescription like %:searchKeyWord% or rs.retirementDescriptionArabic like %:searchKeyWord%")
	Integer predictiveRetirementSetupSearchCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select rs from FARetirementSetup rs where rs.isDeleted=false and rs.retirementDescription like %:searchKeyWord% or rs.retirementDescriptionArabic like %:searchKeyWord% ")
	List<FARetirementSetup> predictiveRetirementSetupSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select rs from FARetirementSetup rs where rs.isDeleted=false and rs.retirementDescription like %:searchKeyWord% or rs.retirementDescriptionArabic like %:searchKeyWord% ")
	List<FARetirementSetup> predictiveRetirementSetupSearch(@Param("searchKeyWord") String searchKeyWord);

}
