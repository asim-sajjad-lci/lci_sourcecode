/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bti.model.PayableAccountClassSetup;

/**
 * Description: Interface for PayableAccountClassSetup 
 * Name of Project: BTI
 * Created on: September 07, 2017
 * Modified on: September 07, 2017 03:19:00 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryPayableAccountClassSetup")
public interface RepositoryPayableAccountClassSetup extends JpaRepository<PayableAccountClassSetup,String>{
      /**
     * @return
     */
    PayableAccountClassSetup findFirstByOrderByIdDesc();
     
      /**
     * @param accountRowIndex
     * @param deleted
     * @return
     */
    List<PayableAccountClassSetup> findByAccountRowIndexAndIsDeleted(int accountRowIndex,boolean deleted);

}
