/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterDocumentType;

/**
 * Description: Interface for MasterDocumentType 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryMasterDocumentType")
public interface RepositoryMasterDocumentType extends JpaRepository<MasterDocumentType, Integer> {
	

	
	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterDocumentType> findByLanguageLanguageIdAndIsDeletedAndMasterType(int langId,boolean deleted,String masterType);
	
}
