/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CustomerAccountTableSetup;

/**
 * Description: Interface for Customer Account Table Setup 
 * Name of Project: BTI
 * Created on: August 29 , 2017
 * Modified on: August 29 , 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCustomerAccountTableSetup")
public interface RepositoryCustomerAccountTableSetup extends JpaRepository<CustomerAccountTableSetup, Integer> {
	
	/**
	 * @param accountType
	 * @param custNumber
	 * @param deleted
	 * @return
	 */
	CustomerAccountTableSetup findByMasterArAccountTypeTypeIdAndCustomerMaintenanceCustnumber(Integer accountType, String custNumber);
	
	/**
	 * @param custNumber
	 * @param deleted
	 * @return
	 */
	List<CustomerAccountTableSetup> findByCustomerMaintenanceCustnumberAndIsDeleted(String custNumber, boolean deleted);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update CustomerAccountTableSetup d set d.isDeleted =:deleted where d.customerMaintenance.custnumber=:id")
	void deleteRecordById(@Param("id") String id, @Param("deleted") Boolean deleted);
	

	CustomerAccountTableSetup findByMasterArAccountTypeTypeIdAndCustomerMaintenanceCustnumberAndIsDeleted(int i,
			String customerNumber, boolean b);
	
	
}
