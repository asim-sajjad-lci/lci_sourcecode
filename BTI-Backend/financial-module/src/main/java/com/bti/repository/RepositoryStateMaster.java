/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.StateMaster;

/**
 * Description: Interface for RepositoryStateMaster 
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryStateMaster")
public interface RepositoryStateMaster extends JpaRepository<StateMaster, Integer> {

	/**
	 * @param countryId
	 * @param deleted
	 * @return
	 */
	List<StateMaster> findByCountryMasterCountryIdAndIsDeleted(int countryId, boolean deleted);
	
	/**
	 * @param stateName
	 * @return
	 */
	StateMaster findByStateName(String stateName);

	/**
	 * @param state
	 * @param countryCode
	 * @param deleted
	 * @return
	 */
	StateMaster findByStateNameAndCountryMasterCountryCodeAndIsDeleted(String state, String countryCode, boolean deleted);
	
	/**
	 * @param state
	 * @param countryCode
	 * @param langId
	 * @param isDeleted
	 * @return
	 */
	StateMaster findByStateCodeAndCountryMasterCountryCodeAndLanguageLanguageIdAndIsDeleted(String state, String countryCode,int langId, boolean isDeleted);
	

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<StateMaster> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	/**
	 * @param i
	 * @param b
	 * @return
	 */
	StateMaster findByStateIdAndIsDeleted(int i, boolean b);
	
	StateMaster findByStateCodeAndLanguageLanguageIdAndIsDeleted(String stateCode,int langId, boolean isDeleted);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
}


