/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.RMSetup;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryRMSetup")
public interface RepositoryRMSetup extends JpaRepository<RMSetup, Integer> {
	
	
	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	RMSetup findByIdAndIsDeleted(int id,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from RMSetup rms where rms.isDeleted=false and rms.checkbookMaintenance.checkBookId like %:searchKeyWord%  ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select rms from RMSetup rms where rms.isDeleted=false and rms.checkbookMaintenance.checkBookId like %:searchKeyWord%  ")
	public List<RMSetup> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select rms from RMSetup rms where rms.isDeleted=false and rms.checkbookMaintenance.checkBookId like %:searchKeyWord%   ")
	public List<RMSetup> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

}
