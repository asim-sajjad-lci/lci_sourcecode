/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ModulesConfigurationPeriod;

/**
 * Description: Interface for RepositoryModulesConfigurationPeriod 
 * Name of Project: BTI
 * Created on: Aug 04, 2017
 * Modified on: Aug 04, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryModulesConfigurationPeriod")
public interface RepositoryModulesConfigurationPeriod extends JpaRepository<ModulesConfigurationPeriod, Integer>{

	/**
	 * @param year
	 * @param seriesId
	 * @return
	 */
	List<ModulesConfigurationPeriod> findByYearAndSeriesTypeTypeIdAndIsDeleted(Integer year, Integer seriesId,boolean deleted);

	/**
	 * @param year
	 * @return
	 */
	List<ModulesConfigurationPeriod> findByYear(int year);

	/**
	 * @param year
	 * @param seriesId
	 * @param series
	 * @param deleted
	 * @return
	 */
	List<ModulesConfigurationPeriod> findByYearAndSeriesTypeTypeIdAndGlConfigurationDefinePeriodsDetailsSeriesAndIsDeleted(Integer year,
			Integer seriesId, int series,boolean deleted);

	/**
	 * @param year
	 * @param seriesId
	 * @param series
	 * @param originId
	 * @param deleted
	 * @return
	 */
	ModulesConfigurationPeriod findByYearAndSeriesTypeTypeIdAndGlConfigurationDefinePeriodsDetailsSeriesAndOriginIdAndIsDeleted(
			Integer year, Integer seriesId, int series, Integer originId,boolean deleted);
	
	List<ModulesConfigurationPeriod> findByYearAndSeriesTypeTypeIdAndOriginIdAndIsDeleted(
			Integer year, Integer seriesId,Integer originId,boolean deleted);

	ModulesConfigurationPeriod findByYearAndSeriesTypeTypeIdAndOriginIdAndClosePeriodAndGlConfigurationDefinePeriodsDetailsSeriesAndIsDeleted(
			Integer year, Integer seriesId,Integer originId,boolean closePeriod,int glSeries, boolean deleted);
 
	List<ModulesConfigurationPeriod> findByYearAndSeriesTypeTypeIdAndModuleDescriptionAndIsDeleted(Integer year, Integer seriesId,String periodName , boolean deleted);
 
	List<ModulesConfigurationPeriod> findByYearAndSeriesTypeTypeIdAndModuleDescriptionAndOriginIdAndIsDeleted(Integer year, Integer seriesId,String periodName ,int originId, boolean deleted);
}
