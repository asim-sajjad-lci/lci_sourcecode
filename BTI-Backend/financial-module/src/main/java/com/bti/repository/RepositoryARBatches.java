/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ARBatches;

/**
 * Description: Interface for ARBatches 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryARBatches")
public interface RepositoryARBatches extends JpaRepository<ARBatches,String>{
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from ARBatches arb where arb.isDeleted=false and "
			+ " ( arb.batchID like %:searchKeyWord% or arb.batchDescription like %:searchKeyWord% "
			+ " or arb.batchTransactionType.transactionType like %:searchKeyWord% )")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select arb from ARBatches arb where arb.isDeleted=false and "
			+ " ( arb.batchID like %:searchKeyWord% or arb.batchDescription like %:searchKeyWord% "
			+ " or arb.batchTransactionType.transactionType like %:searchKeyWord% ) ")
	public List<ARBatches> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select arb from ARBatches arb where arb.isDeleted=false and "
			+ " ( arb.batchID like %:searchKeyWord% or arb.batchDescription like %:searchKeyWord% "
			+ " or arb.batchTransactionType.transactionType like %:searchKeyWord% ) ")
	List<ARBatches> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

	ARBatches findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(String batchId, Integer transactionTypeId,
			boolean b);

	List<ARBatches> findByBatchTransactionTypeTypeIdAndIsDeleted(Integer transactionTypeId, boolean b);

	
}
