/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.APPaymentTransactions;
import com.bti.model.ARPaymentTransactions;

/**
 * Description: Interface for APPaymentTransactions 
 * Name of Project: BTI
 * Created on: Dec 4, 2017
 * Modified on: Dec 4, 2017 4:54:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryAPPaymentTransactions")
public interface RepositoryAPPaymentTransactions extends JpaRepository<APPaymentTransactions,Integer>{
	
	APPaymentTransactions findByApTransactionNumberAndVendorId(String transactionNumber, String vendorId);

}
