/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.BtiMessage;

/**
 * Description: Interface for RepositoryException 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryException")
public interface RepositoryException extends JpaRepository<BtiMessage, Integer> {

	
	/**
	 * @param message
	 * @param deleted
	 * @return
	 */
	public BtiMessage findByMessageShortAndIsDeleted(String message, boolean deleted);
	
	/**
	 * @param message
	 * @param langId
	 * @param deleted
	 * @return
	 */
	public BtiMessage findByMessageShortAndLanguageLanguageIdAndIsDeleted(String message, int langId,boolean deleted);
	
}
