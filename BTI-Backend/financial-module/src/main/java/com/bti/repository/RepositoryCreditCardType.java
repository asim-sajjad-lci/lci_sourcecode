/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.CreditCardType;

/**
 * Description: Interface for RepositoryCreditCardType 
 * Name of Project: BTI
 * Created on: Aug 17, 2017
 * Modified on: Aug 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCreditCardType")
public interface RepositoryCreditCardType extends JpaRepository<CreditCardType, Integer> {


	/**
	 * @param cardType
	 * @param deleted
	 * @return
	 */
	CreditCardType findTopOneByTypeIdAndIsDeleted(Integer cardType,boolean deleted);
	/**
	 * @param cardType
	 * @param langId
	 * @return
	 */
	CreditCardType findTopOneByTypeIdAndLanguageLanguageId(Integer cardType,int langId);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<CreditCardType> findByLanguageLanguageIdAndIsDeleted(int langId, boolean deleted);

	/**
	 * @param typeId
	 * @param langId
	 * @param deleted
	 * @return
	 */
	CreditCardType findByTypeIdAndLanguageLanguageIdAndIsDeleted(Integer typeId, int langId,boolean deleted);
	
	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<CreditCardType> findByIsDeletedAndLanguageLanguageId(boolean b, int i);
	
	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langId);
	
}


