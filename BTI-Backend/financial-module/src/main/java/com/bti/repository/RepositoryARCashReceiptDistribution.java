/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.ARCashReceiptDistribution;

@Repository("repositoryARCashReceiptDistribution")
public interface RepositoryARCashReceiptDistribution extends JpaRepository<ARCashReceiptDistribution, Integer>{

	List<ARCashReceiptDistribution> findByCashReceiptNumber(String cashReceiptNumber);
}
