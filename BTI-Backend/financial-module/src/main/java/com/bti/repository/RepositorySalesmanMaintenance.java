/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.SalesmanMaintenance;

/**
 * Description: Interface for RepositorySalesmanMaintenance 
 * Name of Project: BTI
 * Created on: Aug 08, 2017
 * Modified on: Aug 08, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositorySalesmanMaintenance")
public interface RepositorySalesmanMaintenance extends JpaRepository<SalesmanMaintenance, Integer> {

	
	/**
	 * @param salesmanId
	 * @param deleted
	 * @return
	 */
	SalesmanMaintenance findBySalesmanIdAndIsDeleted(String salesmanId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select sm from SalesmanMaintenance sm where sm.isDeleted=false and sm.salesmanId like %:searchKeyWord% or sm.salesmanFirstName like %:searchKeyWord% or sm.salesmanFirstNameArabic like %:searchKeyWord% or sm.salesTerritoryId like %:searchKeyWord% or sm.employeeId like %:searchKeyWord% ")
	List<SalesmanMaintenance> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from SalesmanMaintenance sm where sm.isDeleted=false and sm.salesmanId like %:searchKeyWord% or sm.salesmanFirstName like %:searchKeyWord% or sm.salesmanFirstNameArabic like %:searchKeyWord% or sm.salesTerritoryId like %:searchKeyWord% or sm.employeeId like %:searchKeyWord% ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select sm from SalesmanMaintenance sm where sm.isDeleted=false and sm.salesmanId like %:searchKeyWord% or sm.salesmanFirstName like %:searchKeyWord% or sm.salesmanFirstNameArabic like %:searchKeyWord% or sm.salesTerritoryId like %:searchKeyWord% or sm.employeeId like %:searchKeyWord% ")
	List<SalesmanMaintenance> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);
	 
	@Query("select count(*) from SalesmanMaintenance sm where sm.isDeleted=false and sm.inactive=0 and "
			+ "(sm.salesmanId like %:searchKeyWord% or sm.salesmanFirstName like %:searchKeyWord% or "
			+ "sm.salesmanFirstNameArabic like %:searchKeyWord% or "
			+ "sm.salesTerritoryId like %:searchKeyWord% or sm.employeeId like %:searchKeyWord% ) ")
	public int predictiveActiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select sm from SalesmanMaintenance sm where sm.isDeleted=false and sm.inactive=0 and "
			+ " (sm.salesmanId like %:searchKeyWord% or sm.salesmanFirstName like %:searchKeyWord% or "
			+ "sm.salesmanFirstNameArabic like %:searchKeyWord% or "
			+ "sm.salesTerritoryId like %:searchKeyWord% or sm.employeeId like %:searchKeyWord%) ")
	List<SalesmanMaintenance> predictiveActiveSearch(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select sm from SalesmanMaintenance sm where sm.isDeleted=false and sm.inactive=0 and "
			+ "(sm.salesmanId like %:searchKeyWord% or sm.salesmanFirstName like %:searchKeyWord% or "
			+ "sm.salesmanFirstNameArabic like %:searchKeyWord% or "
			+ "sm.salesTerritoryId like %:searchKeyWord% or sm.employeeId like %:searchKeyWord% ) ")
	List<SalesmanMaintenance> predictiveActiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	
	 
	 
}


