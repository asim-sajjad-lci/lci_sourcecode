package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.ReportProfitAndLoss;


@Repository("repositoryReportProfitAndLoss")
public interface RepossitoryReportProfitAndLoss extends JpaRepository<ReportProfitAndLoss,Integer> {

	@Query("SELECT RPAL FROM ReportProfitAndLoss RPAL WHERE RPAL.AccountCategoryIndex IN :index and RPAL.period <= :period")
	List<ReportProfitAndLoss> findByAccountCategoryIndex(@Param("index") int accountsCategoriesIds,@Param("period") int period );
}
