/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.VendorMaintenance;

@Repository("repositoryVendorMaintenance")
public interface RepositoryVendorMaintenance extends JpaRepository<VendorMaintenance, String> {

	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select v from VendorMaintenance v where v.isDeleted=false and (v.vendorid like %:searchKeyWord% or v.vendorName like %:searchKeyWord% or v.vendorNameArabic like %:searchKeyWord% or v.vendorShortName like %:searchKeyWord% or v.address1 like %:searchKeyWord% or v.phoneNumber1 like %:searchKeyWord%)")
	public List<VendorMaintenance> predictiveVendorSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select v from VendorMaintenance v where v.isDeleted=false and (v.vendorid like %:searchKeyWord% or v.vendorName like %:searchKeyWord% or v.vendorNameArabic like %:searchKeyWord% or v.vendorShortName like %:searchKeyWord% or v.address1 like %:searchKeyWord% or v.phoneNumber1 like %:searchKeyWord%)")
	public List<VendorMaintenance> predictiveVendorSearch(@Param("searchKeyWord") String searchKeyWord);

	
	/**
	 * @param vendorId
	 * @param deleted
	 * @return
	 */
	public VendorMaintenance findByVendoridAndIsDeleted(String vendorId,boolean deleted);
	
	/**
	 * @param deleted
	 * @return
	 */
	public List<VendorMaintenance> findByIsDeleted(boolean deleted);
	
	@Query("select v from VendorMaintenance v where v.isDeleted=false and vendorHoldStatus=0 and masterVendorStatus.statusId=1 "
			+ "and (v.vendorid like %:searchKeyWord% or v.vendorName like %:searchKeyWord% or "
			+ "v.vendorNameArabic like %:searchKeyWord% or v.vendorShortName like %:searchKeyWord% or "
			+ "v.address1 like %:searchKeyWord% or v.phoneNumber1 like %:searchKeyWord%)")
	public List<VendorMaintenance> predictiveActiveVendorSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select v from VendorMaintenance v where v.isDeleted=false and vendorHoldStatus=0 and masterVendorStatus.statusId=1 "
			+ "and (v.vendorid like %:searchKeyWord% or v.vendorName like %:searchKeyWord% or "
			+ "v.vendorNameArabic like %:searchKeyWord% or v.vendorShortName like %:searchKeyWord% or "
			+ "v.address1 like %:searchKeyWord% or v.phoneNumber1 like %:searchKeyWord%)")
	public List<VendorMaintenance> predictiveActiveVendorSearch(@Param("searchKeyWord") String searchKeyWord);
	
	
	@Query("select count(*) from VendorMaintenance v where v.isDeleted=false and vendorHoldStatus=0 and masterVendorStatus.statusId=1 "
			+ "and (v.vendorid like %:searchKeyWord% or v.vendorName like %:searchKeyWord% or "
			+ "v.vendorNameArabic like %:searchKeyWord% or v.vendorShortName like %:searchKeyWord% or "
			+ "v.address1 like %:searchKeyWord% or v.phoneNumber1 like %:searchKeyWord%)")
	public int predictiveActiveVendorSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	@Query("select count(*) from VendorMaintenance v where v.isDeleted=false "
			+ "and (v.vendorid like %:searchKeyWord% or v.vendorName like %:searchKeyWord% or "
			+ "v.vendorNameArabic like %:searchKeyWord% or v.vendorShortName like %:searchKeyWord% or "
			+ "v.address1 like %:searchKeyWord% or v.phoneNumber1 like %:searchKeyWord%)")
	public int predictiveVendorSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
}
