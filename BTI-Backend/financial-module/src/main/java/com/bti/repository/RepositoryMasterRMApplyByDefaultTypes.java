/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.MasterRMApplyByDefaultTypes;

@Repository("repositoryMasterRMApplyByDefaultTypes")
public interface RepositoryMasterRMApplyByDefaultTypes extends JpaRepository<MasterRMApplyByDefaultTypes, Integer>{
	
	/**
	 * @param typeId
	 * @param langid
	 * @param deleted
	 * @return
	 */
	MasterRMApplyByDefaultTypes findByTypeIdAndLanguageLanguageIdAndIsDeleted(int typeId,int langid,boolean deleted);

	/**
	 * @param langId
	 * @param deleted
	 * @return
	 */
	List<MasterRMApplyByDefaultTypes> findByLanguageLanguageIdAndIsDeleted(int langId,boolean deleted);

	/**
	 * @param b
	 * @param i
	 * @return
	 */
	List<MasterRMApplyByDefaultTypes> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	long countByIsDeletedAndLanguageLanguageId(boolean b, Integer langid);
}
