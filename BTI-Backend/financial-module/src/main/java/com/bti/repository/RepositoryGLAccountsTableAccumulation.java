/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLYTDOpenTransactions;

/**
 * Description: Interface for GLAccountsTableAccumulation 
 * Name of Project: BTI
 * Created on: August 24, 2017
 * Modified on: August 24, 2017 10:08:38 AM
 * @author seasia
 * Version: 
 */
@Repository("repositoryGLAccountsTableAccumulation")
public interface RepositoryGLAccountsTableAccumulation extends JpaRepository<GLAccountsTableAccumulation, Integer> {
	
	
	/**
	 * @param indexId
	 * @param deleted
	 * @return
	 */
	GLAccountsTableAccumulation findByAccountTableRowIndexAndIsDeleted(String indexId, boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from GLAccountsTableAccumulation glc where glc.isDeleted=false and glc.accountTableRowIndex like %:searchKeyWord%  ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select glc from GLAccountsTableAccumulation glc where glc.isDeleted=false and glc.accountTableRowIndex like %:searchKeyWord%  ")
	public List<GLAccountsTableAccumulation> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select glc from GLAccountsTableAccumulation glc where glc.isDeleted=false and glc.accountTableRowIndex like %:searchKeyWord%  ")
	public List<GLAccountsTableAccumulation> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);
	
	
	/**
	 * @param accountNumberfullIndex
	 * @param deleted
	 * @return
	 */
	List<GLAccountsTableAccumulation> findByAccountNumberFullIndexAndIsDeleted(String accountNumberfullIndex, boolean deleted);

	@Query("select glc from GLAccountsTableAccumulation glc where glc.isDeleted=false and "
			+ " glc.accountTableRowIndex NOT IN (:accountTableRowIndexList)")
	List<GLAccountsTableAccumulation> getAccountTableRowIndexListWhereNotInUsed(@Param("accountTableRowIndexList") List<String> accountTableRowIndexList);



	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(DISTINCT a.accountTableRowIndex ) from GLAccountsTableAccumulation a where (a.accountDescription like %:searchKeyWord% or a.accountDescriptionArabic like %:searchKeyWord% or a.accountNumber like %:searchKeyWord%  ) and a.isDeleted=false")
	public Integer predictiveGLAccountsTableAccumulationSearchTotalCount(@Param("searchKeyWord")String searchkeyWord);
	
	
	@Query("select DISTINCT a from GLAccountsTableAccumulation a where (a.accountDescription like %:searchKeyWord% or a.accountDescriptionArabic like %:searchKeyWord% or a.accountNumber like %:searchKeyWord%  ) and a.isDeleted=false ")
	public List<GLAccountsTableAccumulation> predictiveJournalSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	@Query("select DISTINCT a  from GLAccountsTableAccumulation a where (a.accountDescription like %:searchKeyWord% or a.accountDescriptionArabic like %:searchKeyWord% or a.accountNumber like %:searchKeyWord% ) and a.isDeleted=false")
	public List<GLAccountsTableAccumulation> predictiveJournalSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	

}
