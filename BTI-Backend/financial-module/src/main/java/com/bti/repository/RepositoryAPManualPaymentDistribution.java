/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.APManualPaymentDistribution;

@Repository("repositoryAPManualPaymentDistribution")
public interface RepositoryAPManualPaymentDistribution extends JpaRepository<APManualPaymentDistribution, Integer>{

	List<APManualPaymentDistribution> findByManualPaymentNumber(String manualPaymentNumber);
}
