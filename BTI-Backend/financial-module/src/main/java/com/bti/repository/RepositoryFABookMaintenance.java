/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FABookMaintenance;

/**
 * Description: Interface for RepositoryFABookMaintenance 
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFABookMaintenance")
public interface RepositoryFABookMaintenance extends JpaRepository<FABookMaintenance, Integer> {
	
	/**
	 * @param assetId
	 * @return
	 */
	FABookMaintenance findByAssetIdAndIsDeleted(String assetId,boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FABookMaintenance fabm where fabm.isDeleted=false and "
			+ " ( fabm.assetId like %:searchKeyWord% or fabm.bookSetup.bookId like %:searchKeyWord%) and "
			+ " fabm.assetId IN ( select gm.assetId from FAGeneralMaintenance gm where gm.isDeleted=false and "
			+ " gm.masterGeneralMaintenanceAssetStatus.typeId=1 ) ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select fabm from FABookMaintenance fabm where fabm.isDeleted=false and "
			+ " ( fabm.assetId like %:searchKeyWord% or fabm.bookSetup.bookId like %:searchKeyWord% ) and  "
			+ " fabm.assetId IN ( select gm.assetId from FAGeneralMaintenance gm where gm.isDeleted=false and "
			+ " gm.masterGeneralMaintenanceAssetStatus.typeId=1) ")
	public List<FABookMaintenance> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select fabm from FABookMaintenance fabm where fabm.isDeleted=false and "
			+ " ( fabm.assetId like %:searchKeyWord% or fabm.bookSetup.bookId like %:searchKeyWord%) and "
			+ " fabm.assetId IN ( select gm.assetId from FAGeneralMaintenance gm where gm.isDeleted=false and "
			+ " gm.masterGeneralMaintenanceAssetStatus.typeId=1)")
	public List<FABookMaintenance> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

}
