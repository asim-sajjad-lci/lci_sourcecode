/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FAPropertyTypes;

/**
 * Description: Interface for FA Property Types
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("RepositoryFAPropertyTypes")
public interface RepositoryFAPropertyTypes extends JpaRepository<FAPropertyTypes, Integer> {
	
	/**
	 * @param classId
	 * @return
	 */
	FAPropertyTypes findByPropertyTypeIdAndIsDeleted(int classId , boolean deleted);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from FAPropertyTypes fapt where fapt.isDeleted=false and fapt.propertyName like %:searchKeyWord%  ")
	public int predictiveSearchCount(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select fapt from FAPropertyTypes fapt where fapt.isDeleted=false and fapt.propertyName like %:searchKeyWord% ")
	public List<FAPropertyTypes> predictiveSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);
	
	/**
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select fapt from FAPropertyTypes fapt where fapt.isDeleted=false and fapt.propertyName like %:searchKeyWord% ")
	public List<FAPropertyTypes> predictiveSearch(@Param("searchKeyWord") String searchKeyWord);

}
