/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.AccountCurrencyAccess;
import com.bti.model.COAFinancialDimensions;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.FixedAllocationAccountIndexHeader;
import com.bti.model.dto.DtoAccountCurrencyAccess;
import com.bti.model.dto.DtoFinancialDimensionSetUp;
import com.bti.model.dto.DtoFinancialDimensionValue;
import com.bti.model.dto.DtoMainAccountSetUp;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryAccountCurrencyAccess;
import com.bti.repository.RepositoryCOAFinancialDimensions;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryFixedAllocationAccountIndexHeader;
import com.bti.service.ServiceAccountPayable;
import com.bti.service.ServiceFinancialDimension;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceMainAccountTypes;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;
import com.bti.util.UtilRandomKey;

/**
 * Description: ControllerGeneralLedgerSetup
 * Name of Project: BTI
 * Created on: Aug 04, 2017
 * Modified on: Aug 04, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/setup/generalLedgerMaster")
public class ControllerGeneralLedgerMaster {
	
	private static final Logger LOGGER = Logger.getLogger(ControllerGeneralLedgerMaster.class);
	
	@Autowired
	ServiceFinancialDimension serviceFinancialDimension;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	ServiceMainAccountTypes serviceMainAccountTypes;
	
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	
	@Autowired
	RepositoryAccountCurrencyAccess repositoryAccountCurrencyAccess; 
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired
	RepositoryFixedAllocationAccountIndexHeader repositoryFixedAllocationAccountIndexHeader;
	
	@Autowired
	RepositoryCOAFinancialDimensions repositoryCOAFinancialDimensions;
	
	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;

	/**
	 * @description setup general Ledger Master financial Dimension Setup
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 
	 */
	@RequestMapping(value = "/financialDimensionSetup", method = RequestMethod.POST)
	public ResponseMessage financialDimensionSetup(HttpServletRequest request,
			@RequestBody DtoFinancialDimensionSetUp dtoFinancialDimensionSetUp)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFinancialDimensionSetUp); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		if (UtilRandomKey.isNotBlank(dtoFinancialDimensionSetUp.getDimensionName())
				|| UtilRandomKey.isNotBlank(dtoFinancialDimensionSetUp.getDimensionMask())) {
			dtoFinancialDimensionSetUp = this.serviceFinancialDimension
					.saveFinancialDimensionSetup(dtoFinancialDimensionSetUp);
			if (dtoFinancialDimensionSetUp.getDimInxd() > 0) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_SETUP_SUCCESS, false),
						dtoFinancialDimensionSetUp);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_SETUP_FAIL, false),
						null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NO_CONTENT.value(), HttpStatus.NO_CONTENT,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master financial Dimension get by id
	 * @param request
	 * @param dtoFinancialDimensionSetUp
	 * @return
	 */
	@RequestMapping(value = "/financialDimensionSetupGetById", method = RequestMethod.POST)
	public ResponseMessage financialDimensionSetupGetById(HttpServletRequest request,
			@RequestBody DtoFinancialDimensionSetUp dtoFinancialDimensionSetUp)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoFinancialDimensionSetUp = this.serviceFinancialDimension
				.financialDimensionSetupGetById(dtoFinancialDimensionSetUp.getDimInxd());
		if (dtoFinancialDimensionSetUp != null && dtoFinancialDimensionSetUp.getDimInxd() > 0) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoFinancialDimensionSetUp);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master financial Dimension Setup update
	 * @param request
	 * @param dtoFinancialDimensionSetUp
	 * @return
	 
	 */
	@RequestMapping(value = "/financialDimensionSetupUpdate", method = RequestMethod.POST)
	public ResponseMessage financialDimensionSetupUpdate(HttpServletRequest request,
			@RequestBody DtoFinancialDimensionSetUp dtoFinancialDimensionSetUp)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFinancialDimensionSetUp); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (UtilRandomKey.isNotBlank(dtoFinancialDimensionSetUp.getDimensionName())
				|| UtilRandomKey.isNotBlank(dtoFinancialDimensionSetUp.getDimensionMask())) {
			COAFinancialDimensions coaFinancialDimensions = repositoryCOAFinancialDimensions
					.findByDimInxdAndIsDeleted(dtoFinancialDimensionSetUp.getDimInxd(),false);
			if (coaFinancialDimensions != null) {
				dtoFinancialDimensionSetUp = this.serviceFinancialDimension
						.financialDimensionSetupUpdate(dtoFinancialDimensionSetUp, coaFinancialDimensions);
				if (dtoFinancialDimensionSetUp.getDimInxd() > 0) {
					responseMessage = new ResponseMessage(
							HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_SETUP_UPDATE_SUCCESS, false),
							dtoFinancialDimensionSetUp);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NO_CONTENT.value(), HttpStatus.NO_CONTENT,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master financial Dimension Setup get all or search
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/searchFinancialDimensionSetup", method = RequestMethod.POST)
	public ResponseMessage searchFinancialDimensionSetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		dtoSearch = serviceFinancialDimension.searchFinancialDimensionSetup(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoFinancialDimensionSetUp> financialDimensionSetUpList = (List<DtoFinancialDimensionSetUp>) dtoSearch
					.getRecords();
				if (financialDimensionSetUpList != null && !financialDimensionSetUpList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master financial Dimension Value
	 * @param request
	 * @param dtoFinancialDimensionValue
	 * @return
	 */
	@RequestMapping(value = "/financialDimensionValue", method = RequestMethod.POST)
	public ResponseMessage financialDimensionValue(HttpServletRequest request,
			@RequestBody DtoFinancialDimensionValue dtoFinancialDimensionValue)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFinancialDimensionValue); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (UtilRandomKey.isNotBlank(dtoFinancialDimensionValue.getDimensionValue())) {
			COAFinancialDimensions coaFinancialDimensions = repositoryCOAFinancialDimensions
					.findByDimInxdAndIsDeleted(dtoFinancialDimensionValue.getDimInxd(),false);
			if (coaFinancialDimensions != null) {
				COAFinancialDimensionsValues coaFinancialDimensionsValues = repositoryCOAFinancialDimensionsValues
						.findByDimensionValueAndCoaFinancialDimensionsDimInxdAndIsDeleted(
								dtoFinancialDimensionValue.getDimensionValue(),
								dtoFinancialDimensionValue.getDimInxd(),false);
				if (coaFinancialDimensionsValues == null) {
					dtoFinancialDimensionValue = this.serviceFinancialDimension
							.saveFinancialDimensionValue(dtoFinancialDimensionValue, coaFinancialDimensions);
					if (dtoFinancialDimensionValue.getDimInxValue() > 0) {
						responseMessage = new ResponseMessage(
								HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse
										.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_VALUE_SUCCESS, false),
								dtoFinancialDimensionValue);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
								this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_VALUE_FAIL,
										false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_VALUE_ALREADY_USED,
									false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_VALUE_FAIL, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_VALUE_FAIL, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master financial Dimension Value get by id
	 * @param request
	 * @param dtoFinancialDimensionValue
	 * @return
	 
	 */
	@RequestMapping(value = "/financialDimensionValueGetById", method = RequestMethod.POST)
	public ResponseMessage financialDimensionValueGetById(HttpServletRequest request,
			@RequestBody DtoFinancialDimensionValue dtoFinancialDimensionValue)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoFinancialDimensionValue = this.serviceFinancialDimension
				.financialDimensionValueGetById(dtoFinancialDimensionValue.getDimInxValue());
		if (dtoFinancialDimensionValue != null && dtoFinancialDimensionValue.getDimInxValue() > 0) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoFinancialDimensionValue);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master main Account Set Up
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 
	 */
	@RequestMapping(value = "/mainAccountSetUp", method = RequestMethod.POST)
	public ResponseMessage mainAccountSetUp(HttpServletRequest request,
			@RequestBody DtoMainAccountSetUp dtoMainAccountSetUp)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoMainAccountSetUp); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoMainAccountSetUp = this.serviceMainAccountTypes.saveMainAccountSetup(dtoMainAccountSetUp);

		if (dtoMainAccountSetUp != null) {
			if (dtoMainAccountSetUp.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MAIN_ACCOUNT_SETUP_SUCCESS, false),
						dtoMainAccountSetUp);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(dtoMainAccountSetUp.getMessageType(), false),
						null);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MAIN_ACCOUNT_SETUP_FAIL, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
	
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master main Account update
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 
	 */
	@RequestMapping(value = "/updateMainAccountSetUp", method = RequestMethod.POST)
	public ResponseMessage updateMainAccountSetUp(HttpServletRequest request,
			@RequestBody DtoMainAccountSetUp dtoMainAccountSetUp)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoMainAccountSetUp); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoMainAccountSetUp = this.serviceMainAccountTypes.updateMainAccountSetup(dtoMainAccountSetUp);
		if (dtoMainAccountSetUp != null) {
			if (dtoMainAccountSetUp.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MAIN_ACCOUNT_SETUP_UPDATE_SUCCESS, false),
						dtoMainAccountSetUp);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(dtoMainAccountSetUp.getMessageType(), false),
						null);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MAIN_ACCOUNT_SETUP_UPDATE_FAIL, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master main Account get by id
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 
	 */
	@RequestMapping(value = "/getMainAccountSetupById", method = RequestMethod.POST)
	public ResponseMessage getMainAccountSetupById(HttpServletRequest request,
			@RequestBody DtoMainAccountSetUp dtoMainAccountSetUp)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		dtoMainAccountSetUp = this.serviceMainAccountTypes.getMainAccountSetupById(dtoMainAccountSetUp);
		if (dtoMainAccountSetUp != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoMainAccountSetUp);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master main Account get all or search
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/searchMainAccountSetup", method = RequestMethod.POST)
	public ResponseMessage searchMainAccountSetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceMainAccountTypes.searchMainAccountSetup(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoMainAccountSetUp> dtoMainAccountSetUpList = (List<DtoMainAccountSetUp>) dtoSearch.getRecords();
			if (dtoMainAccountSetUpList != null && !dtoMainAccountSetUpList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master financial Dimension Value Update
	 * @param request
	 * @param dtoFinancialDimensionValue
	 * @return
	 */
	@RequestMapping(value = "/financialDimensionValueUpdate", method = RequestMethod.POST)
	public ResponseMessage financialDimensionValueUpdate(HttpServletRequest request,
			@RequestBody DtoFinancialDimensionValue dtoFinancialDimensionValue)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFinancialDimensionValue); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		if (UtilRandomKey.isNotBlank(dtoFinancialDimensionValue.getDimensionValue())) {
			COAFinancialDimensionsValues coaFinancialDimensionsValues = repositoryCOAFinancialDimensionsValues
					.findByDimInxValueAndIsDeleted(dtoFinancialDimensionValue.getDimInxValue(),false);
			if (coaFinancialDimensionsValues != null) {
				COAFinancialDimensions coaFinancialDimensions = repositoryCOAFinancialDimensions
						.findByDimInxdAndIsDeleted(dtoFinancialDimensionValue.getDimInxd(),false);
				if (coaFinancialDimensions != null) {
					COAFinancialDimensionsValues coaFinancialDimensionsValues2 = repositoryCOAFinancialDimensionsValues
							.findByDimensionValueAndCoaFinancialDimensionsDimInxdAndCoaDimensionValueIndexNotEqual(
									dtoFinancialDimensionValue.getDimensionValue(),
									dtoFinancialDimensionValue.getDimInxd(),
									dtoFinancialDimensionValue.getDimInxValue());
					if (coaFinancialDimensionsValues2 == null) {
						dtoFinancialDimensionValue = this.serviceFinancialDimension.financialDimensionValueUpdate(
								dtoFinancialDimensionValue, coaFinancialDimensions, coaFinancialDimensionsValues);
						if (dtoFinancialDimensionValue != null && dtoFinancialDimensionValue.getDimInxValue() > 0) {
							responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
									serviceResponse.getMessageByShortAndIsDeleted(
												MessageLabel.FINANCIAL_DIMENSION_VALUE_UPDATE_SUCCESS, false),
									dtoFinancialDimensionValue);
						} else {
								responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(),
										HttpStatus.NOT_FOUND, serviceResponse
												.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
						}
					} else {
							responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(),
									HttpStatus.BAD_REQUEST, this.serviceResponse.getMessageByShortAndIsDeleted(
											MessageLabel.FINANCIAL_DIMENSION_VALUE_ALREADY_USED, false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
								this.serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.FINANCIAL_DIMENSION_VALUE_FAIL, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FINANCIAL_DIMENSION_VALUE_EMPTY,
								false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master financial Dimension Value get all or search
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/searchFinancialDimensionValue", method = RequestMethod.POST)
	public ResponseMessage searchFinancialDimensionValue(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceFinancialDimension.searchFinancialDimensionValue(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoFinancialDimensionValue> financialDimensionValueList = (List<DtoFinancialDimensionValue>) dtoSearch
					.getRecords();
			if (financialDimensionValueList != null && !financialDimensionValueList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master get Typical Balance Type Status
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getTypicalBalanceTypeStatus", method = RequestMethod.POST)
	public ResponseMessage getTypicalBalanceTypeStatus(HttpServletRequest request)  {
		LOGGER.info("inside getTypicalBalanceTypeStatus method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceMainAccountTypes.getTypicalBalanceTypeStatus();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master add account Currency Access
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 
	 */
	@RequestMapping(value = "/accountCurrencyAccess/add", method = RequestMethod.POST)
	public ResponseMessage saveAccountCurrencyAccess(HttpServletRequest request,
			@RequestBody DtoAccountCurrencyAccess dtoAccountCurrencyAccess) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAccountCurrencyAccess); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		dtoAccountCurrencyAccess = this.serviceMainAccountTypes.saveAccountCurrencyAccess(dtoAccountCurrencyAccess);

		if (dtoAccountCurrencyAccess != null) {
				responseMessage = new ResponseMessage(
						HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse
					.getMessageByShortAndIsDeleted(dtoAccountCurrencyAccess.getMessageType(), false),
					dtoAccountCurrencyAccess);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_CURRENCY_ACCESS_FAIL,
								false),
						null);
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master get account Currency Access
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 
	 */
	@RequestMapping(value = "/getAccountCurrencyAccess", method = RequestMethod.GET)
	public ResponseMessage getAccountCurrencyAccessById(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		List<AccountCurrencyAccess> accountCurrencyAccessList = repositoryAccountCurrencyAccess.findAll();
			if (accountCurrencyAccessList != null && !accountCurrencyAccessList.isEmpty()) {
			DtoAccountCurrencyAccess dtoAccountCurrencyAccess = this.serviceMainAccountTypes
					.getAccountCurrencyAccess(accountCurrencyAccessList.get(0));
			if (dtoAccountCurrencyAccess != null) {
					responseMessage = new ResponseMessage(
							HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse
						.getMessageByShortAndIsDeleted(dtoAccountCurrencyAccess.getMessageType(), false),
						dtoAccountCurrencyAccess);
			} else {
					responseMessage = new ResponseMessage(
							HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_CURRENCY_ACCESS_FAIL, false),
						null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master save Fixed Allocation Detail
	 * @param request
	 * @param dtoPayableAccountClassSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/saveFixedAllocationDetail", method = RequestMethod.POST)
	public ResponseMessage saveFixedAllocationDetail(HttpServletRequest request,
			@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup)  {
		LOGGER.info("inside save fixed allocation detail  method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPayableAccountClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPayableAccountClassSetup = serviceAccountPayable.saveFixedAllocationAccount(dtoPayableAccountClassSetup);
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FIXED_ALLOCATION_ACCOUNT_SAVE, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(dtoPayableAccountClassSetup.getMessageType(), false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master fixed Allocation Detail get By Account Number
	 * @param request
	 * @param dtoPayableAccountClassSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/fixedAllocationDetail/getByAccountNumber", method = RequestMethod.POST)
	public ResponseMessage fixedAllocationDetailGetByAccountNumber(HttpServletRequest request,
			@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup)  {
		LOGGER.info("inside get fixed allocation detail by account number method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		FixedAllocationAccountIndexHeader fixedAllocationAccountIndexHeader = repositoryFixedAllocationAccountIndexHeader
				.findByCoaMainAccountsActIndxAndIsDeleted(dtoPayableAccountClassSetup.getMainAccountID(),false);
		if (fixedAllocationAccountIndexHeader != null) {
			dtoPayableAccountClassSetup = serviceAccountPayable
					.getFixedAllocationAccountByAccountIndexHeaderNumber(fixedAllocationAccountIndexHeader);
			if (dtoPayableAccountClassSetup.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoPayableAccountClassSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(dtoPayableAccountClassSetup.getMessageType(),
								false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_NUMBER_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master get all fixed Allocation Detail
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/fixedAllocationDetail/getAll", method = RequestMethod.GET)
	public ResponseMessage fixedAllocationDetailGetAll(HttpServletRequest request)  {
		LOGGER.info("inside get fixed allocation detail method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoPayableAccountClassSetup> dtoPayableAccountClassSetupList = serviceAccountPayable
				.getFixedAllocationAccount();
		if (dtoPayableAccountClassSetupList != null && !dtoPayableAccountClassSetupList.isEmpty()) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoPayableAccountClassSetupList);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description setup general Ledger Master main Account get all or search
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/searchActiveMainAccountSetup", method = RequestMethod.POST)
	public ResponseMessage searchActiveMainAccountSetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceMainAccountTypes.searchActiveMainAccountSetup(dtoSearch);
			if (dtoSearch.getRecords() != null) {
				@SuppressWarnings("unchecked")
				List<DtoMainAccountSetUp> dtoMainAccountSetUpList = (List<DtoMainAccountSetUp>) dtoSearch.getRecords();
				if (dtoMainAccountSetUpList != null && !dtoMainAccountSetUpList.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/accountCurrencyAccess/getById", method = RequestMethod.POST)
	public ResponseMessage getAccountCurrencyAccess(HttpServletRequest request,@RequestBody DtoAccountCurrencyAccess dtoAccountCurrencyAccessData) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<AccountCurrencyAccess> accountCurrencyAccessList = repositoryAccountCurrencyAccess.findByCoaMainAccountsActIndx(dtoAccountCurrencyAccessData.getAccountIndex());
			if (accountCurrencyAccessList != null && !accountCurrencyAccessList.isEmpty()) {
				DtoAccountCurrencyAccess dtoAccountCurrencyAccess = this.serviceMainAccountTypes
						.getAccountCurrencyAccess(accountCurrencyAccessList.get(0));
				if (dtoAccountCurrencyAccess != null) {
					responseMessage = new ResponseMessage(
							HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse
									.getMessageByShortAndIsDeleted(dtoAccountCurrencyAccess.getMessageType(), false),
							dtoAccountCurrencyAccess);
				} 
				else 
				{
					responseMessage = new ResponseMessage(
							HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_CURRENCY_ACCESS_FAIL, false),
							null);
				}
			} else {
				DtoAccountCurrencyAccess dtoAccountCurrencyAccess=null;
				COAMainAccounts coaMainAccounts=repositoryCOAMainAccounts.findByActIndxAndIsDeleted(dtoAccountCurrencyAccessData.getAccountIndex(), false);
				if(coaMainAccounts!=null){
					dtoAccountCurrencyAccess=new DtoAccountCurrencyAccess();
					dtoAccountCurrencyAccess.setAccountDescription(coaMainAccounts.getMainAccountDescription());
					dtoAccountCurrencyAccess.setAccountDescriptionArabic(coaMainAccounts.getMainAccountDescriptionArabic());
					
				}
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false),dtoAccountCurrencyAccess);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
}
