/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoVendor;
import com.bti.model.dto.DtoVendorMaintenanceOptions;
import com.bti.model.dto.DtoVendorStatus;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.service.ServiceVender;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerAccountPayableMaster
 * Name of Project: BTI
 * Created on: Aug 09, 2017
 * Modified on: Aug 09, 2017 02:35:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/setup/accountPayableMaster")
public class ControllerAccountPayableMaster {

	private static final Logger LOGGER = Logger.getLogger(ControllerAccountPayableMaster.class);
	
	@Autowired
	ServiceVender serviceVender;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @Description This service will add new vendor
	 * @param dtoVendor
	 * @return
	 *  
	 */
	@RequestMapping(value = "/add/vender", method = RequestMethod.POST)
	public ResponseMessage addAndUpdateVendor(@RequestBody DtoVendor dtoVendor) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoVendor); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoVendor != null) {
				boolean isVenderIdExist = serviceVender.getVenderByVenderId(dtoVendor.getVenderId());
				if (isVenderIdExist) {
				dtoVendor = serviceVender.saveOrUpdateVender(dtoVendor);
					responseMessage = new ResponseMessage(
							HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.VENDER_SETUP_CREATED_SUCCESS, false),
						dtoVendor);
			} else {
				dtoVendor = serviceVender.saveOrUpdateVender(dtoVendor);
					responseMessage = new ResponseMessage(
							HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.VENDOR_SETUP_UPDATED_SUCCESS, false),
						dtoVendor);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VENDER_SETUP_FAIL, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will get vendor
	 * @param dtoVendor
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getOne/vender", method = RequestMethod.POST)
	public ResponseMessage getVendor(@RequestBody DtoVendor dtoVendor)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoVendor.getVenderId() != null) {
			boolean isVenderIdExist = serviceVender.getVenderByVenderId(dtoVendor.getVenderId());
			if (isVenderIdExist) {
				dtoVendor = serviceVender.getVendorById(dtoVendor.getVenderId());
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoVendor);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VENDER_NOT_AVAILABLE, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.CREATED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VENDER_ID_REQUIRED, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will get all and search any specific vendor
	 * @param dtoSearch
	 * @return
	 *  
	 */
	@RequestMapping(value = "/find/vender", method = RequestMethod.POST)
	public ResponseMessage getAllOrSearchVendors(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			if (dtoSearch!=null) {
				dtoSearch = serviceVender.getAllOrSearchVendor(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		return responseMessage;
	}
	
	/**
	 * @Description This service will return vendor status drop down list
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getVendorStatus", method = RequestMethod.GET)
	public ResponseMessage getDisplayType() {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoVendorStatus> listObj = serviceVender.getAllVendorStatus();
		if(!listObj.isEmpty()){
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse
					.getMessageByShortAndIsDeleted(MessageLabel.VENDOR_STATUS_FETCHED_SUCCESSFULLY, false), listObj);
		}else{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will add vendor maintenance options
	 * @param request
	 * @param dtoVendorMaintenanceOptions
	 * @return
	 *  
	 */
	@RequestMapping(value = "/vendorMaintenanceOptions", method = RequestMethod.POST)
	public ResponseMessage vendorMaintenanceOptions(HttpServletRequest request, @RequestBody DtoVendorMaintenanceOptions dtoVendorMaintenanceOptions) {
		LOGGER.info("inside vendorMaintenanceOptions method");
		ResponseMessage responseMessage = null;
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoVendorMaintenanceOptions); 
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoVendorMaintenanceOptions != null) {
			DtoVendorMaintenanceOptions response = serviceVender.saveVendorMaintenanceOptions(dtoVendorMaintenanceOptions);
			if (response != null) {
				String message = response.getMessageType();
				response.setMessageType(null);
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
						.getMessageByShortAndIsDeleted(message, false),
						response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VENDOR_MAINTENANCE_OPTION_FAIL, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will get vendor maintenance options
	 * @param request
	 * @return
	 *  
	 */
	@RequestMapping(value = "/getVendorMaintenanceOptions", method = RequestMethod.GET)
	public ResponseMessage getVendorMaintenanceOptions(HttpServletRequest request) 
	{
		LOGGER.info("inside getVendorMaintenanceOptions method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		DtoVendorMaintenanceOptions dtoVendorMaintenanceOptions = serviceVender.getVendorMaintenanceOptions();
			if(dtoVendorMaintenanceOptions!=null )
			{
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoVendorMaintenanceOptions);
		}
			else
			{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoVendorMaintenanceOptions
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getVendorMaintenanceOptionsByVendorId", method = RequestMethod.POST)
	public ResponseMessage getVendorMaintenanceOptionsByVendorId(HttpServletRequest request, @RequestBody DtoVendorMaintenanceOptions dtoVendorMaintenanceOptions) 
	{
		LOGGER.info("inside getVendorMaintenanceOptions method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoVendorMaintenanceOptions = serviceVender.getVendorMaintenanceOptionsByVendorId(dtoVendorMaintenanceOptions.getVendorId());
		if(dtoVendorMaintenanceOptions!=null ){
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoVendorMaintenanceOptions);
		}
		else{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/find/Activevender", method = RequestMethod.POST)
	public ResponseMessage getAllOrSearchActiveVendors(@RequestBody DtoSearch dtoSearch) 
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			if (dtoSearch!=null) {
				dtoSearch = serviceVender.getAllOrSearchActiveVendor(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
}
