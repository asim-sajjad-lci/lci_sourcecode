/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoAccountStatementReport;
import com.bti.model.dto.DtoBalanceSheetReport;
import com.bti.model.dto.DtoGLReportHeader;
import com.bti.model.dto.DtoProfitAndLossReport;
import com.bti.model.dto.DtoTrailBalanceReport;
import com.bti.service.ServiceGLReport;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;

/**
 * Description: ControllerGLReport
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/generalLedger/reports")
public class ControllerGLReport {

	/*@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ControllerGLReport.class);*/

	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	ServiceGLReport serviceGLReport;
 
	/**
	 * @Description:this service will use to get GL Transaction Report  
	 * @param request
	 * @param dtoGLReportHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getTransactionReport", method = RequestMethod.POST)
	public ResponseMessage getReport(HttpServletRequest request,@RequestBody DtoGLReportHeader dtoGLReportHeader)  
    {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLReportHeader = serviceGLReport.getTransactionReport(dtoGLReportHeader);
			if (dtoGLReportHeader != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoGLReportHeader);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @Description:this service will use to get GL Transaction Report  
	 * @param request
	 * @param dtoTrailBalanceReport
	 * @return
	 */
	@RequestMapping(value = "/trailBalanceReport", method = RequestMethod.POST)
	public ResponseMessage trailBalanceReport(HttpServletRequest request,@RequestBody DtoTrailBalanceReport  dtoTrailBalanceReport)  
    {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
//			dtoTrailBalanceReport = serviceGLReport.getTrailBalanceReport(dtoTrailBalanceReport);
//			dtoTrailBalanceReport = serviceGLReport.getTrailBalanceReportNew(dtoTrailBalanceReport);
			dtoTrailBalanceReport = serviceGLReport.getTrailBalanceReport(dtoTrailBalanceReport);
			if (dtoTrailBalanceReport != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoTrailBalanceReport);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/accountStatementReport", method = RequestMethod.POST)
	public ResponseMessage accountStatementReport(HttpServletRequest request, @RequestBody DtoTrailBalanceReport dtoTrailBalanceReport)  
    {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoTrailBalanceReport = serviceGLReport.getTrailBalanceReport(dtoTrailBalanceReport);
			if (dtoTrailBalanceReport != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoTrailBalanceReport);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}


	
	@RequestMapping(value = "/accountStatementReportNew", method = RequestMethod.POST)
	public ResponseMessage accountStatementReportNew(HttpServletRequest request, @RequestBody DtoAccountStatementReport dtoAccountStatementReport)  
    {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoAccountStatementReport = serviceGLReport.getAccountStatementReport(dtoAccountStatementReport);
			if (dtoAccountStatementReport != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoAccountStatementReport);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/profitAndLossReport", method = RequestMethod.POST)
	public ResponseMessage profitAndLossReport(HttpServletRequest request, @RequestBody DtoProfitAndLossReport dtoProfitAndLossReport)  
    {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoProfitAndLossReport = serviceGLReport.getProfitAndLossReport(dtoProfitAndLossReport);
			if (dtoProfitAndLossReport != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoProfitAndLossReport);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/BalanceSheetReport", method = RequestMethod.POST)
	public ResponseMessage balanceSheetReport(HttpServletRequest request, @RequestBody DtoBalanceSheetReport dtoBalanceSheetReport)  
    {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoBalanceSheetReport = serviceGLReport.getBalanceSheetReport(dtoBalanceSheetReport);
			if (dtoBalanceSheetReport != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoBalanceSheetReport);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

}
