/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.CommonConstant;
import com.bti.constant.MessageLabel;
import com.bti.model.GLClearingHeader;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.service.ServiceCurrencyExchange;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceGLClearingGeneralEntry;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: Controller General Ledger Transaction
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/generalLedger")
public class ControllerGLClearingGeneralEntry {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ControllerGLClearingGeneralEntry.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	
	@Autowired
	ServiceCurrencyExchange serviceCurrencyExchange;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	ServiceGLClearingGeneralEntry serviceGLClearingGeneralEntry;
	
	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;
	
	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;
	
	@RequestMapping(value = "/getAccountNumberForClearingJVEntry", method = RequestMethod.GET)
	public ResponseMessage getAccountNumberForClearingJVEntry(HttpServletRequest request)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoPayableAccountClassSetup> accNumberList = serviceGLClearingGeneralEntry.getAccountNumberForClearingJVEntry();
			if (!accNumberList.isEmpty()) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), accNumberList);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getJournalIdListForUpdateClearingJVEntry", method = RequestMethod.GET)
	public ResponseMessage getJournalIdListForUpdateClearingJVEntry(HttpServletRequest request)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoJournalEntryHeader> journalIdList = serviceGLClearingGeneralEntry.getDistnictJournalIdForUpdateClearingJvEntry();
			if (!journalIdList.isEmpty()) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), journalIdList);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to save clearing journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/saveClearingJournalEntry", method = RequestMethod.POST)
	public ResponseMessage saveClearingJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			dtoJournalEntryHeader=serviceGLClearingGeneralEntry.saveAndUpdateClearingJournalEntry(dtoJournalEntryHeader,CommonConstant.SAVE);
			if(dtoJournalEntryHeader!=null){
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),dtoJournalEntryHeader);
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	 

	/**
	 * @description this service will use to update clearing journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/updateClearingJournalEntry", method = RequestMethod.POST)
	public ResponseMessage updateClearingJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			dtoJournalEntryHeader=serviceGLClearingGeneralEntry.saveAndUpdateClearingJournalEntry(dtoJournalEntryHeader,CommonConstant.UPDATE);
			if(dtoJournalEntryHeader!=null){
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),dtoJournalEntryHeader);
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to save clearing journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/postClearingJournalEntry", method = RequestMethod.POST)
	public ResponseMessage postClearingJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			dtoJournalEntryHeader=serviceGLClearingGeneralEntry.postClearingJournalEntry(dtoJournalEntryHeader);
			if(dtoJournalEntryHeader!=null){
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), 
						HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_POST_SUCCESSFULLY, false),dtoJournalEntryHeader);
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to get clearing journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getClearingJournalEntryByJournalId", method = RequestMethod.POST)
	public ResponseMessage getClearingJournalEntryByJournalId(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			GLClearingHeader glClearingHeader= repositoryGLClearingHeader.findByJournalIDAndIsDeleted(dtoJournalEntryHeader.getJournalID(), false);
			if(glClearingHeader!=null)
			{
				dtoJournalEntryHeader=serviceGLClearingGeneralEntry.setGlJournalClearingInDto(glClearingHeader);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoJournalEntryHeader);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to delete clearing journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/deleteClearingJournalEntry", method = RequestMethod.POST)
	public ResponseMessage deleteClearingJournalEntryByJournalId(HttpServletRequest request,
			@RequestBody DtoJournalEntryHeader dtoJournalEntryHeader) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceGLClearingGeneralEntry.deleteClearingJournalEntryByJournalId(dtoJournalEntryHeader);
			if (status) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
}
