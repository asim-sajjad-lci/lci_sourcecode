/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.BookSetup;
import com.bti.model.FAAccountGroupsSetup;
import com.bti.model.FAClassSetup;
import com.bti.model.FACompanySetup;
import com.bti.model.FAGeneralMaintenance;
import com.bti.model.FAInsuranceClassSetup;
import com.bti.model.FALeaseCompanySetup;
import com.bti.model.FALocationSetup;
import com.bti.model.FAPhysicalLocationSetup;
import com.bti.model.FAPurchasePostingAccountSetup;
import com.bti.model.FARetirementSetup;
import com.bti.model.VendorMaintenance;
import com.bti.model.dto.DtoBookSetup;
import com.bti.model.dto.DtoFABookClassSetup;
import com.bti.model.dto.DtoFACalendarSetup;
import com.bti.model.dto.DtoFAClassSetup;
import com.bti.model.dto.DtoFACompanySetup;
import com.bti.model.dto.DtoFAGeneralMaintenance;
import com.bti.model.dto.DtoFAPurchasePostingAccountSetup;
import com.bti.model.dto.DtoFAStructureSetup;
import com.bti.model.dto.DtoFaAccountGroupSetup;
import com.bti.model.dto.DtoInsuranceClass;
import com.bti.model.dto.DtoLeaseCompanySetup;
import com.bti.model.dto.DtoLocationSetup;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoPhysicalLocationSetup;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoRetirementSetup;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryBookSetup;
import com.bti.repository.RepositoryFAAccountGroupSetup;
import com.bti.repository.RepositoryFAClassSetup;
import com.bti.repository.RepositoryFACompanySetup;
import com.bti.repository.RepositoryFAGeneralMaintenance;
import com.bti.repository.RepositoryFAInsuranceClassSetup;
import com.bti.repository.RepositoryFALeaseCompanySetup;
import com.bti.repository.RepositoryFALocationSetup;
import com.bti.repository.RepositoryFAPhysicalLocationSetup;
import com.bti.repository.RepositoryFAPurchasePostingAccountSetup;
import com.bti.repository.RepositoryFARetirementSetup;
import com.bti.repository.RepositoryVendorMaintenance;
import com.bti.service.ServiceFixedAssets;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;
import com.bti.util.UtilRandomKey;

/**
 * Description: ControllerFixedAssets Name of Project: BTI Created on: Aug 22,
 * 2017 Modified on: Aug 22, 2017 11:08:38 AM
 * 
 * @author seasia Version:
 */
@RestController
@RequestMapping("/fixed/assets")
public class ControllerFixedAssets {

	private static final Logger logger = Logger.getLogger(ControllerFixedAssets.class);

	@Autowired
	RepositoryFARetirementSetup repositoryFARetirementSetup;

	@Autowired
	ServiceFixedAssets serviceFixedAssets;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryFACompanySetup repositoryFACompanySetup;

	@Autowired
	RepositoryBookSetup repositoryBookSetup;

	@Autowired
	RepositoryFALocationSetup repositoryFALocationSetup;

	@Autowired
	RepositoryFAPhysicalLocationSetup repositoryFAPhysicalLocationSetup;

	@Autowired
	RepositoryFAGeneralMaintenance repositoryFAGeneralMaintenance;

	@Autowired
	RepositoryFALeaseCompanySetup repositoryFALeaseCompanySetup;

	@Autowired
	RepositoryVendorMaintenance repositoryVendorMaintenance;

	@Autowired
	RepositoryFAClassSetup repositoryFAClassSetup;

	@Autowired
	RepositoryFAInsuranceClassSetup repositoryFAInsuranceClassSetup;

	@Autowired
	RepositoryFAAccountGroupSetup repositoryFAAccountGroupSetup;

	@Autowired
	RepositoryFAPurchasePostingAccountSetup repositoryFAPurchasePostingAccountSetup;

	@Autowired
	ServiceHome serviceHome;

	/**
	 * @Description This method will use to create a new Fixed Assets Retirement
	 *              Setup
	 * @param dtoRetirementSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/retirementSetup", method = RequestMethod.POST)
	public ResponseMessage saveRetirementSetup(@RequestBody DtoRetirementSetup dtoRetirementSetup) {
		logger.info("inside retirementSetup method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoRetirementSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			// check if the same faRetirementSetup is already exist with
			// faRetirementid or not
			FARetirementSetup faRetirementSetup = repositoryFARetirementSetup
					.findByRetirementIdAndIsDeleted(dtoRetirementSetup.getFaRetirementId(), false);
			if (faRetirementSetup == null) {
				// create new faRetirement setup
				dtoRetirementSetup = serviceFixedAssets.saveFixedAssetsRetirementSetup(dtoRetirementSetup);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_RETIREMENT_SETUP_SUCCESSFULLY, false),
						dtoRetirementSetup);
			} else {
				// faRetirementId already exist
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FA_RETIREMENT_SETUP_ALREADY_EXISTS,
								false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will use to update the already exists Retirement
	 *              Setup
	 * @param dtoRetirementSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/retirementSetup/update", method = RequestMethod.POST)
	public ResponseMessage updateRetirementSetup(@RequestBody DtoRetirementSetup dtoRetirementSetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoRetirementSetup); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			// check if the same faRetirementSetup is already exist with
			// faRetirementid or not
			FARetirementSetup faRetirementSetup = repositoryFARetirementSetup
					.findByRetirementIdAndIsDeleted(dtoRetirementSetup.getFaRetirementId(), false);
			if (faRetirementSetup != null) {
				// update faRetirement setup
				dtoRetirementSetup = serviceFixedAssets.updateFixedAssetsRetirementSetup(dtoRetirementSetup,
						faRetirementSetup);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_RETIREMENT_SETUP_UPDATED_SUCCESSFULLY, false),
						dtoRetirementSetup);
			} else {
				// faRetirementId not exist
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FA_RETIREMENT_SETUP_NOT_FOUND,
								false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will use to search the retirement setup with
	 *              pagination or get all retirement setups
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/retirementSetup/search", method = RequestMethod.POST)
	public ResponseMessage searchRetirementSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchOrGetAllFixedAssetsRetirementSetup(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_RETIREMENT_SETUP_FETCHED_SUCCESSFULLY, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will use to get the retirement setup by
	 *              retirement setup id
	 * @param dtoRetirementSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/retirementSetup/getById", method = RequestMethod.POST)
	public ResponseMessage getRetirementSetup(@RequestBody DtoRetirementSetup dtoRetirementSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FARetirementSetup faRetirementSetup = repositoryFARetirementSetup
					.findByRetirementIdAndIsDeleted(dtoRetirementSetup.getFaRetirementId(), false);
			if (faRetirementSetup != null) {
				dtoRetirementSetup = serviceFixedAssets.getFixedAssetsRetirementSetupById(dtoRetirementSetup);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoRetirementSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will save fixed asset company setup
	 * @param dtoFACompanySetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/saveFixedAssetCompanySetup", method = RequestMethod.POST)
	public ResponseMessage saveFixedAssetCompanySetup(@RequestBody DtoFACompanySetup dtoFACompanySetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFACompanySetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFACompanySetup != null) {
				BookSetup bookSetup = repositoryBookSetup.findByBookInxdAndIsDeleted(dtoFACompanySetup.getBookIndexId(),
						false);
				if (bookSetup != null) {
					dtoFACompanySetup = serviceFixedAssets.saveFixAssetCompanySetup(dtoFACompanySetup);
					if (dtoFACompanySetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(
										dtoFACompanySetup.getUpdateAndSaveMessage(), false));
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
										.getMessageByShortAndIsDeleted(dtoFACompanySetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BOOK_INDEX_ID_IS_NOT_FOUND,
									false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will get Fixed Asset Company Setup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getFixedAssetCompanySetup", method = RequestMethod.GET)
	public ResponseMessage getFixedAssetCompanySetupByIndexId() {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<FACompanySetup> faCompanySetupList = repositoryFACompanySetup.findAll();
			if (faCompanySetupList != null && !faCompanySetupList.isEmpty()) {
				DtoFACompanySetup dtoFACompanySetup = serviceFixedAssets
						.getFixAssetCompanySetup(faCompanySetupList.get(0));
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoFACompanySetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will search Fixed Asset Company Setup
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/searchFixedAssetCompanySetup", method = RequestMethod.POST)
	public ResponseMessage searchFixedAssetCompanySetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchFixAssetCompanySetup(dtoSearch);
				@SuppressWarnings("unchecked")
				List<DtoFACompanySetup> list = (List<DtoFACompanySetup>) dtoSearch.getRecords();
				if (!list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will save Fixed Asset Book Setup
	 * @param dtoBookSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/saveFixedAssetBookSetup", method = RequestMethod.POST)
	public ResponseMessage saveFixedAssetBookSetup(@RequestBody DtoBookSetup dtoBookSetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoBookSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoBookSetup != null) {
				BookSetup bookSetup = repositoryBookSetup.findByBookIdAndIsDeleted(dtoBookSetup.getBookId(), false);
				if (bookSetup == null) {
					dtoBookSetup = serviceFixedAssets.saveBookSetup(dtoBookSetup);
					if (dtoBookSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.FA_BOOK_SETUP_SAVE_SUCCESSFULLY, false));
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(dtoBookSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BOOK_ID_ALREADY_USED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will get Fixed Asset Book Setup By BookIndexId
	 * @param dtoBookSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getFixedAssetBookSetupByBookIndexId", method = RequestMethod.POST)
	public ResponseMessage getFixedAssetBookSetupByBookIndexId(@RequestBody DtoBookSetup dtoBookSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoBookSetup != null && dtoBookSetup.getBookIndexId() != null) {
				BookSetup bookSetup = repositoryBookSetup.findByBookInxdAndIsDeleted(dtoBookSetup.getBookIndexId(),
						false);
				if (bookSetup != null) {
					dtoBookSetup = serviceFixedAssets.getBookSetupByBookIndexId(dtoBookSetup, bookSetup);
					if (dtoBookSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
								dtoBookSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(dtoBookSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BOOK_INDEX_ID_IS_NOT_FOUND,
									false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will update Fixed Asset Book Setup By
	 *              BookIndexId
	 * @param dtoBookSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/updateFixedAssetBookSetupByBookIndexId", method = RequestMethod.POST)
	public ResponseMessage updateFixedAssetBookSetupByBookIndexId(@RequestBody DtoBookSetup dtoBookSetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoBookSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoBookSetup != null && dtoBookSetup.getBookIndexId() != null) {
				BookSetup bookSetup = repositoryBookSetup.findByBookInxdAndIsDeleted(dtoBookSetup.getBookIndexId(),
						false);
				if (bookSetup != null) {
					BookSetup checkBookIdAlreadyUsed = repositoryBookSetup
							.findByBookIdAndBookInxdNotEqual(dtoBookSetup.getBookId(), dtoBookSetup.getBookIndexId());
					if (checkBookIdAlreadyUsed == null) {
						dtoBookSetup = serviceFixedAssets.updateBookSetup(dtoBookSetup, bookSetup);
						if (dtoBookSetup.getMessageType() == null) {
							responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
									serviceResponse.getMessageByShortAndIsDeleted(
											MessageLabel.FA_BOOK_SETUP_UPDATE_SUCCESSFULLY, false));
						} else {
							responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
									HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
											.getMessageByShortAndIsDeleted(dtoBookSetup.getMessageType(), false));
						}
					} else {
						responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BOOK_ID_ALREADY_USED,
										false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BOOK_INDEX_ID_IS_NOT_FOUND,
									false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will search Book Setup
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/searchBookSetup", method = RequestMethod.POST)
	public ResponseMessage searchBookSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchBookSetup(dtoSearch);
				@SuppressWarnings("unchecked")
				List<DtoBookSetup> list = (List<DtoBookSetup>) dtoSearch.getRecords();
				if (!list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will calendar Monthly Setup
	 * @param dtoFACalendarSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/calendarMonthlySetup", method = RequestMethod.POST)
	public ResponseMessage calendarMonthlySetup(@RequestBody DtoFACalendarSetup dtoFACalendarSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFACalendarSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFACalendarSetup != null) {
				dtoFACalendarSetup = serviceFixedAssets.saveFixAssetCalendarMonthlySetup(dtoFACalendarSetup);
				if (dtoFACalendarSetup != null) {
					if (dtoFACalendarSetup.getMessageType() != null) {
						if (dtoFACalendarSetup.getMessageType()
								.equals(MessageLabel.CALENDAR_SETUP_SAVED_SUCCESSFULLY)) {
							responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
									serviceResponse.getMessageByShortAndIsDeleted(
											MessageLabel.CALENDAR_SETUP_SAVED_SUCCESSFULLY, false),
									dtoFACalendarSetup);
						}
						if (dtoFACalendarSetup.getMessageType()
								.equals(MessageLabel.CALENDAR_SETUP_UPDATED_SUCCESSFULLY)) {
							responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
									serviceResponse.getMessageByShortAndIsDeleted(
											MessageLabel.CALENDAR_SETUP_UPDATED_SUCCESSFULLY, false),
									dtoFACalendarSetup);
						}

					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
										.getMessageByShortAndIsDeleted(dtoFACalendarSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.CALENDAR_SETUP_SAVE_FAIL, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will get Calendar Monthly Setup
	 * @param request
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getCalendarMonthlySetup", method = RequestMethod.GET)
	public ResponseMessage getCalendarMonthlySetupById(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			DtoFACalendarSetup dtoFACalendarSetup = serviceFixedAssets.getCalendarMonthlySetup();
			if (dtoFACalendarSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoFACalendarSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will calendar Quarter Setup
	 * @param dtoFACalendarSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/calendarQuarterSetup", method = RequestMethod.POST)
	public ResponseMessage calendarQuarterSetup(@RequestBody DtoFACalendarSetup dtoFACalendarSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFACalendarSetup); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFACalendarSetup != null) {
				dtoFACalendarSetup = serviceFixedAssets.calendarQuarterSetup(dtoFACalendarSetup);
				if (dtoFACalendarSetup != null) {
					if (dtoFACalendarSetup.getMessageType() != null) {
						if (dtoFACalendarSetup.getMessageType()
								.equals(MessageLabel.CALENDAR_SETUP_SAVED_SUCCESSFULLY)) {
							responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
									serviceResponse.getMessageByShortAndIsDeleted(
											MessageLabel.CALENDAR_SETUP_SAVED_SUCCESSFULLY, false),
									dtoFACalendarSetup);
						}
						if (dtoFACalendarSetup.getMessageType()
								.equals(MessageLabel.CALENDAR_SETUP_UPDATED_SUCCESSFULLY)) {
							responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
									serviceResponse.getMessageByShortAndIsDeleted(
											MessageLabel.CALENDAR_SETUP_UPDATED_SUCCESSFULLY, false),
									dtoFACalendarSetup);
						}
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
										.getMessageByShortAndIsDeleted(dtoFACalendarSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.CALENDAR_SETUP_SAVE_FAIL, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will get Calendar Quarter Setup
	 * @param request
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getCalendarQuarterSetup", method = RequestMethod.GET)
	public ResponseMessage getCalendarQuarterSetupById(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			DtoFACalendarSetup dtoFACalendarSetup = serviceFixedAssets.getCalendarQuarterSetup();
			if (dtoFACalendarSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoFACalendarSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will structure Setup
	 * @param dtoFAStructureSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/structureSetup", method = RequestMethod.POST)
	public ResponseMessage structureSetup(@RequestBody DtoFAStructureSetup dtoFAStructureSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFAStructureSetup); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAStructureSetup != null) {
				dtoFAStructureSetup = serviceFixedAssets.structureSetup(dtoFAStructureSetup);
				if (dtoFAStructureSetup != null) {
					if (dtoFAStructureSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.STRUCTURE_SETUP_SAVED_SUCCESSFULLY, false),
								dtoFAStructureSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
										.getMessageByShortAndIsDeleted(dtoFAStructureSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.STRUCTURE_SETUP_SAVE_FAIL, false));
				}
				return responseMessage;
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will update Structure Setup
	 * @param dtoFAStructureSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/updateStructureSetup", method = RequestMethod.POST)
	public ResponseMessage updateStructureSetup(@RequestBody DtoFAStructureSetup dtoFAStructureSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFAStructureSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAStructureSetup != null) {
				dtoFAStructureSetup = serviceFixedAssets.updateCalendarQuarterSetup(dtoFAStructureSetup);
				if (dtoFAStructureSetup != null) {
					if (dtoFAStructureSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.STRUCTURE_SETUP_UPDATED_SUCCESSFULLY, false),
								dtoFAStructureSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
										.getMessageByShortAndIsDeleted(dtoFAStructureSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.STRUCTURE_SETUP_UPDATED_FAIL, false));
				}
				return responseMessage;
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will update Structure Setup
	 * @param dtoFAStructureSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getStructureSetupById", method = RequestMethod.POST)
	public ResponseMessage getStructureSetupById(@RequestBody DtoFAStructureSetup dtoFAStructureSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAStructureSetup != null) {
				dtoFAStructureSetup = serviceFixedAssets.getStructureSetupById(dtoFAStructureSetup);
				if (dtoFAStructureSetup != null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
							dtoFAStructureSetup);
				}
				return responseMessage;
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This method will search Structure Setup
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/searchStructureSetup", method = RequestMethod.POST)
	public ResponseMessage searchStructureSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchStructureSetup(dtoSearch);
				@SuppressWarnings("unchecked")
				List<DtoFAStructureSetup> list = (List<DtoFAStructureSetup>) dtoSearch.getRecords();
				if (list != null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description Location setup
	 * @param dtoLocationSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/locationSetup", method = RequestMethod.POST)
	public ResponseMessage saveLocationSetup(@RequestBody DtoLocationSetup dtoLocationSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoLocationSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FALocationSetup faLocationSetup = repositoryFALocationSetup
					.findByLocationIdAndIsDeleted(dtoLocationSetup.getLocationId(), false);
			if (faLocationSetup == null) {
				dtoLocationSetup = serviceFixedAssets.saveLocationSetup(dtoLocationSetup);

				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FA_LOCATION_SETUP_SUCCESS, false),
						dtoLocationSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FA_LOCATION_ALREADY_EXISTS, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description Location setup update
	 * @param dtoLocationSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/locationSetup/update", method = RequestMethod.POST)
	public ResponseMessage updateLocationSetup(@RequestBody DtoLocationSetup dtoLocationSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoLocationSetup); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FALocationSetup faLocationSetup = repositoryFALocationSetup
					.findByLocationIdAndIsDeleted(dtoLocationSetup.getLocationId(), false);
			if (faLocationSetup != null) {
				dtoLocationSetup = serviceFixedAssets.updateLocationSetup(dtoLocationSetup, faLocationSetup);

				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_LOCATION_SETUP_UPDATED_SUCCESS, false),
						dtoLocationSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description Get location setup by id
	 * @param dtoLocationSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/locationSetup/getById", method = RequestMethod.POST)
	public ResponseMessage getSalesTerritorySetup(@RequestBody DtoLocationSetup dtoLocationSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FALocationSetup faLocationSetup = repositoryFALocationSetup
					.findByLocationIdAndIsDeleted(dtoLocationSetup.getLocationId(), false);
			if (faLocationSetup != null) {
				dtoLocationSetup = serviceFixedAssets.getFALocationSetupById(faLocationSetup);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoLocationSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description Search location setup
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/locationSetup/search", method = RequestMethod.POST)
	public ResponseMessage searchLocationSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchFALocationSetup(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_LOCATION_SETUP_FETCHED_SUCCESS, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description Save physical location setup
	 * @param dtoPhysicalLocationSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/physicalLocationSetup", method = RequestMethod.POST)
	public ResponseMessage savePhysicalLocationSetup(@RequestBody DtoPhysicalLocationSetup dtoPhysicalLocationSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPhysicalLocationSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FAPhysicalLocationSetup faPhysicalLocationSetup = repositoryFAPhysicalLocationSetup
					.findByPhysicalLocationIdAndIsDeleted(dtoPhysicalLocationSetup.getPhysicalLocationId(), false);
			if (faPhysicalLocationSetup == null) {
				dtoPhysicalLocationSetup = serviceFixedAssets.savePhysicalLocationSetup(dtoPhysicalLocationSetup);

				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_PHYSICAL_LOCATION_SETUP_SUCCESS, false),
						dtoPhysicalLocationSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_PHYSICAL_LOCATION_ALREADY_EXISTS, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description Update physical location setup
	 * @param dtoPhysicalLocationSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/physicalLocationSetup/update", method = RequestMethod.POST)
	public ResponseMessage updatePhysicalLocationSetup(@RequestBody DtoPhysicalLocationSetup dtoPhysicalLocationSetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPhysicalLocationSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FAPhysicalLocationSetup faPhysicalLocationSetup = repositoryFAPhysicalLocationSetup
					.findByPhysicalLocationIdAndIsDeleted(dtoPhysicalLocationSetup.getPhysicalLocationId(), false);
			if (faPhysicalLocationSetup != null) {
				dtoPhysicalLocationSetup = serviceFixedAssets.updatePhysicalLocationSetup(dtoPhysicalLocationSetup,
						faPhysicalLocationSetup);

				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(
								MessageLabel.FA_PHYSICAL_LOCATION_SETUP_UPDATED_SUCCESS, false),
						dtoPhysicalLocationSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description Get physical location setup by id
	 * @param dtoPhysicalLocationSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/physicalLocationSetup/getById", method = RequestMethod.POST)
	public ResponseMessage getPhysicalLocationSetup(@RequestBody DtoPhysicalLocationSetup dtoPhysicalLocationSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FAPhysicalLocationSetup faPhysicalLocationSetup = repositoryFAPhysicalLocationSetup
					.findByPhysicalLocationIdAndIsDeleted(dtoPhysicalLocationSetup.getPhysicalLocationId(), false);
			if (faPhysicalLocationSetup != null) {
				dtoPhysicalLocationSetup = serviceFixedAssets.getPhysicalLocationSetupById(faPhysicalLocationSetup);

				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoPhysicalLocationSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description Search physical location setup
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/physicalLocationSetup/search", method = RequestMethod.POST)
	public ResponseMessage searchPhysicalLocationSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchFAPhysicalLocationSetup(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_PHYSICAL_LOCATION_SETUP_FETCHED_SUCCESS, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description save FA General Maintenance
	 * @param dtoFAGeneralMaintenance
	 * @return
	 * 
	 */
	@RequestMapping(value = "/saveFAGeneralMaintenance", method = RequestMethod.POST)
	public ResponseMessage saveFAGeneralMaintenance(@RequestBody DtoFAGeneralMaintenance dtoFAGeneralMaintenance) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFAGeneralMaintenance); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAGeneralMaintenance != null) {
				FAGeneralMaintenance faGeneralMaintenance = repositoryFAGeneralMaintenance
						.findByAssetIdAndIsDeleted(dtoFAGeneralMaintenance.getAssetId(), false);
				if (faGeneralMaintenance == null) {
					dtoFAGeneralMaintenance = serviceFixedAssets.saveFAGeneralMaintenance(dtoFAGeneralMaintenance);
					if (dtoFAGeneralMaintenance.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.FA_GENERAL_MAINTENANCE_SAVE_SUCCESS, false),
								dtoFAGeneralMaintenance);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse.getMessageByShortAndIsDeleted(
										dtoFAGeneralMaintenance.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ASSET_ID_ALREDAY_USED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description update FA General Maintenance
	 * @param dtoFAGeneralMaintenance
	 * @return
	 * 
	 */
	@RequestMapping(value = "/updateFAGeneralMaintenance", method = RequestMethod.POST)
	public ResponseMessage updateFAGeneralMaintenance(@RequestBody DtoFAGeneralMaintenance dtoFAGeneralMaintenance) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFAGeneralMaintenance); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAGeneralMaintenance != null) {
				FAGeneralMaintenance faGeneralMaintenance = repositoryFAGeneralMaintenance
						.findByAssetIdAndIsDeleted(dtoFAGeneralMaintenance.getAssetId(), false);
				if (faGeneralMaintenance != null) {
					dtoFAGeneralMaintenance = serviceFixedAssets.updateFAGeneralMaintenance(dtoFAGeneralMaintenance,
							faGeneralMaintenance);
					if (dtoFAGeneralMaintenance.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.FA_GENERAL_MAINTENANCE_UPDATE_SUCCESS, false),
								dtoFAGeneralMaintenance);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse.getMessageByShortAndIsDeleted(
										dtoFAGeneralMaintenance.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ASSET_ID_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		
		return responseMessage;
	}

	/**
	 * @Description get FA General Maintenance By AssetId
	 * @param dtoFAGeneralMaintenance
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getFAGeneralMaintenanceByAssetId", method = RequestMethod.POST)
	public ResponseMessage getFAGeneralMaintenanceByAssetId(
			@RequestBody DtoFAGeneralMaintenance dtoFAGeneralMaintenance) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAGeneralMaintenance != null) {
				FAGeneralMaintenance faGeneralMaintenance = repositoryFAGeneralMaintenance
						.findByAssetIdAndIsDeleted(dtoFAGeneralMaintenance.getAssetId(), false);
				if (faGeneralMaintenance != null) {
					dtoFAGeneralMaintenance = serviceFixedAssets
							.getFAGeneralMaintenanceByAssetId(dtoFAGeneralMaintenance, faGeneralMaintenance);
					if (dtoFAGeneralMaintenance.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
								dtoFAGeneralMaintenance);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse.getMessageByShortAndIsDeleted(
										dtoFAGeneralMaintenance.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ASSET_ID_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description Save and search FA General Maintenance
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/searchFAGeneralMaintenance", method = RequestMethod.POST)
	public ResponseMessage searchFAGeneralMaintenance(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchFAGeneralMaintenance(dtoSearch);
				@SuppressWarnings("unchecked")
				List<DtoFAGeneralMaintenance> list = (List<DtoFAGeneralMaintenance>) dtoSearch.getRecords();
				if (list != null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description Save and update lease company setup
	 * @param dtoLeaseCompanySetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/leaseCompanySetup", method = RequestMethod.POST)
	public ResponseMessage leaseCompanySetup(@RequestBody DtoLeaseCompanySetup dtoLeaseCompanySetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoLeaseCompanySetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			VendorMaintenance maintenance = repositoryVendorMaintenance
					.findByVendoridAndIsDeleted(dtoLeaseCompanySetup.getVendorId(), false);
			if (maintenance != null) {
				dtoLeaseCompanySetup = serviceFixedAssets.saveLeaseCompanySetup(dtoLeaseCompanySetup);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_LEASE_COMPANY_SETUP_SUCCESS, false),
						dtoLeaseCompanySetup);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VENDOR_NOT_AVIALABLE, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description Get lease company setup, it should be single entity
	 * @return
	 * 
	 */
	@RequestMapping(value = "/leaseCompanySetup/getById", method = RequestMethod.POST)
	public ResponseMessage leaseCompanySetupGetById(@RequestBody DtoLeaseCompanySetup dtoLeaseCompanySetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FALeaseCompanySetup faLeaseCompanySetup = repositoryFALeaseCompanySetup
					.findOne(dtoLeaseCompanySetup.getCompanyIndex());
			if (faLeaseCompanySetup != null) {

				dtoLeaseCompanySetup = serviceFixedAssets.getLeaseCompanySetup(faLeaseCompanySetup);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoLeaseCompanySetup);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description save FA Book Class Setup
	 * @param dtoFABookClassSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/saveFABookClassSetup", method = RequestMethod.POST)
	public ResponseMessage saveFABookClassSetup(@RequestBody DtoFABookClassSetup dtoFABookClassSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFABookClassSetup); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFABookClassSetup != null) {
				dtoFABookClassSetup = serviceFixedAssets.saveFABookClassSetup(dtoFABookClassSetup);
				if (dtoFABookClassSetup != null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FA_BOOK_CLASS_SETUP_SAVE_SUCCESS,
									false),
							dtoFABookClassSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.FA_BOOK_CLASS_SETUP_FAIL, false));
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description update FA Book Class Setup
	 * @param dtoFABookClassSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/updateFABookClassSetup", method = RequestMethod.POST)
	public ResponseMessage updateFABookClassSetup(@RequestBody DtoFABookClassSetup dtoFABookClassSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFABookClassSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFABookClassSetup != null) {
				dtoFABookClassSetup = serviceFixedAssets.updateFABookClassSetup(dtoFABookClassSetup);
				if (dtoFABookClassSetup != null) {

					if (dtoFABookClassSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
								.getMessageByShortAndIsDeleted(MessageLabel.FA_BOOK_CLASS_SETUP_UPDATE_SUCCESS, false),
								dtoFABookClassSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
										.getMessageByShortAndIsDeleted(dtoFABookClassSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description get FA Book Class Setup ById
	 * @param dtoFABookClassSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getFABookClassSetupById", method = RequestMethod.POST)
	public ResponseMessage getFABookClassSetupById(@RequestBody DtoFABookClassSetup dtoFABookClassSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFABookClassSetup != null) {
				dtoFABookClassSetup = serviceFixedAssets.getFABookClassSetupById(dtoFABookClassSetup);
				if (dtoFABookClassSetup != null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
							dtoFABookClassSetup);

				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description search FA Book Class Setup
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/searchFABookClassSetup", method = RequestMethod.POST)
	public ResponseMessage searchFABookClassSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchFABookClassSetup(dtoSearch);
				@SuppressWarnings("unchecked")
				List<DtoFAGeneralMaintenance> list = (List<DtoFAGeneralMaintenance>) dtoSearch.getRecords();
				if (list != null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description save Fixed Asset Class Setup
	 * @param dtoFAClassSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/saveFixedAssetClassSetup", method = RequestMethod.POST)
	public ResponseMessage saveFixedAssetClassSetup(@RequestBody DtoFAClassSetup dtoFAClassSetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFAClassSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAClassSetup != null) {
				FAClassSetup faClassSetup = repositoryFAClassSetup
						.findByClassIdAndIsDeleted(dtoFAClassSetup.getClassId(), false);
				if (faClassSetup == null) {
					dtoFAClassSetup = serviceFixedAssets.saveFaClassSetup(dtoFAClassSetup);
					if (dtoFAClassSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.FA_CLASS_SETUP_SAVE_SUCCESSFULLY, false),
								dtoFAClassSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(dtoFAClassSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CLASS_ID_ALREADY_USED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description update Fixed Asset Class Setup
	 * @param dtoFAClassSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/updateFixedAssetClassSetup", method = RequestMethod.POST)
	public ResponseMessage updateFixedAssetClassSetup(@RequestBody DtoFAClassSetup dtoFAClassSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFAClassSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAClassSetup != null) {
				FAClassSetup faClassSetup = repositoryFAClassSetup
						.findByClassIdAndIsDeleted(dtoFAClassSetup.getClassId(), false);
				if (faClassSetup != null) {
					dtoFAClassSetup = serviceFixedAssets.updateFaClassSetup(dtoFAClassSetup, faClassSetup);
					if (dtoFAClassSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
								.getMessageByShortAndIsDeleted(MessageLabel.FA_CLASS_SETUP_UPDATE_SUCCESSFULLY, false),
								dtoFAClassSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(dtoFAClassSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CLASS_ID_IS_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description get Fixed Asset Class Setup By ClassId
	 * @param dtoFAClassSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getFixedAssetClassSetupByClassId", method = RequestMethod.POST)
	public ResponseMessage getFixedAssetClassSetupByClassId(@RequestBody DtoFAClassSetup dtoFAClassSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAClassSetup != null) {
				FAClassSetup faClassSetup = repositoryFAClassSetup
						.findByClassIdAndIsDeleted(dtoFAClassSetup.getClassId(), false);
				if (faClassSetup != null) {
					dtoFAClassSetup = serviceFixedAssets.getFaClassSetupByClassId(faClassSetup);
					if (dtoFAClassSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
								dtoFAClassSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(dtoFAClassSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CLASS_ID_IS_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description search FA Class Setup
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/searchFAClassSetup", method = RequestMethod.POST)
	public ResponseMessage searchFAClassSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchFAClassSetup(dtoSearch);
				@SuppressWarnings("unchecked")
				List<DtoFAClassSetup> list = (List<DtoFAClassSetup>) dtoSearch.getRecords();
				if (!list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This service will use to create new insurance class setup
	 * @param dtoInsuranceClass
	 * @return
	 * 
	 */
	@RequestMapping(value = "/insuranceClassSetup", method = RequestMethod.POST)
	public ResponseMessage insuranceClassSetup(@RequestBody DtoInsuranceClass dtoInsuranceClass) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoInsuranceClass); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoInsuranceClass != null) {
				FAInsuranceClassSetup faInsuranceClassSetup = repositoryFAInsuranceClassSetup
						.findByInsuranceClassId(dtoInsuranceClass.getInsuranceClassId());
				if (faInsuranceClassSetup == null) {
					dtoInsuranceClass = serviceFixedAssets.saveInsuranceClassSetup(dtoInsuranceClass);
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FA_INSURANCE_CLASS_SETUP_SUCCESS,
									false),
							dtoInsuranceClass);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This service will use to update insurance class setup
	 * @param dtoInsuranceClass
	 * @return
	 * 
	 */
	@RequestMapping(value = "/insuranceClassSetup/update", method = RequestMethod.POST)
	public ResponseMessage updateInsuranceClassSetup(@RequestBody DtoInsuranceClass dtoInsuranceClass) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoInsuranceClass); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FAInsuranceClassSetup faInsuranceClassSetup = repositoryFAInsuranceClassSetup
					.findOne(dtoInsuranceClass.getInsuranceClassIndex());
			if (faInsuranceClassSetup != null) {
				FAInsuranceClassSetup faInsuranceClassSetup2 = repositoryFAInsuranceClassSetup
						.checkInsuranceClassIdButSkipCurrentIndecClassId(faInsuranceClassSetup.getInsuranceClassIndex(),
								dtoInsuranceClass.getInsuranceClassId());
				if (faInsuranceClassSetup2 == null) {
					dtoInsuranceClass = serviceFixedAssets.insuranceClassSetupUpdate(dtoInsuranceClass,
							faInsuranceClassSetup);
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(
									MessageLabel.FA_INSURANCE_CLASS_UPDATED_SUCCESS, false),
							dtoInsuranceClass);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This service will use to get insurance class setup
	 * @param dtoInsuranceClass
	 * @return
	 * 
	 */
	@RequestMapping(value = "/insuranceClassSetup/getById", method = RequestMethod.POST)
	public ResponseMessage getInsuranceClassSetup(@RequestBody DtoInsuranceClass dtoInsuranceClass) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FAInsuranceClassSetup faInsuranceClassSetup = repositoryFAInsuranceClassSetup
					.findOne(dtoInsuranceClass.getInsuranceClassIndex());
			if (faInsuranceClassSetup != null) {
				dtoInsuranceClass = serviceFixedAssets.insuranceClassSetupGetById(faInsuranceClassSetup);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoInsuranceClass);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This service will use to search and get all insurance class
	 *              setups
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/insuranceClassSetup/search", method = RequestMethod.POST)
	public ResponseMessage searchInsuranceClassSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.insuranceClassSetupSearch(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_INSURANCE_CLASS_FETCHED_SUCCESS, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This service will account Group Setup
	 * @param dtoFaAccountGroupSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/accountGroupSetup", method = RequestMethod.POST)
	public ResponseMessage accountGroupSetup(@RequestBody DtoPayableAccountClassSetup dtoFaAccountGroupSetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFaAccountGroupSetup); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFaAccountGroupSetup != null
					&& UtilRandomKey.isNotBlank(dtoFaAccountGroupSetup.getAccountGroupId())) {
				dtoFaAccountGroupSetup = serviceFixedAssets.saveFaAccountGroupSetup(dtoFaAccountGroupSetup);
				if (dtoFaAccountGroupSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_FROUP_SETUP_SAVE_SUCCESS,
									false),
							dtoFaAccountGroupSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(dtoFaAccountGroupSetup.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This service will get Account Group Setup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getAccountGroupSetupByAccountGroupId", method = RequestMethod.POST)
	public ResponseMessage getAccountGroupSetup(@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FAAccountGroupsSetup faAccountGroupsSetup = repositoryFAAccountGroupSetup
					.findByfaAccountGroupIdAndIsDeleted(dtoPayableAccountClassSetup.getAccountGroupId(), false);
			if (faAccountGroupsSetup != null) {
				dtoPayableAccountClassSetup = serviceFixedAssets
						.getAccountGroupSetbyByAccountGroupId(dtoPayableAccountClassSetup.getAccountGroupId());
				if (dtoPayableAccountClassSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoPayableAccountClassSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse.getMessageByShortAndIsDeleted(
									dtoPayableAccountClassSetup.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This service will use to create and update purchase posting
	 *              account setup
	 * @param dtoFAPurchasePostingAccountSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/purchasePostingAccountSetup", method = RequestMethod.POST)
	public ResponseMessage purchasePostingAccountSetup(
			@RequestBody DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFAPurchasePostingAccountSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAPurchasePostingAccountSetup != null) {
				dtoFAPurchasePostingAccountSetup = serviceFixedAssets
						.savePurchasePostingAccountSetup(dtoFAPurchasePostingAccountSetup);
				if (dtoFAPurchasePostingAccountSetup != null) {
					if (dtoFAPurchasePostingAccountSetup.getMessageType() == null) {
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.FA_PURCHASE_POSTING_ACCOUNT_SETUP_SUCCESS, false),
								dtoFAPurchasePostingAccountSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(
										dtoFAPurchasePostingAccountSetup.getMessageType(), false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(
									MessageLabel.FA_PURCHASE_POSTING_ACCOUNT_SETUP_FAIL, false));
				}
			} else {

				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This service will use to get purchase posting account setup
	 * @param dtoFAPurchasePostingAccountSetup
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getPurchasePostingAccountSetupByClassIndex", method = RequestMethod.POST)
	public ResponseMessage getPurchasePostingAccountSetup(HttpServletRequest request,
			@RequestBody DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoFAPurchasePostingAccountSetup = serviceFixedAssets.getPurchasePostingAccountSetupByClassIndex(
					dtoFAPurchasePostingAccountSetup.getFixedAssetClassIndex());
			if (dtoFAPurchasePostingAccountSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoFAPurchasePostingAccountSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/leaseCompanySetup/update", method = RequestMethod.POST)
	public ResponseMessage leaseCompanySetupUpdate(@RequestBody DtoLeaseCompanySetup dtoLeaseCompanySetup) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoLeaseCompanySetup); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FALeaseCompanySetup faLeaseCompanySetup = repositoryFALeaseCompanySetup
					.findOne(dtoLeaseCompanySetup.getCompanyIndex());
			if (faLeaseCompanySetup != null) {
				VendorMaintenance maintenance = repositoryVendorMaintenance.findOne(dtoLeaseCompanySetup.getVendorId());
				if (maintenance != null) {
					dtoLeaseCompanySetup = serviceFixedAssets.updateLeaseCompanySetup(dtoLeaseCompanySetup,
							faLeaseCompanySetup);
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
							.getMessageByShortAndIsDeleted(MessageLabel.FA_LEASE_COMPANY_SETUP_SUCCESS, false),
							dtoLeaseCompanySetup);

				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VENDOR_NOT_AVIALABLE, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	@RequestMapping(value = "/leaseCompanySetup/getAll", method = RequestMethod.POST)
	public ResponseMessage leaseCompanySetupGetAll(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<FALeaseCompanySetup> faLeaseCompanySetupList = repositoryFALeaseCompanySetup.findAll();
			if (faLeaseCompanySetupList != null && !faLeaseCompanySetupList.isEmpty()) {
				dtoSearch = serviceFixedAssets.getAllLeaseCompanySetup(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FA_LEASE_COMPANY_SETUP_FETCHED_SUCCESS, false),
						dtoSearch);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Description This service will get Account Group Setup By Search
	 * @return
	 * 
	 */
	@RequestMapping(value = "/getAccountGroupSetupBySearch", method = RequestMethod.POST)
	public ResponseMessage getAccountGroupSetupBySearch(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoSearch = serviceFixedAssets.getAccountGroupSetupBySearch(dtoSearch);
			if (dtoSearch.getRecords() != null) {
				@SuppressWarnings("unchecked")
				List<DtoFaAccountGroupSetup> dtoFaAccountGroupSetup = (List<DtoFaAccountGroupSetup>) dtoSearch
						.getRecords();
				if (dtoFaAccountGroupSetup != null && !dtoFaAccountGroupSetup.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/searchFAPurchasePostingAccountSetup", method = RequestMethod.POST)
	public ResponseMessage searchFAPurchasePostingAccountSetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.searchPurchasePostingAccountSetup(dtoSearch);
				List<FAPurchasePostingAccountSetup> list = (List<FAPurchasePostingAccountSetup>) dtoSearch.getRecords();
				if (list != null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @Desc Update purchase posting account setup
	 * @param dtoFAPurchasePostingAccountSetup
	 * @return
	 */
	@RequestMapping(value = "/purchasePostingAccountSetup/update", method = RequestMethod.POST)
	public ResponseMessage purchasePostingAccountSetupUpdate(
			@RequestBody DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFAPurchasePostingAccountSetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoFAPurchasePostingAccountSetup != null) {

				FAPurchasePostingAccountSetup faPurchasePostingAccountSetup = repositoryFAPurchasePostingAccountSetup
						.findOne(dtoFAPurchasePostingAccountSetup.getFixedAssetClassIndex());
				if (faPurchasePostingAccountSetup != null) {
					dtoFAPurchasePostingAccountSetup = serviceFixedAssets.updatePurchasePostingAccountSetup(
							dtoFAPurchasePostingAccountSetup, faPurchasePostingAccountSetup);
					if (dtoFAPurchasePostingAccountSetup != null) {
						if (dtoFAPurchasePostingAccountSetup.getMessageType() == null) {
							responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
									serviceResponse.getMessageByShortAndIsDeleted(
											MessageLabel.FA_PURCHASE_POSTING_ACCOUNT_SETUP_SUCCESS, false),
									dtoFAPurchasePostingAccountSetup);
						} else {
							responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
									serviceResponse.getMessageByShortAndIsDeleted(
											dtoFAPurchasePostingAccountSetup.getMessageType(), false));
						}
					} else {
						responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.FA_PURCHASE_POSTING_ACCOUNT_SETUP_FAIL, false));
					}
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));

				}

			} else {

				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @Description This method will get Fixed Asset Company Setup
	 * @return
	 */
	@RequestMapping(value = "/getFixedAssetCompanySetupByBookId", method = RequestMethod.POST)
	public ResponseMessage getFixedAssetCompanySetupByBookId(@RequestBody DtoFACompanySetup dtoFACompanySetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			FACompanySetup faCompanySetup = repositoryFACompanySetup
					.findByBookSetupBookInxdAndIsDeleted(dtoFACompanySetup.getBookIndexId(), false);
			if (faCompanySetup != null) {
				dtoFACompanySetup = serviceFixedAssets.getFixAssetCompanySetup(faCompanySetup);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoFACompanySetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getPurchasePostingAccountSetupByClassId", method = RequestMethod.POST)
	public ResponseMessage getPurchasePostingAccountSetupByClassId(HttpServletRequest request,
			@RequestBody DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoFAPurchasePostingAccountSetup = serviceFixedAssets
					.getPurchasePostingAccountSetupByClassId(dtoFAPurchasePostingAccountSetup.getClassId());
			if (dtoFAPurchasePostingAccountSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoFAPurchasePostingAccountSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getActiveFAGeneralMaintenance", method = RequestMethod.POST)
	public ResponseMessage getActiveFAGeneralMaintenance(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearch != null) {
				dtoSearch = serviceFixedAssets.getActiveFAGeneralMaintenance(dtoSearch);
				@SuppressWarnings("unchecked")
				List<DtoFAGeneralMaintenance> list = (List<DtoFAGeneralMaintenance>) dtoSearch.getRecords();
				if (list != null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

}
