/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.BankSetup;
import com.bti.model.CreditCardsSetup;
import com.bti.model.PaymentTermsSetup;
import com.bti.model.dto.DtoAccountTableSetup;
import com.bti.model.dto.DtoBankSetUp;
import com.bti.model.dto.DtoCreditCardSetUp;
import com.bti.model.dto.DtoModuleConfiguration;
import com.bti.model.dto.DtoPaymentTermSetUp;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoShipmentMethodSetup;
import com.bti.model.dto.DtoStatusType;
import com.bti.model.dto.DtoVatDetailSetup;
import com.bti.repository.RepositoryBankSetup;
import com.bti.repository.RepositoryCreditCardsSetup;
import com.bti.repository.RepositoryPaymentTermSetup;
import com.bti.repository.RepositoryShipmentMethodSetup;
import com.bti.service.ServiceBankSetup;
import com.bti.service.ServiceCompanySetup;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerHome
 * Name of Project: BTI
 * Created on: Aug 04, 2017
 * Modified on: Aug 04, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/setup/company")
public class ControllerCompanySetup {

	private static final Logger LOGGER = Logger.getLogger(ControllerCompanySetup.class);
	
	@Autowired
	ServiceCompanySetup serviceCompanySetup;
	
	@Autowired
	ServiceBankSetup serviceBankSetup;
	
	@Autowired
	RepositoryBankSetup repositoryBankSetup;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryPaymentTermSetup repositoryPaymentTermSetup;
	
	@Autowired
	RepositoryShipmentMethodSetup repositoryShipmentMethodSetup;
	
	@Autowired
	RepositoryCreditCardsSetup repositoryCreditCardsSetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @description : method will save or update the company module
	 * @param 
	 * @param 
	 * @return
	 */
	@RequestMapping(value = "/configure/module", method = RequestMethod.POST)
	public ResponseMessage saveOrUpdateModuleConfiguration(@RequestBody DtoModuleConfiguration dtoModuleConfiguration)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoModuleConfiguration); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if (dtoModuleConfiguration != null) {
				if (dtoModuleConfiguration.getSeriesId() > 0) {
					dtoModuleConfiguration = serviceCompanySetup.saveOrUpdateModuleConfiguration(dtoModuleConfiguration);
	
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MODULE_CONFIG_SETUP_UPDATED_SUCCESS, false),
							dtoModuleConfiguration);
				} else {
					dtoModuleConfiguration = serviceCompanySetup.saveOrUpdateModuleConfiguration(dtoModuleConfiguration);
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MODULE_CONFIG_SETUP_SUCCESS, false),
							dtoModuleConfiguration);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MODULE_CONFIG_SETUP_FAIL, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will create payment term setup
	 * @param dtoPaymentTermSetUp
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/paymentTermSetup", method = RequestMethod.POST)
	public ResponseMessage paymentTermSetup(@RequestBody DtoPaymentTermSetUp dtoPaymentTermSetUp)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		   PaymentTermsSetup paymentTermsSetup = repositoryPaymentTermSetup
				.findByPaymentTermIdAndIsDeleted(dtoPaymentTermSetUp.getPaymentTermId(),false);
			if (paymentTermsSetup == null) {
				dtoPaymentTermSetUp = serviceCompanySetup.newCompanyPaymentTermSetup(dtoPaymentTermSetUp);
				if (dtoPaymentTermSetUp.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.PAYMENT_TERM_SET_UP_ADDED, false));
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(dtoPaymentTermSetUp.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.PAYMENT_TERM_ID_EXIST, false));
		    }
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will create bank setup
	 * @param request
	 * @param dtoBankSetUp
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/bankSetup", method = RequestMethod.POST)
	public ResponseMessage bankSetup(HttpServletRequest request, @RequestBody DtoBankSetUp dtoBankSetUp)  {
		ResponseMessage responseMessage = null;
		String checkCompanyAccess=serviceHome.checkValidCompanyAccess();
		if(checkCompanyAccess.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		boolean flag = false;
		if (dtoBankSetUp != null) {
			BankSetup bankSetup = repositoryBankSetup.findByBankIdAndIsDeleted(dtoBankSetUp.getBankId(),false);
			if (bankSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BANK_ALREADY_EXIST, false));
			} else {
				flag = serviceBankSetup.saveBankSetup(dtoBankSetUp);
				if (flag) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BANK_SETUP_SUCCESS, false), dtoBankSetUp);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BANK_SETUP_FAIL, false), dtoBankSetUp);
				}
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(checkCompanyAccess, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will get payment term setup by id
	 * @param dtoPaymentTermSetUp
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/paymentTermSetupGetById", method = RequestMethod.POST)
	public ResponseMessage paymentTermSetupGetById(@RequestBody DtoPaymentTermSetUp dtoPaymentTermSetUp)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPaymentTermSetUp = serviceCompanySetup.getPaymentTermSetUpById(dtoPaymentTermSetUp);
		if (dtoPaymentTermSetUp != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), dtoPaymentTermSetUp);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will get bank setup by id
	 * @param request
	 * @param dtoBankSetUp
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/bankSetupGetById", method = RequestMethod.POST)
	public ResponseMessage bankSetupGetById(HttpServletRequest request, @RequestBody DtoBankSetUp dtoBankSetUp)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoBankSetUp = serviceBankSetup.bankSetupGetById(dtoBankSetUp.getBankId());
		if (dtoBankSetUp != null && dtoBankSetUp.getBankDescription() != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), dtoBankSetUp);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		return responseMessage;
	}
	
	/**
	 * @description : method will get configured module
	 * @param dtoSearch
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/get/configure/module", method = RequestMethod.POST)
	public ResponseMessage getModuleConfiguration(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSearch.getSearchKeyword() != null) {
			dtoSearch = serviceCompanySetup.getModuleConfigurationList(dtoSearch);
			if (dtoSearch != null) {
				@SuppressWarnings("unchecked")
				List<DtoModuleConfiguration> list = (List<DtoModuleConfiguration>) dtoSearch.getRecords();
				if (list != null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MODULE_CONFIG_SETUP_LIST_SUCCESS, false),
							dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
 	}
	
	/**
	 * @description : method will update payment term setup
	 * @param dtoPaymentTermSetUp
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/updatePaymentTermSetup", method = RequestMethod.POST)
	public ResponseMessage updatePaymentTermSetup(@RequestBody DtoPaymentTermSetUp dtoPaymentTermSetUp)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPaymentTermSetUp); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		PaymentTermsSetup paymentTermsSetup = repositoryPaymentTermSetup
				.findByPaymentTermIdAndIsDeleted(dtoPaymentTermSetUp.getPaymentTermId(),false);
		if (paymentTermsSetup != null) {
			dtoPaymentTermSetUp = serviceCompanySetup.updateCompanyPaymentTermSetup(dtoPaymentTermSetUp,
					paymentTermsSetup);
			if (dtoPaymentTermSetUp.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.PAYMENT_TERM_SET_UP_UPDATED, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(dtoPaymentTermSetUp.getMessageType(), false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.PAYMENT_TERM_ID_NOT_EXIST, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get all or search payment term setup
	 * @param dtoSearch
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/searchPaymentTermSetup", method = RequestMethod.POST)
	public ResponseMessage searchPaymentTermSetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceCompanySetup.searchPaymentTermSetUp(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoPaymentTermSetUp> paymentTermSetUpsList = (List<DtoPaymentTermSetUp>) dtoSearch.getRecords();
			if (paymentTermSetUpsList != null && !paymentTermSetUpsList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will update bank setup
	 * @param request
	 * @param dtoBankSetUp
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/bankSetupUpdate", method = RequestMethod.POST)
	public ResponseMessage bankSetupUpdate(HttpServletRequest request, @RequestBody DtoBankSetUp dtoBankSetUp)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoBankSetUp); 
		
		String checkCompanyAccess=serviceHome.checkValidCompanyAccess();
		if(checkCompanyAccess.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		boolean flag = false;
		flag = serviceBankSetup.updateBankSetup(dtoBankSetUp);
		if (flag) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BANK_SETUP_UPDATE_SUCCESS, false), dtoBankSetUp);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(checkCompanyAccess, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get configured module
	 * @param dtoModuleConfiguration
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getOne/configure/module", method = RequestMethod.POST)
	public ResponseMessage getModuleConfigurationById(@RequestBody DtoModuleConfiguration dtoModuleConfiguration)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoModuleConfiguration.getSeriesId() > 0) {
			dtoModuleConfiguration = serviceCompanySetup.getOneModuleBySeriesId(dtoModuleConfiguration.getSeriesId());
			if (dtoModuleConfiguration != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoModuleConfiguration);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MODULE_CONFIG_SETUP_LIST_FAIL, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MODULE_CONFIG_SETUP_LIST_FAIL, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will get all or search bank setup
	 * @param dtoSearch
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/searchBankSetup", method = RequestMethod.POST)
	public ResponseMessage searchBankSetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceBankSetup.searchBankSetUp(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoPaymentTermSetUp> paymentTermSetUpsList = (List<DtoPaymentTermSetUp>) dtoSearch.getRecords();
			if (paymentTermSetUpsList != null && !paymentTermSetUpsList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will save the credit card setup under the company setup module
	 * @param request
	 * @param dtoCreditCardSetUp
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/saveCreditCardSetup", method = RequestMethod.POST)
	public ResponseMessage saveCreditCardSetup(HttpServletRequest request,
			@RequestBody DtoCreditCardSetUp dtoCreditCardSetUp)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCreditCardSetUp); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCreditCardSetUp != null) {
			CreditCardsSetup creditCardsSetup = repositoryCreditCardsSetup.findByCreditCardNameAndIsDeleted(dtoCreditCardSetUp.getCreditCardName(),false);
			if(creditCardsSetup!=null){
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), dtoCreditCardSetUp);
			}
			else
			{
				dtoCreditCardSetUp = this.serviceCompanySetup.saveCreditCardSetup(dtoCreditCardSetUp);
				if (dtoCreditCardSetUp != null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CREDIT_CARD_SETUP_SUCCESSFULLY, false),
							dtoCreditCardSetUp);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CREDIT_CARD_SETUP_FAIL, false), null);
				}
			}
			
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will return the credit card setup by ID under the company setup module
	 * @param request
	 * @param dtoSearchs
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getCreditCardSetupById", method = RequestMethod.POST)
	public ResponseMessage getCreditCardSetupById(HttpServletRequest request,
			@RequestBody DtoCreditCardSetUp dtoCreditCardSetUp)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCreditCardSetUp != null) {
			DtoCreditCardSetUp response = this.serviceCompanySetup.getCreditCardSetupById(dtoCreditCardSetUp);
			if (response != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will update the credit card setup under the company setup module
	 * @param request
	 * @param dtoCreditCardSetUp
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/updateCreditCardSetup", method = RequestMethod.POST)
	public ResponseMessage updateCreditCardSetup(HttpServletRequest request,
			@RequestBody DtoCreditCardSetUp dtoCreditCardSetUp)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCreditCardSetUp); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCreditCardSetUp != null) {
			DtoCreditCardSetUp result = this.serviceCompanySetup.updateCreditCardSetup(dtoCreditCardSetUp);
			if (result != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.CREDIT_CARD_SETUP_UPDATED_SUCCESSFULLY, false),
						dtoCreditCardSetUp);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CREDIT_CARD_SETUP_UPDATE_FAIL, false),
						null);
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	/**
	 * @description : method will search the credit card setup under the company setup module
	 * @param request
	 * @param dtoSearchs
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/searchCreditCardSetup", method = RequestMethod.POST)
	public ResponseMessage searchCreditCardSetup(HttpServletRequest request, @RequestBody DtoSearch dtoSearchs)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSearchs != null) {
			DtoSearch response = this.serviceCompanySetup.searchCreditCardSetup(dtoSearchs);
			if (response != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CREDIT_CARD_SETUP_LIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will save the shipment method setup under the company setup module
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/shipmentMethodSetup", method = RequestMethod.POST)
	public ResponseMessage shipmentMethodSetup(HttpServletRequest request,
			@RequestBody DtoShipmentMethodSetup dtoShipmentMethodSetup)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoShipmentMethodSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoShipmentMethodSetup = this.serviceCompanySetup.saveShipmentMethodSetup(dtoShipmentMethodSetup);

		if (dtoShipmentMethodSetup != null) {
			if (dtoShipmentMethodSetup.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SHIPMENT_METHOD_SETUP_SUCCESS, false),
						dtoShipmentMethodSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(dtoShipmentMethodSetup.getMessageType(),
								false),
						null);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SHIPMENT_METHOD_SETUP_FAIL, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	
	/**
	 * @description : method will update the shipment method setup under the company setup module
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/updateShipmentMethodSetup", method = RequestMethod.POST)
	public ResponseMessage updateShipmentMethodSetup(HttpServletRequest request,
			@RequestBody DtoShipmentMethodSetup dtoShipmentMethodSetup)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoShipmentMethodSetup); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoShipmentMethodSetup = this.serviceCompanySetup.updateShipmentMethodSetup(dtoShipmentMethodSetup);
		if (dtoShipmentMethodSetup != null) {
			if (dtoShipmentMethodSetup.getMessageType() == null) {
				responseMessage = new ResponseMessage(
						HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse
								.getMessageByShortAndIsDeleted(MessageLabel.SHIPMENT_METHOD_SETUP_UPDATE_SUCCESS, false),
						dtoShipmentMethodSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(dtoShipmentMethodSetup.getMessageType(),
								false),
						null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SHIPMENT_METHOD_SETUP_UPDATE_FAIL, false),
					null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will return the shipment method setup by ID under the company setup module
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getShipmentMethodSetupById", method = RequestMethod.POST)
	public ResponseMessage getShipmentMethodSetupById(HttpServletRequest request,
			@RequestBody DtoShipmentMethodSetup dtoShipmentMethodSetup)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoShipmentMethodSetup = this.serviceCompanySetup.getShipmentMethodSetupById(dtoShipmentMethodSetup);
		if (dtoShipmentMethodSetup != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoShipmentMethodSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will search the shipment method setup under the company setup module
	 * @param dtoSearch
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/searchShipmentMethodSetup", method = RequestMethod.POST)
	public ResponseMessage searchShipmentMethodSetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceCompanySetup.searchShipmentMethodSetup(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoShipmentMethodSetup> dtoMainAccountSetUpList = (List<DtoShipmentMethodSetup>) dtoSearch
					.getRecords();
			if (dtoMainAccountSetUpList != null && !dtoMainAccountSetUpList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will return the master type of shipment method setup under the company setup module
	 * @param request
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getShipmentMethodTypeStatus", method = RequestMethod.POST)
	public ResponseMessage getShipmentMethodTypeStatus(HttpServletRequest request)  {
		LOGGER.info("inside maintenance method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceCompanySetup.getShipmentMethodTypeStatus();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will return the master type of credit card setup under the company setup module
	 * @param request
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getCreditCardTypeStatus", method = RequestMethod.POST)
	public ResponseMessage getCreditCardTypeStatus(HttpServletRequest request)  {
		LOGGER.info("inside getCreditCardTypeStatus method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceCompanySetup.getCreditCardTypeStatus();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	
	/**
	 * @description : method will save the vat detail setup under the company setup module
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/vatDetailSetup", method = RequestMethod.POST)
	public ResponseMessage vatDetailSetup(HttpServletRequest request,
			@RequestBody DtoVatDetailSetup dtoVatDetailSetup)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoVatDetailSetup); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoVatDetailSetup = this.serviceCompanySetup.saveVatDetailSetup(dtoVatDetailSetup);

		if (dtoVatDetailSetup != null) {
			if (dtoVatDetailSetup.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VAT_DETAIL_SETUP_SUCCESS, false),
						dtoVatDetailSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(dtoVatDetailSetup.getMessageType(), false),
						null);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VAT_DETAIL_SETUP_FAIL, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will update the vat detail setup under the company setup module
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/updateVatDetailSetup", method = RequestMethod.POST)
	public ResponseMessage updateVatDetailSetup(HttpServletRequest request,
			@RequestBody DtoVatDetailSetup dtoVatDetailSetup)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoVatDetailSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoVatDetailSetup = this.serviceCompanySetup.updateVatDetailSetup(dtoVatDetailSetup);
		if (dtoVatDetailSetup != null) {
			if (dtoVatDetailSetup.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VAT_DETAIL_SETUP_UPDATE_SUCCESS, false),
						dtoVatDetailSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(dtoVatDetailSetup.getMessageType(), false),
						null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VAT_DETAIL_SETUP_UPDATE_FAIL, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will return  the vat detail setup by ID under the company setup module
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getVatDetailSetupById", method = RequestMethod.POST)
	public ResponseMessage getVatDetailSetupById(HttpServletRequest request,
			@RequestBody DtoVatDetailSetup dtoVatDetailSetup)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoVatDetailSetup = this.serviceCompanySetup.getVatDetailSetupById(dtoVatDetailSetup);
		if (dtoVatDetailSetup != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoVatDetailSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will search the vat detail setup under the company setup module
	 * @param dtoSearch
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/searchVatDetailSetup", method = RequestMethod.POST)
	public ResponseMessage searchVatDetailSetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceCompanySetup.searchVatDetailSetup(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoShipmentMethodSetup> dtoMainAccountSetUpList = (List<DtoShipmentMethodSetup>) dtoSearch
					.getRecords();
			if (dtoMainAccountSetUpList != null && !dtoMainAccountSetUpList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description : method will return the master vat series type of vat detail setup under the company setup module
	 * @param request
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getVatSeriesTypeStatus", method = RequestMethod.POST)
	public ResponseMessage getVatSeriesTypeStatus(HttpServletRequest request)  {
		LOGGER.info("inside getVatSeriesTypeStatus method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceCompanySetup.getVatType();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will return the master vat based on type of vat detail setup under the company setup module
	 * @param request
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getVatBasedOnTypeStatus", method = RequestMethod.POST)
	public ResponseMessage getVatBasedOnTypeStatus(HttpServletRequest request)  {
		LOGGER.info("inside getVatBasedOnTypeStatus method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceCompanySetup.getVatBasedOnTypeStatus();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoPaymentTermSetUp
	 * @return
	 * @
	 */
	@RequestMapping(value = "/paymentTermSetup/delete", method = RequestMethod.POST)
	public ResponseMessage paymentTermSetupDelete(@RequestBody DtoPaymentTermSetUp dtoPaymentTermSetUp)  
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPaymentTermSetUp); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.deleteMultiplePaymentTermSetup(dtoPaymentTermSetUp);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(),
							HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_FAIL, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @param dtoPaymentTermSetUp
	 * @return
	 * @
	 */
	@RequestMapping(value = "/paymentTermSetup/checkRelation", method = RequestMethod.POST)
	public ResponseMessage paymentTermSetupCheckRelation(@RequestBody DtoPaymentTermSetUp dtoPaymentTermSetUp)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.checkRelationOfPaymentTermSetup(dtoPaymentTermSetUp);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RELATION_TO_ANOTHER_TABLES_EXIST, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(),
							HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoCreditCardSetUp
	 * @return
	 * @
	 */
	@RequestMapping(value = "/creditCardSetup/delete", method = RequestMethod.POST)
	public ResponseMessage creditCardSetupDelete(@RequestBody DtoCreditCardSetUp dtoCreditCardSetUp)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCreditCardSetUp); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.deleteMultipleCreditCardsSetup(dtoCreditCardSetUp);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(),
							HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_FAIL, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @param dtoCreditCardSetUp
	 * @return
	 * @
	 */
	@RequestMapping(value = "/creditCardSetup/checkRelation", method = RequestMethod.POST)
	public ResponseMessage creditCardSetupCheckRelation(@RequestBody DtoCreditCardSetUp dtoCreditCardSetUp)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.checkRelationOfCreditCardsSetup(dtoCreditCardSetUp);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RELATION_TO_ANOTHER_TABLES_EXIST, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(),
							HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("RECORD_NOT_FOUND", false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoBankSetUp
	 * @return
	 * @
	 */
	@RequestMapping(value = "/bankSetup/delete", method = RequestMethod.POST)
	public ResponseMessage bankSetupDelete(@RequestBody DtoBankSetUp dtoBankSetUp)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoBankSetUp); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.deleteMultipleBankSetup(dtoBankSetUp);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(),
							HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_FAIL, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @param dtoBankSetUp
	 * @return
	 * @
	 */
	@RequestMapping(value = "/bankSetup/checkRelation", method = RequestMethod.POST)
	public ResponseMessage bankSetupCheckRelation(@RequestBody DtoBankSetUp dtoBankSetUp)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.checkRelationOfBankSetup(dtoBankSetUp);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RELATION_TO_ANOTHER_TABLES_EXIST, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(),
							HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoModuleConfiguration
	 * @return
	 * @
	 */
	@RequestMapping(value = "/configure/module/delete", method = RequestMethod.POST)
	public ResponseMessage configureModuleDelete(@RequestBody DtoModuleConfiguration dtoModuleConfiguration)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoModuleConfiguration); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.deleteMultipleConfigureModule(dtoModuleConfiguration);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(),
							HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_FAIL, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @param dtoModuleConfiguration
	 * @return
	 * @
	 */
	@RequestMapping(value = "/configure/module/checkRelation", method = RequestMethod.POST)
	public ResponseMessage configureModuleCheckRelation(@RequestBody DtoModuleConfiguration dtoModuleConfiguration)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.checkRelationOfConfigureModule(dtoModuleConfiguration);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RELATION_TO_ANOTHER_TABLES_EXIST, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(),
							HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoShipmentMethodSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/shipmentMethodSetup/delete", method = RequestMethod.POST)
	public ResponseMessage shipmentMethodSetupDelete(@RequestBody DtoShipmentMethodSetup dtoShipmentMethodSetup)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoShipmentMethodSetup); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.deleteMultipleShipmentMethodSetup(dtoShipmentMethodSetup);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(),
							HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_FAIL, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @param dtoShipmentMethodSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/shipmentMethodSetup/checkRelation", method = RequestMethod.POST)
	public ResponseMessage shipmentMethodSetupCheckRelation(@RequestBody DtoShipmentMethodSetup dtoShipmentMethodSetup)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.checkRelationOfShipmentMethodSetup(dtoShipmentMethodSetup);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RELATION_TO_ANOTHER_TABLES_EXIST, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(),
							HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param dtoVatDetailSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/vatDetailSetup/delete", method = RequestMethod.POST)
	public ResponseMessage vatDetailSetupDelete(@RequestBody DtoVatDetailSetup dtoVatDetailSetup)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoVatDetailSetup); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.deleteMultipleVatDetailSetup(dtoVatDetailSetup);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(),
							HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_FAIL, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @param dtoVatDetailSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/vatDetailSetup/checkRelation", method = RequestMethod.POST)
	public ResponseMessage vatDetailSetupCheckRelation(@RequestBody DtoVatDetailSetup dtoVatDetailSetup)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				boolean result = serviceCompanySetup.checkRelationOfVatDetailSetup(dtoVatDetailSetup);
				if (result) 
				{
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RELATION_TO_ANOTHER_TABLES_EXIST, false));
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(),
							HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
				}
	
	@RequestMapping(value = "/getSalePurchaseAmountByAccountNumber", method = RequestMethod.POST)
	public ResponseMessage getSalePurchaseAmountByAccountNumber(@RequestBody DtoAccountTableSetup dtoAccountTableSetup)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				DtoVatDetailSetup dtoVatDetailSetup=serviceCompanySetup.getSalePurchaseAmountByAccountNumber(dtoAccountTableSetup.getAccountTableRowIndex());
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),dtoVatDetailSetup);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
}
