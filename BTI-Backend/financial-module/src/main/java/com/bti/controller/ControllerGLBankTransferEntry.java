/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoGLBankTransferCharges;
import com.bti.model.dto.DtoGLBankTransferDistribution;
import com.bti.model.dto.DtoGLBankTransferHeader;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.service.ServiceCurrencyExchange;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceGLBankTransferEntry;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerGLBankTransferEntry
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/generalLedger")
public class ControllerGLBankTransferEntry {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ControllerGLBankTransferEntry.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	
	@Autowired
	ServiceCurrencyExchange serviceCurrencyExchange;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	ServiceGLBankTransferEntry serviceGLBankTransferEntry;
	
	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;
	
	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;
	
	/**
	 * @Description:this service will use to save bank transfer entry  
	 * @param request
	 * @param dtoGLCashReceiptBank
	 * @return
	 * @
	 */
	@RequestMapping(value = "/saveBankTransferEntry", method = RequestMethod.POST)
	public ResponseMessage saveBankTransferEntry(HttpServletRequest request,@RequestBody DtoGLBankTransferHeader dtoGLBankTransferHeader)  
    {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLBankTransferHeader); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLBankTransferHeader = serviceGLBankTransferEntry.saveBankTransferEntry(dtoGLBankTransferHeader);
			if (dtoGLBankTransferHeader != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),
						dtoGLBankTransferHeader);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BANK_TRANSFER_NUMBER_ALREADY_EXIST, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description:this service will use to update bank transfer entry  
	 * @param request
	 * @param dtoGLCashReceiptBank
	 * @return
	 * @
	 */
	@RequestMapping(value = "/updateBankTransferEntry", method = RequestMethod.POST)
	public ResponseMessage updateBankTransferEntry(HttpServletRequest request,@RequestBody DtoGLBankTransferHeader dtoGLBankTransferHeader)  
    {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLBankTransferHeader); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLBankTransferHeader = serviceGLBankTransferEntry.updateBankTransferEntry(dtoGLBankTransferHeader);
			if (dtoGLBankTransferHeader != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_UPDATED_SUCCESSFULLY, false),
						dtoGLBankTransferHeader);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	
	/**
	 * @Description:this service will use to get bank transfer entry  
	 * @param request
	 * @param dtoGLCashReceiptBank
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getBankTransferEntry", method = RequestMethod.POST)
	public ResponseMessage getBankTransferEntry(HttpServletRequest request,@RequestBody DtoGLBankTransferHeader dtoGLBankTransferHeader)  
    {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLBankTransferHeader = serviceGLBankTransferEntry.getBankTransferEntry(dtoGLBankTransferHeader);
			if (dtoGLBankTransferHeader != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoGLBankTransferHeader);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to bank transfer distribution
	 * @param request
	 * @param dtoGLBankTransferDistribution
	 * @return
	 */
	@RequestMapping(value = "/getBankTransferDistrubutionByBankTransferNumber", method = RequestMethod.POST)
	public ResponseMessage getBankTransferDistrubutionByBankTransferNumber(HttpServletRequest request,@RequestBody DtoGLBankTransferDistribution dtoGLBankTransferDistribution)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoGLBankTransferDistribution = serviceGLBankTransferEntry.getBankTransferDistrubutionByBankTransferNumber(dtoGLBankTransferDistribution.getBankTransferNumber());
			if (dtoGLBankTransferDistribution != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoGLBankTransferDistribution);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to save bank transfer distribution
	 * @param request
	 * @param dtoGLBankTransferDistribution
	 * @return
	 */
	@RequestMapping(value = "/saveBankTransferDistribution", method = RequestMethod.POST)
	public ResponseMessage saveBankTransferDistrubution(HttpServletRequest request,@RequestBody DtoGLBankTransferDistribution dtoGLBankTransferDistribution)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLBankTransferDistribution); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoGLBankTransferDistribution = serviceGLBankTransferEntry.saveBankTransferDistribution(dtoGLBankTransferDistribution);
			if (dtoGLBankTransferDistribution!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY,false), dtoGLBankTransferDistribution);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to bank transfer charges
	 * @param request
	 * @param dtoGLBankTransferCharges
	 * @return
	 */
	@RequestMapping(value = "/getBankTransferChargesByBankTransferNumber", method = RequestMethod.POST)
	public ResponseMessage getBankTransferChargesByBankTransferNumber(HttpServletRequest request,@RequestBody DtoGLBankTransferCharges dtoGLBankTransferCharges)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoGLBankTransferCharges = serviceGLBankTransferEntry.getBankTransferChargesByBankTransferNumber(dtoGLBankTransferCharges);
			if (dtoGLBankTransferCharges != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoGLBankTransferCharges);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to check charge number already exist or not
	 * @param request
	 * @param dtoGLBankTransferCharges
	 * @return
	 */
	@RequestMapping(value = "/getBankTransferChargesByChargeNumber", method = RequestMethod.POST)
	public ResponseMessage getBankTransferChargesByChargeNumber(HttpServletRequest request,@RequestBody DtoGLBankTransferCharges dtoGLBankTransferCharges)  
	{
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLBankTransferCharges = serviceGLBankTransferEntry
					.getBankTransferChargesByChargeNumber(dtoGLBankTransferCharges);
			if (dtoGLBankTransferCharges!=null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CHARGE_NUMBER_ALREADY_EXIST, false),dtoGLBankTransferCharges);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to save bank transfer charges
	 * @param request
	 * @param dtoGLBankTransferCharges
	 * @return
	 */
	@RequestMapping(value = "/saveBankTransferCharges", method = RequestMethod.POST)
	public ResponseMessage saveBankTransferCharges(HttpServletRequest request,@RequestBody DtoGLBankTransferCharges dtoGLBankTransferCharges)  
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLBankTransferCharges); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoGLBankTransferCharges = serviceGLBankTransferEntry.saveBankTransferCharges(dtoGLBankTransferCharges);
			if (dtoGLBankTransferCharges!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY,false), dtoGLBankTransferCharges);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_FAIL, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoGLBankTransferHeader
	 * @return
	 * @throws ScriptException 
	 */
	@RequestMapping(value = "/postBankTransferEntry", method = RequestMethod.POST)
	public ResponseMessage postBankTransferEntry(HttpServletRequest request,@RequestBody DtoGLBankTransferHeader dtoGLBankTransferHeader) throws ScriptException  
    {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLBankTransferHeader); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			
				boolean response = serviceGLBankTransferEntry.postBankTransferEntry(dtoGLBankTransferHeader);
				if (response) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_POST_SUCCESSFULLY, false));
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	@RequestMapping(value = "/getBankTranferNumbersList", method = RequestMethod.GET)
	public ResponseMessage getBankTranferNumbersList(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoGLBankTransferHeader> list = serviceGLBankTransferEntry.getBankTransferNumbersList();
			if (!list.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to delete Bank Transfer Number 
	 * @param request
	 * @param dtoGLBankTransferCharges
	 * @return
	 * @
	 */
	@RequestMapping(value = "/deleteBankTransferEntry", method = RequestMethod.POST)
	public ResponseMessage deleteBankTransferEntry(HttpServletRequest request,
			@RequestBody DtoGLBankTransferCharges dtoGLBankTransferCharges) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLBankTransferCharges); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceGLBankTransferEntry.deleteBankTransferEntry(dtoGLBankTransferCharges);
			if (status) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	@RequestMapping(value = "/deleteBankTransferEntryCharges", method = RequestMethod.POST)
	public ResponseMessage deleteBankTransferEntryCharges(HttpServletRequest request,
			@RequestBody DtoGLBankTransferCharges dtoGLBankTransferCharges) 
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLBankTransferCharges); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceGLBankTransferEntry.deleteBankTransferEntryCharges(dtoGLBankTransferCharges);
			if (status) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/*@RequestMapping(value = "/checkBankTransferDistribution", method = RequestMethod.POST)
	public ResponseMessage checkCashReceiptDistribution(HttpServletRequest request, @RequestBody DtoGLBankTransferHeader dtoGLBankTransferHeader ) throws ScriptException  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 boolean response = this.serviceGLBankTransferEntry.checkDistributionDetailIsAvailable(dtoGLBankTransferHeader.getBankTransferNumber());
			if (response) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WRONG_DISTRIBUTION, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}*/
}
