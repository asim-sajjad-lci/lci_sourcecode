/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.NationalAccountMaintenanceHeader;
import com.bti.model.SalesTerritoryMaintenance;
import com.bti.model.dto.DtoNationalAccountMaintenanceHeader;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSalesTerritorySetup;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryNationalAccountMaintenanceHeader;
import com.bti.repository.RepositorySalesTerritoryMaintenance;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceNationalAccountMaintenance;
import com.bti.service.ServiceResponse;
import com.bti.service.ServiceSalesTerritorySetup;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerAccountPayableMaster Name of Project: BTI Created on:
 * Aug 24, 2017 Modified on: Aug 24, 2017 09:42:38 AM
 * 
 * @author seasia Version:
 */
@RestController
@RequestMapping("/account/receivable")
public class ControllerAccountReceivableMaster {

	private static final Logger LOG = Logger.getLogger(ControllerAccountReceivableMaster.class);
	
	@Autowired
	RepositorySalesTerritoryMaintenance repositorySalesTerritoryMaintenance;
	
	@Autowired
	ServiceSalesTerritorySetup serviceSalesTerritorySetup;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryNationalAccountMaintenanceHeader repositoryNationalAccountMaintenanceHeader;
	
	@Autowired
	ServiceNationalAccountMaintenance serviceNationalAccountMaintenance;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @Description Save sales territory setup
	 * @param dtoSalesTerritorySetup
	 * @return
	 */
	@RequestMapping(value = "/salesTerritorySetup", method = RequestMethod.POST)
	public ResponseMessage salesTerritorySetup(@RequestBody DtoSalesTerritorySetup dtoSalesTerritorySetup) {
		LOG.info("inside salesTerritorySetup method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoSalesTerritorySetup); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		dtoSalesTerritorySetup = serviceSalesTerritorySetup.saveSalesTerritorySetup(dtoSalesTerritorySetup);
		if(dtoSalesTerritorySetup.getSalesTerritoryId() != null){
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.SALES_TERRITORY_SETUP_SUCCESS, false),
					dtoSalesTerritorySetup);
		}else{
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SALES_TERRITORY_SETUP_FAIL, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description Update sales territory setup
	 * @param dtoSalesTerritorySetup
	 * @return
	 */
	@RequestMapping(value = "/salesTerritorySetup/ ", method = RequestMethod.POST)
	public ResponseMessage updateSalesTerritorySetup(@RequestBody DtoSalesTerritorySetup dtoSalesTerritorySetup) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoSalesTerritorySetup); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		SalesTerritoryMaintenance maintenance = repositorySalesTerritoryMaintenance
				.findOne(dtoSalesTerritorySetup.getSalesTerritoryId());
		if (maintenance != null) {
			dtoSalesTerritorySetup = serviceSalesTerritorySetup.updateSalesTerritorySetup(dtoSalesTerritorySetup,
					maintenance);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.SALES_TERRITORY_SETUP_UPDATE_SUCCESS, false),
					dtoSalesTerritorySetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description Get sales territory setup by id
	 * @param dtoSalesTerritorySetup
	 * @return
	 */
	@RequestMapping(value = "/salesTerritorySetup/getById", method = RequestMethod.POST)
	public ResponseMessage getSalesTerritorySetup(@RequestBody DtoSalesTerritorySetup dtoSalesTerritorySetup) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		SalesTerritoryMaintenance maintenance = repositorySalesTerritoryMaintenance
				.findOne(dtoSalesTerritorySetup.getSalesTerritoryId());
		if (maintenance != null) {
			dtoSalesTerritorySetup = serviceSalesTerritorySetup.getSalesTerritorySetupById(maintenance);
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoSalesTerritorySetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description Search or get all sales territory setup
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/salesTerritorySetup/search", method = RequestMethod.POST)
	public ResponseMessage searchSalesTerritorySetup(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) 
		{
		if(dtoSearch != null){
			dtoSearch = serviceSalesTerritorySetup.searchSalesTerritorySetup(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.SALES_TERRITORY_SETUP_FETCHED_SUCCESS, false),
					dtoSearch);
		}else{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will add national account maintenance
	 * @param dtoAccountMaintenanceHeader
	 * @return
	 * 
	 */
	@RequestMapping(value = "/nationalAccountMaintenance", method = RequestMethod.POST)
	public ResponseMessage nationalAccountMaintenance(
			@RequestBody DtoNationalAccountMaintenanceHeader dtoAccountMaintenanceHeader) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAccountMaintenanceHeader); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		NationalAccountMaintenanceHeader accountMaintenanceHeader = repositoryNationalAccountMaintenanceHeader
					.findByCustomerMaintenanceCustnumberAndIsDeleted(dtoAccountMaintenanceHeader.getCustNumber(),
							false);
		if (accountMaintenanceHeader == null) {
			dtoAccountMaintenanceHeader = serviceNationalAccountMaintenance
					.saveNationalAccountHeader(dtoAccountMaintenanceHeader);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.NATIONAL_ACCOUNT_ADDED_SUCCESS, false),
					dtoAccountMaintenanceHeader);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.NATIONAL_ACCOUNT_ALREADY_EXISTS,
								false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will update national account maintenance
	 * @param dtoAccountMaintenanceHeader
	 * @return
	 */
	@RequestMapping(value = "/nationalAccountMaintenance/update", method = RequestMethod.POST)
	public ResponseMessage updateNationalAccountMaintenance(
			@RequestBody DtoNationalAccountMaintenanceHeader dtoAccountMaintenanceHeader) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAccountMaintenanceHeader); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		NationalAccountMaintenanceHeader accountMaintenanceHeader2 = repositoryNationalAccountMaintenanceHeader
					.findByCustomerMaintenanceCustnumberAndIsDeleted(dtoAccountMaintenanceHeader.getCustNumber(),
							false);
		if (accountMaintenanceHeader2 != null) {
			dtoAccountMaintenanceHeader = serviceNationalAccountMaintenance
					.updateNationalAccountHeader(dtoAccountMaintenanceHeader, accountMaintenanceHeader2);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.NATIONAL_ACCOUNT_UPDATED_SUCCESS, false),
					dtoAccountMaintenanceHeader);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will get national account maintenance
	 * @param dtoAccountMaintenanceHeader
	 * @return
	 * 
	 */
	@RequestMapping(value = "/nationalAccountMaintenance/getById", method = RequestMethod.POST)
	public ResponseMessage getNationalAccountMaintenance(
			@RequestBody DtoNationalAccountMaintenanceHeader dtoAccountMaintenanceHeader) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		NationalAccountMaintenanceHeader accountMaintenanceHeader2 = repositoryNationalAccountMaintenanceHeader
					.findByCustomerMaintenanceCustnumberAndIsDeleted(dtoAccountMaintenanceHeader.getCustNumber(),
							false);
		if (accountMaintenanceHeader2 != null) {
			dtoAccountMaintenanceHeader = serviceNationalAccountMaintenance
					.getNationalAccountHeaderById(dtoAccountMaintenanceHeader, accountMaintenanceHeader2);
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoAccountMaintenanceHeader);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will get all or search any national account
	 *              maintenance
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/nationalAccountMaintenance/search", method = RequestMethod.POST)
	public ResponseMessage searchNationalAccountMaintenance(@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		if (dtoSearch != null) {
			dtoSearch = serviceNationalAccountMaintenance.searchNationalAccountHeader(dtoSearch);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.NATIONAL_ACCOUNT_FETCHED_SUCCESS, false),
					dtoSearch);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
}
