/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoAPManualPayment;
import com.bti.model.dto.DtoAPManualPaymentDistribution;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoStatusType;
import com.bti.model.dto.DtoTypes;
import com.bti.service.ServiceAPManualPayment;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerAPManualPayment
 * Name of Project: BTI
 * Created on: Dec 27, 2017
 * Modified on: Dec 27, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/accountPayable")
public class ControllerAPManualPayment {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ControllerAPManualPayment.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	ServiceAPManualPayment serviceAPManualPayment;
	
	/**
	 * @description this service will use for get master manual payment type of APManualPayment
	 * @param request, dtoFiscalFinancialPeriodSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/getManualPaymentType", method = RequestMethod.GET)
	public ResponseMessage getManualPaymentType(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 List<DtoStatusType> response = this.serviceAPManualPayment.getManualPaymentType();
			if (response!=null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use for getAllManualPaymentNumber
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAllManualPaymentNumber", method = RequestMethod.GET)
	public ResponseMessage getAllManualPaymentNumber(HttpServletRequest request)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<String> receiptNumberList = serviceAPManualPayment.getAllManualPaymentNumber();
			if (receiptNumberList!=null && !receiptNumberList.isEmpty()) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), receiptNumberList);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description:this service will use to saveManualPaymentEntry
	 * @param request
	 * @param dtoAPManualPayment
	 * @return
	 * @
	 */
	@RequestMapping(value = "/saveManualPaymentEntry", method = RequestMethod.POST)
	public ResponseMessage saveManualPaymentEntry(HttpServletRequest request,@RequestBody DtoAPManualPayment dtoAPManualPayment)  
    {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAPManualPayment); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoAPManualPayment = serviceAPManualPayment.saveManualPaymentEntry(dtoAPManualPayment);
			if (dtoAPManualPayment != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),
						dtoAPManualPayment);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECEIPT_NUMBER_ALREADY_EXIST, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);		
		return responseMessage;
	}
	
	/**
	 * @Description:this service will use to updateManualPaymentEntry
	 * @param request
	 * @param dtoAPManualPayment
	 * @return
	 * @
	 */
	@RequestMapping(value = "/updateManualPaymentEntry", method = RequestMethod.POST)
	public ResponseMessage updateManualPaymentEntry(HttpServletRequest request,@RequestBody DtoAPManualPayment dtoAPManualPayment)  
    {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAPManualPayment); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoAPManualPayment = serviceAPManualPayment.updateManualPaymentEntry(dtoAPManualPayment);
			if (dtoAPManualPayment != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_UPDATED_SUCCESSFULLY, false),
						dtoAPManualPayment);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to getManualPaymentEntry
	 * @param request
	 * @param dtoAPManualPayment
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getManualPaymentEntryByManualPaymentNumber", method = RequestMethod.POST)
	public ResponseMessage getManualPaymentEntryByManualPaymentNumber(HttpServletRequest request,@RequestBody DtoAPManualPayment dtoAPManualPayment)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoAPManualPayment = serviceAPManualPayment.getManualPaymentEntryByManualPaymentNumber(dtoAPManualPayment);
			if (dtoAPManualPayment != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoAPManualPayment);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to delete CashReceiptEntry 
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/deleteManualPaymentEntry", method = RequestMethod.POST)
	public ResponseMessage deleteManualPaymentEntryByManualPaymentNumber(HttpServletRequest request,
			@RequestBody DtoAPManualPayment dtoAPManualPayment) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAPManualPayment); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceAPManualPayment.deleteManualPaymentEntryByManualPaymentNumber(dtoAPManualPayment);
			if (status) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description:this service will use to save post cash receipt entry  
	 * @param request
	 * @param dtoAPManualPayment
	 * @return
	 */
	@RequestMapping(value = "/postManualPaymentEntry", method = RequestMethod.POST)
	public ResponseMessage postManualPaymentEntry(HttpServletRequest request,@RequestBody DtoAPManualPayment dtoAPManualPayment)  
    {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) 
		{
			/*boolean checkDistribution = serviceAPManualPayment.checkDistributionDetailIsAvailable(dtoAPManualPayment.getManualPaymentNumber());
			if(checkDistribution)
			{*/
				dtoAPManualPayment = serviceAPManualPayment.postManualPaymentEntry(dtoAPManualPayment);
				if (dtoAPManualPayment.getMessageType()==null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_POST_SUCCESSFULLY, false),
							dtoAPManualPayment);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(dtoAPManualPayment.getMessageType(), false));
				}
			/*}
			else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WRONG_DISTRIBUTION, false));
			}*/
		}
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to cash receipt entry  distribution
	 * @param request
	 * @param dtoAPManualPaymentDistribution
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getManualPaymentDistributionByManualPaymentNumber", method = RequestMethod.POST)
	public ResponseMessage getManualPaymentDistributionByManualPaymentNumber(HttpServletRequest request,@RequestBody DtoAPManualPayment dtoAPManualPayment)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			DtoAPManualPaymentDistribution dtoAPManualPaymentDistribution = serviceAPManualPayment.getManualPaymentDistributionByManualPaymentNumber(dtoAPManualPayment);
			if (dtoAPManualPaymentDistribution != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoAPManualPaymentDistribution);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to get distribution account types list 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getDistributionAccountTypesList", method = RequestMethod.GET)
	public ResponseMessage getDistributionAccountTypesList(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			List<DtoTypes> list = serviceAPManualPayment.getDistributionAccountTypesList();
			if (!list.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to saveManualPaymentDistribution
	 * @param request
	 * @param dtoAPManualPaymentDistribution
	 * @return
	 * @
	 */
	@RequestMapping(value = "/saveManualPaymentDistribution", method = RequestMethod.POST)
	public ResponseMessage saveManualPaymentDistribution(HttpServletRequest request,@RequestBody DtoAPManualPaymentDistribution dtoAPManualPaymentDistribution)  
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAPManualPaymentDistribution); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			dtoAPManualPaymentDistribution = serviceAPManualPayment.saveManualPaymentDistribution(dtoAPManualPaymentDistribution);
			if (dtoAPManualPaymentDistribution!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY,false), dtoAPManualPaymentDistribution);
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_FAIL, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	@RequestMapping(value = "/deleteManualPaymentDistribution", method = RequestMethod.POST)
	public ResponseMessage deleteManualPaymentDistribution(HttpServletRequest request,
			@RequestBody DtoAPManualPayment dtoAPManualPayment) 
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAPManualPayment); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) 
		{
			boolean status = serviceAPManualPayment.deleteDistributionManualPaymentEntry(dtoAPManualPayment);
			if (status) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				return responseMessage;
			}
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
}
