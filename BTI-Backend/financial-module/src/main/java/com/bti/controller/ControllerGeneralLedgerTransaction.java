/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 *//*
package com.bti.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoBatches;
import com.bti.model.dto.DtoGLBankTransferCharges;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.service.ServiceCurrencyExchange;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceGeneralLedgerTransaction;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;

*//**
 * Description: Controller General Ledger Transaction
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 *//*

@RestController
@RequestMapping("/transaction/generalLedger")
public class ControllerGeneralLedgerTransaction {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ControllerGeneralLedgerTransaction.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	
	@Autowired
	ServiceCurrencyExchange serviceCurrencyExchange;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	ServiceGeneralLedgerTransaction serviceGeneralLedgerTransaction;
	
	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;
	
	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;
	 
	
	
}
*/