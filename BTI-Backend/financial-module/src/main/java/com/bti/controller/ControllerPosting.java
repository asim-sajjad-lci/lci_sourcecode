package com.bti.controller;

import java.text.ParseException;

import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.BaseEntity;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.service.ServiceGeneralLedgerSetup;
import com.bti.service.ServiceHome;
import com.bti.service.ServicePosting;
import com.bti.service.ServiceResponse;

@RestController
@RequestMapping("post")
public class ControllerPosting {
	
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	ServiceGeneralLedgerSetup serviceGeneralLedgerSetup;
	
	@Autowired
	ServicePosting servicePosting;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	
	@RequestMapping(value="/postingWithNextNumber", method=RequestMethod.POST)
	public ResponseMessage getNextNumber(@RequestBody DtoJournalEntryHeader dtoJournalEntryHeader, @RequestParam(required = false) int flage) throws ParseException, NumberFormatException, ScriptException {
		ResponseMessage response = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			String sequence = servicePosting.saveAndPostingWithSequence(dtoJournalEntryHeader, flage);
			if(!sequence.equalsIgnoreCase("FOUND")) {
				response = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),
						sequence);
			} else {
				response = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		return response;
	}
	
	// posting JV (No need for sequence , take it from /postingWithNextNumber API while saving)
	
	
	
	
	

}
