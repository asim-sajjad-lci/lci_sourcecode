/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoGLCashReceiptBank;
import com.bti.model.dto.DtoGLCashReceiptDistribution;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.service.ServiceCurrencyExchange;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceGLCashReceiptEntry;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: Controller General Ledger Transaction
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/generalLedger")
public class ControllerGLCashReceiptEntry {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ControllerGLCashReceiptEntry.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	
	@Autowired
	ServiceCurrencyExchange serviceCurrencyExchange;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	ServiceGLCashReceiptEntry serviceGLCashReceiptEntry;
	
	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;
	
	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;
	
	/**
	 * @Description:this service will use to save cash receipt entry  
	 * @param request
	 * @param dtoGLCashReceiptBank
	 * @return
	 * @
	 */
	@RequestMapping(value = "/saveCashReceiptEntry", method = RequestMethod.POST)
	public ResponseMessage saveCashReceiptEntry(HttpServletRequest request,@RequestBody DtoGLCashReceiptBank dtoGLCashReceiptBank)  
    {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLCashReceiptBank); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLCashReceiptBank = serviceGLCashReceiptEntry.saveCashReceiptEntry(dtoGLCashReceiptBank);
			if (dtoGLCashReceiptBank != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),
						dtoGLCashReceiptBank);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECEIPT_NUMBER_ALREADY_EXIST, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	@RequestMapping(value = "/getAllCashReceiptNumber", method = RequestMethod.GET)
	public ResponseMessage getAllCashReceiptNumber(HttpServletRequest request)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<String> receiptNumberList = serviceGLCashReceiptEntry.getAllCashReceiptNumber();
			if (receiptNumberList!=null && !receiptNumberList.isEmpty()) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), receiptNumberList);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description:this service will use to save cash receipt entry  
	 * @param request
	 * @param dtoGLCashReceiptBank
	 * @return
	 * @
	 */
	@RequestMapping(value = "/updateCashReceiptEntry", method = RequestMethod.POST)
	public ResponseMessage updateCashReceiptEntry(HttpServletRequest request,@RequestBody DtoGLCashReceiptBank dtoGLCashReceiptBank)  
    {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLCashReceiptBank); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLCashReceiptBank = serviceGLCashReceiptEntry.updateCashReceiptEntry(dtoGLCashReceiptBank);
			if (dtoGLCashReceiptBank != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_UPDATED_SUCCESSFULLY, false),
						dtoGLCashReceiptBank);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to get cash receipt entry
	 * @param request
	 * @param dtoGLCashReceiptBank
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getCashReceiptEntryByReceiptNumber", method = RequestMethod.POST)
	public ResponseMessage getCashReceiptEntryByReceiptNumber(HttpServletRequest request,@RequestBody DtoGLCashReceiptBank dtoGLCashReceiptBank)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoGLCashReceiptBank = serviceGLCashReceiptEntry.getCashReceiptEntryByReceiptNumber(dtoGLCashReceiptBank);
			if (dtoGLCashReceiptBank != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoGLCashReceiptBank);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description:this service will use to save cash receipt entry  
	 * @param request
	 * @param dtoGLCashReceiptBank
	 * @return
	 * @throws ScriptException 
	 * @throws NumberFormatException 
	 * @
	 */
	@RequestMapping(value = "/postCashReceiptEntry", method = RequestMethod.POST)
	public ResponseMessage postCashReceiptEntry(HttpServletRequest request,@RequestBody DtoGLCashReceiptBank dtoGLCashReceiptBank) throws NumberFormatException, ScriptException  
    {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLCashReceiptBank); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			/*boolean checkDistribution = serviceGLCashReceiptEntry.checkDistributionDetailIsAvailable(dtoGLCashReceiptBank.getReceiptNumber());
			if(checkDistribution){*/
				dtoGLCashReceiptBank = serviceGLCashReceiptEntry.postCashReceiptEntry(dtoGLCashReceiptBank);
				if (dtoGLCashReceiptBank != null && dtoGLCashReceiptBank.getMessageType()==null) 
				{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_POST_SUCCESSFULLY, false),
							dtoGLCashReceiptBank);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(dtoGLCashReceiptBank.getMessageType(), false));
				}
			/*
			 * }
			 * else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WRONG_DISTRIBUTION, false));
			
			}*/
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use for get master payment method type of CashReceiptBank
	 * @param request, dtoFiscalFinancialPeriodSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/getPaymentMethodType", method = RequestMethod.GET)
	public ResponseMessage getPaymentMethodType(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 List<DtoStatusType> response = this.serviceGLCashReceiptEntry.getPaymentMethodType();
			if (response!=null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use for get master payment method type of CashReceiptBank
	 * @param request, dtoFiscalFinancialPeriodSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/getReceiptType", method = RequestMethod.GET)
	public ResponseMessage getReceiptType(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 List<DtoStatusType> response = this.serviceGLCashReceiptEntry.getReceiptType();
			if (response!=null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to save  cash receipt entry distribution
	 * @param request
	 * @param dtoGLCashReceiptDistribution
	 * @return
	 * @
	 */
	@RequestMapping(value = "/saveCashReceiptDistribution", method = RequestMethod.POST)
	public ResponseMessage saveCashReceiptDistribution(HttpServletRequest request,@RequestBody DtoGLCashReceiptDistribution dtoGLCashReceiptDistribution)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLCashReceiptDistribution); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoGLCashReceiptDistribution = serviceGLCashReceiptEntry.saveCashReceiptDistribution(dtoGLCashReceiptDistribution);
			if (dtoGLCashReceiptDistribution!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY,false), dtoGLCashReceiptDistribution);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_FAIL, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	/**
	 * @description this service will use to cash receipt entry  distribution
	 * @param request
	 * @param dtoGLCashReceiptDistribution
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getCashReceiptDistributionByReceiptNumber", method = RequestMethod.POST)
	public ResponseMessage getCashReceiptDistributionByReceiptNumber(HttpServletRequest request,@RequestBody DtoGLCashReceiptDistribution dtoGLCashReceiptDistribution)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoGLCashReceiptDistribution = serviceGLCashReceiptEntry.getCashReceiptDistributionByReceiptNumber(dtoGLCashReceiptDistribution.getReceiptNumber());
			if (dtoGLCashReceiptDistribution != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoGLCashReceiptDistribution);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to delete CashReceiptEntry 
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/deleteCashReceiptEntry", method = RequestMethod.POST)
	public ResponseMessage deleteCashReceiptEntryByReceiptNumber(HttpServletRequest request,
			@RequestBody DtoGLCashReceiptBank dtoGLCashReceiptBank) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLCashReceiptBank); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceGLCashReceiptEntry.deleteCashReceiptEntryByReceiptNumber(dtoGLCashReceiptBank);
			if (status) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getUniqueReceiptNumber", method = RequestMethod.GET)
	public ResponseMessage getUniqueReceiptNumber(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 String response = this.serviceGLCashReceiptEntry.getUniqueReceiptNumber();
			if (response!=null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/checkCashReceiptDistribution", method = RequestMethod.POST)
	public ResponseMessage checkCashReceiptDistribution(HttpServletRequest request, @RequestBody DtoGLCashReceiptBank dtoGLCashReceiptBank )  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 boolean response = this.serviceGLCashReceiptEntry.checkDistributionDetailIsAvailable(dtoGLCashReceiptBank.getReceiptNumber());
			if (response) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WRONG_DISTRIBUTION, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	
}
