/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: ControllerGeneralLedgerSetup Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 13, 2017 11:39:38 AM
 * 
 * @author seasia Version:
 */

@RestController
@RequestMapping("/transaction/accountPayable")
public class ControllerAccountPayableTransaction {

}
