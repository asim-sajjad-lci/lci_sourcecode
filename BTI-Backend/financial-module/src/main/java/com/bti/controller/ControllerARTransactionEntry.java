/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoARCashReceiptDistribution;
import com.bti.model.dto.DtoARTransactionEntry;
import com.bti.model.dto.DtoCustomerClassSetup;
import com.bti.model.dto.DtoCustomerMaintenance;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryARBatches;
import com.bti.service.ServiceARTransactionEntry;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerARBatches
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/accountReceivable")
public class ControllerARTransactionEntry {

	private static final Logger LOGGER = Logger.getLogger(ControllerARTransactionEntry.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	ServiceARTransactionEntry serviceARTransactionEntry;

	@Autowired
	RepositoryARBatches repositoryARBatches;

	@RequestMapping(value = "/saveTransactionEntry", method = RequestMethod.POST)
	public ResponseMessage saveTransactionEntry(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry)
	{
		LOGGER.info("inside saveTransactionEntry method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoARTransactionEntry); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoARTransactionEntry = serviceARTransactionEntry.saveTransactionEntry(dtoARTransactionEntry);
				if (dtoARTransactionEntry != null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),
							dtoARTransactionEntry);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
				}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	@RequestMapping(value = "/updateTransactionEntry", method = RequestMethod.POST)
	public ResponseMessage updateTransactionEntry(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry)
	{
		LOGGER.info("inside updateTransactionEntry method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoARTransactionEntry); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoARTransactionEntry = serviceARTransactionEntry.updateTransactionEntry(dtoARTransactionEntry);
				if (dtoARTransactionEntry != null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_UPDATED_SUCCESSFULLY, false),
							dtoARTransactionEntry);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	@RequestMapping(value = "/getTransactionEntryByTransactionNumber", method = RequestMethod.POST)
	public ResponseMessage getTransactionEntryByTransactionNumber(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry)
	{
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoARTransactionEntry = serviceARTransactionEntry.getTransactionEntryByTransactionNumber(dtoARTransactionEntry);
			if (dtoARTransactionEntry != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoARTransactionEntry);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getTransactionNumberByTransactionType", method = RequestMethod.POST)
	public ResponseMessage getTransactionNumberByTransactionType(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<String> transactionNumberList = serviceARTransactionEntry.getTransactionNumberByTransactionType(dtoARTransactionEntry.getArTransactionType());
			if (transactionNumberList!=null && !transactionNumberList.isEmpty()) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), transactionNumberList);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use for get master payment method type of CashReceiptBank
	 * @param request, dtoFiscalFinancialPeriodSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/getTransactionType", method = RequestMethod.GET)
	public ResponseMessage getTransactionType(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 List<DtoStatusType> response = this.serviceARTransactionEntry.getTransactionType();
			if (response!=null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getTransactionNumber", method = RequestMethod.POST)
	public ResponseMessage getTransactionNumber(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 String response = this.serviceARTransactionEntry.getTransactionNumber(dtoARTransactionEntry.getArTransactionType());
			if (response!=null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, 
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getCustomerAccountDetailByCustomerId", method = RequestMethod.POST)
	public ResponseMessage getCustomerAccountDetailByCustomerId(
			@RequestBody DtoCustomerMaintenance dtoCustomerMaintenance)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			DtoCustomerClassSetup dtoCustomerClassSetup = serviceARTransactionEntry
						.getCustomerClassSetupByClassId(dtoCustomerMaintenance);
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoCustomerClassSetup);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getDistributionDetailByTransactionNumber", method = RequestMethod.POST)
	public ResponseMessage getDistributionDetailByTransactionNumber(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry)
	{
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			DtoARCashReceiptDistribution dtoARCashReceiptDistribution = serviceARTransactionEntry.getTransactionalDistribution(dtoARTransactionEntry);
			if (dtoARCashReceiptDistribution != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoARCashReceiptDistribution);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/saveDistributionDetail", method = RequestMethod.POST)
	public ResponseMessage saveDistributionDetail(HttpServletRequest request, @RequestBody DtoARCashReceiptDistribution dtoARCashReceiptDistribution) throws ScriptException
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoARCashReceiptDistribution); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean response = serviceARTransactionEntry.saveDistributionDetail(dtoARCashReceiptDistribution);
			if (response) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	@RequestMapping(value = "/postARTransactionEntry", method = RequestMethod.POST)
	public ResponseMessage postTransactionEntry(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry) throws ScriptException
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoARTransactionEntry); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) 
		{
			/*boolean checkDistribution = serviceARTransactionEntry.checkTransDistributionDetailIsAvailable(dtoARTransactionEntry.getArTransactionNumber());
			if(checkDistribution){*/
				dtoARTransactionEntry= serviceARTransactionEntry.postARTransactionEntry(dtoARTransactionEntry);
				if (dtoARTransactionEntry.getMessageType()==null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_POST_SUCCESSFULLY, false));
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(dtoARTransactionEntry.getMessageType(), false));
				}
			/*}
			else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WRONG_DISTRIBUTION, false));
			}*/
		}
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/*@RequestMapping(value = "/checkTransactionDistribution", method = RequestMethod.POST)
	public ResponseMessage checkTransactionDistribution(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry )  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 boolean response = this.serviceARTransactionEntry.checkDistributionDetailIsAvailable(dtoARTransactionEntry.getArTransactionNumber());
			if (response) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WRONG_DISTRIBUTION, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}*/
	
	@RequestMapping(value = "/deleteTransactionEntry", method = RequestMethod.POST)
	public ResponseMessage deleteTransactionEntry(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry)
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoARTransactionEntry); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			boolean response = serviceARTransactionEntry.deleteTransactionEntry(dtoARTransactionEntry.getArTransactionNumber());
			if (response) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	@RequestMapping(value = "/deleteTransactionEntryDistribution", method = RequestMethod.POST)
	public ResponseMessage deleteTransactionEntryDistribution(HttpServletRequest request, @RequestBody DtoARTransactionEntry dtoARTransactionEntry)
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoARTransactionEntry); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			boolean response = serviceARTransactionEntry.deleteTransactionEntryDistribution(dtoARTransactionEntry.getArTransactionNumber());
			if (response) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

}
