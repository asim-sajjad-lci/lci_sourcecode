package com.bti.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.GLPaymentVoucherHeader;
import com.bti.model.dto.DtoCheckBookMaintenance;
import com.bti.model.dto.DtoGLPaymentVoucherHeader;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.service.ServiceGLPaymentVoucher;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

@RestController
@RequestMapping("/transaction/paymentVoucher")
public class ControllerGLPaymentVoucher {

	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceGLPaymentVoucher serviceGLPaymentVoucher;
	
	
	//get next Payment Voucher
	@RequestMapping(value="/getNextPaymentVoucherID",method=RequestMethod.GET)
	public ResponseMessage getNextPaymentVoucherID(HttpServletRequest request ) {
		
		ResponseMessage response = null;
		DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader = new DtoGLPaymentVoucherHeader();
		String flage = serviceHome.checkValidCompanyAccess();
		if(flage.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLPaymentVoucherHeader = serviceGLPaymentVoucher.getNextPaymentVoucherID(dtoGLPaymentVoucherHeader);
			if(dtoGLPaymentVoucherHeader.getPaymentVoucherID() != 0 || !dtoGLPaymentVoucherHeader.equals(null)) {
				response = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY,false),dtoGLPaymentVoucherHeader);
			}else {
				response = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED,serviceResponse.getMessageByShortAndIsDeleted(flage, false),false);
		}
		return response;
	}
	
	//Create Operation 
	@RequestMapping(value="/savePaymentVoucher",method=RequestMethod.POST)
	public ResponseMessage savePaymentVoucher(HttpServletRequest request , @RequestBody DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLPaymentVoucherHeader); 

		String flage = serviceHome.checkValidCompanyAccess();
		
		ResponseMessage responseMessage = null;
		
		if(flage.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			GLPaymentVoucherHeader glPaymentVoucherHeader = serviceGLPaymentVoucher.findOne(dtoGLPaymentVoucherHeader);
			if(glPaymentVoucherHeader == null) {
				dtoGLPaymentVoucherHeader = serviceGLPaymentVoucher.save(dtoGLPaymentVoucherHeader);
				if(dtoGLPaymentVoucherHeader != null && dtoGLPaymentVoucherHeader.getMessageType() == null) {
					 responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED
							,this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false), 
							dtoGLPaymentVoucherHeader);
				}else {
					 responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),HttpStatus.INTERNAL_SERVER_ERROR,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR, false));
				}
				
				
			}else {
				 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(),HttpStatus.FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
			}
		}else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED,
					this.serviceResponse.getMessageByShortAndIsDeleted(flage, false),false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	
	
	@RequestMapping(value="/getAllVoucherIDs",method=RequestMethod.GET)
	public ResponseMessage getPaymentVoucerIDs(HttpServletRequest request) {
		DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader = new DtoGLPaymentVoucherHeader();
		ResponseMessage res = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLPaymentVoucherHeader = serviceGLPaymentVoucher.getPaymentVoucherIDs(dtoGLPaymentVoucherHeader);
			if(dtoGLPaymentVoucherHeader.getIds() != null ) {
				res = new ResponseMessage(HttpStatus.FOUND.value(),HttpStatus.FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST,false),dtoGLPaymentVoucherHeader);
			}else {
				res = new ResponseMessage(HttpStatus.NOT_FOUND.value(),HttpStatus.NOT_FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}else {
			res = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED,serviceResponse.getMessageByShortAndIsDeleted(flag, false),false);
		}
		return res;
		
	}
	 
	@RequestMapping(value="/getPaymentVoucherById",method=RequestMethod.POST)
	public ResponseMessage getPaymentVoucherById(HttpServletRequest request ,@RequestBody DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		ResponseMessage response = null;
		String flage = serviceHome.checkValidCompanyAccess();
		if(flage.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLPaymentVoucherHeader = serviceGLPaymentVoucher.getPaymentVoucherById(dtoGLPaymentVoucherHeader);
			if(dtoGLPaymentVoucherHeader != null) {
				response = new ResponseMessage(HttpStatus.FOUND.value(),HttpStatus.FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoGLPaymentVoucherHeader);
			}else {
				response = new ResponseMessage(HttpStatus.NOT_FOUND.value(),HttpStatus.NOT_FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED,serviceResponse.getMessageByShortAndIsDeleted(flage, false),false);
		}
		
		
		return response;
	}
	
	@RequestMapping(value="/deletePaymentVoucherById",method=RequestMethod.POST)
	public ResponseMessage deletePaymentVoucherById(HttpServletRequest httpServletRequest,@RequestBody DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		ResponseMessage respnse = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLPaymentVoucherHeader); 
		
		String flage = serviceHome.checkValidCompanyAccess();
		if(flage.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceGLPaymentVoucher.deleteJournalEntry(dtoGLPaymentVoucherHeader);
			if(status) {
				respnse = new ResponseMessage(HttpStatus.FOUND.value(),HttpStatus.FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
			}else {
				respnse = new ResponseMessage(HttpStatus.NOT_FOUND.value(),HttpStatus.NOT_FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}else {
			respnse = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED,serviceResponse.getMessageByShortAndIsDeleted(flage,false));
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, respnse);
		
		return respnse;
	}
	
	@RequestMapping(value="/updatePaymentVoucher",method=RequestMethod.POST)
	public ResponseMessage updatePaymentVoucher(HttpServletRequest httpServletRequest , @RequestBody DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		ResponseMessage response = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLPaymentVoucherHeader); 

		String flage = serviceHome.checkValidCompanyAccess();
		if(flage.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoGLPaymentVoucherHeader = serviceGLPaymentVoucher.updatePaymentVoucher(dtoGLPaymentVoucherHeader);
			if(dtoGLPaymentVoucherHeader != null) {
				response = new ResponseMessage(HttpStatus.CREATED.value(),HttpStatus.CREATED,this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_UPDATED_SUCCESSFULLY, false),dtoGLPaymentVoucherHeader);
			}else {
				response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),HttpStatus.INTERNAL_SERVER_ERROR,this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR, false));
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flage, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, response);
		return response;
	}
	
	@RequestMapping(value="/deletePaymentVoucher",method=RequestMethod.POST)
	public ResponseMessage deletePaymentVoucher(HttpServletRequest request , DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		ResponseMessage response = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLPaymentVoucherHeader); 
		
		String flage = serviceHome.checkValidCompanyAccess();
		if(flage.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceGLPaymentVoucher.deleteJournalEntry(dtoGLPaymentVoucherHeader);
			if(status) {
				response = new ResponseMessage(HttpStatus.FOUND.value(),HttpStatus.FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
			}else {
				response = new ResponseMessage(HttpStatus.NOT_FOUND.value(),HttpStatus.NOT_FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_FAIL, false));
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED,serviceResponse.getMessageByShortAndIsDeleted(flage, false));
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, response);
		return response;
	}
	
	@RequestMapping(value="/postPaymentVoucher",method=RequestMethod.POST)
	public ResponseMessage postPaymentVoucher(HttpServletRequest request , @RequestBody DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		ResponseMessage response = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGLPaymentVoucherHeader); 
		
		String flage = serviceHome.checkValidCompanyAccess();
		if(flage.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			GLPaymentVoucherHeader data = serviceGLPaymentVoucher.postPaymentVoucher(dtoGLPaymentVoucherHeader);
			if((data.getPaymentVoucherId() != 0) && (data.getPaymentVoucherId() != 99)) {
				response = new ResponseMessage(HttpStatus.FOUND.value(),HttpStatus.FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_POST_SUCCESSFULLY,false ));
			}else {
				if(data.getPaymentVoucherId() != 0) {
					response = new ResponseMessage(HttpStatus.CONFLICT.value(), HttpStatus.CONFLICT, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
				}else {
					response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),HttpStatus.INTERNAL_SERVER_ERROR,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR,false ));
				}
				
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED,serviceResponse.getMessageByShortAndIsDeleted(flage, false));
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, response);
		return response;
	}
	
	
	
	@RequestMapping(value="/getCheckbookIDAccount",method=RequestMethod.POST)
	public ResponseMessage getCheckbookIDAccount (HttpServletRequest request ,@RequestBody DtoCheckBookMaintenance dtoCheckBookMaintenance) {
		ResponseMessage response = null;
		
		String flage = serviceHome.checkValidCompanyAccess();
		if(flage.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoCheckBookMaintenance = serviceGLPaymentVoucher.getCheckboodIDAccount(dtoCheckBookMaintenance);
			if(dtoCheckBookMaintenance != null) {
				response = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_POST_SUCCESSFULLY,false),dtoCheckBookMaintenance);
			}else {
				response = new ResponseMessage(HttpStatus.NOT_FOUND.value(),HttpStatus.NOT_FOUND,serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND,false),flage);
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED,serviceResponse.getMessageByShortAndIsDeleted(flage, false));
		}
		
		return response;
	}
	
	
	
}
