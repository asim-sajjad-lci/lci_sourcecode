package com.bti.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoPostingAccount;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.service.ServiceHome;
import com.bti.service.ServicePostingAccount;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

@RestController
@RequestMapping("/general/ledger")
public class ControllerGeneralLedger {
	
	private static final Logger LOGGER = Logger.getLogger(ControllerGeneralLedger.class);
	
	@Autowired
	ServicePostingAccount servicePostingAccount;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @Description This service will add general ledger posting account
	 * @param dtoPostingAccount
	 * @return
	 * 
	 */
	@RequestMapping(value = "/posting/account", method = RequestMethod.POST)
	public ResponseMessage savePostingAccount(@RequestBody DtoPostingAccount dtoPostingAccount)  {
		LOGGER.info("inside savePostingAccount method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPostingAccount); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		dtoPostingAccount = servicePostingAccount.savePostingAccount(dtoPostingAccount);
		if (dtoPostingAccount.getPostingId() > 0) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.POSTING_ACCOUNT_SAVED_SUCCESSFULLY, false),
					dtoPostingAccount);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.POSTING_ACCOUNT_SAVED_FAIL, false),
					dtoPostingAccount);
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will update general ledger posting account
	 * @param dtoPostingAccount
	 * @return
	 * 
	 */
	@RequestMapping(value = "/posting/account/update", method = RequestMethod.POST)
	public ResponseMessage updatePostingAccount(@RequestBody DtoPostingAccount dtoPostingAccount)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPostingAccount); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		boolean isPostingAccount = servicePostingAccount.getPostingAccount(dtoPostingAccount.getPostingId());
		if (isPostingAccount) {
			dtoPostingAccount = servicePostingAccount.updatePostingAccount(dtoPostingAccount);
			if (dtoPostingAccount.getPostingId() > 0) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(
									MessageLabel.POSTING_ACCOUNT_UPDATED_SUCCESSFULLY, false),
						dtoPostingAccount);
			} else {
					responseMessage = new ResponseMessage(
							HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.POSTING_ACCOUNT_UPDATED_FAIL, false),
						dtoPostingAccount);
			}
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will get all or search general ledger posting
	 *              account
	 * @param dtoSearch
	 * @return
	 * 
	 */
	@RequestMapping(value = "/posting/account/search", method = RequestMethod.POST)
	public ResponseMessage getAllPostingAccount(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		dtoSearch = servicePostingAccount.searchPostingAccount(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoPostingAccount> dtoPostingAccounts = (List<DtoPostingAccount>) dtoSearch.getRecords();
				if (dtoPostingAccounts != null && !dtoPostingAccounts.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
							dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will get general ledger posting account
	 * @param dtoPostingAccount
	 * @return
	 * 
	 */
	@RequestMapping(value = "/posting/account/getById", method = RequestMethod.POST)
	public ResponseMessage getPostingAccount(@RequestBody DtoPostingAccount dtoPostingAccount)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		boolean isPostingAccount = servicePostingAccount.getPostingAccount(dtoPostingAccount.getPostingId());
		if (isPostingAccount) {
				dtoPostingAccount = servicePostingAccount
						.getPostingAccountByPostingId(dtoPostingAccount.getPostingId());
			if (dtoPostingAccount.getPostingId() > 0) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoPostingAccount);
			} else {
					responseMessage = new ResponseMessage(
							HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.POSTING_ACCOUNT_FETCHED_FAIL, false),
						dtoPostingAccount);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.POSTING_ACCOUNT_FETCHED_FAIL,
								false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
}
