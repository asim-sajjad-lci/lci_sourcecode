package com.bti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoAuditTrailCode;
import com.bti.service.ServiceAuditTrial;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;

@RequestMapping("audittrial/")
public class ControllerAuditTrial {

	
	@Autowired
	ServiceAuditTrial serviceAuditTrial;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	
	//get All
	public ResponseMessage getAll() {
		ResponseMessage response = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<DtoAuditTrailCode> dtoList = serviceAuditTrial.getAllAuditTrialCode();
			if(! dtoList.isEmpty() && dtoList.size()>0) {
				response = new ResponseMessage(HttpStatus.OK.value(),HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.AUDIT_TRAIL_CODE_LIST, false)
						,dtoList);
			}else {
				response = new ResponseMessage(HttpStatus.OK.value(),HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED
					,serviceResponse.getMessageByShortAndIsDeleted(flag, false));
		}
		return response;
	}

	//get By Id
	@RequestMapping(value="/getById")
	public ResponseMessage getAuditTrialByIndexId(@RequestBody DtoAuditTrailCode dtoAuditTrailCode) {
		ResponseMessage response = null;
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoAuditTrailCode = serviceAuditTrial.getAuditTrialCodeById(dtoAuditTrailCode.getSeriesIndex());
			if(dtoAuditTrailCode != null) {
				response = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(flag, false),dtoAuditTrailCode);
			}else {
				response = new ResponseMessage(HttpStatus.OK.value(),HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}else {
			response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED
					,serviceResponse.getMessageByShortAndIsDeleted(flag, false));
		}
		return response;
	}
	//update 
	
	//delete
	
	//create 
	
	//search
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
