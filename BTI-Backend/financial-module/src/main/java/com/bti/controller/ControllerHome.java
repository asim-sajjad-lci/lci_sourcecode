/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoAuditTrailCode;
import com.bti.model.dto.DtoCountry;
import com.bti.model.dto.DtoFinancialDimensionValue;
import com.bti.model.dto.DtoGLBankTransferCharges;
import com.bti.model.dto.DtoGeneralLedgerSetup;
import com.bti.model.dto.DtoMainAccountSetUp;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoTypes;
import com.bti.repository.RepositoryException;
import com.bti.repository.RepositoryLanguage;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.UtilFindIPAddress;

/**
 * Description: ControllerHome
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 11:39:38 AM
 * @author seasia
 */
@RestController
@RequestMapping("/")
public class ControllerHome {

	private static final Logger LOGGER = Logger.getLogger(ControllerHome.class);

	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryException repositoryException;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryLanguage repositoryLanguage;
	
	/**
	 * @description : Get Country List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getCountryList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCountryList(HttpServletRequest request)  {
		LOGGER.info("Get Country List");
	
		
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		
		List<DtoCountry> countryList = serviceHome.getCountryList();
		responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GET_COUNTRY_LIST, false), countryList);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description : Get State List By Country Id
	 * @param request
	 * @param dtoCountry
	 * @return
	 
	 */
	@RequestMapping(value = "/getStateListByCountryId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getStateListByCountryId(HttpServletRequest request, @RequestBody DtoCountry dtoCountry)  {
		LOGGER.info("Get State List By Country Id");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCountry.getCountryId() != null) {
			List<DtoCountry> stateList = serviceHome.getStateListByCountryId(dtoCountry.getCountryId());
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), stateList);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COUNTRY_ID_REQUIRED, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description : Get City List By State Id
	 * @param request
	 * @param dtoCountry
	 * @return
	 
	 */
	@RequestMapping(value = "/getCityListByStateId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getCityListByStateId(HttpServletRequest request, @RequestBody DtoCountry dtoCountry)  {
		LOGGER.info("Get City List By State Id");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCountry.getStateId() != null) {
			List<DtoCountry> cityList = serviceHome.getCityListByStateId(dtoCountry.getStateId());
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GET_CITY_LIST, false), cityList);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.STATE_ID_REQUIRED, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description : Get Country Code By Country Id
	 * @param request
	 * @param dtoCountry
	 * @return
	 
	 */
	@RequestMapping(value = "/getCountryCodeByCountryId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getCountryCodeByCountryId(HttpServletRequest request, @RequestBody DtoCountry dtoCountry)  {
		LOGGER.info("Get Country Code By Country  Id");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCountry.getCountryId() != null) {
			DtoCountry countryCode = serviceHome.getCountryCodeByCountryId(dtoCountry.getCountryId());

			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GET_COUNTRY_CODE, false), countryCode);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COUNTRY_ID_REQUIRED, false));
		}
	}
	else
	{
		responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
				serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
	}

		return responseMessage;
	}	
	
	/**
	 * @description : Get due types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getDueTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getDueTypes(HttpServletRequest request)  {
		LOGGER.info("Get Due Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getDueTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get discount types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getDiscountTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getDiscountTypes(HttpServletRequest request)  {
		LOGGER.info("Get Discount Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getDiscountTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get discount period types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getDiscountPeriodTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getDiscountPeriodTypes(HttpServletRequest request)  {
		LOGGER.info("Get Discount Type Period Type List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getDiscountTypesPeriod();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get negative symbol sign types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getNegativeSymbolSignTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getNegativeSymbolSignTypes(HttpServletRequest request)  {
		LOGGER.info("Get Negative Symbol Sign Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getNegativeSymbolSignTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get negative symbol types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getNegativeSymbolTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getNegativeSymbolTypes(HttpServletRequest request)  {
		LOGGER.info("Get Negative Symbol Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getNegativeSymbolTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get rate frequency types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getRateFrequencyTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getRateFrequencyTypes(HttpServletRequest request)  {
		LOGGER.info("Get Rate Frequency Type List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getRateFrequencyTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get separator thousand types list
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getSeperatorThousandsTypesList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getSeperatorThousandsTypesList(HttpServletRequest request)  {
		LOGGER.info("Get Seperator Thousands Type List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getSeperatoryThousandsTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Depreciation Period Types List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getDepreciationPeriodTypesList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getDepreciationPeriodTypesList(HttpServletRequest request)  {
		LOGGER.info("Get Depreciation Period Type List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getDepreciationPeriodTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Seperator Decimal Types List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getSeperatorDecimalTypesList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getSeperatorDecimalTypesList(HttpServletRequest request)  {
		LOGGER.info("Get Seperator Thousands Type List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getSeperatoryDecimalTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Rm Apply By Default List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getRmApplyByDefaultList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getRmApplyByDefaultList(HttpServletRequest request)  {
		LOGGER.info("Get RM Apply By Default List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getRmApplybyDefaultList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Rm Ageing List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getRmAgeingList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getRmAgeingList(HttpServletRequest request)  {
		LOGGER.info("Get Rm Ageing List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getRmAgeingTypesList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get Customer Class Balance Type List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getCustomerClassBalanceTypeList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCustomerClassBalanceTypeList(HttpServletRequest request)  {
		LOGGER.info("Get Customer Class Balance Type List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getCustomeClassBalanceTypesList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get Customer Class Minimum Charge List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getCustomerClassMinimumChargeList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCustomerClassMinimumChargeList(HttpServletRequest request)  {
		LOGGER.info("Get Customer Class Minimum Charge List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getCustomeClassMinimumChargeList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get Customer Class Finance Charge List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getCustomerClassFinanceChargeList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCustomerClassFinanceChargeList(HttpServletRequest request)  {
		LOGGER.info("Get Customer Class Finance Charge List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getCustomeClassFinanceChargeList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get Customer Class Credit Limit List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getCustomerClassCreditLimitList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getCustomerClassCreditLimitList(HttpServletRequest request)  {
		LOGGER.info("Get Customer Class Credit Limit List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getCustomeClassCreditLimitList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get FA General Maintenance Asset Type List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getFAGeneralMaintenanceAssetTypesList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getFAGeneralMaintenanceAssetTypesList(HttpServletRequest request)  {
		LOGGER.info("Get General Maintenance Asset Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getFAGeneralMaintenanceAssetTypeList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get FA General Maintenance Asset Status List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getFAGeneralMaintenanceAssetStatusList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getFAGeneralMaintenanceAssetStatusList(HttpServletRequest request)  {
		LOGGER.info("Get General Maintenance Asset Status List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getFAGeneralMaintenanceAssetStatusList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get FA Property Types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getFAPropertyTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getFAPropertyTypes(HttpServletRequest request)  {
		LOGGER.info("Get FA Property Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getFAPropertyTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : check Main Account Number
	 * @param request
	 * @param dtoMainAccountSetUp
	 * @return
	 */
	@RequestMapping(value = "/checkMainAccountNumber", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage checkMainAccountNumber(HttpServletRequest request,
			@RequestBody DtoMainAccountSetUp dtoMainAccountSetUp)  {
		LOGGER.info("Check main account number");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoMainAccountSetUp = serviceHome.checkMainAccount(dtoMainAccountSetUp.getMainAccountNumber());
		if (dtoMainAccountSetUp != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
					dtoMainAccountSetUp);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MAIN_ACCOUNT_NUMBER_IS_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : check Dimension Values
	 * @param request
	 * @param dtoFinancialDimensionValue
	 * @return
	 */
	@RequestMapping(value = "/checkDimensionValues", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage checkDimensionValues(HttpServletRequest request,
	    @RequestBody DtoFinancialDimensionValue dtoFinancialDimensionValue)  {
		LOGGER.info("Check main account number");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			dtoFinancialDimensionValue = serviceHome
					.checkFinancialDimensionValue(dtoFinancialDimensionValue.getDimensionValue(),dtoFinancialDimensionValue.getSegmentNumber());
			if (dtoFinancialDimensionValue.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoFinancialDimensionValue);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(dtoFinancialDimensionValue.getMessageType(), false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get Account Types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getAccountTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAccountTypes(HttpServletRequest request)  {
		LOGGER.info("Get Account Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getAccountTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get Gl Account Number List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getGlAccountNumberList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getGlAccountNumberList(HttpServletRequest request)  {
		LOGGER.info("Get account number  List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoPayableAccountClassSetup> list = serviceHome.getGlAccountTableAcumulationList();
		if (list != null && !list.isEmpty()) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), list);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getGlAccountNumberListNew", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getGlAccountNumberListNew(HttpServletRequest request)  {
		LOGGER.info("Get account number  List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoPayableAccountClassSetup> list = serviceHome.getGlAccountTableAcumulationListNew();
		if (list != null && !list.isEmpty()) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), list);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchGlAccountNumberListNew", method = RequestMethod.POST)
	public ResponseMessage searchGlAccountNumberListNew(@RequestBody DtoSearch dtoSearch,HttpServletRequest request)  {
		LOGGER.info("Get account number  List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceHome.searchGlAccountTableAcumulationList(dtoSearch);
		if (dtoSearch != null && dtoSearch.getRecords() != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), dtoSearch);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get Ar Account Types
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getArAccountTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getArAccountTypes(HttpServletRequest request)  {
		LOGGER.info("Get AR Account Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getArAccountTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get FA Account Types
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getFAAccountTypes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getFAAccountTypes(HttpServletRequest request)  {
		LOGGER.info("Get Property Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getFAAccountTypes();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : get Gl Account Number Detail ById
	 * @param request
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */ 
	@RequestMapping(value = "/getGlAccountNumberDetailById", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getGlAccountNumberDetailById(HttpServletRequest request,
			@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup)  {
		LOGGER.info("Get account number  List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		DtoPayableAccountClassSetup detail = serviceHome
				.getGlAccountTableAcumulationDetailById(dtoPayableAccountClassSetup);
		if (detail != null) {
			if(detail.getIsTransactionAllow()){
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), detail);
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.MAIN_ACCOUNT_TRANSACTION_NOT_ALLOWED, false), detail);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getPriceLevelList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getPriceLevelList(HttpServletRequest request)  {
		LOGGER.info("Get Property Types List");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoTypes> list = serviceHome.getPriceLevelList();
		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/myIP", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getMyIP(HttpServletRequest request) {
		LOGGER.info("Getting my IP address");
		ResponseMessage responseMessage = null;
		String userIp = UtilFindIPAddress.getUserIp(request);
		if (userIp != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.IP_FETCHED, false), userIp);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.IP_NOT_FETCHED, false), userIp);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/checkGlAccountNumber", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage checkGlAccountNumber(HttpServletRequest request, @RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup)  
	{
		LOGGER.info("Check account number  ");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			boolean result = serviceHome.checkGlAccountNumber(dtoPayableAccountClassSetup.getAccountNumber());
			if (result) 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
 
	@RequestMapping(value = "/getBankDistributionDetailByCheckbookId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getBankDistributionDetailByCheckbookId(HttpServletRequest request,@RequestBody DtoGLBankTransferCharges dtoGLBankTransferCharges) 
	{
		ResponseMessage responseMessage = null;
			Map<String,String> map = this.serviceHome.getBankDistributionDetailByCheckbookId(dtoGLBankTransferCharges.getCheckbookID());
			if (map!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), map);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		return responseMessage;
	}
	
	@RequestMapping(value = "/exportCompanyMasterData", method = RequestMethod.GET)
	public void exportCompanyMasterData(HttpServletRequest request, HttpServletResponse response) {
		serviceHome.exportMasterData(request, response);
	}
	
	@RequestMapping(value = "/importCompanyMasterData",consumes = { "multipart/mixed", "multipart/form-data" }, method = RequestMethod.POST)
	public @ResponseBody ResponseMessage importMasterData(@RequestParam(value = "languageOrientation", required = true) String languageOrientation, 
		   @RequestParam(value = "languageName", required = true) String languageName, 
			@RequestParam(value = "file", required = true) MultipartFile file) {
		ResponseMessage responseMessage = null;
		boolean response = serviceHome.importMasterData(languageName, languageOrientation, file);
		if (response) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FILE_IMPORT_SUCCESSFULLY, false));
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.UPLOAD_OPERTATION_FAILED, false));
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAccountNumberDetailByAccountId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAccountNumberDetailByAcoountId(HttpServletRequest request,
			@RequestBody DtoGeneralLedgerSetup dtoGeneralLedgerSetup) 
	{
		    ResponseMessage responseMessage = null;
			Map<String,String> map = this.serviceHome.getAccountNumberDetailByAcoountId(dtoGeneralLedgerSetup.getAccountTableRowIndex());
			if (map!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), map);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		return responseMessage;
	}
	
	@RequestMapping(value = "/exportCompanyTestData", method = RequestMethod.GET)
	public void exportTestData(HttpServletRequest request, HttpServletResponse response) {
		serviceHome.exporttestData(request, response);
	}
	
	@RequestMapping(value = "/exportCompanyMasterDataForUpdateLanguage", method = RequestMethod.POST)
	public void exportCompanyMasterDataForUpdateLanguage(HttpServletRequest request, HttpServletResponse response,
			@RequestBody DtoCountry dtoCountry) {
		serviceHome.exportMasterData(request, response,dtoCountry.getLanguageId());
	}
	
	@RequestMapping(value = "/importCompanyMasterDataForUpdateLanguage",consumes = { "multipart/mixed", "multipart/form-data" }, method = RequestMethod.POST)
	public @ResponseBody ResponseMessage importCompanyMasterDataForUpdateLanguage(@RequestParam(value = "languageOrientation", required = true) String languageOrientation, 
		   @RequestParam(value = "languageName", required = true) String languageName,@RequestParam(value = "languageId", required = true) Integer languageId,  
			@RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
		ResponseMessage responseMessage = null;
		boolean response = serviceHome.importMasterDataForUpdateLanguage(languageName, languageOrientation, file,languageId);
		if (response) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FILE_IMPORT_SUCCESSFULLY, false));
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.UPLOAD_OPERTATION_FAILED, false));
		}
		return responseMessage;
	}
	
	@RequestMapping(value="/getAuditTrialByTransactionType", method=RequestMethod.POST)
	public ResponseMessage getAuditTrialByTransactionType(@RequestBody DtoAuditTrailCode dtoAuditTrailCode) {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoAuditTrailCode = serviceHome.getAuditTrialByTransactionType(dtoAuditTrailCode);
			if(dtoAuditTrailCode.getSeriesIndex() != 0) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(),HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),dtoAuditTrailCode);
			}else {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(),HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false),dtoAuditTrailCode);
			}
		}else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
			
		return responseMessage ;
	}
}
