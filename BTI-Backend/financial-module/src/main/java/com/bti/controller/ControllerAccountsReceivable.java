/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.CustomerClassesSetup;
import com.bti.model.CustomerMaintenance;
import com.bti.model.CustomerMaintenanceOptions;
import com.bti.model.RMSetup;
import com.bti.model.dto.DtoCustomerClassSetup;
import com.bti.model.dto.DtoCustomerMaintenance;
import com.bti.model.dto.DtoCustomerMaintenanceOptions;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoRMDocumentType;
import com.bti.model.dto.DtoRMSetup;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSalesmanMaintenance;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryCustomerClassesSetup;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryCustomerMaintenanceOptions;
import com.bti.repository.RepositoryRMSetup;
import com.bti.service.ServiceAccountRecievableSetup;
import com.bti.service.ServiceCustomerMaintenance;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerAccountReceivable
 * Name of Project: BTI
 * Created on: Aug 08, 2017
 * Modified on: Aug 08, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/setup/accounts")
public class ControllerAccountsReceivable {
	private static final Logger LOGGER = Logger.getLogger(ControllerCompanySetup.class);
	
	@Autowired
	ServiceCustomerMaintenance serviceCustomerMaintenance;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryRMSetup repositoryRMSetup;
	
	@Autowired
	ServiceAccountRecievableSetup serviceAccountRecievableSetup;
	
	@Autowired
	RepositoryCustomerClassesSetup repositoryCustomerClassesSetup;
	
	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;
	
	@Autowired
	RepositoryCustomerMaintenanceOptions repositoryCustomerMaintenanceOptions;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @description : method will setup account maintenance
	 * @param request
	 * @param dtoCustomerMaintenance
	 * @return
	 */
	@RequestMapping(value = "/maintenance", method = RequestMethod.POST)
	public ResponseMessage maintenance(HttpServletRequest request,
			@RequestBody DtoCustomerMaintenance dtoCustomerMaintenance) {
		LOGGER.info("inside maintenance method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCustomerMaintenance); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCustomerMaintenance != null) {

			dtoCustomerMaintenance = serviceCustomerMaintenance.saveCustomerMaintenanceData(dtoCustomerMaintenance);
			if (dtoCustomerMaintenance != null) {
				if (dtoCustomerMaintenance.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_MAINTENANCE_SUCCESS, false),
							dtoCustomerMaintenance);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoCustomerMaintenance.getMessageType(),
									false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_MAINTENANCE_FAIL, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get account maintenance
	 * @param request
	 * @param dtoCustomerMaintenance
	 * @return
	 */
	@RequestMapping(value = "/maintenanceGetById", method = RequestMethod.POST)
	public ResponseMessage maintenanceGetById(HttpServletRequest request,
			@RequestBody DtoCustomerMaintenance dtoCustomerMaintenance)  {
		LOGGER.info("inside maintenanceGetById method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCustomerMaintenance); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCustomerMaintenance != null) {
			DtoCustomerMaintenance response = serviceCustomerMaintenance.maintenanceGetById(dtoCustomerMaintenance);
			if (response != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_MAINTENANCE_NOT_FOUND, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		
		return responseMessage;
	}

	/**
	 * @description : method will update account maintenance
	 * @param request
	 * @param dtoCustomerMaintenance
	 * @return
	 */
	@RequestMapping(value = "/updateMaintenance", method = RequestMethod.POST)
	public ResponseMessage updateMaintenance(HttpServletRequest request,
			@RequestBody DtoCustomerMaintenance dtoCustomerMaintenance)  {
		LOGGER.info("inside updateMaintenance method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCustomerMaintenance); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCustomerMaintenance != null) {
			dtoCustomerMaintenance = serviceCustomerMaintenance.updateMaintenanceData(dtoCustomerMaintenance);
			if (dtoCustomerMaintenance != null) {
				if (dtoCustomerMaintenance.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_MAINTENANCE_UPDATE_SUCCESS, false),
							dtoCustomerMaintenance);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(dtoCustomerMaintenance.getMessageType(),
									false));
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_MAINTENANCE_UPDATE_FAIL, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get all or search account maintenance
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/searchMaintenance", method = RequestMethod.POST)
	public ResponseMessage searchMaintenance(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceCustomerMaintenance.searchMaintenance(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoCustomerMaintenance> paymentTermSetUpsList = (List<DtoCustomerMaintenance>) dtoSearch.getRecords();
			if (paymentTermSetUpsList != null && !paymentTermSetUpsList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will get account maintenance active type status
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getMaintenanceActiveTypeStatus", method = RequestMethod.POST)
	public ResponseMessage getMaintenanceActiveTypeStatus(HttpServletRequest request)  {
		LOGGER.info("inside getMaintenanceActiveTypeStatus method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceCustomerMaintenance.getMaintenanceActiveTypeStatus();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will get account maintenance hold type status
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getMaintenanceHoldTypeStatus", method = RequestMethod.POST)
	public ResponseMessage getMaintenanceHoldTypeStatus(HttpServletRequest request)  {
		LOGGER.info("inside getMaintenanceHoldTypeStatus method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceCustomerMaintenance.getMaintenanceHoldTypeStatus();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will delete account maintenance
	 * @param request
	 * @param dtoCustomerMaintenance
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/deleteMaintenance", method = RequestMethod.POST)
	public ResponseMessage deleteMaintenance(HttpServletRequest request,
			@RequestBody DtoCustomerMaintenance dtoCustomerMaintenance)  {
		LOGGER.info("inside deleteMaintenance method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCustomerMaintenance); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCustomerMaintenance != null) {
			boolean response = serviceCustomerMaintenance.deleteMaintenance(dtoCustomerMaintenance);
			if (response) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_MAINTENANCE_DELETED_SUCCESSFULLY, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_MAINTENANCE_NOT_FOUND, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		
		return responseMessage;
	}
	
	/**
	 * @description : method will setup salesman maintenance
	 * @param request
	 * @param dtoSalesmanMaintenance
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/salesmanMaintenance", method = RequestMethod.POST)
	public ResponseMessage salespersonsMaintenance(HttpServletRequest request,
			@RequestBody DtoSalesmanMaintenance dtoSalesmanMaintenance)  
	{
		LOGGER.info("inside salespersonsMaintenance method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoSalesmanMaintenance); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if (dtoSalesmanMaintenance != null) 
			{
			dtoSalesmanMaintenance = serviceCustomerMaintenance.saveSalesmanMaintenance(dtoSalesmanMaintenance);
				if (dtoSalesmanMaintenance != null) 
				{
					if (dtoSalesmanMaintenance.getMessageType() == null) 
					{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SALESPERSON_MAINTENANCE_SUCCESS, false),
							dtoSalesmanMaintenance);
					}
					else 
					{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoSalesmanMaintenance.getMessageType(),
									false));
				}
				} 
				else 
				{
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SALESPERSON_MAINTENANCE_FAIL, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will setup salesman maintenance by id
	 * @param request
	 * @param dtoSalesmanMaintenance
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/salesmanMaintenanceGetById", method = RequestMethod.POST)
	public ResponseMessage salesmanMaintenanceGetById(HttpServletRequest request,
			@RequestBody DtoSalesmanMaintenance dtoSalesmanMaintenance)  {
		LOGGER.info("inside salesmanMaintenanceGetById method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoSalesmanMaintenance); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSalesmanMaintenance != null) {
			dtoSalesmanMaintenance = serviceCustomerMaintenance.salesmanMaintenanceGetById(dtoSalesmanMaintenance);
			if (dtoSalesmanMaintenance != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoSalesmanMaintenance);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description : method will update salesman maintenance
	 * @param request
	 * @param dtoSalesmanMaintenance
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/updateSalesmanMaintenance", method = RequestMethod.POST)
	public ResponseMessage updateSalespersonsMaintenance(HttpServletRequest request,
			@RequestBody DtoSalesmanMaintenance dtoSalesmanMaintenance)  {
		LOGGER.info("inside updateSalespersonsMaintenance method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoSalesmanMaintenance); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSalesmanMaintenance != null) {
			dtoSalesmanMaintenance = serviceCustomerMaintenance.updateSalespersonsMaintenance(dtoSalesmanMaintenance);
			if (dtoSalesmanMaintenance != null) {
				if (dtoSalesmanMaintenance.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
							.getMessageByShortAndIsDeleted(MessageLabel.SALESPERSON_MAINTENANCE_UPDATE_SUCCESS, false),
							dtoSalesmanMaintenance);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(dtoSalesmanMaintenance.getMessageType(),
									false));
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SALESPERSON_MAINTENANCE_UPDATE_FAIL, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get all or search salesman maintenance
	 * @param dtoSearch
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/searchSalesmanMaintenance", method = RequestMethod.POST)
	public ResponseMessage searchSalespersonsMaintenance(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceCustomerMaintenance.searchSalesmanMaintenance(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoSalesmanMaintenance> paymentTermSetUpsList = (List<DtoSalesmanMaintenance>) dtoSearch.getRecords();
			if (paymentTermSetUpsList != null && !paymentTermSetUpsList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will save account receivable setup 
	 * @param dtoRMSetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/saveAccountRecievableSetup", method = RequestMethod.POST)
	public ResponseMessage saveAccountRecievableSetup(@RequestBody DtoRMSetup dtoRMSetup)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoRMSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		if (dtoRMSetup != null) {
			dtoRMSetup = serviceAccountRecievableSetup.saveRmSetup(dtoRMSetup);
			if (dtoRMSetup.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_RECIEVABLE_SETUP_SAVE_SUCCESS, false),
						dtoRMSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(dtoRMSetup.getMessageType(), false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	

	/**
	 * @description : method will get account receivable setup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getAccountRecievableSetup", method = RequestMethod.GET)
	public ResponseMessage getAccountRecievableSetupById()  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<RMSetup> rmSetupList = repositoryRMSetup.findAll();
		if (rmSetupList != null && !rmSetupList.isEmpty()) {
			DtoRMSetup dtoRMSetup = serviceAccountRecievableSetup.getRmSetup(rmSetupList.get(0));
			if (dtoRMSetup.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoRMSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(dtoRMSetup.getMessageType(), false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will save account receivable class setup
	 * @param dtoCustomerClassSetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/saveAccountRecievableClassSetup", method = RequestMethod.POST)
	public ResponseMessage saveAccountRecievableClassSetup(@RequestBody DtoCustomerClassSetup dtoCustomerClassSetup)  
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCustomerClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if (dtoCustomerClassSetup != null) 
			{
			CustomerClassesSetup customerClassesSetup = repositoryCustomerClassesSetup
					.findByCustomerClassIdAndIsDeleted(dtoCustomerClassSetup.getCustomerClassId(),false);
				if (customerClassesSetup == null) 
				{
				dtoCustomerClassSetup = serviceAccountRecievableSetup.saveCustomerClassSetup(dtoCustomerClassSetup);
					if (dtoCustomerClassSetup.getMessageType() == null) 
					{
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_RECIEVABLE_CLASS_SETUP_SAVE_SUCCESS,
									false),
							dtoCustomerClassSetup);
					} 
					else 
					{
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(dtoCustomerClassSetup.getMessageType(), false));
				}
				} 
				else 
				{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_CLASS_ID_ALREADY_USED, false));
			}
			} 
			else 
			{
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will update account receivable class setup
	 * @param dtoCustomerClassSetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/updateAccountRecievableClassSetup", method = RequestMethod.POST)
	public ResponseMessage updateAccountRecievableClassSetup(@RequestBody DtoCustomerClassSetup dtoCustomerClassSetup)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCustomerClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCustomerClassSetup != null) {
			CustomerClassesSetup customerClassesSetup = repositoryCustomerClassesSetup
					.findByCustomerClassIdAndIsDeleted(dtoCustomerClassSetup.getCustomerClassId(),false);
			if (customerClassesSetup != null) {
				dtoCustomerClassSetup = serviceAccountRecievableSetup.updateCustomerClassSetup(dtoCustomerClassSetup,
						customerClassesSetup);
				if (dtoCustomerClassSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
							.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_RECIEVABLE_CLASS_SETUP_UPDATE_SUCCESS, false),
							dtoCustomerClassSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(dtoCustomerClassSetup.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_CLASS_ID_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get account receivable class setup by class id
	 * @param dtoCustomerClassSetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getAccountRecievableClassSetupByClassId", method = RequestMethod.POST)
	public ResponseMessage getAccountRecievableClassSetupByClassId(
			@RequestBody DtoCustomerClassSetup dtoCustomerClassSetup)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCustomerClassSetup != null) {
			CustomerClassesSetup customerClassesSetup = repositoryCustomerClassesSetup
					.findByCustomerClassIdAndIsDeleted(dtoCustomerClassSetup.getCustomerClassId(),false);
			if (customerClassesSetup != null) {
				dtoCustomerClassSetup = serviceAccountRecievableSetup
						.getCustomerClassSetupByClassId(dtoCustomerClassSetup, customerClassesSetup);
				if (dtoCustomerClassSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
							dtoCustomerClassSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(dtoCustomerClassSetup.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_CLASS_ID_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will get all or search account receivable class setup
	 * @param dtoSearch
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/searchAccountRecievableClassSetup", method = RequestMethod.POST)
	public ResponseMessage searchAccountRecievableClassSetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSearch != null) {
			dtoSearch = serviceAccountRecievableSetup.searchCustomerClassSetup(dtoSearch);
			@SuppressWarnings("unchecked")
			List<DtoCustomerClassSetup> list = (List<DtoCustomerClassSetup>) dtoSearch.getRecords();
			if (!list.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		return responseMessage;
	}
	
	/**
	 * @description : method will save account receivable setup options
	 * @param dtoRMDocumentType
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/saveAccountRecievableSetupOptions", method = RequestMethod.POST)
	public ResponseMessage saveAccountRecievableSetupOptions(@RequestBody DtoRMDocumentType dtoRMDocumentType)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoRMDocumentType); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoRMDocumentType != null) {
			dtoRMDocumentType = serviceAccountRecievableSetup.saveRMPeriodSetup(dtoRMDocumentType);
			if (dtoRMDocumentType.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_RECIEVABLE_OPTIONS_SAVE_SUCCESS, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(dtoRMDocumentType.getMessageType(), false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get account receivable setup options
	 * @param dtoSearch
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getRecievableSetupOptions", method = RequestMethod.POST)
	public ResponseMessage getRecievableSetupOptions(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSearch != null) {
			dtoSearch = serviceAccountRecievableSetup.getAccountRecievableOptions(dtoSearch);
			if (dtoSearch.getRecords() != null) {
				@SuppressWarnings("unchecked")
				List<DtoRMDocumentType> list = (List<DtoRMDocumentType>) dtoSearch.getRecords();
				if (!list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will save customer maintenance options
	 * @param dtoCustomerMaintenanceOptions
	 * @return
	 */
	@RequestMapping(value = "/saveCustomerMaintenanceOptions", method = RequestMethod.POST)
	public ResponseMessage saveCustomerMaintenanceOptions(
			@RequestBody DtoCustomerMaintenanceOptions dtoCustomerMaintenanceOptions)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCustomerMaintenanceOptions); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoCustomerMaintenanceOptions != null) {
			CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance
					.findByCustnumberAndIsDeleted(dtoCustomerMaintenanceOptions.getCustomerNumberId(),false);
			if (customerMaintenance != null) {
				dtoCustomerMaintenanceOptions = serviceAccountRecievableSetup
						.saveCustomerMaintenanceOptions(dtoCustomerMaintenanceOptions);
				if (dtoCustomerMaintenanceOptions.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(
									MessageLabel.CUSTOMER_MAINTENANCE_OPTIONS_SAVE_SUCCESSFULLY, false),
							dtoCustomerMaintenanceOptions);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse.getMessageByShortAndIsDeleted(
									dtoCustomerMaintenanceOptions.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_NUMBER_ID_IS_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get customer maintenance option
	 * @return
	 */
	@RequestMapping(value = "/getCustomerMaintenanceOptionByCustomerId", method = RequestMethod.POST)
	public ResponseMessage getCustomerMaintenanceOption(@RequestBody DtoCustomerMaintenanceOptions dtoCustomerMaintenanceOptions )  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			 dtoCustomerMaintenanceOptions = serviceAccountRecievableSetup
					.getCustomerMaintenanceOptions(dtoCustomerMaintenanceOptions);
			if (dtoCustomerMaintenanceOptions.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoCustomerMaintenanceOptions);
			}
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
								.getMessageByShortAndIsDeleted(dtoCustomerMaintenanceOptions.getMessageType(), false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will save customer account maintenance
	 * @param request
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	@RequestMapping(value = "/saveCustomerAccountMaintenance", method = RequestMethod.POST)
	public ResponseMessage saveCustomerAccountMaintenance(HttpServletRequest request,
			@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup)  {
		LOGGER.info("inside save customer Account maintenance  method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPayableAccountClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPayableAccountClassSetup = serviceAccountRecievableSetup
				.saveCustomerAccountMaintenance(dtoPayableAccountClassSetup);
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
					.getMessageByShortAndIsDeleted(MessageLabel.CUSTOMER_ACCOUNT_MAINTENANCE_SAVE_SUCCESSFULLY, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(dtoPayableAccountClassSetup.getMessageType(), false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get customer account maintenance
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getCustomerAccountMaintenanceByCustomerId", method = RequestMethod.POST)
	public ResponseMessage getCustomerAccountMaintenanceByCustomerId(HttpServletRequest request, @RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup )  {
		LOGGER.info("inside get customer account maintenance method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		 dtoPayableAccountClassSetup = serviceAccountRecievableSetup
				.getCustomerAccountMaintenanceByCustomerId(dtoPayableAccountClassSetup.getCustomerId());
		if (dtoPayableAccountClassSetup != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will setup account receivable class
	 * @param request
	 * @param dtoPayableAccountClassSetup
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/accountRecievableClassGlSetup", method = RequestMethod.POST)
	public ResponseMessage accountRecievableClassGlSetup(HttpServletRequest request,
			@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup)  {
		LOGGER.info("inside save account Recievable Class Gl Setup  method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPayableAccountClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPayableAccountClassSetup = serviceAccountRecievableSetup
				.saveAccountRecievableClassGlSetup(dtoPayableAccountClassSetup);
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_RECIEVANLE_CLASS_GL_SETUP, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(dtoPayableAccountClassSetup.getMessageType(), false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get account receivable class
	 * @param request
	 * @return
	 * @ 
	 */
	@RequestMapping(value = "/getAccountRecievableClassGlSetupByCustomerClassId", method = RequestMethod.POST)
	public ResponseMessage getAccountRecievableClassGlSetupByCustomerClassId(HttpServletRequest request, @RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup)  {
		LOGGER.info("inside get customer account maintenance method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPayableAccountClassSetup = serviceAccountRecievableSetup
				.getCustomerAccountClassGlSetupByCustomerClassId(dtoPayableAccountClassSetup.getCustomerClassId());
		if (dtoPayableAccountClassSetup != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchActiveCustomerMaintenance", method = RequestMethod.POST)
	public ResponseMessage searchActiveCustomerMaintenance(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceCustomerMaintenance.searchActiveCustomerMaintenance(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoCustomerMaintenance> paymentTermSetUpsList = (List<DtoCustomerMaintenance>) dtoSearch.getRecords();
			if (paymentTermSetUpsList != null && !paymentTermSetUpsList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/searchActiveSalesmanMaintenance", method = RequestMethod.POST)
	public ResponseMessage searchActiveSalesmanMaintenance(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceCustomerMaintenance.searchActiveSalesmanMaintenance(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoSalesmanMaintenance> paymentTermSetUpsList = (List<DtoSalesmanMaintenance>) dtoSearch.getRecords();
			if (paymentTermSetUpsList != null && !paymentTermSetUpsList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
}
