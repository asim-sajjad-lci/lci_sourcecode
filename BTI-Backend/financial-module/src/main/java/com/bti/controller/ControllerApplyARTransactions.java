/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoARYTDOpenTransactions;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryARBatches;
import com.bti.service.ServiceApplyARTransactions;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerApplyARTransactions
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/accountReceivable")
public class ControllerApplyARTransactions {

	private static final Logger LOGGER = Logger.getLogger(ControllerApplyARTransactions.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	ServiceApplyARTransactions serviceApplyARTransactions;

	@Autowired
	RepositoryARBatches repositoryARBatches;

	/**
	 * @description this service will use for get master document type
	 * @param request, dtoFiscalFinancialPeriodSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/getDocumentType", method = RequestMethod.GET)
	public ResponseMessage getDocumentType(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 List<DtoStatusType> response = this.serviceApplyARTransactions.getDocumentType();
			if (response!=null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getOpenTransactionsByCustomerId", method = RequestMethod.POST)
	public ResponseMessage getOpenTransactionsByCustomerIdAndPostingDate(HttpServletRequest request, @RequestBody DtoARYTDOpenTransactions dtoARYTDOpenTransactions)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			DtoSearch dtoSearch = this.serviceApplyARTransactions.getOpenTransactionsByCustomerId(dtoARYTDOpenTransactions);
			if (dtoSearch!=null) 
			{
				@SuppressWarnings("unchecked")
				List<DtoARYTDOpenTransactions> dtoARYTDOpenTransactionsList= (List<DtoARYTDOpenTransactions>) dtoSearch.getRecords();
				if(dtoARYTDOpenTransactionsList==null || dtoARYTDOpenTransactionsList.isEmpty()){
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
					return responseMessage;
				}
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getOpenTransactionsByPostingDate", method = RequestMethod.POST)
	public ResponseMessage getOpenTransactionsByPostingDate(HttpServletRequest request, @RequestBody DtoARYTDOpenTransactions dtoARYTDOpenTransactions)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			DtoSearch dtoSearch = this.serviceApplyARTransactions.getOpenTransactionsByPostingDateAndCustomerId(dtoARYTDOpenTransactions);
			if (dtoSearch!=null) 
			{
				@SuppressWarnings("unchecked")
				List<DtoARYTDOpenTransactions> dtoARYTDOpenTransactionsList= (List<DtoARYTDOpenTransactions>) dtoSearch.getRecords();
				if(dtoARYTDOpenTransactionsList==null || dtoARYTDOpenTransactionsList.isEmpty()){
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
					return responseMessage;
				}
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/applyTransaction", method = RequestMethod.POST)
	public ResponseMessage applyTransaction(HttpServletRequest request, @RequestBody DtoARYTDOpenTransactions dtoARYTDOpenTransactions){
		LOGGER.info("inside applyTransaction method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoARYTDOpenTransactions); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			dtoARYTDOpenTransactions = serviceApplyARTransactions.applyTransaction(dtoARYTDOpenTransactions);
			if (dtoARYTDOpenTransactions != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),
						dtoARYTDOpenTransactions);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	@RequestMapping(value = "/unApplyTransaction", method = RequestMethod.POST)
	public ResponseMessage unApplyTransaction(HttpServletRequest request, @RequestBody DtoARYTDOpenTransactions dtoARYTDOpenTransactions){
		LOGGER.info("inside applyTransaction method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoARYTDOpenTransactions); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean response = serviceApplyARTransactions.unApplyTransaction(dtoARYTDOpenTransactions);
			if (response) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_UPDATED_SUCCESSFULLY, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	@RequestMapping(value = "/getDocumentNumberByCustomerAndDocType", method = RequestMethod.POST)
	public ResponseMessage getDocumentNumberByCustomerAndDocType(HttpServletRequest request, @RequestBody DtoARYTDOpenTransactions dtoARYTDOpenTransactions)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoARYTDOpenTransactions> dtoARYTDOpenTransactionsList = this.serviceApplyARTransactions.getDocumentNumberByCustomerAndDocType(dtoARYTDOpenTransactions);
			if (dtoARYTDOpenTransactionsList!=null && !dtoARYTDOpenTransactionsList.isEmpty()) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoARYTDOpenTransactionsList);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAmountByPaymentNumber", method = RequestMethod.POST)
	public ResponseMessage getAmountByPaymentNumber(HttpServletRequest request, @RequestBody DtoARYTDOpenTransactions dtoARYTDOpenTransactions ) throws ScriptException  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoARYTDOpenTransactions = this.serviceApplyARTransactions.getAmountByPaymentNumber(dtoARYTDOpenTransactions);
			if (dtoARYTDOpenTransactions!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoARYTDOpenTransactions);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	
	
	
}
