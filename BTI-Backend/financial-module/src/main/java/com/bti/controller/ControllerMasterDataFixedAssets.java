/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.FAInsuranceClassSetup;
import com.bti.model.FAInsuranceMaintenance;
import com.bti.model.FALeaseMaintenance;
import com.bti.model.dto.DtoAccountTableSetup;
import com.bti.model.dto.DtoFABookMaintenance;
import com.bti.model.dto.DtoFACalendarSetup;
import com.bti.model.dto.DtoFALeaseMaintenance;
import com.bti.model.dto.DtoInsuranceMaintenance;
import com.bti.model.dto.DtoLeaseType;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryFAInsuranceClassSetup;
import com.bti.repository.RepositoryFAInsuranceMaintenance;
import com.bti.repository.RepositoryFALeaseMaintenance;
import com.bti.repository.RepositoryLeaseType;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceMasterDataFixedAssets;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;
import com.bti.util.UtilRandomKey;

/**
 * Description: ControllerFixedAssets
 * Name of Project: BTI
 * Created on: Aug 22, 2017
 * Modified on: Aug 22, 2017 11:08:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/masterdata/fixed/assets")
public class ControllerMasterDataFixedAssets {

	private static final Logger LOG = Logger.getLogger(ControllerMasterDataFixedAssets.class);

	@Autowired
	ServiceMasterDataFixedAssets serviceMasterDataFixedAssets;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryFAInsuranceMaintenance repositoryFAInsuranceMaintenance;

	@Autowired
	RepositoryLeaseType repositoryLeaseType;

	@Autowired
	RepositoryFALeaseMaintenance repositoryFALeaseMaintenance;
	
	@Autowired
	RepositoryFAInsuranceClassSetup repositoryFAInsuranceClassSetup;
	
	@Autowired
	ServiceHome serviceHome;
	 	 
	/**
	 * @Description This service will use to add book maintenance
	 * @param dtoFABookMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/bookMaintenance/add", method = RequestMethod.POST)
	public ResponseMessage bookMaintenance(@RequestBody DtoFABookMaintenance dtoFABookMaintenance) {
		LOG.info("inside bookMaintenance method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFABookMaintenance); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		if (dtoFABookMaintenance != null && UtilRandomKey.isNotBlank(dtoFABookMaintenance.getAssetId())) {
			dtoFABookMaintenance = serviceMasterDataFixedAssets.saveBookMaintenance(dtoFABookMaintenance);
			if (dtoFABookMaintenance != null) {
				if (dtoFABookMaintenance.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(
										MessageLabel.BOOK_MAINTENANCE_SAVED_SUCCESSFULLY, false),
							dtoFABookMaintenance);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(dtoFABookMaintenance.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.BOOK_MAINTENANCE_SAVE_FAIL, false));
			}
			} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
		
	/**
	 * @Description This service will use to update book maintenance
	 * @param dtoFABookMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/bookMaintenance/update", method = RequestMethod.POST)
	public ResponseMessage updateCalendarMonthlySetup(@RequestBody DtoFABookMaintenance dtoFABookMaintenance)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFABookMaintenance); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoFABookMaintenance != null) {
			dtoFABookMaintenance = serviceMasterDataFixedAssets.updateBookMaintenance(dtoFABookMaintenance);
			if (dtoFABookMaintenance != null) {
				if (dtoFABookMaintenance.getMessageType() == null) {
					responseMessage = new ResponseMessage(
							HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
									.getMessageByShortAndIsDeleted(MessageLabel.BOOK_MAINTENANCE_UPDATED_SUCCESSFULLY, false),
							dtoFABookMaintenance);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
									.getMessageByShortAndIsDeleted(dtoFABookMaintenance.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.BOOK_MAINTENANCE_UPDATED_FAIL, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
		
	/**
	 * @Description This service will use to get book maintenance
	 * @param dtoFABookMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/bookMaintenance/getById", method = RequestMethod.POST)
	public ResponseMessage getCalendarMonthlySetupById(@RequestBody DtoFABookMaintenance dtoFABookMaintenance)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoFABookMaintenance != null) 
		{
			dtoFABookMaintenance = serviceMasterDataFixedAssets.getById(dtoFABookMaintenance);
			if (dtoFABookMaintenance != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoFABookMaintenance);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
		
	/**
	 * @Description This service will use to search and get all book maintenance
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/bookMaintenance/search", method = RequestMethod.POST)
	public ResponseMessage searchCalendarMonthlySetup(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSearch != null) {
			dtoSearch = serviceMasterDataFixedAssets.searchBookMaintenance(dtoSearch);
			@SuppressWarnings("unchecked")
			List<DtoFACalendarSetup> list = (List<DtoFACalendarSetup>) dtoSearch.getRecords();
			if (list != null && !list.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
		
	/**
	 * @Description This service will use to display master averaging Convention Type
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/bookMaintenance/averagingConventionType", method = RequestMethod.GET)
	public ResponseMessage averagingConventionType(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceMasterDataFixedAssets.getAveragingConventionType();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
		
	/**
	 * @Description This service will use to display master switch Over Type
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/bookMaintenance/switchOverType", method = RequestMethod.GET)
	public ResponseMessage switchOverType(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceMasterDataFixedAssets.getSwitchOverType();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to display master amortization Code Type
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/bookMaintenance/amortizationCodeType", method = RequestMethod.GET)
	public ResponseMessage amortizationCodeType(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceMasterDataFixedAssets.getAmortizationCodeType();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
		
	/**
	 * @Description This service will use to display master depreciation method
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/bookMaintenance/depreciationMethod", method = RequestMethod.GET)
	public ResponseMessage getDepreciationMethodType(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> response = serviceMasterDataFixedAssets.getDepreciationMethodType();
		if (response != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
		
	/**
	 * @Description This service will use to create new insurance maintenance
	 * @param dtoInsuranceMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/insuranceMaintenance", method = RequestMethod.POST)
	public ResponseMessage insuranceMaintenance(@RequestBody DtoInsuranceMaintenance dtoInsuranceMaintenance)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoInsuranceMaintenance); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		    FAInsuranceClassSetup faInsuranceClassSetup= repositoryFAInsuranceClassSetup.findByInsuranceClassId(dtoInsuranceMaintenance.getInsClassId());
			if (faInsuranceClassSetup !=null &&  UtilRandomKey.isNotBlank(dtoInsuranceMaintenance.getInsClassId())) 
			{
				FAInsuranceMaintenance faInsuranceMaintenance= repositoryFAInsuranceMaintenance.findByFaInsuranceClassSetupInsuranceClassIdAndIsDeleted(dtoInsuranceMaintenance.getInsClassId(),false);
				if(faInsuranceMaintenance==null)
				{
					dtoInsuranceMaintenance = serviceMasterDataFixedAssets.saveInsuranceMaintenence(dtoInsuranceMaintenance);
					if(dtoInsuranceMaintenance!=null)
					{
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INSURANCE_MAINTENANCE_SAVED_SUCCESSFULLY, false),
								dtoInsuranceMaintenance);
					}
					else
					{
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR, false));
					}	
				}
				else
				{
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INSURANCE_CLASS_ID_ALREADY_USED, false));
				}
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
		
	/**
	 * @Description This service will use to update insurance maintenance
	 * @param dtoInsuranceMaintenance
	 * @return
	 */
	@RequestMapping(value = "/insuranceMaintenance/update", method = RequestMethod.POST)
	public ResponseMessage updateInsuranceMaintenance(@RequestBody DtoInsuranceMaintenance dtoInsuranceMaintenance)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoInsuranceMaintenance); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
		FAInsuranceMaintenance faInsuranceMaintenance = repositoryFAInsuranceMaintenance
					.findByFaInsuranceClassSetupInsuranceClassIdAndIsDeleted(dtoInsuranceMaintenance.getInsClassId(),
							false);
		if (faInsuranceMaintenance != null) {
				dtoInsuranceMaintenance = serviceMasterDataFixedAssets
						.updateInsuranceMaintenance(dtoInsuranceMaintenance, faInsuranceMaintenance);
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(
								MessageLabel.INSURANCE_MAINTENANCE_UPDATE_SUCCESSFULLY, false),
					dtoInsuranceMaintenance);
			} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to get insurance maintenance
	 * @param dtoInsuranceMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/insuranceMaintenance/getById", method = RequestMethod.POST)
	public ResponseMessage getInsuranceMaintenance(@RequestBody DtoInsuranceMaintenance dtoInsuranceMaintenance)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		FAInsuranceMaintenance faInsuranceMaintenance = repositoryFAInsuranceMaintenance
				.findByFaInsuranceClassSetupInsuranceClassIdAndIsDeleted(dtoInsuranceMaintenance.getInsClassId(),false);
		if (faInsuranceMaintenance != null) {
			dtoInsuranceMaintenance = serviceMasterDataFixedAssets.insuranceMaintenanceGetById(faInsuranceMaintenance);
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
					dtoInsuranceMaintenance);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to search and get all insurance maintenances
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/insuranceMaintenance/search", method = RequestMethod.POST)
	public ResponseMessage searchInsuranceMaintenance(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSearch != null) {
			dtoSearch = serviceMasterDataFixedAssets.insuranceMaintenanceSearch(dtoSearch);
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INSURANCE_MAINTENANCE_FETCHED_SUCCESSFULLY, false),
					dtoSearch);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to display master lease types
	 * @return
	 */
	@RequestMapping(value = "/leaseType", method = RequestMethod.GET)
	public ResponseMessage getLeaseType()  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoLeaseType> dtoLeaseTypes = serviceMasterDataFixedAssets.getAllLeaseType();
		if (!dtoLeaseTypes.isEmpty()) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LEASE_TYPE_FETCHED_SUCCESSFULLY, false),
					dtoLeaseTypes);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to create new lease maintenance
	 * @param faLeaseMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/leaseMaintenance", method = RequestMethod.POST)
	public ResponseMessage leaseMaintenance(@RequestBody DtoFALeaseMaintenance faLeaseMaintenance)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(faLeaseMaintenance); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (faLeaseMaintenance != null) {
			FALeaseMaintenance leaseMaintenance = repositoryFALeaseMaintenance
					.findByFaLeaseCompanySetupLeaseCompanyIndexAndIsDeleted(faLeaseMaintenance.getLeaseCompanyIndex(),false);
			if (leaseMaintenance == null) {
				faLeaseMaintenance = serviceMasterDataFixedAssets.saveLeaseMaintenence(faLeaseMaintenance);
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LEASE_MAINTENANCE_SAVED_SUCCESSFULLY, false),
						faLeaseMaintenance);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST,false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to update lease maintenance
	 * @param faLeaseMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/leaseMaintenance/update", method = RequestMethod.POST)
	public ResponseMessage updateLeaseMaintenance(@RequestBody DtoFALeaseMaintenance faLeaseMaintenance)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(faLeaseMaintenance); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		FALeaseMaintenance leaseMaintenance = repositoryFALeaseMaintenance
				.findOne(faLeaseMaintenance.getLeaseMaintenanceIndex());
		if (leaseMaintenance != null) {
			faLeaseMaintenance = serviceMasterDataFixedAssets.updateLeaseMaintenence(faLeaseMaintenance,
					leaseMaintenance);
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LEASE_MAINTENANCE_UPDATED_SUCCESSFULLY, false),
					faLeaseMaintenance);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to get lease maintenance
	 * @param faLeaseMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/leaseMaintenance/getById", method = RequestMethod.POST)
	public ResponseMessage getLeaseMaintenance(@RequestBody DtoFALeaseMaintenance faLeaseMaintenance)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		FALeaseMaintenance leaseMaintenance = repositoryFALeaseMaintenance
				.findOne(faLeaseMaintenance.getLeaseMaintenanceIndex());
		if (leaseMaintenance != null) {
			faLeaseMaintenance = serviceMasterDataFixedAssets.leaseMaintenenceGetById(leaseMaintenance);
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), faLeaseMaintenance);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to search and get all lease maintenances
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/leaseMaintenance/search", method = RequestMethod.POST)
	public ResponseMessage searchLeaseMaintenance(@RequestBody DtoSearch dtoSearch)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoSearch != null) {
			dtoSearch = serviceMasterDataFixedAssets.leaseMaintenanceSearch(dtoSearch);
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LEASE_MAINTENANCE_FETCHED_SUCCESSFULLY, false),
					dtoSearch);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to display master account types
	 * @return
	 
	 */
	@RequestMapping(value = "/accountTableSetup/accountType", method = RequestMethod.GET)
	public ResponseMessage accountType()  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoStatusType> dtoStatusTypes = serviceMasterDataFixedAssets.getAllAccountType();
		if (dtoStatusTypes != null && !dtoStatusTypes.isEmpty()) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoStatusTypes);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to create new account table setup
	 * @param dtoAccountTableSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/accountTableSetup", method = RequestMethod.POST)
	public ResponseMessage accountTableSetup(@RequestBody DtoAccountTableSetup dtoAccountTableSetup)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAccountTableSetup); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoAccountTableSetup != null && UtilRandomKey.isNotBlank(dtoAccountTableSetup.getAssetId())) {

			dtoAccountTableSetup = serviceMasterDataFixedAssets.saveAccountTableSetup(dtoAccountTableSetup);
			if (dtoAccountTableSetup.getMessageType() == null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TABLE_SETUP_SAVE_SUCCESSFULLY, false),
						dtoAccountTableSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(dtoAccountTableSetup.getMessageType(), false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @Description This service will use to get account table setup
	 * @return
	 
	 */
	@RequestMapping(value = "/getAccountTableSetup", method = RequestMethod.GET)
	public ResponseMessage getAccountTableSetup()  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		DtoAccountTableSetup dtoAccountTableSetup = serviceMasterDataFixedAssets.getAccountTableSetup();
		if (dtoAccountTableSetup != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
					dtoAccountTableSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getLifeDayByFABookId", method = RequestMethod.POST)
	public ResponseMessage getLifeDayByFABookId(@RequestBody DtoFABookMaintenance dtoFABookMaintenance)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoFABookMaintenance != null) 
		{
			Map<String, Object> map = serviceMasterDataFixedAssets.getLifeDayByBookId(dtoFABookMaintenance);
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						map);
		}
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getLifeDayDynamicByFABookId", method = RequestMethod.POST)
	public ResponseMessage getLifeDayDynamicByFABookId(@RequestBody DtoFABookMaintenance dtoFABookMaintenance)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoFABookMaintenance != null) 
		{
			Map<String, Object> map = serviceMasterDataFixedAssets.getLifeDayDynamic(dtoFABookMaintenance);
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						map);
		}
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
}
