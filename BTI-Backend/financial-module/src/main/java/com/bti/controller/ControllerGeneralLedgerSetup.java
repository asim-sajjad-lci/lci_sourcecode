/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.CurrencyExchangeDetail;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.GLConfigurationAccountCategories;
import com.bti.model.GLConfigurationDefinePeriodsHeader;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.dto.DtoARTransactionEntry;
import com.bti.model.dto.DtoAccountCategory;
import com.bti.model.dto.DtoAuditTrailCode;
import com.bti.model.dto.DtoCheckBookMaintenance;
import com.bti.model.dto.DtoCurrencyExchangeDetail;
import com.bti.model.dto.DtoCurrencyExchangeTableSetupHeader;
import com.bti.model.dto.DtoCurrencySetup;
import com.bti.model.dto.DtoFinancialDimensionSetUp;
import com.bti.model.dto.DtoFinancialDimensionValue;
import com.bti.model.dto.DtoFiscalFinancialPeriodSetup;
import com.bti.model.dto.DtoGLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.bti.model.dto.DtoGLDisplaytype;
import com.bti.model.dto.DtoGeneralLedgerSetup;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoMainAccountSetUp;
import com.bti.model.dto.DtoMainAccountTypes;
import com.bti.model.dto.DtoMassCloseFiscalPeriodSetup;
import com.bti.model.dto.DtoMassCloseFiscalPeriodSetupRequest;
import com.bti.model.dto.DtoOrigin;
import com.bti.model.dto.DtoPeriods;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCurrencyExchangeDetail;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLConfigurationAccountCategories;
import com.bti.repository.RepositoryGLConfigurationDefinePeriodsHeader;
import com.bti.repository.RepositoryGLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.service.ServiceCurrencyExchange;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceGeneralLedgerSetup;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceMainAccountTypes;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;
import com.bti.util.UtilRandomKey;

/**
 * Description: ControllerGeneralLedgerSetup Name of Project: BTI Created on:
 * Aug 04, 2017 Modified on: Aug 04, 2017 11:39:38 AM
 * 
 * @author seasia Version:
 */

@RestController
@RequestMapping("/setup/generalLedger")
public class ControllerGeneralLedgerSetup {

	private static final Logger LOGGER = Logger.getLogger(ControllerGeneralLedgerSetup.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	ServiceCurrencyExchange serviceCurrencyExchange;

	@Autowired
	ServiceMainAccountTypes serviceMainAccountTypes;

	@Autowired
	RepositoryCurrencyExchangeDetail repositoryCurrencyExchangeDetail;

	@Autowired
	ServiceGeneralLedgerSetup serviceGeneralLedgerSetup;

	@Autowired
	RepositoryGLConfigurationDefinePeriodsHeader repositoryGLConfigurationDefinePeriodsHeader;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	RepositoryGLConfigurationRelationBetweenDimensionsAndMainAccount repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount;

	@Autowired
	RepositoryGLConfigurationAccountCategories repositoryGLConfigurationAccountCategories;

	@Autowired
	ServiceHome serviceHome;

	/**
	 * @description this service will use to setup general Ledger currency Setup
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 */
	@RequestMapping(value = "/currencySetup", method = RequestMethod.POST)
	public ResponseMessage currencySetup(HttpServletRequest request, @RequestBody DtoCurrencySetup dtoCurrencySetup)  
	{
		LOGGER.info("inside currencySetup method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCurrencySetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if(dtoCurrencySetup != null)
			{
				CurrencySetup currencySetup = this.repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCurrencySetup.getCurrencyId(),false);
				if(currencySetup != null)
				{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_ALREADY_EXIST, false));
				} 
				else 
				{
					if(UtilRandomKey.isNotBlank(dtoCurrencySetup.getCurrencyId()))
					{
						Boolean result  = this.serviceCurrencySetup.saveCurrencySetup(dtoCurrencySetup);
						if (result) {
							responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
									this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_SETUP_SUCCESS, false),
									dtoCurrencySetup);
						} else {
							responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_SETUP_FAIL, false), null);
						}
					}
					else
					{
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_ID_EMPTY, false), null);
					}
				}
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Currency Setup Details
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 
	 */
	@RequestMapping(value = "/getCurrencySetupDetails", method = RequestMethod.POST)
	public ResponseMessage getCurrencySetupDetails(HttpServletRequest request, @RequestBody DtoCurrencySetup dtoCurrencySetup)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoCurrencySetup != null && dtoCurrencySetup.getCurrencyId() != null && dtoCurrencySetup.getCurrencyId().trim().length() > 0){
			dtoCurrencySetup = this.serviceCurrencySetup.getCurrencySetupDeatailsByCurrencyId(dtoCurrencySetup.getCurrencyId());
			if(dtoCurrencySetup != null){
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), dtoCurrencySetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_ID_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger search Currency Setup Details
	 * @param request
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/searchCurrencySetups", method = RequestMethod.POST)
	public ResponseMessage searchPaymentTermSetup(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage=null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = this.serviceCurrencySetup.searchCurrencuSetups(dtoSearch);
		if (dtoSearch != null && dtoSearch.getRecords() != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_SETUP_LIST_SUCCESS, false), dtoSearch);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger update Currency Setup Details
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 */
	@RequestMapping(value = "/updateCurrencySetup", method = RequestMethod.POST)
	public ResponseMessage updateCurrencySetup(HttpServletRequest request, @RequestBody DtoCurrencySetup dtoCurrencySetup)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCurrencySetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoCurrencySetup != null)
		{
				CurrencySetup currencySetup = this.repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCurrencySetup.getCurrencyId(),false);
				if(currencySetup!=null)
				{
					Boolean result  = this.serviceCurrencySetup.updateCurrencySetup(dtoCurrencySetup,currencySetup);
					if (result) {
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_SETUP_UPDATE_SUCCESS, false), dtoCurrencySetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_SETUP_UPDATE_FAIL, false), null);
					}
				
			    } 
			    else 
			    {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			    }	
		 }
		 else
		 {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		 }
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger save Currency Exchange Header Setup
	 * @param request
	 * @param dtoCurrencyExchangeTableSetupHeader
	 * @return
	 */
	@RequestMapping(value = "/saveCurrencyExchangeHeaderSetup", method = RequestMethod.POST)
	public ResponseMessage saveCurrencyExchangeHeaderSetup(HttpServletRequest request, @RequestBody DtoCurrencyExchangeTableSetupHeader dtoCurrencyExchangeTableSetupHeader)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCurrencyExchangeTableSetupHeader); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoCurrencyExchangeTableSetupHeader != null){
			CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIdAndIsDeleted(dtoCurrencyExchangeTableSetupHeader.getExchangeId(),false);
			if(currencyExchangeHeader != null){
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_ID_ALREADY_EXIST, false));
			} 
			else 
			{
				if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeTableSetupHeader.getExchangeId()))
				{
					Boolean result  = this.serviceCurrencyExchange.saveCurrencyExchangeHeaderSetUp(dtoCurrencyExchangeTableSetupHeader);
					if (result) {
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_SETUP_SUCCESS, false));
					} else {
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_SETUP_FAIL, false));
					}
				}
				else
				{
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_SETUP_FAIL, false));
				}
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Currency Exchange Header Setup
	 * @param request
	 * @param dtoCurrencyExchangeTableSetupHeader
	 * @return
	 */
	@RequestMapping(value = "/getCurrencyExchangeHeaderSetup", method = RequestMethod.POST)
	public ResponseMessage getCurrencyExchangeHeaderSetup(HttpServletRequest request, @RequestBody DtoCurrencyExchangeTableSetupHeader dtoCurrencyExchangeTableSetupHeader)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if(dtoCurrencyExchangeTableSetupHeader != null)
			{
				CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIdAndIsDeleted(dtoCurrencyExchangeTableSetupHeader.getExchangeId(),false);
				if(currencyExchangeHeader == null){
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_ID_NOT_FOUND, false));
				} 
				else 
				{
					dtoCurrencyExchangeTableSetupHeader  = this.serviceCurrencyExchange.getCurrencyExchateSetupByExchangeId(dtoCurrencyExchangeTableSetupHeader);
						responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoCurrencyExchangeTableSetupHeader);
				}
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger update Currency Exchange Header Setup
	 * @param request
	 * @param dtoCurrencyExchangeTableSetupHeader
	 * @return
	 */
	@RequestMapping(value = "/updateCurrencyExchangeHeaderSetup", method = RequestMethod.POST)
	public ResponseMessage updateCurrencyExchangeHeaderSetup(HttpServletRequest request, @RequestBody DtoCurrencyExchangeTableSetupHeader dtoCurrencyExchangeTableSetupHeader)  
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCurrencyExchangeTableSetupHeader); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if(dtoCurrencyExchangeTableSetupHeader != null)
			{
				CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIdAndIsDeleted(dtoCurrencyExchangeTableSetupHeader.getExchangeId(),false);
				if(currencyExchangeHeader == null)
				{
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_ID_NOT_FOUND, false));
				} 
				else 
				{
					Boolean result  = this.serviceCurrencyExchange.updateExchangeTableSetUp(dtoCurrencyExchangeTableSetupHeader);
					if (result) 
					{
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_UPDATED_SUCCESS, false));
					} 
					else 
					{
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_UPDATED_FAIL, false));
					}
				}
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get all or search Currency Exchange Header Setup
	 * @param request
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/searchCurrencyExchangeHeaderSetup", method = RequestMethod.POST)
	public ResponseMessage searchCurrencyExchangeHeaderSetup(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage=null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = this.serviceCurrencyExchange.searchCurrencyExchangeHeaderSetups(dtoSearch);
		if (dtoSearch != null && dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoCurrencyExchangeTableSetupHeader> list = (List<DtoCurrencyExchangeTableSetupHeader>) dtoSearch.getRecords();
			if(!list.isEmpty()){
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger save Currency Exchange details
	 * @param request
	 * @param dtoCurrencyExchangeDetail
	 * @return
	 
	 */
	@RequestMapping(value = "/saveCurrencyExchangeDetail", method = RequestMethod.POST)
	public ResponseMessage saveCurrencyExchangeDetail(HttpServletRequest request, @RequestBody DtoCurrencyExchangeDetail dtoCurrencyExchangeDetail)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCurrencyExchangeDetail); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if(dtoCurrencyExchangeDetail != null)
			{
				CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIdAndIsDeleted(dtoCurrencyExchangeDetail.getExchangeId(),false);
				if(currencyExchangeHeader == null){
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_ID_NOT_FOUND, false));
				} 
				else 
				{
					if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeId())){
						Boolean result  = this.serviceCurrencyExchange.saveCurrencyExchangeDetail(dtoCurrencyExchangeDetail);
						if (result) {
							responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SAVE_CURRENCY_EXCHANGE_DETAIL_SUCCESS, false));
						} else {
							responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SAVE_CURRENCY_EXCHANGE_DETAIL_FAIL, false));
						}
					}
					else
					{
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SAVE_CURRENCY_EXCHANGE_DETAIL_FAIL, false));
					}
				}
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Currency Exchange details
	 * @param request
	 * @param dtoCurrencyExchangeDetail
	 * @return
	 
	 */
	@RequestMapping(value = "/getCurrencyExchangeDetail", method = RequestMethod.POST)
	public ResponseMessage getCurrencyExchangeDetail(HttpServletRequest request, @RequestBody DtoCurrencyExchangeDetail dtoCurrencyExchangeDetail)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoCurrencyExchangeDetail != null)
		{
			CurrencyExchangeDetail currencyExchangeDetail = repositoryCurrencyExchangeDetail.findByCurrencyExchangeHeaderExchangeIdAndIsDeleted(dtoCurrencyExchangeDetail.getExchangeId(),false);
			if(currencyExchangeDetail == null){
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_ID_NOT_FOUND, false));
			} 
			else 
			{
				dtoCurrencyExchangeDetail  = this.serviceCurrencyExchange.getCurrencyExchangeDetailByExchangeId(dtoCurrencyExchangeDetail,currencyExchangeDetail);
					responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoCurrencyExchangeDetail);
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger save Account Type
	 * @param request
	 * @param dtoMainAccountTypes
	 * @return
	 */
	@RequestMapping(value = "/saveAccountType", method = RequestMethod.POST)
	public ResponseMessage saveAccountType(HttpServletRequest request, @RequestBody DtoMainAccountTypes dtoMainAccountTypes)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoMainAccountTypes); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if (dtoMainAccountTypes != null) {
				dtoMainAccountTypes = this.serviceMainAccountTypes.saveAccountType(dtoMainAccountTypes);
				if (dtoMainAccountTypes != null && UtilRandomKey.isNotBlank(dtoMainAccountTypes.getAccountTypeName())) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_SAVE_SUCCESS, false),
							dtoMainAccountTypes);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_SAVE_FAIL, false));
				}
			} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger account Type List
	 * @param request
	 * @param dtoSearchs
	 * @return
	 
	 */
	@RequestMapping(value = "/accountTypeList", method = RequestMethod.POST)
	public ResponseMessage accountTypeList(HttpServletRequest request, @RequestBody DtoSearch dtoSearchs)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoSearchs != null){
			DtoSearch response  = this.serviceMainAccountTypes.accountTypeList(dtoSearchs);
			if (response!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_LIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger save Account Category
	 * @param request
	 * @param dtoAccountCategory
	 * @return
	 */
	@RequestMapping(value = "/saveAccountCategory", method = RequestMethod.POST)
	public ResponseMessage saveAccountCategory(HttpServletRequest request, @RequestBody DtoAccountCategory dtoAccountCategory)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAccountCategory); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoAccountCategory != null){
			if(UtilRandomKey.isNotBlank(dtoAccountCategory.getAccountCategoryDescription()))
			{
				dtoAccountCategory  = this.serviceMainAccountTypes.saveAccountCategory(dtoAccountCategory);
				if (dtoAccountCategory!=null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_CATEGORY_SAVE_SUCCESS, false), dtoAccountCategory);
				} else {
		 			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_CATEGORY_SAVE_FAIL, false), null);
				}
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_CATEGORY_SAVE_FAIL, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger account Category List
	 * @param request
	 * @param dtoSearchs
	 * @return
	 
	 */
	@RequestMapping(value = "/accountCategoryList", method = RequestMethod.POST)
	public ResponseMessage accountCategoryList(HttpServletRequest request, @RequestBody DtoSearch dtoSearchs)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoSearchs != null) {
				DtoSearch response = this.serviceMainAccountTypes.accountCategoryList(dtoSearchs);
				if (response != null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse
							.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_CATEGORY_LIST, false), response);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false),
							null);
				}
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger save Audit Trial Code
	 * @param request
	 * @param dtoAccountCategory
	 * @return
	 
	 */
	@RequestMapping(value = "/saveAuditTrialCode", method = RequestMethod.POST)
	public ResponseMessage saveAuditTrialCode(HttpServletRequest request, @RequestBody DtoAuditTrailCode dtoAuditTrailCode)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAuditTrailCode); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			if(dtoAuditTrailCode != null)
			{
				if(UtilRandomKey.isNotBlank(dtoAuditTrailCode.getSourceCode()))
				{
					dtoAuditTrailCode  = this.serviceMainAccountTypes.saveAuditTrialCode(dtoAuditTrailCode);
					if (dtoAuditTrailCode!=null) 
					{
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.AUDIT_TRAIL_CODE_SAVE_SUCCESS, false), dtoAuditTrailCode);
					}
					else 
					{
			 			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_SAVE_FAIL, false), null);
					}
				}
				else
				{
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_SAVE_FAIL, false), null);
				}
			} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger search Audit Trial Code
	 * @param request
	 * @param dtoSearchs
	 * @return
	 
	 */
	@RequestMapping(value = "/searchAuditTrialCode", method = RequestMethod.POST)
	public ResponseMessage auditTrialCodeList(HttpServletRequest request, @RequestBody DtoSearch dtoSearchs)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoSearchs != null){
			DtoSearch response  = this.serviceMainAccountTypes.searchAuditTrialCode(dtoSearchs);
			if(response.getRecords()!=null)
			{
				@SuppressWarnings("unchecked")
				List<DtoAuditTrailCode> dtoAuditTrailCodeList = (List<DtoAuditTrailCode>) response.getRecords();
				if(dtoAuditTrailCodeList!=null && !dtoAuditTrailCodeList.isEmpty()){
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.AUDIT_TRAIL_CODE_LIST, false), response);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
				}
			}else{
			 
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger update Account Type
	 * @param request
	 * @param dtoMainAccountTypes
	 * @return
	 
	 */
	@RequestMapping(value = "/updateAccountType", method = RequestMethod.POST)
	public ResponseMessage updateAccountType(HttpServletRequest request, @RequestBody DtoMainAccountTypes dtoMainAccountTypes)  
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoMainAccountTypes); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoMainAccountTypes != null)
		{
			if(UtilRandomKey.isNotBlank(dtoMainAccountTypes.getAccountTypeName())){
				dtoMainAccountTypes  = this.serviceMainAccountTypes.updateAccountType(dtoMainAccountTypes);
				if (dtoMainAccountTypes!=null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_UPDATE_SUCCESS, false), dtoMainAccountTypes);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_UPDATE_FAIL, false), null);
				}
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_UPDATE_FAIL, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Account Type By Id
	 * @param request
	 * @param dtoSearchs
	 * @return
	 
	 */
	@RequestMapping(value = "/getAccountTypeById", method = RequestMethod.POST)
	public ResponseMessage getAccountTypeById(HttpServletRequest request, @RequestBody DtoMainAccountTypes dtoMainAccountTypes)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoMainAccountTypes != null){
			DtoMainAccountTypes response  = this.serviceMainAccountTypes.getAccountTypeById(dtoMainAccountTypes);
			if (response!=null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger update Account Category
	 * @param request
	 * @param dtoAccountCategory
	 * @return
	 
	 */
	@RequestMapping(value = "/updateAccountCategory", method = RequestMethod.POST)
	public ResponseMessage updateAccountCategory(HttpServletRequest request, @RequestBody DtoAccountCategory dtoAccountCategory)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAccountCategory); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoAccountCategory != null){
			if(UtilRandomKey.isNotBlank(dtoAccountCategory.getAccountCategoryDescription())){
				GLConfigurationAccountCategories glConfigurationAccountCategories = repositoryGLConfigurationAccountCategories.findByAccountCategoryIdAndIsDeleted(dtoAccountCategory.getAccountCategoryId(),false);
				if(glConfigurationAccountCategories!=null)
				{
					dtoAccountCategory  = this.serviceMainAccountTypes.updateAccountCategory(dtoAccountCategory,glConfigurationAccountCategories);
					if (dtoAccountCategory!=null) {
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_CATEGORY_UPDATE_SUCCESS, false), dtoAccountCategory);
					} else {
			 			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_UPDATE_FAIL, false), null);
					}
				}
				else{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
				
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_TYPE_UPDATE_FAIL, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Account Category By Id
	 * @param request
	 * @param dtoSearchs
	 * @return
	 
	 */
	@RequestMapping(value = "/getAccountCategoryById", method = RequestMethod.POST)
	public ResponseMessage getAccountCategoryById(HttpServletRequest request, @RequestBody DtoAccountCategory dtoAccountCategory)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoAccountCategory != null){
			DtoAccountCategory response  = this.serviceMainAccountTypes.getAccountCategoryById(dtoAccountCategory);
			if (response!=null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @param request
	 * @param dtoAccountCategory
	 * @return
	 
	 */
	@RequestMapping(value = "/updateAuditTrialCode", method = RequestMethod.POST)
	public ResponseMessage updateAuditTrialCode(HttpServletRequest request, @RequestBody DtoAuditTrailCode dtoAuditTrailCode)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAuditTrailCode); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoAuditTrailCode != null){
			if(UtilRandomKey.isNotBlank(dtoAuditTrailCode.getSourceCode())){
				dtoAuditTrailCode = this.serviceMainAccountTypes.updateAuditTrialCode(dtoAuditTrailCode);
				if (dtoAuditTrailCode!=null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.AUDIT_TRAIL_CODE_UPDATE_SUCCESS, false), dtoAuditTrailCode);
				} else {
		 			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.AUDIT_TRAIL_CODE_UPDATE_FAIL, false), null);
				}
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.AUDIT_TRAIL_CODE_UPDATE_FAIL, false), null);
			}
			
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Audit Trial Code By Id
	 * @param request
	 * @param dtoSearchs
	 * @return
	 
	 */
	@RequestMapping(value = "/getAuditTrialCodeById", method = RequestMethod.POST)
	public ResponseMessage getAuditTrialCodeById(HttpServletRequest request, @RequestBody DtoAuditTrailCode dtoAuditTrailCode)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoAuditTrailCode != null){
			DtoAuditTrailCode response  = this.serviceMainAccountTypes.getAuditTrialCodeById(dtoAuditTrailCode);
			if (response!=null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger update Currency Exchange Detail
	 * @param request
	 * @param dtoCurrencyExchangeDetail
	 * @return
	 
	 */
	@RequestMapping(value = "/updateCurrencyExchangeDetail", method = RequestMethod.POST)
	public ResponseMessage updateCurrencyExchangeDetail(HttpServletRequest request, @RequestBody DtoCurrencyExchangeDetail dtoCurrencyExchangeDetail)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCurrencyExchangeDetail); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoCurrencyExchangeDetail != null){
			CurrencyExchangeDetail currencyExchangeDetail = repositoryCurrencyExchangeDetail.findByCurrencyExchangeHeaderExchangeIdAndIsDeleted(dtoCurrencyExchangeDetail.getExchangeId(),false);
			if(currencyExchangeDetail == null){
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.EXCHANGE_ID_NOT_FOUND, false));
			} 
			else 
			{
				Boolean result  = this.serviceCurrencyExchange.updateCurrencyExchangeDetail(dtoCurrencyExchangeDetail);
				if (result) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.UPDATE_CURRENCY_EXCHANGE_DETAIL_SUCCESS, false));
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.UPDATE_CURRENCY_EXCHANGE_DETAIL_FAIL, false));
				}
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @param request
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/searchCurrencyExchangeDetail", method = RequestMethod.POST)
	public ResponseMessage searchCurrencyExchangeDetail(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage=null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = this.serviceCurrencyExchange.searchCurrencyExchangeDetail(dtoSearch);
		if (dtoSearch != null && dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoCurrencyExchangeDetail> list = (List<DtoCurrencyExchangeDetail>) dtoSearch.getRecords();
			if(!list.isEmpty()){
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger general Ledger Setup
	 * @param dtoGeneralLedgerSetup
	 * @return
	 
	 */
	@RequestMapping(value = "/generalLedgerSetup", method = RequestMethod.POST)
	public ResponseMessage createGeneralLedgerSetup(@RequestBody DtoGeneralLedgerSetup dtoGeneralLedgerSetup)  {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoGeneralLedgerSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		GLConfigurationSetup glConfigurationSetup;
		List<GLConfigurationSetup> glConfigurationSetupList = repositoryGLConfigurationSetup.findAll();
		if(!glConfigurationSetupList.isEmpty())
		{
				glConfigurationSetup = repositoryGLConfigurationSetup.findAll().get(0);
				if(glConfigurationSetup != null)
				{
					dtoGeneralLedgerSetup = serviceGeneralLedgerSetup.saveGeneralLedgerSetup(dtoGeneralLedgerSetup, glConfigurationSetup);
					if (dtoGeneralLedgerSetup != null) {
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
								this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GENERAL_LEDGER_SETUP_UPDATED_SUCCESSFULLY, false),
								dtoGeneralLedgerSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
								this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GENERAL_LEDGER_SETUP_FAIL, false));
					}
				}
				else
				{
					dtoGeneralLedgerSetup = serviceGeneralLedgerSetup.saveGeneralLedgerSetup(dtoGeneralLedgerSetup, new GLConfigurationSetup());
					if (dtoGeneralLedgerSetup != null) {
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
								this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GENERAL_LEDGER_SETUP_SUCCESSFULLY, false),
								dtoGeneralLedgerSetup);
					} else {
						responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
								this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GENERAL_LEDGER_SETUP_FAIL, false));
					}
				}
		}
		else
		{
			dtoGeneralLedgerSetup = serviceGeneralLedgerSetup.saveGeneralLedgerSetup(dtoGeneralLedgerSetup, new GLConfigurationSetup());
			if (dtoGeneralLedgerSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GENERAL_LEDGER_SETUP_SUCCESSFULLY, false),
						dtoGeneralLedgerSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GENERAL_LEDGER_SETUP_FAIL, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get general Ledger Setup
	 * @return
	 
	 */
	@RequestMapping(value = "/generalLedgerSetup/get", method = RequestMethod.GET)
	public ResponseMessage getGeneralLedgerSetup()  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<GLConfigurationSetup> glConfigurationSetupList = repositoryGLConfigurationSetup.findAll();
		if(!glConfigurationSetupList.isEmpty()){
			GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findAll().get(0);
			if (glConfigurationSetup != null) {
				DtoGeneralLedgerSetup response = serviceGeneralLedgerSetup.getGeneralLedgerSetup();
				if (response != null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.GENERAL_LEDGER_SETUP_FETCHED_SUCCESSFULLY, false),
							response);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			}
		}else{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger display type
	 * @return
	 
	 */
	@RequestMapping(value = "/generalLedgerSetup/displayType", method = RequestMethod.GET)
	public ResponseMessage getDisplayType()  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoGLDisplaytype> listObj = serviceGeneralLedgerSetup.getAllDisplayStatus();
		if(!listObj.isEmpty()){
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse
					.getMessageByShortAndIsDeleted(MessageLabel.GENERAL_LEDGER_DISPLAY_STATUS_SUCCESSFULLY, false), listObj);
		}else{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger setup fiscal Finance Period
	 * @param dtoFiscalFinancialPeriodSetup
	 * @return
	 */
	@RequestMapping(value = "/fiscalFinancePeriod/setup", method = RequestMethod.POST)
	public ResponseMessage createFiscalFinancePeriodSetup(
			@RequestBody DtoFiscalFinancialPeriodSetup dtoFiscalFinancialPeriodSetup)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoFiscalFinancialPeriodSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
	 
		DtoFiscalFinancialPeriodSetup listObj = serviceGeneralLedgerSetup.saveAndUpdateFiscalFinancePeriodSetup(
				dtoFiscalFinancialPeriodSetup/*, glConfigurationDefinePeriodsHeader*/);

		responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
				this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FISCAL_FINANCE_PERIOD_CREATED_SUCCESSFULLY, false),
				listObj);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = "/fiscalFinancePeriod/get", method = RequestMethod.GET)
	public ResponseMessage getFiscalFinancePeriodSetupYear()  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.findAll().get(0);
		if(glConfigurationDefinePeriodsHeader != null){
			DtoFiscalFinancialPeriodSetup listObj = serviceGeneralLedgerSetup.getFiscalFinancePeriodSetup();
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse
					.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), listObj);
		}
		else{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/fiscalFinancePeriod/getByYear", method = RequestMethod.POST)
	public ResponseMessage getFiscalFinancePeriodSetupByYear(@RequestBody DtoFiscalFinancialPeriodSetup dtoFiscalFinancialPeriodSetup)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.
				findByYearAndIsDeleted(dtoFiscalFinancialPeriodSetup.getYear(), false);
		if(glConfigurationDefinePeriodsHeader != null){
			DtoFiscalFinancialPeriodSetup listObj = serviceGeneralLedgerSetup.getFiscalFinancePeriodSetupByYear(glConfigurationDefinePeriodsHeader);
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse
					.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), listObj);
		}
		else{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}


	/**
	 * @description this service will use to setup general Ledger save Check Book Maintenance
	 * @param dtoCheckBookMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/saveCheckBookMaintenance", method = RequestMethod.POST)
	public ResponseMessage saveCheckBookMaintenance(@RequestBody DtoCheckBookMaintenance dtoCheckBookMaintenance)  {
		ResponseMessage responseMessage=null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCheckBookMaintenance); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoCheckBookMaintenance!=null)
		{
			CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoCheckBookMaintenance.getCheckBookId(),false);
			if(checkbookMaintenance==null)
			{
				    dtoCheckBookMaintenance=serviceGeneralLedgerSetup.saveCheckBookMaintenance(dtoCheckBookMaintenance);
					if(dtoCheckBookMaintenance.getMessageType()==null){
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CHECK_BOOK_MAINTENANCE_SAVE_SUCCESSFULLY, false),dtoCheckBookMaintenance);
					}
					else
					{
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(dtoCheckBookMaintenance.getMessageType(), false));
					}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CHECK_BOOK_ID_ALREADY_USED, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @param dtoCheckBookMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/getCheckBookMaintenanceByCheckbookId", method = RequestMethod.POST)
	public ResponseMessage getCheckBookMaintenanceByCheckbookId(@RequestBody DtoCheckBookMaintenance dtoCheckBookMaintenance)  {
		ResponseMessage responseMessage=null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoCheckBookMaintenance!=null)
		{
			CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoCheckBookMaintenance.getCheckBookId(),false);
			if(checkbookMaintenance!=null)
			{
				    dtoCheckBookMaintenance=serviceGeneralLedgerSetup.getCheckBookMaintenanceByCheckBookId(dtoCheckBookMaintenance,checkbookMaintenance);
					if(dtoCheckBookMaintenance.getMessageType()==null){
						responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoCheckBookMaintenance);
					}
					else
					{
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(dtoCheckBookMaintenance.getMessageType(), false));
					}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CHECK_BOOK_ID_IS_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger update Check Book Maintenance By Checkbook Id
	 * @param dtoCheckBookMaintenance
	 * @return
	 
	 */
	@RequestMapping(value = "/updateCheckBookMaintenanceByCheckbookId", method = RequestMethod.POST)
	public ResponseMessage updateCheckBookMaintenanceByCheckbookId(@RequestBody DtoCheckBookMaintenance dtoCheckBookMaintenance)  {
		ResponseMessage responseMessage=null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoCheckBookMaintenance); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoCheckBookMaintenance!=null)
		{
			CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoCheckBookMaintenance.getCheckBookId(),false);
			if(checkbookMaintenance!=null)
			{
				    dtoCheckBookMaintenance=serviceGeneralLedgerSetup.updateCheckBookMaintenance(dtoCheckBookMaintenance,checkbookMaintenance);
					if(dtoCheckBookMaintenance.getMessageType()==null){
						responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CHECK_BOOK_MAINTENANCE_UPDATE_SUCCESSFULLY, false),dtoCheckBookMaintenance);
					}
					else
					{
						responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(dtoCheckBookMaintenance.getMessageType(), false));
					}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CHECK_BOOK_ID_IS_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger search Check Book Maintenance
	 * @param request
	 * @param dtoSearch
	 * @return
	 
	 */
	@RequestMapping(value = "/searchCheckBookMaintenance", method = RequestMethod.POST)
	public ResponseMessage searchCheckBookMaintenance(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage=null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = this.serviceGeneralLedgerSetup.searchCheckBookMaintenance(dtoSearch);
		if (dtoSearch != null && dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoCheckBookMaintenance> list = (List<DtoCheckBookMaintenance>) dtoSearch.getRecords();
			if(!list.isEmpty()){
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Main Account List
	 * @param request
	 * @return
	 
	 */
	@RequestMapping(value = "/getMainAccountList", method = RequestMethod.GET)
	public ResponseMessage getMainAccountList(HttpServletRequest request) 
	{
		    ResponseMessage responseMessage=null;
		    String flag=serviceHome.checkValidCompanyAccess();
			if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			List<DtoMainAccountSetUp> list = serviceGeneralLedgerSetup.getMainAccountList();
			if(!list.isEmpty()){
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), list);
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
						serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
			}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Financial Dimensions Values By Financial Dimension Id
	 * @param request
	 * @param dtoFinancialDimensionValue
	 * @return
	 
	 */
	@RequestMapping(value = "/getFinancialDimensionsValuesByFinancialDimensionId", method = RequestMethod.POST)
	public ResponseMessage getFinancialDimensionsValuesByFinancialDimensionId(HttpServletRequest request , @RequestBody DtoFinancialDimensionValue dtoFinancialDimensionValue) 
	{
		    ResponseMessage responseMessage=null;
		    String flag=serviceHome.checkValidCompanyAccess();
			if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			List<DtoFinancialDimensionValue> list = serviceGeneralLedgerSetup.getFinancialDimensionsValuesByFinancialDimensionId(dtoFinancialDimensionValue);
			if(!list.isEmpty()){
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), list);
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
						serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
			}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Financial
	 *              Dimensions List
	 * @param request
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getFinancialDimensionsList", method = RequestMethod.GET)
	public ResponseMessage getFinancialDimensionsList(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<DtoFinancialDimensionSetUp> list = serviceGeneralLedgerSetup.getFinancialDimensionsList();
			if (!list.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						list);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger save Account
	 *              Structure Setup
	 * @param dtoObj
	 * @return
	 * @
	 */
	@RequestMapping(value = "/saveAccountStructureSetup", method = RequestMethod.POST)
	public ResponseMessage saveAccountStructureSetup(
			@RequestBody DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dtoObj)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoObj); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			if (dtoObj != null) {
				dtoObj = serviceGeneralLedgerSetup.saveGlDimAndMain(dtoObj);
				if (dtoObj.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(
									MessageLabel.ACCOUNT_STRUCTURE_SETUP_SAVE_SUCCESSFULLY, false),
							dtoObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(dtoObj.getMessageType(), false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.REQUEST_BODY_IS_EMPTY, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Account
	 *              Structure Setup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getAccountStructureSetup", method = RequestMethod.GET)
	public ResponseMessage updateAccountStructureSetup()  {
		ResponseMessage responseMessage = null;

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dtoObj = serviceGeneralLedgerSetup
					.getGlDimAndMainList();
			if (dtoObj != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						dtoObj);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use to setup general Ledger get Audit
	 *              Trial Series Type Status
	 * @param request
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getAuditTrialSeriesTypeStatus", method = RequestMethod.GET)
	public ResponseMessage getAuditTrialSeriesTypeStatus(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<DtoStatusType> response = this.serviceMainAccountTypes.getAuditTrialSeriesTypeStatus();
			if (response != null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use for get master series of
	 *              MassCloseFiscalPeriodSetup
	 * @param request,
	 *            dtoFiscalFinancialPeriodSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/massCloseFiscalPeriodSetup/getSeries", method = RequestMethod.GET)
	public ResponseMessage getSeries(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<DtoStatusType> response = this.serviceGeneralLedgerSetup.getSeries();
			if (response != null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use for get MassCloseFiscalPeriodSetup
	 *              years
	 * @param request,
	 *            dtoFiscalFinancialPeriodSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/massCloseFiscalPeriodSetup/getYears", method = RequestMethod.GET)
	public ResponseMessage getYears(HttpServletRequest request)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<Integer> response = this.serviceGeneralLedgerSetup.getYears();
			if (response != null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use for get MassCloseFiscalPeriodSetup
	 *              periods
	 * @param request,
	 *            dtoFiscalFinancialPeriodSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/massCloseFiscalPeriodSetup/getMaxPeriod", method = RequestMethod.POST)
	public ResponseMessage getMaxPeriod(HttpServletRequest request,
			@RequestBody DtoFiscalFinancialPeriodSetup dtoFiscalFinancialPeriodSetup)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<DtoPeriods> response = this.serviceGeneralLedgerSetup.getMaxPeriod(dtoFiscalFinancialPeriodSetup);
			if (response != null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use for get MassCloseFiscalPeriodSetup
	 * @param request,
	 *            dtoMassCloseFiscalPeriodSetupRequest
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getMassCloseFiscalPeriodSetup", method = RequestMethod.POST)
	public ResponseMessage getMassCloseFiscalPeriodSetup(HttpServletRequest request,
			@RequestBody DtoMassCloseFiscalPeriodSetupRequest dtoMassCloseFiscalPeriodSetupRequest)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<DtoMassCloseFiscalPeriodSetup> response = this.serviceGeneralLedgerSetup
					.getMassCloseFiscalPeriodSetup(dtoMassCloseFiscalPeriodSetupRequest);
			if (response != null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}

	/**
	 * @description this service will use for save MassCloseFiscalPeriodSetup
	 * @param request,
	 *            dtoMassCloseFiscalPeriodSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/massCloseFiscalPeriodSetup", method = RequestMethod.POST)
	public ResponseMessage massCloseFiscalPeriodSetup(HttpServletRequest request,
			@RequestBody DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup)  {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoMassCloseFiscalPeriodSetup); 

				String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			DtoMassCloseFiscalPeriodSetup response = this.serviceGeneralLedgerSetup
					.saveMassCloseFiscalPeriodSetup(dtoMassCloseFiscalPeriodSetup);
			if (response != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(
								MessageLabel.MASS_CLOSE_FISCAL_PERIOD_SETUP_SAVED_SUCCESSFULLY, false),
						response);
			} else {
				responseMessage = new ResponseMessage(
						HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, this.serviceResponse
								.getMessageByShortAndIsDeleted(MessageLabel.MASS_CLOSE_FISCAL_PERIOD_SETUP_FAIL, false),
						null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	/**
	 * @description this service will use for get master series of
	 *              MassCloseFiscalPeriodSetup
	 * @param request,
	 *            dtoFiscalFinancialPeriodSetup
	 * @return
	 * @
	 */
	@RequestMapping(value = "/massCloseFiscalPeriodSetup/getOrigin", method = RequestMethod.POST)
	public ResponseMessage getOrigin(HttpServletRequest request,
			@RequestBody DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup)  {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			List<DtoOrigin> response = this.serviceGeneralLedgerSetup.getOrigin(dtoMassCloseFiscalPeriodSetup);
			if (response != null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
						response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/searchActiveCheckBookMaintenance", method = RequestMethod.POST)
	public ResponseMessage searchActiveCheckBookMaintenance(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage=null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = this.serviceGeneralLedgerSetup.searchActiveCheckBookMaintenance(dtoSearch);
		if (dtoSearch != null && dtoSearch.getRecords() != null) 
		{
			@SuppressWarnings("unchecked")
			List<DtoCheckBookMaintenance> list = (List<DtoCheckBookMaintenance>) dtoSearch.getRecords();
			if(!list.isEmpty()){
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} 
		else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getActiveCurrencySetup", method = RequestMethod.POST)
	public ResponseMessage getActiveCurrencySetup(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage=null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
		    dtoSearch = this.serviceCurrencySetup.getActiveCurrencySetup(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) 
			{
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.CURRENCY_SETUP_LIST_SUCCESS, false), dtoSearch);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to setup general Ledger get Account Category By Name
	 * @param request
	 * @param dtoSearchs
	 * @return
	 
	 */
	@RequestMapping(value = "/getAccountCategoryByName", method = RequestMethod.POST)
	public ResponseMessage getAccountCategoryByName(HttpServletRequest request, @RequestBody DtoAccountCategory dtoAccountCategory)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoAccountCategory != null){
			DtoAccountCategory response  = this.serviceMainAccountTypes.getAccountCategoryByName(dtoAccountCategory);
			if (response!=null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to setup general Ledger get Account Type By Name
	 * @param request
	 * @param dtoSearchs
	 * @return
	 
	 */
	@RequestMapping(value = "/getAccountTypeByName", method = RequestMethod.POST)
	public ResponseMessage getAccountTypeByName(HttpServletRequest request, @RequestBody DtoMainAccountTypes dtoMainAccountTypes)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoMainAccountTypes != null){
			DtoMainAccountTypes response  = this.serviceMainAccountTypes.getAccountTypeByName(dtoMainAccountTypes);
			if (response!=null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		} 
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/checkTransactionDateIsValid", method = RequestMethod.POST)
	public ResponseMessage checkTransactionDateIsValid(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  {
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			boolean response  = this.serviceGeneralLedgerSetup.checkTransactionDateIsValid(dtoJournalEntryHeader);
			if (response) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_ACCEPTABLE.value(), HttpStatus.NOT_ACCEPTABLE, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INVALID_TRANSACTION_DATE, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

}
