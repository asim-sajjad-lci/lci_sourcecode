/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.CommonConstant;
import com.bti.constant.MessageLabel;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.JournalEntryHeader;
import com.bti.model.dto.DtoCurrencyExchangeTableSetupHeader;
import com.bti.model.dto.DtoGeneralLedgerSetup;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoTypes;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryEntityManager;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.service.ServiceCurrencyExchange;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceGLJournalEntry;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: Controller General Ledger Transaction
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/generalLedger")
public class ControllerGLJournalEntry {

	private static final Logger LOGGER = Logger.getLogger(ControllerGLJournalEntry.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	
	@Autowired
	ServiceCurrencyExchange serviceCurrencyExchange;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	ServiceGLJournalEntry serviceGLJournalEntry;
	
	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;
	
	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;
	
	@Autowired
	RepositoryEntityManager repositoryEntityManager;
	
	/**
	 * @description this service will use to get currency exchange by currecy Id
	 * @param request
	 * @param dtoCurrencySetup
	 * @return
	 
	 */
	
	@RequestMapping(value = "/getExchangeTableSetupByCurrencyId", method = RequestMethod.POST)
	public ResponseMessage getExchangeTableSetupByCurrencyId(HttpServletRequest request, @RequestBody DtoSearch dtoSearch)  
	{
		
		LOGGER.info("inside getExchangeTableSetupByCurrencyId method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
				Integer count = this.repositoryCurrencyExchangeHeader.getCountByCurrencySetupCurrencyIdAndIsDeleted(dtoSearch.getCurrencyId());
				if(count!= null && count>0)
				{
					dtoSearch= serviceCurrencyExchange.getCurrencyExchangeHeaderSetupsByCurrencyId(dtoSearch);
					@SuppressWarnings("unchecked")
					List<DtoCurrencyExchangeTableSetupHeader> list = (List<DtoCurrencyExchangeTableSetupHeader>) dtoSearch.getRecords();
					if(!list.isEmpty()){
						dtoSearch.setTotalCount(count);
						responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoSearch);
					   return responseMessage;
					}
				} 
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));	
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to save journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/saveJournalEntry", method = RequestMethod.POST)
	public ResponseMessage saveJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			 JournalEntryHeader journalEntryHeader = repositoryJournalEntryHeader.findByJournalIDAndIsDeleted(dtoJournalEntryHeader.getJournalID(), false);
			 if(journalEntryHeader==null)
			 {
				 dtoJournalEntryHeader=serviceGLJournalEntry.saveAndUpdateJournalEntry(dtoJournalEntryHeader,CommonConstant.SAVE);
				 if(dtoJournalEntryHeader!=null && dtoJournalEntryHeader.getMessageType()==null)
				 {
					 responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, 
							 this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),dtoJournalEntryHeader);
				 }
				 else
				 {
					 responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR, false)); 
				 }
			 }
			 else{
				 responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST,false));  
			 }
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}

		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to update journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/updateJournalEntry", method = RequestMethod.POST)
	public ResponseMessage updateJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			 dtoJournalEntryHeader=serviceGLJournalEntry.saveAndUpdateJournalEntry(dtoJournalEntryHeader,CommonConstant.UPDATE);
			 if(dtoJournalEntryHeader.getMessageType()==null)
			 {
				 responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, 
						 this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_UPDATED_SUCCESSFULLY, false),dtoJournalEntryHeader);
			 }
			 else
			 {
				 responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(dtoJournalEntryHeader.getMessageType(), false)); 
			 }
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to post journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/postJournalEntry", method = RequestMethod.POST)
	public ResponseMessage postJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader) 
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<GLYTDOpenTransactions> glytdOpenTransactionsList = this.repositoryGlytdOpenTransaction.findByJournalEntryIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(dtoJournalEntryHeader.getJournalID(),dtoJournalEntryHeader.getSourceDocumentId(), false);
			//List<Object[]> glytdOpenTransactionsList = this.repositoryEntityManager.getGlTransactionByJORNIDAndSerialCodeId(dtoJournalEntryHeader.getJournalID(), dtoJournalEntryHeader.getSourceDocumentId());
			if(glytdOpenTransactionsList!=null && !glytdOpenTransactionsList.isEmpty())
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
				return responseMessage;
			} 
			dtoJournalEntryHeader.setCorrection(false);
			dtoJournalEntryHeader=serviceGLJournalEntry.postOrCorrectionJournalEntry(dtoJournalEntryHeader);
			if(dtoJournalEntryHeader!=null){
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, 
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_POST_SUCCESSFULLY, false),dtoJournalEntryHeader);
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INTERNAL_SERVER_ERROR, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to get journal Id List
	 * @param request
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getJournalIdList", method = RequestMethod.GET)
	public ResponseMessage getJournalIdList(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoJournalEntryHeader> list = serviceGLJournalEntry.getJournalIdListForUpdateJvEntry();
			if(list!=null && !list.isEmpty()){
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),list);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to get journal entry by journal Id
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getJournalEntryByJournalId", method = RequestMethod.POST)
	public ResponseMessage getJournalEntryByJournalId(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			JournalEntryHeader journalEntryHeader = this.repositoryJournalEntryHeader.findByJournalIDAndIsDeleted(dtoJournalEntryHeader.getJournalID(),false);
			if(journalEntryHeader!=null)
			{
				dtoJournalEntryHeader= serviceGLJournalEntry.getJournalEntryByJournalId(journalEntryHeader);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoJournalEntryHeader);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to get journal entry by journal Id
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getJournalEntryByJournalIdAndAudit", method = RequestMethod.POST)
	public ResponseMessage getJournalEntryByJournalIdAndAudit(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			JournalEntryHeader journalEntryHeader = this.repositoryJournalEntryHeader.findByJournalIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(dtoJournalEntryHeader.getJournalID(), dtoJournalEntryHeader.getSourceDocumentId(), false);
			if(journalEntryHeader!=null)
			{
				dtoJournalEntryHeader= serviceGLJournalEntry.getJournalEntryByJournalIdAndAuditTrial(journalEntryHeader);
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoJournalEntryHeader);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to get journal entry by year
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getJournalEntryByYear", method = RequestMethod.POST)
	public ResponseMessage getJournalEntryByYearForCopyJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoJournalEntryHeader> journalIdList= serviceGLJournalEntry.getJournalEntryIdByYearForCopyJournaEntry(dtoJournalEntryHeader.getYear());
			if(journalIdList!=null && !journalIdList.isEmpty())
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),journalIdList);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to copy journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/copyJournalEntry", method = RequestMethod.POST)
	public ResponseMessage copyJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			dtoJournalEntryHeader= serviceGLJournalEntry.getCopyJournalEntryDetail(dtoJournalEntryHeader);
			if(dtoJournalEntryHeader!=null)
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoJournalEntryHeader);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	 
	/**
	 * @description this service will use to get journal entry Id by year
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getCorrectJournalEntryIdByYear", method = RequestMethod.POST)
	public ResponseMessage getCorrectJournalEntryIdByYear(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoJournalEntryHeader> journalIdList= serviceGLJournalEntry.getCorrectJournalEntryIdByYear(dtoJournalEntryHeader.getYear());
			if(journalIdList!=null && !journalIdList.isEmpty())
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),journalIdList);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description this service will use to correct journal entry
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/correctJournalEntry", method = RequestMethod.POST)
	public ResponseMessage correctJournalEntry(HttpServletRequest request, @RequestBody DtoJournalEntryHeader dtoJournalEntryHeader)  
	{
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 

		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			dtoJournalEntryHeader= serviceGLJournalEntry.getCorrectJournalEntryDetail(dtoJournalEntryHeader);
			if(dtoJournalEntryHeader!=null)
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoJournalEntryHeader);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to get action for correct journal entry
	 * @param request
	 * @return
	 * @
	 */
	@RequestMapping(value = "/correctJournalEntry/action", method = RequestMethod.GET)
	public ResponseMessage correctJournalEntryAction(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoTypes> actionList= serviceGLJournalEntry.getCorrectJournalEntryActions();
			if(actionList!=null && !actionList.isEmpty())
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),actionList);
				return responseMessage;
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	
	
	
	 
	
	 
	/**
	 * @description this service will use to get next journal entry
	 * @param request
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getGLNextJournalEntry", method = RequestMethod.GET)
	public ResponseMessage getGLNextJournalEntry(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			DtoGeneralLedgerSetup dtoGeneralLedgerSetup = serviceGLJournalEntry.getNextJournalEntry();
			if (dtoGeneralLedgerSetup!=null) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoGeneralLedgerSetup);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
 
	/**
	 * @description this service will use to delete CashReceiptEntry 
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/deleteJournalEntry", method = RequestMethod.POST)
	public ResponseMessage deleteJournalEntry(HttpServletRequest request,
			@RequestBody DtoJournalEntryHeader dtoJournalEntryHeader) {
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceGLJournalEntry.deleteJournalEntry(dtoJournalEntryHeader);
			if (status) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description this service will use to delete CashReceiptEntry 
	 * @param request
	 * @param dtoJournalEntryHeader
	 * @return
	 * @
	 */
	@RequestMapping(value = "/deleteJournalEntries", method = RequestMethod.POST)
	public ResponseMessage deleteJournalEntries(HttpServletRequest request,
			@RequestBody DtoJournalEntryHeader dtoJournalEntryHeader) {
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoJournalEntryHeader); 

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean status = serviceGLJournalEntry.deleteJournalEntries(dtoJournalEntryHeader.getIds());
			if (status) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_DELETED_SUCCESSFULLY, false));
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getAllJournalHeader", method = RequestMethod.POST)
	public ResponseMessage getAllJournalEntry(HttpServletRequest request,
			@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;

		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			
			dtoSearch = serviceGLJournalEntry.getAllJournal(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),dtoSearch);
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAllPostedJournalHeader", method = RequestMethod.POST)
	public ResponseMessage getAllPostedJournalEntry(HttpServletRequest request,
			@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			
			dtoSearch = serviceGLJournalEntry.getAllPostedJournal(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),dtoSearch);
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getPostedJournalHeaderById", method = RequestMethod.POST)
	public ResponseMessage getPostedJournalEntryById(HttpServletRequest request,@RequestBody DtoJournalEntryHeader dtoJournalEntryHeader) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			
			dtoJournalEntryHeader = serviceGLJournalEntry.getPostedJournalById(dtoJournalEntryHeader);
			if (dtoJournalEntryHeader != null && !dtoJournalEntryHeader.equals( null)) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),dtoJournalEntryHeader);
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getPostedJournalHeaderByIdAndAudit", method = RequestMethod.POST)
	public ResponseMessage getPostedJournalEntryByIdAndAudit(HttpServletRequest request,@RequestBody DtoJournalEntryHeader dtoJournalEntryHeader) {
		ResponseMessage responseMessage = null;
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			
			dtoJournalEntryHeader = serviceGLJournalEntry.getPostedJournalByIdAndAudit(dtoJournalEntryHeader);
			if (dtoJournalEntryHeader != null && !dtoJournalEntryHeader.equals( null)) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),dtoJournalEntryHeader);
				return responseMessage;
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false),
					false);
		}
		return responseMessage;
	}
	
	 
 
}
