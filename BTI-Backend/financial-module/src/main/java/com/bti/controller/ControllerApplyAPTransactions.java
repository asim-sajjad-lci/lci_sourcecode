/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoAPApplyDocumentsPayments;
import com.bti.model.dto.DtoAPYTDOpenTransactions;
import com.bti.model.dto.DtoARYTDOpenTransactions;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryARBatches;
import com.bti.service.ServiceApplyAPTransactions;
import com.bti.service.ServiceCurrencySetup;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerApplyARTransactions
 * Name of Project: BTI
 * Created on: Dec 01, 2017
 * Modified on: Aug 13, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */

@RestController
@RequestMapping("/transaction/accountPayable")
public class ControllerApplyAPTransactions {

	private static final Logger LOGGER = Logger.getLogger(ControllerApplyAPTransactions.class);

	@Autowired
	ServiceCurrencySetup serviceCurrencySetup;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	ServiceApplyAPTransactions serviceApplyAPTransactions;

	@Autowired
	RepositoryARBatches repositoryARBatches;
 
	/**
	 * @description this service will use for get master document type
	 * @param request, dtoFiscalFinancialPeriodSetup
	 * @return
	 */
	@RequestMapping(value = "/getDocumentType", method = RequestMethod.GET)
	public ResponseMessage getDocumentType(HttpServletRequest request)  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			 List<DtoStatusType> response = this.serviceApplyAPTransactions.getDocumentType();
			if (response!=null && !response.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getOpenTransactionsByVendorId", method = RequestMethod.POST)
	public ResponseMessage getOpenTransactionsByVendorId(HttpServletRequest request, @RequestBody DtoAPYTDOpenTransactions dtoAPYTDOpenTransaction)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			DtoSearch dtoSearch = this.serviceApplyAPTransactions.getOpenTransactionsByVendorId(dtoAPYTDOpenTransaction);
			if (dtoSearch!=null) 
			{
				@SuppressWarnings("unchecked")
				List<DtoAPYTDOpenTransactions> dtoAPYTDOpenTransactionsList= (List<DtoAPYTDOpenTransactions>) dtoSearch.getRecords();
				if(dtoAPYTDOpenTransactionsList==null || dtoAPYTDOpenTransactionsList.isEmpty()){
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
					return responseMessage;
				}
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
				responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
						serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
	     return responseMessage;
	}
	
	@RequestMapping(value = "/getOpenTransactionsByPostingDate", method = RequestMethod.POST)
	public ResponseMessage getOpenTransactionsByPostingDate(HttpServletRequest request, @RequestBody DtoAPYTDOpenTransactions dtoAPYTDOpenTransaction)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			DtoSearch dtoSearch = this.serviceApplyAPTransactions.getOpenTransactionsByPostingDateAndVendorId(dtoAPYTDOpenTransaction);
			if (dtoSearch!=null) 
			{
				@SuppressWarnings("unchecked")
				List<DtoAPYTDOpenTransactions> dtoAPYTDOpenTransactionsList= (List<DtoAPYTDOpenTransactions>) dtoSearch.getRecords();
				if(dtoAPYTDOpenTransactionsList==null || dtoAPYTDOpenTransactionsList.isEmpty()){
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
					return responseMessage;
				}
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
				responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
						serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
	     return responseMessage;
	}
	
	@RequestMapping(value = "/applyTransaction", method = RequestMethod.POST)
	public ResponseMessage applyTransaction(HttpServletRequest request, @RequestBody DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions)
	{
		LOGGER.info("inside applyTransaction method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAPYTDOpenTransactions); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) 
		{
			dtoAPYTDOpenTransactions = serviceApplyAPTransactions.applyTransaction(dtoAPYTDOpenTransactions);
			if (dtoAPYTDOpenTransactions != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_SAVE_SUCCESSFULLY, false),
						dtoAPYTDOpenTransactions);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}

	@RequestMapping(value = "/unApplyTransaction", method = RequestMethod.POST)
	public ResponseMessage unApplyTransaction(HttpServletRequest request, @RequestBody DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions){
		LOGGER.info("inside applyTransaction method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoAPYTDOpenTransactions); 
		
		String flag = serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)) {
			boolean response = serviceApplyAPTransactions.unApplyTransaction(dtoAPYTDOpenTransactions);
			if (response) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_UPDATED_SUCCESSFULLY, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	@RequestMapping(value = "/getDocumentNumberByVendorAndDocType", method = RequestMethod.POST)
	public ResponseMessage getDocumentNumberByVendorAndDocType(HttpServletRequest request, @RequestBody DtoAPYTDOpenTransactions dtApytdOpenTransactions)
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY))
		{
			List<DtoAPYTDOpenTransactions> dtoAPYTDOpenTransactionsList = this.serviceApplyAPTransactions.getDocumentNumberByVendorIdAndDocType(dtApytdOpenTransactions);
			if (dtoAPYTDOpenTransactionsList!=null && !dtoAPYTDOpenTransactionsList.isEmpty()) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoAPYTDOpenTransactionsList);
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getAmountByPaymentNumber", method = RequestMethod.POST)
	public ResponseMessage getAmountByPaymentNumber(HttpServletRequest request, @RequestBody DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions) throws ScriptException  
	{
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
			dtoAPYTDOpenTransactions = this.serviceApplyAPTransactions.getAmountByPaymentNumber(dtoAPYTDOpenTransactions);
			if (dtoAPYTDOpenTransactions!=null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoAPYTDOpenTransactions);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), null);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	
}
