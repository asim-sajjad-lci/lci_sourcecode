/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.PMSetup;
import com.bti.model.dto.DtoCustomerMaintenance;
import com.bti.model.dto.DtoPMDocumentsTypeSetup;
import com.bti.model.dto.DtoPMSetup;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.model.dto.DtoVendorClassSetup;
import com.bti.repository.RepositoryPMSetup;
import com.bti.service.ServiceAccountPayable;
import com.bti.service.ServiceHome;
import com.bti.service.ServiceResponse;
import com.bti.util.RequestResponseLogger;

/**
 * Description: ControllerAccountsPayables
 * Name of Project: BTI
 * Created on: Aug 28, 2017
 * Modified on: Aug 28, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/account/payables")
public class ControllerAccountsPayables {
	private static final Logger LOGGER = Logger.getLogger(ControllerCompanySetup.class);
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryPMSetup repositoryPMSetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @description : method will save or update the account payable setup module
	 * @param request
	 * @param dtoPMSetup
	 * @return
	 */
	@RequestMapping(value = "/accountPayableSetup", method = RequestMethod.POST)
	public ResponseMessage accountPayableSetup(HttpServletRequest request, @RequestBody DtoPMSetup dtoPMSetup)  {
		LOGGER.info("inside accountPayableSetup method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPMSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if(dtoPMSetup!=null){
			
			dtoPMSetup = serviceAccountPayable.saveAccountPayableSetup(dtoPMSetup);
				if (dtoPMSetup!=null) {
					if(dtoPMSetup.getMessageType()==null){
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_PAYABLE_SETUP_SUCCESS, false), dtoPMSetup);
					}else{
						responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
								serviceResponse.getMessageByShortAndIsDeleted(dtoPMSetup.getMessageType(), false));
					}
					
				}else{ 
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_PAYABLE_SETUP_FAIL, false));
				}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get the account payable setup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAccountPayableSetup", method = RequestMethod.GET)
	public ResponseMessage getAccountPayableSetup(HttpServletRequest request)  
	{
		LOGGER.info("inside accountPayableSetupGetById method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<PMSetup> list = repositoryPMSetup.findAll();
		if(list!=null && !list.isEmpty()){
			DtoPMSetup dtoPMSetup = serviceAccountPayable.getAccountPayableSetup(list.get(0));
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoPMSetup);
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will return the master check format type 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getCheckFormatTypeStatus", method = RequestMethod.GET)
	public ResponseMessage getMaintenanceActiveTypeStatus(HttpServletRequest request)  {
		LOGGER.info("inside getCheckFormatTypeStatus method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		 
			List<DtoStatusType> response = serviceAccountPayable.getCheckFormatTypeStatus();
				if (response!=null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), response);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will save the account payable class setup module
	 * @param request
	 * @param dtoVendorClassSetup
	 * @return
	 */
	@RequestMapping(value = "/accountPayableClassSetup", method = RequestMethod.POST)
	public ResponseMessage accountPayableClassSetup(HttpServletRequest request,
			@RequestBody DtoVendorClassSetup dtoVendorClassSetup)  {
		LOGGER.info("inside accountPayableClassSetup method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoVendorClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoVendorClassSetup != null) {

			dtoVendorClassSetup = serviceAccountPayable.saveAccountPayableClassSetup(dtoVendorClassSetup);
			if (dtoVendorClassSetup != null) {
				if (dtoVendorClassSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_PAYABLE_CLASS_SETUP_SUCCESS, false),
							dtoVendorClassSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoVendorClassSetup.getMessageType(), false));
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_PAYABLE_CLASS_SETUP_FAIL, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will return the account payable class setup by Id
	 * @param request
	 * @param dtoVendorClassSetup
	 * @return
	 */
	@RequestMapping(value = "/accountPayableClassSetupGetById", method = RequestMethod.POST)
	public ResponseMessage accountPayableClassSetupGetById(HttpServletRequest request,
			@RequestBody DtoVendorClassSetup dtoVendorClassSetup)  {
		LOGGER.info("inside accountPayableClassSetupGetById method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoVendorClassSetup != null) {
			dtoVendorClassSetup = serviceAccountPayable.accountPayableSetupGetById(dtoVendorClassSetup);
			if (dtoVendorClassSetup != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
						dtoVendorClassSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}

	/**
	 * @description : method will update the account payable class setup module
	 * @param request
	 * @param dtoVendorClassSetup
	 * @return
	 */
	@RequestMapping(value = "/updateAccountPayableClassSetup", method = RequestMethod.POST)
	public ResponseMessage updateAccountPayableClassSetup(HttpServletRequest request,
			@RequestBody DtoVendorClassSetup dtoVendorClassSetup) {
		LOGGER.info("inside updateAccountPayableClassSetup method");
		ResponseMessage responseMessage = null;

		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoVendorClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoVendorClassSetup != null) {
			dtoVendorClassSetup = serviceAccountPayable.updateAccountPayableClassSetup(dtoVendorClassSetup);
			if (dtoVendorClassSetup != null) {
				if (dtoVendorClassSetup.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
							.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_PAYABLE_CLASS_SETUP_UPDATE_SUCCESS, false),
							dtoVendorClassSetup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(dtoVendorClassSetup.getMessageType(), false));
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_PAYABLE_CLASS_SETUP_UPDATE_FAIL,
								false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will search the account payable class setup
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/searchAccountPayableClassSetup", method = RequestMethod.POST)
	public ResponseMessage searchAccountPayableClassSetup(@RequestBody DtoSearch dtoSearch)  {
		LOGGER.info("inside searchAccountPayableClassSetup method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoSearch = serviceAccountPayable.searchAccountPayableClassSetup(dtoSearch);
		if (dtoSearch.getRecords() != null) {
			@SuppressWarnings("unchecked")
			List<DtoCustomerMaintenance> paymentTermSetUpsList = (List<DtoCustomerMaintenance>) dtoSearch.getRecords();
			if (paymentTermSetUpsList != null && !paymentTermSetUpsList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will save or update the account payable option setup module
	 * @param request
	 * @param dtoPMDocumentsTypeSetup
	 * @return
	 */
	@RequestMapping(value = "/accountPayableOptionSetup", method = RequestMethod.POST)
	public ResponseMessage accountPayableOptionSetup(HttpServletRequest request,
			@RequestBody List<DtoPMDocumentsTypeSetup> dtoPMDocumentsTypeSetup)  {
		LOGGER.info("inside accountPayableOptionSetup method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPMDocumentsTypeSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		if (dtoPMDocumentsTypeSetup != null) {
			dtoPMDocumentsTypeSetup = serviceAccountPayable.saveAccountPayableOptionSetup(dtoPMDocumentsTypeSetup);
			if (dtoPMDocumentsTypeSetup != null && !dtoPMDocumentsTypeSetup.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_PAYABLE_OPTION_SETUP_SUCCESS, false),
						dtoPMDocumentsTypeSetup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCOUNT_PAYABLE_OPTION_SETUP_FAIL, false));
			}
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will return the account payable option setup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAccountPayableOptionSetup", method = RequestMethod.GET)
	public ResponseMessage getAccountPayableOptionSetup(HttpServletRequest request)  {
		LOGGER.info("inside getAccountPayableOptionSetup method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		List<DtoPMDocumentsTypeSetup> dtoPMDocumentsTypeSetup = serviceAccountPayable.getAccountPayableOptionSetup();
		if (dtoPMDocumentsTypeSetup != null && !dtoPMDocumentsTypeSetup.isEmpty()) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
					dtoPMDocumentsTypeSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will add account payable class
	 * @param request
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	@RequestMapping(value = "/saveAccountPayableClassSetup", method = RequestMethod.POST)
	public ResponseMessage saveAccountPayableClassSetup(HttpServletRequest request,
			@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup)  {
		LOGGER.info("inside save Account Payable Class Setup method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPayableAccountClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPayableAccountClassSetup = serviceAccountPayable.savePayableAccountClassSetup(dtoPayableAccountClassSetup);
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SAV_ACCOUNT_PAYABLE_CLASS_SETUP, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(dtoPayableAccountClassSetup.getMessageType(), false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get account payable class
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAccountPayableClassSetup", method = RequestMethod.POST)
	public ResponseMessage getAccountPayableClassSetup(HttpServletRequest request, @RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup) {
		LOGGER.info("inside get Account Payable Class Setup method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPayableAccountClassSetup = serviceAccountPayable.getPayableAccountClassSetup(dtoPayableAccountClassSetup.getClassId());
		if (dtoPayableAccountClassSetup != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @description : method will add vendor account maintenance
	 * @param request
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	@RequestMapping(value = "/saveVendorAccountMaintenance", method = RequestMethod.POST)
	public ResponseMessage savevendorAccountMaintenance(HttpServletRequest request,
			@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup) {
		LOGGER.info("inside save vendor Account maintenance  method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPayableAccountClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPayableAccountClassSetup = serviceAccountPayable.saveVendorAccountMaintenance(dtoPayableAccountClassSetup);
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
					.getMessageByShortAndIsDeleted(MessageLabel.VENDOR_ACCOUNT_MAINTENANCE_SAVE_SUCCESSFULLY, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(dtoPayableAccountClassSetup.getMessageType(), false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	
	/**
	 * @description : method will get vendor account maintenance
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getVendorAccountMaintenanceByVendorId", method = RequestMethod.POST)
	public ResponseMessage getVendorAccountMaintenance(HttpServletRequest request, @RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup) {
		LOGGER.info("inside get vendor account maintenance method");
		ResponseMessage responseMessage = null;
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		 dtoPayableAccountClassSetup = serviceAccountPayable.getVendorAccountMaintenanceByVendorId(dtoPayableAccountClassSetup.getVendorId());
		if (dtoPayableAccountClassSetup != null) {
			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		return responseMessage;
	}
	
	/**
	 * @param request
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	@RequestMapping(value = "/createNewGlAccountNumber", method = RequestMethod.POST)
	public ResponseMessage createNewGlAccountNumber(HttpServletRequest request,
			@RequestBody DtoPayableAccountClassSetup dtoPayableAccountClassSetup) {
		LOGGER.info("inside create new account number method");
		ResponseMessage responseMessage = null;
		
		DtoRequestResponseLog dtoRequestResponseLog = serviceHome.logRequest(dtoPayableAccountClassSetup); 
		
		String flag=serviceHome.checkValidCompanyAccess();
		if(flag.equalsIgnoreCase(MessageLabel.RECORD_GET_SUCCESSFULLY)){
		dtoPayableAccountClassSetup = serviceAccountPayable.createNewAccountNumber(dtoPayableAccountClassSetup);
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
					.getMessageByShortAndIsDeleted(MessageLabel.GL_ACCOUNT_NUMBER_CREATED, false),
					dtoPayableAccountClassSetup);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted(dtoPayableAccountClassSetup.getMessageType(), false));
		}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(flag, false), false);
		}
		
		RequestResponseLogger.logResponse(dtoRequestResponseLog, responseMessage);
		return responseMessage;
	}
	 
}
