package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.dto.DtoAuditTrailCode;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.util.mapping.GL.MappingAuditTrialCode;

@Service("serviceAuditTrial")
public class ServiceAuditTrial {

	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	@Autowired
	MappingAuditTrialCode mappingAuditTrialCode;
	
	public List<DtoAuditTrailCode> getAllAuditTrialCode() {
		List<GLConfigurationAuditTrialCodes> list = 
				repositoryGLConfigurationAuditTrialCodes.findAll();
		List<DtoAuditTrailCode> dtoList = new ArrayList<>();
		list.stream().forEach(e -> {
			DtoAuditTrailCode dto = mappingAuditTrialCode.modelAuditToDtoAudit(e);
			dtoList.add(dto);
		});
		return dtoList;
		
	}

	public DtoAuditTrailCode getAuditTrialCodeById(Integer auditId) {
		
		GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = 
				repositoryGLConfigurationAuditTrialCodes.findBySeriesIndexAndIsDeleted(auditId, false);
		
		DtoAuditTrailCode dtoAudit = null;
		
		if(glConfigurationAuditTrialCodes != null) {
			dtoAudit = mappingAuditTrialCode.modelAuditToDtoAudit(glConfigurationAuditTrialCodes);
		}	
		
		return dtoAudit;
	}

	
}
