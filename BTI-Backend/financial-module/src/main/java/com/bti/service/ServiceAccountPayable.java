/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.constant.ConfigSetting;
import com.bti.constant.MessageLabel;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.FAAccountGroupAccountTableSetup;
import com.bti.model.FixedAllocationAccountIndexDetails;
import com.bti.model.FixedAllocationAccountIndexHeader;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.MasterAccountType;
import com.bti.model.MasterFAAccountGroupAccountType;
import com.bti.model.PMDocumentsTypeSetup;
import com.bti.model.PMPeriodSetup;
import com.bti.model.PMSetup;
import com.bti.model.PMSetupFormatType;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.VendorAccountTableSetup;
import com.bti.model.VendorClassAccountTableSetup;
import com.bti.model.VendorClassesSetup;
import com.bti.model.VendorMaintenance;
import com.bti.model.dto.DtoFaAccountGroupSetup;
import com.bti.model.dto.DtoPMDocumentsTypeSetup;
import com.bti.model.dto.DtoPMPeriodSetup;
import com.bti.model.dto.DtoPMSetup;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.model.dto.DtoVendorClassSetup;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryFAAccountGroupAccountTableSetup;
import com.bti.repository.RepositoryFixedAllocationAccountIndexDetail;
import com.bti.repository.RepositoryFixedAllocationAccountIndexHeader;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryMasterAccountType;
import com.bti.repository.RepositoryMasterFAAccountGroupAccountType;
import com.bti.repository.RepositoryPMDocumentsTypeSetup;
import com.bti.repository.RepositoryPMPeriodSetup;
import com.bti.repository.RepositoryPMSetup;
import com.bti.repository.RepositoryPMSetupFormatType;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.repository.RepositoryPaymentTermSetup;
import com.bti.repository.RepositoryShipmentMethodSetup;
import com.bti.repository.RepositoryVATSetup;
import com.bti.repository.RepositoryVendorAccountTableSetup;
import com.bti.repository.RepositoryVendorClassAccountTableSetup;
import com.bti.repository.RepositoryVendorClassesSetup;
import com.bti.repository.RepositoryVendorMaintenance;
import com.bti.util.UtilRandomKey;

@Service("serviceAccountPayable")
public class ServiceAccountPayable {
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	@Autowired
	RepositoryPMSetup repositoryPMSetup;
	@Autowired
	RepositoryPMSetupFormatType repositoryPMSetupFormatType;
	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;
	@Autowired
	RepositoryVendorClassesSetup repositoryVendorClassesSetup;
	@Autowired
	RepositoryVATSetup repositoryVATSetup;
	@Autowired
	RepositoryShipmentMethodSetup repositoryShipmentMethodSetup;
	@Autowired
	RepositoryPaymentTermSetup repositoryPaymentTermSetup;
	@Autowired
	RepositoryVendorClassAccountTableSetup repositoryVendorClassAccountTableSetup;
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	@Autowired
	RepositoryPMPeriodSetup repositoryPMPeriodSetup;
	@Autowired
	RepositoryPMDocumentsTypeSetup repositoryPMDocumentsTypeSetup;
	@Autowired
	RepositoryMasterAccountType repositoryMasterAccountType;
	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	@Autowired
	RepositoryVendorAccountTableSetup repositoryVendorAccountTableSetup;
	@Autowired
	RepositoryVendorMaintenance repositoryVendorMaintenance;
	@Autowired
	RepositoryFixedAllocationAccountIndexDetail repositoryFixedAllocationAccountIndexDetail;
	@Autowired
	RepositoryFixedAllocationAccountIndexHeader repositoryFixedAllocationAccountIndexHeader;
	@Autowired
	RepositoryFAAccountGroupAccountTableSetup repositoryFAAccountGroupAccountTableSetup;
	@Autowired
	RepositoryMasterFAAccountGroupAccountType repositoryMasterFAAccountGroupAccountType;
	@Autowired
	ServiceHome serviceHome;
	
	static Logger log = Logger.getLogger(ServiceAccountPayable.class.getName());

	/**
	 * @param dtoPMSetup
	 * @return
	 */
	public DtoPMSetup saveAccountPayableSetup(DtoPMSetup dtoPMSetup) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			int langId = serviceHome.getLanngugaeId();
			PMSetup pmSetup = null;
			List<PMSetup> list = repositoryPMSetup.findAll();
			if (list != null && !list.isEmpty()) {
				pmSetup = list.get(0);
			} else {
				pmSetup = new PMSetup();
			}
			pmSetup.setAgeingBy(dtoPMSetup.getAgeingBy());

			pmSetup.setAgeUnappliedCreditAmounts(dtoPMSetup.getIsAgeUnappliedCreditAmounts());
			pmSetup.setApplyByDefault(dtoPMSetup.getApplyByDefault());
			pmSetup.setApTrackingDiscountAvailable(dtoPMSetup.getApTrackingDiscountAvailable());
			pmSetup.setCheckbookMaintenance(
					repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoPMSetup.getCheckBookBankId(),false));
					
			pmSetup.setPmSetupFormatType(repositoryPMSetupFormatType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoPMSetup.getCheckFormat(),langId,false));
			pmSetup.setCreatedBy(loggedInUserId);
			pmSetup.setDeleteUnpostedPrintedDocuments(dtoPMSetup.getDeleteUnpostedPrintedDocuments());
			pmSetup.setExceedMaximumInvoiceAmount(dtoPMSetup.getExceedMaximumInvoiceAmount());
			pmSetup.setExceedMaximumWriteoffAmount(dtoPMSetup.getExceedMaximumWriteoffAmount());
			pmSetup.setFreightVatScheduleId(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoPMSetup.getFreightVatScheduleId(),false));
			pmSetup.setMiscVatScheduleId(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoPMSetup.getMiscVatScheduleId(),false));
			pmSetup.setOverrideVoucherNumberTransactionEntry(dtoPMSetup.getOverrideVoucherNumberTransactionEntry());
			pmSetup.setPrintHistoricalAgedTrialBalance(dtoPMSetup.getPrintHistoricalAgedTrialBalance());
			pmSetup.setPurchaseVatScheduleId(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoPMSetup.getPurchaseVatScheduleId(),false));
			pmSetup.setRemoveVendorHoldPassword(dtoPMSetup.getRemoveVendorHoldPassword());
			pmSetup.setUserDefine1(dtoPMSetup.getUserDefine1());
			pmSetup.setUserDefine2(dtoPMSetup.getUserDefine2());
			pmSetup.setUserDefine3(dtoPMSetup.getUserDefine3());
			pmSetup = repositoryPMSetup.saveAndFlush(pmSetup);
			savePMPeriodSetup(dtoPMSetup.getPmPeriodSetupsList(), pmSetup);
			dtoPMSetup.setId(pmSetup.getId());
			return dtoPMSetup;
		} catch (Exception e) {
            log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}
	
	/**
	 * @param dtoPMPeriodSetupsList
	 * @param pmSetup
	 */
	public void savePMPeriodSetup(List<DtoPMPeriodSetup> dtoPMPeriodSetupsList, PMSetup pmSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		repositoryPMPeriodSetup.deleteAll();
		if (dtoPMPeriodSetupsList != null && !dtoPMPeriodSetupsList.isEmpty()) {
			for (DtoPMPeriodSetup dtoRMPeriodSetup : dtoPMPeriodSetupsList) {
				PMPeriodSetup rmPeriodSetup = new PMPeriodSetup();
				rmPeriodSetup.setPeriodDescription(dtoRMPeriodSetup.getPeriodDescription());
				rmPeriodSetup.setPeriodNumberOfDays(dtoRMPeriodSetup.getPeriodNoofDays());
				rmPeriodSetup.setPeriodEnd(dtoRMPeriodSetup.getPeriodEnd());
				rmPeriodSetup.setCreatedBy(loggedInUserId);
				rmPeriodSetup.setPmSetup(pmSetup);
				repositoryPMPeriodSetup.save(rmPeriodSetup);
			}
		}
	}

	/**
	 * @param pmSetup
	 * @return
	 */
	public DtoPMSetup getAccountPayableSetup(PMSetup pmSetup) {
		DtoPMSetup dtoPMSetup = null;
		if (pmSetup != null) {
			dtoPMSetup = new DtoPMSetup(pmSetup);
			dtoPMSetup.setPmPeriodSetupsList(getPmPeriodSetupListByRmSetupId(pmSetup));
		}
		return dtoPMSetup;
	}
	
	/**
	 * @param pmSetup
	 * @return
	 */
	public List<DtoPMPeriodSetup> getPmPeriodSetupListByRmSetupId(PMSetup pmSetup) {
		List<PMPeriodSetup> pmPeriodSetupsList = repositoryPMPeriodSetup.findByPmSetupIdAndIsDeleted(pmSetup.getId(),false);
		List<DtoPMPeriodSetup> list = new ArrayList<>();
		DtoPMPeriodSetup dtoRMPeriodSetup = null;
		if (pmPeriodSetupsList != null && !pmPeriodSetupsList.isEmpty()) {
			for (PMPeriodSetup pmPeriodSetup : pmPeriodSetupsList) {
				dtoRMPeriodSetup = new DtoPMPeriodSetup(pmPeriodSetup);
				list.add(dtoRMPeriodSetup);
			}
		}
		return list;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getCheckFormatTypeStatus() {
		int langId=serviceHome.getLanngugaeId();
		List<PMSetupFormatType> list = repositoryPMSetupFormatType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if (list != null) {
			for (PMSetupFormatType pmSetupFormatType : list) {
				responseList.add(new DtoStatusType(pmSetupFormatType));
			}
		}
		return responseList;
	}

	/**
	 * @param dtoVendorClassSetup
	 * @return
	 */
	public DtoVendorClassSetup saveAccountPayableClassSetup(DtoVendorClassSetup dtoVendorClassSetup) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			VendorClassesSetup vendorClassesSetup = repositoryVendorClassesSetup
					.findByVendorClassIdAndIsDeleted(dtoVendorClassSetup.getVendorClassId(),false);
			if (vendorClassesSetup != null) {
				dtoVendorClassSetup.setMessageType("ACCOUNT_PAYABLE_CLASS_SETUP_ALREADY_EXIST");
				return dtoVendorClassSetup;
			}
			vendorClassesSetup = new VendorClassesSetup();
			vendorClassesSetup.setCreatedBy(loggedInUserId);
			vendorClassesSetup.setVendorClassId(dtoVendorClassSetup.getVendorClassId());
			vendorClassesSetup.setClassDescription(dtoVendorClassSetup.getClassDescription());
			vendorClassesSetup.setClassDescriptionArabic(dtoVendorClassSetup.getClassDescriptionArabic());
			vendorClassesSetup.setCreditLimit(dtoVendorClassSetup.getCreditLimit());
			vendorClassesSetup.setCreditLimitAmount(dtoVendorClassSetup.getCreditLimitAmount());
			vendorClassesSetup
					.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoVendorClassSetup.getCurrencyId(),false));
			vendorClassesSetup.setMaximumInvoiceAmount(dtoVendorClassSetup.getMaximumInvoiceAmount());
			vendorClassesSetup.setMaximumInvoiceAmountValue(dtoVendorClassSetup.getMaximumInvoiceAmountValue());
			vendorClassesSetup.setMinimumCharge(dtoVendorClassSetup.getMinimumCharge());
			vendorClassesSetup.setMinimumChargeAmount(dtoVendorClassSetup.getMinimumChargeAmount());
			vendorClassesSetup.setMinimumOrderAmount(dtoVendorClassSetup.getMinimumOrderAmount());
			vendorClassesSetup
					.setOpenMaintenanceHistoryCalendarYear(dtoVendorClassSetup.getOpenMaintenanceHistoryCalendarYear());
			vendorClassesSetup
					.setOpenMaintenanceHistoryDistribution(dtoVendorClassSetup.getOpenMaintenanceHistoryDistribution());
			vendorClassesSetup
					.setOpenMaintenanceHistoryFiscalYear(dtoVendorClassSetup.getOpenMaintenanceHistoryFiscalYear());
			vendorClassesSetup
					.setOpenMaintenanceHistoryTransaction(dtoVendorClassSetup.getOpenMaintenanceHistoryTransaction());
			vendorClassesSetup.setTradeDiscountPercent(dtoVendorClassSetup.getTradeDiscountPercent());
			vendorClassesSetup.setUserDefine1(dtoVendorClassSetup.getUserDefine1());
			vendorClassesSetup.setUserDefine2(dtoVendorClassSetup.getUserDefine2());
			vendorClassesSetup.setUserDefine3(dtoVendorClassSetup.getUserDefine3());
			vendorClassesSetup
					.setVatSetup(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoVendorClassSetup.getVatScheduleId(),false));
			vendorClassesSetup.setShipmentMethodSetup(
					repositoryShipmentMethodSetup.findByShipmentMethodIdAndIsDeleted(dtoVendorClassSetup.getShipmentMethodId(),false));
			vendorClassesSetup.setPaymentTermsSetup(
					repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(dtoVendorClassSetup.getPaymentTermId(),false));
			vendorClassesSetup.setCheckbookMaintenance(
					repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoVendorClassSetup.getCheckBookId(),false));
			vendorClassesSetup.setRowDateIndex(new Date());
			repositoryVendorClassesSetup.saveAndFlush(vendorClassesSetup);
			return accountPayableSetupGetById(dtoVendorClassSetup);
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoVendorClassSetup
	 * @return
	 */
	public DtoVendorClassSetup accountPayableSetupGetById(DtoVendorClassSetup dtoVendorClassSetup) {
		VendorClassesSetup vendorClassesSetup = repositoryVendorClassesSetup
				.findByVendorClassIdAndIsDeleted(dtoVendorClassSetup.getVendorClassId(),false);
		if (vendorClassesSetup != null) {
			return new DtoVendorClassSetup(vendorClassesSetup);
		}
		return null;
	}

	/**
	 * @param dtoVendorClassSetup
	 * @return
	 */
	public DtoVendorClassSetup updateAccountPayableClassSetup(DtoVendorClassSetup dtoVendorClassSetup) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			VendorClassesSetup vendorClassesSetup = repositoryVendorClassesSetup
					.findByVendorClassIdAndIsDeleted(dtoVendorClassSetup.getVendorClassId(),false);
			if (vendorClassesSetup != null) {
				vendorClassesSetup.setChangeBy(loggedInUserId);
				vendorClassesSetup.setModifyDate(new Date());
				vendorClassesSetup.setClassDescription(dtoVendorClassSetup.getClassDescription());
				vendorClassesSetup.setClassDescriptionArabic(dtoVendorClassSetup.getClassDescriptionArabic());
				vendorClassesSetup.setCreditLimit(dtoVendorClassSetup.getCreditLimit());
				vendorClassesSetup.setCreditLimitAmount(dtoVendorClassSetup.getCreditLimitAmount());
				vendorClassesSetup.setCurrencySetup(
						repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoVendorClassSetup.getCurrencyId(),false));
				vendorClassesSetup.setMaximumInvoiceAmount(dtoVendorClassSetup.getMaximumInvoiceAmount());
				vendorClassesSetup.setMaximumInvoiceAmountValue(dtoVendorClassSetup.getMaximumInvoiceAmountValue());
				vendorClassesSetup.setMinimumCharge(dtoVendorClassSetup.getMinimumCharge());
				vendorClassesSetup.setMinimumChargeAmount(dtoVendorClassSetup.getMinimumChargeAmount());
				vendorClassesSetup.setMinimumOrderAmount(dtoVendorClassSetup.getMinimumOrderAmount());
				vendorClassesSetup.setOpenMaintenanceHistoryCalendarYear(
						dtoVendorClassSetup.getOpenMaintenanceHistoryCalendarYear());
				vendorClassesSetup.setOpenMaintenanceHistoryDistribution(
						dtoVendorClassSetup.getOpenMaintenanceHistoryDistribution());
				vendorClassesSetup
						.setOpenMaintenanceHistoryFiscalYear(dtoVendorClassSetup.getOpenMaintenanceHistoryFiscalYear());
				vendorClassesSetup.setOpenMaintenanceHistoryTransaction(
						dtoVendorClassSetup.getOpenMaintenanceHistoryTransaction());
				vendorClassesSetup.setTradeDiscountPercent(dtoVendorClassSetup.getTradeDiscountPercent());
				vendorClassesSetup.setUserDefine1(dtoVendorClassSetup.getUserDefine1());
				vendorClassesSetup.setUserDefine2(dtoVendorClassSetup.getUserDefine2());
				vendorClassesSetup.setUserDefine3(dtoVendorClassSetup.getUserDefine3());
				vendorClassesSetup
						.setVatSetup(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoVendorClassSetup.getVatScheduleId(),false));
				vendorClassesSetup.setShipmentMethodSetup(repositoryShipmentMethodSetup
						.findByShipmentMethodIdAndIsDeleted(dtoVendorClassSetup.getShipmentMethodId(),false));
				vendorClassesSetup.setPaymentTermsSetup(
						repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(dtoVendorClassSetup.getPaymentTermId(),false));
				vendorClassesSetup.setCheckbookMaintenance(
						repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoVendorClassSetup.getCheckBookId(),false));
				repositoryVendorClassesSetup.saveAndFlush(vendorClassesSetup);

			}
			return accountPayableSetupGetById(dtoVendorClassSetup);
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchAccountPayableClassSetup(DtoSearch dtoSearchs) {
		try {
			DtoSearch dtoSearch = new DtoSearch();
			dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
			dtoSearch.setPageSize(dtoSearchs.getPageSize());
			dtoSearch.setTotalCount(repositoryVendorClassesSetup.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
			List<DtoVendorClassSetup> dtoVendorClassSetupList = new ArrayList<>();
			List<VendorClassesSetup> list = null;
			if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoSearchs.getPageNumber(), dtoSearchs.getPageSize(),
						Direction.DESC, "createdDate");
				list = repositoryVendorClassesSetup.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(),
						pageable);
			} else {

				list = repositoryVendorClassesSetup.predictiveSearch(dtoSearchs.getSearchKeyword());
			}
			if (list != null) {
				for (VendorClassesSetup vendorClassesSetup : list) {
					dtoVendorClassSetupList.add(new DtoVendorClassSetup(vendorClassesSetup));
				}
			}
			dtoSearch.setRecords(dtoVendorClassSetupList);
			return dtoSearch;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @return
	 */
	public List<DtoPMDocumentsTypeSetup> getAccountPayableOptionSetup() {
		List<DtoPMDocumentsTypeSetup> dtoPMDocumentsTypeSetupList = new ArrayList<>();
		try {
			List<PMDocumentsTypeSetup> list = repositoryPMDocumentsTypeSetup.findAll();
			
			for (PMDocumentsTypeSetup pmDocumentsTypeSetup : list) {
				dtoPMDocumentsTypeSetupList.add(new DtoPMDocumentsTypeSetup(pmDocumentsTypeSetup));
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoPMDocumentsTypeSetupList;
	}

	/**
	 * @param dtoPMDocumentsTypeSetupList
	 * @return
	 */
	public List<DtoPMDocumentsTypeSetup> saveAccountPayableOptionSetup(
			List<DtoPMDocumentsTypeSetup> dtoPMDocumentsTypeSetupList) {
		try 
		{
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			for (DtoPMDocumentsTypeSetup dtoPMDocumentsTypeSetup : dtoPMDocumentsTypeSetupList) 
			{
				PMDocumentsTypeSetup pmDocumentsTypeSetup=repositoryPMDocumentsTypeSetup.findByDocumentCodeAndIsDeleted(dtoPMDocumentsTypeSetup.getDocumentCode(), false);
				if(pmDocumentsTypeSetup==null){
					pmDocumentsTypeSetup = new PMDocumentsTypeSetup();
				}
				pmDocumentsTypeSetup.setCreatedBy(loggedInUserId);
				pmDocumentsTypeSetup.setChangeBy(loggedInUserId);
				pmDocumentsTypeSetup.setDocumentCode(dtoPMDocumentsTypeSetup.getDocumentCode());
				pmDocumentsTypeSetup.setDocumentNumberLastNumber(dtoPMDocumentsTypeSetup.getDocumentNumberLastNumber());
				pmDocumentsTypeSetup.setDocumentType(dtoPMDocumentsTypeSetup.getDocumentType());
				pmDocumentsTypeSetup.setDocumentTypeDescription(dtoPMDocumentsTypeSetup.getDocumentTypeDescription());
				pmDocumentsTypeSetup
						.setDocumentTypeDescriptionArabic(dtoPMDocumentsTypeSetup.getDocumentTypeDescriptionArabic());

				pmDocumentsTypeSetup = repositoryPMDocumentsTypeSetup.saveAndFlush(pmDocumentsTypeSetup);
				dtoPMDocumentsTypeSetup.setApDocumentTypeId(pmDocumentsTypeSetup.getApDocumentTypeId());
			}
		

		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoPMDocumentsTypeSetupList;
	}
	
	/**
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	public DtoPayableAccountClassSetup savePayableAccountClassSetup(
			DtoPayableAccountClassSetup dtoPayableAccountClassSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid = serviceHome.getLanngugaeId();
		GLAccountsTableAccumulation glAccountsTableAccumulation = null;
		try 
		{
			repositoryVendorClassAccountTableSetup.deleteRecordById(dtoPayableAccountClassSetup.getClassId(),true);
			if (dtoPayableAccountClassSetup.getAccountNumberList() != null
					&& !dtoPayableAccountClassSetup.getAccountNumberList().isEmpty()) {
				for (DtoPayableAccountClassSetup accountNumber : dtoPayableAccountClassSetup.getAccountNumberList()) {
					VendorClassAccountTableSetup vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup
							.findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(accountNumber.getAccountType(),dtoPayableAccountClassSetup.getClassId());
					if (vendorClassAccountTableSetup == null) {
						vendorClassAccountTableSetup = new VendorClassAccountTableSetup();
					}
					
					vendorClassAccountTableSetup.setDeleted(false);
					vendorClassAccountTableSetup.setVendorClassesSetup(
							repositoryVendorClassesSetup.findByVendorClassIdAndIsDeleted(dtoPayableAccountClassSetup.getClassId(),false));
					vendorClassAccountTableSetup.setMasterAccountType(
							repositoryMasterAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(accountNumber.getAccountType(),langid,false));
					vendorClassAccountTableSetup.setCreatedBy(loggedInUserId);

					StringBuilder sb = new StringBuilder();
					String fullIndex = "";
					if (accountNumber.getAccountNumberIndex() != null
							&& !accountNumber.getAccountNumberIndex().isEmpty()) {
						for (Long i : accountNumber.getAccountNumberIndex()) {
							sb.append(String.valueOf(i));
						}
						fullIndex = sb.toString();
					}
					
					List<GLAccountsTableAccumulation> glAccountsTableAccumulationsList = repositoryGLAccountsTableAccumulation
							.findByAccountNumberFullIndexAndIsDeleted(fullIndex,false);
					if (glAccountsTableAccumulationsList != null && !glAccountsTableAccumulationsList.isEmpty()) 
					{
						glAccountsTableAccumulation = glAccountsTableAccumulationsList.get(0);
						if (UtilRandomKey.isNotBlank(accountNumber.getAccountDescription())) {
							glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
						}
					}
					else 
					{
						glAccountsTableAccumulation = new GLAccountsTableAccumulation();
						int actRowIndexId = 1;
						PayableAccountClassSetup payableAccountClassSetup2 = repositoryPayableAccountClassSetup
								.findFirstByOrderByIdDesc();
						if (payableAccountClassSetup2 != null) {
							actRowIndexId = payableAccountClassSetup2.getAccountRowIndex();
							actRowIndexId = actRowIndexId + 1;
						}
						
						int segmentNumber = 1;
						if (accountNumber.getAccountNumberIndex() != null
								&& !accountNumber.getAccountNumberIndex().isEmpty()) 
						{
							StringBuilder accountNumberfullIndex = new StringBuilder("");
							for (Long indexId : accountNumber.getAccountNumberIndex()) {
								accountNumberfullIndex.append(indexId);
								PayableAccountClassSetup payableAccountClassSetup = new PayableAccountClassSetup();
								payableAccountClassSetup.setIndexId(indexId);
								payableAccountClassSetup.setSegmentNumber(segmentNumber);
								segmentNumber++;
								payableAccountClassSetup.setAccountRowIndex(actRowIndexId);
								payableAccountClassSetup.setCreatedBy(loggedInUserId);
								repositoryPayableAccountClassSetup.save(payableAccountClassSetup);
							}

							glAccountsTableAccumulation.setAccountTableRowIndex(String.valueOf(actRowIndexId));
							glAccountsTableAccumulation.setSegmentNumber(segmentNumber);
							glAccountsTableAccumulation.setAccountNumberFullIndex(accountNumberfullIndex.toString());
							if (UtilRandomKey.isNotBlank(accountNumber.getAccountDescription())) {
								glAccountsTableAccumulation
										.setAccountDescription(accountNumber.getAccountDescription());
							}
							if (accountNumber.getTransactionType() != null) {
								glAccountsTableAccumulation.setTransactionType(accountNumber.getTransactionType());
							}
							glAccountsTableAccumulation.setCreatedBy(loggedInUserId);
						}
					}
					glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
							.saveAndFlush(glAccountsTableAccumulation);
					vendorClassAccountTableSetup.setGlAccountsTableAccumulation(glAccountsTableAccumulation);
					repositoryVendorClassAccountTableSetup.saveAndFlush(vendorClassAccountTableSetup);
				}
			} else {
				dtoPayableAccountClassSetup.setMessageType(MessageLabel.REQUEST_BODY_IS_EMPTY);
			}
		} 
		catch (Exception e) 
		{
			dtoPayableAccountClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			log.info(Arrays.toString(e.getStackTrace()));
		}
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			return getPayableAccountClassSetup(dtoPayableAccountClassSetup.getClassId());
		} else {
			return dtoPayableAccountClassSetup;
		}
	}
	
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public DtoPayableAccountClassSetup getPayableAccountClassSetup(String classId) 
	{
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		DtoPayableAccountClassSetup main = null;
		List<VendorClassAccountTableSetup> vendorList = repositoryVendorClassAccountTableSetup.findByVendorClassesSetupVendorClassIdAndIsDeleted(classId,false);
		List<DtoPayableAccountClassSetup> list = new ArrayList<>();
		if (vendorList != null && !vendorList.isEmpty()) 
		{
			main = new DtoPayableAccountClassSetup();
			main.setClassId(classId);
			for (VendorClassAccountTableSetup vendorClassAccountTableSetup : vendorList) {
				List<Long> accountNumberIndex = new ArrayList<>();
				DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
				dtoPayableAccountClassSetup.setAccountDescription("");
				dtoPayableAccountClassSetup.setAccountType(0);
				dtoPayableAccountClassSetup.setAccountTypeValue("");
				dtoPayableAccountClassSetup.setClassId("");
				if (vendorClassAccountTableSetup.getVendorClassesSetup() != null) {
					dtoPayableAccountClassSetup
							.setClassId(vendorClassAccountTableSetup.getVendorClassesSetup().getVendorClassId());
				}
				MasterAccountType masterAccountType =vendorClassAccountTableSetup.getMasterAccountType();
				if (masterAccountType!= null) 
				{
					int lang = Integer.parseInt(langId);
					if(masterAccountType.getLanguage().getLanguageId()!=lang){
						masterAccountType = repositoryMasterAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterAccountType.getTypeId(),lang,false);
					}
					if(masterAccountType!=null)
					{
						dtoPayableAccountClassSetup
						.setAccountType(masterAccountType.getTypeId());
					dtoPayableAccountClassSetup
							.setAccountTypeValue(masterAccountType.getAccountType());
					}
				}
				
				GLAccountsTableAccumulation glAccountsTableAccumulation = vendorClassAccountTableSetup
						.getGlAccountsTableAccumulation();
				String accountNumber = "";
				List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
				if (glAccountsTableAccumulation != null) 
				{
					dtoPayableAccountClassSetup
							.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
					Object[] arrayOfObject= this.getAccountNumberMultipleWays(glAccountsTableAccumulation, langId);
					accountNumber=(String) arrayOfObject[0];
					accountNumberIndex=(List<Long>) arrayOfObject[1];
					listOfKeyValue=(List<DtoPayableAccountClassSetup>) arrayOfObject[2];
					dtoPayableAccountClassSetup.setAccountDescription(arrayOfObject[3].toString());
				}
				dtoPayableAccountClassSetup.setAccountNumberIndex(accountNumberIndex);
				dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
				dtoPayableAccountClassSetup.setAccNumberIndexValue(listOfKeyValue);
				list.add(dtoPayableAccountClassSetup);
			}
			main.setAccountNumberList(list);
		}
		else
		{
			main= new DtoPayableAccountClassSetup();
			main.setMessageType("RECORD_NOT_FOUND");
		}
		return main;
	}
	
	/**
	 * Create new GL account number
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	public DtoPayableAccountClassSetup saveVendorAccountMaintenance(
			DtoPayableAccountClassSetup dtoPayableAccountClassSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid = serviceHome.getLanngugaeId();
		GLAccountsTableAccumulation glAccountsTableAccumulation = null;
		try {
			repositoryVendorAccountTableSetup.deleteRecordById(dtoPayableAccountClassSetup.getVendorId(),true);
			if (dtoPayableAccountClassSetup.getAccountNumberList() != null
					&& !dtoPayableAccountClassSetup.getAccountNumberList().isEmpty()) {
				for (DtoPayableAccountClassSetup accountNumber : dtoPayableAccountClassSetup.getAccountNumberList()) {
					VendorAccountTableSetup vendorAccountTableSetup = repositoryVendorAccountTableSetup
							.findByMasterAccountTypeTypeIdAndVendorMaintenanceVendorid(accountNumber.getAccountType(),dtoPayableAccountClassSetup.getVendorId());
					if (vendorAccountTableSetup == null) {
						vendorAccountTableSetup = new VendorAccountTableSetup();
					}
					vendorAccountTableSetup.setDeleted(false);
					vendorAccountTableSetup.setVendorMaintenance(
							repositoryVendorMaintenance.findByVendoridAndIsDeleted(dtoPayableAccountClassSetup.getVendorId(),false));
					vendorAccountTableSetup.setMasterAccountType(
							repositoryMasterAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(accountNumber.getAccountType(),langid,false));
					vendorAccountTableSetup.setCreatedBy(loggedInUserId);
					
					StringBuilder sb = new StringBuilder();
					String fullIndex = "";
					if (accountNumber.getAccountNumberIndex() != null
							&& !accountNumber.getAccountNumberIndex().isEmpty()) {
						for (Long i : accountNumber.getAccountNumberIndex()) {
							sb.append(String.valueOf(i));
						}
						fullIndex = sb.toString();
					}
					List<GLAccountsTableAccumulation> glAccountsTableAccumulationsList = repositoryGLAccountsTableAccumulation
							.findByAccountNumberFullIndexAndIsDeleted(fullIndex,false);
					if (glAccountsTableAccumulationsList != null && !glAccountsTableAccumulationsList.isEmpty()) {
						glAccountsTableAccumulation = glAccountsTableAccumulationsList.get(0);
						glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
					} else {
						glAccountsTableAccumulation = new GLAccountsTableAccumulation();
						int actRowIndexId = 1;
						PayableAccountClassSetup payableAccountClassSetup2 = repositoryPayableAccountClassSetup
								.findFirstByOrderByIdDesc();
						if (payableAccountClassSetup2 != null) {
							actRowIndexId = payableAccountClassSetup2.getAccountRowIndex();
							actRowIndexId = actRowIndexId + 1;
						}
						int segmentNumber = 1;
						if (accountNumber.getAccountNumberIndex() != null
								&& !accountNumber.getAccountNumberIndex().isEmpty()) {
							StringBuilder accountNumberfullIndex = new StringBuilder("");
							for (Long indexId : accountNumber.getAccountNumberIndex()) 
							{
								accountNumberfullIndex.append(indexId);
								PayableAccountClassSetup payableAccountClassSetup = new PayableAccountClassSetup();
								payableAccountClassSetup.setIndexId(indexId);
								payableAccountClassSetup.setSegmentNumber(segmentNumber);
								segmentNumber++;
								payableAccountClassSetup.setAccountRowIndex(actRowIndexId);
								payableAccountClassSetup.setCreatedBy(loggedInUserId);
								repositoryPayableAccountClassSetup.save(payableAccountClassSetup);
							}
							
							glAccountsTableAccumulation.setAccountTableRowIndex(String.valueOf(actRowIndexId));
							glAccountsTableAccumulation.setSegmentNumber(segmentNumber);
							glAccountsTableAccumulation.setAccountNumberFullIndex(accountNumberfullIndex.toString());
							glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
							if (accountNumber.getTransactionType() != null) {
								glAccountsTableAccumulation.setTransactionType(accountNumber.getTransactionType());
							}
							glAccountsTableAccumulation.setCreatedBy(loggedInUserId);
							
						}
					}
					glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
							.saveAndFlush(glAccountsTableAccumulation);

					vendorAccountTableSetup.setGlAccountsTableAccumulation(glAccountsTableAccumulation);
					repositoryVendorAccountTableSetup.saveAndFlush(vendorAccountTableSetup);
				}
			} else {
				dtoPayableAccountClassSetup.setMessageType(MessageLabel.REQUEST_BODY_IS_EMPTY);
			}
		} catch (Exception e) {
			dtoPayableAccountClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			log.info(Arrays.toString(e.getStackTrace()));
		}
		
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			return getVendorAccountMaintenanceByVendorId(dtoPayableAccountClassSetup.getVendorId());
		} else {
			return dtoPayableAccountClassSetup;
		}
	}
	
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public DtoPayableAccountClassSetup getVendorAccountMaintenanceByVendorId(String vendorId) {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		DtoPayableAccountClassSetup main = null;
		List<VendorAccountTableSetup> vendorList = repositoryVendorAccountTableSetup.findByVendorMaintenanceVendoridAndIsDeleted(vendorId,false);
		List<DtoPayableAccountClassSetup> list = new ArrayList<>();
		if (vendorList != null && !vendorList.isEmpty()) 
		{
			main = new DtoPayableAccountClassSetup();
			main.setVendorId(vendorId);
			for (VendorAccountTableSetup vendorAccountTableSetup : vendorList) 
			{
				List<Long> accountNumberIndex = new ArrayList<>();
				DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
				dtoPayableAccountClassSetup.setAccountDescription("");
				dtoPayableAccountClassSetup.setAccountType(0);
				dtoPayableAccountClassSetup.setAccountTypeValue("");
				dtoPayableAccountClassSetup.setVendorId("");
				if (vendorAccountTableSetup.getVendorMaintenance() != null) {
					dtoPayableAccountClassSetup
							.setVendorId(vendorAccountTableSetup.getVendorMaintenance().getVendorid());
				}
				
				MasterAccountType masterAccountType =vendorAccountTableSetup.getMasterAccountType();
				if (masterAccountType!= null) 
				{
					int lang = Integer.parseInt(langId);
					if(masterAccountType.getLanguage().getLanguageId()!=lang){
						masterAccountType = repositoryMasterAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterAccountType.getTypeId(),lang,false);
					}
					if(masterAccountType!=null)
					{
						dtoPayableAccountClassSetup
						.setAccountType(masterAccountType.getTypeId());
					    dtoPayableAccountClassSetup
							.setAccountTypeValue(masterAccountType.getAccountType());
					}
				}

				GLAccountsTableAccumulation glAccountsTableAccumulation = vendorAccountTableSetup
						.getGlAccountsTableAccumulation();
				String accountNumber = "";
				List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
				if (glAccountsTableAccumulation != null) {
					dtoPayableAccountClassSetup
							.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
					Object[] arrayOfObject = getAccountNumberMultipleWays(glAccountsTableAccumulation,langId);
					accountNumber=(String) arrayOfObject[0];
					accountNumberIndex=(List<Long>) arrayOfObject[1];
					listOfKeyValue=(List<DtoPayableAccountClassSetup>) arrayOfObject[2];
					dtoPayableAccountClassSetup.setAccountDescription(arrayOfObject[3].toString());
				}
				
				dtoPayableAccountClassSetup.setAccountNumberIndex(accountNumberIndex);
				dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
				dtoPayableAccountClassSetup.setAccNumberIndexValue(listOfKeyValue);
				list.add(dtoPayableAccountClassSetup);
				
			}
			main.setAccountNumberList(list);
		}
		else
		{
			VendorMaintenance vendorMaintenance= repositoryVendorMaintenance.findByVendoridAndIsDeleted(vendorId, false);
			if(vendorMaintenance!=null && vendorMaintenance.getVendorClassesSetup()!=null){
				List<VendorClassAccountTableSetup> vendorClassAccountTableSetupList=repositoryVendorClassAccountTableSetup.findByVendorClassesSetupVendorClassIdAndIsDeleted(vendorMaintenance.getVendorClassesSetup().getVendorClassId(), false);
			    if(vendorClassAccountTableSetupList!=null && !vendorClassAccountTableSetupList.isEmpty()){
			    	
			    	main = new DtoPayableAccountClassSetup();
					main.setVendorId(vendorId);
			    	for (VendorClassAccountTableSetup vendorClassAccountTableSetup : vendorClassAccountTableSetupList) {
			    		List<Long> accountNumberIndex = new ArrayList<>();
						DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
						dtoPayableAccountClassSetup.setAccountDescription("");
						dtoPayableAccountClassSetup.setAccountType(0);
						dtoPayableAccountClassSetup.setAccountTypeValue("");
						dtoPayableAccountClassSetup.setVendorId("");
						
							dtoPayableAccountClassSetup
									.setVendorId(vendorMaintenance.getVendorid());
						
						MasterAccountType masterAccountType =vendorClassAccountTableSetup.getMasterAccountType();
						if (masterAccountType!= null) 
						{
							int lang = Integer.parseInt(langId);
							if(masterAccountType.getLanguage().getLanguageId()!=lang){
								masterAccountType = repositoryMasterAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterAccountType.getTypeId(),lang,false);
							}
							if(masterAccountType!=null)
							{
								dtoPayableAccountClassSetup
								.setAccountType(masterAccountType.getTypeId());
							dtoPayableAccountClassSetup
									.setAccountTypeValue(masterAccountType.getAccountType());
							}
							
						}

						GLAccountsTableAccumulation glAccountsTableAccumulation = vendorClassAccountTableSetup
								.getGlAccountsTableAccumulation();
						String accountNumber = "";
						List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
						if (glAccountsTableAccumulation != null) {
							dtoPayableAccountClassSetup
									.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
							
							Object[] arrayOfObject = getAccountNumberMultipleWays(glAccountsTableAccumulation,langId);
							accountNumber=(String) arrayOfObject[0];
							accountNumberIndex=(List<Long>) arrayOfObject[1];
							listOfKeyValue=(List<DtoPayableAccountClassSetup>) arrayOfObject[2];
							dtoPayableAccountClassSetup.setAccountDescription(arrayOfObject[3].toString());
						}
						dtoPayableAccountClassSetup.setAccountNumberIndex(accountNumberIndex);
						dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
						dtoPayableAccountClassSetup.setAccNumberIndexValue(listOfKeyValue);
						list.add(dtoPayableAccountClassSetup);
					}
			    	main.setAccountNumberList(list);
			    }
			}
		}
		return main;
	}
	
	/**
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	public DtoPayableAccountClassSetup saveFixedAllocationAccount(
			DtoPayableAccountClassSetup dtoPayableAccountClassSetup) {
		FixedAllocationAccountIndexHeader fixedAllocationAccountIndexHeader = null;
		try {
			int loggedInUserId = 0;
			if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {

				loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			if (UtilRandomKey.isNotBlank(dtoPayableAccountClassSetup.getMainAccountID().toString())) {
				COAMainAccounts coaMainAccounts = repositoryCOAMainAccounts.findByActIndxAndIsDeleted(dtoPayableAccountClassSetup.getMainAccountID(),false);
				if (coaMainAccounts != null) {
					fixedAllocationAccountIndexHeader = repositoryFixedAllocationAccountIndexHeader
							.findByCoaMainAccountsActIndxAndIsDeleted(coaMainAccounts.getActIndx(),false);
					if (fixedAllocationAccountIndexHeader == null) {
						fixedAllocationAccountIndexHeader = new FixedAllocationAccountIndexHeader();
						fixedAllocationAccountIndexHeader.setCoaMainAccounts(coaMainAccounts);
						fixedAllocationAccountIndexHeader.setCreatedBy(loggedInUserId);
						fixedAllocationAccountIndexHeader = repositoryFixedAllocationAccountIndexHeader
								.save(fixedAllocationAccountIndexHeader);
					}

					if (dtoPayableAccountClassSetup.getAccountNumberList() != null
							&& !dtoPayableAccountClassSetup.getAccountNumberList().isEmpty()) {
						repositoryFixedAllocationAccountIndexDetail
								.deleteByHeaderId(fixedAllocationAccountIndexHeader.getActFixIndex());
						for (DtoPayableAccountClassSetup accountNumbers : dtoPayableAccountClassSetup.getAccountNumberList()) {
							GLAccountsTableAccumulation glAccountsTableAccumulation2 = repositoryGLAccountsTableAccumulation
									.findByAccountTableRowIndexAndIsDeleted(accountNumbers.getAccountTableRowIndex(),false);
							if (glAccountsTableAccumulation2 != null) {
								FixedAllocationAccountIndexDetails fixedAllocationAccountIndexDetail = new FixedAllocationAccountIndexDetails();
								fixedAllocationAccountIndexDetail
										.setFixedAllocationAccountIndexHeader(fixedAllocationAccountIndexHeader);
								fixedAllocationAccountIndexDetail
										.setGlAccountsTableAccumulation(glAccountsTableAccumulation2);
								fixedAllocationAccountIndexDetail
										.setPercentage(Double.valueOf(accountNumbers.getPercentage()));
								fixedAllocationAccountIndexDetail.setCreatedBy(loggedInUserId);
								repositoryFixedAllocationAccountIndexDetail.save(fixedAllocationAccountIndexDetail);
							}

						}
					}
				}
			} else {
				dtoPayableAccountClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			dtoPayableAccountClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			log.info(Arrays.toString(e.getStackTrace()));
		}
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			return getFixedAllocationAccountByAccountIndexHeaderNumber(fixedAllocationAccountIndexHeader);
		} else {
			return dtoPayableAccountClassSetup;
		}
	}
	
	/**
	 * @param fixedAllocationAccountIndexHeader
	 * @return
	 */
	public DtoPayableAccountClassSetup getFixedAllocationAccountByAccountIndexHeaderNumber(
			FixedAllocationAccountIndexHeader fixedAllocationAccountIndexHeader) {
		DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
		try {
			
			if (fixedAllocationAccountIndexHeader.getCoaMainAccounts() != null) {
				dtoPayableAccountClassSetup.setMainAccountID(
						fixedAllocationAccountIndexHeader.getCoaMainAccounts().getActIndx());
				dtoPayableAccountClassSetup.setMainAccountNumber(fixedAllocationAccountIndexHeader.getCoaMainAccounts().getMainAccountNumber());
			}
			List<DtoPayableAccountClassSetup> accountNumbersList = new ArrayList<>();
			List<FixedAllocationAccountIndexDetails> listofAccountNumbers = repositoryFixedAllocationAccountIndexDetail
					.findByFixedAllocationAccountIndexHeaderActFixIndexAndIsDeleted(
							fixedAllocationAccountIndexHeader.getActFixIndex(),false);
			if (listofAccountNumbers != null && !listofAccountNumbers.isEmpty()) {
				for (FixedAllocationAccountIndexDetails fixedAllocationAccountIndexDetails : listofAccountNumbers) {
					DtoPayableAccountClassSetup dtPayableAccountClassSetup = new DtoPayableAccountClassSetup();
					dtPayableAccountClassSetup.setAccountTableRowIndex("");
					if (fixedAllocationAccountIndexDetails.getGlAccountsTableAccumulation() != null) {
						dtPayableAccountClassSetup.setAccountTableRowIndex(fixedAllocationAccountIndexDetails
								.getGlAccountsTableAccumulation().getAccountTableRowIndex());
						
						Object[] object =getAccountNumber(fixedAllocationAccountIndexDetails.getGlAccountsTableAccumulation());
						String accountNumber=object[0].toString();
						
						dtPayableAccountClassSetup.setAccountNumber(accountNumber);
						
					}
					dtPayableAccountClassSetup.setPercentage(String.valueOf(fixedAllocationAccountIndexDetails.getPercentage()));
					accountNumbersList.add(dtPayableAccountClassSetup);
				}
			}
			dtoPayableAccountClassSetup.setAccountNumberList(accountNumbersList);

		} catch (Exception e) {
			dtoPayableAccountClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoPayableAccountClassSetup;
	}
	
	/**
	 * @return
	 */
	public List<DtoPayableAccountClassSetup> getFixedAllocationAccount() {
		List<DtoPayableAccountClassSetup> listOfAccounts = new ArrayList<>();
		List<FixedAllocationAccountIndexHeader> fixedAllocationAccountIndexHeaderList = repositoryFixedAllocationAccountIndexHeader
				.findAll();
		if (fixedAllocationAccountIndexHeaderList != null && !fixedAllocationAccountIndexHeaderList.isEmpty()) {
			for (FixedAllocationAccountIndexHeader fixedAllocationAccountIndexHeader : fixedAllocationAccountIndexHeaderList) {
				DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
				
				if (fixedAllocationAccountIndexHeader.getCoaMainAccounts() != null) {
					dtoPayableAccountClassSetup.setMainAccountID(fixedAllocationAccountIndexHeader
							.getCoaMainAccounts().getActIndx());
					dtoPayableAccountClassSetup.setMainAccountNumber(fixedAllocationAccountIndexHeader.getCoaMainAccounts().getMainAccountNumber());
				}
				List<DtoPayableAccountClassSetup> accountNumbersList = new ArrayList<>();
				List<FixedAllocationAccountIndexDetails> listofAccountNumbers = repositoryFixedAllocationAccountIndexDetail
						.findByFixedAllocationAccountIndexHeaderActFixIndexAndIsDeleted(
								fixedAllocationAccountIndexHeader.getActFixIndex(),false);
				if (listofAccountNumbers != null && !listofAccountNumbers.isEmpty()) {
					for (FixedAllocationAccountIndexDetails fixedAllocationAccountIndexDetails : listofAccountNumbers) {
						DtoPayableAccountClassSetup dtPayableAccountClassSetup = new DtoPayableAccountClassSetup();
						dtPayableAccountClassSetup.setAccountTableRowIndex("");
						if (fixedAllocationAccountIndexDetails.getGlAccountsTableAccumulation() != null) {
							dtPayableAccountClassSetup.setAccountTableRowIndex(fixedAllocationAccountIndexDetails
									.getGlAccountsTableAccumulation().getAccountTableRowIndex());
							
							
							
							Object[] object =getAccountNumber(fixedAllocationAccountIndexDetails
									.getGlAccountsTableAccumulation());
							String accountNumber = object[0].toString();
							dtPayableAccountClassSetup.setAccountNumber(accountNumber);
												
						}
						dtPayableAccountClassSetup.setPercentage(String.valueOf(fixedAllocationAccountIndexDetails.getPercentage()));
						accountNumbersList.add(dtPayableAccountClassSetup);
					}
				}
				dtoPayableAccountClassSetup.setAccountNumberList(accountNumbersList);
				listOfAccounts.add(dtoPayableAccountClassSetup);
			}
		}
		return listOfAccounts;
	}
	
	public String getAccountNumberIdsWithZero(GLAccountsTableAccumulation glAccountsTableAccumulation)
	{
		List<PayableAccountClassSetup> listOfPayable = repositoryPayableAccountClassSetup
				.findByAccountRowIndexAndIsDeleted(Integer.parseInt(glAccountsTableAccumulation.getAccountTableRowIndex()),false);
		StringBuilder accountNumber = new StringBuilder("");
		int i = 0;
		for (PayableAccountClassSetup payableAccountClassSetup : listOfPayable) {
			if (i == 0) 
			{
				COAMainAccounts coaMainAccounts = repositoryCOAMainAccounts
						.findByActIndxAndIsDeleted(payableAccountClassSetup.getIndexId(),false);
				if (coaMainAccounts != null) 
				{
						accountNumber.append(String.valueOf(coaMainAccounts.getActIndx()));
				}
				else
				{
					accountNumber.append("0");
				}
			} 
			else 
			{
				if(payableAccountClassSetup.getIndexId()==0){
					accountNumber.append("-"+"0");
				}
				else
				{
					COAFinancialDimensionsValues coafValue = repositoryCOAFinancialDimensionsValues
							.findByDimInxValueAndIsDeleted(payableAccountClassSetup.getIndexId(),false);
					if (coafValue != null) {
							accountNumber.append("-" + coafValue.getDimInxValue());
					}
					else{
						accountNumber.append("-"+"0");
					}
				}
			}
			i++;
		}
		return accountNumber.toString();
	}
	
	public Object[] getAccountNumber(GLAccountsTableAccumulation glAccountsTableAccumulation)
	{
		List<PayableAccountClassSetup> listOfPayable = repositoryPayableAccountClassSetup
				.findByAccountRowIndexAndIsDeleted(Integer.parseInt(glAccountsTableAccumulation.getAccountTableRowIndex()),false);
		StringBuilder accountNumber = new StringBuilder("");
		StringBuilder accountDescription = new StringBuilder("");
		int i = 0;
		boolean isTransactionAllowed=false;
		String typicalBalanceType="";
		for (PayableAccountClassSetup payableAccountClassSetup : listOfPayable) 
		{
			if (i == 0) 
			{
				COAMainAccounts coaMainAccounts = repositoryCOAMainAccounts
						.findByActIndxAndIsDeleted(payableAccountClassSetup.getIndexId(),false);
				if (coaMainAccounts != null) 
				{
					typicalBalanceType=coaMainAccounts.getTpclBalanceType().getTypePrimary();
					if(coaMainAccounts.getAllowAccountTransactionEntry()!=null){
						isTransactionAllowed=coaMainAccounts.getAllowAccountTransactionEntry();
					}
					
					if (UtilRandomKey.isNotBlank(coaMainAccounts.getMainAccountNumber())) {
						accountNumber.append(coaMainAccounts.getMainAccountNumber());
					}
					
					if(UtilRandomKey.isNotBlank(coaMainAccounts.getMainAccountDescription())){
						accountDescription.append(coaMainAccounts.getMainAccountDescription());
					}
					
				}
				else
				{
					accountNumber.append(" ");
				}
			} 
			else 
			{
				if(payableAccountClassSetup.getIndexId()==0){
					accountNumber.append("-"+" ");
				}
				else
				{
					COAFinancialDimensionsValues coafValue = repositoryCOAFinancialDimensionsValues
							.findByDimInxValueAndIsDeleted(payableAccountClassSetup.getIndexId(),false);
					if (coafValue != null) {
						if (UtilRandomKey.isNotBlank(coafValue.getDimensionValue())) {
							accountNumber.append("-" + coafValue.getDimensionValue());
						}
						if(UtilRandomKey.isNotBlank(coafValue.getDimensionDescription())){
							accountDescription.append(" - "+coafValue.getDimensionDescription());
						}
					}
					else{
						accountNumber.append("-"+" ");
					}
				}
			}
			i++;
		}
		return new Object[]{accountNumber.toString(),isTransactionAllowed,typicalBalanceType,accountDescription.toString()} ;
	}
	
	public Object[] getAccountNumberMultipleWays(GLAccountsTableAccumulation glAccountsTableAccumulation,String langId)
	{
		int i = 0;
		StringBuilder accountNumber = new StringBuilder("");
		StringBuilder accountDesc = new StringBuilder("");
		List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
		List<Long> accountNumberIndex = new ArrayList<>();
		List<PayableAccountClassSetup> listOfPayable = repositoryPayableAccountClassSetup
				.findByAccountRowIndexAndIsDeleted(
						Integer.parseInt(glAccountsTableAccumulation.getAccountTableRowIndex()),false);
		for (PayableAccountClassSetup payableAccountClassSetup : listOfPayable) 
		{
			DtoPayableAccountClassSetup dtoPayableAccountClassSetup2 = new DtoPayableAccountClassSetup();
			dtoPayableAccountClassSetup2.setIndexId(payableAccountClassSetup.getIndexId());
			if (i == 0) 
			{
				dtoPayableAccountClassSetup2.setType("MAIN_ACCOUNT");
				dtoPayableAccountClassSetup2.setDescription("");
				dtoPayableAccountClassSetup2.setAccountValue("");
				COAMainAccounts coaMainAccounts = repositoryCOAMainAccounts
						.findByActIndxAndIsDeleted(payableAccountClassSetup.getIndexId(),false);
				if (coaMainAccounts != null) 
				{
					if (UtilRandomKey.isNotBlank(coaMainAccounts.getMainAccountNumber())) {
						dtoPayableAccountClassSetup2
								.setAccountValue(coaMainAccounts.getMainAccountNumber());
						accountNumber.append(coaMainAccounts.getMainAccountNumber());
					}
					dtoPayableAccountClassSetup2
					.setDescription("");
					if(UtilRandomKey.isNotBlank(coaMainAccounts.getMainAccountDescription()))
					{
						dtoPayableAccountClassSetup2
						.setDescription(coaMainAccounts.getMainAccountDescription());
						accountDesc.append(coaMainAccounts.getMainAccountDescription());
					}
					
					
					
				}
				else
				{
					accountNumber.append(" ");
				}
			} 
			else 
			{
				dtoPayableAccountClassSetup2.setType("DIMENSION_VALUE");
				dtoPayableAccountClassSetup2.setAccountValue("0");
				dtoPayableAccountClassSetup2.setDescription("");
				if(payableAccountClassSetup.getIndexId()==0){
					accountNumber.append("-"+" ");
				}
				else
				{
					COAFinancialDimensionsValues coafValue = repositoryCOAFinancialDimensionsValues
							.findByDimInxValueAndIsDeleted(payableAccountClassSetup.getIndexId(),false);
					if (coafValue != null) {
						if (UtilRandomKey.isNotBlank(coafValue.getDimensionValue())) {
							dtoPayableAccountClassSetup2.setAccountValue(coafValue.getDimensionValue());
							accountNumber.append("-" + coafValue.getDimensionValue());
						}

						dtoPayableAccountClassSetup2.setDescription("");
						if(UtilRandomKey.isNotBlank(coafValue.getDimensionDescription())){
							dtoPayableAccountClassSetup2.setDescription(coafValue.getDimensionDescription());
							accountDesc.append(" "+coafValue.getDimensionDescription());
						}
						
					}
					else{
						accountNumber.append("-"+" ");
					}
				}
			}
			accountNumberIndex.add(payableAccountClassSetup.getIndexId());
			listOfKeyValue.add(dtoPayableAccountClassSetup2);
			i++;
		}
		return new Object[]{accountNumber.toString(),accountNumberIndex,listOfKeyValue,accountDesc.toString()};
	}
	
	public DtoPayableAccountClassSetup createNewAccountNumber(
			DtoPayableAccountClassSetup dtoPayableAccountClassSetup) 
	{
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		GLAccountsTableAccumulation glAccountsTableAccumulation = null;
		try {
			if (dtoPayableAccountClassSetup.getAccountNumberList() != null
					&& !dtoPayableAccountClassSetup.getAccountNumberList().isEmpty()) {
				
				for (DtoPayableAccountClassSetup accountNumber : dtoPayableAccountClassSetup.getAccountNumberList()) {
					
					StringBuilder sb = new StringBuilder();
					String fullIndex = "";
					if (accountNumber.getAccountNumberIndex() != null
							&& !accountNumber.getAccountNumberIndex().isEmpty()) {
						for (Long i : accountNumber.getAccountNumberIndex()) {
							sb.append(String.valueOf(i));
						}
						fullIndex = sb.toString();
					}
					List<GLAccountsTableAccumulation> glAccountsTableAccumulationsList = repositoryGLAccountsTableAccumulation
							.findByAccountNumberFullIndexAndIsDeleted(fullIndex,false);
					if (glAccountsTableAccumulationsList != null && !glAccountsTableAccumulationsList.isEmpty()) {
						glAccountsTableAccumulation = glAccountsTableAccumulationsList.get(0);
						glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
					}
					else 
					{
						glAccountsTableAccumulation = new GLAccountsTableAccumulation();
						int actRowIndexId = 1;
						PayableAccountClassSetup payableAccountClassSetup2 = repositoryPayableAccountClassSetup
								.findFirstByOrderByIdDesc();
						if (payableAccountClassSetup2 != null) {
							actRowIndexId = payableAccountClassSetup2.getAccountRowIndex();
							actRowIndexId = actRowIndexId + 1;
						}
						int segmentNumber = 1;
						if (accountNumber.getAccountNumberIndex() != null
								&& !accountNumber.getAccountNumberIndex().isEmpty()) {
							StringBuilder accountNumberfullIndex = new StringBuilder("");
							for (Long indexId : accountNumber.getAccountNumberIndex()) {
								accountNumberfullIndex.append(indexId);
								PayableAccountClassSetup payableAccountClassSetup = new PayableAccountClassSetup();
								payableAccountClassSetup.setIndexId(indexId);
								payableAccountClassSetup.setSegmentNumber(segmentNumber);
								segmentNumber++;
								payableAccountClassSetup.setAccountRowIndex(actRowIndexId);
								payableAccountClassSetup.setCreatedBy(loggedInUserId);
								repositoryPayableAccountClassSetup.save(payableAccountClassSetup);
							}
							glAccountsTableAccumulation.setAccountTableRowIndex(String.valueOf(actRowIndexId));
							glAccountsTableAccumulation.setSegmentNumber(segmentNumber);
							glAccountsTableAccumulation.setAccountNumberFullIndex(accountNumberfullIndex.toString());
							glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
							glAccountsTableAccumulation.setAccountDescriptionArabic(accountNumber.getAccountDescriptionArabic());
							glAccountsTableAccumulation.setAccountNumber(accountNumber.getAccountNumber());
							if (accountNumber.getTransactionType() != null) {
								glAccountsTableAccumulation.setTransactionType(accountNumber.getTransactionType());
							}
							glAccountsTableAccumulation.setCreatedBy(loggedInUserId);
						}
					}
					glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
							.saveAndFlush(glAccountsTableAccumulation);
					Object[] object=getAccountNumber(glAccountsTableAccumulation);
					dtoPayableAccountClassSetup.setAccountNumber(object[0].toString());
				}
			} else {
				dtoPayableAccountClassSetup.setMessageType(MessageLabel.REQUEST_BODY_IS_EMPTY);
			}
		} catch (Exception e) {
			dtoPayableAccountClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			log.info(Arrays.toString(e.getStackTrace()));
		}
		
		return dtoPayableAccountClassSetup;
	}
	
	public List<DtoFaAccountGroupSetup> getFaAccountGroupSetupAccountNumberList(int faAccountGroupIndex, String langId)
	{
		List<FAAccountGroupAccountTableSetup> list= repositoryFAAccountGroupAccountTableSetup.findByfaAccountGroupsSetupFaAccountGroupIndexAndIsDeleted(faAccountGroupIndex,false);
		List<DtoFaAccountGroupSetup> accountDetailList = new ArrayList<>();
		if(list!=null && !list.isEmpty())
	    {
	    	for (FAAccountGroupAccountTableSetup faAccountGroupAccountTableSetup : list) 
	    	{
	    		 DtoFaAccountGroupSetup accountDetail = new DtoFaAccountGroupSetup();
		    	 accountDetail.setAccountNumber("");
		    	 if(faAccountGroupAccountTableSetup.getGlAccountsTableAccumulation()!=null){
		    		accountDetail.setAccountNumber(faAccountGroupAccountTableSetup.getGlAccountsTableAccumulation().getAccountTableRowIndex());
		    	 }
		    	 accountDetail.setAccountType(0);
		    	 accountDetail.setAccountTypeValue("");
		    	 MasterFAAccountGroupAccountType masterFAAccountGroupAccountType =faAccountGroupAccountTableSetup.getAccountType();
		    	 if(masterFAAccountGroupAccountType!=null)
		    	 {
		    		    int lang = Integer.parseInt(langId);
						if(masterFAAccountGroupAccountType.getLanguage().getLanguageId()!=lang){
							masterFAAccountGroupAccountType = repositoryMasterFAAccountGroupAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterFAAccountGroupAccountType.getTypeId(),lang,false);
						}
						
						if(masterFAAccountGroupAccountType!=null)
						{
							accountDetail
							.setAccountType(masterFAAccountGroupAccountType.getTypeId());
							accountDetail
								.setAccountTypeValue(masterFAAccountGroupAccountType.getAccountType());
						}
		    	 }
		    	 accountDetailList.add(accountDetail);
			  }
	     }
		return accountDetailList;
	}
	
}
