/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bti.constant.ARMasterDocumentTypeConstant;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.APPaymentTransactions;
import com.bti.model.APYTDOpenTransaction;
import com.bti.model.ARApplyDocumentsPayments;
import com.bti.model.ARPaymentTransactions;
import com.bti.model.ARYTDOpenTransactions;
import com.bti.model.ApplyDocumentsPayments;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.MasterDocumentType;
import com.bti.model.PMDocumentsTypeSetup;
import com.bti.model.PaymentTermsSetup;
import com.bti.model.RMDocumentsTypeSetup;
import com.bti.model.dto.DtoAPApplyDocumentsPayments;
import com.bti.model.dto.DtoAPYTDOpenTransactions;
import com.bti.model.dto.DtoARYTDOpenTransactions;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryAPApplyDocumentsPayments;
import com.bti.repository.RepositoryAPPaymentTransactions;
import com.bti.repository.RepositoryAPYTDOpenTransaction;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryMasterDocumentType;
import com.bti.repository.RepositoryPMDocumentsTypeSetup;
import com.bti.repository.RepositoryPaymentTermSetup;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * @Description: ServiceApplyAPTransactions Name of Project: BTI 
 * @Created on:	Jan 18, 2018 
 * @Modified on: Jan 19, 2018 05:38:38 PM
 * @author seasia Version:
 */
@Service("serviceApplyAPTransactions")
public class ServiceApplyAPTransactions {

	private static final Logger LOG = Logger.getLogger(ServiceApplyAPTransactions.class);

	private static final String USER_ID = "userid";
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	@Autowired
	ServiceHome serviceHome;
	@Autowired
	RepositoryAPPaymentTransactions repositoryAPPaymentTransactions;
	@Autowired
	RepositoryAPApplyDocumentsPayments repositoryAPApplyDocumentsPayments;
	@Autowired
	RepositoryMasterDocumentType repositoryMasterDocumentType;
	@Autowired
	RepositoryAPYTDOpenTransaction repositoryAPYTDOpenTransaction;
	@Autowired
	RepositoryPMDocumentsTypeSetup repositoryPMDocumentsTypeSetup;
	@Autowired
	RepositoryPaymentTermSetup repositoryPaymentTermSetup;
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	
	public List<DtoStatusType> getDocumentType() {
		List<DtoStatusType> list = new ArrayList<>();
		int langId = serviceHome.getLanngugaeId();
		try {
			List<MasterDocumentType> masterDocumentTypeList = repositoryMasterDocumentType.findByLanguageLanguageIdAndIsDeletedAndMasterType(langId, false,"AP");
			if (masterDocumentTypeList != null && !masterDocumentTypeList.isEmpty()) {
				masterDocumentTypeList.forEach(masterDocumentType -> list.add(new DtoStatusType(masterDocumentType)));
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}
	
	public DtoSearch getOpenTransactionsByVendorId(DtoAPYTDOpenTransactions dtoAPYTDOpenTransaction) {
		DtoSearch dtoSearch= new DtoSearch();
		dtoSearch.setPageNumber(0);
		dtoSearch.setPageSize(0);
		dtoSearch.setTotalCount(repositoryAPYTDOpenTransaction.getCountByVendorId(dtoAPYTDOpenTransaction.getVendorID()));
		List<DtoAPYTDOpenTransactions> list = new ArrayList<>();
		try 
		{
			List<APYTDOpenTransaction> apYTDOpenTransactionsList=null;
			if(dtoAPYTDOpenTransaction.getPageNumber()!=null && dtoAPYTDOpenTransaction.getPageSize()!=null && 
					dtoAPYTDOpenTransaction.getPageSize()>0)
			{
				dtoSearch.setPageNumber(dtoAPYTDOpenTransaction.getPageNumber());
				dtoSearch.setPageSize(dtoAPYTDOpenTransaction.getPageSize());
				Pageable pageable = new PageRequest(dtoAPYTDOpenTransaction.getPageNumber(), dtoAPYTDOpenTransaction.getPageSize());
				apYTDOpenTransactionsList = repositoryAPYTDOpenTransaction.findByVendorID(dtoAPYTDOpenTransaction.getVendorID(),pageable);
			}
			else
			{
				apYTDOpenTransactionsList=repositoryAPYTDOpenTransaction.findByVendorID(dtoAPYTDOpenTransaction.getVendorID());
			}
			
			if (apYTDOpenTransactionsList != null && !apYTDOpenTransactionsList.isEmpty()) 
			{
				for (APYTDOpenTransaction apytdOpenTransaction : apYTDOpenTransactionsList) 
				{
					DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions=new DtoAPYTDOpenTransactions(apytdOpenTransaction);
					PMDocumentsTypeSetup pmDocumentsTypeSetup = repositoryPMDocumentsTypeSetup.findByApDocumentTypeIdAndIsDeleted(apytdOpenTransaction.getApTransactionType(), false);
				    if(pmDocumentsTypeSetup!=null){
				    	dtoAPYTDOpenTransactions.setApTransactionType(pmDocumentsTypeSetup.getDocumentCode());
				    }
				  
				    PaymentTermsSetup paymentTermsSetup=repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(apytdOpenTransaction.getPaymentTermsID(), false);
				    if(paymentTermsSetup!=null){
				    	Calendar cal =Calendar.getInstance();
				    	cal.setTime(apytdOpenTransaction.getApTransactionDate());
				    	int days=paymentTermsSetup.getDueDaysDate();
				    	cal.add(Calendar.DATE, days);
				    	dtoAPYTDOpenTransactions.setDueDate(UtilDateAndTime.dateToStringddmmyyyy(cal.getTime()));
				    }
				    
					double applyAmount=  repositoryAPApplyDocumentsPayments.getApplyAmountByApTransactionNumber(apytdOpenTransaction.getApTransactionNumber());
				    dtoAPYTDOpenTransactions.setApplyAmount(applyAmount);
				    double remainingAmount = apytdOpenTransaction.getApTransactionTotalAmount()-applyAmount;
				    dtoAPYTDOpenTransactions.setRemainingAmount(remainingAmount);
				    list.add(dtoAPYTDOpenTransactions);
				}
			}
		} 
		catch (Exception e) {
			LOG.error(e.getMessage());
		}
		dtoSearch.setRecords(list);
		return dtoSearch;
	}

	public DtoSearch getOpenTransactionsByPostingDateAndVendorId(DtoAPYTDOpenTransactions dtoAPYTDOpenTransaction) 
	{
		DtoSearch dtoSearch= new DtoSearch();
		dtoSearch.setPageNumber(0);
		dtoSearch.setPageSize(0);
		dtoSearch.setTotalCount(repositoryAPYTDOpenTransaction.getCountByPostingDateAndVendorId(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPYTDOpenTransaction.getPostingDate()),
				dtoAPYTDOpenTransaction.getVendorID()));
		List<DtoAPYTDOpenTransactions> list = new ArrayList<>();
		try 
		{
			List<APYTDOpenTransaction> apYTDOpenTransactionsList=null;
			if(dtoAPYTDOpenTransaction.getPageNumber()!=null && dtoAPYTDOpenTransaction.getPageSize()!=null && 
					dtoAPYTDOpenTransaction.getPageSize()>0)
			{
				dtoSearch.setPageNumber(dtoAPYTDOpenTransaction.getPageNumber());
				dtoSearch.setPageSize(dtoAPYTDOpenTransaction.getPageSize());
				Pageable pageable = new PageRequest(dtoAPYTDOpenTransaction.getPageNumber(), dtoAPYTDOpenTransaction.getPageSize());
				apYTDOpenTransactionsList = repositoryAPYTDOpenTransaction.findByPostingDateAndVendorId(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPYTDOpenTransaction.getPostingDate()),dtoAPYTDOpenTransaction.getVendorID(),pageable);
			}
			else
			{
				apYTDOpenTransactionsList = repositoryAPYTDOpenTransaction.findByPostingDateAndVendorId(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPYTDOpenTransaction.getPostingDate()),dtoAPYTDOpenTransaction.getVendorID());
			}
			
			if (apYTDOpenTransactionsList != null && !apYTDOpenTransactionsList.isEmpty()) {
				for (APYTDOpenTransaction apytdOpenTransaction : apYTDOpenTransactionsList) {
					DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions=new DtoAPYTDOpenTransactions(apytdOpenTransaction);
					PMDocumentsTypeSetup pmDocumentsTypeSetup = repositoryPMDocumentsTypeSetup.findByApDocumentTypeIdAndIsDeleted(apytdOpenTransaction.getApTransactionType(), false);
				    if(pmDocumentsTypeSetup!=null){
				    	dtoAPYTDOpenTransactions.setApTransactionType(pmDocumentsTypeSetup.getDocumentCode());
				    }
				  
				    PaymentTermsSetup paymentTermsSetup=repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(apytdOpenTransaction.getPaymentTermsID(), false);
				    if(paymentTermsSetup!=null){
				    	Calendar cal =Calendar.getInstance();
				    	cal.setTime(apytdOpenTransaction.getApTransactionDate());
				    	int days=paymentTermsSetup.getDueDaysDate();
				    	cal.add(Calendar.DATE, days);
				    	dtoAPYTDOpenTransactions.setDueDate(UtilDateAndTime.dateToStringddmmyyyy(cal.getTime()));
				    }
				    
				    double applyAmount=  repositoryAPApplyDocumentsPayments.getApplyAmountByApTransactionNumber(apytdOpenTransaction.getApTransactionNumber());
				    dtoAPYTDOpenTransactions.setApplyAmount(applyAmount);
				    double remainingAmount = apytdOpenTransaction.getApTransactionTotalAmount()-applyAmount;
				    dtoAPYTDOpenTransactions.setRemainingAmount(remainingAmount);
				    list.add(dtoAPYTDOpenTransactions);
				}
			}
		} 
		catch (Exception e) 
		{
			LOG.error(e.getMessage());
		}
		dtoSearch.setRecords(list);
		return dtoSearch;
	}
	
    public DtoAPYTDOpenTransactions applyTransaction(DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions) {
		APYTDOpenTransaction apytdOpenTransactions=repositoryAPYTDOpenTransaction.findByCashReceiptNumber(dtoAPYTDOpenTransactions.getPaymentNumber());
		if(apytdOpenTransactions!=null)
		{
			APPaymentTransactions apPaymentTransactions=repositoryAPPaymentTransactions.findByApTransactionNumberAndVendorId(dtoAPYTDOpenTransactions.getPaymentNumber(),apytdOpenTransactions.getVendorID());
			if(apPaymentTransactions==null){
				apPaymentTransactions=new APPaymentTransactions();
			}
			apPaymentTransactions.setApTransactionNumber(dtoAPYTDOpenTransactions.getPaymentNumber());
			apPaymentTransactions.setApTransactionDate(apytdOpenTransactions.getApTransactionDate());
			apPaymentTransactions.setApTransactionPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPYTDOpenTransactions.getPostingDate()));
			apPaymentTransactions.setVendorId(dtoAPYTDOpenTransactions.getVendorID());
			apPaymentTransactions.setCheckbookId(apytdOpenTransactions.getCheckbookID());
			apPaymentTransactions.setCurrencyId(apytdOpenTransactions.getCurrencyID());
			apPaymentTransactions.setExchangeTableIndex(String.valueOf(apytdOpenTransactions.getExchangeTableIndex()));
			apPaymentTransactions.setApPaymentAmount(dtoAPYTDOpenTransactions.getOriginalAmount());
			apPaymentTransactions.setApFunctionalPaymentAmount(dtoAPYTDOpenTransactions.getFunctionalAmount());
			//arPaymentTransactions.setAuditTrialCode(auditTrialCode); //Set audit trial code for Payment
			apPaymentTransactions.setPaymentVoid(apytdOpenTransactions.isTransactionVoid());
			apPaymentTransactions.setRowDateindex(new Date());
			repositoryAPPaymentTransactions.saveAndFlush(apPaymentTransactions);
			List<DtoAPYTDOpenTransactions> list=dtoAPYTDOpenTransactions.getApOpenTransactionList();
			if(!list.isEmpty())
			{
				for (DtoAPYTDOpenTransactions openTransactionApply : list) 
				{
					double oldApplyAmount=0.0;
					ApplyDocumentsPayments applyDocumentsPayments=repositoryAPApplyDocumentsPayments.findByApplyDocumentNumberPaymentNumberAndApTransactionNumber(dtoAPYTDOpenTransactions.getPaymentNumber(), openTransactionApply.getApTransactionNumber());
					if(applyDocumentsPayments==null){
						applyDocumentsPayments=new ApplyDocumentsPayments();
						
					}
					else
					{
						oldApplyAmount=applyDocumentsPayments.getApplyAmount();
					}
					
					double applyAmount=  repositoryAPApplyDocumentsPayments.
							  getApplyAmountByApTransactionNumber(openTransactionApply.getApTransactionNumber());
					 
					double remainingApplyAmount=openTransactionApply.getApplyAmount()-applyAmount;
					remainingApplyAmount=remainingApplyAmount+oldApplyAmount;
					applyDocumentsPayments.setApplyAmount(remainingApplyAmount);
					
					applyDocumentsPayments.setVendorNumber(dtoAPYTDOpenTransactions.getVendorID());
					applyDocumentsPayments.setApTransactionNumber(openTransactionApply.getApTransactionNumber());
					applyDocumentsPayments.setApTransactionDate(apytdOpenTransactions.getApTransactionDate());
					applyDocumentsPayments.setApTransactionPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPYTDOpenTransactions.getPostingDate()));
					applyDocumentsPayments.setPaymentAlreadyPosted(1); //1 for posted
					applyDocumentsPayments.setApplyDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPYTDOpenTransactions.getApplyDate()));
					applyDocumentsPayments.setOriginalAmount(openTransactionApply.getOriginalAmount()!=null?
							openTransactionApply.getOriginalAmount():0.0);
					applyDocumentsPayments.setRemainAmountAfterApply(openTransactionApply.getRemainingAmount()!=null?
							openTransactionApply.getRemainingAmount():0.0);
					applyDocumentsPayments.setModifyByUserID(httpServletRequest.getHeader(USER_ID));
					applyDocumentsPayments.setApplyDocumentNumberPaymentNumber(dtoAPYTDOpenTransactions.getPaymentNumber());
					applyDocumentsPayments.setRowDateIndex(new Date());
					repositoryAPApplyDocumentsPayments.saveAndFlush(applyDocumentsPayments);
				}
			}
			return dtoAPYTDOpenTransactions;
		}
		return null;
	}

	public boolean unApplyTransaction(DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions) 
	{
		List<DtoAPYTDOpenTransactions> list = dtoAPYTDOpenTransactions.getApOpenTransactionList();
		if(!list.isEmpty()){
			for (DtoAPYTDOpenTransactions transactions : list) {
				ApplyDocumentsPayments apApplyDocumentsPayments=repositoryAPApplyDocumentsPayments.findByApplyDocumentNumberPaymentNumberAndApTransactionNumber(dtoAPYTDOpenTransactions.getPaymentNumber(), transactions.getApTransactionNumber());
				if(apApplyDocumentsPayments!=null){
					repositoryAPApplyDocumentsPayments.delete(apApplyDocumentsPayments);
				}
			}
			return true;
		}
		return false;
	}
	
	public DtoAPYTDOpenTransactions getAmountByPaymentNumber(DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions) throws ScriptException {
		APYTDOpenTransaction  apytdOpenTransactions= repositoryAPYTDOpenTransaction.findByCashReceiptNumber(dtoAPYTDOpenTransactions.getPaymentNumber());
		if(apytdOpenTransactions!=null)
		{
			dtoAPYTDOpenTransactions.setPostingDate(UtilDateAndTime.dateToStringddmmyyyy(apytdOpenTransactions.getPostingDate()));
			dtoAPYTDOpenTransactions.setOriginalAmount(apytdOpenTransactions.getApTransactionTotalAmount());
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			double amount =apytdOpenTransactions.getApTransactionTotalAmount();
			String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
			if(UtilRandomKey.isNotBlank(apytdOpenTransactions.getExchangeTableIndex())){
				CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(apytdOpenTransactions.getExchangeTableIndex()), false);
			    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
				   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
			    }
			}
			Double functionalAmount =amount;
            double exchangeTableRate= apytdOpenTransactions.getExchangeTableRate();
            if(exchangeTableRate>0.0){
            	functionalAmount=(double) mathEval.eval(amount + calcMethod + exchangeTableRate);
            }
			dtoAPYTDOpenTransactions.setFunctionalAmount(functionalAmount);
			return dtoAPYTDOpenTransactions;
		}
		return null;
	}
	
	public List<DtoAPYTDOpenTransactions> getDocumentNumberByVendorIdAndDocType(DtoAPYTDOpenTransactions dtoAPYTDOpenTransactions) {

		List<DtoAPYTDOpenTransactions> list = new ArrayList<>();
		try {
			List<APYTDOpenTransaction> apytdOpenTransactionsList = null;
			if(ARMasterDocumentTypeConstant.PURCHASE.getIndex()==dtoAPYTDOpenTransactions.getDocumentType()){
				apytdOpenTransactionsList=repositoryAPYTDOpenTransaction.
						findByVendorIDAndIsPaymentAndPurchasesAmountGreaterThan(
								dtoAPYTDOpenTransactions.getVendorID(),true, 0.0);
			}
			if (apytdOpenTransactionsList != null && !apytdOpenTransactionsList.isEmpty()) {
				for (APYTDOpenTransaction apytdOpenTransactions2 : apytdOpenTransactionsList) {
					if(UtilRandomKey.isNotBlank(apytdOpenTransactions2.getCashReceiptNumber())){
						DtoAPYTDOpenTransactions arYTDOpenTransactions=	 new DtoAPYTDOpenTransactions();
						arYTDOpenTransactions.setPaymentNumber(apytdOpenTransactions2.getCashReceiptNumber());
						list.add(arYTDOpenTransactions);
					}
				}
			}
		} 
		catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}
	
}
