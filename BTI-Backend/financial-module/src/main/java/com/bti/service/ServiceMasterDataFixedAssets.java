/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.FAAccountTableAccountType;
import com.bti.model.FAAccountTableSetup;
import com.bti.model.FABookClassSetup;
import com.bti.model.FABookMaintenance;
import com.bti.model.FABookMaintenanceAmortizationCodeType;
import com.bti.model.FABookMaintenanceAveragingConventionType;
import com.bti.model.FABookMaintenanceSwitchOverType;
import com.bti.model.FADepreciationMethods;
import com.bti.model.FAGeneralMaintenance;
import com.bti.model.FAInsuranceMaintenance;
import com.bti.model.FALeaseCompanySetup;
import com.bti.model.FALeaseMaintenance;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.LeaseType;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.dto.DtoAccountTableSetup;
import com.bti.model.dto.DtoFABookMaintenance;
import com.bti.model.dto.DtoFALeaseMaintenance;
import com.bti.model.dto.DtoInsuranceMaintenance;
import com.bti.model.dto.DtoLeaseType;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryBookSetup;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryFAAccountGroupSetup;
import com.bti.repository.RepositoryFAAccountTableAccountType;
import com.bti.repository.RepositoryFAAccountTableSetup;
import com.bti.repository.RepositoryFABookClassSetup;
import com.bti.repository.RepositoryFABookMaintenance;
import com.bti.repository.RepositoryFABookMaintenanceAmortizationCodeType;
import com.bti.repository.RepositoryFABookMaintenanceAveragingConventionType;
import com.bti.repository.RepositoryFABookMaintenanceSwitchOverType;
import com.bti.repository.RepositoryFADepreciationMethods;
import com.bti.repository.RepositoryFAGeneralMaintenance;
import com.bti.repository.RepositoryFAInsuranceClassSetup;
import com.bti.repository.RepositoryFAInsuranceMaintenance;
import com.bti.repository.RepositoryFALeaseCompanySetup;
import com.bti.repository.RepositoryFALeaseMaintenance;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryLeaseType;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;

/**
 * Description: Service Fixed Assets
 * Name of Project: BTI
 * Created on: Aug 22, 2017
 * Modified on: Aug 22, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service("serviceMasterDataFixedAssets")
public class ServiceMasterDataFixedAssets {

	private static final Logger LOG = Logger.getLogger(ServiceMasterDataFixedAssets.class);
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryFABookMaintenance repositoryFABookMaintenance;
	@Autowired
	RepositoryBookSetup repositoryBookSetup;
	@Autowired
	RepositoryFADepreciationMethods repositoryFADepreciationMethods;
	@Autowired
	RepositoryFABookMaintenanceAveragingConventionType repositoryFABookMaintenanceAveragingConventionType;
	@Autowired
	RepositoryFABookMaintenanceSwitchOverType repositoryFABookMaintenanceSwitchOverType;
	@Autowired
	RepositoryFABookMaintenanceAmortizationCodeType repositoryFABookMaintenanceAmortizationCodeType;
	
	@Autowired
	RepositoryFAInsuranceMaintenance repositoryFAInsuranceMaintenance;
	
	@Autowired
	RepositoryFAGeneralMaintenance repositoryFAGeneralMaintenance;
	
	@Autowired
	RepositoryLeaseType repositoryLeaseType; 
	
	@Autowired
	RepositoryFALeaseMaintenance repositoryFALeaseMaintenance;
	
	@Autowired
	RepositoryFALeaseCompanySetup repositoryFALeaseCompanySetup; 
	@Autowired
	RepositoryFAAccountTableAccountType repositoryFAAccountTableAccountType;
	@Autowired
	RepositoryFAAccountTableSetup repositoryFAAccountTableSetup;
	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	@Autowired
	RepositoryFAAccountGroupSetup repositoryFAAccountGroupSetup;
	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	@Autowired
	RepositoryFAInsuranceClassSetup repositoryFAInsuranceClassSetup;
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	@Autowired
	ServiceHome serviceHome;
	@Autowired
	RepositoryFABookClassSetup repositoryFABookClassSetup;
	
	/**
	 * @param dtoFABookMaintenance
	 * @return
	 */
	public DtoFABookMaintenance saveBookMaintenance(DtoFABookMaintenance dtoFABookMaintenance) {
		int loggedInUserId=0;
		int langId = serviceHome.getLanngugaeId();
		if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
			loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		
		try{
		FABookMaintenance faBookMaintenance = repositoryFABookMaintenance.findByAssetIdAndIsDeleted(dtoFABookMaintenance.getAssetId(),false);
		
		if(faBookMaintenance!=null){
			dtoFABookMaintenance.setMessageType("BOOK_MAINTENANCE_ALREADY_EXIST");
			return dtoFABookMaintenance;
		}else{
			faBookMaintenance = new FABookMaintenance();
			faBookMaintenance.setStaus(dtoFABookMaintenance.getStatus());
			faBookMaintenance.setAmortizationAmount(dtoFABookMaintenance.getAmortizationAmount());
			faBookMaintenance.setAmortizationCodeType(repositoryFABookMaintenanceAmortizationCodeType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId(dtoFABookMaintenance.getAmortizationCode(),false,langId));
			faBookMaintenance.setAssetId(dtoFABookMaintenance.getAssetId());
			faBookMaintenance.setAveragingConventionType(repositoryFABookMaintenanceAveragingConventionType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFABookMaintenance.getAveragingConvention(),langId,false));
			faBookMaintenance.setBeginYearCost(dtoFABookMaintenance.getBeginYearCost());
			faBookMaintenance.setBookSetup(repositoryBookSetup.findByBookIdAndIsDeleted(dtoFABookMaintenance.getBookId(),false));
			faBookMaintenance.setCostBasis(dtoFABookMaintenance.getCostBasis());
			faBookMaintenance.setCurrentDepreciation(dtoFABookMaintenance.getCurrentDepreciation());
			faBookMaintenance.setCreatedBy(loggedInUserId);
			if(UtilRandomKey.isNotBlank(dtoFABookMaintenance.getDepreciatedDate())){
				faBookMaintenance.setDepreciatedDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFABookMaintenance.getDepreciatedDate()));
			}
		
			faBookMaintenance.setFaDepreciationMethods(repositoryFADepreciationMethods.findByDepreciationMethodIdAndIsDeleted(dtoFABookMaintenance.getDepreciationMethodId(),false));
			faBookMaintenance.setFullDepreciationFlag(dtoFABookMaintenance.getFullDepreciationFlag());
			faBookMaintenance.setLifeToDateDepreciation(dtoFABookMaintenance.getLifeToDateDepreciation());
			faBookMaintenance.setNetFAValue(dtoFABookMaintenance.getNetFAValue());
			faBookMaintenance.setOriginalLifeDay(dtoFABookMaintenance.getOriginalLifeDay());
			faBookMaintenance.setOriginalLifeYear(dtoFABookMaintenance.getOriginalLifeYear());
			if(UtilRandomKey.isNotBlank(dtoFABookMaintenance.getPlacedInServiceDate())){
				faBookMaintenance.setPlacedInServiceDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFABookMaintenance.getPlacedInServiceDate()));
			}
			faBookMaintenance.setRemainingLifeDay(dtoFABookMaintenance.getRemainingLifeDay());
			faBookMaintenance.setRemainingLifeYear(dtoFABookMaintenance.getRemainingLifeYear());
			faBookMaintenance.setSalvageAmount(dtoFABookMaintenance.getSalvageAmount());
			faBookMaintenance.setSwitchOverType(repositoryFABookMaintenanceSwitchOverType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFABookMaintenance.getSwitchOver(),langId,false));
			faBookMaintenance.setYearlyDepreciatedRate(dtoFABookMaintenance.getYearlyDepreciatedRate());
			faBookMaintenance.setYearToDateDepreciation(dtoFABookMaintenance.getYearToDateDepreciation());
			repositoryFABookMaintenance.saveAndFlush(faBookMaintenance);
			return dtoFABookMaintenance;
			
		}
		
		}catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoFABookMaintenance
	 * @return
	 */
	public DtoFABookMaintenance updateBookMaintenance(DtoFABookMaintenance dtoFABookMaintenance) {
		int loggedInUserId=0;
		int langid = serviceHome.getLanngugaeId();
		if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
			loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		
		try{
		FABookMaintenance faBookMaintenance = repositoryFABookMaintenance.findByAssetIdAndIsDeleted(dtoFABookMaintenance.getAssetId(),false);
		if(faBookMaintenance!=null)
		{
			faBookMaintenance = new FABookMaintenance();
			faBookMaintenance.setAmortizationAmount(dtoFABookMaintenance.getAmortizationAmount());
			faBookMaintenance.setAmortizationCodeType(repositoryFABookMaintenanceAmortizationCodeType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId(dtoFABookMaintenance.getAmortizationCode(),false,langid));
			faBookMaintenance.setAssetId(dtoFABookMaintenance.getAssetId());
			faBookMaintenance.setAveragingConventionType(repositoryFABookMaintenanceAveragingConventionType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFABookMaintenance.getAveragingConvention(),langid,false));
			faBookMaintenance.setBeginYearCost(dtoFABookMaintenance.getBeginYearCost());
			faBookMaintenance.setBookSetup(repositoryBookSetup.findByBookIdAndIsDeleted(dtoFABookMaintenance.getBookId(),false));
			faBookMaintenance.setCostBasis(dtoFABookMaintenance.getCostBasis());
			faBookMaintenance.setCurrentDepreciation(dtoFABookMaintenance.getCurrentDepreciation());
			faBookMaintenance.setChangeBy(loggedInUserId);
			if(UtilRandomKey.isNotBlank(dtoFABookMaintenance.getDepreciatedDate()))
			{
				faBookMaintenance.setDepreciatedDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFABookMaintenance.getDepreciatedDate()));
			}
			faBookMaintenance.setFaDepreciationMethods(repositoryFADepreciationMethods.findByDepreciationMethodIdAndIsDeleted(dtoFABookMaintenance.getDepreciationMethodId(),false));
			faBookMaintenance.setStaus(dtoFABookMaintenance.getStatus());
			faBookMaintenance.setFullDepreciationFlag(dtoFABookMaintenance.getFullDepreciationFlag());
			faBookMaintenance.setLifeToDateDepreciation(dtoFABookMaintenance.getLifeToDateDepreciation());
			faBookMaintenance.setNetFAValue(dtoFABookMaintenance.getNetFAValue());
			faBookMaintenance.setOriginalLifeDay(dtoFABookMaintenance.getOriginalLifeDay());
			faBookMaintenance.setOriginalLifeYear(dtoFABookMaintenance.getOriginalLifeYear());
			if(UtilRandomKey.isNotBlank(dtoFABookMaintenance.getPlacedInServiceDate())){
				faBookMaintenance.setPlacedInServiceDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFABookMaintenance.getPlacedInServiceDate()));
			}
			faBookMaintenance.setRemainingLifeDay(dtoFABookMaintenance.getRemainingLifeDay());
			faBookMaintenance.setRemainingLifeYear(dtoFABookMaintenance.getRemainingLifeYear());
			faBookMaintenance.setSalvageAmount(dtoFABookMaintenance.getSalvageAmount());
			faBookMaintenance.setSwitchOverType(repositoryFABookMaintenanceSwitchOverType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFABookMaintenance.getSwitchOver(),langid,false));
			faBookMaintenance.setYearlyDepreciatedRate(dtoFABookMaintenance.getYearlyDepreciatedRate());
			faBookMaintenance.setYearToDateDepreciation(dtoFABookMaintenance.getYearToDateDepreciation());
			repositoryFABookMaintenance.saveAndFlush(faBookMaintenance);
			return dtoFABookMaintenance;
		}
		
		}
		catch(Exception e)
		{
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoFABookMaintenance
	 * @return
	 */
	public DtoFABookMaintenance getById(DtoFABookMaintenance dtoFABookMaintenance) {
		FABookMaintenance faBookMaintenance = repositoryFABookMaintenance.findByAssetIdAndIsDeleted(dtoFABookMaintenance.getAssetId(),false);
		if(faBookMaintenance!=null){
			return new DtoFABookMaintenance(faBookMaintenance);
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchBookMaintenance(DtoSearch dtoSearchs) {

		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
	 	dtoSearch.setTotalCount(repositoryFABookMaintenance.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		List<FABookMaintenance> faBookMaintenanceList = null;
		List<DtoFABookMaintenance> dtoFABookMaintenanceList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			faBookMaintenanceList = repositoryFABookMaintenance
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			faBookMaintenanceList = repositoryFABookMaintenance
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (faBookMaintenanceList != null) {
			for (FABookMaintenance faBookMaintenance : faBookMaintenanceList) {
				dtoFABookMaintenanceList.add(new DtoFABookMaintenance(faBookMaintenance));
			}
		}
		dtoSearch.setRecords(dtoFABookMaintenanceList);
		return dtoSearch;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getAveragingConventionType() {
		int langId=serviceHome.getLanngugaeId();
		List<FABookMaintenanceAveragingConventionType> list = repositoryFABookMaintenanceAveragingConventionType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if(list!=null){
			for(FABookMaintenanceAveragingConventionType faBookMaintenanceAveragingConventionType : list){
				responseList.add(new DtoStatusType(faBookMaintenanceAveragingConventionType));
			}
		}
		return responseList;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getSwitchOverType() {
		int langId=serviceHome.getLanngugaeId();
		List<FABookMaintenanceSwitchOverType> list = repositoryFABookMaintenanceSwitchOverType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if(list!=null){
			for(FABookMaintenanceSwitchOverType faBookMaintenanceSwitchOverType : list){
				responseList.add(new DtoStatusType(faBookMaintenanceSwitchOverType));
			}
		}
		return responseList;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getAmortizationCodeType() {
		int langId=serviceHome.getLanngugaeId();
		List<FABookMaintenanceAmortizationCodeType> list = repositoryFABookMaintenanceAmortizationCodeType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if(list!=null){
			for(FABookMaintenanceAmortizationCodeType faBookMaintenanceAmortizationCodeType : list){
				responseList.add(new DtoStatusType(faBookMaintenanceAmortizationCodeType));
			}
		}
		return responseList;
	}
	
	/**
	 * @return
	 */
	public List<DtoStatusType> getDepreciationMethodType() {
		List<FADepreciationMethods> list = repositoryFADepreciationMethods.findAll();
		List<DtoStatusType> responseList = new ArrayList<>();
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		if(list!=null){
			for(FADepreciationMethods faDepreciationMethods : list){
				responseList.add(new DtoStatusType(faDepreciationMethods,langId));
			}
		}
		return responseList;
	}

	/**
	 * @Description saveInsuranceMaintenence
	 * @param dtoInsuranceMaintenance
	 * @return
	 */
	public DtoInsuranceMaintenance saveInsuranceMaintenence(DtoInsuranceMaintenance dtoInsuranceMaintenance) {
		FAInsuranceMaintenance faInsuranceMaintenance = null;
		int langId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.LANG_ID));
		try {
		
			FAGeneralMaintenance faGeneralMaintenance = repositoryFAGeneralMaintenance
					.findByAssetIdAndIsDeleted(dtoInsuranceMaintenance.getAssetId(),false);
				faInsuranceMaintenance=new FAInsuranceMaintenance();
				faInsuranceMaintenance.setCreatedBy(langId);
				faInsuranceMaintenance.setFaInsuranceClassSetup(repositoryFAInsuranceClassSetup.findByInsuranceClassId(dtoInsuranceMaintenance.getInsClassId()));
				faInsuranceMaintenance.setInsuranceYears(dtoInsuranceMaintenance.getInsuranceYear());
				faInsuranceMaintenance.setInsuranceValue(dtoInsuranceMaintenance.getInsuranceValue());
				faInsuranceMaintenance.setReplacementCost(dtoInsuranceMaintenance.getReplacementCost());
				faInsuranceMaintenance.setReproductionCost(dtoInsuranceMaintenance.getReproductionCost());
				faInsuranceMaintenance.setFaGeneralMaintenance(faGeneralMaintenance);
				repositoryFAInsuranceMaintenance.saveAndFlush(faInsuranceMaintenance);
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}

		return new DtoInsuranceMaintenance(faInsuranceMaintenance);
	}

	/**
	 * @Description updateInsuranceMaintenance
	 * @param dtoInsuranceMaintenance
	 * @param faInsuranceMaintenance
	 * @return
	 */
	public DtoInsuranceMaintenance updateInsuranceMaintenance(DtoInsuranceMaintenance dtoInsuranceMaintenance,
			FAInsuranceMaintenance faInsuranceMaintenance) {
		int langId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.LANG_ID));
		try {
			FAGeneralMaintenance faGeneralMaintenance = repositoryFAGeneralMaintenance
					.findByAssetIdAndIsDeleted(dtoInsuranceMaintenance.getAssetId(),false);
			faInsuranceMaintenance.setChangeBy(langId);
			faInsuranceMaintenance.setFaInsuranceClassSetup(repositoryFAInsuranceClassSetup.findByInsuranceClassId(dtoInsuranceMaintenance.getInsClassId()));
			faInsuranceMaintenance.setInsuranceYears(dtoInsuranceMaintenance.getInsuranceYear());
			faInsuranceMaintenance.setInsuranceValue(dtoInsuranceMaintenance.getInsuranceValue());
			faInsuranceMaintenance.setReplacementCost(dtoInsuranceMaintenance.getReplacementCost());
			faInsuranceMaintenance.setReproductionCost(dtoInsuranceMaintenance.getReproductionCost());
			faInsuranceMaintenance.setFaGeneralMaintenance(faGeneralMaintenance);
			repositoryFAInsuranceMaintenance.saveAndFlush(faInsuranceMaintenance);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}

		return new DtoInsuranceMaintenance(faInsuranceMaintenance);
	}

	/**
	 * @Description insuranceMaintenanceGetById
	 * @param faInsuranceMaintenance
	 * @return
	 */
	public DtoInsuranceMaintenance insuranceMaintenanceGetById(FAInsuranceMaintenance faInsuranceMaintenance) {
		return new DtoInsuranceMaintenance(faInsuranceMaintenance);
	}

	/**
	 * @Description insuranceMaintenanceSearch
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch insuranceMaintenanceSearch(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryFAInsuranceMaintenance.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		DtoInsuranceMaintenance dtoInsuranceMaintenance = null;
		List<FAInsuranceMaintenance> list = null;
		List<DtoInsuranceMaintenance> list2 = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			list = repositoryFAInsuranceMaintenance
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			list = repositoryFAInsuranceMaintenance
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (list != null) {
			for (FAInsuranceMaintenance checkbookMaintenance : list) {
				dtoInsuranceMaintenance = new DtoInsuranceMaintenance(checkbookMaintenance);
				list2.add(dtoInsuranceMaintenance);
			}
		}
		dtoSearch.setRecords(list2);
		return dtoSearch;
	}

	/**
	 * @Description getAllLeaseType
	 * @return
	 */
	public List<DtoLeaseType> getAllLeaseType() {
		int langId = serviceHome.getLanngugaeId();
		List<LeaseType> leaseTypes = repositoryLeaseType.findByIsDeletedAndLanguageLanguageId(false,langId);
		List<DtoLeaseType> dtoLeaseTypes = new ArrayList<>();
		DtoLeaseType dtoLeaseType;
		for(LeaseType leaseType : leaseTypes){
			dtoLeaseType = new DtoLeaseType(leaseType);
			dtoLeaseTypes.add(dtoLeaseType);
		}
		return dtoLeaseTypes;
	}

	/**
	 * @Description saveLeaseMaintenence
	 * @param faLeaseMaintenance
	 * @return
	 */
	public DtoFALeaseMaintenance saveLeaseMaintenence(DtoFALeaseMaintenance faLeaseMaintenance) {
		FALeaseMaintenance leaseMaintenance = new FALeaseMaintenance();
		FALeaseCompanySetup faLeaseCompanySetup = repositoryFALeaseCompanySetup.getOne(faLeaseMaintenance.getLeaseCompanyIndex()); 
		try 
		{
			LeaseType leaseType = repositoryLeaseType.findOne(faLeaseMaintenance.getLeaseTypeIndex());
			int langId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.LANG_ID));
			leaseMaintenance.setAssetId(faLeaseMaintenance.getAssetId());
			leaseMaintenance.setCreatedBy(langId);
			leaseMaintenance.setFaLeaseCompanySetup(faLeaseCompanySetup);
			leaseMaintenance.setAsstserid(faLeaseMaintenance.getAssetSerialId());
			leaseMaintenance.setLeaseType(leaseType);
			leaseMaintenance.setLeaseContractNumber(faLeaseMaintenance.getLeaseContactNumber());
			leaseMaintenance.setLeasePayment(faLeaseMaintenance.getLeasePayment());
			leaseMaintenance.setInterestRatePercent(faLeaseMaintenance.getInterestRatePercent());
			leaseMaintenance.setLeaseEndDate(UtilDateAndTime.ddmmyyyyStringToDate(faLeaseMaintenance.getLeaseEndDate()));
			leaseMaintenance = repositoryFALeaseMaintenance.saveAndFlush(leaseMaintenance);
		} 
		catch (Exception e) 
		{
			LOG.info(Arrays.toString(e.getStackTrace()));
		}

		return new DtoFALeaseMaintenance(leaseMaintenance);
	}

	/**
	 * @Description updateLeaseMaintenence
	 * @param faLeaseMaintenance
	 * @param leaseMaintenance
	 * @return
	 */
	public DtoFALeaseMaintenance updateLeaseMaintenence(DtoFALeaseMaintenance faLeaseMaintenance,
			FALeaseMaintenance leaseMaintenance) {
		FALeaseCompanySetup faLeaseCompanySetup = repositoryFALeaseCompanySetup.getOne(faLeaseMaintenance.getLeaseCompanyIndex());
		try {
			
			LeaseType leaseType = repositoryLeaseType.findOne(faLeaseMaintenance.getLeaseTypeIndex());
			int langId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.LANG_ID));
			leaseMaintenance.setAssetId(faLeaseMaintenance.getAssetId());
			leaseMaintenance.setChangeBy(langId);
			leaseMaintenance.setFaLeaseCompanySetup(faLeaseCompanySetup);
			leaseMaintenance.setAsstserid(faLeaseMaintenance.getAssetSerialId());
			leaseMaintenance.setLeaseType(leaseType);
			leaseMaintenance.setLeaseContractNumber(faLeaseMaintenance.getLeaseContactNumber());
			leaseMaintenance.setLeasePayment(faLeaseMaintenance.getLeasePayment());
			leaseMaintenance.setInterestRatePercent(faLeaseMaintenance.getInterestRatePercent());
			leaseMaintenance.setLeaseEndDate(UtilDateAndTime.ddmmyyyyStringToDate(faLeaseMaintenance.getLeaseEndDate()));
			leaseMaintenance = repositoryFALeaseMaintenance.saveAndFlush(leaseMaintenance);
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}

		return new DtoFALeaseMaintenance(leaseMaintenance);
	}

	/**
	 * @Description leaseMaintenenceGetById
	 * @param leaseMaintenance
	 * @return
	 */
	public DtoFALeaseMaintenance leaseMaintenenceGetById(FALeaseMaintenance leaseMaintenance) {
		return new DtoFALeaseMaintenance(leaseMaintenance);
	}

	/**
	 * @Description leaseMaintenanceSearch
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch leaseMaintenanceSearch(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		try {
			dtoSearch.setTotalCount(
					repositoryFALeaseMaintenance.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
			DtoFALeaseMaintenance dtoInsuranceMaintenance = null;
			List<FALeaseMaintenance> list = null;
			List<DtoFALeaseMaintenance> list2 = new ArrayList<>();
			if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
						"createdDate");
				list = repositoryFALeaseMaintenance
						.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
			}
			else 
			{
				list = repositoryFALeaseMaintenance.predictiveSearch(dtoSearchs.getSearchKeyword());
						
			}
			if (list != null) {
				for (FALeaseMaintenance checkbookMaintenance : list) {
					dtoInsuranceMaintenance = new DtoFALeaseMaintenance(checkbookMaintenance);
					list2.add(dtoInsuranceMaintenance);
				}
			}
			dtoSearch.setRecords(list2);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		return dtoSearch;
	}

	/**
	 * @Description get Master account type
	 * @return
	 */
	public List<DtoStatusType> getAllAccountType() {
		int langId=serviceHome.getLanngugaeId();
		List<FAAccountTableAccountType> faAccountTableAccountTypes = repositoryFAAccountTableAccountType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> dtoStatusTypes = new ArrayList<>();
		for(FAAccountTableAccountType faAccountTableAccountType : faAccountTableAccountTypes){
			dtoStatusTypes.add(new DtoStatusType(faAccountTableAccountType));
		}
		return dtoStatusTypes;
	}

	/**
	 * @Description get account type setup
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public DtoAccountTableSetup getAccountTableSetup() {

		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		DtoAccountTableSetup main= null;
		List<FAAccountTableSetup> faAccountTableSetupList = repositoryFAAccountTableSetup.findAll();
		List<DtoAccountTableSetup> list = new ArrayList<>();
		if(faAccountTableSetupList!=null && !faAccountTableSetupList.isEmpty())
		{
			main=new DtoAccountTableSetup();
			for (FAAccountTableSetup faAccountTableSetup : faAccountTableSetupList) 
			{
				List<Long> accountNumberIndex = new ArrayList<>();
				DtoAccountTableSetup dtoAccountTableSetup= new DtoAccountTableSetup();
				main.setAssetId("");
				main.setAssetDescription("");
				main.setAssetDescriptionArabic("");
				main.setAccountDescription("");
				main.setAccountDescriptionArabic("");
				if(faAccountTableSetup.getFaGeneralMaintenance()!=null){
					main.setAssetId(faAccountTableSetup.getFaGeneralMaintenance().getAssetId());
					main.setAssetDescription(faAccountTableSetup.getFaGeneralMaintenance().getFADescription());
					main.setAssetDescriptionArabic(faAccountTableSetup.getFaGeneralMaintenance().getFADescriptionArabic());
				}
				if(faAccountTableSetup.getFaAccountGroupsSetup()!=null){
					main.setAccountGroupIndex(faAccountTableSetup.getFaAccountGroupsSetup().getFaAccountGroupIndex());
					main.setAccountDescription(faAccountTableSetup.getFaAccountGroupsSetup().getFaAccountGroupDescription());
					main.setAccountDescriptionArabic(faAccountTableSetup.getFaAccountGroupsSetup().getFaAccountGroupDescriptionArabic());
				}
				
				dtoAccountTableSetup.setAccountDescription("");
				dtoAccountTableSetup.setAccountType(0);
				dtoAccountTableSetup.setAccountTypeValue("");
				dtoAccountTableSetup.setAssetId("");
				if(faAccountTableSetup.getFaGeneralMaintenance()!=null){
					dtoAccountTableSetup.setAssetId(faAccountTableSetup.getFaGeneralMaintenance().getAssetId());
				}
				
				FAAccountTableAccountType accountTableAccountType = faAccountTableSetup.getFaAccountTableAccountType();
				if(accountTableAccountType!=null){
							int lang = Integer.parseInt(langId);
					if(accountTableAccountType.getLanguage().getLanguageId()!=lang){
						accountTableAccountType = repositoryFAAccountTableAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(accountTableAccountType.getTypeId(),lang,false);
					}
					if(accountTableAccountType!=null)
					{
						dtoAccountTableSetup
						.setAccountType(accountTableAccountType.getTypeId());
						dtoAccountTableSetup
							.setAccountTypeValue(accountTableAccountType.getTypePrimary());
					}
				}
				
				GLAccountsTableAccumulation glAccountsTableAccumulation = faAccountTableSetup.getGlAccountsTableAccumulation();
				String accountNumber="";
				List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
				if(glAccountsTableAccumulation!=null)
				{
					dtoAccountTableSetup.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
						if(UtilRandomKey.isNotBlank(glAccountsTableAccumulation.getAccountDescription())){
							dtoAccountTableSetup.setAccountDescription(glAccountsTableAccumulation.getAccountDescription());
						}
					
						Object[] arrayOfObject = serviceAccountPayable.getAccountNumberMultipleWays(glAccountsTableAccumulation,langId);
						accountNumber=(String) arrayOfObject[0];
						accountNumberIndex=(List<Long>) arrayOfObject[1];
						listOfKeyValue=(List<DtoPayableAccountClassSetup>) arrayOfObject[2];
				}
				dtoAccountTableSetup.setAccountNumberIndex(accountNumberIndex);
				dtoAccountTableSetup.setAccountNumber(accountNumber);
				dtoAccountTableSetup.setAccNumberIndexValue(listOfKeyValue);
				list.add(dtoAccountTableSetup);
			}
			main.setAccountNumberList(list);
		}
		return main;
	}

	/**
	 * @Description save account type setup
	 * @param dtoAccountTableSetup
	 * @return
	 */
	public DtoAccountTableSetup saveAccountTableSetup(DtoAccountTableSetup dtoAccountTableSetup) {
		int loggedInUserId=0;
		if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
			loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		int langid = serviceHome.getLanngugaeId();
		GLAccountsTableAccumulation glAccountsTableAccumulation = null;
		try {
			repositoryFAAccountTableSetup.deleteRecordById(dtoAccountTableSetup.getAccountGroupIndex(),dtoAccountTableSetup.getAssetId(), true);
			if (dtoAccountTableSetup.getAccountNumberList() != null
					&& !dtoAccountTableSetup.getAccountNumberList().isEmpty()) {
				for (DtoAccountTableSetup accountNumber : dtoAccountTableSetup.getAccountNumberList()) {
					FAAccountTableSetup faAccountTableSetup =repositoryFAAccountTableSetup.
							findByFaAccountTableAccountTypeTypeIdAndFaAccountGroupsSetupFaAccountGroupIndexAndFaGeneralMaintenanceAssetId
							(accountNumber.getAccountType(),dtoAccountTableSetup.getAccountGroupIndex(),dtoAccountTableSetup.getAssetId());
					if(faAccountTableSetup==null){
						faAccountTableSetup = new FAAccountTableSetup();
					}
					faAccountTableSetup.setDeleted(false);
					faAccountTableSetup.setFaAccountGroupsSetup(repositoryFAAccountGroupSetup
							.findByFaAccountGroupIndexAndIsDeleted(dtoAccountTableSetup.getAccountGroupIndex(),false));
					faAccountTableSetup.setFaGeneralMaintenance(
							repositoryFAGeneralMaintenance.findByAssetIdAndIsDeleted(dtoAccountTableSetup.getAssetId(),false));
					faAccountTableSetup.setFaAccountTableAccountType(
							repositoryFAAccountTableAccountType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(accountNumber.getAccountType(),langid,false));
					faAccountTableSetup.setCreatedBy(loggedInUserId);
					
					StringBuilder sb = new StringBuilder();
					String fullIndex = "";
					if (accountNumber.getAccountNumberIndex() != null
							&& !accountNumber.getAccountNumberIndex().isEmpty()) {
						for (Long i : accountNumber.getAccountNumberIndex()) {
							sb.append(String.valueOf(i));
						}
						fullIndex = sb.toString();
					}
					List<GLAccountsTableAccumulation> glAccountsTableAccumulationsList = repositoryGLAccountsTableAccumulation
							.findByAccountNumberFullIndexAndIsDeleted(fullIndex,false);
					if (glAccountsTableAccumulationsList != null && !glAccountsTableAccumulationsList.isEmpty()) {
						glAccountsTableAccumulation = glAccountsTableAccumulationsList.get(0);
						glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
					} else {
						glAccountsTableAccumulation = new GLAccountsTableAccumulation();
						int actRowIndexId = 1;
						PayableAccountClassSetup payableAccountClassSetup2 = repositoryPayableAccountClassSetup
								.findFirstByOrderByIdDesc();
						if (payableAccountClassSetup2 != null) {
							actRowIndexId = payableAccountClassSetup2.getAccountRowIndex();
							actRowIndexId = actRowIndexId + 1;
						}
						int segmentNumber = 1;
						if (accountNumber.getAccountNumberIndex() != null
								&& !accountNumber.getAccountNumberIndex().isEmpty()) {
							StringBuilder accountNumberfullIndex = new StringBuilder("");
							for (Long indexId : accountNumber.getAccountNumberIndex()) {
								accountNumberfullIndex.append(String.valueOf(indexId));
								PayableAccountClassSetup payableAccountClassSetup = new PayableAccountClassSetup();
								payableAccountClassSetup.setIndexId(indexId);
								payableAccountClassSetup.setSegmentNumber(segmentNumber);
								segmentNumber++;
								payableAccountClassSetup.setAccountRowIndex(actRowIndexId);
								payableAccountClassSetup.setCreatedBy(loggedInUserId);

								repositoryPayableAccountClassSetup.save(payableAccountClassSetup);
							}
							glAccountsTableAccumulation.setAccountTableRowIndex(String.valueOf(actRowIndexId));
							glAccountsTableAccumulation.setSegmentNumber(segmentNumber);
							glAccountsTableAccumulation.setAccountNumberFullIndex(accountNumberfullIndex.toString());
							glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
							if (accountNumber.getTransactionType() != null) {
								glAccountsTableAccumulation.setTransactionType(accountNumber.getTransactionType());
							}
							glAccountsTableAccumulation.setCreatedBy(loggedInUserId);
							
						}
					}
					    glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation.saveAndFlush(glAccountsTableAccumulation);
						faAccountTableSetup.setGlAccountsTableAccumulation(glAccountsTableAccumulation);
						repositoryFAAccountTableSetup.saveAndFlush(faAccountTableSetup);
				}
			} else {
				dtoAccountTableSetup.setMessageType("REQUEST_BODY_IS_EMPTY");
			}
		} catch (Exception e) {
			dtoAccountTableSetup.setMessageType("INTERNAL_SERVER_ERROR");
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if (dtoAccountTableSetup.getMessageType() == null) {
			return getAccountTableSetup();
		} else {
			return dtoAccountTableSetup;
		}
	}
	
	public Map<String,Object> getLifeDayByBookId(DtoFABookMaintenance dtoFABookMaintenance) 
	{
		FABookClassSetup faBookClassSetup = repositoryFABookClassSetup.findByBookSetupBookIdAndIsDeleted(dtoFABookMaintenance.getBookId(), false);
		Map<String,Object> map = new HashedMap();
		map.put("originalLifeDay",0);
		map.put("originalLifeYear",0);
		map.put("remainingLifeDay",0);
		map.put("remainingLifeYear",0);
		map.put("specialDeprAllowance", 0);
		map.put("specialDeprPercentage",0);
		long year=0;
    	long days=0;
		if(faBookClassSetup!=null)
		{
			Date currentDate = new Date();
			Date dateBefore=faBookClassSetup.getCreatedDate();
		    long difference = currentDate.getTime() - dateBefore.getTime();
		    long usedDays = (difference/(1000*60*60*24));
		    long totalLifeDays = (faBookClassSetup.getOrigLifeYears()*365)+faBookClassSetup.getOrigLifeDays();
		    if(totalLifeDays>usedDays)
		    {
		    	long totalRemainingLifeDays=totalLifeDays-usedDays;
		    	if(totalRemainingLifeDays>365)
		    	{
		    		year=totalRemainingLifeDays/365;
		    		days = totalRemainingLifeDays%365;
		    		 
		    	}
		    }
			map.put("originalLifeDay",faBookClassSetup.getOrigLifeDays());
			map.put("originalLifeYear",faBookClassSetup.getOrigLifeYears());
			map.put("remainingLifeDay",days);
			map.put("remainingLifeYear",year);
			map.put("specialDeprAllowance", faBookClassSetup.getSpecialDepreciationAllowance());
			map.put("specialDeprPercentage", UtilRoundDecimal.roundDecimalValue(faBookClassSetup.getSpecialDepreciationAllowancePercent()));
		}
		return map;
	}
	
	public Map<String,Object> getLifeDayDynamic(DtoFABookMaintenance dtoFABookMaintenance) 
	{
		FABookClassSetup faBookClassSetup = repositoryFABookClassSetup.findByBookSetupBookIdAndIsDeleted(dtoFABookMaintenance.getBookId(), false);
		Map<String,Object> map = new HashedMap();
		map.put("originalLifeDay",0);
		map.put("originalLifeYear",0);
		map.put("remainingLifeDay",0);
		map.put("remainingLifeYear",0);
		map.put("specialDeprAllowance", 0);
		map.put("specialDeprPercentage",0);
		long year=0;
    	long days=0;
		if(faBookClassSetup!=null)
		{
			Date currentDate = new Date();
			Date dateBefore=faBookClassSetup.getCreatedDate();
		    long difference = currentDate.getTime() - dateBefore.getTime();
		    long usedDays = (difference/(1000*60*60*24));
		    long totalLifeDays = (dtoFABookMaintenance.getOriginalLifeYear()*365)+dtoFABookMaintenance.getOriginalLifeDay();
		    if(totalLifeDays>usedDays)
		    {
		    	long totalRemainingLifeDays=totalLifeDays-usedDays;
		    	if(totalRemainingLifeDays>365)
		    	{
		    		year=totalRemainingLifeDays/365;
		    		days = totalRemainingLifeDays%365;
		    		 
		    	}
		    }
			map.put("originalLifeDay",dtoFABookMaintenance.getOriginalLifeDay());
			map.put("originalLifeYear",dtoFABookMaintenance.getOriginalLifeYear());
			map.put("remainingLifeDay",days);
			map.put("remainingLifeYear",year);
			map.put("specialDeprAllowance", faBookClassSetup.getSpecialDepreciationAllowance());
			map.put("specialDeprPercentage", UtilRoundDecimal.roundDecimalValue(faBookClassSetup.getSpecialDepreciationAllowancePercent()));
		}
		return map;
	}
}
