/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.constant.MessageLabel;
import com.bti.model.CustomerAccountTableSetup;
import com.bti.model.CustomerClassAccountTableSetup;
import com.bti.model.CustomerClassesSetup;
import com.bti.model.CustomerMaintenance;
import com.bti.model.CustomerMaintenanceOptions;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.MasterArAccountType;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.RMDocumentsTypeSetup;
import com.bti.model.RMPeriodSetup;
import com.bti.model.RMSetup;
import com.bti.model.dto.DtoCustomerClassSetup;
import com.bti.model.dto.DtoCustomerMaintenanceOptions;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoRMDocumentType;
import com.bti.model.dto.DtoRMPeriodSetup;
import com.bti.model.dto.DtoRMSetup;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryCustomerAccountTableSetup;
import com.bti.repository.RepositoryCustomerClassAccountTableSetup;
import com.bti.repository.RepositoryCustomerClassesSetup;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryCustomerMaintenanceOptions;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryMasterArAccountType;
import com.bti.repository.RepositoryMasterCustomerClassMinimumCharge;
import com.bti.repository.RepositoryMasterCustomerClassSetupBalanceType;
import com.bti.repository.RepositoryMasterCustomerClassSetupCreditLimit;
import com.bti.repository.RepositoryMasterCustomerClassSetupFinanaceCharge;
import com.bti.repository.RepositoryMasterRMAgeingTypes;
import com.bti.repository.RepositoryMasterRMApplyByDefaultTypes;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.repository.RepositoryPaymentTermSetup;
import com.bti.repository.RepositoryPriceLevelSetup;
import com.bti.repository.RepositoryRMDocumentType;
import com.bti.repository.RepositoryRMPeriodSetup;
import com.bti.repository.RepositoryRMSetup;
import com.bti.repository.RepositorySalesTerritoryMaintenance;
import com.bti.repository.RepositorySalesmanMaintenance;
import com.bti.repository.RepositoryShipmentMethodSetup;
import com.bti.repository.RepositoryVATSetup;
import com.bti.util.GenerateUUID;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: Service Account Recievable Setup
 * Name of Project: BTI
 * Created on: August 28, 2017
 * Modified on: August 28, 2017 12:35:38 PM
 * @author seasia
 * Version: 
 */
@Service("serviceAccountRecievableSetup")
public class ServiceAccountRecievableSetup{
	
	private static final Logger LOG = Logger.getLogger(ServiceAccountRecievableSetup.class);
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryRMSetup repositoryRMSetup;
	
	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;
	
	@Autowired
	RepositoryMasterRMAgeingTypes repositoryMasterRMAgeingTypes;
	
	@Autowired
	RepositoryMasterRMApplyByDefaultTypes repositoryMasterRMApplyByDefaultTypes;
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	RepositorySalesmanMaintenance repositorySalesmanMaintenance;
	
	@Autowired
	RepositorySalesTerritoryMaintenance repositorySalesTerritoryMaintenance;
	
	@Autowired
	RepositoryCustomerClassAccountTableSetup repositoryCustomerClassAccountTableSetup;
	
	@Autowired
	RepositoryPaymentTermSetup repositoryPaymentTermSetup;
	
	@Autowired
	RepositoryShipmentMethodSetup repositoryShipmentMethodSetup;
	
	@Autowired
	RepositoryVATSetup repositoryVATSetup;
	
	@Autowired
	RepositoryCustomerClassesSetup repositoryCustomerClassesSetup;
	
	@Autowired
	RepositoryMasterCustomerClassMinimumCharge repositoryMasterCustomerClassMinimumCharge;
	
	@Autowired
	RepositoryMasterCustomerClassSetupBalanceType repositoryMasterCustomerClassSetupBalanceType;
	
	@Autowired
	RepositoryMasterCustomerClassSetupCreditLimit repositoryMasterCustomerClassSetupCreditLimit;
	
	@Autowired
	RepositoryMasterCustomerClassSetupFinanaceCharge repositoryMasterCustomerClassSetupFinanaceCharge;
	
	@Autowired
	RepositoryRMPeriodSetup repositoryRMPeriodSetup;
	
	@Autowired
	RepositoryRMDocumentType repositoryRMDocumentType;
	
	@Autowired
	RepositoryCustomerMaintenanceOptions repositoryCustomerMaintenanceOptions;
	
	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;
	
	@Autowired
	RepositoryCustomerAccountTableSetup repositoryCustomerAccountTableSetup;
	
	@Autowired
	RepositoryMasterArAccountType repositoryMasterArAccountType;
	
	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;
	
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;
	
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	
	@Autowired
	RepositoryPriceLevelSetup repositoryPriceLevelSetup;
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @param dtoRMSetup
	 * @return
	 */
	public DtoRMSetup saveRmSetup(DtoRMSetup dtoRMSetup) 
	{
		RMSetup rmSetup=null;
		try{
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			int langid = serviceHome.getLanngugaeId();
			List<RMSetup> rmSetupList =repositoryRMSetup.findAll();
			if(rmSetupList!=null && !rmSetupList.isEmpty()){
				rmSetup=rmSetupList.get(0);
			}
			else{
				rmSetup=new RMSetup();
			}
			
			// Aging Period
			rmSetup.setMasterRMAgeingTypes(repositoryMasterRMAgeingTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoRMSetup.getAgeingBy(),langid,false));
			
			rmSetup.setMasterRMApplyByDefaultTypes(repositoryMasterRMApplyByDefaultTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoRMSetup.getApplyByDefault(),langid,false));
			//Options
			rmSetup.setCompoundFinanceCharge(dtoRMSetup.getCompoundFinanceCharge());
			rmSetup.setArTrackingDiscountAvailable(dtoRMSetup.getArTrackingDiscountAvailable());
			rmSetup.setArPayCommissionsInvoicePay(dtoRMSetup.getArPayCommissionsInvoicePay());
			rmSetup.setDeleteUnpostedPrintedDocuments(dtoRMSetup.getDeleteUnpostedPrintedDocuments());
			rmSetup.setPrintHistoricalAgedTrialBalance(dtoRMSetup.getPrintHistoricalAgedTrialBalance());
			// set checkbook id
			rmSetup.setCheckbookMaintenance(repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoRMSetup.getCheckbookId(),false));
			rmSetup.setPriceLevel(dtoRMSetup.getPriceLevel());
			rmSetup.setCreatedBy(loggedInUserId);
			//Set Passwords
			rmSetup.setCreditLimitPassword(dtoRMSetup.getCreditLimitPassword());
			rmSetup.setCustomerHoldPassword(dtoRMSetup.getCustomerHoldPassword());
			rmSetup.setWaivedFinanceChargePassword(dtoRMSetup.getWaivedFinanceChargePassword());
			rmSetup.setWriteoffPassword(dtoRMSetup.getWriteoffPassword());
			// Set Vat Scheduled Id
			rmSetup.setFreightVatScheduleId(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoRMSetup.getFreightVatScheduleId(),false));
			rmSetup.setMiscVatScheduleId(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoRMSetup.getMiscVatScheduleId(),false));
			rmSetup.setSalesVatScheduleId(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoRMSetup.getSalesVatScheduleId(),false));
			//Dates Of Last
			rmSetup.setLastDateBalanceForwardAge(UtilDateAndTime.ddmmyyyyStringToDate(dtoRMSetup.getLastDateBalanceForwardAge()));
			rmSetup.setLastDateStatementPrinted(UtilDateAndTime.ddmmyyyyStringToDate(dtoRMSetup.getLastDateStatementPrinted()));
			rmSetup.setLastFinanceChargeDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoRMSetup.getLastFinanceChargeDate()));
			rmSetup.setDeleteUnpostedPrintedDocumentsDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoRMSetup.getDeleteUnpostedPrintedDocumentDate()));			
			
			rmSetup.setUserDefine1(dtoRMSetup.getUserDefine1());
			rmSetup.setUserDefine2(dtoRMSetup.getUserDefine2());
			rmSetup.setUserDefine3(dtoRMSetup.getUserDefine3());
			
			rmSetup=repositoryRMSetup.save(rmSetup);
			rmSetup=repositoryRMSetup.findByIdAndIsDeleted(rmSetup.getId(),false);
			saveRMPeriodSetup(dtoRMSetup.getDtoRMPeriodSetupsList(),rmSetup);
		}
		
		catch(Exception e){
			dtoRMSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		if(dtoRMSetup.getMessageType()==null){
			dtoRMSetup=new DtoRMSetup(rmSetup);
			dtoRMSetup.setDtoRMPeriodSetupsList(getRmPeriodSetupListByRmSetupId(rmSetup));
		}
		return dtoRMSetup;
	}
	
	/**
	 * @param dtoRMPeriodSetupsList
	 * @param rmSetup
	 */
	public void saveRMPeriodSetup(List<DtoRMPeriodSetup> dtoRMPeriodSetupsList,RMSetup rmSetup){
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		repositoryRMPeriodSetup.deleteAll();
		if(dtoRMPeriodSetupsList!=null && !dtoRMPeriodSetupsList.isEmpty()){
			for (DtoRMPeriodSetup dtoRMPeriodSetup : dtoRMPeriodSetupsList) {
				RMPeriodSetup rmPeriodSetup = new RMPeriodSetup();
				rmPeriodSetup.setPeriodDescription(dtoRMPeriodSetup.getPeriodDescription());
				rmPeriodSetup.setPeriodNoofDays(dtoRMPeriodSetup.getPeriodNoofDays());
				rmPeriodSetup.setPeriodEnd(dtoRMPeriodSetup.getPeriodEnd());
				rmPeriodSetup.setCreatedBy(loggedInUserId);
				rmPeriodSetup.setRmSetup(rmSetup);
				repositoryRMPeriodSetup.save(rmPeriodSetup);
			}
		}
	}
	
	public DtoRMSetup getRmSetup(RMSetup rmSetup) 
	{
		DtoRMSetup dtoRMSetup=null;
		try{
			dtoRMSetup=new DtoRMSetup(rmSetup);
			dtoRMSetup.setDtoRMPeriodSetupsList(getRmPeriodSetupListByRmSetupId(rmSetup));
			
		}
		catch(Exception e){
			dtoRMSetup=new DtoRMSetup();
			dtoRMSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoRMSetup;
	}
	
	public List<DtoRMPeriodSetup> getRmPeriodSetupListByRmSetupId(RMSetup rmSetup){
		List<RMPeriodSetup> rmPeriodSetupsList= repositoryRMPeriodSetup.findByRmSetupIdAndIsDeleted(rmSetup.getId(),false);
		List<DtoRMPeriodSetup> list = new ArrayList<>();
		DtoRMPeriodSetup dtoRMPeriodSetup=null;
		if(rmPeriodSetupsList!=null && !rmPeriodSetupsList.isEmpty()){
			for (RMPeriodSetup rmPeriodSetup : rmPeriodSetupsList) {
				dtoRMPeriodSetup= new DtoRMPeriodSetup(rmPeriodSetup);
				list.add(dtoRMPeriodSetup);
			}
		}
		return list;
	}
	
	
	/**
	 * @param dtoCustomerClassSetup
	 * @return
	 */
	public DtoCustomerClassSetup saveCustomerClassSetup(DtoCustomerClassSetup dtoCustomerClassSetup) 
	{
		CustomerClassesSetup customerClassesSetup=null;
		try
		{
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			int langid = serviceHome.getLanngugaeId();
			customerClassesSetup=new CustomerClassesSetup();
			// Set Class id
			customerClassesSetup.setCustomerClassId(dtoCustomerClassSetup.getCustomerClassId());
			//Set Description
			customerClassesSetup.setCustomeClassDescriptionArabic(dtoCustomerClassSetup.getCustomeClassDescriptionArabic());
			customerClassesSetup.setCustomerClassDescription(dtoCustomerClassSetup.getCustomerClassDescription());
			//Step 2
			customerClassesSetup.setMasterCustomerClassSetupBalanceType(repositoryMasterCustomerClassSetupBalanceType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerClassSetup.getBalanceType(),langid,false));
			customerClassesSetup.setMasterCustomerClassSetupFinanaceCharge(repositoryMasterCustomerClassSetupFinanaceCharge.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerClassSetup.getFinanceCharge(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getFinanceChargeAmount())){
				customerClassesSetup.setFinanceChargeAmount(Double.valueOf(dtoCustomerClassSetup.getFinanceChargeAmount()));
			}
			customerClassesSetup.setMasterCustomerClassMinimumCharge(repositoryMasterCustomerClassMinimumCharge.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerClassSetup.getMinimumCharge(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getMinimumChargeAmount())){
				customerClassesSetup.setMinimumChargeAmount(Double.valueOf(dtoCustomerClassSetup.getMinimumChargeAmount()));
			}
			customerClassesSetup.setMasterCustomerClassSetupCreditLimit(repositoryMasterCustomerClassSetupCreditLimit.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerClassSetup.getCreditLimit(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getCreditLimitAmount())){
				customerClassesSetup.setCreditLimitAmount(Double.valueOf(dtoCustomerClassSetup.getCreditLimitAmount()));
			}
			
			//Step 3 Left
			if(UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getTradeDiscountPercent())){
				customerClassesSetup.setTradeDiscountPercent(Double.valueOf(dtoCustomerClassSetup.getTradeDiscountPercent()));
			}
			
			customerClassesSetup.setPaymentTermsSetup(repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(dtoCustomerClassSetup.getPaymentTermId(),false));
			customerClassesSetup.setShipmentMethodSetup(repositoryShipmentMethodSetup.findByShipmentMethodIdAndIsDeleted(dtoCustomerClassSetup.getShipmentMethodId(),false));
			customerClassesSetup.setVatSetup(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoCustomerClassSetup.getVatId(),false));
			customerClassesSetup.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCustomerClassSetup.getCurrencyId(),false));
			customerClassesSetup.setPriceLevelId(dtoCustomerClassSetup.getPriceLevelId());
			//Step 3 Right
			customerClassesSetup.setSalesmanMaintenance(repositorySalesmanMaintenance.findBySalesmanIdAndIsDeleted(dtoCustomerClassSetup.getSalesmanId(),false));
			customerClassesSetup.setSalesTerritoryMaintenance(repositorySalesTerritoryMaintenance.findBySalesTerritoryIdAndIsDeleted(dtoCustomerClassSetup.getSalesTerritoryId(),false));
			customerClassesSetup.setCheckbookMaintenance(repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoCustomerClassSetup.getCheckbookId(),false));
			customerClassesSetup.setCustomerPriority(dtoCustomerClassSetup.getCustomerPriority());
			
			// Set User Define Parameters
			customerClassesSetup.setUserDefine1(dtoCustomerClassSetup.getUserDefine1());
			customerClassesSetup.setUserDefine2(dtoCustomerClassSetup.getUserDefine2());
			customerClassesSetup.setUserDefine3(dtoCustomerClassSetup.getUserDefine3());
			// Set Maintenance History
			customerClassesSetup.setOpenMaintenanceHistoryCalendarYear(dtoCustomerClassSetup.getOpenMaintenanceHistoryCalendarYear());
			customerClassesSetup.setOpenMaintenanceHistoryDistribution(dtoCustomerClassSetup.getOpenMaintenanceHistoryDistribution());
			customerClassesSetup.setOpenMaintenanceHistoryFiscalYear(dtoCustomerClassSetup.getOpenMaintenanceHistoryFiscalYear());
			customerClassesSetup.setOpenMaintenanceHistoryTransaction(dtoCustomerClassSetup.getOpenMaintenanceHistoryTransaction());
			// Set Created By
			customerClassesSetup.setCreatedBy(loggedInUserId);
			customerClassesSetup=repositoryCustomerClassesSetup.save(customerClassesSetup);
			customerClassesSetup=repositoryCustomerClassesSetup.findByCustomerClassIdAndIsDeleted(customerClassesSetup.getCustomerClassId(),false);
		}
		catch(Exception e)
		{
			dtoCustomerClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if(customerClassesSetup !=null && dtoCustomerClassSetup.getMessageType()==null){
			dtoCustomerClassSetup=new DtoCustomerClassSetup(customerClassesSetup);
		}
		return dtoCustomerClassSetup;
	}
	
	/**
	 * @param dtoCustomerClassSetup
	 * @param customerClassesSetup
	 * @return
	 */
	public DtoCustomerClassSetup updateCustomerClassSetup(DtoCustomerClassSetup dtoCustomerClassSetup,CustomerClassesSetup customerClassesSetup) 
	{
		try{
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			int langid = serviceHome.getLanngugaeId();
		
			//Set Description
			customerClassesSetup.setCustomeClassDescriptionArabic(dtoCustomerClassSetup.getCustomeClassDescriptionArabic());
			customerClassesSetup.setCustomerClassDescription(dtoCustomerClassSetup.getCustomerClassDescription());
			//Step 2
			customerClassesSetup.setMasterCustomerClassSetupBalanceType(repositoryMasterCustomerClassSetupBalanceType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerClassSetup.getBalanceType(),langid,false));
			customerClassesSetup.setMasterCustomerClassSetupFinanaceCharge(repositoryMasterCustomerClassSetupFinanaceCharge.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerClassSetup.getFinanceCharge(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getFinanceChargeAmount())){
				customerClassesSetup.setFinanceChargeAmount(Double.valueOf(dtoCustomerClassSetup.getFinanceChargeAmount()));
			}
			customerClassesSetup.setMasterCustomerClassMinimumCharge(repositoryMasterCustomerClassMinimumCharge.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerClassSetup.getMinimumCharge(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getMinimumChargeAmount())){
				customerClassesSetup.setMinimumChargeAmount(Double.valueOf(dtoCustomerClassSetup.getMinimumChargeAmount()));
			}
			customerClassesSetup.setMasterCustomerClassSetupCreditLimit(repositoryMasterCustomerClassSetupCreditLimit.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerClassSetup.getCreditLimit(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getCreditLimitAmount())){
				customerClassesSetup.setCreditLimitAmount(Double.valueOf(dtoCustomerClassSetup.getCreditLimitAmount()));
			}
			
			//Step 3 Left
			if(UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getTradeDiscountPercent())){
				customerClassesSetup.setTradeDiscountPercent(Double.valueOf(dtoCustomerClassSetup.getTradeDiscountPercent()));
			}
			customerClassesSetup.setPaymentTermsSetup(repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(dtoCustomerClassSetup.getPaymentTermId(),false));
			customerClassesSetup.setShipmentMethodSetup(repositoryShipmentMethodSetup.findByShipmentMethodIdAndIsDeleted(dtoCustomerClassSetup.getShipmentMethodId(),false));
			customerClassesSetup.setVatSetup(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoCustomerClassSetup.getVatId(),false));
			customerClassesSetup.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCustomerClassSetup.getCurrencyId(),false));
			customerClassesSetup.setPriceLevelId(dtoCustomerClassSetup.getPriceLevelId());
			//Step 3 Right
			customerClassesSetup.setSalesmanMaintenance(repositorySalesmanMaintenance.findBySalesmanIdAndIsDeleted(dtoCustomerClassSetup.getSalesmanId(),false));
			customerClassesSetup.setSalesTerritoryMaintenance(repositorySalesTerritoryMaintenance.findBySalesTerritoryIdAndIsDeleted(dtoCustomerClassSetup.getSalesTerritoryId(),false));
			customerClassesSetup.setCheckbookMaintenance(repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoCustomerClassSetup.getCheckbookId(),false));
			customerClassesSetup.setCustomerPriority(dtoCustomerClassSetup.getCustomerPriority());
			
			// Set User Define Parameters
			customerClassesSetup.setUserDefine1(dtoCustomerClassSetup.getUserDefine1());
			customerClassesSetup.setUserDefine2(dtoCustomerClassSetup.getUserDefine2());
			customerClassesSetup.setUserDefine3(dtoCustomerClassSetup.getUserDefine3());
			// Set Maintenance History
			customerClassesSetup.setOpenMaintenanceHistoryCalendarYear(dtoCustomerClassSetup.getOpenMaintenanceHistoryCalendarYear());
			customerClassesSetup.setOpenMaintenanceHistoryDistribution(dtoCustomerClassSetup.getOpenMaintenanceHistoryDistribution());
			customerClassesSetup.setOpenMaintenanceHistoryFiscalYear(dtoCustomerClassSetup.getOpenMaintenanceHistoryFiscalYear());
			customerClassesSetup.setOpenMaintenanceHistoryTransaction(dtoCustomerClassSetup.getOpenMaintenanceHistoryTransaction());
			// Set Created By
			customerClassesSetup.setChangeBy(loggedInUserId);
			customerClassesSetup=repositoryCustomerClassesSetup.save(customerClassesSetup);
			customerClassesSetup=repositoryCustomerClassesSetup.findByCustomerClassIdAndIsDeleted(customerClassesSetup.getCustomerClassId(),false);
		}
		catch(Exception e){
			dtoCustomerClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if(customerClassesSetup !=null && dtoCustomerClassSetup.getMessageType()==null){
			dtoCustomerClassSetup=new DtoCustomerClassSetup(customerClassesSetup);
		}
		
		return dtoCustomerClassSetup;
	}
	
	/**
	 * @param dtoCustomerClassSetup
	 * @param customerClassesSetup
	 * @return
	 */
	public DtoCustomerClassSetup getCustomerClassSetupByClassId(DtoCustomerClassSetup dtoCustomerClassSetup,CustomerClassesSetup customerClassesSetup) 
	{
		try{
			dtoCustomerClassSetup=new DtoCustomerClassSetup(customerClassesSetup);
		}
		catch(Exception e)
		{
			dtoCustomerClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoCustomerClassSetup;
	}
	
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchCustomerClassSetup(DtoSearch dtoSearchs) 
	{
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryCustomerClassesSetup.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		DtoCustomerClassSetup dtoCustomerClassSetup = null;
		List<CustomerClassesSetup> customerClassesSetups = null;
		List<DtoCustomerClassSetup> dtoCustomerClassSetupsList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			customerClassesSetups = repositoryCustomerClassesSetup
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			customerClassesSetups = repositoryCustomerClassesSetup
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (customerClassesSetups != null) {
			for (CustomerClassesSetup customerClassesSetup : customerClassesSetups) {
				dtoCustomerClassSetup = new DtoCustomerClassSetup(customerClassesSetup);
				dtoCustomerClassSetupsList.add(dtoCustomerClassSetup);
			}
		}
		dtoSearch.setRecords(dtoCustomerClassSetupsList);
		return dtoSearch;
	}
	
	/**
	 * @param documentType
	 * @return
	 */
	public DtoRMDocumentType saveRMPeriodSetup(DtoRMDocumentType documentType)
	{
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try{
			if(documentType.getRmDocumentTypeList()!=null && !documentType.getRmDocumentTypeList().isEmpty()){
				
				RMDocumentsTypeSetup rmDocumentsTypeSetup =null;
				for (DtoRMDocumentType dtoRMDocumentTypes : documentType.getRmDocumentTypeList()) {
					rmDocumentsTypeSetup = repositoryRMDocumentType.findByDocumentCodeAndIsDeleted(dtoRMDocumentTypes.getDocumentCode(), false);
					if(rmDocumentsTypeSetup == null){
						rmDocumentsTypeSetup = new RMDocumentsTypeSetup();
					}
					rmDocumentsTypeSetup.setDocumentType(dtoRMDocumentTypes.getDocType());
					rmDocumentsTypeSetup.setDocumentCode(dtoRMDocumentTypes.getDocumentCode());
					rmDocumentsTypeSetup.setDocumentTypeDescription(dtoRMDocumentTypes.getDocumentTypeDescription());
					rmDocumentsTypeSetup.setDocumentTypeDescriptionArabic(dtoRMDocumentTypes.getDocumentTypeDescriptionArabic());
					rmDocumentsTypeSetup.setDocumentNumber(dtoRMDocumentTypes.getDocumentNumber());
					rmDocumentsTypeSetup.setCreatedBy(loggedInUserId);
					repositoryRMDocumentType.saveAndFlush(rmDocumentsTypeSetup);
				}
			}
		}
		catch(Exception e)
		{
			documentType.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return documentType;
	}
	
	/**
	 * @param documentType
	 * @return
	 */
	public DtoRMDocumentType updateRMPeriodSetup(DtoRMDocumentType documentType){
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try
		{
			if(documentType.getRmDocumentTypeList()!=null && !documentType.getRmDocumentTypeList().isEmpty())
			{
				for (DtoRMDocumentType dtoRMDocumentTypes : documentType.getRmDocumentTypeList()) 
				{
					RMDocumentsTypeSetup rmDocumentsTypeSetup = repositoryRMDocumentType.findByArDocumentTypeIdAndIsDeleted(dtoRMDocumentTypes.getDocumentTypeId(),false);
					if(rmDocumentsTypeSetup!=null)
					{
						rmDocumentsTypeSetup.setDocumentType(dtoRMDocumentTypes.getDocType());
						rmDocumentsTypeSetup.setDocumentCode(dtoRMDocumentTypes.getDocumentCode());
						rmDocumentsTypeSetup.setDocumentTypeDescription(dtoRMDocumentTypes.getDocumentTypeDescription());
						rmDocumentsTypeSetup.setDocumentTypeDescriptionArabic(dtoRMDocumentTypes.getDocumentTypeDescriptionArabic());
						rmDocumentsTypeSetup.setDocumentNumber(dtoRMDocumentTypes.getDocumentNumber());
						rmDocumentsTypeSetup.setChangeBy(loggedInUserId);
						repositoryRMDocumentType.save(rmDocumentsTypeSetup);
					}
					
				}
			}
		}
		catch(Exception e)
		{
			documentType.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return documentType;
	}
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch getAccountRecievableOptions(DtoSearch dtoSearch)
	{
		DtoSearch dtoSearch2=new DtoSearch();
		dtoSearch2.setPageNumber(dtoSearch.getPageNumber());
		dtoSearch2.setPageSize(dtoSearch.getPageSize());
		dtoSearch2.setTotalCount(repositoryRMDocumentType.countAll());
		List<RMDocumentsTypeSetup> rmDocumentsTypeSetupsList =null;
		List<DtoRMDocumentType> list = new ArrayList<>();
		if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			rmDocumentsTypeSetupsList=repositoryRMDocumentType.getAllWithPagination(pageable);
		}
		else{
			rmDocumentsTypeSetupsList=repositoryRMDocumentType.findAll();
		}
		if(rmDocumentsTypeSetupsList!=null && !rmDocumentsTypeSetupsList.isEmpty()){
			for (RMDocumentsTypeSetup rmDocumentsTypeSetup : rmDocumentsTypeSetupsList) {
				DtoRMDocumentType dtoRMDocumentType2 = new DtoRMDocumentType(rmDocumentsTypeSetup);
				list.add(dtoRMDocumentType2);
			}
		}
		dtoSearch2.setRecords(list);
		return dtoSearch2;
	}
	
	/**
	 * @param dtoCustomerMaintenanceOptions
	 * @return
	 */
	public DtoCustomerMaintenanceOptions saveCustomerMaintenanceOptions(DtoCustomerMaintenanceOptions dtoCustomerMaintenanceOptions) 
	{
		CustomerMaintenanceOptions customerMaintenanceOptions=null;
		try{
			int langid = serviceHome.getLanngugaeId();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			customerMaintenanceOptions = repositoryCustomerMaintenanceOptions.findByCustomerMaintenanceCustnumberAndIsDeleted(dtoCustomerMaintenanceOptions.getCustomerNumberId(),false);
			if(customerMaintenanceOptions!=null)
			{
				customerMaintenanceOptions.setChangeBy(loggedInUserId);
			}
			else
			{
				customerMaintenanceOptions=new CustomerMaintenanceOptions();
				customerMaintenanceOptions.setId(GenerateUUID.getRandomUUID());
				// Set Created By
				customerMaintenanceOptions.setCreatedBy(loggedInUserId);
				customerMaintenanceOptions.setCustomerMaintenance(repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(dtoCustomerMaintenanceOptions.getCustomerNumberId(),false));
			}
			
			//Step 2
			customerMaintenanceOptions.setMasterCustomerClassSetupBalanceType(repositoryMasterCustomerClassSetupBalanceType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerMaintenanceOptions.getBalanceType(),langid,false));
			customerMaintenanceOptions.setMasterCustomerClassSetupFinanaceCharge(repositoryMasterCustomerClassSetupFinanaceCharge.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerMaintenanceOptions.getFinanceCharge(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerMaintenanceOptions.getFinanceChargeAmount())){
				customerMaintenanceOptions.setFinanceChargeAmount(Double.valueOf(dtoCustomerMaintenanceOptions.getFinanceChargeAmount()));
			}
			customerMaintenanceOptions.setMasterCustomerClassMinimumCharge(repositoryMasterCustomerClassMinimumCharge.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerMaintenanceOptions.getMinimumCharge(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerMaintenanceOptions.getMinimumChargeAmount())){
				customerMaintenanceOptions.setMinimumChargeAmount(Double.valueOf(dtoCustomerMaintenanceOptions.getMinimumChargeAmount()));
			}
			customerMaintenanceOptions.setMasterCustomerClassSetupCreditLimit(repositoryMasterCustomerClassSetupCreditLimit.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCustomerMaintenanceOptions.getCreditLimit(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoCustomerMaintenanceOptions.getCreditLimitAmount())){
				customerMaintenanceOptions.setCreditLimitAmount(Double.valueOf(dtoCustomerMaintenanceOptions.getCreditLimitAmount()));
			}
			
			//Step 3 Left
			if(UtilRandomKey.isNotBlank(dtoCustomerMaintenanceOptions.getTradeDiscountPercent())){
				customerMaintenanceOptions.setTradeDiscountPercent(Double.valueOf(dtoCustomerMaintenanceOptions.getTradeDiscountPercent()));
			}
			
			customerMaintenanceOptions.setPaymentTermsSetup(repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(dtoCustomerMaintenanceOptions.getPaymentTermId(),false));
			customerMaintenanceOptions.setShipmentMethodSetup(repositoryShipmentMethodSetup.findByShipmentMethodIdAndIsDeleted(dtoCustomerMaintenanceOptions.getShipmentMethodId(),false));
			customerMaintenanceOptions.setVatSetup(repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoCustomerMaintenanceOptions.getVatId(),false));
			customerMaintenanceOptions.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCustomerMaintenanceOptions.getCurrencyId(),false));
			customerMaintenanceOptions.setPriceLevelSetup(repositoryPriceLevelSetup.findOne(Integer.parseInt(dtoCustomerMaintenanceOptions.getPriceLevel())));
			//Step 3 Right
			customerMaintenanceOptions.setSalesmanMaintenance(repositorySalesmanMaintenance.findBySalesmanIdAndIsDeleted(dtoCustomerMaintenanceOptions.getSalesmanId(),false));
			customerMaintenanceOptions.setSalesTerritoryMaintenance(repositorySalesTerritoryMaintenance.findBySalesTerritoryIdAndIsDeleted(dtoCustomerMaintenanceOptions.getSalesTerritoryId(),false));
			customerMaintenanceOptions.setCheckbookMaintenance(repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoCustomerMaintenanceOptions.getCheckbookId(),false));
			customerMaintenanceOptions.setCustomerPriority(dtoCustomerMaintenanceOptions.getCustomerPriority());
			
			// Set Maintenance History
			customerMaintenanceOptions.setOpenMaintenanceHistoryCalendarYear(dtoCustomerMaintenanceOptions.getOpenMaintenanceHistoryCalendarYear());
			customerMaintenanceOptions.setOpenMaintenanceHistoryDistribution(dtoCustomerMaintenanceOptions.getOpenMaintenanceHistoryDistribution());
			customerMaintenanceOptions.setOpenMaintenanceHistoryFiscalYear(dtoCustomerMaintenanceOptions.getOpenMaintenanceHistoryFiscalYear());
			customerMaintenanceOptions.setOpenMaintenanceHistoryTransaction(dtoCustomerMaintenanceOptions.getOpenMaintenanceHistoryTransaction());
			customerMaintenanceOptions=repositoryCustomerMaintenanceOptions.save(customerMaintenanceOptions);
			customerMaintenanceOptions=repositoryCustomerMaintenanceOptions.findByCustomerMaintenanceCustnumberAndIsDeleted(dtoCustomerMaintenanceOptions.getCustomerNumberId(),false);
		
		}
		catch(Exception e){
			dtoCustomerMaintenanceOptions.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if(customerMaintenanceOptions !=null && dtoCustomerMaintenanceOptions.getMessageType()==null){
			dtoCustomerMaintenanceOptions=new DtoCustomerMaintenanceOptions(customerMaintenanceOptions);
		}
		return dtoCustomerMaintenanceOptions;
	}
	
	/**
	 * @param customerMaintenanceOptions
	 * @return
	 */
	public DtoCustomerMaintenanceOptions getCustomerMaintenanceOptions(DtoCustomerMaintenanceOptions dtoCustomerMaintenanceOptions) 
	{
		CustomerMaintenanceOptions customerMaintenanceOptions = repositoryCustomerMaintenanceOptions.
				findByCustomerMaintenanceCustnumberAndIsDeleted(dtoCustomerMaintenanceOptions.getCustomerNumberId(),false);
		try
		{
			if(customerMaintenanceOptions!=null){
				dtoCustomerMaintenanceOptions=new DtoCustomerMaintenanceOptions(customerMaintenanceOptions);
				return dtoCustomerMaintenanceOptions;
			}
			else
			{
				CustomerMaintenance customerMaintenance=repositoryCustomerMaintenance.
						findByCustnumberAndIsDeleted(dtoCustomerMaintenanceOptions.getCustomerNumberId(), false);
				if(customerMaintenance!=null && customerMaintenance.getCustomerClassesSetup()!=null){
					dtoCustomerMaintenanceOptions=new DtoCustomerMaintenanceOptions(customerMaintenance);
					return dtoCustomerMaintenanceOptions;
				}
				else
				{
					dtoCustomerMaintenanceOptions.setMessageType(MessageLabel.RECORD_NOT_FOUND);
				}
			}
		}
		catch(Exception e)
		{
			dtoCustomerMaintenanceOptions=new DtoCustomerMaintenanceOptions();
			dtoCustomerMaintenanceOptions.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoCustomerMaintenanceOptions;
	}
	
	/**
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	public DtoPayableAccountClassSetup saveCustomerAccountMaintenance(DtoPayableAccountClassSetup dtoPayableAccountClassSetup)
	{
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid= serviceHome.getLanngugaeId();
		GLAccountsTableAccumulation glAccountsTableAccumulation=null;
		try
		{
			repositoryCustomerAccountTableSetup.deleteRecordById(dtoPayableAccountClassSetup.getCustomerId() , true);
			if(dtoPayableAccountClassSetup.getAccountNumberList()!=null && !dtoPayableAccountClassSetup.getAccountNumberList().isEmpty() )
			{
				for (DtoPayableAccountClassSetup accountNumber : dtoPayableAccountClassSetup.getAccountNumberList()) 
				{
					CustomerAccountTableSetup customerAccountTableSetup =repositoryCustomerAccountTableSetup.
							findByMasterArAccountTypeTypeIdAndCustomerMaintenanceCustnumber(accountNumber.getAccountType(),dtoPayableAccountClassSetup.getCustomerId());
					if(customerAccountTableSetup==null){
						customerAccountTableSetup= new CustomerAccountTableSetup();
					}
					customerAccountTableSetup.setDeleted(false);
					customerAccountTableSetup.setCustomerMaintenance(repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(dtoPayableAccountClassSetup.getCustomerId(),false));
					customerAccountTableSetup.setMasterArAccountType(repositoryMasterArAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(accountNumber.getAccountType(),langid,false));
					customerAccountTableSetup.setCreatedBy(loggedInUserId);
					StringBuilder sb = new StringBuilder();
					String fullIndex="";
					if(accountNumber.getAccountNumberIndex()!=null && !accountNumber.getAccountNumberIndex().isEmpty())
					{
						for(Long i : accountNumber.getAccountNumberIndex()){
						    sb.append(String.valueOf(i));
						}
						fullIndex = sb.toString();
					}
					
					List<GLAccountsTableAccumulation> glAccountsTableAccumulationsList = repositoryGLAccountsTableAccumulation.findByAccountNumberFullIndexAndIsDeleted(fullIndex,false);
					if(glAccountsTableAccumulationsList!=null && !glAccountsTableAccumulationsList.isEmpty())
					{
						glAccountsTableAccumulation=glAccountsTableAccumulationsList.get(0);
						glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
					}
					else
					{
						glAccountsTableAccumulation = new GLAccountsTableAccumulation();
						int actRowIndexId=1;
						PayableAccountClassSetup payableAccountClassSetup2 = repositoryPayableAccountClassSetup.findFirstByOrderByIdDesc();
						if(payableAccountClassSetup2!=null){
							actRowIndexId=payableAccountClassSetup2.getAccountRowIndex();
							actRowIndexId=actRowIndexId+1;
						}
						int segmentNumber=1;
						if(accountNumber.getAccountNumberIndex()!=null && !accountNumber.getAccountNumberIndex().isEmpty())
						{
							StringBuilder accountNumberfullIndex= new StringBuilder("");
							for (Long indexId : accountNumber.getAccountNumberIndex()) 
							{
								accountNumberfullIndex.append(String.valueOf(indexId));
								PayableAccountClassSetup payableAccountClassSetup = new PayableAccountClassSetup();
								payableAccountClassSetup.setIndexId(indexId);
								payableAccountClassSetup.setSegmentNumber(segmentNumber);
								segmentNumber++;
								payableAccountClassSetup.setAccountRowIndex(actRowIndexId);
								payableAccountClassSetup.setCreatedBy(loggedInUserId);
								repositoryPayableAccountClassSetup.save(payableAccountClassSetup);
								
							}
							
							glAccountsTableAccumulation.setAccountTableRowIndex(String.valueOf(actRowIndexId));
							glAccountsTableAccumulation.setSegmentNumber(segmentNumber);
							glAccountsTableAccumulation.setAccountNumberFullIndex(accountNumberfullIndex.toString());
							glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
							if(accountNumber.getTransactionType()!=null){
								glAccountsTableAccumulation.setTransactionType(accountNumber.getTransactionType());
							}
							glAccountsTableAccumulation.setCreatedBy(loggedInUserId);
							
						}
					}
					glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.saveAndFlush(glAccountsTableAccumulation);
						customerAccountTableSetup.setGlAccountsTableAccumulation(glAccountsTableAccumulation);
						repositoryCustomerAccountTableSetup.saveAndFlush(customerAccountTableSetup);
				}
			}
			else
			{
				dtoPayableAccountClassSetup.setMessageType(MessageLabel.REQUEST_BODY_IS_EMPTY);
			}
		}
		catch(Exception e){
			dtoPayableAccountClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if(dtoPayableAccountClassSetup.getMessageType()==null){
			return getCustomerAccountMaintenanceByCustomerId(dtoPayableAccountClassSetup.getCustomerId());
		}
		else
		{
			return dtoPayableAccountClassSetup;
		}
	}
	
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public DtoPayableAccountClassSetup getCustomerAccountMaintenanceByCustomerId(String customerId)
	{
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		DtoPayableAccountClassSetup main= null;
		List<CustomerAccountTableSetup> customerAccountTableSetupsList = repositoryCustomerAccountTableSetup.findByCustomerMaintenanceCustnumberAndIsDeleted(customerId,false);
		List<DtoPayableAccountClassSetup> list = new ArrayList<>();
		if(customerAccountTableSetupsList!=null && !customerAccountTableSetupsList.isEmpty())
		{
			main=new DtoPayableAccountClassSetup();
			main.setCustomerId(customerId);
			for (CustomerAccountTableSetup customerAccountTableSetup : customerAccountTableSetupsList) 
			{
				List<Long> accountNumberIndex = new ArrayList<>();
				DtoPayableAccountClassSetup dtoPayableAccountClassSetup= new DtoPayableAccountClassSetup();
				dtoPayableAccountClassSetup.setAccountDescription("");
				dtoPayableAccountClassSetup.setAccountType(0);
				dtoPayableAccountClassSetup.setAccountTypeValue("");
				dtoPayableAccountClassSetup.setCustomerId("");
				if(customerAccountTableSetup.getCustomerMaintenance()!=null){
					dtoPayableAccountClassSetup.setCustomerId(customerAccountTableSetup.getCustomerMaintenance().getCustnumber());
				}
				
				MasterArAccountType masterArAccountType = customerAccountTableSetup.getMasterArAccountType();
				if(masterArAccountType!=null){
					int lang = Integer.parseInt(langId);
					if(masterArAccountType.getLanguage().getLanguageId()!=lang){
						masterArAccountType = repositoryMasterArAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterArAccountType.getTypeId(),lang,false);
					}
					if(masterArAccountType!=null)
					{
						dtoPayableAccountClassSetup
						.setAccountType(masterArAccountType.getTypeId());
					dtoPayableAccountClassSetup
							.setAccountTypeValue(masterArAccountType.getAccountType());
					}
				}
				
				GLAccountsTableAccumulation glAccountsTableAccumulation = customerAccountTableSetup.getGlAccountsTableAccumulation();
				String accountNumber="";
				List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
				if(glAccountsTableAccumulation!=null)
				{
					    dtoPayableAccountClassSetup.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
						Object[] arrayOfObject = serviceAccountPayable.getAccountNumberMultipleWays(glAccountsTableAccumulation,langId);
						accountNumber=(String) arrayOfObject[0];
						accountNumberIndex=(List<Long>) arrayOfObject[1];
						listOfKeyValue=(List<DtoPayableAccountClassSetup>) arrayOfObject[2];
						dtoPayableAccountClassSetup.setAccountDescription(arrayOfObject[3].toString());
				}
				dtoPayableAccountClassSetup.setAccountNumberIndex(accountNumberIndex);
				dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
				dtoPayableAccountClassSetup.setAccNumberIndexValue(listOfKeyValue);
				list.add(dtoPayableAccountClassSetup);
			}
			main.setAccountNumberList(list);
		}
		else
		{
			CustomerMaintenance customerMaintenance= repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(customerId, false);
			if(customerMaintenance!=null && customerMaintenance.getCustomerClassesSetup()!=null)
			{
				List<CustomerClassAccountTableSetup> customerClassAccountTableSetupList=repositoryCustomerClassAccountTableSetup
				.findByCustomerClassesSetupCustomerClassIdAndIsDeleted
				(customerMaintenance.getCustomerClassesSetup().getCustomerClassId(), false);
				if(customerClassAccountTableSetupList!=null && !customerClassAccountTableSetupList.isEmpty())
				{
					  main=new DtoPayableAccountClassSetup();
					  main.setCustomerId(customerId);
					  for (CustomerClassAccountTableSetup customerClassAccountTableSetup : customerClassAccountTableSetupList) 
					  {
						    List<Long> accountNumberIndex = new ArrayList<>();
							DtoPayableAccountClassSetup dtoPayableAccountClassSetup= new DtoPayableAccountClassSetup();
							dtoPayableAccountClassSetup.setAccountDescription("");
							dtoPayableAccountClassSetup.setAccountType(0);
							dtoPayableAccountClassSetup.setAccountTypeValue("");
							dtoPayableAccountClassSetup.setCustomerId("");
						    dtoPayableAccountClassSetup.setCustomerId(customerMaintenance.getCustnumber());
							MasterArAccountType masterArAccountType = customerClassAccountTableSetup.getMasterArAccountType();
							if(masterArAccountType!=null){
								int lang = Integer.parseInt(langId);
								if(masterArAccountType.getLanguage().getLanguageId()!=lang){
									masterArAccountType = repositoryMasterArAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterArAccountType.getTypeId(),lang,false);
								}
								if(masterArAccountType!=null)
								{
									dtoPayableAccountClassSetup
									.setAccountType(masterArAccountType.getTypeId());
								dtoPayableAccountClassSetup
										.setAccountTypeValue(masterArAccountType.getAccountType());
								}
							}
							
							GLAccountsTableAccumulation glAccountsTableAccumulation = customerClassAccountTableSetup.getGlAccountsTableAccumulation();
							String accountNumber="";
							List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
							if(glAccountsTableAccumulation!=null)
							{
								    dtoPayableAccountClassSetup.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
									Object[] arrayOfObject = serviceAccountPayable.getAccountNumberMultipleWays(glAccountsTableAccumulation,langId);
									accountNumber=(String) arrayOfObject[0];
									accountNumberIndex=(List<Long>) arrayOfObject[1];
									listOfKeyValue=(List<DtoPayableAccountClassSetup>) arrayOfObject[2];
									dtoPayableAccountClassSetup.setAccountDescription(arrayOfObject[3].toString());
							}
							dtoPayableAccountClassSetup.setAccountNumberIndex(accountNumberIndex);
							dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
							dtoPayableAccountClassSetup.setAccNumberIndexValue(listOfKeyValue);
							list.add(dtoPayableAccountClassSetup);
					  }
					  main.setAccountNumberList(list);
				}
			}
		}
		return main;
	}
	
	/**
	 * @param dtoPayableAccountClassSetup
	 * @return
	 */
	public DtoPayableAccountClassSetup saveAccountRecievableClassGlSetup(DtoPayableAccountClassSetup dtoPayableAccountClassSetup)
	{
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid = serviceHome.getLanngugaeId();
		GLAccountsTableAccumulation glAccountsTableAccumulation=null;
		try
		{
			List<Integer> listOfIds= repositoryCustomerClassAccountTableSetup.getByCustomerClassId(dtoPayableAccountClassSetup.getCustomerClassId());
			if(listOfIds!=null && !listOfIds.isEmpty()){
				repositoryCustomerClassAccountTableSetup.deleteRecordById(listOfIds,true);
			}
			
			if(dtoPayableAccountClassSetup.getAccountNumberList()!=null && !dtoPayableAccountClassSetup.getAccountNumberList().isEmpty() )
			{
				for (DtoPayableAccountClassSetup accountNumber : dtoPayableAccountClassSetup.getAccountNumberList()) 
				{
					CustomerClassAccountTableSetup customerClassAccountTableSetup=repositoryCustomerClassAccountTableSetup.findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(accountNumber.getAccountType(),dtoPayableAccountClassSetup.getCustomerClassId());
					if(customerClassAccountTableSetup==null){
						 customerClassAccountTableSetup= new CustomerClassAccountTableSetup();
					}
					customerClassAccountTableSetup.setDeleted(false);
					customerClassAccountTableSetup.setCustomerClassesSetup(repositoryCustomerClassesSetup.findByCustomerClassIdAndIsDeleted(dtoPayableAccountClassSetup.getCustomerClassId(),false));
					customerClassAccountTableSetup.setMasterArAccountType(repositoryMasterArAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(accountNumber.getAccountType(),langid,false));
					customerClassAccountTableSetup.setCreatedBy(loggedInUserId);
					StringBuilder sb = new StringBuilder();
					String fullIndex="";
					
					if(accountNumber.getAccountNumberIndex()!=null && !accountNumber.getAccountNumberIndex().isEmpty())
					{
						for(Long i : accountNumber.getAccountNumberIndex()){
						    sb.append(String.valueOf(i));
						}
						fullIndex = sb.toString();
					}
					List<GLAccountsTableAccumulation> glAccountsTableAccumulationsList = repositoryGLAccountsTableAccumulation.findByAccountNumberFullIndexAndIsDeleted(fullIndex,false);
					if(glAccountsTableAccumulationsList!=null && !glAccountsTableAccumulationsList.isEmpty())
					{
						glAccountsTableAccumulation=glAccountsTableAccumulationsList.get(0);
						glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
						
					}
					else
					{
						glAccountsTableAccumulation = new GLAccountsTableAccumulation();
						int actRowIndexId=1;
						PayableAccountClassSetup payableAccountClassSetup2 = repositoryPayableAccountClassSetup.findFirstByOrderByIdDesc();
						if(payableAccountClassSetup2!=null){
							actRowIndexId=payableAccountClassSetup2.getAccountRowIndex();
							actRowIndexId=actRowIndexId+1;
						}
						int segmentNumber=1;
						if(accountNumber.getAccountNumberIndex()!=null && !accountNumber.getAccountNumberIndex().isEmpty())
						{
							StringBuilder accountNumberfullIndex= new StringBuilder("");
							for (Long indexId : accountNumber.getAccountNumberIndex()) 
							{
								accountNumberfullIndex.append(String.valueOf(indexId));
								PayableAccountClassSetup payableAccountClassSetup = new PayableAccountClassSetup();
								payableAccountClassSetup.setIndexId(indexId);
								payableAccountClassSetup.setSegmentNumber(segmentNumber);
								segmentNumber++;
								payableAccountClassSetup.setAccountRowIndex(actRowIndexId);
								payableAccountClassSetup.setCreatedBy(loggedInUserId);
								repositoryPayableAccountClassSetup.save(payableAccountClassSetup);
							}
							
							glAccountsTableAccumulation.setAccountTableRowIndex(String.valueOf(actRowIndexId));
							glAccountsTableAccumulation.setSegmentNumber(segmentNumber);
							glAccountsTableAccumulation.setAccountNumberFullIndex(accountNumberfullIndex.toString());
							glAccountsTableAccumulation.setAccountDescription(accountNumber.getAccountDescription());
							if(accountNumber.getTransactionType()!=null){
								glAccountsTableAccumulation.setTransactionType(accountNumber.getTransactionType());
							}
							glAccountsTableAccumulation.setCreatedBy(loggedInUserId);
						}
					}
					    glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.saveAndFlush(glAccountsTableAccumulation);
						customerClassAccountTableSetup.setGlAccountsTableAccumulation(glAccountsTableAccumulation);
						repositoryCustomerClassAccountTableSetup.save(customerClassAccountTableSetup);
				}
			}
			else
			{
				dtoPayableAccountClassSetup.setMessageType(MessageLabel.REQUEST_BODY_IS_EMPTY);
			}
		}
		catch(Exception e){
			dtoPayableAccountClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if(dtoPayableAccountClassSetup.getMessageType()==null){
			return getCustomerAccountClassGlSetupByCustomerClassId(dtoPayableAccountClassSetup.getCustomerClassId());
		}
		else
		{
			return dtoPayableAccountClassSetup;
		}
	}
	
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public DtoPayableAccountClassSetup getCustomerAccountClassGlSetupByCustomerClassId(String customerClassId)
	{
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		DtoPayableAccountClassSetup main= null;
		
		List<CustomerClassAccountTableSetup> customerClassAccountTableSetupsList = repositoryCustomerClassAccountTableSetup.findByCustomerClassesSetupCustomerClassIdAndIsDeleted(customerClassId,false);
		List<DtoPayableAccountClassSetup> list = new ArrayList<>();
		if(customerClassAccountTableSetupsList!=null && !customerClassAccountTableSetupsList.isEmpty())
		{
			main=new DtoPayableAccountClassSetup();
			main.setCustomerClassId(customerClassId);
			for (CustomerClassAccountTableSetup customerAccountTableSetup : customerClassAccountTableSetupsList) 
			{
				List<Long> accountNumberIndex = new ArrayList<>();
				DtoPayableAccountClassSetup dtoPayableAccountClassSetup= new DtoPayableAccountClassSetup();
				dtoPayableAccountClassSetup.setAccountDescription("");
				dtoPayableAccountClassSetup.setAccountType(0);
				dtoPayableAccountClassSetup.setAccountTypeValue("");
				dtoPayableAccountClassSetup.setCustomerClassId("");
				if(customerAccountTableSetup.getCustomerClassesSetup()!=null){
					dtoPayableAccountClassSetup.setCustomerClassId(customerAccountTableSetup.getCustomerClassesSetup().getCustomerClassId());
				}
				
				
				MasterArAccountType masterArAccountType = customerAccountTableSetup.getMasterArAccountType();
				if(masterArAccountType!=null){
					int lang = Integer.parseInt(langId);
					if(masterArAccountType.getLanguage().getLanguageId()!=lang){
						masterArAccountType = repositoryMasterArAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterArAccountType.getTypeId(),lang,false);
					}
					if(masterArAccountType!=null)
					{
						dtoPayableAccountClassSetup
						.setAccountType(masterArAccountType.getTypeId());
					dtoPayableAccountClassSetup
							.setAccountTypeValue(masterArAccountType.getAccountType());
					}
				}
				
				GLAccountsTableAccumulation glAccountsTableAccumulation = customerAccountTableSetup.getGlAccountsTableAccumulation();
				String accountNumber="";
				List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
				if(glAccountsTableAccumulation!=null)
				{
					dtoPayableAccountClassSetup.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
					Object[] arrayOfObject= serviceAccountPayable.getAccountNumberMultipleWays(glAccountsTableAccumulation, langId);
					accountNumber=(String) arrayOfObject[0];
					accountNumberIndex=(List<Long>) arrayOfObject[1];
					listOfKeyValue=(List<DtoPayableAccountClassSetup>) arrayOfObject[2];
					dtoPayableAccountClassSetup.setAccountDescription(arrayOfObject[3].toString());
				}
				dtoPayableAccountClassSetup.setAccountNumberIndex(accountNumberIndex);
				dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
				dtoPayableAccountClassSetup.setAccNumberIndexValue(listOfKeyValue);
				list.add(dtoPayableAccountClassSetup);
			}
			main.setAccountNumberList(list);
		}
		return main;
	}
}