/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.MasterPostingAccountsSequence;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.dto.DtoPostingAccount;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryMasterAccountType;
import com.bti.repository.RepositoryMasterPostingAccountsSequence;
import com.bti.repository.RepositoryModuleConfiguration;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.util.UtilRandomKey;

@Service("servicePostingAccount")
public class ServicePostingAccount {
	
	private static final Logger LOGGER = Logger.getLogger(ServicePostingAccount.class);
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryMasterPostingAccountsSequence repositoryMasterPostingAccountsSequence;
	
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;
	
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	
	@Autowired
	RepositoryMasterAccountType repositoryMasterAccountType;
	
	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;
	
	@Autowired
	RepositoryModuleConfiguration repositoryModuleConfiguration;
	
	/**
	 * @param dtoPostingAccount
	 * @return
	 */
	public DtoPostingAccount savePostingAccount(DtoPostingAccount dtoPostingAccount) {
		MasterPostingAccountsSequence masterPostingAccountsSequence = null;
		try 
		{
			if (dtoPostingAccount != null) 
			{
				masterPostingAccountsSequence = new MasterPostingAccountsSequence();
				masterPostingAccountsSequence.setSeries(repositoryModuleConfiguration.findByModuleSeriesNumberAndIsDeleted(dtoPostingAccount.getSeriesNumber(),false));
				masterPostingAccountsSequence.setPostingAccountDescription(dtoPostingAccount.getDescriptionPrimary());
				masterPostingAccountsSequence
						.setPostingAccountDescriptionArabic(dtoPostingAccount.getDescriptionSecondary());
				
				masterPostingAccountsSequence.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoPostingAccount.getAccountTableRowIndex(),false));
				masterPostingAccountsSequence = repositoryMasterPostingAccountsSequence
						.saveAndFlush(masterPostingAccountsSequence);
			}
			
		} 
		catch (Exception e) {
			LOGGER.log(null, e);
		}
		return new DtoPostingAccount(masterPostingAccountsSequence);
	}

	/**
	 * @param dtoPostingAccount
	 * @return
	 */
	public DtoPostingAccount updatePostingAccount(DtoPostingAccount dtoPostingAccount) {
		MasterPostingAccountsSequence masterPostingAccountsSequence = null;

		try {
			masterPostingAccountsSequence = repositoryMasterPostingAccountsSequence
					.findOne(dtoPostingAccount.getPostingId());
			masterPostingAccountsSequence.setSeries(repositoryModuleConfiguration.findByModuleSeriesNumberAndIsDeleted(dtoPostingAccount.getSeriesNumber(),false));
			masterPostingAccountsSequence.setPostingAccountDescription(dtoPostingAccount.getDescriptionPrimary());
			masterPostingAccountsSequence
					.setPostingAccountDescriptionArabic(dtoPostingAccount.getDescriptionSecondary());
			
			masterPostingAccountsSequence.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoPostingAccount.getAccountTableRowIndex(),false));
			
			masterPostingAccountsSequence = repositoryMasterPostingAccountsSequence
					.saveAndFlush(masterPostingAccountsSequence);
		} catch (Exception e) {
			LOGGER.log(null, e);
		}

		return new DtoPostingAccount(masterPostingAccountsSequence);
	}

	/**
	 * @param postingId
	 * @return
	 */
	public boolean getPostingAccount(int postingId) {
		boolean status = false;
		try {
			MasterPostingAccountsSequence masterPostingAccountsSequence = repositoryMasterPostingAccountsSequence
					.findOne(postingId);
			if (masterPostingAccountsSequence != null) {
				status = true;
			}
		} catch (Exception e) {
			LOGGER.log(null, e);
		}

		return status;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchPostingAccount(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryMasterPostingAccountsSequence.predictivePostingAccountSearchCount(dtoSearchs.getSearchKeyword()));
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		DtoPostingAccount dtoPostingAccount = null;
		List<MasterPostingAccountsSequence> postingAccountsSequences = null;
		List<DtoPostingAccount> dtoPostingAccounts = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			postingAccountsSequences = repositoryMasterPostingAccountsSequence
					.predictivePostingAccountSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			postingAccountsSequences = repositoryMasterPostingAccountsSequence
					.predictivePostingAccountSearch(dtoSearchs.getSearchKeyword());
		}
		if (postingAccountsSequences != null) {
			for (MasterPostingAccountsSequence postingAccountsSequence : postingAccountsSequences) {
				dtoPostingAccount = new DtoPostingAccount(postingAccountsSequence, langId);
				if(postingAccountsSequence.getGlAccountsTableAccumulation()!=null)
				{
					dtoPostingAccount.setAccountNumber(getAccountNumberByAccountTableRowIndex(Integer.parseInt(postingAccountsSequence.getGlAccountsTableAccumulation().getAccountTableRowIndex())));
				}
				else
				{
					dtoPostingAccount.setAccountNumber("");
				}
				dtoPostingAccounts.add(dtoPostingAccount);
			}
		}
		dtoSearch.setRecords(dtoPostingAccounts);
		return dtoSearch;
	}

	/**
	 * @param postingId
	 * @return
	 */
	public DtoPostingAccount getPostingAccountByPostingId(int postingId) {
		DtoPostingAccount dtoPostingAccount = null;
		try {
			MasterPostingAccountsSequence masterPostingAccountsSequence = repositoryMasterPostingAccountsSequence
					.findOne(postingId);
			if (masterPostingAccountsSequence != null) {
				dtoPostingAccount = new DtoPostingAccount(masterPostingAccountsSequence);
				
				if(masterPostingAccountsSequence.getGlAccountsTableAccumulation()!=null)
				{
					dtoPostingAccount.setAccountNumber(getAccountNumberByAccountTableRowIndex(Integer.parseInt(masterPostingAccountsSequence.getGlAccountsTableAccumulation().getAccountTableRowIndex())));
				}
				else
				{
					dtoPostingAccount.setAccountNumber("");
				}
			}
		} catch (Exception e) {
			LOGGER.log(null, e);
		}

		return dtoPostingAccount;
	}
	
	
	/**
	 * @param accountTableRowIndexId
	 * @return
	 */
	public String getAccountNumberByAccountTableRowIndex(int accountTableRowIndexId) {
		StringBuilder accountNumber = new StringBuilder("");
		List<PayableAccountClassSetup> listOfPayable = repositoryPayableAccountClassSetup
				.findByAccountRowIndexAndIsDeleted(accountTableRowIndexId, false);

		int i = 0;
		for (PayableAccountClassSetup payableAccountClassSetup : listOfPayable) {
			if (i == 0) {
				COAMainAccounts coaMainAccounts = repositoryCOAMainAccounts
						.findByActIndxAndIsDeleted(payableAccountClassSetup.getIndexId(), false);

				if (coaMainAccounts != null && UtilRandomKey.isNotBlank(coaMainAccounts.getMainAccountNumber())) {
					accountNumber.append(coaMainAccounts.getMainAccountNumber());
				}

			} else {
				COAFinancialDimensionsValues coafValue = repositoryCOAFinancialDimensionsValues
						.findByDimInxValueAndIsDeleted(payableAccountClassSetup.getIndexId(), false);

				if (coafValue != null && UtilRandomKey.isNotBlank(coafValue.getDimensionValue())) {
					accountNumber.append("-" + coafValue.getDimensionValue());
				}

			}
			i++;
		}

		return accountNumber.toString();
	}
	
}
