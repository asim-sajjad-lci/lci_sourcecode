package com.bti.service;

import static org.mockito.Matchers.doubleThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.mapping.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jta.atomikos.AtomikosConnectionFactoryBean;
import org.springframework.stereotype.Service;

import com.bti.config.ResponseMessage;
import com.bti.constant.AuditTrailSeriesTypeConstant;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLBatches;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumber;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumberKey;
import com.bti.model.GLCurrentSummaryMasterTableByDimensions;
import com.bti.model.GLCurrentSummaryMasterTableByDimensionsKey;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccount;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccountKey;
import com.bti.model.GLPaymentVoucherDetail;
import com.bti.model.GLPaymentVoucherHeader;
import com.bti.model.GLYTDOpenBankTransactionsEntryHeader;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.GLYTDPaymentVoucherHeader;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.PaymentMethodType;
import com.bti.model.dto.DtoCheckBookMaintenance;
import com.bti.model.dto.DtoCreditCardSetUp;
import com.bti.model.dto.DtoGLPaymentVoucherDetail;
import com.bti.model.dto.DtoGLPaymentVoucherHeader;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCreditCardsSetup;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByDimensionIndex;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByMainAccount;
import com.bti.repository.RepositoryGLPaymentVoucherDetail;
import com.bti.repository.RepositoryGLPaymentVoucherHeader;
import com.bti.repository.RepositoryGLYTDPaymentVoucherDetail;
import com.bti.repository.RepositoryGLYTDPaymentVoucherHeader;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.repository.RepositoryPaymentMethodType;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.mapping.GL.MappingPaymentVoucher;
import com.jayway.restassured.path.json.JsonPath;

@Service("serviceGLPaymentVoucher")
public class ServiceGLPaymentVoucher {

	static Logger log = Logger.getLogger(ServiceCurrencyExchange.class.getName());

	@Autowired
	RepositoryGLPaymentVoucherHeader repositoryGLPaymentVoucherHeader;

	@Autowired
	RepositoryGLPaymentVoucherDetail repositoryGLPaymentVoucherDetail;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryPaymentMethodType repositoryPaymentMethodType;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;

	@Autowired
	ServiceAccountPayable serviceAccountPayable;

	@Autowired
	HttpServletRequest httpServlet;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;

	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;

	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;

	@Autowired
	RepositoryCreditCardsSetup repositoryCreditCardsSetup;

	@Autowired
	MappingPaymentVoucher mappingPaymentVoucher;
	
	@Autowired
	RepositoryGLYTDPaymentVoucherHeader repositoryGLYTDPaymentVoucherHeader;
	
	@Autowired
	RepositoryGLYTDPaymentVoucherDetail repositoryGLYTDPaymentVoucherDetail;
	
	@Autowired
	RepositoryGLCurrentSummaryMasterTableByMainAccount repositoryGLCurrentSummaryMasterTableByMainAccount;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByDimensionIndex repositoryGLCurrentSummaryMasterTableByDimensionIndex;
	
	@Autowired
	ServicePosting servicePosting;
	

	public DtoGLPaymentVoucherHeader getAllVoucersIDs(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {

		List<GLPaymentVoucherHeader> list = repositoryGLPaymentVoucherHeader.findAll();
		List<Integer> ids = new ArrayList<>();
		for (GLPaymentVoucherHeader glpaymentHeader : list) {
			ids.add(glpaymentHeader.getId());
		}
		dtoGLPaymentVoucherHeader.setIds(ids);
		return dtoGLPaymentVoucherHeader;
	}

	@Transactional
	public DtoGLPaymentVoucherHeader save(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		try {

			// ======= payment voucher header =========== //
			GLPaymentVoucherHeader glPaymentVoucherHeader = mappingPaymentVoucher
					.DTOToUnposted(dtoGLPaymentVoucherHeader);
			// save payment voucher header
			repositoryGLPaymentVoucherHeader.save(glPaymentVoucherHeader);
			repositoryGLPaymentVoucherDetail.save(glPaymentVoucherHeader.getPaymentVoucherDetail());
			return dtoGLPaymentVoucherHeader;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return null;
		}

	}

	public GLPaymentVoucherHeader findOne(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {

		GLPaymentVoucherHeader glPaymentVoucherHeader = repositoryGLPaymentVoucherHeader
				.getByPaymentVoucherIdAndIsDeleted(dtoGLPaymentVoucherHeader.getPaymentVoucherID());
		return glPaymentVoucherHeader;
	}

	public DtoGLPaymentVoucherHeader getNextPaymentVoucherID(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {

		int maxID = repositoryGLPaymentVoucherHeader.getMaxId();
		dtoGLPaymentVoucherHeader.setPaymentVoucherID(maxID);
		return dtoGLPaymentVoucherHeader;
	}

	public DtoGLPaymentVoucherHeader getPaymentVoucherIDs(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		List<Integer> ids = repositoryGLPaymentVoucherHeader.getPaymentVoucherId();
		Collections.sort(ids);
		dtoGLPaymentVoucherHeader.setIds(ids);

		return dtoGLPaymentVoucherHeader;
	}

	public DtoGLPaymentVoucherHeader getPaymentVoucherById(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {

		GLPaymentVoucherHeader glPaymentVoucherHeader = repositoryGLPaymentVoucherHeader
				.findByPaymentVoucherId(dtoGLPaymentVoucherHeader.getPaymentVoucherID());
		List<GLPaymentVoucherDetail> glPaymentVoucherDetailList = repositoryGLPaymentVoucherDetail
				.findByGlPaymentVoucherHeader(glPaymentVoucherHeader);
		List<DtoGLPaymentVoucherDetail> voucherDetailList = new ArrayList<>();
		GLAccountsTableAccumulation glAccountsTableAccumulation2 = null;
		String accountNumberH = "";
		String accounDecH = "";

			JsonPath jsonForm = null;
			glAccountsTableAccumulation2 = repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(glPaymentVoucherHeader.getAccountTableRowIndex(), false);

		dtoGLPaymentVoucherHeader.setAccountTableRowIndex(glPaymentVoucherHeader.getAccountTableRowIndex());
		dtoGLPaymentVoucherHeader.setAccountNumber(glAccountsTableAccumulation2.getAccountNumber());
		
		dtoGLPaymentVoucherHeader.setAccountNumber(accountNumberH);

		for (GLPaymentVoucherDetail glPaymentVoucherDetail : glPaymentVoucherDetailList) {
			DtoGLPaymentVoucherDetail dtoGLPaymentVoucherDetail = new DtoGLPaymentVoucherDetail();
			GLAccountsTableAccumulation glAccountsTableAccumulation = null;
				
				
			glAccountsTableAccumulation =  repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(glPaymentVoucherDetail.getAccountTableRowIndex(), false);
			dtoGLPaymentVoucherDetail.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
			dtoGLPaymentVoucherDetail.setAccountDecription(glAccountsTableAccumulation.getAccountDescription());
			dtoGLPaymentVoucherDetail.setAccountNumber(glAccountsTableAccumulation.getAccountNumber());
			dtoGLPaymentVoucherDetail.setDebitAmount(glPaymentVoucherDetail.getDebitAmount());
			dtoGLPaymentVoucherDetail.setOriginalDebitAmount(glPaymentVoucherDetail.getOriginalDebitAmount());
			dtoGLPaymentVoucherDetail.setDistributionDescription(glPaymentVoucherDetail.getDistributionDescription());
            String companyName = serviceHome.getCompanyName(
                    Integer.parseInt(glPaymentVoucherDetail.getInterCompanyIDMulticompanyTransaction()));
			dtoGLPaymentVoucherDetail.setInterCompanyIDMultiCompanyTransaction(
					glPaymentVoucherDetail.getInterCompanyIDMulticompanyTransaction());
			dtoGLPaymentVoucherDetail.setCompanyName(companyName);
			voucherDetailList.add(dtoGLPaymentVoucherDetail);
		

		}
		
		dtoGLPaymentVoucherHeader = new DtoGLPaymentVoucherHeader(glPaymentVoucherHeader);
		dtoGLPaymentVoucherHeader.setAccountTableRowIndex(glAccountsTableAccumulation2.getAccountTableRowIndex());;
		dtoGLPaymentVoucherHeader.setAccountNumber(glAccountsTableAccumulation2.getAccountNumber());
		dtoGLPaymentVoucherHeader.setVoucherDetailList(voucherDetailList);	
		
		return dtoGLPaymentVoucherHeader;
	}

	public boolean deleteJournalEntry(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		try {
			GLPaymentVoucherHeader glPaymentVoucherHeader = repositoryGLPaymentVoucherHeader
					.getByPaymentVoucherIdAndIsDeleted(dtoGLPaymentVoucherHeader.getPaymentVoucherID());
			if (glPaymentVoucherHeader != null) {
				List<GLPaymentVoucherDetail> glPaymentVoucherDetails = repositoryGLPaymentVoucherDetail
						.findByGlPaymentVoucherHeader(glPaymentVoucherHeader);
				if (glPaymentVoucherDetails != null && !glPaymentVoucherDetails.isEmpty()) {
					for (GLPaymentVoucherDetail gl : glPaymentVoucherDetails) {
						gl.setIsDeleted(true);
						repositoryGLPaymentVoucherDetail.save(gl);
					}
				}
				glPaymentVoucherHeader.setIsDeleted(true);
				repositoryGLPaymentVoucherHeader.save(glPaymentVoucherHeader);
			}
			return true;
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}

	public DtoGLPaymentVoucherHeader updatePaymentVoucher(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {

		try {
			
			GLPaymentVoucherHeader newgl2 = mappingPaymentVoucher.DTOToUnposted(dtoGLPaymentVoucherHeader);
			
			GLPaymentVoucherHeader gl2 = repositoryGLPaymentVoucherHeader
					.findByPaymentVoucherId(dtoGLPaymentVoucherHeader.getPaymentVoucherID());
			
			repositoryGLPaymentVoucherDetail.delete(gl2.getPaymentVoucherDetail());
			repositoryGLPaymentVoucherHeader.delete(gl2);
			

			// delete old one
			repositoryGLPaymentVoucherHeader.save(newgl2);
			repositoryGLPaymentVoucherDetail.save(newgl2.getPaymentVoucherDetail());
		} catch (Exception e) {
			// TODO: handle exception
			dtoGLPaymentVoucherHeader = null;
		}

		return dtoGLPaymentVoucherHeader;
	}

//	public boolean deletePaymentVoucher(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
//
//		GLPaymentVoucherHeader glPaymentVoucherHeader = repositoryGLPaymentVoucherHeader
//				.findByPaymentVoucherId(dtoGLPaymentVoucherHeader.getPaymentVoucherID());
//		if (glPaymentVoucherHeader != null) {
//			List<GLPaymentVoucherDetail> paymentDetailList = repositoryGLPaymentVoucherDetail
//					.findByGlPaymentVoucherHeader(glPaymentVoucherHeader);
//
//			if (paymentDetailList != null) {
//				repositoryGLPaymentVoucherDetail.deleteInBatch(paymentDetailList);
//			}
//
//			repositoryGLPaymentVoucherHeader.delete(glPaymentVoucherHeader);
//		}
//		return false;
//	}

	public GLPaymentVoucherHeader postPaymentVoucher(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		// TODO: post to gl91101 & gl91100	
		// post to posted stage gl90200
		GLPaymentVoucherHeader glpaymentvoucherHeader = mappingPaymentVoucher.DTOToUnposted(dtoGLPaymentVoucherHeader);
		DtoJournalEntryHeader dtoJournalHeader = mappingPaymentVoucher.modelPVPostedToDtoJVUnposted(glpaymentvoucherHeader);
		try {
	
			double creditAmount = 0.0;
			
			String number = servicePosting.saveAndPostingWithSequence(dtoJournalHeader, 2);
			
			//validate sequence 
			if((number == "FOUND") || number.equalsIgnoreCase("FOUND")) {
				glpaymentvoucherHeader.setPaymentVoucherId(0);
				return glpaymentvoucherHeader;
			}else {
				//================================================================================================================//
				
				if (dtoGLPaymentVoucherHeader.getVoucherDetailList() != null
						&& !dtoGLPaymentVoucherHeader.getVoucherDetailList().isEmpty()) {

					for (DtoGLPaymentVoucherDetail dtoGLPaymentVoucherDetail : dtoGLPaymentVoucherHeader
							.getVoucherDetailList()) {

						double debitAmount = 0.0;

						//TODO: post to gl11100  & gl11101
						
						GLPaymentVoucherHeader glPaymentVoucherHeader = repositoryGLPaymentVoucherHeader.findByPaymentVoucherId(dtoGLPaymentVoucherHeader.getPaymentVoucherID());
						
						GLYTDPaymentVoucherHeader glytdPaymentVoucherHeader = mappingPaymentVoucher.unpostedToPosted(glPaymentVoucherHeader);
						
						repositoryGLYTDPaymentVoucherHeader.save(glytdPaymentVoucherHeader);
						repositoryGLYTDPaymentVoucherDetail.save(glytdPaymentVoucherHeader.getGlytdPaymentVoucherDetails());
						// TODO: post to gl90110 & gl90112


						String accountNumberWithZero = null;
						Integer year = null;
						Integer periodId = null;
						int dimIndex1 = 0;
						int dimIndex2 = 0;
						int dimIndex3 = 0;
						int dimIndex4 = 0;
						int dimIndex5 = 0;
						int mainAccountIndex = 0;
						int accountTableRowIndex = 0;

						GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
								.findByAccountTableRowIndexAndIsDeleted(dtoGLPaymentVoucherDetail.getAccountTableRowIndex(),
										false);
						if (glAccountsTableAccumulation != null) {
							accountTableRowIndex = Integer.parseInt(dtoGLPaymentVoucherDetail.getAccountTableRowIndex());
							accountNumberWithZero = serviceAccountPayable
									.getAccountNumberIdsWithZero(glAccountsTableAccumulation);
						}

						if (accountNumberWithZero != null && UtilRandomKey.isNotBlank(accountNumberWithZero)) {
							String[] accountNo = accountNumberWithZero.split("-");
							for (int i = 0; i < accountNo.length; i++) {
								if (i > 0) {
									if (i == 1) {
										dimIndex1 = Integer.parseInt(accountNo[i]);
									}
									if (i == 2) {
										dimIndex2 = Integer.parseInt(accountNo[i]);
									}
									if (i == 3) {
										dimIndex3 = Integer.parseInt(accountNo[i]);
									}
									if (i == 4) {
										dimIndex4 = Integer.parseInt(accountNo[i]);
									}
									if (i == 5) {
										dimIndex5 = Integer.parseInt(accountNo[i]);
									}
								}
							}
							mainAccountIndex = Integer.parseInt(accountNo[0]);
						}

						Calendar cal = Calendar.getInstance();
						if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTransactionDate())) {
							Date transDate = UtilDateAndTime
									.yyyymmddStringToDate(dtoGLPaymentVoucherHeader.getTransactionDate());
							cal.setTime(transDate);
							year = cal.get(Calendar.YEAR);
							periodId = cal.get(Calendar.MONTH);
						}

						// find by composite key
						GLCurrentSummaryMasterTableByMainAccountKey currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
						currSumaryByMainAccKey.setYear(year);
						currSumaryByMainAccKey.setPeriodID(periodId + 1);
						currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
						GLCurrentSummaryMasterTableByMainAccount currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount
								.findOne(currSumaryByMainAccKey);
						if (currSumaryByMainAcc != null) {
							currSumaryByMainAcc.setDebitAmount(currSumaryByMainAcc.getDebitAmount() + debitAmount);
							currSumaryByMainAcc.setCreditAmount(currSumaryByMainAcc.getCreditAmount() + creditAmount);
							currSumaryByMainAcc.setPeriodBalance(
									currSumaryByMainAcc.getDebitAmount() - currSumaryByMainAcc.getCreditAmount());
						} else {
							currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
							currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
							currSumaryByMainAcc.setYear(year);
							currSumaryByMainAcc.setPeriodID(periodId + 1);
							currSumaryByMainAcc.setDebitAmount(debitAmount);
							currSumaryByMainAcc.setCreditAmount(creditAmount);
							currSumaryByMainAcc.setPeriodBalance(debitAmount - creditAmount);
						}

						currSumaryByMainAcc.setRowDateIndex(new Date());
						repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);

						GLCurrentSummaryMasterTableByAccountNumberKey currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
						currSumaryByAccNumberKey.setYear(year);
						currSumaryByAccNumberKey.setAccountTableRowIndex(accountTableRowIndex);
						currSumaryByAccNumberKey.setPeriodID(periodId + 1);
						GLCurrentSummaryMasterTableByAccountNumber currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
								.findOne(currSumaryByAccNumberKey);
						if (currSumaryByAccNumber != null) {
							currSumaryByAccNumber.setDebitAmount(currSumaryByAccNumber.getDebitAmount() + debitAmount);
							currSumaryByAccNumber.setCreditAmount(currSumaryByAccNumber.getCreditAmount() + creditAmount);
							currSumaryByAccNumber.setPeriodBalance(
									currSumaryByAccNumber.getDebitAmount() - currSumaryByAccNumber.getCreditAmount());
						} else {
							currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
							currSumaryByAccNumber.setAccountTableRowIndex(accountTableRowIndex);
							currSumaryByAccNumber.setYear(year);
							currSumaryByAccNumber.setPeriodID(periodId + 1);
							currSumaryByAccNumber.setDebitAmount(debitAmount);
							currSumaryByAccNumber.setCreditAmount(creditAmount);
							currSumaryByAccNumber.setPeriodBalance(debitAmount - creditAmount);
						}
						currSumaryByAccNumber.setIsDeleted(false);
						currSumaryByAccNumber.setRowDateIndex(new Date());
						repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

						GLCurrentSummaryMasterTableByDimensionsKey glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
						glCurreSummaryByDimInxKey.setYear(year);
						glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
						glCurreSummaryByDimInxKey.setPeriodID(periodId + 1);
						GLCurrentSummaryMasterTableByDimensions glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
								.findOne(glCurreSummaryByDimInxKey);
						if (glCurreSummaryByDimInx != null) {
							glCurreSummaryByDimInx.setCreditAmount(glCurreSummaryByDimInx.getCreditAmount() + creditAmount);
							glCurreSummaryByDimInx.setDebitAmount(glCurreSummaryByDimInx.getDebitAmount() + debitAmount);
							glCurreSummaryByDimInx.setPeriodBalance(
									glCurreSummaryByDimInx.getDebitAmount() - glCurreSummaryByDimInx.getCreditAmount());
						} else {
							glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
							glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
							glCurreSummaryByDimInx.setYear(year);
							glCurreSummaryByDimInx.setPeriodID(periodId + 1);
							glCurreSummaryByDimInx.setCreditAmount(creditAmount);
							glCurreSummaryByDimInx.setDebitAmount(debitAmount);
							glCurreSummaryByDimInx.setPeriodBalance(debitAmount - creditAmount);
						}

						glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
						glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
						glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
						glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
						glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
						glCurreSummaryByDimInx.setRowDateIndex(new Date());
						repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);
						
						
					}

					// delet from unposted tables ( gl11100 & gl11101 )
					GLPaymentVoucherHeader glPaymentVoucherHeader = repositoryGLPaymentVoucherHeader
							.findByPaymentVoucherId(dtoGLPaymentVoucherHeader.getPaymentVoucherID());
//					DtoJournalEntryHeader dtoJournalEntryHeader =  mappingPaymentVoucher.modelPVPostedToDtoJVUnposted(glPaymentVoucherHeader);
//					String nextNumber = servicePosting.saveAndPostingWithSequence(dtoJournalEntryHeader, 2);
					if (glPaymentVoucherHeader != null) {
						List<GLPaymentVoucherDetail> list = repositoryGLPaymentVoucherDetail
								.findByGlPaymentVoucherHeader(glPaymentVoucherHeader);
						if (list != null && !list.isEmpty()) {
							for (GLPaymentVoucherDetail glPaymentVoucherDetail : list) {
								glPaymentVoucherDetail.setIsDeleted(true);
								repositoryGLPaymentVoucherDetail.save(glPaymentVoucherDetail);
							}
						}
						glPaymentVoucherHeader.setIsPosted(true);
						repositoryGLPaymentVoucherHeader.save(glPaymentVoucherHeader);
					}

				}

				return glpaymentvoucherHeader;
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			glpaymentvoucherHeader.setPaymentVoucherId(911);
			return glpaymentvoucherHeader;
		}

	}

	public DtoCheckBookMaintenance getCheckboodIDAccount(DtoCheckBookMaintenance dtoCheckBookMaintenance) {

		CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance
				.findByCheckBookIdAndIsDeleted(dtoCheckBookMaintenance.getCheckBookId(), false);
		
		GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation.
				findByAccountTableRowIndexAndIsDeleted(checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex(), false);
		

		if (checkbookMaintenance != null) {
			dtoCheckBookMaintenance = new DtoCheckBookMaintenance(checkbookMaintenance);
			dtoCheckBookMaintenance.setAccountNumber(glAccountsTableAccumulation.getAccountNumber());
			dtoCheckBookMaintenance.setAccountRowIDIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
			dtoCheckBookMaintenance.setBankName(checkbookMaintenance.getBankSetup().getBankDescription());
		} else {
			dtoCheckBookMaintenance = null;
		}
		return dtoCheckBookMaintenance;
	}

	public DtoCreditCardSetUp getCreditCardAccountDetail(DtoCreditCardSetUp dtoCreditCardSetUp) {

		return dtoCreditCardSetUp;
	}

}
