/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.boot.jaxb.Origin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.constant.ConfigSetting;
import com.bti.constant.MessageLabel;
import com.bti.model.COAFinancialDimensions;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationDefinePeriodsDetails;
import com.bti.model.GLConfigurationDefinePeriodsHeader;
import com.bti.model.GLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.MasterBlncdspl;
import com.bti.model.MasterMassCloseOrigin;
import com.bti.model.ModulesConfigurationPeriod;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.SeriesType;
import com.bti.model.dto.DtoCheckBookMaintenance;
import com.bti.model.dto.DtoFinancialDimensionSetUp;
import com.bti.model.dto.DtoFinancialDimensionValue;
import com.bti.model.dto.DtoFiscalFinancialPeriodSetup;
import com.bti.model.dto.DtoFiscalFinancialPeriodSetupDetail;
import com.bti.model.dto.DtoGLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.bti.model.dto.DtoGLDisplaytype;
import com.bti.model.dto.DtoGeneralLedgerSetup;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoMainAccountSetUp;
import com.bti.model.dto.DtoMassCloseFiscalPeriodSetup;
import com.bti.model.dto.DtoMassCloseFiscalPeriodSetupRequest;
import com.bti.model.dto.DtoOrigin;
import com.bti.model.dto.DtoPeriods;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryBankSetup;
import com.bti.repository.RepositoryCOAFinancialDimensions;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationDefinePeriodsDetails;
import com.bti.repository.RepositoryGLConfigurationDefinePeriodsHeader;
import com.bti.repository.RepositoryGLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryMassCloseOrigin;
import com.bti.repository.RepositoryMasterBlncdspl;
import com.bti.repository.RepositoryModuleConfiguration;
import com.bti.repository.RepositoryModulesConfigurationPeriod;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.repository.RepositorySeriesType;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceGeneralLedgerSetup
 * Name of Project: BTI
 * Created on: Aug 09, 2017
 * Modified on: Aug 09, 2017 02:35:38 AM
 * @author seasia
 * Version: 
 */
@Service("serviceGeneralLedgerSetup")
public class ServiceGeneralLedgerSetup {

	private static final Logger LOG = Logger.getLogger(ServiceGeneralLedgerSetup.class);
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;
	
	@Autowired
	RepositoryMasterBlncdspl repositoryMasterBlncdspl;
	
	@Autowired
	RepositoryGLConfigurationDefinePeriodsHeader repositoryGLConfigurationDefinePeriodsHeader;
	
	@Autowired
	RepositoryGLConfigurationDefinePeriodsDetails repositoryGLConfigurationDefinePeriodsDetails;
	
	@Autowired
	RepositoryBankSetup repositoryBankSetup;
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;
	
	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;
	
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	
	@Autowired
	RepositoryCOAFinancialDimensions repositoryCOAFinancialDimensions;
	
	@Autowired
	RepositoryGLConfigurationRelationBetweenDimensionsAndMainAccount repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount;
	
	@Autowired
	RepositorySeriesType repositorySeriesType;
	
	@Autowired
	RepositoryModulesConfigurationPeriod repositoryModulesConfigurationPeriod;
	
	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;
	
	@Autowired
	RepositoryModuleConfiguration repositoryModuleConfiguration;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryMassCloseOrigin repositoryMassCloseOrigin;
	
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	public String getNextJournalEntry(String transactionType ,Calendar transactionDate) {
		
		StringBuilder sequence = new StringBuilder();;
		GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
		
		if(glConfigurationSetup.getSequenceType() == 1) {
			sequence.append(glConfigurationSetup.getNextJournalEntryVoucher()+"");
			
		}else if(glConfigurationSetup.getSequenceType() == 2) { 
			
		}
		
		if(glConfigurationSetup.getIncludeDate()) {
			Calendar cal = Calendar.getInstance();
			System.out.println( String.valueOf(cal.get(Calendar.MONTH)) );
			System.out.println( cal.get(Calendar.MONTH) );
			
			sequence.insert(0,String.valueOf(cal.get(Calendar.YEAR)).substring(2) + String.format("%02d", cal.get(Calendar.MONTH)));
		}
		
		return sequence.toString();
	}
	
	
	/**
	 * @param dtoGeneralLedgerSetup
	 * @param glConfigurationSetup
	 * @return
	 */
	public DtoGeneralLedgerSetup saveGeneralLedgerSetup(DtoGeneralLedgerSetup dtoGeneralLedgerSetup, GLConfigurationSetup glConfigurationSetup) {
		
		int loggedInUserId=0;
		if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
			loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		
		int langId = serviceHome.getLanngugaeId();
		try {
			glConfigurationSetup.setMaintainAccountsInHistory(dtoGeneralLedgerSetup.isMaintainHistoryAccounts());
			glConfigurationSetup.setMasterBlncdspl(repositoryMasterBlncdspl.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoGeneralLedgerSetup.getDisplayNCorPB(),langId,false));
			glConfigurationSetup
					.setMaintainBudgetTransactionsHistory(dtoGeneralLedgerSetup.isMaintainHistoryBudgetTransactions());
			glConfigurationSetup.setCreatedBy(loggedInUserId);
			glConfigurationSetup
					.setAllowDeletionOfSavedTransactions(dtoGeneralLedgerSetup.isAllowDeletionOfSavedTrans());
			glConfigurationSetup.setNextJournalEntryVoucher(dtoGeneralLedgerSetup.getNextJournalEntry());
			if(dtoGeneralLedgerSetup.isRetainedEraningsCheckBox()){
				glConfigurationSetup.setLastCloseYear(dtoGeneralLedgerSetup.getAccountSegments());
			}
			
			if(UtilRandomKey.isNotBlank(dtoGeneralLedgerSetup.getCashPayment()+"")) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGeneralLedgerSetup.getCashPayment(), false);
				glConfigurationSetup.setCashAuditTrialPayment(audit);
			}
			if(UtilRandomKey.isNotBlank(dtoGeneralLedgerSetup.getCashReceipt()+"")) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGeneralLedgerSetup.getCashReceipt(), false);
				glConfigurationSetup.setCashAuditTrialReceipt(audit);
			}
			
			if(UtilRandomKey.isNotBlank(dtoGeneralLedgerSetup.getCheckPayment()+"")) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGeneralLedgerSetup.getCheckPayment(), false);
				glConfigurationSetup.setCheckAuditTrialPayment(audit);
			}
			if(UtilRandomKey.isNotBlank(dtoGeneralLedgerSetup.getCheckReceipt()+"")) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGeneralLedgerSetup.getCheckReceipt(), false);
				glConfigurationSetup.setcheckAuditTrialReceipt(audit);
			}
			
			if(UtilRandomKey.isNotBlank(dtoGeneralLedgerSetup.getTransferPayment()+"")) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGeneralLedgerSetup.getTransferPayment(), false);
				glConfigurationSetup.setTransferAuditTrialPayment(audit);
			}
			if(UtilRandomKey.isNotBlank(dtoGeneralLedgerSetup.getTransferReceipt()+"")) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGeneralLedgerSetup.getTransferReceipt(), false);
				glConfigurationSetup.setTransferAuditTrialReceipt(audit);
			}
			
			if(UtilRandomKey.isNotBlank(dtoGeneralLedgerSetup.getCreditPayment()+"")) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGeneralLedgerSetup.getCreditPayment(), false);
				glConfigurationSetup.setCreditAuditTrialPayment(audit);
			}
			if(UtilRandomKey.isNotBlank(dtoGeneralLedgerSetup.getCreditReceipt()+"")) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGeneralLedgerSetup.getCreditReceipt(), false);
				glConfigurationSetup.setCreditAuditTrialReceipt(audit);
			}
			
			
			glConfigurationSetup.setNextReconciliationNumber(dtoGeneralLedgerSetup.getNextReconciliation());
			glConfigurationSetup.setNextBudgetJournalId(dtoGeneralLedgerSetup.getNextBudgetJournalEntry());
			glConfigurationSetup.setAllowPostingToHistory(dtoGeneralLedgerSetup.isAllowPostingToHistory());
			glConfigurationSetup.setRetainedEarningsAccountIndex1(repositoryCOAMainAccounts.findByActIndxAndIsDeleted(dtoGeneralLedgerSetup.getMainAccount1(),false));
			glConfigurationSetup.setRetainedEarningsAccountIndex2(repositoryCOAMainAccounts.findByActIndxAndIsDeleted(dtoGeneralLedgerSetup.getMainAccount2(),false));
			glConfigurationSetup
					.setMaintainTransactionsInHistory(dtoGeneralLedgerSetup.isMaintainHistoryTransactions());
			glConfigurationSetup.setTrueRetainedEaringClosing(dtoGeneralLedgerSetup.isRetainedEraningsCheckBox());
			glConfigurationSetup.setLabelUserDefine1(dtoGeneralLedgerSetup.getUserDefine1());
			glConfigurationSetup.setLabelUserDefine52(dtoGeneralLedgerSetup.getUserDefine2());
			glConfigurationSetup.setLabelUserDefine3(dtoGeneralLedgerSetup.getUserDefine3());
			glConfigurationSetup.setLabelUserDefine4(dtoGeneralLedgerSetup.getUserDefine4());
			glConfigurationSetup.setLabelUserDefine5(dtoGeneralLedgerSetup.getUserDefine5());
			glConfigurationSetup
					.setAllowVoidingCorrectingOfTransactions(dtoGeneralLedgerSetup.isAllowvoidingCorrectingSubTrans());
			glConfigurationSetup.setIncludeDate(dtoGeneralLedgerSetup.isIncludeDate());
			glConfigurationSetup.setSequenceType(dtoGeneralLedgerSetup.getSequenceType());
			glConfigurationSetup.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoGeneralLedgerSetup.getAccountTableRowIndex(),false));
			glConfigurationSetup = repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);
		
		} catch (Exception e) {
			LOG.info(e.getStackTrace());
		}
		MasterBlncdspl masterBlncdspl = glConfigurationSetup.getMasterBlncdspl();
		if(masterBlncdspl!=null){
			masterBlncdspl = repositoryMasterBlncdspl.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterBlncdspl.getTypeId(),langId,false);
		}
		return new DtoGeneralLedgerSetup(glConfigurationSetup, masterBlncdspl);
	}

	/**
	 * @return
	 */
	public List<DtoGLDisplaytype> getAllDisplayStatus() {
		int langId =serviceHome.getLanngugaeId();
		List<DtoGLDisplaytype> displaytypes = new ArrayList<>();
		DtoGLDisplaytype displaytype;
		List<MasterBlncdspl> blncdspls = repositoryMasterBlncdspl.findByLanguageLanguageIdAndIsDeleted(langId,false);
		for (MasterBlncdspl masterBlncdspl : blncdspls) {
			displaytype = new DtoGLDisplaytype(masterBlncdspl);
			displaytypes.add(displaytype);
		}
		return displaytypes;
	}

	/**
	 * @Description Saving and updating Fiscal Finance Period Setup
	 * @param dtoFiscalFinancialPeriodSetup
	 * @param configurationDefinePeriodsHeader
	 * @return
	 */
	public DtoFiscalFinancialPeriodSetup saveAndUpdateFiscalFinancePeriodSetup(
			DtoFiscalFinancialPeriodSetup dtoFiscalFinancialPeriodSetup/*, GLConfigurationDefinePeriodsHeader configurationDefinePeriodsHeader*/) {
			GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetails; 
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
				// create new period setup
				//saving configuration Define Periods Header
			
			List<ModulesConfigurationPeriod> list= repositoryModulesConfigurationPeriod.findByYear(dtoFiscalFinancialPeriodSetup.getYear());
			if(list!=null && !list.isEmpty()){
				repositoryModulesConfigurationPeriod.deleteInBatch(list);
			}
			
			// Check header detail exist for this year 
			GLConfigurationDefinePeriodsHeader configurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.findByYearAndIsDeleted(
					dtoFiscalFinancialPeriodSetup.getYear(), false);
			if(configurationDefinePeriodsHeader==null){
				configurationDefinePeriodsHeader=new GLConfigurationDefinePeriodsHeader();
			}
			
			configurationDefinePeriodsHeader.setYear(dtoFiscalFinancialPeriodSetup.getYear());
			if(UtilRandomKey.isNotBlank(dtoFiscalFinancialPeriodSetup.getFirstDay())){
				configurationDefinePeriodsHeader.setFirstFiscalDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFiscalFinancialPeriodSetup.getFirstDay()));
			}
			
			if(UtilRandomKey.isNotBlank(dtoFiscalFinancialPeriodSetup.getLastDay())){
				configurationDefinePeriodsHeader.setLastFiscalDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFiscalFinancialPeriodSetup.getLastDay()));
			}
			
			configurationDefinePeriodsHeader.setHistoricalYear(dtoFiscalFinancialPeriodSetup.isHistoricalYear());
			configurationDefinePeriodsHeader.setNumberOfPeriods(dtoFiscalFinancialPeriodSetup.getNumberOfPeriods());
			configurationDefinePeriodsHeader.setCreatedBy(loggedInUserId);
			configurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.saveAndFlush(configurationDefinePeriodsHeader);
			
			//delete old configuration period detail
			List<GLConfigurationDefinePeriodsDetails> definePeriodDetailList= repositoryGLConfigurationDefinePeriodsDetails.
					findByGlConfigurationDefinePeriodsHeaderYearAndIsDeleted(dtoFiscalFinancialPeriodSetup.getYear(), false);
			if(definePeriodDetailList!=null && !definePeriodDetailList.isEmpty()){
				repositoryGLConfigurationDefinePeriodsDetails.deleteInBatch(definePeriodDetailList);
			}
			//saving new configuration Define Periods Details
				if(configurationDefinePeriodsHeader !=null  && !dtoFiscalFinancialPeriodSetup.getDtoFiscalFinancialPeriodSetupDetail().isEmpty()){
					for(DtoFiscalFinancialPeriodSetupDetail detail : dtoFiscalFinancialPeriodSetup.getDtoFiscalFinancialPeriodSetupDetail()){
						glConfigurationDefinePeriodsDetails = new GLConfigurationDefinePeriodsDetails();
						glConfigurationDefinePeriodsDetails.setPeriodId(detail.getPeriodId());
						if(UtilRandomKey.isNotBlank(detail.getStartPeriodDate())){
							glConfigurationDefinePeriodsDetails.setStartPeriodDate(UtilDateAndTime.ddmmyyyyStringToDate(detail.getStartPeriodDate()));
						}
						if(UtilRandomKey.isNotBlank(detail.getEndPeriodDate())){
							glConfigurationDefinePeriodsDetails.setEndPeriodDate(UtilDateAndTime.ddmmyyyyStringToDate(detail.getEndPeriodDate()));
						}
						glConfigurationDefinePeriodsDetails.setPeriodName(detail.getPeriodNamePrimary());
						glConfigurationDefinePeriodsDetails.setPeriodNameArabic(detail.getPeriodNameSecondary());
						glConfigurationDefinePeriodsDetails.setGlConfigurationDefinePeriodsHeader(configurationDefinePeriodsHeader);
						glConfigurationDefinePeriodsDetails.setPeriodSeriesFinancial(detail.isPeriodSeriesFinancial());
						glConfigurationDefinePeriodsDetails.setPeriodSeriesInventory(detail.isPeriodSeriesInventory());
						glConfigurationDefinePeriodsDetails.setPeriodSeriesPayroll(detail.isPeriodSeriesPayroll());
						glConfigurationDefinePeriodsDetails.setPeriodSeriesProject(detail.isPeriodSeriesProject());
						glConfigurationDefinePeriodsDetails.setPeriodSeriesPurchase(detail.isPeriodSeriesPurchase());
						glConfigurationDefinePeriodsDetails.setPeriodSeriesSales(detail.isPeriodSeriesSales());
						glConfigurationDefinePeriodsDetails.setCreatedBy(loggedInUserId);
						repositoryGLConfigurationDefinePeriodsDetails.saveAndFlush(glConfigurationDefinePeriodsDetails);
					}
				}
				
				List<SeriesType> seriesTypeList = repositorySeriesType.findByLanguageLanguageIdAndIsDeleted(1,false);
				if(seriesTypeList!=null && !seriesTypeList.isEmpty()){
					for (SeriesType seriesType : seriesTypeList) {
						DtoMassCloseFiscalPeriodSetupRequest searchRequest= new DtoMassCloseFiscalPeriodSetupRequest();
						searchRequest.setYear(dtoFiscalFinancialPeriodSetup.getYear());
						searchRequest.setSeriesId(seriesType.getTypeId());
						searchRequest.setAllRecord(true);
						searchRequest.setAllOrigin(true);
						searchRequest.setOriginId(0);
						List<DtoMassCloseFiscalPeriodSetup> saveRecordsList  = getMassCloseFiscalPeriodSetup(searchRequest);
						
						DtoMassCloseFiscalPeriodSetup saveRecords= new DtoMassCloseFiscalPeriodSetup();
						saveRecords.setYear(dtoFiscalFinancialPeriodSetup.getYear());
						saveRecords.setSeriesId(seriesType.getTypeId());
						saveRecords.setRecords(saveRecordsList);
						saveMassCloseFiscalPeriodSetup(saveRecords);
					}
				}
				
		return dtoFiscalFinancialPeriodSetup;
		
	}

	/**
	 * @return
	 */
	public DtoFiscalFinancialPeriodSetup getFiscalFinancePeriodSetup() 
	{
		DtoFiscalFinancialPeriodSetup dtoFiscalFinancialPeriodSetup = new DtoFiscalFinancialPeriodSetup();
		List<DtoFiscalFinancialPeriodSetupDetail> dtoFiscalFinancialPeriodSetupDetailList=new ArrayList<>();
		try 
		{
			GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.
					findAll().get(0);
			List<GLConfigurationDefinePeriodsDetails> configurationDefinePeriodsDetails;
			List<DtoFiscalFinancialPeriodSetupDetail> dtoFiscalFinancialPeriodSetupDetails = new ArrayList<>();
			DtoFiscalFinancialPeriodSetupDetail dtoFiscalFinancialPeriodSetupDetail;
			if(glConfigurationDefinePeriodsHeader != null)
			{
				dtoFiscalFinancialPeriodSetup = new DtoFiscalFinancialPeriodSetup(glConfigurationDefinePeriodsHeader);
				configurationDefinePeriodsDetails = repositoryGLConfigurationDefinePeriodsDetails.findByGlConfigurationDefinePeriodsHeaderYearAndIsDeleted(glConfigurationDefinePeriodsHeader.getYear(),false);
				for(GLConfigurationDefinePeriodsDetails configurationDefinePeriodsDetails2 : configurationDefinePeriodsDetails){
					dtoFiscalFinancialPeriodSetupDetail = new DtoFiscalFinancialPeriodSetupDetail(configurationDefinePeriodsDetails2);
					dtoFiscalFinancialPeriodSetupDetails.add(dtoFiscalFinancialPeriodSetupDetail);
				}
			}
			
			dtoFiscalFinancialPeriodSetupDetailList = dtoFiscalFinancialPeriodSetupDetails;
			dtoFiscalFinancialPeriodSetup.setDtoFiscalFinancialPeriodSetupDetail(dtoFiscalFinancialPeriodSetupDetailList);
		
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoFiscalFinancialPeriodSetup;
	}
	
	/**
	 * @param dtoCheckBookMaintenance
	 * @return
	 */
	public DtoCheckBookMaintenance saveCheckBookMaintenance(DtoCheckBookMaintenance dtoCheckBookMaintenance) 
	{
		CheckbookMaintenance checkbookMaintenance = null;
		int loggedInUserId=0;
		if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
			loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		try {
			checkbookMaintenance= new CheckbookMaintenance();
			//Step 1 
			checkbookMaintenance.setCheckBookId(dtoCheckBookMaintenance.getCheckBookId());
			checkbookMaintenance.setCheckbookDescription(dtoCheckBookMaintenance.getCheckbookDescription());
			checkbookMaintenance.setInactive(dtoCheckBookMaintenance.getInactive());
			checkbookMaintenance.setCheckbookDescriptionArabic(dtoCheckBookMaintenance.getCheckbookDescriptionArabic());
			checkbookMaintenance.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoCheckBookMaintenance.getGlAccountId(),false));
			checkbookMaintenance.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCheckBookMaintenance.getCurrencyId(),false));
			//Step 2
			checkbookMaintenance.setNextCheckNumber(dtoCheckBookMaintenance.getNextCheckNumber());
			checkbookMaintenance.setNextDepositNumber(dtoCheckBookMaintenance.getNextDepositNumber());
			if(UtilRandomKey.isNotBlank(dtoCheckBookMaintenance.getLastReconciledBalance())){
				checkbookMaintenance.setLastReconciledBalance(Double.valueOf(dtoCheckBookMaintenance.getLastReconciledBalance()));
			}
			if(UtilRandomKey.isNotBlank(dtoCheckBookMaintenance.getLastReconciledDate())){
				checkbookMaintenance.setLastReconciledDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoCheckBookMaintenance.getLastReconciledDate()));
			}
			
			//Step 3
			checkbookMaintenance.setOfficialBankAccountNumber(dtoCheckBookMaintenance.getOfficialBankAccountNumber());
			checkbookMaintenance.setBankSetup(repositoryBankSetup.findByBankIdAndIsDeleted(dtoCheckBookMaintenance.getBankId(),false));
			checkbookMaintenance.setUserDefine1(dtoCheckBookMaintenance.getUserDefine1());
			checkbookMaintenance.setUserDefine2(dtoCheckBookMaintenance.getUserDefine2());
			// Step4
			if(UtilRandomKey.isNotBlank(dtoCheckBookMaintenance.getCurrentCheckbookBalance())){
				checkbookMaintenance.setCurrentCheckbookBalance(Double.valueOf(dtoCheckBookMaintenance.getCurrentCheckbookBalance()));
			}
			if(UtilRandomKey.isNotBlank(dtoCheckBookMaintenance.getCashAccountBalance())){
				checkbookMaintenance.setCashAccountBalance(Double.valueOf(dtoCheckBookMaintenance.getCashAccountBalance()));
			}
			//Payable Options
			checkbookMaintenance.setExceedMaxCheckAmount(dtoCheckBookMaintenance.getExceedMaxCheckAmount());
			checkbookMaintenance.setPasswordOfMaxCheckAmount(dtoCheckBookMaintenance.getPasswordOfMaxCheckAmount());
			checkbookMaintenance.setDuplicateCheckNumber(dtoCheckBookMaintenance.getDuplicateCheckNumber());
			checkbookMaintenance.setOverrideCheckNumber(dtoCheckBookMaintenance.getOverrideCheckNumber());
			checkbookMaintenance.setCreatedBy(loggedInUserId);
			checkbookMaintenance=repositoryCheckBookMaintenance.save(checkbookMaintenance);
			checkbookMaintenance=repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoCheckBookMaintenance.getCheckBookId(),false);
			
		} catch (Exception e) {
			dtoCheckBookMaintenance.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
			
		}
		if(checkbookMaintenance!=null && dtoCheckBookMaintenance.getMessageType()==null){
			dtoCheckBookMaintenance= new DtoCheckBookMaintenance(checkbookMaintenance);
		}
		return dtoCheckBookMaintenance;
	}
	
	/**
	 * @param dtoCheckBookMaintenance
	 * @param checkbookMaintenance
	 * @return
	 */
	public DtoCheckBookMaintenance getCheckBookMaintenanceByCheckBookId(DtoCheckBookMaintenance dtoCheckBookMaintenance,CheckbookMaintenance checkbookMaintenance) 
	{
		try 
		{
			dtoCheckBookMaintenance= new DtoCheckBookMaintenance(checkbookMaintenance);
			if(checkbookMaintenance.getGlAccountsTableAccumulation()!=null)
			{
				dtoCheckBookMaintenance.setAccountNumber(getAccountNumberByAccountTableRowIndex(Integer.parseInt(checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex())));
			}
			else
			{
				dtoCheckBookMaintenance.setAccountNumber("");
			}
		} 
		catch (Exception e) 
		{
			dtoCheckBookMaintenance.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoCheckBookMaintenance;
	}
	
	/**
	 * @param dtoCheckBookMaintenance
	 * @param checkbookMaintenance
	 * @return
	 */
	public DtoCheckBookMaintenance updateCheckBookMaintenance(DtoCheckBookMaintenance dtoCheckBookMaintenance,CheckbookMaintenance checkbookMaintenance) 
	{
		int loggedInUserId=0;
		if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
			loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		try {
			//Step 1
			checkbookMaintenance.setCheckbookDescription(dtoCheckBookMaintenance.getCheckbookDescription());
			checkbookMaintenance.setInactive(dtoCheckBookMaintenance.getInactive());
			checkbookMaintenance.setCheckbookDescriptionArabic(dtoCheckBookMaintenance.getCheckbookDescriptionArabic());
			checkbookMaintenance.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoCheckBookMaintenance.getGlAccountId(),false));
			checkbookMaintenance.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCheckBookMaintenance.getCurrencyId(),false));
			//Step 2
			checkbookMaintenance.setNextCheckNumber(dtoCheckBookMaintenance.getNextCheckNumber());
			checkbookMaintenance.setNextDepositNumber(dtoCheckBookMaintenance.getNextDepositNumber());
			if(UtilRandomKey.isNotBlank(dtoCheckBookMaintenance.getLastReconciledBalance())){
				checkbookMaintenance.setLastReconciledBalance(Double.valueOf(dtoCheckBookMaintenance.getLastReconciledBalance()));
			}
			if(UtilRandomKey.isNotBlank(dtoCheckBookMaintenance.getLastReconciledDate())){
				checkbookMaintenance.setLastReconciledDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoCheckBookMaintenance.getLastReconciledDate()));
			}
			//Step 3
			checkbookMaintenance.setOfficialBankAccountNumber(dtoCheckBookMaintenance.getOfficialBankAccountNumber());
			checkbookMaintenance.setBankSetup(repositoryBankSetup.findByBankIdAndIsDeleted(dtoCheckBookMaintenance.getBankId(),false));
			checkbookMaintenance.setUserDefine1(dtoCheckBookMaintenance.getUserDefine1());
			checkbookMaintenance.setUserDefine2(dtoCheckBookMaintenance.getUserDefine2());
			// Step4
			if(UtilRandomKey.isNotBlank(dtoCheckBookMaintenance.getCurrentCheckbookBalance())){
				checkbookMaintenance.setCurrentCheckbookBalance(Double.valueOf(dtoCheckBookMaintenance.getCurrentCheckbookBalance()));
			}
			if(UtilRandomKey.isNotBlank(dtoCheckBookMaintenance.getCashAccountBalance())){
				checkbookMaintenance.setCashAccountBalance(Double.valueOf(dtoCheckBookMaintenance.getCashAccountBalance()));
			}
			//Payable Options
			checkbookMaintenance.setExceedMaxCheckAmount(dtoCheckBookMaintenance.getExceedMaxCheckAmount());
			checkbookMaintenance.setPasswordOfMaxCheckAmount(dtoCheckBookMaintenance.getPasswordOfMaxCheckAmount());
			checkbookMaintenance.setDuplicateCheckNumber(dtoCheckBookMaintenance.getDuplicateCheckNumber());
			checkbookMaintenance.setOverrideCheckNumber(dtoCheckBookMaintenance.getOverrideCheckNumber());
			checkbookMaintenance.setCreatedBy(loggedInUserId);
			checkbookMaintenance=repositoryCheckBookMaintenance.save(checkbookMaintenance);
			checkbookMaintenance=repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoCheckBookMaintenance.getCheckBookId(),false);
			
		} catch (Exception e) {
			dtoCheckBookMaintenance.setMessageType("INTERNAL_SERVER_ERROR");
			LOG.info(Arrays.toString(e.getStackTrace()));
			
		}
		if(checkbookMaintenance!=null && dtoCheckBookMaintenance.getMessageType()==null){
			dtoCheckBookMaintenance= new DtoCheckBookMaintenance(checkbookMaintenance);
		}
		return dtoCheckBookMaintenance;
	}
	
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchCheckBookMaintenance(DtoSearch dtoSearchs) 
	{
		
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryCheckBookMaintenance.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		DtoCheckBookMaintenance dtoCheckBookMaintenance = null;
		List<CheckbookMaintenance> checkbookMaintenances = null;
		List<DtoCheckBookMaintenance> dtoCheckBookMaintenancesList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			checkbookMaintenances = repositoryCheckBookMaintenance
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			checkbookMaintenances = repositoryCheckBookMaintenance
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (checkbookMaintenances != null) {
			for (CheckbookMaintenance checkbookMaintenance : checkbookMaintenances) {
				dtoCheckBookMaintenance = new DtoCheckBookMaintenance(checkbookMaintenance);
				if(checkbookMaintenance.getGlAccountsTableAccumulation()!=null)
				{
					dtoCheckBookMaintenance.setAccountNumber(getAccountNumberByAccountTableRowIndex(Integer.parseInt(checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex())));
				}
				else
				{
					dtoCheckBookMaintenance.setAccountNumber("");
				}
				dtoCheckBookMaintenancesList.add(dtoCheckBookMaintenance);
			}
		}
		dtoSearch.setRecords(dtoCheckBookMaintenancesList);
		return dtoSearch;
	}

	/**
	 * @return
	 */
	public DtoGeneralLedgerSetup getGeneralLedgerSetup() {
		GLConfigurationSetup glConfigurationSetup = null;
		int langId=serviceHome.getLanngugaeId();
		try {
			glConfigurationSetup = repositoryGLConfigurationSetup.findAll().get(0);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		MasterBlncdspl masterBlncdspl = glConfigurationSetup!=null?glConfigurationSetup.getMasterBlncdspl():null;
		if(masterBlncdspl!=null){
			masterBlncdspl = repositoryMasterBlncdspl.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterBlncdspl.getTypeId(),langId,false);
		}
		return new DtoGeneralLedgerSetup(glConfigurationSetup, masterBlncdspl);
	}
	
	/**
	 * @return
	 */
	public List<DtoMainAccountSetUp>  getMainAccountList(){
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
	    List<COAMainAccounts> mainAccountsList =	repositoryCOAMainAccounts.findByActiveAndAllowAccountTransactionEntryAndIsDeleted(true, true,false);
	    List<DtoMainAccountSetUp> list = new ArrayList<>();
	    if(mainAccountsList!=null && !mainAccountsList.isEmpty()){
	    	for (COAMainAccounts coaMainAccounts : mainAccountsList) {
				DtoMainAccountSetUp dtoMainAccountSetUp= new DtoMainAccountSetUp();
				dtoMainAccountSetUp.setMainAccountNumber(coaMainAccounts.getMainAccountNumber());
				if(ConfigSetting.PRIMARY.getValue().equalsIgnoreCase(langId)){
					dtoMainAccountSetUp.setMainAccountDescription(coaMainAccounts.getMainAccountDescription());
				}
				else if(ConfigSetting.SECONDARY.getValue().equalsIgnoreCase(langId)){
					dtoMainAccountSetUp.setMainAccountDescriptionArabic(coaMainAccounts.getMainAccountDescriptionArabic());
				}
				dtoMainAccountSetUp.setActIndx(coaMainAccounts.getActIndx());
				list.add(dtoMainAccountSetUp);
			}
	    }
	    return list;
	}
	
      /**
     * @param dtoFinancialDimensionValue
     * @return
     */
    public List<DtoFinancialDimensionValue>  getFinancialDimensionsValuesByFinancialDimensionId(DtoFinancialDimensionValue dtoFinancialDimensionValue)
      {
    	  List<COAFinancialDimensionsValues> coaFinancialDimensionsValuesList =  repositoryCOAFinancialDimensionsValues.findByCoaFinancialDimensionsDimInxdAndIsDeleted(dtoFinancialDimensionValue.getDimInxd(),false) ;
	      List<DtoFinancialDimensionValue> list = new ArrayList<>();
	      if(coaFinancialDimensionsValuesList!=null && !coaFinancialDimensionsValuesList.isEmpty()){
	    	for (COAFinancialDimensionsValues coaFinancialDimensionsValues : coaFinancialDimensionsValuesList) {
	    		DtoFinancialDimensionValue dtoFinancialDimensionValue2= new DtoFinancialDimensionValue();
				dtoFinancialDimensionValue2.setDimensionDescription("");
				if(UtilRandomKey.isNotBlank(coaFinancialDimensionsValues.getDimensionDescription())){
					dtoFinancialDimensionValue2.setDimensionDescription(coaFinancialDimensionsValues.getDimensionDescription());
				}
				dtoFinancialDimensionValue2.setDimensionDescriptionArabic("");
				if(UtilRandomKey.isNotBlank(coaFinancialDimensionsValues.getDimensionDescriptionArabic())){
					dtoFinancialDimensionValue2.setDimensionDescriptionArabic(coaFinancialDimensionsValues.getDimensionDescriptionArabic());
				}
				dtoFinancialDimensionValue2.setDimInxd(coaFinancialDimensionsValues.getCoaFinancialDimensions().getDimInxd());
				dtoFinancialDimensionValue2.setDimInxValue(coaFinancialDimensionsValues.getDimInxValue());
				dtoFinancialDimensionValue2.setDimensionValue("");
				if(UtilRandomKey.isNotBlank(coaFinancialDimensionsValues.getDimensionValue())){
					dtoFinancialDimensionValue2.setDimensionValue(coaFinancialDimensionsValues.getDimensionValue());
				}
				list.add(dtoFinancialDimensionValue2);
			}
	     }
	     return list;
	 }
      
      /**
     * @return
     */
    public List<DtoFinancialDimensionSetUp>  getFinancialDimensionsList()
      {
    	  List<COAFinancialDimensions> coaFinancialDimensionsList =  repositoryCOAFinancialDimensions.findAll();
	      List<DtoFinancialDimensionSetUp> list = new ArrayList<>();
	      if(coaFinancialDimensionsList!=null && !coaFinancialDimensionsList.isEmpty()){
	    	for (COAFinancialDimensions coaFinancialDimensions : coaFinancialDimensionsList) {
	    		DtoFinancialDimensionSetUp dtoFinancialDimension2= new DtoFinancialDimensionSetUp();
	    		
	    		dtoFinancialDimension2.setDimensionName("");
	    		if(UtilRandomKey.isNotBlank(coaFinancialDimensions.getDimensioncolumnname())){
	    			dtoFinancialDimension2.setDimensionName(coaFinancialDimensions.getDimensioncolumnname());
	    		}
	    		dtoFinancialDimension2.setDimensionMask("");
	    		if(UtilRandomKey.isNotBlank(coaFinancialDimensions.getDimensionMask())){
	    			dtoFinancialDimension2.setDimensionMask(coaFinancialDimensions.getDimensionMask());
	    		}
				dtoFinancialDimension2.setDimensionDescription("");
				if(UtilRandomKey.isNotBlank(coaFinancialDimensions.getDimensionDescription())){
						dtoFinancialDimension2.setDimensionDescription(coaFinancialDimensions.getDimensionDescription());
				}
				
				dtoFinancialDimension2.setDimensionDescriptionArabic("");
				if(UtilRandomKey.isNotBlank(coaFinancialDimensions.getDimensionDescriptionArabic())){
						dtoFinancialDimension2.setDimensionDescriptionArabic(coaFinancialDimensions.getDimensionDescriptionArabic());
				}
				dtoFinancialDimension2.setDimInxd(coaFinancialDimensions.getDimInxd());
				list.add(dtoFinancialDimension2);
			}
	     }
	     return list;
	  }
      
      /**
     * @param dtoObj
     * @return
     */
    public DtoGLConfigurationRelationBetweenDimensionsAndMainAccount saveGlDimAndMain(DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dtoObj)
    {
    	  repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount.deleteAll();
    	  try
    	  {
    		  
    		  // Save Main Account
    		  GLConfigurationRelationBetweenDimensionsAndMainAccount mainAccountObject= new GLConfigurationRelationBetweenDimensionsAndMainAccount();
    		  mainAccountObject.setIsMainAccount(1); 
    		  if(dtoObj.getCoaMainAccountsFromActIndexId()!=null){
    			  mainAccountObject.setCoaMainAccountsFrom(repositoryCOAMainAccounts.findByActIndxAndIsDeleted(dtoObj.getCoaMainAccountsFromActIndexId(),false));
    		  }
    		  if(dtoObj.getCoaMainAccountsToActIndexId()!=null){
    			  mainAccountObject.setCoaMainAccountsTo(repositoryCOAMainAccounts.findByActIndxAndIsDeleted(dtoObj.getCoaMainAccountsToActIndexId(),false));
    		  }
    		  mainAccountObject.setSegmentNumber(dtoObj.getSegmentNumber());
    		  repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount.save(mainAccountObject);
    		  
    		  //Save dimension values
        	  if(dtoObj.getDimensionList()!=null && !dtoObj.getDimensionList().isEmpty())
        	  {
        		  for (DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dtoListObj : dtoObj.getDimensionList()) 
        		  {
	        			  GLConfigurationRelationBetweenDimensionsAndMainAccount entityObj= new GLConfigurationRelationBetweenDimensionsAndMainAccount();
		            	  entityObj.setSegmentNumber(dtoObj.getSegmentNumber());
	            		  entityObj.setIsMainAccount(0);
	            		  if(dtoListObj.getCoaFinancialDimensionsIndexId()!=null){
//		            		  entityObj.setCoaFinancialDimensions(repositoryCOAFinancialDimensions.findByDimInxdAndIsDeleted(dtoListObj.getCoaFinancialDimensionsIndexId(),false));
		            	  }
	        			  if(dtoListObj.getCoaFinancialDimensionsFromIndexValue()!=null){
	        			    	 entityObj.setCoaFinancialDimensionsValuesFrom(repositoryCOAFinancialDimensionsValues.findByDimInxValueAndIsDeleted(dtoListObj.getCoaFinancialDimensionsFromIndexValue(),false));
	        			  } 
	        			  if(dtoListObj.getCoaFinancialDimensionsToIndexValue()!=null){
	        				  entityObj.setCoaFinancialDimensionsValuesTo(repositoryCOAFinancialDimensionsValues.findByDimInxValueAndIsDeleted(dtoListObj.getCoaFinancialDimensionsToIndexValue(),false));
	        			  }
        	          repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount.save(entityObj);
			      }
        	  }
    	  }
    	  catch(Exception e){
    		  dtoObj.setMessageType("INTERNAL_SERVER_ERROR");
    		  LOG.info(Arrays.toString(e.getStackTrace()));
    	  }
    	 
    	 if( dtoObj.getMessageType()==null){
    		  dtoObj= getGlDimAndMainList();
    	  }
    	  return dtoObj;
      }
      
      /**
     * @return
     */
    public DtoGLConfigurationRelationBetweenDimensionsAndMainAccount getGlDimAndMainList()
      {
    	  DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dto = null;
    	  List<DtoGLConfigurationRelationBetweenDimensionsAndMainAccount> dimensionList = new ArrayList<>();
    	  List<GLConfigurationRelationBetweenDimensionsAndMainAccount> list =repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount.findAll();
    	  if(list!=null && !list.isEmpty())
    	  {
    		 dto=new DtoGLConfigurationRelationBetweenDimensionsAndMainAccount();
    		 for (GLConfigurationRelationBetweenDimensionsAndMainAccount obj : list) 
    		 {
    			 dto.setSegmentNumber(obj.getSegmentNumber());
    			 if(obj.getIsMainAccount()==1)
    			 {
    				 dto.setIsMainAccount(obj.getIsMainAccount());
        			 dto.setCoaMainAccountsFromActIndexId(0L);
        			 if(obj.getCoaMainAccountsFrom()!=null){
        				 dto.setCoaMainAccountsFromActIndexId(obj.getCoaMainAccountsFrom().getActIndx());
        			 }
        			 dto.setCoaMainAccountsToActIndexId(0L);
        			 if(obj.getCoaMainAccountsTo()!=null){
        				 dto.setCoaMainAccountsToActIndexId(obj.getCoaMainAccountsTo().getActIndx());
        			 }
    			 }
    			 else
    			 {
    				 DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dimensionListObj= new DtoGLConfigurationRelationBetweenDimensionsAndMainAccount();
        			 dimensionListObj.setId(obj.getId());
        			 dimensionListObj.setCoaFinancialDimensionsIndexId(0);
//        			 if(obj.getCoaFinancialDimensions()!=null){
//        				 dimensionListObj.setCoaFinancialDimensionsIndexId(obj.getCoaFinancialDimensions().getDimInxd());
//        			 }
        			 dimensionListObj.setCoaFinancialDimensionsFromIndexValue(0L);
        			 if(obj.getCoaFinancialDimensionsValuesFrom()!=null){
        				 dimensionListObj.setCoaFinancialDimensionsFromIndexValue(obj.getCoaFinancialDimensionsValuesFrom().getDimInxValue());
        			 }
        			 dimensionListObj.setCoaFinancialDimensionsToIndexValue(0L);
        			 if(obj.getCoaFinancialDimensionsValuesTo()!=null){
        				 dimensionListObj.setCoaFinancialDimensionsToIndexValue(obj.getCoaFinancialDimensionsValuesTo().getDimInxValue());
        			 }
        			 dimensionList.add(dimensionListObj);
    			 }
			 }
    		 dto.setDimensionList(dimensionList);
    	  }
    	  return dto;
      }

	/**
	 * @return
	 */
	public List<DtoStatusType> getSeries() {
		int langId=serviceHome.getLanngugaeId();
		List<SeriesType> seriesTypeList = repositorySeriesType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> list = new ArrayList<>();
		if(seriesTypeList!=null){
			for(SeriesType seriesType: seriesTypeList){
				list.add(new DtoStatusType(seriesType));
			}
			return list;
		}
		return list;
	}

	/**
	 * @return
	 */
	public List<Integer> getYears() {
		try{
			return repositoryGLConfigurationDefinePeriodsHeader.findByAllYears();
			
		}catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));	
		}
		
		return new ArrayList<>();
	}

	/**
	 * @param dtoFiscalFinancialPeriodSetup
	 * @return
	 */
	public List<DtoPeriods> getMaxPeriod(DtoFiscalFinancialPeriodSetup dtoFiscalFinancialPeriodSetup) {
		List<DtoPeriods> list = new ArrayList<>();
		try 
		{
			if(dtoFiscalFinancialPeriodSetup.getYear()!=null && dtoFiscalFinancialPeriodSetup.getYear()>0)
			{
				GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader
						.findByYearAndIsDeleted(dtoFiscalFinancialPeriodSetup.getYear(),false);
				if (glConfigurationDefinePeriodsHeader != null) {

					for (GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetail : glConfigurationDefinePeriodsHeader
							.getGlConfigurationDefinePeriodsDetails()) {
						DtoPeriods dtoPeriod = new DtoPeriods();
						dtoPeriod.setPeriodName(glConfigurationDefinePeriodsDetail.getPeriodName());
						dtoPeriod.setPeriodNumber(glConfigurationDefinePeriodsDetail.getPeriodId());
						list.add(dtoPeriod);
					}
				}
			}
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return list;
	}

	/**
	 * @param dtoMassCloseFiscalPeriodSetupRequest
	 * @return
	 */
	public List<DtoMassCloseFiscalPeriodSetup> getMassCloseFiscalPeriodSetup(
		DtoMassCloseFiscalPeriodSetupRequest dtoMassCloseFiscalPeriodSetupRequest) {
		int langId = serviceHome.getLanngugaeId();
		List<DtoMassCloseFiscalPeriodSetup> response = new ArrayList<>();
		int langid = serviceHome.getLanngugaeId();
		try {
			/*GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader
					.findByYearAndIsDeleted(dtoMassCloseFiscalPeriodSetupRequest.getYear(),false);*/
			List<Integer> originTypelist = new ArrayList<>();
			SeriesType seriesType = repositorySeriesType
					.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoMassCloseFiscalPeriodSetupRequest.getSeriesId(),langid,false);
			if (dtoMassCloseFiscalPeriodSetupRequest.getAllRecord()) 
			{
					List<ModulesConfigurationPeriod> modulesConfigurationPeriodAllOrigin = null;
					List<ModulesConfigurationPeriod> modulesConfigurationPeriodSpecificOriginList=null;
					if(dtoMassCloseFiscalPeriodSetupRequest.getAllOrigin())
					{
						 modulesConfigurationPeriodAllOrigin = repositoryModulesConfigurationPeriod
								.findByYearAndSeriesTypeTypeIdAndIsDeleted(
										dtoMassCloseFiscalPeriodSetupRequest.getYear(),
										dtoMassCloseFiscalPeriodSetupRequest.getSeriesId(),false);

                         if(modulesConfigurationPeriodAllOrigin!=null && !modulesConfigurationPeriodAllOrigin.isEmpty())
                         {
                        	 for (ModulesConfigurationPeriod modulesConfigurationPeriod : modulesConfigurationPeriodAllOrigin) 
                        	 {
                        		DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
  								dtoMassCloseFiscalPeriodSetup.setIsClose(modulesConfigurationPeriod.getClosePeriod()!=null?modulesConfigurationPeriod.getClosePeriod():false);
  								MasterMassCloseOrigin masterMassCloseOrigin=repositoryMassCloseOrigin.findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(modulesConfigurationPeriod.getOriginId(),langid,dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
  								dtoMassCloseFiscalPeriodSetup.setOrigin("");
  								dtoMassCloseFiscalPeriodSetup.setOriginId(0);
  								dtoMassCloseFiscalPeriodSetup.setIsClose(false);
  								if(masterMassCloseOrigin!=null){
  									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
  									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
  									originTypelist.add(masterMassCloseOrigin.getTypeId());
  								}
  								dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
								dtoMassCloseFiscalPeriodSetup.setSeries(0);
								dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriod.getModuleDescription());
								if(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails()!=null)
								{
									dtoMassCloseFiscalPeriodSetup.setPeriodId(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getPeriodId());
   									dtoMassCloseFiscalPeriodSetup.setSeries(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getSeries());
   									dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getPeriodName());
								}
  								
  								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
  								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType != null ?seriesType.getTypeId():0);
  								response.add(dtoMassCloseFiscalPeriodSetup);
							 }
                         }
                         
                         
                        	 List<MasterMassCloseOrigin> massCloseOriginList=repositoryMassCloseOrigin.
                        			 findBySeriesTypeTypeIdAndLanguageLanguageId(seriesType.getTypeId(), langid);
                        	 GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.
                     				findByYearAndIsDeleted(dtoMassCloseFiscalPeriodSetupRequest.getYear(), false);
                        	 if(glConfigurationDefinePeriodsHeader!=null && massCloseOriginList!=null){
                        			 int numberOfPeriod=glConfigurationDefinePeriodsHeader.getNumberOfPeriods();
                                	 for (MasterMassCloseOrigin masterMassCloseOrigin : massCloseOriginList) 
                                	 {
                                		 if(!originTypelist.contains(masterMassCloseOrigin.getTypeId()))
                                		 {
                                			 for(int i=1;i<=numberOfPeriod;i++){
                                     			DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
                   								dtoMassCloseFiscalPeriodSetup.setIsClose(false);
                   							
                   									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
                   									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
                   									
                   									 GLConfigurationDefinePeriodsDetails configurationDefinePeriodsDetail =repositoryGLConfigurationDefinePeriodsDetails.
                   											findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodId(glConfigurationDefinePeriodsHeader.getYear(),false,i);
                   									dtoMassCloseFiscalPeriodSetup.setPeriodName("");
                   									dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
                   									dtoMassCloseFiscalPeriodSetup.setSeries(0);
                   							   if(configurationDefinePeriodsDetail!=null){
                   									dtoMassCloseFiscalPeriodSetup.setPeriodName(configurationDefinePeriodsDetail.getPeriodName());
                   									dtoMassCloseFiscalPeriodSetup.setPeriodId(configurationDefinePeriodsDetail.getPeriodId());
                   									dtoMassCloseFiscalPeriodSetup.setSeries(configurationDefinePeriodsDetail.getSeries());
                   								}
                   								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
                   								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType != null ?seriesType.getTypeId():0);
                   								response.add(dtoMassCloseFiscalPeriodSetup);
                                     			
                                     		}
                                		 }
    								 }
                        	 }
                             
                         
					 }
					 else
					 {
						 modulesConfigurationPeriodSpecificOriginList = repositoryModulesConfigurationPeriod
								.findByYearAndSeriesTypeTypeIdAndOriginIdAndIsDeleted(
										dtoMassCloseFiscalPeriodSetupRequest.getYear(),
										dtoMassCloseFiscalPeriodSetupRequest.getSeriesId(),
										dtoMassCloseFiscalPeriodSetupRequest.getOriginId(),false);
						if (modulesConfigurationPeriodSpecificOriginList != null && !modulesConfigurationPeriodSpecificOriginList.isEmpty()) 
						{
							for (ModulesConfigurationPeriod modulesConfigurationPeriodSpecificOrigin : modulesConfigurationPeriodSpecificOriginList) {
								DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
								dtoMassCloseFiscalPeriodSetup.setIsClose(modulesConfigurationPeriodSpecificOrigin.getClosePeriod()!=null?modulesConfigurationPeriodSpecificOrigin.getClosePeriod():false);
								MasterMassCloseOrigin masterMassCloseOrigin=repositoryMassCloseOrigin.
										findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId
										(modulesConfigurationPeriodSpecificOrigin.getOriginId(),langid,
												dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
  								
								dtoMassCloseFiscalPeriodSetup.setOrigin("");
								dtoMassCloseFiscalPeriodSetup.setOriginId(0);
								if(masterMassCloseOrigin!=null){
									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
								}
								dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriodSpecificOrigin.getModuleDescription());
								dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
								dtoMassCloseFiscalPeriodSetup.setSeries(0);
								dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriodSpecificOrigin.getModuleDescription());
								if(modulesConfigurationPeriodSpecificOrigin.getGlConfigurationDefinePeriodsDetails()!=null)
								{
									dtoMassCloseFiscalPeriodSetup.setPeriodId(modulesConfigurationPeriodSpecificOrigin.getGlConfigurationDefinePeriodsDetails().getPeriodId());
   									dtoMassCloseFiscalPeriodSetup.setSeries(modulesConfigurationPeriodSpecificOrigin.getGlConfigurationDefinePeriodsDetails().getSeries());
   									dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriodSpecificOrigin.getGlConfigurationDefinePeriodsDetails().getPeriodName());
								}
								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType.getTypeId());
								
								response.add(dtoMassCloseFiscalPeriodSetup);
							}
						} 
					    else
	                     {
	                        	MasterMassCloseOrigin masterMassCloseOrigin=repositoryMassCloseOrigin.
	                        			findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(dtoMassCloseFiscalPeriodSetupRequest.getOriginId(),
	                        					langid,dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
  								
	                        	 GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.
	                     				findByYearAndIsDeleted(dtoMassCloseFiscalPeriodSetupRequest.getYear(), false);
	                        	 if(glConfigurationDefinePeriodsHeader!=null && masterMassCloseOrigin!=null){
	                        			 int numberOfPeriod=glConfigurationDefinePeriodsHeader.getNumberOfPeriods();
	    									
	                                		for(int i=1;i<=numberOfPeriod;i++){
	                                			
	                                			DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
	              								dtoMassCloseFiscalPeriodSetup.setIsClose(false);
	              							
	              									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
	              									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
	              									
	              									 GLConfigurationDefinePeriodsDetails configurationDefinePeriodsDetail =repositoryGLConfigurationDefinePeriodsDetails.
	              											findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodId(glConfigurationDefinePeriodsHeader.getYear(),false,i);
	              									dtoMassCloseFiscalPeriodSetup.setPeriodName("");
                   									dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
                   									dtoMassCloseFiscalPeriodSetup.setSeries(0);
	              									if(configurationDefinePeriodsDetail!=null){
	              									dtoMassCloseFiscalPeriodSetup.setPeriodName(configurationDefinePeriodsDetail.getPeriodName());
	              									dtoMassCloseFiscalPeriodSetup.setPeriodId(configurationDefinePeriodsDetail.getPeriodId());
                   									dtoMassCloseFiscalPeriodSetup.setSeries(configurationDefinePeriodsDetail.getSeries());
	              								}
	              								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
	              								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType != null ?seriesType.getTypeId():0);
	              								response.add(dtoMassCloseFiscalPeriodSetup);
	                                			
	                                		}
	                        	 }
	                             
	                        }
					 }
			} 
			else 
			{
				/*List<GLConfigurationDefinePeriodsDetails> list = repositoryGLConfigurationDefinePeriodsDetails
						.findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodIdBetween(glConfigurationDefinePeriodsHeader.getYear(),false,
								dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId(),
								dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId());
				for (GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetails : list) 
				{*/
				        List<ModulesConfigurationPeriod> modulesConfigurationPeriodList = null;
						if(dtoMassCloseFiscalPeriodSetupRequest.getAllOrigin() &&  
								(dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId()==null || 
								dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId()==0 || 
								   dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId()==null ||
								   dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId()==0))
						{
							modulesConfigurationPeriodList = repositoryModulesConfigurationPeriod
									.findByYearAndSeriesTypeTypeIdAndIsDeleted(
											dtoMassCloseFiscalPeriodSetupRequest.getYear(),
											dtoMassCloseFiscalPeriodSetupRequest.getSeriesId(),false);

	                          if(modulesConfigurationPeriodList!=null && !modulesConfigurationPeriodList.isEmpty()){
	                        	  for (ModulesConfigurationPeriod modulesConfigurationPeriod : modulesConfigurationPeriodList) 
	                        	  {
	                        		  DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
	  								  dtoMassCloseFiscalPeriodSetup.setIsClose(modulesConfigurationPeriod.getClosePeriod()!=null?modulesConfigurationPeriod.getClosePeriod():false);
		  							  MasterMassCloseOrigin masterMassCloseOrigin=repositoryMassCloseOrigin.
		                        			findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(modulesConfigurationPeriod.getOriginId(),
		                        					langid,dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
		  							  
	  								 dtoMassCloseFiscalPeriodSetup.setOrigin("");
	  								 dtoMassCloseFiscalPeriodSetup.setOriginId(0);
	  								 if(masterMassCloseOrigin!=null){
	  									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
	  									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
	  									originTypelist.add(masterMassCloseOrigin.getTypeId());
	  								 }
	  								dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
									dtoMassCloseFiscalPeriodSetup.setSeries(0);
									dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriod.getModuleDescription());
									if(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails()!=null)
									{
										dtoMassCloseFiscalPeriodSetup.setPeriodId(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getPeriodId());
       									dtoMassCloseFiscalPeriodSetup.setSeries(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getSeries());
       									dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getPeriodName());
									}
	  								 
	  								 dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
	  								 dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType.getTypeId());
	  								 response.add(dtoMassCloseFiscalPeriodSetup);
								  }
	                          }
	                          
	                         	 List<MasterMassCloseOrigin> massCloseOriginList=repositoryMassCloseOrigin.findBySeriesTypeTypeIdAndLanguageLanguageId(seriesType.getTypeId(), langid);
	                         	 GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.
	                      				findByYearAndIsDeleted(dtoMassCloseFiscalPeriodSetupRequest.getYear(), false);
	                         	 if(glConfigurationDefinePeriodsHeader!=null && massCloseOriginList!=null)
	                         	 {
	                         			 int numberOfPeriod=glConfigurationDefinePeriodsHeader.getNumberOfPeriods();
	                                 	 for (MasterMassCloseOrigin masterMassCloseOrigin : massCloseOriginList) 
	                                 	 {
	                                 		if(!originTypelist.contains(masterMassCloseOrigin.getTypeId()))
	                                 		{
	                                 			for(int i=1;i<=numberOfPeriod;i++){
		                                 			
		                                 			DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
		               								dtoMassCloseFiscalPeriodSetup.setIsClose(false);
		               							
		               									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
		               									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
		               									
		               									 GLConfigurationDefinePeriodsDetails configurationDefinePeriodsDetail =repositoryGLConfigurationDefinePeriodsDetails.
		               											findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodId(glConfigurationDefinePeriodsHeader.getYear(),false,i);
		               									dtoMassCloseFiscalPeriodSetup.setPeriodName("");
	                   									dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
	                   									dtoMassCloseFiscalPeriodSetup.setSeries(0);
		               								if(configurationDefinePeriodsDetail!=null){
		               									dtoMassCloseFiscalPeriodSetup.setPeriodName(configurationDefinePeriodsDetail.getPeriodName());
		               									dtoMassCloseFiscalPeriodSetup.setPeriodId(configurationDefinePeriodsDetail.getPeriodId());
	                   									dtoMassCloseFiscalPeriodSetup.setSeries(configurationDefinePeriodsDetail.getSeries());
		               								}
		               								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
		               								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType != null ?seriesType.getTypeId():0);
		               								response.add(dtoMassCloseFiscalPeriodSetup);
		                                 			
		                                 		}
	                                 		}
	     								 }
	                         	 }
	                          
						}
						else if(dtoMassCloseFiscalPeriodSetupRequest.getAllOrigin() &&  
								(dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId()!=null && 
								dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId()>0 &&
								   dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId()!=null &&
								   dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId()>0)
								)
						{
							for(int i=dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId();
									i<=dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId();i++)
							{
								GLConfigurationDefinePeriodsDetails configurationDefinePeriodsDetail =repositoryGLConfigurationDefinePeriodsDetails.
											findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodId(dtoMassCloseFiscalPeriodSetupRequest.getYear(),false,i);
								modulesConfigurationPeriodList = repositoryModulesConfigurationPeriod
										.findByYearAndSeriesTypeTypeIdAndModuleDescriptionAndIsDeleted(
												dtoMassCloseFiscalPeriodSetupRequest.getYear(),
												dtoMassCloseFiscalPeriodSetupRequest.getSeriesId(),configurationDefinePeriodsDetail.getPeriodName(),false);
		                          if(modulesConfigurationPeriodList!=null && !modulesConfigurationPeriodList.isEmpty()){
		                        	  for (ModulesConfigurationPeriod modulesConfigurationPeriod : modulesConfigurationPeriodList) {
		                        		  DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
		  								dtoMassCloseFiscalPeriodSetup.setIsClose(modulesConfigurationPeriod.getClosePeriod()!=null?modulesConfigurationPeriod.getClosePeriod():false);
		  								MasterMassCloseOrigin masterMassCloseOrigin = repositoryMassCloseOrigin.findOne(modulesConfigurationPeriod.getOriginId());
		  								if(masterMassCloseOrigin.getLanguage().getLanguageId()!=langId){
		  									masterMassCloseOrigin=repositoryMassCloseOrigin.findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(masterMassCloseOrigin.getTypeId(),langid,dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
		  								}
		  								dtoMassCloseFiscalPeriodSetup.setOrigin("");
		  								dtoMassCloseFiscalPeriodSetup.setOriginId(0);
		  								if(masterMassCloseOrigin!=null){
		  									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
		  									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
		  									originTypelist.add(masterMassCloseOrigin.getTypeId());
		  								}
		  								dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
										dtoMassCloseFiscalPeriodSetup.setSeries(0);
										dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriod.getModuleDescription());
										if(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails()!=null)
										{
											dtoMassCloseFiscalPeriodSetup.setPeriodId(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getPeriodId());
	       									dtoMassCloseFiscalPeriodSetup.setSeries(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getSeries());
	       									dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getPeriodName());
										}
		  								
		  								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
		  								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType.getTypeId());
		  								response.add(dtoMassCloseFiscalPeriodSetup);
									  }
		                          }
		                          
		                        		 List<MasterMassCloseOrigin> massCloseOriginList=repositoryMassCloseOrigin.
		                        				 findBySeriesTypeTypeIdAndLanguageLanguageId(seriesType.getTypeId(), langid);
			                         	 if(massCloseOriginList!=null)
			                         	 {
			                                 	 for (MasterMassCloseOrigin masterMassCloseOrigin : massCloseOriginList) 
			                                 	 {
			                                 			if(!originTypelist.contains(masterMassCloseOrigin.getTypeId())){
			                                 				DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
				               								dtoMassCloseFiscalPeriodSetup.setIsClose(false);
				               							
				               									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
				               									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
				               									
				               									dtoMassCloseFiscalPeriodSetup.setPeriodName("");
			                   									dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
			                   									dtoMassCloseFiscalPeriodSetup.setSeries(0);
				               								if(configurationDefinePeriodsDetail!=null){
				               									dtoMassCloseFiscalPeriodSetup.setPeriodName(configurationDefinePeriodsDetail.getPeriodName());
				               									dtoMassCloseFiscalPeriodSetup.setPeriodId(configurationDefinePeriodsDetail.getPeriodId());
			                   									dtoMassCloseFiscalPeriodSetup.setSeries(configurationDefinePeriodsDetail.getSeries());
				               								}
				               								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
				               								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType != null ?seriesType.getTypeId():0);
				               								response.add(dtoMassCloseFiscalPeriodSetup);
			                                 			}
			     						       }
			                           }
							}
							
						}
						else if(!dtoMassCloseFiscalPeriodSetupRequest.getAllOrigin() &&
								dtoMassCloseFiscalPeriodSetupRequest.getOriginId()!=null && 
								dtoMassCloseFiscalPeriodSetupRequest.getOriginId()>0 &&  
								(dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId()!=null && 
								dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId()>0 &&
								   dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId()!=null &&
								   dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId()>0)
								)
						{
							for(int i=dtoMassCloseFiscalPeriodSetupRequest.getStartPeriodId();
									i<=dtoMassCloseFiscalPeriodSetupRequest.getEndPeriodId();i++)
							{
								GLConfigurationDefinePeriodsDetails configurationDefinePeriodsDetail =repositoryGLConfigurationDefinePeriodsDetails.
										findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodId(dtoMassCloseFiscalPeriodSetupRequest.getYear(),false,i);
							
								modulesConfigurationPeriodList = repositoryModulesConfigurationPeriod
										.findByYearAndSeriesTypeTypeIdAndModuleDescriptionAndOriginIdAndIsDeleted(
												dtoMassCloseFiscalPeriodSetupRequest.getYear(),dtoMassCloseFiscalPeriodSetupRequest.getSeriesId(),
												configurationDefinePeriodsDetail.getPeriodName(),dtoMassCloseFiscalPeriodSetupRequest.getOriginId(),false);
		                          if(modulesConfigurationPeriodList!=null && !modulesConfigurationPeriodList.isEmpty()){
		                        	  for (ModulesConfigurationPeriod modulesConfigurationPeriod : modulesConfigurationPeriodList) {
		                        		  DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
		  								dtoMassCloseFiscalPeriodSetup.setIsClose(modulesConfigurationPeriod.getClosePeriod()!=null?modulesConfigurationPeriod.getClosePeriod():false);
		  								MasterMassCloseOrigin masterMassCloseOrigin = repositoryMassCloseOrigin.findOne(modulesConfigurationPeriod.getOriginId());
		  								if(masterMassCloseOrigin.getLanguage().getLanguageId()!=langId){
		  									masterMassCloseOrigin=repositoryMassCloseOrigin.findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(masterMassCloseOrigin.getTypeId(),langid,dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
		  								}
		  								dtoMassCloseFiscalPeriodSetup.setOrigin("");
		  								dtoMassCloseFiscalPeriodSetup.setOriginId(0);
		  								if(masterMassCloseOrigin!=null){
		  									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
		  									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
		  								}
		  								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
		  								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType.getTypeId());
		  								dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
										dtoMassCloseFiscalPeriodSetup.setSeries(0);
										dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriod.getModuleDescription());
										if(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails()!=null)
										{
											dtoMassCloseFiscalPeriodSetup.setPeriodId(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getPeriodId());
	       									dtoMassCloseFiscalPeriodSetup.setSeries(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getSeries());
	       									dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriod.getGlConfigurationDefinePeriodsDetails().getPeriodName());
										}
		  								
		  								response.add(dtoMassCloseFiscalPeriodSetup);
									  }
		                          }
		                          
		                          else
		                          {
		                        	  MasterMassCloseOrigin massCloseOrigin=repositoryMassCloseOrigin.findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(dtoMassCloseFiscalPeriodSetupRequest.getOriginId(),langid,dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
			                          if(massCloseOrigin!=null)
			                          {
			                                 			DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
			               								dtoMassCloseFiscalPeriodSetup.setIsClose(false);
			               							
			               									dtoMassCloseFiscalPeriodSetup.setOrigin(massCloseOrigin.getOrigin());
			               									dtoMassCloseFiscalPeriodSetup.setOriginId(massCloseOrigin.getId());
			               									dtoMassCloseFiscalPeriodSetup.setPeriodName("");
		                   									dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
		                   									dtoMassCloseFiscalPeriodSetup.setSeries(0);
			               								if(configurationDefinePeriodsDetail!=null){
			               									dtoMassCloseFiscalPeriodSetup.setPeriodName(configurationDefinePeriodsDetail.getPeriodName());
			               									dtoMassCloseFiscalPeriodSetup.setPeriodId(configurationDefinePeriodsDetail.getPeriodId());
		                   									dtoMassCloseFiscalPeriodSetup.setSeries(configurationDefinePeriodsDetail.getSeries());
			               								}
			               								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
			               								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType != null ?seriesType.getTypeId():0);
			               								response.add(dtoMassCloseFiscalPeriodSetup);
			                           }
		                          }
							}
						}
						
						else if (!dtoMassCloseFiscalPeriodSetupRequest.getAllOrigin() &&
								dtoMassCloseFiscalPeriodSetupRequest.getOriginId()!=null && 
								dtoMassCloseFiscalPeriodSetupRequest.getOriginId()>0
							 )
						 {
							List<ModulesConfigurationPeriod> modulesConfigurationPeriodSpecificOriginList = repositoryModulesConfigurationPeriod
									.findByYearAndSeriesTypeTypeIdAndOriginIdAndIsDeleted(
											dtoMassCloseFiscalPeriodSetupRequest.getYear(),
											dtoMassCloseFiscalPeriodSetupRequest.getSeriesId(),
											dtoMassCloseFiscalPeriodSetupRequest.getOriginId(),false);
							if (modulesConfigurationPeriodSpecificOriginList != null && !modulesConfigurationPeriodSpecificOriginList.isEmpty()) 
							{
								for (ModulesConfigurationPeriod modulesConfigurationPeriodSpecificOrigin : modulesConfigurationPeriodSpecificOriginList) 
								{
									  DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
										dtoMassCloseFiscalPeriodSetup.setIsClose(modulesConfigurationPeriodSpecificOrigin.getClosePeriod()!=null?modulesConfigurationPeriodSpecificOrigin.getClosePeriod():false);
										MasterMassCloseOrigin masterMassCloseOrigin = repositoryMassCloseOrigin.findOne(modulesConfigurationPeriodSpecificOrigin.getOriginId());
										if(masterMassCloseOrigin.getLanguage().getLanguageId()!=langId){
											masterMassCloseOrigin=repositoryMassCloseOrigin.findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(masterMassCloseOrigin.getTypeId(),langid,dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
			  							 }
										dtoMassCloseFiscalPeriodSetup.setOrigin("");
										dtoMassCloseFiscalPeriodSetup.setOriginId(0);
										if(masterMassCloseOrigin!=null){
											dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
											dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
										}
										
										dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
										dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType.getTypeId());
										dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
										dtoMassCloseFiscalPeriodSetup.setSeries(0);
										dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriodSpecificOrigin.getModuleDescription());
										if(modulesConfigurationPeriodSpecificOrigin.getGlConfigurationDefinePeriodsDetails()!=null)
										{
											dtoMassCloseFiscalPeriodSetup.setPeriodId(modulesConfigurationPeriodSpecificOrigin.getGlConfigurationDefinePeriodsDetails().getPeriodId());
	       									dtoMassCloseFiscalPeriodSetup.setSeries(modulesConfigurationPeriodSpecificOrigin.getGlConfigurationDefinePeriodsDetails().getSeries());
	       									dtoMassCloseFiscalPeriodSetup.setPeriodName(modulesConfigurationPeriodSpecificOrigin.getGlConfigurationDefinePeriodsDetails().getPeriodName());
										}
										response.add(dtoMassCloseFiscalPeriodSetup);
								}
							} 
						    else
		                     {
		                        	MasterMassCloseOrigin masterMassCloseOrigin=repositoryMassCloseOrigin.
		                        			findByTypeIdAndLanguageLanguageIdAndSeriesTypeTypeId(
		                        					dtoMassCloseFiscalPeriodSetupRequest.getOriginId(),langid,
		                        					dtoMassCloseFiscalPeriodSetupRequest.getSeriesId());
		                        	 GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader = repositoryGLConfigurationDefinePeriodsHeader.
		                     				findByYearAndIsDeleted(dtoMassCloseFiscalPeriodSetupRequest.getYear(), false);
		                        	 if(glConfigurationDefinePeriodsHeader!=null && masterMassCloseOrigin!=null){
		                        			 int numberOfPeriod=glConfigurationDefinePeriodsHeader.getNumberOfPeriods();
		                                		for(int i=1;i<=numberOfPeriod;i++){
		                                			
		                                			DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup = new DtoMassCloseFiscalPeriodSetup();
		              								dtoMassCloseFiscalPeriodSetup.setIsClose(false);
		              							
		              									dtoMassCloseFiscalPeriodSetup.setOrigin(masterMassCloseOrigin.getOrigin());
		              									dtoMassCloseFiscalPeriodSetup.setOriginId(masterMassCloseOrigin.getTypeId());
		              									
		              									 GLConfigurationDefinePeriodsDetails configurationDefinePeriodsDetail =repositoryGLConfigurationDefinePeriodsDetails.
		              											findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodId(glConfigurationDefinePeriodsHeader.getYear(),false,i);
		              									dtoMassCloseFiscalPeriodSetup.setPeriodName("");
	                   									dtoMassCloseFiscalPeriodSetup.setPeriodId(0);
	                   									dtoMassCloseFiscalPeriodSetup.setSeries(0);
		              								if(configurationDefinePeriodsDetail!=null){
		              									dtoMassCloseFiscalPeriodSetup.setPeriodName(configurationDefinePeriodsDetail.getPeriodName());
		              									dtoMassCloseFiscalPeriodSetup.setPeriodId(configurationDefinePeriodsDetail.getPeriodId());
	                   									dtoMassCloseFiscalPeriodSetup.setSeries(configurationDefinePeriodsDetail.getSeries());
		              								}
		              								dtoMassCloseFiscalPeriodSetup.setSeriesName(seriesType != null ? seriesType.getTypePrimary() : "");
		              								dtoMassCloseFiscalPeriodSetup.setSeriesId(seriesType != null ?seriesType.getTypeId():0);
		              								response.add(dtoMassCloseFiscalPeriodSetup);
		                                			
		                                		}
		                        	 }
		                             
		                         }
						 }
				/*}*/
			}
			return response;
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return response;
	}

	/**
	 * @param dtoMassCloseFiscalPeriodSetup
	 * @return
	 */
	public DtoMassCloseFiscalPeriodSetup saveMassCloseFiscalPeriodSetup(
		DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup) {
		try
		{
			String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
			int loggedInUserId=0;
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
				loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			
			SeriesType seriesType = repositorySeriesType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoMassCloseFiscalPeriodSetup.getSeriesId(),Integer.parseInt(langId),false);
			if(seriesType!=null)
			{
				List<ModulesConfigurationPeriod> list= repositoryModulesConfigurationPeriod.findByYearAndSeriesTypeTypeIdAndIsDeleted(dtoMassCloseFiscalPeriodSetup.getYear(), seriesType.getTypeId(), false);
				if(list!=null && !list.isEmpty()){
					repositoryModulesConfigurationPeriod.deleteInBatch(list);
				}
			}
			
			for(DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriod : dtoMassCloseFiscalPeriodSetup.getRecords())
			{
				GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetail  = repositoryGLConfigurationDefinePeriodsDetails.findByGlConfigurationDefinePeriodsHeaderYearAndIsDeletedAndPeriodId(dtoMassCloseFiscalPeriodSetup.getYear(), false, dtoMassCloseFiscalPeriod.getPeriodId());
				if(glConfigurationDefinePeriodsDetail!=null)
				{
					ModulesConfigurationPeriod modulesConfigurationPeriod = new ModulesConfigurationPeriod();
					modulesConfigurationPeriod.setYear(dtoMassCloseFiscalPeriodSetup.getYear());
					modulesConfigurationPeriod.setSeriesType(seriesType);
					modulesConfigurationPeriod.setGlConfigurationDefinePeriodsDetails(glConfigurationDefinePeriodsDetail);
					modulesConfigurationPeriod.setCreatedBy(loggedInUserId);
					modulesConfigurationPeriod.setClosePeriod(dtoMassCloseFiscalPeriod.getIsClose());
					modulesConfigurationPeriod.setOriginId(dtoMassCloseFiscalPeriod.getOriginId());
					modulesConfigurationPeriod.setModuleDescription(dtoMassCloseFiscalPeriod.getPeriodName());
					repositoryModulesConfigurationPeriod.saveAndFlush(modulesConfigurationPeriod);
					if(seriesType!=null){
							dtoMassCloseFiscalPeriod.setSeriesName(seriesType.getTypePrimary());
					}
				}
			}
			return dtoMassCloseFiscalPeriodSetup;
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}
	
	
	/**
	 * @param accountTableRowIndexId
	 * @return
	 */
	public String getAccountNumberByAccountTableRowIndex(int accountTableRowIndexId) {
		StringBuilder accountNumber = new StringBuilder("");
		List<PayableAccountClassSetup> listOfPayable = repositoryPayableAccountClassSetup
				.findByAccountRowIndexAndIsDeleted(accountTableRowIndexId, false);
		int i = 0;
		for (PayableAccountClassSetup payableAccountClassSetup : listOfPayable) {
			if (i == 0) {
				COAMainAccounts coaMainAccounts = repositoryCOAMainAccounts
						.findByActIndxAndIsDeleted(payableAccountClassSetup.getIndexId(), false);

				if (coaMainAccounts != null && UtilRandomKey.isNotBlank(coaMainAccounts.getMainAccountNumber())) {
					accountNumber.append(coaMainAccounts.getMainAccountNumber());
				}

			} else {
				COAFinancialDimensionsValues coafValue = repositoryCOAFinancialDimensionsValues
						.findByDimInxValueAndIsDeleted(payableAccountClassSetup.getIndexId(), false);

				if (coafValue != null && UtilRandomKey.isNotBlank(coafValue.getDimensionValue())) {
					accountNumber.append("-" + coafValue.getDimensionValue());
				}
			}
			i++;
		}
		return accountNumber.toString();
	}

	public List<DtoOrigin> getOrigin(DtoMassCloseFiscalPeriodSetup dtoMassCloseFiscalPeriodSetup) {
		int langId= serviceHome.getLanngugaeId();
		List<MasterMassCloseOrigin> masterMassCloseOrigins = repositoryMassCloseOrigin.findBySeriesTypeTypeIdAndLanguageLanguageId(dtoMassCloseFiscalPeriodSetup.getSerieTypeId(), langId);
		List<DtoOrigin> list = new ArrayList<>();
		if(masterMassCloseOrigins!=null && !masterMassCloseOrigins.isEmpty()){
			for(MasterMassCloseOrigin origin: masterMassCloseOrigins){
				list.add(new DtoOrigin(origin));
			}
		}
		return list;
	}
	
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchActiveCheckBookMaintenance(DtoSearch dtoSearchs) 
	{
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryCheckBookMaintenance.predictiveActiveSearchCount(dtoSearchs.getSearchKeyword()));
		DtoCheckBookMaintenance dtoCheckBookMaintenance = null;
		List<CheckbookMaintenance> checkbookMaintenances = null;
		List<DtoCheckBookMaintenance> dtoCheckBookMaintenancesList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			checkbookMaintenances = repositoryCheckBookMaintenance
					.predictiveActiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			checkbookMaintenances = repositoryCheckBookMaintenance
					.predictiveActiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (checkbookMaintenances != null) {
			for (CheckbookMaintenance checkbookMaintenance : checkbookMaintenances) {
				dtoCheckBookMaintenance = new DtoCheckBookMaintenance(checkbookMaintenance);
				if(checkbookMaintenance.getGlAccountsTableAccumulation()!=null)
				{
					dtoCheckBookMaintenance.setAccountNumber(getAccountNumberByAccountTableRowIndex(Integer.parseInt(checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex())));
				}
				else
				{
					dtoCheckBookMaintenance.setAccountNumber("");
				}
				
				dtoCheckBookMaintenancesList.add(dtoCheckBookMaintenance);
			}
		}
		dtoSearch.setRecords(dtoCheckBookMaintenancesList);
		return dtoSearch;
	}
	
	
	public DtoFiscalFinancialPeriodSetup getFiscalFinancePeriodSetupByYear(GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader) 
	{
		DtoFiscalFinancialPeriodSetup dtoFiscalFinancialPeriodSetup = new DtoFiscalFinancialPeriodSetup();
		List<DtoFiscalFinancialPeriodSetupDetail> dtoFiscalFinancialPeriodSetupDetailList=new ArrayList<>();
		try 
		{
			List<GLConfigurationDefinePeriodsDetails> configurationDefinePeriodsDetails;
			List<DtoFiscalFinancialPeriodSetupDetail> dtoFiscalFinancialPeriodSetupDetails = new ArrayList<>();
			DtoFiscalFinancialPeriodSetupDetail dtoFiscalFinancialPeriodSetupDetail;
			if(glConfigurationDefinePeriodsHeader != null)
			{
				dtoFiscalFinancialPeriodSetup = new DtoFiscalFinancialPeriodSetup(glConfigurationDefinePeriodsHeader);
				configurationDefinePeriodsDetails = repositoryGLConfigurationDefinePeriodsDetails.findByGlConfigurationDefinePeriodsHeaderYearAndIsDeleted(glConfigurationDefinePeriodsHeader.getYear(),false);
				for(GLConfigurationDefinePeriodsDetails configurationDefinePeriodsDetails2 : configurationDefinePeriodsDetails){
					dtoFiscalFinancialPeriodSetupDetail = new DtoFiscalFinancialPeriodSetupDetail(configurationDefinePeriodsDetails2);
					dtoFiscalFinancialPeriodSetupDetails.add(dtoFiscalFinancialPeriodSetupDetail);
				}
			}
			
			dtoFiscalFinancialPeriodSetupDetailList = dtoFiscalFinancialPeriodSetupDetails;
			dtoFiscalFinancialPeriodSetup.setDtoFiscalFinancialPeriodSetupDetail(dtoFiscalFinancialPeriodSetupDetailList);
		
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoFiscalFinancialPeriodSetup;
	}
	
	public boolean checkTransactionDateIsValid(DtoJournalEntryHeader dtoJournalEntryHeader) 
	{
		if(UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate()))
		{
			   Date transactionDate= UtilDateAndTime.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate());
			   Calendar cal=Calendar.getInstance();
			   cal.setTime(transactionDate);
			   int year = cal.get(Calendar.YEAR);
			   GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader= repositoryGLConfigurationDefinePeriodsHeader.findByYearAndIsDeleted(year, false);
			   if(glConfigurationDefinePeriodsHeader==null){
					  return false;
			   }
			   if(dtoJournalEntryHeader.getModuleName().equalsIgnoreCase("GL"))
			   {
				   List<GLConfigurationDefinePeriodsDetails> list= repositoryGLConfigurationDefinePeriodsDetails.getByTransactionDateAndYearAndPeriodSeriesFinancial(transactionDate,year,true);
				   if(list!=null && !list.isEmpty())
				   {
					   // Series Table Master Data : 1=GL Module
					   for (GLConfigurationDefinePeriodsDetails glSeries : list) {
						   ModulesConfigurationPeriod origin= repositoryModulesConfigurationPeriod.findByYearAndSeriesTypeTypeIdAndOriginIdAndClosePeriodAndGlConfigurationDefinePeriodsDetailsSeriesAndIsDeleted(
								   year,1, dtoJournalEntryHeader.getOriginId(),false,glSeries.getSeries(),false);
					       if(origin!=null){
					    	   return true;
					       }
					   }
				   }
			   }
			   
			   if(dtoJournalEntryHeader.getModuleName().equalsIgnoreCase("AR"))
			   {
				   List<GLConfigurationDefinePeriodsDetails> list= repositoryGLConfigurationDefinePeriodsDetails.getByTransactionDateAndYearAndPeriodSeriesSales(transactionDate,year,true);
				   if(list!=null && !list.isEmpty())
				   {
					  // Series Table Master Data : 2=AR Modules
					   for (GLConfigurationDefinePeriodsDetails glSeries : list) {
						   ModulesConfigurationPeriod origin= repositoryModulesConfigurationPeriod.findByYearAndSeriesTypeTypeIdAndOriginIdAndClosePeriodAndGlConfigurationDefinePeriodsDetailsSeriesAndIsDeleted(
								   year,2, dtoJournalEntryHeader.getOriginId(),false,glSeries.getSeries(),false);
					       if(origin!=null){
					    	   return true;
					       }
					   }
				   }
			   }
			   
			   if(dtoJournalEntryHeader.getModuleName().equalsIgnoreCase("AP"))
			   {
				   List<GLConfigurationDefinePeriodsDetails> list= repositoryGLConfigurationDefinePeriodsDetails.getByTransactionDateAndYearAndPeriodSeriesPurchase(transactionDate,year,true);
				   if(list!=null && !list.isEmpty()){
					// Series Table Master Data : 3=AP Modules
					   for (GLConfigurationDefinePeriodsDetails glSeries : list) {
						   ModulesConfigurationPeriod origin= repositoryModulesConfigurationPeriod.findByYearAndSeriesTypeTypeIdAndOriginIdAndClosePeriodAndGlConfigurationDefinePeriodsDetailsSeriesAndIsDeleted(
								   year,3, dtoJournalEntryHeader.getOriginId(),false,glSeries.getSeries(),false);
					       if(origin!=null){
					    	   return true;
					       }
					   }
				   }
			   }
		}
		return false;
	}
}
