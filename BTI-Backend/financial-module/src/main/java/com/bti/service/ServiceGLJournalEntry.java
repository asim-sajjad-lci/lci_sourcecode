/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.MessageLabel;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumber;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumberKey;
import com.bti.model.GLCurrentSummaryMasterTableByDimensions;
import com.bti.model.GLCurrentSummaryMasterTableByDimensionsKey;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccount;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccountKey;
import com.bti.model.GLYTDHistoryTransactions;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.MasterCorrectJournalEntryActions;
import com.bti.model.dto.DtoCompany;
import com.bti.model.dto.DtoGLYTDOpenTransactions;
import com.bti.model.dto.DtoGeneralLedgerSetup;
import com.bti.model.dto.DtoJournalEntryDetail;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoTypes;
import com.bti.repository.RepositoryCorrectJournalEntryActions;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByDimensionIndex;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByMainAccount;
import com.bti.repository.RepositoryGlytdHistoryTransaction;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.mapping.GL.MappingPaymentVoucher;
import com.bti.util.mapping.GL.MappingPostedJournalEntry;
import com.jayway.restassured.path.json.JsonPath;



/**
 * Description: ServiceGLJournalEntry Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceGLJournalEntry")
public class ServiceGLJournalEntry {

	private static final Logger LOG = Logger.getLogger(ServiceGLJournalEntry.class);

	@Autowired(required = false) HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;

	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;

	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;

	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;

	@Autowired
	RepositoryGlytdHistoryTransaction repositoryGlytdHistoryTransaction;

	@Autowired
	RepositoryCorrectJournalEntryActions repositoryCorrectJournalEntryActions;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	ServiceAccountPayable serviceAccountPayable;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByMainAccount repositoryGLCurrentSummaryMasterTableByMainAccount;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByDimensionIndex repositoryGLCurrentSummaryMasterTableByDimensionIndex;

	@Autowired
	MappingPostedJournalEntry mappingPostedJournalEntry;
	
	/**
	 * @description this service will use to save and update journal entry
	 * @param dtoJournalEntryHeader
	 * @param action
	 * @return
	 */
	public DtoJournalEntryHeader saveAndUpdateJournalEntry(DtoJournalEntryHeader dtoJournalEntryHeader, String action) {
		JournalEntryHeader journalEntryHeader = null;
		List<JournalEntryDetails> journalEntryDetailsList = new ArrayList<>();
		List<JournalEntryDetails> tempJournalEntryDetail = new ArrayList<>();
		
		try {
			int nextJournalId = 0;
			GLConfigurationSetup glConfigurationSetup = null;
			if (action.equalsIgnoreCase(CommonConstant.SAVE)) {
				glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
				if (glConfigurationSetup == null) {
					return null;
				}
				journalEntryHeader = new JournalEntryHeader();
				nextJournalId = glConfigurationSetup.getNextJournalEntryVoucher();
				journalEntryHeader.setJournalID(String.valueOf(nextJournalId));
				journalEntryHeader.setOriginalJournalEntryID(dtoJournalEntryHeader.getOriginalJournalID());
				journalEntryHeader.setCreatedBy(httpServletRequest.getIntHeader(CommonConstant.USER_ID));
			}

			else if (action.equalsIgnoreCase(CommonConstant.UPDATE)) {
				journalEntryHeader = this.repositoryJournalEntryHeader
						.findByJournalIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(dtoJournalEntryHeader.getJournalID(),dtoJournalEntryHeader.getSourceDocumentId(), false);
				if (journalEntryHeader == null) {
					return null;
				}
				journalEntryHeader.setUpdatedBy(httpServletRequest.getIntHeader(CommonConstant.USER_ID));
				// journalEntryDetailsList = repositoryJournalEntryDetail
				//		.findByJournalEntryHeaderJournalIDAndIsDeleted(journalEntryHeader.getJournalID(), false);
				journalEntryDetailsList = repositoryJournalEntryDetail
						.findByJournalEntryHeaderJournalIDAndJournalEntryHeaderGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(journalEntryHeader.getJournalID(), journalEntryHeader.getGlConfigurationAuditTrialCodes().getSeriesIndex(), false);
//				if (journalEntryDetailsList != null && !journalEntryDetailsList.isEmpty()) {
//					repositoryJournalEntryDetail.deleteInBatch(journalEntryDetailsList);
//				}
			}

			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			journalEntryHeader.setInterCompany(dtoJournalEntryHeader.getInterCompany());
			journalEntryHeader.setGlBatches(
					repositoryGLBatches.findByBatchIDAndIsDeleted(dtoJournalEntryHeader.getGlBatchId(), false));
			journalEntryHeader.setTransactionType(dtoJournalEntryHeader.getTransactionType());
			if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate())) {
				journalEntryHeader.setTransactionDate(
						UtilDateAndTime.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate()));
			} else {
				journalEntryHeader.setTransactionDate(null);
			}

			GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
					.findBySeriesIndexAndIsDeleted(dtoJournalEntryHeader.getSourceDocumentId(), false);
			journalEntryHeader.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
			journalEntryHeader.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
			journalEntryHeader.setJournalDescription(dtoJournalEntryHeader.getJournalDescription());
			journalEntryHeader.setJournalDescriptionArabic(dtoJournalEntryHeader.getJournalDescriptionArabic());
			journalEntryHeader.setTransactionSource("JV");
			CurrencySetup currencySetup = repositoryCurrencySetup
					.findByCurrencyIdAndIsDeleted(dtoJournalEntryHeader.getCurrencyID(), false);
			journalEntryHeader.setCurrencySetup(currencySetup);

			if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) {
				// this is original credit amount with user selected currecy
				Double originalCreditAmount = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit());
				journalEntryHeader.setOriginalTotalJournalEntryCredit(originalCreditAmount);
				// Set default amount if do not required conversion, meaning if
				// you have already selected currency Id USD
				journalEntryHeader.setTotalJournalEntryCredit(originalCreditAmount);
			}

			if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) {
				// this is original debit amount with user selected currecy
				Double originalDebitAmount = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit());
				journalEntryHeader.setOriginalTotalJournalEntryDebit(originalDebitAmount);
				// Set default amount if do not required conversion, meaning if
				// you have already selected currency Id USD
				journalEntryHeader.setTotalJournalEntryDebit((originalDebitAmount));
			}

			Double currentExchangeRate = Double.valueOf(dtoJournalEntryHeader.getExchangeRate());
			journalEntryHeader.setExchangeTableRate(currentExchangeRate);
			String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
			CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
					.findByExchangeIndexAndIsDeleted(dtoJournalEntryHeader.getExchangeTableIndex(), false);
			if (currencyExchangeHeader != null) {
				journalEntryHeader.setCurrencyExchangeHeader(currencyExchangeHeader);
				if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getExchangeRate())) {
					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getCurrencyID())) {
						if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
							calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
									.getMethod();
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) {
							double totalDebitAmount = (double) mathEval
									.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit()) + calcMethod
											+ currentExchangeRate);
							journalEntryHeader.setTotalJournalEntryDebit(totalDebitAmount);
						}
						if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) {
							double totalCreditAmount = (double) mathEval
									.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit())
											+ calcMethod + currentExchangeRate);
							journalEntryHeader.setTotalJournalEntryCredit(totalCreditAmount);
						}
					}
				}
			}

			journalEntryHeader = repositoryJournalEntryHeader.save(journalEntryHeader);
			if (action.equalsIgnoreCase(CommonConstant.SAVE)) {
				glConfigurationSetup.setNextJournalEntryVoucher(nextJournalId + 1);
				repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);
			}
			
			
			JournalEntryDetails journalEntryDetails= null;

			 
	    
			
			
			if (dtoJournalEntryHeader.getJournalEntryDetailsList() != null
					&& !dtoJournalEntryHeader.getJournalEntryDetailsList().isEmpty()) {
				for (DtoJournalEntryDetail dtoJournalEntryDetail : dtoJournalEntryHeader.getJournalEntryDetailsList()) {
					
					journalEntryDetails= null;
					
					 journalEntryDetails=journalEntryDetailsList.stream()
						.filter(journalDetail -> journalDetail.getJournalSequence() == dtoJournalEntryDetail.getJournalSequence())
						.findFirst().orElse(null);
								
					if(journalEntryDetails != null) {
								journalEntryDetails = repositoryJournalEntryDetail.findOne(dtoJournalEntryDetail.getJournalSequence());
								journalEntryDetails.setUpdatedBy(Integer.valueOf(httpServletRequest.getHeader(CommonConstant.USER_ID)));
						journalEntryDetailsList.removeIf( e -> e.getJournalSequence() == dtoJournalEntryDetail.getJournalSequence());
					}else {
								journalEntryDetails = new JournalEntryDetails();
								journalEntryDetails.setCreatedBy(Integer.valueOf(httpServletRequest.getHeader(CommonConstant.USER_ID)));
							}
							
				
//					journalEntryDetails= null;
//					
//					if( journalEntryDetailsList != null && (!journalEntryDetailsList.isEmpty()) ) {
//						int i = 1;
//						for(JournalEntryDetails journalEntryDetail:journalEntryDetailsList) {
//							
//							
//							
//							// update journal entry detail
//							if( dtoJournalEntryDetail.getJournalSequence() ==  journalEntryDetail.getJournalSequence()) {
//								
//								journalEntryDetails = repositoryJournalEntryDetail.findOne(dtoJournalEntryDetail.getJournalSequence());
//								journalEntryDetails.setUpdatedBy(Integer.valueOf(httpServletRequest.getHeader(CommonConstant.USER_ID)));
//								i++;
//								int x = journalEntryDetailsList.indexOf(journalEntryDetail);
//								journalEntryDetailsList.removeIf(e -> e.getJournalSequence() == journalEntryDetail.getJournalSequence());
//								break;
//							}
//							// save journal entry detail
//						else if(journalEntryDetailsList.size() == 1 && journalEntryDetails == null) {
//								journalEntryDetails = new JournalEntryDetails();
//								journalEntryDetails.setCreatedBy(Integer.valueOf(httpServletRequest.getHeader(CommonConstant.USER_ID)));
//							}
//							i++;
//						}
//					}else {
//							journalEntryDetails = new JournalEntryDetails();
//							journalEntryDetails.setCreatedBy(Integer.valueOf(httpServletRequest.getHeader(CommonConstant.USER_ID)));
//							
//					}
//				
					
					
					journalEntryDetails.setDistributionDescription(dtoJournalEntryDetail.getDistributionDescription());
					if (dtoJournalEntryDetail.getIsCredit()) {
						journalEntryDetails.setBalanceType(2);
					} else {
						journalEntryDetails.setBalanceType(1);
					}
					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) {
						Double originalDebitAmount = Double.valueOf(dtoJournalEntryDetail.getCreditAmount());
						journalEntryDetails.setOriginalCreditAmount(originalDebitAmount);
						journalEntryDetails.setCreditAmount(originalDebitAmount);
					}
					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) {
						Double originalDebitAmount = Double.valueOf(dtoJournalEntryDetail.getDebitAmount());
						journalEntryDetails.setOriginalDebitAmount(originalDebitAmount);
						journalEntryDetails.setDebitAmount(originalDebitAmount);
					}
					journalEntryDetails
							.setIntercompanyIDMultiCompanyTransaction(dtoJournalEntryDetail.getIntercompanyId());
					journalEntryDetails.setAccountTableRowIndex(dtoJournalEntryDetail.getAccountTableRowIndex());
					journalEntryDetails.setJournalEntryHeader(journalEntryHeader);
					journalEntryDetails.setExchangeTableRate(journalEntryHeader.getExchangeTableRate());
					// Currency Is Dollar check at top for if selected currency
					// already usd.
					if (UtilRandomKey.isNotBlank(calcMethod) && journalEntryHeader.getExchangeTableRate() != null) {
						if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) {
							double creditAmount = (double) mathEval
									.eval(Double.valueOf(dtoJournalEntryDetail.getCreditAmount()) + calcMethod
											+ journalEntryHeader.getExchangeTableRate());
							journalEntryDetails.setCreditAmount(creditAmount);
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) {
							double debitAmount = (double) mathEval
									.eval(Double.valueOf(dtoJournalEntryDetail.getDebitAmount()) + calcMethod
											+ journalEntryHeader.getExchangeTableRate());
							journalEntryDetails.setDebitAmount(debitAmount);
						}

					}
					
					tempJournalEntryDetail.add(journalEntryDetails);
					
				}
				if(journalEntryDetailsList != null) {
					repositoryJournalEntryDetail.delete(journalEntryDetailsList);
				}
				
				repositoryJournalEntryDetail.save(tempJournalEntryDetail);

			}
		} catch (Exception e) {
			dtoJournalEntryHeader.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR+": "+e.getMessage());
			return dtoJournalEntryHeader;
		}

		if (journalEntryHeader != null && dtoJournalEntryHeader.getMessageType() == null) {
			return getJournalEntryByJournalId(journalEntryHeader);
		} else {
			dtoJournalEntryHeader.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			return dtoJournalEntryHeader;
		}
	}

	/**
	 * @description this service will use to post and correct journal entry
	 * @param dtoJournalEntryHeader
	 * @return
	 */
	public DtoJournalEntryHeader postOrCorrectionJournalEntry(DtoJournalEntryHeader dtoJournalEntryHeader) {
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}

		try {
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");

			if (dtoJournalEntryHeader.getJournalEntryDetailsList() != null
					&& !dtoJournalEntryHeader.getJournalEntryDetailsList().isEmpty()) {

				for (DtoJournalEntryDetail dtoJournalEntryDetail : dtoJournalEntryHeader.getJournalEntryDetailsList()) 
				{
					GLYTDOpenTransactions glytdOpenTransactions = new GLYTDOpenTransactions();
					glytdOpenTransactions.setOpenYear(Calendar.getInstance().get(Calendar.YEAR));
					glytdOpenTransactions.setJournalEntryID(dtoJournalEntryHeader.getJournalID());
					if (dtoJournalEntryHeader.isCorrection()) //Reversing the transactions
					{
						glytdOpenTransactions.setOriginalJournalEntryID(dtoJournalEntryHeader.getOriginalJournalID());
						glytdOpenTransactions.setTransactionReversingDate(new Date());
						glytdOpenTransactions.setTransactionType(2); // 2 for reversing and 1 for standard
						String creditAmount = dtoJournalEntryDetail.getCreditAmount();
						String debitAmount = dtoJournalEntryDetail.getDebitAmount();
						dtoJournalEntryDetail.setDebitAmount(creditAmount);
						dtoJournalEntryDetail.setCreditAmount(debitAmount);
					}
					else
					{
						glytdOpenTransactions.setTransactionType(dtoJournalEntryHeader.getTransactionType());
					}
					
					GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
							.findBySeriesIndexAndIsDeleted(dtoJournalEntryHeader.getSourceDocumentId(), false);
					glytdOpenTransactions.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
					glytdOpenTransactions.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
					glytdOpenTransactions.setJournalDescription(dtoJournalEntryHeader.getJournalDescription());
					glytdOpenTransactions
							.setJournalDescriptionArabic(dtoJournalEntryHeader.getJournalDescriptionArabic());
					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate())) {
						glytdOpenTransactions.setTransactionDate(
								UtilDateAndTime.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate()));
					}

					glytdOpenTransactions.setTransactionPostingDate(new Date());
					glytdOpenTransactions.setTransactionSource("JV");

					
					glytdOpenTransactions.setAccountTableRowIndex(dtoJournalEntryDetail.getAccountTableRowIndex());
					glytdOpenTransactions.setUserCreateTransaction(loggedInUserId + "");
					glytdOpenTransactions.setUserPostingTransaction(loggedInUserId + "");
					glytdOpenTransactions
							.setDistributionDescription(dtoJournalEntryDetail.getDistributionDescription());
					glytdOpenTransactions.setCurrencyId(dtoJournalEntryHeader.getCurrencyID());

					double debitAmount = 0.0;
					double creditAmount = 0.0;
					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) {
						Double originalCreditAmount = Double.valueOf(dtoJournalEntryDetail.getCreditAmount());
						glytdOpenTransactions.setOriginalCreditAmount(originalCreditAmount);
						// Set Original amount in above field if selected
						// currecncy Id USD
						glytdOpenTransactions.setCreditAmount(originalCreditAmount);
						creditAmount = originalCreditAmount;
					}

					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) {
						Double originalDebitAmount = Double.valueOf(dtoJournalEntryDetail.getDebitAmount());
						glytdOpenTransactions.setOriginalDebitAmount(originalDebitAmount);
						// Set Original amount in above field if selected
						// currency Id USD
						glytdOpenTransactions.setDebitAmount(originalDebitAmount);
						debitAmount = originalDebitAmount;
					}

					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) {
						Double originalTotalCreditAmount = Double
								.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit());
						glytdOpenTransactions.setOriginalTotalJournalEntryCredit(originalTotalCreditAmount);
						// Set Original amount in above field if selected
						// currency Id USD
						glytdOpenTransactions.setTotalJournalEntryCredit(originalTotalCreditAmount);
					}

					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) {
						Double originalDebitAmount = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit());
						glytdOpenTransactions.setOriginalTotalJournalEntryDebit(originalDebitAmount);
						// Set Original amount in above field if selected
						// currency Id USD
						glytdOpenTransactions.setTotalJournalEntryDebit(originalDebitAmount);
					}

					glytdOpenTransactions.setOriginalCompanyID(dtoJournalEntryDetail.getIntercompanyId());
					if (dtoJournalEntryDetail.getIsCredit()) {
						glytdOpenTransactions.setBalanceType(2);
					} else {
						glytdOpenTransactions.setBalanceType(1);
					}

					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
							.findByExchangeIndexAndIsDeleted(dtoJournalEntryHeader.getExchangeTableIndex(), false);
					if (currencyExchangeHeader != null) {
						glytdOpenTransactions.setExchangeTableIndex(dtoJournalEntryHeader.getExchangeTableIndex());
						if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getExchangeRate())) {
							Double currentExchangeRate = Double.valueOf(dtoJournalEntryHeader.getExchangeRate());
							glytdOpenTransactions.setExchangeTableRate(currentExchangeRate);
							String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
							if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
								calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
										.getMethod();
							}

							if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) {
								debitAmount = (double) mathEval
										.eval(Double.valueOf(dtoJournalEntryDetail.getDebitAmount()) + calcMethod
												+ currentExchangeRate);
								glytdOpenTransactions.setDebitAmount(debitAmount);
							}
							if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) {
								creditAmount = (double) mathEval
										.eval(Double.valueOf(dtoJournalEntryDetail.getCreditAmount()) + calcMethod
												+ currentExchangeRate);
								glytdOpenTransactions.setCreditAmount(creditAmount);
							}

							if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) {
								double totalDebitAmount = (double) mathEval
										.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit())
												+ calcMethod + currentExchangeRate);
								glytdOpenTransactions.setTotalJournalEntryDebit(totalDebitAmount);
							}
							if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) {
								double totalCreditAmount = (double) mathEval
										.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit())
												+ calcMethod + currentExchangeRate);
								glytdOpenTransactions.setTotalJournalEntryCredit(totalCreditAmount);
							}

							glytdOpenTransactions.setRowDateIndex(new Date());
						}
					}
					repositoryGlytdOpenTransaction.save(glytdOpenTransactions);

					String accountNumberWithZero = null;
					Integer year = null;
					Integer periodId = null;
					int dimIndex1 = 0;
					int dimIndex2 = 0;
					int dimIndex3 = 0;
					int dimIndex4 = 0;
					int dimIndex5 = 0;
					int mainAccountIndex = 0;
					int accountTableRowIndex = 0;

					GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
							.findByAccountTableRowIndexAndIsDeleted(dtoJournalEntryDetail.getAccountTableRowIndex(),
									false);
					if (glAccountsTableAccumulation != null) {
						accountTableRowIndex = Integer.parseInt(dtoJournalEntryDetail.getAccountTableRowIndex());
						accountNumberWithZero = serviceAccountPayable
								.getAccountNumberIdsWithZero(glAccountsTableAccumulation);
					}

					if (accountNumberWithZero != null && UtilRandomKey.isNotBlank(accountNumberWithZero)) {
						String[] accountNo = accountNumberWithZero.split("-");
						for (int i = 0; i < accountNo.length; i++) {
							if (i > 0) {
								if (i == 1) {
									dimIndex1 = Integer.parseInt(accountNo[i]);
								}
								if (i == 2) {
									dimIndex2 = Integer.parseInt(accountNo[i]);
								}
								if (i == 3) {
									dimIndex3 = Integer.parseInt(accountNo[i]);
								}
								if (i == 4) {
									dimIndex4 = Integer.parseInt(accountNo[i]);
								}
								if (i == 5) {
									dimIndex5 = Integer.parseInt(accountNo[i]);
								}
							}
						}
						mainAccountIndex = Integer.parseInt(accountNo[0]);
					}

					Calendar cal = Calendar.getInstance();
					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate())) {
						Date transDate = UtilDateAndTime
								.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate());
						cal.setTime(transDate);
						year = cal.get(Calendar.YEAR);
						periodId = cal.get(Calendar.MONTH);
					}

					// find by composite key
					GLCurrentSummaryMasterTableByMainAccountKey currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
					currSumaryByMainAccKey.setYear(year);
					currSumaryByMainAccKey.setPeriodID(periodId + 1);
					currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
					GLCurrentSummaryMasterTableByMainAccount currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount
							.findOne(currSumaryByMainAccKey);
					if (currSumaryByMainAcc != null) {
						currSumaryByMainAcc.setDebitAmount(currSumaryByMainAcc.getDebitAmount() + debitAmount);
						currSumaryByMainAcc.setCreditAmount(currSumaryByMainAcc.getCreditAmount() + creditAmount);
						currSumaryByMainAcc.setPeriodBalance(
								currSumaryByMainAcc.getDebitAmount() - currSumaryByMainAcc.getCreditAmount());
					} else {
						currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
						currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
						currSumaryByMainAcc.setYear(year);
						currSumaryByMainAcc.setPeriodID(periodId + 1);
						currSumaryByMainAcc.setDebitAmount(debitAmount);
						currSumaryByMainAcc.setCreditAmount(creditAmount);
						currSumaryByMainAcc.setPeriodBalance(debitAmount - creditAmount);
					}

					currSumaryByMainAcc.setRowDateIndex(new Date());
					repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);

					GLCurrentSummaryMasterTableByAccountNumberKey currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
					currSumaryByAccNumberKey.setYear(year);
					currSumaryByAccNumberKey.setAccountTableRowIndex(accountTableRowIndex);
					currSumaryByAccNumberKey.setPeriodID(periodId + 1);
					GLCurrentSummaryMasterTableByAccountNumber currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
							.findOne(currSumaryByAccNumberKey);
					if (currSumaryByAccNumber != null) {
						currSumaryByAccNumber.setDebitAmount(currSumaryByAccNumber.getDebitAmount() + debitAmount);
						currSumaryByAccNumber.setCreditAmount(currSumaryByAccNumber.getCreditAmount() + creditAmount);
						currSumaryByAccNumber.setPeriodBalance(
								currSumaryByAccNumber.getDebitAmount() - currSumaryByAccNumber.getCreditAmount());
					} else {
						currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
						currSumaryByAccNumber.setAccountTableRowIndex(accountTableRowIndex);
						currSumaryByAccNumber.setYear(year);
						currSumaryByAccNumber.setPeriodID(periodId + 1);
						currSumaryByAccNumber.setDebitAmount(debitAmount);
						currSumaryByAccNumber.setCreditAmount(creditAmount);
						currSumaryByAccNumber.setPeriodBalance(debitAmount - creditAmount);
					}
					currSumaryByAccNumber.setIsDeleted(false);
					currSumaryByAccNumber.setRowDateIndex(new Date());
					repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

					GLCurrentSummaryMasterTableByDimensionsKey glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
					glCurreSummaryByDimInxKey.setYear(year);
					glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
					glCurreSummaryByDimInxKey.setPeriodID(periodId + 1);
					GLCurrentSummaryMasterTableByDimensions glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
							.findOne(glCurreSummaryByDimInxKey);
					if (glCurreSummaryByDimInx != null) {
						glCurreSummaryByDimInx.setCreditAmount(glCurreSummaryByDimInx.getCreditAmount() + creditAmount);
						glCurreSummaryByDimInx.setDebitAmount(glCurreSummaryByDimInx.getDebitAmount() + debitAmount);
						glCurreSummaryByDimInx.setPeriodBalance(
								glCurreSummaryByDimInx.getDebitAmount() - glCurreSummaryByDimInx.getCreditAmount());
					} else {
						glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
						glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
						glCurreSummaryByDimInx.setYear(year);
						glCurreSummaryByDimInx.setPeriodID(periodId + 1);
						glCurreSummaryByDimInx.setCreditAmount(creditAmount);
						glCurreSummaryByDimInx.setDebitAmount(debitAmount);
						glCurreSummaryByDimInx.setPeriodBalance(debitAmount - creditAmount);
					}

					glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
					glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
					glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
					glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
					glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
					glCurreSummaryByDimInx.setRowDateIndex(new Date());
					repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);
				}

				JournalEntryHeader journalEntryHeader = repositoryJournalEntryHeader
						.findByJournalIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(dtoJournalEntryHeader.getJournalID(), dtoJournalEntryHeader.getSourceDocumentId(),false);
				if (journalEntryHeader != null) {
					List<JournalEntryDetails> journalEntryDetailList = repositoryJournalEntryDetail
							.findByJournalEntryHeaderJournalIDAndJournalEntryHeaderGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(journalEntryHeader.getJournalID(),journalEntryHeader.getGlConfigurationAuditTrialCodes().getSeriesIndex(), false);
					if (journalEntryDetailList != null && !journalEntryDetailList.isEmpty()) {
						repositoryJournalEntryDetail.deleteInBatch(journalEntryDetailList);
					}
					repositoryJournalEntryHeader.delete(journalEntryHeader);
				}
				return dtoJournalEntryHeader;
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	/**
	 * @description this service will use to get journal Id List
	 * @return
	 */
	public List<DtoJournalEntryHeader> getJournalIdListForUpdateJvEntry() {
		List<DtoJournalEntryHeader> list = new ArrayList<>();
		List<JournalEntryHeader> journalEntryHeaders = repositoryJournalEntryHeader.getJournalIdListForUpdateJvEntry();
		if (journalEntryHeaders != null && !journalEntryHeaders.isEmpty()) {
			for (JournalEntryHeader journalEntryHeader : journalEntryHeaders) {
				DtoJournalEntryHeader journalId = new DtoJournalEntryHeader();
				journalId.setJournalID(journalEntryHeader.getJournalID());
				list.add(journalId);
			}
		}
		return list;
	}

	/**
	 * @description this service will use to get journal entry by journal Id
	 * @param journalEntryHeader
	 * @return
	 */
	public DtoJournalEntryHeader getJournalEntryByJournalId(JournalEntryHeader journalEntryHeader) {
		DtoJournalEntryHeader dtoJournalEntryHeader = new DtoJournalEntryHeader(journalEntryHeader);
		List<DtoJournalEntryDetail> detailList = new ArrayList<>();
		List<JournalEntryDetails> journalEntryDetailsList = repositoryJournalEntryDetail
				.findByJournalEntryHeaderJournalIDAndIsDeleted(journalEntryHeader.getJournalID(), false);
		if (journalEntryDetailsList != null && !journalEntryDetailsList.isEmpty()) 
		{
			for (JournalEntryDetails journalEntryDetails : journalEntryDetailsList) 
			{
				DtoJournalEntryDetail detailListt = new DtoJournalEntryDetail(journalEntryDetails);
				detailListt.setAccountNumber("");
				GLAccountsTableAccumulation glAccountsTableAccumulation=null;
				String accountNumber = "";
				String accountDesc = "";
				String accountTableRowIndex="";
				String companyName="";
				if(UtilRandomKey.isNotBlank(journalEntryDetails.getIntercompanyIDMultiCompanyTransaction()))
				{
						String tenantIdFrom = serviceHome
								.getCompanyDetail(Integer.parseInt(journalEntryDetails.getIntercompanyIDMultiCompanyTransaction()));
						companyName= serviceHome.getCompanyName(Integer.parseInt(journalEntryDetails.getIntercompanyIDMultiCompanyTransaction()));
						JsonPath jsonFrom = null;
						if (tenantIdFrom != null) {
							
							String headerTenant = httpServletRequest.getHeader(CommonConstant.TENANT_ID);
							if (!headerTenant.equalsIgnoreCase(tenantIdFrom)) {
								jsonFrom = serviceHome.getAccountNumberDetailByAcoountIdAndCompanyId(
										journalEntryDetails.getAccountTableRowIndex(), tenantIdFrom);
								if (jsonFrom != null) {
									accountNumber = jsonFrom.getString("result.accountNumber");
									accountDesc = jsonFrom.getString("result.accountDesc");
									accountTableRowIndex = jsonFrom.getString("result.accountTableRowIndex");
								}
							}
							else
							{
								glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.
										findByAccountTableRowIndexAndIsDeleted(journalEntryDetails.getAccountTableRowIndex(), false);
								if(glAccountsTableAccumulation!=null)
								{
									
									Object[] object	=	serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
									accountNumber=object[0].toString();
									if(UtilRandomKey.isNotBlank(object[3].toString())){
										accountDesc=object[3].toString();
									}
									accountTableRowIndex = glAccountsTableAccumulation.getAccountTableRowIndex();
								}
							}
					 }
				}
				else
				{
						glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.
								findByAccountTableRowIndexAndIsDeleted(journalEntryDetails.getAccountTableRowIndex(), false);
						if(glAccountsTableAccumulation!=null)
						{
							Object[] object	=	serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
							accountNumber=object[0].toString();
							if(UtilRandomKey.isNotBlank(object[3].toString())){
								accountDesc=object[3].toString();
							}
							accountTableRowIndex = glAccountsTableAccumulation.getAccountTableRowIndex();
						}
			    }
				detailListt.setCompanyName(companyName);
				detailListt.setAccountNumber(accountNumber);
				detailListt.setAccountDescription(accountDesc);
				detailList.add(detailListt);
		}
	}
		dtoJournalEntryHeader.setJournalEntryDetailsList(detailList);
		return dtoJournalEntryHeader;
	}
	
	/**
	 * @description this service will use to get journal entry by journal Id
	 * @param journalEntryHeader
	 * @return
	 */
	public DtoJournalEntryHeader getJournalEntryByJournalIdAndAuditTrial(JournalEntryHeader journalEntryHeader) {
		DtoJournalEntryHeader dtoJournalEntryHeader = new DtoJournalEntryHeader(journalEntryHeader);
		List<DtoJournalEntryDetail> detailList = new ArrayList<>();
		List<JournalEntryDetails> journalEntryDetailsList = repositoryJournalEntryDetail
				.findByJournalEntryHeaderIdAndIsDeleted(journalEntryHeader.getId(), false);
		GLAccountsTableAccumulation glTableAccumulation = null;
		if (journalEntryDetailsList != null && !journalEntryDetailsList.isEmpty()) 
		{
			for (JournalEntryDetails journalEntryDetails : journalEntryDetailsList) 
			{
				DtoJournalEntryDetail detailListt = new DtoJournalEntryDetail(journalEntryDetails);

				String companyName="";
                companyName = serviceHome.getCompanyName(
                        Integer.parseInt(journalEntryDetails.getIntercompanyIDMultiCompanyTransaction()));
				glTableAccumulation = repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(journalEntryDetails.getAccountTableRowIndex(), false);
				detailListt.setAccountNumber(glTableAccumulation.getAccountNumber());
				detailListt.setAccountDescription(glTableAccumulation.getAccountDescription());
				detailListt.setAccountDescriptionArabic(glTableAccumulation.getAccountDescriptionArabic());
				detailListt.setCompanyName(companyName);
				detailList.add(detailListt);
		}
	}
		dtoJournalEntryHeader.setJournalEntryDetailsList(detailList);
		return dtoJournalEntryHeader;
	}

	/**
	 * @description this service will use to get journal entry Id by year
	 * @param year
	 * @return
	 */
	public List<DtoJournalEntryHeader> getJournalEntryIdByYearForCopyJournaEntry(int year) {
		List<DtoJournalEntryHeader> list = new ArrayList<>();
		List<String> distnicJounalIdList = null;
		DtoJournalEntryHeader dtoJournalEntryHeader = null;
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		if (year == currentYear) {
			distnicJounalIdList = repositoryJournalEntryHeader.getDistnictJournalIdListForCopyJournalEntry();
			if (distnicJounalIdList != null && !distnicJounalIdList.isEmpty()) {
				for (String journalId : distnicJounalIdList) {
					dtoJournalEntryHeader = new DtoJournalEntryHeader();
					dtoJournalEntryHeader.setJournalID(journalId);
					list.add(dtoJournalEntryHeader);
				}
			}

			distnicJounalIdList = repositoryGlytdOpenTransaction
					.getDistnictJournalIdForCopyJVEntryAndNotReversingForJV();
			if (distnicJounalIdList != null && !distnicJounalIdList.isEmpty()) {
				for (String journalId : distnicJounalIdList) {
					dtoJournalEntryHeader = new DtoJournalEntryHeader();
					dtoJournalEntryHeader.setJournalID(journalId);
					list.add(dtoJournalEntryHeader);
				}
			}
		} else {
			distnicJounalIdList = repositoryGlytdHistoryTransaction.getDistnictJournalIdForCopyJVEntry();
			if (distnicJounalIdList != null && !distnicJounalIdList.isEmpty()) {
				for (String journalId : distnicJounalIdList) {
					dtoJournalEntryHeader = new DtoJournalEntryHeader();
					dtoJournalEntryHeader.setJournalID(journalId);
					list.add(dtoJournalEntryHeader);
				}
			}
		}
		return list;
	}

	/**
	 * @description this service will use to get copy journal entry detail
	 * @param dtoJournalEntryHeader
	 * @return
	 */
	public DtoJournalEntryHeader getCopyJournalEntryDetail(DtoJournalEntryHeader dtoJournalEntryHeader) {
		String journalId = dtoJournalEntryHeader.getJournalID();
		int year = dtoJournalEntryHeader.getYear();
		dtoJournalEntryHeader = null;
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		if (year >= currentYear) {
			List<GLYTDOpenTransactions> glytdOpenTransactions = repositoryGlytdOpenTransaction
					.findByJournalEntryIDAndIsDeleted(journalId, false);
			if (glytdOpenTransactions != null && !glytdOpenTransactions.isEmpty()) {
				dtoJournalEntryHeader = setGlYtdOpenTransactionDataInDto(glytdOpenTransactions);
			} else {
				JournalEntryHeader journalEntryHeader = repositoryJournalEntryHeader
						.findByJournalIDAndIsDeleted(journalId, false);
				if (journalEntryHeader != null) {
					dtoJournalEntryHeader = getJournalEntryByJournalId(journalEntryHeader);
				}
			}
		} else {
			List<GLYTDHistoryTransactions> glytdHistoryTransactions = repositoryGlytdHistoryTransaction
					.findByJournalEntryIDAndIsDeleted(journalId, false);
			if (glytdHistoryTransactions != null && !glytdHistoryTransactions.isEmpty()) {
				dtoJournalEntryHeader = setGlYTDHistoryTransactionDataInDto(glytdHistoryTransactions);
			}
		}
		return dtoJournalEntryHeader;
	}

	/**
	 * @description this service will use to get correct journal entry id by year
	 * @param year
	 * @return
	 */
	public List<DtoJournalEntryHeader> getCorrectJournalEntryIdByYear(int year) {
		List<DtoJournalEntryHeader> list = new ArrayList<>();
		List<String> distnicJounalIdList = null;
		DtoJournalEntryHeader dtoJournalEntryHeader = null;
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		if (year == currentYear) 
		{
				List<String> originalJournalIds = repositoryGlytdOpenTransaction.getDistnictOriginalJournalIdReversingEntry();
				if(originalJournalIds.isEmpty())
				{
					distnicJounalIdList = repositoryGlytdOpenTransaction.
							getDistnictJournalIdForCorrectJVEntryForJV();
				}
				else
				{
					distnicJounalIdList = repositoryGlytdOpenTransaction
							.getDistnictJournalIdForCorrectJVEntryAndNotReversingForJV(originalJournalIds);
					
				}     
				if (distnicJounalIdList != null && !distnicJounalIdList.isEmpty()) {
					for (String journalId : distnicJounalIdList) {
						dtoJournalEntryHeader = new DtoJournalEntryHeader();
						dtoJournalEntryHeader.setJournalID(journalId);
						list.add(dtoJournalEntryHeader);
					}
				}
		}
		return list;
	}

	/**
	 * @description this service will use to get correct journal entry detail
	 * @param dtoJournalEntryHeader
	 * @return
	 */
	public DtoJournalEntryHeader getCorrectJournalEntryDetail(DtoJournalEntryHeader dtoJournalEntryHeader) {
		String journalId = dtoJournalEntryHeader.getJournalID();
		int year = dtoJournalEntryHeader.getYear();
		dtoJournalEntryHeader = null;
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		if (year == currentYear) 
		{
			List<GLYTDOpenTransactions> glytdOpenTransactions = repositoryGlytdOpenTransaction
					.findByJournalEntryIDAndIsDeleted(journalId, false);
			if (glytdOpenTransactions != null && !glytdOpenTransactions.isEmpty()) {
				dtoJournalEntryHeader = setGlYtdOpenTransactionDataInDto(glytdOpenTransactions);
				if (dtoJournalEntryHeader != null) 
				{
					dtoJournalEntryHeader.setActionType(dtoJournalEntryHeader.getActionType());
					dtoJournalEntryHeader.setAction(dtoJournalEntryHeader.getAction());
					dtoJournalEntryHeader.setCorrection(true);
					
					GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
					if(glConfigurationSetup!=null){
						dtoJournalEntryHeader.setOriginalJournalID(dtoJournalEntryHeader.getJournalID());
						dtoJournalEntryHeader.setJournalID(String.valueOf(glConfigurationSetup.getNextJournalEntryVoucher()));
						glConfigurationSetup.setNextJournalEntryVoucher(glConfigurationSetup.getNextJournalEntryVoucher()+1);
						repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);
					}
					postOrCorrectionJournalEntry(dtoJournalEntryHeader);
				}
			}
		}
		return dtoJournalEntryHeader;
	}

	/**
	 * @param glytdOpenTransactionsList
	 * @return
	 */
	public DtoJournalEntryHeader setGlYtdOpenTransactionDataInDto(
			List<GLYTDOpenTransactions> glytdOpenTransactionsList) {
		DtoJournalEntryHeader dtoJournalEntryHeader = new DtoJournalEntryHeader(glytdOpenTransactionsList.get(0));
		List<DtoJournalEntryDetail> detailList = new ArrayList<>();
		for (GLYTDOpenTransactions glytdOpenTransactions : glytdOpenTransactionsList) {
			DtoJournalEntryDetail detail = new DtoJournalEntryDetail(glytdOpenTransactions);
			detail.setAccountNumber("");
			if (UtilRandomKey.isNotBlank(glytdOpenTransactions.getAccountTableRowIndex())) {
				GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(glytdOpenTransactions.getAccountTableRowIndex(), false);
				if (glAccountsTableAccumulation != null) {
					detail.setAccountNumber(serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation)[0].toString());
				}
			}
			detailList.add(detail);
		}
		dtoJournalEntryHeader.setJournalEntryDetailsList(detailList);
		return dtoJournalEntryHeader;
	}

	/**
	 * @description this service will use to set data in dto from another object
	 * @param glytdHistoryTransactionsList
	 * @return
	 */
	public DtoJournalEntryHeader setGlYTDHistoryTransactionDataInDto(
			List<GLYTDHistoryTransactions> glytdHistoryTransactionsList) {
		DtoJournalEntryHeader dtoJournalEntryHeader = new DtoJournalEntryHeader(glytdHistoryTransactionsList.get(0));
		List<DtoJournalEntryDetail> detailList = new ArrayList<>();
		for (GLYTDHistoryTransactions glytdHistoryTransactions : glytdHistoryTransactionsList) {
			DtoJournalEntryDetail detail = new DtoJournalEntryDetail(glytdHistoryTransactions);
			detail.setAccountNumber("");
			if (UtilRandomKey.isNotBlank(glytdHistoryTransactions.getAccountTableRowIndex())) {
				GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(glytdHistoryTransactions.getAccountTableRowIndex(),
								false);
				if (glAccountsTableAccumulation != null) {
					detail.setAccountNumber(serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation)[0].toString());
				}
			}
			detailList.add(detail);
		}
		dtoJournalEntryHeader.setJournalEntryDetailsList(detailList);
		return dtoJournalEntryHeader;
	}

	/**
	 * @description this service will use to get actions of journal entry
	 * @return
	 */
	public List<DtoTypes> getCorrectJournalEntryActions() {
		int languageId = serviceHome.getLanngugaeId();
		List<MasterCorrectJournalEntryActions> actionList = repositoryCorrectJournalEntryActions.findByIsDeletedAndLanguageLanguageId(false,languageId);
		List<DtoTypes> list = new ArrayList<>();
		if (actionList != null && !actionList.isEmpty()) {
			for (MasterCorrectJournalEntryActions action : actionList) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setAction(action.getAction());
				dtoTypes.setActiontype(action.getActionType());
				list.add(dtoTypes);
			}
		}
		return list;
	}

	/**
	 * @description this service will use to get next journal entry
	 * @return
	 */
	public DtoGeneralLedgerSetup getNextJournalEntry() {

		GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
		if (glConfigurationSetup != null) {
			DtoGeneralLedgerSetup dtoGeneralLedgerSetup = new DtoGeneralLedgerSetup();
			dtoGeneralLedgerSetup.setNextJournalEntry(glConfigurationSetup.getNextJournalEntryVoucher());
			dtoGeneralLedgerSetup.setId(glConfigurationSetup.getId());
			return dtoGeneralLedgerSetup;
		} else {
			return null;
		}
	}

	public boolean deleteJournalEntry(DtoJournalEntryHeader dtoJournalEntryHeader) {
		try {
			JournalEntryHeader journalEntryHeader = repositoryJournalEntryHeader
					.findByJournalIDAndIsDeleted(dtoJournalEntryHeader.getJournalID(), false);
			if (journalEntryHeader != null) {
				List<JournalEntryDetails> journalEntryDetailList = repositoryJournalEntryDetail
						.findByJournalEntryHeaderJournalIDAndIsDeleted(journalEntryHeader.getJournalID(), false);
				if (journalEntryDetailList != null && !journalEntryDetailList.isEmpty()) {
					repositoryJournalEntryDetail.deleteInBatch(journalEntryDetailList);
				}
				repositoryJournalEntryHeader.delete(journalEntryHeader);
			}
			return true;
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}
	
	public boolean deleteJournalEntries(List<Integer> ids) {
		JournalEntryHeader journalEntryHeader= null;
		try {
			for(Integer id:ids) {
				journalEntryHeader = repositoryJournalEntryHeader
						.findOne(id.intValue());
				if (journalEntryHeader != null) {
					List<JournalEntryDetails> journalEntryDetailList = repositoryJournalEntryDetail
							.findByJournalEntryHeaderId(journalEntryHeader.getId(), false);
					if (journalEntryDetailList != null && !journalEntryDetailList.isEmpty()) {
						repositoryJournalEntryDetail.deleteInBatch(journalEntryDetailList);
					}
					repositoryJournalEntryHeader.delete(journalEntryHeader);
				}
			}
			return true;
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}
	
	
	public DtoSearch getAllJournal(DtoSearch dtoSearch){
		if(dtoSearch != null)
		{
			dtoSearch.setTotalCount(this.repositoryJournalEntryHeader.predictiveJournalHeaderSearchTotalCount(dtoSearch.getSearchKeyword()));
			List<JournalEntryHeader> journalList =null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				journalList= this.repositoryJournalEntryHeader.predictiveJournalSearchWithPagination(dtoSearch.getSearchKeyword() , new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC, "createdDate"));
			}
			else{
				journalList= this.repositoryJournalEntryHeader.predictiveCompanySearchWithPagination(dtoSearch.getSearchKeyword());
			}
			
			if(journalList != null && !journalList.isEmpty()){
				
				List<DtoJournalEntryHeader> dtoJournalEntryHeader = new ArrayList<>();
				for (JournalEntryHeader journalHeader : journalList) {
					//DtoCompany dtoCompany = new DtoCompany(company);
					DtoJournalEntryHeader dtoJournalHeader = new DtoJournalEntryHeader(journalHeader);

					dtoJournalEntryHeader.add(dtoJournalHeader);
				}
				dtoSearch.setRecords(dtoJournalEntryHeader);
			}
		}
		return dtoSearch;
		
		
	}
	
	 
	public DtoSearch getAllPostedJournal(DtoSearch dtoSearch){
		
		
		
		if(dtoSearch != null) {
			List<Integer> dataList = repositoryGlytdOpenTransaction.predictiveJournalHeaderSearchTotalCount(dtoSearch.getSearchKeyword());
			dtoSearch.setTotalCount(dataList.size());
			List<GLYTDOpenTransactions> postedJournalEntryList = null;
			if(dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
				postedJournalEntryList = this.repositoryGlytdOpenTransaction.predictiveJournalSearchWithPagination(dtoSearch.getSearchKeyword(),new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC, "createdDate"));
				
			}else {
				postedJournalEntryList = this.repositoryGlytdOpenTransaction.predictiveJournalSearchWithPagination(dtoSearch.getSearchKeyword());
			}
			
			if(postedJournalEntryList != null && !postedJournalEntryList.isEmpty()) {
				List<DtoGLYTDOpenTransactions> dtoPostedJournalEntryList = new ArrayList<>();
				for(GLYTDOpenTransactions postedJournal:postedJournalEntryList) {
					DtoGLYTDOpenTransactions dtoPostedJournal = new DtoGLYTDOpenTransactions(postedJournal);
					dtoPostedJournalEntryList.add(dtoPostedJournal);
				}
				dtoSearch.setRecords(dtoPostedJournalEntryList);
			}
		}
		
		return dtoSearch;
		
	}
	
	public DtoJournalEntryHeader getPostedJournalById(DtoJournalEntryHeader dtoJournalEntryHeader) {
		
		
		
		List<GLYTDOpenTransactions> journalEntryPosted = repositoryGlytdOpenTransaction.findByJournalEntryIDAndIsDeleted(dtoJournalEntryHeader.getJournalID(), false);
		if(!journalEntryPosted.isEmpty() && journalEntryPosted != null) {
			dtoJournalEntryHeader = mappingPostedJournalEntry.postedToDto(journalEntryPosted, dtoJournalEntryHeader);
		}
 		
		
		return dtoJournalEntryHeader;
	}
	
	public DtoJournalEntryHeader getPostedJournalByIdAndAudit(DtoJournalEntryHeader dtoJournalEntryHeader) {

		
		List<GLYTDOpenTransactions> journalEntryPosted = repositoryGlytdOpenTransaction.findByJournalEntryIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(dtoJournalEntryHeader.getJournalID(),dtoJournalEntryHeader.getSourceDocumentId(), false);
		if(!journalEntryPosted.isEmpty() && journalEntryPosted != null) {
			dtoJournalEntryHeader = mappingPostedJournalEntry.postedToDto(journalEntryPosted, dtoJournalEntryHeader);
		}
 		
		
		return dtoJournalEntryHeader;
	}
}
