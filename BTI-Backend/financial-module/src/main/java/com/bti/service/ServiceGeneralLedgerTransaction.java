/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 *//*
package com.bti.service;

import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.model.GLBankTransferDetails;
import com.bti.model.GLBankTransferDistribution;
import com.bti.model.GLBankTransferEntryCharges;
import com.bti.model.GLBankTransferHeader;
import com.bti.model.GLCashReceiptBank;
import com.bti.model.GLClearingHeader;
import com.bti.model.JournalEntryHeader;
import com.bti.model.dto.DtoBatches;
import com.bti.model.dto.DtoGLBankTransferCharges;
import com.bti.model.dto.DtoGLBankTransferHeader;
import com.bti.model.dto.DtoGLCashReceiptBank;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.repository.RepositoryBankTransactionEntryAction;
import com.bti.repository.RepositoryBankTransactionEntryType;
import com.bti.repository.RepositoryBankTransactionType;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCorrectJournalEntryActions;
import com.bti.repository.RepositoryCurrencyExchangeDetail;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBankTransferDetails;
import com.bti.repository.RepositoryGLBankTransferDistribution;
import com.bti.repository.RepositoryGLBankTransferEntryCharges;
import com.bti.repository.RepositoryGLBankTransferHeader;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLCashReceiptBank;
import com.bti.repository.RepositoryGLCashReceiptDistribution;
import com.bti.repository.RepositoryGLClearingDetail;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByDimensionIndex;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByMainAccount;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryDetails;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryHeader;
import com.bti.repository.RepositoryGlytdHistoryTransaction;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.repository.RepositoryPaymentMethodType;
import com.bti.repository.RepositoryReceiptType;
import com.bti.repository.RepositoryVATSetup;
import com.bti.util.UtilRandomKey;

*//**
 * Description: ServiceGeneralLedgerTransaction Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 *//*
@Service("serviceGeneralLedgerTransaction")
public class ServiceGeneralLedgerTransaction {

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(ServiceGeneralLedgerTransaction.class);
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;

	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;

	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	@Autowired
	RepositoryCurrencyExchangeDetail repositoryCurrencyExchangeDetail;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;

	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;

	@Autowired
	RepositoryGlytdHistoryTransaction repositoryGlytdHistoryTransaction;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;

	@Autowired
	RepositoryCorrectJournalEntryActions repositoryCorrectJournalEntryActions;

	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;

	@Autowired
	RepositoryGLClearingDetail repositoryGLClearingDetail;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryReceiptType repositoryReceiptType;

	@Autowired
	RepositoryPaymentMethodType repositoryPaymentMethodType;

	@Autowired
	RepositoryGLCashReceiptDistribution repositoryGLCashReceiptDistribution;

	@Autowired
	RepositoryGLCashReceiptBank repositoryGLCashReceiptBank;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	ServiceAccountPayable serviceAccountPayable;

	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByMainAccount repositoryGLCurrentSummaryMasterTableByMainAccount;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByDimensionIndex repositoryGLCurrentSummaryMasterTableByDimensionIndex;

	@Autowired
	RepositoryVATSetup repositoryVATSetup;
	@Autowired
	RepositoryBankTransactionEntryAction repositoryBankTransactionEntryAction;

	@Autowired
	RepositoryBankTransactionEntryType repositoryBankTransactionEntryType;
	@Autowired
	RepositoryBankTransactionType repositoryBankTransactionType;
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryDetails repositoryGLYTDOpenBankTransactionsEntryDetails;
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryHeader repositoryGLYTDOpenBankTransactionsEntryHeader;

	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;

	@Autowired
	RepositoryGLBankTransferHeader repositoryGLBankTransferHeader;
	@Autowired
	RepositoryGLBankTransferDetails repositoryGLBankTransferDetails;
	@Autowired
	RepositoryGLBankTransferDistribution repositoryGLBankTransferDistribution;
	@Autowired
	RepositoryGLBankTransferEntryCharges repositoryGLBankTransferEntryCharges;
	@Autowired
	ServiceGLCashReceiptEntry serviceGLCashReceiptEntry;
	@Autowired
	ServiceGLClearingGeneralEntry serviceGLClearingGeneralEntry;
	@Autowired
	ServiceGLJournalEntry serviceGLJournalEntry;
	@Autowired
	ServiceGLBankTransferEntry serviceGLBankTransferEntry;


	
}
*/