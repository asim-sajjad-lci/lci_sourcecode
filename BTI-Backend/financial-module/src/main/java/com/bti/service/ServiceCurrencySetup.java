/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.CurrencySetup;
import com.bti.model.MasterNegativeSymbolSignTypes;
import com.bti.model.MasterNegativeSymbolTypes;
import com.bti.model.MasterSeperatorDecimal;
import com.bti.model.MasterSeperatorThousands;
import com.bti.model.dto.DtoCurrencySetup;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryMasterNegativeSymbolSignTypes;
import com.bti.repository.RepositoryMasterNegativeSymbolTypes;
import com.bti.repository.RepositoryMasterSeperatorDecimal;
import com.bti.repository.RepositoryMasterSeperatorThousands;
import com.bti.util.UtilRandomKey;

/**
 * Description: Service Authentication
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service("serviceCurrencySetup")
public class ServiceCurrencySetup {

	static Logger log = Logger.getLogger(ServiceCurrencySetup.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	RepositoryMasterNegativeSymbolSignTypes repositoryMasterNegativeSymbolSignTypes;

	@Autowired
	RepositoryMasterNegativeSymbolTypes repositoryMasterNegativeSymbolTypes;

	@Autowired
	RepositoryMasterSeperatorDecimal repositoryMasterSeperatorDecimal;

	@Autowired
	RepositoryMasterSeperatorThousands repositoryMasterSeperatorThousands;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @param dtoCurrencySetup
	 * @return
	 */
	public Boolean saveCurrencySetup(DtoCurrencySetup dtoCurrencySetup){
		int langid = serviceHome.getLanngugaeId();
		if(dtoCurrencySetup != null){
			CurrencySetup currencySetup = new CurrencySetup();
			currencySetup.setCreatedBy(this.getLoggedInUserId());
			currencySetup.setCreatedDate(new Date());
			currencySetup.setCurrencyId(dtoCurrencySetup.getCurrencyId());
			// currency description
			currencySetup.setCurrencyDescription(dtoCurrencySetup.getCurrencyDescription());
			currencySetup.setCurrencyDescriptionArabic(dtoCurrencySetup.getCurrencyDescriptionArabic());
			// currency symbol
			currencySetup.setCurrencySymbol(dtoCurrencySetup.getCurrencySymbol());
			//currency unit
			currencySetup.setCurrencyUnit(dtoCurrencySetup.getCurrencyUnit());
			currencySetup.setCurrencyUnitArabic(dtoCurrencySetup.getCurrencyUnitArabic());
			// currency subunit connector
			currencySetup.setUnitSubunitConnector(dtoCurrencySetup.getUnitSubunitConnector());
			currencySetup.setUnitSubunitConnectorArabic(dtoCurrencySetup.getUnitSubunitConnectorArabic());
			// currency subunit 
			currencySetup.setCurrencySubunit(dtoCurrencySetup.getCurrencySubunit());
			currencySetup.setCurrencySubunitArabic(dtoCurrencySetup.getCurrencySubunitArabic());
			// separator decimal & separators thousands
			currencySetup.setMasterSeperatorDecimal(repositoryMasterSeperatorDecimal.findBySeperatorDecimalIdAndLanguageLanguageIdAndIsDeleted(dtoCurrencySetup.getSeparatorsDecimal(),langid,false));
			currencySetup.setMasterSeperatorThousands(repositoryMasterSeperatorThousands.findBySeperatorThousandIdAndLanguageLanguageIdAndIsDeleted(dtoCurrencySetup.getSeparatorsThousands(),langid,false));
			// row index ID & Date
			currencySetup.setRowIndexId(0);
			currencySetup.setRowDateIndex(null);
			// include space after currency symbol
			currencySetup.setIncludeSpaceAfterCurrencySymbol(dtoCurrencySetup.getIncludeSpaceAfterCurrencySymbol());
			// negative symbol
			currencySetup.setMasterNegativeSymbolTypes(repositoryMasterNegativeSymbolTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCurrencySetup.getNegativeSymbol(),langid,false));
			// display negative symbol sign
			currencySetup.setMasterNegativeSymbolSignTypes(repositoryMasterNegativeSymbolSignTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCurrencySetup.getDisplayNegativeSymbolSign(),langid,false));
			currencySetup.setDisplayCurrencySymbol(dtoCurrencySetup.getDisplayCurrencySymbol());
			currencySetup.setChangeBy(0); // initializing to default value as NULL can't be assigned to int field
			
			currencySetup = this.repositoryCurrencySetup.saveAndFlush(currencySetup);
			if(currencySetup != null){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param currencyId
	 * @return
	 */
	public DtoCurrencySetup getCurrencySetupDeatailsByCurrencyId(String currencyId) {
		int langId=serviceHome.getLanngugaeId();
		if (currencyId != null) {
			CurrencySetup currencySetup = this.repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(currencyId,false);
			if (currencySetup != null) {
				MasterSeperatorDecimal masterSeperatorDecimal = currencySetup.getMasterSeperatorDecimal();
				if(masterSeperatorDecimal!=null){
					masterSeperatorDecimal=repositoryMasterSeperatorDecimal.findBySeperatorDecimalIdAndLanguageLanguageIdAndIsDeleted(masterSeperatorDecimal.getSeperatorDecimalId(),langId,false);
				}
				MasterNegativeSymbolTypes masterNegativeSymbolTypes = currencySetup.getMasterNegativeSymbolTypes();
				if(masterNegativeSymbolTypes!=null){
					masterNegativeSymbolTypes=repositoryMasterNegativeSymbolTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterNegativeSymbolTypes.getTypeId(),langId,false);
				}
				MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes = currencySetup.getMasterNegativeSymbolSignTypes();
				if(masterNegativeSymbolSignTypes!=null){
					masterNegativeSymbolSignTypes=repositoryMasterNegativeSymbolSignTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterNegativeSymbolSignTypes.getTypeId(),langId,false);
				}
				MasterSeperatorThousands masterSeperatorThousands = currencySetup.getMasterSeperatorThousands();
				if(masterSeperatorThousands!=null){
					masterSeperatorThousands=repositoryMasterSeperatorThousands.findBySeperatorThousandIdAndLanguageLanguageIdAndIsDeleted(masterSeperatorThousands.getSeperatorThousandId(),langId,false);
				}
				return new DtoCurrencySetup(currencySetup,masterSeperatorDecimal,masterNegativeSymbolTypes,masterNegativeSymbolSignTypes,masterSeperatorThousands);
			}
		}
		return null;
	}
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchCurrencuSetups(DtoSearch dtoSearch) {
		int langId=serviceHome.getLanngugaeId();
		if (dtoSearch != null) {
			dtoSearch.setTotalCount(
					this.repositoryCurrencySetup.predictiveCurrencySetupSearchCount(dtoSearch.getSearchKeyword()));
			List<CurrencySetup> currencySetupList = null;
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
				currencySetupList = this.repositoryCurrencySetup.predictiveCurrencySetupSearchWithPagination(
						dtoSearch.getSearchKeyword(), new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Direction.DESC, "createdDate"));
			} else {
				currencySetupList = this.repositoryCurrencySetup
						.predictiveCurrencySetupSearch(dtoSearch.getSearchKeyword());
			}

			if (currencySetupList != null && !currencySetupList.isEmpty()) {
				List<DtoCurrencySetup> dtoCurrencySetupList = new ArrayList<>();
				for (CurrencySetup currencySetup : currencySetupList) {
					MasterSeperatorDecimal masterSeperatorDecimal = currencySetup.getMasterSeperatorDecimal();
					if(masterSeperatorDecimal!=null){
						masterSeperatorDecimal=repositoryMasterSeperatorDecimal.findBySeperatorDecimalIdAndLanguageLanguageIdAndIsDeleted(masterSeperatorDecimal.getSeperatorDecimalId(),langId,false);
					}
					MasterNegativeSymbolTypes masterNegativeSymbolTypes = currencySetup.getMasterNegativeSymbolTypes();
					if(masterNegativeSymbolTypes!=null){
						masterNegativeSymbolTypes=repositoryMasterNegativeSymbolTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterNegativeSymbolTypes.getTypeId(),langId,false);
					}
					MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes = currencySetup.getMasterNegativeSymbolSignTypes();
					if(masterNegativeSymbolSignTypes!=null){
						masterNegativeSymbolSignTypes=repositoryMasterNegativeSymbolSignTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterNegativeSymbolSignTypes.getTypeId(),langId,false);
					}
					MasterSeperatorThousands masterSeperatorThousands = currencySetup.getMasterSeperatorThousands();
					if(masterSeperatorThousands!=null){
						masterSeperatorThousands=repositoryMasterSeperatorThousands.findBySeperatorThousandIdAndLanguageLanguageIdAndIsDeleted(masterSeperatorThousands.getSeperatorThousandId(),langId,false);
					}
					dtoCurrencySetupList.add(new DtoCurrencySetup(currencySetup,masterSeperatorDecimal,masterNegativeSymbolTypes,masterNegativeSymbolSignTypes,masterSeperatorThousands));
				}
				dtoSearch.setRecords(dtoCurrencySetupList);
			} else {
				dtoSearch.setRecords(null);
			}
		}
		return dtoSearch;
	}
	
	/**
	 * @param dtoCurrencySetup
	 * @return
	 */
	public Boolean updateCurrencySetup(DtoCurrencySetup dtoCurrencySetup, CurrencySetup currencySetup){
		int langid = serviceHome.getLanngugaeId();
		if(dtoCurrencySetup != null && currencySetup!=null){
			
			
				currencySetup.setChangeBy(this.getLoggedInUserId());
				currencySetup.setModifyDate(new Date());
				
				// currency description
				currencySetup.setCurrencyDescription(dtoCurrencySetup.getCurrencyDescription());
				currencySetup.setCurrencyDescriptionArabic(dtoCurrencySetup.getCurrencyDescriptionArabic());
				// currency symbol
				currencySetup.setCurrencySymbol(dtoCurrencySetup.getCurrencySymbol());
				//currency unit
				currencySetup.setCurrencyUnit(dtoCurrencySetup.getCurrencyUnit());
				currencySetup.setCurrencyUnitArabic(dtoCurrencySetup.getCurrencyUnitArabic());
				// currency subunit connector
				currencySetup.setUnitSubunitConnector(dtoCurrencySetup.getUnitSubunitConnector());
				currencySetup.setUnitSubunitConnectorArabic(dtoCurrencySetup.getUnitSubunitConnectorArabic());
				// currency subunit 
				currencySetup.setCurrencySubunit(dtoCurrencySetup.getCurrencySubunit());
				currencySetup.setCurrencySubunitArabic(dtoCurrencySetup.getCurrencySubunitArabic());
				// separator decimal & separators thousands
				currencySetup.setMasterSeperatorDecimal(repositoryMasterSeperatorDecimal.findBySeperatorDecimalIdAndLanguageLanguageIdAndIsDeleted(dtoCurrencySetup.getSeparatorsDecimal(),langid,false));
				currencySetup.setMasterSeperatorThousands(repositoryMasterSeperatorThousands.findBySeperatorThousandIdAndLanguageLanguageIdAndIsDeleted(dtoCurrencySetup.getSeparatorsThousands(),langid,false));
				
				// include space after currency symbol
				currencySetup.setIncludeSpaceAfterCurrencySymbol(dtoCurrencySetup.getIncludeSpaceAfterCurrencySymbol());
				// negative symbol
				currencySetup.setMasterNegativeSymbolTypes(repositoryMasterNegativeSymbolTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCurrencySetup.getNegativeSymbol(),langid,false));
				// display negative symbol sign
				currencySetup.setMasterNegativeSymbolSignTypes(repositoryMasterNegativeSymbolSignTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoCurrencySetup.getDisplayNegativeSymbolSign(),langid,false));
				currencySetup.setDisplayCurrencySymbol(dtoCurrencySetup.getDisplayCurrencySymbol());
				currencySetup.setRowDateIndex(new Date());
				currencySetup = this.repositoryCurrencySetup.saveAndFlush(currencySetup);
				if(currencySetup != null){
					return true;
				}
		}
		return false;
	}
	
	/**
	 * @return
	 */
	private int getLoggedInUserId() {
		int loggedInUserId = -1;
		try {
			if (UtilRandomKey.isNotBlank(this.httpServletRequest.getHeader(CommonConstant.USER_ID))) {
				loggedInUserId = Integer.parseInt(this.httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
		} catch (NumberFormatException e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return loggedInUserId;
	}
	
	public DtoSearch getActiveCurrencySetup(DtoSearch dtoSearch) {
		int langId=serviceHome.getLanngugaeId();
		if (dtoSearch != null) {
			dtoSearch.setTotalCount(
					this.repositoryCurrencySetup.predictiveCurrencySetupSearchCountByExpireDate(dtoSearch.getSearchKeyword(),new Date()));
			List<CurrencySetup> currencySetupList = null;
			if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
				currencySetupList = this.repositoryCurrencySetup.predictiveCurrencySetupSearchWithPaginationAndByExpireDate(
						dtoSearch.getSearchKeyword(), new Date(), new PageRequest(dtoSearch.getPageNumber(),
								dtoSearch.getPageSize(), Direction.DESC, "createdDate"));
			} else {
				currencySetupList = this.repositoryCurrencySetup
						.predictiveCurrencySetupSearchAndByExpireDate(dtoSearch.getSearchKeyword(),new Date());
			}

			if (currencySetupList != null && !currencySetupList.isEmpty()) {
				List<DtoCurrencySetup> dtoCurrencySetupList = new ArrayList<>();
				for (CurrencySetup currencySetup : currencySetupList) 
				{
					MasterSeperatorDecimal masterSeperatorDecimal = currencySetup.getMasterSeperatorDecimal();
					if(masterSeperatorDecimal!=null){
						masterSeperatorDecimal=repositoryMasterSeperatorDecimal.findBySeperatorDecimalIdAndLanguageLanguageIdAndIsDeleted(masterSeperatorDecimal.getSeperatorDecimalId(),langId,false);
					}
					MasterNegativeSymbolTypes masterNegativeSymbolTypes = currencySetup.getMasterNegativeSymbolTypes();
					if(masterNegativeSymbolTypes!=null){
						masterNegativeSymbolTypes=repositoryMasterNegativeSymbolTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterNegativeSymbolTypes.getTypeId(),langId,false);
					}
					MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes = currencySetup.getMasterNegativeSymbolSignTypes();
					if(masterNegativeSymbolSignTypes!=null){
						masterNegativeSymbolSignTypes=repositoryMasterNegativeSymbolSignTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterNegativeSymbolSignTypes.getTypeId(),langId,false);
					}
					MasterSeperatorThousands masterSeperatorThousands = currencySetup.getMasterSeperatorThousands();
					if(masterSeperatorThousands!=null){
						masterSeperatorThousands=repositoryMasterSeperatorThousands.findBySeperatorThousandIdAndLanguageLanguageIdAndIsDeleted(masterSeperatorThousands.getSeperatorThousandId(),langId,false);
					}
					dtoCurrencySetupList.add(new DtoCurrencySetup(currencySetup,masterSeperatorDecimal,masterNegativeSymbolTypes,masterNegativeSymbolSignTypes,masterSeperatorThousands));
				}
				dtoSearch.setRecords(dtoCurrencySetupList);
			} else {
				dtoSearch.setRecords(null);
			}
		}
		return dtoSearch;
	}
}
