/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.BankSetup;
import com.bti.model.CityMaster;
import com.bti.model.CountryMaster;
import com.bti.model.StateMaster;
import com.bti.model.dto.DtoBankSetUp;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryBankSetup;
import com.bti.repository.RepositoryCityMaster;
import com.bti.repository.RepositoryCountryMaster;
import com.bti.repository.RepositoryStateMaster;

//import com.bti.se

/**
 * Description: Service Authentication
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service
public class ServiceBankSetup {

	static Logger log = Logger.getLogger(ServiceBankSetup.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryCountryMaster repositoryCountryMaster;
	
	@Autowired
	RepositoryCityMaster repositoryCityMaster;
	
	@Autowired
	RepositoryStateMaster repositoryStateMaster;
	
	@Autowired
	RepositoryBankSetup repositoryBankSetup;
	
	@Autowired
	ServiceHome serviceHome;

	/**
	 * @param dtoBankSetUp
	 * @return
	 */
	public boolean saveBankSetup(DtoBankSetUp dtoBankSetUp) {

		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		boolean flag = false;
		BankSetup bankSetup = new BankSetup();

		bankSetup.setBankId(dtoBankSetUp.getBankId());
		bankSetup.setBankDescription(dtoBankSetUp.getBankDescription());
		if (dtoBankSetUp.getBankArabicDescription() != null) {
			bankSetup.setBankDescriptionArabic(dtoBankSetUp.getBankArabicDescription());
		}

		if (dtoBankSetUp.getAddress1() != null) {
			bankSetup.setAddress1(dtoBankSetUp.getAddress1());
		}
		if (dtoBankSetUp.getAddress2() != null) {
			bankSetup.setAddress2(dtoBankSetUp.getAddress2());
		}

		if (dtoBankSetUp.getAddress3() != null) {
			bankSetup.setAddress3(dtoBankSetUp.getAddress3());
		}

		if (dtoBankSetUp.getPhone1() != null) {
			bankSetup.setPhone1(dtoBankSetUp.getPhone1());
		}
		if (dtoBankSetUp.getPhone2() != null) {
			bankSetup.setPhone2(dtoBankSetUp.getPhone2());
		}
		if (dtoBankSetUp.getPhone3() != null) {
			bankSetup.setPhone3(dtoBankSetUp.getPhone3());
		}
		if (dtoBankSetUp.getFax() != null) {
			bankSetup.setFax1(dtoBankSetUp.getFax());
		}

		bankSetup.setCountry(repositoryCountryMaster.findOne(dtoBankSetUp.getCountryId()).getCountryName());

		bankSetup.setState(repositoryStateMaster.findOne(dtoBankSetUp.getStateId()).getStateName());

		bankSetup.setCity(repositoryCityMaster.findOne(dtoBankSetUp.getCityId()).getCityName());

		bankSetup.setCreatedBy(loggedInUserId);
		repositoryBankSetup.saveAndFlush(bankSetup);

		flag = true;

		return flag;
	}

	/**
	 * @param bankSetUpId
	 * @return
	 */
	public DtoBankSetUp bankSetupGetById(String bankSetUpId) {
		
		int langId = serviceHome.getLanngugaeId();
		BankSetup bankSetup=repositoryBankSetup.findByBankIdAndIsDeleted(bankSetUpId,false);
		DtoBankSetUp dtoBankSetUp=null;
		if(bankSetup!=null){
			dtoBankSetUp=new DtoBankSetUp(bankSetup);
			dtoBankSetUp.setCityId(0);
			dtoBankSetUp.setStateId(0);
			dtoBankSetUp.setCountryId(0);
			CountryMaster countryMaster=repositoryCountryMaster.findByCountryNameAndIsDeletedAndIsActive(dtoBankSetUp.getCountryName(),false,true);
			if(countryMaster!=null && countryMaster.getLanguage().getLanguageId()!=langId){
				countryMaster=repositoryCountryMaster.findByCountryCodeAndIsDeletedAndIsActiveAndLanguageLanguageId(countryMaster.getCountryCode(), false, true,langId);
			}
			
			if(countryMaster!=null)
			{
				dtoBankSetUp.setCountryId(countryMaster.getCountryId());
				StateMaster stateMaster=repositoryStateMaster.findByStateNameAndCountryMasterCountryCodeAndIsDeleted(dtoBankSetUp.getStateName(),countryMaster.getCountryCode(),false);
				if(stateMaster!=null && stateMaster.getLanguage().getLanguageId()!=langId){
					stateMaster= repositoryStateMaster.findByStateCodeAndCountryMasterCountryCodeAndLanguageLanguageIdAndIsDeleted(stateMaster.getStateCode(), countryMaster.getCountryCode(),langId,false);
				}
				
				if(stateMaster!= null)
				{
					dtoBankSetUp.setStateId(stateMaster.getStateId());
					CityMaster cityMaster=repositoryCityMaster.findByCityNameAndStateMasterStateCodeAndIsDeleted(dtoBankSetUp.getCityName(),stateMaster.getStateCode(),false);
					if(cityMaster!=null && cityMaster.getLanguage().getLanguageId()!=langId)
					{
						cityMaster=repositoryCityMaster.findByCityCodeAndStateMasterStateCodeAndIsDeleted(cityMaster.getCityCode(),stateMaster.getStateCode(),false);
					}
					if(cityMaster!=null){
						dtoBankSetUp.setCityId(cityMaster.getCityId());
					}
				}
			}
			
			
		}
		return dtoBankSetUp;
	}

	/**
	 * @param dtoBankSetUp
	 * @return
	 */
	public boolean updateBankSetup(DtoBankSetUp dtoBankSetUp) 
	{
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		boolean flag=false;
		BankSetup bankSetup=repositoryBankSetup.findByBankIdAndIsDeleted(dtoBankSetUp.getBankId(),false);
		if(bankSetup!=null){
		bankSetup.setBankId(dtoBankSetUp.getBankId());
		bankSetup.setBankDescription(dtoBankSetUp.getBankDescription());
		
		if(dtoBankSetUp.getBankArabicDescription()!=null){
		bankSetup.setBankDescriptionArabic(dtoBankSetUp.getBankArabicDescription());
		}
		
		if(dtoBankSetUp.getAddress1()!=null){
			bankSetup.setAddress1(dtoBankSetUp.getAddress1());
		}
		
		if(dtoBankSetUp.getAddress2()!=null){
			bankSetup.setAddress2(dtoBankSetUp.getAddress2());
		}
		
		if(dtoBankSetUp.getAddress3()!=null){
			bankSetup.setAddress3(dtoBankSetUp.getAddress3());
		}
		
		if(dtoBankSetUp.getPhone1()!=null){
			bankSetup.setPhone1(dtoBankSetUp.getPhone1());
		}
		
		if(dtoBankSetUp.getPhone2()!=null){
			bankSetup.setPhone2(dtoBankSetUp.getPhone2());
		}
		
		if(dtoBankSetUp.getPhone3()!=null){
			bankSetup.setPhone3(dtoBankSetUp.getPhone3());
		}
		
		if(dtoBankSetUp.getFax()!=null){
			bankSetup.setFax1(dtoBankSetUp.getFax());
		}
		
		bankSetup.setCountry(repositoryCountryMaster.findOne(dtoBankSetUp.getCountryId()).getCountryName());
		bankSetup.setState(repositoryStateMaster.findOne(dtoBankSetUp.getStateId()).getStateName());
		bankSetup.setCity(repositoryCityMaster.findOne(dtoBankSetUp.getCityId()).getCityName());
		bankSetup.setChangeBy(loggedInUserId);
		repositoryBankSetup.saveAndFlush(bankSetup);
		
		flag=true;
		}
		else
		{
			flag=false;
		}
		
		return flag;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchBankSetUp(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch= new DtoSearch();
		int langId = serviceHome.getLanngugaeId();
	    dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
	    dtoSearch.setPageSize(dtoSearchs.getPageSize());
	    dtoSearch.setTotalCount(repositoryBankSetup.predictiveBankSetupSearchCount(dtoSearchs.getSearchKeyword()));
        DtoBankSetUp dtoBankSetUp =null;
        List<BankSetup> bankSetupList=null;
        List<DtoBankSetUp> dtoBankSetUpList = new ArrayList<>();
        if(dtoSearchs.getPageNumber()!=null && dtoSearchs.getPageSize()!= null){
        	Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
        	bankSetupList=repositoryBankSetup.predictiveBankSetupSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
        }
        else
        {
        	bankSetupList=repositoryBankSetup.predictiveBankSetupSearch(dtoSearchs.getSearchKeyword());
        }
        
        if(bankSetupList!=null)
        {
        	for (BankSetup bankSetup : bankSetupList) 
        	{
        		dtoBankSetUp= new DtoBankSetUp(bankSetup);
        		dtoBankSetUp.setCityId(0);
    			dtoBankSetUp.setStateId(0);
    			dtoBankSetUp.setCountryId(0);
    			CountryMaster countryMaster=repositoryCountryMaster.findByCountryNameAndIsDeletedAndIsActive(dtoBankSetUp.getCountryName(),false,true);
    			if(countryMaster!=null && countryMaster.getLanguage().getLanguageId()!=langId){
    				countryMaster=repositoryCountryMaster.findByCountryCodeAndIsDeletedAndIsActiveAndLanguageLanguageId(countryMaster.getCountryCode(), false, true,langId);
    			}
    			
    			if(countryMaster!=null)
    			{
    				dtoBankSetUp.setCountryId(countryMaster.getCountryId());
    				StateMaster stateMaster=repositoryStateMaster.findByStateNameAndCountryMasterCountryCodeAndIsDeleted(dtoBankSetUp.getStateName(),countryMaster.getCountryCode(),false);
    				if(stateMaster!=null && stateMaster.getLanguage().getLanguageId()!=langId){
    					stateMaster= repositoryStateMaster.findByStateCodeAndCountryMasterCountryCodeAndLanguageLanguageIdAndIsDeleted(stateMaster.getStateCode(), countryMaster.getCountryCode(),langId,false);
    				}
    				if(stateMaster!= null)
    				{
    					dtoBankSetUp.setStateId(stateMaster.getStateId());
    					CityMaster cityMaster=repositoryCityMaster.findByCityNameAndStateMasterStateCodeAndIsDeleted(dtoBankSetUp.getCityName(),stateMaster.getStateCode(),false);
    					if(cityMaster!=null && cityMaster.getLanguage().getLanguageId()!=langId)
    					{
    						cityMaster=repositoryCityMaster.findByCityCodeAndStateMasterStateCodeAndIsDeleted(cityMaster.getCityCode(),stateMaster.getStateCode(),false);
    					}
    					if(cityMaster!=null){
    						dtoBankSetUp.setCityId(cityMaster.getCityId());
    					}
    				}
    			}
        		dtoBankSetUpList.add(dtoBankSetUp);
			}
        }
        dtoSearch.setRecords(dtoBankSetUpList);
        return dtoSearch;
	}

}