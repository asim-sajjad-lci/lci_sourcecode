/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.SalesTerritoryMaintenance;
import com.bti.model.dto.DtoSalesTerritorySetup;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositorySalesTerritoryMaintenance;

/**
 * Description: Service Sales Territory Setup
 * Name of Project: BTI
 * Created on: Aug 24, 2017
 * Modified on: Aug 24, 2017 10:26:38 AM
 * @author seasia
 * Version: 
 */
@Service("serviceSalesTerritorySetup")
public class ServiceSalesTerritorySetup {
	
	private static final Logger LOG = Logger.getLogger(ServiceSalesTerritorySetup.class);
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositorySalesTerritoryMaintenance repositorySalesTerritoryMaintenance;

	/**
	 * @param dtoSalesTerritorySetup
	 * @return
	 */
	public DtoSalesTerritorySetup saveSalesTerritorySetup(DtoSalesTerritorySetup dtoSalesTerritorySetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		SalesTerritoryMaintenance salesTerritoryMaintenance = null;
		try {
			salesTerritoryMaintenance = new SalesTerritoryMaintenance();
			
			salesTerritoryMaintenance.setSalesTerritoryId(dtoSalesTerritorySetup.getSalesTerritoryId());
			salesTerritoryMaintenance.setCreatedBy(loggedInUserId);
			salesTerritoryMaintenance.setTerritoryDescription(dtoSalesTerritorySetup.getDescriptionPrimary());
			salesTerritoryMaintenance.setTerritoryDescriptionArabic(dtoSalesTerritorySetup.getDescriptionSecondary());
			salesTerritoryMaintenance.setManagerFirstName(dtoSalesTerritorySetup.getManagerFirstNamePrimary());
			salesTerritoryMaintenance.setManagerFirstNameArabic(dtoSalesTerritorySetup.getManagerFirstNameSecondary());
			salesTerritoryMaintenance.setManagerMidName(dtoSalesTerritorySetup.getManagerMidNamePrimary());
			salesTerritoryMaintenance.setManagerMidNameArabic(dtoSalesTerritorySetup.getManagerMidNameSecondary());
			salesTerritoryMaintenance.setManagerLastName(dtoSalesTerritorySetup.getManagerLastNamePrimary());
			salesTerritoryMaintenance.setManagerLastNameArabic(dtoSalesTerritorySetup.getManagerLastNameSecondary());
			salesTerritoryMaintenance.setTotalCommissionsYTD(dtoSalesTerritorySetup.getTotalCommissionsYTD());
			salesTerritoryMaintenance.setTotalCommissionsLastYear(dtoSalesTerritorySetup.getTotalCommissionsLY());
			salesTerritoryMaintenance.setSalesCommissionsYTD(dtoSalesTerritorySetup.getCommissionsSalesYTD());
			salesTerritoryMaintenance.setSalesCommissionsLastYear(dtoSalesTerritorySetup.getCommissionsSalesLY());
			salesTerritoryMaintenance.setTotalCostSalesYTD(dtoSalesTerritorySetup.getCostOfSalesYTD());
			salesTerritoryMaintenance.setTotalCostSalesLastYear(dtoSalesTerritorySetup.getCostOfSalesLY());
			salesTerritoryMaintenance.setPhone1(dtoSalesTerritorySetup.getPhone1());
			salesTerritoryMaintenance.setPhone2(dtoSalesTerritorySetup.getPhone2());
			
			salesTerritoryMaintenance = repositorySalesTerritoryMaintenance.saveAndFlush(salesTerritoryMaintenance);
		} catch (Exception e) {
			LOG.info(e);
		}
		
		return new DtoSalesTerritorySetup(salesTerritoryMaintenance);
	}

	/**
	 * @param dtoSalesTerritorySetup
	 * @param maintenance
	 * @return
	 */
	public DtoSalesTerritorySetup updateSalesTerritorySetup(DtoSalesTerritorySetup dtoSalesTerritorySetup,
			SalesTerritoryMaintenance maintenance) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		SalesTerritoryMaintenance salesTerritoryMaintenance = null;
		try {
			salesTerritoryMaintenance = maintenance;
			
			salesTerritoryMaintenance.setSalesTerritoryId(dtoSalesTerritorySetup.getSalesTerritoryId());
			salesTerritoryMaintenance.setChangeBy(loggedInUserId);
			salesTerritoryMaintenance.setTerritoryDescription(dtoSalesTerritorySetup.getDescriptionPrimary());
			salesTerritoryMaintenance.setTerritoryDescriptionArabic(dtoSalesTerritorySetup.getDescriptionSecondary());
			salesTerritoryMaintenance.setManagerFirstName(dtoSalesTerritorySetup.getManagerFirstNamePrimary());
			salesTerritoryMaintenance.setManagerFirstNameArabic(dtoSalesTerritorySetup.getManagerFirstNameSecondary());
			salesTerritoryMaintenance.setManagerMidName(dtoSalesTerritorySetup.getManagerMidNamePrimary());
			salesTerritoryMaintenance.setManagerMidNameArabic(dtoSalesTerritorySetup.getManagerMidNameSecondary());
			salesTerritoryMaintenance.setManagerLastName(dtoSalesTerritorySetup.getManagerLastNamePrimary());
			salesTerritoryMaintenance.setManagerLastNameArabic(dtoSalesTerritorySetup.getManagerLastNameSecondary());
			salesTerritoryMaintenance.setTotalCommissionsYTD(dtoSalesTerritorySetup.getTotalCommissionsYTD());
			salesTerritoryMaintenance.setTotalCommissionsLastYear(dtoSalesTerritorySetup.getTotalCommissionsLY());
			salesTerritoryMaintenance.setSalesCommissionsYTD(dtoSalesTerritorySetup.getCommissionsSalesYTD());
			salesTerritoryMaintenance.setSalesCommissionsLastYear(dtoSalesTerritorySetup.getCommissionsSalesLY());
			salesTerritoryMaintenance.setTotalCostSalesYTD(dtoSalesTerritorySetup.getCostOfSalesYTD());
			salesTerritoryMaintenance.setTotalCostSalesLastYear(dtoSalesTerritorySetup.getCostOfSalesLY());
			salesTerritoryMaintenance.setPhone1(dtoSalesTerritorySetup.getPhone1());
			salesTerritoryMaintenance.setPhone2(dtoSalesTerritorySetup.getPhone2());
			salesTerritoryMaintenance = repositorySalesTerritoryMaintenance.saveAndFlush(salesTerritoryMaintenance);
		} catch (Exception e) {
			LOG.info(e);
		}
		
		return new DtoSalesTerritorySetup(salesTerritoryMaintenance);
	}

	/**
	 * @param maintenance
	 * @return
	 */
	public DtoSalesTerritorySetup getSalesTerritorySetupById(SalesTerritoryMaintenance maintenance) {
		return new DtoSalesTerritorySetup(maintenance);
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchSalesTerritorySetup(DtoSearch dtoSearch) {
		List<DtoSalesTerritorySetup> dtoSalesTerritorySetups = new ArrayList<>();
		DtoSalesTerritorySetup dtoSalesTerritorySetup;
		List<SalesTerritoryMaintenance> list;
		try {
			// check if the request is for search or not else return all the
			// records based upon the pagination
			List<SalesTerritoryMaintenance> listSize = repositorySalesTerritoryMaintenance.findByIsDeleted(false);
			if(!listSize.isEmpty()){
				dtoSearch.setTotalCount(listSize.size());
			}
			if (dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null) 
			{
				Pageable pageable  = new PageRequest(dtoSearch.getPageNumber(),
						dtoSearch.getPageSize(), Direction.DESC, "createdDate");
				list = repositorySalesTerritoryMaintenance.predictiveSalesTerritoryMaintenanceSearchWithPagination(
						dtoSearch.getSearchKeyword(), pageable);
			} 
			else 
			{
				list = repositorySalesTerritoryMaintenance.predictiveSalesTerritoryMaintenanceSearch(
						dtoSearch.getSearchKeyword());
			}
			
			if (!list.isEmpty()) {
				for (SalesTerritoryMaintenance salesTerritoryMaintenance : list) {
					dtoSalesTerritorySetup = new DtoSalesTerritorySetup(salesTerritoryMaintenance);
					dtoSalesTerritorySetups.add(dtoSalesTerritorySetup);
				}
			}
			
		} catch (Exception e) {
			LOG.info(e);
		}
		dtoSearch.setRecords(dtoSalesTerritorySetups);
		return dtoSearch ;
	}

}
