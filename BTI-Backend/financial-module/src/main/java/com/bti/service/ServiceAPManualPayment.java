/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.APDistributionTypeConstant;
import com.bti.constant.AuditTrailSeriesTypeConstant;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.GlSerialConfigurationConstant;
import com.bti.constant.MasterAPAccountTypeConstant;
import com.bti.constant.MessageLabel;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.APManualPayment;
import com.bti.model.APManualPaymentDistribution;
import com.bti.model.APYTDOpenPayments;
import com.bti.model.APYTDOpenPaymentsDistribution;
import com.bti.model.BatchTransactionType;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLBatches;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLSerialsConfigurationForTransactions;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.MasterAPManualPaymentDistributionAccountTypes;
import com.bti.model.MasterAPManualPaymentType;
import com.bti.model.VendorAccountTableSetup;
import com.bti.model.VendorMaintenance;
import com.bti.model.dto.DtoAPDistributionDetail;
import com.bti.model.dto.DtoAPManualPayment;
import com.bti.model.dto.DtoAPManualPaymentDistribution;
import com.bti.model.dto.DtoStatusType;
import com.bti.model.dto.DtoTypes;
import com.bti.repository.RepositoryAPBatches;
import com.bti.repository.RepositoryAPManualPayment;
import com.bti.repository.RepositoryAPManualPaymentDistribution;
import com.bti.repository.RepositoryAPYTDOpenPayments;
import com.bti.repository.RepositoryAPYTDOpenPaymentsDistribution;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGLSerialConfigurationForTransaction;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.repository.RepositoryMasterAPManualPaymentDistributionAccountTypes;
import com.bti.repository.RepositoryMasterAPManualPaymentType;
import com.bti.repository.RepositoryVendorAccountTableSetup;
import com.bti.repository.RepositoryVendorMaintenance;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceAPManualPayment Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceAPManualPayment")
public class ServiceAPManualPayment {

	private static final Logger LOG = Logger.getLogger(ServiceAPManualPayment.class);
 
	private final String AP_ACCOUNT_NUMBER_MISSING="AP POST Manual Payment : Account number is missing........ ";
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryAPBatches repositoryAPBatches;
  
	@Autowired
	RepositoryMasterAPManualPaymentType repositoryMasterAPManualPaymentType;

	@Autowired
	RepositoryAPManualPaymentDistribution repositoryAPManualPaymentDistribution;

	@Autowired
	RepositoryAPManualPayment repositoryAPManualPayment;
  
	@Autowired
	RepositoryVendorMaintenance repositoryVendorMaintenance;

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	
	@Autowired
	RepositoryAPYTDOpenPayments repositoryAPYTDOpenPayments;
	
	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;
	
	@Autowired
	RepositoryVendorAccountTableSetup repositoryVendorAccountTableSetup;
	
	@Autowired
	RepositoryAPYTDOpenPaymentsDistribution repositoryAPYTDOpenPaymentsDistribution;
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired
	RepositoryMasterAPManualPaymentDistributionAccountTypes repositoryMasterAPManualPaymentDistributionAccountTypes;
	
	@Autowired
	RepositoryGLSerialConfigurationForTransaction repositoryGLSerialConfigurationForTransaction;
	
	@Autowired
	ServiceGLCashReceiptEntry serviceGLCashReceiptEntry;
	
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;
	
	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;
	
	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;
	
	

 	/**
	 * @description this service will use to get Cash Receipt Types
	 * @return
	 */
	public List<DtoStatusType> getManualPaymentType() {
		List<DtoStatusType> list = new ArrayList<>();
		try {
			int langid = serviceHome.getLanngugaeId();
			List<MasterAPManualPaymentType> masterAPManualPaymentTypeList = repositoryMasterAPManualPaymentType
					.findByIsDeletedAndLanguageLanguageId(false, langid);
			if (masterAPManualPaymentTypeList != null && !masterAPManualPaymentTypeList.isEmpty()) {
				masterAPManualPaymentTypeList.forEach(masterAPManualPaymentType -> list.add(new DtoStatusType(masterAPManualPaymentType)) );
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}
	
	/**
	 * @description this service will use to get AllManualPaymentNumber
	 * @return
	 */
	public List<String> getAllManualPaymentNumber() {
		List<String> manualPaymentNumberList = new ArrayList<>();
		try {
			List<APManualPayment> apManualPaymentList = repositoryAPManualPayment.findAll();

			if(apManualPaymentList!=null && !apManualPaymentList.isEmpty()){
				apManualPaymentList.forEach(apManualPayment -> manualPaymentNumberList.add(apManualPayment.getManualPaymentNumber()));
			}
		 
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return manualPaymentNumberList;
	}
	
	/**
	 * @description this service will use to save Manual Payment Entry
	 * @param dtoAPManualPayment
	 * @return
	 */
	public DtoAPManualPayment saveManualPaymentEntry(DtoAPManualPayment dtoAPManualPayment) {
		try {
			int langid = serviceHome.getLanngugaeId();
			
			String uniqueManualPaymentNumber=serviceGLCashReceiptEntry.getUniqueReceiptNumber();
			APManualPayment apManualPayment = repositoryAPManualPayment
					.findByManualPaymentNumber(uniqueManualPaymentNumber);
			if (apManualPayment == null) {
				apManualPayment = new APManualPayment();
			} else {
				return null;
			}
			apManualPayment.setManualPaymentNumber(uniqueManualPaymentNumber);
			apManualPayment.setManualPaymentDescription(dtoAPManualPayment.getManualPaymentDescription());
			apManualPayment
			.setManualPaymentCreateDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPManualPayment.getManualPaymentCreateDate()));
			
			VendorMaintenance vendorMaintenance=  repositoryVendorMaintenance.findByVendoridAndIsDeleted(dtoAPManualPayment.getVendorID(), false);
			if(vendorMaintenance!=null){
				apManualPayment.setVendorID(vendorMaintenance.getVendorid());
				apManualPayment.setVendorName(vendorMaintenance.getVendorName());
				apManualPayment.setVendorNameArabic(vendorMaintenance.getVendorNameArabic());
				
			}
			apManualPayment.setCurrencyID(dtoAPManualPayment.getCurrencyID());
			apManualPayment.setApBatches(
					repositoryAPBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoAPManualPayment.getBatchID(),BatchTransactionTypeConstant.AP_CASH_RECEIPT.getIndex(), false));
			if (dtoAPManualPayment.getManualPaymentType()!= null) {
				apManualPayment
						.setMasterAPManualPaymentType(repositoryMasterAPManualPaymentType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
								dtoAPManualPayment.getManualPaymentType(), langid, false));
			}
			apManualPayment.setCheckbookID(dtoAPManualPayment.getCheckBookId());
			apManualPayment.setCheckNumber(dtoAPManualPayment.getCheckNumber());
			if(UtilRandomKey.isNotBlank(dtoAPManualPayment.getCreditCardID())){
				apManualPayment.setCreditCardID(dtoAPManualPayment.getCreditCardID());
				apManualPayment.setCreditCardNumber(dtoAPManualPayment.getCreditCardNumber());
				apManualPayment.setCreditCardExpireMonth(dtoAPManualPayment.getCreditCardExpireMonth());
				apManualPayment.setCreditCardExpireYear(dtoAPManualPayment.getCreditCardExpireYear());
			}
			
			apManualPayment.setManualPaymentAmount(dtoAPManualPayment.getManualPaymentAmount());
			apManualPayment.setTransactionVoid(dtoAPManualPayment.getTransactionVoid());
			apManualPayment.setExchangeTableRate(dtoAPManualPayment.getExchangeTableRate()!=null?
					dtoAPManualPayment.getExchangeTableRate():0.0);
			apManualPayment.setExchangeTableIndex(dtoAPManualPayment.getExchangeTableIndex());
			apManualPayment.setRowDateIndex(new Date());
			apManualPayment.setModifyByUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
			
			apManualPayment = repositoryAPManualPayment.saveAndFlush(apManualPayment);
			
			List<GLSerialsConfigurationForTransactions> glSerialsConfigurationForTransactionsList = 
					repositoryGLSerialConfigurationForTransaction.findAll();
			if(glSerialsConfigurationForTransactionsList!=null && !glSerialsConfigurationForTransactionsList.isEmpty())
			{
				GLSerialsConfigurationForTransactions glSerialsConfigurationForTransactions=glSerialsConfigurationForTransactionsList.get(0);
				glSerialsConfigurationForTransactions.setGlSerialNumber(glSerialsConfigurationForTransactions.getGlSerialNumber()+1);
				repositoryGLSerialConfigurationForTransaction.saveAndFlush(glSerialsConfigurationForTransactions);
			}
			
			return new DtoAPManualPayment(apManualPayment);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return null;
		}
	}

	/**
	 * @description this service will use to updateManualPaymentEntry
	 * @param DtoAPManualPayment
	 * @return
	 */
	public DtoAPManualPayment updateManualPaymentEntry(DtoAPManualPayment dtoAPManualPayment) {
		try {
			int langid = serviceHome.getLanngugaeId();
			APManualPayment apManualPayment = repositoryAPManualPayment
					.findByManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
			if (apManualPayment == null) {
				return null;
			}
			apManualPayment.setManualPaymentDescription(dtoAPManualPayment.getManualPaymentDescription());
			apManualPayment
			.setManualPaymentCreateDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPManualPayment.getManualPaymentCreateDate()));
			
			VendorMaintenance vendorMaintenance=  repositoryVendorMaintenance.findByVendoridAndIsDeleted(dtoAPManualPayment.getVendorID(), false);
			if(vendorMaintenance!=null){
				apManualPayment.setVendorID(vendorMaintenance.getVendorid());
				apManualPayment.setVendorName(vendorMaintenance.getVendorName());
				apManualPayment.setVendorNameArabic(vendorMaintenance.getVendorNameArabic());
			}
			apManualPayment.setCurrencyID(dtoAPManualPayment.getCurrencyID());
			apManualPayment.setApBatches(
					repositoryAPBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoAPManualPayment.getBatchID(),BatchTransactionTypeConstant.AP_CASH_RECEIPT.getIndex(), false));
			if (dtoAPManualPayment.getManualPaymentType()!= null) {
				apManualPayment
						.setMasterAPManualPaymentType(repositoryMasterAPManualPaymentType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
								dtoAPManualPayment.getManualPaymentType(), langid, false));
			}
			apManualPayment.setCheckbookID(dtoAPManualPayment.getCheckBookId());
			apManualPayment.setCheckNumber(dtoAPManualPayment.getCheckNumber());
			
			if(UtilRandomKey.isNotBlank(dtoAPManualPayment.getCreditCardID())){
				apManualPayment.setCreditCardID(dtoAPManualPayment.getCreditCardID());
				apManualPayment.setCreditCardNumber(dtoAPManualPayment.getCreditCardNumber());
				apManualPayment.setCreditCardExpireMonth(dtoAPManualPayment.getCreditCardExpireMonth());
				apManualPayment.setCreditCardExpireYear(dtoAPManualPayment.getCreditCardExpireYear());
			}
			apManualPayment.setManualPaymentAmount(dtoAPManualPayment.getManualPaymentAmount());
			apManualPayment.setTransactionVoid(dtoAPManualPayment.getTransactionVoid());
			apManualPayment.setExchangeTableRate(dtoAPManualPayment.getExchangeTableRate()!=null?
					dtoAPManualPayment.getExchangeTableRate():0.0);
			apManualPayment.setExchangeTableIndex(dtoAPManualPayment.getExchangeTableIndex());
			apManualPayment.setRowDateIndex(new Date());
			apManualPayment.setModifyByUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
			apManualPayment = repositoryAPManualPayment.saveAndFlush(apManualPayment);
			return new DtoAPManualPayment(apManualPayment);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return null;
		}
	}

	/**
	 * @description this service will use to get cash receipt entry by receipt
	 *              number
	 * @param DtoAPManualPayment
	 * @return
	 */
	public DtoAPManualPayment getManualPaymentEntryByManualPaymentNumber(DtoAPManualPayment dtoAPManualPayment) {
		try {
			APManualPayment apManualPayment = repositoryAPManualPayment
					.findByManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
			if (apManualPayment != null) {
				return new DtoAPManualPayment(apManualPayment);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}
	
	public boolean deleteManualPaymentEntryByManualPaymentNumber(DtoAPManualPayment dtoAPManualPayment) {
		try{
			APManualPayment apManualPayment = repositoryAPManualPayment
					.findByManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
			if (apManualPayment != null) 
			{
				List<APManualPaymentDistribution> apManualPaymentDistributionList = repositoryAPManualPaymentDistribution
						.findByManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
				if (apManualPaymentDistributionList != null && !apManualPaymentDistributionList.isEmpty()) {
					repositoryAPManualPaymentDistribution.deleteInBatch(apManualPaymentDistributionList);
				}
				repositoryAPManualPayment.delete(apManualPayment);
				return true;
			}
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return false;
	}
	
	public DtoAPManualPayment postManualPaymentEntry(DtoAPManualPayment dtoAPManualPayment) {
		
		int langId=serviceHome.getLanngugaeId();
	    try 
	    {			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
			CurrencyExchangeHeader currencyExchangeHeader= repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(
					Integer.parseInt(dtoAPManualPayment.getExchangeTableIndex()!=null?dtoAPManualPayment.getExchangeTableIndex():"0"), false);
			if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
							.getMethod();
			}
			
			List<APYTDOpenPaymentsDistribution> apytdOpenPaymentDistributionList= new ArrayList<>();
			/*List<APManualPaymentDistribution> manulaPaymentDistributionList = repositoryAPManualPaymentDistribution.findByManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
			if(manulaPaymentDistributionList!=null && !manulaPaymentDistributionList.isEmpty())
			{
				for (APManualPaymentDistribution apManualPaymentDistribution : manulaPaymentDistributionList) 
				{
					GLAccountsTableAccumulation glAccountsTableAccumulation= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(apManualPaymentDistribution.getAccountTableRowIndex()), false);
					if(glAccountsTableAccumulation!=null)
					{
						//Save AR Open Payment Distribution Detail For Debit (ar90401)
						APYTDOpenPaymentsDistribution apytdOpenPaymentsDistributionDebit= new APYTDOpenPaymentsDistribution();
						apytdOpenPaymentsDistributionDebit.setPaymentNumber(apManualPaymentDistribution.getManualPaymentNumber());
						apytdOpenPaymentsDistributionDebit.setAccountTableRowIndex(apManualPaymentDistribution.getAccountTableRowIndex());
						if(apManualPaymentDistribution.getAccountTypes()!=null){
							apytdOpenPaymentsDistributionDebit.setTransactionType(apManualPaymentDistribution.getAccountTypes().getTypeId());
						}
						apytdOpenPaymentsDistributionDebit.setDebitAmount(apManualPaymentDistribution.getDebitAmount());
						apytdOpenPaymentsDistributionDebit.setOriginalDebitAmount(apManualPaymentDistribution.getOriginalDebitAmount());
						apytdOpenPaymentsDistributionDebit.setCreditAmount(apManualPaymentDistribution.getCreditAmount());
						apytdOpenPaymentsDistributionDebit.setOriginalCreditAmount(apManualPaymentDistribution.getOriginalCreditAmount());
						apytdOpenPaymentsDistributionDebit.setDistributionDescription(apManualPaymentDistribution.getDistributionDescription());
						apytdOpenPaymentsDistributionDebit.setRowDateIndex(new Date());
						apytdOpenPaymentDistributionList.add(apytdOpenPaymentsDistributionDebit);
					}
					else
					{
						LOG.error(AP_ACCOUNT_NUMBER_MISSING);
						dtoAPManualPayment.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
						return dtoAPManualPayment;
					}
				}
			}*/
			//else
			//{
				dtoAPManualPayment.setButtonType(MessageLabel.NEW_DISTRIBUTION);
				DtoAPManualPaymentDistribution dtoAPManualPaymentDistribution = getManualPaymentDistributionByManualPaymentNumber(dtoAPManualPayment);
			    if(dtoAPManualPaymentDistribution!=null && dtoAPManualPaymentDistribution.getListDtoAPDistributionDetail()!=null 
			    		&& !dtoAPManualPaymentDistribution.getListDtoAPDistributionDetail().isEmpty())
			    {
			    	for (DtoAPDistributionDetail dtoAPDistributionDetail : dtoAPManualPaymentDistribution.getListDtoAPDistributionDetail()) 
			    	{
			    		if(UtilRandomKey.isNotBlank(dtoAPDistributionDetail.getAccountTableRowIndex()) &&
			    				UtilRandomKey.isNotBlank(dtoAPDistributionDetail.getAccountNumber()))
			    		{
			    			APYTDOpenPaymentsDistribution apytdOpenPaymentsDistributionDebit= new APYTDOpenPaymentsDistribution();
							apytdOpenPaymentsDistributionDebit.setPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
							apytdOpenPaymentsDistributionDebit.setAccountTableRowIndex(Integer.parseInt(dtoAPDistributionDetail.getAccountTableRowIndex()));
							apytdOpenPaymentsDistributionDebit.setTransactionType(dtoAPDistributionDetail.getTypeId());
							Double debitAmount=0.0;
							Double creditAmount=0.0;
							apytdOpenPaymentsDistributionDebit.setOriginalDebitAmount(0.0);
							apytdOpenPaymentsDistributionDebit.setOriginalCreditAmount(0.0);
							
							if(dtoAPDistributionDetail.getDebitAmount()!=null && dtoAPDistributionDetail.getDebitAmount()>0){
								apytdOpenPaymentsDistributionDebit.setOriginalDebitAmount(dtoAPDistributionDetail.getDebitAmount());
								if(dtoAPManualPayment.getExchangeTableRate()!=null && dtoAPManualPayment.getExchangeTableRate()>0){
									debitAmount = (double) mathEval
											.eval(dtoAPDistributionDetail.getDebitAmount() + calcMethod
													+ dtoAPManualPayment.getExchangeTableRate());
								}
							}
							
							if(dtoAPDistributionDetail.getCreditAmount()!=null && dtoAPDistributionDetail.getCreditAmount()>0){
								apytdOpenPaymentsDistributionDebit.setOriginalCreditAmount(dtoAPDistributionDetail.getCreditAmount());
								if(dtoAPManualPayment.getExchangeTableRate()!=null && dtoAPManualPayment.getExchangeTableRate()>0){
									creditAmount = (double) mathEval
											.eval(dtoAPDistributionDetail.getCreditAmount() + calcMethod
													+ dtoAPManualPayment.getExchangeTableRate());
								}
							}
							
							apytdOpenPaymentsDistributionDebit.setDebitAmount(debitAmount);
							apytdOpenPaymentsDistributionDebit.setCreditAmount(creditAmount);
							apytdOpenPaymentsDistributionDebit.setDistributionDescription(dtoAPDistributionDetail.getDistributionReference());
							apytdOpenPaymentsDistributionDebit.setRowDateIndex(new Date());
							apytdOpenPaymentDistributionList.add(apytdOpenPaymentsDistributionDebit);
			    		}
			    		else
			    		{
			    			LOG.error(AP_ACCOUNT_NUMBER_MISSING);
							dtoAPManualPayment.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
							return dtoAPManualPayment;
			    		}
					}
			    }
			//}
			
			if(!apytdOpenPaymentDistributionList.isEmpty()){
				// Save in ar90400
				APYTDOpenPayments apYTDOpenPayments= new APYTDOpenPayments();
				APManualPayment apManualPayment = repositoryAPManualPayment.findByManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
				apYTDOpenPayments.setPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
				// Set Customer Information
				VendorMaintenance vendorMaintenance=  repositoryVendorMaintenance.findByVendoridAndIsDeleted(dtoAPManualPayment.getVendorID(), false);
				if(vendorMaintenance!=null){
					apYTDOpenPayments.setVendorID(vendorMaintenance.getVendorid());
				}
				apYTDOpenPayments.setCheckbookID(dtoAPManualPayment.getCheckBookId());
				apYTDOpenPayments.setManualPaymentType(dtoAPManualPayment.getManualPaymentType());
				apYTDOpenPayments.setCurrencyID(dtoAPManualPayment.getCurrencyID());
				//Set credit card information
				if(UtilRandomKey.isNotBlank(dtoAPManualPayment.getCreditCardID())) {
					apYTDOpenPayments.setCreditCardID(dtoAPManualPayment.getCreditCardID());
					apYTDOpenPayments.setCreditCardNumber(dtoAPManualPayment.getCreditCardNumber());
					apYTDOpenPayments.setCreditCardExpireMonth(String.valueOf(dtoAPManualPayment.getCreditCardExpireMonth()));
					apYTDOpenPayments.setCreditCardExpireYear(String.valueOf(dtoAPManualPayment.getCreditCardExpireYear()));
				}
				//Set Check Information
				if(UtilRandomKey.isNotBlank(dtoAPManualPayment.getCheckNumber())){
					apYTDOpenPayments.setCheckNumber(dtoAPManualPayment.getCheckNumber());
				}
				apYTDOpenPayments.setPaymentDescription(dtoAPManualPayment.getManualPaymentDescription());
				apYTDOpenPayments.setPaymentCreateDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPManualPayment.getManualPaymentCreateDate()));
				if(apManualPayment!=null){
					apYTDOpenPayments.setPaymentCreateByUserID(apManualPayment.getModifyByUserID());
				}
				apYTDOpenPayments.setPaymentPostingDate(new Date());
				apYTDOpenPayments.setPaymentPostingByUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
				apYTDOpenPayments.setOriginalPaymentAmount(dtoAPManualPayment.getManualPaymentAmount());
				Double amount=dtoAPManualPayment.getManualPaymentAmount();
				if(dtoAPManualPayment.getExchangeTableRate()!=null){
					amount = (double) mathEval
							.eval(dtoAPManualPayment.getManualPaymentAmount() + calcMethod
									+ dtoAPManualPayment.getExchangeTableRate());
				}
				
				apYTDOpenPayments.setPaymentAmount(amount);
				//Set Currency Information
				apYTDOpenPayments.setExchangeTableID(dtoAPManualPayment.getExchangeTableIndex());
				apYTDOpenPayments.setExchangeTableRate(dtoAPManualPayment.getExchangeTableRate()!=null?dtoAPManualPayment.getExchangeTableRate():0.0);
				apYTDOpenPayments.setPaymentVoid(false);
				if(dtoAPManualPayment.getTransactionVoid()==1){
					apYTDOpenPayments.setPaymentVoid(true);
				}
				repositoryAPYTDOpenPayments.saveAndFlush(apYTDOpenPayments);
				//Save in ap90401 list of distribution
				repositoryAPYTDOpenPaymentsDistribution.save(apytdOpenPaymentDistributionList);
				//Save New Batch For AP Cash Receipt IN GL Batch
				GLBatches glBatches = createNewGlBatchInPostCashReceipt(dtoAPManualPayment, langId);
				//Save Data in Gl Jv (gl10100 and gl10101)
				saveDataInGLJVEntryInPostCashReceipt(dtoAPManualPayment, apytdOpenPaymentDistributionList, glBatches, mathEval);
				//delete cash receipt entry
				deleteCashReceiptEntryByReceiptNumber(dtoAPManualPayment.getManualPaymentNumber());
				return dtoAPManualPayment;
			}
			else{
				LOG.error(AP_ACCOUNT_NUMBER_MISSING);
				dtoAPManualPayment.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
				return dtoAPManualPayment;
			}
			
		} catch (Exception e) {
			LOG.error(e.getMessage());
			dtoAPManualPayment.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
			return dtoAPManualPayment;
		}
	}
	
	public boolean saveDataInGLJVEntryInPostCashReceipt(DtoAPManualPayment dtoAPManualPayment,
			List<APYTDOpenPaymentsDistribution> apytdOpenPaymentDistributionList, GLBatches glBatches,
			ScriptEngine mathEval) throws ScriptException{
		int nextJournalId = 0;
		GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
			if (glConfigurationSetup == null) {
				return true;
			}
			JournalEntryHeader journalEntryHeader = new JournalEntryHeader();
			nextJournalId = glConfigurationSetup.getNextJournalEntryVoucher();
			journalEntryHeader.setJournalID(String.valueOf(nextJournalId));

			journalEntryHeader.setOriginalTransactionNumberFromModule(dtoAPManualPayment.getManualPaymentNumber());
		journalEntryHeader.setGlBatches(glBatches);
		journalEntryHeader.setTransactionType(CommonConstant.STANDARD);
		
		if (UtilRandomKey.isNotBlank(dtoAPManualPayment.getManualPaymentCreateDate())) {
			journalEntryHeader.setTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoAPManualPayment.getManualPaymentCreateDate()));
		} 

		GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
				.findBySeriesIndexAndIsDeleted(AuditTrailSeriesTypeConstant.AP.getIndex(), false);
		journalEntryHeader.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
		journalEntryHeader.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
		journalEntryHeader.setJournalDescription(dtoAPManualPayment.getManualPaymentDescription());
		journalEntryHeader.setTransactionSource("JV");
		CurrencySetup currencySetup = repositoryCurrencySetup
				.findByCurrencyIdAndIsDeleted(dtoAPManualPayment.getCurrencyID(), false);
		journalEntryHeader.setCurrencySetup(currencySetup);
		journalEntryHeader.setOriginalTotalJournalEntryCredit(dtoAPManualPayment.getManualPaymentAmount());
		journalEntryHeader.setOriginalTotalJournalEntryDebit(dtoAPManualPayment.getManualPaymentAmount());
		
		Double currentExchangeRate = Double.valueOf(dtoAPManualPayment.getExchangeTableRate());
		journalEntryHeader.setExchangeTableRate(currentExchangeRate);
		CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
				.findByExchangeIndexAndIsDeleted(Integer.parseInt(dtoAPManualPayment.getExchangeTableIndex()), false);
		String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
		if (currencyExchangeHeader != null) {
			journalEntryHeader.setCurrencyExchangeHeader(currencyExchangeHeader);
			
				if (UtilRandomKey.isNotBlank(dtoAPManualPayment.getCurrencyID())) {
					if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
						calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
								.getMethod();
					}
						double totalAmount = (double) mathEval
								.eval(Double.valueOf(dtoAPManualPayment.getManualPaymentAmount()) + calcMethod
										+ currentExchangeRate);
						journalEntryHeader.setTotalJournalEntryDebit(totalAmount);
						
						journalEntryHeader.setTotalJournalEntryCredit(totalAmount);

				}
		}

		journalEntryHeader = repositoryJournalEntryHeader.save(journalEntryHeader);
		
			glConfigurationSetup.setNextJournalEntryVoucher(nextJournalId + 1);
			repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);

			//Save In GL JournalEntryDetails
			if(apytdOpenPaymentDistributionList!=null && !apytdOpenPaymentDistributionList.isEmpty()){
				for (APYTDOpenPaymentsDistribution apManualPaymentDistribution : apytdOpenPaymentDistributionList) {
					
					JournalEntryDetails journalEntryDetails = new JournalEntryDetails();
					journalEntryDetails.setAccountTableRowIndex(String.valueOf(apManualPaymentDistribution.getAccountTableRowIndex()));
					
					journalEntryDetails.setDistributionDescription(apManualPaymentDistribution.getDistributionDescription());
					if (apManualPaymentDistribution.getOriginalCreditAmount()>0) {
						journalEntryDetails.setBalanceType(2);
					} else {
						journalEntryDetails.setBalanceType(1);
					}
					
					journalEntryDetails.setOriginalCreditAmount(apManualPaymentDistribution.getOriginalCreditAmount());
					journalEntryDetails.setCreditAmount(apManualPaymentDistribution.getCreditAmount());
					journalEntryDetails.setOriginalDebitAmount(apManualPaymentDistribution.getOriginalDebitAmount());
					journalEntryDetails.setDebitAmount(apManualPaymentDistribution.getDebitAmount());
			
					journalEntryDetails.setJournalEntryHeader(journalEntryHeader);
					journalEntryDetails.setExchangeTableRate(journalEntryHeader.getExchangeTableRate());
					repositoryJournalEntryDetail.save(journalEntryDetails);
				}
			}
			return true;
	}
	
	public GLBatches createNewGlBatchInPostCashReceipt(DtoAPManualPayment dtoAPManualPayment,int langId ){
		
		int year=0;
		int month=0;
		int day=0;
		Calendar cal = Calendar.getInstance();
		if (UtilRandomKey.isNotBlank(dtoAPManualPayment.getManualPaymentCreateDate())) {
			Date transDate = UtilDateAndTime
					.ddmmyyyyStringToDate(dtoAPManualPayment.getManualPaymentCreateDate());
			cal.setTime(transDate);
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH)+1;
			day = cal.get(Calendar.DATE);
		}
		
		String batchId=AuditTrailSeriesTypeConstant.AP+
				GlSerialConfigurationConstant.CASH_RECEIPT.getVaue()+day+month+year;
		String batchDesc=AuditTrailSeriesTypeConstant.AP+" "+
				GlSerialConfigurationConstant.CASH_RECEIPT.getVaue()+" "+dtoAPManualPayment.getManualPaymentNumber();
		
		GLBatches glBatches=repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(batchId, BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex(), false);
		if(glBatches==null){
			glBatches= new GLBatches();
			glBatches.setTotalAmountTransactionInBatch(dtoAPManualPayment.getManualPaymentAmount());
			glBatches.setTotalNumberTransactionInBatch(1);
		}
		else{
			double amount;
			amount=glBatches.getTotalAmountTransactionInBatch()!=null?glBatches.getTotalAmountTransactionInBatch():0.0;
			glBatches.setTotalAmountTransactionInBatch(amount+dtoAPManualPayment.getManualPaymentAmount());
			glBatches.setTotalNumberTransactionInBatch(glBatches.getTotalNumberTransactionInBatch()+1);
		}
		
		glBatches.setBatchID(batchId);
		glBatches.setBatchDescription(batchDesc);
		
		BatchTransactionType batchTransactionType = repositoryBatchTransactionType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex(), langId, false);
		if (batchTransactionType != null) {
			glBatches.setBatchTransactionType(batchTransactionType);
		}
		glBatches.setPostingDate(new Date());
		glBatches = repositoryGLBatches.saveAndFlush(glBatches);
		return glBatches;
	}
	 
	private boolean deleteCashReceiptEntryByReceiptNumber(String manualPaymentNumber) {
		try{
			APManualPayment apManualPayment = repositoryAPManualPayment
					.findByManualPaymentNumber(manualPaymentNumber);
			if (apManualPayment != null) {
				List<APManualPaymentDistribution> apManualPaymentDistributionList = repositoryAPManualPaymentDistribution
						.findByManualPaymentNumber(manualPaymentNumber);
				if (apManualPaymentDistributionList != null && !apManualPaymentDistributionList.isEmpty()) {
					repositoryAPManualPaymentDistribution.deleteInBatch(apManualPaymentDistributionList);
				}
				repositoryAPManualPayment.delete(apManualPayment);
			}
			return true;
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}

	/**
	 * @description this service will use to saveManualPaymentDistribution
	 * @param dtoAPManualPaymentDistribution
	 * @return
	 */
	public DtoAPManualPaymentDistribution saveManualPaymentDistribution(
			DtoAPManualPaymentDistribution dtoAPManualPaymentDistribution) {
		int langId = serviceHome.getLanngugaeId();
		try {
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			if(dtoAPManualPaymentDistribution.getListDtoAPDistributionDetail()!=null && !dtoAPManualPaymentDistribution.getListDtoAPDistributionDetail().isEmpty()){
				List<APManualPaymentDistribution> apManualPaymentDistributionList = repositoryAPManualPaymentDistribution
						.findByManualPaymentNumber(dtoAPManualPaymentDistribution.getManualPaymentNumber());
				if (apManualPaymentDistributionList != null && !apManualPaymentDistributionList.isEmpty()) {
					repositoryAPManualPaymentDistribution.deleteInBatch(apManualPaymentDistributionList);
				}

				for (DtoAPDistributionDetail distributionDetail : dtoAPManualPaymentDistribution.getListDtoAPDistributionDetail()) 
				{
					APManualPaymentDistribution apManualPaymentDistribution = new APManualPaymentDistribution();
					if(UtilRandomKey.isNotBlank(distributionDetail.getAccountTableRowIndex())){
						apManualPaymentDistribution.setAccountTableRowIndex(Integer.parseInt(distributionDetail.getAccountTableRowIndex()));
					}
					apManualPaymentDistribution.setManualPaymentNumber(dtoAPManualPaymentDistribution.getManualPaymentNumber());
					apManualPaymentDistribution.setAccountTypes(repositoryMasterAPManualPaymentDistributionAccountTypes.findByLanguageLanguageIdAndIsDeletedAndTypeId(langId, false,distributionDetail.getTypeId()));
					Double amount=0.0;
					Double exchangeTableRate=null;
					String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
					APManualPayment apManualPayment= repositoryAPManualPayment.findByManualPaymentNumber(dtoAPManualPaymentDistribution.getManualPaymentNumber());
					if(apManualPayment!=null && UtilRandomKey.isNotBlank(apManualPayment.getExchangeTableIndex()))
					{
						   CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(apManualPayment.getExchangeTableIndex()), false);
						   if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
							   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
						   }
						  exchangeTableRate=apManualPayment.getExchangeTableRate();
					}
					
					if(distributionDetail.getDebitAmount()!=null && distributionDetail.getDebitAmount()>0){
						amount=distributionDetail.getDebitAmount();
						apManualPaymentDistribution.setOriginalDebitAmount(amount);
						if(exchangeTableRate!=null && exchangeTableRate>0.0){
							amount= (double) mathEval.eval(distributionDetail.getDebitAmount() + calcMethod +exchangeTableRate );
						}
						apManualPaymentDistribution.setDebitAmount(amount);
					}
					
					if(distributionDetail.getCreditAmount()!=null && distributionDetail.getCreditAmount()>0){
						amount=distributionDetail.getCreditAmount();
						apManualPaymentDistribution.setOriginalCreditAmount(amount);
						if(exchangeTableRate!=null){
							amount= (double) mathEval.eval(distributionDetail.getCreditAmount() + calcMethod +exchangeTableRate );
						}
						apManualPaymentDistribution.setCreditAmount(amount);
					}
					apManualPaymentDistribution.setDistributionDescription(distributionDetail.getDistributionReference());
					apManualPaymentDistribution.setRowDateIndex(new Date());
					repositoryAPManualPaymentDistribution.save(apManualPaymentDistribution);
				}
			}
			return dtoAPManualPaymentDistribution;
			
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	/**
	 * @description this service will use to getManualPaymentDistribution by
	 *              ManualPaymentNumber
	 * @param DtoAPManualPaymentDistribution
	 * @return
	 */
	public DtoAPManualPaymentDistribution getManualPaymentDistributionByManualPaymentNumber(DtoAPManualPayment dtoAPManualPayment) {

		int langId = serviceHome.getLanngugaeId();
		try 
		{
			List<DtoAPDistributionDetail> distributionDetailList= new ArrayList<>();
			/*APManualPayment apManualPayment = repositoryAPManualPayment
					.findByManualPaymentNumber(dtoAPManualPaymentDistribution.getManualPaymentNumber());
			if (apManualPayment != null) 
			{ */
				DtoAPManualPaymentDistribution distribution = new DtoAPManualPaymentDistribution();
				VendorMaintenance vendorMaintenance= repositoryVendorMaintenance.findByVendoridAndIsDeleted(dtoAPManualPayment.getVendorID(), false);
				if(vendorMaintenance!=null){
					distribution.setVendorId(vendorMaintenance.getVendorid()!=null?vendorMaintenance.getVendorid():"");
					distribution.setVendorName(vendorMaintenance.getVendorName()!=null?vendorMaintenance.getVendorName():"");	
				}
				distribution.setCurrencyID(dtoAPManualPayment.getCurrencyID());
				distribution.setManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
				
				MasterAPManualPaymentType manualPaymentType= repositoryMasterAPManualPaymentType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
						dtoAPManualPayment.getManualPaymentType(), langId, false);
				if(manualPaymentType!=null && manualPaymentType.getLanguage().getLanguageId()!=langId){
					manualPaymentType=repositoryMasterAPManualPaymentType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(manualPaymentType.getTypeId(), langId, false);
				}
				
				distribution.setTransactionType(manualPaymentType!=null?manualPaymentType.getValue():"");
				distribution.setOriginalAmount(dtoAPManualPayment.getManualPaymentAmount());
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double amount  = dtoAPManualPayment.getManualPaymentAmount();
				String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
				if(UtilRandomKey.isNotBlank(dtoAPManualPayment.getExchangeTableIndex())){
					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(dtoAPManualPayment.getExchangeTableIndex()), false);
				   if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
				   }
				}
				
				Double functionalAmount = (double) mathEval.eval(amount + calcMethod + dtoAPManualPayment.getExchangeTableRate());
				distribution.setFunctionalAmount(functionalAmount);
				
				List<APManualPaymentDistribution> manulaPaymentDistributionList =null;
				if(dtoAPManualPayment.getButtonType().equalsIgnoreCase(MessageLabel.NEW_DISTRIBUTION))
				{
					manulaPaymentDistributionList= repositoryAPManualPaymentDistribution.findByManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
					for (APManualPaymentDistribution apManualPaymentDistribution : manulaPaymentDistributionList) 
					{
						DtoAPDistributionDetail distributionDetail= new DtoAPDistributionDetail();
						distributionDetail.setAccountNumber("");
						distributionDetail.setAccountDescription("");
						distributionDetail.setAccountTableRowIndex("");
						GLAccountsTableAccumulation glAccountsTableAccumulation=null;
						if(apManualPaymentDistribution.getAccountTableRowIndex()>0){
							glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(apManualPaymentDistribution.getAccountTableRowIndex()), false);
						}
						
						distributionDetail.setDistributionReference("");
						if(UtilRandomKey.isNotBlank(apManualPaymentDistribution.getDistributionDescription())){
							distributionDetail.setDistributionReference(apManualPaymentDistribution.getDistributionDescription());
						}
						
						distributionDetail.setType("");
						distributionDetail.setTypeId(0);
						if(apManualPaymentDistribution.getAccountTypes()!=null){
							distributionDetail.setType(APDistributionTypeConstant.getById(apManualPaymentDistribution.getAccountTypes().getTypeId()).name());
							distributionDetail.setTypeId(apManualPaymentDistribution.getAccountTypes().getTypeId());
						}
						
						distributionDetail.setDebitAmount(0.0);
						if(apManualPaymentDistribution.getOriginalDebitAmount()>0){
							distributionDetail.setDebitAmount(apManualPaymentDistribution.getOriginalDebitAmount());
						}
						
						distributionDetail.setCreditAmount(0.0);
						if(apManualPaymentDistribution.getOriginalCreditAmount()>0){
							distributionDetail.setCreditAmount(apManualPaymentDistribution.getOriginalCreditAmount());
						}
						
						if(glAccountsTableAccumulation!=null){
							distributionDetail.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
							Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
							distributionDetail.setAccountNumber(object[0].toString());
							distributionDetail.setAccountDescription("");
							distributionDetail.setAccountDescription(object[3].toString());
						}
						distributionDetailList.add(distributionDetail);
					}
				}
				
				if(dtoAPManualPayment.getButtonType().equalsIgnoreCase(MessageLabel.DEFAULT_DISTRIBUTION) || manulaPaymentDistributionList==null)
				{
					DtoAPDistributionDetail distributionDetailDebit= new DtoAPDistributionDetail();
					//Set CheckBook Account Number Information
					CheckbookMaintenance checkbookMaintenance= repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoAPManualPayment.getCheckBookId(), false);
					GLAccountsTableAccumulation glAccountsTableAccumulation=null;
					if(checkbookMaintenance!=null){
						glAccountsTableAccumulation= checkbookMaintenance.getGlAccountsTableAccumulation();
					}
					distributionDetailDebit.setAccountNumber("");
					distributionDetailDebit.setAccountDescription("");
					distributionDetailDebit.setAccountTableRowIndex("");
					distributionDetailDebit.setDistributionReference("");
					distributionDetailDebit.setType(APDistributionTypeConstant.CASH.name());
					distributionDetailDebit.setTypeId(APDistributionTypeConstant.CASH.getIndex());
					distributionDetailDebit.setDebitAmount(amount);
					distributionDetailDebit.setCreditAmount(0.0);
					if(glAccountsTableAccumulation!=null){
						distributionDetailDebit.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
						Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
						distributionDetailDebit.setAccountNumber(object[0].toString());
						distributionDetailDebit.setAccountDescription(object[3].toString());
					}
					distributionDetailList.add(distributionDetailDebit);
					//Set Account Payable Options Setup
					DtoAPDistributionDetail distributionDetailCredit= new DtoAPDistributionDetail();
					VendorAccountTableSetup vendorAccountTableSetup= repositoryVendorAccountTableSetup.findByMasterAccountTypeTypeIdAndVendorMaintenanceVendoridAndIsDeleted(MasterAPAccountTypeConstant.ACCOUNT_PAYABLE.getIndex(), dtoAPManualPayment.getVendorID(), false);
					if(vendorAccountTableSetup!=null){
						glAccountsTableAccumulation=vendorAccountTableSetup.getGlAccountsTableAccumulation();
					}
					
					distributionDetailCredit.setAccountNumber("");
					distributionDetailCredit.setAccountDescription("");
					distributionDetailCredit.setAccountTableRowIndex("");
					distributionDetailCredit.setDistributionReference("");
					distributionDetailCredit.setType(APDistributionTypeConstant.PAYP.name());
					distributionDetailCredit.setTypeId(APDistributionTypeConstant.PAYP.getIndex());
					distributionDetailCredit.setCreditAmount(amount);
					distributionDetailCredit.setDebitAmount(0.0);
					
					if(glAccountsTableAccumulation!=null){
						distributionDetailCredit.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
						Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
						distributionDetailCredit.setAccountNumber(object[0].toString());
						distributionDetailCredit.setAccountDescription(object[3].toString());
					}
					distributionDetailList.add(distributionDetailCredit);
				}
				distribution.setListDtoAPDistributionDetail(distributionDetailList);
				return distribution;
			//}
		} 
		catch (Exception e) 
		{
			LOG.error(e.getMessage());
		}
		return null;
	}

	public List<DtoTypes> getDistributionAccountTypesList() {
		   int langId = serviceHome.getLanngugaeId();
		   List<DtoTypes> typesList = new ArrayList<>();
		   List<MasterAPManualPaymentDistributionAccountTypes> accountTypes= repositoryMasterAPManualPaymentDistributionAccountTypes.findByLanguageLanguageIdAndIsDeleted(langId,false);
		   if(accountTypes!=null && !accountTypes.isEmpty())
		   {
			   for (MasterAPManualPaymentDistributionAccountTypes masterAPManualPaymentDistributionAccountTypes : accountTypes) 
			   {
				   DtoTypes dtoTypes= new DtoTypes();
				   dtoTypes.setTypeCode(masterAPManualPaymentDistributionAccountTypes.getTypeCode());
				   dtoTypes.setTypeValue(masterAPManualPaymentDistributionAccountTypes.getTypeDescription());
				   dtoTypes.setTypeId(masterAPManualPaymentDistributionAccountTypes.getTypeId());
				   typesList.add(dtoTypes);
			    }
		   }
		return typesList;
	}
	
	public boolean deleteDistributionManualPaymentEntry(DtoAPManualPayment dtoAPManualPayment) 
	{
		try
		{
				List<APManualPaymentDistribution> apManualPaymentDistributionList = repositoryAPManualPaymentDistribution
						.findByManualPaymentNumber(dtoAPManualPayment.getManualPaymentNumber());
				if (apManualPaymentDistributionList != null && !apManualPaymentDistributionList.isEmpty()) {
					repositoryAPManualPaymentDistribution.deleteInBatch(apManualPaymentDistributionList);
					return true;
				}
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return false;
	}

	public boolean checkDistributionDetailIsAvailable(String manualPaymentNumber) {
		List<APManualPaymentDistribution> apManualPaymentDistributionList = repositoryAPManualPaymentDistribution
				.findByManualPaymentNumber(manualPaymentNumber);
		return apManualPaymentDistributionList != null && !apManualPaymentDistributionList.isEmpty()? true:false;
	}
}
