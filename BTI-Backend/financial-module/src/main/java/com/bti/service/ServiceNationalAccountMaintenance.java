/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.NationalAccountMaintenanceDetails;
import com.bti.model.NationalAccountMaintenanceHeader;
import com.bti.model.dto.DtoNationalAccountMaintenanceDetail;
import com.bti.model.dto.DtoNationalAccountMaintenanceHeader;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryNationalAccountMaintenanceDetails;
import com.bti.repository.RepositoryNationalAccountMaintenanceHeader;

@Service("serviceNationalAccountMaintenance")
public class ServiceNationalAccountMaintenance {
	
	private static final Logger LOG = Logger.getLogger(ServiceNationalAccountMaintenance.class);
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryNationalAccountMaintenanceHeader repositoryNationalAccountMaintenanceHeader;
	
	@Autowired
	RepositoryNationalAccountMaintenanceDetails repositoryNationalAccountMaintenanceDetails;
	
	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;

	/**
	 * @param dtoAccountMaintenanceHeader
	 * @return
	 */
	public DtoNationalAccountMaintenanceHeader saveNationalAccountHeader(
			DtoNationalAccountMaintenanceHeader dtoAccountMaintenanceHeader) {
		NationalAccountMaintenanceHeader accountMaintenanceHeader = null;
		try {
			accountMaintenanceHeader = new NationalAccountMaintenanceHeader();
			accountMaintenanceHeader.setCustomerMaintenance(repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(dtoAccountMaintenanceHeader.getCustNumber(),false));
			accountMaintenanceHeader.setCustomerName(dtoAccountMaintenanceHeader.getCustNamePrimary());
			accountMaintenanceHeader.setCustomerNameArabic(dtoAccountMaintenanceHeader.getCustNameSecondary());
			accountMaintenanceHeader.setAllowReceiptsEntrChildrenNationalAccount(dtoAccountMaintenanceHeader.getAllowReceiptEntry());
			accountMaintenanceHeader.setBaseCreditCheckConsolidatedNationalAccount(dtoAccountMaintenanceHeader.getBaseCreditCheck());
			accountMaintenanceHeader.setApplyHoldOrInactiveStatusParentAcrossNationalAccount(dtoAccountMaintenanceHeader.getApplyStatus());
			accountMaintenanceHeader.setBaseFinanceChargConsolidatedNationalAccount(dtoAccountMaintenanceHeader.getBaseFinanceCharge());
			
			accountMaintenanceHeader = repositoryNationalAccountMaintenanceHeader.saveAndFlush(accountMaintenanceHeader);
			
			if(!dtoAccountMaintenanceHeader.getAccountMaintenanceDetail().isEmpty()){
				NationalAccountMaintenanceDetails accountMaintenanceDetails = null;
				for (DtoNationalAccountMaintenanceDetail accountMaintenanceDetail : dtoAccountMaintenanceHeader.getAccountMaintenanceDetail()) {
					accountMaintenanceDetails = new NationalAccountMaintenanceDetails();
					
					accountMaintenanceDetails.setAccountMaintenanceHeader(accountMaintenanceHeader);
					accountMaintenanceDetails.setCustomerMaintenance(repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(accountMaintenanceDetail.getChildCustId(),false));
					accountMaintenanceDetails.setCustomerNameChildren(accountMaintenanceDetail.getChildCustNamePrimary());
					accountMaintenanceDetails.setCustomerNameChildrenArabic(accountMaintenanceDetail.getChilsCustNameSecondary());
					accountMaintenanceDetails.setCustomerChildrenBalance(accountMaintenanceDetail.getCustChildBalance());
					
					repositoryNationalAccountMaintenanceDetails.saveAndFlush(accountMaintenanceDetails);
				}
				
			}
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		
		return dtoAccountMaintenanceHeader;
	}

	/**
	 * @param dtoAccountMaintenanceHeader
	 * @param accountMaintenanceHeader
	 * @return
	 */
	public DtoNationalAccountMaintenanceHeader updateNationalAccountHeader(
			DtoNationalAccountMaintenanceHeader dtoAccountMaintenanceHeader,
			NationalAccountMaintenanceHeader accountMaintenanceHeader) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try {
			accountMaintenanceHeader.setCustomerMaintenance(repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(dtoAccountMaintenanceHeader.getCustNumber(),false));
			accountMaintenanceHeader.setCustomerName(dtoAccountMaintenanceHeader.getCustNamePrimary());
			accountMaintenanceHeader.setCustomerNameArabic(dtoAccountMaintenanceHeader.getCustNameSecondary());
			accountMaintenanceHeader.setAllowReceiptsEntrChildrenNationalAccount(dtoAccountMaintenanceHeader.getAllowReceiptEntry());
			accountMaintenanceHeader.setBaseCreditCheckConsolidatedNationalAccount(dtoAccountMaintenanceHeader.getBaseCreditCheck());
			accountMaintenanceHeader.setApplyHoldOrInactiveStatusParentAcrossNationalAccount(dtoAccountMaintenanceHeader.getApplyStatus());
			accountMaintenanceHeader.setBaseFinanceChargConsolidatedNationalAccount(dtoAccountMaintenanceHeader.getBaseFinanceCharge());
			accountMaintenanceHeader.setChangeBy(loggedInUserId);
			accountMaintenanceHeader = repositoryNationalAccountMaintenanceHeader.saveAndFlush(accountMaintenanceHeader);
			if(accountMaintenanceHeader!=null)
			{
				List<NationalAccountMaintenanceDetails> entity = repositoryNationalAccountMaintenanceDetails.findByAccountMaintenanceHeaderIdAndIsDeleted(accountMaintenanceHeader.getId(),false);
				if(entity!=null && !entity.isEmpty()){
					repositoryNationalAccountMaintenanceDetails.deleteInBatch(entity);
				}
			}
			
			
			
			if(!dtoAccountMaintenanceHeader.getAccountMaintenanceDetail().isEmpty()){
				NationalAccountMaintenanceDetails accountMaintenanceDetails = null;
				for (DtoNationalAccountMaintenanceDetail accountMaintenanceDetail : dtoAccountMaintenanceHeader.getAccountMaintenanceDetail()) {
					accountMaintenanceDetails = new NationalAccountMaintenanceDetails();
					
					accountMaintenanceDetails.setAccountMaintenanceHeader(accountMaintenanceHeader);
					accountMaintenanceDetails.setCustomerMaintenance(repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(accountMaintenanceDetail.getChildCustId(),false));
					accountMaintenanceDetails.setCustomerNameChildren(accountMaintenanceDetail.getChildCustNamePrimary());
					accountMaintenanceDetails.setCustomerNameChildrenArabic(accountMaintenanceDetail.getChilsCustNameSecondary());
					accountMaintenanceDetails.setCustomerChildrenBalance(accountMaintenanceDetail.getCustChildBalance());
					repositoryNationalAccountMaintenanceDetails.saveAndFlush(accountMaintenanceDetails);
				}
				
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}

		
		return dtoAccountMaintenanceHeader;
	}

	/**
	 * @param dtoAccountMaintenanceHeader
	 * @param accountMaintenanceHeader2
	 * @return
	 */
	public DtoNationalAccountMaintenanceHeader getNationalAccountHeaderById(
			DtoNationalAccountMaintenanceHeader dtoAccountMaintenanceHeader,
			NationalAccountMaintenanceHeader accountMaintenanceHeader2) {
		try {
			List<DtoNationalAccountMaintenanceDetail> dtoNationalAccountMaintenanceDetails;
			List<NationalAccountMaintenanceDetails> nationalAccountMaintenanceDetails;
			List<DtoNationalAccountMaintenanceDetail> dtoNationalAccountMaintenanceDetails2 = new ArrayList<>();
			DtoNationalAccountMaintenanceDetail dtoNationalAccountMaintenanceDetail;
			
			if(accountMaintenanceHeader2 != null){
				dtoAccountMaintenanceHeader = new DtoNationalAccountMaintenanceHeader(accountMaintenanceHeader2);
				
				nationalAccountMaintenanceDetails = repositoryNationalAccountMaintenanceDetails.findByAccountMaintenanceHeaderIdAndIsDeleted(accountMaintenanceHeader2.getId(),false);
				if(nationalAccountMaintenanceDetails!=null && !nationalAccountMaintenanceDetails.isEmpty())
				{
					for(NationalAccountMaintenanceDetails nationalAccountMaintenanceDetails1 : nationalAccountMaintenanceDetails)
					{
						dtoNationalAccountMaintenanceDetail = new DtoNationalAccountMaintenanceDetail(nationalAccountMaintenanceDetails1);
						dtoNationalAccountMaintenanceDetails2.add(dtoNationalAccountMaintenanceDetail);
					}
				}
			}
			dtoNationalAccountMaintenanceDetails = dtoNationalAccountMaintenanceDetails2;
			dtoAccountMaintenanceHeader.setAccountMaintenanceDetail(dtoNationalAccountMaintenanceDetails);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		return dtoAccountMaintenanceHeader;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchNationalAccountHeader(DtoSearch dtoSearch) {
		List<DtoNationalAccountMaintenanceHeader> dtoNationalAccountMaintenanceHeaders = new ArrayList<>();
		DtoNationalAccountMaintenanceHeader dtoNationalAccountMaintenanceHeader;
		List<NationalAccountMaintenanceHeader> list;
		List<NationalAccountMaintenanceDetails> list2;
		List<DtoNationalAccountMaintenanceDetail> accountMaintenanceDetails;
		DtoNationalAccountMaintenanceDetail accountMaintenanceDetail;
		try {
			// check if the request is for search or not else return all the
			// records based upon the pagination
			int listSize = repositoryNationalAccountMaintenanceHeader.getAllNationalAccount();
				dtoSearch.setTotalCount(listSize);
			if (dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null) 
			{
				Pageable pageable  = new PageRequest(dtoSearch.getPageNumber(),
						dtoSearch.getPageSize(), Direction.DESC, "createdDate");
				list = repositoryNationalAccountMaintenanceHeader.predictiveNationalAccountMaintenanceSearchWithPagination(
						dtoSearch.getSearchKeyword(), pageable);
			} else 
			{
				list = repositoryNationalAccountMaintenanceHeader.predictiveNationalAccountMaintenanceSearch(
						dtoSearch.getSearchKeyword());
			}
			if (!list.isEmpty()) {
				for (NationalAccountMaintenanceHeader accountMaintenanceHeader : list) {
					accountMaintenanceDetails = new ArrayList<>();
					dtoNationalAccountMaintenanceHeader = new DtoNationalAccountMaintenanceHeader(accountMaintenanceHeader);
					dtoNationalAccountMaintenanceHeaders.add(dtoNationalAccountMaintenanceHeader);
					
					list2 = repositoryNationalAccountMaintenanceDetails.findByAccountMaintenanceHeaderIdAndIsDeleted(accountMaintenanceHeader.getId(),false);
					if(list2!=null && !list2.isEmpty()){
						for(NationalAccountMaintenanceDetails accountMaintenanceDetails2 : list2){
							accountMaintenanceDetail = new DtoNationalAccountMaintenanceDetail(accountMaintenanceDetails2);
							
							accountMaintenanceDetails.add(accountMaintenanceDetail);
							
							dtoNationalAccountMaintenanceHeader.setAccountMaintenanceDetail(accountMaintenanceDetails);
						}
					}
					
				}
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		dtoSearch.setRecords(dtoNationalAccountMaintenanceHeaders);
		return dtoSearch ;
	}
}
