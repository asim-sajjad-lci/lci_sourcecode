/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.CurrencyExchangeDetail;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.MasterRateFrequencyTypes;
import com.bti.model.dto.DtoCurrencyExchangeDetail;
import com.bti.model.dto.DtoCurrencyExchangeTableSetupHeader;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCurrencyExchangeDetail;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryMasterRateFrequencyTypes;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: Service Exchange Table Setup
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service("serviceCurrencyExchange")
public class ServiceCurrencyExchange {
	
	static Logger log = Logger.getLogger(ServiceCurrencyExchange.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	RepositoryCurrencyExchangeDetail repositoryCurrencyExchangeDetail;
	
	@Autowired
	RepositoryMasterRateFrequencyTypes repositoryMasterRateFrequencyTypes;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @param dtoExchangeTableSetup
	 * @return
	 */
	public Boolean saveCurrencyExchangeHeaderSetUp(DtoCurrencyExchangeTableSetupHeader dtoExchangeTableSetup){
		String userid = httpServletRequest.getHeader(CommonConstant.USER_ID);
		int langid = serviceHome.getLanngugaeId();
		int createdBy=0;
		if(userid!=null){
			createdBy=Integer.parseInt(userid);
		}
		if(dtoExchangeTableSetup != null){
			CurrencyExchangeHeader currencyExchangeHeader = new CurrencyExchangeHeader();
			currencyExchangeHeader.setExchangeId(dtoExchangeTableSetup.getExchangeId());
			currencyExchangeHeader.setExchangeDescription(dtoExchangeTableSetup.getDescription());
			currencyExchangeHeader.setExchangeDescriptionArabic(dtoExchangeTableSetup.getDescriptionArabic());
			currencyExchangeHeader.setExchangeRateSource(dtoExchangeTableSetup.getExchangeRateSource());
			currencyExchangeHeader.setExchangeRateSourceArabic(dtoExchangeTableSetup.getExchangeRateSourceArabic());
			currencyExchangeHeader.setMasterRateFrequencyTypes(repositoryMasterRateFrequencyTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoExchangeTableSetup.getRateFrequency(),langid,false));
			if(UtilRandomKey.isNotBlank(dtoExchangeTableSetup.getRateVariance())){
				currencyExchangeHeader.setRateVariance(Double.valueOf(dtoExchangeTableSetup.getRateVariance()));
			}
			currencyExchangeHeader.setCreatedBy(createdBy);
			currencyExchangeHeader.setRateCalcMethod(dtoExchangeTableSetup.getRateCalcMethod());
			currencyExchangeHeader.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoExchangeTableSetup.getCurrencyId(),false));
			currencyExchangeHeader=repositoryCurrencyExchangeHeader.save(currencyExchangeHeader);
			if(currencyExchangeHeader!=null){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param dtoExchangeTableSetups
	 * @return
	 */
	public DtoCurrencyExchangeTableSetupHeader getCurrencyExchateSetupByExchangeId(DtoCurrencyExchangeTableSetupHeader dtoExchangeTableSetups){
		DtoCurrencyExchangeTableSetupHeader dtoCurrencyExchangeTableSetupHeader=null;
		int langId = serviceHome.getLanngugaeId();
		CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIdAndIsDeleted(dtoExchangeTableSetups.getExchangeId(),false);
		if(currencyExchangeHeader!=null)
		{
			MasterRateFrequencyTypes masterRateFrequencyTypes = currencyExchangeHeader.getMasterRateFrequencyTypes();
			if(masterRateFrequencyTypes!=null){
				masterRateFrequencyTypes = repositoryMasterRateFrequencyTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterRateFrequencyTypes.getTypeId(),langId,false);
			}
			dtoCurrencyExchangeTableSetupHeader= new DtoCurrencyExchangeTableSetupHeader(currencyExchangeHeader,masterRateFrequencyTypes);
		}
		return dtoCurrencyExchangeTableSetupHeader;
	}
	
	/**
	 * @param dtoExchangeTableSetup
	 * @return
	 */
	public Boolean updateExchangeTableSetUp(DtoCurrencyExchangeTableSetupHeader dtoExchangeTableSetup){
		String userid = httpServletRequest.getHeader(CommonConstant.USER_ID);
		int langid = serviceHome.getLanngugaeId();
		int changeBy=0;
		if(UtilRandomKey.isNotBlank(userid)){
			changeBy=Integer.parseInt(userid);
		}
		
		if(dtoExchangeTableSetup!=null)
		{
			CurrencyExchangeHeader currencyExchangeHeader =repositoryCurrencyExchangeHeader.findByExchangeIdAndIsDeleted(dtoExchangeTableSetup.getExchangeId(),false);
			if(currencyExchangeHeader != null)
			{
				currencyExchangeHeader.setExchangeDescription(dtoExchangeTableSetup.getDescription());
				currencyExchangeHeader.setExchangeDescriptionArabic(dtoExchangeTableSetup.getDescriptionArabic());
				currencyExchangeHeader.setExchangeRateSource(dtoExchangeTableSetup.getExchangeRateSource());
				currencyExchangeHeader.setExchangeRateSourceArabic(dtoExchangeTableSetup.getExchangeRateSourceArabic());
				currencyExchangeHeader.setMasterRateFrequencyTypes(repositoryMasterRateFrequencyTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoExchangeTableSetup.getRateFrequency(),langid,false));
				if(UtilRandomKey.isNotBlank(dtoExchangeTableSetup.getRateVariance())){
					currencyExchangeHeader.setRateVariance(Double.valueOf(dtoExchangeTableSetup.getRateVariance()));
				}
				
				currencyExchangeHeader.setModifyDate(new Date());
				currencyExchangeHeader.setChangeBy(changeBy);
				currencyExchangeHeader.setRateCalcMethod(dtoExchangeTableSetup.getRateCalcMethod());
				currencyExchangeHeader.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoExchangeTableSetup.getCurrencyId(),false));
				currencyExchangeHeader=repositoryCurrencyExchangeHeader.saveAndFlush(currencyExchangeHeader);
				if(currencyExchangeHeader!=null){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchCurrencyExchangeHeaderSetups(DtoSearch dtoSearch){
		if(dtoSearch != null){
			int langId = serviceHome.getLanngugaeId();
			dtoSearch.setTotalCount(this.repositoryCurrencyExchangeHeader.predictiveSearchCount(dtoSearch.getSearchKeyword()));
			List<CurrencyExchangeHeader> currencyExchangeHeaderList = null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				currencyExchangeHeaderList=this.repositoryCurrencyExchangeHeader.predictiveSearchWithPagination(dtoSearch.getSearchKeyword(),
						new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC, "createdDate"));
			}
			else
			{
				currencyExchangeHeaderList=this.repositoryCurrencyExchangeHeader.predictiveSearchWithPagination(dtoSearch.getSearchKeyword());
			}
			
			if(currencyExchangeHeaderList != null && !currencyExchangeHeaderList.isEmpty()){
				List<DtoCurrencyExchangeTableSetupHeader> dtoExchangeTableSetupHeader= new ArrayList<>();
				for(CurrencyExchangeHeader currencyExchangeHeader : currencyExchangeHeaderList){
					MasterRateFrequencyTypes masterRateFrequencyTypes = currencyExchangeHeader.getMasterRateFrequencyTypes();
					if(masterRateFrequencyTypes!=null){
						masterRateFrequencyTypes = repositoryMasterRateFrequencyTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterRateFrequencyTypes.getTypeId(),langId,false);
					}
					dtoExchangeTableSetupHeader.add(new DtoCurrencyExchangeTableSetupHeader(currencyExchangeHeader,masterRateFrequencyTypes));
				}
				dtoSearch.setRecords(dtoExchangeTableSetupHeader);
			} else {
				dtoSearch.setRecords(null);
			}
		}
		return dtoSearch;
	}
	
	/**
	 * @param dtoCurrencyExchangeDetail
	 * @return
	 */
	public Boolean saveCurrencyExchangeDetail(DtoCurrencyExchangeDetail dtoCurrencyExchangeDetail)
	{
		String userid = httpServletRequest.getHeader(CommonConstant.USER_ID);
		int createdBy=0;
		if(UtilRandomKey.isNotBlank(userid)){
			createdBy=Integer.parseInt(userid);
		}
		
		if(dtoCurrencyExchangeDetail != null)
		{
			CurrencyExchangeDetail currencyExchangeDetail = repositoryCurrencyExchangeDetail.findByCurrencyExchangeHeaderExchangeIdAndIsDeleted(dtoCurrencyExchangeDetail.getExchangeId(),false);
			if(currencyExchangeDetail == null){
				currencyExchangeDetail = new CurrencyExchangeDetail();
				currencyExchangeDetail.setCreatedBy(createdBy);
			}
			else{
				currencyExchangeDetail.setChangeBy(createdBy);
			}
			currencyExchangeDetail.setCurrencyExchangeHeader(repositoryCurrencyExchangeHeader.findByExchangeIdAndIsDeleted(dtoCurrencyExchangeDetail.getExchangeId(),false));
			currencyExchangeDetail.setExchangeDescription(dtoCurrencyExchangeDetail.getDescription());
			currencyExchangeDetail.setExchangeDescriptionArabic(dtoCurrencyExchangeDetail.getDescriptionArabic());
			if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeRate())){
				currencyExchangeDetail.setExchangeRate(Double.valueOf(dtoCurrencyExchangeDetail.getExchangeRate()));
			}
			if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeDate())){
				currencyExchangeDetail.setExchangeDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoCurrencyExchangeDetail.getExchangeDate()));
			}
			if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeTime())){
				currencyExchangeDetail.setTime(UtilDateAndTime.getStaticDateWithTimeFromTime24Formats(dtoCurrencyExchangeDetail.getExchangeTime()));
			}
			if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeExpirationDate())){
				currencyExchangeDetail.setExpirationExchangeDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoCurrencyExchangeDetail.getExchangeExpirationDate()));
			}
			currencyExchangeDetail.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCurrencyExchangeDetail.getCurrencyId(),false));
			currencyExchangeDetail=repositoryCurrencyExchangeDetail.save(currencyExchangeDetail);
			if(currencyExchangeDetail!=null){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param dtoCurrencyExchangeDetail
	 * @return
	 */
	public DtoCurrencyExchangeDetail getCurrencyExchangeDetailByExchangeId(DtoCurrencyExchangeDetail dtoCurrencyExchangeDetail, CurrencyExchangeDetail currencyExchangeDetail){
		DtoCurrencyExchangeDetail dtoCurrencyExchangeDetails=null;
		String langid = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		dtoCurrencyExchangeDetails= new DtoCurrencyExchangeDetail(currencyExchangeDetail,langid);
		return dtoCurrencyExchangeDetails;
	}
	
	/**
	 * @param dtoCurrencyExchangeDetail
	 * @return
	 */
	public Boolean updateCurrencyExchangeDetail(DtoCurrencyExchangeDetail dtoCurrencyExchangeDetail)
	{
		String userid = httpServletRequest.getHeader(CommonConstant.USER_ID);
		int changeBy=0;
		if(UtilRandomKey.isNotBlank(userid)){
			changeBy=Integer.parseInt(userid);
		}
		
		if(dtoCurrencyExchangeDetail != null)
		{
			CurrencyExchangeDetail currencyExchangeDetail=repositoryCurrencyExchangeDetail.findByCurrencyExchangeHeaderExchangeIdAndIsDeleted(dtoCurrencyExchangeDetail.getExchangeId(),false);
			if(currencyExchangeDetail!=null)
			{
				currencyExchangeDetail.setExchangeDescription(dtoCurrencyExchangeDetail.getDescription());
				currencyExchangeDetail.setExchangeDescriptionArabic(dtoCurrencyExchangeDetail.getDescriptionArabic());
				if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeRate())){
					currencyExchangeDetail.setExchangeRate(Double.valueOf(dtoCurrencyExchangeDetail.getExchangeRate()));
				}
				if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeDate())){
					currencyExchangeDetail.setExchangeDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoCurrencyExchangeDetail.getExchangeDate()));
				}
				if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeTime())){
					currencyExchangeDetail.setTime(UtilDateAndTime.getStaticDateWithTimeFromTime24Formats(dtoCurrencyExchangeDetail.getExchangeTime()));
				}
				if(UtilRandomKey.isNotBlank(dtoCurrencyExchangeDetail.getExchangeExpirationDate())){
					currencyExchangeDetail.setExpirationExchangeDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoCurrencyExchangeDetail.getExchangeExpirationDate()));
				}
				currencyExchangeDetail.setModifyDate(new Date());
				currencyExchangeDetail.setChangeBy(changeBy);
				currencyExchangeDetail.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoCurrencyExchangeDetail.getCurrencyId(),false));
				currencyExchangeDetail=repositoryCurrencyExchangeDetail.save(currencyExchangeDetail);
			}
			
			if(currencyExchangeDetail!=null){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchCurrencyExchangeDetail(DtoSearch dtoSearch){
		if(dtoSearch != null){
			String langid = httpServletRequest.getHeader(CommonConstant.LANG_ID);
			dtoSearch.setTotalCount(this.repositoryCurrencyExchangeDetail.predictiveSearchCount(dtoSearch.getSearchKeyword()));
			List<CurrencyExchangeDetail> currencyExchangeDetailList = null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
				currencyExchangeDetailList=this.repositoryCurrencyExchangeDetail.predictiveSearchWithPagination(dtoSearch.getSearchKeyword(),
						new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC, "createdDate"));
			}
			else
			{
				currencyExchangeDetailList=this.repositoryCurrencyExchangeDetail.predictiveSearchWithPagination(dtoSearch.getSearchKeyword());
			}
			
			if(currencyExchangeDetailList != null && !currencyExchangeDetailList.isEmpty()){
				List<DtoCurrencyExchangeDetail> dtoExchangeDetail= new ArrayList<>();
				for(CurrencyExchangeDetail currencyExchangeDetail : currencyExchangeDetailList){
					dtoExchangeDetail.add(new DtoCurrencyExchangeDetail(currencyExchangeDetail,langid));
				}
				dtoSearch.setRecords(dtoExchangeDetail);
			} else {
				dtoSearch.setRecords(null);
			}
		}
		return dtoSearch;
	}
	
	public DtoSearch getCurrencyExchangeHeaderSetupsByCurrencyId(DtoSearch dtoSearch)
	{
		if(dtoSearch != null)
		{
			int langId = serviceHome.getLanngugaeId();
			List<CurrencyExchangeHeader> currencyExchangeHeaderList = null;
			if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null)
			{
				currencyExchangeHeaderList=this.repositoryCurrencyExchangeHeader.findByCurrencySetupCurrencyIdAndIsDeleted(dtoSearch.getCurrencyId(),false,
						new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC, "createdDate"));
			}
			else
			{
				currencyExchangeHeaderList=this.repositoryCurrencyExchangeHeader.findByCurrencySetupCurrencyIdAndIsDeleted(dtoSearch.getCurrencyId(), false);
			}
			
			if(currencyExchangeHeaderList != null && !currencyExchangeHeaderList.isEmpty()){
				List<DtoCurrencyExchangeTableSetupHeader> dtoExchangeTableSetupHeader= new ArrayList<>();
				for(CurrencyExchangeHeader currencyExchangeHeader : currencyExchangeHeaderList){
					
					String newDateString = UtilDateAndTime.dateToStringddmmyyyy(new Date());
					if(currencyExchangeHeader.getCurrencyExchangeDetail()!=null)
					{
						Date expireDate = currencyExchangeHeader.getCurrencyExchangeDetail().getExpirationExchangeDate();
						String expiryDateString = UtilDateAndTime.dateToStringddmmyyyy(expireDate);
						if(expireDate.after(new Date()) || expiryDateString.equalsIgnoreCase(newDateString))
						{
							MasterRateFrequencyTypes masterRateFrequencyTypes = currencyExchangeHeader.getMasterRateFrequencyTypes();
							if(masterRateFrequencyTypes!=null){
								masterRateFrequencyTypes = repositoryMasterRateFrequencyTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterRateFrequencyTypes.getTypeId(),langId,false);
							}
							dtoExchangeTableSetupHeader.add(new DtoCurrencyExchangeTableSetupHeader(currencyExchangeHeader,masterRateFrequencyTypes));
						}
					}
				}
				dtoSearch.setRecords(dtoExchangeTableSetupHeader);
			} else {
				dtoSearch.setRecords(null);
			}
		}
		return dtoSearch;
	}
}
