/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bti.constant.ARMasterDocumentTypeConstant;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.ARApplyDocumentsPayments;
import com.bti.model.ARPaymentTransactions;
import com.bti.model.ARYTDOpenTransactions;
import com.bti.model.ApplyDocumentsPayments;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.MasterDocumentType;
import com.bti.model.PaymentTermsSetup;
import com.bti.model.RMDocumentsTypeSetup;
import com.bti.model.dto.DtoARApplyDocumentsPayments;
import com.bti.model.dto.DtoARYTDOpenTransactions;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryARApplyDocumentsPayments;
import com.bti.repository.RepositoryARPaymentTransactions;
import com.bti.repository.RepositoryARYTDOpenTransaction;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryMasterDocumentType;
import com.bti.repository.RepositoryPaymentTermSetup;
import com.bti.repository.RepositoryRMDocumentType;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceApplyARTransactions Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceApplyARTransactions")
public class ServiceApplyARTransactions {

	private static final Logger LOG = Logger.getLogger(ServiceApplyARTransactions.class);

	private static final String USER_ID = "userid";
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryARPaymentTransactions repositoryARPaymentTransactions;
	@Autowired
	RepositoryARApplyDocumentsPayments repositoryARApplyDocumentsPayments;
	@Autowired
	RepositoryMasterDocumentType repositoryMasterDocumentType;
	@Autowired
	RepositoryARYTDOpenTransaction repositoryARYTDOpenTransaction;
	@Autowired
	RepositoryRMDocumentType repositoryRMDocumentType;
	
	@Autowired
	RepositoryPaymentTermSetup repositoryPaymentTermSetup;
	
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	public List<DtoStatusType> getDocumentType() {
		List<DtoStatusType> list = new ArrayList<>();
		int langId = serviceHome.getLanngugaeId();
		try {
			List<MasterDocumentType> masterDocumentTypeList = repositoryMasterDocumentType.findByLanguageLanguageIdAndIsDeletedAndMasterType(langId, false,"AR");
			if (masterDocumentTypeList != null && !masterDocumentTypeList.isEmpty()) {
				masterDocumentTypeList.forEach(masterDocumentType -> list.add(new DtoStatusType(masterDocumentType)));
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}

	public DtoSearch getOpenTransactionsByCustomerId(DtoARYTDOpenTransactions dtoARYTDOpenTransaction) {
		DtoSearch dtoSearch= new DtoSearch();
		dtoSearch.setPageNumber(0);
		dtoSearch.setPageSize(0);
		dtoSearch.setTotalCount(repositoryARYTDOpenTransaction.getCountByCustomerId(dtoARYTDOpenTransaction.getCustomerID()));
		List<DtoARYTDOpenTransactions> list = new ArrayList<>();
		try {
			List<ARYTDOpenTransactions> arYTDOpenTransactionsList=null;
			if(dtoARYTDOpenTransaction.getPageNumber()!=null && dtoARYTDOpenTransaction.getPageSize()!=null && 
					dtoARYTDOpenTransaction.getPageSize()>0){
				dtoSearch.setPageNumber(dtoARYTDOpenTransaction.getPageNumber());
				dtoSearch.setPageSize(dtoARYTDOpenTransaction.getPageSize());
				Pageable pageable = new PageRequest(dtoARYTDOpenTransaction.getPageNumber(), dtoARYTDOpenTransaction.getPageSize());
				arYTDOpenTransactionsList = repositoryARYTDOpenTransaction.findByCustomerID(dtoARYTDOpenTransaction.getCustomerID(),pageable);
			}
			else{
				arYTDOpenTransactionsList=repositoryARYTDOpenTransaction.findByCustomerID(dtoARYTDOpenTransaction.getCustomerID());
			}
			
			if (arYTDOpenTransactionsList != null && !arYTDOpenTransactionsList.isEmpty()) {
				for (ARYTDOpenTransactions arytdOpenTransactions2 : arYTDOpenTransactionsList) {
					DtoARYTDOpenTransactions dtoARYTDOpenTransactions=new DtoARYTDOpenTransactions(arytdOpenTransactions2);
					RMDocumentsTypeSetup rmDocumentsTypeSetup = repositoryRMDocumentType.findByArDocumentTypeIdAndIsDeleted(arytdOpenTransactions2.getArTransactionType(), false);
				    if(rmDocumentsTypeSetup!=null){
				    	dtoARYTDOpenTransactions.setArTransactionType(rmDocumentsTypeSetup.getDocumentCode());
				    }
				  
				    PaymentTermsSetup paymentTermsSetup=repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(arytdOpenTransactions2.getPaymentTermsID(), false);
				    if(paymentTermsSetup!=null){
				    	Calendar cal =Calendar.getInstance();
				    	cal.setTime(arytdOpenTransactions2.getArTransactionDate());
				    	int days=paymentTermsSetup.getDueDaysDate();
				    	cal.add(Calendar.DATE, days);
				    	dtoARYTDOpenTransactions.setDueDate(UtilDateAndTime.dateToStringddmmyyyy(cal.getTime()));
				    }
					  double applyAmount=  repositoryARApplyDocumentsPayments.
							  getApplyAmountByArTransactionNumber(arytdOpenTransactions2.getArTransactionNumber());
					 
						  dtoARYTDOpenTransactions.setApplyAmount(applyAmount);
						  double remainingAmount = arytdOpenTransactions2.getArTransactionTotalAmount()-applyAmount;
						  dtoARYTDOpenTransactions.setRemainingAmount(remainingAmount);
					  
				    list.add(dtoARYTDOpenTransactions);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		dtoSearch.setRecords(list);
		return dtoSearch;
	}
	
	public DtoSearch getOpenTransactionsByPostingDateAndCustomerId(DtoARYTDOpenTransactions dtoARYTDOpenTransaction) {
			DtoSearch dtoSearch= new DtoSearch();
			dtoSearch.setPageNumber(0);
			dtoSearch.setPageSize(0);
			dtoSearch.setTotalCount(repositoryARYTDOpenTransaction.getCountByCustomerIdAndPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARYTDOpenTransaction.getPostingDate()),dtoARYTDOpenTransaction.getCustomerID()));
			List<DtoARYTDOpenTransactions> list = new ArrayList<>();
			try {
				List<ARYTDOpenTransactions> arYTDOpenTransactionsList=null;
				if(dtoARYTDOpenTransaction.getPageNumber()!=null && dtoARYTDOpenTransaction.getPageSize()!=null && 
						dtoARYTDOpenTransaction.getPageSize()>0){
					dtoSearch.setPageNumber(dtoARYTDOpenTransaction.getPageNumber());
					dtoSearch.setPageSize(dtoARYTDOpenTransaction.getPageSize());
					Pageable pageable = new PageRequest(dtoARYTDOpenTransaction.getPageNumber(), dtoARYTDOpenTransaction.getPageSize());
					arYTDOpenTransactionsList = repositoryARYTDOpenTransaction.findByPostingDateAndCustomerId(UtilDateAndTime.ddmmyyyyStringToDate(dtoARYTDOpenTransaction.getPostingDate()),dtoARYTDOpenTransaction.getCustomerID(),pageable);
				}
				else{
					arYTDOpenTransactionsList=repositoryARYTDOpenTransaction.findByPostingDateAndCustomerId(UtilDateAndTime.ddmmyyyyStringToDate(dtoARYTDOpenTransaction.getPostingDate()),dtoARYTDOpenTransaction.getCustomerID());
				}
				
				if (arYTDOpenTransactionsList != null && !arYTDOpenTransactionsList.isEmpty()) {
					for (ARYTDOpenTransactions arytdOpenTransactions2 : arYTDOpenTransactionsList) {
						DtoARYTDOpenTransactions dtoARYTDOpenTransactions=new DtoARYTDOpenTransactions(arytdOpenTransactions2);
						RMDocumentsTypeSetup rmDocumentsTypeSetup = repositoryRMDocumentType.findByArDocumentTypeIdAndIsDeleted(arytdOpenTransactions2.getArTransactionType(), false);
					    if(rmDocumentsTypeSetup!=null){
					    	dtoARYTDOpenTransactions.setArTransactionType(rmDocumentsTypeSetup.getDocumentCode());
					    }
					  
					    PaymentTermsSetup paymentTermsSetup=repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(arytdOpenTransactions2.getPaymentTermsID(), false);
					    if(paymentTermsSetup!=null){
					    	Calendar cal =Calendar.getInstance();
					    	cal.setTime(arytdOpenTransactions2.getArTransactionDate());
					    	int days=paymentTermsSetup.getDueDaysDate();
					    	cal.add(Calendar.DATE, days);
					    	dtoARYTDOpenTransactions.setDueDate(UtilDateAndTime.dateToStringddmmyyyy(cal.getTime()));
					    }
					    double applyAmount=  repositoryARApplyDocumentsPayments.
								  getApplyAmountByArTransactionNumber(arytdOpenTransactions2.getArTransactionNumber());
						 
							  dtoARYTDOpenTransactions.setApplyAmount(applyAmount);
							  double remainingAmount = arytdOpenTransactions2.getArTransactionTotalAmount()-applyAmount;
							  dtoARYTDOpenTransactions.setRemainingAmount(remainingAmount);
						  
					    list.add(dtoARYTDOpenTransactions);
					}
				}
			} catch (Exception e) {
				LOG.error(e.getMessage());
			}
			dtoSearch.setRecords(list);
			return dtoSearch;
	}
	
	public DtoARYTDOpenTransactions applyTransaction(DtoARYTDOpenTransactions dtoARYTDOpenTransactions) {
		
		ARYTDOpenTransactions arytdOpenTransactions=repositoryARYTDOpenTransaction.findByCashReceiptNumber(dtoARYTDOpenTransactions.getPaymentNumber());
		if(arytdOpenTransactions!=null){
			ARPaymentTransactions arPaymentTransactions=repositoryARPaymentTransactions.findByArTransactionNumberAndCustomerNumber(dtoARYTDOpenTransactions.getPaymentNumber(),arytdOpenTransactions.getCustomerID());
			if(arPaymentTransactions==null){
				arPaymentTransactions=new ARPaymentTransactions();
			}
			arPaymentTransactions.setArTransactionNumber(dtoARYTDOpenTransactions.getPaymentNumber());
			arPaymentTransactions.setArTransactionDate(arytdOpenTransactions.getArTransactionDate());
			arPaymentTransactions.setArTransactionPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARYTDOpenTransactions.getPostingDate()));
			arPaymentTransactions.setCustomerNumber(dtoARYTDOpenTransactions.getCustomerID());
			arPaymentTransactions.setCheckbookId(arytdOpenTransactions.getCheckbookID());
			arPaymentTransactions.setCurrencyId(arytdOpenTransactions.getCurrencyID());
			arPaymentTransactions.setExchangeTableIndex(String.valueOf(arytdOpenTransactions.getExchangeTableIndex()));
			arPaymentTransactions.setArPaymentAmount(dtoARYTDOpenTransactions.getOriginalAmount());
			arPaymentTransactions.setArFunctionalPaymentAmount(dtoARYTDOpenTransactions.getFunctionalAmount());
			//arPaymentTransactions.setAuditTrialCode(auditTrialCode); //Set audit trial code for Payment
			arPaymentTransactions.setPaymentVoid(arytdOpenTransactions.getTransactionVoid());
			arPaymentTransactions.setRowDateindex(new Date());
			repositoryARPaymentTransactions.saveAndFlush(arPaymentTransactions);
			
			List<DtoARYTDOpenTransactions> list=dtoARYTDOpenTransactions.getArYTDOpenTransactionsList();
			if(!list.isEmpty()){
				for (DtoARYTDOpenTransactions openTransactionApply : list) {
					
					ARApplyDocumentsPayments applyDocumentsPayments=repositoryARApplyDocumentsPayments.
							findByApplyDocumentNumberPaymentNumberAndArTransactionNumber(dtoARYTDOpenTransactions.getPaymentNumber(), openTransactionApply.getArTransactionNumber());
					double oldApplyAmount=0.0;
					if(applyDocumentsPayments==null){
						applyDocumentsPayments=new ARApplyDocumentsPayments();
					}
					else{
						oldApplyAmount=applyDocumentsPayments.getApplyAmount();
					}
					double applyAmount=  repositoryARApplyDocumentsPayments.
							  getApplyAmountByArTransactionNumber(openTransactionApply.getArTransactionNumber());
					 
					double remainingApplyAmount=openTransactionApply.getApplyAmount()-applyAmount;
					remainingApplyAmount=remainingApplyAmount+oldApplyAmount;
					applyDocumentsPayments.setApplyAmount(remainingApplyAmount);	
					applyDocumentsPayments.setCustomerNumber(dtoARYTDOpenTransactions.getCustomerID());
					applyDocumentsPayments.setArTransactionNumber(openTransactionApply.getArTransactionNumber());
					applyDocumentsPayments.setArTransactionDate(arytdOpenTransactions.getArTransactionDate());
					applyDocumentsPayments.setArTransactionPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARYTDOpenTransactions.getPostingDate()));
					applyDocumentsPayments.setPaymentAlreadyPosted(1); //1 for posted
					applyDocumentsPayments.setApplyDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARYTDOpenTransactions.getApplyDate()));
					
					applyDocumentsPayments.setOriginalAmount(openTransactionApply.getOriginalAmount()!=null?
							openTransactionApply.getOriginalAmount():0.0);
					applyDocumentsPayments.setRemainAmountAfterApply(openTransactionApply.getRemainingAmount()!=null?
							openTransactionApply.getRemainingAmount():0.0);
					applyDocumentsPayments.setModifyByUserID(httpServletRequest.getHeader(USER_ID));
					applyDocumentsPayments.setApplyDocumentNumberPaymentNumber(dtoARYTDOpenTransactions.getPaymentNumber());
					applyDocumentsPayments.setRowDateIndex(new Date());
					repositoryARApplyDocumentsPayments.saveAndFlush(applyDocumentsPayments);
					
				}
			}
			return dtoARYTDOpenTransactions;
		}
		return null;
	}

	public boolean unApplyTransaction(DtoARYTDOpenTransactions dtoARYTDOpenTransactions) 
	{
		List<DtoARYTDOpenTransactions> list = dtoARYTDOpenTransactions.getArYTDOpenTransactionsList();
		if(!list.isEmpty()){
			for (DtoARYTDOpenTransactions dtoARYTDOpenTransactions2 : list) 
			{
				ARApplyDocumentsPayments arApplyDocumentsPayments=repositoryARApplyDocumentsPayments.findByApplyDocumentNumberPaymentNumberAndArTransactionNumber(dtoARYTDOpenTransactions.getPaymentNumber(), dtoARYTDOpenTransactions2.getArTransactionNumber());
				if(arApplyDocumentsPayments!=null){
					repositoryARApplyDocumentsPayments.delete(arApplyDocumentsPayments);
				}
			}
			return true;
		}
		return false;
	}
	
	public List<DtoARYTDOpenTransactions> getDocumentNumberByCustomerAndDocType(DtoARYTDOpenTransactions dtoARYTDOpenTransactions) {

		List<DtoARYTDOpenTransactions> list = new ArrayList<>();
		try {
			List<ARYTDOpenTransactions> arYTDOpenTransactionsList = null;
					
					if(ARMasterDocumentTypeConstant.SALES.getIndex()==dtoARYTDOpenTransactions.getDocumentType()){
						arYTDOpenTransactionsList=repositoryARYTDOpenTransaction.
								findByCustomerIDAndArTransactionSalesAmountIsNotNullAndIsPaymentAndArTransactionSalesAmountGreaterThan(
										dtoARYTDOpenTransactions.getCustomerID(),true, 0.0);
					}
					else if(ARMasterDocumentTypeConstant.FINANCE_CHARGE.getIndex()==dtoARYTDOpenTransactions.getDocumentType()){
						arYTDOpenTransactionsList=repositoryARYTDOpenTransaction.
								findByCustomerIDAndArTransactionFinanceChargeAmountIsNotNullAndIsPaymentAndArTransactionFinanceChargeAmountGreaterThan(
										dtoARYTDOpenTransactions.getCustomerID(),true, 0.0);
					}
					else if(ARMasterDocumentTypeConstant.DEBIT_MEMO.getIndex()==dtoARYTDOpenTransactions.getDocumentType()){
						arYTDOpenTransactionsList=repositoryARYTDOpenTransaction.
								findByCustomerIDAndArTransactionDebitMemoAmountIsNotNullAndIsPaymentAndArTransactionDebitMemoAmountGreaterThan(
										dtoARYTDOpenTransactions.getCustomerID(),true, 0.0);			
					}
					else if(ARMasterDocumentTypeConstant.SERVICE.getIndex()==dtoARYTDOpenTransactions.getDocumentType()){
						arYTDOpenTransactionsList=repositoryARYTDOpenTransaction.
								findByCustomerIDAndArServiceRepairAmountIsNotNullAndIsPaymentAndArServiceRepairAmountGreaterThan(
										dtoARYTDOpenTransactions.getCustomerID(),true, 0.0);
					}
			
			if (arYTDOpenTransactionsList != null && !arYTDOpenTransactionsList.isEmpty()) {
				for (ARYTDOpenTransactions arytdOpenTransactions2 : arYTDOpenTransactionsList) {
					if(UtilRandomKey.isNotBlank(arytdOpenTransactions2.getCashReceiptNumber())){
						DtoARYTDOpenTransactions arYTDOpenTransactions=	 new DtoARYTDOpenTransactions();
						arYTDOpenTransactions.setPaymentNumber(arytdOpenTransactions2.getCashReceiptNumber());
						list.add(arYTDOpenTransactions);
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}

	public DtoARYTDOpenTransactions getAmountByPaymentNumber(DtoARYTDOpenTransactions dtoARYTDOpenTransactions) throws ScriptException {
		ARYTDOpenTransactions  arytdOpenTransactions= repositoryARYTDOpenTransaction.findByCashReceiptNumber(dtoARYTDOpenTransactions.getPaymentNumber());
		if(arytdOpenTransactions!=null)
		{
			dtoARYTDOpenTransactions.setPostingDate(UtilDateAndTime.dateToStringddmmyyyy(arytdOpenTransactions.getPostingDate()));
			dtoARYTDOpenTransactions.setOriginalAmount(arytdOpenTransactions.getArTransactionTotalAmount());
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			double amount =arytdOpenTransactions.getArTransactionTotalAmount();
			String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
			if(UtilRandomKey.isNotBlank(String.valueOf(arytdOpenTransactions.getExchangeTableIndex()))){
				CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(arytdOpenTransactions.getExchangeTableIndex(), false);
			    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
				   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
			    }
			}
			Double functionalAmount =amount;
            double exchangeTableRate= arytdOpenTransactions.getExchangeTableRate();
            if(exchangeTableRate>0.0){
            	functionalAmount = (double) mathEval.eval(amount + calcMethod + exchangeTableRate);
            }
			
			dtoARYTDOpenTransactions.setFunctionalAmount(functionalAmount);
			return dtoARYTDOpenTransactions;
		}
		return null;
	}
	


	
}
