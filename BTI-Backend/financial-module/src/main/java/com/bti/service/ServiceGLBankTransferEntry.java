/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.AuditTrailSeriesTypeConstant;
import com.bti.constant.BankTransactionEntryTypeConstant;
import com.bti.constant.BankTransactionTypeConstant;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLBankTransferDetails;
import com.bti.model.GLBankTransferDistribution;
import com.bti.model.GLBankTransferEntryCharges;
import com.bti.model.GLBankTransferHeader;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumber;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumberKey;
import com.bti.model.GLCurrentSummaryMasterTableByDimensions;
import com.bti.model.GLCurrentSummaryMasterTableByDimensionsKey;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccount;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccountKey;
import com.bti.model.GLYTDOpenBankTransactionsEntryDetails;
import com.bti.model.GLYTDOpenBankTransactionsEntryHeader;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.dto.DtoGLBankTransferCharges;
import com.bti.model.dto.DtoGLBankTransferDistribution;
import com.bti.model.dto.DtoGLBankTransferHeader;
import com.bti.repository.RepositoryBankTransactionEntryAction;
import com.bti.repository.RepositoryBankTransactionEntryType;
import com.bti.repository.RepositoryBankTransactionType;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCorrectJournalEntryActions;
import com.bti.repository.RepositoryCurrencyExchangeDetail;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBankTransferDetails;
import com.bti.repository.RepositoryGLBankTransferDistribution;
import com.bti.repository.RepositoryGLBankTransferEntryCharges;
import com.bti.repository.RepositoryGLBankTransferHeader;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLCashReceiptBank;
import com.bti.repository.RepositoryGLCashReceiptDistribution;
import com.bti.repository.RepositoryGLClearingDetail;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByDimensionIndex;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByMainAccount;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryDetails;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryHeader;
import com.bti.repository.RepositoryGlytdHistoryTransaction;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.repository.RepositoryPaymentMethodType;
import com.bti.repository.RepositoryReceiptType;
import com.bti.repository.RepositoryVATSetup;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.jayway.restassured.path.json.JsonPath;

/**
 * Description: ServiceGLBankTransferEntry Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceGLBankTransferEntry")
public class ServiceGLBankTransferEntry {

	private static final Logger LOG = Logger.getLogger(ServiceGLBankTransferEntry.class);

	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;

	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryGLCashReceiptBank repositoryGLCashReceiptBank;

	@Autowired
	RepositoryGLBankTransferHeader repositoryGLBankTransferHeader;
	@Autowired
	RepositoryGLBankTransferDetails repositoryGLBankTransferDetails;
	@Autowired
	RepositoryGLBankTransferDistribution repositoryGLBankTransferDistribution;
	@Autowired
	RepositoryGLBankTransferEntryCharges repositoryGLBankTransferEntryCharges;
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByMainAccount repositoryGLCurrentSummaryMasterTableByMainAccount;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByDimensionIndex repositoryGLCurrentSummaryMasterTableByDimensionIndex;

	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	@Autowired
	RepositoryCurrencyExchangeDetail repositoryCurrencyExchangeDetail;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;

	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;

	@Autowired
	RepositoryGlytdHistoryTransaction repositoryGlytdHistoryTransaction;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;

	@Autowired
	RepositoryCorrectJournalEntryActions repositoryCorrectJournalEntryActions;

	@Autowired
	RepositoryGLClearingDetail repositoryGLClearingDetail;

	@Autowired
	RepositoryReceiptType repositoryReceiptType;

	@Autowired
	RepositoryPaymentMethodType repositoryPaymentMethodType;

	@Autowired
	RepositoryGLCashReceiptDistribution repositoryGLCashReceiptDistribution;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;

	@Autowired
	RepositoryVATSetup repositoryVATSetup;
	@Autowired
	RepositoryBankTransactionEntryAction repositoryBankTransactionEntryAction;

	@Autowired
	RepositoryBankTransactionEntryType repositoryBankTransactionEntryType;
	@Autowired
	RepositoryBankTransactionType repositoryBankTransactionType;
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryDetails repositoryGLYTDOpenBankTransactionsEntryDetails;
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryHeader repositoryGLYTDOpenBankTransactionsEntryHeader;

	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;

	@Autowired
	ServiceGLCashReceiptEntry serviceGLCashReceiptEntry;
	@Autowired
	ServiceGLClearingGeneralEntry serviceGLClearingGeneralEntry;
	@Autowired
	ServiceGLJournalEntry serviceGLJournalEntry;


	public DtoGLBankTransferHeader saveBankTransferEntry(DtoGLBankTransferHeader dtoGLBankTransferHeader) {
		try {
			GLBankTransferHeader glBankTransferHeader = repositoryGLBankTransferHeader
					.findByBankTransferNumber(dtoGLBankTransferHeader.getBankTransferNumber());
			if (glBankTransferHeader != null) {
				return null;
			}
			glBankTransferHeader = new GLBankTransferHeader();
			glBankTransferHeader.setBankTransferNumber(dtoGLBankTransferHeader.getBankTransferNumber());
			glBankTransferHeader.setCheckBookIdFrom(dtoGLBankTransferHeader.getCheckbookIDFrom());
			glBankTransferHeader.setCheckbookIdTo(dtoGLBankTransferHeader.getCheckbookIDTo());
			glBankTransferHeader.setCommentFrom(dtoGLBankTransferHeader.getCommentFrom());
			glBankTransferHeader.setCommentTo(dtoGLBankTransferHeader.getCommentTo());
			glBankTransferHeader.setCompanyTransferFrom(dtoGLBankTransferHeader.getCompanyTransferFrom());
			glBankTransferHeader.setCompanyTransferTo(dtoGLBankTransferHeader.getCompanyTransferTo());
			glBankTransferHeader.setCurrencyId(dtoGLBankTransferHeader.getCurrencyID());
			glBankTransferHeader.setExchangeRateFrom(dtoGLBankTransferHeader.getExchangeRateFrom());
			glBankTransferHeader.setExchangeRateTo(dtoGLBankTransferHeader.getExchangeRateTo());
			glBankTransferHeader.setExchangeTableID(dtoGLBankTransferHeader.getExchangeTableID());
			glBankTransferHeader.setGlBatches(
					repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoGLBankTransferHeader.getBatchID(),BatchTransactionTypeConstant.BANK_TRANSFER.getIndex(), false));
			glBankTransferHeader.setRateCalculateMethodFrom(dtoGLBankTransferHeader.getRateCalculateMethodFrom());
			glBankTransferHeader.setRateCalculateMethodTo(dtoGLBankTransferHeader.getRateCalculateMethodTo());
			glBankTransferHeader.setTransferAmountFrom(dtoGLBankTransferHeader.getTransferAmountFrom());
			glBankTransferHeader.setTransferAmountTo(dtoGLBankTransferHeader.getTransferAmountTo());
			glBankTransferHeader
					.setTransferDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoGLBankTransferHeader.getTransferDate()));
			glBankTransferHeader = repositoryGLBankTransferHeader.saveAndFlush(glBankTransferHeader);

			GLBankTransferDetails glBankTransferDetailsFrom = new GLBankTransferDetails();
			glBankTransferDetailsFrom.setGlBankTransferHeader(glBankTransferHeader);
			glBankTransferDetailsFrom.setCheckbookId(dtoGLBankTransferHeader.getCheckbookIDFrom());
			glBankTransferDetailsFrom.setCompanyID(dtoGLBankTransferHeader.getCompanyTransferFrom());
			glBankTransferDetailsFrom.setTransferAmount(dtoGLBankTransferHeader.getTransferAmountFrom());
			glBankTransferDetailsFrom.setTypeTransfer(CommonConstant.FROM);
			repositoryGLBankTransferDetails.saveAndFlush(glBankTransferDetailsFrom);

			GLBankTransferDetails glBankTransferDetailsTo = new GLBankTransferDetails();
			glBankTransferDetailsTo.setGlBankTransferHeader(glBankTransferHeader);
			glBankTransferDetailsTo.setCheckbookId(dtoGLBankTransferHeader.getCheckbookIDTo());
			glBankTransferDetailsTo.setCompanyID(dtoGLBankTransferHeader.getCompanyTransferTo());
			glBankTransferDetailsTo.setTransferAmount(dtoGLBankTransferHeader.getTransferAmountTo());
			glBankTransferDetailsTo.setTypeTransfer(CommonConstant.TO);
			repositoryGLBankTransferDetails.saveAndFlush(glBankTransferDetailsTo);
			return getBankTransferEntry(dtoGLBankTransferHeader);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoGLBankTransferHeader updateBankTransferEntry(DtoGLBankTransferHeader dtoGLBankTransferHeader) {
		try {
			GLBankTransferHeader glBankTransferHeader = repositoryGLBankTransferHeader
					.findByBankTransferNumber(dtoGLBankTransferHeader.getBankTransferNumber());
			if (glBankTransferHeader == null) {
				return null;
			}
			glBankTransferHeader.setBankTransferNumber(dtoGLBankTransferHeader.getBankTransferNumber());
			glBankTransferHeader.setCheckBookIdFrom(dtoGLBankTransferHeader.getCheckbookIDFrom());
			glBankTransferHeader.setCheckbookIdTo(dtoGLBankTransferHeader.getCheckbookIDTo());
			glBankTransferHeader.setCommentFrom(dtoGLBankTransferHeader.getCommentFrom());
			glBankTransferHeader.setCommentTo(dtoGLBankTransferHeader.getCommentTo());
			glBankTransferHeader.setCompanyTransferFrom(dtoGLBankTransferHeader.getCompanyTransferFrom());
			glBankTransferHeader.setCompanyTransferTo(dtoGLBankTransferHeader.getCompanyTransferTo());
			glBankTransferHeader.setCurrencyId(dtoGLBankTransferHeader.getCurrencyID());
			glBankTransferHeader.setExchangeRateFrom(dtoGLBankTransferHeader.getExchangeRateFrom());
			glBankTransferHeader.setExchangeRateTo(dtoGLBankTransferHeader.getExchangeRateTo());
			glBankTransferHeader.setExchangeTableID(dtoGLBankTransferHeader.getExchangeTableID());
			glBankTransferHeader.setGlBatches(
					repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoGLBankTransferHeader.getBatchID(),BatchTransactionTypeConstant.BANK_TRANSFER.getIndex(), false));
			glBankTransferHeader.setRateCalculateMethodFrom(dtoGLBankTransferHeader.getRateCalculateMethodFrom());
			glBankTransferHeader.setRateCalculateMethodTo(dtoGLBankTransferHeader.getRateCalculateMethodTo());
			glBankTransferHeader.setTransferAmountFrom(dtoGLBankTransferHeader.getTransferAmountFrom());
			glBankTransferHeader.setTransferAmountTo(dtoGLBankTransferHeader.getTransferAmountTo());
			glBankTransferHeader
					.setTransferDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoGLBankTransferHeader.getTransferDate()));
			repositoryGLBankTransferHeader.saveAndFlush(glBankTransferHeader);

			List<GLBankTransferDetails> glBankTransferDetails = repositoryGLBankTransferDetails
					.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
			if (glBankTransferDetails != null && !glBankTransferDetails.isEmpty()) {
				repositoryGLBankTransferDetails.deleteInBatch(glBankTransferDetails);
			}

			GLBankTransferDetails glBankTransferDetailsFrom = new GLBankTransferDetails();
			glBankTransferDetailsFrom.setGlBankTransferHeader(glBankTransferHeader);
			glBankTransferDetailsFrom.setCheckbookId(dtoGLBankTransferHeader.getCheckbookIDFrom());
			glBankTransferDetailsFrom.setCompanyID(dtoGLBankTransferHeader.getCompanyTransferFrom());
			glBankTransferDetailsFrom.setTransferAmount(dtoGLBankTransferHeader.getTransferAmountFrom());
			glBankTransferDetailsFrom.setTypeTransfer(CommonConstant.FROM);
			repositoryGLBankTransferDetails.saveAndFlush(glBankTransferDetailsFrom);

			GLBankTransferDetails glBankTransferDetailsTo = new GLBankTransferDetails();
			glBankTransferDetailsTo.setGlBankTransferHeader(glBankTransferHeader);
			glBankTransferDetailsTo.setCheckbookId(dtoGLBankTransferHeader.getCheckbookIDTo());
			glBankTransferDetailsTo.setCompanyID(dtoGLBankTransferHeader.getCompanyTransferTo());
			glBankTransferDetailsTo.setTransferAmount(dtoGLBankTransferHeader.getTransferAmountTo());
			glBankTransferDetailsTo.setTypeTransfer(CommonConstant.TO);
			repositoryGLBankTransferDetails.saveAndFlush(glBankTransferDetailsTo);

			return getBankTransferEntry(dtoGLBankTransferHeader);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoGLBankTransferHeader getBankTransferEntry(DtoGLBankTransferHeader dtoGLBankTransferHeader) {
		try {
			GLBankTransferHeader glBankTransferHeader = repositoryGLBankTransferHeader
					.findByBankTransferNumber(dtoGLBankTransferHeader.getBankTransferNumber());
			if (glBankTransferHeader != null) {
				return new DtoGLBankTransferHeader(glBankTransferHeader);
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoGLBankTransferDistribution getBankTransferDistrubutionByBankTransferNumber(
			int bankTransfernumber) {
		try {
			GLBankTransferHeader glBankTransferHeader = repositoryGLBankTransferHeader
					.findByBankTransferNumber(bankTransfernumber);
			if (glBankTransferHeader != null) {
				String tenantIdFrom = serviceHome
						.getCompanyDetail(Integer.parseInt(glBankTransferHeader.getCompanyTransferFrom()));
				JsonPath jsonFrom = null;
				String debitAccountNumber = "";
				String debitAccountDesc = "";
				String debitBankName = "";
				String debitAccountTableRowIndex = "";
				if (tenantIdFrom != null) {
					String headerTenant = httpServletRequest.getHeader(CommonConstant.TENANT_ID);
					if (!headerTenant.equalsIgnoreCase(tenantIdFrom)) {
						jsonFrom = serviceHome.getBankDistributionDetailByCheckbookId(
								glBankTransferHeader.getCheckBookIdFrom(), tenantIdFrom);
						if (jsonFrom != null) {
							debitAccountNumber = jsonFrom.getString("result.accountNumber");
							debitAccountDesc = jsonFrom.getString("result.accountDesc");
							debitBankName = jsonFrom.getString("result.bankName");
							debitAccountTableRowIndex = jsonFrom.getString("result.accountTableRowIndex");
						}
					}
					else 
					{
						CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance
								.findByCheckBookIdAndIsDeleted(glBankTransferHeader.getCheckBookIdFrom(), false);
						String accountNumber = "";
						String accountDesc = "";
						String bankName = "";
						String accountTableRowIndex = "";
						if (checkbookMaintenance != null
								&& checkbookMaintenance.getGlAccountsTableAccumulation() != null) {
							Object[] object=serviceAccountPayable
							.getAccountNumber(checkbookMaintenance.getGlAccountsTableAccumulation());
							accountNumber = object[0].toString();
							accountDesc = object[3].toString();
							accountTableRowIndex = checkbookMaintenance.getGlAccountsTableAccumulation()
									.getAccountTableRowIndex();
						}
						if (checkbookMaintenance != null && checkbookMaintenance.getBankSetup() != null) {
							bankName = checkbookMaintenance.getBankSetup().getBankDescription();
						}

						debitAccountNumber = accountNumber;
						debitAccountDesc = accountDesc;
						debitBankName = bankName;
						debitAccountTableRowIndex = accountTableRowIndex;
					}
				}

				String tenantIdTo = serviceHome
						.getCompanyDetail(Integer.parseInt(glBankTransferHeader.getCompanyTransferTo()));
				JsonPath jsonTo = null;
				String creditAccountNumber = "";
				String creditAccountDesc = "";
				String creditBankName = "";
				String creditAccountTableRowIndex = "";
				if (tenantIdTo != null) {
					String headerTenant = httpServletRequest.getHeader(CommonConstant.TENANT_ID);
					if (!headerTenant.equalsIgnoreCase(tenantIdTo)) {
						jsonTo = serviceHome.getBankDistributionDetailByCheckbookId(
								glBankTransferHeader.getCheckbookIdTo(), tenantIdTo);
						if (jsonTo != null) {
							creditAccountNumber = jsonTo.getString("result.accountNumber");
							creditAccountDesc = jsonTo.getString("result.accountDesc");
							creditBankName = jsonTo.getString("result.bankName");
							creditAccountTableRowIndex = jsonTo.getString("result.accountTableRowIndex");
						}
					} else {

						CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance
								.findByCheckBookIdAndIsDeleted(glBankTransferHeader.getCheckbookIdTo(), false);
						String accountNumber = "";
						String accountDesc = "";
						String bankName = "";
						String accountTableRowIndex = "";
						if (checkbookMaintenance != null
								&& checkbookMaintenance.getGlAccountsTableAccumulation() != null) {
							Object[] object= serviceAccountPayable
									.getAccountNumber(checkbookMaintenance.getGlAccountsTableAccumulation());
							accountNumber = object[0].toString();
							accountDesc = object[3].toString();
							accountTableRowIndex = checkbookMaintenance.getGlAccountsTableAccumulation()
									.getAccountTableRowIndex();
						}
						if (checkbookMaintenance != null && checkbookMaintenance.getBankSetup() != null) {
							bankName = checkbookMaintenance.getBankSetup().getBankDescription();
						}

						creditAccountNumber = accountNumber;
						creditAccountDesc = accountDesc;
						creditBankName = bankName;
						creditAccountTableRowIndex = accountTableRowIndex;

					}
				}

				DtoGLBankTransferDistribution dtoGLBankTransferDistribution = new DtoGLBankTransferDistribution(glBankTransferHeader);

				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double exchangeRateFrom = 0.0;
				Double debitAmount = glBankTransferHeader.getTransferAmountFrom();
				if (glBankTransferHeader.getExchangeRateFrom() != null) {
					exchangeRateFrom = glBankTransferHeader.getExchangeRateFrom();
				}

				String calcMethod;
				if (glBankTransferHeader.getRateCalculateMethodFrom() == 1) {
					calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
				} else {
					calcMethod = RateCalculationMethod.DIVIDE.getMethod();
				}

				Double transferAmount = (double) mathEval.eval(debitAmount + calcMethod + exchangeRateFrom);

				dtoGLBankTransferDistribution.setTransferAmount(transferAmount);
				dtoGLBankTransferDistribution.setTransferFromBank(debitBankName);
				dtoGLBankTransferDistribution.setTransferToBank(creditBankName);
				dtoGLBankTransferDistribution.setAccountTableRowIndexFrom(debitAccountTableRowIndex);
				dtoGLBankTransferDistribution.setAccountTableRowIndexTo(creditAccountTableRowIndex);
				Map<String, Object> debitDetail = new HashMap<>();
				debitDetail.put("accountNumber", debitAccountNumber);
				debitDetail.put("accountDescription", debitAccountDesc);
				debitDetail.put("debitAmount", glBankTransferHeader.getTransferAmountFrom());
				dtoGLBankTransferDistribution.setAccountDetailDebit(debitDetail);

				Map<String, Object> creditDetail = new HashMap<>();
				creditDetail.put("accountNumber", creditAccountNumber);
				creditDetail.put("accountDescription", creditAccountDesc);
				creditDetail.put("creditAmount", glBankTransferHeader.getTransferAmountTo());
				dtoGLBankTransferDistribution.setAccountDetailCredit(creditDetail);
				
				GLBankTransferEntryCharges glBankTransferEntryCharges = repositoryGLBankTransferEntryCharges.findByGlBankTransferHeaderBankTransferNumberAndIsDeleted(bankTransfernumber, false);
				if(glBankTransferEntryCharges!=null)
				{
					Map<String, Object> chargeDetail = new HashMap<>();
					GLAccountsTableAccumulation chargeAccountNumber = repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(glBankTransferEntryCharges.getAccountTableRowIndex(), false);
					chargeDetail.put("accountNumber","");
					chargeDetail.put("accountDescription","");
					if(chargeAccountNumber!=null)
					{
						Object[] object=serviceAccountPayable.getAccountNumber(chargeAccountNumber);
						chargeDetail.put("accountNumber", object[0].toString());
						if(UtilRandomKey.isNotBlank(glBankTransferEntryCharges.getChargeComments())){
							chargeDetail.put("accountDescription",glBankTransferEntryCharges.getChargeComments());
						}
						else{
							chargeDetail.put("accountDescription",object[3].toString());
						}
					}
					chargeDetail.put("debitAmount", glBankTransferEntryCharges.getChargeAmount());
					dtoGLBankTransferDistribution.setAccountDetailCharge(chargeDetail);
				}
				return dtoGLBankTransferDistribution;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	public DtoGLBankTransferDistribution saveBankTransferDistribution(
			DtoGLBankTransferDistribution dtoGLBankTransferDistribution) {
		try {
			
			
			List<GLBankTransferDistribution> distributionsList= repositoryGLBankTransferDistribution.findByGlBankTransferHeaderBankTransferNumber(dtoGLBankTransferDistribution.getBankTransferNumber());
			if(distributionsList!=null && !distributionsList.isEmpty()){
				repositoryGLBankTransferDistribution.deleteInBatch(distributionsList);
			}
			
			GLBankTransferHeader glBankTransferHeader = repositoryGLBankTransferHeader
					.findByBankTransferNumber(dtoGLBankTransferDistribution.getBankTransferNumber());

			if (glBankTransferHeader != null) {
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double exchangeRateFrom = 0.0;
				if (glBankTransferHeader.getExchangeRateFrom() != null) {
					exchangeRateFrom = Double.valueOf(glBankTransferHeader.getExchangeRateFrom());
				}
				
				Double exchangeRateTo = 0.0;
				if (glBankTransferHeader.getExchangeRateTo() != null) {
					exchangeRateTo = Double.valueOf(glBankTransferHeader.getExchangeRateTo());
				}
				
				String calcMethodFrom = RateCalculationMethod.MULTIPLY.getMethod();
				String calcMethodTo = RateCalculationMethod.MULTIPLY.getMethod();

				if (glBankTransferHeader.getRateCalculateMethodFrom() == 2) {
					calcMethodFrom = RateCalculationMethod.DIVIDE.getMethod();
				}

				if (glBankTransferHeader.getRateCalculateMethodTo() == 2) {
					calcMethodTo = RateCalculationMethod.DIVIDE.getMethod();
				}
				
				Double originalDebitAmount = glBankTransferHeader.getTransferAmountFrom();
				Double originalCreditAmount = glBankTransferHeader.getTransferAmountTo();

				Double debitAmount = (double) mathEval.eval(originalDebitAmount + calcMethodFrom + exchangeRateFrom);
				Double creditAmount = (double) mathEval.eval(originalCreditAmount + calcMethodTo + exchangeRateTo);

				GLBankTransferDistribution glBankTransferDistributionDebit = new GLBankTransferDistribution();
				glBankTransferDistributionDebit.setDebitAmount(debitAmount);
				glBankTransferDistributionDebit.setAccountType(1); // 1=From
				glBankTransferDistributionDebit
						.setAccountTableRowIndex(dtoGLBankTransferDistribution.getAccountTableRowIndexFrom());
				glBankTransferDistributionDebit.setGlBankTransferHeader(glBankTransferHeader);
				glBankTransferDistributionDebit.setOriginalDebitAmount(originalDebitAmount);
				repositoryGLBankTransferDistribution.saveAndFlush(glBankTransferDistributionDebit);

				GLBankTransferDistribution glBankTransferDistributionCredit = new GLBankTransferDistribution();
				glBankTransferDistributionCredit.setCreditAmount(creditAmount);
				glBankTransferDistributionCredit.setAccountType(2);// 2=To
				glBankTransferDistributionCredit
						.setAccountTableRowIndex(dtoGLBankTransferDistribution.getAccountTableRowIndexTo());
				glBankTransferDistributionCredit.setGlBankTransferHeader(glBankTransferHeader);
				glBankTransferDistributionCredit.setOriginalCreditAmount(originalCreditAmount);
				repositoryGLBankTransferDistribution.saveAndFlush(glBankTransferDistributionCredit);
				
				GLBankTransferEntryCharges glBankTransferEntryCharges = repositoryGLBankTransferEntryCharges.
						findByGlBankTransferHeaderBankTransferNumberAndIsDeleted(dtoGLBankTransferDistribution.getBankTransferNumber(), false);
				if(glBankTransferEntryCharges!=null)
				{
					Double debitChargeAmount = (double) mathEval.eval(glBankTransferEntryCharges.getChargeAmount() + calcMethodFrom + exchangeRateFrom);
					GLBankTransferDistribution glBankTransferDistributionDebitCharge = new GLBankTransferDistribution();
					glBankTransferDistributionDebitCharge.setDebitAmount(debitChargeAmount);
					glBankTransferDistributionDebitCharge.setAccountType(3);// 3=Charge
					glBankTransferDistributionDebitCharge
							.setAccountTableRowIndex(glBankTransferEntryCharges.getAccountTableRowIndex());
					glBankTransferDistributionDebitCharge.setGlBankTransferHeader(glBankTransferHeader);
					glBankTransferDistributionDebitCharge.setOriginalCreditAmount(glBankTransferEntryCharges.getChargeAmount());
					repositoryGLBankTransferDistribution.saveAndFlush(glBankTransferDistributionDebitCharge);
					
				}
				return getBankTransferDistrubutionByBankTransferNumber(dtoGLBankTransferDistribution.getBankTransferNumber());
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}

		return null;

	}

	public DtoGLBankTransferCharges getBankTransferChargesByBankTransferNumber(
			DtoGLBankTransferCharges dtoGLBankTransferCharges) {
		try {
			GLBankTransferEntryCharges bankTransferEntryCharges = repositoryGLBankTransferEntryCharges
					.findByGlBankTransferHeaderBankTransferNumberAndIsDeleted(
							dtoGLBankTransferCharges.getBankTransferNumber(), false);
			if (bankTransferEntryCharges != null) {
				return new DtoGLBankTransferCharges(bankTransferEntryCharges);
			} else {
				GLBankTransferHeader bankTransferHeader = repositoryGLBankTransferHeader
						.findByBankTransferNumber(dtoGLBankTransferCharges.getBankTransferNumber());
				if (bankTransferHeader != null) {
					DtoGLBankTransferCharges dtoGLBankTransferCharges2 = new DtoGLBankTransferCharges(
							bankTransferHeader);
					String tenantIdFrom = serviceHome
							.getCompanyDetail(Integer.parseInt(dtoGLBankTransferCharges2.getCompanyID()));
					dtoGLBankTransferCharges2.setTenantId(tenantIdFrom);
					return dtoGLBankTransferCharges2;
				}
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoGLBankTransferCharges getBankTransferChargesByChargeNumber(DtoGLBankTransferCharges dtoGLBankTransferCharges) {
		try {
			GLBankTransferEntryCharges bankTransferEntryCharges = repositoryGLBankTransferEntryCharges
					.findByChargeNumberAndIsDeleted(dtoGLBankTransferCharges.getChargeNumber(), false);
			if (bankTransferEntryCharges != null) {
				dtoGLBankTransferCharges=new DtoGLBankTransferCharges(bankTransferEntryCharges);
				return dtoGLBankTransferCharges;
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoGLBankTransferCharges saveBankTransferCharges(DtoGLBankTransferCharges dtoGLBankTransferCharges) {
		try {
			
			GLBankTransferEntryCharges bankTransferEntryCharges = repositoryGLBankTransferEntryCharges
					.findByGlBankTransferHeaderBankTransferNumberAndIsDeleted(dtoGLBankTransferCharges.getBankTransferNumber(), false);
			if (bankTransferEntryCharges == null) {
				bankTransferEntryCharges = new GLBankTransferEntryCharges();
			}
			
			bankTransferEntryCharges.setChargeAmount(dtoGLBankTransferCharges.getChargeAmount());
			bankTransferEntryCharges.setChargeComments(dtoGLBankTransferCharges.getChargeComments());
			bankTransferEntryCharges.setChargeNumber(dtoGLBankTransferCharges.getChargeNumber());
			bankTransferEntryCharges.setCheckbookId(dtoGLBankTransferCharges.getCheckbookID());
			bankTransferEntryCharges.setAccountTableRowIndex(dtoGLBankTransferCharges.getAccountNumber());
			bankTransferEntryCharges.setGlBankTransferHeader(repositoryGLBankTransferHeader
					.findByBankTransferNumber(dtoGLBankTransferCharges.getBankTransferNumber()));
			bankTransferEntryCharges.setIsDeleted(false);
			repositoryGLBankTransferEntryCharges.saveAndFlush(bankTransferEntryCharges);
			return dtoGLBankTransferCharges;
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}
	
	//@Transactional
	public Boolean postBankTransferEntry(DtoGLBankTransferHeader dtoGLBankTransferHeader)  {

		try {
			int langid = serviceHome.getLanngugaeId();
			// Save in gl90400
			GLYTDOpenBankTransactionsEntryHeader glytdOpenTransactionEntryHeader = new GLYTDOpenBankTransactionsEntryHeader();
			glytdOpenTransactionEntryHeader.setBankTransactionEntryID(String.valueOf(dtoGLBankTransferHeader.getBankTransferNumber()));
			
			glytdOpenTransactionEntryHeader.setBankTransactionType(
					repositoryBankTransactionType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
					(BankTransactionTypeConstant.BANK_TRANSFER.getIndex(), langid, false));
			glytdOpenTransactionEntryHeader.setBankTransactionEntryType(
					repositoryBankTransactionEntryType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
					(BankTransactionEntryTypeConstant.TRANSACTION.getIndex(), langid, false));
			glytdOpenTransactionEntryHeader.setBankTransactionEntryAction(null);
			glytdOpenTransactionEntryHeader.setBankTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoGLBankTransferHeader.getTransferDate()));
			glytdOpenTransactionEntryHeader.setCheckbookId(dtoGLBankTransferHeader.getCheckbookIDFrom());
			glytdOpenTransactionEntryHeader.setCurrencyId(dtoGLBankTransferHeader.getCurrencyID());			
			glytdOpenTransactionEntryHeader.setExchangeTableIndex(dtoGLBankTransferHeader.getExchangeTableID());
			glytdOpenTransactionEntryHeader.setExchangeRate(dtoGLBankTransferHeader.getExchangeRateFrom());
			glytdOpenTransactionEntryHeader.setBankTransactionDescription(dtoGLBankTransferHeader.getCommentFrom());
			glytdOpenTransactionEntryHeader.setBankTransactionAmount(dtoGLBankTransferHeader.getTransferAmountFrom());
			glytdOpenTransactionEntryHeader.setPostingDate(new Date());
			glytdOpenTransactionEntryHeader.setPostingbyUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
			glytdOpenTransactionEntryHeader.setRowDateindex(new Date());
			glytdOpenTransactionEntryHeader = repositoryGLYTDOpenBankTransactionsEntryHeader
					.saveAndFlush(glytdOpenTransactionEntryHeader);
			
			
			// Save in gl90401
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			Double currentExchangeRateFrom = dtoGLBankTransferHeader.getExchangeRateFrom();
			Double currentExchangeRateTo=dtoGLBankTransferHeader.getExchangeRateTo();
			String calcMethodFrom = RateCalculationMethod.MULTIPLY.getMethod();
			String calcMethodTo = RateCalculationMethod.MULTIPLY.getMethod();
			
			if(dtoGLBankTransferHeader.getRateCalculateMethodFrom()==2){
				calcMethodFrom=RateCalculationMethod.DIVIDE.getMethod();
			}
			
			if(dtoGLBankTransferHeader.getRateCalculateMethodTo()==2){
				calcMethodTo=RateCalculationMethod.DIVIDE.getMethod();
			}

			Double originalDebitAmount = dtoGLBankTransferHeader.getTransferAmountFrom();
			Double originalCreditAmount = dtoGLBankTransferHeader.getTransferAmountTo();
			Double debitAmount = (double) mathEval.eval(originalDebitAmount + calcMethodFrom + currentExchangeRateFrom);
			Double creditAmount = (double) mathEval.eval(originalCreditAmount + calcMethodTo + currentExchangeRateTo);
			
			// save in 90401
			String debitAccountTableRowIndex = "0";
			String creditAccountTableRowIndex = "0";
			
			String accountNumberWithZeroFrom="";
			String accountNumberWithZeroTo="";
			
			String tenantIdFrom = serviceHome
					.getCompanyDetail(Integer.parseInt(dtoGLBankTransferHeader.getCompanyTransferFrom()));
			JsonPath jsonFrom = null;
			
			if (tenantIdFrom != null) {
				String headerTenant = httpServletRequest.getHeader("tenantid");
				if (!headerTenant.equalsIgnoreCase(tenantIdFrom)) {
					jsonFrom = serviceHome.getBankDistributionDetailByCheckbookId(
							dtoGLBankTransferHeader.getCheckbookIDFrom(), tenantIdFrom);
					if (jsonFrom != null) {
						debitAccountTableRowIndex = jsonFrom.getString("result.accountTableRowIndex");
						accountNumberWithZeroFrom= jsonFrom.getString("result.accountNumberWithZero");
					}
				} else {
					CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance
							.findByCheckBookIdAndIsDeleted(dtoGLBankTransferHeader.getCheckbookIDFrom(), false);
					if (checkbookMaintenance != null
							&& checkbookMaintenance.getGlAccountsTableAccumulation() != null) {
						debitAccountTableRowIndex = checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex();
						accountNumberWithZeroFrom=serviceAccountPayable.getAccountNumberIdsWithZero(checkbookMaintenance.getGlAccountsTableAccumulation());
				    }
				}
			}
			
			String tenantIdTo = serviceHome
					.getCompanyDetail(Integer.parseInt(dtoGLBankTransferHeader.getCompanyTransferTo()));
			JsonPath jsonTo = null;
			if (tenantIdTo != null) 
			{
				String headerTenant = httpServletRequest.getHeader("tenantid");
				if (!headerTenant.equalsIgnoreCase(tenantIdTo)) 
				{
					jsonTo = serviceHome.getBankDistributionDetailByCheckbookId(
							dtoGLBankTransferHeader.getCheckbookIDTo(), tenantIdTo);
					if (jsonTo != null) {
						creditAccountTableRowIndex = jsonTo.getString("result.accountTableRowIndex");
						accountNumberWithZeroTo=jsonTo.getString("result.accountNumberWithZero");
					}
				}
				else 
				{
					CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance
							.findByCheckBookIdAndIsDeleted(dtoGLBankTransferHeader.getCheckbookIDTo(), false);
					if (checkbookMaintenance != null
							&& checkbookMaintenance.getGlAccountsTableAccumulation() != null) {
						creditAccountTableRowIndex = checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex();
						accountNumberWithZeroTo=serviceAccountPayable.getAccountNumberIdsWithZero(checkbookMaintenance.getGlAccountsTableAccumulation());
				    }
				}
			}

			GLYTDOpenBankTransactionsEntryDetails glytdBankEntryDetailDebit = new GLYTDOpenBankTransactionsEntryDetails();
			glytdBankEntryDetailDebit.setGlytdOpenBankTransactionsEntryHeader(glytdOpenTransactionEntryHeader);
			glytdBankEntryDetailDebit.setDebitAmount(debitAmount);
			glytdBankEntryDetailDebit.setOriginalDebitAmount(originalDebitAmount);
			glytdBankEntryDetailDebit.setAccountTableRowIndex(Integer.parseInt(debitAccountTableRowIndex));
			glytdBankEntryDetailDebit.setDistributionDescription(dtoGLBankTransferHeader.getCommentFrom());
			glytdBankEntryDetailDebit.setRowDateindex(new Date());
			repositoryGLYTDOpenBankTransactionsEntryDetails.saveAndFlush(glytdBankEntryDetailDebit);
			
			GLYTDOpenBankTransactionsEntryDetails glytdBankEntryDetailCredit = new GLYTDOpenBankTransactionsEntryDetails();
			glytdBankEntryDetailCredit.setGlytdOpenBankTransactionsEntryHeader(glytdOpenTransactionEntryHeader);
			glytdBankEntryDetailCredit.setCreditAmount(creditAmount);
			glytdBankEntryDetailCredit.setOriginalCreditAmount(originalCreditAmount);
			glytdBankEntryDetailCredit.setAccountTableRowIndex(Integer.parseInt(creditAccountTableRowIndex));
			glytdBankEntryDetailCredit.setDistributionDescription(dtoGLBankTransferHeader.getCommentTo());
			glytdBankEntryDetailCredit.setRowDateindex(new Date());
			repositoryGLYTDOpenBankTransactionsEntryDetails.saveAndFlush(glytdBankEntryDetailCredit);
			
			GLBankTransferEntryCharges glBankTransferEntryCharges = repositoryGLBankTransferEntryCharges.
					findByGlBankTransferHeaderBankTransferNumberAndIsDeleted(dtoGLBankTransferHeader.getBankTransferNumber(), false);
			Double debitAmountCharges=0.0;
			if(glBankTransferEntryCharges!=null)
			{
				debitAmountCharges = (double) mathEval.eval(glBankTransferEntryCharges.getChargeAmount() + calcMethodFrom + currentExchangeRateFrom);
				GLYTDOpenBankTransactionsEntryDetails glytdBankEntryDetailCharges = new GLYTDOpenBankTransactionsEntryDetails();
				glytdBankEntryDetailCharges.setGlytdOpenBankTransactionsEntryHeader(glytdOpenTransactionEntryHeader);
				glytdBankEntryDetailCharges.setDebitAmount(debitAmountCharges);
				glytdBankEntryDetailCharges.setOriginalCreditAmount(glBankTransferEntryCharges.getChargeAmount());
				glytdBankEntryDetailCharges.setAccountTableRowIndex(Integer.parseInt(glBankTransferEntryCharges.getAccountTableRowIndex()));
				glytdBankEntryDetailCharges.setDistributionDescription(glBankTransferEntryCharges.getChargeComments());
				glytdBankEntryDetailCharges.setRowDateindex(new Date());
				repositoryGLYTDOpenBankTransactionsEntryDetails.saveAndFlush(glytdBankEntryDetailCharges);
			}
			// save in 90200
			int year = 0;
			int periodId = 0;
			Calendar cal = Calendar.getInstance();
			if (UtilRandomKey.isNotBlank(dtoGLBankTransferHeader.getTransferDate())) {
				Date transDate = UtilDateAndTime.ddmmyyyyStringToDate(dtoGLBankTransferHeader.getTransferDate());
				cal.setTime(transDate);
				year = cal.get(Calendar.YEAR);
				periodId = cal.get(Calendar.MONTH);
			}

			GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
			int nextJournalId = 0;
			if (glConfigurationSetup != null) {
				nextJournalId = glConfigurationSetup.getNextJournalEntryVoucher();
			}

			// Save for Debit
			GLYTDOpenTransactions glytdOpenTransactionsDebit = new GLYTDOpenTransactions();
			glytdOpenTransactionsDebit.setOpenYear(year);
			glytdOpenTransactionsDebit.setJournalEntryID(String.valueOf(nextJournalId));
			glytdOpenTransactionsDebit.setTransactionSource("BT");
			glytdOpenTransactionsDebit.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
					.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(
							AuditTrailSeriesTypeConstant.GL.getIndex(),false));
			glytdOpenTransactionsDebit.setJournalDescription(dtoGLBankTransferHeader.getCommentFrom());
			if (UtilRandomKey.isNotBlank(dtoGLBankTransferHeader.getTransferDate())) {
				glytdOpenTransactionsDebit.setTransactionDate(
						UtilDateAndTime.ddmmyyyyStringToDate(dtoGLBankTransferHeader.getTransferDate()));
			}
			glytdOpenTransactionsDebit.setTransactionPostingDate(new Date());
			glytdOpenTransactionsDebit.setAccountTableRowIndex(String.valueOf(debitAccountTableRowIndex));
			glytdOpenTransactionsDebit.setUserCreateTransaction(httpServletRequest.getHeader(CommonConstant.USER_ID));
			glytdOpenTransactionsDebit.setUserPostingTransaction(httpServletRequest.getHeader(CommonConstant.USER_ID));
			glytdOpenTransactionsDebit.setModuleSeriesNumber(repositoryGLConfigurationAuditTrialCodes
					.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(
							AuditTrailSeriesTypeConstant.GL.getIndex(),false));
			glytdOpenTransactionsDebit.setOriginalTransactionNumber(String.valueOf(dtoGLBankTransferHeader.getBankTransferNumber()));
			glytdOpenTransactionsDebit.setCurrencyId(dtoGLBankTransferHeader.getCurrencyID());
			glytdOpenTransactionsDebit.setExchangeTableIndex(Integer.parseInt(dtoGLBankTransferHeader.getExchangeTableID()));
			glytdOpenTransactionsDebit.setExchangeTableRate(dtoGLBankTransferHeader.getExchangeRateFrom());
			glytdOpenTransactionsDebit.setDebitAmount(debitAmount);
			glytdOpenTransactionsDebit.setOriginalDebitAmount(originalDebitAmount);
			glytdOpenTransactionsDebit.setRowDateIndex(new Date());
			repositoryGlytdOpenTransaction.saveAndFlush(glytdOpenTransactionsDebit);

			// save for credit
			GLYTDOpenTransactions glytdOpenTransactionsCredit = new GLYTDOpenTransactions();
			glytdOpenTransactionsCredit.setOpenYear(year);
			glytdOpenTransactionsCredit.setJournalEntryID(String.valueOf(nextJournalId));
			glytdOpenTransactionsCredit.setTransactionSource("BT");
			glytdOpenTransactionsCredit.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
					.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
							false));
			glytdOpenTransactionsCredit.setJournalDescription(dtoGLBankTransferHeader.getCommentTo());
			if (UtilRandomKey.isNotBlank(dtoGLBankTransferHeader.getTransferDate())) {
				glytdOpenTransactionsCredit.setTransactionDate(
						UtilDateAndTime.ddmmyyyyStringToDate(dtoGLBankTransferHeader.getTransferDate()));
			}
			glytdOpenTransactionsCredit.setTransactionPostingDate(new Date());
			glytdOpenTransactionsCredit.setAccountTableRowIndex(String.valueOf(creditAccountTableRowIndex));
			glytdOpenTransactionsCredit.setUserCreateTransaction(httpServletRequest.getHeader("userid"));
			glytdOpenTransactionsCredit.setUserPostingTransaction(httpServletRequest.getHeader("userid"));
			glytdOpenTransactionsCredit.setModuleSeriesNumber(repositoryGLConfigurationAuditTrialCodes
					.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
							false));
			glytdOpenTransactionsCredit.setOriginalTransactionNumber(String.valueOf(dtoGLBankTransferHeader.getBankTransferNumber()));
			glytdOpenTransactionsCredit.setCurrencyId(dtoGLBankTransferHeader.getCurrencyID());
			glytdOpenTransactionsCredit
					.setExchangeTableIndex(Integer.parseInt(dtoGLBankTransferHeader.getExchangeTableID()));
			glytdOpenTransactionsCredit.setExchangeTableRate(dtoGLBankTransferHeader.getExchangeRateTo());
			glytdOpenTransactionsCredit.setCreditAmount(creditAmount);
			glytdOpenTransactionsCredit.setOriginalCreditAmount(originalCreditAmount);
			glytdOpenTransactionsCredit.setRowDateIndex(new Date());
			repositoryGlytdOpenTransaction.saveAndFlush(glytdOpenTransactionsCredit);

			// save for Charges
			if(glBankTransferEntryCharges!=null)
			{
				GLYTDOpenTransactions glytdOpenTransactionsDebitCharges = new GLYTDOpenTransactions();
				glytdOpenTransactionsDebitCharges.setOpenYear(year);
				glytdOpenTransactionsDebitCharges.setJournalEntryID(String.valueOf(nextJournalId));
				glytdOpenTransactionsDebitCharges.setTransactionSource("BT");
				glytdOpenTransactionsDebitCharges.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
						.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
								false));
				glytdOpenTransactionsDebitCharges.setJournalDescription(glBankTransferEntryCharges.getChargeComments());
				if (UtilRandomKey.isNotBlank(dtoGLBankTransferHeader.getTransferDate())) {
					glytdOpenTransactionsDebitCharges.setTransactionDate(
							UtilDateAndTime.ddmmyyyyStringToDate(dtoGLBankTransferHeader.getTransferDate()));
				}
				glytdOpenTransactionsDebitCharges.setTransactionPostingDate(new Date());
				glytdOpenTransactionsDebitCharges.setAccountTableRowIndex(String.valueOf(glBankTransferEntryCharges.getAccountTableRowIndex()));
				glytdOpenTransactionsDebitCharges.setUserCreateTransaction(httpServletRequest.getHeader("userid"));
				glytdOpenTransactionsDebitCharges.setUserPostingTransaction(httpServletRequest.getHeader("userid"));
				glytdOpenTransactionsDebitCharges.setModuleSeriesNumber(repositoryGLConfigurationAuditTrialCodes
						.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
								false));
				glytdOpenTransactionsDebitCharges.setOriginalTransactionNumber(String.valueOf(dtoGLBankTransferHeader.getBankTransferNumber()));
				glytdOpenTransactionsDebitCharges.setCurrencyId(dtoGLBankTransferHeader.getCurrencyID());
				glytdOpenTransactionsDebitCharges
						.setExchangeTableIndex(Integer.parseInt(dtoGLBankTransferHeader.getExchangeTableID()));
				glytdOpenTransactionsDebitCharges.setExchangeTableRate(dtoGLBankTransferHeader.getExchangeRateFrom());
				glytdOpenTransactionsDebitCharges.setCreditAmount(debitAmountCharges);
				glytdOpenTransactionsDebitCharges.setOriginalCreditAmount(glBankTransferEntryCharges.getChargeAmount());
				glytdOpenTransactionsDebitCharges.setRowDateIndex(new Date());
				repositoryGlytdOpenTransaction.saveAndFlush(glytdOpenTransactionsDebitCharges);
			}
			
			// Update Gl Configuration Setup for net Journal entry Id
			if (glConfigurationSetup != null) {
				glConfigurationSetup.setNextJournalEntryVoucher(nextJournalId + 1);
				repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);
			}

			int dimIndex1 = 0;
			int dimIndex2 = 0;
			int dimIndex3 = 0;
			int dimIndex4 = 0;
			int dimIndex5 = 0;
			int mainAccountIndex = 0;

			if (UtilRandomKey.isNotBlank(accountNumberWithZeroFrom)) {
				String[] accountNo = accountNumberWithZeroFrom.split("-");
				for (int i = 0; i < accountNo.length; i++) {
					if (i > 0) {
						if (i == 1) {
							dimIndex1 = Integer.parseInt(accountNo[i]);
						}
						if (i == 2) {
							dimIndex2 = Integer.parseInt(accountNo[i]);
						}
						if (i == 3) {
							dimIndex3 = Integer.parseInt(accountNo[i]);
						}
						if (i == 4) {
							dimIndex4 = Integer.parseInt(accountNo[i]);
						}
						if (i == 5) {
							dimIndex5 = Integer.parseInt(accountNo[i]);
						}
					}
				}
				mainAccountIndex = Integer.parseInt(accountNo[0]);
			}

			GLCurrentSummaryMasterTableByMainAccountKey currSumaryByMainAccKey = null;
			GLCurrentSummaryMasterTableByMainAccount currSumaryByMainAcc = null;
			GLCurrentSummaryMasterTableByAccountNumberKey currSumaryByAccNumberKey = null;
			GLCurrentSummaryMasterTableByAccountNumber currSumaryByAccNumber = null;
			GLCurrentSummaryMasterTableByDimensionsKey glCurreSummaryByDimInxKey = null;
			GLCurrentSummaryMasterTableByDimensions glCurreSummaryByDimInx = null;
			
			// Save For debt gl90110, gl90111, gl90112 and find by composite key
			currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
			currSumaryByMainAccKey.setYear(year);
			currSumaryByMainAccKey.setPeriodID(periodId + 1);
			currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
			currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount.findOne(currSumaryByMainAccKey);
			if (currSumaryByMainAcc != null) {
				currSumaryByMainAcc.setDebitAmount(currSumaryByMainAcc.getDebitAmount() + debitAmount);
				currSumaryByMainAcc
						.setPeriodBalance(currSumaryByMainAcc.getDebitAmount() - currSumaryByMainAcc.getCreditAmount());
			} else {
				currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
				currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
				currSumaryByMainAcc.setYear(year);
				currSumaryByMainAcc.setPeriodID(periodId + 1);
				currSumaryByMainAcc.setDebitAmount(debitAmount);
				currSumaryByMainAcc.setPeriodBalance(debitAmount - 0);
			}

			currSumaryByMainAcc.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);

			// Save data in gl90111
			currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
			currSumaryByAccNumberKey.setYear(year);
			currSumaryByAccNumberKey.setAccountTableRowIndex(Integer.parseInt(debitAccountTableRowIndex));
			currSumaryByAccNumberKey.setPeriodID(periodId + 1);
			currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
					.findOne(currSumaryByAccNumberKey);

			if (currSumaryByAccNumber != null) {
				currSumaryByAccNumber.setDebitAmount(currSumaryByAccNumber.getDebitAmount() + debitAmount);
				currSumaryByAccNumber.setPeriodBalance(
						currSumaryByAccNumber.getDebitAmount() - currSumaryByAccNumber.getCreditAmount());
			} else {
				currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
				currSumaryByAccNumber.setAccountTableRowIndex(Integer.parseInt(debitAccountTableRowIndex));
				currSumaryByAccNumber.setYear(year);
				currSumaryByAccNumber.setPeriodID(periodId + 1);
				currSumaryByAccNumber.setDebitAmount(debitAmount);
				currSumaryByAccNumber.setPeriodBalance(debitAmount - 0);
			}
			currSumaryByAccNumber.setIsDeleted(false);
			currSumaryByAccNumber.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

			// Save data in gl90112
			glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
			glCurreSummaryByDimInxKey.setYear(year);
			glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
			glCurreSummaryByDimInxKey.setPeriodID(periodId + 1);
			glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
					.findOne(glCurreSummaryByDimInxKey);

			if (glCurreSummaryByDimInx != null) {
				glCurreSummaryByDimInx.setDebitAmount(glCurreSummaryByDimInx.getDebitAmount() + debitAmount);
				glCurreSummaryByDimInx.setPeriodBalance(
						glCurreSummaryByDimInx.getDebitAmount() - glCurreSummaryByDimInx.getCreditAmount());
			} else {
				glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
				glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
				glCurreSummaryByDimInx.setYear(year);
				glCurreSummaryByDimInx.setPeriodID(periodId + 1);
				glCurreSummaryByDimInx.setDebitAmount(debitAmount);
				glCurreSummaryByDimInx.setPeriodBalance(debitAmount - 0);
			}

			glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
			glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
			glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
			glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
			glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
			glCurreSummaryByDimInx.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);
			
			
			
			if(glBankTransferEntryCharges!=null)
			{
				GLAccountsTableAccumulation glAccountsTableAccumulation= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(glBankTransferEntryCharges.getAccountTableRowIndex(),false);
				if(glAccountsTableAccumulation!=null)
				{
					String accountNumberCharge = serviceAccountPayable.getAccountNumberIdsWithZero(glAccountsTableAccumulation);
					if (UtilRandomKey.isNotBlank(accountNumberCharge)) 
					{
						String[] accountNo = accountNumberCharge.split("-");
						for (int i = 0; i < accountNo.length; i++) {
							if (i > 0) {
								if (i == 1) {
									dimIndex1 = Integer.parseInt(accountNo[i]);
								}
								if (i == 2) {
									dimIndex2 = Integer.parseInt(accountNo[i]);
								}
								if (i == 3) {
									dimIndex3 = Integer.parseInt(accountNo[i]);
								}
								if (i == 4) {
									dimIndex4 = Integer.parseInt(accountNo[i]);
								}
								if (i == 5) {
									dimIndex5 = Integer.parseInt(accountNo[i]);
								}
							}
						}
						mainAccountIndex = Integer.parseInt(accountNo[0]);
					}

					
					
					// Save For debt gl90110, gl90111, gl90112 and find by composite key
					currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
					currSumaryByMainAccKey.setYear(year);
					currSumaryByMainAccKey.setPeriodID(periodId + 1);
					currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
					currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount.findOne(currSumaryByMainAccKey);
					if (currSumaryByMainAcc != null) {
						currSumaryByMainAcc.setDebitAmount(currSumaryByMainAcc.getDebitAmount() + debitAmountCharges);
						currSumaryByMainAcc
								.setPeriodBalance(currSumaryByMainAcc.getDebitAmount() - currSumaryByMainAcc.getCreditAmount());
					} else {
						currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
						currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
						currSumaryByMainAcc.setYear(year);
						currSumaryByMainAcc.setPeriodID(periodId + 1);
						currSumaryByMainAcc.setDebitAmount(debitAmountCharges);
						currSumaryByMainAcc.setPeriodBalance(debitAmountCharges - 0);
					}

					currSumaryByMainAcc.setRowDateIndex(new Date());
					repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);
					// Save data in gl90111
					currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
					currSumaryByAccNumberKey.setYear(year);
					currSumaryByAccNumberKey.setAccountTableRowIndex(Integer.parseInt(glBankTransferEntryCharges.getAccountTableRowIndex()));
					currSumaryByAccNumberKey.setPeriodID(periodId + 1);
					currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
							.findOne(currSumaryByAccNumberKey);

					if (currSumaryByAccNumber != null) {
						currSumaryByAccNumber.setDebitAmount(currSumaryByAccNumber.getDebitAmount() + debitAmountCharges);
						currSumaryByAccNumber.setPeriodBalance(
								currSumaryByAccNumber.getDebitAmount() - currSumaryByAccNumber.getCreditAmount());
					} else {
						currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
						currSumaryByAccNumber.setAccountTableRowIndex(Integer.parseInt(glBankTransferEntryCharges.getAccountTableRowIndex()));
						currSumaryByAccNumber.setYear(year);
						currSumaryByAccNumber.setPeriodID(periodId + 1);
						currSumaryByAccNumber.setDebitAmount(debitAmountCharges);
						currSumaryByAccNumber.setPeriodBalance(debitAmountCharges - 0);
					}
					currSumaryByAccNumber.setIsDeleted(false);
					currSumaryByAccNumber.setRowDateIndex(new Date());
					repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

					// Save data in gl90112
					glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
					glCurreSummaryByDimInxKey.setYear(year);
					glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
					glCurreSummaryByDimInxKey.setPeriodID(periodId + 1);
					glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
							.findOne(glCurreSummaryByDimInxKey);

					if (glCurreSummaryByDimInx != null) {
						glCurreSummaryByDimInx.setDebitAmount(glCurreSummaryByDimInx.getDebitAmount() + debitAmountCharges);
						glCurreSummaryByDimInx.setPeriodBalance(
								glCurreSummaryByDimInx.getDebitAmount() - glCurreSummaryByDimInx.getCreditAmount());
					} else {
						glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
						glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
						glCurreSummaryByDimInx.setYear(year);
						glCurreSummaryByDimInx.setPeriodID(periodId + 1);
						glCurreSummaryByDimInx.setDebitAmount(debitAmountCharges);
						glCurreSummaryByDimInx.setPeriodBalance(debitAmountCharges - 0);
					}

					glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
					glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
					glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
					glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
					glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
					glCurreSummaryByDimInx.setRowDateIndex(new Date());
					repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);
				}
			}
			
			

			// Account Number for credit
			if (UtilRandomKey.isNotBlank(accountNumberWithZeroTo)) {
				String[] accountNo = accountNumberWithZeroTo.split("-");
				for (int i = 0; i < accountNo.length; i++) {
					if (i > 0) {
						if (i == 1) {
							dimIndex1 = Integer.parseInt(accountNo[i]);
						}
						if (i == 2) {
							dimIndex2 = Integer.parseInt(accountNo[i]);
						}
						if (i == 3) {
							dimIndex3 = Integer.parseInt(accountNo[i]);
						}
						if (i == 4) {
							dimIndex4 = Integer.parseInt(accountNo[i]);
						}
						if (i == 5) {
							dimIndex5 = Integer.parseInt(accountNo[i]);
						}
					}
				}
				mainAccountIndex = Integer.parseInt(accountNo[0]);
			}

			currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
			currSumaryByMainAccKey.setYear(year);
			currSumaryByMainAccKey.setPeriodID(periodId + 1);
			currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
			currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount.findOne(currSumaryByMainAccKey);
			if (currSumaryByMainAcc != null) {
				currSumaryByMainAcc.setCreditAmount(currSumaryByMainAcc.getCreditAmount() + creditAmount);
				currSumaryByMainAcc
						.setPeriodBalance(currSumaryByMainAcc.getDebitAmount() - currSumaryByMainAcc.getCreditAmount());
			} else {
				currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
				currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
				currSumaryByMainAcc.setYear(year);
				currSumaryByMainAcc.setPeriodID(periodId + 1);
				currSumaryByMainAcc.setCreditAmount(creditAmount);
				currSumaryByMainAcc.setPeriodBalance(0 - creditAmount);
			}

			currSumaryByMainAcc.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);

			// Save data in gl90111

			currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
			currSumaryByAccNumberKey.setYear(year);
			currSumaryByAccNumberKey.setAccountTableRowIndex(Integer.parseInt(creditAccountTableRowIndex));
			currSumaryByAccNumberKey.setPeriodID(periodId + 1);
			currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
					.findOne(currSumaryByAccNumberKey);
			if (currSumaryByAccNumber != null) {
				currSumaryByAccNumber.setCreditAmount(currSumaryByAccNumber.getCreditAmount() + creditAmount);
				currSumaryByAccNumber.setPeriodBalance(
						currSumaryByAccNumber.getDebitAmount() - currSumaryByAccNumber.getCreditAmount());
			} else {
				currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
				currSumaryByAccNumber.setAccountTableRowIndex(Integer.parseInt(creditAccountTableRowIndex));
				currSumaryByAccNumber.setYear(year);
				currSumaryByAccNumber.setPeriodID(periodId + 1);
				currSumaryByAccNumber.setCreditAmount(creditAmount);
				currSumaryByAccNumber.setPeriodBalance(0 - creditAmount);
			}
			currSumaryByAccNumber.setIsDeleted(false);
			currSumaryByAccNumber.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

			// Save data in gl90112
			glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
			glCurreSummaryByDimInxKey.setYear(year);
			glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
			glCurreSummaryByDimInxKey.setPeriodID(periodId + 1);
			glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
					.findOne(glCurreSummaryByDimInxKey);
			if (glCurreSummaryByDimInx != null) {
				glCurreSummaryByDimInx.setCreditAmount(glCurreSummaryByDimInx.getCreditAmount() + creditAmount);
				glCurreSummaryByDimInx.setPeriodBalance(
						glCurreSummaryByDimInx.getDebitAmount() - glCurreSummaryByDimInx.getCreditAmount());
			} else {
				glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
				glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
				glCurreSummaryByDimInx.setYear(year);
				glCurreSummaryByDimInx.setPeriodID(periodId + 1);
				glCurreSummaryByDimInx.setCreditAmount(creditAmount);
				glCurreSummaryByDimInx.setPeriodBalance(0 - creditAmount);
			}

			glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
			glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
			glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
			glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
			glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
			glCurreSummaryByDimInx.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);

			
			//Delete from GL Bank Transfer Tables (GL10500,GL10501,GL10502) (Charge Entry Need To be discussed GL10503)
			GLBankTransferHeader glBankTransferHeader = repositoryGLBankTransferHeader.findByBankTransferNumber(dtoGLBankTransferHeader.getBankTransferNumber());
			if (glBankTransferHeader != null) 
			{
				List<GLBankTransferDistribution> glBankTransferDistribution= repositoryGLBankTransferDistribution.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
				if(glBankTransferDistribution!=null && !glBankTransferDistribution.isEmpty()){
					repositoryGLBankTransferDistribution.deleteInBatch(glBankTransferDistribution);
				}
				
				List<GLBankTransferEntryCharges> glBankTransferEntryChargesDelete= repositoryGLBankTransferEntryCharges.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
				if(glBankTransferEntryChargesDelete!=null && !glBankTransferEntryChargesDelete.isEmpty()){
					repositoryGLBankTransferEntryCharges.deleteInBatch(glBankTransferEntryChargesDelete);
				}
				
				List<GLBankTransferDetails> glBankTransferDetailsList= repositoryGLBankTransferDetails.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
				if(glBankTransferDetailsList!=null && !glBankTransferDetailsList.isEmpty()){
					repositoryGLBankTransferDetails.deleteInBatch(glBankTransferDetailsList);
				}
				
				List<GLBankTransferHeader> list = new ArrayList<>();
				list.add(glBankTransferHeader);
				repositoryGLBankTransferHeader.deleteInBatch(list);
			}
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return false;
		}
	}
	
	public List<DtoGLBankTransferHeader> getBankTransferNumbersList(){
		List<GLBankTransferHeader> glBankTransferHeadersList = repositoryGLBankTransferHeader.findAll();
		List<DtoGLBankTransferHeader> list = new ArrayList<>();
		if(glBankTransferHeadersList!=null && !glBankTransferHeadersList.isEmpty()){
			for (GLBankTransferHeader glBankTransferHeader : glBankTransferHeadersList) {
				DtoGLBankTransferHeader dtoGLBankTransferHeader= new DtoGLBankTransferHeader();
				dtoGLBankTransferHeader.setBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
				list.add(dtoGLBankTransferHeader);
			}
		}
		return list;
	}

	public boolean deleteBankTransferEntry(DtoGLBankTransferCharges dtoGLBankTransferCharges) {
		try{
		GLBankTransferHeader glBankTransferHeader = repositoryGLBankTransferHeader.findByBankTransferNumber(dtoGLBankTransferCharges.getBankTransferNumber());
		if (glBankTransferHeader != null) 
		{
			List<GLBankTransferDistribution> glBankTransferDistribution= repositoryGLBankTransferDistribution.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
			if(glBankTransferDistribution!=null && !glBankTransferDistribution.isEmpty()){
				repositoryGLBankTransferDistribution.deleteInBatch(glBankTransferDistribution);
			}
			
			List<GLBankTransferEntryCharges> glBankTransferEntryCharges= repositoryGLBankTransferEntryCharges.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
			if(glBankTransferEntryCharges!=null && !glBankTransferEntryCharges.isEmpty()){
				repositoryGLBankTransferEntryCharges.deleteInBatch(glBankTransferEntryCharges);
			}
			
			List<GLBankTransferDetails> glBankTransferDetailsList= repositoryGLBankTransferDetails.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
			if(glBankTransferDetailsList!=null && !glBankTransferDetailsList.isEmpty()){
				repositoryGLBankTransferDetails.deleteInBatch(glBankTransferDetailsList);
			}
			
			List<GLBankTransferHeader> list = new ArrayList<>();
			list.add(glBankTransferHeader);
			repositoryGLBankTransferHeader.deleteInBatch(list);
		}
		return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public boolean deleteBankTransferEntryCharges(DtoGLBankTransferCharges dtoGLBankTransferCharges) {
		try{
			GLBankTransferEntryCharges glBankTransferEntryCharges= repositoryGLBankTransferEntryCharges.findByChargeNumberAndIsDeleted(dtoGLBankTransferCharges.getChargeNumber(), false);
			if(glBankTransferEntryCharges!=null){
				repositoryGLBankTransferEntryCharges.delete(glBankTransferEntryCharges);
			}
		   return true;
		}
		catch(Exception e){
			return false;
		}
	}

	/*public boolean checkDistributionDetailIsAvailable(int bankTransferNumber) throws ScriptException {
		
		GLBankTransferHeader glBankTransferHeader= repositoryGLBankTransferHeader.findByBankTransferNumber(bankTransferNumber);
		DtoGLBankTransferDistribution dtoGLBankTransferDistribution = getBankTransferDistrubutionByBankTransferNumber(bankTransferNumber);
		GLBankTransferDistribution distributionFrom = repositoryGLBankTransferDistribution.
				findByGlBankTransferHeaderBankTransferNumberAndAccountType(bankTransferNumber,1);//1= from
		GLBankTransferDistribution distributionTo = repositoryGLBankTransferDistribution.
				findByGlBankTransferHeaderBankTransferNumberAndAccountType(bankTransferNumber,2);//1= to
		
		if(distributionFrom!=null && distributionTo!=null && glBankTransferHeader!=null && dtoGLBankTransferDistribution!=null){
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			
			String accountTableRowIndexFrom = dtoGLBankTransferDistribution.getAccountTableRowIndexFrom();
			String accountTableRowIndexTo = dtoGLBankTransferDistribution.getAccountTableRowIndexTo();
			
			double originalDebitAmountFrom = glBankTransferHeader.getTransferAmountFrom();
			double originalCreditAmountTo = glBankTransferHeader.getTransferAmountTo();
			
			Double exchangeRateFrom = 0.0;
			if (glBankTransferHeader.getExchangeRateFrom() != null) {
				exchangeRateFrom = Double.valueOf(glBankTransferHeader.getExchangeRateFrom());
			}
			
			Double exchangeRateTo = 0.0;
			if (glBankTransferHeader.getExchangeRateTo() != null) {
				exchangeRateTo = Double.valueOf(glBankTransferHeader.getExchangeRateTo());
			}
			
			String calcMethodFrom = RateCalculationMethod.MULTIPLY.getMethod();
			String calcMethodTo = RateCalculationMethod.MULTIPLY.getMethod();

			if (glBankTransferHeader.getRateCalculateMethodFrom() == 2) {
				calcMethodFrom = RateCalculationMethod.DIVIDE.getMethod();
			}

			if (glBankTransferHeader.getRateCalculateMethodTo() == 2) {
				calcMethodTo = RateCalculationMethod.DIVIDE.getMethod();
			}

			Double debitAmount = (double) mathEval.eval(originalDebitAmountFrom + calcMethodFrom + exchangeRateFrom);
			Double creditAmount = (double) mathEval.eval(originalCreditAmountTo + calcMethodTo + exchangeRateTo);
			
			if(!distributionFrom.getAccountTableRowIndex().equalsIgnoreCase(accountTableRowIndexFrom)
					|| originalDebitAmountFrom!=distributionFrom.getOriginalDebitAmount() ||
					debitAmount!=distributionFrom.getDebitAmount()){
				return false;
			}
			
			if(!distributionTo.getAccountTableRowIndex().equalsIgnoreCase(accountTableRowIndexTo)
					|| originalCreditAmountTo!=distributionTo.getOriginalCreditAmount() || 
					creditAmount!=distributionTo.getCreditAmount()){
				return false;
			}
		}
		else{
			return false;
		}
		return true;
	}*/
	
	
}
