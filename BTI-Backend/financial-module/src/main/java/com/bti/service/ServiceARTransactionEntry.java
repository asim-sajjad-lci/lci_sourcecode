/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.APDistributionTypeConstant;
import com.bti.constant.ARDistributionType;
import com.bti.constant.ARTransactionTypeConstant;
import com.bti.constant.AuditTrailSeriesTypeConstant;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.GlSerialConfigurationConstant;
import com.bti.constant.MasterArAcoountTypeConstant;
import com.bti.constant.MessageLabel;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.APTransactionDistribution;
import com.bti.model.ARBalancebyPeriods;
import com.bti.model.ARCashReceiptDistribution;
import com.bti.model.ARSummaryBalances;
import com.bti.model.ARTransactionEntry;
import com.bti.model.ARTransactionEntryDistribution;
import com.bti.model.ARYTDOpenPayments;
import com.bti.model.ARYTDOpenPaymentsDistribution;
import com.bti.model.ARYTDOpenTransactions;
import com.bti.model.ARYTDOpenTransactionsDistribution;
import com.bti.model.BatchTransactionType;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.CustomerClassAccountTableSetup;
import com.bti.model.CustomerClassesSetup;
import com.bti.model.CustomerMaintenance;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLBatches;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.RMDocumentsTypeSetup;
import com.bti.model.RMSetup;
import com.bti.model.SalesTerritoryMaintenance;
import com.bti.model.SalesmanMaintenance;
import com.bti.model.VATSetup;
import com.bti.model.dto.DtoAPTransactionEntry;
import com.bti.model.dto.DtoARCashReceipt;
import com.bti.model.dto.DtoARCashReceiptDistribution;
import com.bti.model.dto.DtoARDistributionDetail;
import com.bti.model.dto.DtoARTransactionEntry;
import com.bti.model.dto.DtoCustomerClassSetup;
import com.bti.model.dto.DtoCustomerMaintenance;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryARBalanceByPeriods;
import com.bti.repository.RepositoryARBatches;
import com.bti.repository.RepositoryARSummaryBalances;
import com.bti.repository.RepositoryARTransactionEntry;
import com.bti.repository.RepositoryARTransactionEntryDistribution;
import com.bti.repository.RepositoryARYTDOpenPayments;
import com.bti.repository.RepositoryARYTDOpenPaymentsDistribution;
import com.bti.repository.RepositoryARYTDOpenTransaction;
import com.bti.repository.RepositoryARYTDOpenTransactionDistribution;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryCustomerClassAccountTableSetup;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.repository.RepositoryMasterARTransactionStatus;
import com.bti.repository.RepositoryRMDocumentType;
import com.bti.repository.RepositoryRMSetup;
import com.bti.repository.RepositoryVATSetup;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceARTransactionEntry Name of Project: BTI Created on: Dec
 * 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceARTransactionEntry")
public class ServiceARTransactionEntry {

	private static final Logger LOG = Logger.getLogger(ServiceARTransactionEntry.class);

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryARTransactionEntry repositoryARTransactionEntry;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;

	@Autowired
	RepositoryARBatches repositoryARBatches;

	@Autowired
	RepositoryMasterARTransactionStatus repositoryMasterARTransactionStatus;

	@Autowired
	RepositoryRMDocumentType repositoryRMDocumentType;

	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;

	@Autowired
	RepositoryRMSetup repositoryRMSetup;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryCustomerClassAccountTableSetup repositoryCustomerClassAccountTableSetup;
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired
	RepositoryVATSetup repositoryVATSetup;
	
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryARTransactionEntryDistribution repositoryARTransactionEntryDistribution;
	
	@Autowired
	RepositoryARSummaryBalances repositoryARSummaryBalances;
	
	@Autowired
	RepositoryARBalanceByPeriods repositoryARBalanceByPeriods;
	
	@Autowired
	RepositoryARYTDOpenTransaction repositoryARYTDOpenTransaction;
	
	@Autowired
	RepositoryARYTDOpenTransactionDistribution repositoryARYTDOpenTransactionDistribution;
	
	@Autowired
	RepositoryARYTDOpenPayments repositoryARYTDOpenPayments;
	
	@Autowired
	RepositoryARYTDOpenPaymentsDistribution repositoryARYTDOpenPaymentsDistribution;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;
	
	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;
	
	
	
	private static final String USER_ID = "userid";

	public DtoARTransactionEntry saveTransactionEntry(DtoARTransactionEntry dtoARTransactionEntry) {
		int langId = serviceHome.getLanngugaeId();
		try {
			ARTransactionEntry arTransactionEntry = new ARTransactionEntry();
			RMDocumentsTypeSetup rmDocumentsTypeSetup = repositoryRMDocumentType
					.findByArDocumentTypeIdAndIsDeleted(dtoARTransactionEntry.getArTransactionType(), false);
			String transactionNumber = getTransactionNumber(dtoARTransactionEntry.getArTransactionType());
			arTransactionEntry.setArTransactionNumber(transactionNumber);
			arTransactionEntry.setTransactionType(rmDocumentsTypeSetup);
			arTransactionEntry.setArTransactionDescription(dtoARTransactionEntry.getArTransactionDescription());
			arTransactionEntry.setArTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoARTransactionEntry.getArTransactionDate()));
			arTransactionEntry.setCustomerID(dtoARTransactionEntry.getCustomerID());
			arTransactionEntry.setCustomerName(dtoARTransactionEntry.getCustomerName());
			arTransactionEntry.setCustomerNameArabic(dtoARTransactionEntry.getCustomerNameArabic());
			arTransactionEntry.setSalesmanID(dtoARTransactionEntry.getSalesmanID());
			arTransactionEntry.setCurrencyID(dtoARTransactionEntry.getCurrencyID());
			arTransactionEntry.setPaymentTermsID(dtoARTransactionEntry.getPaymentTermsID());
			arTransactionEntry.setShippingMethodID(dtoARTransactionEntry.getShippingMethodID());
			arTransactionEntry.setVatScheduleID(dtoARTransactionEntry.getVatScheduleID());
			arTransactionEntry.setSalesTerritoryId(dtoARTransactionEntry.getTerritoryId());
			arTransactionEntry.setArBatches(repositoryARBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(
					dtoARTransactionEntry.getBatchID(),
					BatchTransactionTypeConstant.AR_TRANSACTION_ENTRY.getIndex(), false));
			// Set Transaction Amount
			arTransactionEntry
					.setArTransactionFreightAmount(dtoARTransactionEntry.getArTransactionFreightAmount() != null
							? dtoARTransactionEntry.getArTransactionFreightAmount() : 0.0);
			arTransactionEntry.setArTransactionCashAmount(dtoARTransactionEntry.getArTransactionCashAmount() != null
					? dtoARTransactionEntry.getArTransactionCashAmount() : 0.0);
			arTransactionEntry.setArTransactionCheckAmount(dtoARTransactionEntry.getArTransactionCheckAmount() != null
					? dtoARTransactionEntry.getArTransactionCheckAmount() : 0.0);
			arTransactionEntry
					.setArTransactionCreditCardAmount(dtoARTransactionEntry.getArTransactionCreditCardAmount() != null
							? dtoARTransactionEntry.getArTransactionCreditCardAmount() : 0.0);
			arTransactionEntry
					.setArTransactionCreditMemoAmount(dtoARTransactionEntry.getArTransactionCreditMemoAmount() != null
							? dtoARTransactionEntry.getArTransactionCreditMemoAmount() : 0.0);
			arTransactionEntry
					.setArTransactionDebitMemoAmount(dtoARTransactionEntry.getArTransactionDebitMemoAmount() != null
							? dtoARTransactionEntry.getArTransactionDebitMemoAmount() : 0.0);
			arTransactionEntry.setArTransactionFinanceChargeAmount(
					dtoARTransactionEntry.getArTransactionFinanceChargeAmount() != null
							? dtoARTransactionEntry.getArTransactionFinanceChargeAmount() : 0.0);
			arTransactionEntry
					.setArTransactionWarrantyAmount(dtoARTransactionEntry.getArTransactionWarrantyAmount() != null
							? dtoARTransactionEntry.getArTransactionWarrantyAmount() : 0.0);
			// Set Other Amounts
			arTransactionEntry.setArTransactionCost(dtoARTransactionEntry.getArTransactionCost() != null
					? dtoARTransactionEntry.getArTransactionCost() : 0.0);
			arTransactionEntry.setArTransactionSalesAmount(dtoARTransactionEntry.getArTransactionSalesAmount() != null
					? dtoARTransactionEntry.getArTransactionSalesAmount() : 0.0);
			arTransactionEntry
					.setArTransactionTradeDiscount(dtoARTransactionEntry.getArTransactionTradeDiscount() != null
							? dtoARTransactionEntry.getArTransactionTradeDiscount() : 0.0);
			arTransactionEntry
					.setArTransactionMiscellaneous(dtoARTransactionEntry.getArTransactionMiscellaneous() != null
							? dtoARTransactionEntry.getArTransactionMiscellaneous() : 0.0);
			arTransactionEntry.setArTransactionVATAmount(dtoARTransactionEntry.getArTransactionVATAmount() != null
					? dtoARTransactionEntry.getArTransactionVATAmount() : 0.0);
			arTransactionEntry.setArTransactionTotalAmount(dtoARTransactionEntry.getArTransactionTotalAmount() != null
					? dtoARTransactionEntry.getArTransactionTotalAmount() : 0.0);
			arTransactionEntry.setArServiceRepairAmount(dtoARTransactionEntry.getArServiceRepairAmount()!=null?
					dtoARTransactionEntry.getArServiceRepairAmount():0.0);
			arTransactionEntry.setArReturnAmount(dtoARTransactionEntry.getArReturnAmount()!=null?dtoARTransactionEntry.getArReturnAmount():0.0);
			arTransactionEntry.setCheckbookID(dtoARTransactionEntry.getCheckbookID());

			if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCheckNumber())) {
				arTransactionEntry.setCheckNumber(dtoARTransactionEntry.getCheckNumber());
			}

			if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCreditCardID())) {
				arTransactionEntry.setCreditCardID(dtoARTransactionEntry.getCreditCardID());
				arTransactionEntry.setCreditCardNumber(dtoARTransactionEntry.getCreditCardNumber());
				arTransactionEntry.setCreditCardExpireMonth(dtoARTransactionEntry.getCreditCardExpireMonth());
				arTransactionEntry.setCreditCardExpireYear(dtoARTransactionEntry.getCreditCardExpireYear());
			}

			arTransactionEntry.setArTransactionStatus(repositoryMasterARTransactionStatus
					.findByStatusIdAndLanguageLanguageId(dtoARTransactionEntry.getArTransactionStatus(), langId));
			if(dtoARTransactionEntry.getPaymentTypeId()!=null){
				arTransactionEntry.setPaymentTypeId(String.valueOf(dtoARTransactionEntry.getPaymentTypeId()));
			}
			//set cash receipt number if payment is true
			arTransactionEntry.setPayment(false);
			if(dtoARTransactionEntry.getIsPayment())
			{
				String paymentNumber = getTransactionNumber(dtoARTransactionEntry.getPaymentTransactionTypeId());
				arTransactionEntry.setCashReceiptNumber(paymentNumber);
				arTransactionEntry.setPayment(true);
				RMDocumentsTypeSetup rmDocumentsTypeSetupPayment = repositoryRMDocumentType
						.findByArDocumentTypeIdAndIsDeleted(dtoARTransactionEntry.getPaymentTransactionTypeId(), false);
				if (rmDocumentsTypeSetupPayment != null) {
					rmDocumentsTypeSetupPayment.setDocumentNumber(rmDocumentsTypeSetupPayment.getDocumentNumber() + 1);
					repositoryRMDocumentType.saveAndFlush(rmDocumentsTypeSetupPayment);
				}
			}
			else
			{
				arTransactionEntry.setCashReceiptNumber(transactionNumber);
			}
			
			arTransactionEntry.setPaymentAmount(0);
			if(UtilRandomKey.isNotBlank(dtoARTransactionEntry.getPaymentAmount())){
				arTransactionEntry.setPaymentAmount(Double.valueOf(dtoARTransactionEntry.getPaymentAmount()));
			}
			
			if (dtoARTransactionEntry.getExchangeTableIndex() != null) {
				arTransactionEntry.setExchangeTableIndex(dtoARTransactionEntry.getExchangeTableIndex());
				arTransactionEntry.setExchangeTableRate(dtoARTransactionEntry.getExchangeTableRate());
			}

			arTransactionEntry.setTransactionVoid(dtoARTransactionEntry.getTransactionVoid());
			arTransactionEntry.setRowDateIndex(new Date());
			arTransactionEntry.setModifyByUserID(httpServletRequest.getHeader(USER_ID));
			repositoryARTransactionEntry.saveAndFlush(arTransactionEntry);

			if (rmDocumentsTypeSetup != null) {
				rmDocumentsTypeSetup.setDocumentNumber(rmDocumentsTypeSetup.getDocumentNumber() + 1);
				repositoryRMDocumentType.saveAndFlush(rmDocumentsTypeSetup);
			}
			
			dtoARTransactionEntry.setArTransactionNumber(transactionNumber);
			return getTransactionEntryByTransactionNumber(dtoARTransactionEntry);
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoARTransactionEntry updateTransactionEntry(DtoARTransactionEntry dtoARTransactionEntry) {
		int langId = serviceHome.getLanngugaeId();
		try 
		{
			ARTransactionEntry arTransactionEntry = repositoryARTransactionEntry
					.findByArTransactionNumber(dtoARTransactionEntry.getArTransactionNumber());
			if (arTransactionEntry == null) {
				return null;
			}

			arTransactionEntry.setTransactionType(repositoryRMDocumentType
					.findByArDocumentTypeIdAndIsDeleted(dtoARTransactionEntry.getArTransactionType(), false));
			arTransactionEntry.setArTransactionDescription(dtoARTransactionEntry.getArTransactionDescription());
			arTransactionEntry.setArTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoARTransactionEntry.getArTransactionDate()));
			arTransactionEntry.setCustomerID(dtoARTransactionEntry.getCustomerID());
			arTransactionEntry.setCustomerName(dtoARTransactionEntry.getCustomerName());
			arTransactionEntry.setCustomerNameArabic(dtoARTransactionEntry.getCustomerNameArabic());
			arTransactionEntry.setSalesmanID(dtoARTransactionEntry.getSalesmanID());
			arTransactionEntry.setCurrencyID(dtoARTransactionEntry.getCurrencyID());
			arTransactionEntry.setPaymentTermsID(dtoARTransactionEntry.getPaymentTermsID());
			arTransactionEntry.setShippingMethodID(dtoARTransactionEntry.getShippingMethodID());
			arTransactionEntry.setVatScheduleID(dtoARTransactionEntry.getVatScheduleID());
			arTransactionEntry.setSalesTerritoryId(dtoARTransactionEntry.getTerritoryId());
			arTransactionEntry.setArBatches(repositoryARBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(
					dtoARTransactionEntry.getBatchID(),
					BatchTransactionTypeConstant.AR_TRANSACTION_ENTRY.getIndex(), false));
			// Set Transaction Amount
			arTransactionEntry
					.setArTransactionFreightAmount(dtoARTransactionEntry.getArTransactionFreightAmount() != null
							? dtoARTransactionEntry.getArTransactionFreightAmount() : 0.0);
			arTransactionEntry.setArTransactionCashAmount(dtoARTransactionEntry.getArTransactionCashAmount() != null
					? dtoARTransactionEntry.getArTransactionCashAmount() : 0.0);
			arTransactionEntry.setArTransactionCheckAmount(dtoARTransactionEntry.getArTransactionCheckAmount() != null
					? dtoARTransactionEntry.getArTransactionCheckAmount() : 0.0);
			arTransactionEntry
					.setArTransactionCreditCardAmount(dtoARTransactionEntry.getArTransactionCreditCardAmount() != null
							? dtoARTransactionEntry.getArTransactionCreditCardAmount() : 0.0);
			arTransactionEntry
					.setArTransactionCreditMemoAmount(dtoARTransactionEntry.getArTransactionCreditMemoAmount() != null
							? dtoARTransactionEntry.getArTransactionCreditMemoAmount() : 0.0);
			arTransactionEntry
					.setArTransactionDebitMemoAmount(dtoARTransactionEntry.getArTransactionDebitMemoAmount() != null
							? dtoARTransactionEntry.getArTransactionDebitMemoAmount() : 0.0);
			arTransactionEntry.setArTransactionFinanceChargeAmount(
					dtoARTransactionEntry.getArTransactionFinanceChargeAmount() != null
							? dtoARTransactionEntry.getArTransactionFinanceChargeAmount() : 0.0);
			arTransactionEntry
					.setArTransactionWarrantyAmount(dtoARTransactionEntry.getArTransactionWarrantyAmount() != null
							? dtoARTransactionEntry.getArTransactionWarrantyAmount() : 0.0);
			// Set Other Costs
			arTransactionEntry.setArTransactionCost(dtoARTransactionEntry.getArTransactionCost() != null
					? dtoARTransactionEntry.getArTransactionCost() : 0.0);
			arTransactionEntry.setArTransactionSalesAmount(dtoARTransactionEntry.getArTransactionSalesAmount() != null
					? dtoARTransactionEntry.getArTransactionSalesAmount() : 0.0);
			arTransactionEntry
					.setArTransactionTradeDiscount(dtoARTransactionEntry.getArTransactionTradeDiscount() != null
							? dtoARTransactionEntry.getArTransactionTradeDiscount() : 0.0);
			arTransactionEntry
					.setArTransactionMiscellaneous(dtoARTransactionEntry.getArTransactionMiscellaneous() != null
							? dtoARTransactionEntry.getArTransactionMiscellaneous() : 0.0);
			arTransactionEntry.setArTransactionVATAmount(dtoARTransactionEntry.getArTransactionVATAmount() != null
					? dtoARTransactionEntry.getArTransactionVATAmount() : 0.0);
			arTransactionEntry.setArTransactionTotalAmount(dtoARTransactionEntry.getArTransactionTotalAmount() != null
					? dtoARTransactionEntry.getArTransactionTotalAmount() : 0.0);
			arTransactionEntry.setArServiceRepairAmount(dtoARTransactionEntry.getArServiceRepairAmount()!=null?
					dtoARTransactionEntry.getArServiceRepairAmount():0.0);
			arTransactionEntry.setArReturnAmount(dtoARTransactionEntry.getArReturnAmount()!=null?dtoARTransactionEntry.getArReturnAmount():0.0);
			arTransactionEntry.setCheckbookID(dtoARTransactionEntry.getCheckbookID());

			if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCheckNumber())) {
				arTransactionEntry.setCheckNumber(dtoARTransactionEntry.getCheckNumber());
			} else {
				arTransactionEntry.setCheckNumber(null);
			}
			
			if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCreditCardID())) {
				arTransactionEntry.setCreditCardID(dtoARTransactionEntry.getCreditCardID());
				arTransactionEntry.setCreditCardNumber(dtoARTransactionEntry.getCreditCardNumber());
				arTransactionEntry.setCreditCardExpireMonth(dtoARTransactionEntry.getCreditCardExpireMonth());
				arTransactionEntry.setCreditCardExpireYear(dtoARTransactionEntry.getCreditCardExpireYear());
			} else {
				arTransactionEntry.setCreditCardID(null);
				arTransactionEntry.setCreditCardNumber(null);
				arTransactionEntry.setCreditCardExpireMonth(null);
				arTransactionEntry.setCreditCardExpireYear(null);
			}

			arTransactionEntry.setArTransactionStatus(repositoryMasterARTransactionStatus
					.findByStatusIdAndLanguageLanguageId(dtoARTransactionEntry.getArTransactionStatus(), langId));

			if (dtoARTransactionEntry.getExchangeTableIndex() != null) {
				arTransactionEntry.setExchangeTableIndex(dtoARTransactionEntry.getExchangeTableIndex());
				arTransactionEntry.setExchangeTableRate(dtoARTransactionEntry.getExchangeTableRate());
			}
			arTransactionEntry.setPayment(dtoARTransactionEntry.getIsPayment());
			arTransactionEntry.setPaymentAmount(0);
			if(UtilRandomKey.isNotBlank(dtoARTransactionEntry.getPaymentAmount())){
				arTransactionEntry.setPaymentAmount(Double.valueOf(dtoARTransactionEntry.getPaymentAmount()));
			}
			
			if(dtoARTransactionEntry.getPaymentTypeId()!=null){
				arTransactionEntry.setPaymentTypeId(String.valueOf(dtoARTransactionEntry.getPaymentTypeId()));
			}
			arTransactionEntry.setTransactionVoid(dtoARTransactionEntry.getTransactionVoid());
			arTransactionEntry.setModifyByUserID(httpServletRequest.getHeader(USER_ID));
			repositoryARTransactionEntry.saveAndFlush(arTransactionEntry);
			return getTransactionEntryByTransactionNumber(dtoARTransactionEntry);
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoARTransactionEntry getTransactionEntryByTransactionNumber(DtoARTransactionEntry dtoARTransactionEntry) {
		try {
			ARTransactionEntry arTransactionEntry = repositoryARTransactionEntry
					.findByArTransactionNumber(dtoARTransactionEntry.getArTransactionNumber());
			if (arTransactionEntry != null) {
				DtoARTransactionEntry respon =new  DtoARTransactionEntry(arTransactionEntry);
				return respon;
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public List<String> getTransactionNumberByTransactionType(int transactionTypeId) {
		List<String> transactionNumberList = new ArrayList<>();
		try {
			List<ARTransactionEntry> arTransactionEntryList = repositoryARTransactionEntry
					.findByTransactionTypeArDocumentTypeId(transactionTypeId);
			if (arTransactionEntryList != null && !arTransactionEntryList.isEmpty()) {
				arTransactionEntryList.forEach(
						arTransactionEntry -> transactionNumberList.add(arTransactionEntry.getArTransactionNumber()));
			}

		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return transactionNumberList;
	}

	public List<DtoStatusType> getTransactionType() {
		List<DtoStatusType> list = new ArrayList<>();
		try {
			List<RMDocumentsTypeSetup> arTransactionTypeList = repositoryRMDocumentType.findByIsDeleted(false);
			if (arTransactionTypeList != null && !arTransactionTypeList.isEmpty()) {
				arTransactionTypeList.forEach(arTransactionType -> list.add(new DtoStatusType(arTransactionType)));
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}

	public String getTransactionNumber(int arTransactionType) {

		String transactionNumber = "";
		RMDocumentsTypeSetup rmDocumentsTypeSetup = repositoryRMDocumentType
				.findByArDocumentTypeIdAndIsDeleted(arTransactionType, false);
		if (rmDocumentsTypeSetup != null) {
			transactionNumber = rmDocumentsTypeSetup.getDocumentCode() + "-" + rmDocumentsTypeSetup.getDocumentNumber();
		}
		return transactionNumber;
	}
	
	public DtoCustomerClassSetup getCustomerClassSetupByClassId(DtoCustomerMaintenance dtoCustomerMaintenance) {
		DtoCustomerClassSetup dtoCustomerClassSetup = new DtoCustomerClassSetup();
		dtoCustomerClassSetup.setSalesTerritoryId("");
		dtoCustomerClassSetup.setTerritoryDescription("");
		dtoCustomerClassSetup.setSalesmanId("");
		dtoCustomerClassSetup.setSalesmanName("");
		dtoCustomerClassSetup.setVatId("");
		dtoCustomerClassSetup.setCurrencyId("");
		dtoCustomerClassSetup.setPaymentTermId("");
		dtoCustomerClassSetup.setCheckbookId("");
		dtoCustomerClassSetup.setName("");
		dtoCustomerClassSetup.setCustomerId("");
		dtoCustomerClassSetup.setShipmentMethodId("");
		dtoCustomerClassSetup.setTradeDiscountPercent("0");
		dtoCustomerClassSetup.setMiscVatPercent("0");
		dtoCustomerClassSetup.setFreightVatPercent("0");
		dtoCustomerClassSetup.setVatPercent("0");
		
		CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance
				.findByCustnumberAndIsDeleted(dtoCustomerMaintenance.getCustomerId(), false);
		try {
			if (customerMaintenance != null) {
				dtoCustomerClassSetup.setName(
						customerMaintenance.getCustomerName() != null ? customerMaintenance.getCustomerName() : "");
				dtoCustomerClassSetup.setCustomerId(customerMaintenance.getCustnumber());
				if (customerMaintenance.getCustomerClassesSetup() != null) {
					CustomerClassesSetup customerClassesSetup = customerMaintenance.getCustomerClassesSetup();
					SalesmanMaintenance salesmanMaintenance = customerClassesSetup.getSalesmanMaintenance();
					if (salesmanMaintenance != null) {
						dtoCustomerClassSetup.setSalesmanId(salesmanMaintenance.getSalesmanId());
						if (UtilRandomKey.isNotBlank(salesmanMaintenance.getSalesmanFirstName())) {
							dtoCustomerClassSetup.setSalesmanName(salesmanMaintenance.getSalesmanFirstName());
						}
					}

					SalesTerritoryMaintenance salesTerritoryMaintenance = customerClassesSetup
							.getSalesTerritoryMaintenance();
					if (salesTerritoryMaintenance != null) {
						dtoCustomerClassSetup.setSalesTerritoryId(salesTerritoryMaintenance.getSalesTerritoryId());
						if (UtilRandomKey.isNotBlank(salesTerritoryMaintenance.getTerritoryDescription())) {
							dtoCustomerClassSetup
									.setTerritoryDescription(salesTerritoryMaintenance.getTerritoryDescription());
						}
					}
					dtoCustomerClassSetup.setShipmentMethodId(customerClassesSetup.getShipmentMethodSetup() != null
							? customerClassesSetup.getShipmentMethodSetup().getShipmentMethodId() : "");
					if(customerClassesSetup.getVatSetup()!=null){
						dtoCustomerClassSetup.setVatId(customerClassesSetup.getVatSetup().getVatScheduleId());
						dtoCustomerClassSetup.setVatPercent(String.valueOf(customerClassesSetup.getVatSetup().getBasperct()));
					}
					
					dtoCustomerClassSetup.setCurrencyId(customerClassesSetup.getCurrencySetup() != null
							? customerClassesSetup.getCurrencySetup().getCurrencyId() : "");
					dtoCustomerClassSetup.setPaymentTermId(customerClassesSetup.getPaymentTermsSetup() != null
							? customerClassesSetup.getPaymentTermsSetup().getPaymentTermId() : "");
					dtoCustomerClassSetup.setCheckbookId(customerClassesSetup.getCheckbookMaintenance() != null
							? customerClassesSetup.getCheckbookMaintenance().getCheckBookId() : "");
					
					dtoCustomerClassSetup.setTradeDiscountPercent(String.valueOf(customerClassesSetup.getTradeDiscountPercent()));
				}
			}

			List<RMSetup> rmSetupList = repositoryRMSetup.findAll();
			if (rmSetupList != null && !rmSetupList.isEmpty()) {
				RMSetup rmSetup = rmSetupList.get(0);
				dtoCustomerClassSetup.setMiscVatScheduleId(rmSetup.getMiscVatScheduleId().getVatScheduleId());
				dtoCustomerClassSetup.setMiscVatPercent(String.valueOf(rmSetup.getMiscVatScheduleId().getBasperct()));
				
				dtoCustomerClassSetup.setFreightVatScheduleId(rmSetup.getFreightVatScheduleId().getVatScheduleId());
				dtoCustomerClassSetup.setFreightVatPercent(String.valueOf(rmSetup.getFreightVatScheduleId().getBasperct()));
				
				if (UtilRandomKey.isNotBlank(dtoCustomerClassSetup.getVatId())) {
					dtoCustomerClassSetup.setVatId(rmSetup.getSalesVatScheduleId().getVatScheduleId());
					dtoCustomerClassSetup.setVatPercent(String.valueOf(rmSetup.getSalesVatScheduleId().getBasperct()));
				}
			}
		} catch (Exception e) {
			dtoCustomerClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoCustomerClassSetup;
	}
	
	public DtoARCashReceiptDistribution getTransactionalDistribution(
			DtoARTransactionEntry trnsactionEntryDto) {
	try 
	{
		    List<DtoARDistributionDetail> distributionDetailList= new ArrayList<>();
			DtoARCashReceiptDistribution distribution = new DtoARCashReceiptDistribution();
			distribution.setCustomerId(trnsactionEntryDto.getCustomerID()!=null?trnsactionEntryDto.getCustomerID():"");
			distribution.setCustomerName(trnsactionEntryDto.getCustomerName()!=null?trnsactionEntryDto.getCustomerName():"");
			distribution.setCurrencyID(trnsactionEntryDto.getCurrencyID());
			distribution.setTransactionNumber(trnsactionEntryDto.getArTransactionNumber());
			distribution.setTransactionType("");
			RMDocumentsTypeSetup rmDocumentsTypeSetup = repositoryRMDocumentType
					.findByArDocumentTypeIdAndIsDeleted(trnsactionEntryDto.getArTransactionType(), false);
			if(rmDocumentsTypeSetup!=null){
				distribution.setTransactionType(rmDocumentsTypeSetup.getDocumentType());
			}
			
			distribution.setOriginalAmount(trnsactionEntryDto.getArTransactionTotalAmount());
				
			
			List<ARTransactionEntryDistribution> arTransactionEntryDistributionsList=null;
			if(trnsactionEntryDto.getButtonType().equalsIgnoreCase(MessageLabel.NEW_DISTRIBUTION))
			{
				arTransactionEntryDistributionsList= repositoryARTransactionEntryDistribution.findByArTransactionNumber(trnsactionEntryDto.getArTransactionNumber());
				if(arTransactionEntryDistributionsList!=null && !arTransactionEntryDistributionsList.isEmpty())
				for (ARTransactionEntryDistribution arTransactionEntryDistribution : arTransactionEntryDistributionsList) 
				{
					DtoARDistributionDetail distributionDetail= new DtoARDistributionDetail();
					distributionDetail.setAccountNumber("");
					distributionDetail.setAccountDescription("");
					distributionDetail.setAccountTableRowIndex("");
					GLAccountsTableAccumulation glAccountsTableAccumulation=null;
					if(arTransactionEntryDistribution.getAccountTableRowIndex()>0){
						glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(arTransactionEntryDistribution.getAccountTableRowIndex()), false);
					}
					
					distributionDetail.setDistributionReference("");
					if(UtilRandomKey.isNotBlank(arTransactionEntryDistribution.getDistributionDescription())){
						distributionDetail.setDistributionReference(arTransactionEntryDistribution.getDistributionDescription());
					}
					
					distributionDetail.setType("");
					distributionDetail.setTypeId(0);
					if(arTransactionEntryDistribution.getTransactionType()>0){
						distributionDetail.setType(ARDistributionType.getById(arTransactionEntryDistribution.getTransactionType()).name());
						distributionDetail.setTypeId(arTransactionEntryDistribution.getTransactionType());
					}
					
					distributionDetail.setDebitAmount(0.0);
					if(arTransactionEntryDistribution.getOriginalDebitAmount()>0){
						distributionDetail.setDebitAmount(arTransactionEntryDistribution.getOriginalDebitAmount());
					}
					
					distributionDetail.setCreditAmount(0.0);
					if(arTransactionEntryDistribution.getOriginalCreditAmount()>0){
						distributionDetail.setCreditAmount(arTransactionEntryDistribution.getOriginalCreditAmount());
					}
					
					if(glAccountsTableAccumulation!=null){
						distributionDetail.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
						Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
						distributionDetail.setAccountNumber(object[0].toString());
						distributionDetail.setAccountDescription("");
						if(UtilRandomKey.isNotBlank(object[3].toString())){
							distributionDetail.setAccountDescription(object[3].toString());
						}
					}
					distributionDetailList.add(distributionDetail);
				}
			}
			
			if(trnsactionEntryDto.getButtonType().equalsIgnoreCase(MessageLabel.DEFAULT_DISTRIBUTION) || arTransactionEntryDistributionsList==null || arTransactionEntryDistributionsList.isEmpty())
			{
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double amount  = trnsactionEntryDto.getArTransactionTotalAmount();
				String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
				if(UtilRandomKey.isNotBlank(String.valueOf(trnsactionEntryDto.getExchangeTableIndex()))){
					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(trnsactionEntryDto.getExchangeTableIndex(), false);
				    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
				    }
				}

				Double functionalAmount = (double) mathEval.eval(amount + calcMethod + trnsactionEntryDto.getExchangeTableRate());
				distribution.setFunctionalAmount(functionalAmount);
				
				String customerClassId="";
				GLAccountsTableAccumulation glAccountsTableAccumulationCostOfSales=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationInventory=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationSales=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationTradeDis=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationVat=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationAccRecivable=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationCash=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFreight=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationMis=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFreightTax=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationMisTax=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationDebitMemo=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFinanceCharge=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationServiceRepair=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationWarrantyExpanse=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationWarrantySales=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationCreditMemo=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationReturn=null;
				
				CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(trnsactionEntryDto.getCustomerID(), false);
				if(customerMaintenance!=null && customerMaintenance.getCustomerClassesSetup()!=null){
					customerClassId=customerMaintenance.getCustomerClassesSetup().getCustomerClassId();
				}
				
				List<RMSetup> rmSetupList = repositoryRMSetup.findAll();
				RMSetup rmSetup=null;
				if (rmSetupList != null && !rmSetupList.isEmpty()) {
					rmSetup = rmSetupList.get(0);
				}
				
				//COGS Account Number
				CustomerClassAccountTableSetup customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								COST_OF_SALES.getIndex(),customerClassId);
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationCostOfSales= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Inventory Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								INVENTORY.getIndex(),customerClassId);
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationInventory= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Sales Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								SALES.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationSales= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Trade Discount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								TRADE_DISCOUNT.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationTradeDis= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Vat Account Number
				VATSetup vatSetup=repositoryVATSetup.findByVatScheduleIdAndIsDeleted(trnsactionEntryDto.getVatScheduleID(), false);
				if(vatSetup!=null){
					glAccountsTableAccumulationVat= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(vatSetup.getAccountRowId()), false);
				}
				
				//Account Receivable Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								ACCOUNT_RECV.getIndex(),customerClassId);
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationAccRecivable= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Cash Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								CASH.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationCash= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Debit Memo Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								DEBIT_MEMO.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationDebitMemo= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Finance Charge  Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								FINANCE_CHARGE.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationFinanceCharge= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Service Repair Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								SERVICE_REPAIR.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationServiceRepair= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Warranty Sales Amount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								WARRANTY.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationWarrantySales= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Warranty Expense Amount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								WARRANTY_EXPENSE.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationWarrantyExpanse= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Cedit Memo Amount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								CREDIT_MEMO.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationCreditMemo= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Return Amount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								SALES_ORDER_RETURNS.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationReturn= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				if(rmSetup!=null){
					//Freight Tax Account Number
					glAccountsTableAccumulationFreightTax= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(rmSetup.getFreightVatScheduleId().getAccountRowId()),false);
					
					//Miscellaneous Tax Account Number
					glAccountsTableAccumulationMisTax= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(rmSetup.getMiscVatScheduleId().getAccountRowId()),false);
					
				}
				
				//Freight Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								FREIGHT.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationFreight= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Miscellaneous Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								MISCELLANEOUS.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationMis= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				    // For Sales
					if(trnsactionEntryDto.getArTransactionSalesAmount()!=null && trnsactionEntryDto.getArTransactionSalesAmount() >0)
					{
						double paymentAmount= Double.parseDouble(trnsactionEntryDto.getPaymentAmount());
						double diffAmount=trnsactionEntryDto.getArTransactionTotalAmount()-paymentAmount;
						if(trnsactionEntryDto.getIsPayment() && diffAmount<=0)
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							if(diffAmount>0 && diffAmount!=trnsactionEntryDto.getArTransactionTotalAmount()){
								distributionDetailList=getDifferentAmountDistribution(paymentAmount, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
								
							}
						}
						
						if(trnsactionEntryDto.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							
							//Set Sales Account Number Information
							distributionDetailList=getSalsDisAccountDistibution(trnsactionEntryDto.getArTransactionSalesAmount(), distributionDetailList, 
									glAccountsTableAccumulationSales);
							
							// Trade Discount
							if(trnsactionEntryDto.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(trnsactionEntryDto.getArTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							//Set Vat Account Number Information
							if(trnsactionEntryDto.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(trnsactionEntryDto.getArTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							//Set Freight Account Number Information
							if(trnsactionEntryDto.getArTransactionFreightAmount()!=0.0){
								distributionDetailList = getFreightAccountDistibution(trnsactionEntryDto.getArTransactionFreightAmount(), rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							//Set Miscellaneous Account Number Information
							if(trnsactionEntryDto.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(trnsactionEntryDto.getArTransactionMiscellaneous(), rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					// For Debit Memo
					if(trnsactionEntryDto.getArTransactionDebitMemoAmount()!=null && trnsactionEntryDto.getArTransactionDebitMemoAmount()>0)
					{
						double paymentAmount= Double.parseDouble(trnsactionEntryDto.getPaymentAmount());
						double diffAmount=trnsactionEntryDto.getArTransactionTotalAmount()-paymentAmount;
						if(trnsactionEntryDto.getIsPayment() && diffAmount<=0)
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							if(diffAmount>0 && diffAmount!=trnsactionEntryDto.getArTransactionTotalAmount()){
								distributionDetailList=getDifferentAmountDistribution(paymentAmount, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
								
							}
						}
						
						//Set Debit Memo Account Number Information
						distributionDetailList=getDebitMemoAccountDistibution(trnsactionEntryDto.getArTransactionDebitMemoAmount(), distributionDetailList, 
								glAccountsTableAccumulationDebitMemo);
						
						if(trnsactionEntryDto.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(trnsactionEntryDto.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(trnsactionEntryDto.getArTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(trnsactionEntryDto.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(trnsactionEntryDto.getArTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(trnsactionEntryDto.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(trnsactionEntryDto.getArTransactionFreightAmount(), rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(trnsactionEntryDto.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(trnsactionEntryDto.getArTransactionMiscellaneous(), rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Finance Charge
					if(trnsactionEntryDto.getArTransactionFinanceChargeAmount()!=null && trnsactionEntryDto.getArTransactionFinanceChargeAmount()>0)
					{
						double paymentAmount= Double.parseDouble(trnsactionEntryDto.getPaymentAmount());
						double diffAmount=trnsactionEntryDto.getArTransactionTotalAmount()-paymentAmount;
						if(trnsactionEntryDto.getIsPayment() && diffAmount<=0)
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							if(diffAmount>0 && diffAmount!=trnsactionEntryDto.getArTransactionTotalAmount()){
								distributionDetailList=getDifferentAmountDistribution(paymentAmount, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
							}
						}
						
						//Set Finance Charge Memo Account Number Information
						distributionDetailList=getFinanceChargeAccountDistibution(trnsactionEntryDto.getArTransactionFinanceChargeAmount(), distributionDetailList, 
								glAccountsTableAccumulationFinanceCharge);
						
						if(trnsactionEntryDto.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(trnsactionEntryDto.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(trnsactionEntryDto.getArTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(trnsactionEntryDto.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(trnsactionEntryDto.getArTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(trnsactionEntryDto.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(trnsactionEntryDto.getArTransactionFreightAmount(), rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(trnsactionEntryDto.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(trnsactionEntryDto.getArTransactionMiscellaneous(), rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					//Warranty Repair
					if(trnsactionEntryDto.getArTransactionWarrantyAmount()!=null && trnsactionEntryDto.getArTransactionWarrantyAmount()>0)
					{
						//Set Account Receivable Account Number Information
						distributionDetailList=getAccountReceivableDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList, 
								glAccountsTableAccumulationAccRecivable);
								
						//Set Warranty Sales Account Number Information
						distributionDetailList=getWarrantySalesAccountDistibution(trnsactionEntryDto.getArTransactionWarrantyAmount(), distributionDetailList, 
								glAccountsTableAccumulationWarrantySales);
						
						//Set Warranty Expenses Memo Account Number Information
						distributionDetailList=getWarrantyExpenceAccountDistibution(trnsactionEntryDto.getArTransactionWarrantyAmount(), distributionDetailList, 
								glAccountsTableAccumulationWarrantyExpanse);
						
						if(trnsactionEntryDto.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(trnsactionEntryDto.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(trnsactionEntryDto.getArTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(trnsactionEntryDto.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(trnsactionEntryDto.getArTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(trnsactionEntryDto.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(trnsactionEntryDto.getArTransactionFreightAmount(), rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(trnsactionEntryDto.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(trnsactionEntryDto.getArTransactionMiscellaneous(), rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					//Credit Memo
					if(trnsactionEntryDto.getArTransactionCreditMemoAmount()!=null && trnsactionEntryDto.getArTransactionCreditMemoAmount()>0)
					{
						
						//Set Account Receivable Account Number Information
						distributionDetailList=getAccountReceivableCreditDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList, 
								glAccountsTableAccumulationAccRecivable);
						
						//Set Credit Memo Account Number Information
						distributionDetailList=getCreditMemoAccountDistribution(trnsactionEntryDto.getArTransactionCreditMemoAmount(), distributionDetailList, 
								glAccountsTableAccumulationCreditMemo);
						
						if(trnsactionEntryDto.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(trnsactionEntryDto.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(trnsactionEntryDto.getArTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(trnsactionEntryDto.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(trnsactionEntryDto.getArTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(trnsactionEntryDto.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(trnsactionEntryDto.getArTransactionFreightAmount(), rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(trnsactionEntryDto.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(trnsactionEntryDto.getArTransactionMiscellaneous(), rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Sales and Returns
					if(trnsactionEntryDto.getArReturnAmount()!=null && trnsactionEntryDto.getArReturnAmount() >0)
					{
						double paymentAmount= Double.parseDouble(trnsactionEntryDto.getPaymentAmount());
						double diffAmount=trnsactionEntryDto.getArTransactionTotalAmount()-paymentAmount;
						if(trnsactionEntryDto.getIsPayment() && diffAmount<=0)
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							
							if(diffAmount>0 && diffAmount!=trnsactionEntryDto.getArTransactionTotalAmount()){
								distributionDetailList=getDifferentAmountDistribution(paymentAmount, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
								
							}
						}
						
						//Set Sales And Returns Account Number Information
						distributionDetailList=getSalesAndReturnsAccountDistibution(trnsactionEntryDto.getArReturnAmount(), distributionDetailList, 
								glAccountsTableAccumulationReturn);
						
						if(trnsactionEntryDto.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(trnsactionEntryDto.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(trnsactionEntryDto.getArTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(trnsactionEntryDto.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(trnsactionEntryDto.getArTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(trnsactionEntryDto.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(trnsactionEntryDto.getArTransactionFreightAmount(), rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(trnsactionEntryDto.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(trnsactionEntryDto.getArTransactionMiscellaneous(), rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Service And Repair
					if(trnsactionEntryDto.getArServiceRepairAmount()!=null && trnsactionEntryDto.getArServiceRepairAmount()>0)
					{
						double paymentAmount= Double.parseDouble(trnsactionEntryDto.getPaymentAmount());
						double diffAmount=trnsactionEntryDto.getArTransactionTotalAmount()-paymentAmount;
						if(trnsactionEntryDto.getIsPayment() && diffAmount<=0)
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							if(diffAmount>0 && diffAmount!=trnsactionEntryDto.getArTransactionTotalAmount()){
								distributionDetailList=getDifferentAmountDistribution(paymentAmount, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(trnsactionEntryDto.getArTransactionTotalAmount(), distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
								
							}
						}
						
						//Set Service and Repair Account Number Information
						distributionDetailList=getServiceRepairAccountDistribution(trnsactionEntryDto.getArServiceRepairAmount(), distributionDetailList, 
								glAccountsTableAccumulationServiceRepair);
						
						if(trnsactionEntryDto.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(trnsactionEntryDto.getArTransactionCost(), distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(trnsactionEntryDto.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(trnsactionEntryDto.getArTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(trnsactionEntryDto.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(trnsactionEntryDto.getArTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(trnsactionEntryDto.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(trnsactionEntryDto.getArTransactionFreightAmount(), rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(trnsactionEntryDto.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(trnsactionEntryDto.getArTransactionMiscellaneous(), rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
			 }
					
				distribution.setListDtoARDistributionDetail(distributionDetailList);
				return distribution;
		}
		catch (Exception e) 
		{
			LOG.error(e.getMessage());
		}
		return null;
	}

	/*public DtoARCashReceiptDistribution getTransactionalDistributionByTransactionNumber(
			String arTransactionNumber) {
	try 
	{
		List<DtoARDistributionDetail> distributionDetailList= new ArrayList<>();
		 ARTransactionEntry arTransactionEntry = repositoryARTransactionEntry.
				 findByArTransactionNumber(arTransactionNumber);
		 if(arTransactionEntry!=null)
		 {
			
			DtoARCashReceiptDistribution distribution = new DtoARCashReceiptDistribution();
			distribution.setCustomerId(arTransactionEntry.getCustomerID()!=null?arTransactionEntry.getCustomerID():"");
			distribution.setCustomerName(arTransactionEntry.getCustomerName()!=null?arTransactionEntry.getCustomerName():"");
			distribution.setCurrencyID(arTransactionEntry.getCurrencyID());
			distribution.setTransactionNumber(arTransactionEntry.getArTransactionNumber());
			distribution.setTransactionType(arTransactionEntry.getTransactionType()!=null?arTransactionEntry.getTransactionType().getDocumentType():"");
			distribution.setOriginalAmount(arTransactionEntry.getArTransactionTotalAmount());
				
				
			List<ARTransactionEntryDistribution> arTransactionEntryDistributionsList= repositoryARTransactionEntryDistribution.findByArTransactionNumber(arTransactionEntry.getArTransactionNumber());
			if(arTransactionEntryDistributionsList!=null && !arTransactionEntryDistributionsList.isEmpty())
			{
				for (ARTransactionEntryDistribution arTransactionEntryDistribution : arTransactionEntryDistributionsList) 
				{
					DtoARDistributionDetail distributionDetail= new DtoARDistributionDetail();
					distributionDetail.setAccountNumber("");
					distributionDetail.setAccountDescription("");
					distributionDetail.setAccountTableRowIndex("");
					GLAccountsTableAccumulation glAccountsTableAccumulation=null;
					if(arTransactionEntryDistribution.getAccountTableRowIndex()>0){
						glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(arTransactionEntryDistribution.getAccountTableRowIndex()), false);
					}
					distributionDetail.setDistributionReference("");
					if(UtilRandomKey.isNotBlank(arTransactionEntryDistribution.getDistributionDescription())){
						distributionDetail.setDistributionReference(arTransactionEntryDistribution.getDistributionDescription());
					}
					distributionDetail.setType("");
					distributionDetail.setTypeId(0);
					if(arTransactionEntryDistribution.getTransactionType()>0){
						distributionDetail.setType(ARDistributionType.getById(arTransactionEntryDistribution.getTransactionType()).name());
						distributionDetail.setTypeId(arTransactionEntryDistribution.getTransactionType());
					}
					distributionDetail.setDebitAmount(0.0);
					if(arTransactionEntryDistribution.getOriginalDebitAmount()>0){
						distributionDetail.setDebitAmount(arTransactionEntryDistribution.getOriginalDebitAmount());
					}
					distributionDetail.setCreditAmount(0.0);
					if(arTransactionEntryDistribution.getOriginalCreditAmount()>0){
						distributionDetail.setCreditAmount(arTransactionEntryDistribution.getOriginalCreditAmount());
					}
					
					if(glAccountsTableAccumulation!=null){
						distributionDetail.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
						Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
						distributionDetail.setAccountNumber(object[0].toString());
						distributionDetail.setAccountDescription("");
						if(UtilRandomKey.isNotBlank(object[3].toString())){
							distributionDetail.setAccountDescription(object[3].toString());
						}
					}
					distributionDetailList.add(distributionDetail);
				}
			}
			else
			{
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double amount  = arTransactionEntry.getArTransactionTotalAmount();
				String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
				if(UtilRandomKey.isNotBlank(String.valueOf(arTransactionEntry.getExchangeTableIndex()))){
					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(arTransactionEntry.getExchangeTableIndex(), false);
				    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
				    }
				}

				Double functionalAmount = (double) mathEval.eval(amount + calcMethod + arTransactionEntry.getExchangeTableRate());
				distribution.setFunctionalAmount(functionalAmount);
				
				String customerClassId="";
				GLAccountsTableAccumulation glAccountsTableAccumulationCostOfSales=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationInventory=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationSales=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationTradeDis=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationVat=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationAccRecivable=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationCash=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFreight=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationMis=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFreightTax=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationMisTax=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationDebitMemo=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFinanceCharge=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationServiceRepair=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationWarrantyExpanse=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationWarrantySales=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationCreditMemo=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationReturn=null;
				
				CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(arTransactionEntry.getCustomerID(), false);
				if(customerMaintenance!=null && customerMaintenance.getCustomerClassesSetup()!=null){
					customerClassId=customerMaintenance.getCustomerClassesSetup().getCustomerClassId();
				}
				
				List<RMSetup> rmSetupList = repositoryRMSetup.findAll();
				RMSetup rmSetup=null;
				if (rmSetupList != null && !rmSetupList.isEmpty()) {
					rmSetup = rmSetupList.get(0);
				}
				
				//COGS Account Number
				CustomerClassAccountTableSetup customerClassAccountTableSetup=null;
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								COST_OF_SALES.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationCostOfSales= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Inventory Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								INVENTORY.getIndex(),customerClassId);
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationInventory= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Sales Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								SALES.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationSales= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Trade Discount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								TRADE_DISCOUNT.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationTradeDis= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Vat Account Number
				VATSetup vatSetup=repositoryVATSetup.findByVatScheduleIdAndIsDeleted(arTransactionEntry.getVatScheduleID(), false);
				
				if(vatSetup!=null){
					glAccountsTableAccumulationVat= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(vatSetup.getAccountRowId()), false);
				}
				
				//Account Receivable Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								ACCOUNT_RECV.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationAccRecivable= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				
				//Cash Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								CASH.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationCash= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Debit Memo Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								DEBIT_MEMO.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationDebitMemo= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Finance Charge  Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								FINANCE_CHARGE.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationFinanceCharge= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Service Repair Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								SERVICE_REPAIR.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationServiceRepair= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Warranty Sales Amount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								WARRANTY.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationWarrantySales= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Warranty Expense Amount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								WARRANTY_EXPENSE.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationWarrantyExpanse= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Cedit Memo Amount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								CREDIT_MEMO.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationCreditMemo= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Return Amount Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								SALES_ORDER_RETURNS.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationReturn= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				if(rmSetup!=null){
					//Freight Tax Account Number
					glAccountsTableAccumulationFreightTax= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(rmSetup.getFreightVatScheduleId().getAccountRowId()),false);
					
					//Miscellaneous Tax Account Number
					glAccountsTableAccumulationMisTax= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(rmSetup.getMiscVatScheduleId().getAccountRowId()),false);
					
				}
				
				//Freight Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								FREIGHT.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationFreight= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Miscellaneous Account Number
				customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup.
						findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(MasterArAcoountTypeConstant.
								MISCELLANEOUS.getIndex(),customerClassId);
				
				if(customerClassAccountTableSetup!=null){
					glAccountsTableAccumulationMis= customerClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				    // For Sales
					if(arTransactionEntry.getArTransactionSalesAmount()>0)
					{
						if(arTransactionEntry.isPayment())
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							double diffAmount=arTransactionEntry.getArTransactionTotalAmount()-arTransactionEntry.getPaymentAmount();
							if(diffAmount>0){
								distributionDetailList=getDifferentAmountDistribution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
								
							}
						}
						
						if(arTransactionEntry.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(arTransactionEntry, distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							
							//Set Sales Account Number Information
							distributionDetailList=getSalsDisAccountDistibution(arTransactionEntry, distributionDetailList, 
									glAccountsTableAccumulationSales);
							
							// Trade Discount
							if(arTransactionEntry.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(arTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							//Set Vat Account Number Information
							if(arTransactionEntry.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							//Set Freight Account Number Information
							if(arTransactionEntry.getArTransactionFreightAmount()!=0.0){
								distributionDetailList = getFreightAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							//Set Miscellaneous Account Number Information
							if(arTransactionEntry.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					// For Debit Memo
					if(arTransactionEntry.getArTransactionDebitMemoAmount()>0)
					{
						if(arTransactionEntry.isPayment())
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							double diffAmount=arTransactionEntry.getArTransactionTotalAmount()-arTransactionEntry.getPaymentAmount();
							if(diffAmount>0){
								distributionDetailList=getDifferentAmountDistribution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
								
							}
						}
						
						//Set Debit Memo Account Number Information
						distributionDetailList=getDebitMemoAccountDistibution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationDebitMemo);
						
						if(arTransactionEntry.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(arTransactionEntry, distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(arTransactionEntry.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(arTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(arTransactionEntry.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(arTransactionEntry.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(arTransactionEntry.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Finance Charge
					if(arTransactionEntry.getArTransactionFinanceChargeAmount()>0)
					{
						if(arTransactionEntry.isPayment())
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							double diffAmount=arTransactionEntry.getArTransactionTotalAmount()-arTransactionEntry.getPaymentAmount();
							if(diffAmount>0){
								distributionDetailList=getDifferentAmountDistribution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
							}
						}
						
						//Set Finance Charge Memo Account Number Information
						distributionDetailList=getFinanceChargeAccountDistibution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationFinanceCharge);
						
						if(arTransactionEntry.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(arTransactionEntry, distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(arTransactionEntry.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(arTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(arTransactionEntry.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(arTransactionEntry.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(arTransactionEntry.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					//Warranty Repair
					if(arTransactionEntry.getArTransactionWarrantyAmount()>0)
					{
						//Set Account Receivable Account Number Information
						distributionDetailList=getAccountReceivableDistibution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationAccRecivable);
								
						//Set Warranty Sales Account Number Information
						distributionDetailList=getWarrantySalesAccountDistibution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationWarrantySales);
						
						//Set Warranty Expenses Memo Account Number Information
						distributionDetailList=getWarrantyExpenceAccountDistibution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationWarrantyExpanse);
						
						if(arTransactionEntry.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(arTransactionEntry, distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(arTransactionEntry.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(arTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(arTransactionEntry.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(arTransactionEntry.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(arTransactionEntry.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					//Credit Memo
					if(arTransactionEntry.getArTransactionCreditMemoAmount()>0)
					{
						
						//Set Account Receivable Account Number Information
						distributionDetailList=getAccountReceivableDistibution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationAccRecivable);
						
						//Set Credit Memo Account Number Information
						distributionDetailList=getCreditMemoAccountDistribution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationCreditMemo);
						
						if(arTransactionEntry.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(arTransactionEntry, distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(arTransactionEntry.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(arTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(arTransactionEntry.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(arTransactionEntry.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(arTransactionEntry.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Sales and Returns
					if(arTransactionEntry.getArReturnAmount()>0)
					{
						if(arTransactionEntry.isPayment())
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							double diffAmount=arTransactionEntry.getArTransactionTotalAmount()-arTransactionEntry.getPaymentAmount();
							if(diffAmount>0){
								distributionDetailList=getDifferentAmountDistribution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
								
							}
						}
						
						//Set Sales And Returns Account Number Information
						distributionDetailList=getSalesAndReturnsAccountDistibution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationReturn);
						
						if(arTransactionEntry.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(arTransactionEntry, distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(arTransactionEntry.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(arTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(arTransactionEntry.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(arTransactionEntry.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(arTransactionEntry.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Service And Repair
					if(arTransactionEntry.getArServiceRepairAmount()>0)
					{
						if(arTransactionEntry.isPayment())
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							double diffAmount=arTransactionEntry.getArTransactionTotalAmount()-arTransactionEntry.getPaymentAmount();
							if(diffAmount>0){
								distributionDetailList=getDifferentAmountDistribution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccRecivable, diffAmount);
							}
							else
							{
								//Set Account Receivable Account Number Information
								distributionDetailList=getAccountReceivableDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationAccRecivable);
								
							}
						}
						
						//Set Service and Repair Account Number Information
						distributionDetailList=getServiceRepairAccountDistribution(arTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationServiceRepair);
						
						if(arTransactionEntry.getArTransactionCost()>0){
							//save COGS for sales with payment 
						    distributionDetailList= getCostOfSalesAccountDistibution(arTransactionEntry, distributionDetailList, 
						    		glAccountsTableAccumulationCostOfSales);
							
							//Set Inventory Account Number Information
							distributionDetailList=getInventoryDisAccountDistibution(arTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationInventory);
						}
							// Trade Discount
							if(arTransactionEntry.getArTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(arTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(arTransactionEntry.getArTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(arTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(arTransactionEntry.getArTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(arTransactionEntry.getArTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(arTransactionEntry, rmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
			 }
					
				distribution.setListDtoARDistributionDetail(distributionDetailList);
				return distribution;
			
		} 
		}
		catch (Exception e) 
		{
			LOG.error(e.getMessage());
		}
		return null;
	}*/
	
	private List<DtoARDistributionDetail> getWarrantySalesAccountDistibution(Double arTransactionWarrantyAmount,
			List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationWarrantySales) 
	{
		DtoARDistributionDetail distributionDetailCostOfSales= new DtoARDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(ARDistributionType.WARS.name());
		distributionDetailCostOfSales.setTypeId(ARDistributionType.WARS.getIndex());
		distributionDetailCostOfSales.setCreditAmount(arTransactionWarrantyAmount);
		
		if(glAccountsTableAccumulationWarrantySales!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationWarrantySales.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationWarrantySales);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
			distributionDetailCostOfSales.setAccountDescription("");
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailCostOfSales.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}
	
	private List<DtoARDistributionDetail> getCreditMemoAccountDistribution(Double arTransactionCreditMemoAmount,
			List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationCreditMemo) 
	{
		DtoARDistributionDetail distributionDetailCostOfSales= new DtoARDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(ARDistributionType.CMEM.name());
		distributionDetailCostOfSales.setTypeId(ARDistributionType.CMEM.getIndex());
		distributionDetailCostOfSales.setDebitAmount(arTransactionCreditMemoAmount);
		
		if(glAccountsTableAccumulationCreditMemo!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationCreditMemo.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationCreditMemo);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
		    distributionDetailCostOfSales.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}
	
	private List<DtoARDistributionDetail> getWarrantyExpenceAccountDistibution(Double arTransactionWarrantyAmount,
			List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationWarrantyExpense) 
	{
		DtoARDistributionDetail distributionDetailCostOfSales= new DtoARDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(ARDistributionType.WARE.name());
		distributionDetailCostOfSales.setTypeId(ARDistributionType.WARE.getIndex());
		distributionDetailCostOfSales.setDebitAmount(arTransactionWarrantyAmount);
		
		
		if(glAccountsTableAccumulationWarrantyExpense!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationWarrantyExpense.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationWarrantyExpense);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
			distributionDetailCostOfSales.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}

	private List<DtoARDistributionDetail> getFinanceChargeAccountDistibution(Double arTransactionFinanceChargeAmount,
			List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationFinanceCharge) {
		DtoARDistributionDetail distributionDetailCostOfSales= new DtoARDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(ARDistributionType.FNCG.name());
		distributionDetailCostOfSales.setTypeId(ARDistributionType.FNCG.getIndex());
		distributionDetailCostOfSales.setCreditAmount(arTransactionFinanceChargeAmount);
		
		
		if(glAccountsTableAccumulationFinanceCharge!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationFinanceCharge.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationFinanceCharge);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
			distributionDetailCostOfSales.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}
	
	private List<DtoARDistributionDetail> getSalesAndReturnsAccountDistibution(Double arReturnAmount,
			List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationSalesAndReturns) 
	{
		DtoARDistributionDetail distributionDetailCostOfSales= new DtoARDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(ARDistributionType.RETN.name());
		distributionDetailCostOfSales.setTypeId(ARDistributionType.RETN.getIndex());
		distributionDetailCostOfSales.setDebitAmount(arReturnAmount);
		
		if(glAccountsTableAccumulationSalesAndReturns!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationSalesAndReturns.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationSalesAndReturns);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
		    distributionDetailCostOfSales.setAccountDescription(object[3].toString());
		}
		
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}
	
	private List<DtoARDistributionDetail> getServiceRepairAccountDistribution(Double arServiceRepairAmount,
			List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationServiceRepair) 
	{
		DtoARDistributionDetail distributionDetailCostOfSales= new DtoARDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(ARDistributionType.SRVC.name());
		distributionDetailCostOfSales.setTypeId(ARDistributionType.SRVC.getIndex());
		distributionDetailCostOfSales.setCreditAmount(arServiceRepairAmount);
		
		
		if(glAccountsTableAccumulationServiceRepair!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationServiceRepair.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationServiceRepair);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
			distributionDetailCostOfSales.setAccountDescription(object[3].toString());
		}
		
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}

	public List<DtoARDistributionDetail> getDifferentAmountDistribution(Double  paymentAmount,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationCash,
			GLAccountsTableAccumulation glAccountsTableAccumulationAccRecivable, Double diffAmount)
	{
		DtoARDistributionDetail distributionDetailCash= new DtoARDistributionDetail();
		distributionDetailCash.setAccountNumber("");
		distributionDetailCash.setAccountDescription("");
		distributionDetailCash.setAccountTableRowIndex("");
		distributionDetailCash.setDistributionReference("");
		distributionDetailCash.setType(ARDistributionType.CASH.name());
		distributionDetailCash.setTypeId(ARDistributionType.CASH.getIndex());
		distributionDetailCash.setDebitAmount(paymentAmount);
		
		
		if(glAccountsTableAccumulationCash!=null){
			distributionDetailCash.setAccountTableRowIndex(glAccountsTableAccumulationCash.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationCash);
			distributionDetailCash.setAccountNumber(object[0].toString());
			distributionDetailCash.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailCash);

		
		//Set Account Receivable Account Number Information
		DtoARDistributionDetail distributionDetailAccountRecv= new DtoARDistributionDetail();
		distributionDetailAccountRecv.setAccountNumber("");
		distributionDetailAccountRecv.setAccountDescription("");
		distributionDetailAccountRecv.setAccountTableRowIndex("");
		distributionDetailAccountRecv.setDistributionReference("");
		distributionDetailAccountRecv.setType(ARDistributionType.RECV.name());
		distributionDetailAccountRecv.setTypeId(ARDistributionType.RECV.getIndex());
		distributionDetailAccountRecv.setDebitAmount(diffAmount);
		
		if(glAccountsTableAccumulationAccRecivable!=null){
			distributionDetailAccountRecv.setAccountTableRowIndex(glAccountsTableAccumulationAccRecivable.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationAccRecivable);
			distributionDetailAccountRecv.setAccountNumber(object[0].toString());
			distributionDetailAccountRecv.setAccountDescription(object[3].toString());
		}
		
		distributionDetailList.add(distributionDetailAccountRecv);
		return distributionDetailList;
	}
	
	public List<DtoARDistributionDetail> getCashDistibution(Double arTransactionTotalAmount,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationCash)
	{
		DtoARDistributionDetail distributionDetailCash= new DtoARDistributionDetail();
		distributionDetailCash.setAccountNumber("");
		distributionDetailCash.setAccountDescription("");
		distributionDetailCash.setAccountTableRowIndex("");
		distributionDetailCash.setDistributionReference("");
		distributionDetailCash.setType(ARDistributionType.CASH.name());
		distributionDetailCash.setTypeId(ARDistributionType.CASH.getIndex());
		distributionDetailCash.setDebitAmount(arTransactionTotalAmount);
		
		
		if(glAccountsTableAccumulationCash!=null){
			distributionDetailCash.setAccountTableRowIndex(glAccountsTableAccumulationCash.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationCash);
			distributionDetailCash.setAccountNumber(object[0].toString());
			distributionDetailCash.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailCash);
		return distributionDetailList;
	}

	
	
	public List<DtoARDistributionDetail> getAccountReceivableDistibution(Double arTransactionTotalAmount,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationAccRecivable)
	{
		DtoARDistributionDetail distributionDetailAccountRecv= new DtoARDistributionDetail();
		distributionDetailAccountRecv.setAccountNumber("");
		distributionDetailAccountRecv.setAccountDescription("");
		distributionDetailAccountRecv.setAccountTableRowIndex("");
		distributionDetailAccountRecv.setDistributionReference("");
		distributionDetailAccountRecv.setType(ARDistributionType.RECV.name());
		distributionDetailAccountRecv.setTypeId(ARDistributionType.RECV.getIndex());
		distributionDetailAccountRecv.setDebitAmount(arTransactionTotalAmount);
		
		if(glAccountsTableAccumulationAccRecivable!=null){
			distributionDetailAccountRecv.setAccountTableRowIndex(glAccountsTableAccumulationAccRecivable.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationAccRecivable);
			distributionDetailAccountRecv.setAccountNumber(object[0].toString());
			distributionDetailAccountRecv.setAccountDescription(object[3].toString());
		}
		
		distributionDetailList.add(distributionDetailAccountRecv);
		return distributionDetailList;
	}
	
	public List<DtoARDistributionDetail> getAccountReceivableCreditDistibution(Double arTransactionTotalAmount,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationAccRecivable)
	{
		DtoARDistributionDetail distributionDetailAccountRecv= new DtoARDistributionDetail();
		distributionDetailAccountRecv.setAccountNumber("");
		distributionDetailAccountRecv.setAccountDescription("");
		distributionDetailAccountRecv.setAccountTableRowIndex("");
		distributionDetailAccountRecv.setDistributionReference("");
		distributionDetailAccountRecv.setType(ARDistributionType.RECV.name());
		distributionDetailAccountRecv.setTypeId(ARDistributionType.RECV.getIndex());
		distributionDetailAccountRecv.setDebitAmount(arTransactionTotalAmount);
		
		if(glAccountsTableAccumulationAccRecivable!=null){
			distributionDetailAccountRecv.setAccountTableRowIndex(glAccountsTableAccumulationAccRecivable.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationAccRecivable);
			distributionDetailAccountRecv.setAccountNumber(object[0].toString());
			distributionDetailAccountRecv.setAccountDescription(object[3].toString());
		}
		
		distributionDetailList.add(distributionDetailAccountRecv);
		return distributionDetailList;
	}
	
	
	public List<DtoARDistributionDetail> getCostOfSalesAccountDistibution(Double arTransactionCost,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationCostOfSales)
	{
		DtoARDistributionDetail distributionDetailCostOfSales= new DtoARDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(ARDistributionType.COGS.name());
		distributionDetailCostOfSales.setTypeId(ARDistributionType.COGS.getIndex());
		distributionDetailCostOfSales.setDebitAmount(arTransactionCost);
		
		
		if(glAccountsTableAccumulationCostOfSales!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationCostOfSales.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationCostOfSales);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
			distributionDetailCostOfSales.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}
	
	
	public List<DtoARDistributionDetail> getInventoryDisAccountDistibution(Double arTransactionCost,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationInventory)
	{
		DtoARDistributionDetail distributionDetailInventory= new DtoARDistributionDetail();
		
		distributionDetailInventory.setAccountNumber("");
		distributionDetailInventory.setAccountDescription("");
		distributionDetailInventory.setAccountTableRowIndex("");
		distributionDetailInventory.setDistributionReference("");
		distributionDetailInventory.setType(ARDistributionType.INVT.name());
		distributionDetailInventory.setTypeId(ARDistributionType.INVT.getIndex());
		distributionDetailInventory.setCreditAmount(arTransactionCost);
		
		if(glAccountsTableAccumulationInventory!=null){
			distributionDetailInventory.setAccountTableRowIndex(glAccountsTableAccumulationInventory.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationInventory);
			distributionDetailInventory.setAccountNumber(object[0].toString());
			distributionDetailInventory.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailInventory);
		return distributionDetailList;
	}
	
	public List<DtoARDistributionDetail> getSalsDisAccountDistibution(Double arTransactionSalesAmount,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationSales){
	
		DtoARDistributionDetail distributionDetailSales= new DtoARDistributionDetail();
		distributionDetailSales.setAccountNumber("");
		distributionDetailSales.setAccountDescription("");
		distributionDetailSales.setAccountTableRowIndex("");
		distributionDetailSales.setDistributionReference("");
		distributionDetailSales.setType(ARDistributionType.SALS.name());
		distributionDetailSales.setTypeId(ARDistributionType.SALS.getIndex());
		distributionDetailSales.setCreditAmount(arTransactionSalesAmount);
		
		if(glAccountsTableAccumulationSales!=null){
			distributionDetailSales.setAccountTableRowIndex(glAccountsTableAccumulationSales.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationSales);
			distributionDetailSales.setAccountNumber(object[0].toString());
			distributionDetailSales.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailSales);
		return distributionDetailList;
	}
	
	public List<DtoARDistributionDetail> getDebitMemoAccountDistibution(Double arTransactionDebitMemoAmount,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationDebitMemo){
	
		DtoARDistributionDetail distributionDetailDebitMemo= new DtoARDistributionDetail();
		distributionDetailDebitMemo.setAccountNumber("");
		distributionDetailDebitMemo.setAccountDescription("");
		distributionDetailDebitMemo.setAccountTableRowIndex("");
		distributionDetailDebitMemo.setDistributionReference("");
		distributionDetailDebitMemo.setType(ARDistributionType.DMEM.name());
		distributionDetailDebitMemo.setTypeId(ARDistributionType.DMEM.getIndex());
		distributionDetailDebitMemo.setCreditAmount(arTransactionDebitMemoAmount);
		
		if(glAccountsTableAccumulationDebitMemo!=null){
			distributionDetailDebitMemo.setAccountTableRowIndex(glAccountsTableAccumulationDebitMemo.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationDebitMemo);
			distributionDetailDebitMemo.setAccountNumber(object[0].toString());
			distributionDetailDebitMemo.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailDebitMemo);
		return distributionDetailList;
	}
	
	public List<DtoARDistributionDetail> getTradeDisAccountDistibution(Double arTransactionTradeDiscount,
			List<DtoARDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationTradeDis){
		//Set Trade Discount Account Number Information
		DtoARDistributionDetail distributionDetailTradeDiscount= new DtoARDistributionDetail();
		distributionDetailTradeDiscount.setAccountNumber("");
		distributionDetailTradeDiscount.setAccountDescription("");
		distributionDetailTradeDiscount.setAccountTableRowIndex("");
		distributionDetailTradeDiscount.setDistributionReference("");
		distributionDetailTradeDiscount.setType(ARDistributionType.TRAD.name());
		distributionDetailTradeDiscount.setTypeId(ARDistributionType.TRAD.getIndex());
		distributionDetailTradeDiscount.setDebitAmount(arTransactionTradeDiscount);
		
		if(glAccountsTableAccumulationTradeDis!=null){
			distributionDetailTradeDiscount.setAccountTableRowIndex(glAccountsTableAccumulationTradeDis.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationTradeDis);
			distributionDetailTradeDiscount.setAccountNumber(object[0].toString());
			distributionDetailTradeDiscount.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailTradeDiscount);
		return distributionDetailList;
	}
	
	public List<DtoARDistributionDetail> getMiscellaneousAccountDistibution(Double arTransactionMiscellaneous,
			RMSetup rmSetup, List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationMis, GLAccountsTableAccumulation glAccountsTableAccumulationMisTax){

		DtoARDistributionDetail distributionDetailMis= new DtoARDistributionDetail();
		distributionDetailMis.setAccountNumber("");
		distributionDetailMis.setAccountDescription("");
		distributionDetailMis.setAccountTableRowIndex("");
		distributionDetailMis.setDistributionReference("");
		distributionDetailMis.setType(ARDistributionType.MISC.name());
		distributionDetailMis.setTypeId(ARDistributionType.MISC.getIndex());
		distributionDetailMis.setCreditAmount(arTransactionMiscellaneous);
		
		
		if(glAccountsTableAccumulationMis!=null){
			distributionDetailMis.setAccountTableRowIndex(glAccountsTableAccumulationMis.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationMis);
			distributionDetailMis.setAccountNumber(object[0].toString());
			distributionDetailMis.setAccountDescription(object[3].toString());
		}
		
		distributionDetailList.add(distributionDetailMis);
		
		//Set Miscellaneous Tax Account Number Information
		
		double misTaxAmount=0.0;
		if(rmSetup!=null && rmSetup.getMiscVatScheduleId()!=null){
			misTaxAmount =(arTransactionMiscellaneous*rmSetup.getMiscVatScheduleId().getBasperct())/100;
		}
		
		
		DtoARDistributionDetail distributionDetailMisTax= new DtoARDistributionDetail();
		distributionDetailMisTax.setAccountNumber("");
		distributionDetailMisTax.setAccountDescription("");
		distributionDetailMisTax.setAccountTableRowIndex("");
		distributionDetailMisTax.setDistributionReference("");
		distributionDetailMisTax.setType(ARDistributionType.MISC_TAX.name());
		distributionDetailMisTax.setTypeId(ARDistributionType.MISC_TAX.getIndex());
		distributionDetailMisTax.setCreditAmount(misTaxAmount);
		
		
		if(glAccountsTableAccumulationMisTax!=null){
			distributionDetailMisTax.setAccountTableRowIndex(glAccountsTableAccumulationMisTax.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationMisTax);
			distributionDetailMisTax.setAccountNumber(object[0].toString());
			distributionDetailMisTax.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailMisTax);
		return distributionDetailList;
	}
	
	public List<DtoARDistributionDetail> getFreightAccountDistibution(Double arTransactionFreightAmount,
			RMSetup rmSetup, List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationFreight, GLAccountsTableAccumulation glAccountsTableAccumulationFreightTax){
		
		DtoARDistributionDetail distributionDetailFreight= new DtoARDistributionDetail();
		distributionDetailFreight.setAccountNumber("");
		distributionDetailFreight.setAccountDescription("");
		distributionDetailFreight.setAccountTableRowIndex("");
		distributionDetailFreight.setDistributionReference("");
		distributionDetailFreight.setType(ARDistributionType.FRGH.name());
		distributionDetailFreight.setTypeId(ARDistributionType.FRGH.getIndex());
		distributionDetailFreight.setCreditAmount(arTransactionFreightAmount);
		
		if(glAccountsTableAccumulationFreight!=null)
		{
			distributionDetailFreight.setAccountTableRowIndex(glAccountsTableAccumulationFreight.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationFreight);
			distributionDetailFreight.setAccountNumber(object[0].toString());
			distributionDetailFreight.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailFreight);
		//Set Freight Tax Account Number Information
		double freightTaxAmount=0.0;
		if(rmSetup!=null && rmSetup.getFreightVatScheduleId()!=null){
			freightTaxAmount =(arTransactionFreightAmount*rmSetup.getFreightVatScheduleId().getBasperct())/100;
		}
		
		DtoARDistributionDetail distributionDetailFreightTax= new DtoARDistributionDetail();
		distributionDetailFreightTax.setAccountNumber("");
		distributionDetailFreightTax.setAccountDescription("");
		distributionDetailFreightTax.setAccountTableRowIndex("");
		distributionDetailFreightTax.setDistributionReference("");
		distributionDetailFreightTax.setType(ARDistributionType.FRGH_TAX.name());
		distributionDetailFreightTax.setTypeId(ARDistributionType.FRGH_TAX.getIndex());
		distributionDetailFreightTax.setCreditAmount(freightTaxAmount);
		
		if(glAccountsTableAccumulationFreightTax!=null){
			distributionDetailFreightTax.setAccountTableRowIndex(glAccountsTableAccumulationFreightTax.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationFreightTax);
			distributionDetailFreightTax.setAccountNumber(object[0].toString());
			distributionDetailFreightTax.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailFreightTax);
		
		return distributionDetailList;
	}
	
	public List<DtoARDistributionDetail> getVatAccountDistibution(Double arTransactionVATAmount,
			List<DtoARDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationVat){
	
		DtoARDistributionDetail distributionDetailVatSchedule= new DtoARDistributionDetail();
		distributionDetailVatSchedule.setAccountNumber("");
		distributionDetailVatSchedule.setAccountDescription("");
		distributionDetailVatSchedule.setAccountTableRowIndex("");
		distributionDetailVatSchedule.setDistributionReference("");
		distributionDetailVatSchedule.setType(ARDistributionType.TAXS.name());
		distributionDetailVatSchedule.setTypeId(ARDistributionType.TAXS.getIndex());
		distributionDetailVatSchedule.setCreditAmount(arTransactionVATAmount);
		
		
		if(glAccountsTableAccumulationVat!=null)
		{
			distributionDetailVatSchedule.setAccountTableRowIndex(glAccountsTableAccumulationVat.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationVat);
			distributionDetailVatSchedule.setAccountNumber(object[0].toString());
			distributionDetailVatSchedule.setAccountDescription(object[3].toString());
		}
		distributionDetailList.add(distributionDetailVatSchedule);
		return distributionDetailList;
   }
	
	public boolean saveDistributionDetail(
			DtoARCashReceiptDistribution dtoARCashReceiptDistribution) throws ScriptException 
	{
		deleteTransactionEntryDistribution(dtoARCashReceiptDistribution.getTransactionNumber());
		ARTransactionEntry arTransactionEntry = repositoryARTransactionEntry.findByArTransactionNumber(dtoARCashReceiptDistribution.getTransactionNumber());
		if(arTransactionEntry!=null)
		{
			Double exchangeTableRate= arTransactionEntry.getExchangeTableRate();
			String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
			if(UtilRandomKey.isNotBlank(String.valueOf(arTransactionEntry.getExchangeTableIndex()))){
				CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(arTransactionEntry.getExchangeTableIndex(), false);
			    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
				   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
			    }
			}
			
			if(!dtoARCashReceiptDistribution.getListDtoARDistributionDetail().isEmpty())
			{
				for (DtoARDistributionDetail dtoARDistributionDetail : dtoARCashReceiptDistribution.getListDtoARDistributionDetail()) 
				{
					ScriptEngineManager manager = new ScriptEngineManager();
					ScriptEngine mathEval = manager.getEngineByName("js");
					Double originalDebitAmount=dtoARDistributionDetail.getDebitAmount(); 
					Double originalCreditAmount = dtoARDistributionDetail.getCreditAmount();
					ARTransactionEntryDistribution arTransactionEntryDistribution= new ARTransactionEntryDistribution();
					arTransactionEntryDistribution.setArTransactionNumber(dtoARCashReceiptDistribution.getTransactionNumber());
					arTransactionEntryDistribution.setOriginalDebitAmount(originalDebitAmount!=null?originalDebitAmount:0.0);
					arTransactionEntryDistribution.setOriginalCreditAmount(originalCreditAmount!=null?originalCreditAmount:0.0);
					arTransactionEntryDistribution.setTransactionType(dtoARDistributionDetail.getTypeId());
					Double debitAmount=0.0;
					if(originalDebitAmount!=null && exchangeTableRate!=null){
						 debitAmount = (double) mathEval.eval(originalDebitAmount + calcMethod + exchangeTableRate);
					}
					
					Double creditAmount=0.0;
					if(originalCreditAmount!=null && exchangeTableRate!=null){
						creditAmount = (double) mathEval.eval(originalCreditAmount + calcMethod + exchangeTableRate);
					}
					
					arTransactionEntryDistribution.setDebitAmount(debitAmount);
					arTransactionEntryDistribution.setCreditAmount(creditAmount);
					arTransactionEntryDistribution.setModifyDate(new Date());
					if(UtilRandomKey.isNotBlank(dtoARDistributionDetail.getAccountTableRowIndex())){
						arTransactionEntryDistribution.setAccountTableRowIndex(Integer.parseInt(dtoARDistributionDetail.getAccountTableRowIndex()));
					}
					arTransactionEntryDistribution.setDistributionDescription(dtoARDistributionDetail.getDistributionReference());
					arTransactionEntryDistribution.setModifyByUserID(httpServletRequest.getHeader(USER_ID));
					repositoryARTransactionEntryDistribution.saveAndFlush(arTransactionEntryDistribution);
				}
				return true;
			}
		}
		return false;
	}
	
	public DtoARTransactionEntry postARTransactionEntry(DtoARTransactionEntry dtoARTransactionEntry) throws ScriptException 
	{
	    ARTransactionEntry arTransactionEntry=	repositoryARTransactionEntry.findByArTransactionNumber(dtoARTransactionEntry.getArTransactionNumber());
		Double exchangeTableRate= dtoARTransactionEntry.getExchangeTableRate();
		String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine mathEval = manager.getEngineByName("js");
		
		if(UtilRandomKey.isNotBlank(String.valueOf(dtoARTransactionEntry.getExchangeTableIndex()))){
			CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(dtoARTransactionEntry.getExchangeTableIndex(), false);
		    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
			   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
		    }
		}
		
		double totalAmount=0.0;
		try {
			totalAmount = (double) mathEval.eval(dtoARTransactionEntry.getArTransactionTotalAmount() + calcMethod + exchangeTableRate);
		} catch (ScriptException e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		 //Get List for saving data in AR90201
		 /*if(arTransactionEntryDistributionList!=null && !arTransactionEntryDistributionList.isEmpty()){
			 openTransactionDistributionList=saveOpenTransactionDistributionDetailFromTransactionDistributionDetail(arTransactionEntryDistributionList, dtoARTransactionEntry);
		 }
		 else
		 {*/
		  List<ARYTDOpenTransactionsDistribution> openTransactionDistributionList=saveOpenTransactionDistribution(dtoARTransactionEntry,calcMethod,mathEval);
		// }
		 
		 if(openTransactionDistributionList!=null && !openTransactionDistributionList.isEmpty())
		 {
			   // Save in Ar50000
				saveArSummaryBalanceInPostTrans(dtoARTransactionEntry, totalAmount);
				//Save In AR50001
				saveArBalanceByPeriodsInPostTrans(dtoARTransactionEntry, totalAmount);
				//Save In AR90200
				saveOpenTransactionInPostTrans(dtoARTransactionEntry, arTransactionEntry);
				//Save In AR90201
			    repositoryARYTDOpenTransactionDistribution.save(openTransactionDistributionList);
			    
			    if(dtoARTransactionEntry.getIsPayment()){
					// Save in Ar90400 and 90401
					arYTDOpenPaymentSaveInPostTransaction(dtoARTransactionEntry, totalAmount, arTransactionEntry);
			    }
			 
				 // create New Batch in Journal Entry Module
				 GLBatches glBatches = createNewGlBatchesInPostCashReceiptEntry(dtoARTransactionEntry);
				 //Create new JV Entry
				 createNewJvEntryInPostTransactionEntry(openTransactionDistributionList, dtoARTransactionEntry, glBatches, mathEval);
				 List<ARTransactionEntryDistribution> arTransactionEntryDistributionList=repositoryARTransactionEntryDistribution.
							findByArTransactionNumber(dtoARTransactionEntry.getArTransactionNumber());
				 if(arTransactionEntryDistributionList!=null){
					 repositoryARTransactionEntryDistribution.deleteInBatch(arTransactionEntryDistributionList);
				 }
			     repositoryARTransactionEntry.delete(arTransactionEntry);
		 }
		 else
		 {
			 dtoARTransactionEntry.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
			 return dtoARTransactionEntry;
		 }
		return dtoARTransactionEntry;
	}
	
	public GLBatches createNewGlBatchesInPostCashReceiptEntry(DtoARTransactionEntry dtoARTransactionEntry){
		int langid = serviceHome.getLanngugaeId();
		
		int year=0;
		int month=0;
		int day=0;
		Calendar cal = Calendar.getInstance();
		if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getArTransactionDate())) {
			Date transDate = UtilDateAndTime
					.ddmmyyyyStringToDate(dtoARTransactionEntry.getArTransactionDate());
			cal.setTime(transDate);
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH)+1;
			day = cal.get(Calendar.DATE);
		}
		
		String batchId=AuditTrailSeriesTypeConstant.AP+
				GlSerialConfigurationConstant.CASH_RECEIPT.getVaue()+day+month+year;
		String batchDesc=AuditTrailSeriesTypeConstant.AP+" "+
				GlSerialConfigurationConstant.CASH_RECEIPT.getVaue()+" "+dtoARTransactionEntry.getArTransactionNumber();
		
		GLBatches glBatches=repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(batchId, BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex(), false);
		if(glBatches==null){
			glBatches= new GLBatches();
			glBatches.setTotalAmountTransactionInBatch(dtoARTransactionEntry.getArTransactionTotalAmount());
			glBatches.setTotalNumberTransactionInBatch(1);
		}
		else{
			double amount;
			amount=glBatches.getTotalAmountTransactionInBatch()!=null?glBatches.getTotalAmountTransactionInBatch():0.0;
			glBatches.setTotalAmountTransactionInBatch(amount+dtoARTransactionEntry.getArTransactionTotalAmount());
			glBatches.setTotalNumberTransactionInBatch(glBatches.getTotalNumberTransactionInBatch()+1);
		}
		 
		glBatches.setBatchID(batchId);
		glBatches.setBatchDescription(batchDesc);
		
		BatchTransactionType batchTransactionType = repositoryBatchTransactionType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex(), langid, false);
		if (batchTransactionType != null) {
			glBatches.setBatchTransactionType(batchTransactionType);
		}
		
		glBatches.setPostingDate(new Date());
		glBatches = repositoryGLBatches.saveAndFlush(glBatches);
		return glBatches;
	}
	
	public boolean createNewJvEntryInPostTransactionEntry(List<ARYTDOpenTransactionsDistribution> arytdOpenTransactionDistributions ,
		DtoARTransactionEntry dtoARTransactionEntry , GLBatches glBatches,ScriptEngine mathEval) throws ScriptException{
	    //Save Data in gl10100
	    int nextJournalId = 0;
	    GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
		if (glConfigurationSetup == null) {
			return false;
		}
		
		JournalEntryHeader journalEntryHeader = new JournalEntryHeader();
		nextJournalId = glConfigurationSetup.getNextJournalEntryVoucher();
		journalEntryHeader.setJournalID(String.valueOf(nextJournalId));
		journalEntryHeader.setOriginalTransactionNumberFromModule(dtoARTransactionEntry.getArTransactionNumber());
	    journalEntryHeader.setGlBatches(glBatches);
	    journalEntryHeader.setTransactionType(CommonConstant.STANDARD);
	    if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getArTransactionDate())) {
		 journalEntryHeader.setTransactionDate(
				UtilDateAndTime.ddmmyyyyStringToDate(dtoARTransactionEntry.getArTransactionDate()));
	    } 

		GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
				.findFirstByGlConfigurationAuditTrialSeriesTypeTypeIdAndGlConfigurationAuditTrialSeriesTypeLanguageLanguageIdAndSourceDocumentContainingIgnoreCase(AuditTrailSeriesTypeConstant.AR.getIndex(),1,"SALES");
		journalEntryHeader.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
		journalEntryHeader.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
		journalEntryHeader.setJournalDescription(dtoARTransactionEntry.getArTransactionDescription());
		journalEntryHeader.setTransactionSource("JV");
		CurrencySetup currencySetup = repositoryCurrencySetup
				.findByCurrencyIdAndIsDeleted(dtoARTransactionEntry.getCurrencyID(), false);
		journalEntryHeader.setCurrencySetup(currencySetup);
		journalEntryHeader.setOriginalTotalJournalEntryCredit(dtoARTransactionEntry.getArTransactionTotalAmount());
		journalEntryHeader.setOriginalTotalJournalEntryDebit(dtoARTransactionEntry.getArTransactionTotalAmount());
		
		Double currentExchangeRate = Double.valueOf(dtoARTransactionEntry.getExchangeTableRate());
		journalEntryHeader.setExchangeTableRate(currentExchangeRate);
		CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
				.findByExchangeIndexAndIsDeleted(dtoARTransactionEntry.getExchangeTableIndex(), false);
		String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
		if (currencyExchangeHeader != null) {
			journalEntryHeader.setCurrencyExchangeHeader(currencyExchangeHeader);
			
				if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCurrencyID())) {
					if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
						calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
								.getMethod();
					}

						double totalAmount = (double) mathEval
								.eval(Double.valueOf(dtoARTransactionEntry.getArTransactionTotalAmount()) + calcMethod
										+ currentExchangeRate);
						journalEntryHeader.setTotalJournalEntryDebit(totalAmount);
						
						journalEntryHeader.setTotalJournalEntryCredit(totalAmount);

				}
		}

		        journalEntryHeader = repositoryJournalEntryHeader.save(journalEntryHeader);
			    glConfigurationSetup.setNextJournalEntryVoucher(nextJournalId + 1);
			    repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);
				 if(arytdOpenTransactionDistributions!=null && !arytdOpenTransactionDistributions.isEmpty())
				 {
					 for (ARYTDOpenTransactionsDistribution arytdOpenTransactionEntryDistribution : arytdOpenTransactionDistributions) 
					 {
						    JournalEntryDetails journalEntryDetails = new JournalEntryDetails();
							journalEntryDetails.setAccountTableRowIndex(String.valueOf(arytdOpenTransactionEntryDistribution.getAccountTableRowIndex()));
							
							journalEntryDetails.setDistributionDescription(arytdOpenTransactionEntryDistribution.getDistributionDescription());
							if (arytdOpenTransactionEntryDistribution.getOriginalCreditAmount()>0) {
								journalEntryDetails.setBalanceType(2);
							} else {
								journalEntryDetails.setBalanceType(1);
							}
							
							journalEntryDetails.setOriginalCreditAmount(arytdOpenTransactionEntryDistribution.getOriginalCreditAmount());
							journalEntryDetails.setCreditAmount(arytdOpenTransactionEntryDistribution.getCreditAmount());
							journalEntryDetails.setOriginalDebitAmount(arytdOpenTransactionEntryDistribution.getOriginalDebitAmount());
							journalEntryDetails.setDebitAmount(arytdOpenTransactionEntryDistribution.getDebitAmount());
							journalEntryDetails.setIntercompanyIDMultiCompanyTransaction(String.valueOf(serviceHome.getCompanyIdFromCompanyTenant(httpServletRequest.getHeader(CommonConstant.TENANT_ID))));
							journalEntryDetails.setJournalEntryHeader(journalEntryHeader);
							journalEntryDetails.setExchangeTableRate(journalEntryHeader.getExchangeTableRate());
							repositoryJournalEntryDetail.save(journalEntryDetails);
					 }
				}
		
			return true;
	}
	
	public void saveArSummaryBalanceInPostTrans(DtoARTransactionEntry dtoARTransactionEntry, Double totalAmount){
		ARSummaryBalances arSummaryBalanceSender = repositoryARSummaryBalances.findByCustomerNumberAndArTransactionType(dtoARTransactionEntry.getCustomerID(),ARTransactionTypeConstant.SALES.getIndex());
		if(arSummaryBalanceSender==null){
			arSummaryBalanceSender= new ARSummaryBalances();
		}
		
		arSummaryBalanceSender.setCustomerNumber(dtoARTransactionEntry.getCustomerID());
		arSummaryBalanceSender.setArTransactionType(ARTransactionTypeConstant.SALES.getIndex());
		arSummaryBalanceSender.setTotalAmount(arSummaryBalanceSender.getTotalAmount()+totalAmount);
		arSummaryBalanceSender.setLastUpdateDate(new Date());
		arSummaryBalanceSender.setLastPostingDateLastTransaction(new Date());
		arSummaryBalanceSender.setRowDateindex(new Date());
		repositoryARSummaryBalances.saveAndFlush(arSummaryBalanceSender);
		
		ARSummaryBalances arSummaryReceiver = repositoryARSummaryBalances.findByCustomerNumberAndArTransactionType(dtoARTransactionEntry.getCustomerID(),ARTransactionTypeConstant.RECEIVABLE.getIndex());
		if(arSummaryReceiver==null){
			arSummaryReceiver= new ARSummaryBalances();
		}
		arSummaryReceiver.setCustomerNumber(dtoARTransactionEntry.getCustomerID());
		arSummaryReceiver.setArTransactionType(ARTransactionTypeConstant.RECEIVABLE.getIndex());
		arSummaryReceiver.setTotalAmount(arSummaryReceiver.getTotalAmount()+totalAmount);
		arSummaryReceiver.setLastUpdateDate(new Date());
		arSummaryReceiver.setLastPostingDateLastTransaction(new Date());
		arSummaryReceiver.setRowDateindex(new Date());
		repositoryARSummaryBalances.saveAndFlush(arSummaryReceiver);
		
		if(dtoARTransactionEntry.getIsPayment()){
			ARSummaryBalances arSummaryPayment = repositoryARSummaryBalances.findByCustomerNumberAndArTransactionType(dtoARTransactionEntry.getCustomerID(),ARTransactionTypeConstant.PAYMENTS.getIndex());
			if(arSummaryPayment==null){
				arSummaryPayment= new ARSummaryBalances();
			}
			arSummaryPayment.setCustomerNumber(dtoARTransactionEntry.getCustomerID());
			arSummaryPayment.setArTransactionType(ARTransactionTypeConstant.PAYMENTS.getIndex());
			arSummaryPayment.setTotalAmount(arSummaryPayment.getTotalAmount()+Double.valueOf(dtoARTransactionEntry.getPaymentAmount()));
			arSummaryPayment.setLastUpdateDate(new Date());
			arSummaryPayment.setLastPostingDateLastTransaction(new Date());
			arSummaryPayment.setRowDateindex(new Date());
			repositoryARSummaryBalances.saveAndFlush(arSummaryPayment);
		}
	}
	
	public void saveArBalanceByPeriodsInPostTrans(DtoARTransactionEntry dtoARTransactionEntry, Double totalAmount){
		ARBalancebyPeriods arBalancebyPeriods = null;
		int periodId=0;
		Calendar cal = Calendar.getInstance();
		if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getArTransactionDate())) {
			Date transDate = UtilDateAndTime
					.ddmmyyyyStringToDate(dtoARTransactionEntry.getArTransactionDate());
			cal.setTime(transDate);
			periodId = cal.get(Calendar.MONTH)+1;
		}
			
		arBalancebyPeriods = repositoryARBalanceByPeriods.
				findByCustomerNumberAndPeriodIndexing(dtoARTransactionEntry.getCustomerID(), periodId);
		if(arBalancebyPeriods==null){
			arBalancebyPeriods = new ARBalancebyPeriods();
		}
		
		arBalancebyPeriods.setCustomerNumber(dtoARTransactionEntry.getCustomerID());
		arBalancebyPeriods.setPeriodIndexing(periodId);
		arBalancebyPeriods.setPeriodDays(0);
		if(dtoARTransactionEntry.getIsPayment()){
			arBalancebyPeriods.setTotalBalancePayments(arBalancebyPeriods.getTotalBalancePayments()+Double.valueOf(dtoARTransactionEntry.getPaymentAmount()));
		}
		else
		{
			arBalancebyPeriods.setTotalBalanceAmount(arBalancebyPeriods.getTotalBalanceAmount()+totalAmount);
		}
		
		arBalancebyPeriods.setRowDateindex(new Date());
		repositoryARBalanceByPeriods.saveAndFlush(arBalancebyPeriods);
	}
	
	public void saveOpenTransactionInPostTrans(DtoARTransactionEntry dtoARTransactionEntry, ARTransactionEntry arTransactionEntry){
		ARYTDOpenTransactions arytdOpenTransactions= new ARYTDOpenTransactions();
		arytdOpenTransactions.setArTransactionNumber(dtoARTransactionEntry.getArTransactionNumber());
		arytdOpenTransactions.setArTransactionType(dtoARTransactionEntry.getArTransactionType());
		arytdOpenTransactions.setArTransactionDescription(dtoARTransactionEntry.getArTransactionDescription());
		arytdOpenTransactions.setArTransactionDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARTransactionEntry.getArTransactionDate()));
		arytdOpenTransactions.setCustomerID(dtoARTransactionEntry.getCustomerID());
		arytdOpenTransactions.setCustomerName(dtoARTransactionEntry.getCustomerName());
		arytdOpenTransactions.setCustomerNameArabic(dtoARTransactionEntry.getCustomerNameArabic());
		arytdOpenTransactions.setCurrencyID(dtoARTransactionEntry.getCurrencyID());
		arytdOpenTransactions.setPaymentTermsID(dtoARTransactionEntry.getPaymentTermsID());
		
		arytdOpenTransactions.setSalesmanID(dtoARTransactionEntry.getSalesmanID());
		arytdOpenTransactions.setShippingMethodID(dtoARTransactionEntry.getShippingMethodID());
		arytdOpenTransactions.setvATScheduleID(dtoARTransactionEntry.getVatScheduleID());
		arytdOpenTransactions.setSalesTerritoryId(dtoARTransactionEntry.getTerritoryId());
		
		// Set Transaction Amount
		arytdOpenTransactions.setArTransactionFreightAmount(dtoARTransactionEntry.getArTransactionFreightAmount() != null
						? dtoARTransactionEntry.getArTransactionFreightAmount() : 0.0);
		arytdOpenTransactions.setArTransactionCashAmount(dtoARTransactionEntry.getArTransactionCashAmount() != null
				? dtoARTransactionEntry.getArTransactionCashAmount() : 0.0);
		arytdOpenTransactions.setArTransactionCheckAmount(dtoARTransactionEntry.getArTransactionCheckAmount() != null
				? dtoARTransactionEntry.getArTransactionCheckAmount() : 0.0);
		arytdOpenTransactions
				.setArTransactionCreditCardAmount(dtoARTransactionEntry.getArTransactionCreditCardAmount() != null
						? dtoARTransactionEntry.getArTransactionCreditCardAmount() : 0.0);
		arytdOpenTransactions
				.setArTransactionCreditMemoAmount(dtoARTransactionEntry.getArTransactionCreditMemoAmount() != null
						? dtoARTransactionEntry.getArTransactionCreditMemoAmount() : 0.0);
		arytdOpenTransactions
				.setArTransactionDebitMemoAmount(dtoARTransactionEntry.getArTransactionDebitMemoAmount() != null
						? dtoARTransactionEntry.getArTransactionDebitMemoAmount() : 0.0);
		arytdOpenTransactions.setArTransactionFinanceChargeAmount(
				dtoARTransactionEntry.getArTransactionFinanceChargeAmount() != null
						? dtoARTransactionEntry.getArTransactionFinanceChargeAmount() : 0.0);
		arytdOpenTransactions
				.setArTransactionWarrantyAmount(dtoARTransactionEntry.getArTransactionWarrantyAmount() != null
						? dtoARTransactionEntry.getArTransactionWarrantyAmount() : 0.0);
		// Set Other Amounts
		arytdOpenTransactions.setaRTransactionCost(dtoARTransactionEntry.getArTransactionCost() != null
				? dtoARTransactionEntry.getArTransactionCost() : 0.0);
		arytdOpenTransactions.setArTransactionSalesAmount(dtoARTransactionEntry.getArTransactionSalesAmount() != null
				? dtoARTransactionEntry.getArTransactionSalesAmount() : 0.0);
		arytdOpenTransactions
				.setArTransactionTradeDiscount(dtoARTransactionEntry.getArTransactionTradeDiscount() != null
						? dtoARTransactionEntry.getArTransactionTradeDiscount() : 0.0);
		arytdOpenTransactions
				.setArTransactionMiscellaneous(dtoARTransactionEntry.getArTransactionMiscellaneous() != null
						? dtoARTransactionEntry.getArTransactionMiscellaneous() : 0.0);
		arytdOpenTransactions.setArTransactionVATAmount(dtoARTransactionEntry.getArTransactionVATAmount() != null
				? dtoARTransactionEntry.getArTransactionVATAmount() : 0.0);
		arytdOpenTransactions.setArTransactionTotalAmount(dtoARTransactionEntry.getArTransactionTotalAmount() != null
				? dtoARTransactionEntry.getArTransactionTotalAmount() : 0.0);
		arytdOpenTransactions.setArServiceRepairAmount(dtoARTransactionEntry.getArServiceRepairAmount()!=null?
				dtoARTransactionEntry.getArServiceRepairAmount():0.0);
		arytdOpenTransactions.setArReturnAmount(dtoARTransactionEntry.getArReturnAmount()!=null?
				dtoARTransactionEntry.getArReturnAmount():0.0);
		arytdOpenTransactions.setCheckbookID(dtoARTransactionEntry.getCheckbookID());

		if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCheckNumber())) {
			arytdOpenTransactions.setCheckNumber(dtoARTransactionEntry.getCheckNumber());
		}

		if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCreditCardID())) {
			arytdOpenTransactions.setCreditCardID(dtoARTransactionEntry.getCreditCardID());
			arytdOpenTransactions.setCreditCardNumber(dtoARTransactionEntry.getCreditCardNumber());
			arytdOpenTransactions.setCreditCardExpireMonth(Integer.parseInt(dtoARTransactionEntry.getCreditCardExpireMonth()));
			arytdOpenTransactions.setCreditCardExpireYear(Integer.parseInt(dtoARTransactionEntry.getCreditCardExpireYear()));
		}
		//Need to update with constant
		arytdOpenTransactions.setArTransactionStatus(3);// 3 = Posting
		if (dtoARTransactionEntry.getExchangeTableIndex() != null) {
			arytdOpenTransactions.setExchangeTableIndex(dtoARTransactionEntry.getExchangeTableIndex());
			arytdOpenTransactions.setExchangeTableRate(dtoARTransactionEntry.getExchangeTableRate());
		}
		
		arytdOpenTransactions.setPayment(false);
		if(dtoARTransactionEntry.getIsPayment())
		{
			arytdOpenTransactions.setPayment(true);
			arytdOpenTransactions.setCashReceiptNumber(arTransactionEntry.getCashReceiptNumber());
			arytdOpenTransactions.setCompletePaymentApplied(true);
		}
		else
		{
			arytdOpenTransactions.setCompletePaymentApplied(false);
		}
		
		arytdOpenTransactions.setTransactionVoid(dtoARTransactionEntry.getTransactionVoid());
		arytdOpenTransactions.setRowDateindex(new Date());
		arytdOpenTransactions.setPostingDate(new Date());
		repositoryARYTDOpenTransaction.saveAndFlush(arytdOpenTransactions);
	}
	
	public List<ARYTDOpenTransactionsDistribution> saveOpenTransactionDistribution(DtoARTransactionEntry dtoARTransactionEntry,
			String calcMethod, ScriptEngine mathEval) throws ScriptException
	{
		List<ARYTDOpenTransactionsDistribution> list = new ArrayList<>();
		dtoARTransactionEntry.setButtonType(MessageLabel.NEW_DISTRIBUTION);
		DtoARCashReceiptDistribution dtoARDistribution=getTransactionalDistribution(dtoARTransactionEntry);
		if(dtoARDistribution!=null && dtoARDistribution.getListDtoARDistributionDetail()!=null && 
				!dtoARDistribution.getListDtoARDistributionDetail().isEmpty())
		{
			for (DtoARDistributionDetail dtoARDistributionDetail : dtoARDistribution.getListDtoARDistributionDetail()) 
			{
				if(UtilRandomKey.isNotBlank(dtoARDistributionDetail.getAccountNumber()) && 
						UtilRandomKey.isNotBlank(dtoARDistributionDetail.getAccountTableRowIndex()) )
				{
					ARYTDOpenTransactionsDistribution arytdOpenTransactionsDistribution=new ARYTDOpenTransactionsDistribution();
					arytdOpenTransactionsDistribution.setArTransactionNumber(dtoARTransactionEntry.getArTransactionNumber());
					arytdOpenTransactionsDistribution.setCustomerNumber(dtoARTransactionEntry.getCustomerID());
					arytdOpenTransactionsDistribution.setAccountTableRowIndex(Integer.parseInt(dtoARDistributionDetail.getAccountTableRowIndex()));
					arytdOpenTransactionsDistribution.setDistributionDescription(dtoARDistributionDetail.getDistributionReference());
					arytdOpenTransactionsDistribution.setRowDateindex(new Date());
					arytdOpenTransactionsDistribution.setTransactionType(dtoARDistributionDetail.getTypeId());
					Double debitAmount=0.0;
					Double creditAmount=0.0;
					
					arytdOpenTransactionsDistribution.setOriginalCreditAmount(0.0);
					arytdOpenTransactionsDistribution.setOriginalDebitAmount(0.0);
					
					if(dtoARDistributionDetail.getCreditAmount()!=null && dtoARDistributionDetail.getCreditAmount()>0){
						arytdOpenTransactionsDistribution.setOriginalCreditAmount(dtoARDistributionDetail.getCreditAmount());
						if(dtoARTransactionEntry.getExchangeTableRate()!=null && dtoARTransactionEntry.getExchangeTableRate()>0){
							creditAmount=(double) mathEval.eval(dtoARDistributionDetail.getCreditAmount() + calcMethod + dtoARTransactionEntry.getExchangeTableRate());
						}
					}
					
					if(dtoARDistributionDetail.getDebitAmount()!=null && dtoARDistributionDetail.getDebitAmount()>0){
						arytdOpenTransactionsDistribution.setOriginalDebitAmount(dtoARDistributionDetail.getDebitAmount());
						if(dtoARTransactionEntry.getExchangeTableRate()!=null && dtoARTransactionEntry.getExchangeTableRate()>0){
							debitAmount=(double) mathEval.eval(dtoARDistributionDetail.getDebitAmount() + calcMethod + dtoARTransactionEntry.getExchangeTableRate());
						}
					}
					
					arytdOpenTransactionsDistribution.setCreditAmount(creditAmount);
					arytdOpenTransactionsDistribution.setDebitAmount(debitAmount);
					
					list.add(arytdOpenTransactionsDistribution);
				}
				else{
					return new ArrayList<>();
				}
			}
		}
		return list;
	}
	
	public List<ARYTDOpenTransactionsDistribution> saveOpenTransactionDistributionDetailFromTransactionDistributionDetail(List<ARTransactionEntryDistribution> arTransactionEntryDistributionList,
			DtoARTransactionEntry dtoARTransactionEntry)
	{
		List<ARYTDOpenTransactionsDistribution> list = new ArrayList<>();
		for (ARTransactionEntryDistribution arTransactionEntryDistribution : arTransactionEntryDistributionList) 
		{
			GLAccountsTableAccumulation glAccountsTableAccumulation= repositoryGLAccountsTableAccumulation.
					findByAccountTableRowIndexAndIsDeleted(String.valueOf(arTransactionEntryDistribution.getAccountTableRowIndex()), false);
			if(glAccountsTableAccumulation!=null)
			{
				ARYTDOpenTransactionsDistribution arytdOpenTransactionsDistribution=new ARYTDOpenTransactionsDistribution();
				arytdOpenTransactionsDistribution.setArTransactionNumber(arTransactionEntryDistribution.getArTransactionNumber());
				arytdOpenTransactionsDistribution.setCustomerNumber(dtoARTransactionEntry.getCustomerID());
				arytdOpenTransactionsDistribution.setAccountTableRowIndex(arTransactionEntryDistribution.getAccountTableRowIndex());
				arytdOpenTransactionsDistribution.setDistributionDescription(arTransactionEntryDistribution.getDistributionDescription());
				arytdOpenTransactionsDistribution.setRowDateindex(new Date());
				arytdOpenTransactionsDistribution.setTransactionType(arTransactionEntryDistribution.getTransactionType());
				arytdOpenTransactionsDistribution.setOriginalCreditAmount(arTransactionEntryDistribution.getOriginalCreditAmount());
				arytdOpenTransactionsDistribution.setOriginalDebitAmount(arTransactionEntryDistribution.getOriginalDebitAmount());
				arytdOpenTransactionsDistribution.setCreditAmount(arTransactionEntryDistribution.getCreditAmount());
				arytdOpenTransactionsDistribution.setDebitAmount(arTransactionEntryDistribution.getDebitAmount());
				list.add(arytdOpenTransactionsDistribution);
			}
			else{
				return new ArrayList<>();
			}
		}
		return list;
	}
	
	public void arYTDOpenPaymentSaveInPostTransaction(DtoARTransactionEntry dtoARTransactionEntry,double totalAmount,
			ARTransactionEntry arTransactionEntry)
	{
		//Save in AR90400 if payment true
		
		ARYTDOpenPayments arytdOpenPayments= new ARYTDOpenPayments();
		arytdOpenPayments.setPaymentNumber(arTransactionEntry.getCashReceiptNumber());
		arytdOpenPayments.setArTransactionNumber(dtoARTransactionEntry.getArTransactionNumber());
		arytdOpenPayments.setArTransactionType(dtoARTransactionEntry.getArTransactionType());
		arytdOpenPayments.setCheckbookID(dtoARTransactionEntry.getCheckbookID());
		arytdOpenPayments.setPaymentDescription(dtoARTransactionEntry.getArTransactionDescription());
		arytdOpenPayments.setPaymentCreateDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARTransactionEntry.getArTransactionDate()));
		arytdOpenPayments.setCustomerNumber(dtoARTransactionEntry.getCustomerID());
		arytdOpenPayments.setCurrencyID(dtoARTransactionEntry.getCurrencyID());
		arytdOpenPayments.setPaymentAmount(totalAmount);
		arytdOpenPayments.setOriginalPaymentAmount(dtoARTransactionEntry.getArTransactionTotalAmount());

		if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCheckNumber())) {
			arytdOpenPayments.setCheckNumber(dtoARTransactionEntry.getCheckNumber());
		}

		if (UtilRandomKey.isNotBlank(dtoARTransactionEntry.getCreditCardID())) {
			arytdOpenPayments.setCreditCardID(dtoARTransactionEntry.getCreditCardID());
			arytdOpenPayments.setCreditCardNumber(dtoARTransactionEntry.getCreditCardNumber());
			arytdOpenPayments.setCreditCardExpireMonth(Integer.parseInt(dtoARTransactionEntry.getCreditCardExpireMonth()));
			arytdOpenPayments.setCreditCardExpireYear(Integer.parseInt(dtoARTransactionEntry.getCreditCardExpireYear()));
		}
		
		if (dtoARTransactionEntry.getExchangeTableIndex() != null) {
			arytdOpenPayments.setExchangeTableID(String.valueOf(dtoARTransactionEntry.getExchangeTableIndex()));
			arytdOpenPayments.setExchangeTableRate(dtoARTransactionEntry.getExchangeTableRate());
		}
		
		arytdOpenPayments.setPaymentCreatebyUserID(arTransactionEntry.getModifyByUserID());
		arytdOpenPayments.setPaymentPostingDate(new Date());
		arytdOpenPayments.setPaymentPostingbyUserID(httpServletRequest.getHeader(USER_ID));
		arytdOpenPayments.setPaymentVoid(dtoARTransactionEntry.getTransactionVoid());
		arytdOpenPayments.setRowDateindex(new Date());
		repositoryARYTDOpenPayments.saveAndFlush(arytdOpenPayments);
		
		//AR90401
		List<ARYTDOpenTransactionsDistribution> arytdOpenTransactionsDistributionList = 
				repositoryARYTDOpenTransactionDistribution.findByArTransactionNumber(dtoARTransactionEntry.getArTransactionNumber());
		if(arytdOpenTransactionsDistributionList!=null && !arytdOpenTransactionsDistributionList.isEmpty()){
			for (ARYTDOpenTransactionsDistribution arytdOpenTransactionsDistribution : arytdOpenTransactionsDistributionList) {
				ARYTDOpenPaymentsDistribution arytdOpenPaymentsDistribution=new ARYTDOpenPaymentsDistribution();
				
				arytdOpenPaymentsDistribution.setPaymentNumber(arTransactionEntry.getCashReceiptNumber());
				arytdOpenPaymentsDistribution.setAccountTableRowIndex(arytdOpenTransactionsDistribution.getAccountTableRowIndex());
				arytdOpenPaymentsDistribution.setDistributionDescription(arytdOpenTransactionsDistribution.getDistributionDescription());
				arytdOpenPaymentsDistribution.setRowDateindex(new Date());
				arytdOpenPaymentsDistribution.setTransactionType(arytdOpenTransactionsDistribution.getTransactionType());
				arytdOpenPaymentsDistribution.setOriginalDebitAmount(arytdOpenTransactionsDistribution.getOriginalDebitAmount());
				arytdOpenPaymentsDistribution.setDebitAmount(arytdOpenTransactionsDistribution.getDebitAmount());
				arytdOpenPaymentsDistribution.setOriginalCreditAmount(arytdOpenTransactionsDistribution.getOriginalCreditAmount());
				arytdOpenPaymentsDistribution.setCreditAmount(arytdOpenTransactionsDistribution.getCreditAmount());
				repositoryARYTDOpenPaymentsDistribution.saveAndFlush(arytdOpenPaymentsDistribution);
			}
		}
	}

	/*public boolean checkDistributionDetailIsAvailable(String arTransactionNumber) 
	{
		DtoARCashReceiptDistribution newDistribution = dtoARTransactionEntry(arTransactionNumber);
		List<ARTransactionEntryDistribution> oldDistributionList = repositoryARTransactionEntryDistribution.findByArTransactionNumber(arTransactionNumber);
        List<DtoARDistributionDetail> distributionDetailList= new ArrayList<>();      
        if(oldDistributionList!=null && !oldDistributionList.isEmpty() && !newDistribution.getListDtoARDistributionDetail().isEmpty()){
        	for (ARTransactionEntryDistribution arTransactionEntryDistribution : oldDistributionList) {
        		
        		DtoARDistributionDetail dtoARDistributionDetail = new DtoARDistributionDetail();
        		dtoARDistributionDetail.setAccountNumber("");
        		dtoARDistributionDetail.setAccountDescription("");
        		dtoARDistributionDetail.setAccountTableRowIndex("");
        		dtoARDistributionDetail.setDistributionReference("");
        		if(arTransactionEntryDistribution.getAccountTableRowIndex()!=0){
        			String accTableRowIndex = String.valueOf(arTransactionEntryDistribution.getAccountTableRowIndex());
        			dtoARDistributionDetail.setAccountTableRowIndex(accTableRowIndex);
        			GLAccountsTableAccumulation glAccountsTableAccumulation= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(accTableRowIndex, false);
        			if(glAccountsTableAccumulation!=null)
        			{
        				Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
        				dtoARDistributionDetail.setAccountNumber(object[0].toString());
        				dtoARDistributionDetail.setAccountDescription(object[3].toString());
        			}
        		}
        		dtoARDistributionDetail.setType(ARDistributionType.getById(arTransactionEntryDistribution.getTransactionType()).name());
        		dtoARDistributionDetail.setTypeId(arTransactionEntryDistribution.getTransactionType());
        		if(arTransactionEntryDistribution.getOriginalDebitAmount()!=0.0){
        			dtoARDistributionDetail.setDebitAmount(arTransactionEntryDistribution.getOriginalDebitAmount());
        		}
        		if(arTransactionEntryDistribution.getOriginalCreditAmount()!=0.0){
        			dtoARDistributionDetail.setCreditAmount(arTransactionEntryDistribution.getOriginalCreditAmount());
        		}
        		distributionDetailList.add(dtoARDistributionDetail);
			}
        }
        else{
        	return false;
        }
        return listEquals(newDistribution.getListDtoARDistributionDetail(),distributionDetailList);
	}	*/
	
	private static boolean listEquals(List<DtoARDistributionDetail> list1, List<DtoARDistributionDetail> list2) {
        for (DtoARDistributionDetail myData : list1) {
        	for(DtoARDistributionDetail myData2:list2){
        		if(myData2.getTypeId()==myData.getTypeId()){
        			if(!myData2.equals(myData)){
        				return false;
        			}
        		}
        	}
        }
        return true;
    }
	
	public boolean checkTransDistributionDetailIsAvailable(String arTransactionNumber) 
	{
		List<ARTransactionEntryDistribution> distributionList = repositoryARTransactionEntryDistribution.findByArTransactionNumber(arTransactionNumber);
        if(distributionList!=null && !distributionList.isEmpty()){
        	return true;
        }
       return false;
	}
	
	public boolean deleteTransactionEntry(String arTransactionNumber) 
	{
		ARTransactionEntry arTransactionEntry= repositoryARTransactionEntry.findByArTransactionNumber(arTransactionNumber);
		if(arTransactionEntry!=null)
		{
			List<ARTransactionEntryDistribution> arTransactionEntryDistributionList=repositoryARTransactionEntryDistribution.
					findByArTransactionNumber(arTransactionNumber);
			 if(arTransactionEntryDistributionList!=null && !arTransactionEntryDistributionList.isEmpty()){
				 repositoryARTransactionEntryDistribution.deleteInBatch(arTransactionEntryDistributionList);

			 }
			 repositoryARTransactionEntry.delete(arTransactionEntry);
			 return true;
		}
		return false;
	}
	
	public boolean deleteTransactionEntryDistribution(String arTransactionNumber) 
	{
			List<ARTransactionEntryDistribution> arTransactionEntryDistributionList=repositoryARTransactionEntryDistribution.
					findByArTransactionNumber(arTransactionNumber);
			 if(arTransactionEntryDistributionList!=null && !arTransactionEntryDistributionList.isEmpty()){
				 repositoryARTransactionEntryDistribution.deleteInBatch(arTransactionEntryDistributionList);
				 return true;
			 }
		return false;
	}
	
	
}
