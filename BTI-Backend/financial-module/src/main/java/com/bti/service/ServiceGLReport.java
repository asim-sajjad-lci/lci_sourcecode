/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.AccountNumberView;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLConfigurationAccountCategories;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumber;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.TrialBalanceView;
import com.bti.model.RPTBS;
import com.bti.model.RPTPL;
import com.bti.model.ReportProfitAndLoss;
import com.bti.model.trl_blnc_n_act_stmt_rpts_v;
import com.bti.model.dto.DtoAccountCategory;
import com.bti.model.dto.DtoAccountStatementDetailRecord;
import com.bti.model.dto.DtoAccountStatementMasterRecord;
import com.bti.model.dto.DtoAccountStatementReport;
import com.bti.model.dto.DtoBalanceSheetReport;
import com.bti.model.dto.DtoGLReportDetail;
import com.bti.model.dto.DtoGLReportHeader;
import com.bti.model.dto.DtoGLYTDOpenTransactions;
import com.bti.model.dto.DtoProfitAndLossReport;
import com.bti.model.dto.DtoTrailBalanceReport;
import com.bti.repository.RepositoryAccountNumberView;
import com.bti.repository.RepositoryAccountStatementReport;
import com.bti.repository.RepositoryEntityManager;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLConfigurationAccountCategories;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.repository.RepositoryTrialBalanceReportNew;
import com.bti.repository.RepositoryProfitAndLossReport;
import com.bti.repository.RepossitoryReportProfitAndLoss;
import com.bti.util.CommonUtils;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.jayway.restassured.path.json.JsonPath;

/**
 * Description: serviceGLReport Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceGLReport")
public class ServiceGLReport {

	private static final Logger log = Logger.getLogger(ServiceGLReport.class);

	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;
	
	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;
	
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;
	
	
	@Autowired
	RepositoryAccountStatementReport repositoryAccountStatementReport;
	
	@Autowired
	RepositoryTrialBalanceReportNew repositoryTrialBalanceReportNew;
	

        @Autowired
        RepositoryAccountNumberView repositoryAccountNumberView;
	
	@Autowired
	RepositoryGLConfigurationAccountCategories repositoryGLConfigurationAccountCategories;
	
	@Autowired
	RepositoryProfitAndLossReport repositoryProfitAndLossReport;
	
	@Autowired
	RepositoryEntityManager repositoryEntityManager;
	
	
	public DtoGLReportHeader getTransactionReport(DtoGLReportHeader dtoGLReportHeader) {
		log.info("inside get report method");
		JournalEntryHeader journalEntryHeader = repositoryJournalEntryHeader
				.findByJournalIDAndIsDeleted(dtoGLReportHeader.getJournalID(), false);
		if (journalEntryHeader != null) {
			dtoGLReportHeader = new DtoGLReportHeader(journalEntryHeader);
			List<DtoGLReportDetail> detailList = new ArrayList<>();
			List<JournalEntryDetails> journalEntryDetailsList = repositoryJournalEntryDetail
					.findByJournalEntryHeaderJournalIDAndIsDeleted(journalEntryHeader.getJournalID(), false);
			Double totalCredit = 0.0;
			Double totalDebit = 0.0;
			if (journalEntryDetailsList != null && !journalEntryDetailsList.isEmpty()) 
			{
				for (JournalEntryDetails journalEntryDetails : journalEntryDetailsList) 
				{
					DtoGLReportDetail dtoJournalEntryDetail = new DtoGLReportDetail(journalEntryDetails);
					dtoJournalEntryDetail.setJournalSequence(journalEntryDetails.getJournalSequence());
					dtoJournalEntryDetail.setCreditAmount("");
					if (journalEntryDetails.getOriginalCreditAmount() != null) {
						dtoJournalEntryDetail
								.setCreditAmount(String.valueOf(journalEntryDetails.getOriginalCreditAmount()));
						totalCredit += journalEntryDetails.getOriginalCreditAmount();
					}
					dtoJournalEntryDetail.setDebitAmount("");
					if (journalEntryDetails.getOriginalDebitAmount() != null) {
						dtoJournalEntryDetail
								.setDebitAmount(String.valueOf(journalEntryDetails.getOriginalDebitAmount()));
						totalDebit += journalEntryDetails.getOriginalDebitAmount();
					}
					dtoJournalEntryDetail.setAccountNumber("");
					dtoJournalEntryDetail.setAccountDescription("");
					
					GLAccountsTableAccumulation glAccountsTableAccumulation=null;
					String accountNumber = "";
					String accountDesc = "";
					if(UtilRandomKey.isNotBlank(journalEntryDetails.getIntercompanyIDMultiCompanyTransaction()))
					{
							String tenantId = serviceHome
									.getCompanyDetail(Integer.parseInt(journalEntryDetails.getIntercompanyIDMultiCompanyTransaction()));
							JsonPath jsonFrom = null;
							if (tenantId != null) {
								
								String headerTenant = httpServletRequest.getHeader(CommonConstant.TENANT_ID);
								if (!headerTenant.equalsIgnoreCase(tenantId)) {
									jsonFrom = serviceHome.getAccountNumberDetailByAcoountIdAndCompanyId(
											journalEntryDetails.getAccountTableRowIndex(), tenantId);
									if (jsonFrom != null) {
										accountNumber = jsonFrom.getString("result.accountNumber");
										accountDesc = jsonFrom.getString("result.accountDesc");
									}
								}
								else
								{
									glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.
											findByAccountTableRowIndexAndIsDeleted(journalEntryDetails.getAccountTableRowIndex(), false);
									if(glAccountsTableAccumulation!=null)
									{
										
										Object[] object	=	serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
										accountNumber=object[0].toString();
										if(UtilRandomKey.isNotBlank(object[3].toString())){
											accountDesc=object[3].toString();
										}
									}
								}
						 }
					}
					else
					{
							glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.
									findByAccountTableRowIndexAndIsDeleted(journalEntryDetails.getAccountTableRowIndex(), false);
							if(glAccountsTableAccumulation!=null)
							{
								Object[] object	=	serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
								accountNumber=object[0].toString();
								if(UtilRandomKey.isNotBlank(object[3].toString())){
									accountDesc=object[3].toString();
								}
							}
				    }
					
					dtoJournalEntryDetail.setAccountNumber(accountNumber);
					dtoJournalEntryDetail.setAccountDescription(accountDesc);
					detailList.add(dtoJournalEntryDetail);
				}
				dtoGLReportHeader.setTotalCredit(totalCredit);
				dtoGLReportHeader.setTotalDebit(totalDebit);
				dtoGLReportHeader.setTotalDistributions(journalEntryDetailsList.size());
				dtoGLReportHeader.setUserId(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			dtoGLReportHeader.setDtoGLReportDetailList(detailList);
			return dtoGLReportHeader;
		}
		return null;
	}

	
	public DtoTrailBalanceReport getTrailBalanceReport(DtoTrailBalanceReport dtoTrailBalanceReport) 
	{
		log.info("inside getTrailBalanceReportFixed()");
		 
		 double beginningBalanceTotal = 0.0;
		 double endingBalanceTotal  = 0.0;
		 double debitTotal = 0.0;
		 double creditTotal = 0.0;
		 double netChangeTotal = 0.0;
		 Integer startYear = null;
		 Integer startPeriod = null;
		 Integer endYear = null;
		 Integer endPeriod = null;
		 Date endDate = null;
		 Date startDate = null;

		if (UtilRandomKey.isNotBlank(dtoTrailBalanceReport.getStartDate())) {
			startDate = UtilDateAndTime.ddmmyyyyStringToDate(dtoTrailBalanceReport.getStartDate());
		}

		if (UtilRandomKey.isNotBlank(dtoTrailBalanceReport.getEndDate())) {
			endDate = UtilDateAndTime.ddmmyyyyStringToDate(dtoTrailBalanceReport.getEndDate());
		}
	 
		if (startDate!=null && endDate!=null && startDate.after(endDate)) {
			return null;
		}

		Calendar sartCal = Calendar.getInstance();
		Calendar endCal = Calendar.getInstance();
		sartCal.setTime(startDate);
		startYear = sartCal.get(Calendar.YEAR);
		startPeriod = sartCal.get(Calendar.MONTH);
		endCal.setTime(endDate);
		endYear = endCal.get(Calendar.YEAR);
		endPeriod = endCal.get(Calendar.MONTH);
		
		dtoTrailBalanceReport = repositoryEntityManager.getTrialBalanceReport(startDate, endDate);
		
		return dtoTrailBalanceReport;
		
	}

	
	public DtoAccountStatementReport getAccountStatementReport(DtoAccountStatementReport dtoAccountStatementReport) 
	{
		 Date endDate = null;
		 Date startDate = null;

		if (UtilRandomKey.isNotBlank(dtoAccountStatementReport.getStartDate())) {
			startDate = UtilDateAndTime.ddmmyyyyStringToDate(dtoAccountStatementReport.getStartDate());
		}

		if (UtilRandomKey.isNotBlank(dtoAccountStatementReport.getEndDate())) {
			endDate = UtilDateAndTime.ddmmyyyyStringToDate(dtoAccountStatementReport.getEndDate());
		}
	 
		if (startDate!=null && endDate!=null && startDate.after(endDate)) {
			return null;
		}
		
		Calendar calStart = Calendar.getInstance();
		Calendar calEnd = Calendar.getInstance();
		calStart.setTime(startDate);
		calEnd.setTime(endDate);
		
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String fromatedStartDate = format.format(startDate);
		String fromatedEndDate = format.format(endDate);
		
		
		
		String strStartDate = dtoAccountStatementReport.getStartDate();
		String strEndDate = dtoAccountStatementReport.getEndDate();
		
		ArrayList<Object[]> result = (ArrayList<Object[]>) repositoryAccountStatementReport.getAccountStatementsByDateRange(fromatedStartDate, fromatedEndDate);
		HashMap<String, DtoAccountStatementMasterRecord> allAccountsMap = repositoryEntityManager.getAllAccountsWithPreviousBalance(fromatedStartDate);
		
		dtoAccountStatementReport = popluateAccountStateReportObject(result, allAccountsMap);
		
		dtoAccountStatementReport.setStartDate(strStartDate);
		dtoAccountStatementReport.setEndDate(strEndDate);
		
		    return dtoAccountStatementReport;	
	}

	
	private DtoAccountStatementReport popluateAccountStateReportObject(ArrayList<Object[]> result, HashMap<String, DtoAccountStatementMasterRecord> allAccountsMap) {
		DtoAccountStatementReport obj = new DtoAccountStatementReport();
		DtoAccountStatementMasterRecord master = null; 
		DtoAccountStatementDetailRecord detail = null;		
		ArrayList<DtoAccountStatementMasterRecord> masterList = new ArrayList<DtoAccountStatementMasterRecord>();
		ArrayList<DtoAccountStatementDetailRecord> detailList = new ArrayList<DtoAccountStatementDetailRecord>();

		
		Set<String> keys = allAccountsMap.keySet();
		
		for (String accountNumber : keys) {
			master = allAccountsMap.get(accountNumber);

			master.setTotalDebitAmount(0.0);
			master.setTotalCreditAmount(0.0);
			master.setTotalBalance(master.getBeginningBalance());
			detailList = new ArrayList<DtoAccountStatementDetailRecord>();
			master.setAccountStatmentDetailRecordList(detailList);
			masterList.add(masterList.size(), master);
		
		}
		

		for (int i=0; i<result.size(); i++) {

			Object[] row = result.get(i);

			long journalNo = CommonUtils.castToLong(row[0]); //row.getJournal_ID();
			String accountNumber = CommonUtils.castToString(row[1]); // row.getAccount_Number();
			String accountDescription = CommonUtils.castToString(row[2]); // row.getAccount_Description();
			String accountCategory = CommonUtils.castToString(row[3]); // row.getAccount_Categrory();
			String journalDate = CommonUtils.castToString(row[4]); // row.getJournal_Date();
			String distributionReference = CommonUtils.castToString(row[5]); // row.getDistribution_Reference();
			double debit = CommonUtils.castToDouble(row[6]); // row.getDB();
			double credit = CommonUtils.castToDouble(row[7]); // row.getCB();
			int history = 0; //(Integer) row[8]; // row.getHistory();

			master = allAccountsMap.get(accountNumber);
		
			master.setAccountCategory(accountCategory);
			master.setDescription(accountDescription);

			master.setTotalDebitAmount(master.getTotalDebitAmount() + debit);
			master.setTotalCreditAmount(master.getTotalCreditAmount() + credit);
			master.setTotalBalance(master.getTotalBalance() + debit - credit);
			
			detailList = master.getAccountStatmentDetailRecordList();

			detail = new DtoAccountStatementDetailRecord();
			detail.setDebitAmount(debit);
			detail.setCreditAmount(credit);
			detail.setBalance(master.getTotalBalance());
			detail.setJournalNo(String.valueOf(journalNo));
			detail.setJournalDate(journalDate);
			detail.setDistributionReference(distributionReference);
			detailList.add(detailList.size(), detail);
			
		}
		obj.setAccountStatmentMasterRecordList(masterList);
		return obj;
	}

	

	
//	public DtoProfitAndLossReport getProfitAndLossReports(DtoProfitAndLossReport dtoProfitAndLossReport) {
//		
//		NumberFormat df2 = new DecimalFormat( "#,###,###,##0.00" );
//		
//		List<RPTPL> data = entityManager.createNamedStoredProcedureQuery("rptPL2").getResultList();
//		
//		//sort and filter the data upon MainAccount
//		Collection<List<RPTPL>> dataSortByAccountCategory = data.stream().collect(Collectors.groupingBy(RPTPL::getAccountCategoryIndex)).values();
//				
//		List<DtoAccountCategory> accountCategories = new ArrayList<>();
//		
//		dataSortByAccountCategory.stream().forEach(accountsCategory ->{
//			
//			DtoAccountCategory dtoAccountCategory = new DtoAccountCategory();
//
//			
//			List<DtoGLYTDOpenTransactions> listOfAccounts = new ArrayList<>();
//			
//			accountsCategory.stream().collect(Collectors.groupingBy(RPTPL::getMainAccount)).values().forEach(e ->{
//				
//				DtoGLYTDOpenTransactions dtoGLYTDOpenTransactions = new DtoGLYTDOpenTransactions();
//				
//				
//				
//				e.forEach(a ->{
//					
//					dtoAccountCategory.setAccountCategoryDescription(a.getAccountCategoryDecription());
//					
//						dtoGLYTDOpenTransactions.setJournalDescription(a.getMainAccountDescription());
//						dtoGLYTDOpenTransactions.setCreadiAmount( dtoGLYTDOpenTransactions.getCreadiAmount() + a.getCreditAmmount());
//						dtoGLYTDOpenTransactions.setDebitAmount(dtoGLYTDOpenTransactions.getDebitAmount() + a.getDebitAmmount());
//						dtoGLYTDOpenTransactions.setAccountTableRowIndex(a.getAccountTableRowIndex()+"");
//						dtoAccountCategory.setTotalTransaction(dtoAccountCategory.getTotalTransaction() + (a.getDebitAmmount() - a.getCreditAmmount() ));
//						
//						if(dtoProfitAndLossReport.getPeriod() == a.getPeriod()) {
//							dtoGLYTDOpenTransactions.setPeroidBalance(dtoGLYTDOpenTransactions.getPeroidBalance() + (a.getDebitAmmount() - a.getCreditAmmount()));
//						}
//					
//
//					
//				});
//				if(!dtoGLYTDOpenTransactions.getAccountTableRowIndex().equals("0")) {
//					listOfAccounts.add(dtoGLYTDOpenTransactions);
//				}
//				
//			});
//			dtoAccountCategory.setListOfAccounts(listOfAccounts);
//			accountCategories.add(dtoAccountCategory);
//			
//		});
//		
//		dtoProfitAndLossReport.setListOfAccountCategory(accountCategories);
//
//		
//		//=====================================//		
//			
//		//calculate netIncome
//		// note the Zakkah is not include ( missing from DB gl40001 )
//		dtoProfitAndLossReport.getListOfAccountCategory().stream().forEach(accountCategory ->{
//			if(accountCategory.getAccountCategoryId()== 33) {
//				dtoProfitAndLossReport.setNetIncome(dtoProfitAndLossReport.getNetIncome() - accountCategory.getTotalTransaction());
//			}else {
//				dtoProfitAndLossReport.setNetIncome(dtoProfitAndLossReport.getNetIncome() + accountCategory.getTotalTransaction());
//			}
//		});
//		
//		//dtoProfitAndLossReport.setNetIncome(Double.valueOf(df2.format(dtoProfitAndLossReport.getNetIncome())));
//		dtoProfitAndLossReport.setTotalNetIncome(df2.format(dtoProfitAndLossReport.getNetIncome()));
//		//dtoProfitAndLossReport.setListOfAccounts(dtoListYTDOpenTransaction);
//		return dtoProfitAndLossReport;
//		
//		
//	}
	
	public DtoProfitAndLossReport getProfitAndLossReport(DtoProfitAndLossReport dtoProfitAndLossReport) 
	{
		//=====================================//
		
		
		// for formatting numbers 
		NumberFormat df2 = new DecimalFormat( "#,###,###,##0.00" );
		
		// Account Categories ids
//		List<Integer> accountCategoriesIds = new ArrayList<>();
//		accountCategoriesIds.add(31);
//		accountCategoriesIds.add(32);
//		accountCategoriesIds.add(33);
//		accountCategoriesIds.add(34);
//		accountCategoriesIds.add(35);
//		accountCategoriesIds.add(36);
//		accountCategoriesIds.add(37);
//		accountCategoriesIds.add(38);
//		accountCategoriesIds.add(39);
//		accountCategoriesIds.add(40);
//		accountCategoriesIds.add(47);
//		accountCategoriesIds.add(43);
//		accountCategoriesIds.add(42);
		
		// get all account category list ( gl40001 )
		//List<ReportProfitAndLoss> data = repositoryProfitAndLossReport.findByAccountCategoryIndex(accountCategoriesIds,dtoProfitAndLossReport.getPeriod());
	    List<Object[]> data = repositoryEntityManager.getPeriod(dtoProfitAndLossReport);
	    List<RPTPL> data2 = new ArrayList<>();
	    if (data == null ) {
	    	// do something...
	    } else {
		    data.stream().forEach(e ->{
	
		    	System.out.println("Test");
		    	
		    	RPTPL rptl = new RPTPL(
		    			Integer.valueOf(e[0].toString()),
		    			Integer.valueOf(e[1].toString()),
		    			Integer.valueOf(e[2].toString()),
		    			e[3].toString(), e[4].toString(),
		    			Integer.valueOf(e[5].toString()),
		    			e[6].toString() ,
		    			Integer.valueOf(e[8].toString()),
		    			Double.valueOf(e[9].toString()),
		    			Double.valueOf(e[10].toString())
		    			) ;
		    	data2.add(rptl);
		    	//RPTPL rr = new RPTPL(id, iDROW, accountTableRowIndex, mainAccount, mainAccountDescription, accountCategoryIndex, accountCategoryDecription, transactionDate, period, debitAmmount, creditAmmount)
		    });
	    }
	    
		//sort and filter the data upon MainAccount
		Collection<List<RPTPL>> dataSortByAccountCategory = data2.stream().collect(Collectors.groupingBy(RPTPL::getAccountCategoryIndex)).values();
		
		
		
		List<DtoAccountCategory> accountCategories = new ArrayList<>();
	
		// loop for each  account category
		dataSortByAccountCategory.stream().forEach(accountsCategory ->{
			
			DtoAccountCategory dtoAccountCategory = new DtoAccountCategory();

			
			List<DtoGLYTDOpenTransactions> listOfAccounts = new ArrayList<>();
			
			// loop in side account category ( Main aacounts )
			accountsCategory.stream().collect(Collectors.groupingBy(RPTPL::getMainAccount)).values().forEach(e ->{
				
				DtoGLYTDOpenTransactions dtoGLYTDOpenTransactions = new DtoGLYTDOpenTransactions();
				
				
				// iterate maint accounts
				e.forEach(a ->{
					
					dtoAccountCategory.setAccountCategoryDescription(a.getAccountCategoryDecription());
					dtoAccountCategory.setAccountCategoryId(a.getAccountCategoryIndex());
						dtoGLYTDOpenTransactions.setJournalDescription(a.getMainAccountDescription());
						dtoGLYTDOpenTransactions.setCreadiAmount( dtoGLYTDOpenTransactions.getCreadiAmount() + a.getCreditAmmount());
						dtoGLYTDOpenTransactions.setDebitAmount(dtoGLYTDOpenTransactions.getDebitAmount() + a.getDebitAmmount());
						dtoGLYTDOpenTransactions.setAccountTableRowIndex(a.getAccountTableRowIndex()+"");
						if(a.getAccountCategoryIndex() == 31 || a.getAccountCategoryIndex() == 43) {
							dtoAccountCategory.setTotalTransaction( dtoAccountCategory.getTotalTransaction() + (a.getCreditAmmount() - a.getDebitAmmount()));
							dtoGLYTDOpenTransactions.setBalance( dtoGLYTDOpenTransactions.getBalance() + (a.getCreditAmmount() - a.getDebitAmmount()));
						}else {
						dtoAccountCategory.setTotalTransaction( dtoAccountCategory.getTotalTransaction() + (a.getDebitAmmount() - a.getCreditAmmount()));
							dtoGLYTDOpenTransactions.setBalance( dtoGLYTDOpenTransactions.getBalance() + (a.getDebitAmmount() - a.getCreditAmmount()));
						}
						
						
						if(dtoProfitAndLossReport.getPeriod() == a.getPeriod()) {
							if(a.getAccountCategoryIndex() == 31 || a.getAccountCategoryIndex() == 43) {
								dtoGLYTDOpenTransactions.setPeroidBalance( dtoGLYTDOpenTransactions.getPeroidBalance() + (a.getCreditAmmount() - a.getDebitAmmount()));
							}else {
							dtoGLYTDOpenTransactions.setPeroidBalance( dtoGLYTDOpenTransactions.getPeroidBalance() + (a.getDebitAmmount() - a.getCreditAmmount()));
						}
					
						}
					

					
				});
				if(!dtoGLYTDOpenTransactions.getAccountTableRowIndex().equals("0")) {
					listOfAccounts.add(dtoGLYTDOpenTransactions);
				}
				
			});
			dtoAccountCategory.setListOfAccounts(listOfAccounts);
			accountCategories.add(dtoAccountCategory);
			
		});
		
		accountCategories.sort(Comparator.comparing(DtoAccountCategory::getAccountCategoryId));
		Collections.rotate(accountCategories.subList(10, 12), -2);
		dtoProfitAndLossReport.setListOfAccountCategory(accountCategories);

		
		//=====================================//		
			
		//calculate netIncome
		// note the Zakkah is not include ( missing from DB gl40001 )
		dtoProfitAndLossReport.getListOfAccountCategory().stream().forEach(accountCategory ->{
			
			if(accountCategory.getAccountCategoryId() == 31 || accountCategory.getAccountCategoryId() == 43) {
				dtoProfitAndLossReport.setNetIncome(dtoProfitAndLossReport.getNetIncome() + accountCategory.getTotalTransaction());
			}else {
				dtoProfitAndLossReport.setNetIncome(dtoProfitAndLossReport.getNetIncome() - accountCategory.getTotalTransaction());
			}
			
		});
		
		//dtoProfitAndLossReport.setNetIncome(Double.valueOf(df2.format(dtoProfitAndLossReport.getNetIncome())));
		dtoProfitAndLossReport.setTotalNetIncome(df2.format(dtoProfitAndLossReport.getNetIncome()));
		//dtoProfitAndLossReport.setListOfAccounts(dtoListYTDOpenTransaction);
		return dtoProfitAndLossReport;
		

		
	}

	public DtoBalanceSheetReport getBalanceSheetReport(DtoBalanceSheetReport dtoBalanceSheetReport) {
		
		//=====================================//
		
		
		// get all account category list ( gl40001 ) using Stored Procedure
		//List<ReportProfitAndLoss> data = repositoryProfitAndLossReport.findByAccountCategoryIndex(accountCategoriesIds,dtoProfitAndLossReport.getPeriod());
	    List<Object[]> data = repositoryEntityManager.getBalanceSheetReport();
	    List<RPTBS> data2 = new ArrayList<>();
	    // add all data to list 
	    data.stream().forEach(e ->{

	    	RPTBS rptbs = new RPTBS(Integer.valueOf(e[0].toString()), Integer.valueOf(e[1].toString()),Integer.valueOf(e[2].toString()),e[3].toString(), e[4].toString(),Integer.valueOf(e[5].toString()),e[6].toString() , Integer.valueOf(e[8].toString()),Double.valueOf(e[9].toString()),Double.valueOf(e[10].toString())) ;
	    	data2.add(rptbs);
	    	
	    });
	    
		//sort and filter the data upon Category Index ( 1 -> 7 )
		Collection<List<RPTBS>> totalCurrentAsset = data2.stream().filter(e-> 1<=e.getAccountCategoryIndex() && e.getAccountCategoryIndex()<=7).collect(Collectors.groupingBy(RPTBS::getAccountCategoryIndex)).values();
		
		//sort and filter the data upon Category Index ( 8 -> 12 ) G1
		Collection<List<RPTBS>> totalAsset = data2.stream().filter(e-> 8<=e.getAccountCategoryIndex() && e.getAccountCategoryIndex()<=12).collect(Collectors.groupingBy(RPTBS::getAccountCategoryIndex)).values();
		
		//sort and filter the data upon Category Index ( 13 -> 21 ) G2
		Collection<List<RPTBS>> totalCurrentLiabilities = data2.stream().filter(e-> 13<=e.getAccountCategoryIndex() && e.getAccountCategoryIndex()<=21).collect(Collectors.groupingBy(RPTBS::getAccountCategoryIndex)).values();

		//sort and filter the data upon Category Index (  22 )
		Collection<List<RPTBS>> totalLiabilities = data2.stream().filter(e-> 22 == e.getAccountCategoryIndex()).collect(Collectors.groupingBy(RPTBS::getAccountCategoryIndex)).values();
		
		//sort and filter the data upon Category Index ( 23 -> 30 )
		Collection<List<RPTBS>> totalX = data2.stream().filter(e-> 23<=e.getAccountCategoryIndex() && e.getAccountCategoryIndex()<=30).collect(Collectors.groupingBy(RPTBS::getAccountCategoryIndex)).values();
		
		
		
		List<DtoAccountCategory> accountCategories = new ArrayList<>();
		List<Double> totals = new ArrayList<>(7);
		// total current assets index ( 1 -> 7 ) 
		totals.add(0.0);
		// loop for Current Asset  -> calculate total
		totalCurrentAsset.stream().forEach(C->{
			DtoAccountCategory dtoAccountCategory = new DtoAccountCategory();
			List<DtoGLYTDOpenTransactions> listOfAccounts = new ArrayList<>();
			
			C.stream().collect(Collectors.groupingBy(RPTBS::getMainAccount)).values().forEach(q -> {
				DtoGLYTDOpenTransactions dtoGLYTDOpenTransactions1 = new DtoGLYTDOpenTransactions();
				q.forEach(account -> {
					dtoAccountCategory.setAccountCategoryDescription(account.getAccountCategoryDecription());
					dtoAccountCategory.setAccountCategoryId(account.getAccountCategoryIndex());
					dtoAccountCategory.setTotalTransaction(dtoAccountCategory.getTotalTransaction() + (account.getDebitAmmount() - account.getCreditAmmount()));
					dtoGLYTDOpenTransactions1.setCreadiAmount(dtoGLYTDOpenTransactions1.getCreadiAmount() + account.getCreditAmmount());
					dtoGLYTDOpenTransactions1.setDebitAmount(dtoGLYTDOpenTransactions1.getDebitAmount() + account.getDebitAmmount());
					dtoGLYTDOpenTransactions1.setBalance(dtoGLYTDOpenTransactions1.getBalance() + ( account.getDebitAmmount() - account.getCreditAmmount() ));
					dtoGLYTDOpenTransactions1.setJournalDescription(account.getMainAccountDescription());
					dtoGLYTDOpenTransactions1.setAccountTableRowIndex(account.getAccountTableRowIndex()+"");
					
				});
				
				if(!dtoGLYTDOpenTransactions1.getAccountTableRowIndex().equals("0")) {
					listOfAccounts.add(dtoGLYTDOpenTransactions1);
				}
			});
			totals.set(0, totals.get(0) + dtoAccountCategory.getTotalTransaction());
			dtoAccountCategory.setListOfAccounts(listOfAccounts);
			accountCategories.add(dtoAccountCategory);
			
		});
		
		//total assets Index ( 8 -> 12 ) G1 totals  index(1)
		totals.add(0.0+totals.get(0));
		//loop for assets  -> calculate total
		totalAsset.stream().forEach(C->{
			DtoAccountCategory dtoAccountCategory = new DtoAccountCategory();
			List<DtoGLYTDOpenTransactions> listOfAccounts = new ArrayList<>();
			
			C.stream().collect(Collectors.groupingBy(RPTBS::getMainAccount)).values().forEach(q -> {
				DtoGLYTDOpenTransactions dtoGLYTDOpenTransactions1 = new DtoGLYTDOpenTransactions();
				q.forEach(account -> {
					dtoAccountCategory.setAccountCategoryDescription(account.getAccountCategoryDecription());
					dtoAccountCategory.setAccountCategoryId(account.getAccountCategoryIndex());
					dtoAccountCategory.setTotalTransaction(dtoAccountCategory.getTotalTransaction() + (account.getDebitAmmount() - account.getCreditAmmount()));
					dtoGLYTDOpenTransactions1.setCreadiAmount(dtoGLYTDOpenTransactions1.getCreadiAmount() + account.getCreditAmmount());
					dtoGLYTDOpenTransactions1.setDebitAmount(dtoGLYTDOpenTransactions1.getDebitAmount() + account.getDebitAmmount());
					dtoGLYTDOpenTransactions1.setBalance(dtoGLYTDOpenTransactions1.getBalance() + ( account.getDebitAmmount() - account.getCreditAmmount() ));
					dtoGLYTDOpenTransactions1.setJournalDescription(account.getMainAccountDescription());
					dtoGLYTDOpenTransactions1.setAccountTableRowIndex(account.getAccountTableRowIndex()+"");
					
				});
				
				if(!dtoGLYTDOpenTransactions1.getAccountTableRowIndex().equals("0")) {
					listOfAccounts.add(dtoGLYTDOpenTransactions1);
				}
			});
			totals.set(1, totals.get(1) + dtoAccountCategory.getTotalTransaction());
			dtoAccountCategory.setListOfAccounts(listOfAccounts);
			accountCategories.add(dtoAccountCategory);
			
		});
		
		//Total Current Liabilities Index ( 13 -> 21 )
		totals.add(0.0); //+totals.get(1)
		// loop for Current Liabilities  -> calculate tables
		totalCurrentLiabilities.stream().forEach(C->{
			DtoAccountCategory dtoAccountCategory = new DtoAccountCategory();
			List<DtoGLYTDOpenTransactions> listOfAccounts = new ArrayList<>();
			
			C.stream().collect(Collectors.groupingBy(RPTBS::getMainAccount)).values().forEach(q -> {
				DtoGLYTDOpenTransactions dtoGLYTDOpenTransactions1 = new DtoGLYTDOpenTransactions();
				q.forEach(account -> {
					dtoAccountCategory.setAccountCategoryDescription(account.getAccountCategoryDecription());
					dtoAccountCategory.setAccountCategoryId(account.getAccountCategoryIndex());
					dtoAccountCategory.setTotalTransaction(dtoAccountCategory.getTotalTransaction() + (account.getCreditAmmount() - account.getDebitAmmount()));
					dtoGLYTDOpenTransactions1.setCreadiAmount(dtoGLYTDOpenTransactions1.getCreadiAmount() + account.getCreditAmmount());
					dtoGLYTDOpenTransactions1.setDebitAmount(dtoGLYTDOpenTransactions1.getDebitAmount() + account.getDebitAmmount());
					dtoGLYTDOpenTransactions1.setBalance(dtoGLYTDOpenTransactions1.getBalance() + ( account.getCreditAmmount() - account.getDebitAmmount() ));
					dtoGLYTDOpenTransactions1.setJournalDescription(account.getMainAccountDescription());
					dtoGLYTDOpenTransactions1.setAccountTableRowIndex(account.getAccountTableRowIndex()+"");
					
				});
				
				if(!dtoGLYTDOpenTransactions1.getAccountTableRowIndex().equals("0")) {
					listOfAccounts.add(dtoGLYTDOpenTransactions1);
				}
			});
			totals.set(2, totals.get(2) + dtoAccountCategory.getTotalTransaction());
			dtoAccountCategory.setListOfAccounts(listOfAccounts);
			accountCategories.add(dtoAccountCategory);
			
		});
		
		//Total Liabilities Index ( 22 ) G2 totals index(3)
		totals.add(0.0+totals.get(2));
		//loop for Liabilities  -> calculate total
		totalLiabilities.stream().forEach(C->{
			DtoAccountCategory dtoAccountCategory = new DtoAccountCategory();
			List<DtoGLYTDOpenTransactions> listOfAccounts = new ArrayList<>();
			
			C.stream().collect(Collectors.groupingBy(RPTBS::getMainAccount)).values().forEach(q -> {
				DtoGLYTDOpenTransactions dtoGLYTDOpenTransactions1 = new DtoGLYTDOpenTransactions();
				q.forEach(account -> {
					dtoAccountCategory.setAccountCategoryDescription(account.getAccountCategoryDecription());
					dtoAccountCategory.setAccountCategoryId(account.getAccountCategoryIndex());
					dtoAccountCategory.setTotalTransaction(dtoAccountCategory.getTotalTransaction() + (account.getCreditAmmount() - account.getDebitAmmount()));
					dtoGLYTDOpenTransactions1.setCreadiAmount(dtoGLYTDOpenTransactions1.getCreadiAmount() + account.getCreditAmmount());
					dtoGLYTDOpenTransactions1.setDebitAmount(dtoGLYTDOpenTransactions1.getDebitAmount() + account.getDebitAmmount());
					dtoGLYTDOpenTransactions1.setBalance(dtoGLYTDOpenTransactions1.getBalance() + ( account.getCreditAmmount() - account.getDebitAmmount() ));
					dtoGLYTDOpenTransactions1.setJournalDescription(account.getMainAccountDescription());
					dtoGLYTDOpenTransactions1.setAccountTableRowIndex(account.getAccountTableRowIndex()+"");
					
				});
				
				if(!dtoGLYTDOpenTransactions1.getAccountTableRowIndex().equals("0")) {
					listOfAccounts.add(dtoGLYTDOpenTransactions1);
				}
			});
			totals.set(3, totals.get(3) + dtoAccountCategory.getTotalTransaction());
			dtoAccountCategory.setListOfAccounts(listOfAccounts);
			accountCategories.add(dtoAccountCategory);
			
		});
		
		//total Gx Index ( 23 -> 30 ) total Gx index ( 4 )
		totals.add(0.0);
		//loop for Gx  -> calculate total
		totalX.stream().forEach(C->{
			DtoAccountCategory dtoAccountCategory = new DtoAccountCategory();
			List<DtoGLYTDOpenTransactions> listOfAccounts = new ArrayList<>();
			
			C.stream().collect(Collectors.groupingBy(RPTBS::getMainAccount)).values().forEach(q -> {
				DtoGLYTDOpenTransactions dtoGLYTDOpenTransactions1 = new DtoGLYTDOpenTransactions();
				q.forEach(account -> {
					dtoAccountCategory.setAccountCategoryDescription(account.getAccountCategoryDecription());
					dtoAccountCategory.setAccountCategoryId(account.getAccountCategoryIndex());
					dtoAccountCategory.setTotalTransaction(dtoAccountCategory.getTotalTransaction() + (account.getCreditAmmount() - account.getDebitAmmount()));
					dtoGLYTDOpenTransactions1.setCreadiAmount(dtoGLYTDOpenTransactions1.getCreadiAmount() + account.getCreditAmmount());
					dtoGLYTDOpenTransactions1.setDebitAmount(dtoGLYTDOpenTransactions1.getDebitAmount() + account.getDebitAmmount());
					dtoGLYTDOpenTransactions1.setBalance(dtoGLYTDOpenTransactions1.getBalance() + ( account.getCreditAmmount() - account.getDebitAmmount() ));
					dtoGLYTDOpenTransactions1.setJournalDescription(account.getMainAccountDescription());
					dtoGLYTDOpenTransactions1.setAccountTableRowIndex(account.getAccountTableRowIndex()+"");
					
				});
				
				if(!dtoGLYTDOpenTransactions1.getAccountTableRowIndex().equals("0")) {
					listOfAccounts.add(dtoGLYTDOpenTransactions1);
				}
			});
			totals.set(4, totals.get(4) + dtoAccountCategory.getTotalTransaction());
			dtoAccountCategory.setListOfAccounts(listOfAccounts);
			accountCategories.add(dtoAccountCategory);
			
		});
		
		// Net Profit G3 index (5)
		totals.add( (totals.get(1)) - (totals.get(3))- (totals.get(4)) );
		
		// Total Equity G4 index (6)
		totals.add(totals.get(5) + totals.get(4) );
		
		//Total Liabilities and Equity
		totals.add(totals.get(6) + (totals.get(3)));
		
		dtoBalanceSheetReport.setTotals(totals);
		dtoBalanceSheetReport.setListOfAccountCategory(accountCategories);


		
		return dtoBalanceSheetReport;
	}

	
	//TODO temporary
/*	
	private ArrayList<DtoAccountStatementMasterRecord> getHCMRList() {
		ArrayList<DtoAccountStatementMasterRecord> list = new ArrayList<DtoAccountStatementMasterRecord>();
		
		DtoAccountStatementMasterRecord rec1 = new DtoAccountStatementMasterRecord("2120----", "Description 1", "Accounts Payable");
		rec1.setBeginningBalance(10000.0);

		rec1.setTotalDebitAmount(519980.000);
		rec1.setTotalCreditAmount(0);
		rec1.setTotalBalance(0);
		
		rec1.setAccountStatmentDetailRecordList(getHCDRList(rec1.getAccountNumber()));
		
		list.add(rec1);
		
		
		rec1 = new DtoAccountStatementMasterRecord("5203-RYD-547--", "Description 5", "Other Expenses");
		rec1.setBeginningBalance(10000.0);

		rec1.setTotalDebitAmount(75.000);
		rec1.setTotalCreditAmount(0);
		rec1.setTotalBalance(0);

		rec1.setAccountStatmentDetailRecordList(getHCDRList(rec1.getAccountNumber()));
		
		list.add(rec1);
		
		
		rec1 = new DtoAccountStatementMasterRecord("1100-JED-102--", "Description 2", "Cash");
		rec1.setBeginningBalance(10000.0);
		rec1.setTotalDebitAmount(5492.89);
		rec1.setTotalCreditAmount(526451.64);
		rec1.setTotalBalance(0);
		rec1.setAccountStatmentDetailRecordList(getHCDRList(rec1.getAccountNumber()));
		
		list.add(rec1);
		
		rec1 = new DtoAccountStatementMasterRecord("1150-JED---", "Description 3", "Accounts Receivable");
		rec1.setBeginningBalance(10000.0);

		rec1.setTotalDebitAmount(3.750);
		rec1.setTotalCreditAmount(0);
		rec1.setTotalBalance(0);
		
		rec1.setAccountStatmentDetailRecordList(getHCDRList(rec1.getAccountNumber()));
		
		list.add(rec1);
		
		
		rec1 = new DtoAccountStatementMasterRecord("5202-JED-521--", "Description 4", "Prepaid Expenses");
		rec1.setBeginningBalance(10000.0);

		
		rec1.setTotalDebitAmount(900.000);
		rec1.setTotalCreditAmount(0);
		rec1.setTotalBalance(0);
		rec1.setAccountStatmentDetailRecordList(getHCDRList(rec1.getAccountNumber()));
		
		list.add(rec1);
		
		
		return list;
		
	}

	private ArrayList<DtoAccountStatementDetailRecord> getHCDRList(String accountNo) {
		ArrayList<DtoAccountStatementDetailRecord> list = new ArrayList<DtoAccountStatementDetailRecord>();
		
		switch(accountNo) {
			case "1100-JED-102--":
				DtoAccountStatementDetailRecord obj = new DtoAccountStatementDetailRecord();
				obj.setJournalNo("8");
				obj.setJournalDate("2018-04-15 00:00:00");
				obj.setDistributionReference("11");
				obj.setDebitAmount(100.000);
				obj.setCreditAmount(0);
				list.add(obj);
				
				obj = new DtoAccountStatementDetailRecord();
				obj.setJournalNo("1");
				obj.setJournalDate("2018-01-04 00:00:00");
				obj.setDistributionReference("Payment of Consultation Fees (GBP)");
				obj.setDebitAmount(0);
				obj.setCreditAmount(520058.750);
				list.add(obj);
				
				obj.setJournalNo("8");
				obj.setJournalDate("2018-04-15 00:00:00");
				obj.setDistributionReference("11");
				obj.setDebitAmount(0);
				obj.setCreditAmount(100.000);
				list.add(obj);

				obj = new DtoAccountStatementDetailRecord();
				obj.setJournalNo("2");
				obj.setJournalDate("2018-01-10 00:00:00");
				obj.setDistributionReference("Hani Abu Dabi Trip");
				obj.setDebitAmount(0);
				obj.setCreditAmount(6292.890);
				list.add(obj);
				
				break;
			
			case "1150-JED---":
				obj = new DtoAccountStatementDetailRecord();
				obj.setJournalNo("1");
				obj.setJournalDate("2018-01-04 00:00:00");
				obj.setDistributionReference("Payment of Consultation Fees (GBP)");
				obj.setDebitAmount(3.750);
				obj.setCreditAmount(0);
				list.add(obj);
				break;
				
			case "2120----":
				obj = new DtoAccountStatementDetailRecord();
				obj.setJournalNo("1");
				obj.setJournalDate("2018-01-04 00:00:00");
				obj.setDistributionReference("Payment of Consultation Fees (GBP)");
				obj.setDebitAmount(519980.000);
				obj.setCreditAmount(0);
				list.add(obj);
				break;
				
			case "5202-JED-521--":
				obj = new DtoAccountStatementDetailRecord();
				obj.setJournalNo("2");
				obj.setJournalDate("2018-01-10 00:00:00");
				obj.setDistributionReference("Hani Abu Dabi Trip");
				obj.setDebitAmount(900.000);
				obj.setCreditAmount(0);
				list.add(obj);
				break;
				
			case "5203-RYD-547--":
				obj = new DtoAccountStatementDetailRecord();
				obj.setJournalNo("1");
				obj.setJournalDate("2018-01-04 00:00:00");
				obj.setDistributionReference("Payment of Consultation Fees (GBP)");
				obj.setDebitAmount(75.000);
				obj.setCreditAmount(0);
				list.add(obj);
				break;
		}
		
		return list;
	}
	
	public DtoAccountStatementReport getHCDASR() {
		DtoAccountStatementReport obj = new DtoAccountStatementReport();
		
		obj.setPrintDate("15-04-2018");
		obj.setPrintTime("07:41 PM");
		obj.setStartDate("15-04-2017");
		obj.setEndDate("15-04-2018");
		
		obj.setAccountStatmentMasterRecordList(getHCMRList());
		
		return obj;
	}
	
*/	
}
