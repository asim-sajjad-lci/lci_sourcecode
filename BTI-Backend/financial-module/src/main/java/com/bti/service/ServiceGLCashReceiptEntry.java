/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.AuditTrailSeriesTypeConstant;
import com.bti.constant.BankTransactionEntryTypeConstant;
import com.bti.constant.BankTransactionTypeConstant;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CashReceiptBankAccountTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.GlSerialConfigurationConstant;
import com.bti.constant.MasterAPAccountTypeConstant;
import com.bti.constant.MasterArAcoountTypeConstant;
import com.bti.constant.MessageLabel;
import com.bti.constant.RateCalculationMethod;
import com.bti.constant.ReceiptTypeConstant;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.CreditCardsSetup;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CustomerClassAccountTableSetup;
import com.bti.model.CustomerClassesSetup;
import com.bti.model.CustomerMaintenance;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLCashReceiptBank;
import com.bti.model.GLCashReceiptDistribution;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumber;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumberKey;
import com.bti.model.GLCurrentSummaryMasterTableByDimensions;
import com.bti.model.GLCurrentSummaryMasterTableByDimensionsKey;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccount;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccountKey;
import com.bti.model.GLSerialsConfigurationForTransactions;
import com.bti.model.GLYTDOpenBankTransactionsEntryDetails;
import com.bti.model.GLYTDOpenBankTransactionsEntryHeader;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.PaymentMethodType;
import com.bti.model.ReceiptType;
import com.bti.model.VATSetup;
import com.bti.model.VendorClassAccountTableSetup;
import com.bti.model.VendorClassesSetup;
import com.bti.model.VendorMaintenance;
import com.bti.model.dto.DtoGLCashReceiptBank;
import com.bti.model.dto.DtoGLCashReceiptDistribution;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryBankTransactionEntryAction;
import com.bti.repository.RepositoryBankTransactionEntryType;
import com.bti.repository.RepositoryBankTransactionType;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCorrectJournalEntryActions;
import com.bti.repository.RepositoryCreditCardsSetup;
import com.bti.repository.RepositoryCurrencyExchangeDetail;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryCustomerClassAccountTableSetup;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBankTransferDetails;
import com.bti.repository.RepositoryGLBankTransferDistribution;
import com.bti.repository.RepositoryGLBankTransferEntryCharges;
import com.bti.repository.RepositoryGLBankTransferHeader;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLCashReceiptBank;
import com.bti.repository.RepositoryGLCashReceiptDistribution;
import com.bti.repository.RepositoryGLClearingDetail;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByDimensionIndex;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByMainAccount;
import com.bti.repository.RepositoryGLSerialConfigurationForTransaction;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryDetails;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryHeader;
import com.bti.repository.RepositoryGlytdHistoryTransaction;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.repository.RepositoryPaymentMethodType;
import com.bti.repository.RepositoryReceiptType;
import com.bti.repository.RepositoryVATSetup;
import com.bti.repository.RepositoryVendorClassAccountTableSetup;
import com.bti.repository.RepositoryVendorMaintenance;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.mapping.GL.MappingReceiptVouhcer;

/**
 * Description: ServiceGeneralLedgerTransaction Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceGLCashReceiptEntry")
public class ServiceGLCashReceiptEntry {

	private static final Logger LOG = Logger.getLogger(ServiceGLCashReceiptEntry.class);
	private

	@Autowired(required = false) HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;

	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;

	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	@Autowired
	RepositoryCurrencyExchangeDetail repositoryCurrencyExchangeDetail;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;

	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;

	@Autowired
	RepositoryGlytdHistoryTransaction repositoryGlytdHistoryTransaction;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;

	@Autowired
	RepositoryCorrectJournalEntryActions repositoryCorrectJournalEntryActions;

	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;

	@Autowired
	RepositoryGLClearingDetail repositoryGLClearingDetail;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryReceiptType repositoryReceiptType;

	@Autowired
	RepositoryPaymentMethodType repositoryPaymentMethodType;

	@Autowired
	RepositoryGLCashReceiptDistribution repositoryGLCashReceiptDistribution;

	@Autowired
	RepositoryGLCashReceiptBank repositoryGLCashReceiptBank;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	ServiceAccountPayable serviceAccountPayable;

	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByMainAccount repositoryGLCurrentSummaryMasterTableByMainAccount;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByDimensionIndex repositoryGLCurrentSummaryMasterTableByDimensionIndex;

	@Autowired
	RepositoryVATSetup repositoryVATSetup;

	@Autowired
	RepositoryBankTransactionEntryAction repositoryBankTransactionEntryAction;

	@Autowired
	RepositoryBankTransactionEntryType repositoryBankTransactionEntryType;
	@Autowired
	RepositoryBankTransactionType repositoryBankTransactionType;
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryDetails repositoryGLYTDOpenBankTransactionsEntryDetails;
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryHeader repositoryGLYTDOpenBankTransactionsEntryHeader;

	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;

	@Autowired
	RepositoryGLBankTransferHeader repositoryGLBankTransferHeader;
	@Autowired
	RepositoryGLBankTransferDetails repositoryGLBankTransferDetails;
	@Autowired
	RepositoryGLBankTransferDistribution repositoryGLBankTransferDistribution;
	@Autowired
	RepositoryGLBankTransferEntryCharges repositoryGLBankTransferEntryCharges;

	@Autowired
	RepositoryGLSerialConfigurationForTransaction repositoryGLSerialConfigurationForTransaction;

	@Autowired
	RepositoryVendorMaintenance repositoryVendorMaintenance;

	@Autowired
	RepositoryCustomerClassAccountTableSetup repositoryCustomerClassAccountTableSetup;

	@Autowired
	RepositoryVendorClassAccountTableSetup repositoryVendorClassAccountTableSetup;

	@Autowired
	RepositoryCreditCardsSetup repositoryCreditCardsSetup;

	@Autowired
	ServicePosting servicePosting;

	@Autowired
	MappingReceiptVouhcer mappingReceiptVouhcer;

	/**
	 * @description this service will use to save cash receipt entry
	 * @param dtoGLCashReceiptBank
	 * @return
	 */
	public DtoGLCashReceiptBank saveCashReceiptEntry(DtoGLCashReceiptBank dtoGLCashReceiptBank) {
		try {
			int langid = serviceHome.getLanngugaeId();
			GLSerialsConfigurationForTransactions glSerialsConfigurationForTransactions = repositoryGLSerialConfigurationForTransaction
					.findByGlSerialCode(GlSerialConfigurationConstant.CASH_RECEIPT.getVaue());
			String uniqueReceiptNumber = "";
			if (glSerialsConfigurationForTransactions != null) {
				uniqueReceiptNumber = glSerialsConfigurationForTransactions.getGlSerialCode() + "-"
						+ glSerialsConfigurationForTransactions.getGlSerialNumber();
			}
			GLCashReceiptBank glCashReceiptBank = repositoryGLCashReceiptBank.findByReceiptNumber(uniqueReceiptNumber);
			if (glCashReceiptBank == null) {
				glCashReceiptBank = new GLCashReceiptBank();
			} else {
				return null;
			}

			glCashReceiptBank.setReceiptNumber(uniqueReceiptNumber);

			if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getBankName())) {
				glCashReceiptBank.setBankName(dtoGLCashReceiptBank.getBankName());
			}

			if (dtoGLCashReceiptBank.getPaymentMethod() == 1) {
				glCashReceiptBank.setGlAccountsTableAccumulationDebit(
						repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(
								dtoGLCashReceiptBank.getAccountTableRowIndexDebit(), false));
			} else {
				glCashReceiptBank.setCheckbookMaintenance(repositoryCheckBookMaintenance
						.findByCheckBookIdAndIsDeleted(dtoGLCashReceiptBank.getCheckBookId(), false));
				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getCheckNumber())) {
					glCashReceiptBank.setCheckNumber(dtoGLCashReceiptBank.getCheckNumber());
				}

				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getCreditCardID())) {
					glCashReceiptBank.setCreditCardID(dtoGLCashReceiptBank.getCreditCardID());
				}

				if (dtoGLCashReceiptBank.getCreditCardExpireMonth() != null) {
					glCashReceiptBank.setCreditCardExpireMonth(dtoGLCashReceiptBank.getCreditCardExpireMonth());
				}

				if (dtoGLCashReceiptBank.getCreditCardExpireYear() != null) {
					glCashReceiptBank.setCreditCardExpireYear(dtoGLCashReceiptBank.getCreditCardExpireYear());
				}

				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getCreditCardNumber())) {
					glCashReceiptBank.setCreditCardNumber(dtoGLCashReceiptBank.getCreditCardNumber());
				}

			}

			if (dtoGLCashReceiptBank.getAuditTrial() != null) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes
						.findBySeriesIndexAndIsDeleted(dtoGLCashReceiptBank.getAuditTrial().intValue(), false);
				glCashReceiptBank.setGlConfigurationAuditTrialCOdes(audit);
			}

			glCashReceiptBank.setCurrencyID(dtoGLCashReceiptBank.getCurrencyID());
			glCashReceiptBank.setDepositorName(dtoGLCashReceiptBank.getDepositorName());
			glCashReceiptBank.setGlBatches(
					repositoryGLBatches.findByBatchIDAndIsDeleted(dtoGLCashReceiptBank.getBatchID(), false));
			if (dtoGLCashReceiptBank.getPaymentMethod() != null) {
				glCashReceiptBank
						.setPaymentMethod(repositoryPaymentMethodType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
								dtoGLCashReceiptBank.getPaymentMethod(), langid, false));
			}
			glCashReceiptBank.setReceiptAmount(dtoGLCashReceiptBank.getReceiptAmount());
			glCashReceiptBank
					.setReceiptDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoGLCashReceiptBank.getReceiptDate()));
			glCashReceiptBank.setReceiptDescription(dtoGLCashReceiptBank.getReceiptDescription());

			if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getAccountTableRowIndex())) {
				glCashReceiptBank.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(dtoGLCashReceiptBank.getAccountTableRowIndex(), false));
			}

			if (dtoGLCashReceiptBank.getReceiptType() == ReceiptTypeConstant.AR.getIndex()) {
				glCashReceiptBank.setCustomerMaintenance(repositoryCustomerMaintenance
						.findByCustnumberAndIsDeleted(dtoGLCashReceiptBank.getCustomerId(), false));
			} else if (dtoGLCashReceiptBank.getReceiptType() == ReceiptTypeConstant.AP.getIndex()) {
				glCashReceiptBank.setVendorMaintenance(repositoryVendorMaintenance
						.findByVendoridAndIsDeleted(dtoGLCashReceiptBank.getVendorId(), false));
			}

			glCashReceiptBank.setReceiptType(repositoryReceiptType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
					dtoGLCashReceiptBank.getReceiptType(), langid, false));
			glCashReceiptBank.setVatAmount(dtoGLCashReceiptBank.getVatAmount());
			glCashReceiptBank.setVatSetup(
					repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoGLCashReceiptBank.getVatScheduleID(), false));
			glCashReceiptBank.setExchangeRate(dtoGLCashReceiptBank.getExchangeRate());
			glCashReceiptBank.setExchangeTableIndex(dtoGLCashReceiptBank.getExchangeTableIndex());

			glCashReceiptBank = repositoryGLCashReceiptBank.saveAndFlush(glCashReceiptBank);

			// Update next receipt number
			if (glSerialsConfigurationForTransactions != null) {
				glSerialsConfigurationForTransactions
						.setGlSerialNumber(glSerialsConfigurationForTransactions.getGlSerialNumber() + 1);
				repositoryGLSerialConfigurationForTransaction.saveAndFlush(glSerialsConfigurationForTransactions);
			}

			return new DtoGLCashReceiptBank(glCashReceiptBank);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return null;
		}
	}

	public List<String> getAllCashReceiptNumber() {
		List<String> receiptNumberList = new ArrayList<>();
		try {
			List<GLCashReceiptBank> glCashReceiptBankList = repositoryGLCashReceiptBank.findAll();

			for (GLCashReceiptBank glCashReceiptBank : glCashReceiptBankList) {
				receiptNumberList.add(glCashReceiptBank.getReceiptNumber());
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return receiptNumberList;
	}

	/**
	 * @description this service will use to save cash receipt entry
	 * @param dtoGLCashReceiptBank
	 * @return
	 */
	public DtoGLCashReceiptBank updateCashReceiptEntry(DtoGLCashReceiptBank dtoGLCashReceiptBank) {
		try {
			int langid = serviceHome.getLanngugaeId();
			GLCashReceiptBank glCashReceiptBank = repositoryGLCashReceiptBank
					.findByReceiptNumber(dtoGLCashReceiptBank.getReceiptNumber());
			if (glCashReceiptBank == null) {
				return null;
			}

			glCashReceiptBank.setBankName(dtoGLCashReceiptBank.getBankName());
			glCashReceiptBank.setCheckbookMaintenance(repositoryCheckBookMaintenance
					.findByCheckBookIdAndIsDeleted(dtoGLCashReceiptBank.getCheckBookId(), false));
			if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getCheckNumber())) {
				glCashReceiptBank.setCheckNumber(dtoGLCashReceiptBank.getCheckNumber());
			}
			if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getCreditCardID())) {
				glCashReceiptBank.setCreditCardID(dtoGLCashReceiptBank.getCreditCardID());
			} else {
				glCashReceiptBank.setCreditCardID(null);
			}
			if (dtoGLCashReceiptBank.getCreditCardExpireMonth() != null) {
				glCashReceiptBank.setCreditCardExpireMonth(dtoGLCashReceiptBank.getCreditCardExpireMonth());
			} else {
				glCashReceiptBank.setCreditCardExpireMonth(0);
			}

			if (dtoGLCashReceiptBank.getCreditCardExpireYear() != null) {
				glCashReceiptBank.setCreditCardExpireYear(dtoGLCashReceiptBank.getCreditCardExpireYear());
			} else {
				glCashReceiptBank.setCreditCardExpireYear(0);
			}

			if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getCreditCardNumber())) {
				glCashReceiptBank.setCreditCardNumber(dtoGLCashReceiptBank.getCreditCardNumber());
			} else {
				glCashReceiptBank.setCreditCardNumber(null);
			}

			glCashReceiptBank.setCurrencyID(dtoGLCashReceiptBank.getCurrencyID());
			glCashReceiptBank.setDepositorName(dtoGLCashReceiptBank.getDepositorName());
			glCashReceiptBank.setGlBatches(
					repositoryGLBatches.findByBatchIDAndIsDeleted(dtoGLCashReceiptBank.getBatchID(), false));
			if (dtoGLCashReceiptBank.getPaymentMethod() != null) {
				glCashReceiptBank
						.setPaymentMethod(repositoryPaymentMethodType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
								dtoGLCashReceiptBank.getPaymentMethod(), langid, false));
			}
			glCashReceiptBank.setReceiptAmount(dtoGLCashReceiptBank.getReceiptAmount());
			glCashReceiptBank
					.setReceiptDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoGLCashReceiptBank.getReceiptDate()));
			glCashReceiptBank.setReceiptDescription(dtoGLCashReceiptBank.getReceiptDescription());

			if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getAccountTableRowIndex())) {
				glCashReceiptBank.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(dtoGLCashReceiptBank.getAccountTableRowIndex(), false));
			}

			if (dtoGLCashReceiptBank.getReceiptType() == ReceiptTypeConstant.AR.getIndex()) {
				glCashReceiptBank.setCustomerMaintenance(repositoryCustomerMaintenance
						.findByCustnumberAndIsDeleted(dtoGLCashReceiptBank.getCustomerId(), false));
			} else if (dtoGLCashReceiptBank.getReceiptType() == ReceiptTypeConstant.AP.getIndex()) {
				glCashReceiptBank.setVendorMaintenance(repositoryVendorMaintenance
						.findByVendoridAndIsDeleted(dtoGLCashReceiptBank.getVendorId(), false));
			}

			if (dtoGLCashReceiptBank.getAuditTrial() != null) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes
						.findBySeriesIndexAndIsDeleted(dtoGLCashReceiptBank.getAuditTrial().intValue(), false);
				glCashReceiptBank.setGlConfigurationAuditTrialCOdes(audit);
			}
			glCashReceiptBank.setReceiptType(repositoryReceiptType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
					dtoGLCashReceiptBank.getReceiptType(), langid, false));
			glCashReceiptBank.setVatAmount(dtoGLCashReceiptBank.getVatAmount());
			glCashReceiptBank.setVatSetup(
					repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoGLCashReceiptBank.getVatScheduleID(), false));
			glCashReceiptBank.setCustomerMaintenance(repositoryCustomerMaintenance
					.findByCustnumberAndIsDeleted(dtoGLCashReceiptBank.getCustomerId(), false));
			glCashReceiptBank = repositoryGLCashReceiptBank.saveAndFlush(glCashReceiptBank);
			return new DtoGLCashReceiptBank(glCashReceiptBank);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return null;
		}
	}

	/**
	 * @description this service will use to get cash receipt entry by receipt
	 *              number
	 * @param dtoGLCashReceiptBank
	 * @return
	 */
	public DtoGLCashReceiptBank getCashReceiptEntryByReceiptNumber(DtoGLCashReceiptBank dtoGLCashReceiptBank) {
		try {
			GLCashReceiptBank glCashReceiptBank = repositoryGLCashReceiptBank
					.findByReceiptNumber(dtoGLCashReceiptBank.getReceiptNumber());
			if (glCashReceiptBank != null) {
				return new DtoGLCashReceiptBank(glCashReceiptBank);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	public DtoGLCashReceiptBank postCashReceiptEntry(DtoGLCashReceiptBank dtoGLCashReceiptBank) {

			try {
				int langid = serviceHome.getLanngugaeId();

				// Save in gl90400
				GLYTDOpenBankTransactionsEntryHeader glytdOpenTransactionEntryHeader = new GLYTDOpenBankTransactionsEntryHeader();
				glytdOpenTransactionEntryHeader.setBankTransactionEntryID(dtoGLCashReceiptBank.getReceiptNumber());
				glytdOpenTransactionEntryHeader.setBankTransactionType(
						repositoryBankTransactionType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
								BankTransactionTypeConstant.BANK_CASH_RECEIPT.getIndex(), langid, false));
				glytdOpenTransactionEntryHeader.setBankTransactionEntryType(
						repositoryBankTransactionEntryType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
								BankTransactionEntryTypeConstant.TRANSACTION.getIndex(), langid, false));
				glytdOpenTransactionEntryHeader.setBankTransactionEntryAction(null);
				glytdOpenTransactionEntryHeader.setBankTransactionDate(
						UtilDateAndTime.ddmmyyyyStringToDate(dtoGLCashReceiptBank.getReceiptDate()));
				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getCheckBookId())) {
					glytdOpenTransactionEntryHeader.setCheckbookId(dtoGLCashReceiptBank.getCheckBookId());
				}
				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getExchangeTableIndex())) {
					glytdOpenTransactionEntryHeader.setExchangeTableIndex(dtoGLCashReceiptBank.getExchangeTableIndex());
					glytdOpenTransactionEntryHeader.setExchangeRate(
							dtoGLCashReceiptBank.getExchangeRate() != null ? dtoGLCashReceiptBank.getExchangeRate()
									: 0.0);
				}
				if (dtoGLCashReceiptBank.getAuditTrial() != null) {
					glytdOpenTransactionEntryHeader
							.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
									.findBySeriesIndexAndIsDeleted(dtoGLCashReceiptBank.getAuditTrial(), false));
				}

				glytdOpenTransactionEntryHeader.setCurrencyId(dtoGLCashReceiptBank.getCurrencyID());
				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getCreditCardID())
						&& !dtoGLCashReceiptBank.getCreditCardID().equalsIgnoreCase("0")) {
					glytdOpenTransactionEntryHeader
							.setCreditCardExpireMonth(dtoGLCashReceiptBank.getCreditCardExpireMonth());
					glytdOpenTransactionEntryHeader
							.setCreditCardExpireYear(dtoGLCashReceiptBank.getCreditCardExpireYear());
					glytdOpenTransactionEntryHeader.setCreditCardID(dtoGLCashReceiptBank.getCreditCardID());
					glytdOpenTransactionEntryHeader.setCreditCardNumber(dtoGLCashReceiptBank.getCreditCardNumber());
				}

				glytdOpenTransactionEntryHeader.setReceiveName(dtoGLCashReceiptBank.getDepositorName());
				glytdOpenTransactionEntryHeader
						.setBankTransactionDescription(dtoGLCashReceiptBank.getReceiptDescription());
				glytdOpenTransactionEntryHeader.setBankTransactionAmount(
						dtoGLCashReceiptBank.getReceiptAmount() + dtoGLCashReceiptBank.getVatAmount());
				glytdOpenTransactionEntryHeader.setPostingDate(new Date());
				glytdOpenTransactionEntryHeader
						.setPostingbyUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
				glytdOpenTransactionEntryHeader.setRowDateindex(new Date());
				

				// Save in gl90401
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double currentExchangeRate = 0.0;
				String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
				if (dtoGLCashReceiptBank.getExchangeRate() != null) {
					currentExchangeRate = Double.valueOf(dtoGLCashReceiptBank.getExchangeRate());
					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
							.findByExchangeIndexAndIsDeleted(
									Integer.parseInt(dtoGLCashReceiptBank.getExchangeTableIndex()), false);
					if (currencyExchangeHeader != null
							&& UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
						calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
								.getMethod();
					}
				}

				Double originalDebitAmount = dtoGLCashReceiptBank.getReceiptAmount();
				Double originalCreditAmount = dtoGLCashReceiptBank.getReceiptAmount()
						+ dtoGLCashReceiptBank.getVatAmount();
				Double originalTaxCreditAmount = dtoGLCashReceiptBank.getVatAmount();
				Double debitAmount = 0.0;
				Double creditAmount = 0.0;
				Double taxCreditAmount = 0.0;

				if (UtilRandomKey.isNotBlank(calcMethod)) {
					debitAmount = (double) mathEval.eval(originalDebitAmount + calcMethod + currentExchangeRate);
					creditAmount = (double) mathEval.eval(originalCreditAmount + calcMethod + currentExchangeRate);
					taxCreditAmount = (double) mathEval
							.eval(originalTaxCreditAmount + calcMethod + currentExchangeRate);
				}
				// save in 90401
				int debitAccountTableRowIndex = 0;
				int vatAccountTableRowIndex = 0;
				int creditAccountTableRowIndex = 0;
				CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance
						.findByCheckBookIdAndIsDeleted(dtoGLCashReceiptBank.getCheckBookId(), false);
				CreditCardsSetup creditCard = repositoryCreditCardsSetup
						.findByCardIndxAndIsDeleted(Integer.parseInt(dtoGLCashReceiptBank.getCreditCardID()), false);
				if (checkbookMaintenance != null && checkbookMaintenance.getGlAccountsTableAccumulation() != null) {
					debitAccountTableRowIndex = Integer
							.parseInt(checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex());
				} else if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getAccountTableRowIndexDebit())) {
					debitAccountTableRowIndex = Integer.parseInt(dtoGLCashReceiptBank.getAccountTableRowIndexDebit());
				} else if (creditCard != null) {
					if (creditCard.getGlAccountsTableAccumulation() == null
							|| UtilRandomKey.isNotBlank(creditCard.getCheckBookIdBank()) ) {
						dtoGLCashReceiptBank.setMessageType(MessageLabel.ACCOUNT_NUMBER_NOT_FOUND_FOR_CREDIT_CARD);
						return dtoGLCashReceiptBank;
					}
					debitAccountTableRowIndex = Integer
							.parseInt(creditCard.getGlAccountsTableAccumulation().getAccountTableRowIndex());
				}
//			else{
//					dtoGLCashReceiptBank.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
//					return dtoGLCashReceiptBank;
//				}

				VATSetup vatSetup = repositoryVATSetup
						.findByVatScheduleIdAndIsDeleted(dtoGLCashReceiptBank.getVatScheduleID(), false);
				if (vatSetup != null) {
					vatAccountTableRowIndex = vatSetup.getAccountRowId();
				}

				GLAccountsTableAccumulation vatAccountTableAccumulation = repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(String.valueOf(vatAccountTableRowIndex), false);
//				if(vatAccountTableAccumulation==null)
//				{
//					dtoGLCashReceiptBank.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
//					return dtoGLCashReceiptBank;
//				}
				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getAccountTableRowIndex())) {
					creditAccountTableRowIndex = Integer.parseInt(dtoGLCashReceiptBank.getAccountTableRowIndex());
				} else {
					if (dtoGLCashReceiptBank.getReceiptType() == ReceiptTypeConstant.AR.getIndex()) {
						CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance
								.findByCustnumberAndIsDeleted(dtoGLCashReceiptBank.getCustomerId(), false);
						if (customerMaintenance != null && customerMaintenance.getCustomerClassesSetup() != null) {
							CustomerClassesSetup customerClassesSetup = customerMaintenance.getCustomerClassesSetup();

							CustomerClassAccountTableSetup customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup
									.findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(
											MasterArAcoountTypeConstant.ACCOUNT_RECV.getIndex(),
											customerClassesSetup.getCustomerClassId());
							if (customerClassAccountTableSetup != null) {
								creditAccountTableRowIndex = Integer.parseInt(customerClassAccountTableSetup
										.getGlAccountsTableAccumulation().getAccountTableRowIndex());
							}
						}
					}
					if (dtoGLCashReceiptBank.getReceiptType() == ReceiptTypeConstant.AP.getIndex()) {

						VendorMaintenance vendorMaintenance = repositoryVendorMaintenance
								.findByVendoridAndIsDeleted(dtoGLCashReceiptBank.getVendorId(), false);
						if (vendorMaintenance != null && vendorMaintenance.getVendorClassesSetup() != null) {
							VendorClassesSetup vendorClassesSetup = vendorMaintenance.getVendorClassesSetup();

							VendorClassAccountTableSetup vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup
									.findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(
											MasterAPAccountTypeConstant.ACCOUNT_PAYABLE.getIndex(),
											vendorClassesSetup.getVendorClassId());
							if (vendorClassAccountTableSetup != null) {
								creditAccountTableRowIndex = Integer.parseInt(vendorClassAccountTableSetup
										.getGlAccountsTableAccumulation().getAccountTableRowIndex());
							}
						}

					}
				}

				GLAccountsTableAccumulation creditAccountTableAccumulation = repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(String.valueOf(creditAccountTableRowIndex), false);
				if (creditAccountTableAccumulation == null) {
					dtoGLCashReceiptBank.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
					return dtoGLCashReceiptBank;
				}

				GLYTDOpenBankTransactionsEntryDetails glytdBankEntryDetailDebit = new GLYTDOpenBankTransactionsEntryDetails();
				glytdBankEntryDetailDebit.setGlytdOpenBankTransactionsEntryHeader(glytdOpenTransactionEntryHeader);
				glytdBankEntryDetailDebit.setDebitAmount(debitAmount);
				glytdBankEntryDetailDebit.setOriginalDebitAmount(originalDebitAmount);
				glytdBankEntryDetailDebit.setAccountTableRowIndex(debitAccountTableRowIndex);
				glytdBankEntryDetailDebit.setDistributionDescription(dtoGLCashReceiptBank.getReceiptDescription());
				glytdBankEntryDetailDebit.setRowDateindex(new Date());
				

//				GLYTDOpenBankTransactionsEntryDetails glytdBankEntryDetailVat = new GLYTDOpenBankTransactionsEntryDetails();
//				glytdBankEntryDetailVat.setGlytdOpenBankTransactionsEntryHeader(glytdOpenTransactionEntryHeader);
//				glytdBankEntryDetailVat.setCreditAmount(taxCreditAmount);
//				glytdBankEntryDetailVat.setOriginalCreditAmount(originalTaxCreditAmount);
//				glytdBankEntryDetailVat.setAccountTableRowIndex(vatAccountTableRowIndex);
//				glytdBankEntryDetailVat.setDistributionDescription(dtoGLCashReceiptBank.getReceiptDescription());
//				glytdBankEntryDetailVat.setRowDateindex(new Date());
//				repositoryGLYTDOpenBankTransactionsEntryDetails.saveAndFlush(glytdBankEntryDetailVat);
//				
				GLYTDOpenBankTransactionsEntryDetails glytdBankEntryDetailCredit = new GLYTDOpenBankTransactionsEntryDetails();
				glytdBankEntryDetailCredit.setGlytdOpenBankTransactionsEntryHeader(glytdOpenTransactionEntryHeader);
				glytdBankEntryDetailCredit.setCreditAmount(creditAmount);
				glytdBankEntryDetailCredit.setOriginalCreditAmount(originalCreditAmount);
				glytdBankEntryDetailCredit.setAccountTableRowIndex(creditAccountTableRowIndex);
				glytdBankEntryDetailCredit.setDistributionDescription(dtoGLCashReceiptBank.getReceiptDescription());
				glytdBankEntryDetailCredit.setRowDateindex(new Date());
				

				// save in 90200
				int year = 0;
				int periodId = 0;
				Calendar cal = Calendar.getInstance();
				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getReceiptDate())) {
					Date transDate = UtilDateAndTime.ddmmyyyyStringToDate(dtoGLCashReceiptBank.getReceiptDate());
					cal.setTime(transDate);
					year = cal.get(Calendar.YEAR);
					periodId = cal.get(Calendar.MONTH);
				}

				GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
				int nextJournalId = 0;
				if (glConfigurationSetup != null) {
					nextJournalId = glConfigurationSetup.getNextJournalEntryVoucher();
				}

//				// Save for Debit
//				GLYTDOpenTransactions glytdOpenTransactionsDebit = new GLYTDOpenTransactions();
//				glytdOpenTransactionsDebit.setOpenYear(year);
//				glytdOpenTransactionsDebit.setJournalEntryID(String.valueOf(nextJournalId));
//				glytdOpenTransactionsDebit.setTransactionSource("CR");
//				glytdOpenTransactionsDebit.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
//						.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
//								false));
//				glytdOpenTransactionsDebit.setJournalDescription(dtoGLCashReceiptBank.getReceiptDescription());
//				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getReceiptDate())) {
//					glytdOpenTransactionsDebit.setTransactionDate(
//							UtilDateAndTime.ddmmyyyyStringToDate(dtoGLCashReceiptBank.getReceiptDate()));
//				}
//				glytdOpenTransactionsDebit.setTransactionPostingDate(new Date());
//				glytdOpenTransactionsDebit.setAccountTableRowIndex(String.valueOf(debitAccountTableRowIndex));
//				glytdOpenTransactionsDebit.setUserCreateTransaction(httpServletRequest.getHeader(CommonConstant.USER_ID));
//				glytdOpenTransactionsDebit.setUserPostingTransaction(httpServletRequest.getHeader(CommonConstant.USER_ID));
//				glytdOpenTransactionsDebit.setModuleSeriesNumber(repositoryGLConfigurationAuditTrialCodes
//						.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
//								false));
//				glytdOpenTransactionsDebit.setOriginalTransactionNumber(dtoGLCashReceiptBank.getReceiptNumber());
//				glytdOpenTransactionsDebit.setCurrencyId(dtoGLCashReceiptBank.getCurrencyID());
//				glytdOpenTransactionsDebit
//						.setExchangeTableIndex(Integer.parseInt(dtoGLCashReceiptBank.getExchangeTableIndex()));
//				glytdOpenTransactionsDebit.setExchangeTableRate(dtoGLCashReceiptBank.getExchangeRate());
//				glytdOpenTransactionsDebit.setDebitAmount(debitAmount);
//				glytdOpenTransactionsDebit.setOriginalDebitAmount(originalDebitAmount);
//				glytdOpenTransactionsDebit.setRowDateIndex(new Date());
//				repositoryGlytdOpenTransaction.saveAndFlush(glytdOpenTransactionsDebit);
				//
////				// Save for Tax
////				GLYTDOpenTransactions glytdOpenTransactionsVat = new GLYTDOpenTransactions();
////				glytdOpenTransactionsVat.setOpenYear(year);
////				glytdOpenTransactionsVat.setJournalEntryID(String.valueOf(nextJournalId));
////				glytdOpenTransactionsVat.setTransactionSource("CR");
////				glytdOpenTransactionsVat.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
////						.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
////								false));
////				glytdOpenTransactionsVat.setJournalDescription(dtoGLCashReceiptBank.getReceiptDescription());
////				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getReceiptDate())) {
////					glytdOpenTransactionsVat.setTransactionDate(
////							UtilDateAndTime.ddmmyyyyStringToDate(dtoGLCashReceiptBank.getReceiptDate()));
////				}
////				glytdOpenTransactionsVat.setTransactionPostingDate(new Date());
////				glytdOpenTransactionsVat.setAccountTableRowIndex(String.valueOf(vatAccountTableRowIndex));
////				glytdOpenTransactionsVat.setUserCreateTransaction(httpServletRequest.getHeader(CommonConstant.USER_ID));
////				glytdOpenTransactionsVat.setUserPostingTransaction(httpServletRequest.getHeader(CommonConstant.USER_ID));
////				glytdOpenTransactionsVat.setModuleSeriesNumber(repositoryGLConfigurationAuditTrialCodes
////						.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
////								false));
////				glytdOpenTransactionsVat.setOriginalTransactionNumber(dtoGLCashReceiptBank.getReceiptNumber());
////				glytdOpenTransactionsVat.setCurrencyId(dtoGLCashReceiptBank.getCurrencyID());
////				glytdOpenTransactionsVat
////						.setExchangeTableIndex(Integer.parseInt(dtoGLCashReceiptBank.getExchangeTableIndex()));
////				glytdOpenTransactionsVat.setExchangeTableRate(dtoGLCashReceiptBank.getExchangeRate());
////				glytdOpenTransactionsVat.setCreditAmount(taxCreditAmount);
////				glytdOpenTransactionsVat.setOriginalCreditAmount(originalTaxCreditAmount);
////				glytdOpenTransactionsVat.setRowDateIndex(new Date());
////				repositoryGlytdOpenTransaction.saveAndFlush(glytdOpenTransactionsVat);
				//
//				// save for credit
//				GLYTDOpenTransactions glytdOpenTransactionsCredit = new GLYTDOpenTransactions();
//				glytdOpenTransactionsCredit.setOpenYear(year);
//				glytdOpenTransactionsCredit.setJournalEntryID(String.valueOf(nextJournalId));
//				glytdOpenTransactionsCredit.setTransactionSource("CR");
//				glytdOpenTransactionsCredit.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
//						.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
//								false));
//				glytdOpenTransactionsCredit.setJournalDescription(dtoGLCashReceiptBank.getReceiptDescription());
//				if (UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getReceiptDate())) {
//					glytdOpenTransactionsCredit.setTransactionDate(
//							UtilDateAndTime.ddmmyyyyStringToDate(dtoGLCashReceiptBank.getReceiptDate()));
//				}
//				glytdOpenTransactionsCredit.setTransactionPostingDate(new Date());
//				glytdOpenTransactionsCredit.setAccountTableRowIndex(String.valueOf(creditAccountTableRowIndex));
//				glytdOpenTransactionsCredit.setUserCreateTransaction(httpServletRequest.getHeader(CommonConstant.USER_ID));
//				glytdOpenTransactionsCredit.setUserPostingTransaction(httpServletRequest.getHeader(CommonConstant.USER_ID));
//				glytdOpenTransactionsCredit.setModuleSeriesNumber(repositoryGLConfigurationAuditTrialCodes
//						.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(AuditTrailSeriesTypeConstant.GL.getIndex(),
//								false));
//				glytdOpenTransactionsCredit.setOriginalTransactionNumber(dtoGLCashReceiptBank.getReceiptNumber());
//				glytdOpenTransactionsCredit.setCurrencyId(dtoGLCashReceiptBank.getCurrencyID());
//				glytdOpenTransactionsCredit
//						.setExchangeTableIndex(Integer.parseInt(dtoGLCashReceiptBank.getExchangeTableIndex()));
//				glytdOpenTransactionsCredit.setExchangeTableRate(dtoGLCashReceiptBank.getExchangeRate());
//				glytdOpenTransactionsCredit.setCreditAmount(creditAmount);
//				glytdOpenTransactionsCredit.setOriginalCreditAmount(originalCreditAmount);
//				glytdOpenTransactionsCredit.setRowDateIndex(new Date());
//				repositoryGlytdOpenTransaction.saveAndFlush(glytdOpenTransactionsCredit);

				// Update Gl Configuration Setup for net Journal entry Id
				//if (glConfigurationSetup != null) {
				//	glConfigurationSetup.setNextJournalEntryVoucher(nextJournalId + 1);
				//	repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);
				//}

				List<GLYTDOpenBankTransactionsEntryDetails> details = Arrays.asList(glytdBankEntryDetailDebit,glytdBankEntryDetailCredit);
				DtoJournalEntryHeader dtoJournalEntryHeader = mappingReceiptVouhcer
						.modelRVPostedToDtoJVUnposted(dtoGLCashReceiptBank,glytdOpenTransactionEntryHeader,details);
				dtoJournalEntryHeader.setTransactionSource("CR");
				String number = servicePosting.saveAndPostingWithSequence(dtoJournalEntryHeader, 2);

				if(number.equalsIgnoreCase("FOUND")) {
					return null;
				}else{
					glytdOpenTransactionEntryHeader = repositoryGLYTDOpenBankTransactionsEntryHeader
							.saveAndFlush(glytdOpenTransactionEntryHeader);
					repositoryGLYTDOpenBankTransactionsEntryDetails.saveAndFlush(glytdBankEntryDetailCredit);
					repositoryGLYTDOpenBankTransactionsEntryDetails.saveAndFlush(glytdBankEntryDetailDebit);
					GLCashReceiptBank glCashReceiptBankDelete = repositoryGLCashReceiptBank
							.findByReceiptNumber(dtoGLCashReceiptBank.getReceiptNumber());
					if (glCashReceiptBankDelete != null) {
						List<GLCashReceiptDistribution> glCashReceiptDistributionList = repositoryGLCashReceiptDistribution
								.findByGlCashReceiptBankBankCashReceipt(glCashReceiptBankDelete.getBankCashReceipt());
						if (glCashReceiptDistributionList != null && !glCashReceiptDistributionList.isEmpty()) {
							repositoryGLCashReceiptDistribution.deleteInBatch(glCashReceiptDistributionList);
						}
						repositoryGLCashReceiptBank.delete(glCashReceiptBankDelete);
					}
				}
				
				int dimIndex1 = 0;
				int dimIndex2 = 0;
				int dimIndex3 = 0;
				int dimIndex4 = 0;
				int dimIndex5 = 0;
				int mainAccountIndex = 0;
				int accountTableRowIndex = 0;
				String accountNumberWithZero = null;

				// Account Number for Debit
				GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(String.valueOf(debitAccountTableRowIndex), false);
				if (glAccountsTableAccumulation != null) {
					accountTableRowIndex = debitAccountTableRowIndex;
					accountNumberWithZero = serviceAccountPayable
							.getAccountNumberIdsWithZero(glAccountsTableAccumulation);
				}

				if (UtilRandomKey.isNotBlank(accountNumberWithZero)) {
					String[] accountNo = accountNumberWithZero.split("-");
					for (int i = 0; i < accountNo.length; i++) {
						if (i > 0) {
							if (i == 1) {
								dimIndex1 = Integer.parseInt(accountNo[i]);
							}
							if (i == 2) {
								dimIndex2 = Integer.parseInt(accountNo[i]);
							}
							if (i == 3) {
								dimIndex3 = Integer.parseInt(accountNo[i]);
							}
							if (i == 4) {
								dimIndex4 = Integer.parseInt(accountNo[i]);
							}
							if (i == 5) {
								dimIndex5 = Integer.parseInt(accountNo[i]);
							}
						}
					}
					mainAccountIndex = Integer.parseInt(accountNo[0]);
				}

				GLCurrentSummaryMasterTableByMainAccountKey currSumaryByMainAccKey = null;
				GLCurrentSummaryMasterTableByMainAccount currSumaryByMainAcc = null;
				GLCurrentSummaryMasterTableByAccountNumberKey currSumaryByAccNumberKey = null;
				GLCurrentSummaryMasterTableByAccountNumber currSumaryByAccNumber = null;
				GLCurrentSummaryMasterTableByDimensionsKey glCurreSummaryByDimInxKey = null;
				GLCurrentSummaryMasterTableByDimensions glCurreSummaryByDimInx = null;
				// Save For debt gl90110, gl90111, gl90112 and find by composite key
				currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
				currSumaryByMainAccKey.setYear(year);
				currSumaryByMainAccKey.setPeriodID(periodId + 1);
				currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
				currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount
						.findOne(currSumaryByMainAccKey);
				if (currSumaryByMainAcc != null) {
					currSumaryByMainAcc.setDebitAmount(currSumaryByMainAcc.getDebitAmount() + debitAmount);
					currSumaryByMainAcc.setPeriodBalance(
							currSumaryByMainAcc.getDebitAmount() - currSumaryByMainAcc.getCreditAmount());
				} else {
					currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
					currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
					currSumaryByMainAcc.setYear(year);
					currSumaryByMainAcc.setPeriodID(periodId + 1);
					currSumaryByMainAcc.setDebitAmount(debitAmount);
					currSumaryByMainAcc.setPeriodBalance(debitAmount - 0);
				}

				currSumaryByMainAcc.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);

				// Save data in gl90111
				currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
				currSumaryByAccNumberKey.setYear(year);
				currSumaryByAccNumberKey.setAccountTableRowIndex(accountTableRowIndex);
				currSumaryByAccNumberKey.setPeriodID(periodId + 1);
				currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
						.findOne(currSumaryByAccNumberKey);

				if (currSumaryByAccNumber != null) {
					currSumaryByAccNumber.setDebitAmount(currSumaryByAccNumber.getDebitAmount() + debitAmount);
					currSumaryByAccNumber.setPeriodBalance(
							currSumaryByAccNumber.getDebitAmount() - currSumaryByAccNumber.getCreditAmount());
				} else {
					currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
					currSumaryByAccNumber.setAccountTableRowIndex(accountTableRowIndex);
					currSumaryByAccNumber.setYear(year);
					currSumaryByAccNumber.setPeriodID(periodId + 1);
					currSumaryByAccNumber.setDebitAmount(debitAmount);
					currSumaryByAccNumber.setPeriodBalance(debitAmount - 0);
				}
				currSumaryByAccNumber.setIsDeleted(false);
				currSumaryByAccNumber.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

				// Save data in gl90112
				glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
				glCurreSummaryByDimInxKey.setYear(year);
				glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
				glCurreSummaryByDimInxKey.setPeriodID(periodId + 1);
				glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
						.findOne(glCurreSummaryByDimInxKey);

				if (glCurreSummaryByDimInx != null) {
					glCurreSummaryByDimInx.setDebitAmount(glCurreSummaryByDimInx.getDebitAmount() + debitAmount);
					glCurreSummaryByDimInx.setPeriodBalance(
							glCurreSummaryByDimInx.getDebitAmount() - glCurreSummaryByDimInx.getCreditAmount());
				} else {
					glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
					glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
					glCurreSummaryByDimInx.setYear(year);
					glCurreSummaryByDimInx.setPeriodID(periodId + 1);
					glCurreSummaryByDimInx.setDebitAmount(debitAmount);
					glCurreSummaryByDimInx.setPeriodBalance(debitAmount - 0);
				}

				glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
				glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
				glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
				glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
				glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
				glCurreSummaryByDimInx.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);

				// Account Number for vat
				GLAccountsTableAccumulation glAccountsTableAccumulationVat = repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(String.valueOf(vatAccountTableRowIndex), false);
				if (glAccountsTableAccumulationVat != null) {
					accountTableRowIndex = vatAccountTableRowIndex;
					accountNumberWithZero = serviceAccountPayable
							.getAccountNumberIdsWithZero(glAccountsTableAccumulationVat);
				}

				if (UtilRandomKey.isNotBlank(accountNumberWithZero)) {
					String[] accountNo = accountNumberWithZero.split("-");
					for (int i = 0; i < accountNo.length; i++) {
						if (i > 0) {
							if (i == 1) {
								dimIndex1 = Integer.parseInt(accountNo[i]);
							}
							if (i == 2) {
								dimIndex2 = Integer.parseInt(accountNo[i]);
							}
							if (i == 3) {
								dimIndex3 = Integer.parseInt(accountNo[i]);
							}
							if (i == 4) {
								dimIndex4 = Integer.parseInt(accountNo[i]);
							}
							if (i == 5) {
								dimIndex5 = Integer.parseInt(accountNo[i]);
							}
						}
					}
					mainAccountIndex = Integer.parseInt(accountNo[0]);
				}

				currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
				currSumaryByMainAccKey.setYear(year);
				currSumaryByMainAccKey.setPeriodID(periodId + 1);
				currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
				currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount
						.findOne(currSumaryByMainAccKey);
				if (currSumaryByMainAcc != null) {
					currSumaryByMainAcc.setCreditAmount(currSumaryByMainAcc.getCreditAmount() + taxCreditAmount);
					currSumaryByMainAcc.setPeriodBalance(
							currSumaryByMainAcc.getDebitAmount() - currSumaryByMainAcc.getCreditAmount());
				} else {
					currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
					currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
					currSumaryByMainAcc.setYear(year);
					currSumaryByMainAcc.setPeriodID(periodId + 1);
					currSumaryByMainAcc.setCreditAmount(taxCreditAmount);
					currSumaryByMainAcc.setPeriodBalance(0 - taxCreditAmount);
				}

				currSumaryByMainAcc.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);

				// Save data in gl90111

				currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
				currSumaryByAccNumberKey.setYear(year);
				currSumaryByAccNumberKey.setAccountTableRowIndex(accountTableRowIndex);
				currSumaryByAccNumberKey.setPeriodID(periodId + 1);
				currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
						.findOne(currSumaryByAccNumberKey);
				if (currSumaryByAccNumber != null) {
					currSumaryByAccNumber.setCreditAmount(currSumaryByAccNumber.getCreditAmount() + taxCreditAmount);
					currSumaryByAccNumber.setPeriodBalance(
							currSumaryByAccNumber.getDebitAmount() - currSumaryByAccNumber.getCreditAmount());
				} else {
					currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
					currSumaryByAccNumber.setAccountTableRowIndex(accountTableRowIndex);
					currSumaryByAccNumber.setYear(year);
					currSumaryByAccNumber.setPeriodID(periodId + 1);
					currSumaryByAccNumber.setCreditAmount(taxCreditAmount);
					currSumaryByAccNumber.setPeriodBalance(0 - taxCreditAmount);
				}
				currSumaryByAccNumber.setIsDeleted(false);
				currSumaryByAccNumber.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

				// Save data in gl90112
				glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
				glCurreSummaryByDimInxKey.setYear(year);
				glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
				glCurreSummaryByDimInxKey.setPeriodID(periodId + 1);
				glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
						.findOne(glCurreSummaryByDimInxKey);
				if (glCurreSummaryByDimInx != null) {
					glCurreSummaryByDimInx.setCreditAmount(glCurreSummaryByDimInx.getCreditAmount() + taxCreditAmount);
					glCurreSummaryByDimInx.setPeriodBalance(
							glCurreSummaryByDimInx.getDebitAmount() - glCurreSummaryByDimInx.getCreditAmount());
				} else {
					glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
					glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
					glCurreSummaryByDimInx.setYear(year);
					glCurreSummaryByDimInx.setPeriodID(periodId + 1);
					glCurreSummaryByDimInx.setCreditAmount(taxCreditAmount);
					glCurreSummaryByDimInx.setPeriodBalance(0 - taxCreditAmount);
				}

				glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
				glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
				glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
				glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
				glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
				glCurreSummaryByDimInx.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);

				// Account Number for credit
				GLAccountsTableAccumulation glAccountsTableAccumulationCredit = repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(String.valueOf(creditAccountTableRowIndex), false);
				if (glAccountsTableAccumulationCredit != null) {
					accountTableRowIndex = creditAccountTableRowIndex;
					accountNumberWithZero = serviceAccountPayable
							.getAccountNumberIdsWithZero(glAccountsTableAccumulationCredit);
				}

				if (UtilRandomKey.isNotBlank(accountNumberWithZero)) {
					String[] accountNo = accountNumberWithZero.split("-");
					for (int i = 0; i < accountNo.length; i++) {
						if (i > 0) {
							if (i == 1) {
								dimIndex1 = Integer.parseInt(accountNo[i]);
							}
							if (i == 2) {
								dimIndex2 = Integer.parseInt(accountNo[i]);
							}
							if (i == 3) {
								dimIndex3 = Integer.parseInt(accountNo[i]);
							}
							if (i == 4) {
								dimIndex4 = Integer.parseInt(accountNo[i]);
							}
							if (i == 5) {
								dimIndex5 = Integer.parseInt(accountNo[i]);
							}
						}
					}
					mainAccountIndex = Integer.parseInt(accountNo[0]);
				}

				currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
				currSumaryByMainAccKey.setYear(year);
				currSumaryByMainAccKey.setPeriodID(periodId + 1);
				currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
				currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount
						.findOne(currSumaryByMainAccKey);
				if (currSumaryByMainAcc != null) {
					currSumaryByMainAcc.setCreditAmount(currSumaryByMainAcc.getCreditAmount() + creditAmount);
					currSumaryByMainAcc.setPeriodBalance(
							currSumaryByMainAcc.getDebitAmount() - currSumaryByMainAcc.getCreditAmount());
				} else {
					currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
					currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
					currSumaryByMainAcc.setYear(year);
					currSumaryByMainAcc.setPeriodID(periodId + 1);
					currSumaryByMainAcc.setCreditAmount(creditAmount);
					currSumaryByMainAcc.setPeriodBalance(0 - creditAmount);
				}

				currSumaryByMainAcc.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);

				// Save data in gl90111

				currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
				currSumaryByAccNumberKey.setYear(year);
				currSumaryByAccNumberKey.setAccountTableRowIndex(accountTableRowIndex);
				currSumaryByAccNumberKey.setPeriodID(periodId + 1);
				currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
						.findOne(currSumaryByAccNumberKey);
				if (currSumaryByAccNumber != null) {
					currSumaryByAccNumber.setCreditAmount(currSumaryByAccNumber.getCreditAmount() + creditAmount);
					currSumaryByAccNumber.setPeriodBalance(
							currSumaryByAccNumber.getDebitAmount() - currSumaryByAccNumber.getCreditAmount());
				} else {
					currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
					currSumaryByAccNumber.setAccountTableRowIndex(accountTableRowIndex);
					currSumaryByAccNumber.setYear(year);
					currSumaryByAccNumber.setPeriodID(periodId + 1);
					currSumaryByAccNumber.setCreditAmount(creditAmount);
					currSumaryByAccNumber.setPeriodBalance(0 - creditAmount);
				}
				currSumaryByAccNumber.setIsDeleted(false);
				currSumaryByAccNumber.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

				// Save data in gl90112
				glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
				glCurreSummaryByDimInxKey.setYear(year);
				glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
				glCurreSummaryByDimInxKey.setPeriodID(periodId + 1);
				glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
						.findOne(glCurreSummaryByDimInxKey);
				if (glCurreSummaryByDimInx != null) {
					glCurreSummaryByDimInx.setCreditAmount(glCurreSummaryByDimInx.getCreditAmount() + creditAmount);
					glCurreSummaryByDimInx.setPeriodBalance(
							glCurreSummaryByDimInx.getDebitAmount() - glCurreSummaryByDimInx.getCreditAmount());
				} else {
					glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
					glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
					glCurreSummaryByDimInx.setYear(year);
					glCurreSummaryByDimInx.setPeriodID(periodId + 1);
					glCurreSummaryByDimInx.setCreditAmount(creditAmount);
					glCurreSummaryByDimInx.setPeriodBalance(0 - creditAmount);
				}

				glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
				glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
				glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
				glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
				glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
				glCurreSummaryByDimInx.setRowDateIndex(new Date());
				repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);



				return dtoGLCashReceiptBank;
			} catch (Exception e) {
				LOG.error(e.getMessage());
				return null;
			}
		

	}

	/**
	 * @description this service will use to get payment method types
	 * @return
	 */
	public List<DtoStatusType> getPaymentMethodType() {
		List<DtoStatusType> list = new ArrayList<>();
		try {
			int langid = serviceHome.getLanngugaeId();
			List<PaymentMethodType> paymentMethodTypeList = repositoryPaymentMethodType
					.findByIsDeletedAndLanguageLanguageId(false, langid);
			if (paymentMethodTypeList != null && !paymentMethodTypeList.isEmpty()) {
				for (PaymentMethodType paymentMethodType : paymentMethodTypeList) {
					list.add(new DtoStatusType(paymentMethodType));
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}

	/**
	 * @description this service will use to fer receipt type
	 * @return
	 */
	public List<DtoStatusType> getReceiptType() {
		List<DtoStatusType> list = new ArrayList<>();
		try {
			int langid = serviceHome.getLanngugaeId();
			List<ReceiptType> receiptTypeList = repositoryReceiptType.findByIsDeletedAndLanguageLanguageId(false,
					langid);
			if (receiptTypeList != null && !receiptTypeList.isEmpty()) {
				for (ReceiptType receiptType : receiptTypeList) {
					list.add(new DtoStatusType(receiptType));
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;

	}

	/**
	 * @description this service will use to save cash receipt distributions
	 * @param dtoGLCashReceiptDistribution
	 * @return
	 */
	public DtoGLCashReceiptDistribution saveCashReceiptDistribution(
			DtoGLCashReceiptDistribution dtoGLCashReceiptDistribution) {
		try {
			List<GLCashReceiptDistribution> list = repositoryGLCashReceiptDistribution
					.findByGlCashReceiptBankReceiptNumber(dtoGLCashReceiptDistribution.getReceiptNumber());
			if (list != null && !list.isEmpty()) {
				repositoryGLCashReceiptDistribution.deleteInBatch(list);
			}

			GLCashReceiptBank glCashReceiptBank = repositoryGLCashReceiptBank
					.findByReceiptNumber(dtoGLCashReceiptDistribution.getReceiptNumber());
			if (glCashReceiptBank != null) {
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double currentExchangeRate = 0.0;
				String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
				if (glCashReceiptBank.getExchangeRate() != null) {
					currentExchangeRate = Double.valueOf(glCashReceiptBank.getExchangeRate());
					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
							.findByExchangeIndexAndIsDeleted(
									Integer.parseInt(glCashReceiptBank.getExchangeTableIndex()), false);
					if (currencyExchangeHeader != null
							&& UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
						calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
								.getMethod();
					}
				}

				Double originalDebitAmount = glCashReceiptBank.getReceiptAmount() + glCashReceiptBank.getVatAmount();
				Double originalCreditAmount = glCashReceiptBank.getReceiptAmount();
				Double originalTaxCreditAmount = glCashReceiptBank.getVatAmount();
				Double debitAmount = 0.0;
				Double creditAmount = 0.0;
				Double taxCreditAmount = 0.0;

				if (UtilRandomKey.isNotBlank(calcMethod)) {

					debitAmount = (double) mathEval.eval(originalDebitAmount + calcMethod + currentExchangeRate);
					creditAmount = (double) mathEval.eval(originalCreditAmount + calcMethod + currentExchangeRate);
					taxCreditAmount = (double) mathEval
							.eval(originalTaxCreditAmount + calcMethod + currentExchangeRate);

				}
				GLCashReceiptDistribution glCashReceiptDistributionDebit = new GLCashReceiptDistribution();
				glCashReceiptDistributionDebit.setDebitAmount(debitAmount);
				if (glCashReceiptBank.getCheckbookMaintenance() != null) {
					glCashReceiptDistributionDebit.setGlAccountsTableAccumulation(
							glCashReceiptBank.getCheckbookMaintenance().getGlAccountsTableAccumulation());
				}
				glCashReceiptDistributionDebit.setGlCashReceiptBank(glCashReceiptBank);
				glCashReceiptDistributionDebit.setAccountType(CashReceiptBankAccountTypeConstant.CASH.getIndex());
				glCashReceiptDistributionDebit.setOriginalDebitAmount(originalDebitAmount);
				repositoryGLCashReceiptDistribution.saveAndFlush(glCashReceiptDistributionDebit);

				GLCashReceiptDistribution glCashReceiptDistributionVat = new GLCashReceiptDistribution();
				glCashReceiptDistributionVat.setCreditAmount(taxCreditAmount);
				if (glCashReceiptBank.getVatSetup() != null) {
					glCashReceiptDistributionVat.setGlAccountsTableAccumulation(
							repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(
									String.valueOf(glCashReceiptBank.getVatSetup().getAccountRowId()), false));
				}
				glCashReceiptDistributionVat.setGlCashReceiptBank(glCashReceiptBank);
				glCashReceiptDistributionVat.setOriginalCreditAmount(originalTaxCreditAmount);
				glCashReceiptDistributionVat.setAccountType(CashReceiptBankAccountTypeConstant.TAX.getIndex());
				repositoryGLCashReceiptDistribution.saveAndFlush(glCashReceiptDistributionVat);

				GLCashReceiptDistribution glCashReceiptDistributionCredit = new GLCashReceiptDistribution();
				glCashReceiptDistributionCredit.setCreditAmount(creditAmount);

				glCashReceiptDistributionCredit
						.setGlAccountsTableAccumulation(glCashReceiptBank.getGlAccountsTableAccumulation());
				glCashReceiptDistributionCredit.setGlCashReceiptBank(glCashReceiptBank);
				glCashReceiptDistributionCredit.setOriginalCreditAmount(originalCreditAmount);
				glCashReceiptDistributionCredit.setAccountType(CashReceiptBankAccountTypeConstant.OTHER.getIndex());
				repositoryGLCashReceiptDistribution.saveAndFlush(glCashReceiptDistributionCredit);
				return getCashReceiptDistributionByReceiptNumber(dtoGLCashReceiptDistribution.getReceiptNumber());
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	/**
	 * @description this service will use to get cash receipt distributions by
	 *              receipt number
	 * @param dtoGLCashReceiptDistribution
	 * @return
	 */
	public DtoGLCashReceiptDistribution getCashReceiptDistributionByReceiptNumber(String receiptNumber) {
		try {
			GLCashReceiptBank glCashReceiptBank = repositoryGLCashReceiptBank.findByReceiptNumber(receiptNumber);
			if (glCashReceiptBank != null) {
				DtoGLCashReceiptDistribution dtoGLCashReceiptDistribution = new DtoGLCashReceiptDistribution(
						glCashReceiptBank);

				Map<String, Object> debitDetail = new HashMap<>();
				String creditAccountNumber = "";
				String creditAccountDescription = "";
				String creditAccountTableRowIndex = "";
				if (glCashReceiptBank.getCheckbookMaintenance() != null
						&& glCashReceiptBank.getCheckbookMaintenance().getGlAccountsTableAccumulation() != null) {

					creditAccountTableRowIndex = glCashReceiptBank.getCheckbookMaintenance()
							.getGlAccountsTableAccumulation().getAccountTableRowIndex();
					Object[] object = serviceAccountPayable.getAccountNumber(
							glCashReceiptBank.getCheckbookMaintenance().getGlAccountsTableAccumulation());
					creditAccountNumber = object[0].toString();
					creditAccountDescription = object[3].toString();
					debitDetail.put(CommonConstant.ACCOUNT_NUMBER, creditAccountNumber);
					debitDetail.put(CommonConstant.ACCOUNT_DESCRIPTION, creditAccountDescription);
					debitDetail.put(CommonConstant.DEBIT_AMOUNT,
							glCashReceiptBank.getReceiptAmount() + glCashReceiptBank.getVatAmount());
					debitDetail.put(CommonConstant.DEBIT_ACCOUNT_TABLE_ROW_INDEX, creditAccountTableRowIndex);
				} else {
					Object[] object = serviceAccountPayable
							.getAccountNumber(glCashReceiptBank.getGlAccountsTableAccumulationDebit());
					creditAccountNumber = object[0].toString();
					creditAccountDescription = object[3].toString();
					debitDetail.put(CommonConstant.ACCOUNT_NUMBER, creditAccountNumber);
					debitDetail.put(CommonConstant.ACCOUNT_DESCRIPTION, creditAccountDescription);
					debitDetail.put(CommonConstant.DEBIT_AMOUNT,
							glCashReceiptBank.getReceiptAmount() + glCashReceiptBank.getVatAmount());
					debitDetail.put(CommonConstant.DEBIT_ACCOUNT_TABLE_ROW_INDEX, creditAccountTableRowIndex);
				}

				dtoGLCashReceiptDistribution.setAccountDetailDebit(debitDetail);

				Map<String, Object> vatCreditDetail = new HashMap<>();
				String vatAccountNumber = "";
				String vatAccountDescription = "";
				String vatAccountTableRowIndex = "";
				if (glCashReceiptBank.getVatSetup() != null) {
					GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
							.findByAccountTableRowIndexAndIsDeleted(
									String.valueOf(glCashReceiptBank.getVatSetup().getAccountRowId()), false);
					vatAccountTableRowIndex = glAccountsTableAccumulation != null
							? glAccountsTableAccumulation.getAccountTableRowIndex()
							: "";
					if (glAccountsTableAccumulation != null) {
						Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
						vatAccountNumber = object[0].toString();
						vatAccountDescription = object[3].toString();
					}
				}

				vatCreditDetail.put(CommonConstant.ACCOUNT_NUMBER, vatAccountNumber);
				vatCreditDetail.put(CommonConstant.ACCOUNT_DESCRIPTION, vatAccountDescription);
				vatCreditDetail.put(CommonConstant.VAT_AMOUNT, glCashReceiptBank.getVatAmount());
				vatCreditDetail.put(CommonConstant.VAT_ACCOUNT_TABLE_ROW_INDEX, vatAccountTableRowIndex);
				dtoGLCashReceiptDistribution.setAccountDetailVat(vatCreditDetail);

				Map<String, Object> creditDetail = new HashMap<>();
				String debitAccountNumber = "";
				String debitAccountDescription = "";
				String debitAccountTableRowIndex = "";
				if (glCashReceiptBank.getGlAccountsTableAccumulation() != null) {
					debitAccountTableRowIndex = glCashReceiptBank.getGlAccountsTableAccumulation()
							.getAccountTableRowIndex();
					Object[] object = serviceAccountPayable
							.getAccountNumber(glCashReceiptBank.getGlAccountsTableAccumulation());
					debitAccountNumber = object[0].toString();
					debitAccountDescription = object[3].toString();
				} else {
					if (glCashReceiptBank.getReceiptType().getTypeId() == ReceiptTypeConstant.AR.getIndex()
							&& glCashReceiptBank.getCustomerMaintenance() != null
							&& glCashReceiptBank.getCustomerMaintenance().getCustomerClassesSetup() != null) {

						CustomerClassesSetup customerClassesSetup = glCashReceiptBank.getCustomerMaintenance()
								.getCustomerClassesSetup();

						CustomerClassAccountTableSetup customerClassAccountTableSetup = repositoryCustomerClassAccountTableSetup
								.findByMasterArAccountTypeTypeIdAndCustomerClassesSetupCustomerClassId(
										MasterArAcoountTypeConstant.ACCOUNT_RECV.getIndex(),
										customerClassesSetup.getCustomerClassId());
						if (customerClassAccountTableSetup != null) {
							debitAccountTableRowIndex = customerClassAccountTableSetup.getGlAccountsTableAccumulation()
									.getAccountTableRowIndex();
							Object[] object = serviceAccountPayable
									.getAccountNumber(customerClassAccountTableSetup.getGlAccountsTableAccumulation());
							debitAccountNumber = object[0].toString();
							debitAccountDescription = object[3].toString();

						}
					} else if (glCashReceiptBank.getReceiptType().getTypeId() == ReceiptTypeConstant.AP.getIndex()
							&& glCashReceiptBank.getVendorMaintenance() != null
							&& glCashReceiptBank.getVendorMaintenance().getVendorClassesSetup() != null) {
						VendorClassesSetup vendorClassesSetup = glCashReceiptBank.getVendorMaintenance()
								.getVendorClassesSetup();

						VendorClassAccountTableSetup vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup
								.findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(
										MasterAPAccountTypeConstant.ACCOUNT_PAYABLE.getIndex(),
										vendorClassesSetup.getVendorClassId());
						if (vendorClassAccountTableSetup != null) {
							debitAccountTableRowIndex = vendorClassAccountTableSetup.getGlAccountsTableAccumulation()
									.getAccountTableRowIndex();
							Object[] object = serviceAccountPayable
									.getAccountNumber(vendorClassAccountTableSetup.getGlAccountsTableAccumulation());
							debitAccountNumber = object[0].toString();
							debitAccountDescription = object[3].toString();

						}
					}
				}

				creditDetail.put(CommonConstant.ACCOUNT_NUMBER, debitAccountNumber);
				creditDetail.put(CommonConstant.ACCOUNT_DESCRIPTION, debitAccountDescription);
				creditDetail.put(CommonConstant.CREDIT_AMOUNT, glCashReceiptBank.getReceiptAmount());
				creditDetail.put(CommonConstant.CREDIT_ACCOUNT_TABLE_ROW_INDEX, debitAccountTableRowIndex);
				dtoGLCashReceiptDistribution.setAccountDetailCredit(creditDetail);
				return dtoGLCashReceiptDistribution;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	public boolean deleteCashReceiptEntryByReceiptNumber(DtoGLCashReceiptBank dtoGLCashReceiptBank) {
		try {
			GLCashReceiptBank glCashReceiptBankDelete = repositoryGLCashReceiptBank
					.findByReceiptNumber(dtoGLCashReceiptBank.getReceiptNumber());
			if (glCashReceiptBankDelete != null) {
				List<GLCashReceiptDistribution> glCashReceiptDistributionList = repositoryGLCashReceiptDistribution
						.findByGlCashReceiptBankBankCashReceipt(glCashReceiptBankDelete.getBankCashReceipt());
				if (glCashReceiptDistributionList != null && !glCashReceiptDistributionList.isEmpty()) {
					repositoryGLCashReceiptDistribution.deleteInBatch(glCashReceiptDistributionList);
				}
				repositoryGLCashReceiptBank.delete(glCashReceiptBankDelete);
			}
			return true;
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}

	public String getUniqueReceiptNumber() {
		String receiptNumber = "";
		GLSerialsConfigurationForTransactions glSerialsConfigurationForTransactions = repositoryGLSerialConfigurationForTransaction
				.findByGlSerialCode(GlSerialConfigurationConstant.CASH_RECEIPT.getVaue());
		if (glSerialsConfigurationForTransactions != null) {
			receiptNumber = glSerialsConfigurationForTransactions.getGlSerialCode() + "-"
					+ glSerialsConfigurationForTransactions.getGlSerialNumber();
		}
		return receiptNumber;
	}

	public boolean checkDistributionDetailIsAvailable(String receiptNumber) {
		DtoGLCashReceiptDistribution dtoGLCashReceiptDistribution = getCashReceiptDistributionByReceiptNumber(
				receiptNumber);
		GLCashReceiptDistribution glCashReceiptDistribution = repositoryGLCashReceiptDistribution
				.findByGlCashReceiptBankReceiptNumberAndAccountType(receiptNumber,
						CashReceiptBankAccountTypeConstant.CASH.getIndex());
		if (glCashReceiptDistribution != null) {
			if (!dtoGLCashReceiptDistribution.getAccountDetailDebit().isEmpty()) {
				String debitAccountTableRowIndex = dtoGLCashReceiptDistribution.getAccountDetailDebit()
						.get(CommonConstant.DEBIT_ACCOUNT_TABLE_ROW_INDEX).toString();
				String debitAmount = dtoGLCashReceiptDistribution.getAccountDetailDebit()
						.get(CommonConstant.DEBIT_AMOUNT).toString();
				if (glCashReceiptDistribution.getGlAccountsTableAccumulation() != null) {
					if (debitAccountTableRowIndex != glCashReceiptDistribution.getGlAccountsTableAccumulation()
							.getAccountTableRowIndex()) {
						return false;
					} else if (glCashReceiptDistribution.getOriginalDebitAmount() != Double.valueOf(debitAmount)) {
						return false;
					}
				}
			}
		} else {
			return false;
		}

		GLCashReceiptDistribution glCashReceiptDistributionVat = repositoryGLCashReceiptDistribution
				.findByGlCashReceiptBankReceiptNumberAndAccountType(receiptNumber,
						CashReceiptBankAccountTypeConstant.TAX.getIndex());
		if (glCashReceiptDistributionVat != null) {
			if (!dtoGLCashReceiptDistribution.getAccountDetailVat().isEmpty()) {
				String vatAccountTableRowIndex = dtoGLCashReceiptDistribution.getAccountDetailVat()
						.get(CommonConstant.VAT_ACCOUNT_TABLE_ROW_INDEX).toString();
				String vatAmount = dtoGLCashReceiptDistribution.getAccountDetailVat().get(CommonConstant.VAT_AMOUNT)
						.toString();
				if (glCashReceiptDistributionVat.getGlAccountsTableAccumulation() != null) {
					if (vatAccountTableRowIndex != glCashReceiptDistributionVat.getGlAccountsTableAccumulation()
							.getAccountTableRowIndex()) {
						return false;
					} else if (glCashReceiptDistributionVat.getOriginalCreditAmount() != Double.valueOf(vatAmount)) {
						return false;
					}
				}
			}

		} else {
			return false;
		}
		GLCashReceiptDistribution glCashReceiptDistributionOther = repositoryGLCashReceiptDistribution
				.findByGlCashReceiptBankReceiptNumberAndAccountType(receiptNumber,
						CashReceiptBankAccountTypeConstant.OTHER.getIndex());
		if (glCashReceiptDistributionOther != null) {
			if (!dtoGLCashReceiptDistribution.getAccountDetailCredit().isEmpty()) {
				String creditAccountTableRowIndex = dtoGLCashReceiptDistribution.getAccountDetailCredit()
						.get(CommonConstant.CREDIT_ACCOUNT_TABLE_ROW_INDEX).toString();
				String creditAmount = dtoGLCashReceiptDistribution.getAccountDetailCredit()
						.get(CommonConstant.CREDIT_AMOUNT).toString();
				if (glCashReceiptDistributionOther.getGlAccountsTableAccumulation() != null) {
					if (creditAccountTableRowIndex != glCashReceiptDistributionOther.getGlAccountsTableAccumulation()
							.getAccountTableRowIndex()) {
						return false;
					} else if (glCashReceiptDistributionOther.getOriginalCreditAmount() != Double
							.valueOf(creditAmount)) {
						return false;
					}
				}
			}
		} else {
			return false;
		}
		return true;
	}

}
