/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.COAFinancialDimensions;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.dto.DtoFinancialDimensionSetUp;
import com.bti.model.dto.DtoFinancialDimensionValue;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCOAFinancialDimensions;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.util.UtilRandomKey;

/**
 * Description: Service Authentication
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service
public class ServiceFinancialDimension {

	static Logger log = Logger.getLogger(ServiceFinancialDimension.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryCOAFinancialDimensions repositoryCOAFinancialDimensions;
	
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	

	/**
	 * @param dtoFinancialDimensionSetUp
	 * @return
	 */
	public DtoFinancialDimensionSetUp saveFinancialDimensionSetup(
			DtoFinancialDimensionSetUp dtoFinancialDimensionSetUp) {
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		COAFinancialDimensions coaFinancialDimensions = new COAFinancialDimensions();
		coaFinancialDimensions.setCreatedBy(loggedInUserId);
		coaFinancialDimensions.setDimensioncolumnname(dtoFinancialDimensionSetUp.getDimensionName());
		coaFinancialDimensions.setDimensionMask(dtoFinancialDimensionSetUp.getDimensionMask());
		coaFinancialDimensions = repositoryCOAFinancialDimensions.saveAndFlush(coaFinancialDimensions);
		dtoFinancialDimensionSetUp.setDimInxd(coaFinancialDimensions.getDimInxd());

		return dtoFinancialDimensionSetUp;
	}

	/**
	 * @param dimInxd
	 * @return
	 */
	public DtoFinancialDimensionSetUp financialDimensionSetupGetById(int dimInxd) {

		COAFinancialDimensions coaFinancialDimensions = repositoryCOAFinancialDimensions.findByDimInxdAndIsDeleted(dimInxd,false);
		DtoFinancialDimensionSetUp dtoFinancialDimensionSetUp = null;
		if (coaFinancialDimensions != null) {
			dtoFinancialDimensionSetUp = new DtoFinancialDimensionSetUp(coaFinancialDimensions);
		}

		return dtoFinancialDimensionSetUp;
	}

	/**
	 * @param dtoFinancialDimensionSetUp
	 * @param coaFinancialDimensions
	 * @return
	 */
	public DtoFinancialDimensionSetUp financialDimensionSetupUpdate(
			DtoFinancialDimensionSetUp dtoFinancialDimensionSetUp, COAFinancialDimensions coaFinancialDimensions) {
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}

		if (coaFinancialDimensions != null) {
			coaFinancialDimensions.setChangeBy(loggedInUserId);
			coaFinancialDimensions.setDimensioncolumnname(dtoFinancialDimensionSetUp.getDimensionName());
			coaFinancialDimensions.setDimensionMask(dtoFinancialDimensionSetUp.getDimensionMask());
			coaFinancialDimensions = repositoryCOAFinancialDimensions.saveAndFlush(coaFinancialDimensions);
			dtoFinancialDimensionSetUp.setDimInxd(coaFinancialDimensions.getDimInxd());
		}

		return dtoFinancialDimensionSetUp;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchFinancialDimensionSetup(DtoSearch dtoSearchs) {

		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositoryCOAFinancialDimensions
				.predictiveCOAFinancialDimensionsSetupSearchCount(dtoSearchs.getSearchKeyword()));
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);

		DtoFinancialDimensionSetUp dtoFinancialDimensionSetUp = null;
		List<COAFinancialDimensions> coaFinancialDimensionList = null;
		List<DtoFinancialDimensionSetUp> dtoFinancialDimensionSetUpList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			coaFinancialDimensionList = repositoryCOAFinancialDimensions
					.predictiveCOAFinancialDimensionsSetupSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			coaFinancialDimensionList = repositoryCOAFinancialDimensions
					.predictiveCOAFinancialDimensionsSetupSearch(dtoSearchs.getSearchKeyword());
		}

		if (coaFinancialDimensionList != null) {
			for (COAFinancialDimensions coaFinancialDimensions : coaFinancialDimensionList) {
				dtoFinancialDimensionSetUp = new DtoFinancialDimensionSetUp(coaFinancialDimensions, langId);
				dtoFinancialDimensionSetUpList.add(dtoFinancialDimensionSetUp);
			}
		}
		dtoSearch.setRecords(dtoFinancialDimensionSetUpList);
		return dtoSearch;
	}

	/**
	 * @param dtoFinancialDimensionValue
	 * @param coaFinancialDimensions
	 * @return
	 */
	public DtoFinancialDimensionValue saveFinancialDimensionValue(DtoFinancialDimensionValue dtoFinancialDimensionValue,
			COAFinancialDimensions coaFinancialDimensions) {
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		COAFinancialDimensionsValues coaFinancialDimensionsValues = new COAFinancialDimensionsValues();
		coaFinancialDimensionsValues.setCreatedBy(loggedInUserId);
		coaFinancialDimensionsValues.setDimensionValue(dtoFinancialDimensionValue.getDimensionValue());
		coaFinancialDimensionsValues.setDimensionDescription(dtoFinancialDimensionValue.getDimensionDescription());
		if (dtoFinancialDimensionValue.getDimensionDescriptionArabic() != null) {
			coaFinancialDimensionsValues
					.setDimensionDescriptionArabic(dtoFinancialDimensionValue.getDimensionDescriptionArabic());
		}
		coaFinancialDimensionsValues.setCoaFinancialDimensions(coaFinancialDimensions);
		coaFinancialDimensionsValues = repositoryCOAFinancialDimensionsValues
				.saveAndFlush(coaFinancialDimensionsValues);
		dtoFinancialDimensionValue.setDimInxValue(coaFinancialDimensionsValues.getDimInxValue());

		return dtoFinancialDimensionValue;
	}

	/**
	 * @param dimInxValue
	 * @return
	 */
	public DtoFinancialDimensionValue financialDimensionValueGetById(Long dimInxValue) 
	{
		COAFinancialDimensionsValues coaFinancialDimensionsValues = repositoryCOAFinancialDimensionsValues
				.findByDimInxValueAndIsDeleted(dimInxValue,false);
		DtoFinancialDimensionValue dtoFinancialDimensionValue = null;
		if (coaFinancialDimensionsValues != null) {
			dtoFinancialDimensionValue = new DtoFinancialDimensionValue(coaFinancialDimensionsValues);
		}
		return dtoFinancialDimensionValue;
	}

	/**
	 * @param dtoFinancialDimensionValue
	 * @param coaFinancialDimensions
	 * @param coaFinancialDimensionsValues
	 * @return
	 */
	public DtoFinancialDimensionValue financialDimensionValueUpdate(
			DtoFinancialDimensionValue dtoFinancialDimensionValue, COAFinancialDimensions coaFinancialDimensions,
			COAFinancialDimensionsValues coaFinancialDimensionsValues) 
	{
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		if (coaFinancialDimensionsValues != null) {
			coaFinancialDimensionsValues.setChangeBy(loggedInUserId);
			coaFinancialDimensionsValues.setDimensionValue(dtoFinancialDimensionValue.getDimensionValue());
			coaFinancialDimensionsValues.setDimensionDescription(dtoFinancialDimensionValue.getDimensionDescription());
			if (dtoFinancialDimensionValue.getDimensionDescriptionArabic() != null) {
				coaFinancialDimensionsValues
						.setDimensionDescriptionArabic(dtoFinancialDimensionValue.getDimensionDescriptionArabic());
			}
			coaFinancialDimensionsValues.setCoaFinancialDimensions(coaFinancialDimensions);
			repositoryCOAFinancialDimensionsValues
					.saveAndFlush(coaFinancialDimensionsValues);
		}
		return dtoFinancialDimensionValue;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchFinancialDimensionValue(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();

		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositoryCOAFinancialDimensionsValues
				.predictiveCOAFinancialDimensionsValueSearchCount(dtoSearchs.getSearchKeyword()));
		DtoFinancialDimensionValue dtoFinancialDimensionValue = null;
		List<COAFinancialDimensionsValues> coaFinancialDimensionsValueList = null;
		List<DtoFinancialDimensionValue> dtoFinancialDimensionValueList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			coaFinancialDimensionsValueList = repositoryCOAFinancialDimensionsValues
					.predictiveCOAFinancialDimensionsValueSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			coaFinancialDimensionsValueList = repositoryCOAFinancialDimensionsValues
					.predictiveCOAFinancialDimensionsValueSearch(dtoSearchs.getSearchKeyword());
		}

		if (coaFinancialDimensionsValueList != null) {
			for (COAFinancialDimensionsValues coaFinancialDimensionsValues : coaFinancialDimensionsValueList) {
				dtoFinancialDimensionValue = new DtoFinancialDimensionValue(coaFinancialDimensionsValues);
				dtoFinancialDimensionValueList.add(dtoFinancialDimensionValue);
			}
		}
		dtoSearch.setRecords(dtoFinancialDimensionValueList);
		return dtoSearch;
	}

}