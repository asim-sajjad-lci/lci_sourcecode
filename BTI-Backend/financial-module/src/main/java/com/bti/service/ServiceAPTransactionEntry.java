/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.APDistributionTypeConstant;
import com.bti.constant.APTransactionTypeConstant;
import com.bti.constant.ARDistributionType;
import com.bti.constant.AuditTrailSeriesTypeConstant;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.GlSerialConfigurationConstant;
import com.bti.constant.MasterAPAccountTypeConstant;
import com.bti.constant.MessageLabel;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.APBalanceByPeriods;
import com.bti.model.APSummaryBalances;
import com.bti.model.APTransactionDistribution;
import com.bti.model.APTransactionEntry;
import com.bti.model.APYTDOpenPayments;
import com.bti.model.APYTDOpenPaymentsDistribution;
import com.bti.model.APYTDOpenTransaction;
import com.bti.model.APYTDOpenTransactionDistribution;
import com.bti.model.ARTransactionEntryDistribution;
import com.bti.model.BatchTransactionType;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLBatches;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.PMDocumentsTypeSetup;
import com.bti.model.PMSetup;
import com.bti.model.VATSetup;
import com.bti.model.VendorClassAccountTableSetup;
import com.bti.model.VendorClassesSetup;
import com.bti.model.VendorMaintenance;
import com.bti.model.dto.DtoAPCashReceiptDistribution;
import com.bti.model.dto.DtoAPDistributionDetail;
import com.bti.model.dto.DtoAPTransactionEntry;
import com.bti.model.dto.DtoARDistributionDetail;
import com.bti.model.dto.DtoARTransactionEntry;
import com.bti.model.dto.DtoStatusType;
import com.bti.model.dto.DtoVendor;
import com.bti.model.dto.DtoVendorClassSetup;
import com.bti.repository.RepositoryAPBalanceByPeriods;
import com.bti.repository.RepositoryAPBatches;
import com.bti.repository.RepositoryAPSummaryBalance;
import com.bti.repository.RepositoryAPTransactionDistribution;
import com.bti.repository.RepositoryAPTransactionEntry;
import com.bti.repository.RepositoryAPYTDOpenPayments;
import com.bti.repository.RepositoryAPYTDOpenPaymentsDistribution;
import com.bti.repository.RepositoryAPYTDOpenTransaction;
import com.bti.repository.RepositoryAPYTDOpenTransactionDistribution;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.repository.RepositoryMasterAPTransactionStatus;
import com.bti.repository.RepositoryPMDocumentType;
import com.bti.repository.RepositoryPMSetup;
import com.bti.repository.RepositoryVATSetup;
import com.bti.repository.RepositoryVendorClassAccountTableSetup;
import com.bti.repository.RepositoryVendorMaintenance;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceapTransactionEntry Name of Project: BTI Created on: Dec
 * 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceAPTransactionEntry")
public class ServiceAPTransactionEntry {

	private static final Logger LOG = Logger.getLogger(ServiceAPTransactionEntry.class);
	
	private static final String USER_ID = "userid";

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryAPTransactionEntry repositoryAPTransactionEntry;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;

	@Autowired
	RepositoryAPBatches repositoryAPBatches;

	@Autowired
	RepositoryMasterAPTransactionStatus repositoryMasterAPTransactionStatus;

	@Autowired
	RepositoryPMDocumentType repositoryPMDocumentType;

	@Autowired
	RepositoryVendorMaintenance repositoryVendorMaintenance;
	
	@Autowired
	RepositoryPMSetup repositoryPMSetup;
	
	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;
	
	@Autowired
	RepositoryVendorClassAccountTableSetup repositoryVendorClassAccountTableSetup;
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired
	RepositoryVATSetup repositoryVATSetup;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryAPTransactionDistribution repositoryAPTransactionDistribution;
	
	@Autowired
	RepositoryAPSummaryBalance repositoryAPSummaryBalance;
	
	@Autowired
	RepositoryAPBalanceByPeriods repositoryAPBalanceByPeriods;
	
	@Autowired
	RepositoryAPYTDOpenTransaction repositoryAPYTDOpenTransaction;
	
	@Autowired
	RepositoryAPYTDOpenTransactionDistribution repositoryAPYTDOpenTransactionDistribution;
	
	@Autowired
	RepositoryAPYTDOpenPayments repositoryAPYTDOpenPayments;
	
	@Autowired
	RepositoryAPYTDOpenPaymentsDistribution repositoryAPYTDOpenPaymentsDistribution;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;
	
	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;
	
	public DtoAPTransactionEntry saveTransactionEntry(DtoAPTransactionEntry dtoAPTransactionEntry) {
		int langId = serviceHome.getLanngugaeId();

		try {
			APTransactionEntry apTransactionEntry = new APTransactionEntry();
			PMDocumentsTypeSetup pmDocumentsTypeSetup = repositoryPMDocumentType
					.findByApDocumentTypeIdAndIsDeleted(dtoAPTransactionEntry.getApTransactionType(), false);
			String transactionNumber = getTransactionNumber(dtoAPTransactionEntry.getApTransactionType());
			apTransactionEntry.setApTransactionNumber(transactionNumber);
			apTransactionEntry.setTransactionType(pmDocumentsTypeSetup);
			apTransactionEntry.setApTransactionDescription(dtoAPTransactionEntry.getApTransactionDescription());
			apTransactionEntry.setApTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoAPTransactionEntry.getApTransactionDate()));
			apTransactionEntry.setApBatches(repositoryAPBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(
					dtoAPTransactionEntry.getBatchID(),
					BatchTransactionTypeConstant.AP_TRANSACTION_ENTRY.getIndex(), false));

			apTransactionEntry.setVendorID(dtoAPTransactionEntry.getVendorID());
			apTransactionEntry.setVendorName(dtoAPTransactionEntry.getVendorName());
			apTransactionEntry.setVendorNameArabic(dtoAPTransactionEntry.getVendorNameArabic());
			apTransactionEntry.setCurrencyID(dtoAPTransactionEntry.getCurrencyID());
			apTransactionEntry.setDocumentNumberOfVendor(dtoAPTransactionEntry.getDocumentNumberOfVendor());
			apTransactionEntry.setPurchasesAmount(dtoAPTransactionEntry.getPurchasesAmount());

			// Set Other Amounts
			apTransactionEntry
					.setApTransactionTradeDiscount(dtoAPTransactionEntry.getApTransactionTradeDiscount() != null
							? dtoAPTransactionEntry.getApTransactionTradeDiscount() : 0.0);
			apTransactionEntry
					.setApTransactionFreightAmount(dtoAPTransactionEntry.getApTransactionFreightAmount() != null
							? dtoAPTransactionEntry.getApTransactionFreightAmount() : 0.0);
			apTransactionEntry
					.setApTransactionMiscellaneous(dtoAPTransactionEntry.getApTransactionMiscellaneous() != null
							? dtoAPTransactionEntry.getApTransactionMiscellaneous() : 0.0);
			apTransactionEntry.setApTransactionVATAmount(dtoAPTransactionEntry.getApTransactionVATAmount() != null
					? dtoAPTransactionEntry.getApTransactionVATAmount() : 0.0);
			apTransactionEntry.setApTransactionFinanceChargeAmount(
					dtoAPTransactionEntry.getApTransactionFinanceChargeAmount() != null
							? dtoAPTransactionEntry.getApTransactionFinanceChargeAmount() : 0.0);
			apTransactionEntry
					.setApTransactionCreditMemoAmount(dtoAPTransactionEntry.getApTransactionCreditMemoAmount() != null
							? dtoAPTransactionEntry.getApTransactionCreditMemoAmount() : 0.0);
			apTransactionEntry.setApTransactionTotalAmount(dtoAPTransactionEntry.getApTransactionTotalAmount() != null
					? dtoAPTransactionEntry.getApTransactionTotalAmount() : 0.0);
			apTransactionEntry.setApTransactionCashAmount(dtoAPTransactionEntry.getApTransactionCashAmount() != null
					? dtoAPTransactionEntry.getApTransactionCashAmount() : 0.0);
			apTransactionEntry.setApTransactionCheckAmount(dtoAPTransactionEntry.getApTransactionCheckAmount() != null
					? dtoAPTransactionEntry.getApTransactionCheckAmount() : 0.0);
			apTransactionEntry
					.setApTransactionCreditCardAmount(dtoAPTransactionEntry.getApTransactionCreditCardAmount() != null
							? dtoAPTransactionEntry.getApTransactionCreditCardAmount() : 0.0);
			apTransactionEntry.setShippingMethod(dtoAPTransactionEntry.getShippingMethodId());
			apTransactionEntry.setApTransactionDebitMemoAmount(dtoAPTransactionEntry.getApTransactionDebitMemoAmount()!=null?
					dtoAPTransactionEntry.getApTransactionDebitMemoAmount():0.0);
			apTransactionEntry.setApServiceRepairAmount(dtoAPTransactionEntry.getApServiceRepairAmount()!=null?
					dtoAPTransactionEntry.getApServiceRepairAmount():0.0);
			apTransactionEntry.setApReturnAmount(dtoAPTransactionEntry.getApReturnAmount()!=null?
					dtoAPTransactionEntry.getApReturnAmount():0.0);
			apTransactionEntry.setApTransactionWarrantyAmount(dtoAPTransactionEntry.getApTransactionWarrantyAmount()!=null?
					dtoAPTransactionEntry.getApTransactionWarrantyAmount():0.0);
			// Set Transaction Amount
			apTransactionEntry.setCheckbookID(dtoAPTransactionEntry.getCheckbookID());
			apTransactionEntry.setCashReceiptNumber(dtoAPTransactionEntry.getCashReceiptNumber());
			if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCheckNumber())) {
				apTransactionEntry.setCheckNumber(dtoAPTransactionEntry.getCheckNumber());
			}

			if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCreditCardID())) {
				apTransactionEntry.setCreditCardID(dtoAPTransactionEntry.getCreditCardID());
				apTransactionEntry.setCreditCardNumber(dtoAPTransactionEntry.getCreditCardNumber());
				apTransactionEntry.setCreditCardExpireMonth(dtoAPTransactionEntry.getCreditCardExpireMonth());
				apTransactionEntry.setCreditCardExpireYear(dtoAPTransactionEntry.getCreditCardExpireYear());
			}
			
			apTransactionEntry.setApTransactionStatus(repositoryMasterAPTransactionStatus
					.findByStatusIdAndLanguageLanguageId(2, langId));// 2= UnPost
			
			apTransactionEntry.setPayment(false);
			if(dtoAPTransactionEntry.getIsPayment())
			{
				String paymentNumber = getTransactionNumber(dtoAPTransactionEntry.getPaymentTransactionTypeId());
				apTransactionEntry.setCashReceiptNumber(paymentNumber);
				apTransactionEntry.setPayment(true);
				PMDocumentsTypeSetup pmDocumentsTypeSetupPayment = repositoryPMDocumentType
						.findByApDocumentTypeIdAndIsDeleted(dtoAPTransactionEntry.getPaymentTransactionTypeId(), false);
				if (pmDocumentsTypeSetupPayment != null) {
					pmDocumentsTypeSetupPayment.setDocumentNumberLastNumber(pmDocumentsTypeSetupPayment.getDocumentNumberLastNumber() + 1);
					repositoryPMDocumentType.saveAndFlush(pmDocumentsTypeSetupPayment);
				}
			}
			else
			{
				apTransactionEntry.setCashReceiptNumber(transactionNumber);
			}
			
			apTransactionEntry.setPaymentTypeId(String.valueOf(dtoAPTransactionEntry.getPaymentTypeId()));
			apTransactionEntry.setPaymentAmount(0);
			if(UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getPaymentAmount())){
				apTransactionEntry.setPaymentAmount(Double.valueOf(dtoAPTransactionEntry.getPaymentAmount()));
			}
			
			if (dtoAPTransactionEntry.getExchangeTableIndex() != null) {
				apTransactionEntry.setExchangeTableIndex(dtoAPTransactionEntry.getExchangeTableIndex());
				apTransactionEntry.setExchangeTableRate(dtoAPTransactionEntry.getExchangeTableRate());
			}
			apTransactionEntry.setTransactionVoid(dtoAPTransactionEntry.isTransactionVoid());
			apTransactionEntry.setPaymentTermsID(dtoAPTransactionEntry.getPaymentTermsID());
			apTransactionEntry.setRowDateIndex(new Date());
			apTransactionEntry.setVatScheduleID(dtoAPTransactionEntry.getVatScheduleID());

			repositoryAPTransactionEntry.saveAndFlush(apTransactionEntry);

			if (pmDocumentsTypeSetup != null) {
				pmDocumentsTypeSetup
						.setDocumentNumberLastNumber(pmDocumentsTypeSetup.getDocumentNumberLastNumber() + 1);
				repositoryPMDocumentType.saveAndFlush(pmDocumentsTypeSetup);
			}
			dtoAPTransactionEntry.setApTransactionNumber(transactionNumber);
			return getTransactionEntryByTransactionNumber(dtoAPTransactionEntry);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoAPTransactionEntry updateTransactionEntry(DtoAPTransactionEntry dtoAPTransactionEntry) {
		int langId = serviceHome.getLanngugaeId();
		try {
			
			APTransactionEntry apTransactionEntry = repositoryAPTransactionEntry
					.findByApTransactionNumber(dtoAPTransactionEntry.getApTransactionNumber());
			if (apTransactionEntry == null) {
				return null;
			}

			PMDocumentsTypeSetup pmDocumentsTypeSetup = repositoryPMDocumentType
					.findByApDocumentTypeIdAndIsDeleted(dtoAPTransactionEntry.getApTransactionType(), false);

			apTransactionEntry.setTransactionType(pmDocumentsTypeSetup);
			apTransactionEntry.setApTransactionDescription(dtoAPTransactionEntry.getApTransactionDescription());
			apTransactionEntry.setApTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoAPTransactionEntry.getApTransactionDate()));
			apTransactionEntry.setApBatches(repositoryAPBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(
					dtoAPTransactionEntry.getBatchID(),
					BatchTransactionTypeConstant.AP_TRANSACTION_ENTRY.getIndex(), false));
			apTransactionEntry.setVendorID(dtoAPTransactionEntry.getVendorID());
			apTransactionEntry.setVendorName(dtoAPTransactionEntry.getVendorName());
			apTransactionEntry.setVendorNameArabic(dtoAPTransactionEntry.getVendorNameArabic());
			apTransactionEntry.setCurrencyID(dtoAPTransactionEntry.getCurrencyID());
			apTransactionEntry.setDocumentNumberOfVendor(dtoAPTransactionEntry.getDocumentNumberOfVendor());
			apTransactionEntry.setPurchasesAmount(dtoAPTransactionEntry.getPurchasesAmount());

			// Set Other Amounts
			apTransactionEntry
					.setApTransactionTradeDiscount(dtoAPTransactionEntry.getApTransactionTradeDiscount() != null
							? dtoAPTransactionEntry.getApTransactionTradeDiscount() : 0.0);
			apTransactionEntry
					.setApTransactionFreightAmount(dtoAPTransactionEntry.getApTransactionFreightAmount() != null
							? dtoAPTransactionEntry.getApTransactionFreightAmount() : 0.0);
			apTransactionEntry
					.setApTransactionMiscellaneous(dtoAPTransactionEntry.getApTransactionMiscellaneous() != null
							? dtoAPTransactionEntry.getApTransactionMiscellaneous() : 0.0);
			apTransactionEntry.setApTransactionVATAmount(dtoAPTransactionEntry.getApTransactionVATAmount() != null
					? dtoAPTransactionEntry.getApTransactionVATAmount() : 0.0);
			apTransactionEntry.setApTransactionFinanceChargeAmount(
					dtoAPTransactionEntry.getApTransactionFinanceChargeAmount() != null
							? dtoAPTransactionEntry.getApTransactionFinanceChargeAmount() : 0.0);
			apTransactionEntry
					.setApTransactionCreditMemoAmount(dtoAPTransactionEntry.getApTransactionCreditMemoAmount() != null
							? dtoAPTransactionEntry.getApTransactionCreditMemoAmount() : 0.0);
			apTransactionEntry.setApTransactionTotalAmount(dtoAPTransactionEntry.getApTransactionTotalAmount() != null
					? dtoAPTransactionEntry.getApTransactionTotalAmount() : 0.0);
			apTransactionEntry.setApTransactionCashAmount(dtoAPTransactionEntry.getApTransactionCashAmount() != null
					? dtoAPTransactionEntry.getApTransactionCashAmount() : 0.0);
			apTransactionEntry.setApTransactionCheckAmount(dtoAPTransactionEntry.getApTransactionCheckAmount() != null
					? dtoAPTransactionEntry.getApTransactionCheckAmount() : 0.0);
			apTransactionEntry
					.setApTransactionCreditCardAmount(dtoAPTransactionEntry.getApTransactionCreditCardAmount() != null
							? dtoAPTransactionEntry.getApTransactionCreditCardAmount() : 0.0);
			apTransactionEntry.setApTransactionDebitMemoAmount(dtoAPTransactionEntry.getApTransactionDebitMemoAmount()!=null?
					dtoAPTransactionEntry.getApTransactionDebitMemoAmount():0.0);
			apTransactionEntry.setApServiceRepairAmount(dtoAPTransactionEntry.getApServiceRepairAmount()!=null?
					dtoAPTransactionEntry.getApServiceRepairAmount():0.0);
			apTransactionEntry.setApReturnAmount(dtoAPTransactionEntry.getApReturnAmount()!=null?
					dtoAPTransactionEntry.getApReturnAmount():0.0);
			apTransactionEntry.setApTransactionWarrantyAmount(dtoAPTransactionEntry.getApTransactionWarrantyAmount()!=null?
					dtoAPTransactionEntry.getApTransactionWarrantyAmount():0.0);
			apTransactionEntry.setShippingMethod(dtoAPTransactionEntry.getShippingMethodId());
			if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCheckbookID())) {
				apTransactionEntry.setCheckbookID(dtoAPTransactionEntry.getCheckbookID());
				apTransactionEntry.setCheckNumber(dtoAPTransactionEntry.getCheckNumber());
			} else {
				apTransactionEntry.setCheckbookID(null);
				apTransactionEntry.setCheckNumber(null);
			}

			if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCreditCardID())) {
				apTransactionEntry.setCreditCardID(dtoAPTransactionEntry.getCreditCardID());
				apTransactionEntry.setCreditCardNumber(dtoAPTransactionEntry.getCreditCardNumber());
				apTransactionEntry.setCreditCardExpireMonth(dtoAPTransactionEntry.getCreditCardExpireMonth());
				apTransactionEntry.setCreditCardExpireYear(dtoAPTransactionEntry.getCreditCardExpireYear());
			} else {
				apTransactionEntry.setCreditCardID(null);
				apTransactionEntry.setCreditCardNumber(null);
				apTransactionEntry.setCreditCardExpireMonth(null);
				apTransactionEntry.setCreditCardExpireYear(null);
			}

			// Set Transaction Amount
			apTransactionEntry.setCashReceiptNumber(dtoAPTransactionEntry.getCashReceiptNumber());
			apTransactionEntry.setApTransactionStatus(repositoryMasterAPTransactionStatus
					.findByStatusIdAndLanguageLanguageId(dtoAPTransactionEntry.getApTransactionStatus(), langId));
			if (dtoAPTransactionEntry.getExchangeTableIndex() != null) {
				apTransactionEntry.setExchangeTableIndex(dtoAPTransactionEntry.getExchangeTableIndex());
				apTransactionEntry.setExchangeTableRate(dtoAPTransactionEntry.getExchangeTableRate());
			}
			
			apTransactionEntry.setPayment(dtoAPTransactionEntry.getIsPayment());
			apTransactionEntry.setPaymentAmount(0);
			if(UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getPaymentAmount())){
				apTransactionEntry.setPaymentAmount(Double.valueOf(dtoAPTransactionEntry.getPaymentAmount()));
			}
			if(dtoAPTransactionEntry.getPaymentTypeId()!=null){
				apTransactionEntry.setPaymentTypeId(String.valueOf(dtoAPTransactionEntry.getPaymentTypeId()));
			}
			
			apTransactionEntry.setTransactionVoid(dtoAPTransactionEntry.isTransactionVoid());
			apTransactionEntry.setPaymentTermsID(dtoAPTransactionEntry.getPaymentTermsID());
			apTransactionEntry.setRowDateIndex(new Date());
			apTransactionEntry.setVatScheduleID(dtoAPTransactionEntry.getVatScheduleID());
			repositoryAPTransactionEntry.saveAndFlush(apTransactionEntry);
			return getTransactionEntryByTransactionNumber(dtoAPTransactionEntry);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoAPTransactionEntry getTransactionEntryByTransactionNumber(DtoAPTransactionEntry dtoAPTransactionEntry) {
		try {
			APTransactionEntry apTransactionEntry = repositoryAPTransactionEntry
					.findByApTransactionNumber(dtoAPTransactionEntry.getApTransactionNumber());
			if (apTransactionEntry != null) {
				return new DtoAPTransactionEntry(apTransactionEntry);
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public List<String> getTransactionNumberByTransactionType(int transactionTypeId) {
		List<String> transactionNumberList = new ArrayList<>();
		try {
			List<APTransactionEntry> apTransactionEntryList = repositoryAPTransactionEntry
					.findByTransactionTypeApDocumentTypeId(transactionTypeId);
			if (apTransactionEntryList != null && !apTransactionEntryList.isEmpty()) {
				apTransactionEntryList.forEach(
						apTransactionEntry -> transactionNumberList.add(apTransactionEntry.getApTransactionNumber()));
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return transactionNumberList;
	}

	public List<DtoStatusType> getTransactionType() {
		List<DtoStatusType> list = new ArrayList<>();
		try {
			List<PMDocumentsTypeSetup> apTransactionTypeList = repositoryPMDocumentType.findByIsDeleted(false);
			if (apTransactionTypeList != null && !apTransactionTypeList.isEmpty()) {
				apTransactionTypeList.forEach(apTransactionType -> list.add(new DtoStatusType(apTransactionType)));
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}

	public String getTransactionNumber(int transactionType) {
		String transactionNumber = "";
		PMDocumentsTypeSetup pmDocumentsTypeSetup = repositoryPMDocumentType
				.findByApDocumentTypeIdAndIsDeleted(transactionType, false);
		if (pmDocumentsTypeSetup != null) {
			transactionNumber = pmDocumentsTypeSetup.getDocumentCode() + "-"
					+ pmDocumentsTypeSetup.getDocumentNumberLastNumber();
		}
		return transactionNumber;
	}

	public DtoVendorClassSetup getVendorClassSetupByClassId(DtoVendor dtoVendor) {

		DtoVendorClassSetup dtoVendorClassSetup = new DtoVendorClassSetup();
		dtoVendorClassSetup.setVatScheduleId("");
		dtoVendorClassSetup.setCurrencyId("");
		dtoVendorClassSetup.setPaymentTermId("");
		dtoVendorClassSetup.setCheckBookId("");
		dtoVendorClassSetup.setVendorId("");
		dtoVendorClassSetup.setVendorName("");
		dtoVendorClassSetup.setShipmentMethodId("");
		dtoVendorClassSetup.setTradeDiscountPercent(0.0);
		dtoVendorClassSetup.setMiscVatPercent(0.0);
		dtoVendorClassSetup.setFreightVatPercent(0.0);
		dtoVendorClassSetup.setVatPercent(0.0);
		
		VendorMaintenance vendorMaintenance = repositoryVendorMaintenance
				.findByVendoridAndIsDeleted(dtoVendor.getVenderId(), false);
		try {
			if (vendorMaintenance != null) {
				dtoVendorClassSetup.setVendorName(
						vendorMaintenance.getVendorName() != null ? vendorMaintenance.getVendorName() : "");
				dtoVendorClassSetup.setVendorId(vendorMaintenance.getVendorid());
				if (vendorMaintenance.getVendorClassesSetup() != null) {
					VendorClassesSetup vendorClassesSetup = vendorMaintenance.getVendorClassesSetup();
					 
					dtoVendorClassSetup.setShipmentMethodId(vendorClassesSetup.getShipmentMethodSetup() != null
							? vendorClassesSetup.getShipmentMethodSetup().getShipmentMethodId() : "");
					if(vendorClassesSetup.getVatSetup()!=null){
						dtoVendorClassSetup.setVatScheduleId(vendorClassesSetup.getVatSetup().getVatScheduleId());
						dtoVendorClassSetup.setVatPercent(vendorClassesSetup.getVatSetup().getBasperct());
					}
					
					dtoVendorClassSetup.setCurrencyId(vendorClassesSetup.getCurrencySetup() != null
							? vendorClassesSetup.getCurrencySetup().getCurrencyId() : "");
					dtoVendorClassSetup.setPaymentTermId(vendorClassesSetup.getPaymentTermsSetup() != null
							? vendorClassesSetup.getPaymentTermsSetup().getPaymentTermId() : "");
					dtoVendorClassSetup.setCheckBookId(vendorClassesSetup.getCheckbookMaintenance() != null
							? vendorClassesSetup.getCheckbookMaintenance().getCheckBookId() : "");
					dtoVendorClassSetup.setTradeDiscountPercent(vendorClassesSetup.getTradeDiscountPercent());
				}
			}

			List<PMSetup> pmSetupList = repositoryPMSetup.findAll();
			if (pmSetupList != null && !pmSetupList.isEmpty()) {
				PMSetup pmSetup = pmSetupList.get(0);
				dtoVendorClassSetup.setMiscVatScheduleId(pmSetup.getMiscVatScheduleId().getVatScheduleId());
				dtoVendorClassSetup.setMiscVatPercent(pmSetup.getMiscVatScheduleId().getBasperct());
				
				dtoVendorClassSetup.setFreightVatScheduleId(pmSetup.getFreightVatScheduleId().getVatScheduleId());
				dtoVendorClassSetup.setFreightVatPercent(pmSetup.getFreightVatScheduleId().getBasperct());
				
				if (UtilRandomKey.isNotBlank(dtoVendorClassSetup.getVatId())) {
					dtoVendorClassSetup.setVatId(pmSetup.getPurchaseVatScheduleId().getVatScheduleId());
					dtoVendorClassSetup.setVatPercent(pmSetup.getPurchaseVatScheduleId().getBasperct());
				}
			}
		} catch (Exception e) {
			dtoVendorClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoVendorClassSetup;
	}
	public DtoAPCashReceiptDistribution getTransactionalDistributionDetail(DtoAPTransactionEntry transactionEntryAPDto) {
		try 
		{
			    List<DtoAPDistributionDetail> distributionDetailList= new ArrayList<>();
				DtoAPCashReceiptDistribution distribution = new DtoAPCashReceiptDistribution();
				distribution.setVendorId(transactionEntryAPDto.getVendorID()!=null?transactionEntryAPDto.getVendorID():"");
				distribution.setVendorName(transactionEntryAPDto.getVendorName()!=null?transactionEntryAPDto.getVendorName():"");
				distribution.setCurrencyID(transactionEntryAPDto.getCurrencyID());
				distribution.setTransactionNumber(transactionEntryAPDto.getApTransactionNumber());
				PMDocumentsTypeSetup pmDocumentsTypeSetup = repositoryPMDocumentType
						.findByApDocumentTypeIdAndIsDeleted(transactionEntryAPDto.getApTransactionType(), false);
				if(transactionEntryAPDto.getApTransactionType()>0){
					distribution.setTransactionType(pmDocumentsTypeSetup.getDocumentType());
				}
				
				distribution.setOriginalAmount(transactionEntryAPDto.getApTransactionTotalAmount());
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double amount  = transactionEntryAPDto.getApTransactionTotalAmount();
				String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
				if(UtilRandomKey.isNotBlank(String.valueOf(transactionEntryAPDto.getExchangeTableIndex()))){
					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(transactionEntryAPDto.getExchangeTableIndex()), false);
				    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
				    }
				}

				Double functionalAmount = (double) mathEval.eval(amount + calcMethod + transactionEntryAPDto.getExchangeTableRate());
				distribution.setFunctionalAmount(functionalAmount);
				
				List<APTransactionDistribution> apTransactionDistributionsList=null;
				if(transactionEntryAPDto.getButtonType().equalsIgnoreCase(MessageLabel.NEW_DISTRIBUTION))
				{
					apTransactionDistributionsList= repositoryAPTransactionDistribution.findByApTransactionNumber(transactionEntryAPDto.getApTransactionNumber());
					if(apTransactionDistributionsList!=null && !apTransactionDistributionsList.isEmpty())
					{
						for (APTransactionDistribution apTransactionDistribution : apTransactionDistributionsList) 
						{
							DtoAPDistributionDetail distributionDetail= new DtoAPDistributionDetail();
							distributionDetail.setAccountNumber("");
							distributionDetail.setAccountDescription("");
							distributionDetail.setAccountTableRowIndex("");
							GLAccountsTableAccumulation glAccountsTableAccumulation=null;
							if(apTransactionDistribution.getAccountTableRowIndex()>0){
								glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(apTransactionDistribution.getAccountTableRowIndex()), false);
							}
							distributionDetail.setDistributionReference("");
							if(UtilRandomKey.isNotBlank(apTransactionDistribution.getDistributionDescription())){
								distributionDetail.setDistributionReference(apTransactionDistribution.getDistributionDescription());
							}
							distributionDetail.setType("");
							distributionDetail.setTypeId(0);
							if(apTransactionDistribution.getTransactionType()>0){
								distributionDetail.setType(APDistributionTypeConstant.getById(apTransactionDistribution.getTransactionType()).name());
								distributionDetail.setTypeId(apTransactionDistribution.getTransactionType());
							}
							distributionDetail.setDebitAmount(0.0);
							if(apTransactionDistribution.getOriginalDebitAmount()>0){
								distributionDetail.setDebitAmount(apTransactionDistribution.getOriginalDebitAmount());
							}
							distributionDetail.setCreditAmount(0.0);
							if(apTransactionDistribution.getOriginalCreditAmount()>0){
								distributionDetail.setCreditAmount(apTransactionDistribution.getOriginalCreditAmount());
							}
							
							if(glAccountsTableAccumulation!=null){
								distributionDetail.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
								Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
								distributionDetail.setAccountNumber(object[0].toString());
								distributionDetail.setAccountDescription("");
								if(UtilRandomKey.isNotBlank(object[3].toString())){
									distributionDetail.setAccountDescription(object[3].toString());
								}
							}
							distributionDetailList.add(distributionDetail);
						}
				    }
				}
				
				if(transactionEntryAPDto.getButtonType().equalsIgnoreCase(MessageLabel.DEFAULT_DISTRIBUTION)|| apTransactionDistributionsList==null || apTransactionDistributionsList.isEmpty())
				{
				GLAccountsTableAccumulation glAccountsTableAccumulationPurchase=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationTradeDis=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationVat=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationAccPayable=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationCash=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFreight=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationMis=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFreightTax=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationMisTax=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationDebitMemo=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFinanceCharge=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationServiceRepair=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationWarrantyExpanse=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationWarrantySales=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationCreditMemo=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationReturn=null;
				
				String vendorClassId="";
				VendorMaintenance vendorMaintenance = repositoryVendorMaintenance.findByVendoridAndIsDeleted(transactionEntryAPDto.getVendorID(), false);
				if(vendorMaintenance!=null && vendorMaintenance.getVendorClassesSetup()!=null){
					vendorClassId=vendorMaintenance.getVendorClassesSetup().getVendorClassId();
				}
				
				List<PMSetup> pmSetupList = repositoryPMSetup.findAll();
				PMSetup pmSetup=null;
				if (pmSetupList != null && !pmSetupList.isEmpty()) {
					pmSetup = pmSetupList.get(0);
				}
				
				VendorClassAccountTableSetup vendorClassAccountTableSetup=null;
				
				//Purchase Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								PURCHASE.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationPurchase= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Trade Discount Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								TRADE_DISCOUNT.getIndex(),vendorClassId);
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationTradeDis= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Vat Account Number
				VATSetup vatSetup=repositoryVATSetup.findByVatScheduleIdAndIsDeleted(transactionEntryAPDto.getVatScheduleID(), false);
				if(vatSetup!=null){
					glAccountsTableAccumulationVat= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(vatSetup.getAccountRowId()), false);
				}
				
				//Account Payable Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								ACCOUNT_PAYABLE.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationAccPayable= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Cash Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								CASH.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationCash= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Debit Memo Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								DEBIT_MEMO.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationDebitMemo= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Finance Charge  Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								FINANCE_CHARGE.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationFinanceCharge= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Service Repair Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								SERVICE_REPAIR.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationServiceRepair= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Warranty Sales Amount Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								WARRANTY.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationWarrantySales= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Warranty Expense Amount Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								WARRANTY_EXPENSE.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationWarrantyExpanse= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Cedit Memo Amount Account Number
				vendorClassAccountTableSetup =repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								CREDIT_MEMO.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationCreditMemo= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Return Amount Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								RETURN.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationReturn= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				if(pmSetup!=null){
					//Freight Tax Account Number
					glAccountsTableAccumulationFreightTax= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(pmSetup.getFreightVatScheduleId().getAccountRowId()),false);
					
					//Miscellaneous Tax Account Number
					glAccountsTableAccumulationMisTax= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(pmSetup.getMiscVatScheduleId().getAccountRowId()),false);
					
				}
				
				//Freight Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								FREIGHT.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationFreight= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Miscellaneous Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								MISCELLANEOUS.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationMis= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				    
				// For Purchase
				if(transactionEntryAPDto.getPurchasesAmount()!=null && transactionEntryAPDto.getPurchasesAmount()>0)
				{
					double paymentAmount= Double.parseDouble(transactionEntryAPDto.getPaymentAmount());
					double diffAmount=transactionEntryAPDto.getApTransactionTotalAmount()-paymentAmount;
					if(transactionEntryAPDto.getIsPayment() && diffAmount<=0)
					{
						//Set Cash Account Number Information
						distributionDetailList=getCashDistibution(transactionEntryAPDto.getApTransactionTotalAmount(), distributionDetailList,
								glAccountsTableAccumulationCash);
					}
					else
					{
						if(diffAmount>0 && diffAmount!=transactionEntryAPDto.getApTransactionTotalAmount()){
							distributionDetailList=getDifferentAmountDistribution(paymentAmount, distributionDetailList, 
									glAccountsTableAccumulationCash, glAccountsTableAccumulationAccPayable, diffAmount);
						}
						else
						{
							//Set Account Payable Account Number Information
							distributionDetailList=getAccountPayableDistibution(transactionEntryAPDto.getApTransactionTotalAmount(), distributionDetailList, 
									glAccountsTableAccumulationAccPayable);
						}
					}
					
									
						//Set Purchase Account Number Information
						distributionDetailList=getPurchaseDisAccountDistibution(transactionEntryAPDto.getPurchasesAmount(), distributionDetailList, 
								glAccountsTableAccumulationPurchase);
						
						// Trade Discount
						if(transactionEntryAPDto.getApTransactionTradeDiscount()!=0.0){
							distributionDetailList= getTradeDisAccountDistibution(transactionEntryAPDto.getApTransactionTradeDiscount(), distributionDetailList,
									glAccountsTableAccumulationTradeDis);
						}
						//Set Vat Account Number Information
						if(transactionEntryAPDto.getApTransactionVATAmount()!=0.0){
							distributionDetailList = getVatAccountDistibution(transactionEntryAPDto.getApTransactionVATAmount(), distributionDetailList, 
									glAccountsTableAccumulationVat);
						}
						//Set Freight Account Number Information
						if(transactionEntryAPDto.getApTransactionFreightAmount()!=0.0){
							distributionDetailList = getFreightAccountDistibution(transactionEntryAPDto.getApTransactionFreightAmount(), pmSetup, distributionDetailList, 
									glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
						}
						//Set Miscellaneous Account Number Information
						if(transactionEntryAPDto.getApTransactionMiscellaneous()!=0.0){
							distributionDetailList = getMiscellaneousAccountDistibution(transactionEntryAPDto.getApTransactionMiscellaneous(), pmSetup, distributionDetailList,
									glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
						}
				}
				
				// For Debit Memo
				if(transactionEntryAPDto.getApTransactionDebitMemoAmount()!=null && transactionEntryAPDto.getApTransactionDebitMemoAmount()>0)
				{
					
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(transactionEntryAPDto.getApTransactionTotalAmount(),distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
					
						//Set Debit Memo Account Number Information
						distributionDetailList=getDebitMemoAccountDistibution(transactionEntryAPDto.getApTransactionDebitMemoAmount(), distributionDetailList, 
								glAccountsTableAccumulationDebitMemo);
					
						// Trade Discount
						if(transactionEntryAPDto.getApTransactionTradeDiscount()!=0.0){
							distributionDetailList= getTradeDisAccountDistibution(transactionEntryAPDto.getApTransactionTradeDiscount(), distributionDetailList,
									glAccountsTableAccumulationTradeDis);
						}
						
						//Set Vat Account Number Information
						if(transactionEntryAPDto.getApTransactionVATAmount()!=0.0){
							distributionDetailList = getVatAccountDistibution(transactionEntryAPDto.getApTransactionVATAmount(), distributionDetailList, 
									glAccountsTableAccumulationVat);
						}
						
						//Set Freight Account Number Information
						if(transactionEntryAPDto.getApTransactionFreightAmount()!=0.0){
							
							distributionDetailList = getFreightAccountDistibution(transactionEntryAPDto.getApTransactionFreightAmount(), pmSetup, distributionDetailList, 
									glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
						}
						
						//Set Miscellaneous Account Number Information
						if(transactionEntryAPDto.getApTransactionMiscellaneous()!=0.0){
							distributionDetailList = getMiscellaneousAccountDistibution(transactionEntryAPDto.getApTransactionMiscellaneous(), pmSetup, distributionDetailList,
									glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
						}
				}
				
					// For Finance Charge
					if(transactionEntryAPDto.getApTransactionFinanceChargeAmount()!=null && transactionEntryAPDto.getApTransactionFinanceChargeAmount()>0)
					{
						
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(transactionEntryAPDto.getApTransactionTotalAmount(), distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
						
						//Set Finance Charge Memo Account Number Information
						distributionDetailList=getFinanceChargeAccountDistibution(transactionEntryAPDto.getApTransactionFinanceChargeAmount(), distributionDetailList, 
								glAccountsTableAccumulationFinanceCharge);
						
						// Trade Discount
						if(transactionEntryAPDto.getApTransactionTradeDiscount()!=0.0){
							distributionDetailList= getTradeDisAccountDistibution(transactionEntryAPDto.getApTransactionTradeDiscount(), distributionDetailList,
									glAccountsTableAccumulationTradeDis);
						}
							
							//Set Vat Account Number Information
							if(transactionEntryAPDto.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(transactionEntryAPDto.getApTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(transactionEntryAPDto.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(transactionEntryAPDto.getApTransactionFreightAmount(), pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(transactionEntryAPDto.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(transactionEntryAPDto.getApTransactionMiscellaneous(), pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					//Credit Memo
					if(transactionEntryAPDto.getApTransactionCreditMemoAmount()!=null && transactionEntryAPDto.getApTransactionCreditMemoAmount()>0)
					{
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(transactionEntryAPDto.getApTransactionTotalAmount(), distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
						
						
						//Set Credit Memo Account Number Information
						distributionDetailList=getCreditMemoAccountDistribution(transactionEntryAPDto.getApTransactionCreditMemoAmount(), distributionDetailList, 
								glAccountsTableAccumulationCreditMemo);
						
						
							// Trade Discount
							if(transactionEntryAPDto.getApTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(transactionEntryAPDto.getApTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(transactionEntryAPDto.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(transactionEntryAPDto.getApTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(transactionEntryAPDto.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(transactionEntryAPDto.getApTransactionFreightAmount(), pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(transactionEntryAPDto.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(transactionEntryAPDto.getApTransactionMiscellaneous(), pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Service And Repair
					if(transactionEntryAPDto.getApServiceRepairAmount()!=null && transactionEntryAPDto.getApServiceRepairAmount()>0)
					{
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(transactionEntryAPDto.getApTransactionTotalAmount(), distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
						
						//Set Service and Repair Account Number Information
						distributionDetailList=getServiceRepairAccountDistribution(transactionEntryAPDto.getApServiceRepairAmount(), distributionDetailList, 
								glAccountsTableAccumulationServiceRepair);
						
						
							// Trade Discount
							if(transactionEntryAPDto.getApTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(transactionEntryAPDto.getApTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(transactionEntryAPDto.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(transactionEntryAPDto.getApTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(transactionEntryAPDto.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(transactionEntryAPDto.getApTransactionFreightAmount(), pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(transactionEntryAPDto.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(transactionEntryAPDto.getApTransactionMiscellaneous(), pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Sales and Returns
					if(transactionEntryAPDto.getApReturnAmount()!=null && transactionEntryAPDto.getApReturnAmount()>0)
					{
						double paymentAmount= Double.parseDouble(transactionEntryAPDto.getPaymentAmount());
						double diffAmount=transactionEntryAPDto.getApTransactionTotalAmount()-paymentAmount;
						if(transactionEntryAPDto.getIsPayment() && diffAmount<=0)
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(transactionEntryAPDto.getApTransactionTotalAmount(), distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							if(diffAmount>0 && diffAmount!=transactionEntryAPDto.getApTransactionTotalAmount()){
								distributionDetailList=getDifferentAmountDistribution(paymentAmount, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccPayable, diffAmount);
							}
							else
							{
								//Set Account Payable Account Number Information
								distributionDetailList=getAccountPayableDistibution(transactionEntryAPDto.getApTransactionTotalAmount(), distributionDetailList, 
										glAccountsTableAccumulationAccPayable);
							}
						}
						
						//Set Sales And Returns Account Number Information
						distributionDetailList=getSalesAndReturnsAccountDistibution(transactionEntryAPDto.getApReturnAmount(), distributionDetailList, 
								glAccountsTableAccumulationReturn);
						
					
							// Trade Discount
							if(transactionEntryAPDto.getApTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(transactionEntryAPDto.getApTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(transactionEntryAPDto.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(transactionEntryAPDto.getApTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(transactionEntryAPDto.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(transactionEntryAPDto.getApTransactionFreightAmount(), pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(transactionEntryAPDto.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(transactionEntryAPDto.getApTransactionMiscellaneous(), pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					//Warranty Repair
					if(transactionEntryAPDto.getApTransactionWarrantyAmount()!=null && transactionEntryAPDto.getApTransactionWarrantyAmount()>0)
					{
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(transactionEntryAPDto.getApTransactionTotalAmount(), distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
								
						//Set Warranty Sales Account Number Information
						distributionDetailList=getWarrantySalesAccountDistibution(transactionEntryAPDto.getApTransactionWarrantyAmount(), distributionDetailList, 
								glAccountsTableAccumulationWarrantySales);
						
						//Set Warranty Expenses Memo Account Number Information
						distributionDetailList=getWarrantyExpenceAccountDistibution(transactionEntryAPDto.getApTransactionWarrantyAmount(), distributionDetailList, 
								glAccountsTableAccumulationWarrantyExpanse);
						
					
							// Trade Discount
							if(transactionEntryAPDto.getApTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(transactionEntryAPDto.getApTransactionTradeDiscount(), distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(transactionEntryAPDto.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(transactionEntryAPDto.getApTransactionVATAmount(), distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(transactionEntryAPDto.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(transactionEntryAPDto.getApTransactionFreightAmount(), pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(transactionEntryAPDto.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(transactionEntryAPDto.getApTransactionMiscellaneous(), pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
				}
					
				distribution.setListDtoAPDistributionDetail(distributionDetailList);
				return distribution;
		
		}
		catch (Exception e) 
		{
			LOG.error(e.getMessage());
		}
		return null;
	}

	/*public DtoAPCashReceiptDistribution getTransactionalDistributionByTransactionNumber(String  transactionNumber) {
		try 
		{
			 List<DtoAPDistributionDetail> distributionDetailList= new ArrayList<>();
			 APTransactionEntry apTransactionEntry = repositoryAPTransactionEntry.
					 findByApTransactionNumber(transactionNumber);
			 if(apTransactionEntry!=null)
			 {
				DtoAPCashReceiptDistribution distribution = new DtoAPCashReceiptDistribution();
				distribution.setVendorId(apTransactionEntry.getVendorID()!=null?apTransactionEntry.getVendorID():"");
				distribution.setVendorName(apTransactionEntry.getVendorName()!=null?apTransactionEntry.getVendorName():"");
				distribution.setCurrencyID(apTransactionEntry.getCurrencyID());
				distribution.setTransactionNumber(apTransactionEntry.getApTransactionNumber());
				distribution.setTransactionType(apTransactionEntry.getTransactionType()!=null?apTransactionEntry.getTransactionType().getDocumentType():"");
				distribution.setOriginalAmount(apTransactionEntry.getApTransactionTotalAmount());
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double amount  = apTransactionEntry.getApTransactionTotalAmount();
				String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
				if(UtilRandomKey.isNotBlank(String.valueOf(apTransactionEntry.getExchangeTableIndex()))){
					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(apTransactionEntry.getExchangeTableIndex()), false);
				    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
				    }
				}

				Double functionalAmount = (double) mathEval.eval(amount + calcMethod + apTransactionEntry.getExchangeTableRate());
				distribution.setFunctionalAmount(functionalAmount);
				
				List<APTransactionDistribution> apTransactionDistributionsList= repositoryAPTransactionDistribution.findByApTransactionNumber(apTransactionEntry.getApTransactionNumber());
				if(apTransactionDistributionsList!=null && !apTransactionDistributionsList.isEmpty())
				{
					for (APTransactionDistribution apTransactionDistribution : apTransactionDistributionsList) 
					{
						DtoAPDistributionDetail distributionDetail= new DtoAPDistributionDetail();
						distributionDetail.setAccountNumber("");
						distributionDetail.setAccountDescription("");
						distributionDetail.setAccountTableRowIndex("");
						GLAccountsTableAccumulation glAccountsTableAccumulation=null;
						if(apTransactionDistribution.getAccountTableRowIndex()>0){
							glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(apTransactionDistribution.getAccountTableRowIndex()), false);
						}
						distributionDetail.setDistributionReference("");
						if(UtilRandomKey.isNotBlank(apTransactionDistribution.getDistributionDescription())){
							distributionDetail.setDistributionReference(apTransactionDistribution.getDistributionDescription());
						}
						distributionDetail.setType("");
						distributionDetail.setTypeId(0);
						if(apTransactionDistribution.getTransactionType()>0){
							distributionDetail.setType(APDistributionTypeConstant.getById(apTransactionDistribution.getTransactionType()).name());
							distributionDetail.setTypeId(apTransactionDistribution.getTransactionType());
						}
						distributionDetail.setDebitAmount(0.0);
						if(apTransactionDistribution.getOriginalDebitAmount()>0){
							distributionDetail.setDebitAmount(apTransactionDistribution.getOriginalDebitAmount());
						}
						distributionDetail.setCreditAmount(0.0);
						if(apTransactionDistribution.getOriginalCreditAmount()>0){
							distributionDetail.setCreditAmount(apTransactionDistribution.getOriginalCreditAmount());
						}
						
						if(glAccountsTableAccumulation!=null){
							distributionDetail.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
							Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
							distributionDetail.setAccountNumber(object[0].toString());
							distributionDetail.setAccountDescription("");
							if(UtilRandomKey.isNotBlank(object[3].toString())){
								distributionDetail.setAccountDescription(object[3].toString());
							}
						}
						distributionDetailList.add(distributionDetail);
					}
				}
				else
				{
				
				GLAccountsTableAccumulation glAccountsTableAccumulationPurchase=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationTradeDis=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationVat=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationAccPayable=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationCash=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFreight=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationMis=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFreightTax=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationMisTax=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationDebitMemo=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationFinanceCharge=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationServiceRepair=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationWarrantyExpanse=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationWarrantySales=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationCreditMemo=null;
				GLAccountsTableAccumulation glAccountsTableAccumulationReturn=null;
				
				String vendorClassId="";
				VendorMaintenance vendorMaintenance = repositoryVendorMaintenance.findByVendoridAndIsDeleted(apTransactionEntry.getVendorID(), false);
				if(vendorMaintenance!=null && vendorMaintenance.getVendorClassesSetup()!=null){
					vendorClassId=vendorMaintenance.getVendorClassesSetup().getVendorClassId();
				}
				
				List<PMSetup> pmSetupList = repositoryPMSetup.findAll();
				PMSetup pmSetup=null;
				if (pmSetupList != null && !pmSetupList.isEmpty()) {
					pmSetup = pmSetupList.get(0);
				}
				
				VendorClassAccountTableSetup vendorClassAccountTableSetup=null;
				
				//Purchase Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								PURCHASE.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationPurchase= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Trade Discount Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								TRADE_DISCOUNT.getIndex(),vendorClassId);
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationTradeDis= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Vat Account Number
				VATSetup vatSetup=repositoryVATSetup.findByVatScheduleIdAndIsDeleted(apTransactionEntry.getVatScheduleID(), false);
				if(vatSetup!=null){
					glAccountsTableAccumulationVat= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(vatSetup.getAccountRowId()), false);
				}
				
				//Account Payable Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								ACCOUNT_PAYABLE.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationAccPayable= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Cash Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								CASH.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationCash= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Debit Memo Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								DEBIT_MEMO.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationDebitMemo= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Finance Charge  Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								FINANCE_CHARGE.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationFinanceCharge= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Service Repair Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								SERVICE_REPAIR.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationServiceRepair= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Warranty Sales Amount Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								WARRANTY.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationWarrantySales= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Warranty Expense Amount Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								WARRANTY_EXPENSE.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationWarrantyExpanse= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Cedit Memo Amount Account Number
				vendorClassAccountTableSetup =repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								CREDIT_MEMO.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationCreditMemo= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Return Amount Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								RETURN.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationReturn= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				if(pmSetup!=null){
					//Freight Tax Account Number
					glAccountsTableAccumulationFreightTax= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(pmSetup.getFreightVatScheduleId().getAccountRowId()),false);
					
					//Miscellaneous Tax Account Number
					glAccountsTableAccumulationMisTax= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(pmSetup.getMiscVatScheduleId().getAccountRowId()),false);
					
				}
				
				//Freight Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								FREIGHT.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationFreight= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				//Miscellaneous Account Number
				vendorClassAccountTableSetup = repositoryVendorClassAccountTableSetup.
						findByMasterAccountTypeTypeIdAndVendorClassesSetupVendorClassId(MasterAPAccountTypeConstant.
								MISCELLANEOUS.getIndex(),vendorClassId);
				
				if(vendorClassAccountTableSetup!=null){
					glAccountsTableAccumulationMis= vendorClassAccountTableSetup.getGlAccountsTableAccumulation();
				}
				
				    
				// For Purchase
				if(apTransactionEntry.getPurchasesAmount()>0)
				{
					if(apTransactionEntry.isPayment())
					{
						//Set Cash Account Number Information
						distributionDetailList=getCashDistibution(apTransactionEntry, distributionDetailList,
								glAccountsTableAccumulationCash);
					}
					else
					{
						double diffAmount=apTransactionEntry.getApTransactionTotalAmount()-apTransactionEntry.getPaymentAmount();
						if(diffAmount>0){
							distributionDetailList=getDifferentAmountDistribution(apTransactionEntry, distributionDetailList, 
									glAccountsTableAccumulationCash, glAccountsTableAccumulationAccPayable, diffAmount);
						}
						else
						{
							//Set Account Payable Account Number Information
							distributionDetailList=getAccountPayableDistibution(apTransactionEntry, distributionDetailList, 
									glAccountsTableAccumulationAccPayable);
						}
					}
					
									
						//Set Purchase Account Number Information
						distributionDetailList=getPurchaseDisAccountDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationPurchase);
						
						// Trade Discount
						if(apTransactionEntry.getApTransactionTradeDiscount()!=0.0){
							distributionDetailList= getTradeDisAccountDistibution(apTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationTradeDis);
						}
						//Set Vat Account Number Information
						if(apTransactionEntry.getApTransactionVATAmount()!=0.0){
							distributionDetailList = getVatAccountDistibution(apTransactionEntry, distributionDetailList, 
									glAccountsTableAccumulationVat);
						}
						//Set Freight Account Number Information
						if(apTransactionEntry.getApTransactionFreightAmount()!=0.0){
							distributionDetailList = getFreightAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList, 
									glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
						}
						//Set Miscellaneous Account Number Information
						if(apTransactionEntry.getApTransactionMiscellaneous()!=0.0){
							distributionDetailList = getMiscellaneousAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList,
									glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
						}
				}
				
				// For Debit Memo
				if(apTransactionEntry.getApTransactionDebitMemoAmount()>0)
				{
					
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
					
						//Set Debit Memo Account Number Information
						distributionDetailList=getDebitMemoAccountDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationDebitMemo);
					
						// Trade Discount
						if(apTransactionEntry.getApTransactionTradeDiscount()!=0.0){
							distributionDetailList= getTradeDisAccountDistibution(apTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationTradeDis);
						}
						
						//Set Vat Account Number Information
						if(apTransactionEntry.getApTransactionVATAmount()!=0.0){
							distributionDetailList = getVatAccountDistibution(apTransactionEntry, distributionDetailList, 
									glAccountsTableAccumulationVat);
						}
						
						//Set Freight Account Number Information
						if(apTransactionEntry.getApTransactionFreightAmount()!=0.0){
							
							distributionDetailList = getFreightAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList, 
									glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
						}
						
						//Set Miscellaneous Account Number Information
						if(apTransactionEntry.getApTransactionMiscellaneous()!=0.0){
							distributionDetailList = getMiscellaneousAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList,
									glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
						}
				}
				
					// For Finance Charge
					if(apTransactionEntry.getApTransactionFinanceChargeAmount()>0)
					{
						
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
						
						//Set Finance Charge Memo Account Number Information
						distributionDetailList=getFinanceChargeAccountDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationFinanceCharge);
						
						// Trade Discount
						if(apTransactionEntry.getApTransactionTradeDiscount()!=0.0){
							distributionDetailList= getTradeDisAccountDistibution(apTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationTradeDis);
						}
							
							//Set Vat Account Number Information
							if(apTransactionEntry.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(apTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(apTransactionEntry.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(apTransactionEntry.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					//Credit Memo
					if(apTransactionEntry.getApTransactionCreditMemoAmount()>0)
					{
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
						
						
						//Set Credit Memo Account Number Information
						distributionDetailList=getCreditMemoAccountDistribution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationCreditMemo);
						
						
							// Trade Discount
							if(apTransactionEntry.getApTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(apTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(apTransactionEntry.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(apTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(apTransactionEntry.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(apTransactionEntry.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Service And Repair
					if(apTransactionEntry.getApServiceRepairAmount()>0)
					{
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
						
						//Set Service and Repair Account Number Information
						distributionDetailList=getServiceRepairAccountDistribution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationServiceRepair);
						
						
							// Trade Discount
							if(apTransactionEntry.getApTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(apTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(apTransactionEntry.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(apTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(apTransactionEntry.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(apTransactionEntry.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					// For Sales and Returns
					if(apTransactionEntry.getApReturnAmount()>0)
					{
						if(apTransactionEntry.isPayment())
						{
							//Set Cash Account Number Information
							distributionDetailList=getCashDistibution(apTransactionEntry, distributionDetailList,
									glAccountsTableAccumulationCash);
						}
						else
						{
							double diffAmount=apTransactionEntry.getApTransactionTotalAmount()-apTransactionEntry.getPaymentAmount();
							if(diffAmount>0){
								distributionDetailList=getDifferentAmountDistribution(apTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationCash, glAccountsTableAccumulationAccPayable, diffAmount);
							}
							else
							{
								//Set Account Payable Account Number Information
								distributionDetailList=getAccountPayableDistibution(apTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationAccPayable);
							}
						}
						
						//Set Sales And Returns Account Number Information
						distributionDetailList=getSalesAndReturnsAccountDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationReturn);
						
					
							// Trade Discount
							if(apTransactionEntry.getApTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(apTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(apTransactionEntry.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(apTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(apTransactionEntry.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(apTransactionEntry.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
					
					//Warranty Repair
					if(apTransactionEntry.getApTransactionWarrantyAmount()>0)
					{
						//Set Account Payable Account Number Information
						distributionDetailList=getAccountPayableDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationAccPayable);
								
						//Set Warranty Sales Account Number Information
						distributionDetailList=getWarrantySalesAccountDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationWarrantySales);
						
						//Set Warranty Expenses Memo Account Number Information
						distributionDetailList=getWarrantyExpenceAccountDistibution(apTransactionEntry, distributionDetailList, 
								glAccountsTableAccumulationWarrantyExpanse);
						
					
							// Trade Discount
							if(apTransactionEntry.getApTransactionTradeDiscount()!=0.0){
								distributionDetailList= getTradeDisAccountDistibution(apTransactionEntry, distributionDetailList,
										glAccountsTableAccumulationTradeDis);
							}
							
							//Set Vat Account Number Information
							if(apTransactionEntry.getApTransactionVATAmount()!=0.0){
								distributionDetailList = getVatAccountDistibution(apTransactionEntry, distributionDetailList, 
										glAccountsTableAccumulationVat);
							}
							
							//Set Freight Account Number Information
							if(apTransactionEntry.getApTransactionFreightAmount()!=0.0){
								
								distributionDetailList = getFreightAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList, 
										glAccountsTableAccumulationFreight, glAccountsTableAccumulationFreightTax);
							}
							
							//Set Miscellaneous Account Number Information
							if(apTransactionEntry.getApTransactionMiscellaneous()!=0.0){
								distributionDetailList = getMiscellaneousAccountDistibution(apTransactionEntry, pmSetup, distributionDetailList,
										glAccountsTableAccumulationMis, glAccountsTableAccumulationMisTax);
							}
					}
				}
					
				distribution.setListDtoAPDistributionDetail(distributionDetailList);
				return distribution;
		} 
		}
		catch (Exception e) 
		{
			LOG.error(e.getMessage());
		}
		return null;
	}*/
	
	private List<DtoAPDistributionDetail> getWarrantySalesAccountDistibution(Double apTransactionWarrantyAmount,
			List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationWarrantySales) 
	{
		DtoAPDistributionDetail distributionDetail= new DtoAPDistributionDetail();
		distributionDetail.setAccountNumber("");
		distributionDetail.setAccountDescription("");
		distributionDetail.setAccountTableRowIndex("");
		distributionDetail.setDistributionReference("");
		distributionDetail.setType(APDistributionTypeConstant.WARS.name());
		distributionDetail.setTypeId(APDistributionTypeConstant.WARS.getIndex());
		distributionDetail.setDebitAmount(apTransactionWarrantyAmount);
		
		if(glAccountsTableAccumulationWarrantySales!=null){
			distributionDetail.setAccountTableRowIndex(glAccountsTableAccumulationWarrantySales.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationWarrantySales);
			distributionDetail.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetail.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetail);
		return distributionDetailList;
	}
	
	private List<DtoAPDistributionDetail> getWarrantyExpenceAccountDistibution(Double apTransactionWarrantyAmount,
			List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationWarrantyExpense) 
	{
		DtoAPDistributionDetail distributionDetailWarrntyExpense= new DtoAPDistributionDetail();
		distributionDetailWarrntyExpense.setAccountNumber("");
		distributionDetailWarrntyExpense.setAccountDescription("");
		distributionDetailWarrntyExpense.setAccountTableRowIndex("");
		distributionDetailWarrntyExpense.setDistributionReference("");
		distributionDetailWarrntyExpense.setType(APDistributionTypeConstant.WARE.name());
		distributionDetailWarrntyExpense.setTypeId(APDistributionTypeConstant.WARE.getIndex());
		distributionDetailWarrntyExpense.setDebitAmount(apTransactionWarrantyAmount);
		
		
		if(glAccountsTableAccumulationWarrantyExpense!=null){
			distributionDetailWarrntyExpense.setAccountTableRowIndex(glAccountsTableAccumulationWarrantyExpense.getAccountTableRowIndex());
			Object[] object = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationWarrantyExpense);
			distributionDetailWarrntyExpense.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailWarrntyExpense.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailWarrntyExpense);
		return distributionDetailList;
	}
	
	public List<DtoAPDistributionDetail> getDebitMemoAccountDistibution(Double apTransactionDebitMemoAmount,
			List<DtoAPDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationDebitMemo){
	
		DtoAPDistributionDetail distributionDetailDebitMemo= new DtoAPDistributionDetail();
		distributionDetailDebitMemo.setAccountNumber("");
		distributionDetailDebitMemo.setAccountDescription("");
		distributionDetailDebitMemo.setAccountTableRowIndex("");
		distributionDetailDebitMemo.setDistributionReference("");
		distributionDetailDebitMemo.setType(APDistributionTypeConstant.DMEM.name());
		distributionDetailDebitMemo.setTypeId(APDistributionTypeConstant.DMEM.getIndex());
		distributionDetailDebitMemo.setDebitAmount(apTransactionDebitMemoAmount);
		
		if(glAccountsTableAccumulationDebitMemo!=null){
			distributionDetailDebitMemo.setAccountTableRowIndex(glAccountsTableAccumulationDebitMemo.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationDebitMemo);
			distributionDetailDebitMemo.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailDebitMemo.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailDebitMemo);
		return distributionDetailList;
	}
	
	
	public List<DtoAPDistributionDetail> getPurchaseDisAccountDistibution(Double purchasesAmount,
			List<DtoAPDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationPurchase){
	
		DtoAPDistributionDetail distributionDetailPurchase= new DtoAPDistributionDetail();
		distributionDetailPurchase.setAccountNumber("");
		distributionDetailPurchase.setAccountDescription("");
		distributionDetailPurchase.setAccountTableRowIndex("");
		distributionDetailPurchase.setDistributionReference("");
		distributionDetailPurchase.setType(APDistributionTypeConstant.PURH.name());
		distributionDetailPurchase.setTypeId(APDistributionTypeConstant.PURH.getIndex());
		distributionDetailPurchase.setDebitAmount(purchasesAmount);
		
		if(glAccountsTableAccumulationPurchase!=null){
			distributionDetailPurchase.setAccountTableRowIndex(glAccountsTableAccumulationPurchase.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationPurchase);
			distributionDetailPurchase.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailPurchase.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailPurchase);
		return distributionDetailList;
	}
	
	
	private List<DtoAPDistributionDetail> getCreditMemoAccountDistribution(Double apTransactionCreditMemoAmount,
			List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationCreditMemo) 
	{
		DtoAPDistributionDetail distributionDetailCreditMemo= new DtoAPDistributionDetail();
		distributionDetailCreditMemo.setAccountNumber("");
		distributionDetailCreditMemo.setAccountDescription("");
		distributionDetailCreditMemo.setAccountTableRowIndex("");
		distributionDetailCreditMemo.setDistributionReference("");
		distributionDetailCreditMemo.setType(APDistributionTypeConstant.CMEM.name());
		distributionDetailCreditMemo.setTypeId(APDistributionTypeConstant.CMEM.getIndex());
		distributionDetailCreditMemo.setCreditAmount(apTransactionCreditMemoAmount);
		
		if(glAccountsTableAccumulationCreditMemo!=null){
			distributionDetailCreditMemo.setAccountTableRowIndex(glAccountsTableAccumulationCreditMemo.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationCreditMemo);
			distributionDetailCreditMemo.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailCreditMemo.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailCreditMemo);
		return distributionDetailList;
	}

	private List<DtoAPDistributionDetail> getFinanceChargeAccountDistibution(Double apTransactionFinanceChargeAmount,
			List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationFinanceCharge) {
		DtoAPDistributionDetail distributionDetailCostOfSales= new DtoAPDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(APDistributionTypeConstant.FNCG.name());
		distributionDetailCostOfSales.setTypeId(APDistributionTypeConstant.FNCG.getIndex());
		distributionDetailCostOfSales.setDebitAmount(apTransactionFinanceChargeAmount);
		
		if(glAccountsTableAccumulationFinanceCharge!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationFinanceCharge.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationFinanceCharge);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailCostOfSales.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}
	
	private List<DtoAPDistributionDetail> getSalesAndReturnsAccountDistibution(Double  apReturnAmount,
			List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationSalesAndReturns) 
	{
		DtoAPDistributionDetail distributionDetailCostOfSales= new DtoAPDistributionDetail();
		distributionDetailCostOfSales.setAccountNumber("");
		distributionDetailCostOfSales.setAccountDescription("");
		distributionDetailCostOfSales.setAccountTableRowIndex("");
		distributionDetailCostOfSales.setDistributionReference("");
		distributionDetailCostOfSales.setType(APDistributionTypeConstant.RETN.name());
		distributionDetailCostOfSales.setTypeId(APDistributionTypeConstant.RETN.getIndex());
		distributionDetailCostOfSales.setCreditAmount(apReturnAmount);
		
		if(glAccountsTableAccumulationSalesAndReturns!=null){
			distributionDetailCostOfSales.setAccountTableRowIndex(glAccountsTableAccumulationSalesAndReturns.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationSalesAndReturns);
			distributionDetailCostOfSales.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailCostOfSales.setAccountDescription(object[3].toString());
			}
		}
		
		distributionDetailList.add(distributionDetailCostOfSales);
		return distributionDetailList;
	}
	
	private List<DtoAPDistributionDetail> getServiceRepairAccountDistribution(Double apServiceRepairAmount,
			List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationServiceRepair) 
	{
		DtoAPDistributionDetail distributionDetailRepair= new DtoAPDistributionDetail();
		distributionDetailRepair.setAccountNumber("");
		distributionDetailRepair.setAccountDescription("");
		distributionDetailRepair.setAccountTableRowIndex("");
		distributionDetailRepair.setDistributionReference("");
		distributionDetailRepair.setType(APDistributionTypeConstant.SRVC.name());
		distributionDetailRepair.setTypeId(APDistributionTypeConstant.SRVC.getIndex());
		distributionDetailRepair.setDebitAmount(apServiceRepairAmount);
		
		if(glAccountsTableAccumulationServiceRepair!=null){
			distributionDetailRepair.setAccountTableRowIndex(glAccountsTableAccumulationServiceRepair.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationServiceRepair);
			distributionDetailRepair.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailRepair.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailRepair);
		return distributionDetailList;
	}

	public List<DtoAPDistributionDetail> getDifferentAmountDistribution(Double paymentAmount,
			List<DtoAPDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationCash,
			GLAccountsTableAccumulation glAccountsTableAccumulationAccPayable, Double diffAmount)
	{
		DtoAPDistributionDetail distributionDetailCash= new DtoAPDistributionDetail();
		distributionDetailCash.setAccountNumber("");
		distributionDetailCash.setAccountDescription("");
		distributionDetailCash.setAccountTableRowIndex("");
		distributionDetailCash.setDistributionReference("");
		distributionDetailCash.setType(APDistributionTypeConstant.CASH.name());
		distributionDetailCash.setTypeId(APDistributionTypeConstant.CASH.getIndex());
		distributionDetailCash.setCreditAmount(paymentAmount);
		
		if(glAccountsTableAccumulationCash!=null){
			distributionDetailCash.setAccountTableRowIndex(glAccountsTableAccumulationCash.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationCash);
			distributionDetailCash.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailCash.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailCash);
		
		//Set Account Receivable Account Number Information
		DtoAPDistributionDetail distributionDetailAccountPay= new DtoAPDistributionDetail();
		distributionDetailAccountPay.setAccountNumber("");
		distributionDetailAccountPay.setAccountDescription("");
		distributionDetailAccountPay.setAccountTableRowIndex("");
		distributionDetailAccountPay.setDistributionReference("");
		distributionDetailAccountPay.setType(APDistributionTypeConstant.PAYP.name());
		distributionDetailAccountPay.setTypeId(APDistributionTypeConstant.PAYP.getIndex());
		distributionDetailAccountPay.setCreditAmount(diffAmount);
		
		if(glAccountsTableAccumulationAccPayable!=null){
			distributionDetailAccountPay.setAccountTableRowIndex(glAccountsTableAccumulationAccPayable.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationAccPayable);
			distributionDetailAccountPay.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailAccountPay.setAccountDescription(object[3].toString());
			}
		}
		
		distributionDetailList.add(distributionDetailAccountPay);
		return distributionDetailList;
	}
	
	public List<DtoAPDistributionDetail> getCashDistibution(Double apTransactionTotalAmount,
			List<DtoAPDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationCash)
	{
		DtoAPDistributionDetail distributionDetailCash= new DtoAPDistributionDetail();
		distributionDetailCash.setAccountNumber("");
		distributionDetailCash.setAccountDescription("");
		distributionDetailCash.setAccountTableRowIndex("");
		distributionDetailCash.setDistributionReference("");
		distributionDetailCash.setType(APDistributionTypeConstant.CASH.name());
		distributionDetailCash.setTypeId(APDistributionTypeConstant.CASH.getIndex());
		distributionDetailCash.setCreditAmount(apTransactionTotalAmount);
		
		
		if(glAccountsTableAccumulationCash!=null){
			distributionDetailCash.setAccountTableRowIndex(glAccountsTableAccumulationCash.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationCash);
			distributionDetailCash.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailCash.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailCash);
		return distributionDetailList;
	}
	
	public List<DtoAPDistributionDetail> getAccountPayableDistibution(Double apTransactionTotalAmount,
			List<DtoAPDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationAccPayable)
	{
		DtoAPDistributionDetail distributionDetailAccountPay= new DtoAPDistributionDetail();
		distributionDetailAccountPay.setAccountNumber("");
		distributionDetailAccountPay.setAccountDescription("");
		distributionDetailAccountPay.setAccountTableRowIndex("");
		distributionDetailAccountPay.setDistributionReference("");
		distributionDetailAccountPay.setType(APDistributionTypeConstant.PAYP.name());
		distributionDetailAccountPay.setTypeId(APDistributionTypeConstant.PAYP.getIndex());
		distributionDetailAccountPay.setCreditAmount(apTransactionTotalAmount);
		
		if(glAccountsTableAccumulationAccPayable!=null){
			distributionDetailAccountPay.setAccountTableRowIndex(glAccountsTableAccumulationAccPayable.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationAccPayable);
			distributionDetailAccountPay.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailAccountPay.setAccountDescription(object[3].toString());
			}
		}
		
		distributionDetailList.add(distributionDetailAccountPay);
		return distributionDetailList;
	}

	public List<DtoAPDistributionDetail> getTradeDisAccountDistibution(Double apTransactionTradeDiscount,
			List<DtoAPDistributionDetail> distributionDetailList, GLAccountsTableAccumulation glAccountsTableAccumulationTradeDis){
		//Set Trade Discount Account Number Information
		DtoAPDistributionDetail distributionDetailTradeDiscount= new DtoAPDistributionDetail();
		distributionDetailTradeDiscount.setAccountNumber("");
		distributionDetailTradeDiscount.setAccountDescription("");
		distributionDetailTradeDiscount.setAccountTableRowIndex("");
		distributionDetailTradeDiscount.setDistributionReference("");
		distributionDetailTradeDiscount.setType(APDistributionTypeConstant.TRAD.name());
		distributionDetailTradeDiscount.setTypeId(APDistributionTypeConstant.TRAD.getIndex());
		distributionDetailTradeDiscount.setCreditAmount(apTransactionTradeDiscount);
		
		if(glAccountsTableAccumulationTradeDis!=null){
			distributionDetailTradeDiscount.setAccountTableRowIndex(glAccountsTableAccumulationTradeDis.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationTradeDis);
			distributionDetailTradeDiscount.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailTradeDiscount.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailTradeDiscount);
		return distributionDetailList;
	}
	
	public List<DtoAPDistributionDetail> getMiscellaneousAccountDistibution(Double apTransactionMiscellaneous,
			PMSetup pmSetup, List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationMis, GLAccountsTableAccumulation glAccountsTableAccumulationMisTax){

		DtoAPDistributionDetail distributionDetailMis= new DtoAPDistributionDetail();
		distributionDetailMis.setAccountNumber("");
		distributionDetailMis.setAccountDescription("");
		distributionDetailMis.setAccountTableRowIndex("");
		distributionDetailMis.setDistributionReference("");
		distributionDetailMis.setType(APDistributionTypeConstant.MISC.name());
		distributionDetailMis.setTypeId(APDistributionTypeConstant.MISC.getIndex());
		distributionDetailMis.setDebitAmount(apTransactionMiscellaneous);
		
		if(glAccountsTableAccumulationMis!=null){
			distributionDetailMis.setAccountTableRowIndex(glAccountsTableAccumulationMis.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationMis);
			distributionDetailMis.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailMis.setAccountDescription(object[3].toString());
			}
		}
		
		distributionDetailList.add(distributionDetailMis);
		//Set Miscellaneous Tax Account Number Information
		double misTaxAmount=0.0;
		if(pmSetup!=null && pmSetup.getMiscVatScheduleId()!=null){
			misTaxAmount =(apTransactionMiscellaneous*pmSetup.getMiscVatScheduleId().getBasperct())/100;
		}
		
		DtoAPDistributionDetail distributionDetailMisTax= new DtoAPDistributionDetail();
		distributionDetailMisTax.setAccountNumber("");
		distributionDetailMisTax.setAccountDescription("");
		distributionDetailMisTax.setAccountTableRowIndex("");
		distributionDetailMisTax.setDistributionReference("");
		distributionDetailMisTax.setType(APDistributionTypeConstant.MISC_TAX.name());
		distributionDetailMisTax.setTypeId(APDistributionTypeConstant.MISC_TAX.getIndex());
		distributionDetailMisTax.setDebitAmount(misTaxAmount);
		
		if(glAccountsTableAccumulationMisTax!=null){
			distributionDetailMisTax.setAccountTableRowIndex(glAccountsTableAccumulationMisTax.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationMisTax);
			distributionDetailMisTax.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailMisTax.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailMisTax);
		return distributionDetailList;
	}
	
	public List<DtoAPDistributionDetail> getFreightAccountDistibution(Double apTransactionFreightAmount,
			PMSetup pmSetup, List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationFreight, GLAccountsTableAccumulation glAccountsTableAccumulationFreightTax){
		
		
		DtoAPDistributionDetail distributionDetailFreight= new DtoAPDistributionDetail();
		distributionDetailFreight.setAccountNumber("");
		distributionDetailFreight.setAccountDescription("");
		distributionDetailFreight.setAccountTableRowIndex("");
		distributionDetailFreight.setDistributionReference("");
		distributionDetailFreight.setType(APDistributionTypeConstant.FRGH_AMT.name());
		distributionDetailFreight.setTypeId(APDistributionTypeConstant.FRGH_AMT.getIndex());
		distributionDetailFreight.setDebitAmount(apTransactionFreightAmount);
		
		
		if(glAccountsTableAccumulationFreight!=null){
			distributionDetailFreight.setAccountTableRowIndex(glAccountsTableAccumulationFreight.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationFreight);
			distributionDetailFreight.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailFreight.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailFreight);
		
		//Set Freight Tax Account Number Information
		
		double freightTaxAmount=0.0;
		if(pmSetup!=null && pmSetup.getFreightVatScheduleId()!=null){
			freightTaxAmount =(apTransactionFreightAmount*pmSetup.getFreightVatScheduleId().getBasperct())/100;
		}
		
		DtoAPDistributionDetail distributionDetailFreightTax= new DtoAPDistributionDetail();
		distributionDetailFreightTax.setAccountNumber("");
		distributionDetailFreightTax.setAccountDescription("");
		distributionDetailFreightTax.setAccountTableRowIndex("");
		distributionDetailFreightTax.setDistributionReference("");
		distributionDetailFreightTax.setType(APDistributionTypeConstant.FRGH_TAX.name());
		distributionDetailFreightTax.setTypeId(APDistributionTypeConstant.FRGH_TAX.getIndex());
		distributionDetailFreightTax.setDebitAmount(freightTaxAmount);
		
		if(glAccountsTableAccumulationFreightTax!=null){
			distributionDetailFreightTax.setAccountTableRowIndex(glAccountsTableAccumulationFreightTax.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationFreightTax);
			distributionDetailFreightTax.setAccountNumber(object[0].toString());
			distributionDetailFreightTax.setAccountDescription("");
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailFreightTax.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailFreightTax);
		
		return distributionDetailList;
	}
	
	public List<DtoAPDistributionDetail> getVatAccountDistibution(Double apTransactionVATAmount,
			List<DtoAPDistributionDetail> distributionDetailList,
			GLAccountsTableAccumulation glAccountsTableAccumulationVat){
	
		DtoAPDistributionDetail distributionDetailVatSchedule= new DtoAPDistributionDetail();
		distributionDetailVatSchedule.setAccountNumber("");
		distributionDetailVatSchedule.setAccountDescription("");
		distributionDetailVatSchedule.setAccountTableRowIndex("");
		distributionDetailVatSchedule.setDistributionReference("");
		distributionDetailVatSchedule.setType(APDistributionTypeConstant.TAXS.name());
		distributionDetailVatSchedule.setTypeId(APDistributionTypeConstant.TAXS.getIndex());
		distributionDetailVatSchedule.setDebitAmount(apTransactionVATAmount);
		
		if(glAccountsTableAccumulationVat!=null){
			distributionDetailVatSchedule.setAccountTableRowIndex(glAccountsTableAccumulationVat.getAccountTableRowIndex());
			Object[] object =serviceAccountPayable.getAccountNumber(glAccountsTableAccumulationVat);
			distributionDetailVatSchedule.setAccountNumber(object[0].toString());
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				distributionDetailVatSchedule.setAccountDescription(object[3].toString());
			}
		}
		distributionDetailList.add(distributionDetailVatSchedule);
		return distributionDetailList;
   }

	public boolean saveDistributionDetail(DtoAPCashReceiptDistribution dtoAPCashReceiptDistribution) throws ScriptException 
	{
			List<APTransactionDistribution> apTransactionEntryDistributionList=repositoryAPTransactionDistribution.
					findByApTransactionNumber(dtoAPCashReceiptDistribution.getTransactionNumber());
			 if(apTransactionEntryDistributionList!=null && !apTransactionEntryDistributionList.isEmpty()){
				 repositoryAPTransactionDistribution.deleteInBatch(apTransactionEntryDistributionList);
			 }
			APTransactionEntry apTransactionEntry = repositoryAPTransactionEntry.findByApTransactionNumber(dtoAPCashReceiptDistribution.getTransactionNumber());
			if(apTransactionEntry!=null)
			{
				Double exchangeTableRate= apTransactionEntry.getExchangeTableRate();
				String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
				if(UtilRandomKey.isNotBlank(String.valueOf(apTransactionEntry.getExchangeTableIndex()))){
					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(apTransactionEntry.getExchangeTableIndex()), false);
				    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
				    }
				}
				
				if(!dtoAPCashReceiptDistribution.getListDtoAPDistributionDetail().isEmpty())
				{
					for (DtoAPDistributionDetail dtoAPDistributionDetail : dtoAPCashReceiptDistribution.getListDtoAPDistributionDetail()) 
					{
						ScriptEngineManager manager = new ScriptEngineManager();
						ScriptEngine mathEval = manager.getEngineByName("js");
						Double originalDebitAmount=dtoAPDistributionDetail.getDebitAmount(); 
						Double originalCreditAmount = dtoAPDistributionDetail.getCreditAmount();
						APTransactionDistribution apTransactionDistribution= new APTransactionDistribution();
						apTransactionDistribution.setApTransactionNumber(dtoAPCashReceiptDistribution.getTransactionNumber());
						apTransactionDistribution.setOriginalDebitAmount(originalDebitAmount!=null?originalDebitAmount:0.0);
						apTransactionDistribution.setOriginalCreditAmount(originalCreditAmount!=null?originalCreditAmount:0.0);
						apTransactionDistribution.setTransactionType(dtoAPDistributionDetail.getTypeId());
						Double debitAmount=0.0;
						if(originalDebitAmount!=null && exchangeTableRate!=null ){
							 debitAmount = (double) mathEval.eval(originalDebitAmount + calcMethod + exchangeTableRate);
						}
						Double creditAmount=0.0;
						if(originalCreditAmount!=null && exchangeTableRate!=null){
							creditAmount = (double) mathEval.eval(originalCreditAmount + calcMethod + exchangeTableRate);
						}
						apTransactionDistribution.setDebitAmount(debitAmount);
						apTransactionDistribution.setCreditAmount(creditAmount);
						apTransactionDistribution.setModifyDate(new Date());
						if(UtilRandomKey.isNotBlank(dtoAPDistributionDetail.getAccountTableRowIndex())){
							apTransactionDistribution.setAccountTableRowIndex(Integer.parseInt(dtoAPDistributionDetail.getAccountTableRowIndex()));
						}
						apTransactionDistribution.setDistributionDescription(dtoAPDistributionDetail.getDistributionReference());
						apTransactionDistribution.setModifyByUserID(httpServletRequest.getHeader(USER_ID));
						repositoryAPTransactionDistribution.saveAndFlush(apTransactionDistribution);
					}
					return true;
				}
			}
			return false;
	}
	
	public boolean deleteTransactionEntry(String apTransactionNumber) 
	{
		APTransactionEntry apTransactionEntry= repositoryAPTransactionEntry.findByApTransactionNumber(apTransactionNumber);
		if(apTransactionEntry!=null)
		{
			List<APTransactionDistribution> apTransactionEntryDistributionList=repositoryAPTransactionDistribution.
					findByApTransactionNumber(apTransactionNumber);
			 if(apTransactionEntryDistributionList!=null && !apTransactionEntryDistributionList.isEmpty()){
				 repositoryAPTransactionDistribution.deleteInBatch(apTransactionEntryDistributionList);
			 }
			 repositoryAPTransactionEntry.delete(apTransactionEntry);
			 return true;
		}
		return false;
	}
	
	public boolean deleteTransactionEntryDistribution(String apTransactionNumber) 
	{
		List<APTransactionDistribution> apTransactionEntryDistributionList=repositoryAPTransactionDistribution.
				findByApTransactionNumber(apTransactionNumber);
		 if(apTransactionEntryDistributionList!=null && !apTransactionEntryDistributionList.isEmpty()){
			 repositoryAPTransactionDistribution.deleteInBatch(apTransactionEntryDistributionList);
             return true;
		 }
		return false;
	}

	public boolean checkTransDistributionDetailIsAvailable(String apTransactionNumber) {
		List<APTransactionDistribution> apTransactionEntryDistributionList=repositoryAPTransactionDistribution.
				findByApTransactionNumber(apTransactionNumber);
		 if(apTransactionEntryDistributionList!=null && !apTransactionEntryDistributionList.isEmpty()){
             return true;
		 }
		return false;
	}

	public DtoAPTransactionEntry postAPTransactionEntry(DtoAPTransactionEntry dtoAPTransactionEntry) throws ScriptException 
	{
	    APTransactionEntry apTransactionEntry=	repositoryAPTransactionEntry.findByApTransactionNumber(dtoAPTransactionEntry.getApTransactionNumber());
		Double exchangeTableRate= dtoAPTransactionEntry.getExchangeTableRate();
		String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine mathEval = manager.getEngineByName("js");
		if(UtilRandomKey.isNotBlank(String.valueOf(dtoAPTransactionEntry.getExchangeTableIndex()))){
			CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(dtoAPTransactionEntry.getExchangeTableIndex()), false);
		    if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
			   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
		    }
		}
		double totalAmount=0.0;
		try {
			totalAmount = (double) mathEval.eval(dtoAPTransactionEntry.getApTransactionTotalAmount() + calcMethod + exchangeTableRate);
		} catch (ScriptException e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		 List<APYTDOpenTransactionDistribution> apytdOpenTransactionsDistributionList=saveOpenTransactionDistributionDetail(dtoAPTransactionEntry,calcMethod,mathEval);
		 if(apytdOpenTransactionsDistributionList!=null && !apytdOpenTransactionsDistributionList.isEmpty())
		 {
		    // Save in Ap50000
			saveArSummaryBalanceInPostTrans(dtoAPTransactionEntry,totalAmount);
			//Save In Ap50001
			saveArBalanceByPeriodsInPostTrans(dtoAPTransactionEntry,totalAmount);
			//Save In Ap90200
			saveOpenTransactionInPostTrans(dtoAPTransactionEntry,apTransactionEntry);
			//Save in Ap90201
		    repositoryAPYTDOpenTransactionDistribution.save(apytdOpenTransactionsDistributionList);
		    if(dtoAPTransactionEntry.getIsPayment()){
				// Save in Ar90400 and 90401
				arYTDOpenPaymentSaveInPostTransaction(dtoAPTransactionEntry, totalAmount, apTransactionEntry);
		    }
			//create new GlBatches in Gl Module
			GLBatches glBatches= createNewGlBatchesInPostCashReceiptEntry(dtoAPTransactionEntry);
			//create New Gl JV Entry In Gl Module
			createNewJvEntryInPostTransactionEntry(apytdOpenTransactionsDistributionList, dtoAPTransactionEntry, glBatches, mathEval);
			//Delete Transaction Entry
			List<APTransactionDistribution> apTransactionEntryDistributionList=repositoryAPTransactionDistribution.
					 findByApTransactionNumber(dtoAPTransactionEntry.getApTransactionNumber());
			if(apTransactionEntryDistributionList!=null){
				repositoryAPTransactionDistribution.deleteInBatch(apTransactionEntryDistributionList);
			}
			//Delete Transaction Entry
			repositoryAPTransactionEntry.delete(apTransactionEntry);
			return dtoAPTransactionEntry;
		 }
		 else
		 {
			 LOG.error("AP_POST_TRANSACTION: Missing account number");
			 dtoAPTransactionEntry.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
			 return dtoAPTransactionEntry;
		 }
	}
	
	public GLBatches createNewGlBatchesInPostCashReceiptEntry(DtoAPTransactionEntry dtoAPTransactionEntry){
		int langid = serviceHome.getLanngugaeId();
		int year=0;
		int month=0;
		int day=0;
		Calendar cal = Calendar.getInstance();
		if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getApTransactionDate())) {
			Date transDate = UtilDateAndTime
					.ddmmyyyyStringToDate(dtoAPTransactionEntry.getApTransactionDate());
			cal.setTime(transDate);
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH)+1;
			day = cal.get(Calendar.DATE);
		}
		
		String batchId=AuditTrailSeriesTypeConstant.AP+
				GlSerialConfigurationConstant.CASH_RECEIPT.getVaue()+day+month+year;
		String batchDesc=AuditTrailSeriesTypeConstant.AP+" "+
				GlSerialConfigurationConstant.CASH_RECEIPT.getVaue()+" "+dtoAPTransactionEntry.getApTransactionNumber();
		
		GLBatches glBatches=repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(batchId, BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex(), false);
		if(glBatches==null)
		{
			glBatches= new GLBatches();
			glBatches.setTotalAmountTransactionInBatch(dtoAPTransactionEntry.getApTransactionTotalAmount());
			glBatches.setTotalNumberTransactionInBatch(1);
		}
		else
		{
			double amount;
			amount=glBatches.getTotalAmountTransactionInBatch()!=null?glBatches.getTotalAmountTransactionInBatch():0.0;
			glBatches.setTotalAmountTransactionInBatch(amount+dtoAPTransactionEntry.getApTransactionTotalAmount());
			glBatches.setTotalNumberTransactionInBatch(glBatches.getTotalNumberTransactionInBatch()+1);
		}
		 
		glBatches.setBatchID(batchId);
		glBatches.setBatchDescription(batchDesc);
		
		BatchTransactionType batchTransactionType = repositoryBatchTransactionType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex(), langid, false);
		if (batchTransactionType != null) {
			glBatches.setBatchTransactionType(batchTransactionType);
		}
		
		glBatches.setPostingDate(new Date());
		glBatches = repositoryGLBatches.saveAndFlush(glBatches);
		return glBatches;
	}
	
	public boolean createNewJvEntryInPostTransactionEntry(List<APYTDOpenTransactionDistribution> apYtdOpenTransactionDistribution ,
			DtoAPTransactionEntry dtoAPTransactionEntry , GLBatches glBatches,ScriptEngine mathEval) throws ScriptException{
		//Save Data in gl10100
		int nextJournalId = 0;
		GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
			if (glConfigurationSetup == null) {
				return false;
			}
			JournalEntryHeader journalEntryHeader = new JournalEntryHeader();
			nextJournalId = glConfigurationSetup.getNextJournalEntryVoucher();
			journalEntryHeader.setJournalID(String.valueOf(nextJournalId));

			journalEntryHeader.setOriginalTransactionNumberFromModule(dtoAPTransactionEntry.getApTransactionNumber());
		journalEntryHeader.setGlBatches(glBatches);
		journalEntryHeader.setTransactionType(CommonConstant.STANDARD);
		
		if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getApTransactionDate())) {
			journalEntryHeader.setTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoAPTransactionEntry.getApTransactionDate()));
		} 

		GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
				.findFirstByGlConfigurationAuditTrialSeriesTypeTypeIdAndGlConfigurationAuditTrialSeriesTypeLanguageLanguageIdAndSourceDocumentContainingIgnoreCase(AuditTrailSeriesTypeConstant.AP.getIndex(), 1,"PURCHASE");
		journalEntryHeader.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
		journalEntryHeader.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
		journalEntryHeader.setJournalDescription(dtoAPTransactionEntry.getApTransactionDescription());
		journalEntryHeader.setTransactionSource("JV");
		CurrencySetup currencySetup = repositoryCurrencySetup
				.findByCurrencyIdAndIsDeleted(dtoAPTransactionEntry.getCurrencyID(), false);
		journalEntryHeader.setCurrencySetup(currencySetup);
		journalEntryHeader.setOriginalTotalJournalEntryCredit(dtoAPTransactionEntry.getApTransactionTotalAmount()!=null?dtoAPTransactionEntry.getApTransactionTotalAmount():0.0);
		journalEntryHeader.setOriginalTotalJournalEntryDebit(dtoAPTransactionEntry.getApTransactionTotalAmount()!=null?dtoAPTransactionEntry.getApTransactionTotalAmount():0.0);
		
		Double currentExchangeRate = Double.valueOf(dtoAPTransactionEntry.getExchangeTableRate());
		journalEntryHeader.setExchangeTableRate(currentExchangeRate);
		CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
				.findByExchangeIndexAndIsDeleted(Integer.parseInt(dtoAPTransactionEntry.getExchangeTableIndex()), false);
		String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
		if (currencyExchangeHeader != null) 
		{
			    journalEntryHeader.setCurrencyExchangeHeader(currencyExchangeHeader);
				if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCurrencyID())) 
				{
						if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
							calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
									.getMethod();
						}
						double totalAmount = (double) mathEval
								.eval(Double.valueOf(dtoAPTransactionEntry.getApTransactionTotalAmount()!=null?
										dtoAPTransactionEntry.getApTransactionTotalAmount():0.0) + calcMethod
										+ currentExchangeRate);
						journalEntryHeader.setTotalJournalEntryDebit(totalAmount);
						
						journalEntryHeader.setTotalJournalEntryCredit(totalAmount);

				}
		}

        journalEntryHeader = repositoryJournalEntryHeader.save(journalEntryHeader);
	    glConfigurationSetup.setNextJournalEntryVoucher(nextJournalId + 1);
	    repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);

	    //Save In GL JournalEntryDetails
		 if(apYtdOpenTransactionDistribution!=null && !apYtdOpenTransactionDistribution.isEmpty())
		 {
			 for (APYTDOpenTransactionDistribution apytdOpenTransaction : apYtdOpenTransactionDistribution) 
			 {
				    JournalEntryDetails journalEntryDetails = new JournalEntryDetails();
					journalEntryDetails.setAccountTableRowIndex(String.valueOf(apytdOpenTransaction.getAccountTableRowIndex()));
					
					journalEntryDetails.setDistributionDescription(apytdOpenTransaction.getDistributionDescription());
					if (apytdOpenTransaction.getOriginalCreditAmount()>0) {
						journalEntryDetails.setBalanceType(2);
					} else {
						journalEntryDetails.setBalanceType(1);
					}
					
					journalEntryDetails.setOriginalCreditAmount(apytdOpenTransaction.getOriginalCreditAmount());
					journalEntryDetails.setCreditAmount(apytdOpenTransaction.getCreditAmount());
					journalEntryDetails.setOriginalDebitAmount(apytdOpenTransaction.getOriginalDebitAmount());
					journalEntryDetails.setDebitAmount(apytdOpenTransaction.getDebitAmount());
					journalEntryDetails.setIntercompanyIDMultiCompanyTransaction(String.valueOf(serviceHome.getCompanyIdFromCompanyTenant(httpServletRequest.getHeader(CommonConstant.TENANT_ID))));
					journalEntryDetails.setJournalEntryHeader(journalEntryHeader);
					journalEntryDetails.setExchangeTableRate(journalEntryHeader.getExchangeTableRate());
					repositoryJournalEntryDetail.save(journalEntryDetails);
			 }
		}
			return true;
	}
	
	public void arYTDOpenPaymentSaveInPostTransaction(DtoAPTransactionEntry dtoAPTransactionEntry,double totalAmount,
			APTransactionEntry apTransactionEntry)
	{
		//Save in AR90400 if payment true
		APYTDOpenPayments apytdOpenPayments= new APYTDOpenPayments();
		apytdOpenPayments.setPaymentNumber(apTransactionEntry.getCashReceiptNumber());
		apytdOpenPayments.setApTransactionNumber(dtoAPTransactionEntry.getApTransactionNumber());
		apytdOpenPayments.setApTransactionType(dtoAPTransactionEntry.getApTransactionType());
		apytdOpenPayments.setCheckbookID(dtoAPTransactionEntry.getCheckbookID());
		
		apytdOpenPayments.setPaymentDescription(dtoAPTransactionEntry.getApTransactionDescription());
		apytdOpenPayments.setPaymentCreateDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPTransactionEntry.getApTransactionDate()));
		apytdOpenPayments.setVendorID(dtoAPTransactionEntry.getVendorID());
		apytdOpenPayments.setCurrencyID(dtoAPTransactionEntry.getCurrencyID());
		apytdOpenPayments.setPaymentAmount(totalAmount);
		apytdOpenPayments.setOriginalPaymentAmount(dtoAPTransactionEntry.getApTransactionTotalAmount());

		if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCheckNumber())) {
			apytdOpenPayments.setCheckNumber(dtoAPTransactionEntry.getCheckNumber());
		}

		if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCreditCardID())) 
		{
			apytdOpenPayments.setCreditCardID(dtoAPTransactionEntry.getCreditCardID());
			apytdOpenPayments.setCreditCardNumber(dtoAPTransactionEntry.getCreditCardNumber());
			apytdOpenPayments.setCreditCardExpireMonth(dtoAPTransactionEntry.getCreditCardExpireMonth());
			apytdOpenPayments.setCreditCardExpireYear(dtoAPTransactionEntry.getCreditCardExpireYear());
		}
	
		if (dtoAPTransactionEntry.getExchangeTableIndex() != null) {
			apytdOpenPayments.setExchangeTableID(String.valueOf(dtoAPTransactionEntry.getExchangeTableIndex()));
			apytdOpenPayments.setExchangeTableRate(dtoAPTransactionEntry.getExchangeTableRate());
		}
		
		apytdOpenPayments.setPaymentCreateByUserID(apTransactionEntry.getModifyByUserID());
		apytdOpenPayments.setPaymentPostingDate(new Date());
		apytdOpenPayments.setPaymentPostingByUserID(httpServletRequest.getHeader(USER_ID));
		apytdOpenPayments.setPaymentVoid(dtoAPTransactionEntry.isTransactionVoid());
		apytdOpenPayments.setRowDateIndex(new Date());
		repositoryAPYTDOpenPayments.saveAndFlush(apytdOpenPayments);
		//AR90201
		List<APYTDOpenTransactionDistribution> apytdOpenTransactionsDistributionList = 
				repositoryAPYTDOpenTransactionDistribution.findByApTransactionNumber(dtoAPTransactionEntry.getApTransactionNumber());
		if(apytdOpenTransactionsDistributionList!=null && !apytdOpenTransactionsDistributionList.isEmpty()){
			for (APYTDOpenTransactionDistribution arytdOpenTransactionsDistribution : apytdOpenTransactionsDistributionList) {
				APYTDOpenPaymentsDistribution apytdOpenPaymentsDistribution=new APYTDOpenPaymentsDistribution();
				apytdOpenPaymentsDistribution.setPaymentNumber(apTransactionEntry.getCashReceiptNumber());
				apytdOpenPaymentsDistribution.setAccountTableRowIndex(arytdOpenTransactionsDistribution.getAccountTableRowIndex());
				apytdOpenPaymentsDistribution.setDistributionDescription(arytdOpenTransactionsDistribution.getDistributionDescription());
				apytdOpenPaymentsDistribution.setRowDateIndex(new Date());
				apytdOpenPaymentsDistribution.setTransactionType(arytdOpenTransactionsDistribution.getTransactionType());
				apytdOpenPaymentsDistribution.setOriginalDebitAmount(arytdOpenTransactionsDistribution.getOriginalDebitAmount());
				apytdOpenPaymentsDistribution.setDebitAmount(arytdOpenTransactionsDistribution.getDebitAmount());
				apytdOpenPaymentsDistribution.setOriginalCreditAmount(arytdOpenTransactionsDistribution.getOriginalCreditAmount());
				apytdOpenPaymentsDistribution.setCreditAmount(arytdOpenTransactionsDistribution.getCreditAmount());
				repositoryAPYTDOpenPaymentsDistribution.saveAndFlush(apytdOpenPaymentsDistribution);
			}
		}
	}
	
	public List<APYTDOpenTransactionDistribution> saveOpenTransactionDistributionDetailFromTransactionDistributionDetail(List<APTransactionDistribution> apTransactionEntryDistributionList,
			DtoAPTransactionEntry dtoAPTransactionEntry)
	{
		List<APYTDOpenTransactionDistribution> list = new ArrayList<>();
		for (APTransactionDistribution apTransactionDistribution : apTransactionEntryDistributionList) 
		{
		    GLAccountsTableAccumulation glAccountsTableAccumulation= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(apTransactionDistribution.getAccountTableRowIndex()), false);
		    if(glAccountsTableAccumulation!=null)
		    {
		    	APYTDOpenTransactionDistribution apytdOpenTransactionsDistribution=new APYTDOpenTransactionDistribution();
				apytdOpenTransactionsDistribution.setApTransactionNumber(apTransactionDistribution.getApTransactionNumber());
				apytdOpenTransactionsDistribution.setVendorID(dtoAPTransactionEntry.getVendorID());
				apytdOpenTransactionsDistribution.setAccountTableRowIndex(apTransactionDistribution.getAccountTableRowIndex());
				apytdOpenTransactionsDistribution.setDistributionDescription(apTransactionDistribution.getDistributionDescription());
				apytdOpenTransactionsDistribution.setRowDateIndex(new Date());
				apytdOpenTransactionsDistribution.setTransactionType(apTransactionDistribution.getTransactionType());
				apytdOpenTransactionsDistribution.setOriginalCreditAmount(apTransactionDistribution.getOriginalCreditAmount());
				apytdOpenTransactionsDistribution.setOriginalDebitAmount(apTransactionDistribution.getOriginalDebitAmount());
				apytdOpenTransactionsDistribution.setCreditAmount(apTransactionDistribution.getCreditAmount());
				apytdOpenTransactionsDistribution.setDebitAmount(apTransactionDistribution.getDebitAmount());
				list.add(apytdOpenTransactionsDistribution);
		    }
		    else{
		    	return new ArrayList<>();
		    }
		}
		return list;
	}
	
	public List<APYTDOpenTransactionDistribution> saveOpenTransactionDistributionDetail(DtoAPTransactionEntry dtoAPTransactionEntry, String calcMethod, ScriptEngine mathEval) throws ScriptException
	{
		List<APYTDOpenTransactionDistribution> list = new ArrayList<>();
		//DtoAPCashReceiptDistribution dtoAPDistribution = getTransactionalDistributionByTransactionNumber(dtoAPTransactionEntry);
		dtoAPTransactionEntry.setButtonType(MessageLabel.NEW_DISTRIBUTION);
		DtoAPCashReceiptDistribution dtoAPDistribution = getTransactionalDistributionDetail(dtoAPTransactionEntry);
		if(dtoAPDistribution!=null && dtoAPDistribution.getListDtoAPDistributionDetail()!=null && 
				!dtoAPDistribution.getListDtoAPDistributionDetail().isEmpty())
		{
			for (DtoAPDistributionDetail apTransactionDistribution : dtoAPDistribution.getListDtoAPDistributionDetail()) 
			{
			    	if(UtilRandomKey.isNotBlank(apTransactionDistribution.getAccountTableRowIndex()) && 
			    			UtilRandomKey.isNotBlank(apTransactionDistribution.getAccountNumber()))
			    	{
				    	APYTDOpenTransactionDistribution apytdOpenTransactionsDistribution=new APYTDOpenTransactionDistribution();
						apytdOpenTransactionsDistribution.setApTransactionNumber(dtoAPTransactionEntry.getApTransactionNumber());
						apytdOpenTransactionsDistribution.setVendorID(dtoAPTransactionEntry.getVendorID());
						apytdOpenTransactionsDistribution.setAccountTableRowIndex(Integer.parseInt(apTransactionDistribution.getAccountTableRowIndex()));
						apytdOpenTransactionsDistribution.setDistributionDescription(apTransactionDistribution.getDistributionReference());
						apytdOpenTransactionsDistribution.setRowDateIndex(new Date());
						apytdOpenTransactionsDistribution.setTransactionType(apTransactionDistribution.getTypeId());
						Double debitAmount=0.0;
						Double creditAmount=0.0;
						apytdOpenTransactionsDistribution.setOriginalCreditAmount(0.0);
						apytdOpenTransactionsDistribution.setOriginalDebitAmount(0.0);
						if(apTransactionDistribution.getCreditAmount()!=null && apTransactionDistribution.getCreditAmount()>0)
						{
							apytdOpenTransactionsDistribution.setOriginalCreditAmount(apTransactionDistribution.getCreditAmount());
							if(dtoAPTransactionEntry.getExchangeTableRate()!=null && dtoAPTransactionEntry.getExchangeTableRate()>0){
								creditAmount = (double) mathEval.eval(apTransactionDistribution.getCreditAmount() + calcMethod + dtoAPTransactionEntry.getExchangeTableRate());
							}
						}
						
						if(apTransactionDistribution.getDebitAmount()!=null && apTransactionDistribution.getDebitAmount()>0)
						{
							apytdOpenTransactionsDistribution.setOriginalDebitAmount(apTransactionDistribution.getDebitAmount());
							if(dtoAPTransactionEntry.getExchangeTableRate()!=null && dtoAPTransactionEntry.getExchangeTableRate()>0){
								debitAmount = (double) mathEval.eval(apTransactionDistribution.getDebitAmount() + calcMethod + dtoAPTransactionEntry.getExchangeTableRate());
							}
						}
						apytdOpenTransactionsDistribution.setCreditAmount(creditAmount);
						apytdOpenTransactionsDistribution.setDebitAmount(debitAmount);
						list.add(apytdOpenTransactionsDistribution);
			      }
			      else
			      {
			    	return new ArrayList<>();
			      }
			}
		}
		return list;
	}
	
	public void saveArSummaryBalanceInPostTrans(DtoAPTransactionEntry dtoAPTransactionEntry, Double totalAmount){
		APSummaryBalances apSummaryBalanceSender = repositoryAPSummaryBalance.
				findByVendorNumberAndApTransactionType(dtoAPTransactionEntry.getVendorID(),APTransactionTypeConstant.PURCHASE.getIndex());
		if(apSummaryBalanceSender==null){
			apSummaryBalanceSender= new APSummaryBalances();
		}
		apSummaryBalanceSender.setVendorNumber(dtoAPTransactionEntry.getVendorID());
		apSummaryBalanceSender.setApTransactionType(APTransactionTypeConstant.PURCHASE.getIndex());
		apSummaryBalanceSender.setTotalAmount(apSummaryBalanceSender.getTotalAmount()+totalAmount);
		apSummaryBalanceSender.setLastUpdateDate(new Date());
		apSummaryBalanceSender.setLastPostingDateLastTransaction(new Date());
		apSummaryBalanceSender.setRowDateIndex(new Date());
		repositoryAPSummaryBalance.saveAndFlush(apSummaryBalanceSender);
		
		APSummaryBalances apSummaryReceiver = repositoryAPSummaryBalance.
				findByVendorNumberAndApTransactionType(dtoAPTransactionEntry.getVendorID(),APTransactionTypeConstant.PAYABLE.getIndex());
		if(apSummaryReceiver==null){
			apSummaryReceiver= new APSummaryBalances();
		}
		apSummaryReceiver.setVendorNumber(dtoAPTransactionEntry.getVendorID());
		apSummaryReceiver.setApTransactionType(APTransactionTypeConstant.PAYABLE.getIndex());
		apSummaryReceiver.setTotalAmount(apSummaryReceiver.getTotalAmount()+totalAmount);
		apSummaryReceiver.setLastUpdateDate(new Date());
		apSummaryReceiver.setLastPostingDateLastTransaction(new Date());
		apSummaryReceiver.setRowDateIndex(new Date());
		repositoryAPSummaryBalance.saveAndFlush(apSummaryReceiver);
		
		if(dtoAPTransactionEntry.getIsPayment()){
			APSummaryBalances apSummaryPayment = repositoryAPSummaryBalance.
					findByVendorNumberAndApTransactionType(dtoAPTransactionEntry.getVendorID(),APTransactionTypeConstant.PAYMENTS.getIndex());
			if(apSummaryPayment==null){
				apSummaryPayment= new APSummaryBalances();
			}
			apSummaryPayment.setVendorNumber(dtoAPTransactionEntry.getVendorID());
			apSummaryPayment.setApTransactionType(APTransactionTypeConstant.PAYMENTS.getIndex());
			apSummaryPayment.setTotalAmount(apSummaryPayment.getTotalAmount()+Double.valueOf(dtoAPTransactionEntry.getPaymentAmount()));
			apSummaryPayment.setLastUpdateDate(new Date());
			apSummaryPayment.setLastPostingDateLastTransaction(new Date());
			apSummaryPayment.setRowDateIndex(new Date());
			repositoryAPSummaryBalance.saveAndFlush(apSummaryPayment);
		}
		
	}
	
	public void saveArBalanceByPeriodsInPostTrans(DtoAPTransactionEntry dtoAPTransactionEntry, Double totalAmount){
		APBalanceByPeriods apBalancebyPeriods = null;
		int periodId=0;
		Calendar cal = Calendar.getInstance();
		if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getApTransactionDate())) {
			Date transDate = UtilDateAndTime
					.ddmmyyyyStringToDate(dtoAPTransactionEntry.getApTransactionDate());
			cal.setTime(transDate);
			periodId = cal.get(Calendar.MONTH)+1;
		}
			
		apBalancebyPeriods = repositoryAPBalanceByPeriods.
				findByVendorNumberAndPeriodIndexing(dtoAPTransactionEntry.getVendorID(), periodId);
		if(apBalancebyPeriods==null){
			apBalancebyPeriods = new APBalanceByPeriods();
		}
		
		apBalancebyPeriods.setVendorNumber(dtoAPTransactionEntry.getVendorID());
		apBalancebyPeriods.setPeriodIndexing(periodId);
		apBalancebyPeriods.setPeriodDays(0);
		if(dtoAPTransactionEntry.getIsPayment()){
			apBalancebyPeriods.setTotalBalancePayments(apBalancebyPeriods.getTotalBalancePayments()+Double.valueOf(dtoAPTransactionEntry.getPaymentAmount()));
		}
		else
		{
			apBalancebyPeriods.setTotalBalanceAmount(apBalancebyPeriods.getTotalBalanceAmount()+totalAmount);
		}
		
		apBalancebyPeriods.setRowDateIndex(new Date());
		repositoryAPBalanceByPeriods.saveAndFlush(apBalancebyPeriods);
	}
	
	
	public void saveOpenTransactionInPostTrans(DtoAPTransactionEntry dtoAPTransactionEntry, APTransactionEntry apTransactionEntry)
	{
		APYTDOpenTransaction apytdOpenTransactions= new APYTDOpenTransaction();
		apytdOpenTransactions.setApTransactionNumber(dtoAPTransactionEntry.getApTransactionNumber());
		apytdOpenTransactions.setVendorID(dtoAPTransactionEntry.getVendorID());
		apytdOpenTransactions.setApTransactionType(dtoAPTransactionEntry.getApTransactionType());
		apytdOpenTransactions.setApTransactionDescription(dtoAPTransactionEntry.getApTransactionDescription());
		apytdOpenTransactions.setApTransactionDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoAPTransactionEntry.getApTransactionDate()));
		apytdOpenTransactions.setVendorName(dtoAPTransactionEntry.getVendorName());
		apytdOpenTransactions.setVendorNameArabic(dtoAPTransactionEntry.getVendorNameArabic());
		apytdOpenTransactions.setCurrencyID(dtoAPTransactionEntry.getCurrencyID());
		apytdOpenTransactions.setPaymentTermsID(dtoAPTransactionEntry.getPaymentTermsID());
		apytdOpenTransactions.setDocumentNumberOfVendor(dtoAPTransactionEntry.getDocumentNumberOfVendor());
		apytdOpenTransactions.setShippingMethod(dtoAPTransactionEntry.getShippingMethodId());
		apytdOpenTransactions.setVatScheduleID(dtoAPTransactionEntry.getVatScheduleID());
		
		// Set Transaction Amount
		apytdOpenTransactions.setApTransactionFreightAmount(dtoAPTransactionEntry.getApTransactionFreightAmount() != null
						? dtoAPTransactionEntry.getApTransactionFreightAmount() : 0.0);
		apytdOpenTransactions.setApTransactionCashAmount(dtoAPTransactionEntry.getApTransactionCashAmount() != null
				? dtoAPTransactionEntry.getApTransactionCashAmount() : 0.0);
		apytdOpenTransactions.setApTransactionCheckAmount(dtoAPTransactionEntry.getApTransactionCheckAmount() != null
				? dtoAPTransactionEntry.getApTransactionCheckAmount() : 0.0);
		apytdOpenTransactions
				.setApTransactionCreditCardAmount(dtoAPTransactionEntry.getApTransactionCreditCardAmount() != null
						? dtoAPTransactionEntry.getApTransactionCreditCardAmount() : 0.0);
		apytdOpenTransactions
				.setApTransactionCreditMemoAmount(dtoAPTransactionEntry.getApTransactionCreditMemoAmount() != null
						? dtoAPTransactionEntry.getApTransactionCreditMemoAmount() : 0.0);
		apytdOpenTransactions
				.setApTransactionDebitMemoAmount(dtoAPTransactionEntry.getApTransactionDebitMemoAmount() != null
						? dtoAPTransactionEntry.getApTransactionDebitMemoAmount() : 0.0);
		apytdOpenTransactions.setApTransactionFinanceChargeAmount(
				dtoAPTransactionEntry.getApTransactionFinanceChargeAmount() != null
						? dtoAPTransactionEntry.getApTransactionFinanceChargeAmount() : 0.0);
		apytdOpenTransactions
				.setApTransactionWarrantyAmount(dtoAPTransactionEntry.getApTransactionWarrantyAmount() != null
						? dtoAPTransactionEntry.getApTransactionWarrantyAmount() : 0.0);
		// Set Other Amounts
		apytdOpenTransactions.setPurchasesAmount(dtoAPTransactionEntry.getPurchasesAmount());
		apytdOpenTransactions
				.setApTransactionTradeDiscount(dtoAPTransactionEntry.getApTransactionTradeDiscount() != null
						? dtoAPTransactionEntry.getApTransactionTradeDiscount() : 0.0);
		apytdOpenTransactions
				.setApTransactionMiscellaneous(dtoAPTransactionEntry.getApTransactionMiscellaneous() != null
						? dtoAPTransactionEntry.getApTransactionMiscellaneous() : 0.0);
		apytdOpenTransactions.setApTransactionVATAmount(dtoAPTransactionEntry.getApTransactionVATAmount() != null
				? dtoAPTransactionEntry.getApTransactionVATAmount() : 0.0);
		apytdOpenTransactions.setApTransactionTotalAmount(dtoAPTransactionEntry.getApTransactionTotalAmount() != null
				? dtoAPTransactionEntry.getApTransactionTotalAmount() : 0.0);
		
		apytdOpenTransactions.setApServiceRepairAmount(dtoAPTransactionEntry.getApServiceRepairAmount()!=null?
				dtoAPTransactionEntry.getApServiceRepairAmount():0.0);
		apytdOpenTransactions.setApReturnAmount(dtoAPTransactionEntry.getApReturnAmount()!=null?
				dtoAPTransactionEntry.getApReturnAmount():0.0);
		
		apytdOpenTransactions.setCheckbookID(dtoAPTransactionEntry.getCheckbookID());

		if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCheckNumber())) {
			apytdOpenTransactions.setCheckNumber(dtoAPTransactionEntry.getCheckNumber());
		}

		if (UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getCreditCardID())) {
			apytdOpenTransactions.setCreditCardID(dtoAPTransactionEntry.getCreditCardID());
			apytdOpenTransactions.setCreditCardNumber(dtoAPTransactionEntry.getCreditCardNumber());
			apytdOpenTransactions.setCreditCardExpireMonth(dtoAPTransactionEntry.getCreditCardExpireMonth());
			apytdOpenTransactions.setCreditCardExpireYear(dtoAPTransactionEntry.getCreditCardExpireYear());

		}
		
		if (dtoAPTransactionEntry.getExchangeTableIndex() != null) {
			apytdOpenTransactions.setExchangeTableIndex(dtoAPTransactionEntry.getExchangeTableIndex());
			apytdOpenTransactions.setExchangeTableRate(dtoAPTransactionEntry.getExchangeTableRate());
		}
		apytdOpenTransactions.setPayment(false);
		if(dtoAPTransactionEntry.getIsPayment())
		{
			apytdOpenTransactions.setPayment(true);
			apytdOpenTransactions.setCashReceiptNumber(apTransactionEntry.getCashReceiptNumber());
		}
		
		apytdOpenTransactions.setTransactionVoid(dtoAPTransactionEntry.isTransactionVoid());
		apytdOpenTransactions.setRowDateIndex(new Date());
		apytdOpenTransactions.setPostingDate(new Date());
		repositoryAPYTDOpenTransaction.saveAndFlush(apytdOpenTransactions);
	}
	
	
	
}
