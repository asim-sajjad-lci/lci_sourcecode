package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.MasterVendorStatus;
import com.bti.model.VendorClassesSetup;
import com.bti.model.VendorMaintenance;
import com.bti.model.VendorMaintenanceOptions;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoVendor;
import com.bti.model.dto.DtoVendorMaintenanceOptions;
import com.bti.model.dto.DtoVendorStatus;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryHoldVendorStatus;
import com.bti.repository.RepositoryMasterVendorStatus;
import com.bti.repository.RepositoryPaymentTermSetup;
import com.bti.repository.RepositoryShipmentMethodSetup;
import com.bti.repository.RepositoryVATSetup;
import com.bti.repository.RepositoryVendorClassesSetup;
import com.bti.repository.RepositoryVendorMaintenance;
import com.bti.repository.RepositoryVendorMaintenanceOptions;
import com.bti.util.UtilRandomKey;

@Service("serviceVender")
public class ServiceVender {
	
	static final Logger LOG = Logger.getLogger(ServiceResponse.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryVendorMaintenance repositoryVendorMaintenance;

	@Autowired
	RepositoryMasterVendorStatus repositoryMasterVendorStatus;

	@Autowired
	RepositoryHoldVendorStatus repositoryHoldVendorStatus;

	@Autowired
	RepositoryVendorMaintenanceOptions repositoryVendorMaintenanceOptions;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	RepositoryVATSetup repositoryVATSetup;

	@Autowired
	RepositoryShipmentMethodSetup repositoryShipmentMethodSetup;

	@Autowired
	RepositoryPaymentTermSetup repositoryPaymentTermSetup;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryVendorClassesSetup repositoryVendorClassesSetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @param dtoVender
	 * @return
	 */
	public DtoVendor saveOrUpdateVender(DtoVendor dtoVender) {
		// Getting logged in user id
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langId = serviceHome.getLanngugaeId();
		VendorClassesSetup vendorClassesSetup = repositoryVendorClassesSetup.findByVendorClassIdAndIsDeleted(dtoVender.getClassId(),false);
		VendorMaintenance vendorMaintenance = null;
		try {
			if (dtoVender.getVenderId() != null) {
				// Getting already saved object for update
				vendorMaintenance = repositoryVendorMaintenance.findOne(dtoVender.getVenderId());
				if(vendorMaintenance == null){
					// Adding new vender values
					vendorMaintenance = new VendorMaintenance();
					vendorMaintenance.setCreatedBy(loggedInUserId);
				}else{
					// Updating vendor values
					vendorMaintenance.setChangeBy(loggedInUserId);
					if(UtilRandomKey.isNotBlank(dtoVender.getPriority())){
						vendorMaintenance.setVendorPriority(dtoVender.getPriority());
					}
					else{
						vendorMaintenance.setVendorPriority("");
					}
				}
					vendorMaintenance.setVendorid(dtoVender.getVenderId());
					vendorMaintenance.setVendorName(dtoVender.getVenderNamePrimary());
					vendorMaintenance.setVendorNameArabic(dtoVender.getVenderNameSecondary());
					vendorMaintenance.setMasterVendorStatus(repositoryMasterVendorStatus.findByStatusIdAndLanguageLanguageIdAndIsDeleted(dtoVender.getVenderStatus(),langId,false));
					vendorMaintenance.setVendorShortName(dtoVender.getShortName());
					vendorMaintenance.setVendorClassesSetup(vendorClassesSetup!=null?vendorClassesSetup:null);
					vendorMaintenance.setVendorStatementName(dtoVender.getStatementName());
					vendorMaintenance.setAddress1(dtoVender.getAddress1());
					vendorMaintenance.setAddress2(dtoVender.getAddress2());
					vendorMaintenance.setAddress3(dtoVender.getAddress3());
					vendorMaintenance.setPhoneNumber1(dtoVender.getPhone1());
					vendorMaintenance.setPhoneNumber2(dtoVender.getPhone2());
					vendorMaintenance.setPhoneNumber3(dtoVender.getPhone3());
					vendorMaintenance.setFaxNumber(dtoVender.getFax());
					vendorMaintenance.setUserDefine1(dtoVender.getUserDefine1());
					vendorMaintenance.setUserDefine2(dtoVender.getUserDefine2());
					vendorMaintenance.setCounrty(dtoVender.getCountry());
					vendorMaintenance.setState(dtoVender.getState());
					vendorMaintenance.setCity(dtoVender.getCity());
					repositoryVendorMaintenance.saveAndFlush(vendorMaintenance);
				 
			}
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		MasterVendorStatus masterVendorStatus= vendorMaintenance!=null?vendorMaintenance.getMasterVendorStatus():null;
		if(masterVendorStatus!=null && masterVendorStatus.getLanguage().getLanguageId()!=langId){
			masterVendorStatus= repositoryMasterVendorStatus.findByStatusIdAndLanguageLanguageIdAndIsDeleted(masterVendorStatus.getStatusId(), langId,false);
		}
		return new DtoVendor(vendorMaintenance,masterVendorStatus);
	}

	/**
	 * @param venderId
	 * @return
	 */
	public boolean getVenderByVenderId(String venderId) {
		boolean status = false;
		try {
			VendorMaintenance vMain = repositoryVendorMaintenance.findOne(venderId);
			if (vMain != null) {
				status = true;
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return status;
	}

	/**
	 * @param venderId
	 * @return
	 */
	public DtoVendor getVendorById(String venderId) 
	{
		int langId= serviceHome.getLanngugaeId();
		VendorMaintenance vMain = repositoryVendorMaintenance.findOne(venderId);
		if(vMain!=null){
			MasterVendorStatus masterVendorStatus= vMain.getMasterVendorStatus();
			if(masterVendorStatus!=null && masterVendorStatus.getLanguage().getLanguageId()!=langId){
				masterVendorStatus= repositoryMasterVendorStatus.findByStatusIdAndLanguageLanguageIdAndIsDeleted(masterVendorStatus.getStatusId(), langId,false);
			}
			return new DtoVendor(vMain,masterVendorStatus);
		}
		return null;
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch getAllOrSearchVendor(DtoSearch dtoSearch) {
		int langId= serviceHome.getLanngugaeId();
		List<DtoVendor> venders = new ArrayList<>();
		DtoVendor dtoVendor;
		List<VendorMaintenance> vlist;
		dtoSearch.setTotalCount(repositoryVendorMaintenance.predictiveVendorSearchCount(dtoSearch.getSearchKeyword()));
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			vlist = repositoryVendorMaintenance.predictiveVendorSearchWithPagination(dtoSearch.getSearchKeyword(),
					pageable);
		} else {
			vlist = repositoryVendorMaintenance.predictiveVendorSearch(dtoSearch.getSearchKeyword());
		}
		if (!vlist.isEmpty()) {
			for (VendorMaintenance vendorMaintenance : vlist) {
				MasterVendorStatus masterVendorStatus= vendorMaintenance.getMasterVendorStatus();
				if(masterVendorStatus!=null && masterVendorStatus.getLanguage().getLanguageId()!=langId){
					masterVendorStatus= repositoryMasterVendorStatus.findByStatusIdAndLanguageLanguageIdAndIsDeleted(masterVendorStatus.getStatusId(), langId,false);
				}
				 
				dtoVendor = new DtoVendor(vendorMaintenance,masterVendorStatus);
				venders.add(dtoVendor);
			}
		}
		
		dtoSearch.setRecords(venders);
		return dtoSearch;
	}

	/**
	 * @return
	 */
	public List<DtoVendorStatus> getAllVendorStatus() {
		List<DtoVendorStatus> vendorStatusList = new ArrayList<>();
		DtoVendorStatus dtoVendorStatus;
		List<MasterVendorStatus> masterVendorStatus = repositoryMasterVendorStatus.findByIsDeleted(false);
		for (MasterVendorStatus vendorStatus : masterVendorStatus) {
			dtoVendorStatus = new DtoVendorStatus(vendorStatus);
			vendorStatusList.add(dtoVendorStatus);
		}
		return vendorStatusList;
	}

	/**
	 * @param dtoVendorMaintenanceOptions
	 * @return
	 */
	public DtoVendorMaintenanceOptions saveVendorMaintenanceOptions(
			DtoVendorMaintenanceOptions dtoVendorMaintenanceOptions) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			VendorMaintenanceOptions vendorMaintenanceOptions = repositoryVendorMaintenanceOptions.findByVendorMaintenanceVendoridAndIsDeleted(dtoVendorMaintenanceOptions.getVendorId(),false);
			if (vendorMaintenanceOptions != null) {
				dtoVendorMaintenanceOptions.setMessageType("VENDOR_MAINTENANCE_OPTION_UPDATED_SUCCESSFLLY");
				vendorMaintenanceOptions.setChangeBy(loggedInUserId);
			} 
			else 
			{
				vendorMaintenanceOptions = new VendorMaintenanceOptions();
				vendorMaintenanceOptions.setCreatedBy(loggedInUserId);
				dtoVendorMaintenanceOptions.setMessageType("VENDOR_MAINTENANCE_OPTION_SAVED_SUCCESSFLLY");

				VendorMaintenance vendorMaintenance = repositoryVendorMaintenance
						.findByVendoridAndIsDeleted(dtoVendorMaintenanceOptions.getVendorId(),false);
				if (vendorMaintenance != null) {
					vendorMaintenanceOptions.setVendorMaintenance(vendorMaintenance);
				}
			}
			vendorMaintenanceOptions.setCreditLimit(dtoVendorMaintenanceOptions.getCreditLimit());
			vendorMaintenanceOptions.setCardLimitAmount(dtoVendorMaintenanceOptions.getCardLimitAmount());
			vendorMaintenanceOptions.setCurrencySetup(
					repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoVendorMaintenanceOptions.getCurrencyId(),false));
			vendorMaintenanceOptions.setMaximumInvoiceAmount(dtoVendorMaintenanceOptions.getMaximumInvoiceAmount());
			vendorMaintenanceOptions.setMinimumCharge(dtoVendorMaintenanceOptions.getMinimumCharge());
			vendorMaintenanceOptions.setMinimumChargeAmount(dtoVendorMaintenanceOptions.getMinimumChargeAmount());
			vendorMaintenanceOptions.setMinimumOrderAmount(dtoVendorMaintenanceOptions.getMinimumOrderAmount());
			vendorMaintenanceOptions
					.setMaximumInvoiceAmountValue(dtoVendorMaintenanceOptions.getMaximumInvoiceAmountValue());
			vendorMaintenanceOptions.setOpenMaintenanceHistoryCalendarYear(
					dtoVendorMaintenanceOptions.getOpenMaintenanceHistoryCalendarYear());
			vendorMaintenanceOptions.setOpenMaintenanceHistoryDistribution(
					dtoVendorMaintenanceOptions.getOpenMaintenanceHistoryDistribution());
			vendorMaintenanceOptions.setOpenMaintenanceHistoryFiscalYear(
					dtoVendorMaintenanceOptions.getOpenMaintenanceHistoryFiscalYear());
			vendorMaintenanceOptions.setOpenMaintenanceHistoryTransaction(
					dtoVendorMaintenanceOptions.getOpenMaintenanceHistoryTransaction());
			vendorMaintenanceOptions.setTradeDiscountPercent(dtoVendorMaintenanceOptions.getTradeDiscountPercent());
			vendorMaintenanceOptions.setUserDefine1(dtoVendorMaintenanceOptions.getUserDefine1());
			vendorMaintenanceOptions.setUserDefine2(dtoVendorMaintenanceOptions.getUserDefine2());
			vendorMaintenanceOptions.setUserDefine3(dtoVendorMaintenanceOptions.getUserDefine3());
			vendorMaintenanceOptions.setVatSetup(
					repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoVendorMaintenanceOptions.getVatScheduleId(),false));
			vendorMaintenanceOptions.setShipmentMethodSetup(repositoryShipmentMethodSetup
					.findByShipmentMethodIdAndIsDeleted(dtoVendorMaintenanceOptions.getShipmentMethodId(),false));
			vendorMaintenanceOptions.setPaymentTermsSetup(
					repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(dtoVendorMaintenanceOptions.getPaymentTermId(),false));
			vendorMaintenanceOptions.setCheckbookMaintenance(
					repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoVendorMaintenanceOptions.getCheckBookId(),false));
			repositoryVendorMaintenanceOptions.save(vendorMaintenanceOptions);
			return dtoVendorMaintenanceOptions;

		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @return
	 */
	public DtoVendorMaintenanceOptions getVendorMaintenanceOptions() {
		try {
			List<VendorMaintenanceOptions> list = repositoryVendorMaintenanceOptions.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				return new DtoVendorMaintenanceOptions(list.get(0));
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}
	
	public DtoVendorMaintenanceOptions getVendorMaintenanceOptionsByVendorId(String vendorId) {
		try {
			DtoVendorMaintenanceOptions dtoVendorMaintenanceOptions=null;
			VendorMaintenanceOptions vendorMaintenanceOptions = repositoryVendorMaintenanceOptions.
					findByVendorMaintenanceVendoridAndIsDeleted(vendorId,false);
			if(vendorMaintenanceOptions!=null){
				dtoVendorMaintenanceOptions= new DtoVendorMaintenanceOptions(vendorMaintenanceOptions);
				return dtoVendorMaintenanceOptions;
			}
			else
			{
				VendorMaintenance vendorMaintenance=repositoryVendorMaintenance.findByVendoridAndIsDeleted(vendorId, false);
				if(vendorMaintenance!=null && vendorMaintenance.getVendorClassesSetup()!=null){
						dtoVendorMaintenanceOptions= new DtoVendorMaintenanceOptions(vendorMaintenance);
						return dtoVendorMaintenanceOptions;
				}
				else{
					return null;
				}
			}
			
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}
	
	public DtoSearch getAllOrSearchActiveVendor(DtoSearch dtoSearch) {
		int langId= serviceHome.getLanngugaeId();
		List<DtoVendor> venders = new ArrayList<>();
		DtoVendor dtoVendor;
		List<VendorMaintenance> vlist;
		
		dtoSearch.setTotalCount(repositoryVendorMaintenance.predictiveActiveVendorSearchCount(dtoSearch.getSearchKeyword()));

		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			vlist = repositoryVendorMaintenance.predictiveActiveVendorSearchWithPagination(dtoSearch.getSearchKeyword(),
					pageable);
		} else {
			vlist = repositoryVendorMaintenance.predictiveActiveVendorSearch(dtoSearch.getSearchKeyword());
		}
		if (!vlist.isEmpty()) {
			for (VendorMaintenance vendorMaintenance : vlist) {
				MasterVendorStatus masterVendorStatus= vendorMaintenance.getMasterVendorStatus();
				if(masterVendorStatus!=null && masterVendorStatus.getLanguage().getLanguageId()!=langId){
					masterVendorStatus= repositoryMasterVendorStatus.findByStatusIdAndLanguageLanguageIdAndIsDeleted(masterVendorStatus.getStatusId(), langId,false);
				}
				dtoVendor = new DtoVendor(vendorMaintenance,masterVendorStatus);
				venders.add(dtoVendor);
			}
		}
		
		dtoSearch.setRecords(venders);
		return dtoSearch;
	}
	
}
