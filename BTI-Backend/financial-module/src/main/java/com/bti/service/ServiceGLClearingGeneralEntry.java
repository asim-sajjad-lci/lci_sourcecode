/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLClearingDetails;
import com.bti.model.GLClearingHeader;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumber;
import com.bti.model.GLCurrentSummaryMasterTableByAccountNumberKey;
import com.bti.model.GLCurrentSummaryMasterTableByDimensions;
import com.bti.model.GLCurrentSummaryMasterTableByDimensionsKey;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccount;
import com.bti.model.GLCurrentSummaryMasterTableByMainAccountKey;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.dto.DtoJournalEntryDetail;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLClearingDetail;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByDimensionIndex;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByMainAccount;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceGLClearingGeneralEntry Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceGLClearingGeneralEntry")
public class ServiceGLClearingGeneralEntry {

	private static final Logger LOG = Logger.getLogger(ServiceGLClearingGeneralEntry.class);
	private

	@Autowired(required = false) HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;

	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;

	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;

	@Autowired
	RepositoryGLClearingDetail repositoryGLClearingDetail;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	ServiceAccountPayable serviceAccountPayable;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByMainAccount repositoryGLCurrentSummaryMasterTableByMainAccount;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByDimensionIndex repositoryGLCurrentSummaryMasterTableByDimensionIndex;

	/**
	 * @description this service will use to get account number for clearing Jv
	 *              entry
	 * @return
	 */
	public List<DtoPayableAccountClassSetup> getAccountNumberForClearingJVEntry() {
		List<Integer> accNumbersList = repositoryGLCurrentSummaryMasterTableByAccountNumber.getDistnictAccountNumbers();
		List<DtoPayableAccountClassSetup> accNumberList = new ArrayList<>();
		if (accNumbersList != null && !accNumbersList.isEmpty()) {
			for (Integer accRowIndex : accNumbersList) {
				String accTableRowIndex = String.valueOf(accRowIndex);
				GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
						.findByAccountTableRowIndexAndIsDeleted(accTableRowIndex, false);
				if (glAccountsTableAccumulation != null) {
					DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
					dtoPayableAccountClassSetup.setAccountTableRowIndex(accTableRowIndex);
					String accountNumber = serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation)[0].toString();
					dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
					accNumberList.add(dtoPayableAccountClassSetup);
				}
			}
		}
		return accNumberList;
	}

	/**
	 * @description this service will use to get Journal Id list for update
	 *              clearing Jv entry
	 * @return
	 */
	public List<DtoJournalEntryHeader> getDistnictJournalIdForUpdateClearingJvEntry() {
		List<DtoJournalEntryHeader> list = new ArrayList<>();
		List<String> distnicJounalIdList = null;
		DtoJournalEntryHeader dtoJournalEntryHeader = null;

		distnicJounalIdList = repositoryGLClearingHeader.getDistnictjournalId();
		if (distnicJounalIdList != null && !distnicJounalIdList.isEmpty()) {
			for (String journalId : distnicJounalIdList) {
				dtoJournalEntryHeader = new DtoJournalEntryHeader();
				dtoJournalEntryHeader.setJournalID(journalId);
				list.add(dtoJournalEntryHeader);
			}
		}

		return list;
	}

	/**
	 * @description this service will use to save and update clearing journal
	 *              entry
	 * @param dtoJournalEntryHeader
	 * @param action
	 * @return
	 */
	public DtoJournalEntryHeader saveAndUpdateClearingJournalEntry(DtoJournalEntryHeader dtoJournalEntryHeader,
			String action) {
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		GLClearingHeader glClearingHeader = null;
		GLConfigurationSetup glConfigurationSetup = null;
		int nextJournalId = 0;

		if (action.equalsIgnoreCase(CommonConstant.SAVE)) {
			glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
			if (glConfigurationSetup == null) {
				return null;
			}

			glClearingHeader = new GLClearingHeader();
			nextJournalId = glConfigurationSetup.getNextJournalEntryVoucher();
			glClearingHeader.setJournalID(String.valueOf(nextJournalId));
		} else {
			glClearingHeader = this.repositoryGLClearingHeader
					.findByJournalIDAndIsDeleted(dtoJournalEntryHeader.getJournalID(), false);
			if (glClearingHeader == null) {
				return null;
			}

			List<GLClearingDetails> detailList = repositoryGLClearingDetail
					.findByJournalIDAndIsDeleted(glClearingHeader.getJournalID(), false);
			if (detailList != null && !detailList.isEmpty()) {
				repositoryGLClearingDetail.deleteInBatch(detailList);
			}
		}

		glClearingHeader.setBalanceType(dtoJournalEntryHeader.getBalanceType());
		if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate())) {
			glClearingHeader.setTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate()));
		} else {
			glClearingHeader.setTransactionDate(null);
		}

		glClearingHeader.setTransactionDescription(dtoJournalEntryHeader.getTransactionDescription());
		glClearingHeader.setGlBatches(
				repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoJournalEntryHeader.getGlBatchId(),BatchTransactionTypeConstant.CLEARING_ENTRY.getIndex(), false));
		GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
				.findBySeriesIndexAndIsDeleted(dtoJournalEntryHeader.getSourceDocumentId(), false);
		glClearingHeader.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
		glClearingHeader.setModifyByUserID(String.valueOf(loggedInUserId));
		glClearingHeader = repositoryGLClearingHeader.saveAndFlush(glClearingHeader);

		if (action.equalsIgnoreCase(CommonConstant.SAVE) && glConfigurationSetup != null) {
			glConfigurationSetup.setNextJournalEntryVoucher(nextJournalId + 1);
			repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);
		}

		if (dtoJournalEntryHeader.getJournalEntryDetailsList() != null
				&& !dtoJournalEntryHeader.getJournalEntryDetailsList().isEmpty()) {
			for (DtoJournalEntryDetail dtoJournalEntryDetail : dtoJournalEntryHeader.getJournalEntryDetailsList()) {
				GLClearingDetails glClearingDetails = new GLClearingDetails();
				glClearingDetails.setJournalID(glClearingHeader.getJournalID());
				glClearingDetails.setGlAccountsTableAccumulation(
						repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(
								dtoJournalEntryDetail.getAccountTableRowIndex(), false));
				glClearingDetails.setGlAccountsTableAccumulationOffset(
						repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(
								dtoJournalEntryDetail.getAccountTableRowIndexOffset(), false));
				glClearingDetails.setDistributionDescription(dtoJournalEntryDetail.getDistributionDescription());
				glClearingDetails.setModifyByUserID(String.valueOf(loggedInUserId));
				repositoryGLClearingDetail.saveAndFlush(glClearingDetails);
			}
		}
		return setGlJournalClearingInDto(glClearingHeader);
	}

	public DtoJournalEntryHeader postClearingJournalEntry(DtoJournalEntryHeader dtoJournalEntryHeader) {
		try {
			if (dtoJournalEntryHeader.getJournalEntryDetailsList() != null
					&& !dtoJournalEntryHeader.getJournalEntryDetailsList().isEmpty()) {

				for (DtoJournalEntryDetail dtoJournalEntryDetail : dtoJournalEntryHeader.getJournalEntryDetailsList()) {
					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getAccountTableRowIndex())) {
						clearingJournalEntry(Integer.parseInt(dtoJournalEntryDetail.getAccountTableRowIndex()),
								dtoJournalEntryHeader);
					}
					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getAccountTableRowIndexOffset())) {
						clearingJournalEntry(Integer.parseInt(dtoJournalEntryDetail.getAccountTableRowIndexOffset()),
								dtoJournalEntryHeader);
					}
					GLClearingHeader glClearingHeader = repositoryGLClearingHeader
							.findByJournalIDAndIsDeleted(dtoJournalEntryHeader.getJournalID(), false);
					if (glClearingHeader != null) {
						List<GLClearingDetails> listGlClearingDetail = repositoryGLClearingDetail
								.findByJournalIDAndIsDeleted(glClearingHeader.getJournalID(), false);
						if (listGlClearingDetail != null && !listGlClearingDetail.isEmpty()) {
							repositoryGLClearingDetail.deleteInBatch(listGlClearingDetail);
						}
						repositoryGLClearingHeader.delete(glClearingHeader);
					}
				}
				return dtoJournalEntryHeader;
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	public void clearingJournalEntry(int accountTableRowIndex, DtoJournalEntryHeader dtoJournalEntryHeader) {
		try {
			int loggedInUserId = 0;
			if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
				loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}

			Integer year = 0;
			Integer periodId = 0;
			Calendar cal = Calendar.getInstance();
			if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate())) {
				Date transDate = UtilDateAndTime.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate());
				cal.setTime(transDate);
				year = cal.get(Calendar.YEAR);
				periodId = cal.get(Calendar.MONTH) + 1;
			}

			List<Object[]> listTotalDebitCreditAmount = null;
			if (dtoJournalEntryHeader.getBalanceType() == 1) {
				listTotalDebitCreditAmount = repositoryGLCurrentSummaryMasterTableByAccountNumber
						.getTotalDebitCreditAmoutByYTD(accountTableRowIndex, year, periodId);
			} else {
				listTotalDebitCreditAmount = repositoryGLCurrentSummaryMasterTableByAccountNumber
						.getTotalDebitCreditAmoutByPeriod(accountTableRowIndex, year, periodId);
			}
			Double totalDebitAmount = 0.0;
			Double totalCreditAmount = 0.0;
			if (listTotalDebitCreditAmount != null && !listTotalDebitCreditAmount.isEmpty()) {
				for (Object[] object : listTotalDebitCreditAmount) {
					totalDebitAmount = (Double) object[0];
					totalCreditAmount = (Double) object[1];
				}
			}

			Double balance = totalDebitAmount - totalCreditAmount;

			GLYTDOpenTransactions glytdOpenTransactions = new GLYTDOpenTransactions();
			glytdOpenTransactions.setOpenYear(Calendar.getInstance().get(Calendar.YEAR));
			glytdOpenTransactions.setJournalEntryID(dtoJournalEntryHeader.getJournalID());
			GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
					.findBySeriesIndexAndIsDeleted(dtoJournalEntryHeader.getSourceDocumentId(), false);
			glytdOpenTransactions.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
			glytdOpenTransactions.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
			glytdOpenTransactions.setJournalDescription(dtoJournalEntryHeader.getTransactionDescription());
			if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate())) {
				glytdOpenTransactions.setTransactionDate(
						UtilDateAndTime.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate()));
			}

			glytdOpenTransactions.setTransactionPostingDate(new Date());
			glytdOpenTransactions.setTransactionSource("CL");
			glytdOpenTransactions.setAccountTableRowIndex(String.valueOf(accountTableRowIndex));
			glytdOpenTransactions.setUserCreateTransaction(loggedInUserId + "");
			glytdOpenTransactions.setUserPostingTransaction(loggedInUserId + "");
			glytdOpenTransactions.setCreditAmount(totalCreditAmount);
			glytdOpenTransactions.setDebitAmount(totalDebitAmount);
			glytdOpenTransactions.setBalanceType(dtoJournalEntryHeader.getBalanceType());
			glytdOpenTransactions.setCurrencyId("USD");
			repositoryGlytdOpenTransaction.saveAndFlush(glytdOpenTransactions);

			String accountNumberWithZero = null;
			int dimIndex1 = 0;
			int dimIndex2 = 0;
			int dimIndex3 = 0;
			int dimIndex4 = 0;
			int dimIndex5 = 0;
			int mainAccountIndex = 0;
			GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
					.findByAccountTableRowIndexAndIsDeleted(String.valueOf(accountTableRowIndex), false);
			if (glAccountsTableAccumulation != null) {
				accountNumberWithZero = serviceAccountPayable.getAccountNumberIdsWithZero(glAccountsTableAccumulation);
			}

			if (UtilRandomKey.isNotBlank(accountNumberWithZero)) {
				String[] accountNo = accountNumberWithZero.split("-");
				for (int i = 0; i < accountNo.length; i++) {
					if (i > 0) {
						if (i == 1) {
							dimIndex1 = Integer.parseInt(accountNo[i]);
						}
						if (i == 2) {
							dimIndex2 = Integer.parseInt(accountNo[i]);
						}
						if (i == 3) {
							dimIndex3 = Integer.parseInt(accountNo[i]);
						}
						if (i == 4) {
							dimIndex4 = Integer.parseInt(accountNo[i]);
						}
						if (i == 5) {
							dimIndex5 = Integer.parseInt(accountNo[i]);
						}
					}
				}
				mainAccountIndex = Integer.parseInt(accountNo[0]);
			}

			// find by composite key
			GLCurrentSummaryMasterTableByMainAccountKey currSumaryByMainAccKey = new GLCurrentSummaryMasterTableByMainAccountKey();
			currSumaryByMainAccKey.setYear(year);
			currSumaryByMainAccKey.setPeriodID(periodId);
			currSumaryByMainAccKey.setMainAccountIndex(mainAccountIndex);
			GLCurrentSummaryMasterTableByMainAccount currSumaryByMainAcc = repositoryGLCurrentSummaryMasterTableByMainAccount
					.findOne(currSumaryByMainAccKey);
			if (currSumaryByMainAcc != null) {
				currSumaryByMainAcc.setDebitAmount(totalDebitAmount);
				currSumaryByMainAcc.setCreditAmount(totalCreditAmount);
				currSumaryByMainAcc.setPeriodBalance(balance);
			} else {
				currSumaryByMainAcc = new GLCurrentSummaryMasterTableByMainAccount();
				currSumaryByMainAcc.setMainAccountIndex(mainAccountIndex);
				currSumaryByMainAcc.setYear(year);
				currSumaryByMainAcc.setPeriodID(periodId);
				currSumaryByMainAcc.setDebitAmount(totalDebitAmount);
				currSumaryByMainAcc.setCreditAmount(totalCreditAmount);
				currSumaryByMainAcc.setPeriodBalance(balance);
			}

			currSumaryByMainAcc.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByMainAccount.saveAndFlush(currSumaryByMainAcc);

			GLCurrentSummaryMasterTableByAccountNumberKey currSumaryByAccNumberKey = new GLCurrentSummaryMasterTableByAccountNumberKey();
			currSumaryByAccNumberKey.setYear(year);
			currSumaryByAccNumberKey.setAccountTableRowIndex(accountTableRowIndex);
			currSumaryByAccNumberKey.setPeriodID(periodId);
			GLCurrentSummaryMasterTableByAccountNumber currSumaryByAccNumber = repositoryGLCurrentSummaryMasterTableByAccountNumber
					.findOne(currSumaryByAccNumberKey);
			if (currSumaryByAccNumber != null) {
				currSumaryByAccNumber.setDebitAmount(totalDebitAmount);
				currSumaryByAccNumber.setCreditAmount(totalCreditAmount);
				currSumaryByAccNumber.setPeriodBalance(balance);
			} else {
				currSumaryByAccNumber = new GLCurrentSummaryMasterTableByAccountNumber();
				currSumaryByAccNumber.setAccountTableRowIndex(accountTableRowIndex);
				currSumaryByAccNumber.setYear(year);
				currSumaryByAccNumber.setPeriodID(periodId);
				currSumaryByAccNumber.setDebitAmount(totalDebitAmount);
				currSumaryByAccNumber.setCreditAmount(totalCreditAmount);
				currSumaryByAccNumber.setPeriodBalance(balance);
			}
			currSumaryByAccNumber.setIsDeleted(false);
			currSumaryByAccNumber.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByAccountNumber.saveAndFlush(currSumaryByAccNumber);

			GLCurrentSummaryMasterTableByDimensionsKey glCurreSummaryByDimInxKey = new GLCurrentSummaryMasterTableByDimensionsKey();
			glCurreSummaryByDimInxKey.setYear(year);
			glCurreSummaryByDimInxKey.setMainAccountIndex(mainAccountIndex);
			glCurreSummaryByDimInxKey.setPeriodID(periodId);
			GLCurrentSummaryMasterTableByDimensions glCurreSummaryByDimInx = repositoryGLCurrentSummaryMasterTableByDimensionIndex
					.findOne(glCurreSummaryByDimInxKey);
			if (glCurreSummaryByDimInx != null) {
				glCurreSummaryByDimInx.setDebitAmount(totalDebitAmount);
				glCurreSummaryByDimInx.setCreditAmount(totalCreditAmount);
				glCurreSummaryByDimInx.setPeriodBalance(balance);
			} else {
				glCurreSummaryByDimInx = new GLCurrentSummaryMasterTableByDimensions();
				glCurreSummaryByDimInx.setMainAccountIndex(mainAccountIndex);
				glCurreSummaryByDimInx.setYear(year);
				glCurreSummaryByDimInx.setPeriodID(periodId);
				glCurreSummaryByDimInx.setCreditAmount(totalCreditAmount);
				glCurreSummaryByDimInx.setDebitAmount(totalDebitAmount);
				glCurreSummaryByDimInx.setPeriodBalance(balance);
			}

			glCurreSummaryByDimInx.setDimensionIndex1(dimIndex1);
			glCurreSummaryByDimInx.setDimensionIndex2(dimIndex2);
			glCurreSummaryByDimInx.setDimensionIndex3(dimIndex3);
			glCurreSummaryByDimInx.setDimensionIndex4(dimIndex4);
			glCurreSummaryByDimInx.setDimensionIndex5(dimIndex5);
			glCurreSummaryByDimInx.setRowDateIndex(new Date());
			repositoryGLCurrentSummaryMasterTableByDimensionIndex.saveAndFlush(glCurreSummaryByDimInx);

			// clear previous period account numbers , set default value zero in
			// debit and credit amount fields.
			if (dtoJournalEntryHeader.getBalanceType() == 1) {
				repositoryGLCurrentSummaryMasterTableByMainAccount.setDebitCreditAmountZeroYTD(mainAccountIndex, year,
						periodId - 1);
				repositoryGLCurrentSummaryMasterTableByAccountNumber.setDebitCreditAmountZeroYTD(accountTableRowIndex,
						year, periodId - 1);
				repositoryGLCurrentSummaryMasterTableByDimensionIndex.setDebitCreditAmountZeroYTD(mainAccountIndex,
						year, periodId - 1);
			}

		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
	}

	/**
	 * @description this service will use to set data in dto from Object
	 * @param glClearingHeader
	 * @return
	 */
	public DtoJournalEntryHeader setGlJournalClearingInDto(GLClearingHeader glClearingHeader) {
		DtoJournalEntryHeader dtoJournalEntryHeader = new DtoJournalEntryHeader(glClearingHeader);
		List<GLClearingDetails> clearingDetailList = repositoryGLClearingDetail
				.findByJournalIDAndIsDeleted(glClearingHeader.getJournalID(), false);
		List<DtoJournalEntryDetail> detailList = new ArrayList<>();
		if (clearingDetailList != null && !clearingDetailList.isEmpty()) {
			for (GLClearingDetails glClearingDetails : clearingDetailList) {
				detailList.add(new DtoJournalEntryDetail(glClearingDetails));
			}
		}
		dtoJournalEntryHeader.setJournalEntryDetailsList(detailList);
		return dtoJournalEntryHeader;
	}

	public boolean deleteClearingJournalEntryByJournalId(DtoJournalEntryHeader dtoJournalEntryHeader) {
		try {
			GLClearingHeader glClearingHeader = repositoryGLClearingHeader
					.findByJournalIDAndIsDeleted(dtoJournalEntryHeader.getJournalID(), false);
			if (glClearingHeader != null) {
				List<GLClearingDetails> listGlClearingDetail = repositoryGLClearingDetail
						.findByJournalIDAndIsDeleted(glClearingHeader.getJournalID(), false);
				if (listGlClearingDetail != null && !listGlClearingDetail.isEmpty()) {
					repositoryGLClearingDetail.deleteInBatch(listGlClearingDetail);
				}
				repositoryGLClearingHeader.delete(glClearingHeader);
			}
			return true;
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}
}
