/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import static org.assertj.core.api.Assertions.filter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.util.StreamReaderDelegate;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bti.constant.CommonConstant;
import com.bti.constant.ConfigSetting;
import com.bti.constant.MessageLabel;
import com.bti.model.AccountNumberView;
import com.bti.model.BtiMessage;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.CityMaster;
import com.bti.model.CountryMaster;
import com.bti.model.CreditCardType;
import com.bti.model.CustomerMaintenanceActiveStatus;
import com.bti.model.CustomerMaintenanceHoldStatus;
import com.bti.model.FAAccountTableAccountType;
import com.bti.model.FABookMaintenanceAmortizationCodeType;
import com.bti.model.FABookMaintenanceAveragingConventionType;
import com.bti.model.FABookMaintenanceSwitchOverType;
import com.bti.model.FAPropertyTypes;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationAuditTrialSeriesType;
import com.bti.model.GLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.HoldVendorStatus;
import com.bti.model.Language;
import com.bti.model.LeaseType;
import com.bti.model.MasterAccountType;
import com.bti.model.MasterArAccountType;
import com.bti.model.MasterBlncdspl;
import com.bti.model.MasterCustomerClassMinimumCharge;
import com.bti.model.MasterCustomerClassSetupBalanceType;
import com.bti.model.MasterCustomerClassSetupCreditLimit;
import com.bti.model.MasterCustomerClassSetupFinanaceCharge;
import com.bti.model.MasterDepreciationPeriodTypes;
import com.bti.model.MasterDiscountType;
import com.bti.model.MasterDiscountTypePeriod;
import com.bti.model.MasterDueTypes;
import com.bti.model.MasterFAAccountGroupAccountType;
import com.bti.model.MasterGeneralMaintenanceAssetStatus;
import com.bti.model.MasterGeneralMaintenanceAssetType;
import com.bti.model.MasterNegativeSymbolSignTypes;
import com.bti.model.MasterNegativeSymbolTypes;
import com.bti.model.MasterRMAgeingTypes;
import com.bti.model.MasterRMApplyByDefaultTypes;
import com.bti.model.MasterRateFrequencyTypes;
import com.bti.model.MasterSeperatorDecimal;
import com.bti.model.MasterSeperatorThousands;
import com.bti.model.MasterVendorStatus;
import com.bti.model.PMSetupFormatType;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.PriceLevelSetup;
import com.bti.model.SeriesType;
import com.bti.model.ShipmentMethodType;
import com.bti.model.StateMaster;
import com.bti.model.TPCLBalanceType;
import com.bti.model.VATBasedOnType;
import com.bti.model.VATType;
import com.bti.model.dto.DtoAuditTrailCode;
import com.bti.model.dto.DtoCompany;
import com.bti.model.dto.DtoCountry;
import com.bti.model.dto.DtoFinancialDimensionValue;
import com.bti.model.dto.DtoMainAccountSetUp;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoTypes;
import com.bti.repository.RepositoryAccountNumberView;
import com.bti.repository.RepositoryBtiMessage;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCityMaster;
import com.bti.repository.RepositoryCountryMaster;
import com.bti.repository.RepositoryCreditCardType;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryCustomerMaintenanceActiveStatus;
import com.bti.repository.RepositoryCustomerMaintenanceHoldStatus;
import com.bti.repository.RepositoryEntityManager;
import com.bti.repository.RepositoryFAAccountTableAccountType;
import com.bti.repository.RepositoryFABookMaintenanceAmortizationCodeType;
import com.bti.repository.RepositoryFABookMaintenanceAveragingConventionType;
import com.bti.repository.RepositoryFABookMaintenanceSwitchOverType;
import com.bti.repository.RepositoryFAPropertyTypes;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationAuditTrialSeriesType;
import com.bti.repository.RepositoryGLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryHoldVendorStatus;
import com.bti.repository.RepositoryLanguage;
import com.bti.repository.RepositoryLeaseType;
import com.bti.repository.RepositoryMasterAccountType;
import com.bti.repository.RepositoryMasterArAccountType;
import com.bti.repository.RepositoryMasterBlncdspl;
import com.bti.repository.RepositoryMasterCustomerClassMinimumCharge;
import com.bti.repository.RepositoryMasterCustomerClassSetupBalanceType;
import com.bti.repository.RepositoryMasterCustomerClassSetupCreditLimit;
import com.bti.repository.RepositoryMasterCustomerClassSetupFinanaceCharge;
import com.bti.repository.RepositoryMasterDerpreciationPeriodTypes;
import com.bti.repository.RepositoryMasterDiscountType;
import com.bti.repository.RepositoryMasterDiscountTypePeriod;
import com.bti.repository.RepositoryMasterDueTypes;
import com.bti.repository.RepositoryMasterFAAccountGroupAccountType;
import com.bti.repository.RepositoryMasterGeneralMaintenanceAssetStatus;
import com.bti.repository.RepositoryMasterGeneralMaintenanceAssetType;
import com.bti.repository.RepositoryMasterNegativeSymbolSignTypes;
import com.bti.repository.RepositoryMasterNegativeSymbolTypes;
import com.bti.repository.RepositoryMasterRMAgeingTypes;
import com.bti.repository.RepositoryMasterRMApplyByDefaultTypes;
import com.bti.repository.RepositoryMasterRateFrequencyTypes;
import com.bti.repository.RepositoryMasterSeperatorDecimal;
import com.bti.repository.RepositoryMasterSeperatorThousands;
import com.bti.repository.RepositoryMasterVendorStatus;
import com.bti.repository.RepositoryPMSetupFormatType;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.repository.RepositoryPriceLevelSetup;
import com.bti.repository.RepositorySeriesType;
import com.bti.repository.RepositoryShipmentMethodType;
import com.bti.repository.RepositoryStateMaster;
import com.bti.repository.RepositoryTPCLBalanceType;
import com.bti.repository.RepositoryVATBasedOnType;
import com.bti.repository.RepositoryVATType;
import com.bti.util.RequestResponseLogger;
import com.bti.util.UtilRandomKey;
import com.bti.util.mapping.GL.MappingAuditTrialCode;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import net.minidev.json.JSONObject;

/**
 * Description: Service Home
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 11:11:38 AM
 * @author seasia
 * Version: 
 */
/**
 * @author skumar1
 *
 */
@Service("serviceHome")
@Transactional
public class ServiceHome {

	private static final Logger LOGGER = Logger.getLogger(ServiceHome.class);
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryCountryMaster repositoryCountryMaster;

	@Autowired
	RepositoryStateMaster repositoryStateMaster;

	@Autowired
	RepositoryCityMaster repositoryCityMaster;
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	RepositoryMasterNegativeSymbolSignTypes repositoryMasterNegativeSymbolSignTypes;
	
	@Autowired
	RepositoryMasterNegativeSymbolTypes repositoryMasterNegativeSymbolTypes;
	
	@Autowired
	RepositoryMasterSeperatorDecimal repositoryMasterSeperatorDecimal;
	
	@Autowired
	RepositoryMasterSeperatorThousands repositoryMasterSeperatorThousands;
	
	@Autowired
	RepositoryMasterDiscountType repositoryMasterDiscountType;
	
	@Autowired
	RepositoryMasterDiscountTypePeriod repositoryMasterDiscountTypePeriod;
	
	@Autowired
	RepositoryMasterDueTypes repositoryMasterDueTypes;
	
	@Autowired
	RepositoryMasterRateFrequencyTypes repositoryMasterRateFrequencyTypes;
	
	@Autowired
	RepositoryMasterDerpreciationPeriodTypes repositoryMasterDerpreciationPeriodTypes;
	
	@Autowired
	RepositoryMasterRMApplyByDefaultTypes repositoryMasterRMApplyByDefaultTypes;
	
	@Autowired
	RepositoryMasterRMAgeingTypes repositoryMasterRMAgeingTypes;
	
	@Autowired
	RepositoryMasterCustomerClassMinimumCharge repositoryMasterCustomerClassMinimumCharge;
	
	@Autowired
	RepositoryMasterCustomerClassSetupBalanceType repositoryMasterCustomerClassSetupBalanceType;
	
	@Autowired
	RepositoryMasterCustomerClassSetupCreditLimit repositoryMasterCustomerClassSetupCreditLimit;
	
	@Autowired
	RepositoryMasterCustomerClassSetupFinanaceCharge repositoryMasterCustomerClassSetupFinanaceCharge;
	
	@Autowired
	RepositoryMasterGeneralMaintenanceAssetStatus repositoryMasterGeneralMaintenanceAssetStatus;
	
	@Autowired
	RepositoryMasterGeneralMaintenanceAssetType repositoryMasterGeneralMaintenanceAssetType;
	
	@Autowired
	RepositoryFAPropertyTypes repositoryFAPropertyTypes;
	
	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;
	
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	
	@Autowired
	RepositoryMasterAccountType repositoryMasterAccountType;
	
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;
	
	@Autowired
	RepositoryMasterArAccountType repositoryMasterArAccountType;
	
	@Autowired
	 RepositoryMasterFAAccountGroupAccountType repositoryMasterFAAccountGroupAccountType;
	
	@Autowired
	RepositoryPriceLevelSetup repositoryPriceLevelSetup;
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;
	
	@Autowired
	MappingAuditTrialCode mappingAuditTrialCode;
	 
	
	@Autowired
	RepositoryBtiMessage repositoryBtiMessage;
	@Autowired
	RepositoryCreditCardType repositoryCreditCardType;
	@Autowired
	RepositoryCustomerMaintenanceActiveStatus repositoryCustomerMaintenanceActiveStatus;
	@Autowired
	RepositoryCustomerMaintenanceHoldStatus repositoryCustomerMaintenanceHoldStatus;
	@Autowired
	RepositoryFAAccountTableAccountType repositoryFAAccountTableAccountType;
	@Autowired
	RepositoryFABookMaintenanceAmortizationCodeType repositoryFABookMaintenanceAmortizationCodeType;
	@Autowired
	RepositoryFABookMaintenanceAveragingConventionType repositoryFABookMaintenanceAveragingConventionType;
	
	@Autowired
	RepositoryGLConfigurationRelationBetweenDimensionsAndMainAccount repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount;
	
	@Autowired
	RepositoryFABookMaintenanceSwitchOverType repositoryFABookMaintenanceSwitchOverType;
	@Autowired
	RepositoryGLConfigurationAuditTrialSeriesType repositoryGLConfigurationAuditTrialSeriesType;
	@Autowired
	RepositoryLeaseType repositoryLeaseType;
	@Autowired
	RepositoryMasterBlncdspl repositoryMasterBlncdspl;
	@Autowired
	RepositoryHoldVendorStatus repositoryHoldVendorStatus ;
	@Autowired
	RepositoryMasterVendorStatus repositoryMasterVendorStatus;
	@Autowired
	RepositoryPMSetupFormatType repositoryPMSetupFormatType;
	@Autowired
	RepositorySeriesType repositorySeriesType;
	@Autowired
	RepositoryShipmentMethodType repositoryShipmentMethodType;
	@Autowired
	RepositoryTPCLBalanceType repositoryTPCLBalanceType;
	@Autowired
	RepositoryVATBasedOnType repositoryVATBasedOnType;
	@Autowired
	RepositoryVATType repositoryVATType;
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
 
	@Autowired
	RepositoryLanguage repositoryLanguage;
	
	@Autowired
	RepositoryEntityManager repositoryEntityManager;
	
	@Value("${script.accesspath}")
	private String accesspath;
	@Value("${script.accessfinancialpath}")
	private String accessfinancialpath;
	
	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;
	
	@Autowired
	RepositoryAccountNumberView repositoryAccountNumberView;

	
	private final Integer ENG_LANG_ID=1;
	
	/**
	 * Description: get country list for drop down
	 * @return
	 */
	public List<DtoCountry> getCountryList() 
	{
		int langId = getLanngugaeId();
		List<DtoCountry> countryList = new ArrayList<>();
		List<CountryMaster> list = repositoryCountryMaster.findByIsDeletedAndIsActiveAndLanguageLanguageId(false, true,langId);
		if(list.isEmpty()){
			list=repositoryCountryMaster.findByIsDeletedAndIsActiveAndLanguageLanguageId(false, true,1);
		}
		if (list != null && !list.isEmpty()) 
		{
			for (CountryMaster countryMaster : list) {
				DtoCountry dtoCountry = new DtoCountry();
				dtoCountry.setCountryId(countryMaster.getCountryId());
				dtoCountry.setCountryName(countryMaster.getCountryName());
				dtoCountry.setShortName(countryMaster.getShortName());
				dtoCountry.setCountryCode(countryMaster.getCountryCode());
				countryList.add(dtoCountry);
			}
		}
		return countryList;
	}

	/**
	 * Description: get state list by country id for drop down
	 * @param countryId
	 * @return
	 */
	public List<DtoCountry> getStateListByCountryId(int countryId) {
		List<DtoCountry> stateList = new ArrayList<>();
		List<StateMaster> list = repositoryStateMaster.findByCountryMasterCountryIdAndIsDeleted(countryId, false);
		if (list != null && !list.isEmpty()) {
			for (StateMaster stateMaster : list) {
				DtoCountry dtoCountry = new DtoCountry();
				dtoCountry.setStateId(stateMaster.getStateId());
				dtoCountry.setStateName(stateMaster.getStateName());
				stateList.add(dtoCountry);
			}
		}
		return stateList;
	}

	/**
	 * Description: get city list by state id for drop down
	 * @param stateId
	 * @return
	 */
	public List<DtoCountry> getCityListByStateId(int stateId) {
		List<DtoCountry> cityList = new ArrayList<>();
		List<CityMaster> list = repositoryCityMaster.findByStateMasterStateIdAndIsDeleted(stateId, false);
		if (list != null && !list.isEmpty()) {
			for (CityMaster cityMaster : list) {
				DtoCountry dtoCountry = new DtoCountry();
				dtoCountry.setCityId(cityMaster.getCityId());
				dtoCountry.setCityName(cityMaster.getCityName());
				cityList.add(dtoCountry);
			}
		}
		return cityList;
	}

	/**
	 * Description: get country code by country id
	 * @param countryId
	 * @return
	 */
	public DtoCountry getCountryCodeByCountryId(int countryId) {
		DtoCountry dtoCountry = new DtoCountry();
		CountryMaster countryMaster = repositoryCountryMaster.findByCountryIdAndIsDeletedAndIsActive(countryId, false,
				true);
		if (countryMaster != null) {
			dtoCountry.setCountryId(countryMaster.getCountryId());
			dtoCountry.setCountryCode("+" + countryMaster.getCountryCode());
		}
		return dtoCountry;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getDueTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterDueTypes> list = repositoryMasterDueTypes.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterDueTypes type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getDueType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getDiscountTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterDiscountType> list = repositoryMasterDiscountType.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterDiscountType type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getDiscountType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getDiscountTypesPeriod() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterDiscountTypePeriod> list = repositoryMasterDiscountTypePeriod.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterDiscountTypePeriod type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getDiscountTypePriod());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getNegativeSymbolSignTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterNegativeSymbolSignTypes> list = repositoryMasterNegativeSymbolSignTypes.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterNegativeSymbolSignTypes type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getNegativeSymbolSignType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getNegativeSymbolTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterNegativeSymbolTypes> list = repositoryMasterNegativeSymbolTypes.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterNegativeSymbolTypes type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getNegativeSymbolType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getRateFrequencyTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterRateFrequencyTypes> list = repositoryMasterRateFrequencyTypes.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterRateFrequencyTypes type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getRateFrequencyType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getSeperatoryDecimalTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterSeperatorDecimal> list = repositoryMasterSeperatorDecimal.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterSeperatorDecimal type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getSeperatorDecimalId());
				dtoTypes.setTypeValue(type.getSeperatorDecimalPrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getSeperatoryThousandsTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterSeperatorThousands> list = repositoryMasterSeperatorThousands.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterSeperatorThousands type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getSeperatorThousandId());
				dtoTypes.setTypeValue(type.getSeperatorThousandPrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getDepreciationPeriodTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterDepreciationPeriodTypes> list = repositoryMasterDerpreciationPeriodTypes.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterDepreciationPeriodTypes type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getDepreciationPeriodType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getRmApplybyDefaultList() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterRMApplyByDefaultTypes> list = repositoryMasterRMApplyByDefaultTypes.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterRMApplyByDefaultTypes type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getTypePrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getRmAgeingTypesList() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterRMAgeingTypes> list = repositoryMasterRMAgeingTypes.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterRMAgeingTypes type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getTypePrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getCustomeClassBalanceTypesList() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterCustomerClassSetupBalanceType> list = repositoryMasterCustomerClassSetupBalanceType.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterCustomerClassSetupBalanceType type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getTypePrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getCustomeClassFinanceChargeList() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterCustomerClassSetupFinanaceCharge> list = repositoryMasterCustomerClassSetupFinanaceCharge.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterCustomerClassSetupFinanaceCharge type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getTypePrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getCustomeClassMinimumChargeList() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterCustomerClassMinimumCharge> list = repositoryMasterCustomerClassMinimumCharge.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterCustomerClassMinimumCharge type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getTypePrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getCustomeClassCreditLimitList() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterCustomerClassSetupCreditLimit> list = repositoryMasterCustomerClassSetupCreditLimit.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterCustomerClassSetupCreditLimit type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getTypePrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getFAGeneralMaintenanceAssetTypeList() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterGeneralMaintenanceAssetType> list = repositoryMasterGeneralMaintenanceAssetType.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterGeneralMaintenanceAssetType type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getTypePrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getFAGeneralMaintenanceAssetStatusList() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterGeneralMaintenanceAssetStatus> list = repositoryMasterGeneralMaintenanceAssetStatus.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) {
			for (MasterGeneralMaintenanceAssetStatus type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getTypePrimary());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getFAPropertyTypes() {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<FAPropertyTypes> list = repositoryFAPropertyTypes.findAll();
		if (list != null && !list.isEmpty()) {
			for (FAPropertyTypes type : list) {
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getPropertyTypeId());
				if (ConfigSetting.PRIMARY.getValue().equalsIgnoreCase(langId)) {
					dtoTypes.setPropertyName(type.getPropertyName());
				} else if (ConfigSetting.SECONDARY.getValue().equalsIgnoreCase(langId)) {
					dtoTypes.setPropertyName(type.getPropertyNameArabic());
				}
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @param mainAccountNumber
	 * @return
	 */
	public DtoMainAccountSetUp checkMainAccount(String mainAccountNumber) 
	{
		DtoMainAccountSetUp dtoMainAccount=null;
		if(UtilRandomKey.isNotBlank(mainAccountNumber))
		{
			COAMainAccounts coaMainAccounts = repositoryCOAMainAccounts.findByMainAccountNumberAndIsDeletedAndActive(mainAccountNumber,false,true);
			if(coaMainAccounts!=null)
			{
				dtoMainAccount= new DtoMainAccountSetUp();
				dtoMainAccount.setActIndx(coaMainAccounts.getActIndx());
				dtoMainAccount.setMainAccountDescription(coaMainAccounts.getMainAccountDescription());
				dtoMainAccount.setMainAccountDescriptionArabic(coaMainAccounts.getMainAccountDescriptionArabic());
				dtoMainAccount.setMainAccountNumber(coaMainAccounts.getMainAccountNumber());
				GLConfigurationRelationBetweenDimensionsAndMainAccount glConfig = repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount.findTopByIsDeletedAndIsMainAccount(false,1);
				if(glConfig!=null){
					Long fromIndex = glConfig.getCoaMainAccountsFrom().getActIndx();
					Long toIndex= glConfig.getCoaMainAccountsTo().getActIndx();
					Long index = coaMainAccounts.getActIndx();
                    if(index>=fromIndex && index<=toIndex ){
                	   return dtoMainAccount;
                    }
				}
			}
		}
		return null;
	}
	
	/**
	 * @param financialDimensionValue
	 * @return
	 */
	public DtoFinancialDimensionValue checkFinancialDimensionValue(String financialDimensionValue,int segmentNumber) 
	{
		DtoFinancialDimensionValue dtoFinancialDimensionValue=new DtoFinancialDimensionValue();
		if(UtilRandomKey.isNotBlank(financialDimensionValue))
		{
			List<COAFinancialDimensionsValues> coaFinancialDimensionsValuesList= repositoryCOAFinancialDimensionsValues.findByDimensionValueAndIsDeleted(financialDimensionValue,false);
			if(coaFinancialDimensionsValuesList!=null && !coaFinancialDimensionsValuesList.isEmpty())
			{
				for (COAFinancialDimensionsValues coaFinancialDimensionsValues : coaFinancialDimensionsValuesList) 
				{
					dtoFinancialDimensionValue.setDimInxValue(coaFinancialDimensionsValues.getDimInxValue());
					dtoFinancialDimensionValue.setDimensionDescription("");
					dtoFinancialDimensionValue.setDimensionDescriptionArabic("");
					dtoFinancialDimensionValue.setDimensionValue(coaFinancialDimensionsValues.getDimensionValue());
					if(UtilRandomKey.isNotBlank(coaFinancialDimensionsValues.getDimensionDescription())){
						dtoFinancialDimensionValue.setDimensionDescription(coaFinancialDimensionsValues.getDimensionDescription());
					}
					if(UtilRandomKey.isNotBlank(coaFinancialDimensionsValues.getDimensionDescriptionArabic()))
					{
						dtoFinancialDimensionValue.setDimensionDescriptionArabic(coaFinancialDimensionsValues.getDimensionDescriptionArabic());
					}
					
					List<GLConfigurationRelationBetweenDimensionsAndMainAccount> glConfigList = repositoryGLConfigurationRelationBetweenDimensionsAndMainAccount.findAll();
					if(glConfigList!=null && !glConfigList.isEmpty()){
						GLConfigurationRelationBetweenDimensionsAndMainAccount	glConfig=glConfigList.get(segmentNumber);
						if(glConfig!=null){
							Long fromIndex = glConfig.getCoaFinancialDimensionsValuesFrom().getDimInxValue();
							Long toIndex= glConfig.getCoaFinancialDimensionsValuesTo().getDimInxValue();
							Long index = coaFinancialDimensionsValues.getDimInxValue();
		                       if(index>=fromIndex && index<=toIndex 
//		                    		   &&
//		                    		   coaFinancialDimensionsValues.getCoaFinancialDimensions().getDimInxd()==glConfig.getCoaFinancialDimensions().getDimInxd()
		                    		   )
		                       {
		                    	   dtoFinancialDimensionValue.setMessageType(null);
		                    	   return dtoFinancialDimensionValue;
		                       }
		                       else{
		                    	   dtoFinancialDimensionValue.setMessageType("FINANCIAL_DIMENSION_VALUE_NOT_FOUND");
		                       }
						}
					}
				}
				return dtoFinancialDimensionValue;
				
			}
		}
		dtoFinancialDimensionValue.setMessageType("FINANCIAL_DIMENSION_VALUE_NOT_FOUND");
		return dtoFinancialDimensionValue;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getAccountTypes() {
		 
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterAccountType> list = repositoryMasterAccountType.findByLanguageLanguageIdAndIsDeleted(getLanngugaeId(), false);
		if (list != null && !list.isEmpty()) 
		{
			for (MasterAccountType type : list) 
			{
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getAccountType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	 
	/**
	 * @return
	 */
	public List<DtoPayableAccountClassSetup> getGlAccountTableAcumulationList() 
	{
		List<DtoPayableAccountClassSetup> listOfAccount = new ArrayList<>();
		List<GLAccountsTableAccumulation> list = repositoryGLAccountsTableAccumulation.findAll();
//		List<AccountNumberView> accountNumberViewList = repositoryAccountNumberView.findAll();
		List<AccountNumberView> accountNumberViewList = new ArrayList<AccountNumberView>();
		Map<String,Object> map = new HashMap<>();
		if (list != null && !list.isEmpty()) 
		{
			for (GLAccountsTableAccumulation accountDetail : list) 
			{
				DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
				dtoPayableAccountClassSetup.setAccountTableRowIndex(accountDetail.getAccountTableRowIndex());
				String accountNumber =serviceAccountPayable.getAccountNumber(accountDetail)[0].toString();
				dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
//				dtoPayableAccountClassSetup.setAccountDescription(accountDetail.getAccountDescription());
//				dtoPayableAccountClassSetup.setDescriptionArabic(descriptionArabic);
				setAccountDescription(dtoPayableAccountClassSetup, accountNumberViewList);
				if(!map.containsKey(accountNumber)){
                	map.put(accountNumber, dtoPayableAccountClassSetup);
                	listOfAccount.add(dtoPayableAccountClassSetup);
                }
			}
		}
		return listOfAccount;
	}
	
	/**
	 * @return
	 */
	public List<DtoPayableAccountClassSetup> getGlAccountTableAcumulationListNew() 
	{
		List<Object[]> accountsDetails = repositoryEntityManager.getAllAccountDetails();
		//List<GLAccountsTableAccumulation> list = repositoryGLAccountsTableAccumulation.findAll();
		List<DtoPayableAccountClassSetup> dtoList = new ArrayList<>();
		//List<PayableAccountClassSetup> accountDetails = repositoryPayableAccountClassSetup.findAll();
		if (accountsDetails != null && !accountsDetails.isEmpty()) 
		{
			for (Object[] accountDetail : accountsDetails) 
			{

				DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
				dtoPayableAccountClassSetup.setAccountTableRowIndex(String.valueOf(accountDetail[0]));
				dtoPayableAccountClassSetup.setAccountNumber(String.valueOf(accountDetail[1]));
				dtoPayableAccountClassSetup.setAccountDescription(String.valueOf(accountDetail[2]));
				dtoPayableAccountClassSetup.setAccountDescriptionArabic(String.valueOf(accountDetail[3]));
				dtoList.add(dtoPayableAccountClassSetup);
				
			}
				
		}
			
		return dtoList;
	}
	
	public DtoSearch searchGlAccountTableAcumulationList(DtoSearch dtoSearch) 
	{
		List<GLAccountsTableAccumulation> list = this.repositoryGLAccountsTableAccumulation.predictiveJournalSearchWithPagination(dtoSearch.getSearchKeyword());;
		List<DtoPayableAccountClassSetup> dtoList = new ArrayList<>();
		if(dtoSearch != null) {
			dtoSearch.setTotalCount(repositoryGLAccountsTableAccumulation.predictiveGLAccountsTableAccumulationSearchTotalCount(dtoSearch.getSearchKeyword()));
			
			if(dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
				if (list != null && !list.isEmpty()) 
				{
					for (GLAccountsTableAccumulation accountDetail : list) 
					{
						DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
						dtoPayableAccountClassSetup.setAccountTableRowIndex(accountDetail.getAccountTableRowIndex());
						dtoPayableAccountClassSetup.setAccountNumber(accountDetail.getAccountNumber());
						dtoPayableAccountClassSetup.setAccountDescription(accountDetail.getAccountDescription());
						dtoPayableAccountClassSetup.setAccountDescriptionArabic(accountDetail.getAccountDescriptionArabic());
						dtoList.add(dtoPayableAccountClassSetup);
					}
				}
				
			}
			
		}
		dtoSearch.setRecords(dtoList);
		
		return dtoSearch;
	}
	
	private void setAccountDescription(DtoPayableAccountClassSetup dtoPayableAccountClassSetup, List<AccountNumberView> accountNumberViewList) {
		LOGGER.info("dtoPayableAccountClassSetup.getAccountTableRowIndex().trim(): " + dtoPayableAccountClassSetup.getAccountTableRowIndex().trim());

		for (AccountNumberView accountNumberView: accountNumberViewList) {
//			LOGGER.info("dtoPayableAccountClassSetup.getAccountTableRowIndex().trim(): " + dtoPayableAccountClassSetup.getAccountTableRowIndex().trim());
//			LOGGER.info("String.valueOf(accountNumberView.getACTROWID()).trim(): " + String.valueOf(accountNumberView.getACTROWID()).trim());
//			LOGGER.info("accountNumberView.getACTROWID(): " + accountNumberView.getACTROWID());
//			
//			LOGGER.info("accountNumberView.getAccountnumber(): " + accountNumberView.getAccountnumber());
//			LOGGER.info("accountNumberView.getAccountcategory(): " + accountNumberView.getAccountcategory());
//			LOGGER.info("accountNumberView.getAccountdescription(): " + accountNumberView.getAccountdescription());
//			LOGGER.info("accountNumberView.getAccountdescriptionarabic(): " + accountNumberView.getAccountdescriptionarabic());
//			LOGGER.info("accountNumberView.getMainaccount(): " + accountNumberView.getMainaccount());

			if (dtoPayableAccountClassSetup.getAccountTableRowIndex().trim().equals(String.valueOf(accountNumberView.getACTROWID()).trim())) {
//				LOGGER.info(">>>>>>>>>>>>>>>>>   MATCHED   <<<<<<<<<<<<<<<<<<<");
					
//					(dtoPayableAccountClassSetup.getAccountNumber() + "-").equals(accountNumberView.getAccountnumber())) {
				dtoPayableAccountClassSetup.setDescription(accountNumberView.getAccountdescription());
				dtoPayableAccountClassSetup.setDescriptionArabic(accountNumberView.getAccountdescriptionarabic());
				break;
			}
		}
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getArAccountTypes() 
	{
		int langId = getLanngugaeId();
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterArAccountType> list = repositoryMasterArAccountType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		if (list != null && !list.isEmpty()) 
		{
			for (MasterArAccountType type : list) 
			{
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
					dtoTypes.setTypeValue(type.getAccountType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @return
	 */
	public List<DtoTypes> getFAAccountTypes() 
	{
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<MasterFAAccountGroupAccountType> list = repositoryMasterFAAccountGroupAccountType.findByLanguageLanguageIdAndIsDeleted(Integer.parseInt(langId),false);
		if (list != null && !list.isEmpty()) 
		{
			for (MasterFAAccountGroupAccountType type : list) 
			{
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setTypeId(type.getTypeId());
				dtoTypes.setTypeValue(type.getAccountType());
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	/**
	 * @param dtoPayableAccountClassSetup2
	 * @return
	 */
	public DtoPayableAccountClassSetup getGlAccountTableAcumulationDetailById(
			DtoPayableAccountClassSetup dtoPayableAccountClassSetup2) {
		DtoPayableAccountClassSetup dtoPayableAccountClassSetup = null;
		GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation
				.findByAccountTableRowIndexAndIsDeleted(dtoPayableAccountClassSetup2.getAccountTableRowIndex(),false);
		if (glAccountsTableAccumulation != null) {
			dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
			dtoPayableAccountClassSetup.setAccountDescription("");
			dtoPayableAccountClassSetup.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
			//Object[] object=serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
			List<Object[]> objects= repositoryEntityManager.getAllAccountDetails(); //serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
			List<Object[]> objectFilterd = objects.stream().filter(e -> Integer.parseInt(e[0].toString()) == Integer.parseInt(dtoPayableAccountClassSetup2.getAccountTableRowIndex()) ).collect(Collectors.toList());
			Object[] object = objectFilterd.get(0);
			dtoPayableAccountClassSetup.setAccountNumber(object[0].toString());
			//boolean transactionAllowed= (boolean) object[1];
			dtoPayableAccountClassSetup.setIsTransactionAllow(true);
			dtoPayableAccountClassSetup.setTypicalBalanceType("");
			if(UtilRandomKey.isNotBlank(object[2].toString())){
				//dtoPayableAccountClassSetup.setTypicalBalanceType(object[2].toString());
			}
			dtoPayableAccountClassSetup.setAccountDescription("");
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				dtoPayableAccountClassSetup.setAccountDescription(object[3].toString());
			}
		
		}
		return dtoPayableAccountClassSetup;
	}
	
	public List<DtoTypes> getPriceLevelList() 
	{
		
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		List<DtoTypes> dtoTypesList = new ArrayList<>();
		List<PriceLevelSetup> list = repositoryPriceLevelSetup.findAll();
		if (list != null && !list.isEmpty()) 
		{
			for (PriceLevelSetup type : list) 
			{
				DtoTypes dtoTypes = new DtoTypes();
				dtoTypes.setPriceLevelIndex(type.getPriceLevelIndex());
				if (ConfigSetting.PRIMARY.getValue().equalsIgnoreCase(langId)) {
					dtoTypes.setDescription(type.getDescription());
				} else if (ConfigSetting.SECONDARY.getValue().equalsIgnoreCase(langId)) {
					dtoTypes.setDescription(type.getDescriptionArabic());
				}
				dtoTypesList.add(dtoTypes);
			}
		}
		return dtoTypesList;
	}
	
	public int getLanngugaeId()
	{
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		if(UtilRandomKey.isNotBlank(langId)){
			return Integer.parseInt(langId);
		}
		else{
			return 0;
		}
	}
	
	public DtoRequestResponseLog logRequest(Object dto) {
		return RequestResponseLogger.logRequest(httpServletRequest, dto);
	}
	
	public String checkValidCompanyAccess() 
    {
          try{
        	  String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
        	  String session=httpServletRequest.getHeader(CommonConstant.SESSION);
        	  String userId=httpServletRequest.getHeader(CommonConstant.USER_ID);
        	  String companyTenantId=httpServletRequest.getHeader(CommonConstant.TENANT_ID);
               JSONObject json = new JSONObject();
               json.put(CommonConstant.USER_ID, userId);
               json.put(CommonConstant.SESSION, session);
               json.put("companyTenantId", companyTenantId);
               String jsonString = json.toJSONString();
               Response response = RestAssured.given().header(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON)                          
                      .header(CommonConstant.USER_ID, userId)
                      .header(CommonConstant.SESSION, session)
                      .header("companyTenantId",companyTenantId)
                      .header(CommonConstant.LANG_ID,langId)
                          .body(jsonString).when()
                         .post(accesspath+"/login/checkCompanyAccessForOtherModule").andReturn();
             JsonPath jsonpath =response.getBody().jsonPath();
             String code = jsonpath.getString("code");
             if(code.equalsIgnoreCase("401")){
            	 return MessageLabel.DO_NOT_HAVE_COMPANY_ACCESS;
             }
             else if(code.equalsIgnoreCase("404")){
            	 return MessageLabel.FORBIDDEN;
             }
             else if(code.equalsIgnoreCase("200")){
            	 return MessageLabel.RECORD_GET_SUCCESSFULLY;
             }
             else{
            	 return MessageLabel.FORBIDDEN;
             }
          }
          catch(Exception e){
        	  return MessageLabel.FORBIDDEN;
          }
    }
	/**
	 * @return
	 */
	public boolean checkGlAccountNumber(String aacountNumberFullIndex) 
	{
	    List<GLAccountsTableAccumulation> glAccountsTableAccumulationList=repositoryGLAccountsTableAccumulation.findByAccountNumberFullIndexAndIsDeleted(aacountNumberFullIndex, false);
	    return glAccountsTableAccumulationList!=null && !glAccountsTableAccumulationList.isEmpty()?true:false;
	}
	
	public DtoCompany getDatabaseCredential()
	{
		try 
		{
			String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
			String userId = httpServletRequest.getHeader(CommonConstant.USER_ID);
			String companyTenantId = httpServletRequest.getHeader(CommonConstant.TENANT_ID);
			JSONObject json = new JSONObject();
			String jsonString = json.toJSONString();
			Response response = RestAssured.given().header(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON).header(CommonConstant.USER_ID, userId)
					.header("companyTenantId", companyTenantId).header(CommonConstant.LANG_ID, langId).body(jsonString).when()
					.post(accesspath + "/login/getCompanyDatabaseCredential").andReturn();
			JsonPath jsonpath = response.getBody().jsonPath();
			String code = jsonpath.getString("code");
			if (code.equalsIgnoreCase("200")) 
			{
				String databaseIP = jsonpath.getString("result.databaseIP");
				String port = jsonpath.getString("result.port");
				String username = jsonpath.getString("result.username");
				String password = jsonpath.getString("result.password");
				DtoCompany dtoCompany = new DtoCompany();
				dtoCompany.setDatabaseIP(databaseIP);
				dtoCompany.setPort(port);
				dtoCompany.setUsername(username);
				dtoCompany.setPassword(password);
				return dtoCompany;
			}
		} 
		catch (Exception e) 
		{
			LOGGER.error(e.getMessage());
		}
		return null;
	}

	public void exportMasterData(HttpServletRequest request, HttpServletResponse response) 
	{
        final ServletContext servletContext = request.getSession().getServletContext();
        MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
        final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        String temperotyFilePath = tempDirectory.getAbsolutePath();
		XSSFWorkbook workbook = new XSSFWorkbook();
        Sheet masterGeneralMaintenanceAssetTypeSheet = workbook.createSheet("MasterGeneralMaintenanceAssetType");
        Sheet masterGeneralMaintenanceAssetStatusSheet = workbook.createSheet("MasterGeneralMaintenanceAssetStatus");
        Sheet btiMessageMasterSheet = workbook.createSheet("BtiMessageMaster");
        Sheet countryMasterSheet = workbook.createSheet("CountryMaster");
        Sheet stateMasterSheet = workbook.createSheet("StateMaster");
        Sheet cityMasterSheet = workbook.createSheet("CityMaster");        
        Sheet creditCardTypeSheet = workbook.createSheet("CreditCardType");
        Sheet customerMaintenanceActiveStatusSheet = workbook.createSheet("CustomerMaintenanceActiveStatus");        
        Sheet customerMaintenanceHoldStatusSheet = workbook.createSheet("CustomerMaintenanceHoldStatus");
        Sheet faAccountTableAccountTypeSheet = workbook.createSheet("FAAccountTableAccountType");
        Sheet faBookMaintenanceAmortizationCodeTypeSheet = workbook.createSheet("fa_book_maintenance_amortization_code_type");              
        Sheet faBookMaintenanceAveragingConventionTypeSheet = workbook.createSheet("fa_book_maintenance_averaging_convention_type");
        Sheet faBookMaintenanceSwitchoverTypeSheet = workbook.createSheet("fa_book_maintenance_switchover_type");
        Sheet glConfigurationAuditTrialSeriesTypeSheet = workbook.createSheet("gl_configuration_audit_trial_series_type");      
        Sheet leaseTypeSheet = workbook.createSheet("lease_type");
        Sheet masterAccountTypeSheet = workbook.createSheet("master_account_type");
        Sheet masterARAccountTypeSheet = workbook.createSheet("master_ar_account_type");     
        Sheet masterBlncdsplSheet = workbook.createSheet("master_blncdspl");
        Sheet masterCustomerClassBalanceTypeSheet = workbook.createSheet("master_customer_class_balance_type");
        Sheet masterCustomerClassMinimumChargeSheet = workbook.createSheet("master_customer_class_minimum_charge");
        Sheet masterCustomerClassSetupCreditLimitSheet = workbook.createSheet("master_customer_class_setup_credit_limit");
        Sheet masterCustomerClassSetupFinanceChargeSheet = workbook.createSheet("master_customer_class_setup_finance_charge");
        Sheet masterDepreciationPeriodTypesSheet = workbook.createSheet("master_depreciation_period_types");
        Sheet masterDiscountTypeSheet = workbook.createSheet("master_discount_type");
        Sheet masterDiscountTypePeriodSheet = workbook.createSheet("master_discount_type_period");
        Sheet masterDueTypesSheet = workbook.createSheet("master_due_types");
        Sheet masterFAAccountGroupAccountTypesSheet = workbook.createSheet("master_fa_account_group_account_types");
        Sheet masterHoldVendorStatusSheet = workbook.createSheet("master_hold_vendor_status");
        Sheet masterNegativeSymbolSignTypesSheet = workbook.createSheet("master_negative_symbol_sign_types");
        Sheet masterNegativeSymbolTypesSheet = workbook.createSheet("master_negative_symbol_types");
        Sheet masterRateFrequencyTypesSheet = workbook.createSheet("master_rate_frequency_types");
        Sheet masterRMAgeingTypesSheet = workbook.createSheet("master_rm_ageing_types");
        Sheet masterRMApplyBYDefaultTypesSheet = workbook.createSheet("master_rm_apply_by_default_types");
        Sheet masterSeperatorDecimalSheet = workbook.createSheet("master_seperator_decimal");
        Sheet masterSeperatorThousandsSheet = workbook.createSheet("master_seperator_thousands");
        Sheet masterVendorStatusSheet = workbook.createSheet("master_vendor_status");
        Sheet pmSetupFormatTypeSheet = workbook.createSheet("pm_setup_format_type");
        Sheet seriesTypeSheet = workbook.createSheet("series_type");
        Sheet shipmentMethodTypeSheet = workbook.createSheet("shipment_method_type");
        Sheet tpclblncTypeSheet = workbook.createSheet("tpclblnc_type");
        Sheet vatBasedOnTypeSheet = workbook.createSheet("vat_based_on_type");
        Sheet vatTypeSheet = workbook.createSheet("vat_type");
        Map<Integer, Object[]> btiMessageData = new TreeMap<>();        
        Map<Integer, Object[]> countryData = new TreeMap<>();
        Map<Integer, Object[]> stateData = new TreeMap<>();
        Map<Integer, Object[]> cityData = new TreeMap<>();
        Map<Integer, Object[]> creditCardTypeData = new TreeMap<>();
        Map<Integer, Object[]> customerMaintenanceActiveStatusData = new TreeMap<>();      
        Map<Integer, Object[]> customerMaintenanceHoldStatusData = new TreeMap<>();
        Map<Integer, Object[]> faAccountTableAccountTypeData = new TreeMap<>();
        Map<Integer, Object[]> faBookMaintenanceAmortizationCodeTypeData = new TreeMap<>();
        Map<Integer, Object[]> faBookMaintenanceAveragingConventionTypeData = new TreeMap<>();
        Map<Integer, Object[]> faBookMaintenanceSwitchoverTypeData = new TreeMap<>();  
        Map<Integer, Object[]> glConfigurationAuditTrialSeriesTypeData = new TreeMap<>();
        Map<Integer, Object[]> leaseTypeData = new TreeMap<>();
        Map<Integer, Object[]> masterAccountTypeData = new TreeMap<>();
        Map<Integer, Object[]> masterARAccountTypeData = new TreeMap<>();
        Map<Integer, Object[]> masterBlncdsplData = new TreeMap<>();
        Map<Integer, Object[]> masterCustomerClassBalanceTypeData = new TreeMap<>();      
        Map<Integer, Object[]> masterCustomerClassMinimumChargeData = new TreeMap<>();
        Map<Integer, Object[]> masterCustomerClassSetupCreditLimitData = new TreeMap<>();
        Map<Integer, Object[]> masterCustomerClassSetupFinanceChargeData = new TreeMap<>();
        Map<Integer, Object[]> masterDepreciationPeriodTypesData = new TreeMap<>();        
        Map<Integer, Object[]> masterDiscountTypeData = new TreeMap<>();      
        Map<Integer, Object[]> masterDiscountTypePeriodData = new TreeMap<>();
        Map<Integer, Object[]> masterDueTypesData = new TreeMap<>();
        Map<Integer, Object[]> masterFAAccountGroupAccountTypesData = new TreeMap<>(); 
        Map<Integer, Object[]> masterGeneralMaintenanceAssetStatusData = new TreeMap<>();
        Map<Integer, Object[]> masterGeneralMaintenanceAssetTypeData = new TreeMap<>();      
        Map<Integer, Object[]> masterHoldVendorStatusData = new TreeMap<>();      
        Map<Integer, Object[]> masternegativeSymbolSignTypesData = new TreeMap<>();
        Map<Integer, Object[]> masterNegativeSymbolTypesData = new TreeMap<>();
        Map<Integer, Object[]> masterRateFrequencyTypesData = new TreeMap<>();        
        Map<Integer, Object[]> masterRMAgeingTypesData = new TreeMap<>();      
        Map<Integer, Object[]> masterRMApplyByDefaultTypesData = new TreeMap<>();
        Map<Integer, Object[]> masterSeperatorDecimalData = new TreeMap<>();
        Map<Integer, Object[]> masterSeperatorThousandsData = new TreeMap<>();
        Map<Integer, Object[]> masterVendorStatusData = new TreeMap<>();      
        Map<Integer, Object[]> pmSetupFormatTypeData = new TreeMap<>();
        Map<Integer, Object[]> seriesTypeData = new TreeMap<>();
        Map<Integer, Object[]> shipmentMethodTypeData = new TreeMap<>();
        Map<Integer, Object[]> tpclblncTypeData = new TreeMap<>();      
        Map<Integer, Object[]> vatBasedOnTypeData = new TreeMap<>();
        Map<Integer, Object[]> vatTypeData = new TreeMap<>();
        
		for (int i = 0; i < 10; i++) 
		{
			countryMasterSheet.setColumnWidth(i, 10000);
			stateMasterSheet.setColumnWidth(i, 10000);
			cityMasterSheet.setColumnWidth(i, 10000);
			btiMessageMasterSheet.setColumnWidth(i, 10000);
			creditCardTypeSheet.setColumnWidth(i, 10000);
			customerMaintenanceActiveStatusSheet.setColumnWidth(i, 10000);
			customerMaintenanceHoldStatusSheet.setColumnWidth(i, 10000);
			faAccountTableAccountTypeSheet.setColumnWidth(i, 10000);
			faBookMaintenanceAmortizationCodeTypeSheet.setColumnWidth(i, 10000);
			faBookMaintenanceAveragingConventionTypeSheet.setColumnWidth(i, 10000);
			faBookMaintenanceSwitchoverTypeSheet.setColumnWidth(i, 10000);
			glConfigurationAuditTrialSeriesTypeSheet.setColumnWidth(i, 10000);
			leaseTypeSheet.setColumnWidth(i, 10000);
			masterAccountTypeSheet.setColumnWidth(i, 10000);
			masterARAccountTypeSheet.setColumnWidth(i, 10000);
			masterBlncdsplSheet.setColumnWidth(i, 10000);
			masterCustomerClassBalanceTypeSheet.setColumnWidth(i, 10000);
			masterCustomerClassMinimumChargeSheet.setColumnWidth(i, 10000);
			masterCustomerClassSetupCreditLimitSheet.setColumnWidth(i, 10000);
			masterCustomerClassSetupFinanceChargeSheet.setColumnWidth(i, 10000);
			masterDepreciationPeriodTypesSheet.setColumnWidth(i, 10000);
			masterDiscountTypeSheet.setColumnWidth(i, 10000);
			masterDiscountTypePeriodSheet.setColumnWidth(i, 10000);
			masterDueTypesSheet.setColumnWidth(i, 10000);
			masterFAAccountGroupAccountTypesSheet.setColumnWidth(i, 10000);
			masterGeneralMaintenanceAssetStatusSheet.setColumnWidth(i, 10000);
			masterGeneralMaintenanceAssetTypeSheet.setColumnWidth(i, 10000);
			masterHoldVendorStatusSheet.setColumnWidth(i, 10000);
			masterNegativeSymbolSignTypesSheet.setColumnWidth(i, 10000);
			masterNegativeSymbolTypesSheet.setColumnWidth(i, 10000);
			masterRateFrequencyTypesSheet.setColumnWidth(i, 10000);
			masterRMAgeingTypesSheet.setColumnWidth(i, 10000);
			masterRMApplyBYDefaultTypesSheet.setColumnWidth(i, 10000);
			masterSeperatorDecimalSheet.setColumnWidth(i, 10000);
			masterSeperatorThousandsSheet.setColumnWidth(i, 10000);
			masterVendorStatusSheet.setColumnWidth(i, 10000);
			pmSetupFormatTypeSheet.setColumnWidth(i, 10000);
			seriesTypeSheet.setColumnWidth(i, 10000);
			shipmentMethodTypeSheet.setColumnWidth(i, 10000);
			tpclblncTypeSheet.setColumnWidth(i, 10000);
			vatBasedOnTypeSheet.setColumnWidth(i, 10000);
			vatTypeSheet.setColumnWidth(i, 10000);
		}
		
			btiMessageData.put(1, new Object[] { "Message Short Name","Message","New Language Message"});
        	countryData.put(1, new Object[] { "Country Code","Short Name","Country Name","New Language Country Name"});
        	stateData.put(1, new Object[] { "Country Id","State Code","State Name","New Language State Name"});
        	cityData.put(1, new Object[] { "State Id","City Code","City Name","New Language City Name"});
        	creditCardTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            customerMaintenanceActiveStatusData.put(1, new Object[] {  "Type ID","Type","New Language Type"});
            customerMaintenanceHoldStatusData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            faAccountTableAccountTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            faBookMaintenanceAmortizationCodeTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            faBookMaintenanceAveragingConventionTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            faBookMaintenanceSwitchoverTypeData.put(1, new Object[] {  "Type ID","Type","New Language Type"});
            glConfigurationAuditTrialSeriesTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            leaseTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            masterAccountTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            masterARAccountTypeData.put(1, new Object[] {"Type ID","Type","New Language Type" });
            masterBlncdsplData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterCustomerClassBalanceTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterCustomerClassMinimumChargeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterCustomerClassSetupCreditLimitData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterCustomerClassSetupFinanceChargeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterDepreciationPeriodTypesData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterDiscountTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterDiscountTypePeriodData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterDueTypesData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterFAAccountGroupAccountTypesData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterGeneralMaintenanceAssetStatusData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterGeneralMaintenanceAssetTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterHoldVendorStatusData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masternegativeSymbolSignTypesData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterNegativeSymbolTypesData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterRateFrequencyTypesData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterRMAgeingTypesData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            masterRMApplyByDefaultTypesData.put(1, new Object[] {"Type ID","Type","New Language Type"});
        	masterSeperatorDecimalData.put(1, new Object[] {"Type ID","Type","New Language Type"});
        	masterSeperatorThousandsData.put(1, new Object[] {"Type ID","Type","New Language Type"});
        	masterVendorStatusData.put(1, new Object[] {"Type ID","Type","New Language Type"});
        	pmSetupFormatTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
        	seriesTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            shipmentMethodTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            tpclblncTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            vatBasedOnTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
            vatTypeData.put(1, new Object[] {"Type ID","Type","New Language Type"});
         
            List<CountryMaster> countryList = repositoryCountryMaster.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<StateMaster> stateList = repositoryStateMaster.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<CityMaster> cityList = repositoryCityMaster.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<BtiMessage> btiMessageList = repositoryBtiMessage.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<CreditCardType> creditCardTypeList = repositoryCreditCardType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<CustomerMaintenanceActiveStatus> customerMaintenanceActiveStatusList = repositoryCustomerMaintenanceActiveStatus.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<CustomerMaintenanceHoldStatus> customerMaintenanceHoldStatusList = repositoryCustomerMaintenanceHoldStatus.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<FAAccountTableAccountType> faAccountTableAccountTypeList = repositoryFAAccountTableAccountType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<FABookMaintenanceAmortizationCodeType> faBookMaintenanceAmortizationCodeTypeList = repositoryFABookMaintenanceAmortizationCodeType.findByIsDeletedAndLanguageLanguageId(false,1);
         	List<FABookMaintenanceAveragingConventionType> faBookMaintenanceAveragingConventionTypeList = repositoryFABookMaintenanceAveragingConventionType.findByIsDeletedAndLanguageLanguageId(false,1);
         	List<FABookMaintenanceSwitchOverType> faBookMaintenanceSwitchOverTypeList = repositoryFABookMaintenanceSwitchOverType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<GLConfigurationAuditTrialSeriesType> glConfigurationAuditTrialSeriesTypeList = repositoryGLConfigurationAuditTrialSeriesType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<LeaseType> leaseTypeList = repositoryLeaseType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterAccountType> masterAccountTypeList = repositoryMasterAccountType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterArAccountType> masterArAccountTypeList = repositoryMasterArAccountType.findByIsDeletedAndLanguageLanguageId(false,1);
         	List<MasterBlncdspl> masterBlncdsplList = repositoryMasterBlncdspl.findByIsDeletedAndLanguageLanguageId(false,1);
          	List<MasterCustomerClassSetupBalanceType> masterCustomerClassSetupBalanceTypeList = repositoryMasterCustomerClassSetupBalanceType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterCustomerClassMinimumCharge> masterCustomerClassMinimumChargeList = repositoryMasterCustomerClassMinimumCharge.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterCustomerClassSetupCreditLimit> masterCustomerClassSetupCreditLimitList = repositoryMasterCustomerClassSetupCreditLimit.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterCustomerClassSetupFinanaceCharge> masterCustomerClassSetupFinanaceChargeList = repositoryMasterCustomerClassSetupFinanaceCharge.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterDepreciationPeriodTypes> masterDepreciationPeriodTypesList = repositoryMasterDerpreciationPeriodTypes.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterDiscountType> masterDiscountTypeList = repositoryMasterDiscountType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterDiscountTypePeriod> masterDiscountTypePeriodList = repositoryMasterDiscountTypePeriod.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterDueTypes> masterDueTypesList = repositoryMasterDueTypes.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterFAAccountGroupAccountType> masterFAAccountGroupAccountTypeList = repositoryMasterFAAccountGroupAccountType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterGeneralMaintenanceAssetStatus> masterGeneralMaintenanceAssetStatusList = repositoryMasterGeneralMaintenanceAssetStatus.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterGeneralMaintenanceAssetType> masterGeneralMaintenanceAssetTypeList = repositoryMasterGeneralMaintenanceAssetType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<HoldVendorStatus> holdVendorStatusList = repositoryHoldVendorStatus.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterNegativeSymbolSignTypes> masterNegativeSymbolSignTypesList = repositoryMasterNegativeSymbolSignTypes.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterNegativeSymbolTypes> masterNegativeSymbolTypesList = repositoryMasterNegativeSymbolTypes.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterRateFrequencyTypes> masterRateFrequencyTypesList = repositoryMasterRateFrequencyTypes.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterRMAgeingTypes> masterRMAgeingTypesList = repositoryMasterRMAgeingTypes.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterRMApplyByDefaultTypes> masterRMApplyByDefaultTypesList = repositoryMasterRMApplyByDefaultTypes.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterSeperatorDecimal> masterSeperatorDecimalList = repositoryMasterSeperatorDecimal.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterSeperatorThousands> masterSeperatorThousandsList = repositoryMasterSeperatorThousands.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<MasterVendorStatus>masterVendorStatusList = repositoryMasterVendorStatus.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<PMSetupFormatType> pmSetupFormatTypeList = repositoryPMSetupFormatType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<SeriesType> seriesTypeList = repositorySeriesType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<ShipmentMethodType> shipmentMethodTypeList = repositoryShipmentMethodType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<TPCLBalanceType> tpclBalanceTypeList = repositoryTPCLBalanceType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<VATBasedOnType> vatBasedOnTypeList = repositoryVATBasedOnType.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<VATType> vatTypeList = repositoryVATType.findByIsDeletedAndLanguageLanguageId(false,1);
        
    	 if (btiMessageList!= null && !btiMessageList.isEmpty() ){
         	int sno = 2;
             for (BtiMessage btiMessage : btiMessageList) {
             String name = btiMessage.getMessageShort();
             String message = btiMessage.getMessage();
                      sno++;
                      btiMessageData.put(sno, new Object[] { name,message,""});
                }
         }
         
		if (countryList != null && !countryList.isEmpty()) {
			int sno = 2;
			for (CountryMaster country : countryList) {
				String countryName = country.getCountryName();
				String countryCode = country.getCountryCode();
				String shortName = country.getShortName();
				sno++;
				countryData.put(sno, new Object[] { countryCode, shortName, countryName, "" });
			}
		}
		
		if (stateList != null && !stateList.isEmpty()) {
			int sno = 2;
			for (StateMaster state : stateList) {
				String stateName = state.getStateName();
				String stateCode = state.getStateCode();
				int contryId = state.getCountryMaster().getCountryId();

				sno++;
				stateData.put(sno, new Object[] { contryId, stateCode, stateName, "" });
			}
		}
		
		if (cityList != null && !cityList.isEmpty()) {
			int sno = 2;
			for (CityMaster city : cityList) {
				String cityName = city.getCityName();
				String cityCode = city.getCityCode();
				int stateId = city.getStateMaster().getStateId();
				sno++;
				cityData.put(sno, new Object[] { stateId, cityCode, cityName, "" });
			}
		}
         
		if (creditCardTypeList != null && !creditCardTypeList.isEmpty()) {
			int sno = 2;
			for (CreditCardType creditCardType : creditCardTypeList) {
				int typeId = creditCardType.getTypeId();
				String type = creditCardType.getTypePrimary();
				sno++;
				creditCardTypeData.put(sno, new Object[] { typeId, type, "" });
			}
		}
        	
		if (customerMaintenanceActiveStatusList != null && !customerMaintenanceActiveStatusList.isEmpty()) {
			int sno = 2;
			for (CustomerMaintenanceActiveStatus customerMaintenanceActiveStatus : customerMaintenanceActiveStatusList) {
				int typeId = customerMaintenanceActiveStatus.getTypeId();
				String type = customerMaintenanceActiveStatus.getTypePrimary();
				sno++;
				customerMaintenanceActiveStatusData.put(sno, new Object[] { typeId, type, "" });
			}
		}
        	 
		if (customerMaintenanceHoldStatusList != null && !customerMaintenanceHoldStatusList.isEmpty()) {
			int sno = 2;
			for (CustomerMaintenanceHoldStatus customerMaintenanceHoldStatus : customerMaintenanceHoldStatusList) {
				int typeId = customerMaintenanceHoldStatus.getTypeId();
				String type = customerMaintenanceHoldStatus.getTypePrimary();
				sno++;
				customerMaintenanceHoldStatusData.put(sno, new Object[] { typeId, type, "" });
			}
		}
        	 
		if (faAccountTableAccountTypeList != null && !faAccountTableAccountTypeList.isEmpty()) {
			int sno = 2;
			for (FAAccountTableAccountType faAccountTableAccountType : faAccountTableAccountTypeList) {
				int typeId = faAccountTableAccountType.getTypeId();
				String type = faAccountTableAccountType.getTypePrimary();
				sno++;
				faAccountTableAccountTypeData.put(sno, new Object[] { typeId, type, "" });
			}
		}
        	 
		if (faBookMaintenanceAmortizationCodeTypeList != null && !faBookMaintenanceAmortizationCodeTypeList.isEmpty()) {
			int sno = 2;
			for (FABookMaintenanceAmortizationCodeType faBookMaintenanceAmortizationCodeType : faBookMaintenanceAmortizationCodeTypeList) {
				int typeId = faBookMaintenanceAmortizationCodeType.getTypeId();
				String type = faBookMaintenanceAmortizationCodeType.getTypePrimary();
				sno++;
				faBookMaintenanceAmortizationCodeTypeData.put(sno, new Object[] { typeId, type, "" });
			}
		}
        		 
		if (faBookMaintenanceAveragingConventionTypeList != null
				&& !faBookMaintenanceAveragingConventionTypeList.isEmpty()) {
			int sno = 2;
			for (FABookMaintenanceAveragingConventionType faBookMaintenanceAveragingConventionType : faBookMaintenanceAveragingConventionTypeList) {
				int typeId = faBookMaintenanceAveragingConventionType.getTypeId();
				String type = faBookMaintenanceAveragingConventionType.getTypePrimary();
				sno++;
				faBookMaintenanceAveragingConventionTypeData.put(sno, new Object[] { typeId, type, "" });
			}
		}
		if (faBookMaintenanceSwitchOverTypeList != null && !faBookMaintenanceSwitchOverTypeList.isEmpty()) {
			int sno = 2;
			for (FABookMaintenanceSwitchOverType faBookMaintenanceSwitchOverType : faBookMaintenanceSwitchOverTypeList) {
				int typeId = faBookMaintenanceSwitchOverType.getTypeId();
				String type = faBookMaintenanceSwitchOverType.getTypePrimary();
				sno++;
				faBookMaintenanceSwitchoverTypeData.put(sno, new Object[] { typeId, type, "" });
			}
		}
        	 
		if (glConfigurationAuditTrialSeriesTypeList != null && !glConfigurationAuditTrialSeriesTypeList.isEmpty()) {
			int sno = 2;
			for (GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType : glConfigurationAuditTrialSeriesTypeList) {
				int typeId = glConfigurationAuditTrialSeriesType.getTypeId();
				String type = glConfigurationAuditTrialSeriesType.getTypePrimary();
				sno++;
				glConfigurationAuditTrialSeriesTypeData.put(sno, new Object[] { typeId, type, "" });
			}
		}
        	
        	if (leaseTypeList != null && !leaseTypeList.isEmpty()) {
    			int sno = 2;
    			for (LeaseType leaseType : leaseTypeList) {
    				int typeId = leaseType.getLeaseTypeId();
    				String type = leaseType.getValue();
    				sno++;
    				leaseTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	 
        	if (masterAccountTypeList != null && !masterAccountTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterAccountType masterAccountType : masterAccountTypeList) {
    				int typeId = masterAccountType.getTypeId();
    				String type = masterAccountType.getAccountType();
    				sno++;
    				masterAccountTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	 
        	if (masterArAccountTypeList != null && !masterArAccountTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterArAccountType masterArAccountType : masterArAccountTypeList) {
    				int typeId = masterArAccountType.getTypeId();
    				String type = masterArAccountType.getAccountType();
    				sno++;
    				masterARAccountTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	 
        	if (masterBlncdsplList != null && !masterBlncdsplList.isEmpty()) {
    			int sno = 2;
    			for (MasterBlncdspl masterBlncdspl : masterBlncdsplList) {
    				int typeId = masterBlncdspl.getTypeId();
    				String type = masterBlncdspl.getTypePrimary();
    				sno++;
    				masterBlncdsplData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	 
        	if (masterCustomerClassSetupBalanceTypeList != null && !masterCustomerClassSetupBalanceTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterCustomerClassSetupBalanceType masterCustomerClassSetupBalanceType : masterCustomerClassSetupBalanceTypeList) {
    				int typeId = masterCustomerClassSetupBalanceType.getTypeId();
    				String type = masterCustomerClassSetupBalanceType.getTypePrimary();
    				sno++;
    				masterCustomerClassBalanceTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	
        	 
        	if (masterCustomerClassMinimumChargeList != null && !masterCustomerClassMinimumChargeList.isEmpty()) {
    			int sno = 2;
    			for (MasterCustomerClassMinimumCharge masterCustomerClassMinimumCharge : masterCustomerClassMinimumChargeList) {
    				int typeId = masterCustomerClassMinimumCharge.getTypeId();
    				String type = masterCustomerClassMinimumCharge.getTypePrimary();
    				sno++;
    				masterCustomerClassMinimumChargeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	 
        	if (masterCustomerClassSetupCreditLimitList != null && !masterCustomerClassSetupCreditLimitList.isEmpty()) {
    			int sno = 2;
    			for (MasterCustomerClassSetupCreditLimit masterCustomerClassSetupCreditLimit : masterCustomerClassSetupCreditLimitList) {
    				int typeId = masterCustomerClassSetupCreditLimit.getTypeId();
    				String type = masterCustomerClassSetupCreditLimit.getTypePrimary();
    				sno++;
    				masterCustomerClassSetupCreditLimitData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	 
        	if (masterCustomerClassSetupFinanaceChargeList != null && !masterCustomerClassSetupFinanaceChargeList.isEmpty()) {
    			int sno = 2;
    			for (MasterCustomerClassSetupFinanaceCharge masterCustomerClassSetupFinanaceCharge : masterCustomerClassSetupFinanaceChargeList) {
    				int typeId = masterCustomerClassSetupFinanaceCharge.getTypeId();
    				String type = masterCustomerClassSetupFinanaceCharge.getTypePrimary();
    				sno++;
    				masterCustomerClassSetupFinanceChargeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterDepreciationPeriodTypesList != null && !masterDepreciationPeriodTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterDepreciationPeriodTypes masterDepreciationPeriodTypes : masterDepreciationPeriodTypesList) {
    				int typeId = masterDepreciationPeriodTypes.getTypeId();
    				String type = masterDepreciationPeriodTypes.getDepreciationPeriodType();
    				sno++;
    				masterDepreciationPeriodTypesData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterDiscountTypeList != null && !masterDiscountTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterDiscountType masterDiscountType : masterDiscountTypeList) {
    				int typeId = masterDiscountType.getTypeId();
    				String type = masterDiscountType.getDiscountType();
    				sno++;
    				masterDiscountTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterDiscountTypePeriodList != null && !masterDiscountTypePeriodList.isEmpty()) {
    			int sno = 2;
    			for (MasterDiscountTypePeriod masterDiscountTypePeriod : masterDiscountTypePeriodList) {
    				int typeId = masterDiscountTypePeriod.getTypeId();
    				String type = masterDiscountTypePeriod.getDiscountTypePriod();
    				sno++;
    				masterDiscountTypePeriodData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterDueTypesList != null && !masterDueTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterDueTypes masterDueTypes : masterDueTypesList) {
    				int typeId = masterDueTypes.getTypeId();
    				String type = masterDueTypes.getDueType();
    				sno++;
    				masterDueTypesData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterFAAccountGroupAccountTypeList != null && !masterFAAccountGroupAccountTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterFAAccountGroupAccountType masterFAAccountGroupAccountType : masterFAAccountGroupAccountTypeList) {
    				int typeId = masterFAAccountGroupAccountType.getTypeId();
    				String type = masterFAAccountGroupAccountType.getAccountType();
    				sno++;
    				masterFAAccountGroupAccountTypesData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterGeneralMaintenanceAssetStatusList != null && !masterGeneralMaintenanceAssetStatusList.isEmpty()) {
    			int sno = 2;
    			for (MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus : masterGeneralMaintenanceAssetStatusList) {
    				int typeId = masterGeneralMaintenanceAssetStatus.getTypeId();
    				String type = masterGeneralMaintenanceAssetStatus.getTypePrimary();
    				sno++;
    				masterGeneralMaintenanceAssetStatusData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterGeneralMaintenanceAssetTypeList != null && !masterGeneralMaintenanceAssetTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType : masterGeneralMaintenanceAssetTypeList) {
    				int typeId = masterGeneralMaintenanceAssetType.getTypeId();
    				String type = masterGeneralMaintenanceAssetType.getTypePrimary();
    				sno++;
    				masterGeneralMaintenanceAssetTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (holdVendorStatusList != null && !holdVendorStatusList.isEmpty()) {
    			int sno = 2;
    			for (HoldVendorStatus holdVendorStatus : holdVendorStatusList) {
    				int typeId = holdVendorStatus.getHoldStatus();
    				String type = holdVendorStatus.getHoldStatusPrimary();
    				sno++;
    				masterHoldVendorStatusData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	if (masterNegativeSymbolSignTypesList != null && !masterNegativeSymbolSignTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes : masterNegativeSymbolSignTypesList) {
    				int typeId = masterNegativeSymbolSignTypes.getTypeId();
    				String type = masterNegativeSymbolSignTypes.getNegativeSymbolSignType();
    				sno++;
    				masternegativeSymbolSignTypesData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterNegativeSymbolTypesList != null && !masterNegativeSymbolTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterNegativeSymbolTypes masterNegativeSymbolTypes : masterNegativeSymbolTypesList) {
    				int typeId = masterNegativeSymbolTypes.getTypeId();
    				String type = masterNegativeSymbolTypes.getNegativeSymbolType();
    				sno++;
    				masterNegativeSymbolTypesData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	if (masterRateFrequencyTypesList != null && !masterRateFrequencyTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterRateFrequencyTypes masterRateFrequencyTypes : masterRateFrequencyTypesList) {
    				int typeId = masterRateFrequencyTypes.getTypeId();
    				String type = masterRateFrequencyTypes.getRateFrequencyType();
    				sno++;
    				masterRateFrequencyTypesData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterRMAgeingTypesList != null && !masterRMAgeingTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterRMAgeingTypes masterRMAgeingTypes : masterRMAgeingTypesList) {
    				int typeId = masterRMAgeingTypes.getTypeId();
    				String type = masterRMAgeingTypes.getTypePrimary();
    				sno++;
    				masterRMAgeingTypesData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterRMApplyByDefaultTypesList != null && !masterRMApplyByDefaultTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterRMApplyByDefaultTypes masterRMApplyByDefaultTypes : masterRMApplyByDefaultTypesList) {
    				int typeId = masterRMApplyByDefaultTypes.getTypeId();
    				String type = masterRMApplyByDefaultTypes.getTypePrimary();
    				sno++;
    				masterRMApplyByDefaultTypesData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterSeperatorDecimalList != null && !masterSeperatorDecimalList.isEmpty()) {
    			int sno = 2;
    			for (MasterSeperatorDecimal masterSeperatorDecimal : masterSeperatorDecimalList) {
    				int typeId = masterSeperatorDecimal.getSeperatorDecimalId();
    				String type = masterSeperatorDecimal.getSeperatorDecimalPrimary();
    				sno++;
    				masterSeperatorDecimalData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (masterSeperatorThousandsList != null && !masterSeperatorThousandsList.isEmpty()) {
    			int sno = 2;
    			for (MasterSeperatorThousands masterSeperatorThousands : masterSeperatorThousandsList) {
    				int typeId = masterSeperatorThousands.getSeperatorThousandId();
    				String type = masterSeperatorThousands.getSeperatorThousandPrimary();
    				sno++;
    				masterSeperatorThousandsData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	if (masterVendorStatusList != null && !masterVendorStatusList.isEmpty()) {
    			int sno = 2;
    			for (MasterVendorStatus masterVendorStatus : masterVendorStatusList) {
    				int typeId = masterVendorStatus.getStatusId();
    				String type = masterVendorStatus.getStatusPrimary();
    				sno++;
    				masterVendorStatusData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (pmSetupFormatTypeList != null && !pmSetupFormatTypeList.isEmpty()) {
    			int sno = 2;
    			for (PMSetupFormatType pmSetupFormatType : pmSetupFormatTypeList) {
    				int typeId = pmSetupFormatType.getTypeId();
    				String type = pmSetupFormatType.getTypePrimary();
    				sno++;
    				pmSetupFormatTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	if (seriesTypeList != null && !seriesTypeList.isEmpty()) {
    			int sno = 2;
    			for (SeriesType seriesType : seriesTypeList) {
    				int typeId = seriesType.getTypeId();
    				String type = seriesType.getTypePrimary();
    				sno++;
    				seriesTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	if (shipmentMethodTypeList != null && !shipmentMethodTypeList.isEmpty()) {
    			int sno = 2;
    			for (ShipmentMethodType shipmentMethodType : shipmentMethodTypeList) {
    				int typeId = shipmentMethodType.getTypeId();
    				String type = shipmentMethodType.getTypePrimary();
    				sno++;
    				shipmentMethodTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	 
        	if (tpclBalanceTypeList != null && !tpclBalanceTypeList.isEmpty()) {
    			int sno = 2;
    			for (TPCLBalanceType tpclBalanceType : tpclBalanceTypeList) {
    				int typeId = tpclBalanceType.getTypeId();
    				String type = tpclBalanceType.getTypePrimary();
    				sno++;
    				tpclblncTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	if (vatBasedOnTypeList != null && !vatBasedOnTypeList.isEmpty()) {
    			int sno = 2;
    			for (VATBasedOnType vatBasedOnType : vatBasedOnTypeList) {
    				int typeId = vatBasedOnType.getTypeId();
    				String type = vatBasedOnType.getTypePrimary();
    				sno++;
    				vatBasedOnTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
			if (vatTypeList != null && !vatTypeList.isEmpty()) {
				int sno = 2;
				for (VATType vatType : vatTypeList) {
					int typeId = vatType.getTypeId();
					String type = vatType.getTypePrimary();
					sno++;
					vatTypeData.put(sno, new Object[] { typeId, type, "" });
				}
			}
        	 
        	
            // Iterate over data and write to sheet
		Set<Integer> btiMessageKeyset = btiMessageData.keySet();
		Set<Integer> countryKeyset = countryData.keySet();
		Set<Integer> stateKeyset = stateData.keySet();
		Set<Integer> cityKeyset = cityData.keySet();
		Set<Integer> creditCardTypeKeyset = creditCardTypeData.keySet();
		Set<Integer> customerMaintenanceActiveStatusKeyset = customerMaintenanceActiveStatusData.keySet();
		Set<Integer> customerMaintenanceHoldStatusKeyset = customerMaintenanceHoldStatusData.keySet();
		Set<Integer> faAccountTableAccountTypeKeyset = faAccountTableAccountTypeData.keySet();
		Set<Integer> faBookMaintenanceAmortizationCodeTypeKeyset = faBookMaintenanceAmortizationCodeTypeData
				.keySet();
		Set<Integer> faBookMaintenanceAveragingConventionTypeKeyset = faBookMaintenanceAveragingConventionTypeData
				.keySet();
		Set<Integer> faBookMaintenanceSwitchoverTypeKeyset = faBookMaintenanceSwitchoverTypeData.keySet();
		Set<Integer> glConfigurationAuditTrialSeriesTypeKeyset = glConfigurationAuditTrialSeriesTypeData
				.keySet();

		Set<Integer> leaseTypeKeyset = leaseTypeData.keySet();
		Set<Integer> masterAccountTypeKeyset = masterAccountTypeData.keySet();
		Set<Integer> masterARAccountTypeKeyset = masterARAccountTypeData.keySet();
		Set<Integer> masterBlncdsplKeyset = masterBlncdsplData.keySet();
		Set<Integer> masterCustomerClassBalanceTypeKeyset = masterCustomerClassBalanceTypeData.keySet();
		Set<Integer> masterCustomerClassMinimumChargeKeyset = masterCustomerClassMinimumChargeData.keySet();
		Set<Integer> masterCustomerClassSetupCreditLimitKeyset = masterCustomerClassSetupCreditLimitData
				.keySet();
		Set<Integer> masterCustomerClassSetupFinanceChargeKeyset = masterCustomerClassSetupFinanceChargeData
				.keySet();
		Set<Integer> masterDepreciationPeriodTypesKeyset = masterDepreciationPeriodTypesData.keySet();
		Set<Integer> masterDiscountTypeKeyset = masterDiscountTypeData.keySet();
		Set<Integer> masterDiscountTypePeriodKeyset = masterDiscountTypePeriodData.keySet();
		Set<Integer> masterDueTypesKeyset = masterDueTypesData.keySet();
		Set<Integer> masterFAAccountGroupAccountTypesKeyset = masterFAAccountGroupAccountTypesData.keySet();
		Set<Integer> masterGeneralMaintenanceAssetStatusKeyset = masterGeneralMaintenanceAssetStatusData
				.keySet();
		Set<Integer> masterGeneralMaintenanceAssetTypeKeyset = masterGeneralMaintenanceAssetTypeData.keySet();
		Set<Integer> masterHoldVendorStatusKeyset = masterHoldVendorStatusData.keySet();
		Set<Integer> masterNegativeSymbolSignTypesKeyset = masternegativeSymbolSignTypesData.keySet();
		Set<Integer> masterNegativeSymbolTypesKeyset = masterNegativeSymbolTypesData.keySet();
		Set<Integer> masterRateFrequencyTypesKeyset = masterRateFrequencyTypesData.keySet();
		Set<Integer> masterRMAgeingTypesKeyset = masterRMAgeingTypesData.keySet();
		Set<Integer> masterRMApplyByDefaultTypesKeyset = masterRMApplyByDefaultTypesData.keySet();

		Set<Integer> masterSeperatorDecimalKeyset = masterSeperatorDecimalData.keySet();
		Set<Integer> masterSeperatorThousandsKeyset = masterSeperatorThousandsData.keySet();
		Set<Integer> masterVendorStatusKeyset = masterVendorStatusData.keySet();
		Set<Integer> pmSetupFormatTypeKeyset = pmSetupFormatTypeData.keySet();
		Set<Integer> seriesTypeKeyset = seriesTypeData.keySet();
		Set<Integer> shipmentMethodTypeKeyset = shipmentMethodTypeData.keySet();
		Set<Integer> tpclblncTypeKeyset = tpclblncTypeData.keySet();
		Set<Integer> vatBasedOnTypeKeyset = vatBasedOnTypeData.keySet();
		Set<Integer> vatTypeKeyset = vatTypeData.keySet();

       	 int rownum = 0;
		for (Integer key : creditCardTypeKeyset) {
			Row row = creditCardTypeSheet.createRow(rownum++);
			CellStyle style;
			if (rownum <= 1) {
				style = workbook.createCellStyle();// Create style
				XSSFFont font = workbook.createFont();// Create font
				font.setFontName("sans-serif");// set font type
				font.setBold(true);
				style.setFont(font);// set it to bold
				style.setAlignment(CellStyle.ALIGN_CENTER);
			} else {
				style = workbook.createCellStyle();
				XSSFFont font = workbook.createFont();
				font.setFontName("sans-serif");
				style.setFont(font);
			}
			Object[] objArr = creditCardTypeData.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				cell.setCellStyle(style);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}
       	rownum = 0;
        for (Integer key : customerMaintenanceActiveStatusKeyset) {
               Row row = customerMaintenanceActiveStatusSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = customerMaintenanceActiveStatusData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	rownum = 0;
        for (Integer key : customerMaintenanceHoldStatusKeyset) {
               Row row = customerMaintenanceHoldStatusSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = customerMaintenanceHoldStatusData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
       	rownum = 0;
        for (Integer key : faAccountTableAccountTypeKeyset) {
               Row row = faAccountTableAccountTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = faAccountTableAccountTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
     	rownum = 0;
        for (Integer key : faBookMaintenanceAmortizationCodeTypeKeyset) {
               Row row = faBookMaintenanceAmortizationCodeTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = faBookMaintenanceAmortizationCodeTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	rownum = 0;
        for (Integer key : faBookMaintenanceAveragingConventionTypeKeyset) {
               Row row = faBookMaintenanceAveragingConventionTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = faBookMaintenanceAveragingConventionTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
     	rownum = 0;
        for (Integer key : faBookMaintenanceSwitchoverTypeKeyset) {
               Row row = faBookMaintenanceSwitchoverTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = faBookMaintenanceSwitchoverTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	 
       	rownum = 0;
        for (Integer key : glConfigurationAuditTrialSeriesTypeKeyset) {
               Row row = glConfigurationAuditTrialSeriesTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = glConfigurationAuditTrialSeriesTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	 
        	rownum = 0;
         for (Integer key : leaseTypeKeyset) {
                Row row = leaseTypeSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = leaseTypeData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
         
         
       	rownum = 0;
        for (Integer key : masterAccountTypeKeyset) {
               Row row = masterAccountTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = masterAccountTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
      	rownum = 0;
        for (Integer key : masterARAccountTypeKeyset) {
               Row row = masterARAccountTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = masterARAccountTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
        
       	rownum = 0;
        for (Integer key : masterBlncdsplKeyset) {
               Row row = masterBlncdsplSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = masterBlncdsplData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
        
       	rownum = 0;
        for (Integer key : masterCustomerClassBalanceTypeKeyset) {
               Row row = masterCustomerClassBalanceTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = masterCustomerClassBalanceTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
         rownum = 0;
         for (Integer key : masterCustomerClassMinimumChargeKeyset) {
                Row row = masterCustomerClassMinimumChargeSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = masterCustomerClassMinimumChargeData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
         
         rownum = 0;
         for (Integer key : masterCustomerClassSetupCreditLimitKeyset) {
                Row row = masterCustomerClassSetupCreditLimitSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = masterCustomerClassSetupCreditLimitData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
       	
            rownum = 0;
            for (Integer key : masterCustomerClassSetupFinanceChargeKeyset) {
                   Row row = masterCustomerClassSetupFinanceChargeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterCustomerClassSetupFinanceChargeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterDepreciationPeriodTypesKeyset) {
                   Row row = masterDepreciationPeriodTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterDepreciationPeriodTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterDiscountTypeKeyset) {
                   Row row = masterDiscountTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterDiscountTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            
            rownum = 0;
            for (Integer key : masterDiscountTypePeriodKeyset) {
                   Row row = masterDiscountTypePeriodSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterDiscountTypePeriodData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterDueTypesKeyset) {
                   Row row = masterDueTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterDueTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterFAAccountGroupAccountTypesKeyset) {
                   Row row = masterFAAccountGroupAccountTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterFAAccountGroupAccountTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterGeneralMaintenanceAssetStatusKeyset) {
                   Row row = masterGeneralMaintenanceAssetStatusSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterGeneralMaintenanceAssetStatusData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterGeneralMaintenanceAssetTypeKeyset) {
                   Row row = masterGeneralMaintenanceAssetTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterGeneralMaintenanceAssetTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterHoldVendorStatusKeyset) {
                   Row row = masterHoldVendorStatusSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterHoldVendorStatusData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterNegativeSymbolSignTypesKeyset) {
                   Row row = masterNegativeSymbolSignTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masternegativeSymbolSignTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterNegativeSymbolTypesKeyset) {
                   Row row = masterNegativeSymbolTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterNegativeSymbolTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterRateFrequencyTypesKeyset) {
                   Row row = masterRateFrequencyTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterRateFrequencyTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterRMAgeingTypesKeyset) {
                   Row row = masterRMAgeingTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterRMAgeingTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterRMApplyByDefaultTypesKeyset) {
                   Row row = masterRMApplyBYDefaultTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterRMApplyByDefaultTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
        	    
            rownum = 0;
            for (Integer key : masterSeperatorDecimalKeyset) {
                   Row row = masterSeperatorDecimalSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterSeperatorDecimalData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterSeperatorThousandsKeyset) {
                   Row row = masterSeperatorThousandsSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterSeperatorThousandsData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterVendorStatusKeyset) {
                   Row row = masterVendorStatusSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterVendorStatusData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : pmSetupFormatTypeKeyset) {
                   Row row = pmSetupFormatTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = pmSetupFormatTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : seriesTypeKeyset) {
                   Row row = seriesTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = seriesTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : shipmentMethodTypeKeyset) {
                   Row row = shipmentMethodTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = shipmentMethodTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : tpclblncTypeKeyset) {
                   Row row = tpclblncTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = tpclblncTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : vatBasedOnTypeKeyset) {
                   Row row = vatBasedOnTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = vatBasedOnTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : vatTypeKeyset) {
                   Row row = vatTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = vatTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
             
            rownum = 0;
            for (Integer key : btiMessageKeyset) {
                   Row row = btiMessageMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = btiMessageData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
             rownum = 0;
            for (Integer key : countryKeyset) {
                   Row row = countryMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = countryData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : stateKeyset) {
                   Row row = stateMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = stateData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : cityKeyset) {
                   Row row = cityMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = cityData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            try {
                   // Write the workbook in file system
                   String excelFileName = "CompanyDB_MasterData.xlsx";
                   String fileName = URLEncoder.encode(excelFileName, "UTF-8");
                   fileName = URLDecoder.decode(fileName, "ISO8859_1");
                   response.setHeader("Content-disposition", "attachment; filename=" + fileName);
                   response.setContentType(mimetypesFileTypeMap.getContentType(excelFileName));

                   FileOutputStream out = new FileOutputStream(new File(temperotyFilePath + File.pathSeparator+ excelFileName));
                   workbook.write(out);
                   out.close();

                   ByteArrayOutputStream baos=null;
                   baos = convertexcelToByteArrayOutputStream(temperotyFilePath + File.pathSeparator + excelFileName);
                   OutputStream os = response.getOutputStream();
                   baos.writeTo(os);
                   os.flush();

            } catch (Exception e) {
            	LOGGER.error(e.getMessage());
            }
		
	}

	private ByteArrayOutputStream convertexcelToByteArrayOutputStream(String fileName) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (InputStream inputStream = new FileInputStream(fileName);){
			
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();

			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}

		} catch (IOException e) {
			LOGGER.error(e.getMessage());
		}  
		return baos;
	}


	public boolean importMasterData(String languageName, String languageOrientation, MultipartFile file) {

		int loggedInUserId = 0;
		String companyTenantId=httpServletRequest.getHeader(CommonConstant.TENANT_ID);
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		
		Language language = repositoryLanguage.findByLanguageNameAndIsDeleted(languageName, false);
		if (language != null) {
			return false;
		}
		language = new Language();
		language.setIsActive(true);
		language.setIsDeleted(false);
		language.setLanguageName(languageName);
		language.setCreatedBy(loggedInUserId);
		language.setUpdatedBy(loggedInUserId);
		language.setLanguageOrientation(languageOrientation);
		language = repositoryLanguage.saveAndFlush(language);
		Workbook workbook;
		
		try {
			    InputStream inputStream = file.getInputStream();
			    workbook = new XSSFWorkbook(inputStream);

				Sheet masterGeneralMaintenanceAssetTypeSheet = workbook.getSheetAt(0);
				Sheet masterGeneralMaintenanceAssetStatusSheet = workbook.getSheetAt(1);
				Sheet btiMessageMasterSheet = workbook.getSheetAt(2);
				Sheet countryMasterSheet = workbook.getSheetAt(3);
				Sheet stateMasterSheet = workbook.getSheetAt(4);
				Sheet cityMasterSheet = workbook.getSheetAt(5);
				Sheet creditCardTypeSheet = workbook.getSheetAt(6);
				Sheet customerMaintenanceActiveStatusSheet = workbook.getSheetAt(7);
				Sheet customerMaintenanceHoldStatusSheet = workbook.getSheetAt(8);
				Sheet faAccountTableAccountTypeSheet = workbook.getSheetAt(9);
				Sheet faBookMaintenanceAmortizationCodeTypeSheet = workbook.getSheetAt(10);
				Sheet faBookMaintenanceAveragingConventionTypeSheet = workbook.getSheetAt(11);
				Sheet faBookMaintenanceSwitchOverTypeSheet = workbook.getSheetAt(12);
				Sheet glConfigurationAuditTrialSeriesTypeSheet = workbook.getSheetAt(13);
				Sheet leaseTypeSheet = workbook.getSheetAt(14);
				Sheet masterAccountTypeSheet = workbook.getSheetAt(15);
				Sheet masterArAccountTypeSheet = workbook.getSheetAt(16);
				Sheet masterBlncdsplSheet = workbook.getSheetAt(17);
				Sheet masterCustomerClassSetupBalanceTypeSheet = workbook.getSheetAt(18);
				Sheet masterCustomerClassMinimumChargeSheet = workbook.getSheetAt(19);
				Sheet masterCustomerClassSetupCreditLimitSheet = workbook.getSheetAt(20);
				Sheet masterCustomerClassSetupFinanaceChargeSheet = workbook.getSheetAt(21);
				Sheet masterDepreciationPeriodTypesSheet = workbook.getSheetAt(22);
				Sheet masterDiscountTypeSheet = workbook.getSheetAt(23);
				Sheet masterDiscountTypePeriodSheet = workbook.getSheetAt(24);
				Sheet masterDueTypesSheet = workbook.getSheetAt(25);
				Sheet masterFAAccountGroupAccountTypeSheet = workbook.getSheetAt(26);
				Sheet holdVendorStatusSheet = workbook.getSheetAt(27);
				Sheet masterNegativeSymbolSignTypesSheet = workbook.getSheetAt(28);
				Sheet masterNegativeSymbolTypesSheet = workbook.getSheetAt(29);
				Sheet masterRateFrequencyTypesSheet = workbook.getSheetAt(30);
				Sheet masterRMAgeingTypesSheet = workbook.getSheetAt(31);
				Sheet masterRMApplyByDefaultTypesSheet = workbook.getSheetAt(32);
				Sheet masterSeperatorDecimalSheet = workbook.getSheetAt(33);
				Sheet masterSeperatorThousandsSheet = workbook.getSheetAt(34);
				Sheet masterVendorStatusSheet = workbook.getSheetAt(35);
				Sheet pmSetupFormatTypeSheet = workbook.getSheetAt(36);
				Sheet seriesTypeSheet = workbook.getSheetAt(37);
				Sheet shipmentMethodTypeSheet = workbook.getSheetAt(38);
				Sheet tpclBalanceTypeSheet = workbook.getSheetAt(39);
				Sheet vatBasedOnTypeSheet = workbook.getSheetAt(40);
				Sheet vatTypeSheet = workbook.getSheetAt(41);
			 	//
				long countAssetType=repositoryMasterGeneralMaintenanceAssetType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countAssetType>masterGeneralMaintenanceAssetTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countAssetStatus=repositoryMasterGeneralMaintenanceAssetStatus.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countAssetStatus>masterGeneralMaintenanceAssetStatusSheet.getLastRowNum()-1){
					return false;
				}
				
				long countBtiMessage= repositoryBtiMessage.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countBtiMessage>btiMessageMasterSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCountry= repositoryCountryMaster.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCountry>countryMasterSheet.getLastRowNum()-1){
					return false;
				}
				
				long countState= repositoryStateMaster.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countState>stateMasterSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCity= repositoryCityMaster.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCity>cityMasterSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCreditCard= repositoryCreditCardType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCreditCard>creditCardTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCustomerMainActive= repositoryCustomerMaintenanceActiveStatus.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCustomerMainActive>customerMaintenanceActiveStatusSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCustomerMainHold= repositoryCustomerMaintenanceHoldStatus.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCustomerMainHold>customerMaintenanceHoldStatusSheet.getLastRowNum()-1){
					return false;
				}
				
				long faAccountType= repositoryFAAccountTableAccountType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(faAccountType>faAccountTableAccountTypeSheet.getLastRowNum()-1){
					return false;
				}

				long faAmortizationType= repositoryFABookMaintenanceAmortizationCodeType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(faAmortizationType>faBookMaintenanceAmortizationCodeTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long faAverageConvType= repositoryFABookMaintenanceAveragingConventionType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(faAverageConvType>faBookMaintenanceAveragingConventionTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long faSwitchOverType= repositoryFABookMaintenanceSwitchOverType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(faSwitchOverType>faBookMaintenanceSwitchOverTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countAuditTrailSeriesType= repositoryGLConfigurationAuditTrialSeriesType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countAuditTrailSeriesType>glConfigurationAuditTrialSeriesTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countLeaseType= repositoryLeaseType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countLeaseType>leaseTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countAccountTypeSheet= repositoryMasterAccountType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countAccountTypeSheet>masterAccountTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countArAccountTypeSheet= repositoryMasterArAccountType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countArAccountTypeSheet>masterArAccountTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countBlncDspl= repositoryMasterBlncdspl.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countBlncDspl>masterBlncdsplSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCustomerClassSetupBalanceType= repositoryMasterCustomerClassSetupBalanceType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCustomerClassSetupBalanceType>masterCustomerClassSetupBalanceTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCustomerClassMinimumCharge= repositoryMasterCustomerClassMinimumCharge.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCustomerClassMinimumCharge>masterCustomerClassMinimumChargeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCustomerClassCreditLimit = repositoryMasterCustomerClassSetupCreditLimit.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCustomerClassCreditLimit>masterCustomerClassSetupCreditLimitSheet.getLastRowNum()-1){
					return false;
				}
				
				long countCustomerClassFinanceCharge= repositoryMasterCustomerClassSetupFinanaceCharge.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countCustomerClassFinanceCharge>masterCustomerClassSetupFinanaceChargeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countDeprecationPeriod= repositoryMasterDerpreciationPeriodTypes.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countDeprecationPeriod>masterDepreciationPeriodTypesSheet.getLastRowNum()-1){
					return false;
				}
				
				long countDiscountType= repositoryMasterDiscountType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countDiscountType>masterDiscountTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countDiscountTypePeriod= repositoryMasterDiscountTypePeriod.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countDiscountTypePeriod>masterDiscountTypePeriodSheet.getLastRowNum()-1){
					return false;
				}
				
				long countDueType= repositoryMasterDueTypes.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countDueType>masterDueTypesSheet.getLastRowNum()-1){
					return false;
				}
				
				long countFaAccountGroupAccountType = repositoryMasterFAAccountGroupAccountType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countFaAccountGroupAccountType>masterFAAccountGroupAccountTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countHoldVendorStatus = repositoryHoldVendorStatus.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countHoldVendorStatus>holdVendorStatusSheet.getLastRowNum()-1){
					return false;
				}
				
				long countNegativeSymbol= repositoryMasterNegativeSymbolSignTypes.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countNegativeSymbol>masterNegativeSymbolSignTypesSheet.getLastRowNum()-1){
					return false;
				}
				
				long countMasterNegativeSymbolTypes= repositoryMasterNegativeSymbolTypes.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countMasterNegativeSymbolTypes>masterNegativeSymbolTypesSheet.getLastRowNum()-1){
					return false;
				}
				
				long countMasterRateFrequencyType= repositoryMasterRateFrequencyTypes.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countMasterRateFrequencyType>masterRateFrequencyTypesSheet.getLastRowNum()-1){
					return false;
				}
				
				long countMasterRmAgeingTypes= repositoryMasterRMAgeingTypes.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countMasterRmAgeingTypes>masterRMAgeingTypesSheet.getLastRowNum()-1){
					return false;
				}
				
				long countRmApplyByDefault= repositoryMasterRMApplyByDefaultTypes.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countRmApplyByDefault>masterRMApplyByDefaultTypesSheet.getLastRowNum()-1){
					return false;
				}
				
				long countSeperatorDecimal= repositoryMasterSeperatorDecimal.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countSeperatorDecimal>masterSeperatorDecimalSheet.getLastRowNum()-1){
					return false;
				}
				
				long countSeperatorThousand= repositoryMasterSeperatorThousands.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countSeperatorThousand>masterSeperatorThousandsSheet.getLastRowNum()-1){
					return false;
				}
				
				long countVendorStatus= repositoryMasterVendorStatus.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countVendorStatus>masterVendorStatusSheet.getLastRowNum()-1){
					return false;
				}
				
				long countPmFormat= repositoryPMSetupFormatType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countPmFormat>pmSetupFormatTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countSeriesType= repositorySeriesType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countSeriesType>seriesTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countShipmentType= repositoryShipmentMethodType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countShipmentType>shipmentMethodTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countTpclBalnceType= repositoryTPCLBalanceType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countTpclBalnceType>tpclBalanceTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countVatBasesOnType= repositoryVATBasedOnType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countVatBasesOnType>vatBasedOnTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				long countVatType= repositoryVATType.countByIsDeletedAndLanguageLanguageId(false,ENG_LANG_ID);
				if(countVatType>vatTypeSheet.getLastRowNum()-1){
					return false;
				}
				
				List<MasterGeneralMaintenanceAssetType> masterGeneralMaintenanceAssetTypeList = new ArrayList<>();
				for (int i = 1; i <= masterGeneralMaintenanceAssetTypeSheet.getLastRowNum(); i++) 
				{
					Row nextRow = masterGeneralMaintenanceAssetTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType = new MasterGeneralMaintenanceAssetType();
					masterGeneralMaintenanceAssetType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterGeneralMaintenanceAssetType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterGeneralMaintenanceAssetType.setLanguage(language);
					masterGeneralMaintenanceAssetTypeList.add(masterGeneralMaintenanceAssetType);
				}
				
				repositoryMasterGeneralMaintenanceAssetType.save(masterGeneralMaintenanceAssetTypeList);
				List<MasterGeneralMaintenanceAssetStatus> masterGeneralMaintenanceAssetStatusList = new ArrayList<>();
				for (int i = 1; i <= masterGeneralMaintenanceAssetStatusSheet.getLastRowNum(); i++) {
					Row nextRow = masterGeneralMaintenanceAssetStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus = new MasterGeneralMaintenanceAssetStatus();
					masterGeneralMaintenanceAssetStatus.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterGeneralMaintenanceAssetStatus.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterGeneralMaintenanceAssetStatus.setLanguage(language);
					masterGeneralMaintenanceAssetStatusList.add(masterGeneralMaintenanceAssetStatus);
					
				}
				repositoryMasterGeneralMaintenanceAssetStatus.save(masterGeneralMaintenanceAssetStatusList);
				
				List<BtiMessage> btiMessageList = new ArrayList<>();
				for (int i = 1; i <= btiMessageMasterSheet.getLastRowNum(); i++) {
					Row nextRow = btiMessageMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					BtiMessage btiMessage = new BtiMessage();

					if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
						btiMessage.setMessageShort(nextRow.getCell(0).getStringCellValue());
					} else if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						btiMessage.setMessageShort(String.valueOf((int) nextRow.getCell(0).getNumericCellValue()));

					}
					if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
						btiMessage.setMessage(nextRow.getCell(2).getStringCellValue());
					} else if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						btiMessage.setMessage(String.valueOf((int) nextRow.getCell(2).getNumericCellValue()));
					}

					btiMessage.setIsDeleted(false);
					btiMessage.setLanguage(language);
					btiMessage.setUpdatedBy(loggedInUserId);
					btiMessage.setCreatedBy(loggedInUserId);
					btiMessageList.add(btiMessage);
				
				}
				repositoryBtiMessage.save(btiMessageList);
				List<CityMaster> cityMasterList = new ArrayList<>();
				for (int i = 1; i <= cityMasterSheet.getLastRowNum(); i++) {
					Row nextRow = cityMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CityMaster cityMaster = new CityMaster();
					cityMaster.setStateMaster(repositoryStateMaster
							.findByStateIdAndIsDeleted((int) (nextRow.getCell(0).getNumericCellValue()), false));

					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						cityMaster.setCityCode(nextRow.getCell(1).getStringCellValue());
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						cityMaster.setCityCode(String.valueOf((int) nextRow.getCell(1).getNumericCellValue()));
					}
					if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_STRING) {
						cityMaster.setCityName(nextRow.getCell(3).getStringCellValue());
					} else if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						cityMaster.setCityName(String.valueOf((int) nextRow.getCell(3).getNumericCellValue()));
					}

					cityMaster.setIsDeleted(false);
					cityMaster.setLanguage(language);
					cityMaster.setUpdatedBy(loggedInUserId);
					cityMaster.setCreatedBy(loggedInUserId);
					cityMasterList.add(cityMaster);
					
				}
				repositoryCityMaster.save(cityMasterList);
				List<StateMaster> stateMasterList = new ArrayList<>();
				for (int i = 1; i <= stateMasterSheet.getLastRowNum(); i++) {
					Row nextRow = stateMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					StateMaster stateMaster = new StateMaster();
					CountryMaster countryMaster = repositoryCountryMaster
							.findByCountryIdAndIsDeleted((int) (nextRow.getCell(0).getNumericCellValue()), false);
					stateMaster.setCountryMaster(countryMaster);
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						stateMaster.setStateCode(nextRow.getCell(1).getStringCellValue());
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						stateMaster.setStateCode(String.valueOf((int) nextRow.getCell(1).getNumericCellValue()));
					}
					if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_STRING) {
						stateMaster.setStateName(nextRow.getCell(3).getStringCellValue());
					} else if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						stateMaster.setStateName(String.valueOf((int) nextRow.getCell(3).getNumericCellValue()));
					}

					stateMaster.setIsDeleted(false);
					stateMaster.setLanguage(language);
					stateMaster.setUpdatedBy(loggedInUserId);
					stateMaster.setCreatedBy(loggedInUserId);
					stateMasterList.add(stateMaster);
				}
				repositoryStateMaster.save(stateMasterList);
				List<CountryMaster> countryMasterList = new ArrayList<>();
				for (int i = 1; i <= countryMasterSheet.getLastRowNum(); i++) {
					Row nextRow = countryMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CountryMaster countryMaster = new CountryMaster();
					countryMaster.setActive(true);

					if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setCountryCode(nextRow.getCell(0).getStringCellValue());
					} else if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setCountryCode(String.valueOf((int) nextRow.getCell(0).getNumericCellValue()));
					}

					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setShortName(nextRow.getCell(1).getStringCellValue());
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setShortName(String.valueOf((int) nextRow.getCell(1).getNumericCellValue()));
					}
					if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setCountryName(nextRow.getCell(3).getStringCellValue());
					} else if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setCountryName(String.valueOf((int) nextRow.getCell(3).getNumericCellValue()));
					}
					countryMaster.setIsDeleted(false);
					countryMaster.setLanguage(language);
					countryMaster.setUpdatedBy(loggedInUserId);
					countryMaster.setCreatedBy(loggedInUserId);
					countryMasterList.add(countryMaster);
				}
				repositoryCountryMaster.save(countryMasterList);
				List<CreditCardType> creditCardTypeList = new ArrayList<>();
				for (int i = 1; i <= creditCardTypeSheet.getLastRowNum(); i++) {
					Row nextRow = creditCardTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CreditCardType creditCardType = new CreditCardType();
					creditCardType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					creditCardType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					creditCardType.setLanguage(language);
					creditCardTypeList.add(creditCardType);
					
				}
				repositoryCreditCardType.save(creditCardTypeList);
				List<CustomerMaintenanceActiveStatus> customerMaintenanceActiveStatusList = new ArrayList<>();
				for (int i = 1; i <= customerMaintenanceActiveStatusSheet.getLastRowNum(); i++) {
					Row nextRow = customerMaintenanceActiveStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CustomerMaintenanceActiveStatus customerMaintenanceActiveStatus = new CustomerMaintenanceActiveStatus();
					customerMaintenanceActiveStatus.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					customerMaintenanceActiveStatus.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					customerMaintenanceActiveStatus.setLanguage(language);
					customerMaintenanceActiveStatusList.add(customerMaintenanceActiveStatus);
			
				}
				repositoryCustomerMaintenanceActiveStatus.save(customerMaintenanceActiveStatusList);
				List<CustomerMaintenanceHoldStatus> customerMaintenanceHoldStatusList = new ArrayList<>();
				
				for (int i = 1; i <= customerMaintenanceHoldStatusSheet.getLastRowNum(); i++) {
					Row nextRow = customerMaintenanceHoldStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CustomerMaintenanceHoldStatus customerMaintenanceHoldStatus = new CustomerMaintenanceHoldStatus();
					customerMaintenanceHoldStatus.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					customerMaintenanceHoldStatus.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					customerMaintenanceHoldStatus.setLanguage(language);
					customerMaintenanceHoldStatusList.add(customerMaintenanceHoldStatus);
					
				}
				repositoryCustomerMaintenanceHoldStatus.save(customerMaintenanceHoldStatusList);
				List<FAAccountTableAccountType> faAccountTableAccountTypeList = new ArrayList<>();
				
				for (int i = 1; i <= faAccountTableAccountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = faAccountTableAccountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					FAAccountTableAccountType faAccountTableAccountType = new FAAccountTableAccountType();
					faAccountTableAccountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					faAccountTableAccountType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					faAccountTableAccountType.setLanguage(language);
					faAccountTableAccountTypeList.add(faAccountTableAccountType);
					
				}
				repositoryFAAccountTableAccountType.save(faAccountTableAccountTypeList);
				List<FABookMaintenanceAmortizationCodeType> faBookMaintenanceAmortizationCodeTypeList = new ArrayList<>();
				
				for (int i = 1; i <= faBookMaintenanceAmortizationCodeTypeSheet.getLastRowNum(); i++) {
					Row nextRow = faBookMaintenanceAmortizationCodeTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					FABookMaintenanceAmortizationCodeType faBookMaintenanceAmortizationCodeType = new FABookMaintenanceAmortizationCodeType();
					faBookMaintenanceAmortizationCodeType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					faBookMaintenanceAmortizationCodeType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					faBookMaintenanceAmortizationCodeType.setLanguage(language);
					faBookMaintenanceAmortizationCodeTypeList.add(faBookMaintenanceAmortizationCodeType);
					
				}
				repositoryFABookMaintenanceAmortizationCodeType.save(faBookMaintenanceAmortizationCodeTypeList);
				
				List<FABookMaintenanceAveragingConventionType> faBookMaintenanceAveragingConventionTypeList = new ArrayList<>();
				
				for (int i = 1; i <= faBookMaintenanceAveragingConventionTypeSheet.getLastRowNum(); i++) {
					Row nextRow = faBookMaintenanceAveragingConventionTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					FABookMaintenanceAveragingConventionType faBookMaintenanceAveragingConventionType = new FABookMaintenanceAveragingConventionType();
					faBookMaintenanceAveragingConventionType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					faBookMaintenanceAveragingConventionType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					faBookMaintenanceAveragingConventionType.setLanguage(language);
					faBookMaintenanceAveragingConventionTypeList.add(faBookMaintenanceAveragingConventionType);
					
				}
				repositoryFABookMaintenanceAveragingConventionType
				.save(faBookMaintenanceAveragingConventionTypeList);
				List<FABookMaintenanceSwitchOverType> faBookMaintenanceSwitchOverTypeList = new ArrayList<>();
				
				for (int i = 1; i <= faBookMaintenanceSwitchOverTypeSheet.getLastRowNum(); i++) {
					Row nextRow = faBookMaintenanceSwitchOverTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					FABookMaintenanceSwitchOverType faBookMaintenanceSwitchOverType = new FABookMaintenanceSwitchOverType();
					faBookMaintenanceSwitchOverType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					faBookMaintenanceSwitchOverType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					faBookMaintenanceSwitchOverType.setLanguage(language);
					faBookMaintenanceSwitchOverTypeList.add(faBookMaintenanceSwitchOverType);
					
				}
				repositoryFABookMaintenanceSwitchOverType.save(faBookMaintenanceSwitchOverTypeList);
				
				List<GLConfigurationAuditTrialSeriesType> glConfigurationAuditTrialSeriesTypeList = new ArrayList<>();
				for (int i = 1; i <= glConfigurationAuditTrialSeriesTypeSheet.getLastRowNum(); i++) {
					Row nextRow = glConfigurationAuditTrialSeriesTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType = new GLConfigurationAuditTrialSeriesType();
					glConfigurationAuditTrialSeriesType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					glConfigurationAuditTrialSeriesType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					glConfigurationAuditTrialSeriesType.setLanguage(language);
					glConfigurationAuditTrialSeriesTypeList.add(glConfigurationAuditTrialSeriesType);
					
				}
				repositoryGLConfigurationAuditTrialSeriesType.save(glConfigurationAuditTrialSeriesTypeList);
				List<LeaseType> leaseTypeList = new ArrayList<>();
				
				for (int i = 1; i <= leaseTypeSheet.getLastRowNum(); i++) {
					Row nextRow = leaseTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					LeaseType leaseType = new LeaseType();
					leaseType.setLeaseTypeId((int) nextRow.getCell(0).getNumericCellValue());
					leaseType.setValue(nextRow.getCell(2).getStringCellValue());
					leaseType.setLanguage(language);
					leaseTypeList.add(leaseType);
					
				}
				repositoryLeaseType.save(leaseTypeList);
				List<MasterAccountType> masterAccountTypeList = new ArrayList<>();
				for (int i = 1; i <= masterAccountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterAccountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterAccountType masterAccountType = new MasterAccountType();
					masterAccountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterAccountType.setAccountType(nextRow.getCell(2).getStringCellValue());
					masterAccountType.setLanguage(language);
					masterAccountTypeList.add(masterAccountType);
				}
				repositoryMasterAccountType.save(masterAccountTypeList);
				List<MasterArAccountType> masterArAccountTypeList = new ArrayList<>();
				for (int i = 1; i <= masterArAccountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterArAccountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterArAccountType masterArAccountType = new MasterArAccountType();
					masterArAccountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterArAccountType.setAccountType(nextRow.getCell(2).getStringCellValue());
					masterArAccountType.setLanguage(language);
					masterArAccountTypeList.add(masterArAccountType);
					
				}
				repositoryMasterArAccountType.save(masterArAccountTypeList);
				List<MasterBlncdspl> masterBlncdsplList = new ArrayList<>();
				for (int i = 1; i <= masterBlncdsplSheet.getLastRowNum(); i++) {
					Row nextRow = masterBlncdsplSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterBlncdspl masterBlncdspl = new MasterBlncdspl();
					masterBlncdspl.setTypeId((short) nextRow.getCell(0).getNumericCellValue());
					masterBlncdspl.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterBlncdspl.setLanguage(language);
					masterBlncdsplList.add(masterBlncdspl);
				}
				repositoryMasterBlncdspl.save(masterBlncdsplList);
				List<MasterCustomerClassSetupBalanceType> masterCustomerClassSetupBalanceTypeList = new ArrayList<>();
				
				for (int i = 1; i <= masterCustomerClassSetupBalanceTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterCustomerClassSetupBalanceTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterCustomerClassSetupBalanceType masterCustomerClassSetupBalanceType = new MasterCustomerClassSetupBalanceType();
					masterCustomerClassSetupBalanceType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterCustomerClassSetupBalanceType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterCustomerClassSetupBalanceType.setLanguage(language);
					masterCustomerClassSetupBalanceTypeList.add(masterCustomerClassSetupBalanceType);
					
				}
				
				repositoryMasterCustomerClassSetupBalanceType.save(masterCustomerClassSetupBalanceTypeList);
				List<MasterCustomerClassMinimumCharge> masterCustomerClassMinimumChargeList = new ArrayList<>();
				
				for (int i = 1; i <= masterCustomerClassMinimumChargeSheet.getLastRowNum(); i++) {
					Row nextRow = masterCustomerClassMinimumChargeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterCustomerClassMinimumCharge masterCustomerClassMinimumCharge = new MasterCustomerClassMinimumCharge();
					masterCustomerClassMinimumCharge.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterCustomerClassMinimumCharge.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterCustomerClassMinimumCharge.setLanguage(language);
					masterCustomerClassMinimumChargeList.add(masterCustomerClassMinimumCharge);
					
				}
				repositoryMasterCustomerClassMinimumCharge.save(masterCustomerClassMinimumChargeList);
				List<MasterCustomerClassSetupCreditLimit> masterCustomerClassSetupCreditLimitList = new ArrayList<>();
				
				for (int i = 1; i <= masterCustomerClassSetupCreditLimitSheet.getLastRowNum(); i++) {
					Row nextRow = masterCustomerClassSetupCreditLimitSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterCustomerClassSetupCreditLimit masterCustomerClassSetupCreditLimit = new MasterCustomerClassSetupCreditLimit();
					masterCustomerClassSetupCreditLimit.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterCustomerClassSetupCreditLimit.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterCustomerClassSetupCreditLimit.setLanguage(language);
					masterCustomerClassSetupCreditLimitList.add(masterCustomerClassSetupCreditLimit);
				}
				repositoryMasterCustomerClassSetupCreditLimit.save(masterCustomerClassSetupCreditLimitList);
				List<MasterCustomerClassSetupFinanaceCharge> masterCustomerClassSetupFinanaceChargeList = new ArrayList<>();
				
				for (int i = 1; i <= masterCustomerClassSetupFinanaceChargeSheet.getLastRowNum(); i++) {
					Row nextRow = masterCustomerClassSetupFinanaceChargeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterCustomerClassSetupFinanaceCharge masterCustomerClassSetupFinanaceCharge = new MasterCustomerClassSetupFinanaceCharge();
					masterCustomerClassSetupFinanaceCharge.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterCustomerClassSetupFinanaceCharge.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterCustomerClassSetupFinanaceCharge.setLanguage(language);
					masterCustomerClassSetupFinanaceChargeList.add(masterCustomerClassSetupFinanaceCharge);
				}
				repositoryMasterCustomerClassSetupFinanaceCharge.save(masterCustomerClassSetupFinanaceChargeList);
				List<MasterDepreciationPeriodTypes> masterDepreciationPeriodTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterDepreciationPeriodTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterDepreciationPeriodTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterDepreciationPeriodTypes masterDepreciationPeriodTypes = new MasterDepreciationPeriodTypes();
					masterDepreciationPeriodTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterDepreciationPeriodTypes.setDepreciationPeriodType(nextRow.getCell(2).getStringCellValue());
					masterDepreciationPeriodTypes.setLanguage(language);
					masterDepreciationPeriodTypesList.add(masterDepreciationPeriodTypes);
					
				}
				repositoryMasterDerpreciationPeriodTypes.save(masterDepreciationPeriodTypesList);
				List<MasterDiscountType> masterDiscountTypeList = new ArrayList<>();
				
				for (int i = 1; i <= masterDiscountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterDiscountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterDiscountType masterDiscountType = new MasterDiscountType();
					masterDiscountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterDiscountType.setDiscountType(nextRow.getCell(2).getStringCellValue());
					masterDiscountType.setLanguage(language);
					masterDiscountTypeList.add(masterDiscountType);
					
				}
				repositoryMasterDiscountType.save(masterDiscountTypeList);
				List<MasterDiscountTypePeriod> masterDiscountTypePeriodList = new ArrayList<>();
				

				for (int i = 1; i <= masterDiscountTypePeriodSheet.getLastRowNum(); i++) {
					Row nextRow = masterDiscountTypePeriodSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterDiscountTypePeriod masterDiscountTypePeriod = new MasterDiscountTypePeriod();
					masterDiscountTypePeriod.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterDiscountTypePeriod.setDiscountTypePriod(nextRow.getCell(2).getStringCellValue());
					masterDiscountTypePeriod.setLanguage(language);
					masterDiscountTypePeriodList.add(masterDiscountTypePeriod);
					
				}
				repositoryMasterDiscountTypePeriod.save(masterDiscountTypePeriodList);
				List<MasterDueTypes> masterDueTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterDueTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterDueTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterDueTypes masterDueTypes = new MasterDueTypes();
					masterDueTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterDueTypes.setDueType(nextRow.getCell(2).getStringCellValue());
					masterDueTypes.setLanguage(language);
					masterDueTypesList.add(masterDueTypes);
					
				}
				repositoryMasterDueTypes.save(masterDueTypesList);
				List<MasterFAAccountGroupAccountType> masterFAAccountGroupAccountTypeList = new ArrayList<>();
				
				for (int i = 1; i <= masterFAAccountGroupAccountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterFAAccountGroupAccountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterFAAccountGroupAccountType masterFAAccountGroupAccountType = new MasterFAAccountGroupAccountType();
					masterFAAccountGroupAccountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterFAAccountGroupAccountType.setAccountType(nextRow.getCell(2).getStringCellValue());
					masterFAAccountGroupAccountType.setLanguage(language);
					masterFAAccountGroupAccountTypeList.add(masterFAAccountGroupAccountType);
				}
				repositoryMasterFAAccountGroupAccountType.save(masterFAAccountGroupAccountTypeList);
				List<HoldVendorStatus> holdVendorStatusList = new ArrayList<>();
				
				for (int i = 1; i <= holdVendorStatusSheet.getLastRowNum(); i++) {
					Row nextRow = holdVendorStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					HoldVendorStatus holdVendorStatus = new HoldVendorStatus();
					holdVendorStatus.setHoldStatus((int) nextRow.getCell(0).getNumericCellValue());
					holdVendorStatus.setHoldStatusPrimary(nextRow.getCell(2).getStringCellValue());
					holdVendorStatus.setLanguage(language);
					holdVendorStatusList.add(holdVendorStatus);
					
				}
				repositoryHoldVendorStatus.save(holdVendorStatusList);
				List<MasterNegativeSymbolSignTypes> masterNegativeSymbolSignTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterNegativeSymbolSignTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterNegativeSymbolSignTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes = new MasterNegativeSymbolSignTypes();
					masterNegativeSymbolSignTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterNegativeSymbolSignTypes.setNegativeSymbolSignType(nextRow.getCell(2).getStringCellValue());
					masterNegativeSymbolSignTypes.setLanguage(language);
					masterNegativeSymbolSignTypesList.add(masterNegativeSymbolSignTypes);
					
				}
				repositoryMasterNegativeSymbolSignTypes.save(masterNegativeSymbolSignTypesList);
				List<MasterNegativeSymbolTypes> masterNegativeSymbolTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterNegativeSymbolTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterNegativeSymbolTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterNegativeSymbolTypes masterNegativeSymbolTypes = new MasterNegativeSymbolTypes();
					masterNegativeSymbolTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterNegativeSymbolTypes.setNegativeSymbolType(nextRow.getCell(2).getStringCellValue());
					masterNegativeSymbolTypes.setLanguage(language);
					masterNegativeSymbolTypesList.add(masterNegativeSymbolTypes);
					
				}
				repositoryMasterNegativeSymbolTypes.save(masterNegativeSymbolTypesList);
				List<MasterRateFrequencyTypes> masterRateFrequencyTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterRateFrequencyTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterRateFrequencyTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterRateFrequencyTypes masterRateFrequencyTypes = new MasterRateFrequencyTypes();
					masterRateFrequencyTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterRateFrequencyTypes.setRateFrequencyType(nextRow.getCell(2).getStringCellValue());
					masterRateFrequencyTypes.setLanguage(language);
					masterRateFrequencyTypesList.add(masterRateFrequencyTypes);
					
				}
				repositoryMasterRateFrequencyTypes.save(masterRateFrequencyTypesList);
				List<MasterRMAgeingTypes> masterRMAgeingTypesList = new ArrayList<>();
				
				
				for (int i = 1; i <= masterRMAgeingTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterRMAgeingTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterRMAgeingTypes masterRMAgeingTypes = new MasterRMAgeingTypes();
					masterRMAgeingTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterRMAgeingTypes.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterRMAgeingTypes.setLanguage(language);
					masterRMAgeingTypesList.add(masterRMAgeingTypes);
					
				}
				repositoryMasterRMAgeingTypes.save(masterRMAgeingTypesList);
				List<MasterRMApplyByDefaultTypes> masterRMApplyByDefaultTypesList = new ArrayList<>();
				
				
				for (int i = 1; i <= masterRMApplyByDefaultTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterRMApplyByDefaultTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterRMApplyByDefaultTypes masterRMApplyByDefaultTypes = new MasterRMApplyByDefaultTypes();
					masterRMApplyByDefaultTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterRMApplyByDefaultTypes.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					masterRMApplyByDefaultTypes.setLanguage(language);
					masterRMApplyByDefaultTypesList.add(masterRMApplyByDefaultTypes);
					
				}
				repositoryMasterRMApplyByDefaultTypes.save(masterRMApplyByDefaultTypesList);
				List<MasterSeperatorDecimal> masterSeperatorDecimalList = new ArrayList<>();
				
				for (int i = 1; i <= masterSeperatorDecimalSheet.getLastRowNum(); i++) {
					Row nextRow = masterSeperatorDecimalSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterSeperatorDecimal masterSeperatorDecimal = new MasterSeperatorDecimal();
					masterSeperatorDecimal.setSeperatorDecimalId((int) nextRow.getCell(0).getNumericCellValue());
					masterSeperatorDecimal.setSeperatorDecimalPrimary(nextRow.getCell(2).getStringCellValue());
					masterSeperatorDecimal.setLanguage(language);
					masterSeperatorDecimalList.add(masterSeperatorDecimal);
					
				}
				repositoryMasterSeperatorDecimal.save(masterSeperatorDecimalList);
				List<MasterSeperatorThousands> masterSeperatorThousandsList = new ArrayList<>();
				for (int i = 1; i <= masterSeperatorThousandsSheet.getLastRowNum(); i++) {
					Row nextRow = masterSeperatorThousandsSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterSeperatorThousands masterSeperatorThousands = new MasterSeperatorThousands();
					masterSeperatorThousands.setSeperatorThousandId((int) nextRow.getCell(0).getNumericCellValue());
					masterSeperatorThousands.setSeperatorThousandPrimary(nextRow.getCell(2).getStringCellValue());
					masterSeperatorThousands.setLanguage(language);
					masterSeperatorThousandsList.add(masterSeperatorThousands);
					
				}
			 
				repositoryMasterSeperatorThousands.save(masterSeperatorThousandsList);
				List<MasterVendorStatus> masterVendorStatusList = new ArrayList<>();
				for (int i = 1; i <= masterVendorStatusSheet.getLastRowNum(); i++) {
					Row nextRow = masterVendorStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterVendorStatus masterVendorStatus = new MasterVendorStatus();
					masterVendorStatus.setStatusId((short) nextRow.getCell(0).getNumericCellValue());
					masterVendorStatus.setStatusPrimary(nextRow.getCell(2).getStringCellValue());
					masterVendorStatus.setLanguage(language);
					masterVendorStatusList.add(masterVendorStatus);
					
				}
				repositoryMasterVendorStatus.save(masterVendorStatusList);
				List<PMSetupFormatType> pmSetupFormatTypeList = new ArrayList<>();
				
				for (int i = 1; i <= pmSetupFormatTypeSheet.getLastRowNum(); i++) {
					Row nextRow = pmSetupFormatTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					PMSetupFormatType pmSetupFormatType = new PMSetupFormatType();
					pmSetupFormatType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					pmSetupFormatType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					pmSetupFormatType.setLanguage(language);
					pmSetupFormatTypeList.add(pmSetupFormatType);
					
				}
				repositoryPMSetupFormatType.save(pmSetupFormatTypeList);
				List<SeriesType> seriesTypeList = new ArrayList<>();
				
				for (int i = 1; i <= seriesTypeSheet.getLastRowNum(); i++) {
					Row nextRow = seriesTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					SeriesType seriesType = new SeriesType();
					seriesType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					seriesType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					seriesType.setLanguage(language);
					seriesTypeList.add(seriesType);
					
				}
				repositorySeriesType.save(seriesTypeList);
				List<ShipmentMethodType> shipmentMethodTypeList = new ArrayList<>();
				
				for (int i = 1; i <= shipmentMethodTypeSheet.getLastRowNum(); i++) {
					Row nextRow = shipmentMethodTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					ShipmentMethodType shipmentMethodType = new ShipmentMethodType();
					shipmentMethodType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					shipmentMethodType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					shipmentMethodType.setLanguage(language);
					shipmentMethodTypeList.add(shipmentMethodType);
					
				}
				repositoryShipmentMethodType.save(shipmentMethodTypeList);
				List<TPCLBalanceType> tpclBalanceTypeList = new ArrayList<>();
				
				for (int i = 1; i <= tpclBalanceTypeSheet.getLastRowNum(); i++) {
					Row nextRow = tpclBalanceTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					TPCLBalanceType tpclBalanceType = new TPCLBalanceType();
					tpclBalanceType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					tpclBalanceType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					tpclBalanceType.setLanguage(language);
					tpclBalanceTypeList.add(tpclBalanceType);
					
				}
				repositoryTPCLBalanceType.save(tpclBalanceTypeList);
				List<VATBasedOnType> vatBasedOnTypeList = new ArrayList<>();
				
				for (int i = 1; i <= vatBasedOnTypeSheet.getLastRowNum(); i++) {
					Row nextRow = vatBasedOnTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					VATBasedOnType vatBasedOnType = new VATBasedOnType();
					vatBasedOnType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					vatBasedOnType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					vatBasedOnType.setLanguage(language);
					vatBasedOnTypeList.add(vatBasedOnType);
					
				}
				repositoryVATBasedOnType.save(vatBasedOnTypeList);
				List<VATType> vatTypeList = new ArrayList<>();
				for (int i = 1; i <= vatTypeSheet.getLastRowNum(); i++) {
					Row nextRow = vatTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					VATType vatType = new VATType();
					vatType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					vatType.setTypePrimary(nextRow.getCell(2).getStringCellValue());
					vatType.setLanguage(language);
					vatTypeList.add(vatType);
					
				}
				repositoryVATType.save(vatTypeList);
				
			inputStream.close();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			language.setIsDeleted(true);
			language.setUpdatedBy(loggedInUserId);
			repositoryLanguage.saveAndFlush(language);
			return false;
		}
		return true;
	}
		
	public String getCompanyDetail(int companyId) 
    {
          try
          {
        	 String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
             JSONObject json = new JSONObject();
             json.put("companyId", companyId);
             String jsonString = json.toJSONString();
             Response response = RestAssured.given().header(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON)                          
                      .header(CommonConstant.LANG_ID,langId)
                          .body(jsonString).when()
                         .post(accesspath+"/getCompanyTenant").andReturn();
             JsonPath jsonpath =response.getBody().jsonPath();
             String code = jsonpath.getString("code");
             if(code.equalsIgnoreCase("200")){
                  return jsonpath.getString("result");
             }
          }
          catch(Exception e){
              LOGGER.info(Arrays.toString(e.getStackTrace()));
          }
          return null;
    }
	
	public String getCompanyName(int companyId) 
    {
          try
          {
        	 String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
             JSONObject json = new JSONObject();
             json.put("companyId", companyId);
             String jsonString = json.toJSONString();
             Response response = RestAssured.given().header(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON)                          
                      .header(CommonConstant.LANG_ID,langId)
                          .body(jsonString).when()
                         .post(accesspath+"/getCompanyName").andReturn();
             JsonPath jsonpath =response.getBody().jsonPath();
             String code = jsonpath.getString("code");
             if(code.equalsIgnoreCase("200")){
                  return jsonpath.getString("result");
             }
          }
          catch(Exception e){
              LOGGER.info(Arrays.toString(e.getStackTrace()));
          }
          return null;
    }
	
	public int getCompanyIdFromCompanyTenant(String tenantId) 
    {
          try
          {
        	 String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
             JSONObject json = new JSONObject();
             json.put("companyTenantId", tenantId);
             String jsonString = json.toJSONString();
             Response response = RestAssured.given().header(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON)                          
                      .header(CommonConstant.LANG_ID,langId)
                          .body(jsonString).when()
                         .post(accesspath+"/getCompanyIdFromTenant").andReturn();
             JsonPath jsonpath =response.getBody().jsonPath();
             String code = jsonpath.getString("code");
             if(code.equalsIgnoreCase("200")){
                  return jsonpath.getInt("result");
             }
          }
          catch(Exception e){
              LOGGER.info(Arrays.toString(e.getStackTrace()));
          }
          return 0;
    }
	
	public JsonPath getBankDistributionDetailByCheckbookId(String checkbookId,String tenantid) 
    {
          try
          {
        	 String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
             JSONObject json = new JSONObject();
             json.put("checkbookID", checkbookId);
             String jsonString = json.toJSONString();
             Response response = RestAssured.given().header(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON)                          
                      .header(CommonConstant.LANG_ID,langId)
                      .header(CommonConstant.TENANT_ID,tenantid)
                          .body(jsonString).when()
                         .post(accessfinancialpath+"/getBankDistributionDetailByCheckbookId").andReturn();
             JsonPath jsonpath =response.getBody().jsonPath();
             String code = jsonpath.getString("code");
             if(code.equalsIgnoreCase("200")){
                  return jsonpath;
             }
          }
          catch(Exception e){
              LOGGER.info(Arrays.toString(e.getStackTrace()));
          }
          return null;
    }

	public Map<String, String> getBankDistributionDetailByCheckbookId(String checkBookId) {
		Map<String, String> map= new HashMap<>();
		CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(checkBookId, false);
		String accountNumber="";
		String accountDesc="";
		String bankName="";
		String accountTableRowIndex="";
		String accountNumberWithZero="";
		if(checkbookMaintenance!=null && checkbookMaintenance.getGlAccountsTableAccumulation()!=null){
			
			Object[] object	=	serviceAccountPayable.getAccountNumber(checkbookMaintenance.getGlAccountsTableAccumulation());
			accountNumber=object[0].toString();
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				accountDesc=object[3].toString();
			}
			accountTableRowIndex=checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex();
			accountNumberWithZero=serviceAccountPayable.getAccountNumberIdsWithZero(checkbookMaintenance.getGlAccountsTableAccumulation());
		}
		if(checkbookMaintenance!=null && checkbookMaintenance.getBankSetup()!=null){
			bankName=checkbookMaintenance.getBankSetup().getBankDescription();
		}
		map.put("accountNumber", accountNumber);
		map.put("accountDesc", accountDesc);
		map.put("bankName",bankName );
		map.put("accountTableRowIndex", accountTableRowIndex);
		map.put("accountNumberWithZero", accountNumberWithZero);
		return map;
	}
	
	public JsonPath getAccountNumberDetailByAcoountIdAndCompanyId(String accountTableRowIndex,String tenantid) 
    {
          try
          {
        	 String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
             JSONObject json = new JSONObject();
             json.put("accountTableRowIndex", accountTableRowIndex);
             String jsonString = json.toJSONString();
             Response response = RestAssured.given().header(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON)                          
                      .header(CommonConstant.LANG_ID,langId)
                      .header(CommonConstant.TENANT_ID,tenantid)
                          .body(jsonString).when()
                         .post(accessfinancialpath+"/getAccountNumberDetailByAccountId").andReturn();
             JsonPath jsonpath =response.getBody().jsonPath();
             String code = jsonpath.getString("code");
             if(code.equalsIgnoreCase("200")){
                  return jsonpath;
             }
          }
          catch(Exception e){
              LOGGER.info(Arrays.toString(e.getStackTrace()));
          }
          return null;
    }

	public Map<String, String> getAccountNumberDetailByAcoountId(String accountTableIndex) {
		Map<String, String> map= new HashMap<>();
		GLAccountsTableAccumulation glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation.
				findByAccountTableRowIndexAndIsDeleted(accountTableIndex, false);
		String accountNumber="";
		String accountDesc="";
		String accountTableRowIndex="";
		if(glAccountsTableAccumulation!=null){
			
			Object[] object	=	serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
			accountNumber=object[0].toString();
			if(UtilRandomKey.isNotBlank(object[3].toString())){
				accountDesc=object[3].toString();
			}
			accountTableRowIndex=glAccountsTableAccumulation.getAccountTableRowIndex();
		}
		map.put("accountNumber", accountNumber);
		map.put("accountDesc", accountDesc);
		map.put("accountTableRowIndex", accountTableRowIndex);
		return map;
	}
	
	public void exporttestData(HttpServletRequest request, HttpServletResponse response) 
	{
        final ServletContext servletContext = request.getSession().getServletContext();
        MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
        final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        String temperotyFilePath = tempDirectory.getAbsolutePath();
		XSSFWorkbook workbook = new XSSFWorkbook();
        Sheet masterGeneralMaintenanceAssetTypeSheet = workbook.createSheet("MasterGeneralMaintenanceAssetType");
        Sheet masterGeneralMaintenanceAssetStatusSheet = workbook.createSheet("MasterGeneralMaintenanceAssetStatus");
      
        Sheet creditCardTypeSheet = workbook.createSheet("CreditCardType");
        Sheet leaseTypeSheet = workbook.createSheet("lease_type");
        Sheet vatTypeSheet = workbook.createSheet("vat_type");
        
        Map<Integer, Object[]> creditCardTypeData = new TreeMap<>();
        Map<Integer, Object[]> leaseTypeData = new TreeMap<>();
        Map<Integer, Object[]> vatTypeData = new TreeMap<>();
        
		for (int i = 0; i < 10; i++) 
		{

			creditCardTypeSheet.setColumnWidth(i, 10000);
			leaseTypeSheet.setColumnWidth(i, 10000);
		}
		
			creditCardTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
            leaseTypeData.put(1, new Object[] { "Type ID","Type","New Language Type"});
         
         	List<CreditCardType> creditCardTypeList = repositoryCreditCardType.findByIsDeletedAndLanguageLanguageId(false,1);
            List<LeaseType> leaseTypeList = repositoryLeaseType.findByIsDeletedAndLanguageLanguageId(false,1);
        
         
		if (creditCardTypeList != null && !creditCardTypeList.isEmpty()) {
			int sno = 2;
			for (CreditCardType creditCardType : creditCardTypeList) {
				int typeId = creditCardType.getTypeId();
				String type = creditCardType.getTypePrimary();
				sno++;
				creditCardTypeData.put(sno, new Object[] { typeId, type, "" });
			}
		}
        
        	
        	if (leaseTypeList != null && !leaseTypeList.isEmpty()) {
    			int sno = 2;
    			for (LeaseType leaseType : leaseTypeList) {
    				int typeId = leaseType.getLeaseTypeId();
    				String type = leaseType.getValue();
    				sno++;
    				leaseTypeData.put(sno, new Object[] { typeId, type, "" });
    			}
    		}
        	
        	
        	
			
        	 
        	
            // Iterate over data and write to sheet

		Set<Integer> creditCardTypeKeyset = creditCardTypeData.keySet();
		Set<Integer> leaseTypeKeyset = leaseTypeData.keySet();

       	 int rownum = 0;
		for (Integer key : creditCardTypeKeyset) {
			Row row = creditCardTypeSheet.createRow(rownum++);
			CellStyle style;
			if (rownum <= 1) {
				style = workbook.createCellStyle();// Create style
				XSSFFont font = workbook.createFont();// Create font
				font.setFontName("sans-serif");// set font type
				font.setBold(true);
				style.setFont(font);// set it to bold
				style.setAlignment(CellStyle.ALIGN_CENTER);
			} else {
				style = workbook.createCellStyle();
				XSSFFont font = workbook.createFont();
				font.setFontName("sans-serif");
				style.setFont(font);
			}
			Object[] objArr = creditCardTypeData.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				cell.setCellStyle(style);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}
       	
       	 
        	rownum = 0;
         for (Integer key : leaseTypeKeyset) {
                Row row = leaseTypeSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = leaseTypeData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
           
            try {
                   // Write the workbook in file system
                   String excelFileName = "CompanyDB_MasterData.xlsx";
                   String fileName = URLEncoder.encode(excelFileName, "UTF-8");
                   fileName = URLDecoder.decode(fileName, "ISO8859_1");
                   response.setHeader("Content-disposition", "attachment; filename=" + fileName);
                   response.setContentType(mimetypesFileTypeMap.getContentType(excelFileName));

                   FileOutputStream out = new FileOutputStream(new File(temperotyFilePath + File.pathSeparator+ excelFileName));
                   workbook.write(out);
                   out.close();

                   ByteArrayOutputStream baos=null;
                   baos = convertexcelToByteArrayOutputStream(temperotyFilePath + File.pathSeparator + excelFileName);
                   OutputStream os = response.getOutputStream();
                   baos.writeTo(os);
                   os.flush();

            } catch (Exception e) {
            	LOGGER.error(e.getMessage());
            }
	}

	public void exportMasterData(HttpServletRequest request, HttpServletResponse response, Integer languageId) {

        final ServletContext servletContext = request.getSession().getServletContext();
        MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
        final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        String temperotyFilePath = tempDirectory.getAbsolutePath();
		XSSFWorkbook workbook = new XSSFWorkbook();
        Sheet masterGeneralMaintenanceAssetTypeSheet = workbook.createSheet("MasterGeneralMaintenanceAssetType");
        Sheet masterGeneralMaintenanceAssetStatusSheet = workbook.createSheet("MasterGeneralMaintenanceAssetStatus");
        Sheet btiMessageMasterSheet = workbook.createSheet("BtiMessageMaster");
        Sheet countryMasterSheet = workbook.createSheet("CountryMaster");
        Sheet stateMasterSheet = workbook.createSheet("StateMaster");
        Sheet cityMasterSheet = workbook.createSheet("CityMaster");        
        Sheet creditCardTypeSheet = workbook.createSheet("CreditCardType");
        Sheet customerMaintenanceActiveStatusSheet = workbook.createSheet("CustomerMaintenanceActiveStatus");        
        Sheet customerMaintenanceHoldStatusSheet = workbook.createSheet("CustomerMaintenanceHoldStatus");
        Sheet faAccountTableAccountTypeSheet = workbook.createSheet("FAAccountTableAccountType");
        Sheet faBookMaintenanceAmortizationCodeTypeSheet = workbook.createSheet("fa_book_maintenance_amortization_code_type");              
        Sheet faBookMaintenanceAveragingConventionTypeSheet = workbook.createSheet("fa_book_maintenance_averaging_convention_type");
        Sheet faBookMaintenanceSwitchoverTypeSheet = workbook.createSheet("fa_book_maintenance_switchover_type");
        Sheet glConfigurationAuditTrialSeriesTypeSheet = workbook.createSheet("gl_configuration_audit_trial_series_type");      
        Sheet leaseTypeSheet = workbook.createSheet("lease_type");
        Sheet masterAccountTypeSheet = workbook.createSheet("master_account_type");
        Sheet masterARAccountTypeSheet = workbook.createSheet("master_ar_account_type");     
        Sheet masterBlncdsplSheet = workbook.createSheet("master_blncdspl");
        Sheet masterCustomerClassBalanceTypeSheet = workbook.createSheet("master_customer_class_balance_type");
        Sheet masterCustomerClassMinimumChargeSheet = workbook.createSheet("master_customer_class_minimum_charge");
        Sheet masterCustomerClassSetupCreditLimitSheet = workbook.createSheet("master_customer_class_setup_credit_limit");
        Sheet masterCustomerClassSetupFinanceChargeSheet = workbook.createSheet("master_customer_class_setup_finance_charge");
        Sheet masterDepreciationPeriodTypesSheet = workbook.createSheet("master_depreciation_period_types");
        Sheet masterDiscountTypeSheet = workbook.createSheet("master_discount_type");
        Sheet masterDiscountTypePeriodSheet = workbook.createSheet("master_discount_type_period");
        Sheet masterDueTypesSheet = workbook.createSheet("master_due_types");
        Sheet masterFAAccountGroupAccountTypesSheet = workbook.createSheet("master_fa_account_group_account_types");
        Sheet masterHoldVendorStatusSheet = workbook.createSheet("master_hold_vendor_status");
        Sheet masterNegativeSymbolSignTypesSheet = workbook.createSheet("master_negative_symbol_sign_types");
        Sheet masterNegativeSymbolTypesSheet = workbook.createSheet("master_negative_symbol_types");
        Sheet masterRateFrequencyTypesSheet = workbook.createSheet("master_rate_frequency_types");
        Sheet masterRMAgeingTypesSheet = workbook.createSheet("master_rm_ageing_types");
        Sheet masterRMApplyBYDefaultTypesSheet = workbook.createSheet("master_rm_apply_by_default_types");
        Sheet masterSeperatorDecimalSheet = workbook.createSheet("master_seperator_decimal");
        Sheet masterSeperatorThousandsSheet = workbook.createSheet("master_seperator_thousands");
        Sheet masterVendorStatusSheet = workbook.createSheet("master_vendor_status");
        Sheet pmSetupFormatTypeSheet = workbook.createSheet("pm_setup_format_type");
        Sheet seriesTypeSheet = workbook.createSheet("series_type");
        Sheet shipmentMethodTypeSheet = workbook.createSheet("shipment_method_type");
        Sheet tpclblncTypeSheet = workbook.createSheet("tpclblnc_type");
        Sheet vatBasedOnTypeSheet = workbook.createSheet("vat_based_on_type");
        Sheet vatTypeSheet = workbook.createSheet("vat_type");
        Map<Integer, Object[]> btiMessageData = new TreeMap<>();        
        Map<Integer, Object[]> countryData = new TreeMap<>();
        Map<Integer, Object[]> stateData = new TreeMap<>();
        Map<Integer, Object[]> cityData = new TreeMap<>();
        Map<Integer, Object[]> creditCardTypeData = new TreeMap<>();
        Map<Integer, Object[]> customerMaintenanceActiveStatusData = new TreeMap<>();      
        Map<Integer, Object[]> customerMaintenanceHoldStatusData = new TreeMap<>();
        Map<Integer, Object[]> faAccountTableAccountTypeData = new TreeMap<>();
        Map<Integer, Object[]> faBookMaintenanceAmortizationCodeTypeData = new TreeMap<>();
        Map<Integer, Object[]> faBookMaintenanceAveragingConventionTypeData = new TreeMap<>();
        Map<Integer, Object[]> faBookMaintenanceSwitchoverTypeData = new TreeMap<>();  
        Map<Integer, Object[]> glConfigurationAuditTrialSeriesTypeData = new TreeMap<>();
        Map<Integer, Object[]> leaseTypeData = new TreeMap<>();
        Map<Integer, Object[]> masterAccountTypeData = new TreeMap<>();
        Map<Integer, Object[]> masterARAccountTypeData = new TreeMap<>();
        Map<Integer, Object[]> masterBlncdsplData = new TreeMap<>();
        Map<Integer, Object[]> masterCustomerClassBalanceTypeData = new TreeMap<>();      
        Map<Integer, Object[]> masterCustomerClassMinimumChargeData = new TreeMap<>();
        Map<Integer, Object[]> masterCustomerClassSetupCreditLimitData = new TreeMap<>();
        Map<Integer, Object[]> masterCustomerClassSetupFinanceChargeData = new TreeMap<>();
        Map<Integer, Object[]> masterDepreciationPeriodTypesData = new TreeMap<>();        
        Map<Integer, Object[]> masterDiscountTypeData = new TreeMap<>();      
        Map<Integer, Object[]> masterDiscountTypePeriodData = new TreeMap<>();
        Map<Integer, Object[]> masterDueTypesData = new TreeMap<>();
        Map<Integer, Object[]> masterFAAccountGroupAccountTypesData = new TreeMap<>(); 
        Map<Integer, Object[]> masterGeneralMaintenanceAssetStatusData = new TreeMap<>();
        Map<Integer, Object[]> masterGeneralMaintenanceAssetTypeData = new TreeMap<>();      
        Map<Integer, Object[]> masterHoldVendorStatusData = new TreeMap<>();      
        Map<Integer, Object[]> masternegativeSymbolSignTypesData = new TreeMap<>();
        Map<Integer, Object[]> masterNegativeSymbolTypesData = new TreeMap<>();
        Map<Integer, Object[]> masterRateFrequencyTypesData = new TreeMap<>();        
        Map<Integer, Object[]> masterRMAgeingTypesData = new TreeMap<>();      
        Map<Integer, Object[]> masterRMApplyByDefaultTypesData = new TreeMap<>();
        Map<Integer, Object[]> masterSeperatorDecimalData = new TreeMap<>();
        Map<Integer, Object[]> masterSeperatorThousandsData = new TreeMap<>();
        Map<Integer, Object[]> masterVendorStatusData = new TreeMap<>();      
        Map<Integer, Object[]> pmSetupFormatTypeData = new TreeMap<>();
        Map<Integer, Object[]> seriesTypeData = new TreeMap<>();
        Map<Integer, Object[]> shipmentMethodTypeData = new TreeMap<>();
        Map<Integer, Object[]> tpclblncTypeData = new TreeMap<>();      
        Map<Integer, Object[]> vatBasedOnTypeData = new TreeMap<>();
        Map<Integer, Object[]> vatTypeData = new TreeMap<>();
        
		for (int i = 0; i < 10; i++) 
		{
			countryMasterSheet.setColumnWidth(i, 10000);
			stateMasterSheet.setColumnWidth(i, 10000);
			cityMasterSheet.setColumnWidth(i, 10000);
			btiMessageMasterSheet.setColumnWidth(i, 10000);
			creditCardTypeSheet.setColumnWidth(i, 10000);
			customerMaintenanceActiveStatusSheet.setColumnWidth(i, 10000);
			customerMaintenanceHoldStatusSheet.setColumnWidth(i, 10000);
			faAccountTableAccountTypeSheet.setColumnWidth(i, 10000);
			faBookMaintenanceAmortizationCodeTypeSheet.setColumnWidth(i, 10000);
			faBookMaintenanceAveragingConventionTypeSheet.setColumnWidth(i, 10000);
			faBookMaintenanceSwitchoverTypeSheet.setColumnWidth(i, 10000);
			glConfigurationAuditTrialSeriesTypeSheet.setColumnWidth(i, 10000);
			leaseTypeSheet.setColumnWidth(i, 10000);
			masterAccountTypeSheet.setColumnWidth(i, 10000);
			masterARAccountTypeSheet.setColumnWidth(i, 10000);
			masterBlncdsplSheet.setColumnWidth(i, 10000);
			masterCustomerClassBalanceTypeSheet.setColumnWidth(i, 10000);
			masterCustomerClassMinimumChargeSheet.setColumnWidth(i, 10000);
			masterCustomerClassSetupCreditLimitSheet.setColumnWidth(i, 10000);
			masterCustomerClassSetupFinanceChargeSheet.setColumnWidth(i, 10000);
			masterDepreciationPeriodTypesSheet.setColumnWidth(i, 10000);
			masterDiscountTypeSheet.setColumnWidth(i, 10000);
			masterDiscountTypePeriodSheet.setColumnWidth(i, 10000);
			masterDueTypesSheet.setColumnWidth(i, 10000);
			masterFAAccountGroupAccountTypesSheet.setColumnWidth(i, 10000);
			masterGeneralMaintenanceAssetStatusSheet.setColumnWidth(i, 10000);
			masterGeneralMaintenanceAssetTypeSheet.setColumnWidth(i, 10000);
			masterHoldVendorStatusSheet.setColumnWidth(i, 10000);
			masterNegativeSymbolSignTypesSheet.setColumnWidth(i, 10000);
			masterNegativeSymbolTypesSheet.setColumnWidth(i, 10000);
			masterRateFrequencyTypesSheet.setColumnWidth(i, 10000);
			masterRMAgeingTypesSheet.setColumnWidth(i, 10000);
			masterRMApplyBYDefaultTypesSheet.setColumnWidth(i, 10000);
			masterSeperatorDecimalSheet.setColumnWidth(i, 10000);
			masterSeperatorThousandsSheet.setColumnWidth(i, 10000);
			masterVendorStatusSheet.setColumnWidth(i, 10000);
			pmSetupFormatTypeSheet.setColumnWidth(i, 10000);
			seriesTypeSheet.setColumnWidth(i, 10000);
			shipmentMethodTypeSheet.setColumnWidth(i, 10000);
			tpclblncTypeSheet.setColumnWidth(i, 10000);
			vatBasedOnTypeSheet.setColumnWidth(i, 10000);
			vatTypeSheet.setColumnWidth(i, 10000);
		}
		
			btiMessageData.put(1, new Object[] { "Message Short Name","Message"});
        	countryData.put(1, new Object[] { "Country Code","Short Name","Country Name"});
        	stateData.put(1, new Object[] { "Country Id","State Code","State Name"});
        	cityData.put(1, new Object[] { "State Id","City Code","City Name"});
        	creditCardTypeData.put(1, new Object[] { "Type ID","Type"});
            customerMaintenanceActiveStatusData.put(1, new Object[] {  "Type ID","Type"});
            customerMaintenanceHoldStatusData.put(1, new Object[] { "Type ID","Type"});
            faAccountTableAccountTypeData.put(1, new Object[] { "Type ID","Type"});
            faBookMaintenanceAmortizationCodeTypeData.put(1, new Object[] { "Type ID","Type"});
            faBookMaintenanceAveragingConventionTypeData.put(1, new Object[] { "Type ID","Type"});
            faBookMaintenanceSwitchoverTypeData.put(1, new Object[] {  "Type ID","Type"});
            glConfigurationAuditTrialSeriesTypeData.put(1, new Object[] { "Type ID","Type"});
            leaseTypeData.put(1, new Object[] { "Type ID","Type"});
            masterAccountTypeData.put(1, new Object[] { "Type ID","Type"});
            masterARAccountTypeData.put(1, new Object[] {"Type ID","Type" });
            masterBlncdsplData.put(1, new Object[] {"Type ID","Type"});
            masterCustomerClassBalanceTypeData.put(1, new Object[] {"Type ID","Type"});
            masterCustomerClassMinimumChargeData.put(1, new Object[] {"Type ID","Type"});
            masterCustomerClassSetupCreditLimitData.put(1, new Object[] {"Type ID","Type"});
            masterCustomerClassSetupFinanceChargeData.put(1, new Object[] {"Type ID","Type"});
            masterDepreciationPeriodTypesData.put(1, new Object[] {"Type ID","Type"});
            masterDiscountTypeData.put(1, new Object[] {"Type ID","Type"});
            masterDiscountTypePeriodData.put(1, new Object[] {"Type ID","Type"});
            masterDueTypesData.put(1, new Object[] {"Type ID","Type"});
            masterFAAccountGroupAccountTypesData.put(1, new Object[] {"Type ID","Type"});
            masterGeneralMaintenanceAssetStatusData.put(1, new Object[] {"Type ID","Type"});
            masterGeneralMaintenanceAssetTypeData.put(1, new Object[] {"Type ID","Type"});
            masterHoldVendorStatusData.put(1, new Object[] {"Type ID","Type"});
            masternegativeSymbolSignTypesData.put(1, new Object[] {"Type ID","Type"});
            masterNegativeSymbolTypesData.put(1, new Object[] {"Type ID","Type"});
            masterRateFrequencyTypesData.put(1, new Object[] {"Type ID","Type"});
            masterRMAgeingTypesData.put(1, new Object[] {"Type ID","Type"});
            masterRMApplyByDefaultTypesData.put(1, new Object[] {"Type ID","Type"});
        	masterSeperatorDecimalData.put(1, new Object[] {"Type ID","Type"});
        	masterSeperatorThousandsData.put(1, new Object[] {"Type ID","Type"});
        	masterVendorStatusData.put(1, new Object[] {"Type ID","Type"});
        	pmSetupFormatTypeData.put(1, new Object[] {"Type ID","Type"});
        	seriesTypeData.put(1, new Object[] {"Type ID","Type"});
            shipmentMethodTypeData.put(1, new Object[] {"Type ID","Type"});
            tpclblncTypeData.put(1, new Object[] {"Type ID","Type"});
            vatBasedOnTypeData.put(1, new Object[] {"Type ID","Type"});
            vatTypeData.put(1, new Object[] {"Type ID","Type"});
         
            List<CountryMaster> countryList = repositoryCountryMaster.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<StateMaster> stateList = repositoryStateMaster.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<CityMaster> cityList = repositoryCityMaster.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<BtiMessage> btiMessageList = repositoryBtiMessage.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<CreditCardType> creditCardTypeList = repositoryCreditCardType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<CustomerMaintenanceActiveStatus> customerMaintenanceActiveStatusList = repositoryCustomerMaintenanceActiveStatus.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<CustomerMaintenanceHoldStatus> customerMaintenanceHoldStatusList = repositoryCustomerMaintenanceHoldStatus.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<FAAccountTableAccountType> faAccountTableAccountTypeList = repositoryFAAccountTableAccountType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<FABookMaintenanceAmortizationCodeType> faBookMaintenanceAmortizationCodeTypeList = repositoryFABookMaintenanceAmortizationCodeType.findByIsDeletedAndLanguageLanguageId(false,languageId);
         	List<FABookMaintenanceAveragingConventionType> faBookMaintenanceAveragingConventionTypeList = repositoryFABookMaintenanceAveragingConventionType.findByIsDeletedAndLanguageLanguageId(false,languageId);
         	List<FABookMaintenanceSwitchOverType> faBookMaintenanceSwitchOverTypeList = repositoryFABookMaintenanceSwitchOverType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<GLConfigurationAuditTrialSeriesType> glConfigurationAuditTrialSeriesTypeList = repositoryGLConfigurationAuditTrialSeriesType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<LeaseType> leaseTypeList = repositoryLeaseType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterAccountType> masterAccountTypeList = repositoryMasterAccountType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterArAccountType> masterArAccountTypeList = repositoryMasterArAccountType.findByIsDeletedAndLanguageLanguageId(false,languageId);
         	List<MasterBlncdspl> masterBlncdsplList = repositoryMasterBlncdspl.findByIsDeletedAndLanguageLanguageId(false,languageId);
          	List<MasterCustomerClassSetupBalanceType> masterCustomerClassSetupBalanceTypeList = repositoryMasterCustomerClassSetupBalanceType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterCustomerClassMinimumCharge> masterCustomerClassMinimumChargeList = repositoryMasterCustomerClassMinimumCharge.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterCustomerClassSetupCreditLimit> masterCustomerClassSetupCreditLimitList = repositoryMasterCustomerClassSetupCreditLimit.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterCustomerClassSetupFinanaceCharge> masterCustomerClassSetupFinanaceChargeList = repositoryMasterCustomerClassSetupFinanaceCharge.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterDepreciationPeriodTypes> masterDepreciationPeriodTypesList = repositoryMasterDerpreciationPeriodTypes.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterDiscountType> masterDiscountTypeList = repositoryMasterDiscountType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterDiscountTypePeriod> masterDiscountTypePeriodList = repositoryMasterDiscountTypePeriod.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterDueTypes> masterDueTypesList = repositoryMasterDueTypes.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterFAAccountGroupAccountType> masterFAAccountGroupAccountTypeList = repositoryMasterFAAccountGroupAccountType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterGeneralMaintenanceAssetStatus> masterGeneralMaintenanceAssetStatusList = repositoryMasterGeneralMaintenanceAssetStatus.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterGeneralMaintenanceAssetType> masterGeneralMaintenanceAssetTypeList = repositoryMasterGeneralMaintenanceAssetType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<HoldVendorStatus> holdVendorStatusList = repositoryHoldVendorStatus.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterNegativeSymbolSignTypes> masterNegativeSymbolSignTypesList = repositoryMasterNegativeSymbolSignTypes.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterNegativeSymbolTypes> masterNegativeSymbolTypesList = repositoryMasterNegativeSymbolTypes.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterRateFrequencyTypes> masterRateFrequencyTypesList = repositoryMasterRateFrequencyTypes.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterRMAgeingTypes> masterRMAgeingTypesList = repositoryMasterRMAgeingTypes.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterRMApplyByDefaultTypes> masterRMApplyByDefaultTypesList = repositoryMasterRMApplyByDefaultTypes.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterSeperatorDecimal> masterSeperatorDecimalList = repositoryMasterSeperatorDecimal.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterSeperatorThousands> masterSeperatorThousandsList = repositoryMasterSeperatorThousands.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<MasterVendorStatus>masterVendorStatusList = repositoryMasterVendorStatus.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<PMSetupFormatType> pmSetupFormatTypeList = repositoryPMSetupFormatType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<SeriesType> seriesTypeList = repositorySeriesType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<ShipmentMethodType> shipmentMethodTypeList = repositoryShipmentMethodType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<TPCLBalanceType> tpclBalanceTypeList = repositoryTPCLBalanceType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<VATBasedOnType> vatBasedOnTypeList = repositoryVATBasedOnType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<VATType> vatTypeList = repositoryVATType.findByIsDeletedAndLanguageLanguageId(false,languageId);
        
    	 if (btiMessageList!= null && !btiMessageList.isEmpty() ){
         	int sno = 2;
             for (BtiMessage btiMessage : btiMessageList) {
             String name = btiMessage.getMessageShort();
             String message = btiMessage.getMessage();
                      sno++;
                      btiMessageData.put(sno, new Object[] { name,message});
                }
         }
         
		if (countryList != null && !countryList.isEmpty()) {
			int sno = 2;
			for (CountryMaster country : countryList) {
				String countryName = country.getCountryName();
				String countryCode = country.getCountryCode();
				String shortName = country.getShortName();
				sno++;
				countryData.put(sno, new Object[] { countryCode, shortName, countryName});
			}
		}
		
		if (stateList != null && !stateList.isEmpty()) {
			int sno = 2;
			for (StateMaster state : stateList) {
				String stateName = state.getStateName();
				String stateCode = state.getStateCode();
				int contryId = state.getCountryMaster().getCountryId();

				sno++;
				stateData.put(sno, new Object[] { contryId, stateCode, stateName});
			}
		}
		
		if (cityList != null && !cityList.isEmpty()) {
			int sno = 2;
			for (CityMaster city : cityList) {
				String cityName = city.getCityName();
				String cityCode = city.getCityCode();
				int stateId = city.getStateMaster().getStateId();
				sno++;
				cityData.put(sno, new Object[] { stateId, cityCode, cityName});
			}
		}
         
		if (creditCardTypeList != null && !creditCardTypeList.isEmpty()) {
			int sno = 2;
			for (CreditCardType creditCardType : creditCardTypeList) {
				int typeId = creditCardType.getTypeId();
				String type = creditCardType.getTypePrimary();
				sno++;
				creditCardTypeData.put(sno, new Object[] { typeId, type});
			}
		}
        	
		if (customerMaintenanceActiveStatusList != null && !customerMaintenanceActiveStatusList.isEmpty()) {
			int sno = 2;
			for (CustomerMaintenanceActiveStatus customerMaintenanceActiveStatus : customerMaintenanceActiveStatusList) {
				int typeId = customerMaintenanceActiveStatus.getTypeId();
				String type = customerMaintenanceActiveStatus.getTypePrimary();
				sno++;
				customerMaintenanceActiveStatusData.put(sno, new Object[] { typeId, type});
			}
		}
        	 
		if (customerMaintenanceHoldStatusList != null && !customerMaintenanceHoldStatusList.isEmpty()) {
			int sno = 2;
			for (CustomerMaintenanceHoldStatus customerMaintenanceHoldStatus : customerMaintenanceHoldStatusList) {
				int typeId = customerMaintenanceHoldStatus.getTypeId();
				String type = customerMaintenanceHoldStatus.getTypePrimary();
				sno++;
				customerMaintenanceHoldStatusData.put(sno, new Object[] { typeId, type});
			}
		}
        	 
		if (faAccountTableAccountTypeList != null && !faAccountTableAccountTypeList.isEmpty()) {
			int sno = 2;
			for (FAAccountTableAccountType faAccountTableAccountType : faAccountTableAccountTypeList) {
				int typeId = faAccountTableAccountType.getTypeId();
				String type = faAccountTableAccountType.getTypePrimary();
				sno++;
				faAccountTableAccountTypeData.put(sno, new Object[] { typeId, type});
			}
		}
        	 
		if (faBookMaintenanceAmortizationCodeTypeList != null && !faBookMaintenanceAmortizationCodeTypeList.isEmpty()) {
			int sno = 2;
			for (FABookMaintenanceAmortizationCodeType faBookMaintenanceAmortizationCodeType : faBookMaintenanceAmortizationCodeTypeList) {
				int typeId = faBookMaintenanceAmortizationCodeType.getTypeId();
				String type = faBookMaintenanceAmortizationCodeType.getTypePrimary();
				sno++;
				faBookMaintenanceAmortizationCodeTypeData.put(sno, new Object[] { typeId, type});
			}
		}
        		 
		if (faBookMaintenanceAveragingConventionTypeList != null
				&& !faBookMaintenanceAveragingConventionTypeList.isEmpty()) {
			int sno = 2;
			for (FABookMaintenanceAveragingConventionType faBookMaintenanceAveragingConventionType : faBookMaintenanceAveragingConventionTypeList) {
				int typeId = faBookMaintenanceAveragingConventionType.getTypeId();
				String type = faBookMaintenanceAveragingConventionType.getTypePrimary();
				sno++;
				faBookMaintenanceAveragingConventionTypeData.put(sno, new Object[] { typeId, type});
			}
		}
		if (faBookMaintenanceSwitchOverTypeList != null && !faBookMaintenanceSwitchOverTypeList.isEmpty()) {
			int sno = 2;
			for (FABookMaintenanceSwitchOverType faBookMaintenanceSwitchOverType : faBookMaintenanceSwitchOverTypeList) {
				int typeId = faBookMaintenanceSwitchOverType.getTypeId();
				String type = faBookMaintenanceSwitchOverType.getTypePrimary();
				sno++;
				faBookMaintenanceSwitchoverTypeData.put(sno, new Object[] { typeId, type});
			}
		}
        	 
		if (glConfigurationAuditTrialSeriesTypeList != null && !glConfigurationAuditTrialSeriesTypeList.isEmpty()) {
			int sno = 2;
			for (GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType : glConfigurationAuditTrialSeriesTypeList) {
				int typeId = glConfigurationAuditTrialSeriesType.getTypeId();
				String type = glConfigurationAuditTrialSeriesType.getTypePrimary();
				sno++;
				glConfigurationAuditTrialSeriesTypeData.put(sno, new Object[] { typeId, type});
			}
		}
        	
        	if (leaseTypeList != null && !leaseTypeList.isEmpty()) {
    			int sno = 2;
    			for (LeaseType leaseType : leaseTypeList) {
    				int typeId = leaseType.getLeaseTypeId();
    				String type = leaseType.getValue();
    				sno++;
    				leaseTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	 
        	if (masterAccountTypeList != null && !masterAccountTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterAccountType masterAccountType : masterAccountTypeList) {
    				int typeId = masterAccountType.getTypeId();
    				String type = masterAccountType.getAccountType();
    				sno++;
    				masterAccountTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	 
        	if (masterArAccountTypeList != null && !masterArAccountTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterArAccountType masterArAccountType : masterArAccountTypeList) {
    				int typeId = masterArAccountType.getTypeId();
    				String type = masterArAccountType.getAccountType();
    				sno++;
    				masterARAccountTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	 
        	if (masterBlncdsplList != null && !masterBlncdsplList.isEmpty()) {
    			int sno = 2;
    			for (MasterBlncdspl masterBlncdspl : masterBlncdsplList) {
    				int typeId = masterBlncdspl.getTypeId();
    				String type = masterBlncdspl.getTypePrimary();
    				sno++;
    				masterBlncdsplData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	 
        	if (masterCustomerClassSetupBalanceTypeList != null && !masterCustomerClassSetupBalanceTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterCustomerClassSetupBalanceType masterCustomerClassSetupBalanceType : masterCustomerClassSetupBalanceTypeList) {
    				int typeId = masterCustomerClassSetupBalanceType.getTypeId();
    				String type = masterCustomerClassSetupBalanceType.getTypePrimary();
    				sno++;
    				masterCustomerClassBalanceTypeData.put(sno, new Object[] { typeId, type });
    			}
    		}
        	
        	
        	 
        	if (masterCustomerClassMinimumChargeList != null && !masterCustomerClassMinimumChargeList.isEmpty()) {
    			int sno = 2;
    			for (MasterCustomerClassMinimumCharge masterCustomerClassMinimumCharge : masterCustomerClassMinimumChargeList) {
    				int typeId = masterCustomerClassMinimumCharge.getTypeId();
    				String type = masterCustomerClassMinimumCharge.getTypePrimary();
    				sno++;
    				masterCustomerClassMinimumChargeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	 
        	if (masterCustomerClassSetupCreditLimitList != null && !masterCustomerClassSetupCreditLimitList.isEmpty()) {
    			int sno = 2;
    			for (MasterCustomerClassSetupCreditLimit masterCustomerClassSetupCreditLimit : masterCustomerClassSetupCreditLimitList) {
    				int typeId = masterCustomerClassSetupCreditLimit.getTypeId();
    				String type = masterCustomerClassSetupCreditLimit.getTypePrimary();
    				sno++;
    				masterCustomerClassSetupCreditLimitData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	 
        	if (masterCustomerClassSetupFinanaceChargeList != null && !masterCustomerClassSetupFinanaceChargeList.isEmpty()) {
    			int sno = 2;
    			for (MasterCustomerClassSetupFinanaceCharge masterCustomerClassSetupFinanaceCharge : masterCustomerClassSetupFinanaceChargeList) {
    				int typeId = masterCustomerClassSetupFinanaceCharge.getTypeId();
    				String type = masterCustomerClassSetupFinanaceCharge.getTypePrimary();
    				sno++;
    				masterCustomerClassSetupFinanceChargeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterDepreciationPeriodTypesList != null && !masterDepreciationPeriodTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterDepreciationPeriodTypes masterDepreciationPeriodTypes : masterDepreciationPeriodTypesList) {
    				int typeId = masterDepreciationPeriodTypes.getTypeId();
    				String type = masterDepreciationPeriodTypes.getDepreciationPeriodType();
    				sno++;
    				masterDepreciationPeriodTypesData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterDiscountTypeList != null && !masterDiscountTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterDiscountType masterDiscountType : masterDiscountTypeList) {
    				int typeId = masterDiscountType.getTypeId();
    				String type = masterDiscountType.getDiscountType();
    				sno++;
    				masterDiscountTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterDiscountTypePeriodList != null && !masterDiscountTypePeriodList.isEmpty()) {
    			int sno = 2;
    			for (MasterDiscountTypePeriod masterDiscountTypePeriod : masterDiscountTypePeriodList) {
    				int typeId = masterDiscountTypePeriod.getTypeId();
    				String type = masterDiscountTypePeriod.getDiscountTypePriod();
    				sno++;
    				masterDiscountTypePeriodData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterDueTypesList != null && !masterDueTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterDueTypes masterDueTypes : masterDueTypesList) {
    				int typeId = masterDueTypes.getTypeId();
    				String type = masterDueTypes.getDueType();
    				sno++;
    				masterDueTypesData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterFAAccountGroupAccountTypeList != null && !masterFAAccountGroupAccountTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterFAAccountGroupAccountType masterFAAccountGroupAccountType : masterFAAccountGroupAccountTypeList) {
    				int typeId = masterFAAccountGroupAccountType.getTypeId();
    				String type = masterFAAccountGroupAccountType.getAccountType();
    				sno++;
    				masterFAAccountGroupAccountTypesData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterGeneralMaintenanceAssetStatusList != null && !masterGeneralMaintenanceAssetStatusList.isEmpty()) {
    			int sno = 2;
    			for (MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus : masterGeneralMaintenanceAssetStatusList) {
    				int typeId = masterGeneralMaintenanceAssetStatus.getTypeId();
    				String type = masterGeneralMaintenanceAssetStatus.getTypePrimary();
    				sno++;
    				masterGeneralMaintenanceAssetStatusData.put(sno, new Object[] { typeId, type });
    			}
    		}
        	 
        	if (masterGeneralMaintenanceAssetTypeList != null && !masterGeneralMaintenanceAssetTypeList.isEmpty()) {
    			int sno = 2;
    			for (MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType : masterGeneralMaintenanceAssetTypeList) {
    				int typeId = masterGeneralMaintenanceAssetType.getTypeId();
    				String type = masterGeneralMaintenanceAssetType.getTypePrimary();
    				sno++;
    				masterGeneralMaintenanceAssetTypeData.put(sno, new Object[] { typeId, type });
    			}
    		}
        	 
        	if (holdVendorStatusList != null && !holdVendorStatusList.isEmpty()) {
    			int sno = 2;
    			for (HoldVendorStatus holdVendorStatus : holdVendorStatusList) {
    				int typeId = holdVendorStatus.getHoldStatus();
    				String type = holdVendorStatus.getHoldStatusPrimary();
    				sno++;
    				masterHoldVendorStatusData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	if (masterNegativeSymbolSignTypesList != null && !masterNegativeSymbolSignTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes : masterNegativeSymbolSignTypesList) {
    				int typeId = masterNegativeSymbolSignTypes.getTypeId();
    				String type = masterNegativeSymbolSignTypes.getNegativeSymbolSignType();
    				sno++;
    				masternegativeSymbolSignTypesData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterNegativeSymbolTypesList != null && !masterNegativeSymbolTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterNegativeSymbolTypes masterNegativeSymbolTypes : masterNegativeSymbolTypesList) {
    				int typeId = masterNegativeSymbolTypes.getTypeId();
    				String type = masterNegativeSymbolTypes.getNegativeSymbolType();
    				sno++;
    				masterNegativeSymbolTypesData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	if (masterRateFrequencyTypesList != null && !masterRateFrequencyTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterRateFrequencyTypes masterRateFrequencyTypes : masterRateFrequencyTypesList) {
    				int typeId = masterRateFrequencyTypes.getTypeId();
    				String type = masterRateFrequencyTypes.getRateFrequencyType();
    				sno++;
    				masterRateFrequencyTypesData.put(sno, new Object[] { typeId, type });
    			}
    		}
        	 
        	if (masterRMAgeingTypesList != null && !masterRMAgeingTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterRMAgeingTypes masterRMAgeingTypes : masterRMAgeingTypesList) {
    				int typeId = masterRMAgeingTypes.getTypeId();
    				String type = masterRMAgeingTypes.getTypePrimary();
    				sno++;
    				masterRMAgeingTypesData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterRMApplyByDefaultTypesList != null && !masterRMApplyByDefaultTypesList.isEmpty()) {
    			int sno = 2;
    			for (MasterRMApplyByDefaultTypes masterRMApplyByDefaultTypes : masterRMApplyByDefaultTypesList) {
    				int typeId = masterRMApplyByDefaultTypes.getTypeId();
    				String type = masterRMApplyByDefaultTypes.getTypePrimary();
    				sno++;
    				masterRMApplyByDefaultTypesData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterSeperatorDecimalList != null && !masterSeperatorDecimalList.isEmpty()) {
    			int sno = 2;
    			for (MasterSeperatorDecimal masterSeperatorDecimal : masterSeperatorDecimalList) {
    				int typeId = masterSeperatorDecimal.getSeperatorDecimalId();
    				String type = masterSeperatorDecimal.getSeperatorDecimalPrimary();
    				sno++;
    				masterSeperatorDecimalData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (masterSeperatorThousandsList != null && !masterSeperatorThousandsList.isEmpty()) {
    			int sno = 2;
    			for (MasterSeperatorThousands masterSeperatorThousands : masterSeperatorThousandsList) {
    				int typeId = masterSeperatorThousands.getSeperatorThousandId();
    				String type = masterSeperatorThousands.getSeperatorThousandPrimary();
    				sno++;
    				masterSeperatorThousandsData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	if (masterVendorStatusList != null && !masterVendorStatusList.isEmpty()) {
    			int sno = 2;
    			for (MasterVendorStatus masterVendorStatus : masterVendorStatusList) {
    				int typeId = masterVendorStatus.getStatusId();
    				String type = masterVendorStatus.getStatusPrimary();
    				sno++;
    				masterVendorStatusData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (pmSetupFormatTypeList != null && !pmSetupFormatTypeList.isEmpty()) {
    			int sno = 2;
    			for (PMSetupFormatType pmSetupFormatType : pmSetupFormatTypeList) {
    				int typeId = pmSetupFormatType.getTypeId();
    				String type = pmSetupFormatType.getTypePrimary();
    				sno++;
    				pmSetupFormatTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	if (seriesTypeList != null && !seriesTypeList.isEmpty()) {
    			int sno = 2;
    			for (SeriesType seriesType : seriesTypeList) {
    				int typeId = seriesType.getTypeId();
    				String type = seriesType.getTypePrimary();
    				sno++;
    				seriesTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	if (shipmentMethodTypeList != null && !shipmentMethodTypeList.isEmpty()) {
    			int sno = 2;
    			for (ShipmentMethodType shipmentMethodType : shipmentMethodTypeList) {
    				int typeId = shipmentMethodType.getTypeId();
    				String type = shipmentMethodType.getTypePrimary();
    				sno++;
    				shipmentMethodTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	 
        	if (tpclBalanceTypeList != null && !tpclBalanceTypeList.isEmpty()) {
    			int sno = 2;
    			for (TPCLBalanceType tpclBalanceType : tpclBalanceTypeList) {
    				int typeId = tpclBalanceType.getTypeId();
    				String type = tpclBalanceType.getTypePrimary();
    				sno++;
    				tpclblncTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
        	if (vatBasedOnTypeList != null && !vatBasedOnTypeList.isEmpty()) {
    			int sno = 2;
    			for (VATBasedOnType vatBasedOnType : vatBasedOnTypeList) {
    				int typeId = vatBasedOnType.getTypeId();
    				String type = vatBasedOnType.getTypePrimary();
    				sno++;
    				vatBasedOnTypeData.put(sno, new Object[] { typeId, type});
    			}
    		}
        	
			if (vatTypeList != null && !vatTypeList.isEmpty()) {
				int sno = 2;
				for (VATType vatType : vatTypeList) {
					int typeId = vatType.getTypeId();
					String type = vatType.getTypePrimary();
					sno++;
					vatTypeData.put(sno, new Object[] { typeId, type});
				}
			}
        	 
        	
            // Iterate over data and write to sheet
		Set<Integer> btiMessageKeyset = btiMessageData.keySet();
		Set<Integer> countryKeyset = countryData.keySet();
		Set<Integer> stateKeyset = stateData.keySet();
		Set<Integer> cityKeyset = cityData.keySet();
		Set<Integer> creditCardTypeKeyset = creditCardTypeData.keySet();
		Set<Integer> customerMaintenanceActiveStatusKeyset = customerMaintenanceActiveStatusData.keySet();
		Set<Integer> customerMaintenanceHoldStatusKeyset = customerMaintenanceHoldStatusData.keySet();
		Set<Integer> faAccountTableAccountTypeKeyset = faAccountTableAccountTypeData.keySet();
		Set<Integer> faBookMaintenanceAmortizationCodeTypeKeyset = faBookMaintenanceAmortizationCodeTypeData
				.keySet();
		Set<Integer> faBookMaintenanceAveragingConventionTypeKeyset = faBookMaintenanceAveragingConventionTypeData
				.keySet();
		Set<Integer> faBookMaintenanceSwitchoverTypeKeyset = faBookMaintenanceSwitchoverTypeData.keySet();
		Set<Integer> glConfigurationAuditTrialSeriesTypeKeyset = glConfigurationAuditTrialSeriesTypeData
				.keySet();

		Set<Integer> leaseTypeKeyset = leaseTypeData.keySet();
		Set<Integer> masterAccountTypeKeyset = masterAccountTypeData.keySet();
		Set<Integer> masterARAccountTypeKeyset = masterARAccountTypeData.keySet();
		Set<Integer> masterBlncdsplKeyset = masterBlncdsplData.keySet();
		Set<Integer> masterCustomerClassBalanceTypeKeyset = masterCustomerClassBalanceTypeData.keySet();
		Set<Integer> masterCustomerClassMinimumChargeKeyset = masterCustomerClassMinimumChargeData.keySet();
		Set<Integer> masterCustomerClassSetupCreditLimitKeyset = masterCustomerClassSetupCreditLimitData
				.keySet();
		Set<Integer> masterCustomerClassSetupFinanceChargeKeyset = masterCustomerClassSetupFinanceChargeData
				.keySet();
		Set<Integer> masterDepreciationPeriodTypesKeyset = masterDepreciationPeriodTypesData.keySet();
		Set<Integer> masterDiscountTypeKeyset = masterDiscountTypeData.keySet();
		Set<Integer> masterDiscountTypePeriodKeyset = masterDiscountTypePeriodData.keySet();
		Set<Integer> masterDueTypesKeyset = masterDueTypesData.keySet();
		Set<Integer> masterFAAccountGroupAccountTypesKeyset = masterFAAccountGroupAccountTypesData.keySet();
		Set<Integer> masterGeneralMaintenanceAssetStatusKeyset = masterGeneralMaintenanceAssetStatusData
				.keySet();
		Set<Integer> masterGeneralMaintenanceAssetTypeKeyset = masterGeneralMaintenanceAssetTypeData.keySet();
		Set<Integer> masterHoldVendorStatusKeyset = masterHoldVendorStatusData.keySet();
		Set<Integer> masterNegativeSymbolSignTypesKeyset = masternegativeSymbolSignTypesData.keySet();
		Set<Integer> masterNegativeSymbolTypesKeyset = masterNegativeSymbolTypesData.keySet();
		Set<Integer> masterRateFrequencyTypesKeyset = masterRateFrequencyTypesData.keySet();
		Set<Integer> masterRMAgeingTypesKeyset = masterRMAgeingTypesData.keySet();
		Set<Integer> masterRMApplyByDefaultTypesKeyset = masterRMApplyByDefaultTypesData.keySet();

		Set<Integer> masterSeperatorDecimalKeyset = masterSeperatorDecimalData.keySet();
		Set<Integer> masterSeperatorThousandsKeyset = masterSeperatorThousandsData.keySet();
		Set<Integer> masterVendorStatusKeyset = masterVendorStatusData.keySet();
		Set<Integer> pmSetupFormatTypeKeyset = pmSetupFormatTypeData.keySet();
		Set<Integer> seriesTypeKeyset = seriesTypeData.keySet();
		Set<Integer> shipmentMethodTypeKeyset = shipmentMethodTypeData.keySet();
		Set<Integer> tpclblncTypeKeyset = tpclblncTypeData.keySet();
		Set<Integer> vatBasedOnTypeKeyset = vatBasedOnTypeData.keySet();
		Set<Integer> vatTypeKeyset = vatTypeData.keySet();

       	 int rownum = 0;
		for (Integer key : creditCardTypeKeyset) {
			Row row = creditCardTypeSheet.createRow(rownum++);
			CellStyle style;
			if (rownum <= 1) {
				style = workbook.createCellStyle();// Create style
				XSSFFont font = workbook.createFont();// Create font
				font.setFontName("sans-serif");// set font type
				font.setBold(true);
				style.setFont(font);// set it to bold
				style.setAlignment(CellStyle.ALIGN_CENTER);
			} else {
				style = workbook.createCellStyle();
				XSSFFont font = workbook.createFont();
				font.setFontName("sans-serif");
				style.setFont(font);
			}
			Object[] objArr = creditCardTypeData.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				cell.setCellStyle(style);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}
       	rownum = 0;
        for (Integer key : customerMaintenanceActiveStatusKeyset) {
               Row row = customerMaintenanceActiveStatusSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = customerMaintenanceActiveStatusData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	rownum = 0;
        for (Integer key : customerMaintenanceHoldStatusKeyset) {
               Row row = customerMaintenanceHoldStatusSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = customerMaintenanceHoldStatusData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
       	rownum = 0;
        for (Integer key : faAccountTableAccountTypeKeyset) {
               Row row = faAccountTableAccountTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = faAccountTableAccountTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
     	rownum = 0;
        for (Integer key : faBookMaintenanceAmortizationCodeTypeKeyset) {
               Row row = faBookMaintenanceAmortizationCodeTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = faBookMaintenanceAmortizationCodeTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	rownum = 0;
        for (Integer key : faBookMaintenanceAveragingConventionTypeKeyset) {
               Row row = faBookMaintenanceAveragingConventionTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = faBookMaintenanceAveragingConventionTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
     	rownum = 0;
        for (Integer key : faBookMaintenanceSwitchoverTypeKeyset) {
               Row row = faBookMaintenanceSwitchoverTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = faBookMaintenanceSwitchoverTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	 
       	rownum = 0;
        for (Integer key : glConfigurationAuditTrialSeriesTypeKeyset) {
               Row row = glConfigurationAuditTrialSeriesTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = glConfigurationAuditTrialSeriesTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	 
        	rownum = 0;
         for (Integer key : leaseTypeKeyset) {
                Row row = leaseTypeSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = leaseTypeData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
         
         
       	rownum = 0;
        for (Integer key : masterAccountTypeKeyset) {
               Row row = masterAccountTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = masterAccountTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
      	rownum = 0;
        for (Integer key : masterARAccountTypeKeyset) {
               Row row = masterARAccountTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = masterARAccountTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
        
       	rownum = 0;
        for (Integer key : masterBlncdsplKeyset) {
               Row row = masterBlncdsplSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = masterBlncdsplData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
        
       	rownum = 0;
        for (Integer key : masterCustomerClassBalanceTypeKeyset) {
               Row row = masterCustomerClassBalanceTypeSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = masterCustomerClassBalanceTypeData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
         rownum = 0;
         for (Integer key : masterCustomerClassMinimumChargeKeyset) {
                Row row = masterCustomerClassMinimumChargeSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = masterCustomerClassMinimumChargeData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
         
         rownum = 0;
         for (Integer key : masterCustomerClassSetupCreditLimitKeyset) {
                Row row = masterCustomerClassSetupCreditLimitSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = masterCustomerClassSetupCreditLimitData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
       	
            rownum = 0;
            for (Integer key : masterCustomerClassSetupFinanceChargeKeyset) {
                   Row row = masterCustomerClassSetupFinanceChargeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterCustomerClassSetupFinanceChargeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterDepreciationPeriodTypesKeyset) {
                   Row row = masterDepreciationPeriodTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterDepreciationPeriodTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterDiscountTypeKeyset) {
                   Row row = masterDiscountTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterDiscountTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            
            rownum = 0;
            for (Integer key : masterDiscountTypePeriodKeyset) {
                   Row row = masterDiscountTypePeriodSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterDiscountTypePeriodData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterDueTypesKeyset) {
                   Row row = masterDueTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterDueTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterFAAccountGroupAccountTypesKeyset) {
                   Row row = masterFAAccountGroupAccountTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterFAAccountGroupAccountTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterGeneralMaintenanceAssetStatusKeyset) {
                   Row row = masterGeneralMaintenanceAssetStatusSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterGeneralMaintenanceAssetStatusData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterGeneralMaintenanceAssetTypeKeyset) {
                   Row row = masterGeneralMaintenanceAssetTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterGeneralMaintenanceAssetTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterHoldVendorStatusKeyset) {
                   Row row = masterHoldVendorStatusSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterHoldVendorStatusData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterNegativeSymbolSignTypesKeyset) {
                   Row row = masterNegativeSymbolSignTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masternegativeSymbolSignTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterNegativeSymbolTypesKeyset) {
                   Row row = masterNegativeSymbolTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterNegativeSymbolTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterRateFrequencyTypesKeyset) {
                   Row row = masterRateFrequencyTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterRateFrequencyTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterRMAgeingTypesKeyset) {
                   Row row = masterRMAgeingTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterRMAgeingTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : masterRMApplyByDefaultTypesKeyset) {
                   Row row = masterRMApplyBYDefaultTypesSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterRMApplyByDefaultTypesData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
        	    
            rownum = 0;
            for (Integer key : masterSeperatorDecimalKeyset) {
                   Row row = masterSeperatorDecimalSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterSeperatorDecimalData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterSeperatorThousandsKeyset) {
                   Row row = masterSeperatorThousandsSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterSeperatorThousandsData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : masterVendorStatusKeyset) {
                   Row row = masterVendorStatusSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = masterVendorStatusData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : pmSetupFormatTypeKeyset) {
                   Row row = pmSetupFormatTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = pmSetupFormatTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : seriesTypeKeyset) {
                   Row row = seriesTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = seriesTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : shipmentMethodTypeKeyset) {
                   Row row = shipmentMethodTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = shipmentMethodTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : tpclblncTypeKeyset) {
                   Row row = tpclblncTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = tpclblncTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : vatBasedOnTypeKeyset) {
                   Row row = vatBasedOnTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = vatBasedOnTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            rownum = 0;
            for (Integer key : vatTypeKeyset) {
                   Row row = vatTypeSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = vatTypeData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
             
            rownum = 0;
            for (Integer key : btiMessageKeyset) {
                   Row row = btiMessageMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = btiMessageData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
             rownum = 0;
            for (Integer key : countryKeyset) {
                   Row row = countryMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = countryData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : stateKeyset) {
                   Row row = stateMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = stateData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : cityKeyset) {
                   Row row = cityMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = cityData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            try {
                   // Write the workbook in file system
                   String excelFileName = "CompanyDB_MasterData.xlsx";
                   String fileName = URLEncoder.encode(excelFileName, "UTF-8");
                   fileName = URLDecoder.decode(fileName, "ISO8859_1");
                   response.setHeader("Content-disposition", "attachment; filename=" + fileName);
                   response.setContentType(mimetypesFileTypeMap.getContentType(excelFileName));

                   FileOutputStream out = new FileOutputStream(new File(temperotyFilePath + File.pathSeparator+ excelFileName));
                   workbook.write(out);
                   out.close();

                   ByteArrayOutputStream baos=null;
                   baos = convertexcelToByteArrayOutputStream(temperotyFilePath + File.pathSeparator + excelFileName);
                   OutputStream os = response.getOutputStream();
                   baos.writeTo(os);
                   os.flush();

            } catch (Exception e) {
            	LOGGER.error(e.getMessage());
            }
	}

	public boolean importMasterDataForUpdateLanguage(String languageName, String languageOrientation, MultipartFile file,
			Integer languageId) throws IOException {


		int loggedInUserId = 0;

		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		
		Language language = repositoryLanguage.findByLanguageIdAndIsDeleted(languageId, false);
		if (language == null) {
			return false;
		}
		
		language.setIsActive(true);
		language.setIsDeleted(false);
		language.setLanguageName(languageName);
		language.setCreatedBy(loggedInUserId);
		language.setUpdatedBy(loggedInUserId);
		language.setLanguageOrientation(languageOrientation);
		language = repositoryLanguage.saveAndFlush(language);
		if(file==null || file.getBytes().length<=0){
			return true;
		}
		Workbook workbook;
		try {
			
			  /*final ServletContext servletContext = httpServletRequest.getSession().getServletContext();      
              final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
             final String temperotyFilePath = tempDirectory.getAbsolutePath();
             */
			    /*String temPath= System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + 
   		        file.getOriginalFilename();
			    System.out.println(temPath);
                File tmpFile = new File(temPath);
                file.transferTo(tmpFile);
                
                File readFile = new File(temPath);
                InputStream inputStream=new FileInputStream(readFile);*/
			    InputStream inputStream = file.getInputStream();
			    workbook = new XSSFWorkbook(inputStream);

				Sheet masterGeneralMaintenanceAssetTypeSheet = workbook.getSheetAt(0);
				Sheet masterGeneralMaintenanceAssetStatusSheet = workbook.getSheetAt(1);
				Sheet btiMessageMasterSheet = workbook.getSheetAt(2);
				Sheet countryMasterSheet = workbook.getSheetAt(3);
				Sheet stateMasterSheet = workbook.getSheetAt(4);
				Sheet cityMasterSheet = workbook.getSheetAt(5);
				Sheet creditCardTypeSheet = workbook.getSheetAt(6);
				Sheet customerMaintenanceActiveStatusSheet = workbook.getSheetAt(7);
				Sheet customerMaintenanceHoldStatusSheet = workbook.getSheetAt(8);
				Sheet faAccountTableAccountTypeSheet = workbook.getSheetAt(9);
				Sheet faBookMaintenanceAmortizationCodeTypeSheet = workbook.getSheetAt(10);
				Sheet faBookMaintenanceAveragingConventionTypeSheet = workbook.getSheetAt(11);
				Sheet faBookMaintenanceSwitchOverTypeSheet = workbook.getSheetAt(12);
				Sheet glConfigurationAuditTrialSeriesTypeSheet = workbook.getSheetAt(13);
				Sheet leaseTypeSheet = workbook.getSheetAt(14);
				Sheet masterAccountTypeSheet = workbook.getSheetAt(15);
				Sheet masterArAccountTypeSheet = workbook.getSheetAt(16);
				Sheet masterBlncdsplSheet = workbook.getSheetAt(17);
				Sheet masterCustomerClassSetupBalanceTypeSheet = workbook.getSheetAt(18);
				Sheet masterCustomerClassMinimumChargeSheet = workbook.getSheetAt(19);
				Sheet masterCustomerClassSetupCreditLimitSheet = workbook.getSheetAt(20);
				Sheet masterCustomerClassSetupFinanaceChargeSheet = workbook.getSheetAt(21);
				Sheet masterDepreciationPeriodTypesSheet = workbook.getSheetAt(22);
				Sheet masterDiscountTypeSheet = workbook.getSheetAt(23);
				Sheet masterDiscountTypePeriodSheet = workbook.getSheetAt(24);
				Sheet masterDueTypesSheet = workbook.getSheetAt(25);
				Sheet masterFAAccountGroupAccountTypeSheet = workbook.getSheetAt(26);
				Sheet holdVendorStatusSheet = workbook.getSheetAt(27);
				Sheet masterNegativeSymbolSignTypesSheet = workbook.getSheetAt(28);
				Sheet masterNegativeSymbolTypesSheet = workbook.getSheetAt(29);
				Sheet masterRateFrequencyTypesSheet = workbook.getSheetAt(30);
				Sheet masterRMAgeingTypesSheet = workbook.getSheetAt(31);
				Sheet masterRMApplyByDefaultTypesSheet = workbook.getSheetAt(32);
				Sheet masterSeperatorDecimalSheet = workbook.getSheetAt(33);
				Sheet masterSeperatorThousandsSheet = workbook.getSheetAt(34);
				Sheet masterVendorStatusSheet = workbook.getSheetAt(35);
				Sheet pmSetupFormatTypeSheet = workbook.getSheetAt(36);
				Sheet seriesTypeSheet = workbook.getSheetAt(37);
				Sheet shipmentMethodTypeSheet = workbook.getSheetAt(38);
				Sheet tpclBalanceTypeSheet = workbook.getSheetAt(39);
				Sheet vatBasedOnTypeSheet = workbook.getSheetAt(40);
				Sheet vatTypeSheet = workbook.getSheetAt(41);
				
				List<MasterGeneralMaintenanceAssetType> masterGeneralMaintenanceAssetTypeList = new ArrayList<>();
				for (int i = 1; i <= masterGeneralMaintenanceAssetTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterGeneralMaintenanceAssetTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType =repositoryMasterGeneralMaintenanceAssetType.
							findByTypeIdAndLanguageLanguageIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterGeneralMaintenanceAssetType==null){
						masterGeneralMaintenanceAssetType= new MasterGeneralMaintenanceAssetType();
					}
					masterGeneralMaintenanceAssetType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterGeneralMaintenanceAssetType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterGeneralMaintenanceAssetType.setLanguage(language);
					masterGeneralMaintenanceAssetTypeList.add(masterGeneralMaintenanceAssetType);
				}
				
				repositoryMasterGeneralMaintenanceAssetType.save(masterGeneralMaintenanceAssetTypeList);
				List<MasterGeneralMaintenanceAssetStatus> masterGeneralMaintenanceAssetStatusList = new ArrayList<>();
				for (int i = 1; i <= masterGeneralMaintenanceAssetStatusSheet.getLastRowNum(); i++) {
					Row nextRow = masterGeneralMaintenanceAssetStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus = repositoryMasterGeneralMaintenanceAssetStatus.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterGeneralMaintenanceAssetStatus==null){
						masterGeneralMaintenanceAssetStatus= new MasterGeneralMaintenanceAssetStatus();
					}
					masterGeneralMaintenanceAssetStatus.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterGeneralMaintenanceAssetStatus.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterGeneralMaintenanceAssetStatus.setLanguage(language);
					masterGeneralMaintenanceAssetStatusList.add(masterGeneralMaintenanceAssetStatus);
				}
				repositoryMasterGeneralMaintenanceAssetStatus.save(masterGeneralMaintenanceAssetStatusList);
				
				List<BtiMessage> btiMessageList = new ArrayList<>();
				for (int i = 1; i <= btiMessageMasterSheet.getLastRowNum(); i++) {
					Row nextRow = btiMessageMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
                    String messageShort="";
					if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
						messageShort=nextRow.getCell(0).getStringCellValue();
					} else if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						messageShort=String.valueOf((int) nextRow.getCell(0).getNumericCellValue());
					}
					
					BtiMessage btiMessage = repositoryBtiMessage.findByMessageShortAndIsDeletedAndLanguageLanguageId(messageShort,false, language.getLanguageId());
                    if(btiMessage==null){
                    	btiMessage= new BtiMessage();
                    }
					btiMessage.setMessageShort(messageShort);
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						btiMessage.setMessage(nextRow.getCell(1).getStringCellValue());
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						btiMessage.setMessage(String.valueOf((int) nextRow.getCell(1).getNumericCellValue()));
					}
					btiMessage.setIsDeleted(false);
					btiMessage.setLanguage(language);
					btiMessage.setUpdatedBy(loggedInUserId);
					btiMessage.setCreatedBy(loggedInUserId);
					btiMessageList.add(btiMessage);
				}
				repositoryBtiMessage.save(btiMessageList);
				List<CityMaster> cityMasterList = new ArrayList<>();
				for (int i = 1; i <= cityMasterSheet.getLastRowNum(); i++) 
				{
					Row nextRow = cityMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					String cityCode="";
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						cityCode=nextRow.getCell(1).getStringCellValue();
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						cityCode=String.valueOf((int) nextRow.getCell(1).getNumericCellValue());
					}
					
					CityMaster cityMaster = repositoryCityMaster.findByCityCodeAndIsDeletedAndLanguageLanguageId(cityCode, false,language.getLanguageId());
					if(cityMaster==null){
						cityMaster= new CityMaster();
					}
					cityMaster.setStateMaster(repositoryStateMaster
							.findByStateIdAndIsDeleted((int) (nextRow.getCell(0).getNumericCellValue()), false));
					cityMaster.setCityCode(cityCode);
					if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
						cityMaster.setCityName(nextRow.getCell(2).getStringCellValue());
					} else if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						cityMaster.setCityName(String.valueOf((int) nextRow.getCell(2).getNumericCellValue()));
					}

					cityMaster.setIsDeleted(false);
					cityMaster.setLanguage(language);
					cityMaster.setUpdatedBy(loggedInUserId);
					cityMaster.setCreatedBy(loggedInUserId);
					cityMasterList.add(cityMaster);
				}
				
				repositoryCityMaster.save(cityMasterList);
				List<StateMaster> stateMasterList = new ArrayList<>();
				for (int i = 1; i <= stateMasterSheet.getLastRowNum(); i++) {
					Row nextRow = stateMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					String stateCode="";
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						stateCode=nextRow.getCell(1).getStringCellValue();
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						stateCode=String.valueOf((int) nextRow.getCell(1).getNumericCellValue());
					}
					
					StateMaster stateMaster = repositoryStateMaster.findByStateCodeAndLanguageLanguageIdAndIsDeleted(stateCode, language.getLanguageId(), false);
					if(stateMaster==null){
						stateMaster= new StateMaster();
					}
					
					CountryMaster countryMaster = repositoryCountryMaster
									.findByCountryIdAndIsDeleted((int) (nextRow.getCell(0).getNumericCellValue()), false);
					stateMaster.setCountryMaster(countryMaster);
					stateMaster.setStateCode(stateCode);
					if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
						stateMaster.setStateName(nextRow.getCell(2).getStringCellValue());
					} else if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						stateMaster.setStateName(String.valueOf((int) nextRow.getCell(2).getNumericCellValue()));
					}

					stateMaster.setIsDeleted(false);
					stateMaster.setLanguage(language);
					stateMaster.setUpdatedBy(loggedInUserId);
					stateMaster.setCreatedBy(loggedInUserId);
					stateMasterList.add(stateMaster);
				}
				repositoryStateMaster.save(stateMasterList);
				List<CountryMaster> countryMasterList = new ArrayList<>();
				for (int i = 1; i <= countryMasterSheet.getLastRowNum(); i++) {
					Row nextRow = countryMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
                     String shortName="";
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						shortName=nextRow.getCell(1).getStringCellValue();
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						shortName=String.valueOf((int) nextRow.getCell(1).getNumericCellValue());
					}
					
					CountryMaster countryMaster = repositoryCountryMaster.findByShortNameAndIsDeletedAndLanguageLanguageId(shortName, false,language.getLanguageId());
					if(countryMaster==null){
						countryMaster= new CountryMaster();
					}
					countryMaster.setActive(true);
					countryMaster.setShortName(shortName);
					
					if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setCountryCode(nextRow.getCell(0).getStringCellValue());
					} else if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setCountryCode(String.valueOf((int) nextRow.getCell(0).getNumericCellValue()));
					}
					
					if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setCountryName(nextRow.getCell(2).getStringCellValue());
					} else if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setCountryName(String.valueOf((int) nextRow.getCell(2).getNumericCellValue()));
					}
					countryMaster.setIsDeleted(false);
					countryMaster.setLanguage(language);
					countryMaster.setUpdatedBy(loggedInUserId);
					countryMaster.setCreatedBy(loggedInUserId);
					countryMasterList.add(countryMaster);
				}
				repositoryCountryMaster.save(countryMasterList);
				List<CreditCardType> creditCardTypeList = new ArrayList<>();
				for (int i = 1; i <= creditCardTypeSheet.getLastRowNum(); i++) 
				{
					Row nextRow = creditCardTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CreditCardType creditCardType =repositoryCreditCardType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(creditCardType==null){
						creditCardType= new CreditCardType();
					}
					creditCardType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					creditCardType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					creditCardType.setLanguage(language);
					creditCardTypeList.add(creditCardType);
				}
				
				repositoryCreditCardType.save(creditCardTypeList);
				List<CustomerMaintenanceActiveStatus> customerMaintenanceActiveStatusList = new ArrayList<>();
				for (int i = 1; i <= customerMaintenanceActiveStatusSheet.getLastRowNum(); i++) {
					Row nextRow = customerMaintenanceActiveStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CustomerMaintenanceActiveStatus customerMaintenanceActiveStatus = repositoryCustomerMaintenanceActiveStatus.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(customerMaintenanceActiveStatus==null){
						customerMaintenanceActiveStatus= new CustomerMaintenanceActiveStatus();
					}
					customerMaintenanceActiveStatus.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					customerMaintenanceActiveStatus.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					customerMaintenanceActiveStatus.setLanguage(language);
					customerMaintenanceActiveStatusList.add(customerMaintenanceActiveStatus);
				}
				
				repositoryCustomerMaintenanceActiveStatus.save(customerMaintenanceActiveStatusList);
				List<CustomerMaintenanceHoldStatus> customerMaintenanceHoldStatusList = new ArrayList<>();
				
				for (int i = 1; i <= customerMaintenanceHoldStatusSheet.getLastRowNum(); i++) 
				{
					Row nextRow = customerMaintenanceHoldStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CustomerMaintenanceHoldStatus customerMaintenanceHoldStatus = repositoryCustomerMaintenanceHoldStatus.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(customerMaintenanceHoldStatus==null){
						customerMaintenanceHoldStatus= new CustomerMaintenanceHoldStatus();
					}
					customerMaintenanceHoldStatus.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					customerMaintenanceHoldStatus.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					customerMaintenanceHoldStatus.setLanguage(language);
					customerMaintenanceHoldStatusList.add(customerMaintenanceHoldStatus);
				}
				
				repositoryCustomerMaintenanceHoldStatus.save(customerMaintenanceHoldStatusList);
				List<FAAccountTableAccountType> faAccountTableAccountTypeList = new ArrayList<>();
				
				for (int i = 1; i <= faAccountTableAccountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = faAccountTableAccountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					FAAccountTableAccountType faAccountTableAccountType = repositoryFAAccountTableAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(faAccountTableAccountType==null){
						faAccountTableAccountType= new FAAccountTableAccountType();
					}
					faAccountTableAccountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					faAccountTableAccountType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					faAccountTableAccountType.setLanguage(language);
					faAccountTableAccountTypeList.add(faAccountTableAccountType);
					
				}
				repositoryFAAccountTableAccountType.save(faAccountTableAccountTypeList);
				List<FABookMaintenanceAmortizationCodeType> faBookMaintenanceAmortizationCodeTypeList = new ArrayList<>();
				
				for (int i = 1; i <= faBookMaintenanceAmortizationCodeTypeSheet.getLastRowNum(); i++) {
					Row nextRow = faBookMaintenanceAmortizationCodeTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					FABookMaintenanceAmortizationCodeType faBookMaintenanceAmortizationCodeType = repositoryFABookMaintenanceAmortizationCodeType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId
							((int) nextRow.getCell(0).getNumericCellValue(), false, language.getLanguageId());
					if(faBookMaintenanceAmortizationCodeType==null){
						faBookMaintenanceAmortizationCodeType= new FABookMaintenanceAmortizationCodeType();
					}
					faBookMaintenanceAmortizationCodeType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					faBookMaintenanceAmortizationCodeType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					faBookMaintenanceAmortizationCodeType.setLanguage(language);
					faBookMaintenanceAmortizationCodeTypeList.add(faBookMaintenanceAmortizationCodeType);
					
				}
				repositoryFABookMaintenanceAmortizationCodeType.save(faBookMaintenanceAmortizationCodeTypeList);
				
				List<FABookMaintenanceAveragingConventionType> faBookMaintenanceAveragingConventionTypeList = new ArrayList<>();
				for (int i = 1; i <= faBookMaintenanceAveragingConventionTypeSheet.getLastRowNum(); i++) {
					Row nextRow = faBookMaintenanceAveragingConventionTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					FABookMaintenanceAveragingConventionType faBookMaintenanceAveragingConventionType = repositoryFABookMaintenanceAveragingConventionType.
							findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(),
									language.getLanguageId(), false);
					if(faBookMaintenanceAveragingConventionType==null){
						faBookMaintenanceAveragingConventionType= new FABookMaintenanceAveragingConventionType();
					}
					faBookMaintenanceAveragingConventionType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					faBookMaintenanceAveragingConventionType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					faBookMaintenanceAveragingConventionType.setLanguage(language);
					faBookMaintenanceAveragingConventionTypeList.add(faBookMaintenanceAveragingConventionType);
				}
				repositoryFABookMaintenanceAveragingConventionType
				.save(faBookMaintenanceAveragingConventionTypeList);
				List<FABookMaintenanceSwitchOverType> faBookMaintenanceSwitchOverTypeList = new ArrayList<>();
				
				for (int i = 1; i <= faBookMaintenanceSwitchOverTypeSheet.getLastRowNum(); i++) {
					Row nextRow = faBookMaintenanceSwitchOverTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					FABookMaintenanceSwitchOverType faBookMaintenanceSwitchOverType = repositoryFABookMaintenanceSwitchOverType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(faBookMaintenanceSwitchOverType==null){
						faBookMaintenanceSwitchOverType= new FABookMaintenanceSwitchOverType();
					}
					faBookMaintenanceSwitchOverType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					faBookMaintenanceSwitchOverType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					faBookMaintenanceSwitchOverType.setLanguage(language);
					faBookMaintenanceSwitchOverTypeList.add(faBookMaintenanceSwitchOverType);
				}
				repositoryFABookMaintenanceSwitchOverType.save(faBookMaintenanceSwitchOverTypeList);
				
				List<GLConfigurationAuditTrialSeriesType> glConfigurationAuditTrialSeriesTypeList = new ArrayList<>();
				for (int i = 1; i <= glConfigurationAuditTrialSeriesTypeSheet.getLastRowNum(); i++) {
					Row nextRow = glConfigurationAuditTrialSeriesTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType = repositoryGLConfigurationAuditTrialSeriesType.
							findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId((int) nextRow.getCell(0).getNumericCellValue(), false, language.getLanguageId());
					if(glConfigurationAuditTrialSeriesType==null){
						glConfigurationAuditTrialSeriesType= new GLConfigurationAuditTrialSeriesType();
					}
					glConfigurationAuditTrialSeriesType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					glConfigurationAuditTrialSeriesType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					glConfigurationAuditTrialSeriesType.setLanguage(language);
					glConfigurationAuditTrialSeriesTypeList.add(glConfigurationAuditTrialSeriesType);
					
				}
				repositoryGLConfigurationAuditTrialSeriesType.save(glConfigurationAuditTrialSeriesTypeList);
				List<LeaseType> leaseTypeList = new ArrayList<>();
				
				for (int i = 1; i <= leaseTypeSheet.getLastRowNum(); i++) {
					Row nextRow = leaseTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					LeaseType leaseType = repositoryLeaseType.findByLeaseTypeIdAndIsDeletedAndLanguageLanguageId((int) nextRow.getCell(0).getNumericCellValue(),false, language.getLanguageId());
					if(leaseType==null){
						leaseType= new LeaseType();
					}
					leaseType.setLeaseTypeId((int) nextRow.getCell(0).getNumericCellValue());
					leaseType.setValue(nextRow.getCell(1).getStringCellValue());
					leaseType.setLanguage(language);
					leaseTypeList.add(leaseType);
				}
				
				repositoryLeaseType.save(leaseTypeList);
				List<MasterAccountType> masterAccountTypeList = new ArrayList<>();
				for (int i = 1; i <= masterAccountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterAccountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterAccountType masterAccountType = repositoryMasterAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterAccountType==null){
						masterAccountType= new MasterAccountType();
					}
					masterAccountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterAccountType.setAccountType(nextRow.getCell(1).getStringCellValue());
					masterAccountType.setLanguage(language);
					masterAccountTypeList.add(masterAccountType);
				}
				repositoryMasterAccountType.save(masterAccountTypeList);
				
				List<MasterArAccountType> masterArAccountTypeList = new ArrayList<>();
				for (int i = 1; i <= masterArAccountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterArAccountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					MasterArAccountType masterArAccountType = repositoryMasterArAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterArAccountType==null){
						masterArAccountType= new MasterArAccountType();
					}
					masterArAccountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterArAccountType.setAccountType(nextRow.getCell(1).getStringCellValue());
					masterArAccountType.setLanguage(language);
					masterArAccountTypeList.add(masterArAccountType);
					
				}
				repositoryMasterArAccountType.save(masterArAccountTypeList);
				List<MasterBlncdspl> masterBlncdsplList = new ArrayList<>();
				for (int i = 1; i <= masterBlncdsplSheet.getLastRowNum(); i++) {
					Row nextRow = masterBlncdsplSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					MasterBlncdspl masterBlncdspl = repositoryMasterBlncdspl.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((short) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterBlncdspl==null){
						masterBlncdspl= new MasterBlncdspl();
					}
					masterBlncdspl.setTypeId((short) nextRow.getCell(0).getNumericCellValue());
					masterBlncdspl.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterBlncdspl.setLanguage(language);
					masterBlncdsplList.add(masterBlncdspl);
					
				}
				repositoryMasterBlncdspl.save(masterBlncdsplList);
				List<MasterCustomerClassSetupBalanceType> masterCustomerClassSetupBalanceTypeList = new ArrayList<>();
				
				for (int i = 1; i <= masterCustomerClassSetupBalanceTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterCustomerClassSetupBalanceTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterCustomerClassSetupBalanceType masterCustomerClassSetupBalanceType = repositoryMasterCustomerClassSetupBalanceType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterCustomerClassSetupBalanceType==null){
						masterCustomerClassSetupBalanceType= new MasterCustomerClassSetupBalanceType();
					}
					masterCustomerClassSetupBalanceType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterCustomerClassSetupBalanceType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterCustomerClassSetupBalanceType.setLanguage(language);
					masterCustomerClassSetupBalanceTypeList.add(masterCustomerClassSetupBalanceType);
					
				}
				
				repositoryMasterCustomerClassSetupBalanceType.save(masterCustomerClassSetupBalanceTypeList);
				List<MasterCustomerClassMinimumCharge> masterCustomerClassMinimumChargeList = new ArrayList<>();
				
				for (int i = 1; i <= masterCustomerClassMinimumChargeSheet.getLastRowNum(); i++) {
					Row nextRow = masterCustomerClassMinimumChargeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterCustomerClassMinimumCharge masterCustomerClassMinimumCharge = repositoryMasterCustomerClassMinimumCharge.
							findByTypeIdAndLanguageLanguageIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(),
									language.getLanguageId(), false);
					if(masterCustomerClassMinimumCharge==null){
						masterCustomerClassMinimumCharge=new MasterCustomerClassMinimumCharge();
					}
					masterCustomerClassMinimumCharge.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterCustomerClassMinimumCharge.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterCustomerClassMinimumCharge.setLanguage(language);
					masterCustomerClassMinimumChargeList.add(masterCustomerClassMinimumCharge);
					
				}
				repositoryMasterCustomerClassMinimumCharge.save(masterCustomerClassMinimumChargeList);
				List<MasterCustomerClassSetupCreditLimit> masterCustomerClassSetupCreditLimitList = new ArrayList<>();
				
				for (int i = 1; i <= masterCustomerClassSetupCreditLimitSheet.getLastRowNum(); i++) {
					Row nextRow = masterCustomerClassSetupCreditLimitSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterCustomerClassSetupCreditLimit masterCustomerClassSetupCreditLimit =repositoryMasterCustomerClassSetupCreditLimit.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterCustomerClassSetupCreditLimit==null){
						masterCustomerClassSetupCreditLimit=new MasterCustomerClassSetupCreditLimit();
					}
					masterCustomerClassSetupCreditLimit.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterCustomerClassSetupCreditLimit.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterCustomerClassSetupCreditLimit.setLanguage(language);
					masterCustomerClassSetupCreditLimitList.add(masterCustomerClassSetupCreditLimit);
				}
				repositoryMasterCustomerClassSetupCreditLimit.save(masterCustomerClassSetupCreditLimitList);
				List<MasterCustomerClassSetupFinanaceCharge> masterCustomerClassSetupFinanaceChargeList = new ArrayList<>();
				
				for (int i = 1; i <= masterCustomerClassSetupFinanaceChargeSheet.getLastRowNum(); i++) {
					Row nextRow = masterCustomerClassSetupFinanaceChargeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterCustomerClassSetupFinanaceCharge masterCustomerClassSetupFinanaceCharge = repositoryMasterCustomerClassSetupFinanaceCharge.
							findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterCustomerClassSetupFinanaceCharge==null){
						masterCustomerClassSetupFinanaceCharge=new MasterCustomerClassSetupFinanaceCharge();
					}
					masterCustomerClassSetupFinanaceCharge.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterCustomerClassSetupFinanaceCharge.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterCustomerClassSetupFinanaceCharge.setLanguage(language);
					masterCustomerClassSetupFinanaceChargeList.add(masterCustomerClassSetupFinanaceCharge);
				}
				repositoryMasterCustomerClassSetupFinanaceCharge.save(masterCustomerClassSetupFinanaceChargeList);
				List<MasterDepreciationPeriodTypes> masterDepreciationPeriodTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterDepreciationPeriodTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterDepreciationPeriodTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterDepreciationPeriodTypes masterDepreciationPeriodTypes =repositoryMasterDerpreciationPeriodTypes.
							findByTypeIdAndLanguageLanguageIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterDepreciationPeriodTypes==null){
						masterDepreciationPeriodTypes=new MasterDepreciationPeriodTypes();
					}
					masterDepreciationPeriodTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterDepreciationPeriodTypes.setDepreciationPeriodType(nextRow.getCell(1).getStringCellValue());
					masterDepreciationPeriodTypes.setLanguage(language);
					masterDepreciationPeriodTypesList.add(masterDepreciationPeriodTypes);
					
				}
				repositoryMasterDerpreciationPeriodTypes.save(masterDepreciationPeriodTypesList);
				List<MasterDiscountType> masterDiscountTypeList = new ArrayList<>();
				
				for (int i = 1; i <= masterDiscountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterDiscountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterDiscountType masterDiscountType = repositoryMasterDiscountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterDiscountType==null){
						masterDiscountType= new MasterDiscountType();
					}
					masterDiscountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterDiscountType.setDiscountType(nextRow.getCell(1).getStringCellValue());
					masterDiscountType.setLanguage(language);
					masterDiscountTypeList.add(masterDiscountType);
					
				}
				repositoryMasterDiscountType.save(masterDiscountTypeList);
				List<MasterDiscountTypePeriod> masterDiscountTypePeriodList = new ArrayList<>();

				for (int i = 1; i <= masterDiscountTypePeriodSheet.getLastRowNum(); i++) {
					Row nextRow = masterDiscountTypePeriodSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterDiscountTypePeriod masterDiscountTypePeriod = repositoryMasterDiscountTypePeriod.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterDiscountTypePeriod==null){
						masterDiscountTypePeriod= new MasterDiscountTypePeriod();
					}
					masterDiscountTypePeriod.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterDiscountTypePeriod.setDiscountTypePriod(nextRow.getCell(1).getStringCellValue());
					masterDiscountTypePeriod.setLanguage(language);
					masterDiscountTypePeriodList.add(masterDiscountTypePeriod);
				}
				repositoryMasterDiscountTypePeriod.save(masterDiscountTypePeriodList);
				List<MasterDueTypes> masterDueTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterDueTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterDueTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterDueTypes masterDueTypes = repositoryMasterDueTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterDueTypes==null){
						masterDueTypes= new MasterDueTypes();
					}
					masterDueTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterDueTypes.setDueType(nextRow.getCell(1).getStringCellValue());
					masterDueTypes.setLanguage(language);
					masterDueTypesList.add(masterDueTypes);
					
				}
				repositoryMasterDueTypes.save(masterDueTypesList);
				List<MasterFAAccountGroupAccountType> masterFAAccountGroupAccountTypeList = new ArrayList<>();
				
				for (int i = 1; i <= masterFAAccountGroupAccountTypeSheet.getLastRowNum(); i++) {
					Row nextRow = masterFAAccountGroupAccountTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					MasterFAAccountGroupAccountType masterFAAccountGroupAccountType = repositoryMasterFAAccountGroupAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterFAAccountGroupAccountType==null){
						masterFAAccountGroupAccountType= new MasterFAAccountGroupAccountType();
					}
					masterFAAccountGroupAccountType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterFAAccountGroupAccountType.setAccountType(nextRow.getCell(1).getStringCellValue());
					masterFAAccountGroupAccountType.setLanguage(language);
					masterFAAccountGroupAccountTypeList.add(masterFAAccountGroupAccountType);
				}
				repositoryMasterFAAccountGroupAccountType.save(masterFAAccountGroupAccountTypeList);
				List<HoldVendorStatus> holdVendorStatusList = new ArrayList<>();
				
				for (int i = 1; i <= holdVendorStatusSheet.getLastRowNum(); i++) 
				{
					Row nextRow = holdVendorStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					HoldVendorStatus holdVendorStatus = repositoryHoldVendorStatus.findByHoldStatusAndLanguageLanguageIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(holdVendorStatus==null){
						holdVendorStatus= new HoldVendorStatus();
					}
					holdVendorStatus.setHoldStatus((int) nextRow.getCell(0).getNumericCellValue());
					holdVendorStatus.setHoldStatusPrimary(nextRow.getCell(1).getStringCellValue());
					holdVendorStatus.setLanguage(language);
					holdVendorStatusList.add(holdVendorStatus);
				}
				
				repositoryHoldVendorStatus.save(holdVendorStatusList);
				List<MasterNegativeSymbolSignTypes> masterNegativeSymbolSignTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterNegativeSymbolSignTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterNegativeSymbolSignTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes = repositoryMasterNegativeSymbolSignTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterNegativeSymbolSignTypes==null){
						masterNegativeSymbolSignTypes=new MasterNegativeSymbolSignTypes();
					}
					masterNegativeSymbolSignTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterNegativeSymbolSignTypes.setNegativeSymbolSignType(nextRow.getCell(1).getStringCellValue());
					masterNegativeSymbolSignTypes.setLanguage(language);
					masterNegativeSymbolSignTypesList.add(masterNegativeSymbolSignTypes);
				}
				repositoryMasterNegativeSymbolSignTypes.save(masterNegativeSymbolSignTypesList);
				List<MasterNegativeSymbolTypes> masterNegativeSymbolTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterNegativeSymbolTypesSheet.getLastRowNum(); i++) 
				{
					Row nextRow = masterNegativeSymbolTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterNegativeSymbolTypes masterNegativeSymbolTypes = repositoryMasterNegativeSymbolTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterNegativeSymbolTypes==null){
						masterNegativeSymbolTypes= new MasterNegativeSymbolTypes();
					}
					masterNegativeSymbolTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterNegativeSymbolTypes.setNegativeSymbolType(nextRow.getCell(1).getStringCellValue());
					masterNegativeSymbolTypes.setLanguage(language);
					masterNegativeSymbolTypesList.add(masterNegativeSymbolTypes);
				}
				repositoryMasterNegativeSymbolTypes.save(masterNegativeSymbolTypesList);
				List<MasterRateFrequencyTypes> masterRateFrequencyTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterRateFrequencyTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterRateFrequencyTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterRateFrequencyTypes masterRateFrequencyTypes = repositoryMasterRateFrequencyTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterRateFrequencyTypes==null){
						masterRateFrequencyTypes= new MasterRateFrequencyTypes();
					}
					masterRateFrequencyTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterRateFrequencyTypes.setRateFrequencyType(nextRow.getCell(1).getStringCellValue());
					masterRateFrequencyTypes.setLanguage(language);
					masterRateFrequencyTypesList.add(masterRateFrequencyTypes);
				}
				
				repositoryMasterRateFrequencyTypes.save(masterRateFrequencyTypesList);
				List<MasterRMAgeingTypes> masterRMAgeingTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterRMAgeingTypesSheet.getLastRowNum(); i++) 
				{
					Row nextRow = masterRMAgeingTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterRMAgeingTypes masterRMAgeingTypes = repositoryMasterRMAgeingTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterRMAgeingTypes==null){
						masterRMAgeingTypes= new MasterRMAgeingTypes();
					}
					masterRMAgeingTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterRMAgeingTypes.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterRMAgeingTypes.setLanguage(language);
					masterRMAgeingTypesList.add(masterRMAgeingTypes);
				}
				repositoryMasterRMAgeingTypes.save(masterRMAgeingTypesList);
				List<MasterRMApplyByDefaultTypes> masterRMApplyByDefaultTypesList = new ArrayList<>();
				
				for (int i = 1; i <= masterRMApplyByDefaultTypesSheet.getLastRowNum(); i++) {
					Row nextRow = masterRMApplyByDefaultTypesSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterRMApplyByDefaultTypes masterRMApplyByDefaultTypes = repositoryMasterRMApplyByDefaultTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterRMApplyByDefaultTypes==null)
					{
						masterRMApplyByDefaultTypes= new MasterRMApplyByDefaultTypes();
					}
					masterRMApplyByDefaultTypes.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					masterRMApplyByDefaultTypes.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					masterRMApplyByDefaultTypes.setLanguage(language);
					masterRMApplyByDefaultTypesList.add(masterRMApplyByDefaultTypes);
					
				}
				repositoryMasterRMApplyByDefaultTypes.save(masterRMApplyByDefaultTypesList);
				List<MasterSeperatorDecimal> masterSeperatorDecimalList = new ArrayList<>();
				
				for (int i = 1; i <= masterSeperatorDecimalSheet.getLastRowNum(); i++) {
					Row nextRow = masterSeperatorDecimalSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterSeperatorDecimal masterSeperatorDecimal = repositoryMasterSeperatorDecimal.findBySeperatorDecimalIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterSeperatorDecimal==null){
						masterSeperatorDecimal= new MasterSeperatorDecimal();
					}
					masterSeperatorDecimal.setSeperatorDecimalId((int) nextRow.getCell(0).getNumericCellValue());
					masterSeperatorDecimal.setSeperatorDecimalPrimary(nextRow.getCell(1).getStringCellValue());
					masterSeperatorDecimal.setLanguage(language);
					masterSeperatorDecimalList.add(masterSeperatorDecimal);
				}
				repositoryMasterSeperatorDecimal.save(masterSeperatorDecimalList);
				List<MasterSeperatorThousands> masterSeperatorThousandsList = new ArrayList<>();
				for (int i = 1; i <= masterSeperatorThousandsSheet.getLastRowNum(); i++) {
					Row nextRow = masterSeperatorThousandsSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterSeperatorThousands masterSeperatorThousands = repositoryMasterSeperatorThousands.findBySeperatorThousandIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterSeperatorThousands==null){
						masterSeperatorThousands= new MasterSeperatorThousands();
					}
					masterSeperatorThousands.setSeperatorThousandId((int) nextRow.getCell(0).getNumericCellValue());
					masterSeperatorThousands.setSeperatorThousandPrimary(nextRow.getCell(1).getStringCellValue());
					masterSeperatorThousands.setLanguage(language);
					masterSeperatorThousandsList.add(masterSeperatorThousands);
					
				}
			 
				repositoryMasterSeperatorThousands.save(masterSeperatorThousandsList);
				List<MasterVendorStatus> masterVendorStatusList = new ArrayList<>();
				for (int i = 1; i <= masterVendorStatusSheet.getLastRowNum(); i++) {
					Row nextRow = masterVendorStatusSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					MasterVendorStatus masterVendorStatus = repositoryMasterVendorStatus.findByStatusIdAndLanguageLanguageIdAndIsDeleted
							((short) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(masterVendorStatus==null){
						masterVendorStatus= new MasterVendorStatus();
					}
					masterVendorStatus.setStatusId((short) nextRow.getCell(0).getNumericCellValue());
					masterVendorStatus.setStatusPrimary(nextRow.getCell(1).getStringCellValue());
					masterVendorStatus.setLanguage(language);
					masterVendorStatusList.add(masterVendorStatus);
				}
				repositoryMasterVendorStatus.save(masterVendorStatusList);
				List<PMSetupFormatType> pmSetupFormatTypeList = new ArrayList<>();
				
				for (int i = 1; i <= pmSetupFormatTypeSheet.getLastRowNum(); i++) {
					Row nextRow = pmSetupFormatTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					PMSetupFormatType pmSetupFormatType = repositoryPMSetupFormatType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(pmSetupFormatType==null){
						pmSetupFormatType= new PMSetupFormatType();
					}
					pmSetupFormatType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					pmSetupFormatType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					pmSetupFormatType.setLanguage(language);
					pmSetupFormatTypeList.add(pmSetupFormatType);
					
				}
				repositoryPMSetupFormatType.save(pmSetupFormatTypeList);
				List<SeriesType> seriesTypeList = new ArrayList<>();
				
				for (int i = 1; i <= seriesTypeSheet.getLastRowNum(); i++) {
					
					Row nextRow = seriesTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					SeriesType seriesType = repositorySeriesType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(seriesType==null){
						seriesType= new SeriesType();
					}
					seriesType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					seriesType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					seriesType.setLanguage(language);
					seriesTypeList.add(seriesType);
				}
				repositorySeriesType.save(seriesTypeList);
				List<ShipmentMethodType> shipmentMethodTypeList = new ArrayList<>();
				
				for (int i = 1; i <= shipmentMethodTypeSheet.getLastRowNum(); i++) 
				{
					Row nextRow = shipmentMethodTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					ShipmentMethodType shipmentMethodType = repositoryShipmentMethodType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(shipmentMethodType==null){
						shipmentMethodType= new ShipmentMethodType();
					}
					shipmentMethodType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					shipmentMethodType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					shipmentMethodType.setLanguage(language);
					shipmentMethodTypeList.add(shipmentMethodType);
				}
				repositoryShipmentMethodType.save(shipmentMethodTypeList);
				List<TPCLBalanceType> tpclBalanceTypeList = new ArrayList<>();
				
				for (int i = 1; i <= tpclBalanceTypeSheet.getLastRowNum(); i++) {
					Row nextRow = tpclBalanceTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					TPCLBalanceType tpclBalanceType = repositoryTPCLBalanceType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(tpclBalanceType==null){
						tpclBalanceType= new TPCLBalanceType();
					}
					tpclBalanceType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					tpclBalanceType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					tpclBalanceType.setLanguage(language);
					tpclBalanceTypeList.add(tpclBalanceType);
				}
				
				repositoryTPCLBalanceType.save(tpclBalanceTypeList);
				List<VATBasedOnType> vatBasedOnTypeList = new ArrayList<>();
				
				for (int i = 1; i <= vatBasedOnTypeSheet.getLastRowNum(); i++) {
					Row nextRow = vatBasedOnTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					VATBasedOnType vatBasedOnType = repositoryVATBasedOnType.findByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(vatBasedOnType==null){
						vatBasedOnType= new VATBasedOnType();
					}
					vatBasedOnType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					vatBasedOnType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					vatBasedOnType.setLanguage(language);
					vatBasedOnTypeList.add(vatBasedOnType);
				}
				
				repositoryVATBasedOnType.save(vatBasedOnTypeList);
				List<VATType> vatTypeList = new ArrayList<>();
				for (int i = 1; i <= vatTypeSheet.getLastRowNum(); i++) {
					Row nextRow = vatTypeSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					VATType vatType = repositoryVATType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted
							((int) nextRow.getCell(0).getNumericCellValue(), language.getLanguageId(), false);
					if(vatType==null){
						vatType= new VATType();
					}
					vatType.setTypeId((int) nextRow.getCell(0).getNumericCellValue());
					vatType.setTypePrimary(nextRow.getCell(1).getStringCellValue());
					vatType.setLanguage(language);
					vatTypeList.add(vatType);
					
				}
				repositoryVATType.save(vatTypeList);
				
			inputStream.close();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			language.setIsDeleted(true);
			language.setUpdatedBy(loggedInUserId);
			repositoryLanguage.saveAndFlush(language);
			return false;
		}
		return true;
	
	}
	
	public DtoAuditTrailCode getAuditTrialByTransactionType(DtoAuditTrailCode dtoAuditTrailCode) {
		GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
		GLConfigurationAuditTrialCodes audit = null;
		if(dtoAuditTrailCode.getSourceCode().equalsIgnoreCase("PV")) {
			
			if(dtoAuditTrailCode.getSeriesNumber() == 1) {
				
				audit = glConfigurationSetup.getCashAuditTrialPayment();
			
			}else if(dtoAuditTrailCode.getSeriesNumber() == 2) {
				
				audit = glConfigurationSetup.getCheckAuditTrialPayment();
				
			}else if(dtoAuditTrailCode.getSeriesNumber() ==3) {
				
				audit = glConfigurationSetup.getCreditAuditTrialPayment();
				
			}else if(dtoAuditTrailCode.getSeriesNumber() == 4) {
				
				audit = glConfigurationSetup.getTransferAuditTrialPayment();
				
			}
			
		}else if(dtoAuditTrailCode.getSourceCode().equalsIgnoreCase("RV")) {
			
			if(dtoAuditTrailCode.getSeriesNumber() == 1) {
				audit = glConfigurationSetup.getCashAuditTrialReceipt();
			}else if(dtoAuditTrailCode.getSeriesNumber() == 2) {
				audit = glConfigurationSetup.getcheckAuditTrialReceipt();
			}else if(dtoAuditTrailCode.getSeriesNumber() ==3) {
				audit = glConfigurationSetup.getCreditAuditTrialReceipt();
			}else if(dtoAuditTrailCode.getSeriesNumber() == 4) {
				audit = glConfigurationSetup.getTransferAuditTrialReceipt();
			}
			
		}
		dtoAuditTrailCode =  mappingAuditTrialCode.modelAuditToDtoAudit(audit);
		return dtoAuditTrailCode;
	}
	
}
