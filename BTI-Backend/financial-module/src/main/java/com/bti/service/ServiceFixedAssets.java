/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.constant.MessageLabel;
import com.bti.model.BookSetup;
import com.bti.model.FAAccountGroupAccountTableSetup;
import com.bti.model.FAAccountGroupsSetup;
import com.bti.model.FABookClassSetup;
import com.bti.model.FACalendarSetup;
import com.bti.model.FACalendarSetupMonthly;
import com.bti.model.FACalendarSetupQuarter;
import com.bti.model.FAClassSetup;
import com.bti.model.FACompanySetup;
import com.bti.model.FAGeneralMaintenance;
import com.bti.model.FAInsuranceClassSetup;
import com.bti.model.FALeaseCompanySetup;
import com.bti.model.FALocationSetup;
import com.bti.model.FAPhysicalLocationSetup;
import com.bti.model.FAPurchasePostingAccountSetup;
import com.bti.model.FARetirementSetup;
import com.bti.model.FAStructureSetup;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.MasterDepreciationPeriodTypes;
import com.bti.model.MasterFAAccountGroupAccountType;
import com.bti.model.MasterGeneralMaintenanceAssetStatus;
import com.bti.model.MasterGeneralMaintenanceAssetType;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.VendorMaintenance;
import com.bti.model.dto.DtoBookSetup;
import com.bti.model.dto.DtoFABookClassSetup;
import com.bti.model.dto.DtoFACalendarSetup;
import com.bti.model.dto.DtoFACalendarSetupMonthly;
import com.bti.model.dto.DtoFACalendarSetupQuarter;
import com.bti.model.dto.DtoFAClassSetup;
import com.bti.model.dto.DtoFACompanySetup;
import com.bti.model.dto.DtoFAGeneralMaintenance;
import com.bti.model.dto.DtoFAPurchasePostingAccountSetup;
import com.bti.model.dto.DtoFAStructureSetup;
import com.bti.model.dto.DtoFaAccountGroupSetup;
import com.bti.model.dto.DtoInsuranceClass;
import com.bti.model.dto.DtoLeaseCompanySetup;
import com.bti.model.dto.DtoLocationSetup;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoPhysicalLocationSetup;
import com.bti.model.dto.DtoRetirementSetup;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryBookSetup;
import com.bti.repository.RepositoryCityMaster;
import com.bti.repository.RepositoryCountryMaster;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryFAAccountGroupAccountTableSetup;
import com.bti.repository.RepositoryFAAccountGroupSetup;
import com.bti.repository.RepositoryFABookClassSetup;
import com.bti.repository.RepositoryFABookMaintenanceAmortizationCodeType;
import com.bti.repository.RepositoryFABookMaintenanceAveragingConventionType;
import com.bti.repository.RepositoryFABookMaintenanceSwitchOverType;
import com.bti.repository.RepositoryFACalendarSetup;
import com.bti.repository.RepositoryFACalendarSetupMonthly;
import com.bti.repository.RepositoryFACalendarSetupQuarter;
import com.bti.repository.RepositoryFAClassSetup;
import com.bti.repository.RepositoryFACompanySetup;
import com.bti.repository.RepositoryFADepreciationMethods;
import com.bti.repository.RepositoryFAGeneralMaintenance;
import com.bti.repository.RepositoryFAInsuranceClassSetup;
import com.bti.repository.RepositoryFAInsuranceMaintenance;
import com.bti.repository.RepositoryFALeaseCompanySetup;
import com.bti.repository.RepositoryFALocationSetup;
import com.bti.repository.RepositoryFAPhysicalLocationSetup;
import com.bti.repository.RepositoryFAPropertyTypes;
import com.bti.repository.RepositoryFAPurchasePostingAccountSetup;
import com.bti.repository.RepositoryFARetirementSetup;
import com.bti.repository.RepositoryFAStructureSetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryMasterDerpreciationPeriodTypes;
import com.bti.repository.RepositoryMasterFAAccountGroupAccountType;
import com.bti.repository.RepositoryMasterGeneralMaintenanceAssetStatus;
import com.bti.repository.RepositoryMasterGeneralMaintenanceAssetType;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.repository.RepositoryStateMaster;
import com.bti.repository.RepositoryVendorClassAccountTableSetup;
import com.bti.repository.RepositoryVendorMaintenance;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: Service Fixed Assets
 * Name of Project: BTI
 * Created on: Aug 22, 2017
 * Modified on: Aug 22, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service("serviceFixedAssets")
public class ServiceFixedAssets {

	private static final Logger LOG = Logger.getLogger(ServiceFixedAssets.class);
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryFARetirementSetup repositoryFARetirementSetup;
	
	@Autowired
	RepositoryBookSetup repositoryBookSetup;
	
	@Autowired
	RepositoryFACompanySetup repositoryFACompanySetup;
	
	@Autowired
	RepositoryMasterDerpreciationPeriodTypes repositoryMasterDerpreciationPeriodTypes;
	
	@Autowired
	RepositoryFACalendarSetup repositoryFACalendarSetup;
	
	@Autowired
	RepositoryFACalendarSetupMonthly repositoryFACalendarSetupMonthly;
	
	@Autowired
	RepositoryFACalendarSetupQuarter repositoryFACalendarSetupQuarter;
	
	@Autowired
	RepositoryFAStructureSetup repositoryFAStructureSetup;
	
	@Autowired
	RepositoryFALocationSetup repositoryFALocationSetup;
	
	@Autowired
	RepositoryCountryMaster repositoryCountryMaster;
	
	@Autowired
	RepositoryStateMaster repositoryStateMaster;
	
	@Autowired
	RepositoryCityMaster repositoryCityMaster;
	
	@Autowired
	RepositoryFAPhysicalLocationSetup repositoryFAPhysicalLocationSetup;
	
	@Autowired
	RepositoryFAClassSetup repositoryFAClassSetup;
	
	@Autowired
	RepositoryFAAccountGroupSetup repositoryFAAccountGroupSetup;
	
	@Autowired
	RepositoryFAPropertyTypes repositoryFAPropertyTypes;
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	
	@Autowired
	RepositoryFAGeneralMaintenance repositoryFAGeneralMaintenance;
	
	@Autowired
	RepositoryMasterGeneralMaintenanceAssetStatus repositoryMasterGeneralMaintenanceAssetStatus;
	
	@Autowired
	RepositoryMasterGeneralMaintenanceAssetType repositoryMasterGeneralMaintenanceAssetType;
	
	@Autowired
	RepositoryFALeaseCompanySetup repositoryFALeaseCompanySetup;
	
	@Autowired
	RepositoryVendorMaintenance repositoryVendorMaintenance;
	
	@Autowired
	RepositoryFAInsuranceClassSetup repositoryFAInsuranceClassSetup;
	
	@Autowired
	RepositoryFAInsuranceMaintenance repositoryFAInsuranceMaintenance;
	
	@Autowired
	RepositoryFABookClassSetup repositoryFABookClassSetup;
	
	@Autowired
	RepositoryFADepreciationMethods repositoryFADepreciationMethods;
	
	@Autowired
	RepositoryFABookMaintenanceAveragingConventionType repositoryFABookMaintenanceAveragingConventionType;
	
	@Autowired
	RepositoryFABookMaintenanceSwitchOverType repositoryFABookMaintenanceSwitchOverType;
	
	@Autowired
	RepositoryFABookMaintenanceAmortizationCodeType repositoryFABookMaintenanceAmortizationCodeType;
	
	@Autowired
	RepositoryFAAccountGroupAccountTableSetup repositoryFAAccountGroupAccountTableSetup;
	
	@Autowired
	RepositoryMasterFAAccountGroupAccountType repositoryMasterFAAccountGroupAccountType;
	
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryFAPurchasePostingAccountSetup repositoryFAPurchasePostingAccountSetup;
	
	@Autowired
	RepositoryVendorClassAccountTableSetup repositoryVendorClassAccountTableSetup;
	
	@Autowired
	ServiceAccountPayable serviceAccountPayable;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;
	
	 
	/**
	 * @param dtoRetirementSetup
	 * @return
	 */
	public DtoRetirementSetup saveFixedAssetsRetirementSetup(DtoRetirementSetup dtoRetirementSetup) {

		FARetirementSetup faRetirementSetup = null;
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try {
			// creating new instance
			faRetirementSetup = new FARetirementSetup();
			faRetirementSetup.setCreatedBy(loggedInUserId);
			faRetirementSetup.setRetirementId(dtoRetirementSetup.getFaRetirementId());
			faRetirementSetup.setRetirementDescription(dtoRetirementSetup.getDescriptionPrimary());
			faRetirementSetup.setRetirementDescriptionArabic(dtoRetirementSetup.getDescriptionSecondary());
			// saving new object into DB
			faRetirementSetup = repositoryFARetirementSetup.saveAndFlush(faRetirementSetup);
		} catch (Exception e) {
			LOG.info(e);
		}
		// Return saved object via DTO
		return new DtoRetirementSetup(faRetirementSetup);
	}

	/**
	 * @param dtoRetirementSetup
	 * @param faRetirementSetup
	 * @return
	 */
	public DtoRetirementSetup updateFixedAssetsRetirementSetup(DtoRetirementSetup dtoRetirementSetup,
			FARetirementSetup faRetirementSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try {
			// updating values
			faRetirementSetup.setChangeBy(loggedInUserId);
			faRetirementSetup.setRetirementDescription(dtoRetirementSetup.getDescriptionPrimary());
			faRetirementSetup.setRetirementDescriptionArabic(dtoRetirementSetup.getDescriptionSecondary());
			// saving updated object into DB
			faRetirementSetup = repositoryFARetirementSetup.saveAndFlush(faRetirementSetup);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		// Return saved object via DTO
		return new DtoRetirementSetup(faRetirementSetup);
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchOrGetAllFixedAssetsRetirementSetup(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryFARetirementSetup.predictiveRetirementSetupSearchCount(dtoSearchs.getSearchKeyword()));
		DtoRetirementSetup dtoRetirementSetup = null;
		List<FARetirementSetup> faRetirementSetups = null;
		List<DtoRetirementSetup> dtoRetirementSetups = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			faRetirementSetups = repositoryFARetirementSetup
					.predictiveRetirementSetupSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			faRetirementSetups = repositoryFARetirementSetup
					.predictiveRetirementSetupSearch(dtoSearchs.getSearchKeyword());
		}
		if (faRetirementSetups != null) {
			for (FARetirementSetup faRetirementSetup : faRetirementSetups) {
				dtoRetirementSetup = new DtoRetirementSetup(faRetirementSetup);
				dtoRetirementSetups.add(dtoRetirementSetup);
			}
		}
		dtoSearch.setRecords(dtoRetirementSetups);
		return dtoSearch;
	}

	/**
	 * @param dtoRetirementSetup
	 * @return
	 */
	public DtoRetirementSetup getFixedAssetsRetirementSetupById(DtoRetirementSetup dtoRetirementSetup) {
		FARetirementSetup faRetirementSetup = null;
		try {
			faRetirementSetup = repositoryFARetirementSetup.findByRetirementIdAndIsDeleted(dtoRetirementSetup.getFaRetirementId(),false);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return new DtoRetirementSetup(faRetirementSetup);
	}
	
	/**
	 * @param dtoFACompanySetup
	 * @return
	 */
	public DtoFACompanySetup saveFixAssetCompanySetup(DtoFACompanySetup dtoFACompanySetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		FACompanySetup faCompanySetup=null;
		try
		{
			faCompanySetup = repositoryFACompanySetup.findByBookSetupBookInxdAndIsDeleted(dtoFACompanySetup.getBookIndexId(),false);
			if(faCompanySetup!=null){
				dtoFACompanySetup.setUpdateAndSaveMessage("FA_COMPANY_SETUP_UPDATE_SUCCESSFULLY");
			}
			else{
				faCompanySetup= new FACompanySetup();
				dtoFACompanySetup.setUpdateAndSaveMessage("FA_COMPANY_SETUP_SAVED_SUCCESSFULLY");
			}
			
			faCompanySetup.setBookSetup(repositoryBookSetup.findByBookInxdAndIsDeleted(dtoFACompanySetup.getBookIndexId(),false));
			// Description
			faCompanySetup.setBookDescription(dtoFACompanySetup.getBookDescription());
			faCompanySetup.setBookDescriptionArabic(dtoFACompanySetup.getBookDescriptionArabic());
		    //Options
			faCompanySetup.setPostPurchaseOrderProcessing(dtoFACompanySetup.getPostPurchaseOrderProcessing());
			faCompanySetup.setRequireAssetAccount(dtoFACompanySetup.getRequireAssetAccount());
			faCompanySetup.setAutoAddBookInformation(dtoFACompanySetup.getAutoAddBookInformation());
			faCompanySetup.setDefaultAssetLabel(dtoFACompanySetup.getDefaultAssetLabel());
			faCompanySetup.setPostDetails(dtoFACompanySetup.getPostDetails());
			//Purchasing Options
			faCompanySetup.setPostPayableManagement(dtoFACompanySetup.getPostPayableManagement());
			faCompanySetup.setPostPurchaseOrderProcessing(dtoFACompanySetup.getPostPurchaseOrderProcessing());
			faCompanySetup.setCreatedBy(loggedInUserId);
			repositoryFACompanySetup.save(faCompanySetup);
		}
		catch (Exception e) {
			dtoFACompanySetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
			
		}
		return dtoFACompanySetup;
	}
	
	/**
	 * @param faCompanySetup
	 * @return
	 */
	public DtoFACompanySetup getFixAssetCompanySetup(FACompanySetup faCompanySetup) 
	{
		DtoFACompanySetup dtoFACompanySetup=null;
		if(faCompanySetup!=null){
			dtoFACompanySetup= new DtoFACompanySetup(faCompanySetup);
		}
		return dtoFACompanySetup;
	}
	
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchFixAssetCompanySetup(DtoSearch dtoSearchs) 
	{
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryFACompanySetup.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		DtoFACompanySetup dtoFACompanySetup = null;
		List<FACompanySetup> faCompanySetupsList = null;
		List<DtoFACompanySetup> dtoFACompanySetupsList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			faCompanySetupsList = repositoryFACompanySetup
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			faCompanySetupsList = repositoryFACompanySetup
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (faCompanySetupsList != null) {
			for (FACompanySetup faCompanySetup : faCompanySetupsList) {
				dtoFACompanySetup = new DtoFACompanySetup(faCompanySetup);
				dtoFACompanySetupsList.add(dtoFACompanySetup);
			}
		}
		dtoSearch.setRecords(dtoFACompanySetupsList);
		return dtoSearch;
	}
	
	/**
	 * @param dtoBookSetup
	 * @return
	 */
	public DtoBookSetup saveBookSetup(DtoBookSetup dtoBookSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid = serviceHome.getLanngugaeId();
		try
		{
			BookSetup bookSetup= new BookSetup();
			bookSetup.setBookId(dtoBookSetup.getBookId());
			// Set Calendar Setup
			bookSetup.setFaCalendarSetup(repositoryFACalendarSetup.findByCalendarIndexAndIsDeleted(dtoBookSetup.getFaCalendarSetupIndexId(),false));
			// Description
			bookSetup.setBookDescription(dtoBookSetup.getBookDescription());
			bookSetup.setBookDescriptionArabic(dtoBookSetup.getBookDescriptionArabic());
		    //Options
			bookSetup.setCurrentYear(dtoBookSetup.getCurrentYear());
			bookSetup.setMasterDepreciationPeriodTypes(repositoryMasterDerpreciationPeriodTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoBookSetup.getDepreciationPeriodId(),langid,false));
			bookSetup.setCreatedBy(loggedInUserId);
			repositoryBookSetup.save(bookSetup);
		}
		catch (Exception e) {
			dtoBookSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoBookSetup;
	}
	
	/**
	 * @param dtoBookSetup
	 * @param bookSetup
	 * @return
	 */
	public DtoBookSetup getBookSetupByBookIndexId(DtoBookSetup dtoBookSetup,BookSetup bookSetup) 
	{
		int langId=serviceHome.getLanngugaeId();
		try
		{
			MasterDepreciationPeriodTypes masterDepreciationPeriodTypes = bookSetup.getMasterDepreciationPeriodTypes();
			if(masterDepreciationPeriodTypes!=null){
				masterDepreciationPeriodTypes = repositoryMasterDerpreciationPeriodTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterDepreciationPeriodTypes.getTypeId(),langId,false);
			}
			dtoBookSetup=new DtoBookSetup(bookSetup,masterDepreciationPeriodTypes);
			
		}
		catch (Exception e) {
			dtoBookSetup.setMessageType("INTERNAL_SERVER_ERROR");
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoBookSetup;
	}
	
	/**
	 * @param dtoBookSetup
	 * @param bookSetup
	 * @return
	 */
	public DtoBookSetup updateBookSetup(DtoBookSetup dtoBookSetup,BookSetup bookSetup) 
	{
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid = serviceHome.getLanngugaeId();
		try
		{
			// Set Calendar Setup
			bookSetup.setFaCalendarSetup(repositoryFACalendarSetup.findByCalendarIndexAndIsDeleted(dtoBookSetup.getFaCalendarSetupIndexId(),false));
			// Description
			bookSetup.setBookDescription(dtoBookSetup.getBookDescription());
			bookSetup.setBookDescriptionArabic(dtoBookSetup.getBookDescriptionArabic());
		    //Options
			bookSetup.setCurrentYear(dtoBookSetup.getCurrentYear());
			bookSetup.setMasterDepreciationPeriodTypes(repositoryMasterDerpreciationPeriodTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoBookSetup.getDepreciationPeriodId(),langid,false));
			bookSetup.setChangeBy(loggedInUserId);
			repositoryBookSetup.save(bookSetup);
		}
		catch (Exception e) {
			dtoBookSetup.setMessageType("INTERNAL_SERVER_ERROR");
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoBookSetup;
	}
	
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchBookSetup(DtoSearch dtoSearchs) 
	{
		int langId=serviceHome.getLanngugaeId();
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryBookSetup.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		DtoBookSetup dtoBookSetup = null;
		List<BookSetup> bookSetups = null;
		List<DtoBookSetup> dtoBookSetupsList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			bookSetups = repositoryBookSetup
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			bookSetups = repositoryBookSetup
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (bookSetups != null) {
			for (BookSetup bookSetup : bookSetups) {
				MasterDepreciationPeriodTypes masterDepreciationPeriodTypes = bookSetup.getMasterDepreciationPeriodTypes();
				if(masterDepreciationPeriodTypes!=null){
					masterDepreciationPeriodTypes = repositoryMasterDerpreciationPeriodTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterDepreciationPeriodTypes.getTypeId(),langId,false);
				}
				dtoBookSetup = new DtoBookSetup(bookSetup,masterDepreciationPeriodTypes);
				dtoBookSetupsList.add(dtoBookSetup);
			}
		}
		dtoSearch.setRecords(dtoBookSetupsList);
		return dtoSearch;
	}

	/**
	 * @param dtoFACalendarSetup
	 * @return
	 */
	public DtoFACalendarSetup saveFixAssetCalendarMonthlySetup(DtoFACalendarSetup dtoFACalendarSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try{
			
		FACalendarSetup faCalendarSetup = repositoryFACalendarSetup.findByDataTypeAndIsDeleted("MONTHLY",false); 
		if(faCalendarSetup!=null){
			repositoryFACalendarSetupMonthly.deleteAllInBatch();
			dtoFACalendarSetup.setMessageType(MessageLabel.CALENDAR_SETUP_UPDATED_SUCCESSFULLY);
		}else{
			faCalendarSetup = new FACalendarSetup();
			dtoFACalendarSetup.setMessageType(MessageLabel.CALENDAR_SETUP_SAVED_SUCCESSFULLY);
		}
			
			faCalendarSetup.setCalendarDescription(dtoFACalendarSetup.getCalendarDescription());
			faCalendarSetup.setCalendarDescriptionArabic(dtoFACalendarSetup.getCalendarDescriptionArabic());
			faCalendarSetup.setYear1(dtoFACalendarSetup.getYear1());
			faCalendarSetup.setCalendarId(dtoFACalendarSetup.getCalendarId());
			faCalendarSetup.setCreatedBy(loggedInUserId);
			faCalendarSetup.setDataType("MONTHLY");
			faCalendarSetup = repositoryFACalendarSetup.saveAndFlush(faCalendarSetup);
			dtoFACalendarSetup.setCalendarIndex(faCalendarSetup.getCalendarIndex());
			for(DtoFACalendarSetupMonthly dtoFACalendarSetupMonthly : dtoFACalendarSetup.getMonthlyCalendar()){
				FACalendarSetupMonthly faCalendarSetupMonthly = new FACalendarSetupMonthly();
				faCalendarSetupMonthly.setCreatedBy(loggedInUserId);
				faCalendarSetupMonthly.setEndDate(dtoFACalendarSetupMonthly.getEndDate());
				faCalendarSetupMonthly.setStartDate(dtoFACalendarSetupMonthly.getStartDate());
				faCalendarSetupMonthly.setPeriodIndex(dtoFACalendarSetupMonthly.getPeriodIndex());
				faCalendarSetupMonthly.setFaCalendarSetup(faCalendarSetup);
				faCalendarSetupMonthly.setYear1(dtoFACalendarSetup.getYear1());
				repositoryFACalendarSetupMonthly.saveAndFlush(faCalendarSetupMonthly);
			}
			return dtoFACalendarSetup;
		}catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @return
	 */
	public DtoFACalendarSetup getCalendarMonthlySetup() {
		FACalendarSetup faCalendarSetup = repositoryFACalendarSetup.findByDataTypeAndIsDeleted("MONTHLY",false);
		 
		if (faCalendarSetup != null) {
			
			return new DtoFACalendarSetup(faCalendarSetup);
		}
		return null;
	}


	/**
	 * @param dtoFACalendarSetup
	 * @return
	 */
	public DtoFACalendarSetup calendarQuarterSetup(DtoFACalendarSetup dtoFACalendarSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try{
		FACalendarSetup faCalendarSetup = repositoryFACalendarSetup.findByDataTypeAndIsDeleted("QUARTER",false); 
		if(faCalendarSetup!=null){
			repositoryFACalendarSetupQuarter.deleteAllInBatch();
			dtoFACalendarSetup.setMessageType(MessageLabel.CALENDAR_SETUP_UPDATED_SUCCESSFULLY);
		}else{
			faCalendarSetup = new FACalendarSetup();
			dtoFACalendarSetup.setMessageType(MessageLabel.CALENDAR_SETUP_SAVED_SUCCESSFULLY);
		}
			faCalendarSetup.setCalendarDescription(dtoFACalendarSetup.getCalendarDescription());
			faCalendarSetup.setCalendarDescriptionArabic(dtoFACalendarSetup.getCalendarDescriptionArabic());
			faCalendarSetup.setYear1(dtoFACalendarSetup.getYear1());
			faCalendarSetup.setCalendarId(dtoFACalendarSetup.getCalendarId());
			faCalendarSetup.setCreatedBy(loggedInUserId);
			faCalendarSetup.setDataType("QUARTER");
			faCalendarSetup = repositoryFACalendarSetup.saveAndFlush(faCalendarSetup);
			dtoFACalendarSetup.setCalendarIndex(faCalendarSetup.getCalendarIndex());
			for(DtoFACalendarSetupQuarter dtoFACalendarSetupQuarter : dtoFACalendarSetup.getQuarterCalendar()){
				FACalendarSetupQuarter faCalendarSetupQuarter = new FACalendarSetupQuarter();
				faCalendarSetupQuarter.setCreatedBy(loggedInUserId);
				faCalendarSetupQuarter.setEndDate(dtoFACalendarSetupQuarter.getEndDate());
				faCalendarSetupQuarter.setStartDate(dtoFACalendarSetupQuarter.getStartDate());
				faCalendarSetupQuarter.setQuarterIndex(dtoFACalendarSetupQuarter.getQuarterIndex());
				faCalendarSetupQuarter.setFaCalendarSetup(faCalendarSetup);
				faCalendarSetupQuarter.setYear1(dtoFACalendarSetup.getYear1());
				faCalendarSetupQuarter.setMidDate(dtoFACalendarSetupQuarter.getMidDate());
				repositoryFACalendarSetupQuarter.saveAndFlush(faCalendarSetupQuarter);
			}
			return dtoFACalendarSetup;
		 
		}catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @return
	 */
	public DtoFACalendarSetup getCalendarQuarterSetup() {
		FACalendarSetup faCalendarSetup = repositoryFACalendarSetup.findByDataTypeAndIsDeleted("QUARTER",false);
	 
		if (faCalendarSetup != null) {
			return new DtoFACalendarSetup(faCalendarSetup);
		}
		return null;
	}

	/**
	 * @param dtoFAStructureSetup
	 * @return
	 */
	public DtoFAStructureSetup structureSetup(DtoFAStructureSetup dtoFAStructureSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try{
		FAStructureSetup faStructureSetup = repositoryFAStructureSetup.findByStructureIdAndIsDeleted(dtoFAStructureSetup.getStructureId(),false); 
		if(faStructureSetup!=null){
			dtoFAStructureSetup.setMessageType("STRUCTURE_SETUP_ALREADY_EXIST");
			return dtoFAStructureSetup;
		}else{
			faStructureSetup = new FAStructureSetup();
			faStructureSetup.setStructureDescription(dtoFAStructureSetup.getStructureDescription());
			faStructureSetup.setStructureDescriptionArabic(dtoFAStructureSetup.getStructureDescriptionArabic());
			faStructureSetup.setStructureId(dtoFAStructureSetup.getStructureId());
			faStructureSetup.setCreatedBy(loggedInUserId);
			faStructureSetup = repositoryFAStructureSetup.saveAndFlush(faStructureSetup);
			dtoFAStructureSetup.setStructureIndex(faStructureSetup.getStructureIndex());
			return dtoFAStructureSetup;
		}
		
		}catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoFAStructureSetup
	 * @return
	 */
	public DtoFAStructureSetup updateCalendarQuarterSetup(DtoFAStructureSetup dtoFAStructureSetup) {

		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try{
			FAStructureSetup faStructureSetup  = repositoryFAStructureSetup.findByStructureIndexAndIsDeleted(dtoFAStructureSetup.getStructureIndex(),false); 
		if(faStructureSetup!=null){
			faStructureSetup.setStructureDescription(dtoFAStructureSetup.getStructureDescription());
			faStructureSetup.setStructureDescriptionArabic(dtoFAStructureSetup.getStructureDescriptionArabic());
			faStructureSetup.setStructureId(dtoFAStructureSetup.getStructureId());
			faStructureSetup.setChangeBy(loggedInUserId);
			faStructureSetup.setModifyDate(new Date());
			faStructureSetup = repositoryFAStructureSetup.saveAndFlush(faStructureSetup);
			dtoFAStructureSetup.setStructureIndex(faStructureSetup.getStructureIndex());
			return dtoFAStructureSetup;
		}else{
			dtoFAStructureSetup.setMessageType("RECORD_NOT_FOUND");
			return dtoFAStructureSetup;
		}
		
		}catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoFAStructureSetup
	 * @return
	 */
	public DtoFAStructureSetup getStructureSetupById(DtoFAStructureSetup dtoFAStructureSetup) {
		FAStructureSetup faStructureSetup  = repositoryFAStructureSetup.findByStructureIndexAndIsDeleted(dtoFAStructureSetup.getStructureIndex(),false); 
		if (faStructureSetup != null) {
			return new DtoFAStructureSetup(faStructureSetup);
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchStructureSetup(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
	 	dtoSearch.setTotalCount(repositoryFAStructureSetup.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		List<FAStructureSetup> faStructureSetupList = null;
		List<DtoFAStructureSetup> dtoFAStructureSetupList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			faStructureSetupList = repositoryFAStructureSetup
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			faStructureSetupList = repositoryFAStructureSetup
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (faStructureSetupList != null) {
			for (FAStructureSetup faStructureSetup : faStructureSetupList) {
				dtoFAStructureSetupList.add(new DtoFAStructureSetup(faStructureSetup));
			}
		}
		dtoSearch.setRecords(dtoFAStructureSetupList);
		return dtoSearch;
	
	}

	/**
	 * @param dtoLocationSetup
	 * @return
	 */
	public DtoLocationSetup saveLocationSetup(DtoLocationSetup dtoLocationSetup) {
		FALocationSetup faLocationSetup = null;
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		try {
			faLocationSetup = new FALocationSetup();
			
			faLocationSetup.setLocationId(dtoLocationSetup.getLocationId());
			faLocationSetup.setCreatedBy(loggedInUserId);
			faLocationSetup.setStateMaster(repositoryStateMaster.findOne(dtoLocationSetup.getStateId()));
			faLocationSetup.setCityMaster(repositoryCityMaster.findOne(dtoLocationSetup.getCityId()));
			faLocationSetup.setCountryMaster(repositoryCountryMaster.findOne(dtoLocationSetup.getCountryId()));
			faLocationSetup.setDistrictName(dtoLocationSetup.getState());
			faLocationSetup.setCityName(dtoLocationSetup.getCity());
			faLocationSetup.setCountryName(dtoLocationSetup.getCountry());
			
			faLocationSetup = repositoryFALocationSetup.saveAndFlush(faLocationSetup);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return new DtoLocationSetup(faLocationSetup);
	}

	/**
	 * @param dtoLocationSetup
	 * @param faLocationSetup
	 * @return
	 */
	public DtoLocationSetup updateLocationSetup(DtoLocationSetup dtoLocationSetup, FALocationSetup faLocationSetup) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			
			faLocationSetup.setLocationId(dtoLocationSetup.getLocationId());
			faLocationSetup.setChangeBy(loggedInUserId);
			faLocationSetup.setStateMaster(repositoryStateMaster.findOne(dtoLocationSetup.getStateId()));
			faLocationSetup.setCityMaster(repositoryCityMaster.findOne(dtoLocationSetup.getCityId()));
			faLocationSetup.setCountryMaster(repositoryCountryMaster.findOne(dtoLocationSetup.getCountryId()));
			faLocationSetup.setDistrictName(dtoLocationSetup.getState());
			faLocationSetup.setCityName(dtoLocationSetup.getCity());
			faLocationSetup.setCountryName(dtoLocationSetup.getCountry());
			
			faLocationSetup = repositoryFALocationSetup.saveAndFlush(faLocationSetup);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return new DtoLocationSetup(faLocationSetup);
	}

	/**
	 * @param faLocationSetup
	 * @return
	 */
	public DtoLocationSetup getFALocationSetupById(FALocationSetup faLocationSetup) {
		return new DtoLocationSetup(faLocationSetup);
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchFALocationSetup(DtoSearch dtoSearch) {
		List<DtoLocationSetup> dtoLocationSetups = new ArrayList<>();
		DtoLocationSetup dtoLocationSetup;
		List<FALocationSetup> list;
		try {
			// check if the request is for search or not else return all the
			// records based upon the pagination
			List<FALocationSetup> listSize = repositoryFALocationSetup.findAll();
			if(!listSize.isEmpty()){
				dtoSearch.setTotalCount(listSize.size());
			}
			if (dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null) 
			{
				Pageable pageable  = new PageRequest(dtoSearch.getPageNumber(),
						dtoSearch.getPageSize(), Direction.DESC, "createdDate");
				list = repositoryFALocationSetup.predictiveFALocationSearchWithPagination(
						dtoSearch.getSearchKeyword(), pageable);
			} else 
			{
				list = repositoryFALocationSetup.predictiveFALocationSearch(
						dtoSearch.getSearchKeyword());
			}
			if (!list.isEmpty()) {
				for (FALocationSetup salesTerritoryMaintenance : list) {
					dtoLocationSetup = new DtoLocationSetup(salesTerritoryMaintenance);
					dtoLocationSetups.add(dtoLocationSetup);
				}
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		dtoSearch.setRecords(dtoLocationSetups);
		return dtoSearch ;
	}

	/**
	 * @param dtoPhysicalLocationSetup
	 * @return
	 */
	public DtoPhysicalLocationSetup savePhysicalLocationSetup(DtoPhysicalLocationSetup dtoPhysicalLocationSetup) {
		FAPhysicalLocationSetup faPhysicalLocationSetup = null;
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));		
		try {
			faPhysicalLocationSetup = new FAPhysicalLocationSetup();
			faPhysicalLocationSetup.setCreatedBy(loggedInUserId);
			faPhysicalLocationSetup.setPhysicalLocationId(dtoPhysicalLocationSetup.getPhysicalLocationId());
			faPhysicalLocationSetup.setFaLocationSetup(repositoryFALocationSetup.findOne(dtoPhysicalLocationSetup.getLocationIndex()));
			faPhysicalLocationSetup.setPhysicalLocationDescription(dtoPhysicalLocationSetup.getDescriptionPrimary());
			faPhysicalLocationSetup.setPhysicalLocationDescriptionArabic(dtoPhysicalLocationSetup.getDescriptionSecondary());
			
			faPhysicalLocationSetup = repositoryFAPhysicalLocationSetup.saveAndFlush(faPhysicalLocationSetup);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		return new DtoPhysicalLocationSetup(faPhysicalLocationSetup);
	}

	/**
	 * @param dtoPhysicalLocationSetup
	 * @param faPhysicalLocationSetup
	 * @return
	 */
	public DtoPhysicalLocationSetup updatePhysicalLocationSetup(DtoPhysicalLocationSetup dtoPhysicalLocationSetup,
			FAPhysicalLocationSetup faPhysicalLocationSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));		
		try {
			faPhysicalLocationSetup.setPhysicalLocationId(dtoPhysicalLocationSetup.getPhysicalLocationId());
			faPhysicalLocationSetup.setFaLocationSetup(repositoryFALocationSetup.findOne(dtoPhysicalLocationSetup.getLocationIndex()));
			faPhysicalLocationSetup.setPhysicalLocationDescription(dtoPhysicalLocationSetup.getDescriptionPrimary());
			faPhysicalLocationSetup.setPhysicalLocationDescriptionArabic(dtoPhysicalLocationSetup.getDescriptionSecondary());
			faPhysicalLocationSetup.setChangeBy(loggedInUserId);
			faPhysicalLocationSetup = repositoryFAPhysicalLocationSetup.saveAndFlush(faPhysicalLocationSetup);
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		return new DtoPhysicalLocationSetup(faPhysicalLocationSetup);
	}

	/**
	 * @param faPhysicalLocationSetup
	 * @return
	 */
	public DtoPhysicalLocationSetup getPhysicalLocationSetupById(FAPhysicalLocationSetup faPhysicalLocationSetup) {
		return new DtoPhysicalLocationSetup(faPhysicalLocationSetup);
	}

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchFAPhysicalLocationSetup(DtoSearch dtoSearch) {
		List<DtoPhysicalLocationSetup> dtoPhysicalLocationSetups = new ArrayList<>();
		DtoPhysicalLocationSetup dtoPhysicalLocationSetup;
		List<FAPhysicalLocationSetup> list;
		try {
			// check if the request is for search or not else return all the
			// records based upon the pagination
			List<FAPhysicalLocationSetup> listSize = repositoryFAPhysicalLocationSetup.findAll();
			if(!listSize.isEmpty()){
				dtoSearch.setTotalCount(listSize.size());
			}
			if (dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null) 
			{
				Pageable pageable  = new PageRequest(dtoSearch.getPageNumber(),
						dtoSearch.getPageSize(), Direction.DESC, "createdDate");
				list = repositoryFAPhysicalLocationSetup.predictiveFAPhysicalLocationSearchWithPagination(
						dtoSearch.getSearchKeyword(), pageable);
			} else 
			{
				list = repositoryFAPhysicalLocationSetup.predictiveFAPhysicalLocationSearch(
						dtoSearch.getSearchKeyword());
			}
			if (!list.isEmpty()) {
				for (FAPhysicalLocationSetup faPhysicalLocationSetup : list) {
					dtoPhysicalLocationSetup = new DtoPhysicalLocationSetup(faPhysicalLocationSetup);
					dtoPhysicalLocationSetups.add(dtoPhysicalLocationSetup);
				}
			}
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		dtoSearch.setRecords(dtoPhysicalLocationSetups);
		return dtoSearch ;
	}
	
	/**
	 * @param dtoFAGeneralMaintenance
	 * @return
	 */
	public DtoFAGeneralMaintenance saveFAGeneralMaintenance(DtoFAGeneralMaintenance dtoFAGeneralMaintenance) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langId=serviceHome.getLanngugaeId();
		FAGeneralMaintenance faGeneralMaintenance=null;
		try
		{
			if(dtoFAGeneralMaintenance.getAssetIdSerial()>0)
			{
				for(int i=1;i<=dtoFAGeneralMaintenance.getAssetIdSerial();i++)
				{
					faGeneralMaintenance= new FAGeneralMaintenance();
					String assetId=dtoFAGeneralMaintenance.getAssetId();
					String assetDesc=dtoFAGeneralMaintenance.getFaDescription();
					String assetDescArabic=dtoFAGeneralMaintenance.getFaDescriptionArabic();
					if(dtoFAGeneralMaintenance.getAssetIdSerial()>1)
					{
						assetId=dtoFAGeneralMaintenance.getAssetId()+"-"+i;
						assetDesc=dtoFAGeneralMaintenance.getFaDescription()+"-"+i;
						assetDescArabic=dtoFAGeneralMaintenance.getFaDescriptionArabic()+"-"+i;
					}
					//faGeneralMaintenance.setAssetIdSerial(dtoFAGeneralMaintenance.getAssetIdSerial());
					faGeneralMaintenance.setAssetId(assetId);
					faGeneralMaintenance.setFADescription(assetDesc);
					faGeneralMaintenance.setFADescriptionArabic(assetDescArabic);
					faGeneralMaintenance.setAssetExtendedDescription(dtoFAGeneralMaintenance.getAssetExtendedDescription());
					faGeneralMaintenance.setFAShortName(dtoFAGeneralMaintenance.getFaShortName());
					faGeneralMaintenance.setFaClassSetup(repositoryFAClassSetup.findByClassIdAndIsDeleted(dtoFAGeneralMaintenance.getFaClassSetupId(),false));
					faGeneralMaintenance.setFaMasterAssetId(dtoFAGeneralMaintenance.getFaMasterAssetId());
					faGeneralMaintenance.setFaAccountGroupsSetup(repositoryFAAccountGroupSetup.findByFaAccountGroupIndexAndIsDeleted(dtoFAGeneralMaintenance.getFaAccountGroupIndex(),false));
					faGeneralMaintenance.setMasterGeneralMaintenanceAssetType(repositoryMasterGeneralMaintenanceAssetType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFAGeneralMaintenance.getAssetType(),langId,false));
					faGeneralMaintenance.setFAAcquisitionDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFAGeneralMaintenance.getFaAcquisitionDate()));
					faGeneralMaintenance.setFaPropertyTypes(repositoryFAPropertyTypes.findByPropertyTypeIdAndIsDeleted(dtoFAGeneralMaintenance.getFaPropertyTypes(),false));
					faGeneralMaintenance.setFAAcquisitionCost(Double.valueOf(dtoFAGeneralMaintenance.getFaAcquisitionCost()));
					faGeneralMaintenance.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoFAGeneralMaintenance.getCurrencyId(),false));
					faGeneralMaintenance.setAssetLabelId(dtoFAGeneralMaintenance.getAssetLabelId());
					faGeneralMaintenance.setFaLocationSetup(repositoryFALocationSetup.findByLocationIdAndIsDeleted(dtoFAGeneralMaintenance.getFaLocationId(),false));
					faGeneralMaintenance.setFaPhysicalLocationSetup(repositoryFAPhysicalLocationSetup.findByPhysicalLocationIdAndIsDeleted(dtoFAGeneralMaintenance.getFaPhysicalLocationId(),false));
					faGeneralMaintenance.setAssetQuantity(dtoFAGeneralMaintenance.getAssetQuantity());
					if(UtilRandomKey.isNotBlank(dtoFAGeneralMaintenance.getLastMaintenance())){
						faGeneralMaintenance.setLastMaintenance(UtilDateAndTime.ddmmyyyyStringToDate(dtoFAGeneralMaintenance.getLastMaintenance()));
					}
					faGeneralMaintenance.setFAAddedDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFAGeneralMaintenance.getFaAddedDate()));
					faGeneralMaintenance.setFaStructureSetup(repositoryFAStructureSetup.findByStructureIdAndIsDeleted(dtoFAGeneralMaintenance.getFaStructureId(),false));
					faGeneralMaintenance.setEmployeeId(dtoFAGeneralMaintenance.getEmployeeId());
					faGeneralMaintenance.setManufacturerName(dtoFAGeneralMaintenance.getManufacturerName());
					faGeneralMaintenance.setMasterGeneralMaintenanceAssetStatus(repositoryMasterGeneralMaintenanceAssetStatus.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFAGeneralMaintenance.getAssetStatus(),langId,false));
					faGeneralMaintenance.setCreatedBy(loggedInUserId);
					faGeneralMaintenance=repositoryFAGeneralMaintenance.save(faGeneralMaintenance);
					faGeneralMaintenance=repositoryFAGeneralMaintenance.findByAssetIdAndIsDeleted(faGeneralMaintenance.getAssetId(),false);
				}
			}
		}
		catch (Exception e) {
			dtoFAGeneralMaintenance.setMessageType("INTERNAL_SERVER_ERROR");
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if(faGeneralMaintenance !=null && dtoFAGeneralMaintenance.getMessageType()==null){
			MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus =faGeneralMaintenance.getMasterGeneralMaintenanceAssetStatus();
			if(masterGeneralMaintenanceAssetStatus!=null){
				masterGeneralMaintenanceAssetStatus =repositoryMasterGeneralMaintenanceAssetStatus.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetStatus.getTypeId(),langId,false);
			}
			MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType =faGeneralMaintenance.getMasterGeneralMaintenanceAssetType();
			if(masterGeneralMaintenanceAssetType!=null){
				masterGeneralMaintenanceAssetType =repositoryMasterGeneralMaintenanceAssetType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetType.getTypeId(),langId,false);
			}
			dtoFAGeneralMaintenance= new DtoFAGeneralMaintenance(faGeneralMaintenance,masterGeneralMaintenanceAssetStatus,masterGeneralMaintenanceAssetType);
		}
		return dtoFAGeneralMaintenance;
	}
	
	/**
	 * @param dtoFAGeneralMaintenance
	 * @param faGeneralMaintenance
	 * @return
	 */
	public DtoFAGeneralMaintenance updateFAGeneralMaintenance(DtoFAGeneralMaintenance dtoFAGeneralMaintenance,FAGeneralMaintenance faGeneralMaintenance) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langId=serviceHome.getLanngugaeId();
		try
		{
			faGeneralMaintenance.setFADescription(dtoFAGeneralMaintenance.getFaDescription());
			faGeneralMaintenance.setFADescriptionArabic(dtoFAGeneralMaintenance.getFaDescriptionArabic());
			faGeneralMaintenance.setAssetExtendedDescription(dtoFAGeneralMaintenance.getAssetExtendedDescription());
			faGeneralMaintenance.setFAShortName(dtoFAGeneralMaintenance.getFaShortName());
			faGeneralMaintenance.setFaClassSetup(repositoryFAClassSetup.findByClassIdAndIsDeleted(dtoFAGeneralMaintenance.getFaClassSetupId(),false));
			faGeneralMaintenance.setFaMasterAssetId(dtoFAGeneralMaintenance.getFaMasterAssetId());
			faGeneralMaintenance.setFaAccountGroupsSetup(repositoryFAAccountGroupSetup.findByFaAccountGroupIndexAndIsDeleted(dtoFAGeneralMaintenance.getFaAccountGroupIndex(),false));
			faGeneralMaintenance.setMasterGeneralMaintenanceAssetType(repositoryMasterGeneralMaintenanceAssetType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFAGeneralMaintenance.getAssetType(),langId,false));
			faGeneralMaintenance.setFAAcquisitionDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFAGeneralMaintenance.getFaAcquisitionDate()));
			faGeneralMaintenance.setFaPropertyTypes(repositoryFAPropertyTypes.findByPropertyTypeIdAndIsDeleted(dtoFAGeneralMaintenance.getFaPropertyTypes(),false));
			faGeneralMaintenance.setFAAcquisitionCost(Double.valueOf(dtoFAGeneralMaintenance.getFaAcquisitionCost()));
			faGeneralMaintenance.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(dtoFAGeneralMaintenance.getCurrencyId(),false));
			faGeneralMaintenance.setAssetLabelId(dtoFAGeneralMaintenance.getAssetLabelId());
			faGeneralMaintenance.setFaLocationSetup(repositoryFALocationSetup.findByLocationIdAndIsDeleted(dtoFAGeneralMaintenance.getFaLocationId(),false));
			faGeneralMaintenance.setFaPhysicalLocationSetup(repositoryFAPhysicalLocationSetup.findByPhysicalLocationIdAndIsDeleted(dtoFAGeneralMaintenance.getFaPhysicalLocationId(),false));
			faGeneralMaintenance.setAssetQuantity(dtoFAGeneralMaintenance.getAssetQuantity());
			if(UtilRandomKey.isNotBlank(dtoFAGeneralMaintenance.getLastMaintenance())){
				faGeneralMaintenance.setLastMaintenance(UtilDateAndTime.ddmmyyyyStringToDate(dtoFAGeneralMaintenance.getLastMaintenance()));
			}
			else{
				faGeneralMaintenance.setLastMaintenance(null);
			}
			
			faGeneralMaintenance.setFAAddedDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoFAGeneralMaintenance.getFaAddedDate()));
			faGeneralMaintenance.setFaStructureSetup(repositoryFAStructureSetup.findByStructureIdAndIsDeleted(dtoFAGeneralMaintenance.getFaStructureId(),false));
			faGeneralMaintenance.setEmployeeId(dtoFAGeneralMaintenance.getEmployeeId());
			faGeneralMaintenance.setManufacturerName(dtoFAGeneralMaintenance.getManufacturerName());
			faGeneralMaintenance.setMasterGeneralMaintenanceAssetStatus(repositoryMasterGeneralMaintenanceAssetStatus.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFAGeneralMaintenance.getAssetStatus(),langId,false));
			faGeneralMaintenance.setChangeBy(loggedInUserId);
			faGeneralMaintenance=repositoryFAGeneralMaintenance.save(faGeneralMaintenance);
			faGeneralMaintenance=repositoryFAGeneralMaintenance.findByAssetIdAndIsDeleted(faGeneralMaintenance.getAssetId(),false);
		}
		catch (Exception e) {
			dtoFAGeneralMaintenance.setMessageType("INTERNAL_SERVER_ERROR");
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if(faGeneralMaintenance !=null && dtoFAGeneralMaintenance.getMessageType()==null){
			MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus =faGeneralMaintenance.getMasterGeneralMaintenanceAssetStatus();
			if(masterGeneralMaintenanceAssetStatus!=null){
				masterGeneralMaintenanceAssetStatus =repositoryMasterGeneralMaintenanceAssetStatus.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetStatus.getTypeId(),langId,false);
			}
			MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType =faGeneralMaintenance.getMasterGeneralMaintenanceAssetType();
			if(masterGeneralMaintenanceAssetType!=null){
				masterGeneralMaintenanceAssetType =repositoryMasterGeneralMaintenanceAssetType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetType.getTypeId(),langId,false);
			}
			dtoFAGeneralMaintenance= new DtoFAGeneralMaintenance(faGeneralMaintenance,masterGeneralMaintenanceAssetStatus,masterGeneralMaintenanceAssetType);
		}
		return dtoFAGeneralMaintenance;
	}
	
	/**
	 * @param dtoFAGeneralMaintenance
	 * @param faGeneralMaintenance
	 * @return
	 */
	public DtoFAGeneralMaintenance getFAGeneralMaintenanceByAssetId(DtoFAGeneralMaintenance dtoFAGeneralMaintenance,FAGeneralMaintenance faGeneralMaintenance) {
		try
		{
			int langId=serviceHome.getLanngugaeId();
			MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus =faGeneralMaintenance.getMasterGeneralMaintenanceAssetStatus();
			if(masterGeneralMaintenanceAssetStatus!=null){
				masterGeneralMaintenanceAssetStatus =repositoryMasterGeneralMaintenanceAssetStatus.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetStatus.getTypeId(),langId,false);
			}
			MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType =faGeneralMaintenance.getMasterGeneralMaintenanceAssetType();
			if(masterGeneralMaintenanceAssetType!=null){
				masterGeneralMaintenanceAssetType =repositoryMasterGeneralMaintenanceAssetType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetType.getTypeId(),langId,false);
			}
			dtoFAGeneralMaintenance= new DtoFAGeneralMaintenance(faGeneralMaintenance,masterGeneralMaintenanceAssetStatus,masterGeneralMaintenanceAssetType);
		}
		catch (Exception e) {
			dtoFAGeneralMaintenance.setMessageType("INTERNAL_SERVER_ERROR");
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		
		return dtoFAGeneralMaintenance;
	}
	
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchFAGeneralMaintenance(DtoSearch dtoSearchs) 
	{
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryFAGeneralMaintenance.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		DtoFAGeneralMaintenance dtoFAGeneralMaintenance = null;
		List<FAGeneralMaintenance> faGeneralMaintenances = null;
		List<DtoFAGeneralMaintenance> dtoFAGeneralMaintenancesList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			faGeneralMaintenances = repositoryFAGeneralMaintenance
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			faGeneralMaintenances = repositoryFAGeneralMaintenance
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (faGeneralMaintenances != null) {
			for (FAGeneralMaintenance faGeneralMaintenance : faGeneralMaintenances) {
				int langId=serviceHome.getLanngugaeId();
				MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus =faGeneralMaintenance.getMasterGeneralMaintenanceAssetStatus();
				if(masterGeneralMaintenanceAssetStatus!=null){
					masterGeneralMaintenanceAssetStatus =repositoryMasterGeneralMaintenanceAssetStatus.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetStatus.getTypeId(),langId,false);
				}
				MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType =faGeneralMaintenance.getMasterGeneralMaintenanceAssetType();
				if(masterGeneralMaintenanceAssetType!=null){
					masterGeneralMaintenanceAssetType =repositoryMasterGeneralMaintenanceAssetType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetType.getTypeId(),langId,false);
				}
				dtoFAGeneralMaintenance = new DtoFAGeneralMaintenance(faGeneralMaintenance,masterGeneralMaintenanceAssetStatus,masterGeneralMaintenanceAssetType);
				dtoFAGeneralMaintenancesList.add(dtoFAGeneralMaintenance);
			}
		}
		dtoSearch.setRecords(dtoFAGeneralMaintenancesList);
		return dtoSearch;
	}

	/**
	 * @param dtoLeaseCompanySetup
	 * @param faLeaseCompanySetup
	 * @return
	 */
	public DtoLeaseCompanySetup saveLeaseCompanySetup(DtoLeaseCompanySetup dtoLeaseCompanySetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		VendorMaintenance maintenance = repositoryVendorMaintenance.findOne(dtoLeaseCompanySetup.getVendorId());
		FALeaseCompanySetup faLeaseCompanySetup = new FALeaseCompanySetup();
		try {
			
				if (faLeaseCompanySetup.getCreatedBy() == 0) {
					faLeaseCompanySetup.setCreatedBy(loggedInUserId);
				} else {
					faLeaseCompanySetup.setChangeBy(loggedInUserId);
				}
				faLeaseCompanySetup.setCompanyId(dtoLeaseCompanySetup.getCompanyId());
				faLeaseCompanySetup.setCompanyName(dtoLeaseCompanySetup.getCompanyName());
				if(maintenance!=null)
				{
					faLeaseCompanySetup.setVendorMaintenance(maintenance);
					faLeaseCompanySetup.setVendorName(maintenance.getVendorName());
				}
				faLeaseCompanySetup=repositoryFALeaseCompanySetup.saveAndFlush(faLeaseCompanySetup);

		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}

		return new DtoLeaseCompanySetup(faLeaseCompanySetup);
	}

	/**
	 * @param faLeaseCompanySetup
	 * @return
	 */
	public DtoLeaseCompanySetup getLeaseCompanySetup(FALeaseCompanySetup faLeaseCompanySetup) {
		return new DtoLeaseCompanySetup(faLeaseCompanySetup);
	}

	/**
	 * @param dtoFABookClassSetup
	 * @return
	 */
	public DtoFABookClassSetup saveFABookClassSetup(DtoFABookClassSetup dtoFABookClassSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid = serviceHome.getLanngugaeId();
		try {
			  FABookClassSetup faBookClassSetup=repositoryFABookClassSetup.findByBookSetupBookIdAndIsDeleted(dtoFABookClassSetup.getBookId(), false);
			  if(faBookClassSetup==null){
				  faBookClassSetup = new FABookClassSetup();
			  }
			  faBookClassSetup.setFaBookMaintenanceAmortizationCodeType(repositoryFABookMaintenanceAmortizationCodeType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId(dtoFABookClassSetup.getAmortizationCode(),false,langid));
			  faBookClassSetup.setAmortizationAmount(dtoFABookClassSetup.getAmortizationAmount());
			  faBookClassSetup.setAmortizationPercentage(dtoFABookClassSetup.getAmortizationPercentage());
			  faBookClassSetup.setAveragingConventionType(repositoryFABookMaintenanceAveragingConventionType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFABookClassSetup.getAveragingConvention(),langid,false));
			  faBookClassSetup.setCreatedBy(loggedInUserId);
			  faBookClassSetup.setFaClassSetup(repositoryFAClassSetup.findByClassIdAndIsDeleted(dtoFABookClassSetup.getClassId(),false));
			  faBookClassSetup.setFaDepreciationMethods(repositoryFADepreciationMethods.findByDepreciationMethodIdAndIsDeleted(dtoFABookClassSetup.getDepreciationMethodId(),false));
			  faBookClassSetup.setInitialAllowancePercentage(dtoFABookClassSetup.getInitialAllowancePercentage());
			  faBookClassSetup.setOrigLifeDays(dtoFABookClassSetup.getOrigLifeDays());
			  faBookClassSetup.setOrigLifeYears(dtoFABookClassSetup.getOrigLifeYears());
			  faBookClassSetup.setSalvageEstimate(dtoFABookClassSetup.getSalvageEstimate());
			  faBookClassSetup.setSalvagePercentage(dtoFABookClassSetup.getSalvagePercentage());
			  faBookClassSetup.setSpecialDepreciationAllowance(dtoFABookClassSetup.getDepreciationMethodId());
			  faBookClassSetup.setSpecialDepreciationAllowancePercent(dtoFABookClassSetup.getSpecialDepreciationAllowancePercent());
			  faBookClassSetup.setBookSetup(repositoryBookSetup.findByBookIdAndIsDeleted(dtoFABookClassSetup.getBookId(),false));
			  faBookClassSetup.setSwitchOverType(repositoryFABookMaintenanceSwitchOverType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFABookClassSetup.getSwtch(),langid,false));
			  faBookClassSetup.setCreatedDate(new Date());
			  faBookClassSetup= repositoryFABookClassSetup.saveAndFlush(faBookClassSetup);
			  return new DtoFABookClassSetup(faBookClassSetup);

		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}

		
		return null;
	}

	/**
	 * @param dtoFABookClassSetup
	 * @return
	 */
	public DtoFABookClassSetup getFABookClassSetupById(DtoFABookClassSetup dtoFABookClassSetup) {
		try {
			FABookClassSetup faBookClassSetup = repositoryFABookClassSetup
					.findByBookIndexAndIsDeleted(dtoFABookClassSetup.getBookIndex(),false);
			if (faBookClassSetup != null) {
				return new DtoFABookClassSetup(faBookClassSetup);
			}
		}catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoFABookClassSetup
	 * @return
	 */
	public DtoFABookClassSetup updateFABookClassSetup(DtoFABookClassSetup dtoFABookClassSetup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid = serviceHome.getLanngugaeId();
		try {
			  FABookClassSetup faBookClassSetup = repositoryFABookClassSetup.findByBookIndexAndIsDeleted(dtoFABookClassSetup.getBookIndex(),false);
			  if(faBookClassSetup ==null){
				  return null;
			  }
			  faBookClassSetup.setFaBookMaintenanceAmortizationCodeType(repositoryFABookMaintenanceAmortizationCodeType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId(dtoFABookClassSetup.getAmortizationCode(),false,langid));
			  faBookClassSetup.setAmortizationAmount(dtoFABookClassSetup.getAmortizationAmount());
			  faBookClassSetup.setAmortizationPercentage(dtoFABookClassSetup.getAmortizationPercentage());
			  faBookClassSetup.setAveragingConventionType(repositoryFABookMaintenanceAveragingConventionType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFABookClassSetup.getAveragingConvention(),langid,false));
			  faBookClassSetup.setCreatedBy(loggedInUserId);
			  faBookClassSetup.setFaClassSetup(repositoryFAClassSetup.findByClassIdAndIsDeleted(dtoFABookClassSetup.getClassId(),false));
			  faBookClassSetup.setFaDepreciationMethods(repositoryFADepreciationMethods.findByDepreciationMethodIdAndIsDeleted(dtoFABookClassSetup.getDepreciationMethodId(),false));
			  faBookClassSetup.setInitialAllowancePercentage(dtoFABookClassSetup.getInitialAllowancePercentage());
			  faBookClassSetup.setOrigLifeDays(dtoFABookClassSetup.getOrigLifeDays());
			  faBookClassSetup.setOrigLifeYears(dtoFABookClassSetup.getOrigLifeYears());
			  faBookClassSetup.setSalvageEstimate(dtoFABookClassSetup.getSalvageEstimate());
			  faBookClassSetup.setSalvagePercentage(dtoFABookClassSetup.getSalvagePercentage());
			  faBookClassSetup.setSpecialDepreciationAllowance(dtoFABookClassSetup.getSpecialDepreciationAllowance());
			  faBookClassSetup.setSpecialDepreciationAllowancePercent(dtoFABookClassSetup.getSpecialDepreciationAllowancePercent());
			  faBookClassSetup.setBookSetup(repositoryBookSetup.findByBookIdAndIsDeleted(dtoFABookClassSetup.getBookId(),false));
			  faBookClassSetup.setSwitchOverType(repositoryFABookMaintenanceSwitchOverType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoFABookClassSetup.getSwtch(),langid,false));
			  faBookClassSetup.setModifyDate(new Date());
			  faBookClassSetup= repositoryFABookClassSetup.saveAndFlush(faBookClassSetup);
			  return new DtoFABookClassSetup(faBookClassSetup);

		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchFABookClassSetup(DtoSearch dtoSearchs) {

		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryFABookClassSetup.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		List<FABookClassSetup> faBookClassSetupList = null;
		List<DtoFABookClassSetup> dtoFABookClassSetupList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			faBookClassSetupList = repositoryFABookClassSetup
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			faBookClassSetupList = repositoryFABookClassSetup
					.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (faBookClassSetupList != null) {
			for (FABookClassSetup faBookClassSetup : faBookClassSetupList) {
				dtoFABookClassSetupList.add(new DtoFABookClassSetup(faBookClassSetup));
			}
		}
		dtoSearch.setRecords(dtoFABookClassSetupList);
		return dtoSearch;
	
	}

	/**
	 * @param dtoFAClassSetup
	 * @return
	 */
	 public DtoFAClassSetup saveFaClassSetup(DtoFAClassSetup dtoFAClassSetup)
     {
           FAClassSetup faClassSetup=null;
           try
           {
                int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
                faClassSetup= new FAClassSetup();
                faClassSetup.setClassId(dtoFAClassSetup.getClassId());
                faClassSetup.setClassSetupDescription(dtoFAClassSetup.getDescription());
                faClassSetup.setClassSetupDescriptionArabic(dtoFAClassSetup.getDescriptionArabic());
                faClassSetup.setFaAccountGroupsSetup(repositoryFAAccountGroupSetup.findByFaAccountGroupIndexAndIsDeleted(dtoFAClassSetup.getFaAccountGroupIndex(),false));
                faClassSetup.setFaInsuranceClassSetup(repositoryFAInsuranceClassSetup.findByInsuranceClassIndexAndIsDeleted(dtoFAClassSetup.getFaInsuranceClassIndexId(),false));
                faClassSetup.setCreatedBy(loggedInUserId);
                faClassSetup=repositoryFAClassSetup.save(faClassSetup);
                faClassSetup=repositoryFAClassSetup.findByClassIdAndIsDeleted(dtoFAClassSetup.getClassId(),false);
           }
           catch(Exception e)
           {
                dtoFAClassSetup.setMessageType("INTERNAL_SERVER_ERROR");
                LOG.info(Arrays.toString(e.getStackTrace()));
           }
     
           if(faClassSetup!=null && dtoFAClassSetup.getMessageType()==null){
                dtoFAClassSetup= new DtoFAClassSetup(faClassSetup);
           }
         return dtoFAClassSetup;
      }
     
     /**
     * @param dtoFAClassSetup
     * @param faClassSetup
     * @return
     */
    public DtoFAClassSetup updateFaClassSetup(DtoFAClassSetup dtoFAClassSetup,FAClassSetup faClassSetup)
    {
           try
           {
                int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
                faClassSetup.setClassSetupDescription(dtoFAClassSetup.getDescription());
                faClassSetup.setClassSetupDescriptionArabic(dtoFAClassSetup.getDescriptionArabic());
                faClassSetup.setFaAccountGroupsSetup(repositoryFAAccountGroupSetup.findByFaAccountGroupIndexAndIsDeleted(dtoFAClassSetup.getFaAccountGroupIndex(),false));
                faClassSetup.setFaInsuranceClassSetup(repositoryFAInsuranceClassSetup.findByInsuranceClassIndexAndIsDeleted(dtoFAClassSetup.getFaInsuranceClassIndexId(),false));
                faClassSetup.setCreatedBy(loggedInUserId);
                faClassSetup=repositoryFAClassSetup.save(faClassSetup);
                faClassSetup=repositoryFAClassSetup.findByClassIdAndIsDeleted(dtoFAClassSetup.getClassId(),false);
           }
           catch(Exception e)
           {
                dtoFAClassSetup.setMessageType("INTERNAL_SERVER_ERROR");
                LOG.info(Arrays.toString(e.getStackTrace()));
           }
     
           if(faClassSetup!=null && dtoFAClassSetup.getMessageType()==null){
                dtoFAClassSetup= new DtoFAClassSetup(faClassSetup);
           }
          return dtoFAClassSetup;
    }
     /**
     * @param faClassSetup
     * @return
     */
    public DtoFAClassSetup getFaClassSetupByClassId(FAClassSetup faClassSetup)
     {
           DtoFAClassSetup dtoFAClassSetup=null;
           try
           {
                dtoFAClassSetup= new DtoFAClassSetup(faClassSetup);
           }
           catch(Exception e)
           {
                dtoFAClassSetup = new DtoFAClassSetup();
                dtoFAClassSetup.setMessageType(MessageLabel.INTERNAL_SERVER_ERROR);
                LOG.info(Arrays.toString(e.getStackTrace()));
           }
         return dtoFAClassSetup;
     }
     
     /**
     * @param dtoSearchs
     * @return
     */
    public DtoSearch searchFAClassSetup(DtoSearch dtoSearchs) 
     {
           DtoSearch dtoSearch = new DtoSearch();
           dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
           dtoSearch.setPageSize(dtoSearchs.getPageSize());
           dtoSearch.setTotalCount(
                     repositoryFAClassSetup.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
           DtoFAClassSetup dtoFAClassSetup = null;
           List<FAClassSetup> faClassSetups = null;
           List<DtoFAClassSetup> dtoFAClassSetupsList = new ArrayList<>();
           if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
                Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
                           "createdDate");
                faClassSetups = repositoryFAClassSetup
                          .predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
           } else {
                faClassSetups = repositoryFAClassSetup
                           .predictiveSearch(dtoSearchs.getSearchKeyword());
           }
           if (faClassSetups != null) {
                for (FAClassSetup faClassSetup : faClassSetups) {
                     dtoFAClassSetup = new DtoFAClassSetup(faClassSetup);
                     dtoFAClassSetupsList.add(dtoFAClassSetup);
                }
           }
           dtoSearch.setRecords(dtoFAClassSetupsList);
           return dtoSearch;
     }
     

     /**
     * @Description saveInsuranceClassSetup
     * @param dtoInsuranceClass
     * @return
     */
    public DtoInsuranceClass saveInsuranceClassSetup(DtoInsuranceClass dtoInsuranceClass) {
           FAInsuranceClassSetup faInsuranceClassSetup = new FAInsuranceClassSetup();
           int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
           try {
                faInsuranceClassSetup.setInsuranceClassId(dtoInsuranceClass.getInsuranceClassId());
                faInsuranceClassSetup.setCreatedBy(loggedInUserId);
                faInsuranceClassSetup.setDepreciationRate(dtoInsuranceClass.getDepriciationRate());
                faInsuranceClassSetup.setInflationPercent(dtoInsuranceClass.getInflationPercent());
                faInsuranceClassSetup.setInsuranceDescription(dtoInsuranceClass.getInsuranceDescriptionPrimary());
                faInsuranceClassSetup.setInsuranceDescriptionArabic(dtoInsuranceClass.getInsuranceDescriptionSecondary());

                faInsuranceClassSetup = repositoryFAInsuranceClassSetup.save(faInsuranceClassSetup);
           } catch (Exception e) {
        	   LOG.info(Arrays.toString(e.getStackTrace()));
           }

           return new DtoInsuranceClass(faInsuranceClassSetup);
     }

     /**
     * @Description insuranceClassSetupUpdate
     * @param dtoInsuranceClass
     * @param faInsuranceClassSetup
     * @return
     */
    public DtoInsuranceClass insuranceClassSetupUpdate(DtoInsuranceClass dtoInsuranceClass,
                FAInsuranceClassSetup faInsuranceClassSetup) {
           int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
           try {
                faInsuranceClassSetup.setInsuranceClassId(dtoInsuranceClass.getInsuranceClassId());
                faInsuranceClassSetup.setChangeBy(loggedInUserId);
                faInsuranceClassSetup.setDepreciationRate(dtoInsuranceClass.getDepriciationRate());
                faInsuranceClassSetup.setInflationPercent(dtoInsuranceClass.getInflationPercent());
                faInsuranceClassSetup.setInsuranceDescription(dtoInsuranceClass.getInsuranceDescriptionPrimary());
                faInsuranceClassSetup.setInsuranceDescriptionArabic(dtoInsuranceClass.getInsuranceDescriptionSecondary());

                faInsuranceClassSetup = repositoryFAInsuranceClassSetup.save(faInsuranceClassSetup);
           } catch (Exception e) {
        	   LOG.info(Arrays.toString(e.getStackTrace()));
           }

           return new DtoInsuranceClass(faInsuranceClassSetup);
     }

     /**
     * @Description insuranceClassSetupGetById
     * @param faInsuranceClassSetup
     * @return
     */
    public DtoInsuranceClass insuranceClassSetupGetById(FAInsuranceClassSetup faInsuranceClassSetup) {
           return new DtoInsuranceClass(faInsuranceClassSetup);
     }

     /**
     * @Description insuranceClassSetupSearch
     * @param dtoSearchs
     * @return
     */
    public DtoSearch insuranceClassSetupSearch(DtoSearch dtoSearchs) {
           DtoSearch dtoSearch = new DtoSearch();
           dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
           dtoSearch.setPageSize(dtoSearchs.getPageSize());
           dtoSearch.setTotalCount(
                     repositoryFAInsuranceClassSetup.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
           DtoInsuranceClass dtoInsuranceClass = null;
           List<FAInsuranceClassSetup> list = null;
           List<DtoInsuranceClass> list2 = new ArrayList<>();
           if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
                Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
                           "createdDate");
                list = repositoryFAInsuranceClassSetup
                          .predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
           } else {
                list = repositoryFAInsuranceClassSetup
                           .predictiveSearch(dtoSearchs.getSearchKeyword());
           }
           if (list != null) {
                for (FAInsuranceClassSetup faRetirementSetup : list) {
                     dtoInsuranceClass = new DtoInsuranceClass(faRetirementSetup);
                     list2.add(dtoInsuranceClass);
                }
           }
           dtoSearch.setRecords(list2);
           return dtoSearch;
     }
	
	public DtoPayableAccountClassSetup saveFaAccountGroupSetup(DtoPayableAccountClassSetup dtoPayableAccountClassSetup)
	{
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		int langid = serviceHome.getLanngugaeId();
		GLAccountsTableAccumulation glAccountsTableAccumulation = null;
		try {
			if (dtoPayableAccountClassSetup.getAccountNumberList() != null
					&& !dtoPayableAccountClassSetup.getAccountNumberList().isEmpty()) 
			{
					FAAccountGroupsSetup faAccountGroupsSetup = repositoryFAAccountGroupSetup
							.findByfaAccountGroupIdAndIsDeleted(dtoPayableAccountClassSetup.getAccountGroupId(),false);
					if (faAccountGroupsSetup == null) {
						faAccountGroupsSetup = new FAAccountGroupsSetup();
					}
					faAccountGroupsSetup.setDeleted(false);
					faAccountGroupsSetup.setFaAccountGroupDescription(dtoPayableAccountClassSetup.getDescription());
					faAccountGroupsSetup.setFaAccountGroupDescriptionArabic(dtoPayableAccountClassSetup.getDescriptionArabic());
					faAccountGroupsSetup.setFaAccountGroupId(dtoPayableAccountClassSetup.getAccountGroupId());
					faAccountGroupsSetup.setCreatedBy(loggedInUserId);
					faAccountGroupsSetup=repositoryFAAccountGroupSetup.saveAndFlush(faAccountGroupsSetup);
				    repositoryFAAccountGroupAccountTableSetup.deleteByAccountGroupIndex(faAccountGroupsSetup.getFaAccountGroupIndex());
				  
				    for (DtoPayableAccountClassSetup accountNumber : dtoPayableAccountClassSetup.getAccountNumberList()) 
				    {
						StringBuilder sb = new StringBuilder();
						String fullIndex = "";
						
						if (accountNumber.getAccountNumberIndex() != null
								&& !accountNumber.getAccountNumberIndex().isEmpty()) {
							for (Long i : accountNumber.getAccountNumberIndex()) {
								sb.append(String.valueOf(i));
							}
							fullIndex = sb.toString();
						}
					
					List<GLAccountsTableAccumulation> glAccountsTableAccumulationsList = repositoryGLAccountsTableAccumulation
							.findByAccountNumberFullIndexAndIsDeleted(fullIndex,false);
					if (glAccountsTableAccumulationsList != null && !glAccountsTableAccumulationsList.isEmpty()) {
						glAccountsTableAccumulation = glAccountsTableAccumulationsList.get(0);
						if (UtilRandomKey.isNotBlank(accountNumber.getAccountDescription())) {
							glAccountsTableAccumulation
									.setAccountDescription(accountNumber.getAccountDescription());
						}
					} else {
						glAccountsTableAccumulation = new GLAccountsTableAccumulation();
						int actRowIndexId = 1;
						PayableAccountClassSetup payableAccountClassSetup2 = repositoryPayableAccountClassSetup
								.findFirstByOrderByIdDesc();
						if (payableAccountClassSetup2 != null) {
							actRowIndexId = payableAccountClassSetup2.getAccountRowIndex();
							actRowIndexId = actRowIndexId + 1;
						}
						int segmentNumber = 1;
						if (accountNumber.getAccountNumberIndex() != null
								&& !accountNumber.getAccountNumberIndex().isEmpty()) {
							StringBuilder accountNumberfullIndex = new StringBuilder("");
							for (Long indexId : accountNumber.getAccountNumberIndex()) {
								accountNumberfullIndex.append(indexId);
								PayableAccountClassSetup payableAccountClassSetup = new PayableAccountClassSetup();
								payableAccountClassSetup.setIndexId(indexId);
								payableAccountClassSetup.setSegmentNumber(segmentNumber);
								segmentNumber++;
								payableAccountClassSetup.setAccountRowIndex(actRowIndexId);
								payableAccountClassSetup.setCreatedBy(loggedInUserId);
								repositoryPayableAccountClassSetup.save(payableAccountClassSetup);
							}

							glAccountsTableAccumulation.setAccountTableRowIndex(String.valueOf(actRowIndexId));
							glAccountsTableAccumulation.setSegmentNumber(segmentNumber);
							glAccountsTableAccumulation.setAccountNumberFullIndex(accountNumberfullIndex.toString());
							if (UtilRandomKey.isNotBlank(accountNumber.getAccountDescription())) {
								glAccountsTableAccumulation
										.setAccountDescription(accountNumber.getAccountDescription());
							}
							if (accountNumber.getTransactionType() != null) {
								glAccountsTableAccumulation.setTransactionType(accountNumber.getTransactionType());
							}
							glAccountsTableAccumulation.setCreatedBy(loggedInUserId);
						}
					}
					glAccountsTableAccumulation = repositoryGLAccountsTableAccumulation.saveAndFlush(glAccountsTableAccumulation);
					FAAccountGroupAccountTableSetup faAccountGroupAccountTableSetup=
					repositoryFAAccountGroupAccountTableSetup.findByfaAccountGroupsSetupFaAccountGroupIndexAndAccountTypeTypeIdAndIsDeleted(faAccountGroupsSetup.getFaAccountGroupIndex(), accountNumber.getAccountType(),false);
					if(faAccountGroupAccountTableSetup==null){
						faAccountGroupAccountTableSetup=new FAAccountGroupAccountTableSetup();
					}
					faAccountGroupAccountTableSetup.setAccountType(repositoryMasterFAAccountGroupAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(accountNumber.getAccountType(),langid,false));
					faAccountGroupAccountTableSetup.setFaAccountGroupsSetup(faAccountGroupsSetup);
					faAccountGroupAccountTableSetup.setGlAccountsTableAccumulation(glAccountsTableAccumulation);
					repositoryFAAccountGroupAccountTableSetup.saveAndFlush(faAccountGroupAccountTableSetup);
				}
			} else {
				dtoPayableAccountClassSetup.setMessageType("REQUEST_BODY_IS_EMPTY");
			}
		} catch (Exception e) {
			dtoPayableAccountClassSetup.setMessageType("INTERNAL_SERVER_ERROR");
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		if (dtoPayableAccountClassSetup.getMessageType() == null) {
			return getAccountGroupSetbyByAccountGroupId(dtoPayableAccountClassSetup.getAccountGroupId());
		} else {
			return dtoPayableAccountClassSetup;
		}
	}

	public DtoFAPurchasePostingAccountSetup savePurchasePostingAccountSetup(
			DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup) 
	{
		try
		{
			GLAccountsTableAccumulation glAccountsTableAccumulation= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoFAPurchasePostingAccountSetup.getAccountTableRowIndex(),false);
			if(glAccountsTableAccumulation==null){
				dtoFAPurchasePostingAccountSetup.setMessageType("ACCOUNT_NUMBER_NOT_FOUND");
				return dtoFAPurchasePostingAccountSetup;
			}
			
			FAClassSetup faClassSetup = repositoryFAClassSetup.findByClassIdAndIsDeleted(dtoFAPurchasePostingAccountSetup.getClassId(),false);
			if(faClassSetup==null){
				dtoFAPurchasePostingAccountSetup.setMessageType("CLASS_ID_NOT_FOUND");
				return dtoFAPurchasePostingAccountSetup;
			}
				
			 int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			 FAPurchasePostingAccountSetup faPurchasePostingAccountSetup= repositoryFAPurchasePostingAccountSetup.findByFaClassSetupClassIdAndIsDeleted(faClassSetup.getClassId(),false);
			 if(faPurchasePostingAccountSetup==null){
				 faPurchasePostingAccountSetup = new FAPurchasePostingAccountSetup();
			 }
			 
			 faPurchasePostingAccountSetup.setCreatedBy(loggedInUserId);
			 faPurchasePostingAccountSetup.setChangeBy(loggedInUserId);
			 faPurchasePostingAccountSetup.setGlAccountsTableAccumulation(glAccountsTableAccumulation);
			 faPurchasePostingAccountSetup.setFaClassSetup(faClassSetup);
			 faPurchasePostingAccountSetup=repositoryFAPurchasePostingAccountSetup.saveAndFlush(faPurchasePostingAccountSetup);
			 return  getPurchasePostingAccountSetupByClassIndex(faPurchasePostingAccountSetup.getFixedAssetClassIndex());
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoSearch searchPurchasePostingAccountSetup(DtoSearch dtoSearchs) 
	{
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearch.getPageNumber());
		dtoSearch.setPageSize(dtoSearch.getPageSize());
		dtoSearch.setTotalCount(repositoryFAPurchasePostingAccountSetup.getCountAll());
		List<DtoFAPurchasePostingAccountSetup> list = new ArrayList<>();
		
		try{
			
			List<FAPurchasePostingAccountSetup> listOfFaPurchasingAccount;
			if(dtoSearchs.getPageNumber()==null && dtoSearchs.getPageSize()==null)
			{
				listOfFaPurchasingAccount=	repositoryFAPurchasePostingAccountSetup.predictiveSearch(dtoSearchs.getSearchKeyword());
			}
			else
			{
				Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
						"createdDate");
				listOfFaPurchasingAccount = repositoryFAPurchasePostingAccountSetup.predictiveSearch(dtoSearchs.getSearchKeyword(),pageable);
			}
			
			if(listOfFaPurchasingAccount!=null && !listOfFaPurchasingAccount.isEmpty()){
					for (FAPurchasePostingAccountSetup faPurchasePostingAccountSetup : listOfFaPurchasingAccount) {
						DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup = new DtoFAPurchasePostingAccountSetup(faPurchasePostingAccountSetup);
						 list.add(dtoFAPurchasePostingAccountSetup);
					}
			}
			 
		}catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		dtoSearch.setRecords(list);
		return dtoSearch;	
	}
	
	/**
	 * @param dtoLeaseCompanySetup
	 * @param faLeaseCompanySetup
	 * @return
	 */
	public DtoLeaseCompanySetup updateLeaseCompanySetup(DtoLeaseCompanySetup dtoLeaseCompanySetup, FALeaseCompanySetup faLeaseCompanySetup) 
	{
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		VendorMaintenance maintenance = repositoryVendorMaintenance.findOne(dtoLeaseCompanySetup.getVendorId());
		try 
		{
				faLeaseCompanySetup.setChangeBy(loggedInUserId);
				faLeaseCompanySetup.setCompanyId(dtoLeaseCompanySetup.getCompanyId());
				faLeaseCompanySetup.setCompanyName(dtoLeaseCompanySetup.getCompanyName());
				if(maintenance!=null){
					faLeaseCompanySetup.setVendorMaintenance(maintenance);
					faLeaseCompanySetup.setVendorName(maintenance.getVendorName());
				}
				faLeaseCompanySetup=repositoryFALeaseCompanySetup.saveAndFlush(faLeaseCompanySetup);
		} 
		catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return new DtoLeaseCompanySetup(faLeaseCompanySetup);
	}
	
	public DtoSearch getAllLeaseCompanySetup(DtoSearch dtoSearchs) {
		
		List<FALeaseCompanySetup> faLeaseCompanySetupList =null;
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositoryFALeaseCompanySetup.countAll());
		if(dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null){
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
		Page<FALeaseCompanySetup> pageList	=repositoryFALeaseCompanySetup.findAll(pageable);
		faLeaseCompanySetupList = pageList.getContent();
		}
		else{
			faLeaseCompanySetupList= repositoryFALeaseCompanySetup.findAll();
		}
		List<DtoLeaseCompanySetup> list = new ArrayList<>();

		for (FALeaseCompanySetup faLeaseCompanySetup : faLeaseCompanySetupList) {
			list.add(new DtoLeaseCompanySetup(faLeaseCompanySetup));
		}
		dtoSearch.setRecords(list);
		return dtoSearch;
	}
	
	public DtoSearch getAccountGroupSetupBySearch(DtoSearch dtoSearch) {
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		DtoFaAccountGroupSetup dtoFaAccountGroupSetups = null;
		List<FAAccountGroupsSetup> faAccountGroupsSetupList = null;
		List<DtoFaAccountGroupSetup> dtoFaAccountGroupSetupList = new ArrayList<>();
		try
		{
			dtoSearch.setTotalCount(
					repositoryFAAccountGroupSetup.predictiveSearchCount(dtoSearch.getSearchKeyword()));
			
			 if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
	                Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
	                           "createdDate");
	                faAccountGroupsSetupList = repositoryFAAccountGroupSetup
	                          .predictiveSearchWithPagination(dtoSearch.getSearchKeyword(), pageable);
	           } else {
	        	   faAccountGroupsSetupList = repositoryFAAccountGroupSetup
	                           .predictiveSearch(dtoSearch.getSearchKeyword());
	           }
	           if (faAccountGroupsSetupList != null && !faAccountGroupsSetupList.isEmpty()) {
	                for (FAAccountGroupsSetup faAccountGroupsSetup : faAccountGroupsSetupList) {
	                	dtoFaAccountGroupSetups = new DtoFaAccountGroupSetup(faAccountGroupsSetup);
	                	List<DtoFaAccountGroupSetup> accountDetailList=  serviceAccountPayable.getFaAccountGroupSetupAccountNumberList(faAccountGroupsSetup.getFaAccountGroupIndex(), langId);
	                	dtoFaAccountGroupSetups.setAccountNumberList(accountDetailList);
	                	dtoFaAccountGroupSetupList.add(dtoFaAccountGroupSetups);
	                }
	           }
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		dtoSearch.setRecords(dtoFaAccountGroupSetupList);
		return dtoSearch;
	}
	
	public DtoFAPurchasePostingAccountSetup getPurchasePostingAccountSetupByClassIndex(int classIndex) 
	{
		
		DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup=null;
		try{
			FAPurchasePostingAccountSetup faPurchasePostingAccountSetup= repositoryFAPurchasePostingAccountSetup.findOne(classIndex);
			if(faPurchasePostingAccountSetup!=null){
				dtoFAPurchasePostingAccountSetup = new DtoFAPurchasePostingAccountSetup(faPurchasePostingAccountSetup);
			}
		}catch(Exception e)
		{
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoFAPurchasePostingAccountSetup;	
	}
	
	public DtoFAPurchasePostingAccountSetup updatePurchasePostingAccountSetup(
			DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup , FAPurchasePostingAccountSetup faPurchasePostingAccountSetup) 
	{
		try
		{
			GLAccountsTableAccumulation glAccountsTableAccumulation= repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoFAPurchasePostingAccountSetup.getAccountTableRowIndex(),false);
			if(glAccountsTableAccumulation==null){
				dtoFAPurchasePostingAccountSetup.setMessageType("ACCOUNT_NUMBER_NOT_FOUND");
				return dtoFAPurchasePostingAccountSetup;
			}
			
			FAClassSetup faClassSetup = repositoryFAClassSetup.findByClassIdAndIsDeleted(dtoFAPurchasePostingAccountSetup.getClassId(),false);
			if(faClassSetup==null){
				dtoFAPurchasePostingAccountSetup.setMessageType("CLASS_ID_NOT_FOUND");
				return dtoFAPurchasePostingAccountSetup;
			}
			 int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			 faPurchasePostingAccountSetup.setCreatedBy(loggedInUserId);
			 faPurchasePostingAccountSetup.setChangeBy(loggedInUserId);
			 faPurchasePostingAccountSetup.setGlAccountsTableAccumulation(glAccountsTableAccumulation);
			 faPurchasePostingAccountSetup.setFaClassSetup(faClassSetup);
			 faPurchasePostingAccountSetup=repositoryFAPurchasePostingAccountSetup.saveAndFlush(faPurchasePostingAccountSetup);
			 return  getPurchasePostingAccountSetupByClassIndex(faPurchasePostingAccountSetup.getFixedAssetClassIndex());
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public DtoPayableAccountClassSetup getAccountGroupSetbyByAccountGroupId(String accountGroupId) 
	{
		String langId = httpServletRequest.getHeader(CommonConstant.LANG_ID);
		DtoPayableAccountClassSetup main = null;
		List<FAAccountGroupAccountTableSetup> accountGroupTableSetupList =null;
		FAAccountGroupsSetup accountGroupSetup= repositoryFAAccountGroupSetup.findByfaAccountGroupIdAndIsDeleted(accountGroupId, false);
		if(accountGroupSetup!=null){
			 accountGroupTableSetupList= repositoryFAAccountGroupAccountTableSetup.findByfaAccountGroupsSetupFaAccountGroupIndexAndIsDeleted(accountGroupSetup.getFaAccountGroupIndex(), false);
		}
		
		List<DtoPayableAccountClassSetup> list = new ArrayList<>();
		if (accountGroupTableSetupList != null && !accountGroupTableSetupList.isEmpty()) {
			main = new DtoPayableAccountClassSetup();
			main.setAccountGroupId(accountGroupId);
			main.setDescription("");
			main.setDescriptionArabic("");
			for (FAAccountGroupAccountTableSetup faAccountGroupTableSetup : accountGroupTableSetupList) {
				List<Long> accountNumberIndex = new ArrayList<>();
				DtoPayableAccountClassSetup dtoPayableAccountClassSetup = new DtoPayableAccountClassSetup();
				
				if(UtilRandomKey.isNotBlank(accountGroupSetup.getFaAccountGroupDescription())){
					main.setDescription(accountGroupSetup.getFaAccountGroupDescription());
				}
				if(UtilRandomKey.isNotBlank(accountGroupSetup.getFaAccountGroupDescriptionArabic())){
					main.setDescriptionArabic(accountGroupSetup.getFaAccountGroupDescriptionArabic());
				}
			
				
				dtoPayableAccountClassSetup.setAccountType(0);
				dtoPayableAccountClassSetup.setAccountTypeValue("");
				dtoPayableAccountClassSetup.setAccountGroupId(accountGroupSetup.getFaAccountGroupId());
				MasterFAAccountGroupAccountType masterAccountType =faAccountGroupTableSetup.getAccountType();
				if (masterAccountType!= null) 
				{
					int lang = Integer.parseInt(langId);
					if(masterAccountType.getLanguage().getLanguageId()!=lang){
						masterAccountType = repositoryMasterFAAccountGroupAccountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterAccountType.getTypeId(),lang,false);
					}
					if(masterAccountType!=null)
					{
						dtoPayableAccountClassSetup
						.setAccountType(masterAccountType.getTypeId());
					dtoPayableAccountClassSetup
							.setAccountTypeValue(masterAccountType.getAccountType());
					}
				}

				GLAccountsTableAccumulation glAccountsTableAccumulation = faAccountGroupTableSetup
						.getGlAccountsTableAccumulation();
				String accountNumber = "";
				List<DtoPayableAccountClassSetup> listOfKeyValue = new ArrayList<>();
				if (glAccountsTableAccumulation != null) {
					dtoPayableAccountClassSetup
							.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
					Object[] arrayOfObject= serviceAccountPayable.getAccountNumberMultipleWays(glAccountsTableAccumulation, langId);
					accountNumber=arrayOfObject[0].toString();
					accountNumberIndex=(List<Long>) arrayOfObject[1];
					listOfKeyValue=(List<DtoPayableAccountClassSetup>) arrayOfObject[2];
					dtoPayableAccountClassSetup.setAccountDescription(arrayOfObject[3].toString());
				}
				dtoPayableAccountClassSetup.setAccountNumberIndex(accountNumberIndex);
				dtoPayableAccountClassSetup.setAccountNumber(accountNumber);
				dtoPayableAccountClassSetup.setAccNumberIndexValue(listOfKeyValue);
				list.add(dtoPayableAccountClassSetup);
			}
			main.setAccountNumberList(list);
		}
		return main;
	}
	
	public DtoFAPurchasePostingAccountSetup getPurchasePostingAccountSetupByClassId(String classId) 
	{
		DtoFAPurchasePostingAccountSetup dtoFAPurchasePostingAccountSetup=null;
		try
		{
			FAPurchasePostingAccountSetup faPurchasePostingAccountSetup= repositoryFAPurchasePostingAccountSetup.findByFaClassSetupClassIdAndIsDeleted(classId, false);
			if(faPurchasePostingAccountSetup!=null){
				dtoFAPurchasePostingAccountSetup = new DtoFAPurchasePostingAccountSetup(faPurchasePostingAccountSetup);
			}
		}
		catch(Exception e)
		{
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoFAPurchasePostingAccountSetup;	
	}
	
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch getActiveFAGeneralMaintenance(DtoSearch dtoSearchs) 
	{
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		DtoFAGeneralMaintenance dtoFAGeneralMaintenance = null;
		List<FAGeneralMaintenance> faGeneralMaintenances = null;
		List<DtoFAGeneralMaintenance> dtoFAGeneralMaintenancesList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			faGeneralMaintenances = repositoryFAGeneralMaintenance
					.findByMasterGeneralMaintenanceAssetStatusTypeIdAndIsDeleted(1,false, pageable);//1=Active
		} else {
			faGeneralMaintenances = repositoryFAGeneralMaintenance
					.findByMasterGeneralMaintenanceAssetStatusTypeIdAndIsDeleted(1,false);//1=Active
		}
		
		if (faGeneralMaintenances != null) {
			for (FAGeneralMaintenance faGeneralMaintenance : faGeneralMaintenances) {
				int langId=serviceHome.getLanngugaeId();
				MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus =faGeneralMaintenance.getMasterGeneralMaintenanceAssetStatus();
				if(masterGeneralMaintenanceAssetStatus!=null){
					masterGeneralMaintenanceAssetStatus =repositoryMasterGeneralMaintenanceAssetStatus.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetStatus.getTypeId(),langId,false);
				}
				MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType =faGeneralMaintenance.getMasterGeneralMaintenanceAssetType();
				if(masterGeneralMaintenanceAssetType!=null){
					masterGeneralMaintenanceAssetType =repositoryMasterGeneralMaintenanceAssetType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterGeneralMaintenanceAssetType.getTypeId(),langId,false);
				}
				dtoFAGeneralMaintenance = new DtoFAGeneralMaintenance(faGeneralMaintenance,masterGeneralMaintenanceAssetStatus,masterGeneralMaintenanceAssetType);
				dtoFAGeneralMaintenancesList.add(dtoFAGeneralMaintenance);
			}
		}
		dtoSearch.setRecords(dtoFAGeneralMaintenancesList);
		dtoSearch.setTotalCount(repositoryFAGeneralMaintenance.activeFaAssetCount());
		return dtoSearch;
	}
}
