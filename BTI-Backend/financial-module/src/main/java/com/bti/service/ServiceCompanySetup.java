/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.APDistributionTypeConstant;
import com.bti.constant.ARDistributionType;
import com.bti.constant.BTICodeType;
import com.bti.constant.CommonConstant;
import com.bti.model.ARYTDOpenTransactions;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.CreditCardType;
import com.bti.model.CreditCardsSetup;
import com.bti.model.MasterDiscountType;
import com.bti.model.MasterDiscountTypePeriod;
import com.bti.model.MasterDueTypes;
import com.bti.model.ModulesConfiguration;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.PaymentTermsSetup;
import com.bti.model.ShipmentMethodSetup;
import com.bti.model.ShipmentMethodType;
import com.bti.model.VATBasedOnType;
import com.bti.model.VATSetup;
import com.bti.model.VATType;
import com.bti.model.dto.DtoAccountTableSetup;
import com.bti.model.dto.DtoBankSetUp;
import com.bti.model.dto.DtoCreditCardSetUp;
import com.bti.model.dto.DtoModuleConfiguration;
import com.bti.model.dto.DtoPaymentTermSetUp;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoShipmentMethodSetup;
import com.bti.model.dto.DtoStatusType;
import com.bti.model.dto.DtoVatDetailSetup;
import com.bti.repository.RepositoryAPApplyDocumentsPayments;
import com.bti.repository.RepositoryAPYTDOpenTransaction;
import com.bti.repository.RepositoryAPYTDOpenTransactionDistribution;
import com.bti.repository.RepositoryARApplyDocumentsPayments;
import com.bti.repository.RepositoryARYTDOpenTransaction;
import com.bti.repository.RepositoryARYTDOpenTransactionDistribution;
import com.bti.repository.RepositoryBankSetup;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCreditCardType;
import com.bti.repository.RepositoryCreditCardsSetup;
import com.bti.repository.RepositoryCustomerClassesSetup;
import com.bti.repository.RepositoryCustomerMaintenanceOptions;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryMasterAccountType;
import com.bti.repository.RepositoryMasterDiscountType;
import com.bti.repository.RepositoryMasterDiscountTypePeriod;
import com.bti.repository.RepositoryMasterDueTypes;
import com.bti.repository.RepositoryMasterPostingAccountsSequence;
import com.bti.repository.RepositoryModuleConfiguration;
import com.bti.repository.RepositoryModulesConfigurationPeriod;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.repository.RepositoryPaymentTermSetup;
import com.bti.repository.RepositoryShipmentMethodSetup;
import com.bti.repository.RepositoryShipmentMethodType;
import com.bti.repository.RepositoryVATBasedOnType;
import com.bti.repository.RepositoryVATSetup;
import com.bti.repository.RepositoryVATType;
import com.bti.repository.RepositoryVendorClassesSetup;
import com.bti.repository.RepositoryVendorMaintenanceOptions;
import com.bti.util.CodeGenerator;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;

/**
 * Description: Service Authentication
 * Name of Project: BTI
 * Created on: Aug 04, 2017
 * Modified on: Aug 04, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service("serviceCompanySetup")
public class ServiceCompanySetup {
	static Logger log = Logger.getLogger(ServiceCompanySetup.class.getName());
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryModuleConfiguration repositoryModuleConfiguration;
	
	@Autowired
	RepositoryPaymentTermSetup repositoryPaymentTermSetup;
	
	@Autowired
	RepositoryCreditCardsSetup repositoryCreditCardsSetup;
	
	@Autowired
	CodeGenerator codeGenerator;
	
	@Autowired
	RepositoryShipmentMethodSetup repositoryShipmentMethodSetup;
	
	@Autowired
	RepositoryCreditCardType repositoryCreditCardType;
	
	@Autowired
	RepositoryShipmentMethodType repositoryShipmentMethodType;
	
	@Autowired
	RepositoryMasterDueTypes repositoryMasterDueTypes;
	
	@Autowired
	RepositoryMasterDiscountType repositoryMasterDiscountType;
	
	@Autowired
	RepositoryMasterDiscountTypePeriod repositoryMasterDiscountTypePeriod;
	
	@Autowired
	RepositoryVATSetup repositoryVATSetup;
	
	@Autowired
	RepositoryVATType repositoryVATType;
	
	@Autowired
	RepositoryVATBasedOnType repositoryVATBasedOnType;
	
	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;
	
	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;
	
	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;
	
	@Autowired
	RepositoryMasterAccountType repositoryMasterAccountType;
	
	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;
	
	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryCustomerClassesSetup repositoryCustomerClassesSetup;
	
	@Autowired
	RepositoryCustomerMaintenanceOptions repositoryCustomerMaintenanceOptions;
	
	@Autowired
	RepositoryVendorClassesSetup repositoryVendorClassesSetup;
	
	@Autowired
	RepositoryVendorMaintenanceOptions repositoryVendorMaintenanceOptions;
	
	@Autowired
	RepositoryBankSetup repositoryBankSetup;
	
	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	@Autowired
	RepositoryMasterPostingAccountsSequence repositoryMasterPostingAccountsSequence;
	
	@Autowired
	RepositoryModulesConfigurationPeriod repositoryModulesConfigurationPeriod;
	
	@Autowired
	RepositoryAPYTDOpenTransaction repositoryAPYTDOpenTransaction;
	
	@Autowired
	RepositoryARYTDOpenTransaction repositoryARYTDOpenTransaction;
	
	@Autowired
	RepositoryARYTDOpenTransactionDistribution repositoryARYTDOpenTransactionDistribution;
	
	@Autowired
	RepositoryAPYTDOpenTransactionDistribution repositoryAPYTDOpenTransactionDistribution;
	
	@Autowired
	RepositoryARApplyDocumentsPayments repositoryARApplyDocumentsPayments;
	
	@Autowired
	RepositoryAPApplyDocumentsPayments repositoryAPApplyDocumentsPayments;
	
	/**
	 * @param dtoModuleConfiguration
	 * @return
	 */
	public DtoModuleConfiguration saveOrUpdateModuleConfiguration(DtoModuleConfiguration dtoModuleConfiguration) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		ModulesConfiguration configuration = null;
		try {
			configuration = repositoryModuleConfiguration.findOne(dtoModuleConfiguration.getSeriesId());
			if(configuration!=null){
				//Update the object values
				configuration.setModuleDescription(dtoModuleConfiguration.getSeriesNamePrimary());
				configuration.setModuleDescriptionArabic(dtoModuleConfiguration.getSeriesNameSecondary());
				configuration.setOriginTransactionDescription(dtoModuleConfiguration.getOriginNamePrimary());
				configuration.setOriginTransactionDescriptionArabic(dtoModuleConfiguration.getOriginNameSecondary());
				configuration.setChangeBy(loggedInUserId);
				//Updating the model object into the database
				configuration = repositoryModuleConfiguration.saveAndFlush(configuration);
			}else{
				//Instantiating new model object and save the new series
				configuration = new ModulesConfiguration();
				configuration.setModuleDescription(dtoModuleConfiguration.getSeriesNamePrimary());
				configuration.setModuleDescriptionArabic(dtoModuleConfiguration.getSeriesNameSecondary());
				configuration.setOriginTransactionDescription(dtoModuleConfiguration.getOriginNamePrimary());
				configuration.setOriginTransactionDescriptionArabic(dtoModuleConfiguration.getOriginNameSecondary());
				configuration.setCreatedBy(loggedInUserId);
				//Saving the model object into the database
				configuration = repositoryModuleConfiguration.saveAndFlush(configuration);
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return new DtoModuleConfiguration(configuration);
	}
	
	/**
	 * @param dtoPaymentTermSetUp
	 * @return
	 */
	public DtoPaymentTermSetUp newCompanyPaymentTermSetup(DtoPaymentTermSetUp dtoPaymentTermSetUp)
    {   
          String userId = httpServletRequest.getHeader(CommonConstant.USER_ID);
          int langid = serviceHome.getLanngugaeId();
          int loggedInUserId=0;
          if(userId!=null)
          {
             loggedInUserId= Integer.parseInt(userId);
          }
          try
          {
            PaymentTermsSetup paymentTermsSetup = new PaymentTermsSetup();
            paymentTermsSetup.setPaymentTermId(dtoPaymentTermSetUp.getPaymentTermId());
            paymentTermsSetup.setPaymentDescription(dtoPaymentTermSetUp.getDescription());
            paymentTermsSetup.setPaymentDescriptionArabic(dtoPaymentTermSetUp.getArabicDescription());
            paymentTermsSetup.setMasterDueTypes(repositoryMasterDueTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoPaymentTermSetUp.getDueType(),langid,false));
            paymentTermsSetup.setDueDaysDate(dtoPaymentTermSetUp.getDueDays());
            if(dtoPaymentTermSetUp.getDiscountPeriod()!=null){
            	paymentTermsSetup.setMasterDiscountTypePeriod(repositoryMasterDiscountTypePeriod.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoPaymentTermSetUp.getDiscountPeriod(),langid,false));
            }
            
            paymentTermsSetup.setDiscountTypesPeriodValue(0);
            if(dtoPaymentTermSetUp.getDiscountPeriodDays()!=null){
            	paymentTermsSetup.setDiscountTypesPeriodValue(dtoPaymentTermSetUp.getDiscountPeriodDays());
            }
            
            if(dtoPaymentTermSetUp.getDiscountType()!=null){
            	  paymentTermsSetup.setMasterDiscountType(repositoryMasterDiscountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoPaymentTermSetUp.getDiscountType(),langid,false));
            }
            paymentTermsSetup.setCreatedBy(loggedInUserId);
            paymentTermsSetup.setDiscountPercent(0.0);
            paymentTermsSetup.setDiscountAmount(0.0);
            if(UtilRandomKey.isNotBlank(dtoPaymentTermSetUp.getDiscountTypeValue())){
            	if(dtoPaymentTermSetUp.getDiscountType()==1){
                    paymentTermsSetup.setDiscountPercent(Double.valueOf(dtoPaymentTermSetUp.getDiscountTypeValue()));
                }
                else{
                    paymentTermsSetup.setDiscountAmount(Double.valueOf(dtoPaymentTermSetUp.getDiscountTypeValue()));
                }
            }
            
            repositoryPaymentTermSetup.save(paymentTermsSetup);
          }
          catch(Exception e)
          {
               dtoPaymentTermSetUp.setMessageType("INTERNAL_SERVER_ERROR");
               log.info(Arrays.toString(e.getStackTrace()));
          }
          return dtoPaymentTermSetUp;
    }

	/**
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch getModuleConfigurationList(DtoSearch dtoSearch) {
		List<DtoModuleConfiguration> configurationsList = new ArrayList<>();
		DtoModuleConfiguration dtoModuleConfigurationObj;
		List<ModulesConfiguration> list;
		try {
			// check if the request is for search or not else return all the
			// records based upon the pagination
			List<ModulesConfiguration> listSize = repositoryModuleConfiguration.findAll();
			if(listSize!=null && !listSize.isEmpty()){
				dtoSearch.setTotalCount(listSize.size());
			}
			if (dtoSearch.getPageNumber()!=null && dtoSearch.getPageSize()!=null) {
				Pageable pageable  = new PageRequest(dtoSearch.getPageNumber(),
						dtoSearch.getPageSize(), Direction.DESC, "createdDate");
				list = repositoryModuleConfiguration.predictiveModulesConfigurationSearchWithPagination(
						dtoSearch.getSearchKeyword(), pageable);
			} else {
				list = repositoryModuleConfiguration.predictiveModulesConfigurationSearch(
						dtoSearch.getSearchKeyword());
			}
			if (list!=null && !list.isEmpty()) {
				for (ModulesConfiguration modulesConfiguration : list) {
					dtoModuleConfigurationObj = new DtoModuleConfiguration(modulesConfiguration);
					configurationsList.add(dtoModuleConfigurationObj);
				}
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		dtoSearch.setRecords(configurationsList);
		return dtoSearch ;
	}
	
	 /**
	 * @param dtoPaymentTermSetUpp
	 * @return
	 */
	public DtoPaymentTermSetUp getPaymentTermSetUpById(DtoPaymentTermSetUp dtoPaymentTermSetUpp){
        int langId = serviceHome.getLanngugaeId();
        
        DtoPaymentTermSetUp dtoPaymentTermSetUp =null;
        PaymentTermsSetup paymentTermsSetup = repositoryPaymentTermSetup.findByPaymentTermIdAndIsDeleted(dtoPaymentTermSetUpp.getPaymentTermId(),false);
        if(paymentTermsSetup!=null)
        {
        	MasterDueTypes masterDueTypes = paymentTermsSetup.getMasterDueTypes();
            if(masterDueTypes!=null && langId!=masterDueTypes.getLanguage().getLanguageId())
            {
            		masterDueTypes= repositoryMasterDueTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterDueTypes.getTypeId(),langId,false);
            }
            MasterDiscountTypePeriod masterDiscountTypePeriod= paymentTermsSetup.getMasterDiscountTypePeriod();
            if(masterDiscountTypePeriod!=null && masterDiscountTypePeriod.getLanguage().getLanguageId()!=langId){
            	masterDiscountTypePeriod=repositoryMasterDiscountTypePeriod.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterDiscountTypePeriod.getTypeId(), langId,false);
            }
            
            MasterDiscountType masterDiscountType= paymentTermsSetup.getMasterDiscountType();
            if(masterDiscountType!=null && masterDiscountType.getLanguage().getLanguageId()!=langId){
            	masterDiscountType=repositoryMasterDiscountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterDiscountType.getTypeId(), langId,false);
            }
             dtoPaymentTermSetUp= new DtoPaymentTermSetUp(paymentTermsSetup,masterDiscountTypePeriod,masterDueTypes,masterDiscountType);
             
        }
        return dtoPaymentTermSetUp;
     }
	 
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchPaymentTermSetUp(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(
				repositoryPaymentTermSetup.predictivePaymentTermSearchCount(dtoSearchs.getSearchKeyword()));
		int langId = serviceHome.getLanngugaeId();
		
		DtoPaymentTermSetUp dtoPaymentTermSetUp = null;
		List<PaymentTermsSetup> paymentTermsSetupList = null;
		List<DtoPaymentTermSetUp> dtoPaymentTermSetUpsList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			paymentTermsSetupList = repositoryPaymentTermSetup
					.predictivePaymentTermSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			paymentTermsSetupList = repositoryPaymentTermSetup
					.predictivePaymentTermSearch(dtoSearchs.getSearchKeyword());
		}
		if (paymentTermsSetupList != null) {
			for (PaymentTermsSetup paymentTermsSetup : paymentTermsSetupList) 
			{
				MasterDueTypes masterDueTypes = paymentTermsSetup.getMasterDueTypes();
	            if(masterDueTypes!=null && langId!=masterDueTypes.getLanguage().getLanguageId())
	            {
	            		masterDueTypes= repositoryMasterDueTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterDueTypes.getTypeId(),langId,false);
	            }
	            MasterDiscountTypePeriod masterDiscountTypePeriod= paymentTermsSetup.getMasterDiscountTypePeriod();
	            if(masterDiscountTypePeriod!=null && masterDiscountTypePeriod.getLanguage().getLanguageId()!=langId){
	            	masterDiscountTypePeriod=repositoryMasterDiscountTypePeriod.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterDiscountTypePeriod.getTypeId(), langId,false);
	            }
	            
	            MasterDiscountType masterDiscountType= paymentTermsSetup.getMasterDiscountType();
	            if(masterDiscountType!=null && masterDiscountType.getLanguage().getLanguageId()!=langId){
	            	masterDiscountType=repositoryMasterDiscountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(masterDiscountType.getTypeId(), langId,false);
	            }
				dtoPaymentTermSetUp = new DtoPaymentTermSetUp(paymentTermsSetup,masterDiscountTypePeriod,masterDueTypes,masterDiscountType);
				dtoPaymentTermSetUpsList.add(dtoPaymentTermSetUp);
			}
		}
		dtoSearch.setRecords(dtoPaymentTermSetUpsList);
		return dtoSearch;
	}
	 
	 /**
	 * @param dtoPaymentTermSetUp
	 * @param paymentTermsSetup
	 * @return
	 */
	public DtoPaymentTermSetUp updateCompanyPaymentTermSetup(DtoPaymentTermSetUp dtoPaymentTermSetUp,PaymentTermsSetup paymentTermsSetup)
 {
		String userId = httpServletRequest.getHeader(CommonConstant.USER_ID);
		int langid = serviceHome.getLanngugaeId();
		int loggedInUserId = 0;
		if (userId != null) {
			loggedInUserId = Integer.parseInt(userId);
		}
		try {
			paymentTermsSetup.setPaymentDescription(dtoPaymentTermSetUp.getDescription());
			paymentTermsSetup.setPaymentDescriptionArabic(dtoPaymentTermSetUp.getArabicDescription());
		    paymentTermsSetup.setMasterDueTypes(repositoryMasterDueTypes.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoPaymentTermSetUp.getDueType(),langid,false));
            paymentTermsSetup.setDueDaysDate(dtoPaymentTermSetUp.getDueDays());
            if(dtoPaymentTermSetUp.getDiscountPeriod()!=null){
            	 paymentTermsSetup.setMasterDiscountTypePeriod(repositoryMasterDiscountTypePeriod.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoPaymentTermSetUp.getDiscountPeriod(),langid,false));
                 
            }
            if(dtoPaymentTermSetUp.getDiscountPeriodDays()!=null){
            	paymentTermsSetup.setDiscountTypesPeriodValue(dtoPaymentTermSetUp.getDiscountPeriodDays());
            }
            if(dtoPaymentTermSetUp.getDiscountType()!=null){
            	 paymentTermsSetup.setMasterDiscountType(repositoryMasterDiscountType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoPaymentTermSetUp.getDiscountType(),langid,false));
            }
           
			paymentTermsSetup.setChangeBy(loggedInUserId);
			paymentTermsSetup.setModifyDate(new Date());
			if(UtilRandomKey.isNotBlank(dtoPaymentTermSetUp.getDiscountTypeValue()))
			{
				if (dtoPaymentTermSetUp.getDiscountType() == 1) {
					paymentTermsSetup.setDiscountPercent(Double.valueOf(dtoPaymentTermSetUp.getDiscountTypeValue()));
				} else {
					paymentTermsSetup.setDiscountAmount(Double.valueOf(dtoPaymentTermSetUp.getDiscountTypeValue()));
				}
			}
			
			repositoryPaymentTermSetup.save(paymentTermsSetup);
		} catch (Exception e) {
			dtoPaymentTermSetUp.setMessageType("INTERNAL_SERVER_ERROR");
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return dtoPaymentTermSetUp;
	}

	/**
	 * @param seriesId
	 * @return
	 */
	public DtoModuleConfiguration getOneModuleBySeriesId(int seriesId) {
		DtoModuleConfiguration configuration = null;
			try {
				//get one object of module configuration by series id
				ModulesConfiguration modulesConfiguration = repositoryModuleConfiguration.findOne(seriesId);
				if(modulesConfiguration!=null){
					//set entity object into dto object
					configuration = new DtoModuleConfiguration(modulesConfiguration);
				}
			} catch (Exception e) {
				log.info(Arrays.toString(e.getStackTrace()));
			}
		return configuration;
	}

	/**
	 * @param dtoCreditCardSetUp
	 * @return
	 */
	public DtoCreditCardSetUp saveCreditCardSetup(DtoCreditCardSetUp dtoCreditCardSetUp) {
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			int langid = serviceHome.getLanngugaeId();
			CreditCardsSetup creditCardsSetup = new CreditCardsSetup();
			creditCardsSetup.setCreatedBy(loggedInUserId);
			creditCardsSetup.setCheckBookIdBank(dtoCreditCardSetUp.getCheckBookId());
			creditCardsSetup.setCreatedDate(new Date());
			creditCardsSetup.setCreditCardName(dtoCreditCardSetUp.getCreditCardName());
			creditCardsSetup.setCreditCardNameArabic(dtoCreditCardSetUp.getCreditCardNameArabic());
			creditCardsSetup.setCardId(codeGenerator.getGeneratedCode(BTICodeType.CREDITCARD.name()));
			creditCardsSetup.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoCreditCardSetUp.getAccountTableRowIndex(),false));
			creditCardsSetup.setCreditCardType(repositoryCreditCardType.findTopOneByTypeIdAndLanguageLanguageId(dtoCreditCardSetUp.getCardType(),langid));
			creditCardsSetup= repositoryCreditCardsSetup.saveAndFlush(creditCardsSetup);
			dtoCreditCardSetUp.setCardId(creditCardsSetup.getCardId());
			return dtoCreditCardSetUp;
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoCreditCardSetUp
	 * @return
	 */
	public DtoCreditCardSetUp getCreditCardSetupById(DtoCreditCardSetUp dtoCreditCardSetUp) 
	{
		int langId= serviceHome.getLanngugaeId();
		CreditCardsSetup creditCardsSetup = repositoryCreditCardsSetup.findByCardIdAndIsDeleted(dtoCreditCardSetUp.getCardId(),false);
		if(creditCardsSetup!=null)
		{
			CreditCardType creditCardType=creditCardsSetup.getCreditCardType();
			if(creditCardType!=null && langId!=creditCardType.getLanguage().getLanguageId()){
				creditCardType = repositoryCreditCardType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(creditCardType.getTypeId(),langId,false);
			}
			DtoCreditCardSetUp dtoCreditCardSetUp2 = new DtoCreditCardSetUp(creditCardsSetup,creditCardType);
			if(creditCardsSetup.getGlAccountsTableAccumulation()!=null)
			{
				dtoCreditCardSetUp2.setAccountNumber(getAccountNumberByAccountTableRowIndex(Integer.parseInt(creditCardsSetup.getGlAccountsTableAccumulation().getAccountTableRowIndex())));
			}
			else
			{
				dtoCreditCardSetUp2.setAccountNumber("");
			}
			return dtoCreditCardSetUp2;
		}
		return null;
	}
	
	/**
	 * @param accountTableRowIndexId
	 * @return
	 */
	public String getAccountNumberByAccountTableRowIndex(int accountTableRowIndexId){
		StringBuilder accountNumber= new StringBuilder("");
			List<PayableAccountClassSetup> listOfPayable = repositoryPayableAccountClassSetup.findByAccountRowIndexAndIsDeleted(accountTableRowIndexId,false);
			
			int i=0;
			for (PayableAccountClassSetup payableAccountClassSetup : listOfPayable) 
			{
				if(i==0)
				{
					COAMainAccounts coaMainAccounts = repositoryCOAMainAccounts.findByActIndxAndIsDeleted(payableAccountClassSetup.getIndexId(),false);
					 
						if(coaMainAccounts!=null && UtilRandomKey.isNotBlank(coaMainAccounts.getMainAccountNumber())){
							accountNumber.append(coaMainAccounts.getMainAccountNumber());
						}
					 
				}
				else
				{
					COAFinancialDimensionsValues coafValue=	repositoryCOAFinancialDimensionsValues.findByDimInxValueAndIsDeleted(payableAccountClassSetup.getIndexId(),false);
					 
						if(coafValue!=null && UtilRandomKey.isNotBlank(coafValue.getDimensionValue())){
							accountNumber.append("-"+coafValue.getDimensionValue());
						}
					 
				}
				i++;
			}
			
			return accountNumber.toString();
	}

	/**
	 * @param dtoCreditCardSetUp
	 * @return
	 */
	public DtoCreditCardSetUp updateCreditCardSetup(DtoCreditCardSetUp dtoCreditCardSetUp) {
		try {
			CreditCardsSetup creditCardsSetup = repositoryCreditCardsSetup.findByCardIdAndIsDeleted(dtoCreditCardSetUp.getCardId(),false);
			if (creditCardsSetup != null) {
				int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
				int langid = serviceHome.getLanngugaeId();
				creditCardsSetup.setChangeBy(loggedInUserId);
				creditCardsSetup.setCheckBookIdBank(dtoCreditCardSetUp.getCheckBookId());
				creditCardsSetup.setModifyDate(new Date());
				creditCardsSetup.setCreditCardName(dtoCreditCardSetUp.getCreditCardName());
				creditCardsSetup.setCreditCardNameArabic(dtoCreditCardSetUp.getCreditCardNameArabic());
				creditCardsSetup.setGlAccountsTableAccumulation(repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(dtoCreditCardSetUp.getAccountTableRowIndex(),false));
				creditCardsSetup.setCreditCardType(repositoryCreditCardType.findTopOneByTypeIdAndLanguageLanguageId(dtoCreditCardSetUp.getCardType(),langid));
				creditCardsSetup = repositoryCreditCardsSetup.saveAndFlush(creditCardsSetup);
				dtoCreditCardSetUp.setCardId(creditCardsSetup.getCardId());
				return dtoCreditCardSetUp;
			}

		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchCreditCardSetup(DtoSearch dtoSearchs) {
		try {
			int langId= serviceHome.getLanngugaeId();
			DtoSearch dtoSearch = new DtoSearch();
			dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
			dtoSearch.setPageSize(dtoSearchs.getPageSize());
			dtoSearch.setTotalCount(repositoryCreditCardsSetup.predictiveCreditCardSetupSearchCount(dtoSearchs.getSearchKeyword()));
			List<DtoCreditCardSetUp> dtoCreditCardSetUpList = new ArrayList<>();
			List<CreditCardsSetup> list = null;
			if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoSearchs.getPageNumber(), dtoSearchs.getPageSize(), Direction.DESC,
						"createdDate");
				list = repositoryCreditCardsSetup.predictiveCreditCardSetupSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
			} else {
			 
				list = repositoryCreditCardsSetup.predictiveCreditCardSetupSearch(dtoSearchs.getSearchKeyword());
			}
			if (list != null) {
				for (CreditCardsSetup creditCardsSetup : list) {

					CreditCardType creditCardType = creditCardsSetup.getCreditCardType();
					if (creditCardType != null && langId != creditCardType.getLanguage().getLanguageId()) {
						creditCardType = repositoryCreditCardType
								.findByTypeIdAndLanguageLanguageIdAndIsDeleted(creditCardType.getTypeId(), langId, false);
					}
				DtoCreditCardSetUp dtoCreditCardSetUp2=	new DtoCreditCardSetUp(creditCardsSetup,creditCardType);
					
					if(creditCardsSetup.getGlAccountsTableAccumulation()!=null)
					{
						dtoCreditCardSetUp2.setAccountNumber(getAccountNumberByAccountTableRowIndex(Integer.parseInt(creditCardsSetup.getGlAccountsTableAccumulation().getAccountTableRowIndex())));
					}
					else
					{
						dtoCreditCardSetUp2.setAccountNumber("");
					}
					
					dtoCreditCardSetUpList.add(dtoCreditCardSetUp2);
				}
			}
			dtoSearch.setRecords(dtoCreditCardSetUpList);
			return dtoSearch;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param dtoShipmentMethodSetup
	 * @return
	 */
	public DtoShipmentMethodSetup saveShipmentMethodSetup(DtoShipmentMethodSetup dtoShipmentMethodSetup) {
		try{
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			int langid = serviceHome.getLanngugaeId();
			ShipmentMethodSetup shipmentMethodSetup = repositoryShipmentMethodSetup.findByShipmentMethodIdAndIsDeleted(dtoShipmentMethodSetup.getShipmentMethodId(),false);
			if(shipmentMethodSetup!=null){ 
				dtoShipmentMethodSetup.setMessageType("SHIPMENT_METHOD_SETUP_ALREADY_EXIST");
				return dtoShipmentMethodSetup;
			}
			
			shipmentMethodSetup = new ShipmentMethodSetup();
			shipmentMethodSetup.setShipmentMethodId(dtoShipmentMethodSetup.getShipmentMethodId());
			shipmentMethodSetup.setCreatedBy(loggedInUserId);
			shipmentMethodSetup.setShipmentCarrier(dtoShipmentMethodSetup.getShipmentCarrier());
			shipmentMethodSetup.setShipmentCarrierAccount(dtoShipmentMethodSetup.getShipmentCarrierAccount());
			shipmentMethodSetup.setShipmentCarrierArabic(dtoShipmentMethodSetup.getShipmentCarrierArabic());
			shipmentMethodSetup.setShipmentCarrierContactName(dtoShipmentMethodSetup.getShipmentCarrierContactName());
			shipmentMethodSetup.setShipmentDescription(dtoShipmentMethodSetup.getShipmentDescription());
			shipmentMethodSetup.setShipmentDescriptionArabic(dtoShipmentMethodSetup.getShipmentDescriptionArabic());
			shipmentMethodSetup.setShipmentMethodType(repositoryShipmentMethodType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoShipmentMethodSetup.getShipmentMethodType(),langid,false));
			shipmentMethodSetup.setShipmentPhoneNumber(dtoShipmentMethodSetup.getShipmentPhoneNumber());
			repositoryShipmentMethodSetup.saveAndFlush(shipmentMethodSetup);
			return dtoShipmentMethodSetup;
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoShipmentMethodSetup
	 * @return
	 */
	public DtoShipmentMethodSetup updateShipmentMethodSetup(DtoShipmentMethodSetup dtoShipmentMethodSetup) {
		try{
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			int langid = serviceHome.getLanngugaeId();
			ShipmentMethodSetup shipmentMethodSetup = repositoryShipmentMethodSetup.findByShipmentMethodIdAndIsDeleted(dtoShipmentMethodSetup.getShipmentMethodId(),false);
			if(shipmentMethodSetup!=null){ 
				shipmentMethodSetup.setChangeBy(loggedInUserId);
				shipmentMethodSetup.setModifyDate(new Date());
				shipmentMethodSetup.setShipmentCarrier(dtoShipmentMethodSetup.getShipmentCarrier());
				shipmentMethodSetup.setShipmentCarrierAccount(dtoShipmentMethodSetup.getShipmentCarrierAccount());
				shipmentMethodSetup.setShipmentCarrierArabic(dtoShipmentMethodSetup.getShipmentCarrierArabic());
				shipmentMethodSetup.setShipmentCarrierContactName(dtoShipmentMethodSetup.getShipmentCarrierContactName());
				shipmentMethodSetup.setShipmentDescription(dtoShipmentMethodSetup.getShipmentDescription());
				shipmentMethodSetup.setShipmentDescriptionArabic(dtoShipmentMethodSetup.getShipmentDescriptionArabic());
				shipmentMethodSetup.setShipmentMethodType(repositoryShipmentMethodType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoShipmentMethodSetup.getShipmentMethodType(),langid,false));
				shipmentMethodSetup.setShipmentPhoneNumber(dtoShipmentMethodSetup.getShipmentPhoneNumber());
				repositoryShipmentMethodSetup.saveAndFlush(shipmentMethodSetup);
				return dtoShipmentMethodSetup;
			} 
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoShipmentMethodSetup
	 * @return
	 */
	public DtoShipmentMethodSetup getShipmentMethodSetupById(DtoShipmentMethodSetup dtoShipmentMethodSetup) {
		try{
			int langId =serviceHome.getLanngugaeId();
			ShipmentMethodSetup shipmentMethodSetup = repositoryShipmentMethodSetup.findByShipmentMethodIdAndIsDeleted(dtoShipmentMethodSetup.getShipmentMethodId(),false);
			if(shipmentMethodSetup!=null){ 
				ShipmentMethodType shipmentMethodType = shipmentMethodSetup.getShipmentMethodType();
				if(shipmentMethodType!=null){
					shipmentMethodType= repositoryShipmentMethodType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(shipmentMethodType.getTypeId(),langId,false);
				}
				return new DtoShipmentMethodSetup(shipmentMethodSetup,shipmentMethodType);
			}
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchShipmentMethodSetup(DtoSearch dtoSearchs) {
		try {
			int langId =serviceHome.getLanngugaeId();
			DtoSearch dtoSearch = new DtoSearch();
			dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
			dtoSearch.setPageSize(dtoSearchs.getPageSize());
			dtoSearch.setTotalCount(repositoryShipmentMethodSetup.predictiveShipmentMethodSetupSearchCount(dtoSearchs.getSearchKeyword()));
			List<DtoShipmentMethodSetup> dtoShipmentMethodSetupList = new ArrayList<>();
			List<ShipmentMethodSetup> list = null;
			if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoSearchs.getPageNumber(), dtoSearchs.getPageSize(), Direction.DESC,
						"createdDate");
				list = repositoryShipmentMethodSetup.predictiveShipmentMethodSetupSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
			} else {
			 
				list = repositoryShipmentMethodSetup.predictiveShipmentMethodSetupSearch(dtoSearchs.getSearchKeyword());
			}
			if (list != null) {
				for (ShipmentMethodSetup shipmentMethodSetup : list) {
					ShipmentMethodType shipmentMethodType = shipmentMethodSetup.getShipmentMethodType();
					if(shipmentMethodType!=null){
						shipmentMethodType= repositoryShipmentMethodType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(shipmentMethodType.getTypeId(),langId,false);
					}
					dtoShipmentMethodSetupList.add(new DtoShipmentMethodSetup(shipmentMethodSetup,shipmentMethodType));
				}
			}
			dtoSearch.setRecords(dtoShipmentMethodSetupList);
			return dtoSearch;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param dtoVatDetailSetup
	 * @return
	 */
	public DtoVatDetailSetup saveVatDetailSetup(DtoVatDetailSetup dtoVatDetailSetup) {
		try{
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			int langid = serviceHome.getLanngugaeId();
			VATSetup vatSetup = repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoVatDetailSetup.getVatScheduleId(),false);
			if(vatSetup!=null){ 
				dtoVatDetailSetup.setMessageType("VAT_DETAIL_SETUP_ALREADY_EXIST");
				return dtoVatDetailSetup;
			}
			vatSetup = new VATSetup();
			vatSetup.setAccountRowId(dtoVatDetailSetup.getAccountRowId());
			if(dtoVatDetailSetup.getBasperct()!=null){
				vatSetup.setBasperct(dtoVatDetailSetup.getBasperct());
			}
			vatSetup.setCreatedBy(loggedInUserId);
			vatSetup.setLastYearSalesPurchaseTaxes(dtoVatDetailSetup.getLastYearSalesPurchaseTaxes());
			vatSetup.setLastYearTaxableSalesPurchase(dtoVatDetailSetup.getLastYearTaxableSalesPurchase());
			vatSetup.setLastYearTotalSalesPurchase(dtoVatDetailSetup.getLastYearTotalSalesPurchase());
			vatSetup.setMaximumVATAmount(dtoVatDetailSetup.getMaximumVATAmount());
			vatSetup.setMinimumVATAmount(dtoVatDetailSetup.getMinimumVATAmount());
			if(dtoVatDetailSetup.getVatBaseOn()!=null){
				vatSetup.setVatBasedOnType(repositoryVATBasedOnType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoVatDetailSetup.getVatBaseOn(),langid,false));
			}
			vatSetup.setVatDescription(dtoVatDetailSetup.getVatDescription());
			vatSetup.setVatDescriptionArabic(dtoVatDetailSetup.getVatDescriptionArabic());
			vatSetup.setVatIdNumber(dtoVatDetailSetup.getVatIdNumber());
			vatSetup.setVatScheduleId(dtoVatDetailSetup.getVatScheduleId());
			vatSetup.setVatType(repositoryVATType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoVatDetailSetup.getVatSeriesType(),langid,false));
			vatSetup.setYtdTotalSalesPurchase(dtoVatDetailSetup.getYtdTotalSalesPurchase());
			vatSetup.setYtdTotalSalesPurchaseTaxes(dtoVatDetailSetup.getYtdTotalSalesPurchaseTaxes());
			vatSetup.setYtdTotalTaxableSalesPurchase(dtoVatDetailSetup.getYtdTotalTaxableSalesPurchase());
			repositoryVATSetup.saveAndFlush(vatSetup);
			return dtoVatDetailSetup;
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoVatDetailSetup
	 * @return
	 */
	public DtoVatDetailSetup updateVatDetailSetup(DtoVatDetailSetup dtoVatDetailSetup) {
		try{
			int langid = serviceHome.getLanngugaeId();
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			VATSetup vatSetup = repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoVatDetailSetup.getVatScheduleId(),false);
			if(vatSetup!=null){ 
				vatSetup.setAccountRowId(dtoVatDetailSetup.getAccountRowId());
				if(dtoVatDetailSetup.getBasperct()!=null){
					vatSetup.setBasperct(dtoVatDetailSetup.getBasperct());
				}
				vatSetup.setChangeBy(loggedInUserId);
				vatSetup.setLastYearSalesPurchaseTaxes(dtoVatDetailSetup.getLastYearSalesPurchaseTaxes());
				vatSetup.setLastYearTaxableSalesPurchase(dtoVatDetailSetup.getLastYearTaxableSalesPurchase());
				vatSetup.setLastYearTotalSalesPurchase(dtoVatDetailSetup.getLastYearTotalSalesPurchase());
				vatSetup.setMaximumVATAmount(dtoVatDetailSetup.getMaximumVATAmount());
				vatSetup.setMinimumVATAmount(dtoVatDetailSetup.getMinimumVATAmount());
				if(dtoVatDetailSetup.getVatBaseOn()!=null){
					vatSetup.setVatBasedOnType(repositoryVATBasedOnType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoVatDetailSetup.getVatBaseOn(),langid,false));
				}
				vatSetup.setVatDescription(dtoVatDetailSetup.getVatDescription());
				vatSetup.setVatDescriptionArabic(dtoVatDetailSetup.getVatDescriptionArabic());
				vatSetup.setVatIdNumber(dtoVatDetailSetup.getVatIdNumber());
				vatSetup.setVatType(repositoryVATType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoVatDetailSetup.getVatSeriesType(),langid,false));
				vatSetup.setYtdTotalSalesPurchase(dtoVatDetailSetup.getYtdTotalSalesPurchase());
				vatSetup.setYtdTotalSalesPurchaseTaxes(dtoVatDetailSetup.getYtdTotalSalesPurchaseTaxes());
				vatSetup.setYtdTotalTaxableSalesPurchase(dtoVatDetailSetup.getYtdTotalTaxableSalesPurchase());
				repositoryVATSetup.saveAndFlush(vatSetup);
				return dtoVatDetailSetup;
			}
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoVatDetailSetup
	 * @return
	 */
	public DtoVatDetailSetup getVatDetailSetupById(DtoVatDetailSetup dtoVatDetailSetup) {
		int langId=serviceHome.getLanngugaeId();
		VATSetup vatSetup = repositoryVATSetup.findByVatScheduleIdAndIsDeleted(dtoVatDetailSetup.getVatScheduleId(),false);
		if(vatSetup!=null){ 
			VATBasedOnType vatBasedOnType = vatSetup.getVatBasedOnType();
			if(vatBasedOnType!=null){
				vatBasedOnType=repositoryVATBasedOnType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(vatBasedOnType.getTypeId(),langId,false);
			}
			return new DtoVatDetailSetup(vatSetup,vatBasedOnType);
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchVatDetailSetup(DtoSearch dtoSearchs) {
		try {
			int langId=serviceHome.getLanngugaeId();
			DtoSearch dtoSearch = new DtoSearch();
			dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
			dtoSearch.setPageSize(dtoSearchs.getPageSize());
			dtoSearch.setTotalCount(repositoryVATSetup.predictiveVatSetupSearchCount(dtoSearchs.getSearchKeyword()));
			List<DtoVatDetailSetup> dtoVatDetailSetupList = new ArrayList<>();
			List<VATSetup> list = null;
			if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoSearchs.getPageNumber(), dtoSearchs.getPageSize(), Direction.DESC,
						"createdDate");
				list = repositoryVATSetup.predictiveVatSetupSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
			} else {
			 
				list = repositoryVATSetup.predictiveVatSetupSearch(dtoSearchs.getSearchKeyword());
			}
			if (list != null) {
				for (VATSetup vatSetup : list) {
					VATBasedOnType vatBasedOnType = vatSetup.getVatBasedOnType();
					if(vatBasedOnType!=null){
						vatBasedOnType=repositoryVATBasedOnType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(vatBasedOnType.getTypeId(),langId,false);
					}
					dtoVatDetailSetupList.add(new DtoVatDetailSetup(vatSetup,vatBasedOnType));
				}
			}
			dtoSearch.setRecords(dtoVatDetailSetupList);
			return dtoSearch;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getShipmentMethodTypeStatus() {
		int langId = serviceHome.getLanngugaeId();
		List<ShipmentMethodType> list = repositoryShipmentMethodType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if(list!=null){
			for(ShipmentMethodType shipmentMethodType : list){
				responseList.add(new DtoStatusType(shipmentMethodType));
			}
		}
		return responseList;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getCreditCardTypeStatus() {
		int langId = serviceHome.getLanngugaeId();
		List<CreditCardType> list = repositoryCreditCardType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if(list!=null){
			for(CreditCardType creditCardType : list){
				responseList.add(new DtoStatusType(creditCardType));
			}
		}
		return responseList;
	}
	
	/**
	 * @return
	 */
	public List<DtoStatusType> getVatType() {
		int langId = serviceHome.getLanngugaeId();
		List<VATType> list = repositoryVATType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if(list!=null){
			for(VATType vatType : list){
				responseList.add(new DtoStatusType(vatType));
			}
		}
		return responseList;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getVatBasedOnTypeStatus() {
		int langId = serviceHome.getLanngugaeId();
		List<VATBasedOnType> list = repositoryVATBasedOnType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if(list!=null){
			for(VATBasedOnType vatBasedOnType : list){
				responseList.add(new DtoStatusType(vatBasedOnType));
			}
		}
		return responseList;
	}
	
	public boolean deleteMultiplePaymentTermSetup(DtoPaymentTermSetUp dtoPaymentTermSetUp)
    {   
		  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
          try
          {
             repositoryPaymentTermSetup.deleteMultiplePaymentTermSetup(dtoPaymentTermSetUp.getPaymentTermIds(), true, loggedInUserId);
             return true;
          }
          catch(Exception e)
          {
               return false;
          }
    }
	
	public boolean checkRelationOfPaymentTermSetup(DtoPaymentTermSetUp dtoPaymentTermSetUp)
    {   
		  Integer count= repositoryCustomerClassesSetup.getCountByPaymentTermsSetupPaymentTermId(dtoPaymentTermSetUp.getPaymentTermId());
         if(count==null || count==0)
         {
        	 count= repositoryCustomerMaintenanceOptions.getCountByPaymentTermsSetupPaymentTermId(dtoPaymentTermSetUp.getPaymentTermId());
             if(count==null || count==0){
            	count= repositoryVendorClassesSetup.getCountByPaymentTermsSetupPaymentTermId(dtoPaymentTermSetUp.getPaymentTermId());
               if(count==null || count==0 )
               {
            	   count= repositoryVendorMaintenanceOptions.getCountByPaymentTermsSetupPaymentTermId(dtoPaymentTermSetUp.getPaymentTermId());
                   if(count==null|| count==0){
                	   return false;
                   }
               }
            }
         }
         return true;
    }
	
	public boolean deleteMultipleCreditCardsSetup(DtoCreditCardSetUp dtoCreditCardSetUp)
    {   
		  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
          try
          {
             repositoryCreditCardsSetup.deleteMultipleCreditCardsSetup(dtoCreditCardSetUp.getCardIds(), true, loggedInUserId);
             return true;
          }
          catch(Exception e)
          {
               return false;
          }
    }
	
	public boolean checkRelationOfCreditCardsSetup(DtoCreditCardSetUp dtoCreditCardSetUp)
    {   
		  
        	Integer count= repositoryCustomerMaintenanceOptions.getCountByCreditCardSetupCardId(dtoCreditCardSetUp.getCardId());
            return count==null || count==0 ?false:true;
    }
	
	public boolean deleteMultipleBankSetup(DtoBankSetUp dtoBankSetUp)
    {   
		  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
          try
          {
        	 repositoryBankSetup.deleteMultipleBankSetup(dtoBankSetUp.getBankIds(), true, loggedInUserId);
             return true;
          }
          catch(Exception e)
          {
               return false;
          }
    }
	
	public boolean checkRelationOfBankSetup(DtoBankSetUp dtoBankSetUp)
    {   
         Integer count= repositoryCheckBookMaintenance.getCountByBankSetupBankId(dtoBankSetUp.getBankId());
         return count==null || count==0?false:true;
    }
	
	public boolean deleteMultipleConfigureModule(DtoModuleConfiguration dtoModuleConfiguration)
    {   
		  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
          try
          {
        	 repositoryModuleConfiguration.deleteMultipleByModuleSeriesNumber(dtoModuleConfiguration.getSeriesIds(), true, loggedInUserId);
             return true;
          }
          catch(Exception e)
          {
               return false;
          }
    }
	
	public boolean checkRelationOfConfigureModule(DtoModuleConfiguration dtoModuleConfiguration)
    {   
		Integer count=repositoryGLConfigurationAuditTrialCodes.getCountByModulesConfiguration(dtoModuleConfiguration.getSeriesId());
            if(count==null || count==0){
            	count=repositoryMasterPostingAccountsSequence.getCountByModulesConfiguration(dtoModuleConfiguration.getSeriesId());
            	 if(count==null || count==0){
            			 return false;
            	 }
            }
         return true;
    }
	
	public boolean deleteMultipleShipmentMethodSetup(DtoShipmentMethodSetup dtoShipmentMethodSetup)
    {   
		  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
          try
          {
        	  repositoryShipmentMethodSetup.deleteMultipleByShipMentMethodId(dtoShipmentMethodSetup.getShipmentMethodIds(), true,loggedInUserId);
             return true;
          }
          catch(Exception e)
          {
               return false;
          }
    }
	
	public boolean checkRelationOfShipmentMethodSetup(DtoShipmentMethodSetup dtoShipmentMethodSetup)
    {   
		Integer count= repositoryCustomerClassesSetup.getCountByShipmentMethodSetup(dtoShipmentMethodSetup.getShipmentMethodId());
            if(count==null || count==0){
            	count= repositoryCustomerMaintenanceOptions.getCountByShipmentMethodSetup(dtoShipmentMethodSetup.getShipmentMethodId());
            	 if(count==null || count==0){
            		 count=repositoryVendorClassesSetup.getCountByShipmentMethodSetup(dtoShipmentMethodSetup.getShipmentMethodId());
            		 if(count==null || count==0)
            		 {
            			 count= repositoryVendorMaintenanceOptions.getCountByShipmentMethodSetup(dtoShipmentMethodSetup.getShipmentMethodId());
            			if(count==null || count==0){
            				 return false;
            			}
            		 }
            	 }
            }
         return true;
    }
	
	public boolean deleteMultipleVatDetailSetup(DtoVatDetailSetup dtoVatDetailSetup)
    {   
		  int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
          try
          {
        	  repositoryVATSetup.deleteMultipleByVATSetup(dtoVatDetailSetup.getVatScheduleIds(), true,loggedInUserId);
             return true;
          }
          catch(Exception e)
          {
               return false;
          }
    }
	
	public boolean checkRelationOfVatDetailSetup(DtoVatDetailSetup dtoVatDetailSetup)
    {   
		/*Integer count= repositoryCustomerClassesSetup.getCountByVatSetup(dtoVatDetailSetup.getVatScheduleId());
        if(count==null || count==0){
            	count= repositoryCustomerMaintenanceOptions.getCountByShipmentMethodSetup(dtoShipmentMethodSetup.getShipmentMethodId());
            	 if(count==null || count==0){
            		 count=repositoryVendorClassesSetup.getCountByShipmentMethodSetup(dtoShipmentMethodSetup.getShipmentMethodId());
            		 if(count==null || count==0)
            		 {
            			 count= repositoryVendorMaintenanceOptions.getCountByShipmentMethodSetup(dtoShipmentMethodSetup.getShipmentMethodId());
            			if(count==null || count==0){
            				 return false;
            			}
            		 }
            	 }
          }*/
         return true;
    }
	
	public DtoVatDetailSetup getSalePurchaseAmountByAccountNumber(String accountTableRowIndex)
    { 
		DtoVatDetailSetup dtoVatDetailSetup=new DtoVatDetailSetup();
		
		
		//Get Date for past year
		Calendar calendar= Calendar.getInstance();
		int currentYear=calendar.get(Calendar.YEAR);
		calendar.set(Calendar.YEAR,currentYear-1);	
		calendar.set(Calendar.DAY_OF_YEAR, 1);
		Date startDate = calendar.getTime();
		
		calendar.set(Calendar.YEAR,currentYear-1);	
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.DAY_OF_MONTH, 31);
		Date endDate = calendar.getTime();
		
		//Get Date for year to date
		
		calendar.set(Calendar.YEAR,currentYear);	
		calendar.set(Calendar.DAY_OF_YEAR, 1);
		Date startDateYTD = calendar.getTime();
		
		Date endDateYTD = new Date();
		
		Double totalSalePurchaseAmount=0.0;
		Double totalSalePurchaseTaxAmount=0.0;
		Double totalSalePurchaseApplyAmount=0.0;
		
		Double totalSalePurchaseAmountYTD=0.0;
		Double totalSalePurchaseTaxAmountYTD=0.0;
		Double totalSalePurchaseApplyAmountYTD=0.0;
		Double totalCreditAmountOfArSales=null;
		Double totalCreditAmountofArTax=null;
		Double totalDebitAmountOfApPurchase=null;
		Double totalDebitAmountofApTax=null;
		
		//Get Amount for past year
		
		List<String> arytdOpenTransactionNumberList= repositoryARYTDOpenTransaction.findByArTransactionDateBetween(startDate,endDate);
		if(arytdOpenTransactionNumberList!=null && !arytdOpenTransactionNumberList.isEmpty())
		{
			totalCreditAmountOfArSales = repositoryARYTDOpenTransactionDistribution.getTotalCreditAmountByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), ARDistributionType.SALS.getIndex(), arytdOpenTransactionNumberList);
			totalCreditAmountofArTax = repositoryARYTDOpenTransactionDistribution.getTotalCreditAmountByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), ARDistributionType.TAXS.getIndex(), arytdOpenTransactionNumberList);
			arytdOpenTransactionNumberList=repositoryARYTDOpenTransactionDistribution.getTransactionNumberByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), ARDistributionType.SALS.getIndex(),
					ARDistributionType.TAXS.getIndex(), arytdOpenTransactionNumberList);
		}
		
		List<String> apytdOpenTransactionNumberList= repositoryAPYTDOpenTransaction.findByApTransactionDateBetween(startDate, endDate);
		if(apytdOpenTransactionNumberList!=null && !apytdOpenTransactionNumberList.isEmpty())
		{
			totalDebitAmountOfApPurchase=repositoryAPYTDOpenTransactionDistribution.getTotalDebitAmountByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), APDistributionTypeConstant.PURH.getIndex(), apytdOpenTransactionNumberList);
			totalDebitAmountofApTax=repositoryAPYTDOpenTransactionDistribution.getTotalDebitAmountByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), APDistributionTypeConstant.TAXS.getIndex(), apytdOpenTransactionNumberList);
			apytdOpenTransactionNumberList=repositoryAPYTDOpenTransactionDistribution.getTransactionNumberByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), APDistributionTypeConstant.PURH.getIndex(),
					APDistributionTypeConstant.TAXS.getIndex(), apytdOpenTransactionNumberList);
		}
	
		totalCreditAmountOfArSales=UtilRoundDecimal.roundDecimalValue(totalCreditAmountOfArSales);
		totalCreditAmountofArTax=UtilRoundDecimal.roundDecimalValue(totalCreditAmountofArTax);
		totalDebitAmountOfApPurchase=UtilRoundDecimal.roundDecimalValue(totalDebitAmountOfApPurchase);
		totalDebitAmountofApTax=UtilRoundDecimal.roundDecimalValue(totalDebitAmountofApTax);
		
		totalSalePurchaseAmount=totalCreditAmountOfArSales+totalDebitAmountOfApPurchase;
		totalSalePurchaseTaxAmount=totalCreditAmountofArTax+totalDebitAmountofApTax;
		
		Double arTotalApplyAmount=null;
	    Double apTotalApplyAmount=null;
		if(arytdOpenTransactionNumberList!=null && !arytdOpenTransactionNumberList.isEmpty()){
			arTotalApplyAmount=repositoryARApplyDocumentsPayments.getApplyAmountofTransactionNumbers(arytdOpenTransactionNumberList);
		}
		
		if(apytdOpenTransactionNumberList!=null && !apytdOpenTransactionNumberList.isEmpty()){
			apTotalApplyAmount=repositoryAPApplyDocumentsPayments.getApplyAmountByApTransactionNumber(apytdOpenTransactionNumberList);
		}
		
		totalSalePurchaseApplyAmount=UtilRoundDecimal.roundDecimalValue(arTotalApplyAmount)+UtilRoundDecimal.roundDecimalValue(apTotalApplyAmount);
		
		
		//Get Amount for year to Date
				 arytdOpenTransactionNumberList= repositoryARYTDOpenTransaction.findByArTransactionDateBetween(startDateYTD,endDateYTD);
				if(arytdOpenTransactionNumberList!=null && !arytdOpenTransactionNumberList.isEmpty())
				{
					totalCreditAmountOfArSales = repositoryARYTDOpenTransactionDistribution.getTotalCreditAmountByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), ARDistributionType.SALS.getIndex(), arytdOpenTransactionNumberList);
					totalCreditAmountofArTax = repositoryARYTDOpenTransactionDistribution.getTotalCreditAmountByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), ARDistributionType.TAXS.getIndex(), arytdOpenTransactionNumberList);
					arytdOpenTransactionNumberList=repositoryARYTDOpenTransactionDistribution.getTransactionNumberByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), ARDistributionType.SALS.getIndex(),
							ARDistributionType.TAXS.getIndex(), arytdOpenTransactionNumberList);
				}
				
				 apytdOpenTransactionNumberList= repositoryAPYTDOpenTransaction.findByApTransactionDateBetween(startDateYTD, endDateYTD);
				if(apytdOpenTransactionNumberList!=null && !apytdOpenTransactionNumberList.isEmpty())
				{
					totalDebitAmountOfApPurchase=repositoryAPYTDOpenTransactionDistribution.getTotalDebitAmountByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), APDistributionTypeConstant.PURH.getIndex(), apytdOpenTransactionNumberList);
					totalDebitAmountofApTax=repositoryAPYTDOpenTransactionDistribution.getTotalDebitAmountByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), APDistributionTypeConstant.TAXS.getIndex(), apytdOpenTransactionNumberList);
					apytdOpenTransactionNumberList=repositoryAPYTDOpenTransactionDistribution.getTransactionNumberByAccountNumberAndTransactionType(Integer.parseInt(accountTableRowIndex), APDistributionTypeConstant.PURH.getIndex(),APDistributionTypeConstant.TAXS.getIndex(), apytdOpenTransactionNumberList);
				}
			
				totalCreditAmountOfArSales=UtilRoundDecimal.roundDecimalValue(totalCreditAmountOfArSales);
				totalCreditAmountofArTax=UtilRoundDecimal.roundDecimalValue(totalCreditAmountofArTax);
				totalDebitAmountOfApPurchase=UtilRoundDecimal.roundDecimalValue(totalDebitAmountOfApPurchase);
				totalDebitAmountofApTax=UtilRoundDecimal.roundDecimalValue(totalDebitAmountofApTax);
				totalSalePurchaseAmountYTD=totalCreditAmountOfArSales+totalDebitAmountOfApPurchase;
				totalSalePurchaseTaxAmountYTD=totalCreditAmountofArTax+totalDebitAmountofApTax;
				
				
				if(arytdOpenTransactionNumberList!=null && !arytdOpenTransactionNumberList.isEmpty()){
					arTotalApplyAmount=repositoryARApplyDocumentsPayments.getApplyAmountofTransactionNumbers(arytdOpenTransactionNumberList);
				}
				
				if(apytdOpenTransactionNumberList!=null && !apytdOpenTransactionNumberList.isEmpty()){
					apTotalApplyAmount=repositoryAPApplyDocumentsPayments.getApplyAmountByApTransactionNumber(apytdOpenTransactionNumberList);
				}
				
				totalSalePurchaseApplyAmountYTD=UtilRoundDecimal.roundDecimalValue(arTotalApplyAmount)+UtilRoundDecimal.roundDecimalValue(apTotalApplyAmount);
		
				dtoVatDetailSetup.setLastYearTotalSalesPurchase(totalSalePurchaseAmount);
				dtoVatDetailSetup.setLastYearSalesPurchaseTaxes(totalSalePurchaseTaxAmount);
				dtoVatDetailSetup.setYtdTotalSalesPurchase(totalSalePurchaseAmountYTD);
				dtoVatDetailSetup.setYtdTotalSalesPurchaseTaxes(totalSalePurchaseTaxAmountYTD);
				dtoVatDetailSetup.setTotalApplyAmount(totalSalePurchaseApplyAmount);
				dtoVatDetailSetup.setTotalApplyAmountYtd(totalSalePurchaseApplyAmountYTD);
				
		return dtoVatDetailSetup;
		
    }
	
	
}
