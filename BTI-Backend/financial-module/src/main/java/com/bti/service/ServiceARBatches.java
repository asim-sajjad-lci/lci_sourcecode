/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.MessageLabel;
import com.bti.model.ARBatches;
import com.bti.model.ARCashReceipt;
import com.bti.model.ARCashReceiptDistribution;
import com.bti.model.ARTransactionEntry;
import com.bti.model.ARTransactionEntryDistribution;
import com.bti.model.BatchTransactionType;
import com.bti.model.dto.DtoARCashReceipt;
import com.bti.model.dto.DtoARTransactionEntry;
import com.bti.model.dto.DtoBatches;
import com.bti.model.dto.DtoBatchesTransactionType;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryARBatches;
import com.bti.repository.RepositoryARCashReceipt;
import com.bti.repository.RepositoryARCashReceiptDistribution;
import com.bti.repository.RepositoryARTransactionEntry;
import com.bti.repository.RepositoryARTransactionEntryDistribution;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceARBatches Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceARBatches")
public class ServiceARBatches {

	private static final Logger LOG = Logger.getLogger(ServiceARBatches.class);

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryARBatches repositoryARBatches;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;
	
	@Autowired
	RepositoryARTransactionEntry repositoryARTransactionEntry;
 
	@Autowired
	RepositoryARCashReceipt repositoryARCashReceipt;
	
	@Autowired
	RepositoryARCashReceiptDistribution repositoryARCashReceiptDistribution;
	
	@Autowired
	ServiceARCashReceiptEntry serviceARCashReceiptEntry;
	
	@Autowired
	ServiceARTransactionEntry serviceARTransactionEntry;
	
	@Autowired
	RepositoryARTransactionEntryDistribution repositoryARTransactionEntryDistribution;

	public DtoBatches saveBatches(DtoBatches dtoBatches) {
		LOG.info("inside saveBatches method");
		int langId = serviceHome.getLanngugaeId();

		ARBatches arBatches = new ARBatches();
		arBatches.setBatchID(dtoBatches.getBatchId());
		arBatches.setBatchDescription(dtoBatches.getDescription());
		BatchTransactionType batchTransactionType = repositoryBatchTransactionType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoBatches.getTransactionTypeId(), langId, false);
		if (batchTransactionType != null) {
			arBatches.setBatchTransactionType(batchTransactionType);
		}
		
		arBatches.setTotalAmountTransactionInBatch(dtoBatches.getTotalTransactions());
		arBatches.setTotalNumberTransactionInBatch(dtoBatches.getQuantityTotal());
		if(UtilRandomKey.isNotBlank(dtoBatches.getPostingDate())){
			arBatches.setPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoBatches.getPostingDate()));
		}
		arBatches = repositoryARBatches.saveAndFlush(arBatches);
		dtoBatches = new DtoBatches(arBatches, batchTransactionType);
		return dtoBatches;
	}

	public Boolean updateBatches(DtoBatches dtoBatches) {
		boolean flag;
		int langId = serviceHome.getLanngugaeId();
		ARBatches arBatches = repositoryARBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(
				dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId(), false);

		if (arBatches == null) {
			arBatches = new ARBatches();
			arBatches.setBatchID(dtoBatches.getBatchId());
		}

		arBatches.setBatchDescription(dtoBatches.getDescription());
		BatchTransactionType batchTransactionType = repositoryBatchTransactionType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoBatches.getTransactionTypeId(), langId, false);
		if (batchTransactionType != null) {
			arBatches.setBatchTransactionType(batchTransactionType);
		}

		arBatches.setTotalAmountTransactionInBatch(dtoBatches.getTotalTransactions());
		arBatches.setTotalNumberTransactionInBatch(dtoBatches.getQuantityTotal());

		if (UtilRandomKey.isNotBlank(dtoBatches.getPostingDate())) {
			arBatches.setPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoBatches.getPostingDate()));
		} else {
			arBatches.setPostingDate(null);
		}

		repositoryARBatches.saveAndFlush(arBatches);
		flag = true;
		return flag;
	}

	public DtoBatches getBatchesByBatchAndTransactionTypeId(String batchId, int transactionTypeId) 
	{
		DtoBatches dtoBatches = null;
		int langId = serviceHome.getLanngugaeId();
		try 
		{
			ARBatches arBatches = repositoryARBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(batchId,
					transactionTypeId, false);
			if (arBatches != null) 
			{
				BatchTransactionType batchTransactionType = arBatches.getBatchTransactionType();
				if (batchTransactionType != null && batchTransactionType.getLanguage().getLanguageId() != langId) {
					batchTransactionType = repositoryBatchTransactionType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
							batchTransactionType.getTypeId(), langId, false);
				}
				dtoBatches = new DtoBatches(arBatches, batchTransactionType);
				DtoBatches totalQuantityAndAmount= getBatchTotalTrasactionsByTransactionType(dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId());
				dtoBatches.setTotalTransactions(totalQuantityAndAmount.getTotalTransactions());
				dtoBatches.setQuantityTotal(totalQuantityAndAmount.getQuantityTotal());
			}
		}
		catch (Exception e) 
		{
			LOG.info(e.getStackTrace());
		}
		return dtoBatches;
	}

	public DtoSearch getAllBatches(DtoSearch dtoSearchs) {
		int langId = serviceHome.getLanngugaeId();
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositoryARBatches.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		List<ARBatches> arBatchesList = null;
		List<DtoBatches> dtoBatchesList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createDate");
			arBatchesList = repositoryARBatches.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			arBatchesList = repositoryARBatches.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (arBatchesList != null) {
			for (ARBatches arBatches : arBatchesList) {

				BatchTransactionType batchTransactionType = arBatches.getBatchTransactionType();
				if (batchTransactionType != null && batchTransactionType.getLanguage().getLanguageId() != langId) {
					batchTransactionType = repositoryBatchTransactionType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
							batchTransactionType.getTypeId(), langId, false);
				}
				DtoBatches dtoBatches = new DtoBatches(arBatches, batchTransactionType);
				DtoBatches totalQuantityAndAmount= getBatchTotalTrasactionsByTransactionType(dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId());
				dtoBatches.setTotalTransactions(totalQuantityAndAmount.getTotalTransactions());
				dtoBatches.setQuantityTotal(totalQuantityAndAmount.getQuantityTotal());
				dtoBatchesList.add(dtoBatches);
			}
		}
		dtoSearch.setRecords(dtoBatchesList);
		return dtoSearch;
	}

	public List<DtoBatchesTransactionType> getAllBatchesTransaction() {
		int langId = serviceHome.getLanngugaeId();
		List<DtoBatchesTransactionType> dtoBatchesTransactionTypesList = new ArrayList<>();

		List<BatchTransactionType> batchTransactionTypesList = repositoryBatchTransactionType
				.findByTransactionModuleAndLanguageLanguageIdAndIsDeleted("AR", langId, false);
		if (batchTransactionTypesList != null) {
			batchTransactionTypesList.forEach(batchTransactionType -> dtoBatchesTransactionTypesList.add(new DtoBatchesTransactionType(batchTransactionType)));
		}

		return dtoBatchesTransactionTypesList;
	}

	public boolean deleteBatchesByBatchId(DtoBatches dtoBatches) {
		try {
			ARBatches arBatches = repositoryARBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(
					dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId(), false);
			if (arBatches != null) {
				// check
				List<ARTransactionEntry> arTransactionEntryList = repositoryARTransactionEntry
						.findByArBatchesBatchID(dtoBatches.getBatchId());
				if (arTransactionEntryList != null && !arTransactionEntryList.isEmpty()) {
					for (ARTransactionEntry arTransactionEntry : arTransactionEntryList) {
						List<ARTransactionEntryDistribution> arTransactionEntryDistributionList=repositoryARTransactionEntryDistribution.
								findByArTransactionNumber(arTransactionEntry.getArTransactionNumber());
						 if(!arTransactionEntryDistributionList.isEmpty()){
							 repositoryARTransactionEntryDistribution.deleteInBatch(arTransactionEntryDistributionList); 
						 }
					}
					repositoryARTransactionEntry.deleteInBatch(arTransactionEntryList);
				}

				List<ARCashReceipt> cashReceiptList = repositoryARCashReceipt
						.findByArBatchesBatchID(dtoBatches.getBatchId());
				if (cashReceiptList != null && !cashReceiptList.isEmpty()) {
					for (ARCashReceipt arCashReceipt : cashReceiptList) {
						List<ARCashReceiptDistribution> arCashReceiptDistributionList = repositoryARCashReceiptDistribution
								.findByCashReceiptNumber(arCashReceipt.getCashReceiptNumber());
						if (arCashReceiptDistributionList != null && !arCashReceiptDistributionList.isEmpty()) {
							repositoryARCashReceiptDistribution.deleteInBatch(arCashReceiptDistributionList);
						}
					}
					repositoryARCashReceipt.deleteInBatch(cashReceiptList);
				}

				repositoryARBatches.delete(arBatches);
			}
			return true;
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}
  
	public DtoBatches getBatchTotalTrasactionsByTransactionType(String batchId, int transactionTypeId) 
	{
		List<Object[]> arrayList=null;
		DtoBatches dtoBatches= new DtoBatches();
		dtoBatches.setQuantityTotal(0);
		dtoBatches.setTotalTransactions(0.0);
		if (transactionTypeId == BatchTransactionTypeConstant.AR_CASH_RECEIPT.getIndex()) 
		{
			arrayList = repositoryARCashReceipt.getTotalAmountAndTotalCountByBatchId(batchId);
		}
		
		if (transactionTypeId == BatchTransactionTypeConstant.AR_TRANSACTION_ENTRY.getIndex()) {
			arrayList = repositoryARTransactionEntry.getTotalAmountAndTotalCountByBatchId(batchId);
        }
		
		if (arrayList != null && !arrayList.isEmpty()) {
			for (Object[] object : arrayList) {
				Long totalQuantiy = (long) object[0];
				double totalTransctionBalance = (double) object[1];
				dtoBatches.setQuantityTotal(0);
				if(totalQuantiy!=null){
					dtoBatches.setQuantityTotal(totalQuantiy.intValue());
				}
				dtoBatches.setTotalTransactions(totalTransctionBalance);
			}
		}
		// Action Pending 
		return dtoBatches;
	}

	public DtoBatches postBatchTransaction(DtoBatches dtoBatches) throws ScriptException {
		
		if (dtoBatches.getTransactionTypeId() == BatchTransactionTypeConstant.AR_CASH_RECEIPT.getIndex()) 
		{
			List<ARCashReceipt> arCashReceiptList = repositoryARCashReceipt.findByArBatchesBatchID(dtoBatches.getBatchId());
			if(arCashReceiptList!=null && !arCashReceiptList.isEmpty()){
				for (ARCashReceipt arCashReceipt : arCashReceiptList) {
					DtoARCashReceipt dtoARCashReceipt= serviceARCashReceiptEntry.postCashReceiptEntry(new DtoARCashReceipt(arCashReceipt));
					if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getMessageType())){
				    	dtoBatches.setMessageType(MessageLabel.SOME_TRANSACTION_IS_NOT_POSTED);
				    }
				}
			}
		}
		
        if (dtoBatches.getTransactionTypeId() == BatchTransactionTypeConstant.AR_TRANSACTION_ENTRY.getIndex()) {
        	
        	List<ARTransactionEntry> arTransactionEntryList = repositoryARTransactionEntry.findByArBatchesBatchID(dtoBatches.getBatchId());
			if(arTransactionEntryList!=null && !arTransactionEntryList.isEmpty()){
				for (ARTransactionEntry arTransactionEntry : arTransactionEntryList) {
					DtoARTransactionEntry dtoTransactionEntry=serviceARTransactionEntry.postARTransactionEntry(new DtoARTransactionEntry(arTransactionEntry));
				    if(UtilRandomKey.isNotBlank(dtoTransactionEntry.getMessageType())){
				    	dtoBatches.setMessageType(MessageLabel.SOME_TRANSACTION_IS_NOT_POSTED);
				    }
				}
			}
        }
		
		return dtoBatches;
	}
	
	public List<DtoBatches> getBatchesByTransactionTypeId(DtoBatches dtoBatches) {
		List<ARBatches> arBatchesList = repositoryARBatches.findByBatchTransactionTypeTypeIdAndIsDeleted(dtoBatches.getTransactionTypeId(), false);
		List<DtoBatches> dtoBatchesList = new ArrayList<>();
		if (arBatchesList != null && !arBatchesList.isEmpty()) {
			for (ARBatches arBatches : arBatchesList) 
			{
				dtoBatches= new DtoBatches();
				dtoBatches.setTransactionTypeId(dtoBatches.getTransactionTypeId());
				dtoBatches.setBatchId(arBatches.getBatchID());
				dtoBatches.setDescription(arBatches.getBatchDescription()!=null?arBatches.getBatchDescription():"");
				dtoBatchesList.add(dtoBatches);
			}
		}
		return dtoBatchesList;
	}
	
}
