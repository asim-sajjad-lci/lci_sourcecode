/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.MessageLabel;
import com.bti.model.APBatches;
import com.bti.model.APManualPayment;
import com.bti.model.APManualPaymentDistribution;
import com.bti.model.APTransactionDistribution;
import com.bti.model.APTransactionEntry;
import com.bti.model.BatchTransactionType;
import com.bti.model.dto.DtoAPManualPayment;
import com.bti.model.dto.DtoAPTransactionEntry;
import com.bti.model.dto.DtoBatches;
import com.bti.model.dto.DtoBatchesTransactionType;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryAPBatches;
import com.bti.repository.RepositoryAPManualPayment;
import com.bti.repository.RepositoryAPManualPaymentDistribution;
import com.bti.repository.RepositoryAPTransactionDistribution;
import com.bti.repository.RepositoryAPTransactionEntry;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceAccountPayableTransaction Name of Project: BTI Created
 * on: Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceAPBatches")
public class ServiceAPBatches {

	private static final Logger LOG = Logger.getLogger(ServiceAPBatches.class);

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryAPBatches repositoryAPBatches;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;
	
	@Autowired
	RepositoryAPManualPayment repositoryAPManualPayment;

	@Autowired
	RepositoryAPManualPaymentDistribution repositoryAPManualPaymentDistribution;
	
	@Autowired
	RepositoryAPTransactionEntry repositoryAPTransactionEntry;
	
	@Autowired
	RepositoryAPTransactionDistribution repositoryAPTransactionDistribution;
	
	@Autowired
	ServiceAPManualPayment serviceAPManualPayment;
	
	@Autowired
	ServiceAPTransactionEntry serviceAPTransactionEntry;

	public DtoBatches saveBatches(DtoBatches dtoBatches) 
	{
		LOG.info("inside saves batches method");
		int langId = serviceHome.getLanngugaeId();
		APBatches apBatches = new APBatches();
		apBatches.setBatchID(dtoBatches.getBatchId());
		apBatches.setBatchDescription(dtoBatches.getDescription());
		BatchTransactionType batchTransactionType = repositoryBatchTransactionType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoBatches.getTransactionTypeId(),langId,false);
		if (batchTransactionType != null) {
			apBatches.setBatchTransactionType(batchTransactionType);
		}
		apBatches.setTotalAmountTransactionInBatch(dtoBatches.getTotalTransactions());
		apBatches.setTotalNumberTransactionInBatch(dtoBatches.getQuantityTotal());
		if (UtilRandomKey.isNotBlank(dtoBatches.getPostingDate())) {
			apBatches.setPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoBatches.getPostingDate()));
		}
		apBatches = repositoryAPBatches.saveAndFlush(apBatches);
		dtoBatches = new DtoBatches(apBatches, batchTransactionType);
		return dtoBatches;
	}

	public Boolean updateBatches(DtoBatches dtoBatches) {
		boolean flag;
		int langId = serviceHome.getLanngugaeId();
		APBatches apBatches = repositoryAPBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(
				dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId(), false);
		if (apBatches == null) {
			apBatches = new APBatches();
			apBatches.setBatchID(dtoBatches.getBatchId());
		}
		
		apBatches.setBatchDescription(dtoBatches.getDescription());		
		BatchTransactionType batchTransactionType = repositoryBatchTransactionType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoBatches.getTransactionTypeId(), langId, false);
		if (batchTransactionType != null) {
			apBatches.setBatchTransactionType(batchTransactionType);
		}

		apBatches.setTotalAmountTransactionInBatch(dtoBatches.getTotalTransactions());
		apBatches.setTotalNumberTransactionInBatch(dtoBatches.getQuantityTotal());
		if (UtilRandomKey.isNotBlank(dtoBatches.getPostingDate())) {
			apBatches.setPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoBatches.getPostingDate()));
		} else {
			apBatches.setPostingDate(null);
		}
		repositoryAPBatches.saveAndFlush(apBatches);
		
		flag = true;
		return flag;
	}

	public DtoBatches getBatchesByBatchAndTransactionTypeId(String batchId, int transactionTypeId) {
		DtoBatches dtoBatches = null;
		int langId = serviceHome.getLanngugaeId();
		try {
			APBatches apBatches = repositoryAPBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(batchId,
					transactionTypeId, false);
			if (apBatches != null) {
				BatchTransactionType batchTransactionType = apBatches.getBatchTransactionType();
				if (batchTransactionType != null && batchTransactionType.getLanguage().getLanguageId() != langId) {
					batchTransactionType = repositoryBatchTransactionType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
							batchTransactionType.getTypeId(), langId, false);
				}
				dtoBatches = new DtoBatches(apBatches, batchTransactionType);
			}
		} catch (Exception e) {
			LOG.info(e.getStackTrace());
		}
		return dtoBatches;
	}

	public DtoSearch getAllBatches(DtoSearch dtoSearchs) {
		int langId = serviceHome.getLanngugaeId();
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositoryAPBatches.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		List<APBatches> apBatchesList = null;
		List<DtoBatches> dtoBatchesList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			apBatchesList = repositoryAPBatches.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			apBatchesList = repositoryAPBatches.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (apBatchesList != null) {
			for (APBatches apBatches : apBatchesList) {

				BatchTransactionType batchTransactionType = apBatches.getBatchTransactionType();
				if (batchTransactionType != null && batchTransactionType.getLanguage().getLanguageId() != langId) {
					batchTransactionType = repositoryBatchTransactionType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
							batchTransactionType.getTypeId(), langId, false);
				}
				DtoBatches dtoBatches = new DtoBatches(apBatches, batchTransactionType);
				DtoBatches totalQuantityAndAmount =getBatchTotalTrasactionsByTransactionType(dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId());
				dtoBatches.setTotalTransactions(totalQuantityAndAmount.getTotalTransactions());
				dtoBatches.setQuantityTotal(totalQuantityAndAmount.getQuantityTotal());
				dtoBatchesList.add(dtoBatches);
			}
		}
		dtoSearch.setRecords(dtoBatchesList);
		return dtoSearch;
	}

	public List<DtoBatchesTransactionType> getAllBatchesTransaction() {
		int langId = serviceHome.getLanngugaeId();
		List<DtoBatchesTransactionType> dtoBatchesTransactionTypesList = new ArrayList<>();

		List<BatchTransactionType> batchTransactionTypesList = repositoryBatchTransactionType
				.findByTransactionModuleAndLanguageLanguageIdAndIsDeleted("AP", langId, false);
		if (batchTransactionTypesList != null) {
			batchTransactionTypesList.forEach(batchTransactionType -> dtoBatchesTransactionTypesList
					.add(new DtoBatchesTransactionType(batchTransactionType)));
		}

		return dtoBatchesTransactionTypesList;
	}

	public boolean deleteBatchesByBatchId(DtoBatches dtoBatches) {
		try {
			APBatches apBatches = repositoryAPBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(
					dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId(), false);
			if (apBatches != null) {
				// check
				List<APTransactionEntry> apTransactionEntryList = repositoryAPTransactionEntry
						.findByApBatchesBatchID(dtoBatches.getBatchId());
				if (apTransactionEntryList != null && !apTransactionEntryList.isEmpty()) {
					for (APTransactionEntry apTransactionEntry : apTransactionEntryList) {
						List<APTransactionDistribution> apTransactionDistributionList=repositoryAPTransactionDistribution.
								findByApTransactionNumber(apTransactionEntry.getApTransactionNumber());
						 if(!apTransactionDistributionList.isEmpty()){
							 repositoryAPTransactionDistribution.deleteInBatch(apTransactionDistributionList); 
						 }
					}
					repositoryAPTransactionEntry.deleteInBatch(apTransactionEntryList);
				}
				
				List<APManualPayment> apManualPaymenttList = repositoryAPManualPayment
						.findByApBatchesBatchID(dtoBatches.getBatchId());
				if (apManualPaymenttList != null && !apManualPaymenttList.isEmpty()) {
					for (APManualPayment apManualPayment : apManualPaymenttList) {
						List<APManualPaymentDistribution> distributionList = repositoryAPManualPaymentDistribution
								.findByManualPaymentNumber(apManualPayment.getManualPaymentNumber());
						if (distributionList != null && !distributionList.isEmpty()) {
							repositoryAPManualPaymentDistribution.deleteInBatch(distributionList);
						}
					}
					repositoryAPManualPayment.deleteInBatch(apManualPaymenttList);
				}
				repositoryAPBatches.delete(apBatches);
			}
			return true;
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}

	public DtoBatches getBatchTotalTrasactionsByTransactionType(String batchId, int transactionTypeId) {
		List<Object[]> arrayList=null;
		DtoBatches dtoBatches= new DtoBatches();
		dtoBatches.setQuantityTotal(0);
		dtoBatches.setTotalTransactions(0.0);
		if (transactionTypeId == BatchTransactionTypeConstant.AP_CASH_RECEIPT.getIndex()) 
		{
			arrayList = repositoryAPManualPayment.getTotalAmountAndTotalCountByBatchId(batchId);
		}
		if (transactionTypeId == BatchTransactionTypeConstant.AP_TRANSACTION_ENTRY.getIndex()) {
			arrayList = repositoryAPTransactionEntry.getTotalAmountAndTotalCountByBatchId(batchId);
        }
		
		if (arrayList != null && !arrayList.isEmpty()) {
			for (Object[] object : arrayList) {
				Long totalQuantiy = (long) object[0];
				double totalTransctionBalance = (double) object[1];
				dtoBatches.setQuantityTotal(0);
				if(totalQuantiy!=null){
					dtoBatches.setQuantityTotal(totalQuantiy.intValue());
				}
				dtoBatches.setTotalTransactions(totalTransctionBalance);
			}
		}
		 
		return dtoBatches;
	
	}

	public DtoBatches postBatchTransaction(DtoBatches dtoBatches) throws ScriptException {
		if (dtoBatches.getTransactionTypeId() == BatchTransactionTypeConstant.AP_CASH_RECEIPT.getIndex()) 
		{
			List<APManualPayment> apManualPaymentList = repositoryAPManualPayment.findByApBatchesBatchID(dtoBatches.getBatchId());
			if(apManualPaymentList!=null && !apManualPaymentList.isEmpty()){
				for (APManualPayment apManualPayment : apManualPaymentList) {
					DtoAPManualPayment dtoAPManualPayment=serviceAPManualPayment.postManualPaymentEntry(new DtoAPManualPayment(apManualPayment));
				    if(UtilRandomKey.isNotBlank(dtoAPManualPayment.getMessageType())){
				    	dtoBatches.setMessageType(MessageLabel.SOME_TRANSACTION_IS_NOT_POSTED);
				    }
				}
			}
		}
		
        if (dtoBatches.getTransactionTypeId() == BatchTransactionTypeConstant.AP_TRANSACTION_ENTRY.getIndex()) {
        	List<APTransactionEntry> apTransactionEntryList = repositoryAPTransactionEntry.findByApBatchesBatchID(dtoBatches.getBatchId());
			if(apTransactionEntryList!=null && !apTransactionEntryList.isEmpty()){
				for (APTransactionEntry apTransactionEntry : apTransactionEntryList) {
					DtoAPTransactionEntry dtoAPTransactionEntry = serviceAPTransactionEntry.postAPTransactionEntry(new DtoAPTransactionEntry(apTransactionEntry));
				   if(UtilRandomKey.isNotBlank(dtoAPTransactionEntry.getMessageType())){
					   dtoBatches.setMessageType(MessageLabel.SOME_TRANSACTION_IS_NOT_POSTED);
				   }
				}
			}
        }
		return dtoBatches;
	}

	public List<DtoBatches> getBatchesByTransactionTypeId(DtoBatches dtoBatches) {
		List<APBatches> apBatchesList = repositoryAPBatches
				.findByBatchTransactionTypeTypeIdAndIsDeleted(dtoBatches.getTransactionTypeId(), false);
		List<DtoBatches> dtoBatchesList = new ArrayList<>();
		if (apBatchesList != null && !apBatchesList.isEmpty()) {
			for (APBatches apBatches : apBatchesList) {
				dtoBatches = new DtoBatches();
				dtoBatches.setTransactionTypeId(dtoBatches.getTransactionTypeId());
				dtoBatches.setBatchId(apBatches.getBatchID());
				dtoBatches
						.setDescription(apBatches.getBatchDescription() != null ? apBatches.getBatchDescription() : "");
				dtoBatchesList.add(dtoBatches);
			}
		}
		return dtoBatchesList;
	}

}
