/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.script.ScriptException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.MessageLabel;
import com.bti.model.BatchTransactionType;
import com.bti.model.GLBankTransferDetails;
import com.bti.model.GLBankTransferDistribution;
import com.bti.model.GLBankTransferEntryCharges;
import com.bti.model.GLBankTransferHeader;
import com.bti.model.GLBatches;
import com.bti.model.GLCashReceiptBank;
import com.bti.model.GLCashReceiptDistribution;
import com.bti.model.GLClearingDetails;
import com.bti.model.GLClearingHeader;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.dto.DtoBatches;
import com.bti.model.dto.DtoBatchesTransactionType;
import com.bti.model.dto.DtoGLBankTransferHeader;
import com.bti.model.dto.DtoGLCashReceiptBank;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.repository.RepositoryGLBankTransferDetails;
import com.bti.repository.RepositoryGLBankTransferDistribution;
import com.bti.repository.RepositoryGLBankTransferEntryCharges;
import com.bti.repository.RepositoryGLBankTransferHeader;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLCashReceiptBank;
import com.bti.repository.RepositoryGLCashReceiptDistribution;
import com.bti.repository.RepositoryGLClearingDetail;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceGLBatches Name of Project: BTI Created on: Dec 04, 2017
 * Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceGLBatches")
public class ServiceGLBatches {

	private static final Logger LOG = Logger.getLogger(ServiceGLBatches.class);

	@Autowired
	ServiceHome serviceHome;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;
	
	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;
	
	@Autowired
	RepositoryGLCashReceiptBank repositoryGLCashReceiptBank;
	
	@Autowired
	RepositoryGLBankTransferHeader repositoryGLBankTransferHeader;
	
	@Autowired
	RepositoryGLClearingDetail repositoryGLClearingDetail;
	
	@Autowired
	RepositoryGLCashReceiptDistribution repositoryGLCashReceiptDistribution;
	
	@Autowired
	ServiceGLCashReceiptEntry serviceGLCashReceiptEntry;
	
	@Autowired
	ServiceGLClearingGeneralEntry serviceGLClearingGeneralEntry;
	
	@Autowired
	ServiceGLJournalEntry serviceGLJournalEntry;
	
	@Autowired
	ServiceGLBankTransferEntry serviceGLBankTransferEntry;
	
	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;

	@Autowired
	RepositoryGLBankTransferDistribution repositoryGLBankTransferDistribution;
	
	@Autowired
	RepositoryGLBankTransferEntryCharges repositoryGLBankTransferEntryCharges;
	
	@Autowired
	RepositoryGLBankTransferDetails repositoryGLBankTransferDetails;
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	

	/**
	 * @description this service will use to save batches
	 * @param dtoBatches
	 * @return
	 */
	public DtoBatches saveBatches(DtoBatches dtoBatches) {
		LOG.info("inside saveBatches method");
		int langId = serviceHome.getLanngugaeId();

		GLBatches glBatches = new GLBatches();

		glBatches.setBatchID(dtoBatches.getBatchId());
		glBatches.setBatchDescription(dtoBatches.getDescription());
		//BatchTransactionType batchTransactionType = repositoryBatchTransactionType
		//		.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoBatches.getTransactionTypeId(), langId, false);
		//if (batchTransactionType != null) {
		//	glBatches.setBatchTransactionType(batchTransactionType);
		//}
		glBatches.setTotalAmountTransactionInBatch(dtoBatches.getTotalTransactions()!=null?dtoBatches.getTotalTransactions(): 0);
		glBatches.setTotalNumberTransactionInBatch(dtoBatches.getQuantityTotal()!=null?dtoBatches.getQuantityTotal():0);
		if(UtilRandomKey.isNotBlank(dtoBatches.getPostingDate())){
			glBatches.setPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoBatches.getPostingDate()));
		}
		
		GLConfigurationAuditTrialCodes sourceDocument = repositoryGLConfigurationAuditTrialCodes.findBySeriesIndexAndIsDeleted(dtoBatches.getSourceDocumentID(), false);
		if(sourceDocument != null) {
			glBatches.setGlConfigurationAuditTrialCodes(sourceDocument);
		}
		glBatches = repositoryGLBatches.saveAndFlush(glBatches);
		dtoBatches = new DtoBatches(glBatches);
		return dtoBatches;
	}

	/**
	 * @description this service will use to updated batches
	 * @param dtoBatches
	 * @return
	 */
	public Boolean updateBatches(DtoBatches dtoBatches) 
	{
		boolean flag = false;
		int langId = serviceHome.getLanngugaeId();
		GLBatches glBatches = repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoBatches.getBatchId(),dtoBatches.getTransactionTypeId(), false);
		if (glBatches == null) {
			glBatches=new GLBatches();
			glBatches.setBatchID(dtoBatches.getBatchId());
		}
			
			glBatches.setBatchDescription(dtoBatches.getDescription());
			BatchTransactionType batchTransactionType = repositoryBatchTransactionType
					.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoBatches.getTransactionTypeId(), langId, false);
			
			if (batchTransactionType != null) {
				glBatches.setBatchTransactionType(batchTransactionType);
			}
			
			glBatches.setTotalAmountTransactionInBatch(dtoBatches.getTotalTransactions());
			glBatches.setTotalNumberTransactionInBatch(dtoBatches.getQuantityTotal());
			
			if(UtilRandomKey.isNotBlank(dtoBatches.getPostingDate())){
				glBatches.setPostingDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoBatches.getPostingDate()));
			}
			else{
				glBatches.setPostingDate(null);
			}

			repositoryGLBatches.saveAndFlush(glBatches);
			flag = true;

		return flag;
	}

	/**
	 * @description this service will use to get batch by batch Id
	 * @param batchId
	 * @return
	 */
	public DtoBatches getBatchesByBatchAndTransactionTypeId(String batchId,int transactionTypeId) {

		DtoBatches dtoBatches = null;
		try{
		int langId = serviceHome.getLanngugaeId();
		GLBatches glBatches = repositoryGLBatches.findByBatchIDAndIsDeleted(batchId, false);
		if (glBatches != null) {
			
			BatchTransactionType batchTransactionType = glBatches.getBatchTransactionType();
			if (batchTransactionType != null && batchTransactionType.getLanguage().getLanguageId() != langId) 
			{
				batchTransactionType = repositoryBatchTransactionType
						.findByTypeIdAndLanguageLanguageIdAndIsDeleted(batchTransactionType.getTypeId(), langId, false);
			}
			
			dtoBatches = new DtoBatches(glBatches, batchTransactionType);
			DtoBatches batchTransactionAndQuantity= getBatchTotalTrasactionsByTransactionType(dtoBatches.getBatchId(),dtoBatches.getTransactionTypeId());
			dtoBatches.setTotalTransactions(batchTransactionAndQuantity.getTotalTransactions());
			dtoBatches.setQuantityTotal(batchTransactionAndQuantity.getQuantityTotal());
			
		}
		}
		catch (Exception e) {
			LOG.info(e.getStackTrace());
		}

		return dtoBatches;
	}

	/**
	 * @description this service will use to get all batches
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch getAllBatches(DtoSearch dtoSearchs) {
		
		int langId = serviceHome.getLanngugaeId();
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositoryGLBatches.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		List<GLBatches> glBatchesList = null;
		List<DtoBatches> dtoBatchesList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
			glBatchesList = repositoryGLBatches.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			glBatchesList = repositoryGLBatches.predictiveSearch(dtoSearchs.getSearchKeyword());
		}
		if (glBatchesList != null) {
			for (GLBatches glBatches : glBatchesList) {

				BatchTransactionType batchTransactionType = glBatches.getBatchTransactionType();
				if (batchTransactionType != null && batchTransactionType.getLanguage().getLanguageId() != langId) {
					batchTransactionType = repositoryBatchTransactionType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(
							batchTransactionType.getTypeId(), langId, false);
				}
				
				
				DtoBatches dtoBatches = new DtoBatches(glBatches);
				DtoBatches batchTransactionAndQuantity= getBatchTotalTrasactionsByTransactionType(dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId());
				dtoBatches.setTotalTransactions(batchTransactionAndQuantity.getTotalTransactions());
				dtoBatches.setQuantityTotal(batchTransactionAndQuantity.getQuantityTotal());
				dtoBatchesList.add(dtoBatches);
			}
		}
		dtoSearch.setRecords(dtoBatchesList);
		return dtoSearch;
	}

	/**
	 * @description this service will use to get transaction of batches
	 * @return
	 */
	public List<DtoBatchesTransactionType> getAllBatchesTransaction() {
		int langId = serviceHome.getLanngugaeId();
		List<DtoBatchesTransactionType> dtoBatchesTransactionTypesList = null;

		List<BatchTransactionType> batchTransactionTypesList = repositoryBatchTransactionType
				.findByTransactionModuleAndLanguageLanguageIdAndIsDeleted("GL", langId, false);
		if (batchTransactionTypesList != null) {
			dtoBatchesTransactionTypesList = new ArrayList<>();
			for (BatchTransactionType batchTransactionType : batchTransactionTypesList) {
				dtoBatchesTransactionTypesList.add(new DtoBatchesTransactionType(batchTransactionType));
			}
		}

		return dtoBatchesTransactionTypesList;
	}

	public boolean deleteBatchesByBatchId(DtoBatches dtoBatches) {
		try {
			GLBatches glBatches = repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoBatches.getBatchId(), dtoBatches.getTransactionTypeId(), false);
			if (glBatches != null) {
				
				//check
				List<JournalEntryHeader>  journalEntryList = repositoryJournalEntryHeader.findByGlBatchesBatchIDAndIsDeleted(dtoBatches.getBatchId(), false);
				if(journalEntryList!=null && !journalEntryList.isEmpty())
				{
						for (JournalEntryHeader journalEntryHeader : journalEntryList) {
							List<JournalEntryDetails> detailList = repositoryJournalEntryDetail.findByJournalEntryHeaderJournalIDAndIsDeleted(journalEntryHeader.getJournalID(), false);
						    if(detailList!=null){
						    	repositoryJournalEntryDetail.deleteInBatch(detailList);
						    }
						}
						repositoryJournalEntryHeader.deleteInBatch(journalEntryList);
				}
				
				List<GLClearingHeader> glClearingHeaderList =  repositoryGLClearingHeader.findByGlBatchesBatchIDAndIsDeleted(dtoBatches.getBatchId(), false);
				if(glClearingHeaderList!=null && !glClearingHeaderList.isEmpty())
				{
					  for (GLClearingHeader glClearingHeader : glClearingHeaderList) {
						    List<GLClearingDetails> list =  repositoryGLClearingDetail.findByJournalIDAndIsDeleted(glClearingHeader.getJournalID(), false);
						    if(list!=null && !list.isEmpty()){
						    	repositoryGLClearingDetail.deleteInBatch(list);
						    }
					  }
					  repositoryGLClearingHeader.deleteInBatch(glClearingHeaderList);
				}
				
				List<GLCashReceiptBank> cashReceiptList= repositoryGLCashReceiptBank.findByGlBatchesBatchID(dtoBatches.getBatchId());
				if(cashReceiptList!=null && !cashReceiptList.isEmpty())
				{
					for (GLCashReceiptBank glCashReceiptBank : cashReceiptList) {
						List<GLCashReceiptDistribution> distributionList = repositoryGLCashReceiptDistribution.findByGlCashReceiptBankBankCashReceipt(glCashReceiptBank.getBankCashReceipt());
					    if(distributionList!=null && !distributionList.isEmpty() ){
					    	repositoryGLCashReceiptDistribution.deleteInBatch(distributionList);
					    }
					}
					repositoryGLCashReceiptBank.deleteInBatch(cashReceiptList);
				}
				
				
				List<GLBankTransferHeader> glBankTransferList= repositoryGLBankTransferHeader.findByGlBatchesBatchID(dtoBatches.getBatchId());
				if(glBankTransferList!=null)
				{
					for (GLBankTransferHeader glBankTransferHeader : glBankTransferList) 
					{
						List<GLBankTransferDistribution> distributionList = repositoryGLBankTransferDistribution.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
					    if(distributionList!=null && !distributionList.isEmpty()){
						   repositoryGLBankTransferDistribution.deleteInBatch(distributionList);
					    }
					    
					    List<GLBankTransferEntryCharges> chargesList = repositoryGLBankTransferEntryCharges.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
					    if(chargesList!=null && !chargesList.isEmpty()){
					    	repositoryGLBankTransferEntryCharges.deleteInBatch(chargesList);
					    }
					    
					    List<GLBankTransferDetails> detailList = repositoryGLBankTransferDetails.findByGlBankTransferHeaderBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
					    if(detailList!=null && !detailList.isEmpty()){
					    	repositoryGLBankTransferDetails.deleteInBatch(detailList);
					    }
					}
					repositoryGLBankTransferHeader.deleteInBatch(glBankTransferList);
				}
				
				repositoryGLBatches.delete(glBatches);
			}
			return true;
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}
	
	public DtoBatches getBatchTotalTrasactionsByTransactionType(String batchId,int transactionTypeId) {
		List<Object[]> arrayList = null;
		List<String> arrayListOfAccountNumbers =new ArrayList<>();
		List<String> arrayListOfAccountNumberOffset= new ArrayList<>();
		DtoBatches dtoBatches= new DtoBatches();
		dtoBatches.setQuantityTotal(0);
		dtoBatches.setTotalTransactions(0.0);
		if (transactionTypeId == BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex()) {
			arrayList = repositoryJournalEntryHeader.getBatchTotalTrasactionsByTransactionType(batchId);
		}

		if (transactionTypeId == BatchTransactionTypeConstant.CLEARING_ENTRY.getIndex()) {
			Long totalQuantity = repositoryGLClearingHeader.getTotalQuantityByBatchId(batchId);
			dtoBatches.setQuantityTotal(0);
			if(totalQuantity!=null){
				dtoBatches.setQuantityTotal(totalQuantity.intValue());
			}
			
			arrayListOfAccountNumbers = repositoryGLClearingDetail.getAccountNumberListByBatchId(batchId);
			arrayListOfAccountNumberOffset = repositoryGLClearingDetail.getAccountNumberOffsetListByBatchId(batchId);
		    if(!arrayListOfAccountNumberOffset.isEmpty()){
		    	 arrayListOfAccountNumbers.addAll(arrayListOfAccountNumberOffset);
		    }
		    
		    List<Integer> l = new ArrayList();
		    if(!arrayListOfAccountNumbers.isEmpty())
		    {
		    	for(String i: arrayListOfAccountNumbers){
		    		l.add(Integer.parseInt(i.trim()));
		    	}
		    	
		    	Double amount = repositoryGLCurrentSummaryMasterTableByAccountNumber.getAmountByAccountNumbersList(l);
				if(amount!=null && amount>0.0){
					dtoBatches.setTotalTransactions(amount);
				}
		    }
		}
		
		if (transactionTypeId == BatchTransactionTypeConstant.CASH_RECEIPT_ENTRY.getIndex()) {
			arrayList = repositoryGLCashReceiptBank.getBatchTotalTrasactionsByTransactionType(batchId);
		}
		
		if (transactionTypeId == BatchTransactionTypeConstant.BANK_TRANSFER.getIndex()){
			arrayList=repositoryGLBankTransferHeader.getTotaldebitAmountAndTotaCountByBatchId(batchId);
		}
		
		if (arrayList != null && !arrayList.isEmpty()) 
		{
			for (Object[] object : arrayList) {
				Long totalQuantiy = (long) object[0];
				double totalTransctionBalance = (double) object[1];
				dtoBatches.setQuantityTotal(0);
				if(totalQuantiy!=null){
					dtoBatches.setQuantityTotal(totalQuantiy.intValue());
				}
				dtoBatches.setTotalTransactions(totalTransctionBalance);
			}
		}
		return dtoBatches;
	}

	public DtoBatches postBatchTransaction(DtoBatches dtoBatches) throws NumberFormatException, ScriptException {
		if (dtoBatches.getTransactionTypeId() == BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex()) {
			List<JournalEntryHeader> journalEntryHeadersList = repositoryJournalEntryHeader
					.findByGlBatchesBatchIDAndIsDeleted(dtoBatches.getBatchId(), false);
			if (journalEntryHeadersList != null && !journalEntryHeadersList.isEmpty()) {
				for (JournalEntryHeader journalEntryHeader : journalEntryHeadersList) {
					DtoJournalEntryHeader dtoJournalEntryHeader = serviceGLJournalEntry.getJournalEntryByJournalId(journalEntryHeader);
					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getOriginalJournalID())) {
						dtoJournalEntryHeader.setCorrection(true);
					}
					serviceGLJournalEntry.postOrCorrectionJournalEntry(dtoJournalEntryHeader);
				}
			}
		}

		if (dtoBatches.getTransactionTypeId() == BatchTransactionTypeConstant.CLEARING_ENTRY.getIndex()) {
			List<GLClearingHeader> glClearingHeadersList = repositoryGLClearingHeader
					.findByGlBatchesBatchIDAndIsDeleted(dtoBatches.getBatchId(), false);
			if (glClearingHeadersList != null && !glClearingHeadersList.isEmpty()) {
				for (GLClearingHeader glClearingHeader : glClearingHeadersList) {
					DtoJournalEntryHeader dtoJournalEntryHeader = serviceGLClearingGeneralEntry.setGlJournalClearingInDto(glClearingHeader);
					serviceGLClearingGeneralEntry.postClearingJournalEntry(dtoJournalEntryHeader);
				}
			}
		}

		if (dtoBatches.getTransactionTypeId() == BatchTransactionTypeConstant.CASH_RECEIPT_ENTRY.getIndex()) {
			List<GLCashReceiptBank> list = repositoryGLCashReceiptBank.findByGlBatchesBatchID(dtoBatches.getBatchId());
			if (list != null && !list.isEmpty()) {
				DtoGLCashReceiptBank dtoGLCashReceiptBank = null;
				for (GLCashReceiptBank glCashReceiptBank : list) {
					dtoGLCashReceiptBank = new DtoGLCashReceiptBank();
					dtoGLCashReceiptBank.setReceiptNumber(glCashReceiptBank.getReceiptNumber());
					dtoGLCashReceiptBank = serviceGLCashReceiptEntry.getCashReceiptEntryByReceiptNumber(dtoGLCashReceiptBank);
					dtoGLCashReceiptBank = serviceGLCashReceiptEntry.postCashReceiptEntry(dtoGLCashReceiptBank);
				    if(UtilRandomKey.isNotBlank(dtoGLCashReceiptBank.getMessageType())){
				    	dtoBatches.setMessageType(MessageLabel.SOME_TRANSACTION_IS_NOT_POSTED);
				    }
				}
			}
		}
		
		if (dtoBatches.getTransactionTypeId() == BatchTransactionTypeConstant.BANK_TRANSFER.getIndex()) {
			List<GLBankTransferHeader> glBankTransferHeadersList= repositoryGLBankTransferHeader.findByGlBatchesBatchID(dtoBatches.getBatchId());
			if(glBankTransferHeadersList!=null && !glBankTransferHeadersList.isEmpty()){
				for (GLBankTransferHeader glBankTransferHeader : glBankTransferHeadersList) {
					serviceGLBankTransferEntry.postBankTransferEntry(new DtoGLBankTransferHeader(glBankTransferHeader));
				}
			}
		}
		return dtoBatches;
	}
	
	public List<DtoBatches> getBatchesByTransactionTypeId(DtoBatches dtoBatches) {
		List<GLBatches> glBatchesList = repositoryGLBatches.findByBatchTransactionTypeTypeIdAndIsDeleted(dtoBatches.getTransactionTypeId(), false);
		List<DtoBatches> dtoBatchesList = new ArrayList<>();
		if (glBatchesList != null && !glBatchesList.isEmpty()) {
			for (GLBatches glBatches : glBatchesList) 
			{
				dtoBatches= new DtoBatches();
				dtoBatches.setTransactionTypeId(dtoBatches.getTransactionTypeId());
				dtoBatches.setBatchId(glBatches.getBatchID());
				dtoBatches.setDescription(glBatches.getBatchDescription()!=null?glBatches.getBatchDescription():"");
				dtoBatchesList.add(dtoBatches);
			}
		}
		return dtoBatchesList;
	}
	
	public List<DtoBatches> getBatchesByPaymentMethod(DtoBatches dtoBatches) {
		List<GLBatches> glBatchesList = repositoryGLBatches.findByGLConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(dtoBatches.getSourceDocumentID(), false);
		List<DtoBatches> dtoBatchesList = new ArrayList<>();
		if (glBatchesList != null && !glBatchesList.isEmpty()) {
			for (GLBatches glBatches : glBatchesList) 
			{
				dtoBatches= new DtoBatches();
				dtoBatches.setTransactionTypeId(dtoBatches.getTransactionTypeId());
				dtoBatches.setBatchId(glBatches.getBatchID());
				dtoBatches.setDescription(glBatches.getBatchDescription()!=null?glBatches.getBatchDescription():"");
				dtoBatchesList.add(dtoBatches);
			}
		}
		return dtoBatchesList;
	}

}
