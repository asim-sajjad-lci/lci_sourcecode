/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.APDistributionTypeConstant;
import com.bti.constant.ARDistributionType;
import com.bti.constant.ARTransactionTypeConstant;
import com.bti.constant.AuditTrailSeriesTypeConstant;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.GlSerialConfigurationConstant;
import com.bti.constant.MasterArAcoountTypeConstant;
import com.bti.constant.MessageLabel;
import com.bti.constant.RateCalculationMethod;
import com.bti.constant.ReceiptTypeConstant;
import com.bti.model.ARCashReceipt;
import com.bti.model.ARCashReceiptDistribution;
import com.bti.model.ARYTDOpenPayments;
import com.bti.model.ARYTDOpenPaymentsDistribution;
import com.bti.model.BatchTransactionType;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.CustomerAccountTableSetup;
import com.bti.model.CustomerMaintenance;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLBatches;
import com.bti.model.GLCashReceiptBank;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLSerialsConfigurationForTransactions;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.MasterARCashDistributionAccountTypes;
import com.bti.model.MasterARCashReceiptType;
import com.bti.model.dto.DtoARCashReceipt;
import com.bti.model.dto.DtoARCashReceiptDistribution;
import com.bti.model.dto.DtoARDistributionDetail;
import com.bti.model.dto.DtoJournalEntryDetail;
import com.bti.model.dto.DtoStatusType;
import com.bti.model.dto.DtoTypes;
import com.bti.repository.RepositoryARBatches;
import com.bti.repository.RepositoryARCashReceipt;
import com.bti.repository.RepositoryARCashReceiptDistribution;
import com.bti.repository.RepositoryARYTDOpenPayments;
import com.bti.repository.RepositoryARYTDOpenPaymentsDistribution;
import com.bti.repository.RepositoryBankTransactionEntryAction;
import com.bti.repository.RepositoryBankTransactionEntryType;
import com.bti.repository.RepositoryBankTransactionType;
import com.bti.repository.RepositoryBatchTransactionType;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCorrectJournalEntryActions;
import com.bti.repository.RepositoryCurrencyExchangeDetail;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryCustomerAccountTableSetup;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBankTransferDetails;
import com.bti.repository.RepositoryGLBankTransferDistribution;
import com.bti.repository.RepositoryGLBankTransferEntryCharges;
import com.bti.repository.RepositoryGLBankTransferHeader;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLCashReceiptBank;
import com.bti.repository.RepositoryGLCashReceiptDistribution;
import com.bti.repository.RepositoryGLClearingDetail;
import com.bti.repository.RepositoryGLClearingHeader;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByAccountNumber;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByDimensionIndex;
import com.bti.repository.RepositoryGLCurrentSummaryMasterTableByMainAccount;
import com.bti.repository.RepositoryGLSerialConfigurationForTransaction;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryDetails;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryHeader;
import com.bti.repository.RepositoryGlytdHistoryTransaction;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.repository.RepositoryMasterARCashReceiptType;
import com.bti.repository.RepositoryMasterArCashDistributionAccountTypes;
import com.bti.repository.RepositoryPaymentMethodType;
import com.bti.repository.RepositoryReceiptType;
import com.bti.repository.RepositoryVATSetup;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: ServiceARCashReceiptEntry Name of Project: BTI Created on:
 * Dec 04, 2017 Modified on: Dec 04, 2017 05:38:38 PM
 * 
 * @author seasia Version:
 */
@Service("serviceARCashReceiptEntry")
public class ServiceARCashReceiptEntry {

	private static final Logger LOG = Logger.getLogger(ServiceARCashReceiptEntry.class);
	
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	ServiceHome serviceHome;

	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;

	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;

	@Autowired
	RepositoryARBatches repositoryARBatches;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	@Autowired
	RepositoryCurrencyExchangeDetail repositoryCurrencyExchangeDetail;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;

	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;

	@Autowired
	RepositoryGlytdHistoryTransaction repositoryGlytdHistoryTransaction;

	@Autowired
	RepositoryBatchTransactionType repositoryBatchTransactionType;

	@Autowired
	RepositoryCorrectJournalEntryActions repositoryCorrectJournalEntryActions;

	@Autowired
	RepositoryGLClearingHeader repositoryGLClearingHeader;

	@Autowired
	RepositoryGLClearingDetail repositoryGLClearingDetail;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryReceiptType repositoryReceiptType;

	@Autowired
	RepositoryMasterARCashReceiptType repositoryMasterARCashReceiptType;

	@Autowired
	RepositoryARCashReceiptDistribution repositoryARCashReceiptDistribution;

	@Autowired
	RepositoryARCashReceipt repositoryARCashReceipt;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	ServiceAccountPayable serviceAccountPayable;

	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByMainAccount repositoryGLCurrentSummaryMasterTableByMainAccount;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByAccountNumber repositoryGLCurrentSummaryMasterTableByAccountNumber;

	@Autowired
	RepositoryGLCurrentSummaryMasterTableByDimensionIndex repositoryGLCurrentSummaryMasterTableByDimensionIndex;

	@Autowired
	RepositoryVATSetup repositoryVATSetup;
	@Autowired
	RepositoryBankTransactionEntryAction repositoryBankTransactionEntryAction;

	@Autowired
	RepositoryBankTransactionEntryType repositoryBankTransactionEntryType;
	@Autowired
	RepositoryBankTransactionType repositoryBankTransactionType;
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryDetails repositoryGLYTDOpenBankTransactionsEntryDetails;
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryHeader repositoryGLYTDOpenBankTransactionsEntryHeader;

	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;

	@Autowired
	RepositoryGLBankTransferHeader repositoryGLBankTransferHeader;
	@Autowired
	RepositoryGLBankTransferDetails repositoryGLBankTransferDetails;
	@Autowired
	RepositoryGLBankTransferDistribution repositoryGLBankTransferDistribution;
	@Autowired
	RepositoryGLBankTransferEntryCharges repositoryGLBankTransferEntryCharges;
	@Autowired
	RepositoryMasterArCashDistributionAccountTypes repositoryMasterArCashDistributionAccountTypes;
	@Autowired
	RepositoryCustomerAccountTableSetup repositoryCustomerAccountTableSetup;
	@Autowired
	RepositoryARYTDOpenPayments repositoryARYTDOpenPayments;
	@Autowired
	RepositoryARYTDOpenPaymentsDistribution repositoryARYTDOpenPaymentsDistribution;
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired
	RepositoryGLSerialConfigurationForTransaction repositoryGLSerialConfigurationForTransaction;
	
	@Autowired
	ServiceGLCashReceiptEntry serviceGLCashReceiptEntry;
	
	@Autowired
	RepositoryPaymentMethodType repositoryPaymentMethodType;
	
	@Autowired
	RepositoryGLCashReceiptBank repositoryGLCashReceiptBank;

	@Autowired
	RepositoryGLCashReceiptDistribution repositoryGLCashReceiptDistribution;
	
	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	/**
	 * @description this service will use to get Cash Receipt Types
	 * @return
	 */
	public List<DtoStatusType> getCashReceiptType() {
		List<DtoStatusType> list = new ArrayList<>();
		try {
			int langid = serviceHome.getLanngugaeId();
			List<MasterARCashReceiptType> masterARCashReceiptTypeList = repositoryMasterARCashReceiptType
					.findByIsDeletedAndLanguageLanguageId(false, langid);
			if (masterARCashReceiptTypeList != null && !masterARCashReceiptTypeList.isEmpty()) {
				masterARCashReceiptTypeList.forEach(masterARCashReceiptType -> list.add(new DtoStatusType(masterARCashReceiptType)) );
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return list;
	}
	
	/**
	 * @description this service will use to get Cash Receipt Number
	 * @return
	 */
	public List<String> getAllCashReceiptNumber() {
		List<String> receiptNumberList = new ArrayList<>();
		try {
			List<ARCashReceipt> glCashReceiptBankList = repositoryARCashReceipt.findAll();

			if(glCashReceiptBankList!=null && !glCashReceiptBankList.isEmpty()){
				glCashReceiptBankList.forEach(glCashReceiptBank -> receiptNumberList.add(glCashReceiptBank.getCashReceiptNumber()));
			}
		 
		} catch (Exception e) {
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return receiptNumberList;
	}
	
	/**
	 * @description this service will use to save cash receipt entry
	 * @param dtoARCashReceiptBank
	 * @return
	 */
	public DtoARCashReceipt saveCashReceiptEntry(DtoARCashReceipt dtoARCashReceipt) {
		try 
		{
			int langid = serviceHome.getLanngugaeId();
			GLSerialsConfigurationForTransactions glSerialsConfigurationForTransactions = 
					repositoryGLSerialConfigurationForTransaction.findByGlSerialCode(GlSerialConfigurationConstant.CASH_RECEIPT.getVaue());
			String uniqueReceiptNumber= "";
			if(glSerialsConfigurationForTransactions!=null){
				uniqueReceiptNumber=glSerialsConfigurationForTransactions.getGlSerialCode()+"-"+glSerialsConfigurationForTransactions.getGlSerialNumber();
			}
			
			ARCashReceipt arCashReceipt = repositoryARCashReceipt
					.findByCashReceiptNumber(uniqueReceiptNumber);
			if (arCashReceipt == null) {
				arCashReceipt = new ARCashReceipt();
			} else {
				return null;
			}
			
			arCashReceipt.setCashReceiptNumber(uniqueReceiptNumber);
			arCashReceipt.setCashReceiptDescription(dtoARCashReceipt.getCashReceiptDescription());
			arCashReceipt.setCashReceiptCreateDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARCashReceipt.getCashReceiptCreateDate()));
			arCashReceipt.setCashReceiptAmount(dtoARCashReceipt.getCashReceiptAmount()!=null?dtoARCashReceipt.getCashReceiptAmount():0.0);
			//Set cash Receipt Type
			if (dtoARCashReceipt.getCashReceiptType() != null) {
				arCashReceipt.setMasterARCashReceiptType(repositoryMasterARCashReceiptType
						.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoARCashReceipt.getCashReceiptType(), langid, false));
			}
			// Set Customer Information
			CustomerMaintenance customerMaintenance=  repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(dtoARCashReceipt.getCustomerNumber(), false);
			if(customerMaintenance!=null){
				arCashReceipt.setCustomerNumber(customerMaintenance.getCustnumber());
				arCashReceipt.setCustomerName(customerMaintenance.getCustomerName());
				arCashReceipt.setCustomerNameArabic(customerMaintenance.getCustomerNameArabic());
			}
			//Set Currency Information
			arCashReceipt.setCurrencyID(dtoARCashReceipt.getCurrencyID());
			arCashReceipt.setExchangeTableID(dtoARCashReceipt.getExchangeTableIndex());
			arCashReceipt.setExchangeTableRate(dtoARCashReceipt.getExchangeTableRate()!=null?dtoARCashReceipt.getExchangeTableRate():0.0);
			arCashReceipt.setArBatches(repositoryARBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoARCashReceipt.getBatchID(), BatchTransactionTypeConstant.AR_CASH_RECEIPT.getIndex(), false));
			arCashReceipt.setCheckbookID(dtoARCashReceipt.getCheckBookId());
			//Set Check Information
			if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCheckNumber())){
				arCashReceipt.setCheckNumber(dtoARCashReceipt.getCheckNumber());
			}
			//Set credit card information
			if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCreditCardID()))
			{
				arCashReceipt.setCreditCardID(dtoARCashReceipt.getCreditCardID());
				arCashReceipt.setCreditCardNumber(dtoARCashReceipt.getCreditCardNumber());
				arCashReceipt.setCreditCardExpireMonth(dtoARCashReceipt.getCreditCardExpireMonth());
				arCashReceipt.setCreditCardExpireYear(dtoARCashReceipt.getCreditCardExpireYear());
			}
			arCashReceipt.setModifybyUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
			arCashReceipt = repositoryARCashReceipt.saveAndFlush(arCashReceipt);
			
			//Update next receipt number
			 if(glSerialsConfigurationForTransactions!=null)
			 {
				 glSerialsConfigurationForTransactions.setGlSerialNumber(glSerialsConfigurationForTransactions.getGlSerialNumber()+1);
				 repositoryGLSerialConfigurationForTransaction.saveAndFlush(glSerialsConfigurationForTransactions);
			 }
			return new DtoARCashReceipt(arCashReceipt);
		} 
		catch (Exception e) 
		{
			LOG.error(e.getMessage());
			return null;
		}
	}

	/**
	 * @description this service will use to save cash receipt entry
	 * @param dtoGLCashReceiptBank
	 * @return
	 */
	public DtoARCashReceipt updateCashReceiptEntry(DtoARCashReceipt dtoARCashReceipt) {
		try {
			int langid = serviceHome.getLanngugaeId();
			ARCashReceipt arCashReceipt = repositoryARCashReceipt
					.findByCashReceiptNumber(dtoARCashReceipt.getCashReceiptNumber());
			if (arCashReceipt == null) {
				return null;
			}
			arCashReceipt.setCashReceiptDescription(dtoARCashReceipt.getCashReceiptDescription());
			arCashReceipt.setCashReceiptCreateDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARCashReceipt.getCashReceiptCreateDate()));
			arCashReceipt.setCashReceiptAmount(dtoARCashReceipt.getCashReceiptAmount()!=null?dtoARCashReceipt.getCashReceiptAmount():0.0);
			//Set cash Receipt Type
			if (dtoARCashReceipt.getCashReceiptType() != null) {
				arCashReceipt.setMasterARCashReceiptType(repositoryMasterARCashReceiptType
						.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoARCashReceipt.getCashReceiptType(), langid, false));
			}
			// Set Customer Information
			CustomerMaintenance customerMaintenance=  repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(dtoARCashReceipt.getCustomerNumber(), false);
			if(customerMaintenance!=null){
				arCashReceipt.setCustomerNumber(customerMaintenance.getCustnumber());
				arCashReceipt.setCustomerName(customerMaintenance.getCustomerName());
				arCashReceipt.setCustomerNameArabic(customerMaintenance.getCustomerNameArabic());
			}
			else
			{
				arCashReceipt.setCustomerNumber(null);
				arCashReceipt.setCustomerName(null);
				arCashReceipt.setCustomerNameArabic(null);
			}
			//Set Currency Information
			arCashReceipt.setCurrencyID(dtoARCashReceipt.getCurrencyID());
			arCashReceipt.setExchangeTableID(dtoARCashReceipt.getExchangeTableIndex());
			arCashReceipt.setExchangeTableRate(dtoARCashReceipt.getExchangeTableRate()!=null?dtoARCashReceipt.getExchangeTableRate():0.0);
			arCashReceipt.setArBatches(repositoryARBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(dtoARCashReceipt.getBatchID(), BatchTransactionTypeConstant.AR_CASH_RECEIPT.getIndex(), false));
			arCashReceipt.setCheckbookID(dtoARCashReceipt.getCheckBookId());
			//Set Checkbook Information
			if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCheckNumber())){
				arCashReceipt.setCheckNumber(dtoARCashReceipt.getCheckNumber());
			}
			else{
				arCashReceipt.setCheckNumber(null);
			}
			//Set credit card information
			if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCreditCardID()))
			{
				arCashReceipt.setCreditCardID(dtoARCashReceipt.getCreditCardID());
				arCashReceipt.setCreditCardNumber(dtoARCashReceipt.getCreditCardNumber());
				arCashReceipt.setCreditCardExpireMonth(dtoARCashReceipt.getCreditCardExpireMonth());
				arCashReceipt.setCreditCardExpireYear(dtoARCashReceipt.getCreditCardExpireYear());
			}
			else
			{
				arCashReceipt.setCreditCardID(null);
				arCashReceipt.setCreditCardNumber(null);
				arCashReceipt.setCreditCardExpireMonth(0);
				arCashReceipt.setCreditCardExpireYear(0);
			}
			arCashReceipt.setModifybyUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
			arCashReceipt = repositoryARCashReceipt.saveAndFlush(arCashReceipt);
			return new DtoARCashReceipt(arCashReceipt);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return null;
		}
	}

	/**
	 * @description this service will use to get cash receipt entry by receipt
	 *              number
	 * @param DtoARCashReceipt
	 * @return
	 */
	public DtoARCashReceipt getCashReceiptEntryByReceiptNumber(DtoARCashReceipt dtoARCashReceipt) {
		try {
			ARCashReceipt arCashReceipt = repositoryARCashReceipt
					.findByCashReceiptNumber(dtoARCashReceipt.getCashReceiptNumber());
			if (arCashReceipt != null) {
				return new DtoARCashReceipt(arCashReceipt);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}
	
	public boolean deleteCashReceiptEntryByReceiptNumber(String cashReceiptNumber) {
		try{
			ARCashReceipt arCashReceipt = repositoryARCashReceipt
					.findByCashReceiptNumber(cashReceiptNumber);
			if (arCashReceipt != null) {
				List<ARCashReceiptDistribution> arCashReceiptDistributionList = repositoryARCashReceiptDistribution
						.findByCashReceiptNumber(cashReceiptNumber);
				if (arCashReceiptDistributionList != null && !arCashReceiptDistributionList.isEmpty()) {
					repositoryARCashReceiptDistribution.deleteInBatch(arCashReceiptDistributionList);
				}
				repositoryARCashReceipt.delete(arCashReceipt);
			}
			return true;
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
			return false;
		}
	}
	
	public DtoARCashReceipt postCashReceiptEntry(DtoARCashReceipt dtoARCashReceipt) 
	{
		int langid = serviceHome.getLanngugaeId();
		try {
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
			CurrencyExchangeHeader currencyExchangeHeader= repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(
					Integer.parseInt(dtoARCashReceipt.getExchangeTableIndex()!=null?dtoARCashReceipt.getExchangeTableIndex():"0"), false);
			if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
							.getMethod();
			}
			
			// Save Distribution Detail In Tables 
			List<ARYTDOpenPaymentsDistribution> openPaymentDistributionList = null;
			//List<ARCashReceiptDistribution> arCashReceiptDistribution = repositoryARCashReceiptDistribution.findByCashReceiptNumber(dtoARCashReceipt.getCashReceiptNumber());
			/*if(arCashReceiptDistribution!=null && !arCashReceiptDistribution.isEmpty())
			{
				openPaymentDistributionList= new ArrayList<>();
				for (ARCashReceiptDistribution arCashReceiptDistribution2 : arCashReceiptDistribution) 
				{
					GLAccountsTableAccumulation glAccountsTableAccumulation= repositoryGLAccountsTableAccumulation.
							findByAccountTableRowIndexAndIsDeleted(String.valueOf(arCashReceiptDistribution2.getAccountTableRowIndex()), false);
					if(glAccountsTableAccumulation!=null)
					{
						ARYTDOpenPaymentsDistribution distributionDetail= new ARYTDOpenPaymentsDistribution();
						distributionDetail.setPaymentNumber(arCashReceiptDistribution2.getCashReceiptNumber());
						distributionDetail.setAccountTableRowIndex(arCashReceiptDistribution2.getAccountTableRowIndex());
						if(arCashReceiptDistribution2.getAccountTypes()!=null){
							distributionDetail.setTransactionType(arCashReceiptDistribution2.getAccountTypes().getTypeId());
						}
						
						distributionDetail.setCreditAmount(arCashReceiptDistribution2.getCreditAmount());
						distributionDetail.setOriginalCreditAmount(arCashReceiptDistribution2.getOriginalCreditAmount());
						distributionDetail.setDebitAmount(arCashReceiptDistribution2.getDebitAmount());
						distributionDetail.setOriginalDebitAmount(arCashReceiptDistribution2.getOriginalDebitAmount());
						distributionDetail.setDistributionDescription(arCashReceiptDistribution2.getDistributionDescription());
						distributionDetail.setRowDateindex(new Date());
						openPaymentDistributionList.add(distributionDetail);
					}
					else
					{
						dtoARCashReceipt.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
						return dtoARCashReceipt;
					}
				}
			}*/
			//else
			//{
			    dtoARCashReceipt.setButtonType(MessageLabel.NEW_DISTRIBUTION);
				DtoARCashReceiptDistribution dtoARCashReceiptDistribution = getCashReceiptDistributionByCashReceiptNumber(dtoARCashReceipt);
				if(dtoARCashReceiptDistribution!=null && dtoARCashReceiptDistribution.getListDtoARDistributionDetail()!=null
						&& !dtoARCashReceiptDistribution.getListDtoARDistributionDetail().isEmpty())
				{
					openPaymentDistributionList= new ArrayList<>();
					for (DtoARDistributionDetail dtoARDistributionDetail : dtoARCashReceiptDistribution.getListDtoARDistributionDetail()) 
					{
						if(UtilRandomKey.isNotBlank(dtoARDistributionDetail.getAccountNumber()) 
								&& UtilRandomKey.isNotBlank(dtoARDistributionDetail.getAccountTableRowIndex()))
						{
							ARYTDOpenPaymentsDistribution distributionDetail= new ARYTDOpenPaymentsDistribution();
							distributionDetail.setPaymentNumber(dtoARCashReceiptDistribution.getCashReceiptNumber());
							distributionDetail.setAccountTableRowIndex(Integer.parseInt(dtoARDistributionDetail.getAccountTableRowIndex()));
							distributionDetail.setTransactionType(dtoARDistributionDetail.getTypeId());
							Double creditAmount=0.0;
							Double debitAmount=0.0;
							distributionDetail.setOriginalCreditAmount(0.0);
							distributionDetail.setOriginalDebitAmount(0.0);
							if(dtoARDistributionDetail.getCreditAmount()!=null && dtoARDistributionDetail.getCreditAmount()>0 ){
								distributionDetail.setOriginalCreditAmount(dtoARDistributionDetail.getCreditAmount());
								if(dtoARCashReceipt.getExchangeTableRate()!=null)
								{
									creditAmount = (double) mathEval
											.eval(dtoARDistributionDetail.getCreditAmount() + calcMethod
													+ dtoARCashReceipt.getExchangeTableRate());
								}
							}
							
							if(dtoARDistributionDetail.getDebitAmount()!=null && dtoARDistributionDetail.getDebitAmount()>0 ){
								distributionDetail.setOriginalDebitAmount(dtoARDistributionDetail.getDebitAmount());
								
								if(dtoARCashReceipt.getExchangeTableRate()!=null){
									debitAmount= (double) mathEval
											.eval(dtoARDistributionDetail.getDebitAmount() + calcMethod
													+ dtoARCashReceipt.getExchangeTableRate());
								}
							}
							
							distributionDetail.setCreditAmount(creditAmount);
							distributionDetail.setDebitAmount(debitAmount);
							distributionDetail.setDistributionDescription(dtoARDistributionDetail.getDistributionReference());
							distributionDetail.setRowDateindex(new Date());
							openPaymentDistributionList.add(distributionDetail);
						}
						else
						{
							dtoARCashReceipt.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
							return dtoARCashReceipt;
						}
					}
				}
			//}
			
			if(openPaymentDistributionList!=null && !openPaymentDistributionList.isEmpty())
			{
				// Save in ar90400
				ARYTDOpenPayments arYTDOpenPayments= new ARYTDOpenPayments();
				ARCashReceipt arCashReceipt = repositoryARCashReceipt.findByCashReceiptNumber(dtoARCashReceipt.getCashReceiptNumber());
				arYTDOpenPayments.setPaymentNumber(dtoARCashReceipt.getCashReceiptNumber());
				// Set Customer Information
				CustomerMaintenance customerMaintenance=  repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(dtoARCashReceipt.getCustomerNumber(), false);
				if(customerMaintenance!=null){
					arYTDOpenPayments.setCustomerNumber(customerMaintenance.getCustnumber());
				}
				arYTDOpenPayments.setCheckbookID(dtoARCashReceipt.getCheckBookId());
				arYTDOpenPayments.setArTransactionType(ARTransactionTypeConstant.PAYMENTS.getIndex());
				arYTDOpenPayments.setPaymentType(dtoARCashReceipt.getCashReceiptType());
				arYTDOpenPayments.setCurrencyID(dtoARCashReceipt.getCurrencyID());
				//Set credit card information
				if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCreditCardID()))
				{
					arYTDOpenPayments.setCreditCardID(dtoARCashReceipt.getCreditCardID());
					arYTDOpenPayments.setCreditCardNumber(dtoARCashReceipt.getCreditCardNumber());
					arYTDOpenPayments.setCreditCardExpireMonth(dtoARCashReceipt.getCreditCardExpireMonth());
					arYTDOpenPayments.setCreditCardExpireYear(dtoARCashReceipt.getCreditCardExpireYear());
				}
				//Set Check Information
				if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCheckNumber())){
					arYTDOpenPayments.setCheckNumber(dtoARCashReceipt.getCheckNumber());
				}
				arYTDOpenPayments.setPaymentDescription(dtoARCashReceipt.getCashReceiptDescription());
				arYTDOpenPayments.setPaymentCreateDate(UtilDateAndTime.ddmmyyyyStringToDate(dtoARCashReceipt.getCashReceiptCreateDate()));
				if(arCashReceipt!=null){
					arYTDOpenPayments.setPaymentCreatebyUserID(arCashReceipt.getModifybyUserID());
				}
				arYTDOpenPayments.setPaymentPostingDate(new Date());
				arYTDOpenPayments.setPaymentPostingbyUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
				arYTDOpenPayments.setOriginalPaymentAmount(dtoARCashReceipt.getCashReceiptAmount());
				
				Double amount=dtoARCashReceipt.getCashReceiptAmount();
				if(dtoARCashReceipt.getExchangeTableRate()!=null){
					amount = (double) mathEval
							.eval(dtoARCashReceipt.getCashReceiptAmount() + calcMethod
									+ dtoARCashReceipt.getExchangeTableRate());
				}
				arYTDOpenPayments.setPaymentAmount(amount);
				
				//Set Currency Information
				arYTDOpenPayments.setExchangeTableID(dtoARCashReceipt.getExchangeTableIndex());
				arYTDOpenPayments.setExchangeTableRate(dtoARCashReceipt.getExchangeTableRate()!=null?dtoARCashReceipt.getExchangeTableRate():0.0);
				arYTDOpenPayments.setPaymentVoid(false);
				if(dtoARCashReceipt.getTransactionVoid()==1){
					arYTDOpenPayments.setPaymentVoid(true);
				}
				
				repositoryARYTDOpenPayments.saveAndFlush(arYTDOpenPayments);
				
				// Save List in ar90401
				repositoryARYTDOpenPaymentsDistribution.save(openPaymentDistributionList);
				
				//Save New Batch For AR Cash Rceipt IN GL Batch
				GLBatches glBatches= createNewGlBatchesInPostCashReceiptEntry(dtoARCashReceipt, langid);
				// Create New Jv Entry gl10100 and gl10101
				createNewJvEntryInPostCashReceiptEntry(openPaymentDistributionList, dtoARCashReceipt, glBatches, mathEval);
				
				deleteCashReceiptEntryByReceiptNumber(dtoARCashReceipt.getCashReceiptNumber());
				
				return dtoARCashReceipt;
			}
			else
			{
				dtoARCashReceipt.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
				return dtoARCashReceipt;
			}
			
		} catch (Exception e) {
			LOG.error(e.getMessage());
			dtoARCashReceipt.setMessageType(MessageLabel.WRONG_DISTRIBUTION);
			return dtoARCashReceipt;
		}
    }


	/**
	 * @description this service will use to save cash receipt distributions
	 * @param dtoARCashReceiptDistribution
	 * @return
	 */
	public DtoARCashReceiptDistribution saveCashReceiptDistribution(
			DtoARCashReceiptDistribution dtoARCashReceiptDistribution) {
		int langId = serviceHome.getLanngugaeId();
		try {
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");
			if (dtoARCashReceiptDistribution.getListDtoARDistributionDetail() != null
					&& !dtoARCashReceiptDistribution.getListDtoARDistributionDetail().isEmpty()){
				
				deleteCashReceiptEntryDistribution(dtoARCashReceiptDistribution.getCashReceiptNumber());
				
				for (DtoARDistributionDetail distributionDetail : dtoARCashReceiptDistribution.getListDtoARDistributionDetail()) 
				{
					ARCashReceiptDistribution arCashReceiptDistribution = new ARCashReceiptDistribution();
					arCashReceiptDistribution.setAccountTableRowIndex(Integer.parseInt(distributionDetail.getAccountTableRowIndex()));
					arCashReceiptDistribution.setCashReceiptNumber(dtoARCashReceiptDistribution.getCashReceiptNumber());
					arCashReceiptDistribution.setAccountTypes(repositoryMasterArCashDistributionAccountTypes.findByLanguageLanguageIdAndIsDeletedAndTypeId(langId, false,distributionDetail.getTypeId()));
					Double amount=0.0;
					Double exchangeTableRate=null;
					String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
					ARCashReceipt arCashReceipt= repositoryARCashReceipt.findByCashReceiptNumber(dtoARCashReceiptDistribution.getCashReceiptNumber());
					if(arCashReceipt!=null && UtilRandomKey.isNotBlank(arCashReceipt.getExchangeTableID()))
					{
							CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(arCashReceipt.getExchangeTableID()), false);
						   if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
							   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
						   }
						  exchangeTableRate=arCashReceipt.getExchangeTableRate();
					}
					
					if(distributionDetail.getDebitAmount()!=null && distributionDetail.getDebitAmount()>0){
						amount=distributionDetail.getDebitAmount();
						arCashReceiptDistribution.setOriginalDebitAmount(amount);
						if(exchangeTableRate!=null){
							amount= (double) mathEval.eval(distributionDetail.getDebitAmount() + calcMethod +exchangeTableRate );
						}
						arCashReceiptDistribution.setDebitAmount(amount);
					}
					
					if(distributionDetail.getCreditAmount()!=null && distributionDetail.getCreditAmount()>0){
						amount=distributionDetail.getCreditAmount();
						arCashReceiptDistribution.setOriginalCreditAmount(amount);
						if(exchangeTableRate!=null){
							amount= (double) mathEval.eval(distributionDetail.getCreditAmount() + calcMethod +exchangeTableRate );
						}
						arCashReceiptDistribution.setCreditAmount(amount);
					}
					arCashReceiptDistribution.setDistributionDescription(distributionDetail.getDistributionReference());
					repositoryARCashReceiptDistribution.saveAndFlush(arCashReceiptDistribution);
				}
			}
			return dtoARCashReceiptDistribution;

		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	/**
	 * @description this service will use to get cash receipt distributions by
	 *              receipt number
	 * @param dtoARCashReceiptDistribution
	 * @return
	 */
	public DtoARCashReceiptDistribution getCashReceiptDistributionByCashReceiptNumber(
			DtoARCashReceipt dtoARCashReceipt) {
		int langId = serviceHome.getLanngugaeId();
		try 
		{
			List<DtoARDistributionDetail> distributionDetailList= new ArrayList<>();
			/*ARCashReceipt arCashReceipt = repositoryARCashReceipt
					.findByCashReceiptNumber(dtoARCashReceipt.getCashReceiptNumber());
			if (arCashReceipt != null) 
			{ */
				DtoARCashReceiptDistribution distribution = new DtoARCashReceiptDistribution();
				
				// Set Customer Information
				CustomerMaintenance customerMaintenance=  repositoryCustomerMaintenance.findByCustnumberAndIsDeleted(dtoARCashReceipt.getCustomerNumber(), false);
				if(customerMaintenance!=null){
					distribution.setCustomerId(customerMaintenance.getCustnumber()!=null?customerMaintenance.getCustnumber():"");
					distribution.setCustomerName(customerMaintenance.getCustomerName()!=null?customerMaintenance.getCustomerName():"");
				}
				distribution.setCurrencyID(dtoARCashReceipt.getCurrencyID());
				distribution.setCashReceiptNumber(dtoARCashReceipt.getCashReceiptNumber());
				MasterARCashReceiptType cashReceiptType= repositoryMasterARCashReceiptType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoARCashReceipt.getCashReceiptType(), langId, false);
				
				if(cashReceiptType!=null && cashReceiptType.getLanguage().getLanguageId()!=langId){
					cashReceiptType=repositoryMasterARCashReceiptType.findByTypeIdAndLanguageLanguageIdAndIsDeleted(cashReceiptType.getTypeId(), langId, false);
				}
				distribution.setTransactionType(cashReceiptType!=null?cashReceiptType.getValue():"");
				distribution.setOriginalAmount(dtoARCashReceipt.getCashReceiptAmount());
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine mathEval = manager.getEngineByName("js");
				Double amount  = dtoARCashReceipt.getCashReceiptAmount();
				String calcMethod=RateCalculationMethod.MULTIPLY.getMethod();
				if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getExchangeTableIndex())){
				   CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(Integer.parseInt(dtoARCashReceipt.getExchangeTableIndex()), false);
				   if(currencyExchangeHeader!=null && UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())){
					   calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod()).getMethod();
				   }
				}

				Double functionalAmount = (double) mathEval.eval(amount + calcMethod + dtoARCashReceipt.getExchangeTableRate());
				distribution.setFunctionalAmount(functionalAmount);
				List<ARCashReceiptDistribution> arCashReceiptDistribution =null;
				if(dtoARCashReceipt.getButtonType().equalsIgnoreCase(MessageLabel.NEW_DISTRIBUTION))
				{
					arCashReceiptDistribution=repositoryARCashReceiptDistribution.findByCashReceiptNumber(dtoARCashReceipt.getCashReceiptNumber());
					if(arCashReceiptDistribution!=null && !arCashReceiptDistribution.isEmpty())
					{
						for (ARCashReceiptDistribution arCashReceiptDistribution2 : arCashReceiptDistribution) 
						{
							DtoARDistributionDetail distributionDetail= new DtoARDistributionDetail();
							distributionDetail.setAccountNumber("");
							distributionDetail.setAccountDescription("");
							distributionDetail.setAccountTableRowIndex("");
							GLAccountsTableAccumulation glAccountsTableAccumulation=null;
							if(arCashReceiptDistribution2.getAccountTableRowIndex()>0){
								glAccountsTableAccumulation=repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(String.valueOf(arCashReceiptDistribution2.getAccountTableRowIndex()), false);
							}
							distributionDetail.setDistributionReference("");
							
							if(UtilRandomKey.isNotBlank(arCashReceiptDistribution2.getDistributionDescription())){
								distributionDetail.setDistributionReference(arCashReceiptDistribution2.getDistributionDescription());
							}
							distributionDetail.setType("");
							
							distributionDetail.setTypeId(0);
							if(arCashReceiptDistribution2.getAccountTypes()!=null){
								distributionDetail.setType(ARDistributionType.getById(arCashReceiptDistribution2.getAccountTypes().getTypeId()).name());
								distributionDetail.setTypeId(arCashReceiptDistribution2.getAccountTypes().getTypeId());
							}
							
							distributionDetail.setDebitAmount(0.0);
							if(arCashReceiptDistribution2.getOriginalDebitAmount()!=null && arCashReceiptDistribution2.getOriginalDebitAmount()>0){
								distributionDetail.setDebitAmount(arCashReceiptDistribution2.getOriginalDebitAmount());
							}
							
							distributionDetail.setCreditAmount(0.0);
							if(arCashReceiptDistribution2.getOriginalCreditAmount()!=null && arCashReceiptDistribution2.getOriginalCreditAmount()>0){
								distributionDetail.setCreditAmount(arCashReceiptDistribution2.getOriginalCreditAmount());
							}
							
							if(glAccountsTableAccumulation!=null){
								distributionDetail.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
								Object[] object=serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
								distributionDetail.setAccountNumber(object[0].toString());
								distributionDetail.setAccountDescription("");
								if(UtilRandomKey.isNotBlank(object[3].toString())){
									distributionDetail.setAccountDescription(object[3].toString());
								}
							}
							distributionDetailList.add(distributionDetail);
						}
					}
				}
				
				if(dtoARCashReceipt.getButtonType().equalsIgnoreCase(MessageLabel.DEFAULT_DISTRIBUTION) ||
						arCashReceiptDistribution==null || arCashReceiptDistribution.isEmpty())
				{
					DtoARDistributionDetail distributionDetailDebit= new DtoARDistributionDetail();
					//Set CheckBook Account Number Information
					CheckbookMaintenance checkbookMaintenance= repositoryCheckBookMaintenance.findByCheckBookIdAndIsDeleted(dtoARCashReceipt.getCheckBookId(), false);
					GLAccountsTableAccumulation glAccountsTableAccumulation=null;
					if(checkbookMaintenance!=null){
						glAccountsTableAccumulation= checkbookMaintenance.getGlAccountsTableAccumulation();
					}
					distributionDetailDebit.setAccountNumber("");
					distributionDetailDebit.setAccountDescription("");
					distributionDetailDebit.setAccountTableRowIndex("");
					distributionDetailDebit.setDistributionReference("");
					distributionDetailDebit.setType(ARDistributionType.CASH.name());
					distributionDetailDebit.setTypeId(ARDistributionType.CASH.getIndex());
					distributionDetailDebit.setDebitAmount(amount);
					distributionDetailDebit.setCreditAmount(0.0);
					
					if(glAccountsTableAccumulation!=null){
						distributionDetailDebit.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
						Object[] object=serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
						distributionDetailDebit.setAccountNumber(object[0].toString());
						distributionDetailDebit.setAccountDescription(object[3].toString());
					}
					
					distributionDetailList.add(distributionDetailDebit);
					//Set Account Receivable Options Setup
					DtoARDistributionDetail distributionDetailCredit= new DtoARDistributionDetail();
					CustomerAccountTableSetup customerAccountTableSetup= repositoryCustomerAccountTableSetup.findByMasterArAccountTypeTypeIdAndCustomerMaintenanceCustnumberAndIsDeleted(MasterArAcoountTypeConstant.ACCOUNT_RECV.getIndex(), dtoARCashReceipt.getCustomerNumber(), false);
					if(customerAccountTableSetup!=null){
						glAccountsTableAccumulation=customerAccountTableSetup.getGlAccountsTableAccumulation();
					}
					
					distributionDetailCredit.setAccountNumber("");
					distributionDetailCredit.setAccountDescription("");
					distributionDetailCredit.setAccountTableRowIndex("");
					distributionDetailCredit.setDistributionReference("");
					distributionDetailCredit.setType(ARDistributionType.RECV.name());
					distributionDetailCredit.setTypeId(ARDistributionType.RECV.getIndex());
					distributionDetailCredit.setCreditAmount(amount);
					distributionDetailCredit.setDebitAmount(0.0);
					if(glAccountsTableAccumulation!=null){
						distributionDetailCredit.setAccountTableRowIndex(glAccountsTableAccumulation.getAccountTableRowIndex());
						Object[] object=serviceAccountPayable.getAccountNumber(glAccountsTableAccumulation);
						distributionDetailCredit.setAccountNumber(object[0].toString());
						distributionDetailCredit.setAccountDescription(object[3].toString());
					}
					
					distributionDetailList.add(distributionDetailCredit);
				}
				
				distribution.setListDtoARDistributionDetail(distributionDetailList);
				return distribution;
			//}
		} 
		catch (Exception e) 
		{
			LOG.error(e.getMessage());
		}
		return null;
	}
	
	public List<DtoTypes> getDistributionAccountTypesList()
    {
		   int langId = serviceHome.getLanngugaeId();
		   List<DtoTypes> typesList = new ArrayList<>();
		   List<MasterARCashDistributionAccountTypes> accountTypes= repositoryMasterArCashDistributionAccountTypes.findByLanguageLanguageIdAndIsDeleted(langId,false);
		   if(accountTypes!=null && !accountTypes.isEmpty())
		   {
			   for (MasterARCashDistributionAccountTypes masterARCashDistributionAccountTypes : accountTypes) 
			   {
				   DtoTypes dtoTypes= new DtoTypes();
				   dtoTypes.setTypeCode(masterARCashDistributionAccountTypes.getTypeCode());
				   dtoTypes.setTypeValue(masterARCashDistributionAccountTypes.getTypeDescription());
				   dtoTypes.setTypeId(masterARCashDistributionAccountTypes.getTypeId());
				   typesList.add(dtoTypes);
			    }
		   }
		return typesList;
	}

	public boolean checkDistributionDetailIsAvailable(String cashReceiptNumber) {
	
		List<ARCashReceiptDistribution> arCashReceiptDistributionsList = repositoryARCashReceiptDistribution.findByCashReceiptNumber(cashReceiptNumber);
		if(arCashReceiptDistributionsList!=null && !arCashReceiptDistributionsList.isEmpty()){
			return true;
		}
       return false;
	}
	
	public boolean deleteCashReceiptEntryDistribution(String cashReceiptNumber) {
		try
		{
				List<ARCashReceiptDistribution> arCashReceiptDistributionList = repositoryARCashReceiptDistribution
						.findByCashReceiptNumber(cashReceiptNumber);
				if (arCashReceiptDistributionList != null && !arCashReceiptDistributionList.isEmpty()) {
					repositoryARCashReceiptDistribution.deleteInBatch(arCashReceiptDistributionList);
					return true;
				}
		}
		catch(Exception e){
			LOG.info(Arrays.toString(e.getStackTrace()));
		}
		return false;
	}
	
	public GLCashReceiptBank saveARCashReceiptInGLCashReceipt(DtoARCashReceipt dtoARCashReceipt){
		int langid=serviceHome.getLanngugaeId();
		GLCashReceiptBank glCashReceiptBank = new GLCashReceiptBank();
		String uniqueCashReceiptNumber=serviceGLCashReceiptEntry.getUniqueReceiptNumber();
		glCashReceiptBank.setReceiptNumber(uniqueCashReceiptNumber);
		glCashReceiptBank.setCheckbookMaintenance(repositoryCheckBookMaintenance
				.findByCheckBookIdAndIsDeleted(dtoARCashReceipt.getCheckBookId(), false));
		glCashReceiptBank.setCurrencyID(dtoARCashReceipt.getCurrencyID());
		glCashReceiptBank.setReceiptDate(UtilDateAndTime.ddmmyyyyStringToDate
				(dtoARCashReceipt.getCashReceiptCreateDate()));
		
		glCashReceiptBank.setReceiptType(repositoryReceiptType.
				findByTypeIdAndLanguageLanguageIdAndIsDeleted(ReceiptTypeConstant.AR.getIndex(), langid, false));
		
		if (dtoARCashReceipt.getCashReceiptType()!= null) {
			glCashReceiptBank.setPaymentMethod(repositoryPaymentMethodType.
					findByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoARCashReceipt.getCashReceiptType(), langid, false));
		}
		
		
		if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCheckNumber())){
			glCashReceiptBank.setCheckNumber(dtoARCashReceipt.getCheckNumber());
		}
		
		if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCreditCardID())){
			glCashReceiptBank.setCreditCardID(dtoARCashReceipt.getCreditCardID());
		}
		
		if(dtoARCashReceipt.getCreditCardExpireMonth()!=null){
			glCashReceiptBank.setCreditCardExpireMonth(dtoARCashReceipt.getCreditCardExpireMonth());
		}
		
		if(dtoARCashReceipt.getCreditCardExpireYear()!=null){
			glCashReceiptBank.setCreditCardExpireYear(dtoARCashReceipt.getCreditCardExpireYear());
		}
		
		if(UtilRandomKey.isNotBlank(dtoARCashReceipt.getCreditCardNumber())){
			glCashReceiptBank.setCreditCardNumber(dtoARCashReceipt.getCreditCardNumber());
		}
		
		
		glCashReceiptBank.setDepositorName(dtoARCashReceipt.getCustomerName());
	
		glCashReceiptBank.setReceiptAmount(dtoARCashReceipt.getCashReceiptAmount());
		
		glCashReceiptBank.setReceiptDescription(dtoARCashReceipt.getCashReceiptDescription());

		glCashReceiptBank.setExchangeRate(dtoARCashReceipt.getExchangeTableRate());
		glCashReceiptBank.setExchangeTableIndex(dtoARCashReceipt.getExchangeTableIndex());
		glCashReceiptBank.setCustomerMaintenance(repositoryCustomerMaintenance
				.findByCustnumberAndIsDeleted(dtoARCashReceipt.getCustomerNumber(), false));
		glCashReceiptBank.setRowDateIndex(new Date());
		glCashReceiptBank.setModifyByUserID(httpServletRequest.getHeader(CommonConstant.USER_ID));
		
		
		return glCashReceiptBank = repositoryGLCashReceiptBank.saveAndFlush(glCashReceiptBank);
		
	}
	
	public GLBatches createNewGlBatchesInPostCashReceiptEntry(DtoARCashReceipt dtoARCashReceipt, int langid){
		
		int year=0;
		int month=0;
		int day=0;
		Calendar cal = Calendar.getInstance();
		if (UtilRandomKey.isNotBlank(dtoARCashReceipt.getCashReceiptCreateDate())) {
			Date transDate = UtilDateAndTime
					.ddmmyyyyStringToDate(dtoARCashReceipt.getCashReceiptCreateDate());
			cal.setTime(transDate);
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH)+1;
			day = cal.get(Calendar.DATE);
		}
		
		
		String batchId=AuditTrailSeriesTypeConstant.AR+
				GlSerialConfigurationConstant.CASH_RECEIPT.getVaue()+day+month+year;
		String batchDesc=AuditTrailSeriesTypeConstant.AR+" "+
				GlSerialConfigurationConstant.CASH_RECEIPT.getVaue()+" "+dtoARCashReceipt.getCashReceiptNumber();
		
		GLBatches glBatches=repositoryGLBatches.findByBatchIDAndBatchTransactionTypeTypeIdAndIsDeleted(batchId, BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex(), false);
		if(glBatches==null){
			glBatches= new GLBatches();
			glBatches.setTotalAmountTransactionInBatch(dtoARCashReceipt.getCashReceiptAmount());
			glBatches.setTotalNumberTransactionInBatch(1);
		}
		else{
			double amount;
			amount=glBatches.getTotalAmountTransactionInBatch()!=null?glBatches.getTotalAmountTransactionInBatch():0.0;
			glBatches.setTotalAmountTransactionInBatch(amount+dtoARCashReceipt.getCashReceiptAmount());
			glBatches.setTotalNumberTransactionInBatch(glBatches.getTotalNumberTransactionInBatch()+1);
		}
		
		glBatches.setBatchID(batchId);
		glBatches.setBatchDescription(batchDesc);
		
		BatchTransactionType batchTransactionType = repositoryBatchTransactionType
				.findByTypeIdAndLanguageLanguageIdAndIsDeleted(BatchTransactionTypeConstant.JOURNAL_ENTRY.getIndex(), langid, false);
		if (batchTransactionType != null) {
			glBatches.setBatchTransactionType(batchTransactionType);
		}
		
		glBatches.setPostingDate(new Date());
		glBatches = repositoryGLBatches.saveAndFlush(glBatches);
		return glBatches;
	}
	
	public boolean createNewJvEntryInPostCashReceiptEntry(List<ARYTDOpenPaymentsDistribution> openPaymentDistributionList ,
			DtoARCashReceipt dtoARCashReceipt , GLBatches glBatches,ScriptEngine mathEval) throws ScriptException{
		//Save Data in gl10100
		int nextJournalId = 0;
		GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
		if (glConfigurationSetup == null) {
			return false;
		}
		JournalEntryHeader journalEntryHeader = new JournalEntryHeader();
		nextJournalId = glConfigurationSetup.getNextJournalEntryVoucher();
		journalEntryHeader.setJournalID(String.valueOf(nextJournalId));

		journalEntryHeader.setOriginalTransactionNumberFromModule(dtoARCashReceipt.getCashReceiptNumber());
		journalEntryHeader.setGlBatches(glBatches);
		journalEntryHeader.setTransactionType(CommonConstant.STANDARD);
		
		if (UtilRandomKey.isNotBlank(dtoARCashReceipt.getCashReceiptCreateDate())) {
			journalEntryHeader.setTransactionDate(
					UtilDateAndTime.ddmmyyyyStringToDate(dtoARCashReceipt.getCashReceiptCreateDate()));
		} 

		GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
				.findBySeriesIndexAndIsDeleted(AuditTrailSeriesTypeConstant.AR.getIndex(), false);
		journalEntryHeader.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
		journalEntryHeader.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
		journalEntryHeader.setJournalDescription(dtoARCashReceipt.getCashReceiptDescription());
		journalEntryHeader.setTransactionSource("JV");
		CurrencySetup currencySetup = repositoryCurrencySetup
				.findByCurrencyIdAndIsDeleted(dtoARCashReceipt.getCurrencyID(), false);
		journalEntryHeader.setCurrencySetup(currencySetup);
		journalEntryHeader.setOriginalTotalJournalEntryCredit(dtoARCashReceipt.getCashReceiptAmount());
		journalEntryHeader.setOriginalTotalJournalEntryDebit(dtoARCashReceipt.getCashReceiptAmount());
		
		Double currentExchangeRate = Double.valueOf(dtoARCashReceipt.getExchangeTableRate());
		journalEntryHeader.setExchangeTableRate(currentExchangeRate);
		CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
				.findByExchangeIndexAndIsDeleted(Integer.parseInt(dtoARCashReceipt.getExchangeTableIndex()), false);
		String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
		if (currencyExchangeHeader != null) {
			journalEntryHeader.setCurrencyExchangeHeader(currencyExchangeHeader);
			
				if (UtilRandomKey.isNotBlank(dtoARCashReceipt.getCurrencyID())) {
					if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
						calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
								.getMethod();
					}

						double totalAmount = (double) mathEval
								.eval(Double.valueOf(dtoARCashReceipt.getCashReceiptAmount()) + calcMethod
										+ currentExchangeRate);
						journalEntryHeader.setTotalJournalEntryDebit(totalAmount);
						journalEntryHeader.setTotalJournalEntryCredit(totalAmount);
				}
		}
		    journalEntryHeader = repositoryJournalEntryHeader.save(journalEntryHeader);
			glConfigurationSetup.setNextJournalEntryVoucher(nextJournalId + 1);
			repositoryGLConfigurationSetup.saveAndFlush(glConfigurationSetup);

			//Save In GL JournalEntryDetails
			if(openPaymentDistributionList!=null && !openPaymentDistributionList.isEmpty())
			{
				for (ARYTDOpenPaymentsDistribution arytdOpenPaymentsDistribution : openPaymentDistributionList) 
				{
					JournalEntryDetails journalEntryDetails = new JournalEntryDetails();
					journalEntryDetails.setAccountTableRowIndex(String.valueOf(arytdOpenPaymentsDistribution.getAccountTableRowIndex()));
					journalEntryDetails.setDistributionDescription(arytdOpenPaymentsDistribution.getDistributionDescription());
					if (arytdOpenPaymentsDistribution.getOriginalCreditAmount()!=null && arytdOpenPaymentsDistribution.getOriginalCreditAmount()>0) {
						journalEntryDetails.setBalanceType(2);
					} else {
						journalEntryDetails.setBalanceType(1);
					}
					
					journalEntryDetails.setOriginalCreditAmount(arytdOpenPaymentsDistribution.getOriginalCreditAmount()!=null
							?arytdOpenPaymentsDistribution.getOriginalCreditAmount():0.0);
					journalEntryDetails.setCreditAmount(arytdOpenPaymentsDistribution.getCreditAmount()!=null?
							arytdOpenPaymentsDistribution.getCreditAmount():0.0);
					journalEntryDetails.setOriginalDebitAmount(arytdOpenPaymentsDistribution.getOriginalDebitAmount()!=null?
							arytdOpenPaymentsDistribution.getOriginalDebitAmount():0.0);
					journalEntryDetails.setDebitAmount(arytdOpenPaymentsDistribution.getDebitAmount()!=null?
							arytdOpenPaymentsDistribution.getDebitAmount():0.0);
					journalEntryDetails.setJournalEntryHeader(journalEntryHeader);
					journalEntryDetails.setExchangeTableRate(journalEntryHeader.getExchangeTableRate());
					repositoryJournalEntryDetail.save(journalEntryDetails);
				}
			}
			
			return true;
	}


}
