package com.bti.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.BaseEntity;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.repository.RepositoryEntityManager;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryGlytdOpenTransaction;
import com.bti.repository.RepositoryJournalEntryDetail;
import com.bti.repository.RepositoryJournalEntryHeader;
import com.bti.util.mapping.GL.MappingPostedJournalEntry;

@Service("servicePosting")
public class ServicePosting {
	
	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	@Autowired
	RepositoryEntityManager repositoryEntityManager;
	
	@Autowired
	RepositoryJournalEntryHeader repositoryJournalEntryHeader;
	
	@Autowired
	RepositoryJournalEntryDetail repositoryJournalEntryDetail;
	
	@Autowired
	RepositoryGlytdOpenTransaction repositoryGlytdOpenTransaction;
	
	@Autowired
	MappingPostedJournalEntry mappingPostedJournalEntry;


	public String getNextJournalSequence(String transactionDate, int seriesID, String transactionSource)  {
		
		StringBuilder sequence = new StringBuilder();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		//String[] dateNumbers = sdf.format(transactionDate).split("/");
		String[] dateNumbers = transactionDate.split("/");
		GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
		
		if(glConfigurationSetup.getSequenceType() == 1) { // normal sequence
			// validation before generate number 
			List<Object[]> Journals = repositoryEntityManager.getGlTransactionsByJORNID(glConfigurationSetup.getNextJournalEntryVoucher()+"");
			if((!Journals.isEmpty()) && (Journals.size()>0)) {
				return "FOUND";
			}else {
				sequence.append(glConfigurationSetup.getNextJournalEntryVoucher());
				if(glConfigurationSetup.getIncludeDate()) { // include date sequence 
					
					sequence = new StringBuilder();
					sequence.insert(0,String.format("%04d", glConfigurationSetup.getNextJournalEntryVoucher()));
					sequence.insert(0, String.format("%02d", Integer.valueOf(dateNumbers[1])));
					sequence.insert(0,String.valueOf( dateNumbers[2]).substring(2)  );
					//List = repositoryJournalEntryHeader.findByJournalIDAndIsDeleted(sequence.toString(), false);
					Journals = repositoryEntityManager.getGlTransactionsByJORNID(sequence.toString());
					if((!Journals.isEmpty()) && (Journals.size() > 0)) {
						return "FOUND";
					}
				}
			}
			
		}else if(glConfigurationSetup.getSequenceType() == 2) { // Audit Trial Sequnce
			GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = 
					repositoryGLConfigurationAuditTrialCodes.findOne(seriesID);
			sequence.append(glConfigurationAuditTrialCodes.getNextTransactionSourceNumber());
			// check from gltransactions view
			//JournalEntryHeader journalEntryHeader = repositoryJournalEntryHeader.findByJournalIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(sequence.toString(), seriesID, false);
			List<Object[]> journals = repositoryEntityManager.getGlTransactionByJORNIDAndSerialCodeId(sequence.toString(), glConfigurationAuditTrialCodes.getSeriesIndex());
			if((!journals.isEmpty()) && (journals.size()>0)) {
				return "FOUND";
			}else {
				if(glConfigurationSetup.getIncludeDate()) { // include date
					sequence = new StringBuilder();
					String sequenceResult = repositoryEntityManager.getMonthSequence(String.valueOf(dateNumbers[2]).substring(2) + String.format("%02d", Integer.valueOf(dateNumbers[1])),seriesID);
					if( !sequenceResult.equals( "null") && sequenceResult != "null") { // check if prevouse month exsiste
						if(sequenceResult.length() > 1) {
							String nextNumber = sequenceResult.substring(4, 8);
							int nextValue = Integer.valueOf(nextNumber);
							nextValue++;
							sequence.insert(0,String.format("%04d", nextValue));
						}
					}else {
						sequence.insert(0,String.format("%04d", glConfigurationAuditTrialCodes.getNextTransactionSourceNumber()));
					}
					sequence.insert(0,String.valueOf(dateNumbers[2]).substring(2) + String.format("%02d", Integer.valueOf(dateNumbers[1])) );
					//journalEntryHeader = repositoryJournalEntryHeader.findByJournalIDAndGlConfigurationAuditTrialCodesSeriesIndexAndIsDeleted(sequence.toString(), glConfigurationAuditTrialCodes.getSeriesIndex(), false);
					journals = repositoryEntityManager.getGlTransactionByJORNIDAndSerialCodeId(sequence.toString(), glConfigurationAuditTrialCodes.getSeriesIndex());
					if((!journals.isEmpty()) && (journals.size()>0)) {
						return "FOUND";
					}
				}	
			}
		}
		
		return sequence.toString();
	}
	
	
	public synchronized String saveAndPostingWithSequence(DtoJournalEntryHeader dtoJournalEntryHeader, int flage) throws NumberFormatException, ScriptException {
		
		String nextNumber =getNextJournalSequence(dtoJournalEntryHeader.getTransactionDate(),
				dtoJournalEntryHeader.getSourceDocumentId(),dtoJournalEntryHeader.getTransactionSource() );
		
		//validate before inserting
		if(!nextNumber.equalsIgnoreCase("FOUND")) {
			GLConfigurationSetup glConfigurationSetup = repositoryGLConfigurationSetup.findTop1ByOrderByIdDesc();
			
			if(flage == 1) {// save into gl10100 JV 
				JournalEntryHeader journalHeader = mappingPostedJournalEntry.dtoUnpostedToModelUnposted(dtoJournalEntryHeader);
				
				journalHeader.setJournalID(nextNumber);
				
				repositoryJournalEntryHeader.save(journalHeader);
				
				journalHeader.getJournalDetails().stream().forEach(details -> {
					repositoryJournalEntryDetail.save(details);
				});
				
				if(glConfigurationSetup.getSequenceType() == 1 ) {
					int nextAfterupdate =  glConfigurationSetup.getNextJournalEntryVoucher(); 
					glConfigurationSetup.setNextJournalEntryVoucher(++nextAfterupdate);
					repositoryGLConfigurationSetup.save(glConfigurationSetup);
				}else if(glConfigurationSetup.getSequenceType() == 2) {
					if(!glConfigurationSetup.getIncludeDate()) {
						// update to next number
						GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = 
								repositoryGLConfigurationAuditTrialCodes.findOne(dtoJournalEntryHeader.getSourceDocumentId());
						int nextAfterupdate =  glConfigurationAuditTrialCodes.getNextTransactionSourceNumber();
						glConfigurationAuditTrialCodes.setNextTransactionSourceNumber(++nextAfterupdate);
						repositoryGLConfigurationAuditTrialCodes.save(glConfigurationAuditTrialCodes);	
					}
				}
			}else if(flage == 2) {// save into gl90200 Othres
				List<GLYTDOpenTransactions> jounralToPost = mappingPostedJournalEntry.dtoUnpotedToModelPosted(dtoJournalEntryHeader);
				
				jounralToPost.stream().forEach(journal -> {
					journal.setJournalEntryID(nextNumber);
					repositoryGlytdOpenTransaction.save(journal);
				});
				// update to next number
				if(glConfigurationSetup.getSequenceType() == 1 ) {
					int nextAfterupdate =  glConfigurationSetup.getNextJournalEntryVoucher(); 
					glConfigurationSetup.setNextJournalEntryVoucher(++nextAfterupdate);
					repositoryGLConfigurationSetup.save(glConfigurationSetup);
				}else if(glConfigurationSetup.getSequenceType() == 2) {
					if(!glConfigurationSetup.getIncludeDate()) {
						GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = 
								repositoryGLConfigurationAuditTrialCodes.findOne(dtoJournalEntryHeader.getSourceDocumentId());
						int updateNextNumber = 1+ glConfigurationAuditTrialCodes.getNextTransactionSourceNumber();
						glConfigurationAuditTrialCodes.setNextTransactionSourceNumber(updateNextNumber);
					}
				}
				
			}
		}

		return nextNumber;
	}
	
	
}
