/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.AccountCurrencyAccess;
import com.bti.model.COAMainAccounts;
import com.bti.model.GLConfigurationAccountCategories;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationAuditTrialSeriesType;
import com.bti.model.MainAccountTypes;
import com.bti.model.TPCLBalanceType;
import com.bti.model.dto.DtoARTransactionEntry;
import com.bti.model.dto.DtoAccountCategory;
import com.bti.model.dto.DtoAccountCurrencyAccess;
import com.bti.model.dto.DtoAuditTrailCode;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.model.dto.DtoMainAccountSetUp;
import com.bti.model.dto.DtoMainAccountTypes;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryAccountCurrencyAccess;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLConfigurationAccountCategories;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationAuditTrialSeriesType;
import com.bti.repository.RepositoryMainAccountTypes;
import com.bti.repository.RepositoryModuleConfiguration;
import com.bti.repository.RepositoryTPCLBalanceType;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

/**
 * Description: Service AccountTypes
 * Name of Project: BTI
 * Created on: Aug 10, 2017
 * Modified on: Aug 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service
public class ServiceMainAccountTypes {

	static Logger log = Logger.getLogger(ServiceMainAccountTypes.class.getName());

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	@Autowired
	RepositoryMainAccountTypes repositoryMainAccountTypes;
	@Autowired
	RepositoryGLConfigurationAccountCategories repositoryGLConfigurationAccountCategories;
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts; 
	@Autowired
	ServiceResponse serviceResponse;
	@Autowired
	RepositoryTPCLBalanceType repositoryTPCLBalanceType;
	@Autowired
	RepositoryGLConfigurationAuditTrialSeriesType repositoryGLConfigurationAuditTrialSeriesType;
	@Autowired
	RepositoryAccountCurrencyAccess repositoryAccountCurrencyAccess; 
	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;
	@Autowired
	RepositoryModuleConfiguration repositoryModuleConfiguration;
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @param dtoMainAccountTypes
	 * @return
	 */
	public DtoMainAccountTypes saveAccountType(DtoMainAccountTypes dtoMainAccountTypes) {
		try {
			int loggedInUserId =0;
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
						loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			MainAccountTypes mainAccountTypes = new MainAccountTypes();
			mainAccountTypes.setAccountTypeName(dtoMainAccountTypes.getAccountTypeName());
			mainAccountTypes.setAccountTypeNameArabic(dtoMainAccountTypes.getAccountTypeNameArabic());
			mainAccountTypes.setCreatedBy(loggedInUserId);
			mainAccountTypes.setCreatedDate(new Date());
			mainAccountTypes = repositoryMainAccountTypes.saveAndFlush(mainAccountTypes);
			dtoMainAccountTypes.setAccountTypeId(mainAccountTypes.getAccountTypeId());
			return dtoMainAccountTypes;
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch accountTypeList(DtoSearch dtoSearchs) {
		try {
			DtoSearch dtoSearch = new DtoSearch();
			dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
			dtoSearch.setPageSize(dtoSearchs.getPageSize());
			dtoSearch.setTotalCount(
					repositoryMainAccountTypes.predictiveMainAccountTypeSearchCount(dtoSearchs.getSearchKeyword()));
			List<DtoMainAccountTypes> dtoMainAccountTypesList = new ArrayList<>();
			List<MainAccountTypes> list = null;

			if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
						"createdDate");
				list = repositoryMainAccountTypes
						.predictiveMainAccountTypeSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
			} else {
				list = repositoryMainAccountTypes.predictiveMainAccountTypeSearch(dtoSearchs.getSearchKeyword());
			}

			if (list != null) {
				for (MainAccountTypes accountTypes : list) {
					dtoMainAccountTypesList.add(new DtoMainAccountTypes(accountTypes));
				}
			}
			dtoSearch.setRecords(dtoMainAccountTypesList);
			return dtoSearch;
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
			return null;
		}
	}

	/**
	 * @param dtoAccountCategory
	 * @return
	 */
	public DtoAccountCategory saveAccountCategory(DtoAccountCategory dtoAccountCategory) {
		try {
			int loggedInUserId =0;
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
						loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			GLConfigurationAccountCategories glConfigurationAccountCategories = new GLConfigurationAccountCategories();
			glConfigurationAccountCategories.setAccountCategoryDescription(dtoAccountCategory.getAccountCategoryDescription());
			glConfigurationAccountCategories.setAccountCategoryDescriptionArabic(dtoAccountCategory.getAccountCategoryDescriptionArabic());
			glConfigurationAccountCategories.setCreatedBy(loggedInUserId);
			glConfigurationAccountCategories.setPreDefined(0);
			glConfigurationAccountCategories.setCreatedDate(new Date());
			glConfigurationAccountCategories = repositoryGLConfigurationAccountCategories.saveAndFlush(glConfigurationAccountCategories);
			dtoAccountCategory.setAccountCategoryId(glConfigurationAccountCategories.getAccountCategoryId());
			dtoAccountCategory.setPreDefined(glConfigurationAccountCategories.getPreDefined());
			return dtoAccountCategory;
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch accountCategoryList(DtoSearch dtoSearchs) {
		try {
			DtoSearch dtoSearch = new DtoSearch();
			dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
			dtoSearch.setPageSize(dtoSearchs.getPageSize());
			dtoSearch.setTotalCount(repositoryGLConfigurationAccountCategories.predictiveAccountCategorySearchCount(dtoSearchs.getSearchKeyword()));
			List<DtoAccountCategory> dtoAccountCategoryList = new ArrayList<>();
			List<GLConfigurationAccountCategories> list = null;
			if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
						"createdDate");
				list = repositoryGLConfigurationAccountCategories.predictiveAccountCategorySearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
			} else {
			 
				list = repositoryGLConfigurationAccountCategories.predictiveAccountCategorySearch(dtoSearchs.getSearchKeyword());
			}
			
			if (list != null) {
				for (GLConfigurationAccountCategories accountCategory : list) {
					dtoAccountCategoryList.add(new DtoAccountCategory(accountCategory));
				}
			}
			dtoSearch.setRecords(dtoAccountCategoryList);
			return dtoSearch;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchAuditTrialCode(DtoSearch dtoSearchs) {
		try {
			int langId = serviceHome.getLanngugaeId();
			DtoSearch dtoSearch = new DtoSearch();
			dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
			dtoSearch.setPageSize(dtoSearchs.getPageSize());
			dtoSearch.setTotalCount(repositoryGLConfigurationAuditTrialCodes.predictiveAuditTrialCodeSearchCount(dtoSearchs.getSearchKeyword()));
			List<DtoAuditTrailCode> dtoAuditTrailCodeList = new ArrayList<>();
		 
			List<GLConfigurationAuditTrialCodes> list = null;
			if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
				Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
						"createdDate");
				list = repositoryGLConfigurationAuditTrialCodes.predictiveAuditTrialCodeSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
			} else {
			 
				list = repositoryGLConfigurationAuditTrialCodes.predictiveAuditTrialCodeSearch(dtoSearchs.getSearchKeyword());
			}
			if (list != null) {
				for (GLConfigurationAuditTrialCodes auditTrailCode : list) {
					GLConfigurationAuditTrialSeriesType seriesType= auditTrailCode.getGlConfigurationAuditTrialSeriesType();
					if(seriesType!=null && auditTrailCode.getGlConfigurationAuditTrialSeriesType()
							.getLanguage().getLanguageId()!=langId)
					{
						seriesType= repositoryGLConfigurationAuditTrialSeriesType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId(seriesType.getTypeId(), false,langId);
					}
					dtoAuditTrailCodeList.add(new DtoAuditTrailCode(auditTrailCode,seriesType));
				}
			}
			dtoSearch.setRecords(dtoAuditTrailCodeList);
			return dtoSearch;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param dtoAuditTrailCode
	 * @return
	 */
	public DtoAuditTrailCode saveAuditTrialCode(DtoAuditTrailCode dtoAuditTrailCode) {
		try {
			int langId= serviceHome.getLanngugaeId(); 
			int loggedInUserId =0;
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
						loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			GLConfigurationAuditTrialCodes gLConfigurationAuditTrialCodes = new GLConfigurationAuditTrialCodes();
			gLConfigurationAuditTrialCodes.setSourceDocument(dtoAuditTrailCode.getSourceDocument());
			gLConfigurationAuditTrialCodes.setTransactionSourceCode(dtoAuditTrailCode.getSourceCode());
			gLConfigurationAuditTrialCodes.setCreatedBy(loggedInUserId);
			gLConfigurationAuditTrialCodes.setNextTransactionSourceNumber(dtoAuditTrailCode.getSeriesNumber());
			if(dtoAuditTrailCode.getSeriesId()!=null && dtoAuditTrailCode.getSeriesId()>0){
				GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType=repositoryGLConfigurationAuditTrialSeriesType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId(dtoAuditTrailCode.getSeriesId(), false,langId);
				if(glConfigurationAuditTrialSeriesType!=null){
					gLConfigurationAuditTrialCodes.setGlConfigurationAuditTrialSeriesType(glConfigurationAuditTrialSeriesType);
				}
				GLConfigurationAuditTrialCodes gLConfigurationAuditTrialCodes1 = repositoryGLConfigurationAuditTrialCodes.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(dtoAuditTrailCode.getSeriesId(),false);
				if(gLConfigurationAuditTrialCodes1!=null){
					gLConfigurationAuditTrialCodes.setSequenceNumber(gLConfigurationAuditTrialCodes1.getSequenceNumber()+1);
				}else{
					gLConfigurationAuditTrialCodes.setSequenceNumber(1);
				}
			}else{
				gLConfigurationAuditTrialCodes.setSequenceNumber(1);
			}
			gLConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes.saveAndFlush(gLConfigurationAuditTrialCodes);
			dtoAuditTrailCode.setSeriesIndex(gLConfigurationAuditTrialCodes.getSeriesIndex());
			return getAuditTrialCodeById(dtoAuditTrailCode);
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoMainAccountTypes
	 * @return
	 */
	public DtoMainAccountTypes updateAccountType(DtoMainAccountTypes dtoMainAccountTypes) {
		try {
			int loggedInUserId =0;
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
						loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			MainAccountTypes mainAccountTypes = repositoryMainAccountTypes.findByAccountTypeIdAndIsDeleted(dtoMainAccountTypes.getAccountTypeId(),false);
			if(mainAccountTypes!=null){
				mainAccountTypes.setAccountTypeName(dtoMainAccountTypes.getAccountTypeName());
				mainAccountTypes.setAccountTypeNameArabic(dtoMainAccountTypes.getAccountTypeNameArabic());
				mainAccountTypes.setChangeBy(loggedInUserId);
				mainAccountTypes.setModifyDate(new Date());
				repositoryMainAccountTypes.saveAndFlush(mainAccountTypes);
				return dtoMainAccountTypes;
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoMainAccountTypes
	 * @return
	 */
	public DtoMainAccountTypes getAccountTypeById(DtoMainAccountTypes dtoMainAccountTypes) {
		MainAccountTypes mainAccountTypes = repositoryMainAccountTypes.findByAccountTypeIdAndIsDeleted(dtoMainAccountTypes.getAccountTypeId(),false);
		if(mainAccountTypes!=null){
			return new DtoMainAccountTypes(mainAccountTypes);
		}
		return null;
	}
	
	/**
	 * @return
	 */
	public List<DtoStatusType> getAuditTrialSeriesTypeStatus() {
		int langId=serviceHome.getLanngugaeId();
		List<GLConfigurationAuditTrialSeriesType> glConfigurationAuditTrialSeriesTypeList = repositoryGLConfigurationAuditTrialSeriesType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> list = new ArrayList<>();
		if(glConfigurationAuditTrialSeriesTypeList!=null){
			glConfigurationAuditTrialSeriesTypeList.forEach(glConfigurationAuditTrialSeriesType -> list.add(new DtoStatusType(glConfigurationAuditTrialSeriesType)));
		}
		return list;
	}

	/**
	 * @param dtoAccountCategory
	 * @return
	 */
	public DtoAccountCategory updateAccountCategory(DtoAccountCategory dtoAccountCategory,GLConfigurationAccountCategories glConfigurationAccountCategories) {
		try {
			int loggedInUserId =0;
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
						loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			
			if(glConfigurationAccountCategories!=null){
				glConfigurationAccountCategories.setAccountCategoryDescription(dtoAccountCategory.getAccountCategoryDescription());
				glConfigurationAccountCategories.setAccountCategoryDescriptionArabic(dtoAccountCategory.getAccountCategoryDescriptionArabic());
				glConfigurationAccountCategories.setChangeBy(loggedInUserId);
				glConfigurationAccountCategories.setModifyDate(new Date());
				glConfigurationAccountCategories.setPreDefined(0);
				repositoryGLConfigurationAccountCategories.saveAndFlush(glConfigurationAccountCategories);
				dtoAccountCategory.setPreDefined(glConfigurationAccountCategories.getPreDefined());
				return dtoAccountCategory;
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoAccountCategory
	 * @return
	 */
	public DtoAccountCategory getAccountCategoryById(DtoAccountCategory dtoAccountCategory) {
		GLConfigurationAccountCategories glConfigurationAccountCategories = repositoryGLConfigurationAccountCategories.findByAccountCategoryIdAndIsDeleted(dtoAccountCategory.getAccountCategoryId(),false);
		if(glConfigurationAccountCategories!=null){
			return new DtoAccountCategory(glConfigurationAccountCategories);
		}
		return null;
	}

	/**
	 * @param dtoAuditTrailCode
	 * @return
	 */
	public DtoAuditTrailCode updateAuditTrialCode(DtoAuditTrailCode dtoAuditTrailCode) {
		try {
			int loggedInUserId =0;
			int langId= serviceHome.getLanngugaeId();
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
						loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes.findBySeriesIndexAndIsDeleted(dtoAuditTrailCode.getSeriesIndex(),false);
			if(glConfigurationAuditTrialCodes!=null){
				glConfigurationAuditTrialCodes.setSourceDocument(dtoAuditTrailCode.getSourceDocument());
				glConfigurationAuditTrialCodes.setTransactionSourceCode(dtoAuditTrailCode.getSourceCode());
				glConfigurationAuditTrialCodes.setNextTransactionSourceNumber(dtoAuditTrailCode.getSeriesNumber());
				if(dtoAuditTrailCode.getSeriesId()!=null && dtoAuditTrailCode.getSeriesId()>0){
					
					GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType=repositoryGLConfigurationAuditTrialSeriesType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId(dtoAuditTrailCode.getSeriesId(), false,langId);
					if(glConfigurationAuditTrialSeriesType!=null){
						glConfigurationAuditTrialCodes.setGlConfigurationAuditTrialSeriesType(glConfigurationAuditTrialSeriesType);
					}
					GLConfigurationAuditTrialCodes gLConfigurationAuditTrialCodes1 = repositoryGLConfigurationAuditTrialCodes.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(dtoAuditTrailCode.getSeriesId(),false);
					if(gLConfigurationAuditTrialCodes1!=null){
						glConfigurationAuditTrialCodes.setSequenceNumber(gLConfigurationAuditTrialCodes1.getSequenceNumber()+1);
					}else{
						glConfigurationAuditTrialCodes.setSequenceNumber(1);
					}
				} 
				glConfigurationAuditTrialCodes.setChangeBy(loggedInUserId);
				glConfigurationAuditTrialCodes.setModifyDate(new Date());
				repositoryGLConfigurationAuditTrialCodes.saveAndFlush(glConfigurationAuditTrialCodes);
				return getAuditTrialCodeById(dtoAuditTrailCode);
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoAuditTrailCode
	 * @return
	 */
	public DtoAuditTrailCode getAuditTrialCodeById(DtoAuditTrailCode dtoAuditTrailCode) {
		int langId=serviceHome.getLanngugaeId(); 
		GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes.findBySeriesIndexAndIsDeleted(dtoAuditTrailCode.getSeriesIndex(),false);
		if(glConfigurationAuditTrialCodes!=null){
			GLConfigurationAuditTrialSeriesType seriesType= glConfigurationAuditTrialCodes.getGlConfigurationAuditTrialSeriesType();
			if(seriesType!=null && glConfigurationAuditTrialCodes.getGlConfigurationAuditTrialSeriesType()
					.getLanguage().getLanguageId()!=langId)
			{
				seriesType= repositoryGLConfigurationAuditTrialSeriesType.findTopOneByTypeIdAndIsDeletedAndLanguageLanguageId(seriesType.getTypeId(), false,langId);
			}
			return new DtoAuditTrailCode(glConfigurationAuditTrialCodes,seriesType);
		}
		return null;
	}

	/**
	 * @param dtoMainAccountSetUp
	 * @return
	 */
	public DtoMainAccountSetUp saveMainAccountSetup(DtoMainAccountSetUp dtoMainAccountSetUp){
		try{
			int loggedInUserId =0;
			int langid = serviceHome.getLanngugaeId();
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
						loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			COAMainAccounts mainAccount = repositoryCOAMainAccounts.findByMainAccountNumberAndIsDeleted(dtoMainAccountSetUp.getMainAccountNumber(),false);
			if(mainAccount!=null){ 
				dtoMainAccountSetUp.setMessageType("ACCOUNT_NUMBER_ALREADY_EXIST");
				return dtoMainAccountSetUp;
			}
			
			mainAccount = new COAMainAccounts();
			GLConfigurationAccountCategories glConfigurationAccountCategories = repositoryGLConfigurationAccountCategories.findByAccountCategoryIdAndIsDeleted(dtoMainAccountSetUp.getAccountCategoryId(),false);
			if(glConfigurationAccountCategories!=null){
				mainAccount.setGlConfigurationAccountCategories(glConfigurationAccountCategories);
			}else{
				dtoMainAccountSetUp.setMessageType("ACCOUNT_CATEGORY_NOT_FOUND");
				return dtoMainAccountSetUp;
			}
			MainAccountTypes mainAccountTypes = repositoryMainAccountTypes.findByAccountTypeIdAndIsDeleted(dtoMainAccountSetUp.getAccountTypeId(),false);
			if(mainAccountTypes!=null){
				mainAccount.setMainAccountTypes(mainAccountTypes);
			}else{
				dtoMainAccountSetUp.setMessageType("ACCOUNT_TYPE_NOT_FOUND");
				return dtoMainAccountSetUp;
			}
			TPCLBalanceType tpclBalanceType = repositoryTPCLBalanceType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoMainAccountSetUp.getTpclblnc(),langid,false);
			if(tpclBalanceType!=null){
				mainAccount.setTpclBalanceType(tpclBalanceType);
			}
			mainAccount.setActive(dtoMainAccountSetUp.getActive());
			mainAccount.setAliasAccount(dtoMainAccountSetUp.getAliasAccount());
			mainAccount.setAliasAccountArabic(dtoMainAccountSetUp.getAliasAccountArabic());
			mainAccount.setAllowAccountTransactionEntry(dtoMainAccountSetUp.getAllowAccountTransactionEntry());
			mainAccount.setCreatedBy(loggedInUserId);
			mainAccount.setMainAccountDescription(dtoMainAccountSetUp.getMainAccountDescription());
			mainAccount.setMainAccountDescriptionArabic(dtoMainAccountSetUp.getMainAccountDescriptionArabic());
			mainAccount.setMainAccountNumber(dtoMainAccountSetUp.getMainAccountNumber());
			mainAccount.setUserDef1(dtoMainAccountSetUp.getUserDef1());
			mainAccount.setUserDef2(dtoMainAccountSetUp.getUserDef2());
			mainAccount.setUserDef3(dtoMainAccountSetUp.getUserDef3());
			mainAccount.setUserDef4(dtoMainAccountSetUp.getUserDef4());
			mainAccount.setUserDef5(dtoMainAccountSetUp.getUserDef5());
			mainAccount = repositoryCOAMainAccounts.saveAndFlush(mainAccount);
			dtoMainAccountSetUp.setMainAccountNumber(mainAccount.getMainAccountNumber());
			return dtoMainAccountSetUp;
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}
	
	/**
	 * @param dtoMainAccountSetUp
	 * @return
	 */
	public DtoMainAccountSetUp updateMainAccountSetup(DtoMainAccountSetUp dtoMainAccountSetUp) {
		try{
			int loggedInUserId =0;
			int langid = serviceHome.getLanngugaeId();
			if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
						loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			}
			COAMainAccounts mainAccount = repositoryCOAMainAccounts.findByMainAccountNumberAndIsDeleted(dtoMainAccountSetUp.getMainAccountNumber(),false);
			if(mainAccount!=null){
				GLConfigurationAccountCategories glConfigurationAccountCategories = repositoryGLConfigurationAccountCategories.findByAccountCategoryIdAndIsDeleted(dtoMainAccountSetUp.getAccountCategoryId(),false);
				if(glConfigurationAccountCategories!=null){
					mainAccount.setGlConfigurationAccountCategories(glConfigurationAccountCategories);
				}else{
					dtoMainAccountSetUp.setMessageType("ACCOUNT_CATEGORY_NOT_FOUND");
					return dtoMainAccountSetUp;
				}
				
				MainAccountTypes mainAccountTypes = repositoryMainAccountTypes.findByAccountTypeIdAndIsDeleted(dtoMainAccountSetUp.getAccountTypeId(),false);
				if(mainAccountTypes!=null){
					mainAccount.setMainAccountTypes(mainAccountTypes);
				}else{
					dtoMainAccountSetUp.setMessageType("ACCOUNT_TYPE_NOT_FOUND");
					return dtoMainAccountSetUp;
				}
				
				TPCLBalanceType tpclBalanceType = repositoryTPCLBalanceType.findTopOneByTypeIdAndLanguageLanguageIdAndIsDeleted(dtoMainAccountSetUp.getTpclblnc(),langid,false);
				if(tpclBalanceType!=null){
					mainAccount.setTpclBalanceType(tpclBalanceType);
				}
				
				mainAccount.setActive(dtoMainAccountSetUp.getActive());
				mainAccount.setAliasAccount(dtoMainAccountSetUp.getAliasAccount());
				mainAccount.setAliasAccountArabic(dtoMainAccountSetUp.getAliasAccountArabic());
				mainAccount.setAllowAccountTransactionEntry(dtoMainAccountSetUp.getAllowAccountTransactionEntry());
				mainAccount.setChangeBy(loggedInUserId);
				mainAccount.setMainAccountDescription(dtoMainAccountSetUp.getMainAccountDescription());
				mainAccount.setMainAccountDescriptionArabic(dtoMainAccountSetUp.getMainAccountDescriptionArabic());
				mainAccount.setMainAccountNumber(dtoMainAccountSetUp.getMainAccountNumber());
				mainAccount.setUserDef1(dtoMainAccountSetUp.getUserDef1());
				mainAccount.setUserDef2(dtoMainAccountSetUp.getUserDef2());
				mainAccount.setUserDef3(dtoMainAccountSetUp.getUserDef3());
				mainAccount.setUserDef4(dtoMainAccountSetUp.getUserDef4());
				mainAccount.setUserDef5(dtoMainAccountSetUp.getUserDef5());
				 repositoryCOAMainAccounts.saveAndFlush(mainAccount);
				return dtoMainAccountSetUp;
			}
			
			
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}
	
	/**
	 * @param dtoMainAccountSetUp
	 * @return
	 */
	public DtoMainAccountSetUp getMainAccountSetupById(DtoMainAccountSetUp dtoMainAccountSetUp) {
		try{
			
			COAMainAccounts mainAccount = repositoryCOAMainAccounts.findByActIndxAndIsDeleted(dtoMainAccountSetUp.getActIndx(),false);
			if(mainAccount!=null){ 
				return new DtoMainAccountSetUp(mainAccount);
			}
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchMainAccountSetup(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch= new DtoSearch();
	    dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
	    dtoSearch.setPageSize(dtoSearchs.getPageSize());
	    dtoSearch.setTotalCount(repositoryCOAMainAccounts.predictiveCOAMainAccountsSearchCount(dtoSearchs.getSearchKeyword()));
        List<COAMainAccounts> coaMainAccountsList=null;
        List<DtoMainAccountSetUp> dtoMainAccountSetUpList = new ArrayList<>();
        if(dtoSearchs.getPageNumber()!=null && dtoSearchs.getPageSize()!= null){
        	Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.ASC,
					"mainAccountNumber");
        	coaMainAccountsList=repositoryCOAMainAccounts.predictiveCOAMainAccountsSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
        }
        else
        {
        	coaMainAccountsList=repositoryCOAMainAccounts.predictiveCOAMainAccountsSearch(dtoSearchs.getSearchKeyword());
        }
        
        if(coaMainAccountsList!=null)
        {
        	coaMainAccountsList.forEach(coaMainAccounts -> dtoMainAccountSetUpList.add(new DtoMainAccountSetUp(coaMainAccounts)));
        	 
        }
        dtoSearch.setRecords(dtoMainAccountSetUpList);
        return dtoSearch;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getTypicalBalanceTypeStatus() {
		int langId=serviceHome.getLanngugaeId();
		List<TPCLBalanceType> list = repositoryTPCLBalanceType.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if(list!=null){
			list.forEach(tpclBalanceType -> responseList.add(new DtoStatusType(tpclBalanceType)));
		}
		return responseList;
	}

	/**
	 * @param dtoAccountCurrencyAccess
	 * @return
	 */
	public DtoAccountCurrencyAccess saveAccountCurrencyAccess(DtoAccountCurrencyAccess dtoAccountCurrencyAccess) {
		try{
			AccountCurrencyAccess accountCurrencyAccess=null;
			if(dtoAccountCurrencyAccess.getCurrencyId()!=null && !dtoAccountCurrencyAccess.getCurrencyId().isEmpty() && dtoAccountCurrencyAccess.getAccountIndex()!=null )
			{
					COAMainAccounts coAMainAccounts = repositoryCOAMainAccounts.findByActIndxAndIsDeleted(dtoAccountCurrencyAccess.getAccountIndex(),false);
					if(coAMainAccounts!=null)
					{
							List<AccountCurrencyAccess> deleteList = repositoryAccountCurrencyAccess.findByCoaMainAccountsActIndx(coAMainAccounts.getActIndx());
							if(deleteList!=null && !deleteList.isEmpty()){
								repositoryAccountCurrencyAccess.deleteInBatch(deleteList);
							}
							
							int loggedInUserId =0;
							if(UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))){
										loggedInUserId= Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
							}
						
						    dtoAccountCurrencyAccess.setMessageType("ACCOUNT_CURRENCY_ACCESS_SAVED_SUCCESS");
						    for(String currencyId :dtoAccountCurrencyAccess.getCurrencyId() ){
						    	accountCurrencyAccess = new AccountCurrencyAccess();
								accountCurrencyAccess.setChangeBy(loggedInUserId);
								accountCurrencyAccess.setCoaMainAccounts(coAMainAccounts);
								accountCurrencyAccess.setCreatedBy(loggedInUserId);
								accountCurrencyAccess.setCurrencySetup(repositoryCurrencySetup.findByCurrencyIdAndIsDeleted(currencyId,false));
								accountCurrencyAccess=repositoryAccountCurrencyAccess.saveAndFlush(accountCurrencyAccess);
						    }
					}
			}
			if(accountCurrencyAccess!=null){
				DtoAccountCurrencyAccess response = getAccountCurrencyAccess(accountCurrencyAccess);
				 response.setMessageType(dtoAccountCurrencyAccess.getMessageType());
				 return response;
			}
			else{
				dtoAccountCurrencyAccess.setMessageType("ACCOUNT_CURRENCY_ACCESS_FAIL");
				return dtoAccountCurrencyAccess;
			}
			 
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param accountCurrencyAccess
	 * @return
	 */
	public DtoAccountCurrencyAccess getAccountCurrencyAccess(AccountCurrencyAccess accountCurrencyAccess) {
		try{
				List<String> currenctIds = repositoryAccountCurrencyAccess.getCurrecyIdByMainAccountActIndex(accountCurrencyAccess.getCoaMainAccounts().getActIndx());
				DtoAccountCurrencyAccess dtoAccountCurrencyAccess= new DtoAccountCurrencyAccess(accountCurrencyAccess,currenctIds);
				dtoAccountCurrencyAccess.setMessageType("RECORD_ALREADY_EXIST");
				return dtoAccountCurrencyAccess;
			
		}catch(Exception e){
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	public DtoSearch searchActiveMainAccountSetup(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch= new DtoSearch();
	    dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
	    dtoSearch.setPageSize(dtoSearchs.getPageSize());
	    dtoSearch.setTotalCount(repositoryCOAMainAccounts.predictiveActiveCOAMainAccountsSearchCount(dtoSearchs.getSearchKeyword()));
        List<COAMainAccounts> coaMainAccountsList=null;
        List<DtoMainAccountSetUp> dtoMainAccountSetUpList = new ArrayList<>();
        if(dtoSearchs.getPageNumber()!=null && dtoSearchs.getPageSize()!= null){
        	Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC,
					"createdDate");
        	coaMainAccountsList=repositoryCOAMainAccounts.predictiveActiveCOAMainAccountsSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
        }
        else
        {
        	coaMainAccountsList=repositoryCOAMainAccounts.predictiveActiveCOAMainAccountsSearch(dtoSearchs.getSearchKeyword());
        }
        
        if(coaMainAccountsList!=null)
        {
        	for (COAMainAccounts coaMainAccounts : coaMainAccountsList) 
        	{
        		dtoMainAccountSetUpList.add(new DtoMainAccountSetUp(coaMainAccounts));
			}
        }
        dtoSearch.setRecords(dtoMainAccountSetUpList);
        return dtoSearch;
	}
	
	/**
	 * @param dtoAccountCategory
	 * @return
	 */
	public DtoAccountCategory getAccountCategoryByName(DtoAccountCategory dtoAccountCategory) {
		GLConfigurationAccountCategories glConfigurationAccountCategories = repositoryGLConfigurationAccountCategories.findByAccountCategoryDescriptionAndIsDeleted(dtoAccountCategory.getCategoryName(),false);
		if(glConfigurationAccountCategories!=null){
			return new DtoAccountCategory(glConfigurationAccountCategories);
		}
		return null;
	}
	
	/**
	 * @param dtoMainAccountTypes
	 * @return
	 */
	public DtoMainAccountTypes getAccountTypeByName(DtoMainAccountTypes dtoMainAccountTypes) {
		MainAccountTypes mainAccountTypes = repositoryMainAccountTypes.findByAccountTypeNameAndIsDeleted(dtoMainAccountTypes.getAccountTypeName(),false);
		if(mainAccountTypes!=null){
			return new DtoMainAccountTypes(mainAccountTypes);
		}
		return null;
	}
	
	
}