/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.model.CityMaster;
import com.bti.model.CountryMaster;
import com.bti.model.CustomerClassesSetup;
import com.bti.model.CustomerMaintenance;
import com.bti.model.CustomerMaintenanceActiveStatus;
import com.bti.model.CustomerMaintenanceHoldStatus;
import com.bti.model.SalesmanMaintenance;
import com.bti.model.StateMaster;
import com.bti.model.dto.DtoCustomerMaintenance;
import com.bti.model.dto.DtoSalesmanMaintenance;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoStatusType;
import com.bti.repository.RepositoryCityMaster;
import com.bti.repository.RepositoryCountryMaster;
import com.bti.repository.RepositoryCustomerClassesSetup;
import com.bti.repository.RepositoryCustomerMaintenance;
import com.bti.repository.RepositoryCustomerMaintenanceActiveStatus;
import com.bti.repository.RepositoryCustomerMaintenanceHoldStatus;
import com.bti.repository.RepositorySalesmanMaintenance;
import com.bti.repository.RepositoryStateMaster;
import com.bti.util.UtilRandomKey;

@Service("serviceCustomerMaintenance")
public class ServiceCustomerMaintenance {

	static Logger log = Logger.getLogger(ServiceCustomerMaintenance.class.getName());
	@Autowired
	RepositoryCustomerMaintenance repositoryCustomerMaintenance;

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryCountryMaster repositoryCountryMaster;

	@Autowired
	RepositoryCityMaster repositoryCityMaster;

	@Autowired
	RepositoryStateMaster repositoryStateMaster;

	@Autowired
	RepositoryCustomerMaintenanceHoldStatus repositoryCustomerMaintenanceHoldStatus;

	@Autowired
	RepositoryCustomerMaintenanceActiveStatus repositoryCustomerMaintenanceActiveStatus;

	@Autowired
	RepositoryCustomerClassesSetup repositoryCustomerClassesSetup;

	@Autowired
	RepositorySalesmanMaintenance repositorySalesmanMaintenance;
	
	@Autowired
	ServiceHome serviceHome;
	
	/**
	 * @param dtoCustomerMaintenance
	 * @return
	 */
	public DtoCustomerMaintenance saveCustomerMaintenanceData(DtoCustomerMaintenance dtoCustomerMaintenance) {
		log.info("inside saveCustomerMaintenanceData method");
		try {
			CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance
					.findByCustnumberAndIsDeleted(dtoCustomerMaintenance.getCustomerId(),false);
			if (customerMaintenance != null) {
				dtoCustomerMaintenance.setMessageType("CUSTOMER_MAINTENANCE_ALREADY_EXIST");
				return dtoCustomerMaintenance;
			}
			CustomerClassesSetup customerClassesSetup = repositoryCustomerClassesSetup
					.findByCustomerClassIdAndIsDeleted(dtoCustomerMaintenance.getClassId(),false);
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			customerMaintenance = new CustomerMaintenance();
			customerMaintenance.setCustomerClassesSetup(customerClassesSetup);
			customerMaintenance.setCustnumber(dtoCustomerMaintenance.getCustomerId());
			customerMaintenance.setAddress1(dtoCustomerMaintenance.getAddress1());
			customerMaintenance.setAddress2(dtoCustomerMaintenance.getAddress2());
			customerMaintenance.setAddress3(dtoCustomerMaintenance.getAddress3());
			customerMaintenance.setCity(dtoCustomerMaintenance.getCityName());
			customerMaintenance.setCounrty(dtoCustomerMaintenance.getCountryName());
			customerMaintenance.setState(dtoCustomerMaintenance.getStateName());
			customerMaintenance.setCreatedBy(loggedInUserId);
			customerMaintenance.setCreatedDate(new Date());
			customerMaintenance.setCustomerShortName(dtoCustomerMaintenance.getShortName());
			customerMaintenance.setCustomerName(dtoCustomerMaintenance.getName());
			customerMaintenance.setCustomerNameArabic(dtoCustomerMaintenance.getArabicName());
			customerMaintenance.setCustomerStateName(dtoCustomerMaintenance.getStatementName());
			customerMaintenance.setPhoneNumber1(dtoCustomerMaintenance.getPhone1());
			customerMaintenance.setPhoneNumber2(dtoCustomerMaintenance.getPhone2());
			customerMaintenance.setPhoneNumber3(dtoCustomerMaintenance.getPhone3());
			customerMaintenance.setFax1(dtoCustomerMaintenance.getFax());
			customerMaintenance.setUserDefine1(dtoCustomerMaintenance.getUserDefine1());
			customerMaintenance.setUserDefine2(dtoCustomerMaintenance.getUserDefine2());
			customerMaintenance.setCustomerHoldStatus(dtoCustomerMaintenance.getCustomerHold());
			customerMaintenance.setCustomerActiveStatus(dtoCustomerMaintenance.getActiveCustomer());
			if (UtilRandomKey.isNotBlank(dtoCustomerMaintenance.getPriority())) {
				customerMaintenance.setCustomerPriority(dtoCustomerMaintenance.getPriority());
			} else {
				customerMaintenance.setCustomerPriority("1");
			}
			repositoryCustomerMaintenance.saveAndFlush(customerMaintenance);
			return dtoCustomerMaintenance;
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
			return null;
		}
	}

	/**
	 * @param dtoCustomerMaintenance
	 * @return
	 */
	public DtoCustomerMaintenance maintenanceGetById(DtoCustomerMaintenance dtoCustomerMaintenance) {
		log.info("inside maintenanceGetById method");
		int langId = serviceHome.getLanngugaeId();
		try {
			CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance
					.findByCustnumberAndIsDeleted(dtoCustomerMaintenance.getCustomerId(),false);
			if (customerMaintenance != null) {
				dtoCustomerMaintenance = new DtoCustomerMaintenance(customerMaintenance);
				dtoCustomerMaintenance.setCityId(0);
				dtoCustomerMaintenance.setStateId(0);
				dtoCustomerMaintenance.setCountryId(0);
				CountryMaster countryMaster=repositoryCountryMaster.findByCountryNameAndIsDeletedAndIsActive(dtoCustomerMaintenance.getCountryName(),false,true);
				if(countryMaster!=null && countryMaster.getLanguage().getLanguageId()!=langId){
					countryMaster=repositoryCountryMaster.findByCountryCodeAndIsDeletedAndIsActiveAndLanguageLanguageId(countryMaster.getCountryCode(), false, true,langId);
				}
				
				if(countryMaster!=null)
				{
					dtoCustomerMaintenance.setCountryId(countryMaster.getCountryId());
					StateMaster stateMaster=repositoryStateMaster.findByStateNameAndCountryMasterCountryCodeAndIsDeleted(dtoCustomerMaintenance.getStateName(),countryMaster.getCountryCode(),false);
					if(stateMaster!=null && stateMaster.getLanguage().getLanguageId()!=langId){
						stateMaster= repositoryStateMaster.findByStateCodeAndCountryMasterCountryCodeAndLanguageLanguageIdAndIsDeleted(stateMaster.getStateCode(), countryMaster.getCountryCode(),langId,false);
					}
					
					if(stateMaster!= null)
					{
						dtoCustomerMaintenance.setStateId(stateMaster.getStateId());
						CityMaster cityMaster=repositoryCityMaster.findByCityNameAndStateMasterStateCodeAndIsDeleted(dtoCustomerMaintenance.getCityName(),stateMaster.getStateCode(),false);
						if(cityMaster!=null && cityMaster.getLanguage().getLanguageId()!=langId)
						{
							cityMaster=repositoryCityMaster.findByCityCodeAndStateMasterStateCodeAndIsDeleted(cityMaster.getCityCode(),stateMaster.getStateCode(),false);
						}
						if(cityMaster!=null){
							dtoCustomerMaintenance.setCityId(cityMaster.getCityId());
						}
					}
				}
				return dtoCustomerMaintenance;
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
			return null;
		}
		return null;
	}

	/**
	 * @param dtoCustomerMaintenance
	 * @return
	 */
	public DtoCustomerMaintenance updateMaintenanceData(DtoCustomerMaintenance dtoCustomerMaintenance) {
		log.info("inside updateMaintenanceData method");
		try {
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance
					.findByCustnumberAndIsDeleted(dtoCustomerMaintenance.getCustomerId(),false);
			if (customerMaintenance != null) {
				CustomerClassesSetup customerClassesSetup = repositoryCustomerClassesSetup
						.findByCustomerClassIdAndIsDeleted(dtoCustomerMaintenance.getClassId(),false);
				customerMaintenance.setCustomerClassesSetup(customerClassesSetup);
				customerMaintenance.setAddress1(dtoCustomerMaintenance.getAddress1());
				customerMaintenance.setAddress2(dtoCustomerMaintenance.getAddress2());
				customerMaintenance.setAddress3(dtoCustomerMaintenance.getAddress3());
				customerMaintenance.setCity(dtoCustomerMaintenance.getCityName());
				customerMaintenance.setCounrty(dtoCustomerMaintenance.getCountryName());
				customerMaintenance.setState(dtoCustomerMaintenance.getStateName());
				customerMaintenance.setChangeBy(loggedInUserId);
				customerMaintenance.setCustomerShortName(dtoCustomerMaintenance.getShortName());
				customerMaintenance.setCustomerName(dtoCustomerMaintenance.getName());
				customerMaintenance.setCustomerNameArabic(dtoCustomerMaintenance.getArabicName());
				customerMaintenance.setCustomerStateName(dtoCustomerMaintenance.getStatementName());
				customerMaintenance.setPhoneNumber1(dtoCustomerMaintenance.getPhone1());
				customerMaintenance.setPhoneNumber2(dtoCustomerMaintenance.getPhone2());
				customerMaintenance.setPhoneNumber3(dtoCustomerMaintenance.getPhone3());
				customerMaintenance.setFax1(dtoCustomerMaintenance.getFax());
				customerMaintenance.setUserDefine1(dtoCustomerMaintenance.getUserDefine1());
				customerMaintenance.setUserDefine2(dtoCustomerMaintenance.getUserDefine2());
				customerMaintenance.setCustomerHoldStatus(dtoCustomerMaintenance.getCustomerHold());
				customerMaintenance.setCustomerActiveStatus(dtoCustomerMaintenance.getActiveCustomer());
				repositoryCustomerMaintenance.saveAndFlush(customerMaintenance);
				return maintenanceGetById(dtoCustomerMaintenance);

			} else {
				dtoCustomerMaintenance.setMessageType("CUSTOMER_MAINTENANCE_NOT_FOUND");
				return dtoCustomerMaintenance;
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
			return null;
		}
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchMaintenance(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();
		int langId= serviceHome.getLanngugaeId();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositoryCustomerMaintenance
				.predictiveMaintenanceSearchCount(dtoSearchs.getSearchKeyword()));

		DtoCustomerMaintenance dtoCustomerMaintenance = null;
		List<CustomerMaintenance> customerMaintenanceList = null;
		List<DtoCustomerMaintenance> dtoCustomerMaintenanceList = new ArrayList<>();

		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearchs.getPageNumber(), dtoSearchs.getPageSize(), Direction.DESC,
					"createdDate");
			customerMaintenanceList = repositoryCustomerMaintenance
					.predictiveMaintenanceSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			customerMaintenanceList = repositoryCustomerMaintenance
					.predictiveMaintenanceSearch(dtoSearchs.getSearchKeyword());
		}

		if (customerMaintenanceList != null) {
			for (CustomerMaintenance customerMaintenance : customerMaintenanceList) {
				dtoCustomerMaintenance = new DtoCustomerMaintenance(customerMaintenance);
				dtoCustomerMaintenance.setCityId(0);
				dtoCustomerMaintenance.setStateId(0);
				dtoCustomerMaintenance.setCountryId(0);
				CountryMaster countryMaster=repositoryCountryMaster.findByCountryNameAndIsDeletedAndIsActive(dtoCustomerMaintenance.getCountryName(),false,true);
				if(countryMaster!=null && countryMaster.getLanguage().getLanguageId()!=langId){
					countryMaster=repositoryCountryMaster.findByCountryCodeAndIsDeletedAndIsActiveAndLanguageLanguageId(countryMaster.getCountryCode(), false, true,langId);
				}
				
				if(countryMaster!=null)
				{
					dtoCustomerMaintenance.setCountryId(countryMaster.getCountryId());
					StateMaster stateMaster=repositoryStateMaster.findByStateNameAndCountryMasterCountryCodeAndIsDeleted(dtoCustomerMaintenance.getStateName(),countryMaster.getCountryCode(),false);
					if(stateMaster!=null && stateMaster.getLanguage().getLanguageId()!=langId){
						stateMaster= repositoryStateMaster.findByStateCodeAndCountryMasterCountryCodeAndLanguageLanguageIdAndIsDeleted(stateMaster.getStateCode(), countryMaster.getCountryCode(),langId,false);
					}
					
					if(stateMaster!= null)
					{
						dtoCustomerMaintenance.setStateId(stateMaster.getStateId());
						CityMaster cityMaster=repositoryCityMaster.findByCityNameAndStateMasterStateCodeAndIsDeleted(dtoCustomerMaintenance.getCityName(),stateMaster.getStateCode(),false);
						if(cityMaster!=null && cityMaster.getLanguage().getLanguageId()!=langId)
						{
							cityMaster=repositoryCityMaster.findByCityCodeAndStateMasterStateCodeAndIsDeleted(cityMaster.getCityCode(),stateMaster.getStateCode(),false);
						}
						if(cityMaster!=null){
							dtoCustomerMaintenance.setCityId(cityMaster.getCityId());
						}
					}
				}
				
				dtoCustomerMaintenanceList.add(dtoCustomerMaintenance);
			}
		}
		dtoSearch.setRecords(dtoCustomerMaintenanceList);
		return dtoSearch;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getMaintenanceActiveTypeStatus() {
		int langId=serviceHome.getLanngugaeId();
		List<CustomerMaintenanceActiveStatus> list = repositoryCustomerMaintenanceActiveStatus.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if (list != null) {
			for (CustomerMaintenanceActiveStatus customerMaintenanceActiveStatus : list) {
				responseList.add(new DtoStatusType(customerMaintenanceActiveStatus));
			}
		}
		return responseList;
	}

	/**
	 * @return
	 */
	public List<DtoStatusType> getMaintenanceHoldTypeStatus() {
		int langId=serviceHome.getLanngugaeId();
		List<CustomerMaintenanceHoldStatus> list = repositoryCustomerMaintenanceHoldStatus.findByLanguageLanguageIdAndIsDeleted(langId,false);
		List<DtoStatusType> responseList = new ArrayList<>();
		if (list != null) {
			for (CustomerMaintenanceHoldStatus customerMaintenanceHoldStatus : list) {
				responseList.add(new DtoStatusType(customerMaintenanceHoldStatus));
			}
		}
		return responseList;
	}

	/**
	 * @param dtoCustomerMaintenance
	 * @return
	 */
	public boolean deleteMaintenance(DtoCustomerMaintenance dtoCustomerMaintenance) {
		CustomerMaintenance customerMaintenance = repositoryCustomerMaintenance
				.findByCustnumberAndIsDeleted(dtoCustomerMaintenance.getCustomerId(),false);
		if (customerMaintenance != null) {
			List<CustomerMaintenance> list = new ArrayList<>();
			list.add(customerMaintenance);
			repositoryCustomerMaintenance.deleteInBatch(list);
			return true;
		}
		return false;
	}

	/**
	 * @param dtoSalesmanMaintenance
	 * @return
	 */
	public DtoSalesmanMaintenance saveSalesmanMaintenance(DtoSalesmanMaintenance dtoSalesmanMaintenance) {

		log.info("inside saveSalesmanMaintenance method");
		try {
			SalesmanMaintenance salesmanMaintenance = repositorySalesmanMaintenance
					.findBySalesmanIdAndIsDeleted(dtoSalesmanMaintenance.getSalesmanId(),false);
			if (salesmanMaintenance != null) {
				dtoSalesmanMaintenance.setMessageType("SALESMAN_MAINTENANCE_ALREADY_EXIST");
				return dtoSalesmanMaintenance;
			}
			int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
			salesmanMaintenance = new SalesmanMaintenance();
			salesmanMaintenance.setCreatedBy(loggedInUserId);
			salesmanMaintenance.setAddress1(dtoSalesmanMaintenance.getAddress1());
			salesmanMaintenance.setAddress2(dtoSalesmanMaintenance.getAddress2());
			//salesmanMaintenance.setApplyPercentage(dtoSalesmanMaintenance.getApplyPercentage());
			if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getCostOfSalesLastYear())) {
				salesmanMaintenance
						.setCostOfSalesLastYear(Double.valueOf(dtoSalesmanMaintenance.getCostOfSalesLastYear()));
			}
			if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getCostOfSalesYTD())) {
				salesmanMaintenance.setCostOfSalesYTD(Double.valueOf(dtoSalesmanMaintenance.getCostOfSalesYTD()));
			}
			salesmanMaintenance.setEmployeeId(dtoSalesmanMaintenance.getEmployeeId());
			salesmanMaintenance.setInactive(dtoSalesmanMaintenance.getInactive());
			//salesmanMaintenance.setPercentageAmount(dtoSalesmanMaintenance.getPercentageAmount());
			salesmanMaintenance.setPhone1(dtoSalesmanMaintenance.getPhone1());
			salesmanMaintenance.setPhone2(dtoSalesmanMaintenance.getPhone2());
			if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getSalesCommissionsLastYear())) {
				salesmanMaintenance.setSalesCommissionsLastYear(
						Double.valueOf(dtoSalesmanMaintenance.getSalesCommissionsLastYear()));
			}
			if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getSalesCommissionsYTD())) {
				salesmanMaintenance
						.setSalesCommissionsYTD(Double.valueOf(dtoSalesmanMaintenance.getSalesCommissionsYTD()));
			}
			salesmanMaintenance.setSalesmanFirstName(dtoSalesmanMaintenance.getSalesmanFirstName());
			salesmanMaintenance.setSalesmanFirstNameArabic(dtoSalesmanMaintenance.getSalesmanFirstNameArabic());
			salesmanMaintenance.setSalesmanId(dtoSalesmanMaintenance.getSalesmanId());
			salesmanMaintenance.setSalesmanLastName(dtoSalesmanMaintenance.getSalesmanLastName());
			salesmanMaintenance.setSalesmanLastNameArabic(dtoSalesmanMaintenance.getSalesmanLastNameArabic());
			salesmanMaintenance.setSalesmanMidName(dtoSalesmanMaintenance.getSalesmanMidName());
			salesmanMaintenance.setSalesmanMidNameArabic(dtoSalesmanMaintenance.getSalesmanMidNameArabic());
			salesmanMaintenance.setSalesTerritoryId(dtoSalesmanMaintenance.getSalesTerritoryId());
			if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getTotalCommissionsLastYear())) {
				salesmanMaintenance.setTotalCommissionsLastYear(
						Double.valueOf(dtoSalesmanMaintenance.getTotalCommissionsLastYear()));
			}
			if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getSalesCommissionsLastYear())) {
				salesmanMaintenance
						.setTotalCommissionsYTD(Double.valueOf(dtoSalesmanMaintenance.getTotalCommissionsYTD()));
			}

			repositorySalesmanMaintenance.saveAndFlush(salesmanMaintenance);
			return dtoSalesmanMaintenance;
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
			return null;
		}
	}

	/**
	 * @param dtoSalesmanMaintenance
	 * @return
	 */
	public DtoSalesmanMaintenance salesmanMaintenanceGetById(DtoSalesmanMaintenance dtoSalesmanMaintenance) {
		log.info("inside salesmanMaintenanceGetById method");
		try {
			SalesmanMaintenance salesmanMaintenance = repositorySalesmanMaintenance
					.findBySalesmanIdAndIsDeleted(dtoSalesmanMaintenance.getSalesmanId(),false);
			if (salesmanMaintenance != null) {
				return new DtoSalesmanMaintenance(salesmanMaintenance);
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSalesmanMaintenance
	 * @return
	 */
	public DtoSalesmanMaintenance updateSalespersonsMaintenance(DtoSalesmanMaintenance dtoSalesmanMaintenance) {
		log.info("inside saveSalesmanMaintenance method");
		try {
			SalesmanMaintenance salesmanMaintenance = repositorySalesmanMaintenance
					.findBySalesmanIdAndIsDeleted(dtoSalesmanMaintenance.getSalesmanId(),false);
			if (salesmanMaintenance != null) {
				int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
				salesmanMaintenance.setChangeBy(loggedInUserId);
				salesmanMaintenance.setAddress1(dtoSalesmanMaintenance.getAddress1());
				salesmanMaintenance.setAddress2(dtoSalesmanMaintenance.getAddress2());
				//salesmanMaintenance.setApplyPercentage(dtoSalesmanMaintenance.getApplyPercentage());
				if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getCostOfSalesLastYear())) {
					salesmanMaintenance
							.setCostOfSalesLastYear(Double.valueOf(dtoSalesmanMaintenance.getCostOfSalesLastYear()));
				}
				if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getCostOfSalesYTD())) {
					salesmanMaintenance.setCostOfSalesYTD(Double.valueOf(dtoSalesmanMaintenance.getCostOfSalesYTD()));
				}
				salesmanMaintenance.setEmployeeId(dtoSalesmanMaintenance.getEmployeeId());
				salesmanMaintenance.setInactive(dtoSalesmanMaintenance.getInactive());
				//salesmanMaintenance.setPercentageAmount(dtoSalesmanMaintenance.getPercentageAmount());
				salesmanMaintenance.setPhone1(dtoSalesmanMaintenance.getPhone1());
				salesmanMaintenance.setPhone2(dtoSalesmanMaintenance.getPhone2());
				if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getSalesCommissionsLastYear())) {
					salesmanMaintenance.setSalesCommissionsLastYear(
							Double.valueOf(dtoSalesmanMaintenance.getSalesCommissionsLastYear()));
				}
				if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getSalesCommissionsYTD())) {
					salesmanMaintenance
							.setSalesCommissionsYTD(Double.valueOf(dtoSalesmanMaintenance.getSalesCommissionsYTD()));
				}
				salesmanMaintenance.setSalesmanFirstName(dtoSalesmanMaintenance.getSalesmanFirstName());
				salesmanMaintenance.setSalesmanFirstNameArabic(dtoSalesmanMaintenance.getSalesmanFirstNameArabic());
				salesmanMaintenance.setSalesmanLastName(dtoSalesmanMaintenance.getSalesmanLastName());
				salesmanMaintenance.setSalesmanLastNameArabic(dtoSalesmanMaintenance.getSalesmanLastNameArabic());
				salesmanMaintenance.setSalesmanMidName(dtoSalesmanMaintenance.getSalesmanMidName());
				salesmanMaintenance.setSalesmanMidNameArabic(dtoSalesmanMaintenance.getSalesmanMidNameArabic());
				salesmanMaintenance.setSalesTerritoryId(dtoSalesmanMaintenance.getSalesTerritoryId());
				if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getTotalCommissionsLastYear())) {
					salesmanMaintenance.setTotalCommissionsLastYear(
							Double.valueOf(dtoSalesmanMaintenance.getTotalCommissionsLastYear()));
				}
				if (UtilRandomKey.isNotBlank(dtoSalesmanMaintenance.getSalesCommissionsLastYear())) {
					salesmanMaintenance
							.setTotalCommissionsYTD(Double.valueOf(dtoSalesmanMaintenance.getTotalCommissionsYTD()));
				}
				repositorySalesmanMaintenance.saveAndFlush(salesmanMaintenance);
				return dtoSalesmanMaintenance;
			}
		} catch (Exception e) {
			log.info(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchSalesmanMaintenance(DtoSearch dtoSearchs) {

		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositorySalesmanMaintenance.predictiveSearchCount(dtoSearchs.getSearchKeyword()));
		List<SalesmanMaintenance> salesmanMaintenanceList = null;
		List<DtoSalesmanMaintenance> dtoSalesmanMaintenanceList = new ArrayList<>();
		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearchs.getPageNumber(), dtoSearchs.getPageSize(), Direction.DESC,
					"createdDate");
			salesmanMaintenanceList = repositorySalesmanMaintenance
					.predictiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			salesmanMaintenanceList = repositorySalesmanMaintenance.predictiveSearch(dtoSearchs.getSearchKeyword());
		}

		if (salesmanMaintenanceList != null) {
			for (SalesmanMaintenance salesmanMaintenance : salesmanMaintenanceList) {
				dtoSalesmanMaintenanceList.add(new DtoSalesmanMaintenance(salesmanMaintenance));
			}
		}
		dtoSearch.setRecords(dtoSalesmanMaintenanceList);
		return dtoSearch;
	}

	public DtoSearch searchActiveCustomerMaintenance(DtoSearch dtoSearchs) {
		DtoSearch dtoSearch = new DtoSearch();
		int langId= serviceHome.getLanngugaeId();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositoryCustomerMaintenance
				.predictiveActiveMaintenanceSearchCount(dtoSearchs.getSearchKeyword()));

		DtoCustomerMaintenance dtoCustomerMaintenance = null;
		List<CustomerMaintenance> customerMaintenanceList = null;
		List<DtoCustomerMaintenance> dtoCustomerMaintenanceList = new ArrayList<>();

		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearchs.getPageNumber(), dtoSearchs.getPageSize(), Direction.DESC,
					"createdDate");
			customerMaintenanceList = repositoryCustomerMaintenance
					.predictiveActiveMaintenanceSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			customerMaintenanceList = repositoryCustomerMaintenance
					.predictiveActiveMaintenanceSearch(dtoSearchs.getSearchKeyword());
		}

		if (customerMaintenanceList != null) {
			for (CustomerMaintenance customerMaintenance : customerMaintenanceList) {
				dtoCustomerMaintenance = new DtoCustomerMaintenance(customerMaintenance);
				dtoCustomerMaintenance.setCityId(0);
				dtoCustomerMaintenance.setStateId(0);
				dtoCustomerMaintenance.setCountryId(0);
				CountryMaster countryMaster=repositoryCountryMaster.findByCountryNameAndIsDeletedAndIsActive(dtoCustomerMaintenance.getCountryName(),false,true);
				if(countryMaster!=null && countryMaster.getLanguage().getLanguageId()!=langId){
					countryMaster=repositoryCountryMaster.findByCountryCodeAndIsDeletedAndIsActiveAndLanguageLanguageId(countryMaster.getCountryCode(), false, true,langId);
				}
				
				if(countryMaster!=null)
				{
					dtoCustomerMaintenance.setCountryId(countryMaster.getCountryId());
					StateMaster stateMaster=repositoryStateMaster.findByStateNameAndCountryMasterCountryCodeAndIsDeleted(dtoCustomerMaintenance.getStateName(),countryMaster.getCountryCode(),false);
					if(stateMaster!=null && stateMaster.getLanguage().getLanguageId()!=langId){
						stateMaster= repositoryStateMaster.findByStateCodeAndCountryMasterCountryCodeAndLanguageLanguageIdAndIsDeleted(stateMaster.getStateCode(), countryMaster.getCountryCode(),langId,false);
					}
					
					if(stateMaster!= null)
					{
						dtoCustomerMaintenance.setStateId(stateMaster.getStateId());
						CityMaster cityMaster=repositoryCityMaster.findByCityNameAndStateMasterStateCodeAndIsDeleted(dtoCustomerMaintenance.getCityName(),stateMaster.getStateCode(),false);
						if(cityMaster!=null && cityMaster.getLanguage().getLanguageId()!=langId)
						{
							cityMaster=repositoryCityMaster.findByCityCodeAndStateMasterStateCodeAndIsDeleted(cityMaster.getCityCode(),stateMaster.getStateCode(),false);
						}
						if(cityMaster!=null){
							dtoCustomerMaintenance.setCityId(cityMaster.getCityId());
						}
					}
				}
				
				dtoCustomerMaintenanceList.add(dtoCustomerMaintenance);
			}
		}
		dtoSearch.setRecords(dtoCustomerMaintenanceList);
		return dtoSearch;
	}
	
	/**
	 * @param dtoSearchs
	 * @return
	 */
	public DtoSearch searchActiveSalesmanMaintenance(DtoSearch dtoSearchs) 
	{
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoSearchs.getPageNumber());
		dtoSearch.setPageSize(dtoSearchs.getPageSize());
		dtoSearch.setTotalCount(repositorySalesmanMaintenance.predictiveActiveSearchCount(dtoSearchs.getSearchKeyword()));

		List<SalesmanMaintenance> salesmanMaintenanceList = null;
		List<DtoSalesmanMaintenance> dtoSalesmanMaintenanceList = new ArrayList<>();

		if (dtoSearchs.getPageNumber() != null && dtoSearchs.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearchs.getPageNumber(), dtoSearchs.getPageSize(), Direction.DESC,
					"createdDate");
			salesmanMaintenanceList = repositorySalesmanMaintenance
					.predictiveActiveSearchWithPagination(dtoSearchs.getSearchKeyword(), pageable);
		} else {
			salesmanMaintenanceList = repositorySalesmanMaintenance.predictiveActiveSearch(dtoSearchs.getSearchKeyword());
		}

		if (salesmanMaintenanceList != null) {
			for (SalesmanMaintenance salesmanMaintenance : salesmanMaintenanceList) {
				dtoSalesmanMaintenanceList.add(new DtoSalesmanMaintenance(salesmanMaintenance));
			}
		}
		dtoSearch.setRecords(dtoSalesmanMaintenanceList);
		return dtoSearch;
	}
}
