/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.util;
import java.security.Key;
import java.util.Random;

import javax.crypto.spec.SecretKeySpec;

/**
 * Description: Utility class for random verification code generation  Name of Project: BTI APP Created on: May 16,
 * 2017 Modified on: May 16, 2017 10:19:38 AM
 * 
 * @author seasia Version:
 */

public class LoginDetailsUtil {

	private static final String ALGO = "AES";

	private static final byte[] keyValue = new byte[] { 'T', 'h', 'e', 'B', 't', 'i', 'S', 'S', 'e', 'c', 'r', 'e', 't',
			'K', 'e', 'y' };

	/**
	 * @return
	 */
	public String getCheckVerificationCode() {

		char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIZKLMNOPQRSTUVWXYZ1234567890".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 6; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString();
		return output;
	}

	/**
	 * @return
	 */
	public String getCheckResetCode() {
		char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIZKLMNOPQRSTUVWXYZ1234567890".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 8; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString();
		return output;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}

}
