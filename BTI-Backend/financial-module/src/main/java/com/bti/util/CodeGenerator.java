/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bti.constant.BTICodeType;
import com.bti.model.CreditCardsSetup;
import com.bti.repository.RepositoryCreditCardsSetup;

/**
 * Description: Utility class for code generation  
 * Name of Project: BTI APP Created on: May 16,
 * 2017 Modified on: May 16, 2017 10:19:38 AM
 * 
 * @author seasia Version:
 */

@Component
public class CodeGenerator {

	@Autowired
	RepositoryCreditCardsSetup repositoryCreditCardsSetup;

	/**
	 * @param codeType
	 * @return
	 */
	public String getGeneratedCode(String codeType) {
		String generatedCode = null;
		int codeId = 1000;
		//User Group Code
		if (BTICodeType.CREDITCARD.name().equalsIgnoreCase(codeType)) {
			CreditCardsSetup userGroup = repositoryCreditCardsSetup.findTop1ByOrderByCardIdDesc();
			if (userGroup != null) {
				String[] codeList = userGroup.getCardId().split("-");
				codeId = Integer.parseInt(codeList[1]) + 1;
				generatedCode = "CC-" + codeId;
			} else {
				generatedCode = "CC-" + codeId;
			}
		}
		 
		return generatedCode;
	}
}
