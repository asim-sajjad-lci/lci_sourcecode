package com.bti.util.mapping.GL;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLYTDOpenTransactions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.head;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.number.money.CurrencyUnitFormatter;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.bti.constant.CommonConstant;
import com.bti.constant.MessageLabel;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.COAFinancialDimensionsValues;
import com.bti.model.COAMainAccounts;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.JournalEntryDetails;
import com.bti.model.JournalEntryHeader;
import com.bti.model.PayableAccountClassSetup;
import com.bti.model.dto.DtoJournalEntryDetail;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.repository.RepositoryCOAFinancialDimensionsValues;
import com.bti.repository.RepositoryCOAMainAccounts;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.util.UtilDateAndTime;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryPayableAccountClassSetup;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

import ch.qos.logback.classic.pattern.Util;


@Service("mappingPostedJournalEntry")
public class MappingPostedJournalEntry {

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryPayableAccountClassSetup repositoryPayableAccountClassSetup;

	@Autowired
	RepositoryCOAMainAccounts repositoryCOAMainAccounts;

	@Autowired
	RepositoryCOAFinancialDimensionsValues repositoryCOAFinancialDimensionsValues;

	@Autowired
	RepositoryGLAccountsTableAccumulation repositoryGLAccountsTableAccumulation;

	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	public DtoJournalEntryHeader postedToDto(List<GLYTDOpenTransactions> glytdOpenTransactions,
			DtoJournalEntryHeader dtoJournalEntryHeader) {

		GLYTDOpenTransactions header = glytdOpenTransactions.get(0);

		dtoJournalEntryHeader.setJournalID(header.getJournalEntryID());
		dtoJournalEntryHeader.setOriginalJournalID((header.getOriginalTransactionNumber() != null)?header.getOriginalTransactionNumber()
				:"");
		dtoJournalEntryHeader.setJournalDescription(header.getJournalDescription());
		dtoJournalEntryHeader.setJournalDescriptionArabic(header.getJournalDescriptionArabic());
		dtoJournalEntryHeader.setTransactionDate(UtilDateAndTime.dateToStringddmmyyyy(header.getTransactionDate()));
		dtoJournalEntryHeader.setCurrencyID(header.getCurrencyId());
		CurrencyExchangeHeader CEH = repositoryCurrencyExchangeHeader.findByExchangeIndexAndIsDeleted(header.getExchangeTableIndex(), false);
		dtoJournalEntryHeader.setExchangeRate(header.getExchangeTableRate()+"");
		dtoJournalEntryHeader.setTotalJournalEntry(header.getOriginalTotalJournalEntryCredit()+"");
		dtoJournalEntryHeader.setTransactionSource(header.getTransactionSource());
		dtoJournalEntryHeader.setTransactionPostedDate(UtilDateAndTime.dateToStringddmmyyyy(header.getTransactionPostingDate()));
		dtoJournalEntryHeader.setSourceDocument(header.getGlConfigurationAuditTrialCodes().getSourceDocument());
		List<DtoJournalEntryDetail> details = new ArrayList<>();
		for(GLYTDOpenTransactions glytdOpenTransaction : glytdOpenTransactions) {
			GLAccountsTableAccumulation accountHeader = getAccountNumber(glytdOpenTransaction.getAccountTableRowIndex());
			DtoJournalEntryDetail dtoJournalEntryDetail = new DtoJournalEntryDetail();
			dtoJournalEntryDetail.setDistributionDescription(glytdOpenTransaction.getDistributionDescription());
			dtoJournalEntryDetail.setDebitAmount(glytdOpenTransaction.getDebitAmount()+"");
			dtoJournalEntryDetail.setCreditAmount(glytdOpenTransaction.getCreditAmount()+"");
			dtoJournalEntryDetail.setAccountNumber(accountHeader.getAccountNumber());
			dtoJournalEntryDetail.setAccountDescriptionArabic(accountHeader.getAccountDescriptionArabic());
			dtoJournalEntryDetail.setAccountDescription(accountHeader.getAccountDescription());
			details.add(dtoJournalEntryDetail);
		}
		
		dtoJournalEntryHeader.setJournalEntryDetailsList(details);
		
		return dtoJournalEntryHeader;
		
	}
	
	
	
	
//	// get Account Number Header 
//	// pass account number id index (ACTROWID)
//	public GLAccountsTableAccumulation getAccountNumber(String ACTROWIDPARAM) {
//		
//		GLAccountsTableAccumulation accountHeader = repositoryGLAccountsTableAccumulation.findByAccountTableRowIndexAndIsDeleted(ACTROWIDPARAM,false);
//				
//		return accountHeader;		
//
//		dtoJournalEntryHeader.setSourceDocument(header.getGlConfigurationAuditTrialCodes().getSourceDocument());
//		dtoJournalEntryHeader.setSourceDocumentId(header.getGlConfigurationAuditTrialCodes().getSeriesIndex());
//		CurrencyExchangeHeader CEH = repositoryCurrencyExchangeHeader
//				.findByExchangeIndexAndIsDeleted(header.getExchangeTableIndex(), false);
//		dtoJournalEntryHeader.setExchangeRate(header.getExchangeTableRate() + "");
//		dtoJournalEntryHeader.setTotalJournalEntry(header.getOriginalTotalJournalEntryCredit() + "");
//		dtoJournalEntryHeader.setTransactionSource(header.getTransactionSource());
//		dtoJournalEntryHeader
//				.setTransactionPostedDate(UtilDateAndTime.dateToStringddmmyyyy(header.getTransactionPostingDate()));
//		List<DtoJournalEntryDetail> details = new ArrayList<>();
//		for (GLYTDOpenTransactions glytdOpenTransaction : glytdOpenTransactions) {
//			GLAccountsTableAccumulation accountHeader = getAccountNumber(
//					glytdOpenTransaction.getAccountTableRowIndex());
//			DtoJournalEntryDetail dtoJournalEntryDetail = new DtoJournalEntryDetail();
//			dtoJournalEntryDetail.setDistributionDescription(glytdOpenTransaction.getDistributionDescription());
//			dtoJournalEntryDetail.setDebitAmount(glytdOpenTransaction.getDebitAmount() + "");
//			dtoJournalEntryDetail.setCreditAmount(glytdOpenTransaction.getCreditAmount() + "");
//			if (accountHeader != null) {
//				dtoJournalEntryDetail.setAccountNumber(accountHeader.getAccountNumber());
//				dtoJournalEntryDetail.setAccountDescriptionArabic(accountHeader.getAccountDescriptionArabic());
//				dtoJournalEntryDetail.setAccountDescription(accountHeader.getAccountDescription());
//			}
//			details.add(dtoJournalEntryDetail);
//		}
//
//		dtoJournalEntryHeader.setJournalEntryDetailsList(details);
//
//		return dtoJournalEntryHeader;
//
//	}

	public JournalEntryHeader dtoUnpostedToModelUnposted(DtoJournalEntryHeader dtoJournalEntryHeader) {
		JournalEntryHeader journalHeader = new JournalEntryHeader();
		List<JournalEntryDetails> journalsDetails = new ArrayList<>();

		try {
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine mathEval = manager.getEngineByName("js");

//			if(UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getInterCompany())) { // enter Company
//				journalHeader.setInterCompany(
//						dtoJournalEntryHeader.getInterCompany());
//			}

			if (dtoJournalEntryHeader.getGlBatchId() != "" && dtoJournalEntryHeader.getGlBatchId() != null) { // Batch
				journalHeader.setGlBatches(
						repositoryGLBatches.findByBatchIDAndIsDeleted(dtoJournalEntryHeader.getGlBatchId(), false));
			}

			if (dtoJournalEntryHeader.getTransactionType() != 0) { // Transaction Type
				journalHeader.setTransactionType(dtoJournalEntryHeader.getTransactionType());
			}

			if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate())) { // Transaction Date
				journalHeader.setTransactionDate(
						UtilDateAndTime.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate()));
			}

			if (dtoJournalEntryHeader.getSourceDocumentId() != 0) { // Audit Trial code
				journalHeader.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
						.findBySeriesIndexAndIsDeleted(dtoJournalEntryHeader.getSourceDocumentId(), false));
			} else {
				journalHeader.setGlConfigurationAuditTrialCodes(null);
			}

			if (dtoJournalEntryHeader.getJournalDescription() != ""
					&& !dtoJournalEntryHeader.getJournalDescription().equals(null)) { // Description
				journalHeader.setJournalDescription(dtoJournalEntryHeader.getJournalDescription());
			} else {
				journalHeader.setJournalDescription("");
			}

			if (dtoJournalEntryHeader.getJournalDescriptionArabic() != ""
					&& !dtoJournalEntryHeader.getJournalDescriptionArabic().equals(null)) { // arabic Description
				journalHeader.setJournalDescriptionArabic(dtoJournalEntryHeader.getJournalDescriptionArabic());
			} else {
				journalHeader.setJournalDescriptionArabic("");
			}

			journalHeader.setTransactionSource("JV"); // Transaction Source

			// ================***Currency****=======================//
			if (dtoJournalEntryHeader.getCurrencyID() != "" && dtoJournalEntryHeader.getCurrencyID() != null) { // Currency
																												// Setup
				CurrencySetup currencySetup = repositoryCurrencySetup
						.findByCurrencyIdAndIsDeleted(dtoJournalEntryHeader.getCurrencyID(), false);
				journalHeader.setCurrencySetup(currencySetup);
			} else {
				return null ;
			}

			if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) { // total credit
				Double originalCreditAmount = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit());
				journalHeader.setOriginalTotalJournalEntryCredit(originalCreditAmount);
				journalHeader.setTotalJournalEntryCredit(originalCreditAmount);
			}

			if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) { // total Debit
				Double OriginalDebitAmount = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit());
				journalHeader.setOriginalTotalJournalEntryDebit(OriginalDebitAmount);
				journalHeader.setTotalJournalEntryDebit(OriginalDebitAmount);
			}

			Double currencyExchangeRate = Double.valueOf(dtoJournalEntryHeader.getExchangeRate()); // exchange rate

			journalHeader.setExchangeTableRate(currencyExchangeRate);
			String calMethod = RateCalculationMethod.MULTIPLY.getMethod();

			CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
					.findByExchangeIndexAndIsDeleted(dtoJournalEntryHeader.getExchangeTableIndex(), false);

			// duplicated code
			//GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
			//		.findBySeriesIndexAndIsDeleted(dtoJournalEntryHeader.getSourceDocumentId(), false);
			//journalHeader.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);

			if (currencyExchangeHeader != null) { // currency convertion
				journalHeader.setCurrencyExchangeHeader(currencyExchangeHeader);

				if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getExchangeRate())) {

					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getCurrencyID())) {

						if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
							calMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
									.getMethod();
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) {
							Double totalJournalEntryDebit = (Double) mathEval
									.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit()) + calMethod
											+ currencyExchangeRate);
							journalHeader.setTotalJournalEntryDebit(totalJournalEntryDebit);
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) {
							Double totalJournalEntryCredit = (Double) mathEval
									.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit()) + calMethod
											+ currencyExchangeRate);
							journalHeader.setTotalJournalEntryCredit(totalJournalEntryCredit);
						}

					}
				}

			}
			if (!dtoJournalEntryHeader.getJournalEntryDetailsList().isEmpty()
					&& dtoJournalEntryHeader.getJournalEntryDetailsList().size() > 0) {
				List<JournalEntryDetails> details = new ArrayList<>();
				for (DtoJournalEntryDetail dtoJournalEntryDetail : dtoJournalEntryHeader.getJournalEntryDetailsList()) {
					JournalEntryDetails journalEntryDetails = new JournalEntryDetails(); // object to be save

//					if(!dtoJournalEntryDetail.getJournalEntryHeaderId().equals("") &&
//							!dtoJournalEntryDetail.getJournalEntryHeaderId().equals(null)) { // set journal header 
//						journalEntryDetails.setJournalEntryHeader(journalHeader);
//						
//					}

					if (dtoJournalEntryDetail.getDistributionDescription() != null
							&& !dtoJournalEntryDetail.getDistributionDescription().equals(null)) { // set journal
																									// distribution
																									// description
						journalEntryDetails
								.setDistributionDescription(dtoJournalEntryDetail.getDistributionDescription());
					}

					if (!dtoJournalEntryDetail.getAccountTableRowIndex().equals("")
							&& !dtoJournalEntryDetail.getAccountTableRowIndex().equals(null)) { // set account row id
						journalEntryDetails.setAccountTableRowIndex(dtoJournalEntryDetail.getAccountTableRowIndex());
					}

					if (dtoJournalEntryDetail.getIntercompanyId() != null
							&& !dtoJournalEntryDetail.getIntercompanyId().equals("")) { // compnay id
						journalEntryDetails
								.setIntercompanyIDMultiCompanyTransaction(dtoJournalEntryDetail.getIntercompanyId());
					}

					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) { // credit
						Double originalCreditAmount = Double.valueOf(dtoJournalEntryDetail.getCreditAmount());
						journalEntryDetails.setOriginalCreditAmount(originalCreditAmount);
						journalEntryDetails.setCreditAmount(originalCreditAmount);
					}

					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) { // debit
						Double originalDebitAmount = Double.valueOf(dtoJournalEntryDetail.getDebitAmount());
						journalEntryDetails.setOriginalDebitAmount(originalDebitAmount);
						journalEntryDetails.setDebitAmount(originalDebitAmount);
					}

					journalEntryDetails
							.setIntercompanyIDMultiCompanyTransaction(dtoJournalEntryDetail.getIntercompanyId());

					if (!dtoJournalEntryHeader.getExchangeRate().equals("")
							&& !dtoJournalEntryHeader.getExchangeRate().equals(null)) { // exchange rate
						journalEntryDetails
								.setExchangeTableRate(Double.valueOf(dtoJournalEntryHeader.getExchangeRate()));
					}

					// dublicate code
//					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) {
//						Double creditAmount = (Double) mathEval
//								.eval(Double.valueOf(dtoJournalEntryDetail.getCreditAmount()) + calMethod
//										+ journalHeader.getExchangeTableRate());
//						journalEntryDetails.setOriginalCreditAmount(creditAmount);
//						journalEntryDetails.setCreditAmount(creditAmount);
//					}
//					// dublicate code
//					if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) {
//						Double debitAmount = (Double) mathEval
//								.eval(Double.valueOf(dtoJournalEntryDetail.getDebitAmount()) + calMethod
//										+ journalHeader.getExchangeTableRate());
//						journalEntryDetails.setDebitAmount(debitAmount);
//						// debug to fix
//						journalEntryDetails.setOriginalCreditAmount(debitAmount);
//					}

					if (UtilRandomKey.isNotBlank(calMethod) && journalHeader.getExchangeTableRate() != null) {
						if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) {
							Double creditAmount = (Double) mathEval
									.eval(Double.valueOf(dtoJournalEntryDetail.getCreditAmount()) + calMethod
											+ journalHeader.getExchangeTableRate());
							journalEntryDetails.setCreditAmount(creditAmount);
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) {
							Double debitAmount = (Double) mathEval
									.eval(Double.valueOf(dtoJournalEntryDetail.getDebitAmount()) + calMethod
											+ journalHeader.getExchangeTableRate());
							journalEntryDetails.setDebitAmount(debitAmount);

						}

					}

					details.add(journalEntryDetails);
				}

				journalHeader.setJournalDetails(details);
			}

			return journalHeader;

		} catch (Exception e) {
			return null;

		}

	}

	public List<GLYTDOpenTransactions> dtoUnpotedToModelPosted(DtoJournalEntryHeader dtoJournalEntryHeader)
			throws NumberFormatException, ScriptException {
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader(CommonConstant.USER_ID))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(CommonConstant.USER_ID));
		}
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine mathEval = manager.getEngineByName("js");
		List<GLYTDOpenTransactions> postedJournals = new ArrayList<>();

		

		if (!dtoJournalEntryHeader.getJournalEntryDetailsList().isEmpty()) {

			for (DtoJournalEntryDetail dtoJournalEntryDetail : dtoJournalEntryHeader.getJournalEntryDetailsList()) {
				GLYTDOpenTransactions headerposted = new GLYTDOpenTransactions();

				if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) {
					// this is original credit amount with user selected currecy
					Double originalCreditAmount = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit());
					headerposted.setOriginalTotalJournalEntryCredit(originalCreditAmount);
					// Set default amount if do not required conversion, meaning if
					// you have already selected currency Id USD
					headerposted.setTotalJournalEntryCredit(originalCreditAmount);
				}

				if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) {
					// this is original debit amount with user selected currecy
					Double originalDebitAmount = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit());
					headerposted.setOriginalTotalJournalEntryDebit(originalDebitAmount);
					// Set default amount if do not required conversion, meaning if
					// you have already selected currency Id USD
					headerposted.setTotalJournalEntryDebit((originalDebitAmount));
				}

				Double currentExchangeRate = Double.valueOf(dtoJournalEntryHeader.getExchangeRate());
				headerposted.setExchangeTableRate(currentExchangeRate);
				String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
				CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
						.findByExchangeIndexAndIsDeleted(dtoJournalEntryHeader.getExchangeTableIndex(), false);
				if (currencyExchangeHeader != null) {
					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getExchangeRate())) {
						if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getCurrencyID())) {
							if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
								calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
										.getMethod();
							}

							if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) {
								double totalDebitAmount = (double) mathEval
										.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit()) + calcMethod
												+ currentExchangeRate);
								headerposted.setTotalJournalEntryDebit(totalDebitAmount);
							}
							if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) {
								double totalCreditAmount = (double) mathEval
										.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit()) + calcMethod
												+ currentExchangeRate);
								headerposted.setTotalJournalEntryCredit(totalCreditAmount);
							}
						}
					}
				}
				headerposted.setTransactionSource(dtoJournalEntryHeader.getTransactionSource());
				headerposted.setGlConfigurationAuditTrialCodes(repositoryGLConfigurationAuditTrialCodes
						.findBySeriesIndexAndIsDeleted(dtoJournalEntryHeader.getSourceDocumentId(), false));

				if (dtoJournalEntryHeader.getOriginalJournalID() != null
						&& !dtoJournalEntryHeader.getOriginalJournalID().equals("")) {
					headerposted.setOriginalTransactionNumber(dtoJournalEntryHeader.getOriginalJournalID());
				}

				headerposted.setOpenYear(Calendar.getInstance().get(Calendar.YEAR));

//				if(!dtoJournalEntryHeader.getJournalID().equals("") &&
//						dtoJournalEntryHeader.getJournalID().equals(null)){
//					headerposted.setJournalEntryID(dtoJournalEntryHeader.getJournalID());
//				}

				GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
						.findBySeriesIndexAndIsDeleted(dtoJournalEntryHeader.getSourceDocumentId(), false);
				headerposted.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);// Audit Trial
				headerposted.setModuleSeriesNumber(glConfigurationAuditTrialCodes);

				headerposted.setJournalDescription(dtoJournalEntryHeader.getJournalDescription()); // Journal
																									// Description
				headerposted.setJournalDescriptionArabic(dtoJournalEntryHeader.getJournalDescriptionArabic()); // Journal
																												// DEscription
																												// arabic

				if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTransactionDate())) { // transaction date
					headerposted.setTransactionDate(
							UtilDateAndTime.ddmmyyyyStringToDate(dtoJournalEntryHeader.getTransactionDate()));
				}

				headerposted.setTransactionPostingDate(new Date()); // posting date

				headerposted.setTransactionSource(dtoJournalEntryHeader.getTransactionSource()); // transaction source

				headerposted.setAccountTableRowIndex(dtoJournalEntryDetail.getAccountTableRowIndex()); // Account ID

				headerposted.setUserCreateTransaction(loggedInUserId + ""); // create user
				headerposted.setUserPostingTransaction(loggedInUserId + ""); // posting user

				headerposted.setDistributionDescription(dtoJournalEntryDetail.getDistributionDescription()); // distribution
																												// description
				headerposted.setCurrencyId(dtoJournalEntryHeader.getCurrencyID()); // currency ID

				Double debitAmount = 0.0;
				Double creditAmount = 0.0;

				if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) { // original credit
					Double originalCreditAmount = Double.valueOf(dtoJournalEntryDetail.getCreditAmount());
					headerposted.setOriginalCreditAmount(originalCreditAmount);
					headerposted.setCreditAmount(originalCreditAmount);
					creditAmount = originalCreditAmount;
				}

				if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) { // original debit
					Double originalDebitAmount = Double.valueOf(dtoJournalEntryDetail.getCreditAmount());
					headerposted.setOriginalDebitAmount(originalDebitAmount);
					headerposted.setTotalJournalEntryCredit(originalDebitAmount);
					debitAmount = originalDebitAmount;
				}

				if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) { // original total
																									// debit
					Double originalTotalCredit = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit());
					headerposted.setOriginalCreditAmount(originalTotalCredit);
					headerposted.setTotalJournalEntryCredit(originalTotalCredit);
				}

				if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) { // original total
																									// credit
					Double originalTotalDebit = Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit());
					headerposted.setOriginalDebitAmount(originalTotalDebit);
					headerposted.setTotalJournalEntryDebit(originalTotalDebit);
				}

				//CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
				//		.findByExchangeIndexAndIsDeleted(dtoJournalEntryHeader.getExchangeTableIndex(), false);

				if (currencyExchangeHeader != null) {
					headerposted.setExchangeTableIndex(dtoJournalEntryHeader.getExchangeTableIndex());
					if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getExchangeRate())) {

						Double exchangeRate = Double.valueOf(dtoJournalEntryHeader.getExchangeRate());
						headerposted.setExchangeTableRate(exchangeRate);
						//String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();

						if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
							calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
									.getMethod();
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getDebitAmount())) {
							debitAmount = (Double) mathEval.eval(Double.valueOf(dtoJournalEntryDetail.getDebitAmount())
									+ calcMethod + dtoJournalEntryHeader.getExchangeRate());
							headerposted.setDebitAmount(debitAmount);
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryDetail.getCreditAmount())) {
							creditAmount = (Double) mathEval
									.eval(Double.valueOf(dtoJournalEntryDetail.getCreditAmount()) + calcMethod
											+ dtoJournalEntryHeader.getExchangeRate());
							headerposted.setCreditAmount(creditAmount);
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryCredit())) {
							Double totalCreditAmount = (Double) mathEval
									.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryCredit())
											+ calcMethod + dtoJournalEntryHeader.getExchangeRate());
							headerposted.setTotalJournalEntryCredit(totalCreditAmount);
						}

						if (UtilRandomKey.isNotBlank(dtoJournalEntryHeader.getTotalJournalEntryDebit())) {
							Double totalDebitAmount = (Double) mathEval
									.eval(Double.valueOf(dtoJournalEntryHeader.getTotalJournalEntryDebit()) + calcMethod
											+ dtoJournalEntryHeader.getExchangeRate());
						}

					}

				}

				headerposted.setOriginalCompanyID(dtoJournalEntryDetail.getIntercompanyId());
				headerposted.setRowDateIndex(new Date());
//				if(dtoJournalEntryDetail.getIsCredit()) {
//					headerposted.setBalanceType(2);
//				}else {
//					headerposted.setBalanceType(1);
//				}

				postedJournals.add(headerposted);
			}

		}

		return postedJournals;
	}

	// get Account Number Header
	// pass account number id index (ACTROWID)
	public GLAccountsTableAccumulation getAccountNumber(String ACTROWIDPARAM) {

		GLAccountsTableAccumulation accountHeader = repositoryGLAccountsTableAccumulation
				.findByAccountTableRowIndexAndIsDeleted(ACTROWIDPARAM, false);

		return accountHeader;
	}
}
