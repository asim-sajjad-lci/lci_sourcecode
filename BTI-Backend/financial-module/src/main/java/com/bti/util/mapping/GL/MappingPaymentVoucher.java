package com.bti.util.mapping.GL;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.constant.AuditTrailSeriesTypeConstant;
import com.bti.constant.BatchTransactionTypeConstant;
import com.bti.constant.CommonConstant;
import com.bti.constant.RateCalculationMethod;
import com.bti.model.CheckbookMaintenance;
import com.bti.model.CreditCardsSetup;
import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.CurrencySetup;
import com.bti.model.GLAccountsTableAccumulation;
import com.bti.model.GLBatches;
import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationSetup;
import com.bti.model.GLPaymentVoucherDetail;
import com.bti.model.GLPaymentVoucherHeader;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.GLYTDPaymentVoucherDetail;
import com.bti.model.GLYTDPaymentVoucherHeader;
import com.bti.model.PaymentMethodType;
import com.bti.model.dto.DtoGLPaymentVoucherDetail;
import com.bti.model.dto.DtoGLPaymentVoucherHeader;
import com.bti.model.dto.DtoJournalEntryDetail;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.repository.RepositoryCheckBookMaintenance;
import com.bti.repository.RepositoryCreditCardsSetup;
import com.bti.repository.RepositoryCurrencyExchangeHeader;
import com.bti.repository.RepositoryCurrencySetup;
import com.bti.repository.RepositoryGLAccountsTableAccumulation;
import com.bti.repository.RepositoryGLBatches;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLConfigurationSetup;
import com.bti.repository.RepositoryPaymentMethodType;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

@Service("mappingPaymentVoucher")
public class MappingPaymentVoucher {

	@Autowired
	HttpServletRequest request;

	@Autowired
	RepositoryPaymentMethodType repositoryPaymentMethodType;

	@Autowired
	RepositoryGLBatches repositoryGLBatches;

	@Autowired
	RepositoryCurrencySetup repositoryCurrencySetup;

	@Autowired
	RepositoryCheckBookMaintenance repositoryCheckBookMaintenance;

	@Autowired
	RepositoryCurrencyExchangeHeader repositoryCurrencyExchangeHeader;

	@Autowired
	RepositoryGLConfigurationSetup repositoryGLConfigurationSetup;

	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;
	
	@Autowired
	RepositoryCreditCardsSetup repositoryCreditCardsSetup;
	
	
	@Autowired (required=false)
	HttpServletRequest httpRequest;
	
	@Autowired(required=false)
	HttpServletRequest httpServlet;

	public GLYTDPaymentVoucherHeader unpostedToPosted(GLPaymentVoucherHeader glPaymentVoucherHeader) {
		GLYTDPaymentVoucherHeader glytdPaymentVoucherHeader = new GLYTDPaymentVoucherHeader();
		List<GLYTDPaymentVoucherDetail> details = new ArrayList<>();
		if( glPaymentVoucherHeader.getPaymentVoucherId() != 0) {
			glytdPaymentVoucherHeader.setPaymentVoucherId(glPaymentVoucherHeader.getPaymentVoucherId());
		}
		//remove batch
		if(glPaymentVoucherHeader.getGlBatches() != null) {
			glytdPaymentVoucherHeader.setGlBatches(glPaymentVoucherHeader.getGlBatches());
		}
		if( glytdPaymentVoucherHeader.getTransactionNumber() != null) {
			glytdPaymentVoucherHeader.setTransactionNumber(glytdPaymentVoucherHeader.getTransactionNumber());
		}
		if( glPaymentVoucherHeader.getPaymentMethodType() != null) {
			glytdPaymentVoucherHeader.setPaymentMethodType(glPaymentVoucherHeader.getPaymentMethodType());
		}
		if( glPaymentVoucherHeader.getGlConfigurationAuditTrialCodes() != null) {
			glytdPaymentVoucherHeader
			.setGlConfigurationAuditTrialCodes(glPaymentVoucherHeader.getGlConfigurationAuditTrialCodes());
		}
		if(glPaymentVoucherHeader.getAccountTableRowIndex() != null) {
			glytdPaymentVoucherHeader.setAccountTableRowIndex(glPaymentVoucherHeader.getAccountTableRowIndex());
		}
		if( glPaymentVoucherHeader.getAccountTableRowIndex() != null) {
			glytdPaymentVoucherHeader.setAccountTableRowIndex(glPaymentVoucherHeader.getAccountTableRowIndex());
		}
		if( glPaymentVoucherHeader.getBalanceType() != 0) {
			glytdPaymentVoucherHeader.setBalanceType(glPaymentVoucherHeader.getBalanceType());
		}
		if(glPaymentVoucherHeader.getAccountType() != 0) {
			glytdPaymentVoucherHeader.setAccountType(glPaymentVoucherHeader.getAccountType());
		}
		if( glPaymentVoucherHeader.getIntercompanyIDMultiCompanyTransaction() != null) {
			glytdPaymentVoucherHeader.setIntercompanyIDMultiCompanyTransaction(
					glPaymentVoucherHeader.getIntercompanyIDMultiCompanyTransaction());
		}

		if( glPaymentVoucherHeader.getExchangeRate() != null) {
			glytdPaymentVoucherHeader.setExchangeRate(glPaymentVoucherHeader.getExchangeRate());
		}
		if(glPaymentVoucherHeader.getCurrencySetup() != null) {
			glytdPaymentVoucherHeader.setCurrencySetup(glPaymentVoucherHeader.getCurrencySetup());
		}
		if( glPaymentVoucherHeader.getCurrencyExchangeHeader() != null) {
			glytdPaymentVoucherHeader.setCurrencyExchangeHeader(glPaymentVoucherHeader.getCurrencyExchangeHeader());
		}
		if( glPaymentVoucherHeader.getTotalVoucherEntry() != null) {
			glytdPaymentVoucherHeader.setTotalVoucherEntry(glPaymentVoucherHeader.getTotalVoucherEntry());
		}
		if(glPaymentVoucherHeader.getOriginalTotalVoucherEntry() != null) {
			glytdPaymentVoucherHeader.setOriginalTotalVoucherEntry(glPaymentVoucherHeader.getOriginalTotalVoucherEntry());
		}
		if(glPaymentVoucherHeader.getTotalVoucherDebit() != null) {
			glytdPaymentVoucherHeader.setTotalVoucherDebit(glPaymentVoucherHeader.getTotalVoucherDebit());
		}
		if(glPaymentVoucherHeader.getOriginalTotalVoucherDebit() != null) {
			glytdPaymentVoucherHeader.setOriginalTotalVoucherDebit(glPaymentVoucherHeader.getOriginalTotalVoucherDebit());
		}
		if( glPaymentVoucherHeader.getTotalVoucherCredit() != null) {
			glytdPaymentVoucherHeader.setTotalVoucherCredit(glPaymentVoucherHeader.getTotalVoucherCredit());
		}
		if(glPaymentVoucherHeader.getOriginalTotlalVoucherCredit() != null) {
			glytdPaymentVoucherHeader
			.setOriginalTotlalVoucherCredit(glPaymentVoucherHeader.getOriginalTotlalVoucherCredit());
		}
		if( glPaymentVoucherHeader.getVoucherDescription() != null) {
			glytdPaymentVoucherHeader.setVoucherDescription(glPaymentVoucherHeader.getVoucherDescription());
		}
		if(glPaymentVoucherHeader.getVoucherDescriptionArabic() != null) {
			glytdPaymentVoucherHeader.setVoucherDescriptionArabic(glPaymentVoucherHeader.getVoucherDescriptionArabic());
		}
		if( glPaymentVoucherHeader.getCreatedDate() != null) {
			glytdPaymentVoucherHeader.setTransactionDate(glPaymentVoucherHeader.getCreatedDate());
		}


		
		String userId = request.getHeader(CommonConstant.USER_ID);

		if(glPaymentVoucherHeader.getPaymentVoucherDetail() != null && !glPaymentVoucherHeader.getPaymentVoucherDetail().isEmpty()) {
			for (GLPaymentVoucherDetail detail : glPaymentVoucherHeader.getPaymentVoucherDetail()) {

				GLYTDPaymentVoucherDetail glytdPaymentVoucherDetail = new GLYTDPaymentVoucherDetail();

				if( glytdPaymentVoucherHeader != null) {
					glytdPaymentVoucherDetail.setGlYTDPaymentVoucherHeader(glytdPaymentVoucherHeader);
				}
				if( detail.getAccountTableRowIndex() != null) {
					glytdPaymentVoucherDetail.setAccountTableRowIndex(detail.getAccountTableRowIndex());
				}
				if( detail.getAccountType() != 0) {
					glytdPaymentVoucherDetail.setAccountType(detail.getAccountType());
				}
				if(detail.getBalanceType() != 0) {
					glytdPaymentVoucherDetail.setBalanceType(detail.getBalanceType());
				}
				if( detail.getInterCompanyIDMulticompanyTransaction() != null) {
					glytdPaymentVoucherDetail
					.setInterCompanyIDMulticompanyTransaction(detail.getInterCompanyIDMulticompanyTransaction());
				}
				if( detail.getExchangeTableRate() != null) {
					glytdPaymentVoucherDetail.setExchangeTableRate(detail.getExchangeTableRate());
				}
				if( detail.getDebitAmount() != null) {
					glytdPaymentVoucherDetail.setDebitAmount(detail.getDebitAmount());
				}
				if( detail.getDebitAmount()!= null) {
					glytdPaymentVoucherDetail.setDebitAmount(detail.getDebitAmount());
				}
				if(detail.getOriginalDebitAmount() != null) {
					glytdPaymentVoucherDetail.setOriginalDebitAmount(detail.getOriginalDebitAmount());
				}
				if(detail.getDistributionDescription() != null) {
					glytdPaymentVoucherDetail.setDistributionDescription(detail.getDistributionDescription());
				}
			
				
				details.add(glytdPaymentVoucherDetail);
			}

		}
		
		glytdPaymentVoucherHeader.setGlytdPaymentVoucherDetails(details);

		return glytdPaymentVoucherHeader;
	}

	public GLPaymentVoucherHeader DTOToUnposted(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		GLPaymentVoucherHeader glPaymentVoucherHeader = new GLPaymentVoucherHeader();

		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine mathEval = manager.getEngineByName("js");
		try {
			// setting independent data (primary)
			glPaymentVoucherHeader.setPaymentVoucherId(dtoGLPaymentVoucherHeader.getPaymentVoucherID());
			// dublicate
			glPaymentVoucherHeader.setAccountTableRowIndex(dtoGLPaymentVoucherHeader.getAccountTableRowIndex());
			glPaymentVoucherHeader.setVoucherDescription(dtoGLPaymentVoucherHeader.getVoucherDescription());
			glPaymentVoucherHeader.setVoucherDescriptionArabic(dtoGLPaymentVoucherHeader.getVoucherDescriptionArabic());
			glPaymentVoucherHeader.setExchangeRate(Double.valueOf(dtoGLPaymentVoucherHeader.getExchangeRate()));
			//glPaymentVoucherHeader.setAccountTableRowIndex(dtoGLPaymentVoucherHeader.getAccountTableRowIndex());
			glPaymentVoucherHeader.setExchangeTableRate(Double.valueOf(dtoGLPaymentVoucherHeader.getExchangeRate()));
			glPaymentVoucherHeader.setTransactionDate(
					UtilDateAndTime.yyyymmddStringToDate(dtoGLPaymentVoucherHeader.getTransactionDate()));
			glPaymentVoucherHeader.setCheckNumber(dtoGLPaymentVoucherHeader.getCheckNumber());

			// setting dependant data
			// Currency dependant
			CurrencySetup currencySetup = repositoryCurrencySetup
					.findByCurrencyIdAndIsDeleted(dtoGLPaymentVoucherHeader.getCurrencyID(), false);
			glPaymentVoucherHeader.setCurrencySetup(currencySetup);
			// Exchange Currency Header
			CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
					.findByExchangeIndexAndIsDeleted(dtoGLPaymentVoucherHeader.getExchangeTableIndex(), false);
			glPaymentVoucherHeader.setCurrencyExchangeHeader(currencyExchangeHeader);
			// Payment Method Type
			PaymentMethodType paymentMethodType = repositoryPaymentMethodType
					.findByTypeIdAndLanguageLanguageIdAndIsDeleted
					(Integer.valueOf(dtoGLPaymentVoucherHeader.getPaymentTypeID()),Integer.parseInt(this.httpRequest.getHeader("langid")),false);
			glPaymentVoucherHeader.setPaymentMethodType(paymentMethodType);
			// Batch
			GLBatches glbatch = repositoryGLBatches.findByBatchIDAndIsDeleted(
					dtoGLPaymentVoucherHeader.getGlBatcheId(),
					false);
			glPaymentVoucherHeader.setGlBatches(glbatch);
			// checkbook if exsist
			if (paymentMethodType.getIdindex() == 1) { // cash
				
			}else if (paymentMethodType.getTypeId() == 2 || paymentMethodType.getIdindex() == 4) {//check Or bank transafer
				CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance
						.findByCheckBookIdAndIsDeleted(dtoGLPaymentVoucherHeader.getCheckbookID(), false);
				glPaymentVoucherHeader.setBankName(checkbookMaintenance.getBankSetup().getBankId());
				glPaymentVoucherHeader.setCheckbookMaintenance(checkbookMaintenance);
				if(UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getCheckNumber()+"")){
					glPaymentVoucherHeader.setCheckNumber(dtoGLPaymentVoucherHeader.getCheckNumber());
				}
			}else if(paymentMethodType.getTypeId() == 3) { // credit card
                CreditCardsSetup creditCardsSetup = repositoryCreditCardsSetup.findByCardIndxAndIsDeleted(Integer.parseInt(dtoGLPaymentVoucherHeader.getCreditCardId()), false);
				if(UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getCreditCardId())){
					glPaymentVoucherHeader.setCreditCardID(creditCardsSetup.getCardId());
				}
				
				if(dtoGLPaymentVoucherHeader.getCreditCardExpireMonth()!=null){
					glPaymentVoucherHeader.setCreditCardExpireMonth(dtoGLPaymentVoucherHeader.getCreditCardExpireMonth());
				}
				
				if(dtoGLPaymentVoucherHeader.getCreditCardExpireYear()!=null){
					glPaymentVoucherHeader.setCreditCardExpireYear(dtoGLPaymentVoucherHeader.getCreditCardExpireYear());
				}
				
				if(UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getCreditCardNumber())){
					glPaymentVoucherHeader.setCreditCardNumber(dtoGLPaymentVoucherHeader.getCreditCardNumber());
				}
				glPaymentVoucherHeader.setAccountTableRowIndex(creditCardsSetup.getGlAccountsTableAccumulation().getAccountTableRowIndex());

			}else if(paymentMethodType.getTypeId() == 4) { // bank transfer
				CheckbookMaintenance checkbookMaintenance = repositoryCheckBookMaintenance
						.findByCheckBookIdAndIsDeleted(dtoGLPaymentVoucherHeader.getCheckbookID(), false);
				glPaymentVoucherHeader.setBankName(checkbookMaintenance.getBankSetup().getBankId());
				glPaymentVoucherHeader.setCheckbookMaintenance(checkbookMaintenance);
			}
			
			if(dtoGLPaymentVoucherHeader.getAuditTrial() != null) {
				GLConfigurationAuditTrialCodes audit = repositoryGLConfigurationAuditTrialCodes.
						findBySeriesIndexAndIsDeleted(dtoGLPaymentVoucherHeader.getAuditTrial().intValue(), false);
				glPaymentVoucherHeader.setGlConfigurationAuditTrialCodes(audit);
			}

			// Transaction Values (Debit & credit)
			// String calMethod = RateCalculationMethod.MULTIPLY.getMethod();
			// original total payment Voucher Values
			if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucher() + "")) {
				Double originalCreaditAmount = Double
						.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit() + "");
				glPaymentVoucherHeader.setTotalVoucherEntry(originalCreaditAmount);
				glPaymentVoucherHeader.setOriginalTotalVoucherEntry(originalCreaditAmount);

				glPaymentVoucherHeader.setTotalVoucherCredit(originalCreaditAmount);
				glPaymentVoucherHeader.setOriginalTotlalVoucherCredit(originalCreaditAmount);
			}

			if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit() + "")) {
				Double originaldebit = Double.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit());
				glPaymentVoucherHeader.setTotalVoucherDebit(originaldebit);
				glPaymentVoucherHeader.setOriginalTotalVoucherDebit(originaldebit);
			}

			// total for payment voucher
			String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
			if (currencyExchangeHeader != null) {
				if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getExchangeRate())) {
					if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getCurrencyID())) {
						if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
							calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
									.getMethod();
						}

						if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucher() + "")) {
							double totalCeaditAmount = (double) mathEval
									.eval(Double.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit())
											+ calcMethod + dtoGLPaymentVoucherHeader.getExchangeRate());
							glPaymentVoucherHeader.setTotalVoucherEntry(totalCeaditAmount);
							glPaymentVoucherHeader.setTotalVoucherCredit(totalCeaditAmount);
						}

						if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit() + "")) {
							double totalDebit = (double) mathEval
									.eval(Double.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit())
											+ calcMethod + dtoGLPaymentVoucherHeader.getExchangeRate());

							glPaymentVoucherHeader.setTotalVoucherDebit(totalDebit);

						}
					}
				}
			}

			// detail conversion Call(this.DTOToUnposted)
			List<GLPaymentVoucherDetail> details = new ArrayList<>();
			for (DtoGLPaymentVoucherDetail dtoGLPaymentVoucherDetail : dtoGLPaymentVoucherHeader
					.getVoucherDetailList()) {
				GLPaymentVoucherDetail glPaymentVoucherDetail = DTOToUnposted(glPaymentVoucherHeader,
						dtoGLPaymentVoucherDetail,mathEval ,calcMethod);
				details.add(glPaymentVoucherDetail);
			}

			glPaymentVoucherHeader.setPaymentVoucherDetail(details);
		} catch (Exception e) {
			return null;
		}

		return glPaymentVoucherHeader;
	}

	private GLPaymentVoucherDetail DTOToUnposted(GLPaymentVoucherHeader glPaymentVoucherHeader,
			DtoGLPaymentVoucherDetail dtoGLPaymentVoucherDetail, ScriptEngine mathEval , String calcMethod) {

		GLPaymentVoucherDetail glPaymentVoucherDetail = new GLPaymentVoucherDetail();
		// total for payment voucher
		
		try {
			glPaymentVoucherDetail.setAccountTableRowIndex(dtoGLPaymentVoucherDetail.getAccountTableRowIndex());
			glPaymentVoucherDetail.setBalanceType(1);
			glPaymentVoucherDetail.setOriginalDebitAmount(dtoGLPaymentVoucherDetail.getOriginalDebitAmount());
			glPaymentVoucherDetail.setDistributionDescription(dtoGLPaymentVoucherDetail.getDistributionDescription());
			glPaymentVoucherDetail.setGlPaymentVoucherHeader(glPaymentVoucherHeader);
			glPaymentVoucherDetail.setInterCompanyIDMulticompanyTransaction(
					dtoGLPaymentVoucherDetail.getInterCompanyIDMultiCompanyTransaction());
			glPaymentVoucherDetail.setExchangeTableRate(glPaymentVoucherHeader.getExchangeRate());
			if (glPaymentVoucherHeader.getExchangeRate() != null) {
				
				if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherDetail.getDebitAmount() + "")) {
					double debitAmount = (double) mathEval
							.eval(Double.valueOf(dtoGLPaymentVoucherDetail.getDebitAmount() + "") + calcMethod
									+ glPaymentVoucherHeader.getExchangeRate());
					glPaymentVoucherDetail.setDebitAmount(debitAmount);

				}
			}
		} catch (Exception e) {
			return null;
		}

		return glPaymentVoucherDetail;
	}

	// TODO: implement the method
	public DtoGLPaymentVoucherHeader unPosteToDTO(GLPaymentVoucherHeader glPaymentVoucherHeader) {
		DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader = new DtoGLPaymentVoucherHeader();

		return dtoGLPaymentVoucherHeader;
	}
	
	public List<GLYTDOpenTransactions> dtoUnpostedToPostedJV(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) throws ScriptException {
		
		List<GLYTDOpenTransactions> data = new ArrayList<>();
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine mathEval = manager.getEngineByName("js");
		
		GLYTDOpenTransactions glydtOpenTransactionHeader = new GLYTDOpenTransactions();
		GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes = repositoryGLConfigurationAuditTrialCodes
				.findTopOneByGlConfigurationAuditTrialSeriesTypeTypeIdAndIsDeletedOrderBySequenceNumberDesc(
						AuditTrailSeriesTypeConstant.GL.getIndex(), false);
		glydtOpenTransactionHeader.setOpenYear(Calendar.getInstance().get(Calendar.YEAR));
		glydtOpenTransactionHeader.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
		glydtOpenTransactionHeader.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
		glydtOpenTransactionHeader.setJournalDescription(dtoGLPaymentVoucherHeader.getVoucherDescription());
		glydtOpenTransactionHeader
				.setJournalDescriptionArabic(dtoGLPaymentVoucherHeader.getVoucherDescriptionArabic());
		if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTransactionDate())) {
			glydtOpenTransactionHeader.setTransactionDate(
					UtilDateAndTime.yyyymmddStringToDate(dtoGLPaymentVoucherHeader.getTransactionDate()));
		}

		glydtOpenTransactionHeader.setTransactionPostingDate(new Date());
		glydtOpenTransactionHeader.setTransactionSource("PV");
		glydtOpenTransactionHeader.setOriginalJournalEntryID(dtoGLPaymentVoucherHeader.getPaymentVoucherID()+"");

		glydtOpenTransactionHeader.setAccountTableRowIndex(dtoGLPaymentVoucherHeader.getAccountTableRowIndex());
		glydtOpenTransactionHeader.setUserCreateTransaction(httpServlet.getHeader(CommonConstant.USER_ID));
		glydtOpenTransactionHeader.setUserPostingTransaction(httpServlet.getHeader(CommonConstant.USER_ID));
		glydtOpenTransactionHeader.setDistributionDescription(dtoGLPaymentVoucherHeader.getVoucherDescription());
		glydtOpenTransactionHeader.setCurrencyId(dtoGLPaymentVoucherHeader.getCurrencyID());
		glydtOpenTransactionHeader.setOriginalCompanyID(dtoGLPaymentVoucherHeader.getInterCompany());
		
		double creditAmount = 0.0;

		if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit() + "")) {
			Double originalCreditAmount = Double.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit());
			glydtOpenTransactionHeader.setOriginalCreditAmount(originalCreditAmount);
			// Set Original amount in above field if selected
			// currency Id USD
			glydtOpenTransactionHeader.setCreditAmount(originalCreditAmount);
			creditAmount = originalCreditAmount;
		}
		if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit() + "")) {
			Double originalTotalCreditAmount = Double
					.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit());
			glydtOpenTransactionHeader.setOriginalTotalJournalEntryCredit(originalTotalCreditAmount);
			// Set Original amount in above field if selected
			// currency Id USD
			glydtOpenTransactionHeader.setTotalJournalEntryCredit(originalTotalCreditAmount);
		}

		glydtOpenTransactionHeader.setBalanceType(2);

		CurrencyExchangeHeader currencyExchangeHeader1 = repositoryCurrencyExchangeHeader
				.findByExchangeIndexAndIsDeleted(dtoGLPaymentVoucherHeader.getExchangeTableIndex(), false);
		if (currencyExchangeHeader1 != null) {
			glydtOpenTransactionHeader.setExchangeTableIndex(dtoGLPaymentVoucherHeader.getExchangeTableIndex());
			if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getExchangeRate())) {
				Double currencyExchangeRate = Double.valueOf(dtoGLPaymentVoucherHeader.getExchangeRate());
				glydtOpenTransactionHeader.setExchangeTableRate(currencyExchangeRate);
				String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
				if (UtilRandomKey.isNotBlank(currencyExchangeHeader1.getRateCalcMethod())) {
					calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader1.getRateCalcMethod())
							.getMethod();
				}

				if (UtilRandomKey
						.isNotBlank(dtoGLPaymentVoucherHeader.getOriginalTotalPaymentVoucherCredit() + "")) {
					creditAmount = (double) mathEval
							.eval(Double.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit())
									+ calcMethod + currencyExchangeRate);
					glydtOpenTransactionHeader.setCreditAmount(creditAmount);
				}

				if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit() + "")) {
					double totalCreditAmount = (double) mathEval
							.eval(Double.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit())
									+ calcMethod + currencyExchangeRate);
					glydtOpenTransactionHeader.setTotalJournalEntryCredit(totalCreditAmount);
				}

			}
			data.add(glydtOpenTransactionHeader);
			
			if (dtoGLPaymentVoucherHeader.getVoucherDetailList() != null
					&& !dtoGLPaymentVoucherHeader.getVoucherDetailList().isEmpty()) {

				for (DtoGLPaymentVoucherDetail dtoGLPaymentVoucherDetail : dtoGLPaymentVoucherHeader
						.getVoucherDetailList()) {
					GLYTDOpenTransactions glytdOpenTransactions = new GLYTDOpenTransactions();
					glytdOpenTransactions.setOriginalCompanyID(dtoGLPaymentVoucherHeader.getInterCompany());
					glydtOpenTransactionHeader.setOriginalJournalEntryID(dtoGLPaymentVoucherHeader.getPaymentVoucherID()+"");
					glytdOpenTransactions.setOpenYear(Calendar.getInstance().get(Calendar.YEAR));
					//glytdOpenTransactions.setJournalEntryID(nextJournalId + "");

					glytdOpenTransactions.setOriginalTotalJournalEntryCredit(
							dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit());
					glytdOpenTransactions
							.setOriginalTotalJournalEntryDebit(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit());

					glytdOpenTransactions.setGlConfigurationAuditTrialCodes(glConfigurationAuditTrialCodes);
					glytdOpenTransactions.setModuleSeriesNumber(glConfigurationAuditTrialCodes);
					glytdOpenTransactions.setJournalDescription(dtoGLPaymentVoucherDetail.getDistributionDescription());
					glytdOpenTransactions
							.setJournalDescriptionArabic(dtoGLPaymentVoucherDetail.getDistributionDescription());
					glydtOpenTransactionHeader
							.setOriginalCompanyID(dtoGLPaymentVoucherDetail.getInterCompanyIDMultiCompanyTransaction());
					if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTransactionDate())) {
						glytdOpenTransactions.setTransactionDate(
								UtilDateAndTime.yyyymmddStringToDate(dtoGLPaymentVoucherHeader.getTransactionDate()));
					}
					glytdOpenTransactions.setTransactionPostingDate(new Date());
					glytdOpenTransactions.setTransactionSource("PV");

					glytdOpenTransactions.setAccountTableRowIndex(dtoGLPaymentVoucherDetail.getAccountTableRowIndex());
					glytdOpenTransactions.setUserCreateTransaction(httpServlet.getHeader(CommonConstant.USER_ID));
					glytdOpenTransactions.setUserPostingTransaction(httpServlet.getHeader(CommonConstant.USER_ID));
					glytdOpenTransactions
							.setDistributionDescription(dtoGLPaymentVoucherDetail.getDistributionDescription());
					glytdOpenTransactions.setCurrencyId(dtoGLPaymentVoucherHeader.getCurrencyID());

					double debitAmount = 0.0;

					if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherDetail.getDebitAmount() + "")) {
						Double originalDebitAmount = Double.valueOf(dtoGLPaymentVoucherDetail.getOriginalDebitAmount());
						glytdOpenTransactions.setOriginalDebitAmount(originalDebitAmount);
						// Set Original amount in above field if selected
						// currency Id USD
						glytdOpenTransactions.setDebitAmount(originalDebitAmount);

						debitAmount = originalDebitAmount;
					}

					if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit() + "")) {
						Double originalDebitAmount = Double
								.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit());
						glytdOpenTransactions.setOriginalTotalJournalEntryDebit(originalDebitAmount);
						// Set Original amount in above field if selected
						// currency Id USD
						glytdOpenTransactions.setTotalJournalEntryDebit(originalDebitAmount);
					}

					glytdOpenTransactions.setBalanceType(1);

					CurrencyExchangeHeader currencyExchangeHeader = repositoryCurrencyExchangeHeader
							.findByExchangeIndexAndIsDeleted(dtoGLPaymentVoucherHeader.getExchangeTableIndex(), false);
					if (currencyExchangeHeader != null) {
						glytdOpenTransactions.setExchangeTableIndex(dtoGLPaymentVoucherHeader.getExchangeTableIndex());
						if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherHeader.getExchangeRate())) {
							Double currencyExchangeRate = Double.valueOf(dtoGLPaymentVoucherHeader.getExchangeRate());
							glytdOpenTransactions.setExchangeTableRate(currencyExchangeRate);
							String calcMethod = RateCalculationMethod.MULTIPLY.getMethod();
							if (UtilRandomKey.isNotBlank(currencyExchangeHeader.getRateCalcMethod())) {
								calcMethod = RateCalculationMethod.getByName(currencyExchangeHeader.getRateCalcMethod())
										.getMethod();
							}

							if (UtilRandomKey.isNotBlank(dtoGLPaymentVoucherDetail.getOriginalDebitAmount() + "")) {
								debitAmount = (double) mathEval
										.eval(Double.valueOf(dtoGLPaymentVoucherDetail.getOriginalDebitAmount()) + calcMethod
												+ currencyExchangeRate);
								glytdOpenTransactions.setDebitAmount(debitAmount);
							}

							if (UtilRandomKey
									.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit() + "")) {
								double totalDebitAmount = (double) mathEval
										.eval(Double.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit())
												+ calcMethod + currencyExchangeRate);
								glytdOpenTransactions.setTotalJournalEntryDebit(totalDebitAmount);
							}

							if (UtilRandomKey
									.isNotBlank(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit() + "")) {
								double totalDebitAmount = (double) mathEval
										.eval(Double.valueOf(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherDebit())
												+ calcMethod + currencyExchangeRate);
								glytdOpenTransactions.setTotalJournalEntryCredit(totalDebitAmount);
							}

						}
					}

					glytdOpenTransactions.setCreditAmount(0.0);
					glytdOpenTransactions.setOriginalCreditAmount(0.0);
					glytdOpenTransactions.setTotalJournalEntryCredit(glydtOpenTransactionHeader.getTotalJournalEntryCredit());
					glytdOpenTransactions.setOriginalTotalJournalEntryCredit(glydtOpenTransactionHeader.getOriginalTotalJournalEntryCredit());

					
					data.add(glytdOpenTransactions);
					
		
				}
				
		}
		glydtOpenTransactionHeader.setAccountTableRowIndex(dtoGLPaymentVoucherHeader.getAccountTableRowIndex());
		glydtOpenTransactionHeader.setDebitAmount(0.0);
		glydtOpenTransactionHeader.setTotalJournalEntryDebit(glydtOpenTransactionHeader.getTotalJournalEntryCredit());
		glydtOpenTransactionHeader.setOriginalDebitAmount(0.0);
		glydtOpenTransactionHeader.setOriginalTotalJournalEntryDebit(dtoGLPaymentVoucherHeader.getTotalPaymentVoucherCredit());
		}
		
		return data;
	}

	public DtoJournalEntryHeader modelPVPostedToDtoJVUnposted(GLPaymentVoucherHeader glPaymentVoucherHeader) {
		
		DtoJournalEntryHeader dtoJournalEntryHeader = new DtoJournalEntryHeader();
		dtoJournalEntryHeader.setJournalDescription(glPaymentVoucherHeader.getVoucherDescription());
		dtoJournalEntryHeader.setJournalDescriptionArabic(glPaymentVoucherHeader.getVoucherDescriptionArabic());
		
		if(UtilRandomKey.isNotBlank(glPaymentVoucherHeader.getTransactionDate().toString())) {
			dtoJournalEntryHeader.setTransactionDate(UtilDateAndTime.dateToStringddmmyyyy(glPaymentVoucherHeader.getTransactionDate()));
		}
		
		dtoJournalEntryHeader.setCurrencyID(glPaymentVoucherHeader.getCurrencySetup().getCurrencyId());
		dtoJournalEntryHeader.setExchangeTableIndex(glPaymentVoucherHeader.getCurrencyExchangeHeader().getExchangeIndex());
		dtoJournalEntryHeader.setExchangeRate(glPaymentVoucherHeader.getExchangeRate()+"");
		
		dtoJournalEntryHeader.setTotalJournalEntry(glPaymentVoucherHeader.getOriginalTotalVoucherEntry()+"");
		dtoJournalEntryHeader.setTotalJournalEntryCredit(glPaymentVoucherHeader.getOriginalTotlalVoucherCredit()+"");
		dtoJournalEntryHeader.setTotalJournalEntryDebit(glPaymentVoucherHeader.getOriginalTotalVoucherDebit()+"");
		
		dtoJournalEntryHeader.setOriginalJournalID(glPaymentVoucherHeader.getPaymentVoucherId()+"");
		dtoJournalEntryHeader.setTransactionSource("PV");
		dtoJournalEntryHeader.setSourceDocumentId(glPaymentVoucherHeader.getGlConfigurationAuditTrialCodes() != null ? 
				glPaymentVoucherHeader.getGlConfigurationAuditTrialCodes().getSeriesIndex(): null);
		
		List<DtoJournalEntryDetail> data = new ArrayList<>();
	
		for(GLPaymentVoucherDetail glPaymentVoucherDetail : glPaymentVoucherHeader.getPaymentVoucherDetail()) {
			DtoJournalEntryDetail dtoJournalEntryDetail = new DtoJournalEntryDetail();
			
			dtoJournalEntryDetail.setAccountTableRowIndex(glPaymentVoucherDetail.getAccountTableRowIndex()+"");
			dtoJournalEntryDetail.setDebitAmount(glPaymentVoucherDetail.getOriginalDebitAmount() != null ? glPaymentVoucherDetail.getOriginalDebitAmount()
					+"": "0.0");
			dtoJournalEntryDetail.setCreditAmount("0.0");
			dtoJournalEntryDetail.setDistributionDescription(glPaymentVoucherDetail.getDistributionDescription());
			
			data.add(dtoJournalEntryDetail);
		}
		DtoJournalEntryDetail dtoJournalEntryDetailH = new DtoJournalEntryDetail();
		
		dtoJournalEntryDetailH.setAccountTableRowIndex(glPaymentVoucherHeader.getAccountTableRowIndex());
		dtoJournalEntryDetailH.setCreditAmount(glPaymentVoucherHeader.getOriginalTotlalVoucherCredit()+"");
		dtoJournalEntryDetailH.setDebitAmount("0.0");
		dtoJournalEntryDetailH.setDistributionDescription(glPaymentVoucherHeader.getVoucherDescription());
		data.add(dtoJournalEntryDetailH);
		
		dtoJournalEntryHeader.setJournalEntryDetailsList(data);
		return dtoJournalEntryHeader;
		
	}


}
