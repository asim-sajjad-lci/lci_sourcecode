package com.bti.util.mapping.GL;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.GLYTDOpenBankTransactionsEntryDetails;
import com.bti.model.GLYTDOpenBankTransactionsEntryHeader;
import com.bti.model.dto.DtoGLCashReceiptBank;
import com.bti.model.dto.DtoJournalEntryDetail;
import com.bti.model.dto.DtoJournalEntryHeader;
import com.bti.repository.RepositoryGLConfigurationAuditTrialCodes;
import com.bti.repository.RepositoryGLYTDOpenBankTransactionsEntryDetails;
import com.bti.util.UtilDateAndTime;

@Service("mappingReceiptVoucher")
public class MappingReceiptVouhcer {
	
	@Autowired
	RepositoryGLYTDOpenBankTransactionsEntryDetails repositoryGLYTDOpenBankTransactionsEntryDetails;
	
	@Autowired
	RepositoryGLConfigurationAuditTrialCodes repositoryGLConfigurationAuditTrialCodes;

	public DtoJournalEntryHeader modelRVPostedToDtoJVUnposted(DtoGLCashReceiptBank dtoGLCashReceiptBank,GLYTDOpenBankTransactionsEntryHeader glytdOpenBankTransactionsEntryHeader,List<GLYTDOpenBankTransactionsEntryDetails>  glytdOpenBankTransactionsEntryDetails) {
		DtoJournalEntryHeader dtoJournalEntryHeader = new DtoJournalEntryHeader();
		
		dtoJournalEntryHeader.setOriginalJournalID(glytdOpenBankTransactionsEntryHeader.getBankTransactionDescription());
		dtoJournalEntryHeader.setTransactionDate(UtilDateAndTime.dateToStringddmmyyyy(glytdOpenBankTransactionsEntryHeader.getBankTransactionDate()));
		dtoJournalEntryHeader.setCurrencyID(glytdOpenBankTransactionsEntryHeader.getCurrencyId());
		dtoJournalEntryHeader.setExchangeRate(glytdOpenBankTransactionsEntryHeader.getExchangeRate()+"");
		dtoJournalEntryHeader.setJournalDescription(glytdOpenBankTransactionsEntryHeader.getBankTransactionDescription());
		dtoJournalEntryHeader.setJournalDescriptionArabic(glytdOpenBankTransactionsEntryHeader.getBankTransactionDescription());
		dtoJournalEntryHeader.setTotalJournalEntry(glytdOpenBankTransactionsEntryHeader.getBankTransactionAmount()+"");
		dtoJournalEntryHeader.setTotalJournalEntryCredit((dtoGLCashReceiptBank.getReceiptAmount())+"");
		dtoJournalEntryHeader.setTotalJournalEntryDebit((dtoGLCashReceiptBank.getReceiptAmount())+"");
		dtoJournalEntryHeader.setTransactionPostedDate(UtilDateAndTime.dateToStringddmmyyyy(glytdOpenBankTransactionsEntryHeader.getPostingDate()));
		dtoJournalEntryHeader.setExchangeTableIndex(Integer.valueOf(glytdOpenBankTransactionsEntryHeader.getExchangeTableIndex()));
		dtoJournalEntryHeader.setSourceDocumentId(glytdOpenBankTransactionsEntryHeader.getGlConfigurationAuditTrialCodes().getSeriesIndex());
		dtoJournalEntryHeader.setTransactionSource("RV");
		dtoJournalEntryHeader.setOriginalJournalID(glytdOpenBankTransactionsEntryHeader.getBankTransactionEntryID());
		List<GLYTDOpenBankTransactionsEntryDetails> details = glytdOpenBankTransactionsEntryDetails;
		List<DtoJournalEntryDetail> dtoDetails = new ArrayList<>();
		for(GLYTDOpenBankTransactionsEntryDetails glytdOpenBankTransactionsEntryDetail: details) {
			DtoJournalEntryDetail dtoJournalEntryDetail = new DtoJournalEntryDetail();
			dtoJournalEntryDetail.setAccountTableRowIndex(glytdOpenBankTransactionsEntryDetail.getAccountTableRowIndex()+"");
			dtoJournalEntryDetail.setDebitAmount( (glytdOpenBankTransactionsEntryDetail.getDebitAmount() != null)? glytdOpenBankTransactionsEntryDetail.getDebitAmount()+"":"0.0");
			dtoJournalEntryDetail.setCreditAmount((glytdOpenBankTransactionsEntryDetail.getCreditAmount() != null) ? glytdOpenBankTransactionsEntryDetail.getCreditAmount()+"":"0.0");
			dtoJournalEntryDetail.setDistributionDescription(glytdOpenBankTransactionsEntryDetail.getDistributionDescription());
			dtoDetails.add(dtoJournalEntryDetail);
			
		}
		dtoJournalEntryHeader.setJournalEntryDetailsList(dtoDetails);
		return dtoJournalEntryHeader;
	}
}
