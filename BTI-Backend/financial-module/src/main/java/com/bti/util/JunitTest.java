package com.bti.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.doubleThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.bti.service.ServiceBankSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JunitTest {

	@Autowired
	ServiceBankSetup serviceBankSetup;
	
	
	private MockMvc mockMvc;
	
	@Test
	public void test() throws Exception
	{
		   HttpUriRequest request = new HttpGet( "https://localhost:8082/getGlAccountNumberList");
		   // When
		   HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );
		   // Then
		   System.out.println( httpResponse.getStatusLine().getStatusCode());
	}
	public static void main(String args[]){
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");   
		double result=0;
		try {
			result = (double) engine.eval(Double.valueOf(2)+"*"+Double.valueOf(2));
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(result);
	}
}
