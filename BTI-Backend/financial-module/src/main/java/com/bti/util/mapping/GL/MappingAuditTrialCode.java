package com.bti.util.mapping.GL;

import org.springframework.stereotype.Service;

import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.dto.DtoAuditTrailCode;

@Service("mappingAuditTrialCode")
public class MappingAuditTrialCode {

	public DtoAuditTrailCode modelAuditToDtoAudit(GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes) {
		DtoAuditTrailCode auditTrialCode = new DtoAuditTrailCode();

		auditTrialCode.setSeriesIndex(glConfigurationAuditTrialCodes.getSeriesIndex());
		auditTrialCode.setSeriesId(0);
		if(glConfigurationAuditTrialCodes.getGlConfigurationAuditTrialSeriesType()!=null){
			auditTrialCode.setSeriesId(glConfigurationAuditTrialCodes.getGlConfigurationAuditTrialSeriesType().getTypeId());
			auditTrialCode.setSeriesName(glConfigurationAuditTrialCodes.getGlConfigurationAuditTrialSeriesType().getTypePrimary());
		}
		auditTrialCode.setSourceDocument(glConfigurationAuditTrialCodes.getSourceDocument());
		auditTrialCode.setSourceCode( glConfigurationAuditTrialCodes.getTransactionSourceCode());
		auditTrialCode.setSequenceNumber(glConfigurationAuditTrialCodes.getSequenceNumber());
		auditTrialCode.setSeriesNumber(glConfigurationAuditTrialCodes.getNextTransactionSourceNumber());

		return auditTrialCode;
	}
}
