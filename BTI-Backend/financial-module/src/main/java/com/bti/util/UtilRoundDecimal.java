/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * Description: Round Decimal Utility for BTI 
 * Name of Project: BTI
 * Created on: May 11, 2017
 * Modified on: May 12, 2017 5:39:32 PM
 * @author seasia
 * Version: 
 */
public class UtilRoundDecimal {

	public static double roundDecimalValue(Double value)
	{
		if(value!=null){
			BigDecimal bigDecimal= new BigDecimal(String.valueOf(value)).setScale(3,BigDecimal.ROUND_HALF_UP);
			return bigDecimal.doubleValue();
		}
		else
		{
			return 0.0;
		}
	}

}
