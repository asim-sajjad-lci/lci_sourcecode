/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.util;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import ch.qos.logback.classic.Logger;

/**
 * Description: Round Decimal Utility for BTI 
 * Name of Project: BTI
 * Created on: May 11, 2017
 * Modified on: May 12, 2017 5:39:32 PM
 * @author SNS
 * Version: 
 */
public class CommonUtils {

	public static double parseDouble(String value)
	{
		double d = 0.0;
		try {
			if(value!=null && !value.isEmpty()){
				d = Double.parseDouble(value);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			d = 0.0;
		}
		return d;
	}
	
	public static float parseFloat(String value)
	{
		float f = 0.0f;
		try {
			if(value!=null && !value.isEmpty()){
				f = Float.parseFloat(value);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			f = 0.0f;
		}
		return f;
	}

	public static Long parseLong(String value)
	{
		Long i = 0l;
		try {
			if(value!=null && !value.isEmpty()){
				i = Long.parseLong(value);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			i = 0l;
		}
		return i;
	}

	public static Integer parseInteger(String value)
	{
		Integer i = 0;
		try {
			if(value!=null && !value.isEmpty()){
				i = Integer.parseInt(value);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			i = 0;
		}
		return i;
	}
	
	public static String removeNull(String str) {
		if (str == null) {
			return "";
		}
		return str;
	}
	

	public static Long castToLong(Object obj) {
		Long value = 0l;

		if (obj == null) {
			return 0l;
		
		} else if (obj instanceof String) {
			return parseLong((String) obj);
		
		} else if ((obj instanceof Number)) {
			try {
				return (Long) obj;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		
		} else {
		
			try {
				value = (Long) obj;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
		}
		
		return value;
	}

	public static Integer castToInteger(Object obj) {
		Integer value = 0;

		if (obj == null) {
			return 0;
		
		} else if (obj instanceof String) {
			return parseInteger((String) obj);
		
		} else if ((obj instanceof Number)) {
			try {
				return (Integer) obj;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		
		} else {
		
			try {
				value = (Integer) obj;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
		}
		
		return value;
	}


	public static Float castToFloat(Object obj) {
		Float value = 0.0f;

		if (obj == null) {
			return 0.0f;
		
		} else if (obj instanceof String) {
			return parseFloat((String) obj);
		
		} else if ((obj instanceof Number)) {
			try {
				return (Float) obj;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		
		} else {
		
			try {
				value = (Float) obj;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
		}
		
		return value;
	}
	
	public static Double castToDouble(Object obj) {
		Double value = 0.0;

		if (obj == null) {
			return 0.0;
		
		} else if (obj instanceof String) {
			return parseDouble((String) obj);
		
		} else if ((obj instanceof Number)) {
			try {
				return (Double) obj;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		
		} else {
		
			try {
				value = (Double) obj;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
		}
		
		return value;
	}

	public static String castToString(Object obj) {
		String value = "";

		if (obj == null) {
			return value;
		
		} else if (obj instanceof String) {
			value = (String) obj;
		
		}  else {
		
			try {
				value = obj.toString();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
		}
		
		return value;
	}

}
