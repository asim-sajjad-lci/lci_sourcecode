/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.SalesmanMaintenance;
import com.bti.util.UtilRoundDecimal;

public class DtoSalesmanMaintenance {

	private String salesmanId;
	private String address1;
	private String address2;
	private int applyPercentage;
	private String employeeId;
	private String salesmanFirstName;
	private String salesmanFirstNameArabic;
	private int inactive;
	private String salesmanLastName;
	private String salesmanLastNameArabic;
	private String salesmanMidName;
	private String salesmanMidNameArabic;
	private Double percentageAmount;
	private String phone1;
	private String phone2;
	private String salesTerritoryId;
	//Double
	private String salesCommissionsYTD;
	private String salesCommissionsLastYear;
	private String totalCommissionsYTD;
	private String totalCommissionsLastYear;
	private String costOfSalesYTD;
	private String costOfSalesLastYear;
	
	private String messageType;
	public DtoSalesmanMaintenance() {
		
	}
	public DtoSalesmanMaintenance(SalesmanMaintenance salesmanMaintenance) {
		this.setAddress1(salesmanMaintenance.getAddress1());
		this.setAddress2(salesmanMaintenance.getAddress2());
		//this.setApplyPercentage(salesmanMaintenance.getApplyPercentage());
		this.setCostOfSalesLastYear("");
		if(salesmanMaintenance.getCostOfSalesLastYear()!=null){
			this.setCostOfSalesLastYear(String.valueOf(salesmanMaintenance.getCostOfSalesLastYear()));
		}
		this.setCostOfSalesYTD("");
		if(salesmanMaintenance.getCostOfSalesYTD()!=null){
			this.setCostOfSalesYTD(String.valueOf(salesmanMaintenance.getCostOfSalesYTD()));
		}
		this.setEmployeeId(salesmanMaintenance.getEmployeeId());
		this.setInactive(salesmanMaintenance.getInactive());
		//this.setPercentageAmount(salesmanMaintenance.getPercentageAmount());
		this.setPhone1(salesmanMaintenance.getPhone1());
		this.setPhone2(salesmanMaintenance.getPhone2());
		this.setSalesCommissionsLastYear("");
		if(salesmanMaintenance.getSalesCommissionsLastYear()!=null){
			this.setSalesCommissionsLastYear(String.valueOf(salesmanMaintenance.getSalesCommissionsLastYear()));
		}
		this.setSalesCommissionsYTD("");
		if(salesmanMaintenance.getSalesCommissionsYTD()!=null){
			this.setSalesCommissionsYTD(String.valueOf(salesmanMaintenance.getSalesCommissionsYTD()));
		}
		
		this.setSalesmanFirstName(salesmanMaintenance.getSalesmanFirstName());
		this.setSalesmanFirstNameArabic(salesmanMaintenance.getSalesmanFirstNameArabic());
		this.setSalesmanId(salesmanMaintenance.getSalesmanId());
		this.setSalesmanLastName(salesmanMaintenance.getSalesmanLastName());
		this.setSalesmanLastNameArabic(salesmanMaintenance.getSalesmanLastNameArabic());
		this.setSalesmanMidName(salesmanMaintenance.getSalesmanMidName());
		this.setSalesmanMidNameArabic(salesmanMaintenance.getSalesmanMidNameArabic());
		this.setSalesTerritoryId(salesmanMaintenance.getSalesTerritoryId());
		this.setTotalCommissionsLastYear("");
		if(salesmanMaintenance.getTotalCommissionsLastYear()!=null){
			this.setTotalCommissionsLastYear(String.valueOf(salesmanMaintenance.getTotalCommissionsLastYear()));
		}
		this.setTotalCommissionsYTD("");
		if(salesmanMaintenance.getTotalCommissionsYTD()!=null){
			this.setTotalCommissionsYTD(String.valueOf(salesmanMaintenance.getTotalCommissionsYTD()));
		}
	}
	public String getSalesmanId() {
		return salesmanId;
	}
	public void setSalesmanId(String salesmanId) {
		this.salesmanId = salesmanId;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public int getApplyPercentage() {
		return applyPercentage;
	}
	public void setApplyPercentage(int applyPercentage) {
		this.applyPercentage = applyPercentage;
	}
	public String getCostOfSalesYTD() {
		return costOfSalesYTD;
	}
	public void setCostOfSalesYTD(String costOfSalesYTD) {
		this.costOfSalesYTD = costOfSalesYTD;
	}
	public String getCostOfSalesLastYear() {
		return costOfSalesLastYear;
	}
	public void setCostOfSalesLastYear(String costOfSalesLastYear) {
		this.costOfSalesLastYear = costOfSalesLastYear;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getSalesmanFirstName() {
		return salesmanFirstName;
	}
	public void setSalesmanFirstName(String salesmanFirstName) {
		this.salesmanFirstName = salesmanFirstName;
	}
	public String getSalesmanFirstNameArabic() {
		return salesmanFirstNameArabic;
	}
	public void setSalesmanFirstNameArabic(String salesmanFirstNameArabic) {
		this.salesmanFirstNameArabic = salesmanFirstNameArabic;
	}
	public int getInactive() {
		return inactive;
	}
	public void setInactive(int inactive) {
		this.inactive = inactive;
	}
	public String getSalesmanLastName() {
		return salesmanLastName;
	}
	public void setSalesmanLastName(String salesmanLastName) {
		this.salesmanLastName = salesmanLastName;
	}
	public String getSalesmanLastNameArabic() {
		return salesmanLastNameArabic;
	}
	public void setSalesmanLastNameArabic(String salesmanLastNameArabic) {
		this.salesmanLastNameArabic = salesmanLastNameArabic;
	}
	public String getSalesmanMidName() {
		return salesmanMidName;
	}
	public void setSalesmanMidName(String salesmanMidName) {
		this.salesmanMidName = salesmanMidName;
	}
	public String getSalesmanMidNameArabic() {
		return salesmanMidNameArabic;
	}
	public void setSalesmanMidNameArabic(String salesmanMidNameArabic) {
		this.salesmanMidNameArabic = salesmanMidNameArabic;
	}
	public Double getPercentageAmount() {
		return percentageAmount;
	}
	public void setPercentageAmount(Double percentageAmount) {
		this.percentageAmount = UtilRoundDecimal.roundDecimalValue(percentageAmount);
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}
	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}
	
	
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getSalesCommissionsYTD() {
		return salesCommissionsYTD;
	}
	public void setSalesCommissionsYTD(String salesCommissionsYTD) {
		this.salesCommissionsYTD = salesCommissionsYTD;
	}
	public String getSalesCommissionsLastYear() {
		return salesCommissionsLastYear;
	}
	public void setSalesCommissionsLastYear(String salesCommissionsLastYear) {
		this.salesCommissionsLastYear = salesCommissionsLastYear;
	}
	public String getTotalCommissionsYTD() {
		return totalCommissionsYTD;
	}
	public void setTotalCommissionsYTD(String totalCommissionsYTD) {
		this.totalCommissionsYTD = totalCommissionsYTD;
	}
	public String getTotalCommissionsLastYear() {
		return totalCommissionsLastYear;
	}
	public void setTotalCommissionsLastYear(String totalCommissionsLastYear) {
		this.totalCommissionsLastYear = totalCommissionsLastYear;
	}
	
	
	
}
