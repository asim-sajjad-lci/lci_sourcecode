/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ap00103 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap00103")
@NamedQuery(name="VendorAccountTableSetup.findAll", query="SELECT a FROM VendorAccountTableSetup a")
public class VendorAccountTableSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACCTABLID")
	private int accountTableIndex;

	@Column(name="ACCTABLPID")
	private int apAccountTableIndex;
	
	@ManyToOne
	@JoinColumn(name="ACCTCTTYPID")
	private MasterAccountType masterAccountType;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CREATDBY")
	private int createdBy;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ap00102
	/*@OneToMany(mappedBy="vendorAccountTableSetup")
	private List<VendorMaintenanceOptions> vendorMaintenanceOptions;*/

	//bi-directional many-to-one association to Ap00101
	@ManyToOne
	@JoinColumn(name="VENDORID")
	private VendorMaintenance vendorMaintenance;

	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;

	public VendorAccountTableSetup() {
	}

	public int getAccountTableIndex() {
		return accountTableIndex;
	}

	public void setAccountTableIndex(int accountTableIndex) {
		this.accountTableIndex = accountTableIndex;
	}

	public int getApAccountTableIndex() {
		return apAccountTableIndex;
	}

	public void setApAccountTableIndex(int apAccountTableIndex) {
		this.apAccountTableIndex = apAccountTableIndex;
	}

	public MasterAccountType getMasterAccountType() {
		return masterAccountType;
	}

	public void setMasterAccountType(MasterAccountType masterAccountType) {
		this.masterAccountType = masterAccountType;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	/*public List<VendorMaintenanceOptions> getVendorMaintenanceOptionsList() {
		return vendorMaintenanceOptionsList;
	}

	public void setVendorMaintenanceOptionsList(List<VendorMaintenanceOptions> vendorMaintenanceOptionsList) {
		this.vendorMaintenanceOptionsList = vendorMaintenanceOptionsList;
	}*/

	public VendorMaintenance getVendorMaintenance() {
		return vendorMaintenance;
	}

	public void setVendorMaintenance(VendorMaintenance vendorMaintenance) {
		this.vendorMaintenance = vendorMaintenance;
	}

	
	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	/*public List<VendorMaintenanceOptions> getVendorMaintenanceOptions() {
		return vendorMaintenanceOptions;
	}

	public void setVendorMaintenanceOptions(List<VendorMaintenanceOptions> vendorMaintenanceOptions) {
		this.vendorMaintenanceOptions = vendorMaintenanceOptions;
	}*/

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	
	
}