package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

//@IdClass(value = rptaccountstatement_lcl.class)
@Entity 
@Immutable
@Table(name = "trl_blnc_n_act_stmt_rpts_v")
public class trl_blnc_n_act_stmt_rpts_v implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="journal_id")
	private long journal_id;

//	@Id
	@Column(name="account_number")
	private String account_number;
	
	
//	distribution_reference
	@Column(name="distribution_reference")
	private String distribution_reference;
	
//	act_row_id
	@Column(name="act_row_id")
	private int act_row_id;
	
	//	account_type
	@Column(name="account_type")
	private int account_type;
	
//	main_account
	@Column(name="main_account")
	private String main_account;
	
//	@Id
	@Column(name="main_account_description")
	private String main_account_description;
	
//	@Id
	@Column(name="account_category_description")
	private String account_category_description;
	
//	@Id
	@Column(name="transaction_date")
	private String transaction_date;
	
//	@Id
	@Column(name="main_account_description_arabic")
	private String main_account_description_arabic;
	
//	@Id
	@Column(name="debit_amount")
	private double debit_amount;
	
//	@Id
	@Column(name="credit_amount")
	private double credit_amount;
	
//	@Id
//	@Column(name="History")
	private int History = 0;

	public long getJournal_ID() {
		return journal_id;
	}

	public void setJournal_ID(long journal_ID) {
		journal_id = journal_ID;
	}

	public String getAccount_Number() {
		return account_number;
	}

	public void setAccount_Number(String account_Number) {
		account_number = account_Number;
	}

	public String getAccount_Description() {
		return main_account_description;
	}

	public void setAccount_Description(String account_Description) {
		main_account_description = account_Description;
	}

	public String getDistribution_reference() {
		return distribution_reference;
	}

	public void setDistribution_reference(String distribution_reference) {
		this.distribution_reference = distribution_reference;
	}

	public int getAct_row_id() {
		return act_row_id;
	}

	public void setAct_row_id(int act_row_id) {
		this.act_row_id = act_row_id;
	}

	public int getAccount_type() {
		return account_type;
	}

	public void setAccount_type(int account_type) {
		this.account_type = account_type;
	}

	public String getMain_account() {
		return main_account;
	}

	public void setMain_account(String main_account) {
		this.main_account = main_account;
	}

	public String getAccount_Categrory() {
		return account_category_description;
	}

	public void setAccount_Categrory(String account_Categrory) {
		account_category_description = account_Categrory;
	}

	public String getJournal_Date() {
		return transaction_date;
	}

	public void setJournal_Date(String journal_Date) {
		transaction_date = journal_Date;
	}

	public String getDistribution_Reference() {
		return main_account_description_arabic;
	}

	public void setDistribution_Reference(String distribution_Reference) {
		main_account_description_arabic = distribution_Reference;
	}


	public double getDebit_amount() {
		return debit_amount;
	}

	public void setDebit_amount(double debit_amount) {
		this.debit_amount = debit_amount;
	}

	public double getCredit_amount() {
		return credit_amount;
	}

	public void setCredit_amount(double credit_amount) {
		this.credit_amount = credit_amount;
	}

	public int getHistory() {
		return History;
	}

	public void setHistory(int history) {
		History = history;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
