/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.NationalAccountMaintenanceDetails;
import com.bti.util.UtilRoundDecimal;

public class DtoNationalAccountMaintenanceDetail {

	private int custNumberChildIndex;
	private String parentCustId;
	private String childCustId;
	private String childCustNamePrimary;
	private String chilsCustNameSecondary;
	private double custChildBalance;
	
	public DtoNationalAccountMaintenanceDetail(){
		
	}
	
	public DtoNationalAccountMaintenanceDetail(NationalAccountMaintenanceDetails accountMaintenanceDetails){
		this.custNumberChildIndex = accountMaintenanceDetails.getCustomerNumberChildrenIndex();
		this.parentCustId = "";
			if(accountMaintenanceDetails.getAccountMaintenanceHeader()!=null && accountMaintenanceDetails.getAccountMaintenanceHeader().getCustomerMaintenance()!=null){
				this.parentCustId=accountMaintenanceDetails.getAccountMaintenanceHeader().getCustomerMaintenance().getCustnumber();
			}
		this.childCustId ="";
		if(accountMaintenanceDetails.getCustomerMaintenance()!=null){
			this.childCustId=accountMaintenanceDetails.getCustomerMaintenance().getCustnumber();
		}
		this.childCustNamePrimary = accountMaintenanceDetails.getCustomerNameChildren();
		this.chilsCustNameSecondary = accountMaintenanceDetails.getCustomerNameChildrenArabic();
		this.custChildBalance = UtilRoundDecimal.roundDecimalValue(accountMaintenanceDetails.getCustomerChildrenBalance());
	}
	
	public int getCustNumberChildIndex() {
		return custNumberChildIndex;
	}
	public void setCustNumberChildIndex(int custNumberChildIndex) {
		this.custNumberChildIndex = custNumberChildIndex;
	}
	public String getParentCustId() {
		return parentCustId;
	}
	public void setParentCustId(String parentCustId) {
		this.parentCustId = parentCustId;
	}
	public String getChildCustId() {
		return childCustId;
	}
	public void setChildCustId(String childCustId) {
		this.childCustId = childCustId;
	}
	public String getChildCustNamePrimary() {
		return childCustNamePrimary;
	}
	public void setChildCustNamePrimary(String childCustNamePrimary) {
		this.childCustNamePrimary = childCustNamePrimary;
	}
	public String getChilsCustNameSecondary() {
		return chilsCustNameSecondary;
	}
	public void setChilsCustNameSecondary(String chilsCustNameSecondary) {
		this.chilsCustNameSecondary = chilsCustNameSecondary;
	}

	public double getCustChildBalance() {
		return custChildBalance;
	}

	public void setCustChildBalance(double custChildBalance) {
		this.custChildBalance = UtilRoundDecimal.roundDecimalValue(custChildBalance);
	}
	
}
