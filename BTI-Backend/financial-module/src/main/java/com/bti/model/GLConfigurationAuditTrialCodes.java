/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the gl40002 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40002")
@NamedQuery(name="GLConfigurationAuditTrialCodes.findAll", query="SELECT g FROM GLConfigurationAuditTrialCodes g")
public class GLConfigurationAuditTrialCodes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SERIESINDX")
	private int seriesIndex;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="NXTTRXNUM")
	private int nextTransactionSourceNumber;

	@Column(name="SEQNUMBR")
	private int sequenceNumber;

	@Column(name="SOURCDOC")
	private String sourceDocument;

	@Column(name="TRXSOURC")
	private String transactionSourceDescription;

	@Column(name="TRXSRCPX")
	private String transactionSourceCode;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	/*//bi-directional one-to-one association to Sy40000
	@OneToOne
	@JoinColumn(name="SERIES")
	private ModulesConfigurationPeriod modulesConfigurationPeriod;*/
	
	 
	@OneToOne
	@JoinColumn(name="SERIES")
	private GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType;
	
	public GLConfigurationAuditTrialCodes() {
	}

	public int getSeriesIndex() {
		return seriesIndex;
	}

	public void setSeriesIndex(int seriesIndex) {
		this.seriesIndex = seriesIndex;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getNextTransactionSourceNumber() {
		return nextTransactionSourceNumber;
	}

	public void setNextTransactionSourceNumber(int nextTransactionSourceNumber) {
		this.nextTransactionSourceNumber = nextTransactionSourceNumber;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getSourceDocument() {
		return sourceDocument;
	}

	public void setSourceDocument(String sourceDocument) {
		this.sourceDocument = sourceDocument;
	}

	public String getTransactionSourceDescription() {
		return transactionSourceDescription;
	}

	public void setTransactionSourceDescription(String transactionSourceDescription) {
		this.transactionSourceDescription = transactionSourceDescription;
	}

	public String getTransactionSourceCode() {
		return transactionSourceCode;
	}

	public void setTransactionSourceCode(String transactionSourceCode) {
		this.transactionSourceCode = transactionSourceCode;
	}

	public GLConfigurationAuditTrialSeriesType getGlConfigurationAuditTrialSeriesType() {
		return glConfigurationAuditTrialSeriesType;
	}

	public void setGlConfigurationAuditTrialSeriesType(
			GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType) {
		this.glConfigurationAuditTrialSeriesType = glConfigurationAuditTrialSeriesType;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	 

	/*public ModulesConfigurationPeriod getModulesConfigurationPeriod() {
		return modulesConfigurationPeriod;
	}

	public void setModulesConfigurationPeriod(ModulesConfigurationPeriod modulesConfigurationPeriod) {
		this.modulesConfigurationPeriod = modulesConfigurationPeriod;
	}*/
	
	

}