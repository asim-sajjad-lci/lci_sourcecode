/**
 * BTI - BAAN for Technology And Trade IntegerL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.ARCashReceipt;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoARCashReceipt {

	private String cashReceiptNumber;
	private String batchID;
	private String customerNumber;
	private String customerName;
	private String customerNameArabic;
	private String currencyID;
	private Integer cashReceiptType;
	private Double cashReceiptAmount;
	private String checkBookId;
	private String creditCardID;
	private String creditCardNumber;
	private String checkNumber;
	private Integer creditCardExpireMonth;
	private Integer creditCardExpireYear;
	private String cashReceiptDescription;
	private String cashReceiptCreateDate;
	private String cashReceiptPostingDate;
	private int transactionVoid;
	private String exchangeTableIndex;
	private Double exchangeTableRate;
	private String messageType;
	private String buttonType;

	public DtoARCashReceipt() {
	}

	public DtoARCashReceipt(ARCashReceipt arCashReceipt) {
		this.cashReceiptNumber = arCashReceipt.getCashReceiptNumber();
		this.batchID = arCashReceipt.getArBatches() != null ? arCashReceipt.getArBatches().getBatchID() : "";
		this.customerNumber = arCashReceipt.getCustomerNumber()!=null?arCashReceipt.getCustomerNumber():"";
		this.customerName = arCashReceipt.getCustomerName()!=null?arCashReceipt.getCustomerName():"";
		this.customerNameArabic = arCashReceipt.getCustomerNameArabic()!=null?arCashReceipt.getCustomerNameArabic():"";
		this.currencyID = arCashReceipt.getCurrencyID();
		this.cashReceiptType = arCashReceipt.getMasterARCashReceiptType() != null
				? arCashReceipt.getMasterARCashReceiptType().getTypeId():0;
		this.cashReceiptAmount = UtilRoundDecimal.roundDecimalValue(arCashReceipt.getCashReceiptAmount());
		this.checkBookId = arCashReceipt.getCheckbookID();
		this.checkNumber="";
		if(UtilRandomKey.isNotBlank(arCashReceipt.getCheckNumber())){
			this.checkNumber = arCashReceipt.getCheckNumber();
		}
		this.creditCardID="";
		this.creditCardNumber="";
		this.creditCardExpireYear=0;
		this.creditCardExpireMonth=0;
		if(UtilRandomKey.isNotBlank( arCashReceipt.getCreditCardID()))
		{
			this.creditCardID = arCashReceipt.getCreditCardID();
			this.creditCardNumber = arCashReceipt.getCreditCardNumber();
			this.creditCardExpireYear = arCashReceipt.getCreditCardExpireYear();
			this.creditCardExpireMonth = arCashReceipt.getCreditCardExpireMonth();
		}
		
		this.cashReceiptDescription = arCashReceipt.getCashReceiptDescription();
		this.cashReceiptCreateDate = arCashReceipt.getCashReceiptCreateDate() != null
				? UtilDateAndTime.dateToStringddmmyyyy(arCashReceipt.getCashReceiptCreateDate()) : "";
		this.cashReceiptPostingDate = arCashReceipt.getCashReceiptPostingDate() != null
				? UtilDateAndTime.dateToStringddmmyyyy(arCashReceipt.getCashReceiptPostingDate()) : "";
		this.transactionVoid = arCashReceipt.getTransactionVoid();
		this.exchangeTableIndex = arCashReceipt.getExchangeTableID()!=null?arCashReceipt.getExchangeTableID():"";
		this.exchangeTableRate = UtilRoundDecimal.roundDecimalValue(arCashReceipt.getExchangeTableRate());

	}

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNameArabic() {
		return customerNameArabic;
	}

	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public Integer getCashReceiptType() {
		return cashReceiptType;
	}

	public void setCashReceiptType(Integer cashReceiptType) {
		this.cashReceiptType = cashReceiptType;
	}

	public Double getCashReceiptAmount() {
		return cashReceiptAmount;
	}

	public void setCashReceiptAmount(Double cashReceiptAmount) {
		this.cashReceiptAmount = UtilRoundDecimal.roundDecimalValue(cashReceiptAmount);
	}

	public String getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Integer getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(Integer creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public Integer getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(Integer creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getCashReceiptDescription() {
		return cashReceiptDescription;
	}

	public void setCashReceiptDescription(String cashReceiptDescription) {
		this.cashReceiptDescription = cashReceiptDescription;
	}

	public String getCashReceiptCreateDate() {
		return cashReceiptCreateDate;
	}

	public void setCashReceiptCreateDate(String cashReceiptCreateDate) {
		this.cashReceiptCreateDate = cashReceiptCreateDate;
	}

	public String getCashReceiptPostingDate() {
		return cashReceiptPostingDate;
	}

	public void setCashReceiptPostingDate(String cashReceiptPostingDate) {
		this.cashReceiptPostingDate = cashReceiptPostingDate;
	}

	public int getTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(int transactionVoid) {
		this.transactionVoid = transactionVoid;
	}
	
	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = UtilRoundDecimal.roundDecimalValue(exchangeTableRate);
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}

}
