/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the gl90400 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl90400")
@NamedQuery(name="GLYTDOpenBankTransactionsEntryHeader.findAll", query="SELECT a FROM GLYTDOpenBankTransactionsEntryHeader a")
public class GLYTDOpenBankTransactionsEntryHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BNKTRXID")
	private String bankTransactionEntryID;
	
	@ManyToOne
	@JoinColumn(name="BAKTRXTYPE")
	private BankTransactionType bankTransactionType;
	
	@ManyToOne
	@JoinColumn(name="BNKTRXTYP")
	private BankTransactionEntryType bankTransactionEntryType;

	@ManyToOne
	@JoinColumn(name="BNKTRXOPT")
	private BankTransactionEntryAction bankTransactionEntryAction;

	@Column(name="BNKTRXDT")
	private Date bankTransactionDate;
	
	@Column(name="CHEKBOKID")
	private String checkbookId;
	
	@Column(name="CURNCYID")
	private String currencyId;
	
	@Column(name="EXGTBLID")
	private String exchangeTableIndex;
	
	@Column(name="XCHGRATE")
	private Double exchangeRate;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	
	@Column(name="CARDEXPTY")
	private int creditCardExpireYear;
	
	@Column(name="CARDEXPTM")
	private int creditCardExpireMonth;
	
	@Column(name="RVRNAME")
	private String receiveName;
	
	@Column(name="BNKDSCR")
	private String bankTransactionDescription;
	
	@Column(name="BNKAMNT")
	private Double bankTransactionAmount;
	
	@Column(name="PSTDDT")
	private Date postingDate;
	
	@Column(name="PSTURS")
	private String postingbyUserID;
	
	@Column(name="BATCHID")
	private String batchID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;
	
	@OneToOne
	@JoinColumn(name="SORCDOC")
	private GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes;

	public BankTransactionType getBankTransactionType() {
		return bankTransactionType;
	}

	public void setBankTransactionType(BankTransactionType bankTransactionType) {
		this.bankTransactionType = bankTransactionType;
	}

	public BankTransactionEntryType getBankTransactionEntryType() {
		return bankTransactionEntryType;
	}

	public void setBankTransactionEntryType(BankTransactionEntryType bankTransactionEntryType) {
		this.bankTransactionEntryType = bankTransactionEntryType;
	}

	public BankTransactionEntryAction getBankTransactionEntryAction() {
		return bankTransactionEntryAction;
	}

	public void setBankTransactionEntryAction(BankTransactionEntryAction bankTransactionEntryAction) {
		this.bankTransactionEntryAction = bankTransactionEntryAction;
	}

	public Date getBankTransactionDate() {
		return bankTransactionDate;
	}

	public void setBankTransactionDate(Date bankTransactionDate) {
		this.bankTransactionDate = bankTransactionDate;
	}
	
	

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getBankTransactionDescription() {
		return bankTransactionDescription;
	}

	public void setBankTransactionDescription(String bankTransactionDescription) {
		this.bankTransactionDescription = bankTransactionDescription;
	}

	public Double getBankTransactionAmount() {
		return bankTransactionAmount;
	}

	public void setBankTransactionAmount(Double bankTransactionAmount) {
		this.bankTransactionAmount = bankTransactionAmount;
	}

	public String getPostingbyUserID() {
		return postingbyUserID;
	}

	public void setPostingbyUserID(String postingbyUserID) {
		this.postingbyUserID = postingbyUserID;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}

	public String getBankTransactionEntryID() {
		return bankTransactionEntryID;
	}

	public void setBankTransactionEntryID(String bankTransactionEntryID) {
		this.bankTransactionEntryID = bankTransactionEntryID;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public String getReceiveName() {
		return receiveName;
	}

	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public GLConfigurationAuditTrialCodes getGlConfigurationAuditTrialCodes() {
		return glConfigurationAuditTrialCodes;
	}

	public void setGlConfigurationAuditTrialCodes(GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes) {
		this.glConfigurationAuditTrialCodes = glConfigurationAuditTrialCodes;
	}
	
	
	
	
}