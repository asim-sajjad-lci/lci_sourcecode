/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl40007 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40007")
@NamedQuery(name="GLBankAccessUsers.findAll", query="SELECT s FROM GLBankAccessUsers s")
public class GLBankAccessUsers implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
 	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CHKUSRID")
	private int setupID;

	@ManyToOne
	@JoinColumn(name="CHEKBOKID")
	private CheckbookMaintenance checkbookMaintenance;

	@Column(name="USRID")
	private String userID;

	@Column(name="FULACCES")
	private int fullAccess;
	
	@Column(name="SATACCES")
	private int saturdayAccess ;
	
	@Column(name="SUNACCES")
	private int sundayAccess;	
	
	@Column(name="MONACCES")
	private int mondayAccess;
	
	@Column(name="TUSACCES")
	private int tuesdayAccess;
	
	@Column(name="WEDACCES")
	private int wednesdayAccess;
	
	@Column(name="THUACCES")
	private int thursdayAccess;
	
	@Column(name="FRDACCES")
	private int fridayAccess;

	@Column(name="CREATDDT")
	private Date createdDate;
	
	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private int changeBy;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	  
	public GLBankAccessUsers() {
	}

	public int getSetupID() {
		return setupID;
	}

	public void setSetupID(int setupID) {
		this.setupID = setupID;
	}

	public CheckbookMaintenance getCheckbookMaintenance() {
		return checkbookMaintenance;
	}

	public void setCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookMaintenance = checkbookMaintenance;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public int getFullAccess() {
		return fullAccess;
	}

	public void setFullAccess(int fullAccess) {
		this.fullAccess = fullAccess;
	}

	public int getSaturdayAccess() {
		return saturdayAccess;
	}

	public void setSaturdayAccess(int saturdayAccess) {
		this.saturdayAccess = saturdayAccess;
	}

	public int getSundayAccess() {
		return sundayAccess;
	}

	public void setSundayAccess(int sundayAccess) {
		this.sundayAccess = sundayAccess;
	}

	public int getMondayAccess() {
		return mondayAccess;
	}

	public void setMondayAccess(int mondayAccess) {
		this.mondayAccess = mondayAccess;
	}

	public int getTuesdayAccess() {
		return tuesdayAccess;
	}

	public void setTuesdayAccess(int tuesdayAccess) {
		this.tuesdayAccess = tuesdayAccess;
	}

	public int getWednesdayAccess() {
		return wednesdayAccess;
	}

	public void setWednesdayAccess(int wednesdayAccess) {
		this.wednesdayAccess = wednesdayAccess;
	}

	public int getThursdayAccess() {
		return thursdayAccess;
	}

	public void setThursdayAccess(int thursdayAccess) {
		this.thursdayAccess = thursdayAccess;
	}

	public int getFridayAccess() {
		return fridayAccess;
	}

	public void setFridayAccess(int fridayAccess) {
		this.fridayAccess = fridayAccess;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	
	
}