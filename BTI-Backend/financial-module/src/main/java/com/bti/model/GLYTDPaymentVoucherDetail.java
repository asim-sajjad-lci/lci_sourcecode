package com.bti.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="gl91101")
@NamedQuery(name="GLYTDPaymentVoucherDetail.findAll",query="select a from GLYTDPaymentVoucherDetail a")
public class GLYTDPaymentVoucherDetail  extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PAYSQEN")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="PAYVOUCHID")
	private GLYTDPaymentVoucherHeader glYTDPaymentVoucherHeader;
	
	@Column(name="ACTROWID")
	private String accountTableRowIndex;
	
	@Column(name="ACCTTYP")
	private int accountType;
	
	@Column(name="TPCLBLNC")
	private int balanceType;
	
	@Column(name="INTERID")
	private String interCompanyIDMulticompanyTransaction;
	
	@Column(name="XCHGRATE")
	private Double exchangeTableRate;
	
	@Column(name="DEBTAMT")
	private Double debitAmount;
	
	@Column(name="ORGDEBTAMT")
	private Double originalDebitAmount;
	
	@Column(name="DISDRCPTN")
	private String distributionDescription;
	
	@Column(name="MODIFDT")
	private Date modityDate;

	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;
	
	@Column(name="CREATDDT")
	private Date createDate;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GLYTDPaymentVoucherHeader getGlYTDPaymentVoucherHeader() {
		return glYTDPaymentVoucherHeader;
	}

	public void setGlYTDPaymentVoucherHeader(GLYTDPaymentVoucherHeader glYTDPaymentVoucherHeader) {
		this.glYTDPaymentVoucherHeader = glYTDPaymentVoucherHeader;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public String getInterCompanyIDMulticompanyTransaction() {
		return interCompanyIDMulticompanyTransaction;
	}

	public void setInterCompanyIDMulticompanyTransaction(String interCompanyIDMulticompanyTransaction) {
		this.interCompanyIDMulticompanyTransaction = interCompanyIDMulticompanyTransaction;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(Double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}

	public Date getModityDate() {
		return modityDate;
	}

	public void setModityDate(Date modityDate) {
		this.modityDate = modityDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
