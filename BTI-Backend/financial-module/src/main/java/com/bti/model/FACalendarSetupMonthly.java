/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the fa40011 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40011")
@NamedQuery(name="FACalendarSetupMonthly.findAll", query="SELECT f FROM FACalendarSetupMonthly f")
public class FACalendarSetupMonthly implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CALENDINDX")
	private int calendarIndex;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	/*@Column(name="ENDTDDT")
	private Date endDate;*/
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name="ENDTDDT")
	private String endDate;

	@Column(name="PERDID")
	private int periodIndex;

	/*@Column(name="STRTDDT")
	private Date startDate;*/
	
	@Column(name="STRTDDT")
	private String startDate;

	@Column(name="YEAR1")
	private int year1;

	//bi-directional one-to-one association to Fa40001
	/*@OneToOne(mappedBy="faCalendarSetupMonthly")
	private FACalendarSetup faCalendarSetup;*/
	
	@ManyToOne
	@JoinColumn(name="CALEND_SETUP")
	private FACalendarSetup faCalendarSetup;


	public FACalendarSetupMonthly() {
	}

	public int getCalendarIndex() {
		return calendarIndex;
	}

	public void setCalendarIndex(int calendarIndex) {
		this.calendarIndex = calendarIndex;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	/*public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
*/
	public int getPeriodIndex() {
		return periodIndex;
	}

	public void setPeriodIndex(int periodIndex) {
		this.periodIndex = periodIndex;
	}

	/*public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}*/

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public int getYear1() {
		return year1;
	}

	public void setYear1(int year1) {
		this.year1 = year1;
	}

	public FACalendarSetup getFaCalendarSetup() {
		return faCalendarSetup;
	}

	public void setFaCalendarSetup(FACalendarSetup faCalendarSetup) {
		this.faCalendarSetup = faCalendarSetup;
	}

	

	
}