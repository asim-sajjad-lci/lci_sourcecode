/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the gl40005 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40005")
@NamedQuery(name="MasterPostingAccountsSequence.findAll", query="SELECT g FROM MasterPostingAccountsSequence g")
public class MasterPostingAccountsSequence implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PSTID")
	private int postingId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PSTACCT")
	private String postingAccountDescription;

	@Column(name="PSTACCTA")
	private String postingAccountDescriptionArabic;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	@ManyToOne
	@JoinColumn(name="SERIES")
	private ModulesConfiguration series;

	//bi-directional many-to-one association to Gl40004
	/*@ManyToOne
	@JoinColumn(name="SERIES")
	private GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetails;
*/
	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;

	public MasterPostingAccountsSequence() {
	}

	public int getPostingId() {
		return postingId;
	}

	public void setPostingId(int postingId) {
		this.postingId = postingId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getPostingAccountDescription() {
		return postingAccountDescription;
	}

	public void setPostingAccountDescription(String postingAccountDescription) {
		this.postingAccountDescription = postingAccountDescription;
	}

	public String getPostingAccountDescriptionArabic() {
		return postingAccountDescriptionArabic;
	}

	public void setPostingAccountDescriptionArabic(String postingAccountDescriptionArabic) {
		this.postingAccountDescriptionArabic = postingAccountDescriptionArabic;
	}

	/*public GLConfigurationDefinePeriodsDetails getGlConfigurationDefinePeriodsDetails() {
		return glConfigurationDefinePeriodsDetails;
	}

	public void setGlConfigurationDefinePeriodsDetails(
			GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetails) {
		this.glConfigurationDefinePeriodsDetails = glConfigurationDefinePeriodsDetails;
	}*/

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public ModulesConfiguration getSeries() {
		return series;
	}

	public void setSeries(ModulesConfiguration series) {
		this.series = series;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
}