/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the `ar00300-header` database table.
 * 
 */
@Entity
@Table(name="ar00300header")
@NamedQuery(name="NationalAccountMaintenanceHeader.findAll", query="SELECT a FROM NationalAccountMaintenanceHeader a")
public class NationalAccountMaintenanceHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@OneToOne
	@JoinColumn(name="CUSTNMBR")
	private CustomerMaintenance customerMaintenance;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="CUSTNAME")
	private String customerName;

	@Column(name="CUSTNAMEA")
	private String customerNameArabic;

	@Column(name="CUSTNMBRCHLD")
	private int customerNumberChildrenIndex;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="OPTALLWRECENT")
	private int allowReceiptsEntrChildrenNationalAccount;

	@Column(name="OPTAPLYHOLD")
	private int applyHoldOrInactiveStatusParentAcrossNationalAccount;

	@Column(name="OPTBASCRDCHK")
	private int baseCreditCheckConsolidatedNationalAccount;

	@Column(name="OPTBASFCHRG")
	private int baseFinanceChargConsolidatedNationalAccount;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional one-to-one association to Ar00101
/*	@OneToOne
	@JoinColumn(name="CUSTNMBR")
	private NationalAccountMaintenanceDetails nationalAccountMaintenanceDetails;*/

	public NationalAccountMaintenanceHeader() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CustomerMaintenance getCustomerMaintenance() {
		return customerMaintenance;
	}

	public void setCustomerMaintenance(CustomerMaintenance customerMaintenance) {
		this.customerMaintenance = customerMaintenance;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNameArabic() {
		return customerNameArabic;
	}

	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}

	public int getCustomerNumberChildrenIndex() {
		return customerNumberChildrenIndex;
	}

	public void setCustomerNumberChildrenIndex(int customerNumberChildrenIndex) {
		this.customerNumberChildrenIndex = customerNumberChildrenIndex;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getAllowReceiptsEntrChildrenNationalAccount() {
		return allowReceiptsEntrChildrenNationalAccount;
	}

	public void setAllowReceiptsEntrChildrenNationalAccount(int allowReceiptsEntrChildrenNationalAccount) {
		this.allowReceiptsEntrChildrenNationalAccount = allowReceiptsEntrChildrenNationalAccount;
	}

	public int getApplyHoldOrInactiveStatusParentAcrossNationalAccount() {
		return applyHoldOrInactiveStatusParentAcrossNationalAccount;
	}

	public void setApplyHoldOrInactiveStatusParentAcrossNationalAccount(
			int applyHoldOrInactiveStatusParentAcrossNationalAccount) {
		this.applyHoldOrInactiveStatusParentAcrossNationalAccount = applyHoldOrInactiveStatusParentAcrossNationalAccount;
	}

	public int getBaseCreditCheckConsolidatedNationalAccount() {
		return baseCreditCheckConsolidatedNationalAccount;
	}

	public void setBaseCreditCheckConsolidatedNationalAccount(int baseCreditCheckConsolidatedNationalAccount) {
		this.baseCreditCheckConsolidatedNationalAccount = baseCreditCheckConsolidatedNationalAccount;
	}

	public int getBaseFinanceChargConsolidatedNationalAccount() {
		return baseFinanceChargConsolidatedNationalAccount;
	}

	public void setBaseFinanceChargConsolidatedNationalAccount(int baseFinanceChargConsolidatedNationalAccount) {
		this.baseFinanceChargConsolidatedNationalAccount = baseFinanceChargConsolidatedNationalAccount;
	}
/*
	public NationalAccountMaintenanceDetails getNationalAccountMaintenanceDetails() {
		return nationalAccountMaintenanceDetails;
	}

	public void setNationalAccountMaintenanceDetails(NationalAccountMaintenanceDetails nationalAccountMaintenanceDetails) {
		this.nationalAccountMaintenanceDetails = nationalAccountMaintenanceDetails;
	}
*/


	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}	
}