/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the gl40004 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40004")
@NamedQuery(name="GLConfigurationDefinePeriodsDetails.findAll", query="SELECT g FROM GLConfigurationDefinePeriodsDetails g")
public class GLConfigurationDefinePeriodsDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SERIES")
	private int series;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PERDENDT")
	private Date endPeriodDate;

	@Column(name="PERIODDT")
	private Date startPeriodDate;

	@Column(name="PERIODID")
	private int periodId;

	@Column(name="PERNAME")
	private String periodName;

	@Column(name="PERNAMEA")
	private String periodNameArabic;

	@Column(name="PSERIES_1")
	private Boolean periodSeriesFinancial;

	@Column(name="PSERIES_2")
	private Boolean periodSeriesSales;

	@Column(name="PSERIES_3")
	private Boolean periodSeriesPurchase;

	@Column(name="PSERIES_4")
	private Boolean periodSeriesInventory;

	@Column(name="PSERIES_5")
	private Boolean periodSeriesProject;

	@Column(name="PSERIES_6")
	private Boolean periodSeriesPayroll;

	@Column(name="SRDESC")
	private String transactionDocumentDescription;

	@Column(name="SRDESCA")
	private String transactionDocumentDescriptionArabic;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Gl40003
	@ManyToOne
	@JoinColumn(name="YEAR1")
	private GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader;

	//bi-directional one-to-one association to Sy40000
	/*@OneToOne
	@JoinColumn(name="SERIES")
	private ModulesConfigurationPeriod modulesConfigurationPeriod;*/

	//bi-directional many-to-one association to Gl40005
	/*@OneToMany(mappedBy="glConfigurationDefinePeriodsDetails")
	private List<MasterPostingAccountsSequence> masterPostingAccountsSequences;*/
	
	//bi-directional many-to-one association to sy40000
		@OneToMany(mappedBy="glConfigurationDefinePeriodsDetails")
		private List<ModulesConfigurationPeriod> modulesConfigurationPeriods;

		
	public List<ModulesConfigurationPeriod> getModulesConfigurationPeriods() {
			return modulesConfigurationPeriods;
		}

		public void setModulesConfigurationPeriods(List<ModulesConfigurationPeriod> modulesConfigurationPeriods) {
			this.modulesConfigurationPeriods = modulesConfigurationPeriods;
		}

	public GLConfigurationDefinePeriodsDetails() {
	}

	public int getSeries() {
		return series;
	}

	public void setSeries(int series) {
		this.series = series;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getEndPeriodDate() {
		return endPeriodDate;
	}

	public void setEndPeriodDate(Date endPeriodDate) {
		this.endPeriodDate = endPeriodDate;
	}

	public Date getStartPeriodDate() {
		return startPeriodDate;
	}

	public void setStartPeriodDate(Date startPeriodDate) {
		this.startPeriodDate = startPeriodDate;
	}

	public int getPeriodId() {
		return periodId;
	}

	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public String getPeriodNameArabic() {
		return periodNameArabic;
	}

	public void setPeriodNameArabic(String periodNameArabic) {
		this.periodNameArabic = periodNameArabic;
	}

	public Boolean getPeriodSeriesFinancial() {
		return periodSeriesFinancial;
	}

	public void setPeriodSeriesFinancial(Boolean periodSeriesFinancial) {
		this.periodSeriesFinancial = periodSeriesFinancial;
	}

	public Boolean getPeriodSeriesSales() {
		return periodSeriesSales;
	}

	public void setPeriodSeriesSales(Boolean periodSeriesSales) {
		this.periodSeriesSales = periodSeriesSales;
	}

	public Boolean getPeriodSeriesPurchase() {
		return periodSeriesPurchase;
	}

	public void setPeriodSeriesPurchase(Boolean periodSeriesPurchase) {
		this.periodSeriesPurchase = periodSeriesPurchase;
	}

	public Boolean getPeriodSeriesInventory() {
		return periodSeriesInventory;
	}

	public void setPeriodSeriesInventory(Boolean periodSeriesInventory) {
		this.periodSeriesInventory = periodSeriesInventory;
	}

	public Boolean getPeriodSeriesProject() {
		return periodSeriesProject;
	}

	public void setPeriodSeriesProject(Boolean periodSeriesProject) {
		this.periodSeriesProject = periodSeriesProject;
	}

	public Boolean getPeriodSeriesPayroll() {
		return periodSeriesPayroll;
	}

	public void setPeriodSeriesPayroll(Boolean periodSeriesPayroll) {
		this.periodSeriesPayroll = periodSeriesPayroll;
	}

	public String getTransactionDocumentDescription() {
		return transactionDocumentDescription;
	}

	public void setTransactionDocumentDescription(String transactionDocumentDescription) {
		this.transactionDocumentDescription = transactionDocumentDescription;
	}

	public String getTransactionDocumentDescriptionArabic() {
		return transactionDocumentDescriptionArabic;
	}

	public void setTransactionDocumentDescriptionArabic(String transactionDocumentDescriptionArabic) {
		this.transactionDocumentDescriptionArabic = transactionDocumentDescriptionArabic;
	}

	public GLConfigurationDefinePeriodsHeader getGlConfigurationDefinePeriodsHeader() {
		return glConfigurationDefinePeriodsHeader;
	}

	public void setGlConfigurationDefinePeriodsHeader(
			GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader) {
		this.glConfigurationDefinePeriodsHeader = glConfigurationDefinePeriodsHeader;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

/*	public ModulesConfigurationPeriod getModulesConfigurationPeriod() {
		return modulesConfigurationPeriod;
	}

	public void setModulesConfigurationPeriod(ModulesConfigurationPeriod modulesConfigurationPeriod) {
		this.modulesConfigurationPeriod = modulesConfigurationPeriod;
	}
*/
/*	public List<MasterPostingAccountsSequence> getMasterPostingAccountsSequences() {
		return masterPostingAccountsSequences;
	}

	public void setMasterPostingAccountsSequences(List<MasterPostingAccountsSequence> masterPostingAccountsSequences) {
		this.masterPostingAccountsSequences = masterPostingAccountsSequences;
	}
*/
	 
	
	
}