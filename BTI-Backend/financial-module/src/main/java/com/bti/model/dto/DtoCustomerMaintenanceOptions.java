/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.CustomerClassesSetup;
import com.bti.model.CustomerMaintenance;
import com.bti.model.CustomerMaintenanceOptions;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoCustomerMaintenanceOptions {
	
	private String customerNumberId;

	private Integer balanceType;

	private String cardExpirationDate;

	private String cardNumber;

	private Integer creditLimit;

	private String creditLimitAmount;

	private String currencyId;
	
	private Integer customerPriority;

	private Integer financeCharge;
	
	private String financeChargeAmount;

	private Integer minimumCharge;

	private String minimumChargeAmount;

	private Integer openMaintenanceHistoryCalendarYear;

	private Integer openMaintenanceHistoryDistribution;

	private Integer openMaintenanceHistoryFiscalYear;

	private Integer openMaintenanceHistoryTransaction;

	private String priceLevel;

	private String tradeDiscountPercent;

	private String customerMaintenanceId;

	private String customerAccountTableSetupId;

	private String salesmanId;

	private String shipmentMethodId;

	private String paymentTermId;

	private String vatId;

	private String salesTerritoryId;
	private String checkbookId;
	private String creditCardsSetupId;
	private String messageType;
	
	public DtoCustomerMaintenanceOptions() {
		
	}
	
	public DtoCustomerMaintenanceOptions(CustomerMaintenanceOptions customerMaintenanceOptions) {
		this.customerNumberId=customerMaintenanceOptions.getCustomerMaintenance().getCustnumber();
		this.balanceType=0;
		if(customerMaintenanceOptions.getMasterCustomerClassSetupBalanceType()!=null){
			this.balanceType=customerMaintenanceOptions.getMasterCustomerClassSetupBalanceType().getTypeId();
		}
		this.checkbookId="";
		if(customerMaintenanceOptions.getCheckbookMaintenance()!=null){
			this.checkbookId=customerMaintenanceOptions.getCheckbookMaintenance().getCheckBookId();
		}
		this.creditLimit=0;
		if(customerMaintenanceOptions.getMasterCustomerClassSetupCreditLimit()!=null){
			this.creditLimit=customerMaintenanceOptions.getMasterCustomerClassSetupCreditLimit().getTypeId();
		}
		this.creditLimitAmount=String.valueOf(UtilRoundDecimal.roundDecimalValue(customerMaintenanceOptions.getCreditLimitAmount()));
		this.currencyId="";
		if(customerMaintenanceOptions.getCurrencySetup()!=null){
			this.currencyId=customerMaintenanceOptions.getCurrencySetup().getCurrencyId();
		}
		
		this.customerPriority=customerMaintenanceOptions.getCustomerPriority();
		this.financeCharge=0;
		if(customerMaintenanceOptions.getMasterCustomerClassSetupFinanaceCharge()!=null){
			this.financeCharge=customerMaintenanceOptions.getMasterCustomerClassSetupFinanaceCharge().getTypeId();
		}
		
		
			this.financeChargeAmount = String.valueOf(UtilRoundDecimal.roundDecimalValue(customerMaintenanceOptions.getFinanceChargeAmount()));
		this.minimumCharge=0;
		if(customerMaintenanceOptions.getMasterCustomerClassMinimumCharge()!=null){
			this.minimumCharge=customerMaintenanceOptions.getMasterCustomerClassMinimumCharge().getTypeId();
		}
		this.minimumChargeAmount=String.valueOf(UtilRoundDecimal.roundDecimalValue(customerMaintenanceOptions.getMinimumChargeAmount()));
		this.openMaintenanceHistoryCalendarYear=customerMaintenanceOptions.getOpenMaintenanceHistoryCalendarYear();
		this.openMaintenanceHistoryDistribution=customerMaintenanceOptions.getOpenMaintenanceHistoryDistribution();
		this.openMaintenanceHistoryFiscalYear=customerMaintenanceOptions.getOpenMaintenanceHistoryFiscalYear();
		this.openMaintenanceHistoryTransaction=customerMaintenanceOptions.getOpenMaintenanceHistoryTransaction();
		this.paymentTermId="";
		if(customerMaintenanceOptions.getPaymentTermsSetup()!=null){
			this.paymentTermId=customerMaintenanceOptions.getPaymentTermsSetup().getPaymentTermId();
		}
		this.priceLevel="";
		if(customerMaintenanceOptions.getPriceLevelSetup()!=null){
			this.priceLevel=String.valueOf(customerMaintenanceOptions.getPriceLevelSetup().getPriceLevelIndex());
		}
			
		
		this.salesmanId="";
		if(customerMaintenanceOptions.getSalesmanMaintenance()!=null){
			this.salesmanId=customerMaintenanceOptions.getSalesmanMaintenance().getSalesmanId();
		}
		this.salesTerritoryId="";
		if(customerMaintenanceOptions.getSalesTerritoryMaintenance()!=null){
			this.salesTerritoryId=customerMaintenanceOptions.getSalesTerritoryMaintenance().getSalesTerritoryId();
		}
		this.shipmentMethodId="";
		if(customerMaintenanceOptions.getShipmentMethodSetup()!=null){
			this.shipmentMethodId=customerMaintenanceOptions.getShipmentMethodSetup().getShipmentMethodId();
		}
		this.tradeDiscountPercent=String.valueOf(UtilRoundDecimal.roundDecimalValue(customerMaintenanceOptions.getTradeDiscountPercent()));
		this.vatId="";
		if(customerMaintenanceOptions.getVatSetup()!=null){
			this.vatId=customerMaintenanceOptions.getVatSetup().getVatScheduleId();
		}
	}

	public DtoCustomerMaintenanceOptions(CustomerMaintenance customerMaintenance) {
	
		CustomerClassesSetup customerClassesSetup= customerMaintenance.getCustomerClassesSetup();
		this.customerNumberId=customerMaintenance.getCustnumber();
		this.balanceType=0;
		if(customerClassesSetup.getMasterCustomerClassSetupBalanceType()!=null){
			this.balanceType=customerClassesSetup.getMasterCustomerClassSetupBalanceType().getTypeId();
		}
		this.checkbookId="";
		if(customerClassesSetup.getCheckbookMaintenance()!=null){
			this.checkbookId=customerClassesSetup.getCheckbookMaintenance().getCheckBookId();
		}
		this.creditLimit=0;
		if(customerClassesSetup.getMasterCustomerClassSetupCreditLimit()!=null){
			this.creditLimit=customerClassesSetup.getMasterCustomerClassSetupCreditLimit().getTypeId();
		}
		
		this.creditLimitAmount=String.valueOf(UtilRoundDecimal.roundDecimalValue(customerClassesSetup.getCreditLimitAmount()));
		this.currencyId="";
		if(customerClassesSetup.getCurrencySetup()!=null){
			this.currencyId=customerClassesSetup.getCurrencySetup().getCurrencyId();
		}
		this.customerPriority=customerClassesSetup.getCustomerPriority();
		this.financeCharge=0;
		if(customerClassesSetup.getMasterCustomerClassSetupFinanaceCharge()!=null){
			this.financeCharge=customerClassesSetup.getMasterCustomerClassSetupFinanaceCharge().getTypeId();
		}
		this.financeChargeAmount = String.valueOf(UtilRoundDecimal.roundDecimalValue(customerClassesSetup.getFinanceChargeAmount()));
		
		this.minimumCharge=0;
		if(customerClassesSetup.getMasterCustomerClassMinimumCharge()!=null){
			this.minimumCharge=customerClassesSetup.getMasterCustomerClassMinimumCharge().getTypeId();
		}
		this.minimumChargeAmount=String.valueOf(UtilRoundDecimal.roundDecimalValue(customerClassesSetup.getMinimumChargeAmount()));
		this.openMaintenanceHistoryCalendarYear=customerClassesSetup.getOpenMaintenanceHistoryCalendarYear();
		this.openMaintenanceHistoryDistribution=customerClassesSetup.getOpenMaintenanceHistoryDistribution();
		this.openMaintenanceHistoryFiscalYear=customerClassesSetup.getOpenMaintenanceHistoryFiscalYear();
		this.openMaintenanceHistoryTransaction=customerClassesSetup.getOpenMaintenanceHistoryTransaction();
		this.paymentTermId="";
		if(customerClassesSetup.getPaymentTermsSetup()!=null){
			this.paymentTermId=customerClassesSetup.getPaymentTermsSetup().getPaymentTermId();
		}
		this.priceLevel="";
		if(customerClassesSetup.getPriceLevelId()!=null){
			this.priceLevel=String.valueOf(customerClassesSetup.getPriceLevelId());
		}
			
		
		this.salesmanId="";
		if(customerClassesSetup.getSalesmanMaintenance()!=null){
			this.salesmanId=customerClassesSetup.getSalesmanMaintenance().getSalesmanId();
		}
		this.salesTerritoryId="";
		if(customerClassesSetup.getSalesTerritoryMaintenance()!=null){
			this.salesTerritoryId=customerClassesSetup.getSalesTerritoryMaintenance().getSalesTerritoryId();
		}
		this.shipmentMethodId="";
		if(customerClassesSetup.getShipmentMethodSetup()!=null){
			this.shipmentMethodId=customerClassesSetup.getShipmentMethodSetup().getShipmentMethodId();
		}
		
		this.tradeDiscountPercent=String.valueOf(UtilRoundDecimal.roundDecimalValue(customerClassesSetup.getTradeDiscountPercent()));
		this.vatId="";
		if(customerClassesSetup.getVatSetup()!=null){
			this.vatId=customerClassesSetup.getVatSetup().getVatScheduleId();
		}
	}

	public String getCustomerNumberId() {
		return customerNumberId;
	}

	public void setCustomerNumberId(String customerNumberId) {
		this.customerNumberId = customerNumberId;
	}

	public Integer getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(Integer balanceType) {
		this.balanceType = balanceType;
	}

	public String getCardExpirationDate() {
		return cardExpirationDate;
	}

	public void setCardExpirationDate(String cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Integer creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public Integer getCustomerPriority() {
		return customerPriority;
	}

	public void setCustomerPriority(Integer customerPriority) {
		this.customerPriority = customerPriority;
	}

	public Integer getFinanceCharge() {
		return financeCharge;
	}

	public void setFinanceCharge(Integer financeCharge) {
		this.financeCharge = financeCharge;
	}

	public String getFinanceChargeAmount() {
		return financeChargeAmount;
	}

	public void setFinanceChargeAmount(String financeChargeAmount) {
		this.financeChargeAmount = financeChargeAmount;
	}

	public Integer getMinimumCharge() {
		return minimumCharge;
	}

	public void setMinimumCharge(Integer minimumCharge) {
		this.minimumCharge = minimumCharge;
	}

	public String getMinimumChargeAmount() {
		return minimumChargeAmount;
	}

	public void setMinimumChargeAmount(String minimumChargeAmount) {
		this.minimumChargeAmount = minimumChargeAmount;
	}

	public Integer getOpenMaintenanceHistoryCalendarYear() {
		return openMaintenanceHistoryCalendarYear;
	}

	public void setOpenMaintenanceHistoryCalendarYear(Integer openMaintenanceHistoryCalendarYear) {
		this.openMaintenanceHistoryCalendarYear = openMaintenanceHistoryCalendarYear;
	}

	public Integer getOpenMaintenanceHistoryDistribution() {
		return openMaintenanceHistoryDistribution;
	}

	public void setOpenMaintenanceHistoryDistribution(Integer openMaintenanceHistoryDistribution) {
		this.openMaintenanceHistoryDistribution = openMaintenanceHistoryDistribution;
	}

	public Integer getOpenMaintenanceHistoryFiscalYear() {
		return openMaintenanceHistoryFiscalYear;
	}

	public void setOpenMaintenanceHistoryFiscalYear(Integer openMaintenanceHistoryFiscalYear) {
		this.openMaintenanceHistoryFiscalYear = openMaintenanceHistoryFiscalYear;
	}

	public Integer getOpenMaintenanceHistoryTransaction() {
		return openMaintenanceHistoryTransaction;
	}

	public void setOpenMaintenanceHistoryTransaction(Integer openMaintenanceHistoryTransaction) {
		this.openMaintenanceHistoryTransaction = openMaintenanceHistoryTransaction;
	}

	public String getPriceLevel() {
		return priceLevel;
	}

	public void setPriceLevel(String priceLevel) {
		this.priceLevel = priceLevel;
	}

	public String getTradeDiscountPercent() {
		return tradeDiscountPercent;
	}

	public void setTradeDiscountPercent(String tradeDiscountPercent) {
		this.tradeDiscountPercent = tradeDiscountPercent;
	}

	public String getCustomerMaintenanceId() {
		return customerMaintenanceId;
	}

	public void setCustomerMaintenanceId(String customerMaintenanceId) {
		this.customerMaintenanceId = customerMaintenanceId;
	}

	public String getCustomerAccountTableSetupId() {
		return customerAccountTableSetupId;
	}

	public void setCustomerAccountTableSetupId(String customerAccountTableSetupId) {
		this.customerAccountTableSetupId = customerAccountTableSetupId;
	}

	public String getCreditLimitAmount() {
		return creditLimitAmount;
	}

	public void setCreditLimitAmount(String creditLimitAmount) {
		this.creditLimitAmount = creditLimitAmount;
	}

	public String getSalesmanId() {
		return salesmanId;
	}

	public void setSalesmanId(String salesmanId) {
		this.salesmanId = salesmanId;
	}

	public String getShipmentMethodId() {
		return shipmentMethodId;
	}

	public void setShipmentMethodId(String shipmentMethodId) {
		this.shipmentMethodId = shipmentMethodId;
	}

	public String getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(String paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public String getVatId() {
		return vatId;
	}

	public void setVatId(String vatId) {
		this.vatId = vatId;
	}

	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}

	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getCreditCardsSetupId() {
		return creditCardsSetupId;
	}

	public void setCreditCardsSetupId(String creditCardsSetupId) {
		this.creditCardsSetupId = creditCardsSetupId;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
}
