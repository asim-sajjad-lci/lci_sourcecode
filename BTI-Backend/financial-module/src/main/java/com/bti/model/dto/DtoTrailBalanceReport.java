/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DtoGLReportHeader class having getter and setter for fields
 * (POJO) Name Name of Project: BTI Created on: JAN 10, 2018 Modified on: JAN
 * 10, 2018 4:19:38 PM
 * 
 * @author seasia Version:
 */

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoTrailBalanceReport {

	private String accountNumber;
	private String startDate;
	private String endDate;
	private String inActive;
	private String accountDescription;
	private String accountDescriptionArabic;
	private Double beginningBalance;
	private Double endingBalance;
	private Double debit;
	private Double credit;
	private Double netChange;
	private Double totalBeginningBalance;
	private Double totalEndingBalance;
	private Double totalDebit;
	private Double totalCredit;
	private Double totalNetChange;
	private Integer totalAccounts;
	List<DtoTrailBalanceReport> trailBalanceReportList;
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getInActive() {
		return inActive;
	}
	public void setInActive(String inActive) {
		this.inActive = inActive;
	}
	public String getAccountDescription() {
		return accountDescription;
	}
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	
	public String getAccountDescriptionArabic() {
		return accountDescriptionArabic;
	}
	public void setAccountDescriptionArabic(String accountDescriptionArabic) {
		this.accountDescriptionArabic = accountDescriptionArabic;
	}
	public Double getBeginningBalance() {
		return beginningBalance;
	}
	public void setBeginningBalance(Double beginningBalance) {
		this.beginningBalance = UtilRoundDecimal.roundDecimalValue(beginningBalance);
	}
	public Double getEndingBalance() {
		return endingBalance;
	}
	public void setEndingBalance(Double endingBalance) {
		this.endingBalance = UtilRoundDecimal.roundDecimalValue(endingBalance);
	}
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = UtilRoundDecimal.roundDecimalValue(debit);
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = UtilRoundDecimal.roundDecimalValue(credit);
	}
	public Double getNetChange() {
		return netChange;
	}
	public void setNetChange(Double netChange) {
		this.netChange = UtilRoundDecimal.roundDecimalValue(netChange);
	}
	public Double getTotalBeginningBalance() {
		return totalBeginningBalance;
	}
	public void setTotalBeginningBalance(Double totalBeginningBalance) {
		this.totalBeginningBalance = UtilRoundDecimal.roundDecimalValue(totalBeginningBalance);
	}
	public Double getTotalEndingBalance() {
		return totalEndingBalance;
	}
	public void setTotalEndingBalance(Double totalEndingBalance) {
		this.totalEndingBalance = UtilRoundDecimal.roundDecimalValue(totalEndingBalance);
	}
	public Double getTotalDebit() {
		return totalDebit;
	}
	public void setTotalDebit(Double totalDebit) {
		this.totalDebit = UtilRoundDecimal.roundDecimalValue(totalDebit);
	}
	public Double getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(Double totalCredit) {
		this.totalCredit = UtilRoundDecimal.roundDecimalValue(totalCredit);
	}
	public Double getTotalNetChange() {
		return totalNetChange;
	}
	public void setTotalNetChange(Double totalNetChange) {
		this.totalNetChange = UtilRoundDecimal.roundDecimalValue(totalNetChange);
	}
	public Integer getTotalAccounts() {
		return totalAccounts;
	}
	public void setTotalAccounts(Integer totalAccounts) {
		this.totalAccounts = totalAccounts;
	}
	public List<DtoTrailBalanceReport> getTrailBalanceReportList() {
		return trailBalanceReportList;
	}
	public void setTrailBalanceReportList(List<DtoTrailBalanceReport> trailBalanceReportList) {
		this.trailBalanceReportList = trailBalanceReportList;
	}
	
	 
}
