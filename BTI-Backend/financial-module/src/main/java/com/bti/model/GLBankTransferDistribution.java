/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10502 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10502")
@NamedQuery(name="GLBankTransferDistribution.findAll", query="SELECT a FROM GLBankTransferDistribution  a")
public class GLBankTransferDistribution implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BNKTRASEQN")
	private int bankTransferAccountSequence;
	
	@ManyToOne
	@JoinColumn(name="BNKTRNSF")
	private GLBankTransferHeader glBankTransferHeader;
	
	@Column(name="ACTROWID")
	private String accountTableRowIndex;

	@Column(name="ACTTYP")
	private int accountType ; /* it will provide later*/	
	
	@Column(name="DEBITAMT")
	private double debitAmount;

	@Column(name="ORGDEBITAMT")
	private double originalDebitAmount;
	
	@Column(name="CRDTAMT")
	private double creditAmount;
	
	@Column(name="ORGCREDTAMT")
	private double originalCreditAmount;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	public int getBankTransferAccountSequence() {
		return bankTransferAccountSequence;
	}

	public void setBankTransferAccountSequence(int bankTransferAccountSequence) {
		this.bankTransferAccountSequence = bankTransferAccountSequence;
	}

	public GLBankTransferHeader getGlBankTransferHeader() {
		return glBankTransferHeader;
	}

	public void setGlBankTransferHeader(GLBankTransferHeader glBankTransferHeader) {
		this.glBankTransferHeader = glBankTransferHeader;
	}

	

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public double getOriginalCreditAmount() {
		return originalCreditAmount;
	}

	public void setOriginalCreditAmount(double originalCreditAmount) {
		this.originalCreditAmount = originalCreditAmount;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}
	

	
	
}