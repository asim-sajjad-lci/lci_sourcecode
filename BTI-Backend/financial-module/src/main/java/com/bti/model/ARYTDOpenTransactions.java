/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar90200 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar90200")
@NamedQuery(name="ARYTDOpenTransactions.findAll", query="SELECT a FROM ARYTDOpenTransactions a")
public class ARYTDOpenTransactions implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ARTRXNO")
	private String arTransactionNumber;
	
	@Column(name="CUSTNMBR")
	private String customerID;
	
	@Column(name="ARTRXTYP")
	private int arTransactionType;

	@Column(name="DSCRPTN")
	private String arTransactionDescription;

	@Column(name="BATCHID")
	private String batchID;
	
	@Column(name="ARTRXDT")
	private Date arTransactionDate;
	//
	@Column(name="CUSTNAME")
	private String customerName;
	
	@Column(name="CUSTNAMEA")
	private String customerNameArabic;
	//
	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="PYMTRMID")
	private String paymentTermsID;
	
	@Column(name="ARCSTAMT")
	private Double aRTransactionCost;
	
	@Column(name="ARSLSAMT")
	private Double arTransactionSalesAmount;
	
	@Column(name="ARTRDAMT")
	private Double arTransactionTradeDiscount;
	
	@Column(name="ARFRGAMT")
	private Double arTransactionFreightAmount;
	
	@Column(name="ARMISCAMT")
	private Double arTransactionMiscellaneous;
	
	@Column(name="ARVATAMT")
	private Double arTransactionVATAmount;
	
	@Column(name="ARDBAMT")
	private Double arTransactionDebitMemoAmount;
	
	@Column(name="ARFRCAMT")
	private Double arTransactionFinanceChargeAmount;
	
	@Column(name="ARWARAMT")
	private Double arTransactionWarrantyAmount;
	
	@Column(name="ARCRAMT")
	private Double arTransactionCreditMemoAmount;
	
	@Column(name="ARTOTAMT")
	private Double arTransactionTotalAmount;
	
	@Column(name="ARCSHAMT")
	private Double arTransactionCashAmount;
	
	@Column(name="ARCHKAMT")
	private Double arTransactionCheckAmount;
	
	@Column(name="ARCRDAMT")
	private Double arTransactionCreditCardAmount;
	
	@Column(name="SALSPERID")
	private String salesmanID;
	
	@Column(name="CHEKBOKID")
	private String checkbookID;
	
	@Column(name="CSHRECNO")
	private String cashReceiptNumber;
	
	@Column(name="CHKNUMBR")
	private String checkNumber;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	
	////
	@Column(name="CARDEXPTY")
	private int creditCardExpireYear;
	
	@Column(name="CARDEXPTM")
	private int creditCardExpireMonth;
	
	@Column(name="TRXSTAT")
	private int arTransactionStatus;
	
	@Column(name="EXGTBLINXD")
	private int exchangeTableIndex;
	
	@Column(name="XCHGRATE")
	private Double exchangeTableRate;
	
	@Column(name="TRXVOID")
	private Boolean transactionVoid;
	
	@Column(name="SHIPMTHD")
	private String shippingMethodID;
	
	@Column(name="TAXSCHDID")
	private String vATScheduleID;
	
	@Column(name="PSTTRXDDT")
	private Date postingDate;
	
	@Column(name="PSTTRXURS")
	private String postingbyUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;
	
	@Column(name="SALSTERRID")
	private String salesTerritoryId;
	
	@Column(name="COPPMY")
	private Boolean completePaymentApplied;
	
	@Column(name="IS_PAYMENT")
	private boolean isPayment;
	
	@Column(name="ARSERREPAMT")
	private double arServiceRepairAmount;
	
	@Column(name="ARRETNAMT")
	private double arReturnAmount;

	public String getArTransactionNumber() {
		return arTransactionNumber;
	}

	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public int getArTransactionType() {
		return arTransactionType;
	}

	public void setArTransactionType(int arTransactionType) {
		this.arTransactionType = arTransactionType;
	}

	public String getArTransactionDescription() {
		return arTransactionDescription;
	}

	public void setArTransactionDescription(String arTransactionDescription) {
		this.arTransactionDescription = arTransactionDescription;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public Date getArTransactionDate() {
		return arTransactionDate;
	}

	public void setArTransactionDate(Date arTransactionDate) {
		this.arTransactionDate = arTransactionDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNameArabic() {
		return customerNameArabic;
	}

	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getPaymentTermsID() {
		return paymentTermsID;
	}

	public void setPaymentTermsID(String paymentTermsID) {
		this.paymentTermsID = paymentTermsID;
	}

	public Double getaRTransactionCost() {
		return aRTransactionCost;
	}

	public void setaRTransactionCost(Double aRTransactionCost) {
		this.aRTransactionCost = aRTransactionCost;
	}

	public Double getArTransactionSalesAmount() {
		return arTransactionSalesAmount;
	}

	public void setArTransactionSalesAmount(Double arTransactionSalesAmount) {
		this.arTransactionSalesAmount = arTransactionSalesAmount;
	}

	public Double getArTransactionTradeDiscount() {
		return arTransactionTradeDiscount;
	}

	public void setArTransactionTradeDiscount(Double arTransactionTradeDiscount) {
		this.arTransactionTradeDiscount = arTransactionTradeDiscount;
	}

	public Double getArTransactionFreightAmount() {
		return arTransactionFreightAmount;
	}

	public void setArTransactionFreightAmount(Double arTransactionFreightAmount) {
		this.arTransactionFreightAmount = arTransactionFreightAmount;
	}

	public Double getArTransactionMiscellaneous() {
		return arTransactionMiscellaneous;
	}

	public void setArTransactionMiscellaneous(Double arTransactionMiscellaneous) {
		this.arTransactionMiscellaneous = arTransactionMiscellaneous;
	}

	public Double getArTransactionVATAmount() {
		return arTransactionVATAmount;
	}

	public void setArTransactionVATAmount(Double arTransactionVATAmount) {
		this.arTransactionVATAmount = arTransactionVATAmount;
	}

	public Double getArTransactionDebitMemoAmount() {
		return arTransactionDebitMemoAmount;
	}

	public void setArTransactionDebitMemoAmount(Double arTransactionDebitMemoAmount) {
		this.arTransactionDebitMemoAmount = arTransactionDebitMemoAmount;
	}

	public Double getArTransactionFinanceChargeAmount() {
		return arTransactionFinanceChargeAmount;
	}

	public void setArTransactionFinanceChargeAmount(Double arTransactionFinanceChargeAmount) {
		this.arTransactionFinanceChargeAmount = arTransactionFinanceChargeAmount;
	}

	public Double getArTransactionWarrantyAmount() {
		return arTransactionWarrantyAmount;
	}

	public void setArTransactionWarrantyAmount(Double arTransactionWarrantyAmount) {
		this.arTransactionWarrantyAmount = arTransactionWarrantyAmount;
	}

	public Double getArTransactionCreditMemoAmount() {
		return arTransactionCreditMemoAmount;
	}

	public void setArTransactionCreditMemoAmount(Double arTransactionCreditMemoAmount) {
		this.arTransactionCreditMemoAmount = arTransactionCreditMemoAmount;
	}

	public Double getArTransactionTotalAmount() {
		return arTransactionTotalAmount;
	}

	public void setArTransactionTotalAmount(Double arTransactionTotalAmount) {
		this.arTransactionTotalAmount = arTransactionTotalAmount;
	}

	public Double getArTransactionCashAmount() {
		return arTransactionCashAmount;
	}

	public void setArTransactionCashAmount(Double arTransactionCashAmount) {
		this.arTransactionCashAmount = arTransactionCashAmount;
	}

	public Double getArTransactionCheckAmount() {
		return arTransactionCheckAmount;
	}

	public void setArTransactionCheckAmount(Double arTransactionCheckAmount) {
		this.arTransactionCheckAmount = arTransactionCheckAmount;
	}

	public Double getArTransactionCreditCardAmount() {
		return arTransactionCreditCardAmount;
	}

	public void setArTransactionCreditCardAmount(Double arTransactionCreditCardAmount) {
		this.arTransactionCreditCardAmount = arTransactionCreditCardAmount;
	}

	public String getSalesmanID() {
		return salesmanID;
	}

	public void setSalesmanID(String salesmanID) {
		this.salesmanID = salesmanID;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public int getArTransactionStatus() {
		return arTransactionStatus;
	}

	public void setArTransactionStatus(int arTransactionStatus) {
		this.arTransactionStatus = arTransactionStatus;
	}

	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public Boolean getTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(Boolean transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getShippingMethodID() {
		return shippingMethodID;
	}

	public void setShippingMethodID(String shippingMethodID) {
		this.shippingMethodID = shippingMethodID;
	}

	public String getvATScheduleID() {
		return vATScheduleID;
	}

	public void setvATScheduleID(String vATScheduleID) {
		this.vATScheduleID = vATScheduleID;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public String getPostingbyUserID() {
		return postingbyUserID;
	}

	public void setPostingbyUserID(String postingbyUserID) {
		this.postingbyUserID = postingbyUserID;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}

	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}

	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}

	public Boolean getCompletePaymentApplied() {
		return completePaymentApplied;
	}

	public void setCompletePaymentApplied(Boolean completePaymentApplied) {
		this.completePaymentApplied = completePaymentApplied;
	}

	public boolean isPayment() {
		return isPayment;
	}

	public void setPayment(boolean isPayment) {
		this.isPayment = isPayment;
	}

	public double getArServiceRepairAmount() {
		return arServiceRepairAmount;
	}

	public void setArServiceRepairAmount(double arServiceRepairAmount) {
		this.arServiceRepairAmount = arServiceRepairAmount;
	}

	public double getArReturnAmount() {
		return arReturnAmount;
	}

	public void setArReturnAmount(double arReturnAmount) {
		this.arReturnAmount = arReturnAmount;
	}
	
	}