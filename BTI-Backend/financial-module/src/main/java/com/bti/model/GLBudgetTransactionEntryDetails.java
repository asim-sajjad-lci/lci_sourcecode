/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10701 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10701")
@NamedQuery(name="GLBudgetTransactionEntryDetails.findAll", query="SELECT a FROM GLBudgetTransactionEntryDetails a")
public class GLBudgetTransactionEntryDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BUDTRSQCN")
	private int sequenceRow;
	
	@ManyToOne
	@JoinColumn(name="BUDTRXID")
	private GLBudgetTransactionEntryHeader glBudgetTransactionEntryHeader;
	
	@Column(name="PERINDX")
	private int periodIndex ;

	@Column(name="PERDT")
	private Date periodDate;
	
	@Column(name="CURBUGAMT")
	private double currentBudgetAmount;

	@Column(name="ADJBUGAMT")
	private double adjustmentBudgetAmount;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	
	public int getSequenceRow() {
		return sequenceRow;
	}

	public void setSequenceRow(int sequenceRow) {
		this.sequenceRow = sequenceRow;
	}

	public GLBudgetTransactionEntryHeader getGlBudgetTransactionEntryHeader() {
		return glBudgetTransactionEntryHeader;
	}

	public void setGlBudgetTransactionEntryHeader(GLBudgetTransactionEntryHeader glBudgetTransactionEntryHeader) {
		this.glBudgetTransactionEntryHeader = glBudgetTransactionEntryHeader;
	}

	public int getPeriodIndex() {
		return periodIndex;
	}

	public void setPeriodIndex(int periodIndex) {
		this.periodIndex = periodIndex;
	}

	public Date getPeriodDate() {
		return periodDate;
	}

	public void setPeriodDate(Date periodDate) {
		this.periodDate = periodDate;
	}

	public double getCurrentBudgetAmount() {
		return currentBudgetAmount;
	}

	public void setCurrentBudgetAmount(double currentBudgetAmount) {
		this.currentBudgetAmount = currentBudgetAmount;
	}

	public double getAdjustmentBudgetAmount() {
		return adjustmentBudgetAmount;
	}

	public void setAdjustmentBudgetAmount(double adjustmentBudgetAmount) {
		this.adjustmentBudgetAmount = adjustmentBudgetAmount;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}                

	
	
	
	
}