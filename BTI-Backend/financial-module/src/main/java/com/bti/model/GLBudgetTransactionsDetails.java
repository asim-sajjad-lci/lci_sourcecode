/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10901 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10901")
@NamedQuery(name=" GLBudgetTransactionsDetails.findAll", query="SELECT a FROM  GLBudgetTransactionsDetails a")
public class GLBudgetTransactionsDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BDGTRXSEQN")
	private int budgetTransactionSequence;
	
	@ManyToOne
	@JoinColumn(name="BDGTRXID")
	private GLBudgetTransactionHeader glBudgetTransactionHeader;
	
	@ManyToOne
	@JoinColumn(name="BUDGTID")
	private GLBudgetMaintenance glBudgetMaintenance;

	@Column(name="PERIOD")
	private int periodID;
	
	@Column(name="PERIODDT")
	private Date periodDate;

	@Column(name="ACTINDX")
	private int mainAccountIndex;
	
	@Column(name="BDGAMNT")
	private double actualBudgetAmount;
	
	@Column(name="BDGAMNTA")
	private double adjustBudgetAmount;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	
	public int getBudgetTransactionSequence() {
		return budgetTransactionSequence;
	}

	public void setBudgetTransactionSequence(int budgetTransactionSequence) {
		this.budgetTransactionSequence = budgetTransactionSequence;
	}

	public GLBudgetTransactionHeader getGlBudgetTransactionHeader() {
		return glBudgetTransactionHeader;
	}

	public void setGlBudgetTransactionHeader(GLBudgetTransactionHeader glBudgetTransactionHeader) {
		this.glBudgetTransactionHeader = glBudgetTransactionHeader;
	}

	public GLBudgetMaintenance getGlBudgetMaintenance() {
		return glBudgetMaintenance;
	}

	public void setGlBudgetMaintenance(GLBudgetMaintenance glBudgetMaintenance) {
		this.glBudgetMaintenance = glBudgetMaintenance;
	}

	public int getPeriodID() {
		return periodID;
	}

	public void setPeriodID(int periodID) {
		this.periodID = periodID;
	}

	public Date getPeriodDate() {
		return periodDate;
	}

	public void setPeriodDate(Date periodDate) {
		this.periodDate = periodDate;
	}

	public int getMainAccountIndex() {
		return mainAccountIndex;
	}

	public void setMainAccountIndex(int mainAccountIndex) {
		this.mainAccountIndex = mainAccountIndex;
	}

	public double getActualBudgetAmount() {
		return actualBudgetAmount;
	}

	public void setActualBudgetAmount(double actualBudgetAmount) {
		this.actualBudgetAmount = actualBudgetAmount;
	}

	public double getAdjustBudgetAmount() {
		return adjustBudgetAmount;
	}

	public void setAdjustBudgetAmount(double adjustBudgetAmount) {
		this.adjustBudgetAmount = adjustBudgetAmount;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	
	
	
	
}