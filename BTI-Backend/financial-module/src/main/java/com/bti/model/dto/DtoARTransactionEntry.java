package com.bti.model.dto;

import com.bti.model.ARTransactionEntry;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoARTransactionEntry {
	
	private String arTransactionNumber;
	private Integer arTransactionType;
	private String arTransactionDescription;
	private String batchID;
	private String arTransactionDate;
	private String customerID;
	private String customerName;
	private String customerNameArabic;
	private String currencyID;
	private String paymentTermsID;
	private Double arTransactionCost;
	private Double arTransactionSalesAmount;
	private Double arTransactionTradeDiscount;
	private Double arTransactionFreightAmount;
	private Double arTransactionMiscellaneous;
	private Double arTransactionVATAmount;
	private Double arTransactionDebitMemoAmount;
	private Double arTransactionFinanceChargeAmount;
	private Double arTransactionWarrantyAmount;
	private Double arTransactionCreditMemoAmount;
	private Double arTransactionTotalAmount;
	private Double arTransactionCashAmount;
	private Double arTransactionCheckAmount;
	private Double arTransactionCreditCardAmount;
	private String salesmanID;
	private String checkbookID;
	private String cashReceiptNumber;
	private String checkNumber;
	private String creditCardID;
	private String creditCardNumber;
	private String creditCardExpireYear;
	private String creditCardExpireMonth;
	private Integer arTransactionStatus;
	private Integer exchangeTableIndex;
	private Double exchangeTableRate;
	private Boolean transactionVoid;
	private String shippingMethodID;
	private String vatScheduleID;
	private Integer paymentTypeId;
	private String territoryId;
	private String paymentAmount;
	private Boolean isPayment;
	private Double arServiceRepairAmount;
	private Double arReturnAmount;
	private int paymentTransactionTypeId;
	private String messageType;
	private String buttonType;
	
	public DtoARTransactionEntry() {
	}
	
	public DtoARTransactionEntry(ARTransactionEntry arTransactionEntry) 
	{
		 this.setArTransactionType(arTransactionEntry.getTransactionType()!=null?arTransactionEntry.getTransactionType().getArDocumentTypeId():0);
		 this.setArTransactionNumber(arTransactionEntry.getArTransactionNumber());
		 this.setArTransactionDescription(arTransactionEntry.getArTransactionDescription());
		 this.setArTransactionDate(arTransactionEntry.getArTransactionDate()!=null?UtilDateAndTime.dateToStringddmmyyyy(arTransactionEntry.getArTransactionDate()):"");
		 this.setCustomerID(arTransactionEntry.getCustomerID());
		 this.setCustomerName(arTransactionEntry.getCustomerName()!=null?arTransactionEntry.getCustomerName():"");
		 this.setCustomerNameArabic(arTransactionEntry.getCustomerNameArabic()!=null?arTransactionEntry.getCustomerNameArabic():"");
		 this.setSalesmanID(arTransactionEntry.getSalesmanID());
		 this.setCurrencyID(arTransactionEntry.getCurrencyID());
		 this.setPaymentTermsID(arTransactionEntry.getPaymentTermsID());
		 this.setShippingMethodID(arTransactionEntry.getShippingMethodID());
		 this.setVatScheduleID("");
		 this.setTerritoryId("");
		 
		 if(UtilRandomKey.isNotBlank(arTransactionEntry.getSalesTerritoryId())){
			 this.setTerritoryId(arTransactionEntry.getSalesTerritoryId());
		 }
		 if(UtilRandomKey.isNotBlank(arTransactionEntry.getVatScheduleID())){
			 this.setVatScheduleID(arTransactionEntry.getVatScheduleID());
		 }
		 
		 this.setBatchID(arTransactionEntry.getArBatches()!=null?arTransactionEntry.getArBatches().getBatchID():"");
		 this.setArTransactionCost(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionCost()));
		 this.setArTransactionSalesAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionSalesAmount()));
		 this.setArTransactionTradeDiscount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionTradeDiscount()));
		 this.setArTransactionMiscellaneous(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionMiscellaneous()));
		 this.setArTransactionFreightAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionFreightAmount()));
		 this.setArTransactionVATAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionVATAmount()));
		 this.setArTransactionTotalAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionTotalAmount()));
		 this.setArServiceRepairAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArServiceRepairAmount()));
		 this.setArReturnAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArReturnAmount()));
		 this.setCheckbookID("");
		 this.setCheckNumber("");
		 if(UtilRandomKey.isNotBlank(arTransactionEntry.getCheckbookID())){
			 this.setCheckbookID(arTransactionEntry.getCheckbookID());
		 }
		 if(UtilRandomKey.isNotBlank(arTransactionEntry.getCheckNumber())){
			 this.setCheckNumber(arTransactionEntry.getCheckNumber());
		 }
		
		 this.setCreditCardID("");
		 this.setCreditCardNumber("");
		 this.setCreditCardExpireMonth("");
		 this.setCreditCardExpireYear("");
		 if(arTransactionEntry.getCreditCardID()!=null){
			 this.setCreditCardID(arTransactionEntry.getCreditCardID());
			 this.setCreditCardNumber(arTransactionEntry.getCreditCardNumber());
			 this.setCreditCardExpireMonth(arTransactionEntry.getCreditCardExpireMonth());
			 this.setCreditCardExpireYear(arTransactionEntry.getCreditCardExpireYear());
		 }
		 
		 this.setArTransactionCashAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionCashAmount()));
		 this.setArTransactionCheckAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionCheckAmount()));
		 this.setArTransactionCreditCardAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionCreditCardAmount()));
		 this.setArTransactionCreditMemoAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionCreditMemoAmount()));
		 this.setArTransactionDebitMemoAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionDebitMemoAmount()));
		 this.setArTransactionFinanceChargeAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionFinanceChargeAmount()));
		 this.setArTransactionStatus(arTransactionEntry.getArTransactionStatus()!=null?arTransactionEntry.getArTransactionStatus().getStatusId():null);
		 this.setArTransactionWarrantyAmount(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getArTransactionWarrantyAmount()));
		 this.setCashReceiptNumber(arTransactionEntry.getCashReceiptNumber());
		 this.setExchangeTableIndex(arTransactionEntry.getExchangeTableIndex());
		 this.setExchangeTableRate(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getExchangeTableRate()));
		 this.setTransactionVoid(arTransactionEntry.isTransactionVoid());
		 this.setPaymentTypeId(0);
		 if(UtilRandomKey.isNotBlank(arTransactionEntry.getPaymentTypeId())){
			 this.setPaymentTypeId(Integer.parseInt(arTransactionEntry.getPaymentTypeId()));
		 }
		 this.setIsPayment(arTransactionEntry.isPayment());
		 this.setPaymentAmount(String.valueOf(UtilRoundDecimal.roundDecimalValue(arTransactionEntry.getPaymentAmount())));
	}
	public String getArTransactionNumber() {
		return arTransactionNumber;
	}
	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}
	public Integer getArTransactionType() {
		return arTransactionType;
	}
	public void setArTransactionType(Integer arTransactionType) {
		this.arTransactionType = arTransactionType;
	}
	
	public String getArTransactionDescription() {
		return arTransactionDescription;
	}
	public void setArTransactionDescription(String arTransactionDescription) {
		this.arTransactionDescription = arTransactionDescription;
	}
	public String getBatchID() {
		return batchID;
	}
	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}
	public String getArTransactionDate() {
		return arTransactionDate;
	}
	public void setArTransactionDate(String arTransactionDate) {
		this.arTransactionDate = arTransactionDate;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerNameArabic() {
		return customerNameArabic;
	}
	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}
	public String getCurrencyID() {
		return currencyID;
	}
	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}
	public String getPaymentTermsID() {
		return paymentTermsID;
	}
	public void setPaymentTermsID(String paymentTermsID) {
		this.paymentTermsID = paymentTermsID;
	}
	public Double getArTransactionCost() {
		return arTransactionCost;
	}
	public void setArTransactionCost(Double arTransactionCost) {
		this.arTransactionCost = UtilRoundDecimal.roundDecimalValue(arTransactionCost);
	}
	public Double getArTransactionSalesAmount() {
		return arTransactionSalesAmount;
	}
	public void setArTransactionSalesAmount(Double arTransactionSalesAmount) {
		this.arTransactionSalesAmount = UtilRoundDecimal.roundDecimalValue(arTransactionSalesAmount);
	}
	public Double getArTransactionTradeDiscount() {
		return arTransactionTradeDiscount;
	}
	public void setArTransactionTradeDiscount(Double arTransactionTradeDiscount) {
		this.arTransactionTradeDiscount = UtilRoundDecimal.roundDecimalValue(arTransactionTradeDiscount);
	}
	public Double getArTransactionFreightAmount() {
		return arTransactionFreightAmount;
	}
	public void setArTransactionFreightAmount(Double arTransactionFreightAmount) {
		this.arTransactionFreightAmount = UtilRoundDecimal.roundDecimalValue(arTransactionFreightAmount);
	}
	public Double getArTransactionMiscellaneous() {
		return arTransactionMiscellaneous;
	}
	public void setArTransactionMiscellaneous(Double arTransactionMiscellaneous) {
		this.arTransactionMiscellaneous = UtilRoundDecimal.roundDecimalValue(arTransactionMiscellaneous);
	}
	public Double getArTransactionVATAmount() {
		return arTransactionVATAmount;
	}
	public void setArTransactionVATAmount(Double arTransactionVATAmount) {
		this.arTransactionVATAmount = UtilRoundDecimal.roundDecimalValue(arTransactionVATAmount);
	}
	public Double getArTransactionDebitMemoAmount() {
		return arTransactionDebitMemoAmount;
	}
	public void setArTransactionDebitMemoAmount(Double arTransactionDebitMemoAmount) {
		this.arTransactionDebitMemoAmount = UtilRoundDecimal.roundDecimalValue(arTransactionDebitMemoAmount);
	}
	public Double getArTransactionFinanceChargeAmount() {
		return arTransactionFinanceChargeAmount;
	}
	public void setArTransactionFinanceChargeAmount(Double arTransactionFinanceChargeAmount) {
		this.arTransactionFinanceChargeAmount = UtilRoundDecimal.roundDecimalValue(arTransactionFinanceChargeAmount);
	}
	public Double getArTransactionWarrantyAmount() {
		return arTransactionWarrantyAmount;
	}
	public void setArTransactionWarrantyAmount(Double arTransactionWarrantyAmount) {
		this.arTransactionWarrantyAmount = UtilRoundDecimal.roundDecimalValue(arTransactionWarrantyAmount);
	}
	public Double getArTransactionCreditMemoAmount() {
		return arTransactionCreditMemoAmount;
	}
	public void setArTransactionCreditMemoAmount(Double arTransactionCreditMemoAmount) {
		this.arTransactionCreditMemoAmount = UtilRoundDecimal.roundDecimalValue(arTransactionCreditMemoAmount);
	}
	public Double getArTransactionTotalAmount() {
		return arTransactionTotalAmount;
	}
	public void setArTransactionTotalAmount(Double arTransactionTotalAmount) {
		this.arTransactionTotalAmount = UtilRoundDecimal.roundDecimalValue(arTransactionTotalAmount);
	}
	public Double getArTransactionCashAmount() {
		return arTransactionCashAmount;
	}
	public void setArTransactionCashAmount(Double arTransactionCashAmount) {
		this.arTransactionCashAmount = UtilRoundDecimal.roundDecimalValue(arTransactionCashAmount);
	}
	public Double getArTransactionCheckAmount() {
		return arTransactionCheckAmount;
	}
	public void setArTransactionCheckAmount(Double arTransactionCheckAmount) {
		this.arTransactionCheckAmount = UtilRoundDecimal.roundDecimalValue(arTransactionCheckAmount);
	}
	public Double getArTransactionCreditCardAmount() {
		return arTransactionCreditCardAmount;
	}
	public void setArTransactionCreditCardAmount(Double arTransactionCreditCardAmount) {
		this.arTransactionCreditCardAmount = UtilRoundDecimal.roundDecimalValue(arTransactionCreditCardAmount);
	}
	public String getSalesmanID() {
		return salesmanID;
	}
	public void setSalesmanID(String salesmanID) {
		this.salesmanID = salesmanID;
	}
	public String getCheckbookID() {
		return checkbookID;
	}
	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}
	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}
	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}
	public String getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
	public String getCreditCardID() {
		return creditCardID;
	}
	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getCreditCardExpireYear() {
		return creditCardExpireYear;
	}
	public void setCreditCardExpireYear(String creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}
	public String getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}
	public void setCreditCardExpireMonth(String creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}
	public Integer getArTransactionStatus() {
		return arTransactionStatus;
	}
	public void setArTransactionStatus(Integer arTransactionStatus) {
		this.arTransactionStatus = arTransactionStatus;
	}
	public Integer getExchangeTableIndex() {
		return exchangeTableIndex;
	}
	public void setExchangeTableIndex(Integer exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}
	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}
	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = UtilRoundDecimal.roundDecimalValue(exchangeTableRate);
	}
	
	public Boolean getTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(Boolean transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getShippingMethodID() {
		return shippingMethodID;
	}
	public void setShippingMethodID(String shippingMethodID) {
		this.shippingMethodID = shippingMethodID;
	}
	public String getVatScheduleID() {
		return vatScheduleID;
	}
	public void setVatScheduleID(String vatScheduleID) {
		this.vatScheduleID = vatScheduleID;
	}

	public String getTerritoryId() {
		return territoryId;
	}

	public void setTerritoryId(String territoryId) {
		this.territoryId = territoryId;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		if(paymentAmount==null || paymentAmount==""){
			paymentAmount="0.0";
		}
		else{
		this.paymentAmount = paymentAmount;
	}
	}

	public Boolean getIsPayment() {
		return isPayment;
	}

	public void setIsPayment(Boolean isPayment) {
		this.isPayment = isPayment;
	}

	public Integer getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(Integer paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	public Double getArServiceRepairAmount() {
		return arServiceRepairAmount;
	}

	public void setArServiceRepairAmount(Double arServiceRepairAmount) {
		this.arServiceRepairAmount = UtilRoundDecimal.roundDecimalValue(arServiceRepairAmount);
	}

	public Double getArReturnAmount() {
		return arReturnAmount;
	}

	public void setArReturnAmount(Double arReturnAmount) {
		this.arReturnAmount = UtilRoundDecimal.roundDecimalValue(arReturnAmount);
	}

	public int getPaymentTransactionTypeId() {
		return paymentTransactionTypeId;
	}

	public void setPaymentTransactionTypeId(int paymentTransactionTypeId) {
		this.paymentTransactionTypeId = paymentTransactionTypeId;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}
}
