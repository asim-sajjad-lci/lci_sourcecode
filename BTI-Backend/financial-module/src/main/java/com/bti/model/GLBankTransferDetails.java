/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10501 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10501")
@NamedQuery(name="GLBankTransferDetails.findAll", query="SELECT a FROM GLBankTransferDetails a")
public class GLBankTransferDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BNKTRXSEQN")
	private int bankTransferSequence;
	
	@ManyToOne
	@JoinColumn(name="BNKTRNSF")
	private GLBankTransferHeader glBankTransferHeader;
	
	@Column(name="TYPTRNF")
	private int typeTransfer;

	@Column(name="COMPID")
	private String companyID;
	
	@Column(name="CHEKBOKID")
	private String checkbookId;

	@Column(name="TRNFAMT")
	private double transferAmount;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	public int getBankTransferSequence() {
		return bankTransferSequence;
	}

	public void setBankTransferSequence(int bankTransferSequence) {
		this.bankTransferSequence = bankTransferSequence;
	}

	public GLBankTransferHeader getGlBankTransferHeader() {
		return glBankTransferHeader;
	}

	public void setGlBankTransferHeader(GLBankTransferHeader glBankTransferHeader) {
		this.glBankTransferHeader = glBankTransferHeader;
	}

	public int getTypeTransfer() {
		return typeTransfer;
	}

	public void setTypeTransfer(int typeTransfer) {
		this.typeTransfer = typeTransfer;
	}

	public String getCompanyID() {
		return companyID;
	}

	public void setCompanyID(String companyID) {
		this.companyID = companyID;
	}


	

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public double getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(double transferAmount) {
		this.transferAmount = transferAmount;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}              
	
	
	
	

}