/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.MainAccountTypes;

/**
 * Description: DtoMainAccountTypes class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 10, 2017
 * Modified on: AUG 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

public class DtoMainAccountTypes {
	 
	private Integer accountTypeId;
	private String accountTypeName;
	private String accountTypeNameArabic;
	private Integer changeBy;
	private Integer createdBy;
	private Integer rowIndexId;
	public DtoMainAccountTypes() {
		
	}
	public DtoMainAccountTypes(MainAccountTypes accountTypes) {
		this.accountTypeId = accountTypes.getAccountTypeId();
		this.accountTypeName = accountTypes.getAccountTypeName();
		this.accountTypeNameArabic = accountTypes.getAccountTypeNameArabic();
		
	}
	public Integer getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(Integer accountTypeId) {
		this.accountTypeId = accountTypeId;
	}
	public String getAccountTypeName() {
		return accountTypeName;
	}
	public void setAccountTypeName(String accountTypeName) {
		this.accountTypeName = accountTypeName;
	}
	public String getAccountTypeNameArabic() {
		return accountTypeNameArabic;
	}
	public void setAccountTypeNameArabic(String accountTypeNameArabic) {
		this.accountTypeNameArabic = accountTypeNameArabic;
	}
	public Integer getChangeBy() {
		return changeBy;
	}
	public void setChangeBy(Integer changeBy) {
		this.changeBy = changeBy;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getRowIndexId() {
		return rowIndexId;
	}
	public void setRowIndexId(Integer rowIndexId) {
		this.rowIndexId = rowIndexId;
	}
	 
}
