/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FAInsuranceClassSetup;
import com.bti.util.UtilRoundDecimal;

/**
 * Description: Dto Insurance Class
 * Name of Project: BTI
 * Created on: Sep 01, 2017
 * Modified on: Sep 01, 2017 09:08:38 AM
 * @author seasia
 * Version: 
 */
public class DtoInsuranceClass {
	
	private int insuranceClassIndex;
	private String insuranceClassId;
	private String insuranceDescriptionPrimary;
	private String insuranceDescriptionSecondary;
	private double inflationPercent;
	private double depriciationRate;
	
	public DtoInsuranceClass(){
		
	}
	public DtoInsuranceClass(FAInsuranceClassSetup faInsuranceClassSetup){
		this.insuranceClassIndex = faInsuranceClassSetup.getInsuranceClassIndex();
		this.insuranceClassId = faInsuranceClassSetup.getInsuranceClassId();
		this.insuranceDescriptionPrimary = faInsuranceClassSetup.getInsuranceDescription();
		this.insuranceDescriptionSecondary = faInsuranceClassSetup.getInsuranceDescriptionArabic();
		this.inflationPercent = faInsuranceClassSetup.getInflationPercent();
		this.depriciationRate = faInsuranceClassSetup.getDepreciationRate();
				
	}
	public int getInsuranceClassIndex() {
		return insuranceClassIndex;
	}
	public void setInsuranceClassIndex(int insuranceClassIndex) {
		this.insuranceClassIndex = insuranceClassIndex;
	}
	public String getInsuranceClassId() {
		return insuranceClassId;
	}
	public void setInsuranceClassId(String insuranceClassId) {
		this.insuranceClassId = insuranceClassId;
	}
	public String getInsuranceDescriptionPrimary() {
		return insuranceDescriptionPrimary;
	}
	public void setInsuranceDescriptionPrimary(String insuranceDescriptionPrimary) {
		this.insuranceDescriptionPrimary = insuranceDescriptionPrimary;
	}
	public String getInsuranceDescriptionSecondary() {
		return insuranceDescriptionSecondary;
	}
	public void setInsuranceDescriptionSecondary(String insuranceDescriptionSecondary) {
		this.insuranceDescriptionSecondary = insuranceDescriptionSecondary;
	}
	public double getInflationPercent() {
		return inflationPercent;
	}
	public void setInflationPercent(double inflationPercent) {
		this.inflationPercent = UtilRoundDecimal.roundDecimalValue(inflationPercent);
	}
	public double getDepriciationRate() {
		return depriciationRate;
	}
	public void setDepriciationRate(double depriciationRate) {
		this.depriciationRate = UtilRoundDecimal.roundDecimalValue(depriciationRate);
	}
}
