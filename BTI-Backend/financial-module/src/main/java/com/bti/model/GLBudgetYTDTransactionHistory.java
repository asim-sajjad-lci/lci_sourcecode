/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl90600 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl90600")
@NamedQuery(name="GLBudgetYTDTransactionHistory.findAll", query="SELECT a FROM GLBudgetYTDTransactionHistory a")
public class GLBudgetYTDTransactionHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BDGTRXID")
	private String budgetTransactionID;
	
	@ManyToOne
	@JoinColumn(name="BUDGTID")
	private GLBudgetMaintenance glBudgetMaintenance;
	
	@Column(name="BATCHID")
	private String batchID;

	@Column(name="PERIOD")
	private int periodID;

	@Column(name="PERIODDT")
	private Date periodDate;
	
	@Column(name="ACTINDX")
	private int mainAccountIndex;
	
	@Column(name="BDGAMNT")
	private Double actualBudgetAmount;
	
	@Column(name="BDGAMNTA")
	private Double adjustBudgetAmount;
	
	@Column(name="BDGTRXUSR")
	private String budgetTransactionEntrybyUserID;
	
	@Column(name="BDGTRXDT")
	private Date budgetTransactionEntryDate;
	
	@Column(name="BDGTRXUSRP")
	private String budgetTransactionPostingbyUserID;
	
	@Column(name="BDGTRXDTP")
	private Date budgetTransactionPostingDate;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public String getBudgetTransactionID() {
		return budgetTransactionID;
	}

	public void setBudgetTransactionID(String budgetTransactionID) {
		this.budgetTransactionID = budgetTransactionID;
	}

	public GLBudgetMaintenance getGlBudgetMaintenance() {
		return glBudgetMaintenance;
	}

	public void setGlBudgetMaintenance(GLBudgetMaintenance glBudgetMaintenance) {
		this.glBudgetMaintenance = glBudgetMaintenance;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public int getPeriodID() {
		return periodID;
	}

	public void setPeriodID(int periodID) {
		this.periodID = periodID;
	}

	public Date getPeriodDate() {
		return periodDate;
	}

	public void setPeriodDate(Date periodDate) {
		this.periodDate = periodDate;
	}

	public int getMainAccountIndex() {
		return mainAccountIndex;
	}

	public void setMainAccountIndex(int mainAccountIndex) {
		this.mainAccountIndex = mainAccountIndex;
	}

	public Double getActualBudgetAmount() {
		return actualBudgetAmount;
	}

	public void setActualBudgetAmount(Double actualBudgetAmount) {
		this.actualBudgetAmount = actualBudgetAmount;
	}

	public Double getAdjustBudgetAmount() {
		return adjustBudgetAmount;
	}

	public void setAdjustBudgetAmount(Double adjustBudgetAmount) {
		this.adjustBudgetAmount = adjustBudgetAmount;
	}

	public String getBudgetTransactionEntrybyUserID() {
		return budgetTransactionEntrybyUserID;
	}

	public void setBudgetTransactionEntrybyUserID(String budgetTransactionEntrybyUserID) {
		this.budgetTransactionEntrybyUserID = budgetTransactionEntrybyUserID;
	}

	public Date getBudgetTransactionEntryDate() {
		return budgetTransactionEntryDate;
	}

	public void setBudgetTransactionEntryDate(Date budgetTransactionEntryDate) {
		this.budgetTransactionEntryDate = budgetTransactionEntryDate;
	}

	public String getBudgetTransactionPostingbyUserID() {
		return budgetTransactionPostingbyUserID;
	}

	public void setBudgetTransactionPostingbyUserID(String budgetTransactionPostingbyUserID) {
		this.budgetTransactionPostingbyUserID = budgetTransactionPostingbyUserID;
	}

	public Date getBudgetTransactionPostingDate() {
		return budgetTransactionPostingDate;
	}

	public void setBudgetTransactionPostingDate(Date budgetTransactionPostingDate) {
		this.budgetTransactionPostingDate = budgetTransactionPostingDate;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}
	
}