/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.VendorMaintenance;
import com.bti.model.VendorMaintenanceOptions;
import com.bti.util.UtilRoundDecimal;

public class DtoVendorMaintenanceOptions {
	
	private String vendorId;
	private int creditLimit;
	private Double cardLimitAmount;
	private String currencyId;
	private int maximumInvoiceAmount;
	private Double maximumInvoiceAmountValue;
	private int minimumCharge;
	private Double minimumChargeAmount;
	private Double minimumOrderAmount;
	private Double tradeDiscountPercent;
	private int openMaintenanceHistoryCalendarYear;
	private int openMaintenanceHistoryDistribution;
	private int openMaintenanceHistoryFiscalYear;
	private int openMaintenanceHistoryTransaction;
	private String userDefine1;
	private String userDefine2;
	private String userDefine3;
	private String messageType;
	private String vatScheduleId;
	private String shipmentMethodId;
	private String checkBookId;
	private String paymentTermId;
	public DtoVendorMaintenanceOptions() {}
	public DtoVendorMaintenanceOptions(VendorMaintenanceOptions vendorMaintenanceOptions) {
		this.setVendorId(vendorMaintenanceOptions.getVendorMaintenance().getVendorid());
		this.setCreditLimit(vendorMaintenanceOptions.getCreditLimit());
		this.setCardLimitAmount(UtilRoundDecimal.roundDecimalValue(vendorMaintenanceOptions.getCardLimitAmount()));
		this.setCurrencyId(vendorMaintenanceOptions.getCurrencySetup()!=null?vendorMaintenanceOptions.getCurrencySetup().getCurrencyId():null);
		this.setMaximumInvoiceAmount(vendorMaintenanceOptions.getMaximumInvoiceAmount());
		this.setMaximumInvoiceAmountValue(UtilRoundDecimal.roundDecimalValue(vendorMaintenanceOptions.getMaximumInvoiceAmountValue()));
		this.setMinimumCharge(vendorMaintenanceOptions.getMinimumCharge());
		this.setMinimumChargeAmount(UtilRoundDecimal.roundDecimalValue(vendorMaintenanceOptions.getMinimumChargeAmount()));
		this.setMinimumOrderAmount(UtilRoundDecimal.roundDecimalValue(vendorMaintenanceOptions.getMinimumOrderAmount()));
		this.setOpenMaintenanceHistoryCalendarYear(vendorMaintenanceOptions.getOpenMaintenanceHistoryCalendarYear());
		this.setOpenMaintenanceHistoryDistribution(vendorMaintenanceOptions.getOpenMaintenanceHistoryDistribution());
		this.setOpenMaintenanceHistoryFiscalYear(vendorMaintenanceOptions.getOpenMaintenanceHistoryFiscalYear());
		this.setOpenMaintenanceHistoryTransaction(vendorMaintenanceOptions.getOpenMaintenanceHistoryTransaction());
		this.setTradeDiscountPercent(UtilRoundDecimal.roundDecimalValue(vendorMaintenanceOptions.getTradeDiscountPercent()));
		this.setUserDefine1(vendorMaintenanceOptions.getUserDefine1());
		this.setUserDefine2(vendorMaintenanceOptions.getUserDefine2());
		this.setUserDefine3(vendorMaintenanceOptions.getUserDefine3());
		this.vatScheduleId = vendorMaintenanceOptions.getVatSetup()!=null?vendorMaintenanceOptions.getVatSetup().getVatScheduleId():null;
		this.shipmentMethodId = vendorMaintenanceOptions.getShipmentMethodSetup()!=null?vendorMaintenanceOptions.getShipmentMethodSetup().getShipmentMethodId():null;
		this.checkBookId = vendorMaintenanceOptions.getCheckbookMaintenance()!=null?vendorMaintenanceOptions.getCheckbookMaintenance().getCheckBookId():null;
		this.paymentTermId = vendorMaintenanceOptions.getPaymentTermsSetup()!=null?vendorMaintenanceOptions.getPaymentTermsSetup().getPaymentTermId():null;
	 
	}

  
	public DtoVendorMaintenanceOptions(VendorMaintenance vendorMaintenance) {
		this.setVendorId(vendorMaintenance.getVendorid());
		this.setCreditLimit(vendorMaintenance.getVendorClassesSetup().getCreditLimit());
		this.setCardLimitAmount(UtilRoundDecimal.roundDecimalValue(vendorMaintenance.getVendorClassesSetup().getCreditLimitAmount()));
		this.setCurrencyId(vendorMaintenance.getVendorClassesSetup().getCurrencySetup()!=null?vendorMaintenance.getVendorClassesSetup().getCurrencySetup().getCurrencyId():null);
		this.setMaximumInvoiceAmount(vendorMaintenance.getVendorClassesSetup().getMaximumInvoiceAmount());
		this.setMaximumInvoiceAmountValue(UtilRoundDecimal.roundDecimalValue(vendorMaintenance.getVendorClassesSetup().getMaximumInvoiceAmountValue()));
		this.setMinimumCharge(vendorMaintenance.getVendorClassesSetup().getMinimumCharge());
		this.setMinimumChargeAmount(UtilRoundDecimal.roundDecimalValue(vendorMaintenance.getVendorClassesSetup().getMinimumChargeAmount()));
		this.setMinimumOrderAmount(UtilRoundDecimal.roundDecimalValue(vendorMaintenance.getVendorClassesSetup().getMinimumOrderAmount()));
		this.setOpenMaintenanceHistoryCalendarYear(vendorMaintenance.getVendorClassesSetup().getOpenMaintenanceHistoryCalendarYear());
		this.setOpenMaintenanceHistoryDistribution(vendorMaintenance.getVendorClassesSetup().getOpenMaintenanceHistoryDistribution());
		this.setOpenMaintenanceHistoryFiscalYear(vendorMaintenance.getVendorClassesSetup().getOpenMaintenanceHistoryFiscalYear());
		this.setOpenMaintenanceHistoryTransaction(vendorMaintenance.getVendorClassesSetup().getOpenMaintenanceHistoryTransaction());
		this.setTradeDiscountPercent(UtilRoundDecimal.roundDecimalValue(vendorMaintenance.getVendorClassesSetup().getTradeDiscountPercent()));
		this.setUserDefine1(vendorMaintenance.getVendorClassesSetup().getUserDefine1());
		this.setUserDefine2(vendorMaintenance.getVendorClassesSetup().getUserDefine2());
		this.setUserDefine3(vendorMaintenance.getVendorClassesSetup().getUserDefine3());
		this.vatScheduleId = vendorMaintenance.getVendorClassesSetup().getVatSetup()!=null?vendorMaintenance.getVendorClassesSetup().getVatSetup().getVatScheduleId():null;
		this.shipmentMethodId = vendorMaintenance.getVendorClassesSetup().getShipmentMethodSetup()!=null?vendorMaintenance.getVendorClassesSetup().getShipmentMethodSetup().getShipmentMethodId():null;
		this.checkBookId = vendorMaintenance.getVendorClassesSetup().getCheckbookMaintenance()!=null?vendorMaintenance.getVendorClassesSetup().getCheckbookMaintenance().getCheckBookId():null;
		this.paymentTermId = vendorMaintenance.getVendorClassesSetup().getPaymentTermsSetup()!=null?vendorMaintenance.getVendorClassesSetup().getPaymentTermsSetup().getPaymentTermId():null;
	 
	}
	public Double getCardLimitAmount() {
		return cardLimitAmount;
	}
	public void setCardLimitAmount(Double cardLimitAmount) {
		this.cardLimitAmount = cardLimitAmount;
	}
	public int getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public int getMaximumInvoiceAmount() {
		return maximumInvoiceAmount;
	}

	public void setMaximumInvoiceAmount(int maximumInvoiceAmount) {
		this.maximumInvoiceAmount = maximumInvoiceAmount;
	}

	public int getMinimumCharge() {
		return minimumCharge;
	}

	public void setMinimumCharge(int minimumCharge) {
		this.minimumCharge = minimumCharge;
	}

	public Double getMinimumChargeAmount() {
		return minimumChargeAmount;
	}

	public void setMinimumChargeAmount(Double minimumChargeAmount) {
		this.minimumChargeAmount = UtilRoundDecimal.roundDecimalValue(minimumChargeAmount);
	}

	public Double getMinimumOrderAmount() {
		return minimumOrderAmount;
	}

	public void setMinimumOrderAmount(Double minimumOrderAmount) {
		this.minimumOrderAmount = UtilRoundDecimal.roundDecimalValue(minimumOrderAmount);
	}

	public Double getTradeDiscountPercent() {
		return tradeDiscountPercent;
	}

	public void setTradeDiscountPercent(Double tradeDiscountPercent) {
		this.tradeDiscountPercent = UtilRoundDecimal.roundDecimalValue(tradeDiscountPercent);
	}

	public int getOpenMaintenanceHistoryCalendarYear() {
		return openMaintenanceHistoryCalendarYear;
	}

	public void setOpenMaintenanceHistoryCalendarYear(int openMaintenanceHistoryCalendarYear) {
		this.openMaintenanceHistoryCalendarYear = openMaintenanceHistoryCalendarYear;
	}

	public int getOpenMaintenanceHistoryDistribution() {
		return openMaintenanceHistoryDistribution;
	}

	public void setOpenMaintenanceHistoryDistribution(int openMaintenanceHistoryDistribution) {
		this.openMaintenanceHistoryDistribution = openMaintenanceHistoryDistribution;
	}

	public int getOpenMaintenanceHistoryFiscalYear() {
		return openMaintenanceHistoryFiscalYear;
	}

	public void setOpenMaintenanceHistoryFiscalYear(int openMaintenanceHistoryFiscalYear) {
		this.openMaintenanceHistoryFiscalYear = openMaintenanceHistoryFiscalYear;
	}

	public int getOpenMaintenanceHistoryTransaction() {
		return openMaintenanceHistoryTransaction;
	}

	public void setOpenMaintenanceHistoryTransaction(int openMaintenanceHistoryTransaction) {
		this.openMaintenanceHistoryTransaction = openMaintenanceHistoryTransaction;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getVatScheduleId() {
		return vatScheduleId;
	}

	public void setVatScheduleId(String vatScheduleId) {
		this.vatScheduleId = vatScheduleId;
	}

	public String getShipmentMethodId() {
		return shipmentMethodId;
	}

	public void setShipmentMethodId(String shipmentMethodId) {
		this.shipmentMethodId = shipmentMethodId;
	}

	public String getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(String paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public Double getMaximumInvoiceAmountValue() {
		return maximumInvoiceAmountValue;
	}
	public void setMaximumInvoiceAmountValue(Double maximumInvoiceAmountValue) {
		this.maximumInvoiceAmountValue = UtilRoundDecimal.roundDecimalValue(maximumInvoiceAmountValue);
	}

}
