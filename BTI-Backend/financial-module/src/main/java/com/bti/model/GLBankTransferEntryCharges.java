/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10503 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10503")
@NamedQuery(name="GLBankTransferEntryCharges .findAll", query="SELECT a FROM GLBankTransferEntryCharges  a")
public class GLBankTransferEntryCharges implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
   // @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CHRGNMBR")
	private String chargeNumber;
	
	@ManyToOne
	@JoinColumn(name="BNKTRNSF")
	private GLBankTransferHeader glBankTransferHeader;
	
	@Column(name="CHEKBOKID")
	private String checkbookId;

	@Column(name="ACTROWID")
	private String accountTableRowIndex;
	
	@Column(name="CHRGAMNT")
	private double chargeAmount;

	@Column(name="CHRGDSCR")
	private String chargeComments;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	@Column(name = "is_deleted", columnDefinition = "tinyint(0) default 0")
	protected Boolean isDeleted;
	
	public String getChargeNumber() {
		return chargeNumber;
	}

	public void setChargeNumber(String chargeNumber) {
		this.chargeNumber = chargeNumber;
	}

	public GLBankTransferHeader getGlBankTransferHeader() {
		return glBankTransferHeader;
	}

	public void setGlBankTransferHeader(GLBankTransferHeader glBankTransferHeader) {
		this.glBankTransferHeader = glBankTransferHeader;
	}

	

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public double getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(double chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getChargeComments() {
		return chargeComments;
	}

	public void setChargeComments(String chargeComments) {
		this.chargeComments = chargeComments;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}              
	
	
    
	
}