/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.RMSetup;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

public class DtoRMSetup {
	
	private int id;
	
	private Integer ageingBy;

	private Integer applyByDefault;

	private Boolean arPayCommissionsInvoicePay;

	private Boolean arTrackingDiscountAvailable;

	private Boolean compoundFinanceCharge;

	private String creditLimitPassword;

	private String customerHoldPassword;

	private Boolean deleteUnpostedPrintedDocuments;

	private String freightVatScheduleId;

	private String lastDateBalanceForwardAge;
	
	private String lastFinanceChargeDate;

	private String lastDateStatementPrinted;

	private String miscVatScheduleId;

	private String priceLevel;

	private Boolean printHistoricalAgedTrialBalance;

	private String salesVatScheduleId;

	private String userDefine1;

	private String userDefine2;

	private String userDefine3;

	private String waivedFinanceChargePassword;

	private String writeoffPassword;
	
	private String messageType;

	private String checkbookId;
	
	private String deleteUnpostedPrintedDocumentDate;
	
	private List<DtoRMPeriodSetup> dtoRMPeriodSetupsList;
	
	public DtoRMSetup() {
		
	}
	
	public DtoRMSetup(RMSetup rmSetup) {
		this.id=rmSetup.getId();
		this.ageingBy=0;
		if(rmSetup.getMasterRMAgeingTypes()!=null){
			this.ageingBy=rmSetup.getMasterRMAgeingTypes().getTypeId();
		}
		this.applyByDefault=0;
		if(rmSetup.getMasterRMApplyByDefaultTypes()!=null){
			this.applyByDefault=rmSetup.getMasterRMApplyByDefaultTypes().getTypeId();
		}
		//check book id
		this.checkbookId="";
		if(rmSetup.getCheckbookMaintenance()!=null){
			this.checkbookId=rmSetup.getCheckbookMaintenance().getCheckBookId();
		}
		this.priceLevel="";
		if(UtilRandomKey.isNotBlank(rmSetup.getPriceLevel())){
			this.priceLevel=rmSetup.getPriceLevel();
		}
		
		
		//Options
		this.compoundFinanceCharge=rmSetup.getCompoundFinanceCharge();
		this.arTrackingDiscountAvailable=rmSetup.getArTrackingDiscountAvailable();
		this.deleteUnpostedPrintedDocuments=rmSetup.getDeleteUnpostedPrintedDocuments();
		this.printHistoricalAgedTrialBalance=rmSetup.getPrintHistoricalAgedTrialBalance();
		this.arPayCommissionsInvoicePay=rmSetup.getArPayCommissionsInvoicePay();
		// Set Passwords
		this.creditLimitPassword="";
		this.customerHoldPassword="";
		this.waivedFinanceChargePassword="";
		this.writeoffPassword="";
		if(UtilRandomKey.isNotBlank(rmSetup.getCreditLimitPassword())){
			this.creditLimitPassword=rmSetup.getCreditLimitPassword();
		}
		if(UtilRandomKey.isNotBlank(rmSetup.getCustomerHoldPassword())){
			this.customerHoldPassword=rmSetup.getCustomerHoldPassword();
		}
		if(UtilRandomKey.isNotBlank(rmSetup.getWaivedFinanceChargePassword())){
			this.waivedFinanceChargePassword=rmSetup.getWaivedFinanceChargePassword();
		}
		if(UtilRandomKey.isNotBlank(rmSetup.getWriteoffPassword())){
			this.writeoffPassword=rmSetup.getWriteoffPassword();
		}
		
		//set Dates
		this.lastDateBalanceForwardAge="";
		this.deleteUnpostedPrintedDocumentDate="";
		this.lastDateStatementPrinted="";
		this.lastFinanceChargeDate="";
		
		if(rmSetup.getLastDateBalanceForwardAge()!=null){
			this.lastDateBalanceForwardAge=UtilDateAndTime.dateToStringddmmyyyy(rmSetup.getLastDateBalanceForwardAge());
		}
		if(rmSetup.getDeleteUnpostedPrintedDocumentsDate()!=null){
			this.deleteUnpostedPrintedDocumentDate=UtilDateAndTime.dateToStringddmmyyyy(rmSetup.getDeleteUnpostedPrintedDocumentsDate());
		}
		if(rmSetup.getLastDateStatementPrinted()!=null){
			this.lastDateStatementPrinted=UtilDateAndTime.dateToStringddmmyyyy(rmSetup.getLastDateStatementPrinted());
		}
		if(rmSetup.getLastFinanceChargeDate()!=null){
			this.lastFinanceChargeDate=UtilDateAndTime.dateToStringddmmyyyy(rmSetup.getLastFinanceChargeDate());
		}
	
	    
	    //Vat Schedule Id 
	    this.miscVatScheduleId="";
	    this.salesVatScheduleId="";
	    this.freightVatScheduleId="";
	    
	    if(rmSetup.getMiscVatScheduleId()!=null){
	    	this.miscVatScheduleId=rmSetup.getMiscVatScheduleId().getVatScheduleId();
	    }
	    
	    if(rmSetup.getSalesVatScheduleId()!=null){
	    	this.salesVatScheduleId=rmSetup.getSalesVatScheduleId().getVatScheduleId();
	    }
	    
	    if(rmSetup.getFreightVatScheduleId()!=null){
	    	  this.freightVatScheduleId=rmSetup.getFreightVatScheduleId().getVatScheduleId();
	    }
	    
	    this.userDefine1="";
	    this.userDefine2="";
	    this.userDefine3="";
	    
	    if(UtilRandomKey.isNotBlank(rmSetup.getUserDefine1())){
	    	this.userDefine1= rmSetup.getUserDefine1();
	    }
	    
	    if(UtilRandomKey.isNotBlank(rmSetup.getUserDefine2())){
	    	   this.userDefine2=rmSetup.getUserDefine2();
	    }
	    
        if(UtilRandomKey.isNotBlank(rmSetup.getUserDefine3())){
    	    this.userDefine3=rmSetup.getUserDefine3();
	    }
	}
	
	public Integer getAgeingBy() {
		return ageingBy;
	}

	public void setAgeingBy(Integer ageingBy) {
		this.ageingBy = ageingBy;
	}

	public Integer getApplyByDefault() {
		return applyByDefault;
	}

	public void setApplyByDefault(Integer applyByDefault) {
		this.applyByDefault = applyByDefault;
	}

	public Boolean getArPayCommissionsInvoicePay() {
		return arPayCommissionsInvoicePay;
	}

	public void setArPayCommissionsInvoicePay(Boolean arPayCommissionsInvoicePay) {
		this.arPayCommissionsInvoicePay = arPayCommissionsInvoicePay;
	}

	public Boolean getArTrackingDiscountAvailable() {
		return arTrackingDiscountAvailable;
	}

	public void setArTrackingDiscountAvailable(Boolean arTrackingDiscountAvailable) {
		this.arTrackingDiscountAvailable = arTrackingDiscountAvailable;
	}

	public Boolean getCompoundFinanceCharge() {
		return compoundFinanceCharge;
	}

	public void setCompoundFinanceCharge(Boolean compoundFinanceCharge) {
		this.compoundFinanceCharge = compoundFinanceCharge;
	}

	public String getCreditLimitPassword() {
		return creditLimitPassword;
	}

	public void setCreditLimitPassword(String creditLimitPassword) {
		this.creditLimitPassword = creditLimitPassword;
	}

	public String getCustomerHoldPassword() {
		return customerHoldPassword;
	}

	public void setCustomerHoldPassword(String customerHoldPassword) {
		this.customerHoldPassword = customerHoldPassword;
	}

	public Boolean getDeleteUnpostedPrintedDocuments() {
		return deleteUnpostedPrintedDocuments;
	}

	public void setDeleteUnpostedPrintedDocuments(Boolean deleteUnpostedPrintedDocuments) {
		this.deleteUnpostedPrintedDocuments = deleteUnpostedPrintedDocuments;
	}

	public String getFreightVatScheduleId() {
		return freightVatScheduleId;
	}

	public void setFreightVatScheduleId(String freightVatScheduleId) {
		this.freightVatScheduleId = freightVatScheduleId;
	}

	public String getLastDateBalanceForwardAge() {
		return lastDateBalanceForwardAge;
	}

	public void setLastDateBalanceForwardAge(String lastDateBalanceForwardAge) {
		this.lastDateBalanceForwardAge = lastDateBalanceForwardAge;
	}

	public String getLastFinanceChargeDate() {
		return lastFinanceChargeDate;
	}

	public void setLastFinanceChargeDate(String lastFinanceChargeDate) {
		this.lastFinanceChargeDate = lastFinanceChargeDate;
	}

	public String getLastDateStatementPrinted() {
		return lastDateStatementPrinted;
	}

	public void setLastDateStatementPrinted(String lastDateStatementPrinted) {
		this.lastDateStatementPrinted = lastDateStatementPrinted;
	}

	public String getMiscVatScheduleId() {
		return miscVatScheduleId;
	}

	public void setMiscVatScheduleId(String miscVatScheduleId) {
		this.miscVatScheduleId = miscVatScheduleId;
	}

	public String getPriceLevel() {
		return priceLevel;
	}

	public void setPriceLevel(String priceLevel) {
		this.priceLevel = priceLevel;
	}

	public Boolean getPrintHistoricalAgedTrialBalance() {
		return printHistoricalAgedTrialBalance;
	}

	public void setPrintHistoricalAgedTrialBalance(Boolean printHistoricalAgedTrialBalance) {
		this.printHistoricalAgedTrialBalance = printHistoricalAgedTrialBalance;
	}

	public String getSalesVatScheduleId() {
		return salesVatScheduleId;
	}

	public void setSalesVatScheduleId(String salesVatScheduleId) {
		this.salesVatScheduleId = salesVatScheduleId;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public String getWaivedFinanceChargePassword() {
		return waivedFinanceChargePassword;
	}

	public void setWaivedFinanceChargePassword(String waivedFinanceChargePassword) {
		this.waivedFinanceChargePassword = waivedFinanceChargePassword;
	}

	public String getWriteoffPassword() {
		return writeoffPassword;
	}

	public void setWriteoffPassword(String writeoffPassword) {
		this.writeoffPassword = writeoffPassword;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getDeleteUnpostedPrintedDocumentDate() {
		return deleteUnpostedPrintedDocumentDate;
	}

	public void setDeleteUnpostedPrintedDocumentDate(String deleteUnpostedPrintedDocumentDate) {
		this.deleteUnpostedPrintedDocumentDate = deleteUnpostedPrintedDocumentDate;
	}

	public List<DtoRMPeriodSetup> getDtoRMPeriodSetupsList() {
		return dtoRMPeriodSetupsList;
	}

	public void setDtoRMPeriodSetupsList(List<DtoRMPeriodSetup> dtoRMPeriodSetupsList) {
		this.dtoRMPeriodSetupsList = dtoRMPeriodSetupsList;
	}

}
