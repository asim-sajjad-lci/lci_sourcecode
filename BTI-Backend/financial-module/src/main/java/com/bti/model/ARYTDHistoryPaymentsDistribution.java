/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar90501 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar90501")
@NamedQuery(name="ARYTDHistoryPaymentsDistribution.findAll", query="SELECT a FROM ARYTDHistoryPaymentsDistribution a")
public class ARYTDHistoryPaymentsDistribution implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DOCSEQC")
	private int paymentSequence;
	
	@Column(name="DOCNUMBR")
	private String paymentNumber;
	
	@Column(name="ACTROWID")
	private int accountTableRowIndex;

	@Column(name="TRXTYP")
	private int transactionTypeListedinrequest;

	@Column(name="DEBITAMT")
	private Double debitAmount;
	
	@Column(name="CRDTAMT")
	private Double creditAmount;
	//
	@Column(name="ORGDEBTAMT")
	private Double originalDebitAmount;
	
	@Column(name="ORGCRETAMT")
	private Double originalCreditAmount;
	//
	@Column(name="DSTDSCR")
	private String distributionDescription;
	
	@Column(name="YEAR1")
	private int year;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private String rowIDindex;
	
	}