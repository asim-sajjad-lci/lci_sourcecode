/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mc40202 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "mc40202")
@NamedQuery(name="CurrencyExchangeDetail.findAll", query="SELECT m FROM CurrencyExchangeDetail m")
public class CurrencyExchangeDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="EXCHDATE")
	private Date exchangeDate;

	@Column(name="EXPNDATE")
	private Date expirationExchangeDate;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="TIME1")
	private Date time;

	@Column(name="XCHGRATE")
	private Double exchangeRate;
	
	@Column(name="EXCHDESC")
	private String exchangeDescription;

	@Column(name="EXCHDESCA")
	private String exchangeDescriptionArabic;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	//bi-directional many-to-one association to Mc40200
	@ManyToOne
	@JoinColumn(name="CURNCYID")
	private CurrencySetup currencySetup;

	//bi-directional many-to-one association to Mc40201
	@ManyToOne
	@JoinColumn(name="EXGTBLID")
	private CurrencyExchangeHeader currencyExchangeHeader;

	public CurrencyExchangeDetail() {
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getExchangeDate() {
		return exchangeDate;
	}

	public void setExchangeDate(Date exchangeDate) {
		this.exchangeDate = exchangeDate;
	}

	public Date getExpirationExchangeDate() {
		return expirationExchangeDate;
	}

	public void setExpirationExchangeDate(Date expirationExchangeDate) {
		this.expirationExchangeDate = expirationExchangeDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public CurrencySetup getCurrencySetup() {
		return currencySetup;
	}

	public void setCurrencySetup(CurrencySetup currencySetup) {
		this.currencySetup = currencySetup;
	}

	public CurrencyExchangeHeader getCurrencyExchangeHeader() {
		return currencyExchangeHeader;
	}

	public void setCurrencyExchangeHeader(CurrencyExchangeHeader currencyExchangeHeader) {
		this.currencyExchangeHeader = currencyExchangeHeader;
	}

	public String getExchangeDescription() {
		return exchangeDescription;
	}

	public void setExchangeDescription(String exchangeDescription) {
		this.exchangeDescription = exchangeDescription;
	}

	public String getExchangeDescriptionArabic() {
		return exchangeDescriptionArabic;
	}

	public void setExchangeDescriptionArabic(String exchangeDescriptionArabic) {
		this.exchangeDescriptionArabic = exchangeDescriptionArabic;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}