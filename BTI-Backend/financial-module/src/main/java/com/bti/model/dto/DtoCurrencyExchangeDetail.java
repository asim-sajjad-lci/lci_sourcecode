/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.CurrencyExchangeDetail;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Exchange Table Setup class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoCurrencyExchangeDetail {

	private String exchangeId;
	private String description;
	private String descriptionArabic;
	private String exchangeRate;
	private String exchangeDate;
	private String exchangeTime;
	private String exchangeExpirationDate;
	private String currencyId;
	private int id;
	
	public DtoCurrencyExchangeDetail() {
		
	}
	
	public DtoCurrencyExchangeDetail(CurrencyExchangeDetail currencyExchangeDetail,String langId) 
	{
		this.id= currencyExchangeDetail.getId();
		if(currencyExchangeDetail.getCurrencyExchangeHeader()!=null){
			this.exchangeId=currencyExchangeDetail.getCurrencyExchangeHeader().getExchangeId();
		}
		this.description=currencyExchangeDetail.getExchangeDescription();
		this.descriptionArabic=currencyExchangeDetail.getExchangeDescriptionArabic();
		if(currencyExchangeDetail.getExchangeRate()!=null){
				this.exchangeRate=String.valueOf(UtilRoundDecimal.roundDecimalValue(currencyExchangeDetail.getExchangeRate()));
		}
		this.exchangeDate="";
		if(currencyExchangeDetail.getExchangeDate()!=null){
			this.exchangeDate=UtilDateAndTime.dateToStringddmmyyyy(currencyExchangeDetail.getExchangeDate());
		}
		this.exchangeExpirationDate="";
		if(currencyExchangeDetail.getExpirationExchangeDate()!=null){
			this.exchangeExpirationDate=UtilDateAndTime.dateToStringddmmyyyy(currencyExchangeDetail.getExpirationExchangeDate());
		}
		this.exchangeTime="";
		if(currencyExchangeDetail.getTime()!=null){
			this.exchangeTime=UtilDateAndTime.convertDateTimeToStringTime24Formats(currencyExchangeDetail.getTime());
		}
		this.currencyId="";
		if(currencyExchangeDetail.getCurrencySetup()!=null){
					this.currencyId=currencyExchangeDetail.getCurrencySetup().getCurrencyId();
		}
	}	
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescriptionArabic() {
		return descriptionArabic;
	}
	
	public void setDescriptionArabic(String descriptionArabic) {
		this.descriptionArabic = descriptionArabic;
	}
	
	public String getExchangeId() {
		return exchangeId;
	}
	
	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}
	
	public String getCurrencyId() {
		return currencyId;
	}
	
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	
	public String getExchangeRate() {
		return exchangeRate;
	}
	
	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
	public String getExchangeDate() {
		return exchangeDate;
	}
	
	public void setExchangeDate(String exchangeDate) {
		this.exchangeDate = exchangeDate;
	}
	
	public String getExchangeTime() {
		return exchangeTime;
	}

	public void setExchangeTime(String exchangeTime) {
		this.exchangeTime = exchangeTime;
	}

	public String getExchangeExpirationDate() {
		return exchangeExpirationDate;
	}

	public void setExchangeExpirationDate(String exchangeExpirationDate) {
		this.exchangeExpirationDate = exchangeExpirationDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
