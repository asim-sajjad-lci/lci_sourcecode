/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10900 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10900")
@NamedQuery(name=" GLBudgetTransactionHeader.findAll", query="SELECT a FROM  GLBudgetTransactionHeader a")
public class GLBudgetTransactionHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BDGTRXID")
	private String budgetTransactionID;
	
	@ManyToOne
	@JoinColumn(name="BATCHID")
	private GLBatches glBatches;
	
	@Column(name="BDGTRXDT")
	private Date budgetTransactionDate ;

	@ManyToOne
	@JoinColumn(name="BUDGTID")
	private GLBudgetMaintenance glBudgetMaintenance;
	
	@Column(name="BDGDSCR")
	private String budgetTransactionDescription;

	@Column(name="CREATDDT")
	private Date createDate;
	
	@Column(name="MODIFTDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	
	public String getBudgetTransactionID() {
		return budgetTransactionID;
	}

	public void setBudgetTransactionID(String budgetTransactionID) {
		this.budgetTransactionID = budgetTransactionID;
	}

	public Date getBudgetTransactionDate() {
		return budgetTransactionDate;
	}

	public void setBudgetTransactionDate(Date budgetTransactionDate) {
		this.budgetTransactionDate = budgetTransactionDate;
	}
	
	public GLBatches getGlBatches() {
		return glBatches;
	}

	public void setGlBatches(GLBatches glBatches) {
		this.glBatches = glBatches;
	}

	public GLBudgetMaintenance getGlBudgetMaintenance() {
		return glBudgetMaintenance;
	}

	public void setGlBudgetMaintenance(GLBudgetMaintenance glBudgetMaintenance) {
		this.glBudgetMaintenance = glBudgetMaintenance;
	}

	public String getBudgetTransactionDescription() {
		return budgetTransactionDescription;
	}

	public void setBudgetTransactionDescription(String budgetTransactionDescription) {
		this.budgetTransactionDescription = budgetTransactionDescription;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	
	
}