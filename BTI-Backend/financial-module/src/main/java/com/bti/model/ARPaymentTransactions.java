/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ar20200 database table.
 * 
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar20200")
@NamedQuery(name = "ARPaymentTransactions.findAll", query = "SELECT a FROM ARPaymentTransactions a")
public class ARPaymentTransactions implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ARROWID")
	private Integer arRowIndexId;
	
	@Column(name = "ARTRXNMBR")
	private String arTransactionNumber;

	@Column(name = "ARTRXDT")
	private Date arTransactionDate;

	@Column(name = "ARTRXPODT")
	private Date arTransactionPostingDate;
	
	@Column(name = "CUSTNMBR")
	private String customerNumber;

	@Column(name = "CHEKBOKID")
	private String checkbookId;

	@Column(name = "CURNCYID")
	private String currencyId;

	@Column(name = "EXGTBLID")
	private String exchangeTableIndex;
	
	@Column(name = "ARPMTAMT")
	private double arPaymentAmount;

	@Column(name = "ARPMTFAMT")
	private double arFunctionalPaymentAmount;

	@Column(name = "AUDTTRALC")
	private String auditTrialCode;
	 
	@Column(name = "PMTVOID")
	private boolean paymentVoid;

	@Column(name = "DEX_ROW_TS")
	private Date rowDateindex;

	@Column(name = "DEX_ROW_ID")
	private int rowIndexId;

	public Integer getArRowIndexId() {
		return arRowIndexId;
	}

	public void setArRowIndexId(Integer arRowIndexId) {
		this.arRowIndexId = arRowIndexId;
	}

	public String getArTransactionNumber() {
		return arTransactionNumber;
	}

	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}

	public Date getArTransactionDate() {
		return arTransactionDate;
	}

	public void setArTransactionDate(Date arTransactionDate) {
		this.arTransactionDate = arTransactionDate;
	}

	public Date getArTransactionPostingDate() {
		return arTransactionPostingDate;
	}

	public void setArTransactionPostingDate(Date arTransactionPostingDate) {
		this.arTransactionPostingDate = arTransactionPostingDate;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public double getArPaymentAmount() {
		return arPaymentAmount;
	}

	public void setArPaymentAmount(double arPaymentAmount) {
		this.arPaymentAmount = arPaymentAmount;
	}

	public double getArFunctionalPaymentAmount() {
		return arFunctionalPaymentAmount;
	}

	public void setArFunctionalPaymentAmount(double arFunctionalPaymentAmount) {
		this.arFunctionalPaymentAmount = arFunctionalPaymentAmount;
	}

	public String getAuditTrialCode() {
		return auditTrialCode;
	}

	public void setAuditTrialCode(String auditTrialCode) {
		this.auditTrialCode = auditTrialCode;
	}

	public boolean isPaymentVoid() {
		return paymentVoid;
	}

	public void setPaymentVoid(boolean paymentVoid) {
		this.paymentVoid = paymentVoid;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

}