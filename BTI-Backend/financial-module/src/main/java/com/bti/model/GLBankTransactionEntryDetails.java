/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10801 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10801")
@NamedQuery(name=" GLBankTransactionEntryDetails.findAll", query="SELECT a FROM  GLBankTransactionEntryDetails a")
public class GLBankTransactionEntryDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BUDTRSQCN")
	private int bankTransactionEntrySequence;
	
	@ManyToOne
	@JoinColumn(name="BNKTRXID")
	private GLBankTransactionEntryHeader glBankTransactionEntryHeader;
	
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation ;

	@Column(name="DEBITAMT")
	private double debitAmount;
	
	@Column(name="CRDTAMT")
	private double creditAmount;

	@Column(name="ORGDEBTAMT")
	private double originalDebitAmount;
	
	@Column(name="ORGCRETAMT")
	private double originalCreditAmount;
	
	@Column(name="DSTDSCR")
	private String distributionDescription;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	
	public int getBankTransactionEntrySequence() {
		return bankTransactionEntrySequence;
	}

	public void setBankTransactionEntrySequence(int bankTransactionEntrySequence) {
		this.bankTransactionEntrySequence = bankTransactionEntrySequence;
	}
    
	public GLBankTransactionEntryHeader getGlBankTransactionEntryHeader() {
		return glBankTransactionEntryHeader;
	}

	public void setGlBankTransactionEntryHeader(GLBankTransactionEntryHeader glBankTransactionEntryHeader) {
		this.glBankTransactionEntryHeader = glBankTransactionEntryHeader;
	}

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public double getOriginalCreditAmount() {
		return originalCreditAmount;
	}

	public void setOriginalCreditAmount(double originalCreditAmount) {
		this.originalCreditAmount = originalCreditAmount;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}         

	
}