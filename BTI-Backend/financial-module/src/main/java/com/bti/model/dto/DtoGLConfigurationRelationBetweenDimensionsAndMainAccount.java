/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.GLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoGLConfigurationRelationBetweenDimensionsAndMainAccount {

	private Integer id;
	
	private Integer segmentNumber;
	
	private Integer isMainAccount;

	private Integer coaFinancialDimensionsIndexId;
	
	private Long coaFinancialDimensionsFromIndexValue;

	private  Long coaFinancialDimensionsToIndexValue;

	private Long coaMainAccountsFromActIndexId;
	
	private Long coaMainAccountsToActIndexId;
	
	private String messageType;

	private List<DtoGLConfigurationRelationBetweenDimensionsAndMainAccount> dimensionList;
	
	public DtoGLConfigurationRelationBetweenDimensionsAndMainAccount(GLConfigurationRelationBetweenDimensionsAndMainAccount obj) 
	{
		this.id=obj.getId();
		this.segmentNumber=obj.getSegmentNumber();
		this.isMainAccount=obj.getIsMainAccount();
		this.coaMainAccountsFromActIndexId=0L;
		if(obj.getCoaMainAccountsFrom()!=null){
				this.coaMainAccountsFromActIndexId=obj.getCoaMainAccountsFrom().getActIndx();
		}
		this.coaMainAccountsToActIndexId=0L;
		if(obj.getCoaMainAccountsTo()!=null){
			this.coaMainAccountsToActIndexId=obj.getCoaMainAccountsTo().getActIndx();
		}
		this.coaFinancialDimensionsIndexId=0;
//		if(obj.getCoaFinancialDimensions()!=null){
//			this.coaFinancialDimensionsIndexId=obj.getCoaFinancialDimensions().getDimInxd();
//		}
		this.coaFinancialDimensionsFromIndexValue=0L;
		if(obj.getCoaFinancialDimensionsValuesFrom()!=null){
			this.coaFinancialDimensionsFromIndexValue=obj.getCoaFinancialDimensionsValuesFrom().getDimInxValue();
		}
		this.coaFinancialDimensionsToIndexValue=0L;
		if(obj.getCoaFinancialDimensionsValuesTo()!=null){
			this.coaFinancialDimensionsToIndexValue=obj.getCoaFinancialDimensionsValuesTo().getDimInxValue();
		}
	}
	
	public DtoGLConfigurationRelationBetweenDimensionsAndMainAccount() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSegmentNumber() {
		return segmentNumber;
	}

	public void setSegmentNumber(Integer segmentNumber) {
		this.segmentNumber = segmentNumber;
	}

	public Integer getIsMainAccount() {
		return isMainAccount;
	}

	public void setIsMainAccount(Integer isMainAccount) {
		this.isMainAccount = isMainAccount;
	}

	public Long getCoaMainAccountsFromActIndexId() {
		return coaMainAccountsFromActIndexId;
	}

	public void setCoaMainAccountsFromActIndexId(Long coaMainAccountsFromActIndexId) {
		this.coaMainAccountsFromActIndexId = coaMainAccountsFromActIndexId;
	}

	public Long getCoaMainAccountsToActIndexId() {
		return coaMainAccountsToActIndexId;
	}

	public void setCoaMainAccountsToActIndexId(Long coaMainAccountsToActIndexId) {
		this.coaMainAccountsToActIndexId = coaMainAccountsToActIndexId;
	}

	public Integer getCoaFinancialDimensionsIndexId() {
		return coaFinancialDimensionsIndexId;
	}

	public void setCoaFinancialDimensionsIndexId(Integer coaFinancialDimensionsIndexId) {
		this.coaFinancialDimensionsIndexId = coaFinancialDimensionsIndexId;
	}

	public Long getCoaFinancialDimensionsFromIndexValue() {
		return coaFinancialDimensionsFromIndexValue;
	}

	public void setCoaFinancialDimensionsFromIndexValue(Long coaFinancialDimensionsFromIndexValue) {
		this.coaFinancialDimensionsFromIndexValue = coaFinancialDimensionsFromIndexValue;
	}

	public Long getCoaFinancialDimensionsToIndexValue() {
		return coaFinancialDimensionsToIndexValue;
	}

	public void setCoaFinancialDimensionsToIndexValue(Long coaFinancialDimensionsToIndexValue) {
		this.coaFinancialDimensionsToIndexValue = coaFinancialDimensionsToIndexValue;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public List<DtoGLConfigurationRelationBetweenDimensionsAndMainAccount> getDimensionList() {
		return dimensionList;
	}

	public void setDimensionList(List<DtoGLConfigurationRelationBetweenDimensionsAndMainAccount> dimensionList) {
		this.dimensionList = dimensionList;
	}
	
}
