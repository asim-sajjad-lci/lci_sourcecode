/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ar40003 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar40003")
@NamedQuery(name="RMDocumentsTypeSetup.findAll", query="SELECT a FROM RMDocumentsTypeSetup a")
public class RMDocumentsTypeSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ARDOCTYPE")
	private int arDocumentTypeId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DOCDESCR")
	private String documentTypeDescription;

	@Column(name="DOCDESCRA")
	private String documentTypeDescriptionArabic;

	@Column(name="DOCNUMBR")
	private int documentNumber;

	@Column(name="DOCODE")
	private String documentCode;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="DOCTYPE")
	private String documentType;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	public RMDocumentsTypeSetup() {
	}

	public int getArDocumentTypeId() {
		return arDocumentTypeId;
	}

	public void setArDocumentTypeId(int arDocumentTypeId) {
		this.arDocumentTypeId = arDocumentTypeId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}

	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	public String getDocumentTypeDescriptionArabic() {
		return documentTypeDescriptionArabic;
	}

	public void setDocumentTypeDescriptionArabic(String documentTypeDescriptionArabic) {
		this.documentTypeDescriptionArabic = documentTypeDescriptionArabic;
	}

	public int getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(int documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	

}