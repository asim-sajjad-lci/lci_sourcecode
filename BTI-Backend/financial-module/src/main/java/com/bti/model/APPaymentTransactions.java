/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ap20200 database table.
 * 
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap20200")
@NamedQuery(name = "APPaymentTransactions.findAll", query = "SELECT a FROM APPaymentTransactions a")
public class APPaymentTransactions implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "APROWID")
	private Integer apRowIndexId;
	
	@Column(name = "APTRXNMBR")
	private String apTransactionNumber;

	@Column(name = "APTRXDT")
	private Date apTransactionDate;

	@Column(name = "APTRXPODT")
	private Date apTransactionPostingDate;
	
	@Column(name = "VENDORID")
	private String vendorId;

	@Column(name = "CHEKBOKID")
	private String checkbookId;

	@Column(name = "CURNCYID")
	private String currencyId;

	@Column(name = "EXGTBLID")
	private String exchangeTableIndex;
	
	@Column(name = "APPMTAMT")
	private double apPaymentAmount;

	@Column(name = "APPMTFAMT")
	private double apFunctionalPaymentAmount;

	@Column(name = "AUDTTRALC")
	private String auditTrialCode;
	 
	@Column(name = "PMTVOID")
	private boolean paymentVoid;

	@Column(name = "DEX_ROW_TS")
	private Date rowDateindex;

	@Column(name = "DEX_ROW_ID")
	private int rowIndexId;

	public Integer getApRowIndexId() {
		return apRowIndexId;
	}

	public void setApRowIndexId(Integer apRowIndexId) {
		this.apRowIndexId = apRowIndexId;
	}

	public String getApTransactionNumber() {
		return apTransactionNumber;
	}

	public void setApTransactionNumber(String apTransactionNumber) {
		this.apTransactionNumber = apTransactionNumber;
	}

	public Date getApTransactionDate() {
		return apTransactionDate;
	}

	public void setApTransactionDate(Date apTransactionDate) {
		this.apTransactionDate = apTransactionDate;
	}

	public Date getApTransactionPostingDate() {
		return apTransactionPostingDate;
	}

	public void setApTransactionPostingDate(Date apTransactionPostingDate) {
		this.apTransactionPostingDate = apTransactionPostingDate;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public double getApPaymentAmount() {
		return apPaymentAmount;
	}

	public void setApPaymentAmount(double apPaymentAmount) {
		this.apPaymentAmount = apPaymentAmount;
	}

	public double getApFunctionalPaymentAmount() {
		return apFunctionalPaymentAmount;
	}

	public void setApFunctionalPaymentAmount(double apFunctionalPaymentAmount) {
		this.apFunctionalPaymentAmount = apFunctionalPaymentAmount;
	}

	public String getAuditTrialCode() {
		return auditTrialCode;
	}

	public void setAuditTrialCode(String auditTrialCode) {
		this.auditTrialCode = auditTrialCode;
	}

	public boolean isPaymentVoid() {
		return paymentVoid;
	}

	public void setPaymentVoid(boolean paymentVoid) {
		this.paymentVoid = paymentVoid;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

}