/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Description: The persistent class for the state_master database table.
 * Name of Project: BTI
 * Created on: June 20, 2017
 * Modified on: June 20, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */ 
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "state_master")
@NamedQuery(name = "StateMaster.findAll", query = "SELECT s FROM StateMaster s")
public class StateMaster extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "state_id")
	private int stateId;

	@Column(name = "state_name")
	private String stateName;
	
	@Column(name = "state_code")
	private String stateCode;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	/*@Column(name = "state_name_secondary")
	private String stateNameSecondary;*/

	// bi-directional many-to-one association to CityMaster
	@OneToMany(mappedBy = "stateMaster")
	private List<CityMaster> cityMasters;

	// bi-directional many-to-one association to CountryMaster
	@ManyToOne
	@JoinColumn(name = "country_id")
	private CountryMaster countryMaster;
	
	//bi-directional many-to-one association to Fa40005
	@OneToMany(mappedBy="stateMaster")
	private List<FALocationSetup> fa40005s;
	
	@ManyToOne
	@JoinColumn(name="lang_Id")
	private Language language;

	public StateMaster() {
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public List<CityMaster> getCityMasters() {
		return this.cityMasters;
	}

	public void setCityMasters(List<CityMaster> cityMasters) {
		this.cityMasters = cityMasters;
	}

	public CityMaster addCityMaster(CityMaster cityMaster) {
		getCityMasters().add(cityMaster);
		cityMaster.setStateMaster(this);

		return cityMaster;
	}

	public CityMaster removeCityMaster(CityMaster cityMaster) {
		getCityMasters().remove(cityMaster);
		cityMaster.setStateMaster(null);

		return cityMaster;
	}


	public CountryMaster getCountryMaster() {
		return this.countryMaster;
	}

	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}

	public List<FALocationSetup> getFa40005s() {
		return fa40005s;
	}

	public void setFa40005s(List<FALocationSetup> fa40005s) {
		this.fa40005s = fa40005s;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
}