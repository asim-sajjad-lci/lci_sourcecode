/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FALeaseCompanySetup;
import com.bti.util.UtilRandomKey;

public class DtoLeaseCompanySetup {

	private int companyIndex;
	private String companyId;
	private String vendorId;
	private String vendorName;
	private String companyName;
	
	public DtoLeaseCompanySetup(){
		
	}
	
	public DtoLeaseCompanySetup(FALeaseCompanySetup faLeaseCompanySetup){
		this.companyIndex = faLeaseCompanySetup.getLeaseCompanyIndex();
		this.companyId = faLeaseCompanySetup.getCompanyId();
		this.companyName="";
		if(UtilRandomKey.isNotBlank(faLeaseCompanySetup.getCompanyName())){
			this.companyName=faLeaseCompanySetup.getCompanyName();
		}
		if(faLeaseCompanySetup.getVendorMaintenance()!=null){
			this.vendorId = faLeaseCompanySetup.getVendorMaintenance().getVendorid();
			this.vendorName = faLeaseCompanySetup.getVendorName();
		}
		else
		{
			this.vendorId = "";
			this.vendorName = "";
		}
	}
	
	public int getCompanyIndex() {
		return companyIndex;
	}
	
	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}
	
	public String getCompanyId() {
		return companyId;
	}
	
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	public String getVendorId() {
		return vendorId;
	}
	
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	
	public String getVendorName() {
		return vendorName;
	}
	
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
