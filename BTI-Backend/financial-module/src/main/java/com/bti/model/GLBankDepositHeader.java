/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10600 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10600")
@NamedQuery(name="GLBankDepositHeader.findAll", query="SELECT a FROM GLBankDepositHeader a")
public class GLBankDepositHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DEPSNO")
	private int depositNumber;
	
	@Column(name="DEPDT")
	private Date depositDate;
	
	
	@Column(name="DEPDSCR")
	private String depositDescription;

	@Column(name="DEPTYPE")
	private int depositType;
	
	@ManyToOne
	@JoinColumn(name="CHEKBOKID")
	private CheckbookMaintenance checkbookMaintenance;

	@Column(name="CURNCYID")
	private String currencyID;
	
	@ManyToOne
	@JoinColumn(name="BATCHID")
	private GLBatches glBatches;
	
	@Column(name="EXGTBLINXD")
	private int exchangeTableIndex;
	
	@Column(name="XCHGRATE")
	private double exchangeTableRate;
	
	@Column(name="CREATDDT")
	private Date createDate;

	@Column(name="MODFITDT")
	private Date modifyDate;

	@Column(name="CHANGEBY")
	private String modifyByUserID ;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex ;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex ;

	
	public int getDepositNumber() {
		return depositNumber;
	}

	public void setDepositNumber(int depositNumber) {
		this.depositNumber = depositNumber;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public String getDepositDescription() {
		return depositDescription;
	}

	public void setDepositDescription(String depositDescription) {
		this.depositDescription = depositDescription;
	}

	public int getDepositType() {
		return depositType;
	}

	public void setDepositType(int depositType) {
		this.depositType = depositType;
	}
	
	public CheckbookMaintenance getCheckbookMaintenance() {
		return checkbookMaintenance;
	}

	public void setCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookMaintenance = checkbookMaintenance;
	}

	public GLBatches getGlBatches() {
		return glBatches;
	}

	public void setGlBatches(GLBatches glBatches) {
		this.glBatches = glBatches;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}        

	
	
	

}