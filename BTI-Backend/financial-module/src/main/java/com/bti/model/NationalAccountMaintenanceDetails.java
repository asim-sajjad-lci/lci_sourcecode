/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the `ar00300-detail` database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name="ar00300detail")
@NamedQuery(name="NationalAccountMaintenanceDetails.findAll", query="SELECT a FROM NationalAccountMaintenanceDetails a")
public class NationalAccountMaintenanceDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CUSTNMBRCHLD")
	private int customerNumberChildrenIndex;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATEDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createDate;

	@Column(name="CUSTBLANC")
	private Double customerChildrenBalance;

	@Column(name="CUSTNAMEC")
	private String customerNameChildren;

	@Column(name="CUSTNAMECA")
	private String CustomerNameChildrenArabic;

	@ManyToOne
	@JoinColumn(name="CUSTNMBRC")
	private CustomerMaintenance customerMaintenance;
	

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="SEQUANCE")
	private int sequenceChildrenId;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ar00101
	@ManyToOne
	@JoinColumn(name="CUSTNMBR")
	private NationalAccountMaintenanceHeader accountMaintenanceHeader;

	public NationalAccountMaintenanceDetails() {
	}

	public int getCustomerNumberChildrenIndex() {
		return customerNumberChildrenIndex;
	}

	public void setCustomerNumberChildrenIndex(int customerNumberChildrenIndex) {
		this.customerNumberChildrenIndex = customerNumberChildrenIndex;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Double getCustomerChildrenBalance() {
		return customerChildrenBalance;
	}

	public void setCustomerChildrenBalance(Double customerChildrenBalance) {
		this.customerChildrenBalance = customerChildrenBalance;
	}

	public String getCustomerNameChildren() {
		return customerNameChildren;
	}

	public void setCustomerNameChildren(String customerNameChildren) {
		this.customerNameChildren = customerNameChildren;
	}

	public String getCustomerNameChildrenArabic() {
		return CustomerNameChildrenArabic;
	}

	public void setCustomerNameChildrenArabic(String customerNameChildrenArabic) {
		CustomerNameChildrenArabic = customerNameChildrenArabic;
	}

	public CustomerMaintenance getCustomerMaintenance() {
		return customerMaintenance;
	}

	public void setCustomerMaintenance(CustomerMaintenance customerMaintenance) {
		this.customerMaintenance = customerMaintenance;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getSequenceChildrenId() {
		return sequenceChildrenId;
	}

	public void setSequenceChildrenId(int sequenceChildrenId) {
		this.sequenceChildrenId = sequenceChildrenId;
	}

	public NationalAccountMaintenanceHeader getAccountMaintenanceHeader() {
		return accountMaintenanceHeader;
	}

	public void setAccountMaintenanceHeader(NationalAccountMaintenanceHeader accountMaintenanceHeader) {
		this.accountMaintenanceHeader = accountMaintenanceHeader;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	



	
}