package com.bti.model.dto;

import com.bti.model.GLBankTransferHeader;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoGLBankTransferHeader {
 
	private int bankTransferNumber;
	private String transferDate;
	private String batchID;
	private String currencyID;
	private String exchangeTableID;
	
	private String companyTransferFrom;
	private String checkbookIDFrom;
	private String commentFrom;
	private double transferAmountFrom;
	private int rateCalculateMethodFrom;
	private double exchangeRateFrom;
	
	private String companyTransferTo;
	private String checkbookIDTo ;
	private String commentTo ;
	private double transferAmountTo ;
	private int rateCalculateMethodTo ;
	private double exchangeRateTo ;
	
	public DtoGLBankTransferHeader() {
		// to do nothing
	}
	public DtoGLBankTransferHeader(GLBankTransferHeader glBankTransferHeader) {
		this.setBankTransferNumber(glBankTransferHeader.getBankTransferNumber());
		
		
		
		this.setCheckbookIDFrom(glBankTransferHeader.getCheckBookIdFrom());
		this.setCheckbookIDTo(glBankTransferHeader.getCheckbookIdTo());
		this.setCommentFrom(glBankTransferHeader.getCommentFrom());
		this.setCommentTo(glBankTransferHeader.getCommentTo());
		this.setCompanyTransferFrom(glBankTransferHeader.getCompanyTransferFrom());
		this.setCompanyTransferTo(glBankTransferHeader.getCompanyTransferTo());
		this.setCurrencyID(glBankTransferHeader.getCurrencyId());
		this.setExchangeRateFrom(UtilRoundDecimal.roundDecimalValue(glBankTransferHeader.getExchangeRateFrom()));
		this.setExchangeRateTo(UtilRoundDecimal.roundDecimalValue(glBankTransferHeader.getExchangeRateTo()));
		this.setExchangeTableID(glBankTransferHeader.getExchangeTableID());
		this.setBatchID(glBankTransferHeader.getGlBatches()!=null?glBankTransferHeader.getGlBatches().getBatchID():"");
		this.setRateCalculateMethodFrom(glBankTransferHeader.getRateCalculateMethodFrom());
		this.setRateCalculateMethodTo(glBankTransferHeader.getRateCalculateMethodTo());
		this.setTransferAmountFrom(UtilRoundDecimal.roundDecimalValue(glBankTransferHeader.getTransferAmountFrom()));
		this.setTransferAmountTo(UtilRoundDecimal.roundDecimalValue(glBankTransferHeader.getTransferAmountTo()));
		this.setTransferDate(glBankTransferHeader.getTransferDate()!=null?UtilDateAndTime.dateToStringddmmyyyy(glBankTransferHeader.getTransferDate()):"");
	}
	public int getBankTransferNumber() {
		return bankTransferNumber;
	}
	public void setBankTransferNumber(int bankTransferNumber) {
		this.bankTransferNumber = bankTransferNumber;
	}
	public String getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	public String getBatchID() {
		return batchID;
	}
	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}
	public String getCurrencyID() {
		return currencyID;
	}
	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}
	public String getExchangeTableID() {
		return exchangeTableID;
	}
	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}
	public String getCompanyTransferFrom() {
		return companyTransferFrom;
	}
	public void setCompanyTransferFrom(String companyTransferFrom) {
		this.companyTransferFrom = companyTransferFrom;
	}
	public String getCheckbookIDFrom() {
		return checkbookIDFrom;
	}
	public void setCheckbookIDFrom(String checkbookIDFrom) {
		this.checkbookIDFrom = checkbookIDFrom;
	}
	public String getCommentFrom() {
		return commentFrom;
	}
	public void setCommentFrom(String commentFrom) {
		this.commentFrom = commentFrom;
	}
	public double getTransferAmountFrom() {
		return transferAmountFrom;
	}
	public void setTransferAmountFrom(double transferAmountFrom) {
		this.transferAmountFrom = UtilRoundDecimal.roundDecimalValue(transferAmountFrom);
	}
	public int getRateCalculateMethodFrom() {
		return rateCalculateMethodFrom;
	}
	public void setRateCalculateMethodFrom(int rateCalculateMethodFrom) {
		this.rateCalculateMethodFrom = rateCalculateMethodFrom;
	}
	public double getExchangeRateFrom() {
		return exchangeRateFrom;
	}
	public void setExchangeRateFrom(double exchangeRateFrom) {
		this.exchangeRateFrom = UtilRoundDecimal.roundDecimalValue(exchangeRateFrom);
	}
	public String getCompanyTransferTo() {
		return companyTransferTo;
	}
	public void setCompanyTransferTo(String companyTransferTo) {
		this.companyTransferTo = companyTransferTo;
	}
	public String getCheckbookIDTo() {
		return checkbookIDTo;
	}
	public void setCheckbookIDTo(String checkbookIDTo) {
		this.checkbookIDTo = checkbookIDTo;
	}
	public String getCommentTo() {
		return commentTo;
	}
	public void setCommentTo(String commentTo) {
		this.commentTo = commentTo;
	}
	public double getTransferAmountTo() {
		return transferAmountTo;
	}
	public void setTransferAmountTo(double transferAmountTo) {
		this.transferAmountTo = UtilRoundDecimal.roundDecimalValue(transferAmountTo);
	}
	public int getRateCalculateMethodTo() {
		return rateCalculateMethodTo;
	}
	public void setRateCalculateMethodTo(int rateCalculateMethodTo) {
		this.rateCalculateMethodTo = rateCalculateMethodTo;
	}
	public double getExchangeRateTo() {
		return exchangeRateTo;
	}
	public void setExchangeRateTo(double exchangeRateTo) {
		this.exchangeRateTo = UtilRoundDecimal.roundDecimalValue(exchangeRateTo);
	}
	
}
