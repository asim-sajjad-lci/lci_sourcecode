/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar90700 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar90700")
@NamedQuery(name="ARHistoryTransactionsCommissions.findAll", query="SELECT a FROM ARHistoryTransactionsCommissions a")
public class ARHistoryTransactionsCommissions implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ARTRXNO")
	private String aRTransactionNumber;
	
	@Column(name="SALSPERID")
	private String salesmanID;
	
	@Column(name="CUSTNMBR")
	private String customerNumber;

	@Column(name="COMMAPPL")
	private int arTransactionCommissionApplied;

	@Column(name="COMSEQC")
	private int commissionSequence;
	
	@Column(name="PERSALE")
	private Double percentOfSales;
	
	@Column(name="COMSLSAMT")
	private Double commissionSalesAmount;
	
	@Column(name="COMAMT")
	private Double commissionAmountOfSalesman;
	//
	@Column(name="COMPER")
	private Double commissionPercentOfSalesman;
	
	@Column(name="YEAR1")
	private int year;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public String getaRTransactionNumber() {
		return aRTransactionNumber;
	}

	public void setaRTransactionNumber(String aRTransactionNumber) {
		this.aRTransactionNumber = aRTransactionNumber;
	}

	public String getSalesmanID() {
		return salesmanID;
	}

	public void setSalesmanID(String salesmanID) {
		this.salesmanID = salesmanID;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public int getArTransactionCommissionApplied() {
		return arTransactionCommissionApplied;
	}

	public void setArTransactionCommissionApplied(int arTransactionCommissionApplied) {
		this.arTransactionCommissionApplied = arTransactionCommissionApplied;
	}

	public int getCommissionSequence() {
		return commissionSequence;
	}

	public void setCommissionSequence(int commissionSequence) {
		this.commissionSequence = commissionSequence;
	}

	public Double getPercentOfSales() {
		return percentOfSales;
	}

	public void setPercentOfSales(Double percentOfSales) {
		this.percentOfSales = percentOfSales;
	}

	public Double getCommissionSalesAmount() {
		return commissionSalesAmount;
	}

	public void setCommissionSalesAmount(Double commissionSalesAmount) {
		this.commissionSalesAmount = commissionSalesAmount;
	}

	public Double getCommissionAmountOfSalesman() {
		return commissionAmountOfSalesman;
	}

	public void setCommissionAmountOfSalesman(Double commissionAmountOfSalesman) {
		this.commissionAmountOfSalesman = commissionAmountOfSalesman;
	}

	public Double getCommissionPercentOfSalesman() {
		return commissionPercentOfSalesman;
	}

	public void setCommissionPercentOfSalesman(Double commissionPercentOfSalesman) {
		this.commissionPercentOfSalesman = commissionPercentOfSalesman;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}
	
	

	}