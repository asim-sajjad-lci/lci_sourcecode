/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the mc40200 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "mc40200")
@NamedQuery(name="CurrencySetup.findAll", query="SELECT m FROM CurrencySetup m")
public class CurrencySetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CURRNIDX")
	private int currencyIndex;
	
	@Column(name="CURNCYID")
	private String currencyId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="CRNCYDSC")
	private String currencyDescription;

	@Column(name="CRNCYDSCA")
	private String currencyDescriptionArabic;

	@Column(name="CRNCYSYM")
	private String currencySymbol;

	@Column(name="CURTEXT_1")
	private String currencyUnit;

	@Column(name="CURTEXT_2")
	private String unitSubunitConnector;

	@Column(name="CURTEXT_3")
	private String currencySubunit;

	@Column(name="CURTEXTA_1")
	private String currencyUnitArabic;

	@Column(name="CURTEXTA_2")
	private String unitSubunitConnectorArabic;

	@Column(name="CURTEXTA_3")
	private String currencySubunitArabic;
	
	@ManyToOne
	@JoinColumn(name = "DECSYMBL")
	private MasterSeperatorDecimal masterSeperatorDecimal;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="INCLSPAC")
	private Boolean includeSpaceAfterCurrencySymbol;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	@Column(name="ISDFLT" , columnDefinition = "tinyint(0) default 0")
	private boolean isDefault;

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	@ManyToOne
	@JoinColumn(name = "NEGSYMBL")
	private MasterNegativeSymbolTypes masterNegativeSymbolTypes;
	
	@ManyToOne
	@JoinColumn(name = "NGSMAMPC")
	private MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes;

	@ManyToOne
	@JoinColumn(name = "THOUSSYM")
	private MasterSeperatorThousands masterSeperatorThousands;

	@Column(name = "DISCURSYMBL")
	private int displayCurrencySymbol;

	public CurrencySetup() {
	}

	public int getCurrencyIndex() {
		return currencyIndex;
	}

	public void setCurrencyIndex(int currencyIndex) {
		this.currencyIndex = currencyIndex;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCurrencyDescription() {
		return currencyDescription;
	}

	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	public String getCurrencyDescriptionArabic() {
		return currencyDescriptionArabic;
	}

	public void setCurrencyDescriptionArabic(String currencyDescriptionArabic) {
		this.currencyDescriptionArabic = currencyDescriptionArabic;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyUnit() {
		return currencyUnit;
	}

	public void setCurrencyUnit(String currencyUnit) {
		this.currencyUnit = currencyUnit;
	}

	public String getUnitSubunitConnector() {
		return unitSubunitConnector;
	}

	public void setUnitSubunitConnector(String unitSubunitConnector) {
		this.unitSubunitConnector = unitSubunitConnector;
	}

	public String getCurrencySubunit() {
		return currencySubunit;
	}

	public void setCurrencySubunit(String currencySubunit) {
		this.currencySubunit = currencySubunit;
	}

	public String getCurrencyUnitArabic() {
		return currencyUnitArabic;
	}

	public void setCurrencyUnitArabic(String currencyUnitArabic) {
		this.currencyUnitArabic = currencyUnitArabic;
	}

	public String getUnitSubunitConnectorArabic() {
		return unitSubunitConnectorArabic;
	}

	public void setUnitSubunitConnectorArabic(String unitSubunitConnectorArabic) {
		this.unitSubunitConnectorArabic = unitSubunitConnectorArabic;
	}

	public String getCurrencySubunitArabic() {
		return currencySubunitArabic;
	}

	public void setCurrencySubunitArabic(String currencySubunitArabic) {
		this.currencySubunitArabic = currencySubunitArabic;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Boolean getIncludeSpaceAfterCurrencySymbol() {
		return includeSpaceAfterCurrencySymbol;
	}

	public void setIncludeSpaceAfterCurrencySymbol(Boolean includeSpaceAfterCurrencySymbol) {
		this.includeSpaceAfterCurrencySymbol = includeSpaceAfterCurrencySymbol;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public MasterSeperatorDecimal getMasterSeperatorDecimal() {
		return masterSeperatorDecimal;
	}

	public void setMasterSeperatorDecimal(MasterSeperatorDecimal masterSeperatorDecimal) {
		this.masterSeperatorDecimal = masterSeperatorDecimal;
	}

	public MasterNegativeSymbolTypes getMasterNegativeSymbolTypes() {
		return masterNegativeSymbolTypes;
	}

	public void setMasterNegativeSymbolTypes(MasterNegativeSymbolTypes masterNegativeSymbolTypes) {
		this.masterNegativeSymbolTypes = masterNegativeSymbolTypes;
	}

	public MasterNegativeSymbolSignTypes getMasterNegativeSymbolSignTypes() {
		return masterNegativeSymbolSignTypes;
	}

	public void setMasterNegativeSymbolSignTypes(MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes) {
		this.masterNegativeSymbolSignTypes = masterNegativeSymbolSignTypes;
	}

	public MasterSeperatorThousands getMasterSeperatorThousands() {
		return masterSeperatorThousands;
	}

	public void setMasterSeperatorThousands(MasterSeperatorThousands masterSeperatorThousands) {
		this.masterSeperatorThousands = masterSeperatorThousands;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getDisplayCurrencySymbol() {
		return displayCurrencySymbol;
	}

	public void setDisplayCurrencySymbol(int displayCurrencySymbol) {
		this.displayCurrencySymbol = displayCurrencySymbol;
	}
	
}