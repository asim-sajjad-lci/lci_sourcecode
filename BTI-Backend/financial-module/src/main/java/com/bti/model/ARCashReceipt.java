/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar10300 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar10300")
@NamedQuery(name="ARCashReceipt.findAll", query="SELECT a FROM ARCashReceipt a")
public class ARCashReceipt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CSHRCPTNO")
	private String cashReceiptNumber;
	
	@ManyToOne
	@JoinColumn(name="BATCHID")
	private ARBatches arBatches;
	
	@Column(name="CUSTNMBR")
	private String customerNumber;

	@Column(name="CUSTNAME")
	private String customerName;

	@Column(name="CUSTNAMEA")
	private String customerNameArabic;
	
	@Column(name="CURNCYID")
	private String currencyID;
	 
	@ManyToOne
	@JoinColumn(name="CSHTYPE")
	private MasterARCashReceiptType masterARCashReceiptType;
	
	@Column(name="CSHAMNT")
	private Double cashReceiptAmount;
	//
	@Column(name="CHEKBOKID")
	private String checkbookID;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	
	@Column(name="CSHCHEKT")
	private String checkNumber;
	//
	@Column(name="CARDEXPTM")
	private int creditCardExpireMonth;
	
	@Column(name="CARDEXPTY")
	private int creditCardExpireYear;
	
	@Column(name="CSHDSCRP")
	private String cashReceiptDescription;
	
	@Column(name="CSHDDT")
	private Date cashReceiptCreateDate;
	
	@Column(name="CSHPST")
	private Date cashReceiptPostingDate;
	
	@Column(name="TRXVOID")
	private int transactionVoid;
	
	@Column(name="EXGTBLID")
	private String exchangeTableID;
	
	@Column(name="XCHGRATE")
	private double exchangeTableRate;
	
	@Column(name="CREATDDT")
	private Date createDate;
	
	@Column(name="MODIFTDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifybyUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public ARBatches getArBatches() {
		return arBatches;
	}

	public void setArBatches(ARBatches arBatches) {
		this.arBatches = arBatches;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNameArabic() {
		return customerNameArabic;
	}

	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public MasterARCashReceiptType getMasterARCashReceiptType() {
		return masterARCashReceiptType;
	}

	public void setMasterARCashReceiptType(MasterARCashReceiptType masterARCashReceiptType) {
		this.masterARCashReceiptType = masterARCashReceiptType;
	}

	public Double getCashReceiptAmount() {
		return cashReceiptAmount;
	}

	public void setCashReceiptAmount(Double cashReceiptAmount) {
		this.cashReceiptAmount = cashReceiptAmount;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getCashReceiptDescription() {
		return cashReceiptDescription;
	}

	public void setCashReceiptDescription(String cashReceiptDescription) {
		this.cashReceiptDescription = cashReceiptDescription;
	}

	public Date getCashReceiptCreateDate() {
		return cashReceiptCreateDate;
	}

	public void setCashReceiptCreateDate(Date cashReceiptCreateDate) {
		this.cashReceiptCreateDate = cashReceiptCreateDate;
	}

	public Date getCashReceiptPostingDate() {
		return cashReceiptPostingDate;
	}

	public void setCashReceiptPostingDate(Date cashReceiptPostingDate) {
		this.cashReceiptPostingDate = cashReceiptPostingDate;
	}

	public int getTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(int transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getExchangeTableID() {
		return exchangeTableID;
	}

	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}

	public double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifybyUserID() {
		return modifybyUserID;
	}

	public void setModifybyUserID(String modifybyUserID) {
		this.modifybyUserID = modifybyUserID;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}
	
	
	}