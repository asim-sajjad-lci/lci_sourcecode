package com.bti.model.dto;

import com.bti.model.GLYTDOpenTransactions;
import com.bti.util.UtilDateAndTime;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoGLYTDOpenTransactions {
	
	
	
	private int rowIndex;
	
	private String journalEntryID;
	
	private String journalDescription;
	
	private String journalDescriptionArabic;
	
	private String transactionDate;
	
	private String transactionPostingDate;
	
	private String transaxtionSource;
	
	private String accountTableRowIndex;
	
	private Double debitAmount;
	
	private Double creadiAmount;
	
	private Double peroidBalance;
	
	private Double totalDebitAmmount;
	
	private Double totalCreditAmmount;
	
	private Double balance;
	
	private String currencyId;
	
	private int sourceDocumentId;
	private String sourceDocument;
	
	private String originalTransactionNumber;
	
	public DtoGLYTDOpenTransactions(GLYTDOpenTransactions glytdOpenTransactions) {
		this.rowIndex = glytdOpenTransactions.getRowIndex();
		this.journalEntryID = glytdOpenTransactions.getJournalEntryID();
		this.journalDescription = glytdOpenTransactions.getJournalDescription();
		this.journalDescriptionArabic = glytdOpenTransactions.getJournalDescriptionArabic();
		if(glytdOpenTransactions.getTransactionDate()!=null){
			this.transactionDate= UtilDateAndTime.dateToStringddmmyyyy(glytdOpenTransactions.getTransactionDate());
		}
		if(glytdOpenTransactions.getTransactionPostingDate()!=null){
			this.transactionPostingDate= UtilDateAndTime.dateToStringddmmyyyy(glytdOpenTransactions.getTransactionPostingDate());
		}
		this.transaxtionSource = glytdOpenTransactions.getTransactionSource();
		this.accountTableRowIndex = glytdOpenTransactions.getAccountTableRowIndex();
		this.debitAmount = glytdOpenTransactions.getDebitAmount();
		this.creadiAmount = glytdOpenTransactions.getDebitAmount();
		this.currencyId = glytdOpenTransactions.getCurrencyId();
		this.totalDebitAmmount = glytdOpenTransactions.getTotalJournalEntryDebit();
		this.totalCreditAmmount = glytdOpenTransactions.getTotalJournalEntryCredit();
		this.sourceDocument  = glytdOpenTransactions.getGlConfigurationAuditTrialCodes().getSourceDocument();
		this.sourceDocumentId = glytdOpenTransactions.getGlConfigurationAuditTrialCodes().getSeriesIndex();
		this.originalTransactionNumber =glytdOpenTransactions.getOriginalTransactionNumber();
	}
	public DtoGLYTDOpenTransactions() {
		// TODO Auto-generated constructor stub
		this.debitAmount = 0.0;
		this.creadiAmount = 0.0;
		this.peroidBalance = 0.0;
		this.balance = 0.0;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public String getJournalEntryID() {
		return journalEntryID;
	}

	public void setJournalEntryID(String journalEntryID) {
		this.journalEntryID = journalEntryID;
	}

	public String getJournalDescription() {
		return journalDescription;
	}

	public void setJournalDescription(String journalDescription) {
		this.journalDescription = journalDescription;
	}

	public String getJournalDescriptionArabic() {
		return journalDescriptionArabic;
	}

	public void setJournalDescriptionArabic(String journalDescriptionArabic) {
		this.journalDescriptionArabic = journalDescriptionArabic;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionPostingDate() {
		return transactionPostingDate;
	}

	public void setTransactionPostingDate(String transactionPostingDate) {
		this.transactionPostingDate = transactionPostingDate;
	}

	public String getTransaxtionSource() {
		return transaxtionSource;
	}

	public void setTransaxtionSource(String transaxtionSource) {
		this.transaxtionSource = transaxtionSource;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getCreadiAmount() {
		return creadiAmount;
	}

	public void setCreadiAmount(Double creadiAmount) {
		this.creadiAmount = creadiAmount;
	}
	
	public Double getPeroidBalance() {
		return peroidBalance;
	}
	
	public void setPeroidBalance(Double peroidBalance) {
		this.peroidBalance = peroidBalance;
	}
	public Double getTotalDebitAmmount() {
		return totalDebitAmmount;
	}
	public void setTotalDebitAmmount(Double totalDebitAmmount) {
		this.totalDebitAmmount = totalDebitAmmount;
	}
	public String getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	public Double getTotalCreditAmmount() {
		return totalCreditAmmount;
	}
	public void setTotalCreditAmmount(Double totalCreditAmmount) {
		this.totalCreditAmmount = totalCreditAmmount;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public int getSourceDocumentId() {
		return sourceDocumentId;
	}
	public void setSourceDocumentId(int sourceDocumentId) {
		this.sourceDocumentId = sourceDocumentId;
	}
	public String getSourceDocument() {
		return sourceDocument;
	}
	public void setSourceDocument(String sourceDocument) {
		this.sourceDocument = sourceDocument;
	}
	public String getOriginalTransactionNumber() {
		return originalTransactionNumber;
	}
	public void setOriginalTransactionNumber(String originalTransactionNumber) {
		this.originalTransactionNumber = originalTransactionNumber;
	}
	
	
	
	

}
