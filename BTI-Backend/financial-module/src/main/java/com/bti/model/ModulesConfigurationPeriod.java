/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the sy40000 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "sy40000")
@NamedQuery(name="ModulesConfigurationPeriod.findAll", query="SELECT s FROM ModulesConfigurationPeriod s")
public class ModulesConfigurationPeriod implements Serializable {
	private static final long serialVersionUID = 1L;
 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="SERIES")
	private GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetails;
	
	@ManyToOne
	@JoinColumn(name="SERIES_TYPE")
	private SeriesType seriesType;
	
	@Column(name="YEAR")
	private int year;
	
	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="MODLDECR")
	private String moduleDescription;

	@Column(name="MODLDECRA")
	private String moduleDescriptionArabic;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	@Column(name="ORGINID")
	private int originId;

	@Column(name="ORTRXN")
	private String originTransactionDescription;

	@Column(name="ORTRXNA")
	private String originTransactionDescriptionArabic;

	@Column(name="PERCLOSE")
	private Boolean closePeriod;

	public ModulesConfigurationPeriod() {
	}

	public GLConfigurationDefinePeriodsDetails getGlConfigurationDefinePeriodsDetails() {
		return glConfigurationDefinePeriodsDetails;
	}

	public void setGlConfigurationDefinePeriodsDetails(
			GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetails) {
		this.glConfigurationDefinePeriodsDetails = glConfigurationDefinePeriodsDetails;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModuleDescription() {
		return moduleDescription;
	}

	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}

	public String getModuleDescriptionArabic() {
		return moduleDescriptionArabic;
	}

	public void setModuleDescriptionArabic(String moduleDescriptionArabic) {
		this.moduleDescriptionArabic = moduleDescriptionArabic;
	}

	public String getOriginTransactionDescription() {
		return originTransactionDescription;
	}

	public void setOriginTransactionDescription(String originTransactionDescription) {
		this.originTransactionDescription = originTransactionDescription;
	}

	public String getOriginTransactionDescriptionArabic() {
		return originTransactionDescriptionArabic;
	}

	public void setOriginTransactionDescriptionArabic(String originTransactionDescriptionArabic) {
		this.originTransactionDescriptionArabic = originTransactionDescriptionArabic;
	}

	public Boolean getClosePeriod() {
		return closePeriod;
	}

	public void setClosePeriod(Boolean closePeriod) {
		this.closePeriod = closePeriod;
	}

	public SeriesType getSeriesType() {
		return seriesType;
	}

	public void setSeriesType(SeriesType seriesType) {
		this.seriesType = seriesType;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getOriginId() {
		return originId;
	}

	public void setOriginId(int originId) {
		this.originId = originId;
	}

}