/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the sy03600 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "sy03600")
@NamedQuery(name="VATSetup.findAll", query="SELECT s FROM VATSetup s")
public class VATSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TAXSCHDID")
	private String vatScheduleId;

	@Column(name="ACTROWID")
	private int accountRowId;

	/*@Column(name="BASDID")
	private int vatBaseOn;*/

	@ManyToOne
	@JoinColumn(name="BASDID")
	private VATBasedOnType vatBasedOnType;
	
	@Column(name="BASPERCT")
	private Double basperct;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRIPTN")
	private String vatDescription;

	@Column(name="DSCRIPTNA")
	private String vatDescriptionArabic;

	@Column(name="LSTTOTSAL")
	private Double lastYearTotalSalesPurchase;

	@Column(name="LSTTOTTAX")
	private Double lastYearSalesPurchaseTaxes;

	@Column(name="LSTTOTVAT")
	private Double lastYearTaxableSalesPurchase;

	@Column(name="MAXVATAMT")
	private Double maximumVATAmount;

	@Column(name="MINVATAMT")
	private Double minimumVATAmount;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="VATIDNUM")
	private String vatIdNumber;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
/*
	@Column(name="VATYP")
	private int VATSeriesType;*/
	
	@ManyToOne
	@JoinColumn(name="VATYP")
	private VATType vatType;
	

	@Column(name="YTDTOTSAL")
	private Double ytdTotalSalesPurchase;

	@Column(name="YTDTOTTAX")
	private Double ytdTotalSalesPurchaseTaxes;

	@Column(name="YTDTOTVAT")
	private Double ytdTotalTaxableSalesPurchase;

	//bi-directional many-to-one association to Ap00102
	@OneToMany(mappedBy="vatSetup")
	private List<VendorMaintenanceOptions> vendorMaintenanceOptions;

	//bi-directional many-to-one association to Ap00200
	@OneToMany(mappedBy="vatSetup")
	private List<VendorClassesSetup> vendorClassesSetups;

	//bi-directional many-to-one association to Ar00102
	@OneToMany(mappedBy="vatSetup")
	private List<CustomerMaintenanceOptions> customerMaintenanceOptions;

	//bi-directional many-to-one association to Ar00200
	@OneToMany(mappedBy="vatSetup")
	private List<CustomerClassesSetup> customerClassesSetups;

	public VATSetup() {
	}

	public String getVatScheduleId() {
		return vatScheduleId;
	}

	public void setVatScheduleId(String vatScheduleId) {
		this.vatScheduleId = vatScheduleId;
	}

	public int getAccountRowId() {
		return accountRowId;
	}

	public void setAccountRowId(int accountRowId) {
		this.accountRowId = accountRowId;
	}

	public Double getBasperct() {
		return basperct;
	}

	public void setBasperct(Double basperct) {
		this.basperct = basperct;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	 
 

	

	public Double getLastYearTotalSalesPurchase() {
		return lastYearTotalSalesPurchase;
	}

	public void setLastYearTotalSalesPurchase(Double lastYearTotalSalesPurchase) {
		this.lastYearTotalSalesPurchase = lastYearTotalSalesPurchase;
	}

	public Double getLastYearSalesPurchaseTaxes() {
		return lastYearSalesPurchaseTaxes;
	}

	public void setLastYearSalesPurchaseTaxes(Double lastYearSalesPurchaseTaxes) {
		this.lastYearSalesPurchaseTaxes = lastYearSalesPurchaseTaxes;
	}

	public Double getLastYearTaxableSalesPurchase() {
		return lastYearTaxableSalesPurchase;
	}

	public void setLastYearTaxableSalesPurchase(Double lastYearTaxableSalesPurchase) {
		this.lastYearTaxableSalesPurchase = lastYearTaxableSalesPurchase;
	}

	public Double getMaximumVATAmount() {
		return maximumVATAmount;
	}

	public void setMaximumVATAmount(Double maximumVATAmount) {
		this.maximumVATAmount = maximumVATAmount;
	}

	public Double getMinimumVATAmount() {
		return minimumVATAmount;
	}

	public void setMinimumVATAmount(Double minimumVATAmount) {
		this.minimumVATAmount = minimumVATAmount;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	 
	
	public VATType getVatType() {
		return vatType;
	}

	public void setVatType(VATType vatType) {
		this.vatType = vatType;
	}

	/*public int getVATSeriesType() {
		return VATSeriesType;
	}

	public void setVATSeriesType(int vATSeriesType) {
		VATSeriesType = vATSeriesType;
	}*/
	public void setVendorMaintenanceOptions(List<VendorMaintenanceOptions> vendorMaintenanceOptions) {
		this.vendorMaintenanceOptions = vendorMaintenanceOptions;
	}

	
	public List<VendorClassesSetup> getVendorClassesSetups() {
		return vendorClassesSetups;
	}

	public void setVendorClassesSetups(List<VendorClassesSetup> vendorClassesSetups) {
		this.vendorClassesSetups = vendorClassesSetups;
	}

	public List<CustomerMaintenanceOptions> getCustomerMaintenanceOptions() {
		return customerMaintenanceOptions;
	}

	public void setCustomerMaintenanceOptions(List<CustomerMaintenanceOptions> customerMaintenanceOptions) {
		this.customerMaintenanceOptions = customerMaintenanceOptions;
	}

	public List<CustomerClassesSetup> getCustomerClassesSetups() {
		return customerClassesSetups;
	}

	public void setCustomerClassesSetups(List<CustomerClassesSetup> customerClassesSetups) {
		this.customerClassesSetups = customerClassesSetups;
	}

	public String getVatDescription() {
		return vatDescription;
	}

	public void setVatDescription(String vatDescription) {
		this.vatDescription = vatDescription;
	}

	public String getVatDescriptionArabic() {
		return vatDescriptionArabic;
	}

	public void setVatDescriptionArabic(String vatDescriptionArabic) {
		this.vatDescriptionArabic = vatDescriptionArabic;
	}

	public String getVatIdNumber() {
		return vatIdNumber;
	}

	public void setVatIdNumber(String vatIdNumber) {
		this.vatIdNumber = vatIdNumber;
	}

	public Double getYtdTotalSalesPurchase() {
		return ytdTotalSalesPurchase;
	}

	public void setYtdTotalSalesPurchase(Double ytdTotalSalesPurchase) {
		this.ytdTotalSalesPurchase = ytdTotalSalesPurchase;
	}

	public Double getYtdTotalSalesPurchaseTaxes() {
		return ytdTotalSalesPurchaseTaxes;
	}

	public void setYtdTotalSalesPurchaseTaxes(Double ytdTotalSalesPurchaseTaxes) {
		this.ytdTotalSalesPurchaseTaxes = ytdTotalSalesPurchaseTaxes;
	}

	public Double getYtdTotalTaxableSalesPurchase() {
		return ytdTotalTaxableSalesPurchase;
	}

	public void setYtdTotalTaxableSalesPurchase(Double ytdTotalTaxableSalesPurchase) {
		this.ytdTotalTaxableSalesPurchase = ytdTotalTaxableSalesPurchase;
	}

	public List<VendorMaintenanceOptions> getVendorMaintenanceOptions() {
		return vendorMaintenanceOptions;
	}

	/*public int getVatBaseOn() {
		return vatBaseOn;
	}

	public void setVatBaseOn(int vatBaseOn) {
		this.vatBaseOn = vatBaseOn;
	}*/


	
	public VATBasedOnType getVatBasedOnType() {
		return vatBasedOnType;
	}

	public void setVatBasedOnType(VATBasedOnType vatBasedOnType) {
		this.vatBasedOnType = vatBasedOnType;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

	
}