/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ap40001 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap40001")
@NamedQuery(name="PMPeriodSetup.findAll", query="SELECT a FROM PMPeriodSetup a")
public class PMPeriodSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PRDINDX")
	private int periodIndexing;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PRDDAYS")
	private int periodNumberOfDays;

	@Column(name="PRDDESC")
	private String periodDescription;

	@Column(name="PRDDESCA")
	private String periodDescriptionArabic;

	@Column(name="PRDENDS")
	private int periodEnd;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	@ManyToOne
	@JoinColumn(name="PMSETUPID")
	PMSetup pmSetup;

	public PMPeriodSetup() {
	}

	public int getPeriodIndexing() {
		return periodIndexing;
	}

	public void setPeriodIndexing(int periodIndexing) {
		this.periodIndexing = periodIndexing;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getPeriodNumberOfDays() {
		return periodNumberOfDays;
	}

	public void setPeriodNumberOfDays(int periodNumberOfDays) {
		this.periodNumberOfDays = periodNumberOfDays;
	}

	public String getPeriodDescription() {
		return periodDescription;
	}

	public void setPeriodDescription(String periodDescription) {
		this.periodDescription = periodDescription;
	}

	public String getPeriodDescriptionArabic() {
		return periodDescriptionArabic;
	}

	public void setPeriodDescriptionArabic(String periodDescriptionArabic) {
		this.periodDescriptionArabic = periodDescriptionArabic;
	}

	public int getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(int periodEnd) {
		this.periodEnd = periodEnd;
	}

	public PMSetup getPmSetup() {
		return pmSetup;
	}

	public void setPmSetup(PMSetup pmSetup) {
		this.pmSetup = pmSetup;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	

	

}