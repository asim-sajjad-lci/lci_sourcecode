/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ap40002 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap40002")
@NamedQuery(name="PMSetup.findAll", query="SELECT a FROM PMSetup a")
public class PMSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="AGEBY")
	private int ageingBy;

	@Column(name="AGEUNAPLYCR")
	private boolean ageUnappliedCreditAmounts;

	@Column(name="APLBYDEF")
	private int applyByDefault;

	
	@Column(name="ALLOW_DUPLICATE_INVOICE_PERVENDOR")
	private int allowDuplicateInvoicePerVendor;
	
	@Column(name="APRTRCDISC")
	private Boolean apTrackingDiscountAvailable;

	@Column(name="CHANGEBY")
	private int changeBy;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

/*	@Column(name="CHEKBOKID")
	private String checkBookBankId;*/
	
	@ManyToOne
	@JoinColumn(name="CHEKBOKID")
	private CheckbookMaintenance checkbookMaintenance;

/*	@Column(name="CHKFORMTY")
	private int checkFormat;*/
	
	@ManyToOne
	@JoinColumn(name="CHKFORMTY")
	private PMSetupFormatType pmSetupFormatType;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DELVENDHLD")
	private String removeVendorHoldPassword;

	@Column(name="DEW_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DTLUNPSTPRT")
	private Boolean deleteUnpostedPrintedDocuments;

	@Column(name="EXCEDINV")
	private String exceedMaximumInvoiceAmount;

	@Column(name="EXCEDWROF")
	private String exceedMaximumWriteoffAmount;

	@OneToOne
	@JoinColumn(name="FRTSCHID")
	private VATSetup freightVatScheduleId;
	
	@OneToOne
	@JoinColumn(name="MISSCHID")
	private VATSetup miscVatScheduleId;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="NXTPYMTNUM")
	private int nextPaymentNumber;

	@Column(name="NXTSCHDNUM")
	private int nextSchedulePaymentNumber;

	@Column(name="NXTVOUCHNUM")
	private int nextVoucherNumber;

	@Column(name="OVRVORCHTRX")
	private Boolean overrideVoucherNumberTransactionEntry;

	/*@Column(name="POSCHID")
	private String purchaseVatScheduleId;*/
	
	@OneToOne
	@JoinColumn(name="POSCHID")
	private VATSetup purchaseVatScheduleId;

	@Column(name="PRTHTTRBLA")
	private Boolean printHistoricalAgedTrialBalance;

	@Column(name="USRDFPR1")
	private String userDefine1;

	@Column(name="USRDFPR2")
	private String userDefine2;

	@Column(name="USRDFPR3")
	private String userDefine3;

	public PMSetup() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAgeingBy() {
		return ageingBy;
	}

	public void setAgeingBy(int ageingBy) {
		this.ageingBy = ageingBy;
	}

	public boolean isAgeUnappliedCreditAmounts() {
		return ageUnappliedCreditAmounts;
	}

	public void setAgeUnappliedCreditAmounts(boolean ageUnappliedCreditAmounts) {
		this.ageUnappliedCreditAmounts = ageUnappliedCreditAmounts;
	}

	public int getApplyByDefault() {
		return applyByDefault;
	}

	public void setApplyByDefault(int applyByDefault) {
		this.applyByDefault = applyByDefault;
	}

	public Boolean getApTrackingDiscountAvailable() {
		return apTrackingDiscountAvailable;
	}

	public void setApTrackingDiscountAvailable(Boolean apTrackingDiscountAvailable) {
		this.apTrackingDiscountAvailable = apTrackingDiscountAvailable;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	
	public CheckbookMaintenance getCheckbookMaintenance() {
		return checkbookMaintenance;
	}

	public void setCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookMaintenance = checkbookMaintenance;
	}

	/*public String getCheckBookBankId() {
		return checkBookBankId;
	}

	public void setCheckBookBankId(String checkBookBankId) {
		this.checkBookBankId = checkBookBankId;
	}*/

	
	public PMSetupFormatType getPmSetupFormatType() {
		return pmSetupFormatType;
	}

	public void setPmSetupFormatType(PMSetupFormatType pmSetupFormatType) {
		this.pmSetupFormatType = pmSetupFormatType;
	}

/*	public int getCheckFormat() {
		return checkFormat;
	}

	public void setCheckFormat(int checkFormat) {
		this.checkFormat = checkFormat;
	}*/

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRemoveVendorHoldPassword() {
		return removeVendorHoldPassword;
	}

	public void setRemoveVendorHoldPassword(String removeVendorHoldPassword) {
		this.removeVendorHoldPassword = removeVendorHoldPassword;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Boolean getDeleteUnpostedPrintedDocuments() {
		return deleteUnpostedPrintedDocuments;
	}

	public void setDeleteUnpostedPrintedDocuments(Boolean deleteUnpostedPrintedDocuments) {
		this.deleteUnpostedPrintedDocuments = deleteUnpostedPrintedDocuments;
	}

	public String getExceedMaximumInvoiceAmount() {
		return exceedMaximumInvoiceAmount;
	}

	public void setExceedMaximumInvoiceAmount(String exceedMaximumInvoiceAmount) {
		this.exceedMaximumInvoiceAmount = exceedMaximumInvoiceAmount;
	}

	public String getExceedMaximumWriteoffAmount() {
		return exceedMaximumWriteoffAmount;
	}

	public void setExceedMaximumWriteoffAmount(String exceedMaximumWriteoffAmount) {
		this.exceedMaximumWriteoffAmount = exceedMaximumWriteoffAmount;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getNextPaymentNumber() {
		return nextPaymentNumber;
	}

	public void setNextPaymentNumber(int nextPaymentNumber) {
		this.nextPaymentNumber = nextPaymentNumber;
	}

	public int getNextSchedulePaymentNumber() {
		return nextSchedulePaymentNumber;
	}

	public void setNextSchedulePaymentNumber(int nextSchedulePaymentNumber) {
		this.nextSchedulePaymentNumber = nextSchedulePaymentNumber;
	}

	public int getNextVoucherNumber() {
		return nextVoucherNumber;
	}

	public void setNextVoucherNumber(int nextVoucherNumber) {
		this.nextVoucherNumber = nextVoucherNumber;
	}

	public Boolean getOverrideVoucherNumberTransactionEntry() {
		return overrideVoucherNumberTransactionEntry;
	}

	public void setOverrideVoucherNumberTransactionEntry(Boolean overrideVoucherNumberTransactionEntry) {
		this.overrideVoucherNumberTransactionEntry = overrideVoucherNumberTransactionEntry;
	}

	public Boolean getPrintHistoricalAgedTrialBalance() {
		return printHistoricalAgedTrialBalance;
	}

	public void setPrintHistoricalAgedTrialBalance(Boolean printHistoricalAgedTrialBalance) {
		this.printHistoricalAgedTrialBalance = printHistoricalAgedTrialBalance;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public int getAllowDuplicateInvoicePerVendor() {
		return allowDuplicateInvoicePerVendor;
	}

	public void setAllowDuplicateInvoicePerVendor(int allowDuplicateInvoicePerVendor) {
		this.allowDuplicateInvoicePerVendor = allowDuplicateInvoicePerVendor;
	}

	public VATSetup getFreightVatScheduleId() {
		return freightVatScheduleId;
	}

	public void setFreightVatScheduleId(VATSetup freightVatScheduleId) {
		this.freightVatScheduleId = freightVatScheduleId;
	}

	public VATSetup getMiscVatScheduleId() {
		return miscVatScheduleId;
	}

	public void setMiscVatScheduleId(VATSetup miscVatScheduleId) {
		this.miscVatScheduleId = miscVatScheduleId;
	}

	public VATSetup getPurchaseVatScheduleId() {
		return purchaseVatScheduleId;
	}

	public void setPurchaseVatScheduleId(VATSetup purchaseVatScheduleId) {
		this.purchaseVatScheduleId = purchaseVatScheduleId;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
}