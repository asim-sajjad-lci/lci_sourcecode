/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.LeaseType;

public class DtoLeaseType {

	private int leaseTypeIndex;
	private int leaseTypeId;
	private String leaseTypeValue;
	
	public DtoLeaseType(){
		
	}
	
	public DtoLeaseType(LeaseType leaseType){
		this.leaseTypeIndex = leaseType.getIdindex();
		this.leaseTypeId = leaseType.getLeaseTypeId();
		this.leaseTypeValue = leaseType.getValue();
	}
	public int getLeaseTypeIndex() {
		return leaseTypeIndex;
	}
	public void setLeaseTypeIndex(int leaseTypeIndex) {
		this.leaseTypeIndex = leaseTypeIndex;
	}
	public int getLeaseTypeId() {
		return leaseTypeId;
	}
	public void setLeaseTypeId(int leaseTypeId) {
		this.leaseTypeId = leaseTypeId;
	}
	public String getLeaseTypeValue() {
		return leaseTypeValue;
	}
	public void setLeaseTypeValue(String leaseTypeValue) {
		this.leaseTypeValue = leaseTypeValue;
	}
	
	
}
