/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.MasterPostingAccountsSequence;
import com.bti.util.UtilRandomKey;


public class DtoPostingAccount {

	private int pASeries;
	private int postingId;
	private String descriptionPrimary;
	private String descriptionSecondary;
	private String accountNumber;
	private String accountTableRowIndex;
	private int seriesNumber;
	private String seriesDesc;
	
	public DtoPostingAccount(){
		
	}
	
	public DtoPostingAccount(MasterPostingAccountsSequence accountsSequence)
	{
		this.postingId = accountsSequence.getPostingId();
		this.descriptionPrimary = accountsSequence.getPostingAccountDescription();
		this.descriptionSecondary = accountsSequence.getPostingAccountDescriptionArabic();
		this.accountTableRowIndex="";
		if(accountsSequence.getGlAccountsTableAccumulation()!=null){
			this.accountTableRowIndex=accountsSequence.getGlAccountsTableAccumulation().getAccountTableRowIndex();
		}
		if(accountsSequence.getSeries()!=null){
			this.seriesNumber=accountsSequence.getSeries().getModuleSeriesNumber();
			this.seriesDesc=accountsSequence.getSeries().getModuleDescription();
		}
	}
	
	public DtoPostingAccount(MasterPostingAccountsSequence accountsSequence, String langId) 
	{
		  this.postingId = accountsSequence.getPostingId();
		  if( UtilRandomKey.isNotBlank(accountsSequence.getPostingAccountDescription())){
			  this.descriptionPrimary=accountsSequence.getPostingAccountDescription();
		  }
		  else{
			  this.descriptionPrimary="";
		  }
		 if( UtilRandomKey.isNotBlank(accountsSequence.getPostingAccountDescriptionArabic())){
			 this.descriptionSecondary = accountsSequence.getPostingAccountDescriptionArabic();
		 }
		 else{
			 this.descriptionSecondary = "";
		 }
		 this.accountTableRowIndex="";
		 
 		if(accountsSequence.getGlAccountsTableAccumulation()!=null){
 			this.accountTableRowIndex=accountsSequence.getGlAccountsTableAccumulation().getAccountTableRowIndex();
 		}
 		
 		if(accountsSequence.getSeries()!=null){
			this.seriesNumber=accountsSequence.getSeries().getModuleSeriesNumber();
			this.seriesDesc=accountsSequence.getSeries().getModuleDescription();
		}
	}
	
	public int getpASeries() {
		return pASeries;
	}
	
	public void setpASeries(int pASeries) {
		this.pASeries = pASeries;
	}
	
	public String getDescriptionPrimary() {
		return descriptionPrimary;
	}
	
	public void setDescriptionPrimary(String descriptionPrimary) {
		this.descriptionPrimary = descriptionPrimary;
	}
	
	public String getDescriptionSecondary() {
		return descriptionSecondary;
	}
	
	public void setDescriptionSecondary(String descriptionSecondary) {
		this.descriptionSecondary = descriptionSecondary;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getPostingId() {
		return postingId;
	}

	public void setPostingId(int postingId) {
		this.postingId = postingId;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(int seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getSeriesDesc() {
		return seriesDesc;
	}

	public void setSeriesDesc(String seriesDesc) {
		this.seriesDesc = seriesDesc;
	}
}
