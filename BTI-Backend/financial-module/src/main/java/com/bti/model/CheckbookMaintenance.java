/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the gl00200 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl00200")
@NamedQuery(name="CheckbookMaintenance.findAll", query="SELECT g FROM CheckbookMaintenance g")
public class CheckbookMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CHEKBOKID")
	private String checkBookId;

	@Column(name="BAKACCNT")
	private String officialBankAccountNumber;

	@Column(name="CAHACCBLANC")
	private Double cashAccountBalance;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CHKFORMAT")
	private int checkFormat;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="CURBLANC")
	private Double currentCheckbookBalance;
	

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRIPTN")
	private String checkbookDescription;

	@Column(name="DSCRIPTNA")
	private String checkbookDescriptionArabic;

	@Column(name="DUPCHKNUM")
	private Boolean duplicateCheckNumber;

	@Column(name="EXCMAXCHK")
	private int exceedMaxCheckAmount;

	@Column(name="INACTIVE")
	private Boolean inactive;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="MXCHKNMPASS")
	private String passwordOfMaxCheckAmount;

	@Column(name="NXTCHKNUM")
	private int nextCheckNumber;

	@Column(name="NXTDEPNUM")
	private int nextDepositNumber;

	@Column(name="OVECHKNUM")
	private Boolean overrideCheckNumber;
	
	@Column(name="LSTRECBAL")
	private Double lastReconciledBalance;
	
	@Column(name="LSTRECDT")
	private Date lastReconciledDate;

	@Column(name="USERDEFIN1")
	private String userDefine1;

	@Column(name="USERDEFIN2")
	private String userDefine2;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ap00102
	@OneToMany(mappedBy="checkbookMaintenance")
	private List<VendorMaintenanceOptions> vendorMaintenanceOptions;
	
	@OneToMany(mappedBy = "checkbookMaintenance")
	private List<PMSetup> pmSetups;

	public List<PMSetup> getPmSetups() {
		return pmSetups;
	}

	public void setPmSetups(List<PMSetup> pmSetups) {
		this.pmSetups = pmSetups;
	}

	//bi-directional many-to-one association to Ap00200
	@OneToMany(mappedBy="checkbookMaintenance")
	private List<VendorClassesSetup> vendorClassesSetups;

	//bi-directional many-to-one association to Ar00102
	@OneToMany(mappedBy="checkbookMaintenance")
	private List<CustomerMaintenanceOptions> customerMaintenanceOptions;

	//bi-directional many-to-one association to Ar00200
	@OneToMany(mappedBy="checkbookMaintenance")
	private List<CustomerClassesSetup> customerClassesSetups;

	//bi-directional many-to-one association to Ar40002
	/*@OneToMany(mappedBy="checkbookMaintenance")
	private List<PMSetup> pmSetups;*/

	//bi-directional many-to-one association to Sy03500
	@ManyToOne
	@JoinColumn(name="BANKID")
	private BankSetup bankSetup;

	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;
	
	@ManyToOne
	@JoinColumn(name="CURNCYID")
	private CurrencySetup currencySetup;

	public CheckbookMaintenance() {
	}

	public String getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getOfficialBankAccountNumber() {
		return officialBankAccountNumber;
	}

	public void setOfficialBankAccountNumber(String officialBankAccountNumber) {
		this.officialBankAccountNumber = officialBankAccountNumber;
	}

	public Double getCashAccountBalance() {
		return cashAccountBalance;
	}

	public void setCashAccountBalance(Double cashAccountBalance) {
		this.cashAccountBalance = cashAccountBalance;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCheckFormat() {
		return checkFormat;
	}

	public void setCheckFormat(int checkFormat) {
		this.checkFormat = checkFormat;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Double getCurrentCheckbookBalance() {
		return currentCheckbookBalance;
	}

	public void setCurrentCheckbookBalance(Double currentCheckbookBalance) {
		this.currentCheckbookBalance = currentCheckbookBalance;
	}

	public CurrencySetup getCurrencySetup() {
		return currencySetup;
	}

	public void setCurrencySetup(CurrencySetup currencySetup) {
		this.currencySetup = currencySetup;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getCheckbookDescription() {
		return checkbookDescription;
	}

	public void setCheckbookDescription(String checkbookDescription) {
		this.checkbookDescription = checkbookDescription;
	}

	public String getCheckbookDescriptionArabic() {
		return checkbookDescriptionArabic;
	}

	public void setCheckbookDescriptionArabic(String checkbookDescriptionArabic) {
		this.checkbookDescriptionArabic = checkbookDescriptionArabic;
	}

	public Boolean getDuplicateCheckNumber() {
		return duplicateCheckNumber;
	}

	public void setDuplicateCheckNumber(Boolean duplicateCheckNumber) {
		this.duplicateCheckNumber = duplicateCheckNumber;
	}

	public int getExceedMaxCheckAmount() {
		return exceedMaxCheckAmount;
	}

	public void setExceedMaxCheckAmount(int exceedMaxCheckAmount) {
		this.exceedMaxCheckAmount = exceedMaxCheckAmount;
	}

	public Boolean getInactive() {
		return inactive;
	}

	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getPasswordOfMaxCheckAmount() {
		return passwordOfMaxCheckAmount;
	}

	public void setPasswordOfMaxCheckAmount(String passwordOfMaxCheckAmount) {
		this.passwordOfMaxCheckAmount = passwordOfMaxCheckAmount;
	}

	public int getNextCheckNumber() {
		return nextCheckNumber;
	}

	public void setNextCheckNumber(int nextCheckNumber) {
		this.nextCheckNumber = nextCheckNumber;
	}

	public int getNextDepositNumber() {
		return nextDepositNumber;
	}

	public void setNextDepositNumber(int nextDepositNumber) {
		this.nextDepositNumber = nextDepositNumber;
	}

	public Boolean getOverrideCheckNumber() {
		return overrideCheckNumber;
	}

	public void setOverrideCheckNumber(Boolean overrideCheckNumber) {
		this.overrideCheckNumber = overrideCheckNumber;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public List<VendorMaintenanceOptions> getVendorMaintenanceOptions() {
		return vendorMaintenanceOptions;
	}

	public void setVendorMaintenanceOptions(List<VendorMaintenanceOptions> vendorMaintenanceOptions) {
		this.vendorMaintenanceOptions = vendorMaintenanceOptions;
	}

	public List<VendorClassesSetup> getVendorClassesSetups() {
		return vendorClassesSetups;
	}

	public void setVendorClassesSetups(List<VendorClassesSetup> vendorClassesSetups) {
		this.vendorClassesSetups = vendorClassesSetups;
	}
	

	public BankSetup getBankSetup() {
		return bankSetup;
	}

	public void setBankSetup(BankSetup bankSetup) {
		this.bankSetup = bankSetup;
	}

	

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public List<CustomerMaintenanceOptions> getCustomerMaintenanceOptions() {
		return customerMaintenanceOptions;
	}

	public void setCustomerMaintenanceOptions(List<CustomerMaintenanceOptions> customerMaintenanceOptions) {
		this.customerMaintenanceOptions = customerMaintenanceOptions;
	}

	public List<CustomerClassesSetup> getCustomerClassesSetups() {
		return customerClassesSetups;
	}

	public void setCustomerClassesSetups(List<CustomerClassesSetup> customerClassesSetups) {
		this.customerClassesSetups = customerClassesSetups;
	}

	public Double getLastReconciledBalance() {
		return lastReconciledBalance;
	}

	public void setLastReconciledBalance(Double lastReconciledBalance) {
		this.lastReconciledBalance = lastReconciledBalance;
	}

	public Date getLastReconciledDate() {
		return lastReconciledDate;
	}

	public void setLastReconciledDate(Date lastReconciledDate) {
		this.lastReconciledDate = lastReconciledDate;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/*public List<PMSetup> getPmSetups() {
		return pmSetups;
	}

	public void setPmSetups(List<PMSetup> pmSetups) {
		this.pmSetups = pmSetups;
	}*/
	
	
	
}