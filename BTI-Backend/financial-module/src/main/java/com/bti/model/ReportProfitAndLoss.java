package com.bti.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="profitandloss")
public class ReportProfitAndLoss {

	@Id
	@Column(name="IDROW")
	private int IDROW;
	
	@Column(name="ACTROWID")
	private int accountTableRowIndex;
	
	@Column(name="mainaccount")
	private String MainAccount;
	
	@Column(name="mainaccountdescription")
	private String MainAccountDescription;
	
	@Column(name="accountcategoryindex")
	private int AccountCategoryIndex;
	
	@Column(name="accountcategorydescription")
	private String AccountCategoryDecription;
	
	@Column(name="trxdate")
	private Date TransactionDate;
	
	@Column(name="period")
	private int period;
	
	@Column(name="debitamount")
	private Double debitAmmount;
	
	@Column(name="creditamount")
	private Double creditAmmount;
	
	public int getRowId() {
		return IDROW;
	}

	public void setRowId(int IDROW) {
		this.IDROW = IDROW;
	}

	public int getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(int accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public String getMainAccount() {
		return MainAccount;
	}

	public void setMainAccount(String mainAccount) {
		MainAccount = mainAccount;
	}

	public String getMainAccountDescription() {
		return MainAccountDescription;
	}

	public void setMainAccountDescription(String mainAccountDescription) {
		MainAccountDescription = mainAccountDescription;
	}

	public int getAccountCategoryIndex() {
		return AccountCategoryIndex;
	}

	public void setAccountCategoryIndex(int accountCategoryIndex) {
		AccountCategoryIndex = accountCategoryIndex;
	}

	public String getAccountCategoryDecription() {
		return AccountCategoryDecription;
	}

	public void setAccountCategoryDecription(String accountCategoryDecription) {
		AccountCategoryDecription = accountCategoryDecription;
	}

	public Date getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		TransactionDate = transactionDate;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public Double getDebitAmmount() {
		return debitAmmount;
	}

	public void setDebitAmmount(Double debitAmmount) {
		this.debitAmmount = debitAmmount;
	}

	public Double getCrediAmmount() {
		return creditAmmount;
	}

	public void setCrediAmmount(Double creditAmmount) {
		this.creditAmmount = creditAmmount;
	}
	
	
	
	
	
}
