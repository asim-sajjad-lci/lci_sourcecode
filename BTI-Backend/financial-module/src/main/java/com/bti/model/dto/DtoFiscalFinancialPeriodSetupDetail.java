/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.GLConfigurationDefinePeriodsDetails;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;

public class DtoFiscalFinancialPeriodSetupDetail {

	private String transactionDocumentDescriptionPrimary;
	private String transactionDocumentDescriptionSecondary;
	private Integer periodId;
	private String startPeriodDate;
	private String endPeriodDate;
	private String periodNamePrimary;
	private String periodNameSecondary;
	private boolean periodSeriesFinancial;
	private boolean periodSeriesSales;
	private boolean periodSeriesPurchase;
	private boolean periodSeriesInventory;
	private boolean periodSeriesProject;
	private boolean periodSeriesPayroll;
	
	public DtoFiscalFinancialPeriodSetupDetail(){
		
	}
	
	public DtoFiscalFinancialPeriodSetupDetail(GLConfigurationDefinePeriodsDetails glConfigurationDefinePeriodsDetails){
		this.transactionDocumentDescriptionPrimary="";
		if(UtilRandomKey.isNotBlank(glConfigurationDefinePeriodsDetails.getTransactionDocumentDescription())){
			this.transactionDocumentDescriptionPrimary = glConfigurationDefinePeriodsDetails.getTransactionDocumentDescription();
		}
		this.transactionDocumentDescriptionSecondary="";
		if(UtilRandomKey.isNotBlank(glConfigurationDefinePeriodsDetails.getTransactionDocumentDescriptionArabic())){
			this.transactionDocumentDescriptionSecondary = glConfigurationDefinePeriodsDetails.getTransactionDocumentDescriptionArabic();
		}
		
		this.periodId = glConfigurationDefinePeriodsDetails.getPeriodId();
		this.startPeriodDate="";
		if(glConfigurationDefinePeriodsDetails.getStartPeriodDate()!=null){
			this.startPeriodDate = UtilDateAndTime.dateToStringddmmyyyy(glConfigurationDefinePeriodsDetails.getStartPeriodDate());
		}
		this.endPeriodDate="";
		if(glConfigurationDefinePeriodsDetails.getEndPeriodDate()!=null){
			this.endPeriodDate = UtilDateAndTime.dateToStringddmmyyyy(glConfigurationDefinePeriodsDetails.getEndPeriodDate());
		}
		this.periodNamePrimary="";
		if(UtilRandomKey.isNotBlank(glConfigurationDefinePeriodsDetails.getPeriodName())){
			this.periodNamePrimary = glConfigurationDefinePeriodsDetails.getPeriodName();
		}
		this.periodNameSecondary="";
		if(UtilRandomKey.isNotBlank(glConfigurationDefinePeriodsDetails.getPeriodNameArabic())){
			this.periodNameSecondary = glConfigurationDefinePeriodsDetails.getPeriodNameArabic();
		}
		
		this.periodSeriesFinancial = glConfigurationDefinePeriodsDetails.getPeriodSeriesFinancial();
		this.periodSeriesSales = glConfigurationDefinePeriodsDetails.getPeriodSeriesSales();
		this.periodSeriesPurchase = glConfigurationDefinePeriodsDetails.getPeriodSeriesPurchase();
		this.periodSeriesInventory = glConfigurationDefinePeriodsDetails.getPeriodSeriesInventory();
		this.periodSeriesProject = glConfigurationDefinePeriodsDetails.getPeriodSeriesProject();
		this.periodSeriesPayroll = glConfigurationDefinePeriodsDetails.getPeriodSeriesPayroll();
	}
	public String getTransactionDocumentDescriptionPrimary() {
		return transactionDocumentDescriptionPrimary;
	}
	public void setTransactionDocumentDescriptionPrimary(String transactionDocumentDescriptionPrimary) {
		this.transactionDocumentDescriptionPrimary = transactionDocumentDescriptionPrimary;
	}
	public String getTransactionDocumentDescriptionSecondary() {
		return transactionDocumentDescriptionSecondary;
	}
	public void setTransactionDocumentDescriptionSecondary(String transactionDocumentDescriptionSecondary) {
		this.transactionDocumentDescriptionSecondary = transactionDocumentDescriptionSecondary;
	}
	public Integer getPeriodId() {
		return periodId;
	}
	public void setPeriodId(Integer periodId) {
		this.periodId = periodId;
	}
	public String getStartPeriodDate() {
		return startPeriodDate;
	}
	public void setStartPeriodDate(String startPeriodDate) {
		this.startPeriodDate = startPeriodDate;
	}
	public String getEndPeriodDate() {
		return endPeriodDate;
	}
	public void setEndPeriodDate(String endPeriodDate) {
		this.endPeriodDate = endPeriodDate;
	}
	public String getPeriodNamePrimary() {
		return periodNamePrimary;
	}
	public void setPeriodNamePrimary(String periodNamePrimary) {
		this.periodNamePrimary = periodNamePrimary;
	}
	public String getPeriodNameSecondary() {
		return periodNameSecondary;
	}
	public void setPeriodNameSecondary(String periodNameSecondary) {
		this.periodNameSecondary = periodNameSecondary;
	}
	public boolean isPeriodSeriesFinancial() {
		return periodSeriesFinancial;
	}
	public void setPeriodSeriesFinancial(boolean periodSeriesFinancial) {
		this.periodSeriesFinancial = periodSeriesFinancial;
	}
	public boolean isPeriodSeriesSales() {
		return periodSeriesSales;
	}
	public void setPeriodSeriesSales(boolean periodSeriesSales) {
		this.periodSeriesSales = periodSeriesSales;
	}
	public boolean isPeriodSeriesPurchase() {
		return periodSeriesPurchase;
	}
	public void setPeriodSeriesPurchase(boolean periodSeriesPurchase) {
		this.periodSeriesPurchase = periodSeriesPurchase;
	}
	public boolean isPeriodSeriesInventory() {
		return periodSeriesInventory;
	}
	public void setPeriodSeriesInventory(boolean periodSeriesInventory) {
		this.periodSeriesInventory = periodSeriesInventory;
	}
	public boolean isPeriodSeriesProject() {
		return periodSeriesProject;
	}
	public void setPeriodSeriesProject(boolean periodSeriesProject) {
		this.periodSeriesProject = periodSeriesProject;
	}
	public boolean isPeriodSeriesPayroll() {
		return periodSeriesPayroll;
	}
	public void setPeriodSeriesPayroll(boolean periodSeriesPayroll) {
		this.periodSeriesPayroll = periodSeriesPayroll;
	}
}
