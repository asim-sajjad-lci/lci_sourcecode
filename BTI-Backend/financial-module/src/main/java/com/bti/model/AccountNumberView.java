package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

//@IdClass(value = rptaccountstatement_lcl.class)
@Entity 
@Immutable
@Table(name = "accountnumberh")
@NamedQuery(name = "AccountNumberView.findAll", query = "SELECT a FROM AccountNumberView a")
public class AccountNumberView implements Serializable {
	private static final long serialVersionUID = 1L;

//	@Id
	@Column(name="mainaccount")
	private long mainaccount;

//	@Id
	@Column(name="dim1")
	private String dim1;
	
//	@Id
	@Column(name="dim2")
	private String dim2;
	
//	@Id
	@Column(name="dim3")
	private String dim3;
	
//	@Id
	@Column(name="dim4")
	private String dim4;
	
//	@Id
	@Column(name="dim5")
	private String dim5;
	
	
	@Id
	@Column(name="ACTROWID")
	private int ACTROWID;
	
//	@Id
	@Column(name="accountcategory")
	private String accountcategory;

//	@Id
	@Column(name="accountnumber")
	private String accountnumber;
	
//	@Id
	@Column(name="accountdescription")
	private String accountdescription;
	
//	@Id
	@Column(name="accountdescriptionarabic")
	private String accountdescriptionarabic;

	public long getMainaccount() {
		return mainaccount;
	}

	public void setMainaccount(long mainaccount) {
		this.mainaccount = mainaccount;
	}

	public String getDim1() {
		return dim1;
	}

	public void setDim1(String dim1) {
		this.dim1 = dim1;
	}

	public String getDim2() {
		return dim2;
	}

	public void setDim2(String dim2) {
		this.dim2 = dim2;
	}

	public String getDim3() {
		return dim3;
	}

	public void setDim3(String dim3) {
		this.dim3 = dim3;
	}

	public String getDim4() {
		return dim4;
	}

	public void setDim4(String dim4) {
		this.dim4 = dim4;
	}

	public String getDim5() {
		return dim5;
	}

	public void setDim5(String dim5) {
		this.dim5 = dim5;
	}

	public int getACTROWID() {
		return ACTROWID;
	}

	public void setACTROWID(int aCTROWID) {
		ACTROWID = aCTROWID;
	}

	public String getAccountcategory() {
		return accountcategory;
	}

	public void setAccountcategory(String accountcategory) {
		this.accountcategory = accountcategory;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public String getAccountdescription() {
		return accountdescription;
	}

	public void setAccountdescription(String accountdescription) {
		this.accountdescription = accountdescription;
	}

	public String getAccountdescriptionarabic() {
		return accountdescriptionarabic;
	}

	public void setAccountdescriptionarabic(String accountdescriptionarabic) {
		this.accountdescriptionarabic = accountdescriptionarabic;
	}

	
	
	
}
