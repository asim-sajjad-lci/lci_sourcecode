package com.bti.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoProfitAndLossReport {
	
	private int year;

	private int period;
	
	private List<DtoAccountCategory> listOfAccountCategory;

	private Double netIncome;
	
	private String totalNetIncome;
	
	public DtoProfitAndLossReport() {
		// TODO Auto-generated constructor stub
		this.netIncome = 0.0;
	}
	
	public DtoProfitAndLossReport(List<DtoAccountCategory> listOfAccountCategory) {
		// TODO Auto-generated constructor stub
		this.listOfAccountCategory = listOfAccountCategory;
		this.netIncome = 0.0;
	}
	
	
	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public List<DtoAccountCategory> getListOfAccountCategory() {
		return listOfAccountCategory;
	}

	public void setListOfAccountCategory(List<DtoAccountCategory> listOfAccountCategory) {
		this.listOfAccountCategory = listOfAccountCategory;
	}

	public Double getNetIncome() {
		return netIncome;
	}

	public void setNetIncome(Double netIncome) {
		this.netIncome = netIncome;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public String getTotalNetIncome() {
		return totalNetIncome;
	}

	public void setTotalNetIncome(String totalNetIncome) {
		this.totalNetIncome = totalNetIncome;
	}



	
	
}
