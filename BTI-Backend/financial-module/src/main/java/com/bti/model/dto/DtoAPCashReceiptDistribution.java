/**
 * BTI - BAAN for Technology And Trade IntegerL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.APManualPayment;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoAPCashReceiptDistribution {

	private String manualPaymentNumber;
	private String vendorId;
	private String vendorName;
	private String currencyID;
	private String transactionType;
	private String transactionNumber;
	private Double functionalAmount;
	private Double originalAmount;
	
	List<DtoAPDistributionDetail> listDtoAPDistributionDetail;
	 

	public DtoAPCashReceiptDistribution() {
	}

	public DtoAPCashReceiptDistribution(APManualPayment apManualPayment) {
		this.manualPaymentNumber = apManualPayment.getManualPaymentNumber();
	}

	public String getManualPaymentNumber() {
		return manualPaymentNumber;
	}

	public void setManualPaymentNumber(String manualPaymentNumber) {
		this.manualPaymentNumber = manualPaymentNumber;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Double getFunctionalAmount() {
		return functionalAmount;
	}

	public void setFunctionalAmount(Double functionalAmount) {
		this.functionalAmount = UtilRoundDecimal.roundDecimalValue(functionalAmount);
	}

	public Double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(Double originalAmount) {
		this.originalAmount = UtilRoundDecimal.roundDecimalValue(originalAmount);
	}

	public List<DtoAPDistributionDetail> getListDtoAPDistributionDetail() {
		return listDtoAPDistributionDetail;
	}

	public void setListDtoAPDistributionDetail(List<DtoAPDistributionDetail> listDtoAPDistributionDetail) {
		this.listDtoAPDistributionDetail = listDtoAPDistributionDetail;
	}

}
