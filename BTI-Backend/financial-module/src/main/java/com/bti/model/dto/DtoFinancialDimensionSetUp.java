/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.COAFinancialDimensions;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoFinancialDimensionSetUp {
	
	private int dimInxd;
	private String dimensionName;
	private String dimensionMask;
	private String dimensionDescription;
	private String dimensionDescriptionArabic;
	
	public DtoFinancialDimensionSetUp() {

	}
	
	public DtoFinancialDimensionSetUp(COAFinancialDimensions coaFinancialDimensions) {
		this.dimInxd=coaFinancialDimensions.getDimInxd();
		this.dimensionName=coaFinancialDimensions.getDimensioncolumnname();		
		this.dimensionMask=coaFinancialDimensions.getDimensionMask();
		this.dimensionDescription="";
		if(UtilRandomKey.isNotBlank(coaFinancialDimensions.getDimensionDescription())){
			this.dimensionDescription=coaFinancialDimensions.getDimensionDescription();
		}
		if(UtilRandomKey.isNotBlank(coaFinancialDimensions.getDimensionDescriptionArabic())){
			this.dimensionDescription=coaFinancialDimensions.getDimensionDescriptionArabic();
		}
	}
	
	public DtoFinancialDimensionSetUp(COAFinancialDimensions coaFinancialDimensions, String langId) {
		this.dimInxd=coaFinancialDimensions.getDimInxd();
		this.dimensionName=coaFinancialDimensions.getDimensioncolumnname();		
		this.dimensionMask=coaFinancialDimensions.getDimensionMask();
	}

	public int getDimInxd() {
		return dimInxd;
	}

	public void setDimInxd(int dimInxd) {
		this.dimInxd = dimInxd;
	}

	public String getDimensionName() {
		return dimensionName;
	}

	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}

	public String getDimensionMask() {
		return dimensionMask;
	}

	public void setDimensionMask(String dimensionMask) {
		this.dimensionMask = dimensionMask;
	}

	public String getDimensionDescription() {
		return dimensionDescription;
	}

	public void setDimensionDescription(String dimensionDescription) {
		this.dimensionDescription = dimensionDescription;
	}

	public String getDimensionDescriptionArabic() {
		return dimensionDescriptionArabic;
	}

	public void setDimensionDescriptionArabic(String dimensionDescriptionArabic) {
		this.dimensionDescriptionArabic = dimensionDescriptionArabic;
	}
}
