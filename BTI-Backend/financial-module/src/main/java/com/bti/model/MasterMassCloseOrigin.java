/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the master seperator decimal database table.
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "master_mass_close_origin")
@NamedQuery(name="MasterMassCloseOrigin.findAll", query="SELECT a FROM MasterMassCloseOrigin a")
public class MasterMassCloseOrigin implements Serializable {
	private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="ID")
		private int id;

		@Column(name="ORIGIN")
		private String origin;

		@ManyToOne
		@JoinColumn(name="SERIES_TYPE")
		private SeriesType seriesType;
		
		@Column(name="TYPE_ID")
		private int typeId;
		
		@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
		private boolean isDeleted;
		
		@ManyToOne
		@JoinColumn(name="lang_Id")
		private Language language;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Language getLanguage() {
			return language;
		}

		public void setLanguage(Language language) {
			this.language = language;
		}

		public boolean isDeleted() {
			return isDeleted;
		}

		public void setDeleted(boolean isDeleted) {
			this.isDeleted = isDeleted;
		}

		public String getOrigin() {
			return origin;
		}

		public void setOrigin(String origin) {
			this.origin = origin;
		}

		public SeriesType getSeriesType() {
			return seriesType;
		}

		public void setSeriesType(SeriesType seriesType) {
			this.seriesType = seriesType;
		}

		public int getTypeId() {
			return typeId;
		}

		public void setTypeId(int typeId) {
			this.typeId = typeId;
		}
		
		
		
}