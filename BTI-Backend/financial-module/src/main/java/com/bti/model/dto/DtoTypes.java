/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoTypes {

	int typeId;
	private String typeValue;
	private String typeCode;
	private String propertyName;
	private String description;
	private Integer mainAccountIndexId;
	private Integer dimValueIndex;
	private String messageType;
	private String action;
	private int actiontype;
	private int priceLevelIndex;
	
	public int getTypeId() {
		return typeId;
	}
	
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	
	public String getTypeValue() {
		return typeValue;
	}
	
	public void setTypeValue(String typeValue) {
		this.typeValue = typeValue;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMainAccountIndexId() {
		return mainAccountIndexId;
	}

	public void setMainAccountIndexId(Integer mainAccountIndexId) {
		this.mainAccountIndexId = mainAccountIndexId;
	}

	public Integer getDimValueIndex() {
		return dimValueIndex;
	}

	public void setDimValueIndex(Integer dimValueIndex) {
		this.dimValueIndex = dimValueIndex;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public int getPriceLevelIndex() {
		return priceLevelIndex;
	}

	public void setPriceLevelIndex(int priceLevelIndex) {
		this.priceLevelIndex = priceLevelIndex;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getActiontype() {
		return actiontype;
	}

	public void setActiontype(int actiontype) {
		this.actiontype = actiontype;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	
	
}
