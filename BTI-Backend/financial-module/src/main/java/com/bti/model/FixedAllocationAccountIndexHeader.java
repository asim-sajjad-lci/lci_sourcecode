/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl00105 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl00105")
@NamedQuery(name="FixedAllocationAccountIndexHeader.findAll", query="SELECT g FROM FixedAllocationAccountIndexHeader g")
public class FixedAllocationAccountIndexHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTFIXID")
	private int actFixIndex;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;
	
	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PRCNTAGE")
	private Double percentage;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	

/*	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;*/

	//bi-directional one-to-one association to Gl00115
	/*@OneToOne
	@JoinColumn(name="ACTFIXID")
	private FixedAllocationAccountIndexDetails fixedAllocationAccountIndexDetails;*/
	
	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private COAMainAccounts coaMainAccounts;

	public FixedAllocationAccountIndexHeader() {
	}


	public int getActFixIndex() {
		return actFixIndex;
	}



	public void setActFixIndex(int actFixIndex) {
		this.actFixIndex = actFixIndex;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}


	public COAMainAccounts getCoaMainAccounts() {
		return coaMainAccounts;
	}


	public void setCoaMainAccounts(COAMainAccounts coaMainAccounts) {
		this.coaMainAccounts = coaMainAccounts;
	}

	/*public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}*/
/*
	public FixedAllocationAccountIndexDetails getFixedAllocationAccountIndexDetails() {
		return fixedAllocationAccountIndexDetails;
	}

	public void setFixedAllocationAccountIndexDetails(
			FixedAllocationAccountIndexDetails fixedAllocationAccountIndexDetails) {
		this.fixedAllocationAccountIndexDetails = fixedAllocationAccountIndexDetails;
	}*/
	
	

	
	
}