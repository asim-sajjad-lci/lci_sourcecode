/**
 * BTI - BAAN for Technology And Trade IntegerL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.HashMap;
import java.util.Map;

import com.bti.model.GLCashReceiptBank;

public class DtoGLCashReceiptDistribution {

	private String receiptNumber;
	private String checkBookId;
	private String depositorName;
	private String transactionType;
	private Map<String,Object> accountDetailDebit = new HashMap<>();
	private Map<String,Object> accountDetailVat = new HashMap<>();
	private Map<String,Object> accountDetailCredit = new HashMap<>();

	public DtoGLCashReceiptDistribution() {
	}

	public DtoGLCashReceiptDistribution(GLCashReceiptBank glCashReceiptBank) {
		this.receiptNumber = glCashReceiptBank.getReceiptNumber();
		this.checkBookId = glCashReceiptBank.getCheckbookMaintenance()!=null?glCashReceiptBank.getCheckbookMaintenance().getCheckBookId():null;
		this.transactionType = glCashReceiptBank.getReceiptType()!=null?glCashReceiptBank.getReceiptType().getValue():null;
		this.depositorName = glCashReceiptBank.getDepositorName();
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getDepositorName() {
		return depositorName;
	}

	public void setDepositorName(String depositorName) {
		this.depositorName = depositorName;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Map<String, Object> getAccountDetailDebit() {
		return accountDetailDebit;
	}

	public void setAccountDetailDebit(Map<String, Object> accountDetailDebit) {
		this.accountDetailDebit = accountDetailDebit;
	}

	public Map<String, Object> getAccountDetailVat() {
		return accountDetailVat;
	}

	public void setAccountDetailVat(Map<String, Object> accountDetailVat) {
		this.accountDetailVat = accountDetailVat;
	}

	public Map<String, Object> getAccountDetailCredit() {
		return accountDetailCredit;
	}

	public void setAccountDetailCredit(Map<String, Object> accountDetailCredit) {
		this.accountDetailCredit = accountDetailCredit;
	}

	 
 
}
