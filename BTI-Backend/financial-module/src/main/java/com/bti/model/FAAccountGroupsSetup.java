/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the fa40100 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40100")
@NamedQuery(name="FAAccountGroupsSetup.findAll", query="SELECT f FROM FAAccountGroupsSetup f")
public class FAAccountGroupsSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ACCGROPINXD")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int faAccountGroupIndex;
	
	@Column(name="ACCGROPID")
	private String faAccountGroupId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="DSCRPTN")
	private String faAccountGroupDescription;

	@Column(name="DSCRPTNA")
	private String faAccountGroupDescriptionArabic;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	public FAAccountGroupsSetup() {
	}

	public int getFaAccountGroupIndex() {
		return faAccountGroupIndex;
	}

	public void setFaAccountGroupIndex(int faAccountGroupIndex) {
		this.faAccountGroupIndex = faAccountGroupIndex;
	}

	public String getFaAccountGroupId() {
		return faAccountGroupId;
	}

	public void setFaAccountGroupId(String faAccountGroupId) {
		this.faAccountGroupId = faAccountGroupId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getFaAccountGroupDescription() {
		return faAccountGroupDescription;
	}

	public void setFaAccountGroupDescription(String faAccountGroupDescription) {
		this.faAccountGroupDescription = faAccountGroupDescription;
	}

	public String getFaAccountGroupDescriptionArabic() {
		return faAccountGroupDescriptionArabic;
	}

	public void setFaAccountGroupDescriptionArabic(String faAccountGroupDescriptionArabic) {
		this.faAccountGroupDescriptionArabic = faAccountGroupDescriptionArabic;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}