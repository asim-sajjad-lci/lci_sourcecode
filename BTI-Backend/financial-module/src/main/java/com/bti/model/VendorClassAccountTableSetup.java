/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ap00201 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap00201")
@NamedQuery(name="VendorClassAccountTableSetup.findAll", query="SELECT a FROM VendorClassAccountTableSetup a")
public class VendorClassAccountTableSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ACCTABLID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int accTableId;

	@ManyToOne
	@JoinColumn(name="ACCTCTTYPID")
	private MasterAccountType masterAccountType;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	

	//bi-directional many-to-one association to Ap00200
	/*@OneToMany(mappedBy="vendorClassAccountTableSetup")
	private List<VendorClassesSetup> vendorClassesSetups;*/

	//bi-directional many-to-one association to Ap00200
	@ManyToOne
	@JoinColumn(name="VENDCLSID")
	private VendorClassesSetup vendorClassesSetup;

	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;

	public VendorClassAccountTableSetup() {
	}

	public int getAccTableId() {
		return accTableId;
	}

	public void setAccTableId(int accTableId) {
		this.accTableId = accTableId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public MasterAccountType getMasterAccountType() {
		return masterAccountType;
	}

	public void setMasterAccountType(MasterAccountType masterAccountType) {
		this.masterAccountType = masterAccountType;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}


	

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	/*public List<VendorClassesSetup> getVendorClassesSetups() {
		return vendorClassesSetups;
	}

	public void setVendorClassesSetups(List<VendorClassesSetup> vendorClassesSetups) {
		this.vendorClassesSetups = vendorClassesSetups;
	}*/

	public VendorClassesSetup getVendorClassesSetup() {
		return vendorClassesSetup;
	}

	public void setVendorClassesSetup(VendorClassesSetup vendorClassesSetup) {
		this.vendorClassesSetup = vendorClassesSetup;
	}

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

}