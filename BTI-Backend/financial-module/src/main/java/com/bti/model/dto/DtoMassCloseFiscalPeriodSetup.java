/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoMassCloseFiscalPeriodSetup {

	private Integer series;
	private Integer seriesId;
	private Integer originId;
	private Integer year;
	private String seriesName;
	private String origin;
	private String periodName;
	private Integer periodId;
	private int serieTypeId;
	private Boolean isClose = false;
	private List<DtoMassCloseFiscalPeriodSetup> records = new ArrayList<>();
	public Integer getSeries() {
		return series;
	}
	public void setSeries(Integer series) {
		this.series = series;
	}
	public Integer getSeriesId() {
		return seriesId;
	}
	public void setSeriesId(Integer seriesId) {
		this.seriesId = seriesId;
	}
	public Integer getOriginId() {
		return originId;
	}
	public void setOriginId(Integer originId) {
		this.originId = originId;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getSeriesName() {
		return seriesName;
	}
	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getPeriodName() {
		return periodName;
	}
	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}
	public Integer getPeriodId() {
		return periodId;
	}
	public void setPeriodId(Integer periodId) {
		this.periodId = periodId;
	}
	public int getSerieTypeId() {
		return serieTypeId;
	}
	public void setSerieTypeId(int serieTypeId) {
		this.serieTypeId = serieTypeId;
	}
	public Boolean getIsClose() {
		return isClose;
	}
	public void setIsClose(Boolean isClose) {
		this.isClose = isClose;
	}
	public List<DtoMassCloseFiscalPeriodSetup> getRecords() {
		return records;
	}
	public void setRecords(List<DtoMassCloseFiscalPeriodSetup> records) {
		this.records = records;
	}
}
