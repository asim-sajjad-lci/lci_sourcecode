/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the hold_vendor_status database table.
 * 
 */
@Entity
@Table(name="master_hold_vendor_status")
@NamedQuery(name="HoldVendorStatus.findAll", query="SELECT h FROM HoldVendorStatus h")
public class HoldVendorStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private short id;

	@Column(name="HOLD_STATUS")
	private int holdStatus;

	@Column(name="HOLD_STATUS_PRIMARY")
	private String holdStatusPrimary;
	
	@ManyToOne
	@JoinColumn(name="lang_Id")
	private Language language;

	/*@Column(name="HOLD_STATUS_SECONDARY")
	private String holdStatusSecondary;*/
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ap00101
	/*@OneToMany(mappedBy="holdVendorStatus")
	private List<VendorMaintenance> ap00101s;*/

	public HoldVendorStatus() {
	}

	public short getId() {
		return this.id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public int getHoldStatus() {
		return this.holdStatus;
	}

	public void setHoldStatus(int holdStatus) {
		this.holdStatus = holdStatus;
	}

	public String getHoldStatusPrimary() {
		return this.holdStatusPrimary;
	}

	public void setHoldStatusPrimary(String holdStatusPrimary) {
		this.holdStatusPrimary = holdStatusPrimary;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/*public List<VendorMaintenance> getAp00101s() {
		return ap00101s;
	}

	public void setAp00101s(List<VendorMaintenance> ap00101s) {
		this.ap00101s = ap00101s;
	}*/
	
	



}