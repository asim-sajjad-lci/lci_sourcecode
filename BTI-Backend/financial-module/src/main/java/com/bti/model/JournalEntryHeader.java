/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the gl10100 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10100")
@NamedQuery(name="JournalEntryHeader.findAll", query="SELECT a FROM JournalEntryHeader a")
public class JournalEntryHeader extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="JORNID")
	private String journalID;
	
	@ManyToOne
	@JoinColumn(name="BATCHID")
	private GLBatches glBatches;
	
	@Column(name="TRXTYP")
	private int transactionType;

	@Column(name="TRXDT")
	private Date transactionDate;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="JORNID")
	private List<JournalEntryDetails> journalDetails;
	
	@Column(name="TRXREDT")
	private Date transactionReversingDate;

	@ManyToOne
	@JoinColumn(name="SOURDOC")
	private GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes;
	
	@Column(name="DSCRPTN")
	private String journalDescription;
	
	@Column(name="DSCRPTNA")
	private String journalDescriptionArabic;
	
	@ManyToOne
	@JoinColumn(name="CURNCYID")
	private CurrencySetup currencySetup;
	
	@Column(name="TOTJRN")
	private Double totalJournalEntry;

	@Column(name="TOTDBJRN")
	private Double totalJournalEntryDebit;

	@Column(name="TOTCRJRN")
	private Double totalJournalEntryCredit ;
	
	@Column(name="OTOTDBJRN")
	private Double originalTotalJournalEntryDebit ;

	@Column(name="OTOTCRJRN")
	private Double originalTotalJournalEntryCredit ;

	@ManyToOne
	@JoinColumn(name="EXGTBLINXD")
	private CurrencyExchangeHeader currencyExchangeHeader ;

	@Column(name="XCHGRATE")
	private Double exchangeTableRate ;

	@ManyToOne
	@JoinColumn(name="SERIES")
	private GLConfigurationAuditTrialCodes moduleSeriesNumber;

	@Column(name="TRXSOURC")
	private String transactionSource ;

	@Column(name="TRXNMBR")
	private String originalTransactionNumberFromModule ;
	
	@Column(name="ORGJRNENTR")
	private String originalJournalEntryID;

	@Column(name="CREATDDT")
	private Date createDate;

	@Column(name="MODIFDT")
	private Date modifyDate ;

	@Column(name="CHANGEBY")
	private String modifyByUserID ;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex ;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex ;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private Boolean isDeleted;
	
	@Column(name="INTERCOMP" , columnDefinition = "tinyint(0) default 1")
	private Boolean interCompany;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJournalID() {
		return journalID;
	}

	public void setJournalID(String journalID) {
		this.journalID = journalID;
	}

	public GLBatches getGlBatches() {
		return glBatches;
	}

	public void setGlBatches(GLBatches glBatches) {
		this.glBatches = glBatches;
	}

	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Date getTransactionReversingDate() {
		return transactionReversingDate;
	}

	public void setTransactionReversingDate(Date transactionReversingDate) {
		this.transactionReversingDate = transactionReversingDate;
	}

	public GLConfigurationAuditTrialCodes getGlConfigurationAuditTrialCodes() {
		return glConfigurationAuditTrialCodes;
	}

	public void setGlConfigurationAuditTrialCodes(GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes) {
		this.glConfigurationAuditTrialCodes = glConfigurationAuditTrialCodes;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getJournalDescription() {
		return journalDescription;
	}

	public void setJournalDescription(String journalDescription) {
		this.journalDescription = journalDescription;
	}

	public String getJournalDescriptionArabic() {
		return journalDescriptionArabic;
	}

	public void setJournalDescriptionArabic(String journalDescriptionArabic) {
		this.journalDescriptionArabic = journalDescriptionArabic;
	}
	
	public CurrencySetup getCurrencySetup() {
		return currencySetup;
	}

	public void setCurrencySetup(CurrencySetup currencySetup) {
		this.currencySetup = currencySetup;
	}

	public Double getTotalJournalEntry() {
		return totalJournalEntry;
	}

	public void setTotalJournalEntry(Double totalJournalEntry) {
		this.totalJournalEntry = totalJournalEntry;
	}

	public Double getTotalJournalEntryDebit() {
		return totalJournalEntryDebit;
	}

	public void setTotalJournalEntryDebit(Double totalJournalEntryDebit) {
		this.totalJournalEntryDebit = totalJournalEntryDebit;
	}

	public Double getTotalJournalEntryCredit() {
		return totalJournalEntryCredit;
	}

	public void setTotalJournalEntryCredit(Double totalJournalEntryCredit) {
		this.totalJournalEntryCredit = totalJournalEntryCredit;
	}

	public Double getOriginalTotalJournalEntryDebit() {
		return originalTotalJournalEntryDebit;
	}

	public void setOriginalTotalJournalEntryDebit(Double originalTotalJournalEntryDebit) {
		this.originalTotalJournalEntryDebit = originalTotalJournalEntryDebit;
	}

	public Double getOriginalTotalJournalEntryCredit() {
		return originalTotalJournalEntryCredit;
	}

	public void setOriginalTotalJournalEntryCredit(Double originalTotalJournalEntryCredit) {
		this.originalTotalJournalEntryCredit = originalTotalJournalEntryCredit;
	}

	public CurrencyExchangeHeader getCurrencyExchangeHeader() {
		return currencyExchangeHeader;
	}

	public void setCurrencyExchangeHeader(CurrencyExchangeHeader currencyExchangeHeader) {
		this.currencyExchangeHeader = currencyExchangeHeader;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}
	
	public GLConfigurationAuditTrialCodes getModuleSeriesNumber() {
		return moduleSeriesNumber;
	}

	public void setModuleSeriesNumber(GLConfigurationAuditTrialCodes moduleSeriesNumber) {
		this.moduleSeriesNumber = moduleSeriesNumber;
	}

	public String getTransactionSource() {
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource) {
		this.transactionSource = transactionSource;
	}

	public String getOriginalTransactionNumberFromModule() {
		return originalTransactionNumberFromModule;
	}

	public void setOriginalTransactionNumberFromModule(String originalTransactionNumberFromModule) {
		this.originalTransactionNumberFromModule = originalTransactionNumberFromModule;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Boolean getInterCompany() {
		return interCompany;
	}

	public void setInterCompany(Boolean interCompany) {
		this.interCompany = interCompany;
	}

	public String getOriginalJournalEntryID() {
		return originalJournalEntryID;
	}

	public void setOriginalJournalEntryID(String originalJournalEntryID) {
		this.originalJournalEntryID = originalJournalEntryID;
	}

	public List<JournalEntryDetails> getJournalDetails() {
		return journalDetails;
	}

	public void setJournalDetails(List<JournalEntryDetails> journalDetails) {
		this.journalDetails = journalDetails;
	}
	
	

}