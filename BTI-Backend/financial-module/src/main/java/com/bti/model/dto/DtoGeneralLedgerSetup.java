/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;


import com.bti.model.GLConfigurationSetup;
import com.bti.model.MasterBlncdspl;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoGeneralLedgerSetup {

	private int id;
	private Integer nextJournalEntry;
	private Integer nextBudgetJournalEntry;
	private Integer nextReconciliation;
	private short displayNCorPB;
	private String displayNCorPBValue;
	private boolean retainedEraningsCheckBox;
	private Integer accountSegments;
	private Long mainAccount1;
	private Long mainAccount2;
	private Integer cashPayment;
	private Integer cashReceipt;
	private Integer checkPayment;
	private Integer checkReceipt;
	private Integer transferPayment;
	private Integer transferReceipt;
	private Integer creditPayment;
	private Integer creditReceipt;
	private boolean maintainHistoryAccounts;
	private boolean maintainHistoryTransactions;
	private boolean maintainHistoryBudgetTransactions;
	private boolean allowPostingToHistory;
	private boolean allowDeletionOfSavedTrans;
	private boolean allowvoidingCorrectingSubTrans;
	private String userDefine1;
	private String userDefine2;
	private String userDefine3;
	private String userDefine4;
	private String userDefine5;
	private String accountTableRowIndex;
	private int sequenceType;
	private boolean includeDate;
	
	public DtoGeneralLedgerSetup(){
		
	}
	
	public DtoGeneralLedgerSetup(GLConfigurationSetup glConfigurationSetup,MasterBlncdspl masterBlncdspl){
		this.id = glConfigurationSetup.getId();
		this.nextJournalEntry = glConfigurationSetup.getNextJournalEntryVoucher();
		this.nextBudgetJournalEntry = glConfigurationSetup.getNextBudgetJournalId();
		this.nextReconciliation = glConfigurationSetup.getNextReconciliationNumber();
		this.sequenceType = glConfigurationSetup.getSequenceType();
		this.includeDate = glConfigurationSetup.getIncludeDate();
		this.displayNCorPB=0;
		this.displayNCorPBValue="";
		if(masterBlncdspl!=null){
			this.displayNCorPB = masterBlncdspl.getTypeId();
			this.displayNCorPBValue = masterBlncdspl.getTypePrimary();
		}
		
		this.retainedEraningsCheckBox=false;
		if(glConfigurationSetup.getTrueRetainedEaringClosing()!=null){
					this.retainedEraningsCheckBox =glConfigurationSetup.getTrueRetainedEaringClosing();
		}
		this.accountSegments = glConfigurationSetup.getLastCloseYear();
		this.mainAccount1=0L;
		if(glConfigurationSetup.getRetainedEarningsAccountIndex1()!=null){
			this.mainAccount1 = glConfigurationSetup.getRetainedEarningsAccountIndex1().getActIndx();
		}
		
		this.mainAccount2=0L;
		if(glConfigurationSetup.getRetainedEarningsAccountIndex2()!=null){
			this.mainAccount2 = glConfigurationSetup.getRetainedEarningsAccountIndex2().getActIndx();
		}
		if(glConfigurationSetup.getRetainedEarningsAccountIndex2()!=null){
			this.mainAccount2 = glConfigurationSetup.getRetainedEarningsAccountIndex2().getActIndx();
		}
		
		if(glConfigurationSetup.getCashAuditTrialPayment() != null) {
			this.cashPayment = glConfigurationSetup.getCashAuditTrialPayment().getSeriesIndex();
		}
		if(glConfigurationSetup.getCashAuditTrialReceipt() != null) {
			this.cashReceipt = glConfigurationSetup.getCashAuditTrialReceipt().getSeriesIndex();
		}
		if(glConfigurationSetup.getCheckAuditTrialPayment() != null) {
			this.checkPayment = glConfigurationSetup.getCheckAuditTrialPayment().getSeriesIndex();;
		}
		if(glConfigurationSetup.getcheckAuditTrialReceipt() != null) {
			this.checkReceipt = glConfigurationSetup.getcheckAuditTrialReceipt().getSeriesIndex();
		}
		if(glConfigurationSetup.getTransferAuditTrialPayment() != null) {
			this.transferPayment = glConfigurationSetup.getTransferAuditTrialPayment().getSeriesIndex();
		}
		if(glConfigurationSetup.getTransferAuditTrialReceipt() != null) {
			this.transferReceipt = glConfigurationSetup.getTransferAuditTrialReceipt().getSeriesIndex();
		}
		if(glConfigurationSetup.getCreditAuditTrialPayment() != null) {
			this.creditPayment = glConfigurationSetup.getCreditAuditTrialPayment().getSeriesIndex();;
		}
		if(glConfigurationSetup.getCreditAuditTrialReceipt() != null) {
			this.creditReceipt = glConfigurationSetup.getCreditAuditTrialReceipt().getSeriesIndex();;
		}

		this.maintainHistoryAccounts = glConfigurationSetup.getMaintainAccountsInHistory();
		this.maintainHistoryTransactions = glConfigurationSetup.getMaintainTransactionsInHistory();
		this.maintainHistoryBudgetTransactions = glConfigurationSetup.getMaintainBudgetTransactionsHistory();
		this.allowPostingToHistory = glConfigurationSetup.getAllowPostingToHistory();
		this.allowDeletionOfSavedTrans = glConfigurationSetup.getAllowDeletionOfSavedTransactions();
		this.allowvoidingCorrectingSubTrans = glConfigurationSetup.getAllowVoidingCorrectingOfTransactions();
		this.userDefine1 = glConfigurationSetup.getLabelUserDefine1();
		this.userDefine2 = glConfigurationSetup.getLabelUserDefine52();
		this.userDefine3 = glConfigurationSetup.getLabelUserDefine3();
		this.userDefine4 = glConfigurationSetup.getLabelUserDefine4();
		this.userDefine5 = glConfigurationSetup.getLabelUserDefine5();
		this.accountTableRowIndex="";
		if(glConfigurationSetup.getGlAccountsTableAccumulation()!=null){
			accountTableRowIndex=glConfigurationSetup.getGlAccountsTableAccumulation().getAccountTableRowIndex();
		}
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getNextJournalEntry() {
		return nextJournalEntry;
	}

	public void setNextJournalEntry(Integer nextJournalEntry) {
		this.nextJournalEntry = nextJournalEntry;
	}

	public Integer getNextBudgetJournalEntry() {
		return nextBudgetJournalEntry;
	}

	public void setNextBudgetJournalEntry(Integer nextBudgetJournalEntry) {
		this.nextBudgetJournalEntry = nextBudgetJournalEntry;
	}

	public Integer getNextReconciliation() {
		return nextReconciliation;
	}

	public void setNextReconciliation(Integer nextReconciliation) {
		this.nextReconciliation = nextReconciliation;
	}

	public short getDisplayNCorPB() {
		return displayNCorPB;
	}

	public void setDisplayNCorPB(short displayNCorPB) {
		this.displayNCorPB = displayNCorPB;
	}

	public String getDisplayNCorPBValue() {
		return displayNCorPBValue;
	}

	public void setDisplayNCorPBValue(String displayNCorPBValue) {
		this.displayNCorPBValue = displayNCorPBValue;
	}

	public boolean isRetainedEraningsCheckBox() {
		return retainedEraningsCheckBox;
	}

	public void setRetainedEraningsCheckBox(boolean retainedEraningsCheckBox) {
		this.retainedEraningsCheckBox = retainedEraningsCheckBox;
	}

	public Integer getAccountSegments() {
		return accountSegments;
	}

	public void setAccountSegments(Integer accountSegments) {
		this.accountSegments = accountSegments;
	}

	public Long getMainAccount1() {
		return mainAccount1;
	}

	public void setMainAccount1(Long mainAccount1) {
		this.mainAccount1 = mainAccount1;
	}

	public Long getMainAccount2() {
		return mainAccount2;
	}

	public void setMainAccount2(Long mainAccount2) {

		this.mainAccount2 = mainAccount2;
	}

	public boolean isMaintainHistoryAccounts() {
		return maintainHistoryAccounts;
	}

	public void setMaintainHistoryAccounts(boolean maintainHistoryAccounts) {
		this.maintainHistoryAccounts = maintainHistoryAccounts;
	}

	public boolean isMaintainHistoryTransactions() {
		return maintainHistoryTransactions;
	}

	public void setMaintainHistoryTransactions(boolean maintainHistoryTransactions) {
		this.maintainHistoryTransactions = maintainHistoryTransactions;
	}

	public boolean isMaintainHistoryBudgetTransactions() {
		return maintainHistoryBudgetTransactions;
	}

	public void setMaintainHistoryBudgetTransactions(boolean maintainHistoryBudgetTransactions) {
		this.maintainHistoryBudgetTransactions = maintainHistoryBudgetTransactions;
	}

	public boolean isAllowPostingToHistory() {
		return allowPostingToHistory;
	}

	public void setAllowPostingToHistory(boolean allowPostingToHistory) {
		this.allowPostingToHistory = allowPostingToHistory;
	}

	public boolean isAllowDeletionOfSavedTrans() {
		return allowDeletionOfSavedTrans;
	}

	public void setAllowDeletionOfSavedTrans(boolean allowDeletionOfSavedTrans) {
		this.allowDeletionOfSavedTrans = allowDeletionOfSavedTrans;
	}

	public boolean isAllowvoidingCorrectingSubTrans() {
		return allowvoidingCorrectingSubTrans;
	}

	public void setAllowvoidingCorrectingSubTrans(boolean allowvoidingCorrectingSubTrans) {
		this.allowvoidingCorrectingSubTrans = allowvoidingCorrectingSubTrans;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public String getUserDefine4() {
		return userDefine4;
	}

	public void setUserDefine4(String userDefine4) {
		this.userDefine4 = userDefine4;
	}

	public String getUserDefine5() {
		return userDefine5;
	}

	public void setUserDefine5(String userDefine5) {
		this.userDefine5 = userDefine5;
	}
	
	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}
	
	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getSequenceType() {
		return sequenceType;
	}

	public void setSequenceType(int sequenceType) {
		this.sequenceType = sequenceType;
	}

	public boolean isIncludeDate() {
		return includeDate;
	}

	public void setIncludeDate(boolean includeDate) {
		this.includeDate = includeDate;
	}

	public Integer getCashPayment() {
		return cashPayment;
	}

	public void setCashPayment(Integer cashPayment) {
		this.cashPayment = cashPayment;
	}

	public Integer getCashReceipt() {
		return cashReceipt;
	}

	public void setCashReceipt(Integer cashReceipt) {
		this.cashReceipt = cashReceipt;
	}

	public Integer getCheckPayment() {
		return checkPayment;
	}

	public void setCheckPayment(Integer checkPayment) {
		this.checkPayment = checkPayment;
	}

	public Integer getCheckReceipt() {
		return checkReceipt;
	}

	public void setCheckReceipt(Integer checkReceipt) {
		this.checkReceipt = checkReceipt;
	}

	public Integer getTransferPayment() {
		return transferPayment;
	}

	public void setTransferPayment(Integer transferPayment) {
		this.transferPayment = transferPayment;
	}

	public Integer getTransferReceipt() {
		return transferReceipt;
	}

	public void setTransferReceipt(Integer transferReceipt) {
		this.transferReceipt = transferReceipt;
	}

	public Integer getCreditPayment() {
		return creditPayment;
	}

	public void setCreditPayment(Integer creditPayment) {
		this.creditPayment = creditPayment;
	}

	public Integer getCreditReceipt() {
		return creditReceipt;
	}

	public void setCreditReceipt(Integer creditReceipt) {
		this.creditReceipt = creditReceipt;
	}
	
	

	
	
}
