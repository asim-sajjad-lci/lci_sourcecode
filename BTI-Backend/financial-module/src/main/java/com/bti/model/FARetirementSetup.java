/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the fa40008 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40008")
@NamedQuery(name="FARetirementSetup.findAll", query="SELECT f FROM FARetirementSetup f")
public class FARetirementSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RETIRINDX")
	private int retirementIndex;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRPTN")
	private String retirementDescription;

	@Column(name="DSCRPTNA")
	private String retirementDescriptionArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="RETIRID")
	private String retirementId;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public FARetirementSetup() {
	}

	public int getRetirementIndex() {
		return retirementIndex;
	}

	public void setRetirementIndex(int retirementIndex) {
		this.retirementIndex = retirementIndex;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getRetirementDescription() {
		return retirementDescription;
	}

	public void setRetirementDescription(String retirementDescription) {
		this.retirementDescription = retirementDescription;
	}

	public String getRetirementDescriptionArabic() {
		return retirementDescriptionArabic;
	}

	public void setRetirementDescriptionArabic(String retirementDescriptionArabic) {
		this.retirementDescriptionArabic = retirementDescriptionArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getRetirementId() {
		return retirementId;
	}

	public void setRetirementId(String retirementId) {
		this.retirementId = retirementId;
	}

	
}