/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl00107 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl00107")
@NamedQuery(name="GLBudgetMaintenance.findAll", query="SELECT a FROM GLBudgetMaintenance a")
public class GLBudgetMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BUDGTID")
	private String budgetID;
	
	@Column(name="DSCRPTN")
	private String budgetDescription;
	
	@Column(name="DSCRPTNA")
	private String budgetDescriptionArabic;

	@Column(name="YEAR1")
	private int budgetYear;
	
	@Column(name="ACTINDX")
	private int mainAccountIndex;

	@Column(name="CALMETHD")
	private int calculationMethod;
	
	@Column(name="CALTYPE")
	private int calculationType;
	
	@Column(name="CREATDDT")
	private Date createDate;
	
	@Column(name="MODIFTDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	public String getBudgetID() {
		return budgetID;
	}

	public void setBudgetID(String budgetID) {
		this.budgetID = budgetID;
	}

	public String getBudgetDescription() {
		return budgetDescription;
	}

	public void setBudgetDescription(String budgetDescription) {
		this.budgetDescription = budgetDescription;
	}

	public String getBudgetDescriptionArabic() {
		return budgetDescriptionArabic;
	}

	public void setBudgetDescriptionArabic(String budgetDescriptionArabic) {
		this.budgetDescriptionArabic = budgetDescriptionArabic;
	}

	public int getBudgetYear() {
		return budgetYear;
	}

	public void setBudgetYear(int budgetYear) {
		this.budgetYear = budgetYear;
	}

	public int getMainAccountIndex() {
		return mainAccountIndex;
	}

	public void setMainAccountIndex(int mainAccountIndex) {
		this.mainAccountIndex = mainAccountIndex;
	}

	public int getCalculationMethod() {
		return calculationMethod;
	}

	public void setCalculationMethod(int calculationMethod) {
		this.calculationMethod = calculationMethod;
	}

	public int getCalculationType() {
		return calculationType;
	}

	public void setCalculationType(int calculationType) {
		this.calculationType = calculationType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	

}