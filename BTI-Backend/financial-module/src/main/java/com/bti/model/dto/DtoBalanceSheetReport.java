package com.bti.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoBalanceSheetReport {
	
	private String year;

	private int period;
	
	private List<DtoAccountCategory> listOfAccountCategory;

	private Double totalLiabilitiesAndEquity;
	
	private List<Double> totals;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public List<DtoAccountCategory> getListOfAccountCategory() {
		return listOfAccountCategory;
	}

	public void setListOfAccountCategory(List<DtoAccountCategory> listOfAccountCategory) {
		this.listOfAccountCategory = listOfAccountCategory;
	}

	public Double getTotalLiabilitiesAndEquity() {
		return totalLiabilitiesAndEquity;
	}

	public void setTotalLiabilitiesAndEquity(Double totalLiabilitiesAndEquity) {
		this.totalLiabilitiesAndEquity = totalLiabilitiesAndEquity;
	}

	public List<Double> getTotals() {
		return totals;
	}

	public void setTotals(List<Double> totals) {
		this.totals = totals;
	}
	
	
	
}
