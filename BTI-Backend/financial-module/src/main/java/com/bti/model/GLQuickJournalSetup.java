/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl40300 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40300")
@NamedQuery(name="GLQuickJournalSetup.findAll", query="SELECT s FROM GLQuickJournalSetup s")
public class GLQuickJournalSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
 	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="JORNALID")
	private String journalID;

	@Column(name="DSCRPTN")
	private String journalDescription;

	@Column(name="DSCRPTNA")
	private String journalDescriptionArabic;

	@Column(name="SOURDOC")
	private String sourceDocumentID;
	
	@Column(name="REFRNCE")
	private String journalReference;
	
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;
	
	@Column(name="CREATDDT")
	private Date createdDate;
	
	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private int modifyByUserID;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	  
	public GLQuickJournalSetup() {
	}

	public String getJournalID() {
		return journalID;
	}

	public void setJournalID(String journalID) {
		this.journalID = journalID;
	}

	public String getJournalDescription() {
		return journalDescription;
	}

	public void setJournalDescription(String journalDescription) {
		this.journalDescription = journalDescription;
	}

	public String getJournalDescriptionArabic() {
		return journalDescriptionArabic;
	}

	public void setJournalDescriptionArabic(String journalDescriptionArabic) {
		this.journalDescriptionArabic = journalDescriptionArabic;
	}

	public String getSourceDocumentID() {
		return sourceDocumentID;
	}

	public void setSourceDocumentID(String sourceDocumentID) {
		this.sourceDocumentID = sourceDocumentID;
	}

	public String getJournalReference() {
		return journalReference;
	}

	public void setJournalReference(String journalReference) {
		this.journalReference = journalReference;
	}
	
	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(int modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	 
	
}