/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FAClassSetup;
import com.bti.model.FAPurchasePostingAccountSetup;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: The DtoFAPurchasePostingAccountSetup class for FAPurchasePostingAccountSetup 
 * Name of Project: BTI
 * Created on: May 19, 2017
 * Modified on: May 19, 2017 12:19:38 PM
 * @author seasia
 * Version: 
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoFAPurchasePostingAccountSetup {
	private String classId;
	private String accountTableRowIndex;
	private String classSetupDescription;
	private String accountDescriptionArabic;
	private String classSetupDescriptionArabic;
	private String accountDescription;
	private String messageType;
	private int fixedAssetClassIndex;

	public DtoFAPurchasePostingAccountSetup() {
		// TODO Auto-generated constructor stub
	}
	public DtoFAPurchasePostingAccountSetup(FAPurchasePostingAccountSetup faPurchasePostingAccountSetup) {
		this.fixedAssetClassIndex=faPurchasePostingAccountSetup.getFixedAssetClassIndex();
		FAClassSetup faClassSetup= faPurchasePostingAccountSetup.getFaClassSetup();
		this.setClassId("");
		this.setClassSetupDescription("");
		this.setClassSetupDescriptionArabic("");
		if(faClassSetup!=null){
			this.setClassId(faClassSetup.getClassId());
			if(UtilRandomKey.isNotBlank(faClassSetup.getClassSetupDescription())){
				this.setClassSetupDescription(faClassSetup.getClassSetupDescription());
			}
			if(UtilRandomKey.isNotBlank(faClassSetup.getClassSetupDescriptionArabic())){
				this.setClassSetupDescriptionArabic(faClassSetup.getClassSetupDescriptionArabic());
			}
			this.setAccountTableRowIndex("");
			this.setAccountDescription("");
			if(faPurchasePostingAccountSetup.getGlAccountsTableAccumulation()!=null){
				this.setAccountTableRowIndex(faPurchasePostingAccountSetup.getGlAccountsTableAccumulation().getAccountTableRowIndex());
				this.setAccountDescription(faPurchasePostingAccountSetup.getGlAccountsTableAccumulation().getAccountDescription());
			}
		}
		
	}
	 
	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}
	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}
	 
	public String getAccountDescriptionArabic() {
		return accountDescriptionArabic;
	}
	public void setAccountDescriptionArabic(String accountDescriptionArabic) {
		this.accountDescriptionArabic = accountDescriptionArabic;
	}
	 
	public String getAccountDescription() {
		return accountDescription;
	}
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getClassSetupDescription() {
		return classSetupDescription;
	}
	public void setClassSetupDescription(String classSetupDescription) {
		this.classSetupDescription = classSetupDescription;
	}
	public String getClassSetupDescriptionArabic() {
		return classSetupDescriptionArabic;
	}
	public void setClassSetupDescriptionArabic(String classSetupDescriptionArabic) {
		this.classSetupDescriptionArabic = classSetupDescriptionArabic;
	}
	public int getFixedAssetClassIndex() {
		return fixedAssetClassIndex;
	}
	public void setFixedAssetClassIndex(int fixedAssetClassIndex) {
		this.fixedAssetClassIndex = fixedAssetClassIndex;
	}
	 
}
