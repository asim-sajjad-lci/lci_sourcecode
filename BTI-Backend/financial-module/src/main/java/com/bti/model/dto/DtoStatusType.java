/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.constant.ConfigSetting;
import com.bti.model.CreditCardType;
import com.bti.model.CustomerMaintenanceActiveStatus;
import com.bti.model.CustomerMaintenanceHoldStatus;
import com.bti.model.FAAccountTableAccountType;
import com.bti.model.FABookMaintenanceAmortizationCodeType;
import com.bti.model.FABookMaintenanceAveragingConventionType;
import com.bti.model.FABookMaintenanceSwitchOverType;
import com.bti.model.FADepreciationMethods;
import com.bti.model.GLConfigurationAuditTrialSeriesType;
import com.bti.model.MasterAPManualPaymentType;
import com.bti.model.MasterARCashReceiptType;
import com.bti.model.MasterDocumentType;
import com.bti.model.PMDocumentsTypeSetup;
import com.bti.model.PMSetupFormatType;
import com.bti.model.PaymentMethodType;
import com.bti.model.RMDocumentsTypeSetup;
import com.bti.model.ReceiptType;
import com.bti.model.SeriesType;
import com.bti.model.ShipmentMethodType;
import com.bti.model.TPCLBalanceType;
import com.bti.model.VATBasedOnType;
import com.bti.model.VATType;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: The DTO class for Search Operations Name of Project: BTI Created
 * on: May 19, 2017 Modified on: May 19, 2017 12:19:38 PM
 * 
 * @author seasia Version:
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoStatusType {
	private String name;
	private Integer typeId;
	private String code;

	public DtoStatusType() {
		
	}

	public DtoStatusType(CustomerMaintenanceActiveStatus customerMaintenanceActiveStatus) {
			this.name = customerMaintenanceActiveStatus.getTypePrimary();
		this.typeId = customerMaintenanceActiveStatus.getTypeId();
	}

	public DtoStatusType(CustomerMaintenanceHoldStatus customerMaintenanceHoldStatus) {
			this.name = customerMaintenanceHoldStatus.getTypePrimary();
		this.typeId = customerMaintenanceHoldStatus.getTypeId();
	}
	
	public DtoStatusType(ShipmentMethodType shipmentMethodType) {
			this.name = shipmentMethodType.getTypePrimary();
		this.typeId = shipmentMethodType.getTypeId();
	}

	public DtoStatusType(CreditCardType creditCardType) {
			this.name = creditCardType.getTypePrimary();
		this.typeId = creditCardType.getTypeId();
	}

	public DtoStatusType(VATType vatType) {
			this.name = vatType.getTypePrimary();
		this.typeId = vatType.getTypeId();
	}

	public DtoStatusType(TPCLBalanceType tpclBalanceType) {
			this.name = tpclBalanceType.getTypePrimary();
		this.typeId = tpclBalanceType.getTypeId();
	}

	public DtoStatusType(VATBasedOnType vatBasedOnType) {
			this.name = vatBasedOnType.getTypePrimary();
		this.typeId = vatBasedOnType.getTypeId();
	}
	
	public DtoStatusType(PMSetupFormatType pmSetupFormatType) {
			this.name = pmSetupFormatType.getTypePrimary();
		this.typeId = pmSetupFormatType.getTypeId();
	}

	public DtoStatusType(FABookMaintenanceAmortizationCodeType faBookMaintenanceAmortizationCodeType) {
			this.name = faBookMaintenanceAmortizationCodeType.getTypePrimary();
		this.typeId = faBookMaintenanceAmortizationCodeType.getTypeId();
	}

	public DtoStatusType(FABookMaintenanceSwitchOverType faBookMaintenanceSwitchOverType) {
			this.name = faBookMaintenanceSwitchOverType.getTypePrimary();
		this.typeId = faBookMaintenanceSwitchOverType.getTypeId();
	}

	public DtoStatusType(FABookMaintenanceAveragingConventionType faBookMaintenanceAveragingConventionType) {
			this.name = faBookMaintenanceAveragingConventionType.getTypePrimary();
		this.typeId = faBookMaintenanceAveragingConventionType.getTypeId();
	}

	public DtoStatusType(FADepreciationMethods faDepreciationMethods, String langId) {
		if (ConfigSetting.PRIMARY.getValue().equalsIgnoreCase(langId)) {
			this.name = faDepreciationMethods.getDepreciationDescription();
			
		} else if (ConfigSetting.SECONDARY.getValue().equalsIgnoreCase(langId)) {
			this.name = faDepreciationMethods.getDepreciationDescriptionArabic();
		}
		this.typeId = faDepreciationMethods.getDepreciationMethodId();
	}
 
	public DtoStatusType(GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType) {
			this.name = glConfigurationAuditTrialSeriesType.getTypePrimary();
		this.typeId = glConfigurationAuditTrialSeriesType.getTypeId();
	}

	public DtoStatusType(FAAccountTableAccountType faAccountTableAccountType) {
			this.name = faAccountTableAccountType.getTypePrimary();
		this.typeId = faAccountTableAccountType.getTypeId();
	}

	public DtoStatusType(SeriesType seriesType) {
			this.name = seriesType.getTypePrimary();
		this.typeId = seriesType.getTypeId();
	}

	public DtoStatusType(PaymentMethodType paymentMethodType) {
		this.name = paymentMethodType.getValue();
		this.typeId = paymentMethodType.getTypeId();
	}

	public DtoStatusType(ReceiptType receiptType) {
		this.name = receiptType.getValue();
		this.typeId = receiptType.getTypeId();
	}

	 

	public DtoStatusType(RMDocumentsTypeSetup rmDocumentsTypeSetup) {
		this.name = rmDocumentsTypeSetup.getDocumentType();
		this.typeId = rmDocumentsTypeSetup.getArDocumentTypeId();
		this.code=rmDocumentsTypeSetup.getDocumentCode();
	}

	public DtoStatusType(MasterARCashReceiptType masterARCashReceiptType) {
		this.name = masterARCashReceiptType.getValue();
		this.typeId = masterARCashReceiptType.getTypeId();
	}
			
	public DtoStatusType(MasterAPManualPaymentType masterAPManualPaymentType) {
		this.name = masterAPManualPaymentType.getValue();
		this.typeId = masterAPManualPaymentType.getTypeId();
		}

	public DtoStatusType(PMDocumentsTypeSetup apTransactionType) {
		this.name = apTransactionType.getDocumentType();
		this.typeId = apTransactionType.getApDocumentTypeId();
		this.code=apTransactionType.getDocumentCode();
	}
	
	public DtoStatusType(MasterDocumentType masterDocumentType) {
		this.name = masterDocumentType.getDocumentType();
		this.typeId = masterDocumentType.getTypeId();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
