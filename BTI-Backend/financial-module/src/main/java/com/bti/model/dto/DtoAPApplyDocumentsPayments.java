package com.bti.model.dto;

import com.bti.util.UtilRoundDecimal;

public class DtoAPApplyDocumentsPayments {

	private String vendorId;
	private String arTransactionNumber;
	private String arTransactionDate;
	private String arTransactionPostingDate;
	private int paymentAlreadyPosted;
	private String applyDate;
	private String applyDocumentNumber;
	private double applyAmount;
	private double originalAmount;
	private double remainAmountAfterApply;
 
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getArTransactionNumber() {
		return arTransactionNumber;
	}
	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}
	public String getArTransactionDate() {
		return arTransactionDate;
	}
	public void setArTransactionDate(String arTransactionDate) {
		this.arTransactionDate = arTransactionDate;
	}
	public String getArTransactionPostingDate() {
		return arTransactionPostingDate;
	}
	public void setArTransactionPostingDate(String arTransactionPostingDate) {
		this.arTransactionPostingDate = arTransactionPostingDate;
	}
	public int getPaymentAlreadyPosted() {
		return paymentAlreadyPosted;
	}
	public void setPaymentAlreadyPosted(int paymentAlreadyPosted) {
		this.paymentAlreadyPosted = paymentAlreadyPosted;
	}
	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	 
	public String getApplyDocumentNumber() {
		return applyDocumentNumber;
	}
	public void setApplyDocumentNumber(String applyDocumentNumber) {
		this.applyDocumentNumber = applyDocumentNumber;
	}
	public double getApplyAmount() {
		return applyAmount;
	}
	public void setApplyAmount(double applyAmount) {
		this.applyAmount = UtilRoundDecimal.roundDecimalValue(applyAmount);
	}
	public double getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(double originalAmount) {
		this.originalAmount = UtilRoundDecimal.roundDecimalValue(originalAmount);
	}
	public double getRemainAmountAfterApply() {
		return remainAmountAfterApply;
	}
	public void setRemainAmountAfterApply(double remainAmountAfterApply) {
		this.remainAmountAfterApply = UtilRoundDecimal.roundDecimalValue(remainAmountAfterApply);
	}
	
	
	
}
