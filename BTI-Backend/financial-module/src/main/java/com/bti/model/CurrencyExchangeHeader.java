/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the mc40201 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "mc40201")
@NamedQuery(name="CurrencyExchangeHeader.findAll", query="SELECT m FROM CurrencyExchangeHeader m")
public class CurrencyExchangeHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="EXGTBLINXD")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int exchangeIndex;
	
	@Column(name="EXGTBLID")
	private String exchangeId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="CRNCYDSCA")
	private String exchangeDescriptionArabic;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="EXGTBDSC")
	private String exchangeDescription;


	@Column(name="EXTBLSRC")
	private String exchangeRateSource;

	@Column(name="EXTBLSRCA")
	private String exchangeRateSourceArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@ManyToOne
	@JoinColumn(name = "RATEEXPR")
	private MasterRateFrequencyTypes masterRateFrequencyTypes;

	@Column(name="RATEVARC")
	private Double rateVariance;
	
	@Column(name="RATECALMTHD")
	private String rateCalcMethod;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	

	//bi-directional many-to-one association to Mc40200
	@ManyToOne
	@JoinColumn(name="CURNCYID")
	private CurrencySetup currencySetup;

	//bi-directional one-to-one association to Mc40202
	@OneToOne(mappedBy="currencyExchangeHeader")
	private CurrencyExchangeDetail currencyExchangeDetail;

	@Column(name="ISDFLT" , columnDefinition = "tinyint(0) default 0")
	private boolean isDefault;


	public CurrencyExchangeHeader() {
	}

	public int getExchangeIndex() {
		return exchangeIndex;
	}

	public void setExchangeIndex(int exchangeIndex) {
		this.exchangeIndex = exchangeIndex;
	}

	

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getExchangeDescriptionArabic() {
		return exchangeDescriptionArabic;
	}

	public void setExchangeDescriptionArabic(String exchangeDescriptionArabic) {
		this.exchangeDescriptionArabic = exchangeDescriptionArabic;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getExchangeDescription() {
		return exchangeDescription;
	}

	public void setExchangeDescription(String exchangeDescription) {
		this.exchangeDescription = exchangeDescription;
	}

	public String getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}

	public String getExchangeRateSource() {
		return exchangeRateSource;
	}

	public void setExchangeRateSource(String exchangeRateSource) {
		this.exchangeRateSource = exchangeRateSource;
	}

	public String getExchangeRateSourceArabic() {
		return exchangeRateSourceArabic;
	}

	public void setExchangeRateSourceArabic(String exchangeRateSourceArabic) {
		this.exchangeRateSourceArabic = exchangeRateSourceArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public MasterRateFrequencyTypes getMasterRateFrequencyTypes() {
		return masterRateFrequencyTypes;
	}

	public void setMasterRateFrequencyTypes(MasterRateFrequencyTypes masterRateFrequencyTypes) {
		this.masterRateFrequencyTypes = masterRateFrequencyTypes;
	}

	public Double getRateVariance() {
		return rateVariance;
	}

	public void setRateVariance(Double rateVariance) {
		this.rateVariance = rateVariance;
	}

	public CurrencySetup getCurrencySetup() {
		return currencySetup;
	}

	public void setCurrencySetup(CurrencySetup currencySetup) {
		this.currencySetup = currencySetup;
	}

	public CurrencyExchangeDetail getCurrencyExchangeDetail() {
		return currencyExchangeDetail;
	}

	public void setCurrencyExchangeDetail(CurrencyExchangeDetail currencyExchangeDetail) {
		this.currencyExchangeDetail = currencyExchangeDetail;
	}

	public String getRateCalcMethod() {
		return rateCalcMethod;
	}

	public void setRateCalcMethod(String rateCalcMethod) {
		this.rateCalcMethod = rateCalcMethod;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}