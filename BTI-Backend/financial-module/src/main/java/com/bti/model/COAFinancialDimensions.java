/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the gl00102 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl00102")
@NamedQuery(name="COAFinancialDimensions.findAll", query="SELECT g FROM COAFinancialDimensions g")
public class COAFinancialDimensions implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DIMINXD")
	private int dimInxd;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DIMCOLNAME")
	private String dimensioncolumnname;

	@Column(name="DIMMASK")
	private String dimensionMask;

	@Column(name="DIMNAME")
	private String dimensionDescription;

	@Column(name="DIMNAMEA")
	private String dimensionDescriptionArabic;

	@Column(name="MODIFDT")
	private Date modiftDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Gl00103
	@OneToMany(mappedBy="coaFinancialDimensions")
	private List<COAFinancialDimensionsValues> coaFinancialDimensionsValues;

	//bi-directional many-to-one association to Gl49998
//	@OneToMany(mappedBy="coaFinancialDimensions")
//	private List<GLConfigurationRelationBetweenDimensionsAndMainAccount> glConfigurationRelationBetweenDimensionsAndMainAccounts;

	//bi-directional many-to-one association to Gl49999
	/*@OneToMany(mappedBy="coaFinancialDimensions")
	private List<GLAccountsTableAccumulation> glAccountsTableAccumulations;*/

	public COAFinancialDimensions() {
	}

	public int getDimInxd() {
		return dimInxd;
	}

	public void setDimInxd(int dimInxd) {
		this.dimInxd = dimInxd;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getDimensioncolumnname() {
		return dimensioncolumnname;
	}

	public void setDimensioncolumnname(String dimensioncolumnname) {
		this.dimensioncolumnname = dimensioncolumnname;
	}

	public String getDimensionMask() {
		return dimensionMask;
	}

	public void setDimensionMask(String dimensionMask) {
		this.dimensionMask = dimensionMask;
	}

	public String getDimensionDescription() {
		return dimensionDescription;
	}

	public void setDimensionDescription(String dimensionDescription) {
		this.dimensionDescription = dimensionDescription;
	}

	public String getDimensionDescriptionArabic() {
		return dimensionDescriptionArabic;
	}

	public void setDimensionDescriptionArabic(String dimensionDescriptionArabic) {
		this.dimensionDescriptionArabic = dimensionDescriptionArabic;
	}

	public Date getModiftDate() {
		return modiftDate;
	}

	public void setModiftDate(Date modiftDate) {
		this.modiftDate = modiftDate;
	}

	public List<COAFinancialDimensionsValues> getCoaFinancialDimensionsValues() {
		return coaFinancialDimensionsValues;
	}

	public void setCoaFinancialDimensionsValues(List<COAFinancialDimensionsValues> coaFinancialDimensionsValues) {
		this.coaFinancialDimensionsValues = coaFinancialDimensionsValues;
	}

	public List<GLConfigurationRelationBetweenDimensionsAndMainAccount> getGlConfigurationRelationBetweenDimensionsAndMainAccounts() {
//		return glConfigurationRelationBetweenDimensionsAndMainAccounts;
		return null;
	}

	public void setGlConfigurationRelationBetweenDimensionsAndMainAccounts(
			List<GLConfigurationRelationBetweenDimensionsAndMainAccount> glConfigurationRelationBetweenDimensionsAndMainAccounts) {
//		this.glConfigurationRelationBetweenDimensionsAndMainAccounts = glConfigurationRelationBetweenDimensionsAndMainAccounts;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/*public List<GLAccountsTableAccumulation> getGlAccountsTableAccumulations() {
		return glAccountsTableAccumulations;
	}

	public void setGlAccountsTableAccumulations(List<GLAccountsTableAccumulation> glAccountsTableAccumulations) {
		this.glAccountsTableAccumulations = glAccountsTableAccumulations;
	}*/
}