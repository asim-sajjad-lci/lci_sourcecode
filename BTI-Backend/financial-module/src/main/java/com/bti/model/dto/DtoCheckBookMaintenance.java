/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.CheckbookMaintenance;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoCheckBookMaintenance {
	
	private String checkBookId;
	private Boolean inactive;
	private String checkbookDescription;
	private String checkbookDescriptionArabic;
	private String currencyId;
	private String glAccountId;
	
	private Integer nextCheckNumber;
	private Integer nextDepositNumber;
	private String lastReconciledDate;
	private String lastReconciledBalance;
	
	private String officialBankAccountNumber;
	private String bankId;
	private String userDefine1;
	private String userDefine2;
	private String currentCheckbookBalance;
	private String cashAccountBalance;
	
	private Integer exceedMaxCheckAmount;
	private String passwordOfMaxCheckAmount;
	private Boolean duplicateCheckNumber;
	private Boolean overrideCheckNumber;
	
	private Integer checkFormat;
	private String messageType;
	private String accountNumber;
	private String AccountRowIDIndex;
	private String bankName;
	
	public DtoCheckBookMaintenance(CheckbookMaintenance checkbookMaintenance) 
	{
		this.checkBookId=checkbookMaintenance.getCheckBookId();
		this.inactive=checkbookMaintenance.getInactive();
		this.checkbookDescription="";
		if(UtilRandomKey.isNotBlank(checkbookMaintenance.getCheckbookDescription())){
			this.checkbookDescription=checkbookMaintenance.getCheckbookDescription();
		}
		this.checkbookDescriptionArabic="";
		if(UtilRandomKey.isNotBlank(checkbookMaintenance.getCheckbookDescriptionArabic())){
			this.checkbookDescriptionArabic=checkbookMaintenance.getCheckbookDescriptionArabic();
		}
		this.currencyId="";
		if(checkbookMaintenance.getCurrencySetup()!=null){
			this.currencyId=checkbookMaintenance.getCurrencySetup().getCurrencyId();
		}
		this.glAccountId="";
		if(checkbookMaintenance.getGlAccountsTableAccumulation()!=null){
			this.glAccountId=String.valueOf(checkbookMaintenance.getGlAccountsTableAccumulation().getAccountTableRowIndex());
		}
		this.nextCheckNumber=checkbookMaintenance.getNextCheckNumber();
		this.nextDepositNumber=checkbookMaintenance.getNextDepositNumber();
		this.lastReconciledBalance="";
		if(checkbookMaintenance.getLastReconciledBalance()!=null){
			this.lastReconciledBalance=String.valueOf(UtilRoundDecimal.roundDecimalValue(checkbookMaintenance.getLastReconciledBalance()));
		}
		this.lastReconciledDate="";
		if(checkbookMaintenance.getLastReconciledDate()!=null){
			this.lastReconciledDate=UtilDateAndTime.dateToStringddmmyyyy(checkbookMaintenance.getLastReconciledDate());
		}
		this.officialBankAccountNumber="";
		if(UtilRandomKey.isNotBlank(checkbookMaintenance.getOfficialBankAccountNumber())){
			this.officialBankAccountNumber=checkbookMaintenance.getOfficialBankAccountNumber();
		}
		this.bankId="";
		if(checkbookMaintenance.getBankSetup()!=null){
			this.bankId=checkbookMaintenance.getBankSetup().getBankId();
		}
		this.userDefine1="";
		this.userDefine2="";
		if(UtilRandomKey.isNotBlank(checkbookMaintenance.getUserDefine1())){
			userDefine1=checkbookMaintenance.getUserDefine1();
		}
        if(UtilRandomKey.isNotBlank(checkbookMaintenance.getUserDefine2())){
        	userDefine2=checkbookMaintenance.getUserDefine2();
		}
        this.currentCheckbookBalance="";
        this.cashAccountBalance="";
		
		if(checkbookMaintenance.getCurrentCheckbookBalance()!=null){
			currentCheckbookBalance=String.valueOf(UtilRoundDecimal.roundDecimalValue(checkbookMaintenance.getCurrentCheckbookBalance()));
		}
		if(checkbookMaintenance.getCashAccountBalance()!=null){
			cashAccountBalance=String.valueOf(UtilRoundDecimal.roundDecimalValue(checkbookMaintenance.getCashAccountBalance()));
		}
		this.exceedMaxCheckAmount=checkbookMaintenance.getExceedMaxCheckAmount();
		this.passwordOfMaxCheckAmount=checkbookMaintenance.getPasswordOfMaxCheckAmount();
		this.duplicateCheckNumber=checkbookMaintenance.getDuplicateCheckNumber();
		this.overrideCheckNumber=checkbookMaintenance.getOverrideCheckNumber();
	}
	
	public DtoCheckBookMaintenance() {
		
	}
	
	public String getCheckBookId() {
		return checkBookId;
	}
	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}
	public String getOfficialBankAccountNumber() {
		return officialBankAccountNumber;
	}
	public void setOfficialBankAccountNumber(String officialBankAccountNumber) {
		this.officialBankAccountNumber = officialBankAccountNumber;
	}
	
	public Integer getCheckFormat() {
		return checkFormat;
	}
	public void setCheckFormat(Integer checkFormat) {
		this.checkFormat = checkFormat;
	}
	public String getCurrentCheckbookBalance() {
		return currentCheckbookBalance;
	}

	public void setCurrentCheckbookBalance(String currentCheckbookBalance) {
		this.currentCheckbookBalance = currentCheckbookBalance;
	}

	public String getCashAccountBalance() {
		return cashAccountBalance;
	}

	public void setCashAccountBalance(String cashAccountBalance) {
		this.cashAccountBalance = cashAccountBalance;
	}

	public String getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	public String getCheckbookDescription() {
		return checkbookDescription;
	}
	public void setCheckbookDescription(String checkbookDescription) {
		this.checkbookDescription = checkbookDescription;
	}
	public String getCheckbookDescriptionArabic() {
		return checkbookDescriptionArabic;
	}
	public void setCheckbookDescriptionArabic(String checkbookDescriptionArabic) {
		this.checkbookDescriptionArabic = checkbookDescriptionArabic;
	}
	public Boolean getDuplicateCheckNumber() {
		return duplicateCheckNumber;
	}
	public void setDuplicateCheckNumber(Boolean duplicateCheckNumber) {
		this.duplicateCheckNumber = duplicateCheckNumber;
	}
	public Integer getExceedMaxCheckAmount() {
		return exceedMaxCheckAmount;
	}
	public void setExceedMaxCheckAmount(Integer exceedMaxCheckAmount) {
		this.exceedMaxCheckAmount = exceedMaxCheckAmount;
	}
	public Boolean getInactive() {
		return inactive;
	}
	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}
	public String getPasswordOfMaxCheckAmount() {
		return passwordOfMaxCheckAmount;
	}
	public void setPasswordOfMaxCheckAmount(String passwordOfMaxCheckAmount) {
		this.passwordOfMaxCheckAmount = passwordOfMaxCheckAmount;
	}
	public Integer getNextCheckNumber() {
		return nextCheckNumber;
	}
	public void setNextCheckNumber(Integer nextCheckNumber) {
		this.nextCheckNumber = nextCheckNumber;
	}
	public Integer getNextDepositNumber() {
		return nextDepositNumber;
	}
	public void setNextDepositNumber(Integer nextDepositNumber) {
		this.nextDepositNumber = nextDepositNumber;
	}
	public Boolean getOverrideCheckNumber() {
		return overrideCheckNumber;
	}
	public void setOverrideCheckNumber(Boolean overrideCheckNumber) {
		this.overrideCheckNumber = overrideCheckNumber;
	}
	public String getUserDefine1() {
		return userDefine1;
	}
	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}
	public String getUserDefine2() {
		return userDefine2;
	}
	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}
	
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getGlAccountId() {
		return glAccountId;
	}
	public void setGlAccountId(String glAccountId) {
		this.glAccountId = glAccountId;
	}
	public String getLastReconciledDate() {
		return lastReconciledDate;
	}
	public void setLastReconciledDate(String lastReconciledDate) {
		this.lastReconciledDate = lastReconciledDate;
	}
	public String getLastReconciledBalance() {
		return lastReconciledBalance;
	}
	public void setLastReconciledBalance(String lastReconciledBalance) {
		this.lastReconciledBalance = lastReconciledBalance;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getAccountRowIDIndex() {
		return AccountRowIDIndex;
	}

	public void setAccountRowIDIndex(String accountRowIDIndex) {
		AccountRowIDIndex = accountRowIDIndex;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	
}
