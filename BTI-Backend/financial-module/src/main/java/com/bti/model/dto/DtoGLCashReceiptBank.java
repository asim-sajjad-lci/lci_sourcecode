/**
 * BTI - BAAN for Technology And Trade IntegerL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.GLCashReceiptBank;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoGLCashReceiptBank {

	private Integer bankCashReceipt;
	private String receiptNumber;
	private String checkBookId;
	private String currencyID;
	private String receiptDate;
	private Integer receiptType;
	private String depositorName;
	private Integer paymentMethod;
	private String accountTableRowIndex;
	private String accountTableRowIndexDebit;
	private String receiptDescription;
	private Double receiptAmount;
	private String checkNumber;
	private String bankName;
	private String creditCardID;
	private String creditCardNumber;
	private Integer creditCardExpireYear;
	private Integer creditCardExpireMonth;
	private String vatScheduleID;
	private Double vatAmount;
	private String batchID;
	private String customerId;
	private String exchangeTableIndex;
	private Double exchangeRate;
	private String messageType;
	private String vendorId;
	private Integer auditTrial;
	
	public DtoGLCashReceiptBank() {
	}

	public DtoGLCashReceiptBank(GLCashReceiptBank glCashReceiptBank) 
	{
		this.bankCashReceipt = glCashReceiptBank.getBankCashReceipt();
		this.receiptNumber = glCashReceiptBank.getReceiptNumber();
		this.checkBookId = glCashReceiptBank.getCheckbookMaintenance()!=null?glCashReceiptBank.getCheckbookMaintenance().getCheckBookId():"";
		this.vendorId = glCashReceiptBank.getVendorMaintenance()!=null?glCashReceiptBank.getVendorMaintenance().getVendorid():"";
		this.customerId = glCashReceiptBank.getCustomerMaintenance()!=null?glCashReceiptBank.getCustomerMaintenance().getCustnumber():"";
		this.currencyID = glCashReceiptBank.getCurrencyID();
		this.receiptDate = glCashReceiptBank.getReceiptDate()!=null ?UtilDateAndTime.dateToStringddmmyyyy(glCashReceiptBank.getReceiptDate()):"";
		this.receiptType = glCashReceiptBank.getReceiptType()!=null?glCashReceiptBank.getReceiptType().getTypeId():null;
		this.paymentMethod = glCashReceiptBank.getPaymentMethod()!=null?glCashReceiptBank.getPaymentMethod().getTypeId():null;
		this.accountTableRowIndex = glCashReceiptBank.getGlAccountsTableAccumulation()!=null?glCashReceiptBank.getGlAccountsTableAccumulation().getAccountTableRowIndex():"";
		this.accountTableRowIndexDebit = glCashReceiptBank.getGlAccountsTableAccumulationDebit() != null ? glCashReceiptBank.getGlAccountsTableAccumulationDebit().getAccountTableRowIndex(): "";
		this.receiptDescription = glCashReceiptBank.getReceiptDescription();
		this.receiptAmount = UtilRoundDecimal.roundDecimalValue(glCashReceiptBank.getReceiptAmount());
		this.checkNumber = glCashReceiptBank.getCheckNumber();
		this.bankName = glCashReceiptBank.getBankName();
		this.creditCardID = glCashReceiptBank.getCreditCardID();
		this.creditCardNumber = glCashReceiptBank.getCreditCardNumber();
		this.creditCardExpireYear = glCashReceiptBank.getCreditCardExpireYear();
		this.creditCardExpireMonth = glCashReceiptBank.getCreditCardExpireMonth();
		this.vatScheduleID = glCashReceiptBank.getVatSetup()!=null?glCashReceiptBank.getVatSetup().getVatScheduleId():"";
		this.vatAmount = UtilRoundDecimal.roundDecimalValue(glCashReceiptBank.getVatAmount());
		this.depositorName = glCashReceiptBank.getDepositorName();
		this.batchID = glCashReceiptBank.getGlBatches()!=null?glCashReceiptBank.getGlBatches().getBatchID():"";
		this.exchangeRate = UtilRoundDecimal.roundDecimalValue(glCashReceiptBank.getExchangeRate());
		this.exchangeTableIndex = glCashReceiptBank.getExchangeTableIndex();
		this.auditTrial = glCashReceiptBank.getGlConfigurationAuditTrialCOdes()!= null?
				glCashReceiptBank.getGlConfigurationAuditTrialCOdes().getSeriesIndex() : null;
		
	}

	public Integer getBankCashReceipt() {
		return bankCashReceipt;
	}

	public void setBankCashReceipt(Integer bankCashReceipt) {
		this.bankCashReceipt = bankCashReceipt;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	public Integer getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(Integer receiptType) {
		this.receiptType = receiptType;
	}

	public Integer getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(Integer paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public String getReceiptDescription() {
		return receiptDescription;
	}

	public void setReceiptDescription(String receiptDescription) {
		this.receiptDescription = receiptDescription;
	}

	public Double getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = UtilRoundDecimal.roundDecimalValue(receiptAmount);
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public Integer getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(Integer creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public Integer getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(Integer creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public String getVatScheduleID() {
		return vatScheduleID;
	}

	public void setVatScheduleID(String vatScheduleID) {
		this.vatScheduleID = vatScheduleID;
	}

	public Double getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(Double vatAmount) {
		this.vatAmount = UtilRoundDecimal.roundDecimalValue(vatAmount);
	}

	public String getDepositorName() {
		return depositorName;
	}

	public void setDepositorName(String depositorName) {
		this.depositorName = depositorName;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = UtilRoundDecimal.roundDecimalValue(exchangeRate);
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getAccountTableRowIndexDebit() {
		return accountTableRowIndexDebit;
	}

	public void setAccountTableRowIndexDebit(String accountTableRowIndexDebit) {
		this.accountTableRowIndexDebit = accountTableRowIndexDebit;
	}

	public Integer getAuditTrial() {
		return auditTrial;
	}

	public void setAuditTrial(Integer auditTrial) {
		this.auditTrial = auditTrial;
	}
	
	

	
}
