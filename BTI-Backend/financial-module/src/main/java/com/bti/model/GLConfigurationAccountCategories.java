/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the gl40001 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40001")
@NamedQuery(name="GLConfigurationAccountCategories.findAll", query="SELECT g FROM GLConfigurationAccountCategories g")
public class GLConfigurationAccountCategories implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACCATNUM")
	private int accountCategoryId;

	@Column(name="ACCATDSC")
	private String accountCategoryDescription;

	@Column(name="ACCATDSCA")
	private String accountCategoryDescriptionArabic;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	
	@Column(name="PREDEFINED")
	private int preDefined;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to gl00101
	@OneToMany(mappedBy="glConfigurationAccountCategories")
	private List<COAMainAccounts> coaMainAccounts;

	public GLConfigurationAccountCategories() {
	}

	public int getAccountCategoryId() {
		return accountCategoryId;
	}

	public void setAccountCategoryId(int accountCategoryId) {
		this.accountCategoryId = accountCategoryId;
	}

	public String getAccountCategoryDescription() {
		return accountCategoryDescription;
	}

	public void setAccountCategoryDescription(String accountCategoryDescription) {
		this.accountCategoryDescription = accountCategoryDescription;
	}

	public String getAccountCategoryDescriptionArabic() {
		return accountCategoryDescriptionArabic;
	}

	public void setAccountCategoryDescriptionArabic(String accountCategoryDescriptionArabic) {
		this.accountCategoryDescriptionArabic = accountCategoryDescriptionArabic;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public List<COAMainAccounts> getCoaMainAccounts() {
		return coaMainAccounts;
	}

	public void setCoaMainAccounts(List<COAMainAccounts> coaMainAccounts) {
		this.coaMainAccounts = coaMainAccounts;
	}

	public int getPreDefined() {
		return preDefined;
	}

	public void setPreDefined(int preDefined) {
		this.preDefined = preDefined;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
    
	



}