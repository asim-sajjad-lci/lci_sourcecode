/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10500 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10500")
@NamedQuery(name="GLBankTransferHeader.findAll", query="SELECT a FROM GLBankTransferHeader a")
public class GLBankTransferHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BNKTRNSF")
	private int bankTransferNumber;
	
	@Column(name="TRFDT")
	private Date transferDate;
	
	@Column(name="CURNCYID")
	private String currencyId;

	@Column(name="EXGTBLID")
	private String exchangeTableID;
	
	@Column(name="COMPIDF")
	private String companyTransferFrom;

	@Column(name="CHEKBOKIDF")
	private String checkBookIdFrom;
	
	@Column(name="COMNTF")
	private String commentFrom;
	
	@Column(name="BNKAMTF")
	private double transferAmountFrom;
	
	@Column(name="RATCLMETHDF")
	private int rateCalculateMethodFrom;
	
	@Column(name="EXCHNRATEF")
	private Double exchangeRateFrom;

	@Column(name="COMPIDT")
	private String companyTransferTo;
	
	@Column(name="CHEKBOKIDT")
	private String checkbookIdTo;
	
	@Column(name="COMNTT")
	private String commentTo ;

	@Column(name="BNKAMTT")
	private double transferAmountTo ;

	@Column(name="RATCLMETHDT")
	private int rateCalculateMethodTo ;

	@Column(name="EXCHNRATET")
	private Double exchangeRateTo ;

	@ManyToOne
	@JoinColumn(name="BATCHID")
	private GLBatches glBatches ;

	@Column(name="CREATDDT")
	private Date createDate ;

	@Column(name="MODFITDT")
	private Date modifyDate ;

	@Column(name="CHANGEBY")
	private String modifyByUserID;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex ;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex ;

	public int getBankTransferNumber() {
		return bankTransferNumber;
	}

	public void setBankTransferNumber(int bankTransferNumber) {
		this.bankTransferNumber = bankTransferNumber;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getExchangeTableID() {
		return exchangeTableID;
	}

	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}

	public String getCompanyTransferFrom() {
		return companyTransferFrom;
	}

	public void setCompanyTransferFrom(String companyTransferFrom) {
		this.companyTransferFrom = companyTransferFrom;
	}

	public String getCommentFrom() {
		return commentFrom;
	}

	public void setCommentFrom(String commentFrom) {
		this.commentFrom = commentFrom;
	}

	public double getTransferAmountFrom() {
		return transferAmountFrom;
	}

	public void setTransferAmountFrom(double transferAmountFrom) {
		this.transferAmountFrom = transferAmountFrom;
	}

	public int getRateCalculateMethodFrom() {
		return rateCalculateMethodFrom;
	}

	public void setRateCalculateMethodFrom(int rateCalculateMethodFrom) {
		this.rateCalculateMethodFrom = rateCalculateMethodFrom;
	}

	public Double getExchangeRateFrom() {
		return exchangeRateFrom;
	}

	public void setExchangeRateFrom(Double exchangeRateFrom) {
		this.exchangeRateFrom = exchangeRateFrom;
	}

	public String getCompanyTransferTo() {
		return companyTransferTo;
	}

	public void setCompanyTransferTo(String companyTransferTo) {
		this.companyTransferTo = companyTransferTo;
	}

	public String getCommentTo() {
		return commentTo;
	}

	public void setCommentTo(String commentTo) {
		this.commentTo = commentTo;
	}

	public double getTransferAmountTo() {
		return transferAmountTo;
	}

	public void setTransferAmountTo(double transferAmountTo) {
		this.transferAmountTo = transferAmountTo;
	}

	public int getRateCalculateMethodTo() {
		return rateCalculateMethodTo;
	}

	public void setRateCalculateMethodTo(int rateCalculateMethodTo) {
		this.rateCalculateMethodTo = rateCalculateMethodTo;
	}

	public Double getExchangeRateTo() {
		return exchangeRateTo;
	}

	public void setExchangeRateTo(Double exchangeRateTo) {
		this.exchangeRateTo = exchangeRateTo;
	}

	public GLBatches getGlBatches() {
		return glBatches;
	}

	public void setGlBatches(GLBatches glBatches) {
		this.glBatches = glBatches;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	public String getCheckBookIdFrom() {
		return checkBookIdFrom;
	}

	public void setCheckBookIdFrom(String checkBookIdFrom) {
		this.checkBookIdFrom = checkBookIdFrom;
	}

	public String getCheckbookIdTo() {
		return checkbookIdTo;
	}

	public void setCheckbookIdTo(String checkbookIdTo) {
		this.checkbookIdTo = checkbookIdTo;
	}

             


}