/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.BankSetup;

public class DtoBankSetUp {
	
	private String bankId;
	private String bankDescription;
	private String bankArabicDescription;
	private String address1;
	private String address2;
	private String address3;
	private String phone1;
	private String phone2;
	private String phone3;
	private Integer cityId;
	private Integer stateId;
	private Integer countryId;
	private String fax;	
	private String countryName;
	private String stateName;
	private String cityName;
	private List<String> bankIds;
	
	public DtoBankSetUp() {

	}
	
	public DtoBankSetUp(BankSetup bankSetup) {
		this.bankId=bankSetup.getBankId();
		this.bankDescription=bankSetup.getBankDescription();
		if(bankSetup.getBankDescriptionArabic()!=null){
			this.bankArabicDescription=bankSetup.getBankDescriptionArabic();
		}
		else
		{
			this.bankArabicDescription="";
		}
		
		if(bankSetup.getAddress1()!=null){
			this.address1=bankSetup.getAddress1();
		}
		else
		{
			this.address1="";
		}
		
		if(bankSetup.getAddress2()!=null){
			this.address2=bankSetup.getAddress2();
		}
		else
		{
			this.address2="";
		}
		
		if(bankSetup.getAddress3()!=null){
			this.address3=bankSetup.getAddress3();
		}
		else
		{
			this.address3="";
		}
		
		if(bankSetup.getPhone1()!=null){
			this.phone1=bankSetup.getPhone1();
		}
		else
		{
			this.phone1="";
		}
		
		if(bankSetup.getPhone2()!=null){
			this.phone2=bankSetup.getPhone2();
		}
		else
		{
			this.phone2="";
		}
		
		if(bankSetup.getPhone3()!=null){
			this.phone3=bankSetup.getPhone3();
		}
		else
		{
			this.phone3="";
		}
		
		if(bankSetup.getFax1()!=null){
			this.fax=bankSetup.getFax1();
		}
		else
		{
			this.fax="";
		}
		this.countryName=bankSetup.getCountry();
		this.stateName=bankSetup.getState();
		this.cityName=bankSetup.getCity();
		
		
	}
	

	public DtoBankSetUp(BankSetup bankSetup, String langId) {
		this.bankId=bankSetup.getBankId();
		this.bankDescription=bankSetup.getBankDescription();
		if(bankSetup.getBankDescriptionArabic()!=null){
			this.bankArabicDescription=bankSetup.getBankDescriptionArabic();
		}
		else
		{
			this.bankArabicDescription="";
		}
		
		if(bankSetup.getAddress1()!=null){
			this.address1=bankSetup.getAddress1();
		}
		else
		{
			this.address1="";
		}
		
		if(bankSetup.getAddress2()!=null){
			this.address2=bankSetup.getAddress2();
		}
		else
		{
			this.address2="";
		}
		
		if(bankSetup.getAddress3()!=null){
			this.address3=bankSetup.getAddress3();
		}
		else
		{
			this.address3="";
		}
		
		if(bankSetup.getPhone1()!=null){
			this.phone1=bankSetup.getPhone1();
		}
		else
		{
			this.phone1="";
		}
		
		if(bankSetup.getPhone2()!=null){
			this.phone2=bankSetup.getPhone2();
		}
		else
		{
			this.phone2="";
		}
		
		if(bankSetup.getPhone3()!=null){
			this.phone3=bankSetup.getPhone3();
		}
		else
		{
			this.phone3="";
		}
		
		if(bankSetup.getFax1()!=null){
			this.fax=bankSetup.getFax1();
		}
		else
		{
			this.fax="";
		}
		this.countryName=bankSetup.getCountry();
		this.stateName=bankSetup.getState();
		this.cityName=bankSetup.getCity();
		
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankDescription() {
		return bankDescription;
	}

	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}

	public String getBankArabicDescription() {
		return bankArabicDescription;
	}

	public void setBankArabicDescription(String bankArabicDescription) {
		this.bankArabicDescription = bankArabicDescription;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public List<String> getBankIds() {
		return bankIds;
	}

	public void setBankIds(List<String> bankIds) {
		this.bankIds = bankIds;
	}
	
	

}
