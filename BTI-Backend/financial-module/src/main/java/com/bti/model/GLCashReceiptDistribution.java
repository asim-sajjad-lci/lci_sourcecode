/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10401 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10401")
@NamedQuery(name="GLCashReceiptDistribution .findAll", query="SELECT a FROM GLCashReceiptDistribution  a")
public class GLCashReceiptDistribution implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BNKSEQUN")
	private int transactionSequence;
	
	@ManyToOne
	@JoinColumn(name="BNKRECPNO")
	private GLCashReceiptBank glCashReceiptBank;
	
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;

	@Column(name="ACTYPE")
	private int accountType;
	
	@Column(name="DEBITAMT")
	private double debitAmount;

	@Column(name="ORGDEBITAMT")
	private double originalDebitAmount;
	
	@Column(name="CRDTAMT")
	private double creditAmount;
	
	@Column(name="ORGCREDTAMT")
	private double originalCreditAmount;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	
	public int getTransactionSequence() {
		return transactionSequence;
	}

	public void setTransactionSequence(int transactionSequence) {
		this.transactionSequence = transactionSequence;
	}

	public GLCashReceiptBank getGlCashReceiptBank() {
		return glCashReceiptBank;
	}

	public void setGlCashReceiptBank(GLCashReceiptBank glCashReceiptBank) {
		this.glCashReceiptBank = glCashReceiptBank;
	}

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public double getOriginalCreditAmount() {
		return originalCreditAmount;
	}

	public void setOriginalCreditAmount(double originalCreditAmount) {
		this.originalCreditAmount = originalCreditAmount;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	
	
		
	

}