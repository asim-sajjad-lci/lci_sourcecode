/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.RMPeriodSetup;
import com.bti.util.UtilRandomKey;

public class DtoRMPeriodSetup {

	private Integer periodIndex;

	private int periodNoofDays;

	private String periodDescription;

	private String periodDescriptionArabic;

	private Integer periodEnd;
	
	public DtoRMPeriodSetup() {
		
	}
	
	public DtoRMPeriodSetup(RMPeriodSetup rmPeriodSetup) {
		this.periodIndex=rmPeriodSetup.getPeriodIndex();
		this.periodNoofDays=rmPeriodSetup.getPeriodNoofDays();
		this.periodDescription="";
		if(UtilRandomKey.isNotBlank(rmPeriodSetup.getPeriodDescription())){
			this.periodDescription=rmPeriodSetup.getPeriodDescription();
		}
		this.periodEnd=rmPeriodSetup.getPeriodEnd();
	}

	public Integer getPeriodIndex() {
		return periodIndex;
	}

	public void setPeriodIndex(Integer periodIndex) {
		this.periodIndex = periodIndex;
	}

	public int getPeriodNoofDays() {
		return periodNoofDays;
	}

	public void setPeriodNoofDays(int periodNoofDays) {
		this.periodNoofDays = periodNoofDays;
	}

	public String getPeriodDescription() {
		return periodDescription;
	}

	public void setPeriodDescription(String periodDescription) {
		this.periodDescription = periodDescription;
	}

	public String getPeriodDescriptionArabic() {
		return periodDescriptionArabic;
	}

	public void setPeriodDescriptionArabic(String periodDescriptionArabic) {
		this.periodDescriptionArabic = periodDescriptionArabic;
	}

	public Integer getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(Integer periodEnd) {
		this.periodEnd = periodEnd;
	}
}
