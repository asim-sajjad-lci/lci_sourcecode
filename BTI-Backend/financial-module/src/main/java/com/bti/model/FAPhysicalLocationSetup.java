/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the fa40006 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40006")
@NamedQuery(name="FAPhysicalLocationSetup.findAll", query="SELECT f FROM FAPhysicalLocationSetup f")
public class FAPhysicalLocationSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PHYSCALINDX")
	private int physicalLocationIndex;
	
	@Column(name="PHYSCALID")
	private String physicalLocationId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRPTN")
	private String physicalLocationDescription;

	@Column(name="DSCRPTNA")
	private String physicalLocationDescriptionArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Fa00101
	@OneToMany(mappedBy="faPhysicalLocationSetup")
	private List<FAGeneralMaintenance> faGeneralMaintenances;

	//bi-directional many-to-one association to Fa40005
	@ManyToOne
	@JoinColumn(name="LOCATNINDX")
	private FALocationSetup faLocationSetup;

	public FAPhysicalLocationSetup() {
	}

	public int getPhysicalLocationIndex() {
		return physicalLocationIndex;
	}

	public void setPhysicalLocationIndex(int physicalLocationIndex) {
		this.physicalLocationIndex = physicalLocationIndex;
	}


	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getPhysicalLocationDescription() {
		return physicalLocationDescription;
	}

	public void setPhysicalLocationDescription(String physicalLocationDescription) {
		this.physicalLocationDescription = physicalLocationDescription;
	}

	public String getPhysicalLocationDescriptionArabic() {
		return physicalLocationDescriptionArabic;
	}

	public void setPhysicalLocationDescriptionArabic(String physicalLocationDescriptionArabic) {
		this.physicalLocationDescriptionArabic = physicalLocationDescriptionArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getPhysicalLocationId() {
		return physicalLocationId;
	}

	public void setPhysicalLocationId(String physicalLocationId) {
		this.physicalLocationId = physicalLocationId;
	}

	public List<FAGeneralMaintenance> getFaGeneralMaintenances() {
		return faGeneralMaintenances;
	}

	public void setFaGeneralMaintenances(List<FAGeneralMaintenance> faGeneralMaintenances) {
		this.faGeneralMaintenances = faGeneralMaintenances;
	}

	public FALocationSetup getFaLocationSetup() {
		return faLocationSetup;
	}

	public void setFaLocationSetup(FALocationSetup faLocationSetup) {
		this.faLocationSetup = faLocationSetup;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	

	
}