/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.RMDocumentsTypeSetup;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoRMDocumentType {

	private int documentTypeId;

	private String documentTypeDescription;

	private String documentTypeDescriptionArabic;

	private Integer documentNumber;

	private String documentCode;
	
	private List<DtoRMDocumentType>  rmDocumentTypeList;
	
	private String docType;
	
	private String messageType;
	
	public DtoRMDocumentType() {
		
	}
	public DtoRMDocumentType(RMDocumentsTypeSetup rmDocumentsTypeSetup) 
	{
		this.documentTypeId=rmDocumentsTypeSetup.getArDocumentTypeId();
		this.documentCode="";
		if(UtilRandomKey.isNotBlank(rmDocumentsTypeSetup.getDocumentCode())){
			this.documentCode=rmDocumentsTypeSetup.getDocumentCode();
		}
		this.documentNumber=rmDocumentsTypeSetup.getDocumentNumber();
		this.documentTypeDescription="";
		if(UtilRandomKey.isNotBlank(rmDocumentsTypeSetup.getDocumentTypeDescription())){
			this.documentTypeDescription=rmDocumentsTypeSetup.getDocumentTypeDescription();
		}
		this.documentTypeDescriptionArabic="";
		if(UtilRandomKey.isNotBlank(rmDocumentsTypeSetup.getDocumentTypeDescriptionArabic())){
			this.documentTypeDescriptionArabic=rmDocumentsTypeSetup.getDocumentTypeDescriptionArabic();
		}
		this.docType="";
		if(UtilRandomKey.isNotBlank(rmDocumentsTypeSetup.getDocumentType())){
			this.docType=rmDocumentsTypeSetup.getDocumentType();
		}
	}

	public int getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(int documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}

	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	public String getDocumentTypeDescriptionArabic() {
		return documentTypeDescriptionArabic;
	}

	public void setDocumentTypeDescriptionArabic(String documentTypeDescriptionArabic) {
		this.documentTypeDescriptionArabic = documentTypeDescriptionArabic;
	}

	public Integer getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(Integer documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public List<DtoRMDocumentType> getRmDocumentTypeList() {
		return rmDocumentTypeList;
	}

	public void setRmDocumentTypeList(List<DtoRMDocumentType> rmDocumentTypeList) {
		this.rmDocumentTypeList = rmDocumentTypeList;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	
}
