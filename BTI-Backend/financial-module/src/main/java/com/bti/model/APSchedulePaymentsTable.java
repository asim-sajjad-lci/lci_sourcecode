/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ap10201 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap10201")
@NamedQuery(name="APSchedulePaymentsTable.findAll", query="SELECT a FROM APSchedulePaymentsTable a")
public class APSchedulePaymentsTable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SCHPSEQC")
	private int schedulePaymentSequence;
	
	@Column(name="SCHPNMBR")
	private String schedulePaymentNumber;
	
	@Column(name="SCHPDUEDT")
	private Date schedulePaymentDueDate;

	@Column(name="SCHPAMNT")
	private double schedulePaymentAmount;
	
	@Column(name="INTRAMNT")
	private double scheduleInterestAmount;

	@Column(name="SCHBALNC")
	private double scheduleBalance;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	public int getSchedulePaymentSequence() {
		return schedulePaymentSequence;
	}

	public void setSchedulePaymentSequence(int schedulePaymentSequence) {
		this.schedulePaymentSequence = schedulePaymentSequence;
	}

	public String getSchedulePaymentNumber() {
		return schedulePaymentNumber;
	}

	public void setSchedulePaymentNumber(String schedulePaymentNumber) {
		this.schedulePaymentNumber = schedulePaymentNumber;
	}

	public Date getSchedulePaymentDueDate() {
		return schedulePaymentDueDate;
	}

	public void setSchedulePaymentDueDate(Date schedulePaymentDueDate) {
		this.schedulePaymentDueDate = schedulePaymentDueDate;
	}

	public double getSchedulePaymentAmount() {
		return schedulePaymentAmount;
	}

	public void setSchedulePaymentAmount(double schedulePaymentAmount) {
		this.schedulePaymentAmount = schedulePaymentAmount;
	}

	public double getScheduleInterestAmount() {
		return scheduleInterestAmount;
	}

	public void setScheduleInterestAmount(double scheduleInterestAmount) {
		this.scheduleInterestAmount = scheduleInterestAmount;
	}

	public double getScheduleBalance() {
		return scheduleBalance;
	}

	public void setScheduleBalance(double scheduleBalance) {
		this.scheduleBalance = scheduleBalance;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}