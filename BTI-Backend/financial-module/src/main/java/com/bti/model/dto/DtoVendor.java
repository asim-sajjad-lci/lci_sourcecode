/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.MasterVendorStatus;
import com.bti.model.VendorMaintenance;
import com.bti.util.UtilRandomKey;

/**
 * Description: DtoVender
 * Name of Project: BTI
 * Created on: Aug 09, 2017
 * Modified on: Aug 09, 2017 02:35:38 AM
 * @author seasia
 * Version: 
 */
public class DtoVendor {

	private String venderId;
	private String venderNamePrimary;
	private String venderNameSecondary;
	private short venderStatus;
	private String venderStatusValue;
	private String shortName;
	private String classId;
	private String statementName;
	private String address1;
	private String address2;
	private String address3;
	private int country;
	private int state;
	private int city;
	private String phone1;
	private String phone2;
	private String phone3;
	private String fax;
	private String userDefine1;
	private String userDefine2;
	private int vendorHold;
	private String vendorHoldvalue;
	private String priority;
	
	public DtoVendor(){
		// TO DO Nothing
	}
	
	public DtoVendor(VendorMaintenance maintenance,MasterVendorStatus masterVendorStatus){
		this.venderId = maintenance.getVendorid();
		this.venderNamePrimary = maintenance.getVendorName();
		this.venderNameSecondary = maintenance.getVendorNameArabic();
		this.vendorHoldvalue="";
		this.vendorHold=maintenance.getVendorHoldStatus();
		this.venderStatusValue="";
		this.venderStatus=0;
		if(masterVendorStatus!=null){
			this.venderStatusValue = masterVendorStatus.getStatusPrimary();
			this.venderStatus = masterVendorStatus.getStatusId();
		}
		this.shortName = maintenance.getVendorShortName();
		if(maintenance.getVendorClassesSetup()!=null){
			this.classId = maintenance.getVendorClassesSetup().getVendorClassId();
		}else{
			this.classId = "";
		}
		
		this.statementName = maintenance.getVendorStatementName();
		this.address1 = maintenance.getAddress1();
		this.address2 = maintenance.getAddress2();
		this.address3 = maintenance.getAddress3();
		this.country = maintenance.getCounrty();
		this.state = maintenance.getState();
		this.city = maintenance.getCity();
		this.phone1 = maintenance.getPhoneNumber1();
		this.phone2 = maintenance.getPhoneNumber2();
		this.phone3 = maintenance.getPhoneNumber3();
		this.fax = maintenance.getFaxNumber();
		this.userDefine1 = maintenance.getUserDefine1();
		this.userDefine2 = maintenance.getUserDefine2();
		this.priority="1";
		if(UtilRandomKey.isNotBlank(maintenance.getVendorPriority())){
			this.priority=maintenance.getVendorPriority();
		}
	}
	
	public String getVenderId() {
		return venderId;
	}
	public void setVenderId(String venderId) {
		this.venderId = venderId;
	}
	public String getVenderNamePrimary() {
		return venderNamePrimary;
	}
	public void setVenderNamePrimary(String venderNamePrimary) {
		this.venderNamePrimary = venderNamePrimary;
	}
	public String getVenderNameSecondary() {
		return venderNameSecondary;
	}
	public void setVenderNameSecondary(String venderNameSecondary) {
		this.venderNameSecondary = venderNameSecondary;
	}
	public short getVenderStatus() {
		return venderStatus;
	}

	public void setVenderStatus(short venderStatus) {
		this.venderStatus = venderStatus;
	}

	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getStatementName() {
		return statementName;
	}
	public void setStatementName(String statementName) {
		this.statementName = statementName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public int getCountry() {
		return country;
	}
	public void setCountry(int country) {
		this.country = country;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getCity() {
		return city;
	}
	public void setCity(int city) {
		this.city = city;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getPhone3() {
		return phone3;
	}
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getUserDefine1() {
		return userDefine1;
	}
	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}
	public String getUserDefine2() {
		return userDefine2;
	}
	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getVenderStatusValue() {
		return venderStatusValue;
	}

	public void setVenderStatusValue(String venderStatusValue) {
		this.venderStatusValue = venderStatusValue;
	}

	public int getVendorHold() {
		return vendorHold;
	}

	public void setVendorHold(int vendorHold) {
		this.vendorHold = vendorHold;
	}

	public String getVendorHoldvalue() {
		return vendorHoldvalue;
	}

	public void setVendorHoldvalue(String vendorHoldvalue) {
		this.vendorHoldvalue = vendorHoldvalue;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
	

}
