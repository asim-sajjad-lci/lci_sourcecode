/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FAClassSetup;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoFAClassSetup {

	private String classId;
	private int classIndex;
	private String description;
	private String descriptionArabic;
	private Integer faInsuranceClassIndexId;
	private String faAccountGroupId;
	private String messageType;
	private int faAccountGroupIndex;
	
	public DtoFAClassSetup() {
		
	}
	
	public DtoFAClassSetup(FAClassSetup faClassSetup) 
	{
		this.classId=faClassSetup.getClassId();
		this.classIndex=faClassSetup.getClassIndx();
		this.description="";
		if(UtilRandomKey.isNotBlank(faClassSetup.getClassSetupDescription())){
			this.description=faClassSetup.getClassSetupDescription();
		}
		
		this.descriptionArabic="";
		if(UtilRandomKey.isNotBlank(faClassSetup.getClassSetupDescriptionArabic())){
			this.descriptionArabic=faClassSetup.getClassSetupDescriptionArabic();
		}
		faInsuranceClassIndexId=0;
		if(faClassSetup.getFaInsuranceClassSetup()!=null){
			this.faInsuranceClassIndexId=faClassSetup.getFaInsuranceClassSetup().getInsuranceClassIndex();
		}
		this.faAccountGroupId="";
		if(faClassSetup.getFaAccountGroupsSetup()!=null){
			this.faAccountGroupId=faClassSetup.getFaAccountGroupsSetup().getFaAccountGroupId();
		    this.faAccountGroupIndex=faClassSetup.getFaAccountGroupsSetup().getFaAccountGroupIndex();
		}
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public int getClassIndex() {
		return classIndex;
	}

	public void setClassIndex(int classIndex) {
		this.classIndex = classIndex;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionArabic() {
		return descriptionArabic;
	}

	public void setDescriptionArabic(String descriptionArabic) {
		this.descriptionArabic = descriptionArabic;
	}

	public String getFaAccountGroupId() {
		return faAccountGroupId;
	}

	public void setFaAccountGroupId(String faAccountGroupId) {
		this.faAccountGroupId = faAccountGroupId;
	}

	public Integer getFaInsuranceClassIndexId() {
		return faInsuranceClassIndexId;
	}

	public void setFaInsuranceClassIndexId(Integer faInsuranceClassIndexId) {
		this.faInsuranceClassIndexId = faInsuranceClassIndexId;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public int getFaAccountGroupIndex() {
		return faAccountGroupIndex;
	}

	public void setFaAccountGroupIndex(int faAccountGroupIndex) {
		this.faAccountGroupIndex = faAccountGroupIndex;
	}

	
}
