/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.MasterVendorStatus;

public class DtoVendorStatus {

	private int id;
	private String status;
	private int statusId;
	
	DtoVendorStatus(){
		
	}
	
	public DtoVendorStatus(MasterVendorStatus masterVendorStatus){
		this.id = masterVendorStatus.getId();
		this.statusId = masterVendorStatus.getStatusId();
			this.status = masterVendorStatus.getStatusPrimary();
		 
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	
	
}
