/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FAPhysicalLocationSetup;

public class DtoPhysicalLocationSetup {

	private String physicalLocationId;
	private String descriptionPrimary;
	private String descriptionSecondary;
	private int locationIndex;
	
	public DtoPhysicalLocationSetup(){
		
	}
	
	public DtoPhysicalLocationSetup(FAPhysicalLocationSetup faPhysicalLocationSetup){
		this.physicalLocationId = faPhysicalLocationSetup.getPhysicalLocationId();
		this.descriptionPrimary = faPhysicalLocationSetup.getPhysicalLocationDescription();
		this.descriptionSecondary = faPhysicalLocationSetup.getPhysicalLocationDescriptionArabic();
		this.locationIndex = faPhysicalLocationSetup.getFaLocationSetup().getLocationIndx();
	}
	
	
	public String getPhysicalLocationId() {
		return physicalLocationId;
	}

	public void setPhysicalLocationId(String physicalLocationId) {
		this.physicalLocationId = physicalLocationId;
	}

	public String getDescriptionPrimary() {
		return descriptionPrimary;
	}
	public void setDescriptionPrimary(String descriptionPrimary) {
		this.descriptionPrimary = descriptionPrimary;
	}
	public String getDescriptionSecondary() {
		return descriptionSecondary;
	}
	public void setDescriptionSecondary(String descriptionSecondary) {
		this.descriptionSecondary = descriptionSecondary;
	}

	public int getLocationIndex() {
		return locationIndex;
	}

	public void setLocationIndex(int locationIndex) {
		this.locationIndex = locationIndex;
	}
	
}
