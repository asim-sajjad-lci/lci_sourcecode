/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar10102 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar10102")
@NamedQuery(name="ARTransactionEntryCommissions.findAll", query="SELECT a FROM ARTransactionEntryCommissions a")
public class ARTransactionEntryCommissions implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COMSEQC")
	private int commissionSequence;
	 
	@Column(name="ARTRXNO")
	private String arTransactionNumber;
	
	@Column(name="COMMAPPL")
	private int arTransactionCommissionApplied;
	
	@Column(name="SALSPERID")
	private String salesmanID;
 
	@Column(name="PERSALE")
	private double percentOfSales;
	
	@Column(name="COMSLSAMT")
	private double commissionSalesAmount;
	
	@Column(name="COMAMT")
	private double commissionAmountOfSalesman;
	
	@Column(name="COMPER")
	private double commissionPercentOfSalesman;
	
	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	public int getCommissionSequence() {
		return commissionSequence;
	}

	public void setCommissionSequence(int commissionSequence) {
		this.commissionSequence = commissionSequence;
	}

	public String getArTransactionNumber() {
		return arTransactionNumber;
	}

	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}

	public int getArTransactionCommissionApplied() {
		return arTransactionCommissionApplied;
	}

	public void setArTransactionCommissionApplied(int arTransactionCommissionApplied) {
		this.arTransactionCommissionApplied = arTransactionCommissionApplied;
	}

	public String getSalesmanID() {
		return salesmanID;
	}

	public void setSalesmanID(String salesmanID) {
		this.salesmanID = salesmanID;
	}

	public double getPercentOfSales() {
		return percentOfSales;
	}

	public void setPercentOfSales(double percentOfSales) {
		this.percentOfSales = percentOfSales;
	}

	public double getCommissionSalesAmount() {
		return commissionSalesAmount;
	}

	public void setCommissionSalesAmount(double commissionSalesAmount) {
		this.commissionSalesAmount = commissionSalesAmount;
	}

	public double getCommissionAmountOfSalesman() {
		return commissionAmountOfSalesman;
	}

	public void setCommissionAmountOfSalesman(double commissionAmountOfSalesman) {
		this.commissionAmountOfSalesman = commissionAmountOfSalesman;
	}

	public double getCommissionPercentOfSalesman() {
		return commissionPercentOfSalesman;
	}

	public void setCommissionPercentOfSalesman(double commissionPercentOfSalesman) {
		this.commissionPercentOfSalesman = commissionPercentOfSalesman;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	 
	 
}