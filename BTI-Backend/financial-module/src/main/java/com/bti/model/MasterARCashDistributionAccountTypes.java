/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Master distribution account types database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "master_ar_cash_distribution_account_types")
@NamedQuery(name="MasterARCashDistributionAccountTypes.findAll", query="SELECT a FROM MasterARCashDistributionAccountTypes a")
public class MasterARCashDistributionAccountTypes implements Serializable {
	private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="ID")
		private int id;

		@Column(name="TYPE_DESCRIPTION")
		private String typeDescription;

		@Column(name="TYPE_CODE")
		private String typeCode;
		
		@Column(name="TYPE_ID")
		private int typeId;
		
		@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
		private boolean isDeleted;
		
		@ManyToOne
		@JoinColumn(name="lang_Id")
		private Language language;
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
		
		public String getTypeDescription() {
			return typeDescription;
		}

		public void setTypeDescription(String typeDescription) {
			this.typeDescription = typeDescription;
		}

		public String getTypeCode() {
			return typeCode;
		}

		public void setTypeCode(String typeCode) {
			this.typeCode = typeCode;
		}

		public Language getLanguage() {
			return language;
		}

		public void setLanguage(Language language) {
			this.language = language;
		}

		public boolean isDeleted() {
			return isDeleted;
		}

		public void setDeleted(boolean isDeleted) {
			this.isDeleted = isDeleted;
		}

		public int getTypeId() {
			return typeId;
		}

		public void setTypeId(int typeId) {
			this.typeId = typeId;
		}
		
}