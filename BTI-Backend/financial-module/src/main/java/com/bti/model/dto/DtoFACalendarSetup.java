/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.bti.model.FACalendarSetup;
import com.bti.model.FACalendarSetupMonthly;
import com.bti.model.FACalendarSetupQuarter;

/**
 * Description: The DtoFACalendarSetup class for FACalendarSetup 
 * Name of Project: BTI
 * Created on: May 19, 2017
 * Modified on: May 19, 2017 12:19:38 PM
 * @author seasia
 * Version: 
 */
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoFACalendarSetup {
	private int calendarIndex;
	private String calendarId;
	private String calendarDescription;
	private String calendarDescriptionArabic;
	private String messageType;
	private int year1;
	private List<DtoFACalendarSetupMonthly> monthlyCalendar = new ArrayList<>(); 
	private List<DtoFACalendarSetupQuarter> quarterCalendar = new ArrayList<>(); 
	
	public DtoFACalendarSetup() {
		
	}
	public DtoFACalendarSetup(FACalendarSetup faCalendarSetup) {

		this.calendarIndex = faCalendarSetup.getCalendarIndex();
		this.calendarDescription  =faCalendarSetup.getCalendarDescription();
		this.calendarDescriptionArabic = faCalendarSetup.getCalendarDescriptionArabic();
		this.calendarId = faCalendarSetup.getCalendarId();
		this.year1 = faCalendarSetup.getYear1();
		
		if(faCalendarSetup.getFaCalendarSetupMonthlies()!=null){
			for(FACalendarSetupMonthly  faCalendarSetupMonthly : faCalendarSetup.getFaCalendarSetupMonthlies()){
				monthlyCalendar.add(new DtoFACalendarSetupMonthly(faCalendarSetupMonthly));
			}
		
		}
		if(faCalendarSetup.getFaCalendarSetupQuarters()!=null){
			for(FACalendarSetupQuarter  faCalendarSetupQuarter : faCalendarSetup.getFaCalendarSetupQuarters()){
				quarterCalendar.add(new DtoFACalendarSetupQuarter(faCalendarSetupQuarter));
			}
		
		}
	}
	public int getCalendarIndex() {
		return calendarIndex;
	}
	public void setCalendarIndex(int calendarIndex) {
		this.calendarIndex = calendarIndex;
	}
	public String getCalendarId() {
		return calendarId;
	}
	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}
	public String getCalendarDescription() {
		return calendarDescription;
	}
	public void setCalendarDescription(String calendarDescription) {
		this.calendarDescription = calendarDescription;
	}
	public String getCalendarDescriptionArabic() {
		return calendarDescriptionArabic;
	}
	public void setCalendarDescriptionArabic(String calendarDescriptionArabic) {
		this.calendarDescriptionArabic = calendarDescriptionArabic;
	}
	public int getYear1() {
		return year1;
	}
	public void setYear1(int year1) {
		this.year1 = year1;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public List<DtoFACalendarSetupMonthly> getMonthlyCalendar() {
		return monthlyCalendar;
	}
	public void setMonthlyCalendar(List<DtoFACalendarSetupMonthly> monthlyCalendar) {
		this.monthlyCalendar = monthlyCalendar;
	}
	public List<DtoFACalendarSetupQuarter> getQuarterCalendar() {
		return quarterCalendar;
	}
	public void setQuarterCalendar(List<DtoFACalendarSetupQuarter> quarterCalendar) {
		this.quarterCalendar = quarterCalendar;
	}

}
