/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.deser.Deserializers.Base;


/**
 * The persistent class for the gl10101 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10101")
@NamedQuery(name="JournalEntryDetails.findAll", query="SELECT a FROM JournalEntryDetails  a")
public class JournalEntryDetails extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="JORSEQN")
	private int journalSequence;
	
	@ManyToOne
	@JoinColumn(name="JORNID")
	private JournalEntryHeader journalEntryHeader;
	
	@Column(name="ACTROWID")
	private String accountTableRowIndex;

	@Column(name="ACCTTYP")
	private int accountType;
	
	@Column(name="TPCLBLNC")
	private int balanceType;

	@Column(name="INTERID")
	private String intercompanyIDMultiCompanyTransaction;
	
	@Column(name="XCHGRATE")
	private Double exchangeTableRate;
	
	@Column(name="DEBTAMT")
	private Double debitAmount;
	
	@Column(name="CRDTAMT")
	private Double creditAmount;
	
	@Column(name="ORGDEBTAMT")
	private Double originalDebitAmount;

	@Column(name="ORGCRETAMT")
	private Double originalCreditAmount;

	@Column(name="DISDRCPTN")
	private String distributionDescription ;
	
	@Column(name="MODIFDT")
	private Date modifyDate ;

	@Column(name="CHANGEBY")
	private String modifyByUserID ;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex ;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex ;

	@Column(name="CREATDDT")
	private Date createDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private Boolean isDeleted;

	public int getJournalSequence() {
		return journalSequence;
	}

	public void setJournalSequence(int journalSequence) {
		this.journalSequence = journalSequence;
	}

	public JournalEntryHeader getJournalEntryHeader() {
		return journalEntryHeader;
	}

	public void setJournalEntryHeader(JournalEntryHeader journalEntryHeader) {
		this.journalEntryHeader = journalEntryHeader;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public String getIntercompanyIDMultiCompanyTransaction() {
		return intercompanyIDMultiCompanyTransaction;
	}

	public void setIntercompanyIDMultiCompanyTransaction(String intercompanyIDMultiCompanyTransaction) {
		this.intercompanyIDMultiCompanyTransaction = intercompanyIDMultiCompanyTransaction;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(Double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public Double getOriginalCreditAmount() {
		return originalCreditAmount;
	}

	public void setOriginalCreditAmount(Double originalCreditAmount) {
		this.originalCreditAmount = originalCreditAmount;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

		
	

}