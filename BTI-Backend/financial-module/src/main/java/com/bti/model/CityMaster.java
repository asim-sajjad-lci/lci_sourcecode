/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
* Description: The persistent class for the city_master database table.
* Name of Project: BTI
* Created on: June 20, 2017
* Modified on: June 20, 2017 11:19:38 AM
* @author seasia
* Version: 
*/ 
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "city_master")
@NamedQuery(name = "CityMaster.findAll", query = "SELECT c FROM CityMaster c")
public class CityMaster extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "city_id")
	private int cityId;

	@Column(name = "city_name")
	private String cityName;
	
	@Column(name = "city_code")
	private String cityCode;

	/*@Column(name = "city_name_secondary")
	private String cityNameSecondary;*/
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	// bi-directional many-to-one association to StateMaster
	@ManyToOne
	@JoinColumn(name = "state_id")
	private StateMaster stateMaster;
	
	//bi-directional many-to-one association to Fa40005
	@OneToMany(mappedBy="cityMaster")
	private List<FALocationSetup> fa40005s;
	
	@ManyToOne
	@JoinColumn(name="lang_Id")
	private Language language;


	public CityMaster() {
	}

	public int getCityId() {
		return this.cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public StateMaster getStateMaster() {
		return this.stateMaster;
	}

	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}

	public List<FALocationSetup> getFa40005s() {
		return fa40005s;
	}

	public void setFa40005s(List<FALocationSetup> fa40005s) {
		this.fa40005s = fa40005s;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
}