/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DtoGLReportHeader class having getter and setter for fields
 * (POJO) Name Name of Project: BTI Created on: JAN 10, 2018 Modified on: JAN
 * 10, 2018 4:19:38 PM
 * 
 * @author seasia Version:
 */

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoAccountStatementReport {

	private String printDate;
	private String printTime;

	private String startDate;
	private String endDate;
	
	private ArrayList<DtoAccountStatementMasterRecord> accountStatmentMasterRecordList;

	public String getPrintDate() {
		return printDate;
	}

	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}

	public String getPrintTime() {
		return printTime;
	}

	public void setPrintTime(String printTime) {
		this.printTime = printTime;
	}

	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public ArrayList<DtoAccountStatementMasterRecord> getAccountStatmentMasterRecordList() {
		return accountStatmentMasterRecordList;
	}

	public void setAccountStatmentMasterRecordList(
			ArrayList<DtoAccountStatementMasterRecord> accountStatmentMasterRecordList) {
		this.accountStatmentMasterRecordList = accountStatmentMasterRecordList;
	}
	
	

	 
}
