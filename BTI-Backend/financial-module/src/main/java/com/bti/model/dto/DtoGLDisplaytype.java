/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.MasterBlncdspl;

public class DtoGLDisplaytype {

	private int id;
	private String type;
	private int typeId;
	
	DtoGLDisplaytype(){
		
	}
	
	public DtoGLDisplaytype(MasterBlncdspl masterBlncdspl){
		this.id = masterBlncdspl.getId();
		this.typeId = masterBlncdspl.getTypeId();
			this.type = masterBlncdspl.getTypePrimary();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	
	
}
