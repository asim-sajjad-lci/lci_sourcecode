/**
 * BTI - BAAN for Technology And Trade IntegerL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.APManualPayment;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoAPManualPayment {

	private String manualPaymentNumber;
	private String batchID;
	private String vendorID;
	private String vendorName;
	private String vendorNameArabic;
	private String currencyID;
	private Integer manualPaymentType;
	private Double manualPaymentAmount;
	private String checkBookId;
	private String creditCardID;
	private String creditCardNumber;
	private String checkNumber;
	private Integer creditCardExpireMonth;
	private Integer creditCardExpireYear;
	private String manualPaymentDescription;
	private String manualPaymentCreateDate;
	private String manualPaymentPostingDate;
	private int transactionVoid;
	private String exchangeTableIndex;
	private Double exchangeTableRate;
	private String messageType;
	private String buttonType;

	public DtoAPManualPayment() {
	}

	public DtoAPManualPayment(APManualPayment apManualPayment) {
		this.manualPaymentNumber = apManualPayment.getManualPaymentNumber();
		this.batchID = apManualPayment.getApBatches() != null ? apManualPayment.getApBatches().getBatchID() : "";
		this.vendorID = apManualPayment.getVendorID();
		this.vendorName = apManualPayment.getVendorName();
		this.vendorNameArabic = apManualPayment.getVendorNameArabic();
		this.currencyID = apManualPayment.getCurrencyID();
		this.manualPaymentType = apManualPayment.getMasterAPManualPaymentType() != null
				? apManualPayment.getMasterAPManualPaymentType().getTypeId() : null;
		this.manualPaymentAmount = UtilRoundDecimal.roundDecimalValue(apManualPayment.getManualPaymentAmount());
		this.checkBookId = apManualPayment.getCheckbookID()!=null?apManualPayment.getCheckbookID():"";
		this.creditCardID = "";
		this.creditCardNumber = "";
		this.creditCardExpireYear = 0;
		this.creditCardExpireMonth = 0;
		if(UtilRandomKey.isNotBlank(apManualPayment.getCreditCardID()))
		{
			this.creditCardID = apManualPayment.getCreditCardID();
			this.creditCardNumber = apManualPayment.getCreditCardNumber();
			this.creditCardExpireYear = apManualPayment.getCreditCardExpireYear();
			this.creditCardExpireMonth = apManualPayment.getCreditCardExpireMonth();
		}
		
		this.checkNumber = apManualPayment.getCheckNumber()!=null?apManualPayment.getCheckNumber():"";
		this.manualPaymentDescription = apManualPayment.getManualPaymentDescription();
		this.manualPaymentCreateDate = apManualPayment.getManualPaymentCreateDate() != null
				? UtilDateAndTime.dateToStringddmmyyyy(apManualPayment.getManualPaymentCreateDate()) : "";
		this.manualPaymentPostingDate = apManualPayment.getManualPaymentPostingDate() != null
				? UtilDateAndTime.dateToStringddmmyyyy(apManualPayment.getManualPaymentPostingDate()) : "";
		this.transactionVoid = apManualPayment.getTransactionVoid();
		this.exchangeTableIndex = apManualPayment.getExchangeTableIndex()!=null?apManualPayment.getExchangeTableIndex():"";
		this.exchangeTableRate = apManualPayment.getExchangeTableRate();

	}

	public String getManualPaymentNumber() {
		return manualPaymentNumber;
	}

	public void setManualPaymentNumber(String manualPaymentNumber) {
		this.manualPaymentNumber = manualPaymentNumber;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorNameArabic() {
		return vendorNameArabic;
	}

	public void setVendorNameArabic(String vendorNameArabic) {
		this.vendorNameArabic = vendorNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public Integer getManualPaymentType() {
		return manualPaymentType;
	}

	public void setManualPaymentType(Integer manualPaymentType) {
		this.manualPaymentType = manualPaymentType;
	}

	public Double getManualPaymentAmount() {
		return manualPaymentAmount;
	}

	public void setManualPaymentAmount(Double manualPaymentAmount) {
		this.manualPaymentAmount =UtilRoundDecimal.roundDecimalValue(manualPaymentAmount);
	}

	public String getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Integer getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(Integer creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public Integer getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(Integer creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getManualPaymentDescription() {
		return manualPaymentDescription;
	}

	public void setManualPaymentDescription(String manualPaymentDescription) {
		this.manualPaymentDescription = manualPaymentDescription;
	}

	public String getManualPaymentCreateDate() {
		return manualPaymentCreateDate;
	}

	public void setManualPaymentCreateDate(String manualPaymentCreateDate) {
		this.manualPaymentCreateDate = manualPaymentCreateDate;
	}

	public String getManualPaymentPostingDate() {
		return manualPaymentPostingDate;
	}

	public void setManualPaymentPostingDate(String manualPaymentPostingDate) {
		this.manualPaymentPostingDate = manualPaymentPostingDate;
	}

	public int getTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(int transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = UtilRoundDecimal.roundDecimalValue(exchangeTableRate);
	}

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}
	

}
