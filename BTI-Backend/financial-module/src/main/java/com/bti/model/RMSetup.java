/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ar40002 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar40002")
@NamedQuery(name="RMSetup.findAll", query="SELECT a FROM RMSetup a")
public class RMSetup implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="AGEBY")
	MasterRMAgeingTypes masterRMAgeingTypes;
	
	@ManyToOne
	@JoinColumn(name="APLBYDEF")
	MasterRMApplyByDefaultTypes masterRMApplyByDefaultTypes;

	@Column(name="ARPYCOMSI")
	private Boolean arPayCommissionsInvoicePay;

	@Column(name="ARTRCDISC")
	private Boolean arTrackingDiscountAvailable;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="COMFNCHAG")
	private Boolean compoundFinanceCharge;

	@Column(name="CRDLMTPSSD")
	private String creditLimitPassword;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="CSTHLDPSSD")
	private String customerHoldPassword;

	@Column(name="DEX_ROW_ID")
	private int dexRowId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DTLUNPSTPRT")
	private Boolean deleteUnpostedPrintedDocuments;
	
	@Column(name="DTLUNPSTPRTDTD")
	private Date deleteUnpostedPrintedDocumentsDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	@OneToOne
	@JoinColumn(name="FRTSCHID")
	private VATSetup freightVatScheduleId;

	@Column(name="LSTBLNFOWAGE")
	private Date lastDateBalanceForwardAge;

	@Column(name="LSTFNCHRDTD")
	private Date lastFinanceChargeDate;

	@Column(name="LSTSTMPRNTDTD")
	private Date lastDateStatementPrinted;

	@OneToOne
	@JoinColumn(name="MISSCHID")
	private VATSetup miscVatScheduleId;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PRCLEVEL")
	private String priceLevel;

	@Column(name="PRTHTTRBLA")
	private Boolean printHistoricalAgedTrialBalance;

	@OneToOne
	@JoinColumn(name="SALSCHID")
	private VATSetup salesVatScheduleId;

	@Column(name="USRDFPR1")
	private String userDefine1;

	@Column(name="USRDFPR2")
	private String userDefine2;

	@Column(name="USRDFPR3")
	private String userDefine3;

	@Column(name="WAVFINPSSD")
	private String waivedFinanceChargePassword;

	@Column(name="WRTOFPSSD")
	private String writeoffPassword;

	//bi-directional many-to-one association to Gl00200
	@ManyToOne
	@JoinColumn(name="CHEKBOKID")
	private CheckbookMaintenance checkbookMaintenance;

	public RMSetup() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getArPayCommissionsInvoicePay() {
		return arPayCommissionsInvoicePay;
	}

	public void setArPayCommissionsInvoicePay(Boolean arPayCommissionsInvoicePay) {
		this.arPayCommissionsInvoicePay = arPayCommissionsInvoicePay;
	}

	public Boolean getArTrackingDiscountAvailable() {
		return arTrackingDiscountAvailable;
	}

	public void setArTrackingDiscountAvailable(Boolean arTrackingDiscountAvailable) {
		this.arTrackingDiscountAvailable = arTrackingDiscountAvailable;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public Boolean getCompoundFinanceCharge() {
		return compoundFinanceCharge;
	}

	public void setCompoundFinanceCharge(Boolean compoundFinanceCharge) {
		this.compoundFinanceCharge = compoundFinanceCharge;
	}

	public String getCreditLimitPassword() {
		return creditLimitPassword;
	}

	public void setCreditLimitPassword(String creditLimitPassword) {
		this.creditLimitPassword = creditLimitPassword;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerHoldPassword() {
		return customerHoldPassword;
	}

	public void setCustomerHoldPassword(String customerHoldPassword) {
		this.customerHoldPassword = customerHoldPassword;
	}

	public int getDexRowId() {
		return dexRowId;
	}

	public void setDexRowId(int dexRowId) {
		this.dexRowId = dexRowId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Boolean getDeleteUnpostedPrintedDocuments() {
		return deleteUnpostedPrintedDocuments;
	}

	public void setDeleteUnpostedPrintedDocuments(Boolean deleteUnpostedPrintedDocuments) {
		this.deleteUnpostedPrintedDocuments = deleteUnpostedPrintedDocuments;
	}

	public Date getLastDateBalanceForwardAge() {
		return lastDateBalanceForwardAge;
	}

	public void setLastDateBalanceForwardAge(Date lastDateBalanceForwardAge) {
		this.lastDateBalanceForwardAge = lastDateBalanceForwardAge;
	}

	public Date getLastFinanceChargeDate() {
		return lastFinanceChargeDate;
	}

	public void setLastFinanceChargeDate(Date lastFinanceChargeDate) {
		this.lastFinanceChargeDate = lastFinanceChargeDate;
	}

	public Date getLastDateStatementPrinted() {
		return lastDateStatementPrinted;
	}

	public void setLastDateStatementPrinted(Date lastDateStatementPrinted) {
		this.lastDateStatementPrinted = lastDateStatementPrinted;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getPriceLevel() {
		return priceLevel;
	}

	public void setPriceLevel(String priceLevel) {
		this.priceLevel = priceLevel;
	}

	public Boolean getPrintHistoricalAgedTrialBalance() {
		return printHistoricalAgedTrialBalance;
	}

	public void setPrintHistoricalAgedTrialBalance(Boolean printHistoricalAgedTrialBalance) {
		this.printHistoricalAgedTrialBalance = printHistoricalAgedTrialBalance;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public String getWaivedFinanceChargePassword() {
		return waivedFinanceChargePassword;
	}

	public void setWaivedFinanceChargePassword(String waivedFinanceChargePassword) {
		this.waivedFinanceChargePassword = waivedFinanceChargePassword;
	}

	public String getWriteoffPassword() {
		return writeoffPassword;
	}

	public void setWriteoffPassword(String writeoffPassword) {
		this.writeoffPassword = writeoffPassword;
	}

	public CheckbookMaintenance getCheckbookMaintenance() {
		return checkbookMaintenance;
	}

	public void setCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookMaintenance = checkbookMaintenance;
	}

	public Date getDeleteUnpostedPrintedDocumentsDate() {
		return deleteUnpostedPrintedDocumentsDate;
	}

	public void setDeleteUnpostedPrintedDocumentsDate(Date deleteUnpostedPrintedDocumentsDate) {
		this.deleteUnpostedPrintedDocumentsDate = deleteUnpostedPrintedDocumentsDate;
	}

	public MasterRMAgeingTypes getMasterRMAgeingTypes() {
		return masterRMAgeingTypes;
	}

	public void setMasterRMAgeingTypes(MasterRMAgeingTypes masterRMAgeingTypes) {
		this.masterRMAgeingTypes = masterRMAgeingTypes;
	}

	public MasterRMApplyByDefaultTypes getMasterRMApplyByDefaultTypes() {
		return masterRMApplyByDefaultTypes;
	}

	public void setMasterRMApplyByDefaultTypes(MasterRMApplyByDefaultTypes masterRMApplyByDefaultTypes) {
		this.masterRMApplyByDefaultTypes = masterRMApplyByDefaultTypes;
	}

	public VATSetup getFreightVatScheduleId() {
		return freightVatScheduleId;
	}

	public void setFreightVatScheduleId(VATSetup freightVatScheduleId) {
		this.freightVatScheduleId = freightVatScheduleId;
	}

	public VATSetup getMiscVatScheduleId() {
		return miscVatScheduleId;
	}

	public void setMiscVatScheduleId(VATSetup miscVatScheduleId) {
		this.miscVatScheduleId = miscVatScheduleId;
	}

	public VATSetup getSalesVatScheduleId() {
		return salesVatScheduleId;
	}

	public void setSalesVatScheduleId(VATSetup salesVatScheduleId) {
		this.salesVatScheduleId = salesVatScheduleId;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
}