/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the fa40001 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40001")
@NamedQuery(name="FACalendarSetup.findAll", query="SELECT f FROM FACalendarSetup f")
public class FACalendarSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CALENDINDX")
	private int calendarIndex;

	@Column(name="CALENDID")
	private String calendarId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRPTN")
	private String calendarDescription;

	@Column(name="DSCRPTNA")
	private String calendarDescriptionArabic;
	
	@Column(name="DATA_TYPE")
	private String dataType;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="YEAR1")
	private int year1;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Fa40000
	@OneToMany(mappedBy="faCalendarSetup")
	private List<BookSetup> bookSetups;

	//bi-directional one-to-one association to Fa40011
	/*@OneToOne
	@JoinColumn(name="CALENDINDX")
	private FACalendarSetupMonthly faCalendarSetupMonthly;*/
	
	@OneToMany(mappedBy="faCalendarSetup")
	private List<FACalendarSetupMonthly> faCalendarSetupMonthlies;
	@OneToMany(mappedBy="faCalendarSetup")
	private List<FACalendarSetupQuarter> faCalendarSetupQuarters;

	//bi-directional one-to-one association to Fa40012
	/*@OneToOne
	@JoinColumn(name="CALENDINDX")
	private FACalendarSetupQuarter faCalendarSetupQuarter;*/

	public FACalendarSetup() {
	}

	public int getCalendarIndex() {
		return calendarIndex;
	}

	public void setCalendarIndex(int calendarIndex) {
		this.calendarIndex = calendarIndex;
	}

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getCalendarDescription() {
		return calendarDescription;
	}

	public void setCalendarDescription(String calendarDescription) {
		this.calendarDescription = calendarDescription;
	}

	public String getCalendarDescriptionArabic() {
		return calendarDescriptionArabic;
	}

	public void setCalendarDescriptionArabic(String calendarDescriptionArabic) {
		this.calendarDescriptionArabic = calendarDescriptionArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getYear1() {
		return year1;
	}

	public void setYear1(int year1) {
		this.year1 = year1;
	}

	public List<BookSetup> getBookSetups() {
		return bookSetups;
	}

	public void setBookSetups(List<BookSetup> bookSetups) {
		this.bookSetups = bookSetups;
	}

	public List<FACalendarSetupMonthly> getFaCalendarSetupMonthlies() {
		return faCalendarSetupMonthlies;
	}

	public void setFaCalendarSetupMonthlies(List<FACalendarSetupMonthly> faCalendarSetupMonthlies) {
		this.faCalendarSetupMonthlies = faCalendarSetupMonthlies;
	}

	public List<FACalendarSetupQuarter> getFaCalendarSetupQuarters() {
		return faCalendarSetupQuarters;
	}

	public void setFaCalendarSetupQuarters(List<FACalendarSetupQuarter> faCalendarSetupQuarters) {
		this.faCalendarSetupQuarters = faCalendarSetupQuarters;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	

	/*public FACalendarSetupMonthly getFaCalendarSetupMonthly() {
		return faCalendarSetupMonthly;
	}

	public void setFaCalendarSetupMonthly(FACalendarSetupMonthly faCalendarSetupMonthly) {
		this.faCalendarSetupMonthly = faCalendarSetupMonthly;
	}

	public FACalendarSetupQuarter getFaCalendarSetupQuarter() {
		return faCalendarSetupQuarter;
	}

	public void setFaCalendarSetupQuarter(FACalendarSetupQuarter faCalendarSetupQuarter) {
		this.faCalendarSetupQuarter = faCalendarSetupQuarter;
	}*/

	
	
}