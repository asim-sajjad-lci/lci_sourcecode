/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
* Description: The persistent class for the Language database table.
* Name of Project: BTI
* Created on: June 20, 2017
* Modified on: June 20, 2017 11:19:38 AM
* @author seasia
* Version: 
*/ 
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "language")
@NamedQuery(name = "Language.findAll", query = "SELECT l FROM Language l")
public class Language extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "lang_id")
	private int languageId;

	@Column(name = "lang_name")
	private String languageName;

	@Column(name = "lang_orientation")
	private String languageOrientation;
	
	@Column(name = "is_active")
	private Boolean isActive;

	public Language() {
	}


	public int getLanguageId() {
		return languageId;
	}


	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}


	public String getLanguageName() {
		return languageName;
	}


	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}


	public String getLanguageOrientation() {
		return languageOrientation;
	}


	public void setLanguageOrientation(String languageOrientation) {
		this.languageOrientation = languageOrientation;
	}


	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
    
}