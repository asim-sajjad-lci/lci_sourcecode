/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.COAMainAccounts;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoMainAccountSetUp {

	private String mainAccountNumber;
	private Long actIndx;
	private String mainAccountDescription;
	private String mainAccountDescriptionArabic;
	private int accountTypeId;
	private String accountTypeName;
	private String accountTypeNameArabic;
	private String accountCategoryDescription;
	private String accountCategoryDescriptionArabic;
	private int accountCategoryId;
	private Integer tpclblnc;
	private String aliasAccount;
	private String aliasAccountArabic;

	private Boolean allowAccountTransactionEntry;
	private Boolean active;
	private String userDef1;
	private String userDef2;
	private String userDef3;
	private String userDef4;
	private String userDef5;
	private String messageType;
	
	public DtoMainAccountSetUp() {
		
	}

	public DtoMainAccountSetUp(COAMainAccounts mainAccount) {
		if(mainAccount.getGlConfigurationAccountCategories()!=null){
			this.setAccountCategoryDescription(mainAccount.getGlConfigurationAccountCategories().getAccountCategoryDescription());
			this.setAccountCategoryDescriptionArabic(mainAccount.getGlConfigurationAccountCategories().getAccountCategoryDescriptionArabic());
			this.accountCategoryId = mainAccount.getGlConfigurationAccountCategories().getAccountCategoryId();
		}
		if(mainAccount.getMainAccountTypes()!=null){
			this.accountTypeId = mainAccount.getMainAccountTypes().getAccountTypeId();
			this.accountTypeName = mainAccount.getMainAccountTypes().getAccountTypeName();
			this.accountTypeNameArabic = mainAccount.getMainAccountTypes().getAccountTypeNameArabic();
		}
		this.mainAccountNumber = mainAccount.getMainAccountNumber();
		this.actIndx = mainAccount.getActIndx();
		this.mainAccountDescription = mainAccount.getMainAccountDescription();
		this.mainAccountDescriptionArabic = mainAccount.getMainAccountDescriptionArabic();
		this.tpclblnc = mainAccount.getTpclBalanceType()!=null?mainAccount.getTpclBalanceType().getTypeId():null;
		this.aliasAccount = mainAccount.getAliasAccount();
		this.aliasAccountArabic = mainAccount.getAliasAccountArabic();
		this.allowAccountTransactionEntry = mainAccount.getAllowAccountTransactionEntry();
		this.active = mainAccount.getActive();
		this.userDef1 = mainAccount.getUserDef1();
		this.userDef2 = mainAccount.getUserDef2();
		this.userDef3 = mainAccount.getUserDef3();
		this.userDef4 = mainAccount.getUserDef4();
		this.userDef5 = mainAccount.getUserDef5();
	}

	public String getMainAccountNumber() {
		return mainAccountNumber;
	}

	public void setMainAccountNumber(String mainAccountNumber) {
		this.mainAccountNumber = mainAccountNumber;
	}

	public Long getActIndx() {
		return actIndx;
	}

	public void setActIndx(Long actIndx) {
		this.actIndx = actIndx;
	}

	public String getMainAccountDescription() {
		return mainAccountDescription;
	}

	public void setMainAccountDescription(String mainAccountDescription) {
		this.mainAccountDescription = mainAccountDescription;
	}

	public String getMainAccountDescriptionArabic() {
		return mainAccountDescriptionArabic;
	}

	public void setMainAccountDescriptionArabic(String mainAccountDescriptionArabic) {
		this.mainAccountDescriptionArabic = mainAccountDescriptionArabic;
	}

	public int getAccountTypeId() {
		return accountTypeId;
	}

	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getAccountTypeName() {
		return accountTypeName;
	}

	public void setAccountTypeName(String accountTypeName) {
		this.accountTypeName = accountTypeName;
	}

	public String getAccountTypeNameArabic() {
		return accountTypeNameArabic;
	}

	public void setAccountTypeNameArabic(String accountTypeNameArabic) {
		this.accountTypeNameArabic = accountTypeNameArabic;
	}

	public Integer getTpclblnc() {
		return tpclblnc;
	}

	public void setTpclblnc(Integer tpclblnc) {
		this.tpclblnc = tpclblnc;
	}

	public String getAliasAccount() {
		return aliasAccount;
	}

	public void setAliasAccount(String aliasAccount) {
		this.aliasAccount = aliasAccount;
	}

	public String getAliasAccountArabic() {
		return aliasAccountArabic;
	}

	public void setAliasAccountArabic(String aliasAccountArabic) {
		this.aliasAccountArabic = aliasAccountArabic;
	}

	public Boolean getAllowAccountTransactionEntry() {
		return allowAccountTransactionEntry;
	}

	public void setAllowAccountTransactionEntry(Boolean allowAccountTransactionEntry) {
		this.allowAccountTransactionEntry = allowAccountTransactionEntry;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getUserDef1() {
		return userDef1;
	}

	public void setUserDef1(String userDef1) {
		this.userDef1 = userDef1;
	}

	public String getUserDef2() {
		return userDef2;
	}

	public void setUserDef2(String userDef2) {
		this.userDef2 = userDef2;
	}

	public String getUserDef3() {
		return userDef3;
	}

	public void setUserDef3(String userDef3) {
		this.userDef3 = userDef3;
	}

	public String getUserDef4() {
		return userDef4;
	}

	public void setUserDef4(String userDef4) {
		this.userDef4 = userDef4;
	}

	public String getUserDef5() {
		return userDef5;
	}

	public void setUserDef5(String userDef5) {
		this.userDef5 = userDef5;
	}

	public int getAccountCategoryId() {
		return accountCategoryId;
	}

	public void setAccountCategoryId(int accountCategoryId) {
		this.accountCategoryId = accountCategoryId;
	}

	public String getAccountCategoryDescriptionArabic() {
		return accountCategoryDescriptionArabic;
	}

	public void setAccountCategoryDescriptionArabic(String accountCategoryDescriptionArabic) {
		this.accountCategoryDescriptionArabic = accountCategoryDescriptionArabic;
	}

	public String getAccountCategoryDescription() {
		return accountCategoryDescription;
	}

	public void setAccountCategoryDescription(String accountCategoryDescription) {
		this.accountCategoryDescription = accountCategoryDescription;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

}
