package com.bti.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;


@NamedStoredProcedureQueries(
		{
			@NamedStoredProcedureQuery(name="rptPL2",procedureName="rptPL2",
					parameters= {
							@StoredProcedureParameter(mode=ParameterMode.IN,name="periodPARAM",type=Integer.class),
							@StoredProcedureParameter(mode=ParameterMode.IN,name="yearParam",type=Integer.class)
							
			}
					)
		}
)
@Entity
public class RPTPL {

	@Id
	@Column(name="IDROWN")
	private long id;
	
	@Column(name="IDROW")
	private int IDROW;
	
	@Column(name="ACTROWID")
	private int accountTableRowIndex;
	
	@Column(name="mainaccount")
	private String MainAccount;
	
	@Column(name="mainaccountdescription")
	private String MainAccountDescription;
	
	@Column(name="accountcategoryindex")
	private int AccountCategoryIndex;
	
	@Column(name="accountcategorydescription")
	private String AccountCategoryDecription;
	
	@Column(name="trxdate")
	private Date TransactionDate;
	
	@Column(name="period")
	private int period;
	
	@Column(name="debitamount")
	private Double debitAmmount;
	
	@Column(name="creditamount")
	private Double creditAmmount;

	
	
	
	public RPTPL(long id, int iDROW, int accountTableRowIndex, String mainAccount, String mainAccountDescription,
			int accountCategoryIndex, String accountCategoryDecription,  int period,
			Double debitAmmount, Double creditAmmount) {
		super();
		this.id = id;
		IDROW = iDROW;
		this.accountTableRowIndex = accountTableRowIndex;
		MainAccount = mainAccount;
		MainAccountDescription = mainAccountDescription;
		AccountCategoryIndex = accountCategoryIndex;
		AccountCategoryDecription = accountCategoryDecription;
		
		this.period = period;
		this.debitAmmount = debitAmmount;
		this.creditAmmount = creditAmmount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getIDROW() {
		return IDROW;
	}

	public void setIDROW(int iDROW) {
		IDROW = iDROW;
	}

	public int getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(int accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public String getMainAccount() {
		return MainAccount;
	}

	public void setMainAccount(String mainAccount) {
		MainAccount = mainAccount;
	}

	public String getMainAccountDescription() {
		return MainAccountDescription;
	}

	public void setMainAccountDescription(String mainAccountDescription) {
		MainAccountDescription = mainAccountDescription;
	}

	public int getAccountCategoryIndex() {
		return AccountCategoryIndex;
	}

	public void setAccountCategoryIndex(int accountCategoryIndex) {
		AccountCategoryIndex = accountCategoryIndex;
	}

	public String getAccountCategoryDecription() {
		return AccountCategoryDecription;
	}

	public void setAccountCategoryDecription(String accountCategoryDecription) {
		AccountCategoryDecription = accountCategoryDecription;
	}

	public Date getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		TransactionDate = transactionDate;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public Double getDebitAmmount() {
		return debitAmmount;
	}

	public void setDebitAmmount(Double debitAmmount) {
		this.debitAmmount = debitAmmount;
	}

	public Double getCreditAmmount() {
		return creditAmmount;
	}

	public void setCreditAmmount(Double creditAmmount) {
		this.creditAmmount = creditAmmount;
	}
	
	
	
	
	
}
