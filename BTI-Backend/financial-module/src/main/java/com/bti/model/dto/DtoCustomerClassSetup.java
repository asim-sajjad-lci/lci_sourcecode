/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.CustomerClassesSetup;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoCustomerClassSetup {

	private String customerClassId;

	private Integer balanceType;

	private String customerClassDescription;

	private String customeClassDescriptionArabic;

	private Integer creditLimit;

	private String currencyId;

	private Integer customerPriority;

	private Integer financeCharge;

	private Integer minimumCharge;

	private Integer openMaintenanceHistoryCalendarYear;

	private Integer openMaintenanceHistoryDistribution;

	private Integer openMaintenanceHistoryFiscalYear;

	private Integer openMaintenanceHistoryTransaction;

	private String priceLevelId;

	private String userDefine1;

	private String userDefine2;

	private String userDefine3;

	private String vatId;

	private String shipmentMethodId;

	private String paymentTermId;

	private String salesmanId;

	private String salesTerritoryId;

	private String checkbookId;
	private String financeChargeAmount;

	private String minimumChargeAmount;

	private String tradeDiscountPercent;

	private String creditLimitAmount;

	private String name;
	
	private String customerId;
	
	private String territoryDescription;
	
	private String salesmanName;

	private String messageType;

	private String miscVatScheduleId;
	
    private String freightVatScheduleId;
    
    private String miscVatPercent;
    private String freightVatPercent;
    private String vatPercent;
   

	public DtoCustomerClassSetup(CustomerClassesSetup customerClassesSetup) {
		this.customerClassId = customerClassesSetup.getCustomerClassId();
		this.balanceType = 0;
		if (customerClassesSetup.getMasterCustomerClassSetupBalanceType() != null) {
			this.balanceType = customerClassesSetup.getMasterCustomerClassSetupBalanceType().getTypeId();
		}
		this.checkbookId = "";
		if (customerClassesSetup.getCheckbookMaintenance() != null) {
			this.checkbookId = customerClassesSetup.getCheckbookMaintenance().getCheckBookId();
		}
		this.creditLimit = 0;
		if (customerClassesSetup.getMasterCustomerClassSetupCreditLimit() != null) {
			this.creditLimit = customerClassesSetup.getMasterCustomerClassSetupCreditLimit().getTypeId();
		}

		this.creditLimitAmount = "";
		if (customerClassesSetup.getCreditLimitAmount() != null) {
			this.creditLimitAmount = String.valueOf(UtilRoundDecimal.roundDecimalValue(customerClassesSetup.getCreditLimitAmount()));
		}

		this.currencyId = "";
		if (customerClassesSetup.getCurrencySetup() != null) {
			this.currencyId = customerClassesSetup.getCurrencySetup().getCurrencyId();
		}
		this.customeClassDescriptionArabic = "";
		if (UtilRandomKey.isNotBlank(customerClassesSetup.getCustomeClassDescriptionArabic())) {
			this.customeClassDescriptionArabic = customerClassesSetup.getCustomeClassDescriptionArabic();
		}
		this.customerClassDescription = "";
		if (UtilRandomKey.isNotBlank(customerClassesSetup.getCustomerClassDescription())) {
			this.customerClassDescription = customerClassesSetup.getCustomerClassDescription();
		}
		this.customerPriority = customerClassesSetup.getCustomerPriority();
		this.financeCharge = 0;
		if (customerClassesSetup.getMasterCustomerClassSetupFinanaceCharge() != null) {
			this.financeCharge = customerClassesSetup.getMasterCustomerClassSetupFinanaceCharge().getTypeId();
		}

		this.financeChargeAmount = "";
		if (customerClassesSetup.getFinanceChargeAmount() != null) {
			this.financeChargeAmount = String.valueOf(UtilRoundDecimal.roundDecimalValue(customerClassesSetup.getFinanceChargeAmount()));
		}
		this.minimumCharge = 0;
		if (customerClassesSetup.getMasterCustomerClassMinimumCharge() != null) {
			this.minimumCharge = customerClassesSetup.getMasterCustomerClassMinimumCharge().getTypeId();
		}

		this.minimumChargeAmount = "";
		if (customerClassesSetup.getMinimumChargeAmount() != null) {
			this.minimumChargeAmount = String.valueOf(UtilRoundDecimal.roundDecimalValue(customerClassesSetup.getMinimumChargeAmount()));
		}

		this.openMaintenanceHistoryCalendarYear = customerClassesSetup.getOpenMaintenanceHistoryCalendarYear();
		this.openMaintenanceHistoryDistribution = customerClassesSetup.getOpenMaintenanceHistoryDistribution();
		this.openMaintenanceHistoryFiscalYear = customerClassesSetup.getOpenMaintenanceHistoryFiscalYear();
		this.openMaintenanceHistoryTransaction = customerClassesSetup.getOpenMaintenanceHistoryTransaction();
		this.paymentTermId = "";
		if (customerClassesSetup.getPaymentTermsSetup() != null) {
			this.paymentTermId = customerClassesSetup.getPaymentTermsSetup().getPaymentTermId();
		}
		this.priceLevelId = "";
		if (UtilRandomKey.isNotBlank(customerClassesSetup.getPriceLevelId())) {
			this.priceLevelId = customerClassesSetup.getPriceLevelId();
		}
		this.salesmanId = "";
		if (customerClassesSetup.getSalesmanMaintenance() != null) {
			this.salesmanId = customerClassesSetup.getSalesmanMaintenance().getSalesmanId();
		}
		this.salesTerritoryId = "";
		if (customerClassesSetup.getSalesTerritoryMaintenance() != null) {
			this.salesTerritoryId = customerClassesSetup.getSalesTerritoryMaintenance().getSalesTerritoryId();
		}
		this.shipmentMethodId = "";
		if (customerClassesSetup.getShipmentMethodSetup() != null) {
			this.shipmentMethodId = customerClassesSetup.getShipmentMethodSetup().getShipmentMethodId();
		}
		this.tradeDiscountPercent = "";
		if (customerClassesSetup.getTradeDiscountPercent() != null) {
			this.tradeDiscountPercent = String.valueOf(UtilRoundDecimal.roundDecimalValue(customerClassesSetup.getTradeDiscountPercent()));
		}
		this.userDefine1 = "";
		this.userDefine2 = "";
		this.userDefine3 = "";
		if (UtilRandomKey.isNotBlank(customerClassesSetup.getUserDefine1())) {
			this.userDefine1 = customerClassesSetup.getUserDefine1();
		}
		if (UtilRandomKey.isNotBlank(customerClassesSetup.getUserDefine2())) {
			this.userDefine2 = customerClassesSetup.getUserDefine2();
		}
		if (UtilRandomKey.isNotBlank(customerClassesSetup.getUserDefine3())) {
			this.userDefine3 = customerClassesSetup.getUserDefine3();
		}
		this.vatId = "";
		if (customerClassesSetup.getVatSetup() != null) {
			this.vatId = customerClassesSetup.getVatSetup().getVatScheduleId();
		}
	}

	public DtoCustomerClassSetup() {

	}

	public String getCustomerClassId() {
		return customerClassId;
	}

	public void setCustomerClassId(String customerClassId) {
		this.customerClassId = customerClassId;
	}

	public Integer getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(Integer balanceType) {
		this.balanceType = balanceType;
	}

	public String getCustomerClassDescription() {
		return customerClassDescription;
	}

	public void setCustomerClassDescription(String customerClassDescription) {
		this.customerClassDescription = customerClassDescription;
	}

	public String getCustomeClassDescriptionArabic() {
		return customeClassDescriptionArabic;
	}

	public void setCustomeClassDescriptionArabic(String customeClassDescriptionArabic) {
		this.customeClassDescriptionArabic = customeClassDescriptionArabic;
	}

	public Integer getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Integer creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCreditLimitAmount() {
		return creditLimitAmount;
	}

	public void setCreditLimitAmount(String creditLimitAmount) {
		this.creditLimitAmount = creditLimitAmount;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public Integer getCustomerPriority() {
		return customerPriority;
	}

	public void setCustomerPriority(Integer customerPriority) {
		this.customerPriority = customerPriority;
	}

	public Integer getFinanceCharge() {
		return financeCharge;
	}

	public void setFinanceCharge(Integer financeCharge) {
		this.financeCharge = financeCharge;
	}

	public Integer getMinimumCharge() {
		return minimumCharge;
	}

	public void setMinimumCharge(Integer minimumCharge) {
		this.minimumCharge = minimumCharge;
	}

	public Integer getOpenMaintenanceHistoryCalendarYear() {
		return openMaintenanceHistoryCalendarYear;
	}

	public void setOpenMaintenanceHistoryCalendarYear(Integer openMaintenanceHistoryCalendarYear) {
		this.openMaintenanceHistoryCalendarYear = openMaintenanceHistoryCalendarYear;
	}

	public Integer getOpenMaintenanceHistoryDistribution() {
		return openMaintenanceHistoryDistribution;
	}

	public void setOpenMaintenanceHistoryDistribution(Integer openMaintenanceHistoryDistribution) {
		this.openMaintenanceHistoryDistribution = openMaintenanceHistoryDistribution;
	}

	public Integer getOpenMaintenanceHistoryFiscalYear() {
		return openMaintenanceHistoryFiscalYear;
	}

	public void setOpenMaintenanceHistoryFiscalYear(Integer openMaintenanceHistoryFiscalYear) {
		this.openMaintenanceHistoryFiscalYear = openMaintenanceHistoryFiscalYear;
	}

	public Integer getOpenMaintenanceHistoryTransaction() {
		return openMaintenanceHistoryTransaction;
	}

	public void setOpenMaintenanceHistoryTransaction(Integer openMaintenanceHistoryTransaction) {
		this.openMaintenanceHistoryTransaction = openMaintenanceHistoryTransaction;
	}

	public String getPriceLevelId() {
		return priceLevelId;
	}

	public void setPriceLevelId(String priceLevelId) {
		this.priceLevelId = priceLevelId;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public String getVatId() {
		return vatId;
	}

	public void setVatId(String vatId) {
		this.vatId = vatId;
	}

	public String getShipmentMethodId() {
		return shipmentMethodId;
	}

	public void setShipmentMethodId(String shipmentMethodId) {
		this.shipmentMethodId = shipmentMethodId;
	}

	public String getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(String paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public String getSalesmanId() {
		return salesmanId;
	}

	public void setSalesmanId(String salesmanId) {
		this.salesmanId = salesmanId;
	}

	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}

	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}

	public String getCheckbookId() {
		return checkbookId;
	}

	public void setCheckbookId(String checkbookId) {
		this.checkbookId = checkbookId;
	}

	public String getFinanceChargeAmount() {
		return financeChargeAmount;
	}

	public void setFinanceChargeAmount(String financeChargeAmount) {
		this.financeChargeAmount = financeChargeAmount;
	}

	public String getMinimumChargeAmount() {
		return minimumChargeAmount;
	}

	public void setMinimumChargeAmount(String minimumChargeAmount) {
		this.minimumChargeAmount = minimumChargeAmount;
	}

	public String getTradeDiscountPercent() {
		return tradeDiscountPercent;
	}

	public void setTradeDiscountPercent(String tradeDiscountPercent) {
		this.tradeDiscountPercent = tradeDiscountPercent;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getTerritoryDescription() {
		return territoryDescription;
	}

	public void setTerritoryDescription(String territoryDescription) {
		this.territoryDescription = territoryDescription;
	}

	public String getSalesmanName() {
		return salesmanName;
	}

	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}

	public String getMiscVatScheduleId() {
		return miscVatScheduleId;
	}

	public void setMiscVatScheduleId(String miscVatScheduleId) {
		this.miscVatScheduleId = miscVatScheduleId;
	}

	public String getFreightVatScheduleId() {
		return freightVatScheduleId;
	}

	public void setFreightVatScheduleId(String freightVatScheduleId) {
		this.freightVatScheduleId = freightVatScheduleId;
	}

	public String getMiscVatPercent() {
		return miscVatPercent;
	}

	public void setMiscVatPercent(String miscVatPercent) {
		this.miscVatPercent = miscVatPercent;
	}

	public String getFreightVatPercent() {
		return freightVatPercent;
	}

	public void setFreightVatPercent(String freightVatPercent) {
		this.freightVatPercent = freightVatPercent;
	}

	public String getVatPercent() {
		return vatPercent;
	}

	public void setVatPercent(String vatPercent) {
		this.vatPercent = vatPercent;
	}
	

}
