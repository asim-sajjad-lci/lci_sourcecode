/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar80000 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "Batch_Transaction_Type")
@NamedQuery(name="BatchTransactionType.findAll", query="SELECT a FROM BatchTransactionType a")
public class BatchTransactionType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int batchTransactionTypeId;
	
	@Column(name="TYPE_ID")
	private int typeId;
	
	@Column(name="TRANSACTION_TYPE")
	private String transactionType;
	
	@Column(name="TRANSACTION_MODULE")
	private String transactionModule;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	@ManyToOne
	@JoinColumn(name="lang_Id")
	private Language language;

	public int getBatchTransactionTypeId() {
		return batchTransactionTypeId;
	}

	public void setBatchTransactionTypeId(int batchTransactionTypeId) {
		this.batchTransactionTypeId = batchTransactionTypeId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionModule() {
		return transactionModule;
	}

	public void setTransactionModule(String transactionModule) {
		this.transactionModule = transactionModule;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}
	
	
}