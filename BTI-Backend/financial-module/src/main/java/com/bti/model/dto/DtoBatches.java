/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.APBatches;
import com.bti.model.ARBatches;
import com.bti.model.BatchTransactionType;
import com.bti.model.GLBatches;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DtoAccountCategory class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 10, 2017
 * Modified on: AUG 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoBatches {

	private String batchId;
	private String description;
	private String transactionType;
	private Integer transactionTypeId;
	private String postingDate;
	private Double totalTransactions;
	private Integer quantityTotal;
	private Boolean isApproved;
	private Integer userId;
	private String approvedDate;
	private String messageType;
	private int sourceDocumentID;
	private String sourceDocument;
	

	public DtoBatches() {

	}
	
	public DtoBatches(GLBatches glBatches) {
		this.batchId = glBatches.getBatchID();
		this.description = glBatches.getBatchDescription();
		this.sourceDocumentID = glBatches.getGlConfigurationAuditTrialCodes().getSeriesIndex();
		this.sourceDocument = glBatches.getGlConfigurationAuditTrialCodes().getSourceDocument();
		this.transactionType="";
		this.transactionTypeId=0;
		this.totalTransactions=UtilRoundDecimal.roundDecimalValue(glBatches.getTotalAmountTransactionInBatch());
		this.quantityTotal=glBatches.getTotalNumberTransactionInBatch();
		this.postingDate="";
		if(glBatches.getPostingDate()!=null){
			this.postingDate=UtilDateAndTime.dateToStringddmmyyyy(glBatches.getPostingDate());
		}
	}

	public DtoBatches(GLBatches glBatches , BatchTransactionType batchTransactionType) {
		this.batchId = glBatches.getBatchID();
		this.description = glBatches.getBatchDescription();
		this.sourceDocumentID = glBatches.getGlConfigurationAuditTrialCodes().getSeriesIndex();
		this.sourceDocument = glBatches.getGlConfigurationAuditTrialCodes().getSourceDocument();
		this.transactionType="";
		this.transactionTypeId=0;
		if(batchTransactionType!=null){
			this.transactionType=batchTransactionType.getTransactionType();
			this.transactionTypeId=batchTransactionType.getTypeId();
		}
		this.totalTransactions=UtilRoundDecimal.roundDecimalValue(glBatches.getTotalAmountTransactionInBatch());
		this.quantityTotal=glBatches.getTotalNumberTransactionInBatch();
		this.postingDate="";
		if(glBatches.getPostingDate()!=null){
			this.postingDate=UtilDateAndTime.dateToStringddmmyyyy(glBatches.getPostingDate());
		}
	}

	public DtoBatches(ARBatches arBatches, BatchTransactionType batchTransactionType) {
		this.batchId = arBatches.getBatchID();
		this.description = arBatches.getBatchDescription();
		this.transactionType="";
		this.transactionTypeId=0;
		if(batchTransactionType!=null){
			this.transactionType=batchTransactionType.getTransactionType();
			this.transactionTypeId=batchTransactionType.getTypeId();
		}
		this.totalTransactions=UtilRoundDecimal.roundDecimalValue(arBatches.getTotalAmountTransactionInBatch());
		this.quantityTotal=arBatches.getTotalNumberTransactionInBatch();
		this.postingDate="";
		if(arBatches.getPostingDate()!=null){
			this.postingDate=UtilDateAndTime.dateToStringddmmyyyy(arBatches.getPostingDate());
		}
	}

	public DtoBatches(APBatches apBatches, BatchTransactionType batchTransactionType) {
		this.batchId = apBatches.getBatchID();
		this.description = apBatches.getBatchDescription();
		this.transactionType="";
		this.transactionTypeId=0;
		if(batchTransactionType!=null){
			this.transactionType=batchTransactionType.getTransactionType();
			this.transactionTypeId=batchTransactionType.getTypeId();
		}
		this.totalTransactions=UtilRoundDecimal.roundDecimalValue(apBatches.getTotalAmountTransactionInBatch());
		this.quantityTotal=apBatches.getTotalNumberTransactionInBatch();
		this.postingDate="";
		if(apBatches.getPostingDate()!=null){
			this.postingDate=UtilDateAndTime.dateToStringddmmyyyy(apBatches.getPostingDate());
		}
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Integer getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Integer transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public String getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}

	public Boolean getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}
	
	public Double getTotalTransactions() {
		return totalTransactions;
	}

	public void setTotalTransactions(Double totalTransactions) {
		this.totalTransactions = UtilRoundDecimal.roundDecimalValue(totalTransactions);
	}

	public Integer getQuantityTotal() {
		return quantityTotal;
	}

	public void setQuantityTotal(Integer quantityTotal) {
		this.quantityTotal = quantityTotal;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public int getSourceDocumentID() {
		return sourceDocumentID;
	}

	public void setSourceDocumentID(int sourceDocumentID) {
		this.sourceDocumentID = sourceDocumentID;
	}

	public String getSourceDocument() {
		return sourceDocument;
	}

	public void setSourceDocument(String sourceDocument) {
		this.sourceDocument = sourceDocument;
	}
	
	
	
	

}
