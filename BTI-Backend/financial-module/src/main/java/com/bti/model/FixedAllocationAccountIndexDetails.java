/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the gl00115 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl00115")
@NamedQuery(name="FixedAllocationAccountIndexDetails.findAll", query="SELECT g FROM FixedAllocationAccountIndexDetails g")
public class FixedAllocationAccountIndexDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PRCNTAGE")
	private Double percentage;

	@Column(name="SEQUANCE")
	private int sequance;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	

	//bi-directional one-to-one association to Gl00105
	/*@OneToOne(mappedBy="fixedAllocationAccountIndexDetails")
	private FixedAllocationAccountIndexHeader fixedAllocationAccountIndexHeader;*/

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;
	
	@OneToOne
	@JoinColumn(name="ACTFIXID")
	private FixedAllocationAccountIndexHeader fixedAllocationAccountIndexHeader;

	public FixedAllocationAccountIndexDetails() {
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public int getSequance() {
		return sequance;
	}

	public void setSequance(int sequance) {
		this.sequance = sequance;
	}

	/*public FixedAllocationAccountIndexHeader getFixedAllocationAccountIndexHeader() {
		return fixedAllocationAccountIndexHeader;
	}

	public void setFixedAllocationAccountIndexHeader(FixedAllocationAccountIndexHeader fixedAllocationAccountIndexHeader) {
		this.fixedAllocationAccountIndexHeader = fixedAllocationAccountIndexHeader;
	}*/

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FixedAllocationAccountIndexHeader getFixedAllocationAccountIndexHeader() {
		return fixedAllocationAccountIndexHeader;
	}

	public void setFixedAllocationAccountIndexHeader(FixedAllocationAccountIndexHeader fixedAllocationAccountIndexHeader) {
		this.fixedAllocationAccountIndexHeader = fixedAllocationAccountIndexHeader;
	}


	

	
	
}