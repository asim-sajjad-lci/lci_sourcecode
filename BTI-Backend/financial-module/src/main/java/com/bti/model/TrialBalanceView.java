package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

//@IdClass(value = rptaccountstatement_lcl.class)
@Entity 
@Immutable
@Table(name = "trialbalancev")
@NamedQuery(name="TrialBalanceView.findAll", query="SELECT v FROM TrialBalanceView v")
public class TrialBalanceView implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ACCOUNTNUMBER")
	private String accountNumber;

//	@Id
	@Column(name="ACCOUNTDESCRIPTION")
	private String accountDescription;
	
//	@Id
	@Column(name="TRXDDT")
	private Date trxDdt;
	
	//	@Id
	@Column(name="DEBITAMT")
	private double debitAmt;
	
//	@Id
	@Column(name="CRDTAMT")
	private double creditAmt;
	
//	@Id
	@Column(name="DR_BB")
	private int drBb;
	
//	@Id
	@Column(name="CR_BB")
	private int crBb;
	
//	@Id
	@Column(name="DR_NET")
	private double drNet;
	
//	@Id
	@Column(name="CR_NET")
	private double crNet;
	
//	@Id
	@Column(name="NET")
	private double net;

	//	@Id
	@Column(name="ENDBB")
	private double endBb;
	
	
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public Date getTrxDdt() {
		return trxDdt;
	}

	public void setTrxDdt(Date trxDdt) {
		this.trxDdt = trxDdt;
	}

	public double getDebitAmt() {
		return debitAmt;
	}

	public void setDebitAmt(double debitAmt) {
		this.debitAmt = debitAmt;
	}

	public double getCreditAmt() {
		return creditAmt;
	}

	public void setCreditAmt(double creditAmt) {
		this.creditAmt = creditAmt;
	}

	public int getDrBb() {
		return drBb;
	}

	public void setDrBb(int drBb) {
		this.drBb = drBb;
	}

	public int getCrBb() {
		return crBb;
	}

	public void setCrBb(int crBb) {
		this.crBb = crBb;
	}

	public double getDrNet() {
		return drNet;
	}

	public void setDrNet(double drNet) {
		this.drNet = drNet;
	}

	public double getCrNet() {
		return crNet;
	}

	public void setCrNet(double crNet) {
		this.crNet = crNet;
	}

	public double getNet() {
		return net;
	}

	public void setNet(double net) {
		this.net = net;
	}

	public double getEndBb() {
		return endBb;
	}

	public void setEndBb(double endBb) {
		this.endBb = endBb;
	}

	
}
