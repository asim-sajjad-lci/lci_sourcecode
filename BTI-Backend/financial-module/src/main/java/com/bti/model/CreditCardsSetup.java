/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the sy03700 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "sy03700")
@NamedQuery(name="CreditCardsSetup.findAll", query="SELECT s FROM CreditCardsSetup s")
public class CreditCardsSetup implements Serializable {
	private static final long serialVersionUID = 1L;


	@Column(name="CARDINDX")
	private int cardIndx;


	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CARDID")
	private String cardId;

	@Column(name="CARDNME")
	private String creditCardName;

	@Column(name="CARDNMEA")
	private String creditCardNameArabic;

	/*@Column(name="CARDTYP")
	private int creditCardType;*/
	
	//bi-directional many-to-one association to CreditCardType
	@ManyToOne
	@JoinColumn(name="CARDTYP")
	private CreditCardType creditCardType;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CHEKBOKID")
	private String checkBookIdBank;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ar00102
	@OneToMany(mappedBy="creditCardsSetup")
	private List<CustomerMaintenanceOptions> customerMaintenanceOptions;
	
	public CreditCardsSetup() {
	}

	public int getCardIndx() {
		return cardIndx;
	}

	public void setCardIndx(int cardIndx) {
		this.cardIndx = cardIndx;
	}


	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getCreditCardName() {
		return creditCardName;
	}

	public void setCreditCardName(String creditCardName) {
		this.creditCardName = creditCardName;
	}

	public String getCreditCardNameArabic() {
		return creditCardNameArabic;
	}

	public void setCreditCardNameArabic(String creditCardNameArabic) {
		this.creditCardNameArabic = creditCardNameArabic;
	}

	/*public int getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardType(int creditCardType) {
		this.creditCardType = creditCardType;
	}*/

	public CreditCardType getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardType(CreditCardType creditCardType) {
		this.creditCardType = creditCardType;
	}

	public int getChangeBy() {
		return changeBy;
	}
	
	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}
	public String getCheckBookIdBank() {
		return checkBookIdBank;
	}

	public void setCheckBookIdBank(String checkBookIdBank) {
		this.checkBookIdBank = checkBookIdBank;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public List<CustomerMaintenanceOptions> getCustomerMaintenanceOptions() {
		return customerMaintenanceOptions;
	}

	public void setCustomerMaintenanceOptions(List<CustomerMaintenanceOptions> customerMaintenanceOptions) {
		this.customerMaintenanceOptions = customerMaintenanceOptions;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	
	
}