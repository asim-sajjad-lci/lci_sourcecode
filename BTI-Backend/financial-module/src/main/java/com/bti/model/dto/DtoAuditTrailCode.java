/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.GLConfigurationAuditTrialCodes;
import com.bti.model.GLConfigurationAuditTrialSeriesType;

/**
 * Description: DtoAuditTrailCode class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 11, 2017
 * Modified on: AUG 11, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

public class DtoAuditTrailCode {
	 
	private Integer seriesIndex;
	private Integer seriesId;
	private String seriesName;
	private String sourceDocument;
	private Integer sequenceNumber;
	private String sourceCode;
	private Integer seriesNumber;
	public DtoAuditTrailCode() {

	}
	public DtoAuditTrailCode(GLConfigurationAuditTrialCodes auditTrialCodes, GLConfigurationAuditTrialSeriesType glConfigurationAuditTrialSeriesType) {
		this.seriesIndex = auditTrialCodes.getSeriesIndex();
		this.seriesId=0;
		if(glConfigurationAuditTrialSeriesType!=null){
			this.seriesId = glConfigurationAuditTrialSeriesType.getTypeId();
			this.seriesName=glConfigurationAuditTrialSeriesType.getTypePrimary();
		}
		this.sourceDocument = auditTrialCodes.getSourceDocument();
		this.sourceCode = auditTrialCodes.getTransactionSourceCode();
		this.sequenceNumber = auditTrialCodes.getSequenceNumber();
		this.seriesNumber=auditTrialCodes.getNextTransactionSourceNumber();
	}
	 
	public Integer getSeriesId() {
		return seriesId;
	}
	public void setSeriesId(Integer seriesId) {
		this.seriesId = seriesId;
	}
	public String getSourceDocument() {
		return sourceDocument;
	}
	public void setSourceDocument(String sourceDocument) {
		this.sourceDocument = sourceDocument;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getSourceCode() {
		return sourceCode;
	}
	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}
	public Integer getSeriesIndex() {
		return seriesIndex;
	}
	public void setSeriesIndex(Integer seriesIndex) {
		this.seriesIndex = seriesIndex;
	}
	public String getSeriesName() {
		return seriesName;
	}
	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public Integer getSeriesNumber() {
		return seriesNumber;
	}
	public void setSeriesNumber(Integer seriesNumber) {
		this.seriesNumber = seriesNumber;
	}
	
	 
}
