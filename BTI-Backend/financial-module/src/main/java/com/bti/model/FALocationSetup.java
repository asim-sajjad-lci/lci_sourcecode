/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the fa40005 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40005")
@NamedQuery(name="FALocationSetup.findAll", query="SELECT f FROM FALocationSetup f")
public class FALocationSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="LOCATNINDX")
	private int locationIndx;
	
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="LOCATNID")
	private String locationId;

	/*@Column(name="CITYID")
	private int cityId;*/

	@Column(name="CITYNAM")
	private String cityName;

	/*@Column(name="COUNTRYID")
	private int countryId;*/

	@Column(name="COUNTRYNAM")
	private String countryName;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;


	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;


	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	/*@Column(name="DISTID")
	private int districtId;*/

	@Column(name="DISTNAM")
	private String districtName;

	@Column(name="MODIFDT")
	private Date modifyDate;

	//bi-directional many-to-one association to Fa00101
	@OneToMany(mappedBy="faLocationSetup")
	private List<FAGeneralMaintenance> faGeneralMaintenances;

	//bi-directional many-to-one association to Fa40006
	@OneToMany(mappedBy="faLocationSetup")
	private List<FAPhysicalLocationSetup> faPhysicalLocationSetups;
	
	@ManyToOne
	@JoinColumn(name="CITYID")
	private CityMaster cityMaster;

	//bi-directional many-to-one association to CountryMaster
	@ManyToOne
	@JoinColumn(name="COUNTRYID")
	private CountryMaster countryMaster;

	//bi-directional many-to-one association to StateMaster
	@ManyToOne
	@JoinColumn(name="DISTID")
	private StateMaster stateMaster;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public FALocationSetup() {
	}

	public int getLocationIndx() {
		return locationIndx;
	}

	public void setLocationIndx(int locationIndx) {
		this.locationIndx = locationIndx;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public List<FAGeneralMaintenance> getFaGeneralMaintenances() {
		return faGeneralMaintenances;
	}

	public void setFaGeneralMaintenances(List<FAGeneralMaintenance> faGeneralMaintenances) {
		this.faGeneralMaintenances = faGeneralMaintenances;
	}

	public List<FAPhysicalLocationSetup> getFaPhysicalLocationSetups() {
		return faPhysicalLocationSetups;
	}

	public void setFaPhysicalLocationSetups(List<FAPhysicalLocationSetup> faPhysicalLocationSetups) {
		this.faPhysicalLocationSetups = faPhysicalLocationSetups;
	}

	public CityMaster getCityMaster() {
		return cityMaster;
	}

	public void setCityMaster(CityMaster cityMaster) {
		this.cityMaster = cityMaster;
	}

	public CountryMaster getCountryMaster() {
		return countryMaster;
	}

	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}

	public StateMaster getStateMaster() {
		return stateMaster;
	}

	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}


}