/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl40301 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40301")
@NamedQuery(name="GLQuickJournalAccountsSetup.findAll", query="SELECT s FROM GLQuickJournalAccountsSetup s")
public class GLQuickJournalAccountsSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
 	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="JORNSEQCN")
	private int journalSequence;

	@ManyToOne
	@JoinColumn(name="JORNALID")
	private GLQuickJournalSetup glQuickJournalSetup;

	@Column(name="ACTROWID")
	private int accountTableRowIndex;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	  
	public GLQuickJournalAccountsSetup() {
		
	}

	public int getJournalSequence() {
		return journalSequence;
	}

	public void setJournalSequence(int journalSequence) {
		this.journalSequence = journalSequence;
	}
	
	public GLQuickJournalSetup getGlQuickJournalSetup() {
		return glQuickJournalSetup;
	}

	public void setGlQuickJournalSetup(GLQuickJournalSetup glQuickJournalSetup) {
		this.glQuickJournalSetup = glQuickJournalSetup;
	}

	public int getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(int accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}
	
}