/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.GLBatches;
import com.bti.model.JournalEntryHeader;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DtoGLReportHeader class having getter and setter for fields
 * (POJO) Name Name of Project: BTI Created on: JAN 10, 2018 Modified on: JAN
 * 10, 2018 4:19:38 PM
 * 
 * @author seasia Version:
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoGLReportHeader {

	private Integer id;
	private String journalID;
	private String userId;
	private String originalJournalID;
	private String glBatchId;
	private String glBatchComment;
	private String transactionType;
	private String transactionDate;
	private String transactionReversingDate;
	private Integer sourceDocumentId;
	private String sourceDocument;
	private String transactionReference;
	private String transactionReferenceArabic;
	private Integer totalDistributions;
	private Double totalDebit;
	private Double totalCredit;
	List<DtoGLReportDetail> dtoGLReportDetailList;

	public DtoGLReportHeader() {
	}

	public DtoGLReportHeader(JournalEntryHeader journalEntryHeader) {
		this.id = journalEntryHeader.getId();
		this.journalID = journalEntryHeader.getJournalID();
		GLBatches glBatches = journalEntryHeader.getGlBatches();
		this.glBatchId = "";
		this.glBatchComment ="";
		if (glBatches != null) {
			this.glBatchId = glBatches.getBatchID();
			this.glBatchComment = glBatches.getBatchDescription();
		}
		this.transactionType = "";
		if(journalEntryHeader.getTransactionType()==1){
			this.transactionType = "Standard";
		}
		if(journalEntryHeader.getTransactionType()==2){
			this.transactionType = "Reversing";
		}
		this.transactionDate = "";
		if (journalEntryHeader.getTransactionDate() != null) {
			this.transactionDate = UtilDateAndTime.dateToStringddmmyyyy(journalEntryHeader.getTransactionDate());
		}

		this.transactionReversingDate = "";
		if (journalEntryHeader.getTransactionReversingDate() != null) {
			this.transactionReversingDate = UtilDateAndTime
					.dateToStringddmmyyyy(journalEntryHeader.getTransactionReversingDate());
		}

		this.sourceDocumentId = 0;
		this.sourceDocument = "";
		if (journalEntryHeader.getGlConfigurationAuditTrialCodes() != null) {
			this.sourceDocumentId = journalEntryHeader.getGlConfigurationAuditTrialCodes().getSeriesIndex();
			if (journalEntryHeader.getGlConfigurationAuditTrialCodes()
					.getGlConfigurationAuditTrialSeriesType() != null) {
//				this.sourceDocument = journalEntryHeader.getGlConfigurationAuditTrialCodes()
//						.getGlConfigurationAuditTrialSeriesType().getTypePrimary();
				
				this.sourceDocument = journalEntryHeader.getGlConfigurationAuditTrialCodes().getSourceDocument();
			}
		}

		this.transactionReference = "";
		if (UtilRandomKey.isNotBlank(journalEntryHeader.getJournalDescription())) {
			this.transactionReference = journalEntryHeader.getJournalDescription();
		}

		this.transactionReferenceArabic = "";
		if (UtilRandomKey.isNotBlank(journalEntryHeader.getJournalDescriptionArabic())) {
			this.transactionReferenceArabic = journalEntryHeader.getJournalDescriptionArabic();
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJournalID() {
		return journalID;
	}

	public void setJournalID(String journalID) {
		this.journalID = journalID;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOriginalJournalID() {
		return originalJournalID;
	}

	public void setOriginalJournalID(String originalJournalID) {
		this.originalJournalID = originalJournalID;
	}

	public String getGlBatchId() {
		return glBatchId;
	}

	public void setGlBatchId(String glBatchId) {
		this.glBatchId = glBatchId;
	}

	public String getGlBatchComment() {
		return glBatchComment;
	}

	public void setGlBatchComment(String glBatchComment) {
		this.glBatchComment = glBatchComment;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionReversingDate() {
		return transactionReversingDate;
	}

	public void setTransactionReversingDate(String transactionReversingDate) {
		this.transactionReversingDate = transactionReversingDate;
	}

	public Integer getSourceDocumentId() {
		return sourceDocumentId;
	}

	public void setSourceDocumentId(Integer sourceDocumentId) {
		this.sourceDocumentId = sourceDocumentId;
	}

	public String getSourceDocument() {
		return sourceDocument;
	}

	public void setSourceDocument(String sourceDocument) {
		this.sourceDocument = sourceDocument;
	}

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getTransactionReferenceArabic() {
		return transactionReferenceArabic;
	}

	public void setTransactionReferenceArabic(String transactionReferenceArabic) {
		this.transactionReferenceArabic = transactionReferenceArabic;
	}

	public Integer getTotalDistributions() {
		return totalDistributions;
	}

	public void setTotalDistributions(Integer totalDistributions) {
		this.totalDistributions = totalDistributions;
	}

	public Double getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(Double totalDebit) {
		this.totalDebit = UtilRoundDecimal.roundDecimalValue(totalDebit);
	}

	public Double getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(Double totalCredit) {
		this.totalCredit = UtilRoundDecimal.roundDecimalValue(totalCredit);
	}

	public List<DtoGLReportDetail> getDtoGLReportDetailList() {
		return dtoGLReportDetailList;
	}

	public void setDtoGLReportDetailList(List<DtoGLReportDetail> dtoGLReportDetailList) {
		this.dtoGLReportDetailList = dtoGLReportDetailList;
	}

}
