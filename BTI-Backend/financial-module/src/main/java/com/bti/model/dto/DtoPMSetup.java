/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.PMSetup;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoPMSetup {

	private Integer id;
	private Integer ageingBy;
	private boolean ageUnappliedCreditAmounts;
	private Integer applyByDefault;
	private Integer allowDuplicateInvoicePerVendor;
	private Boolean apTrackingDiscountAvailable;
	private String checkBookBankId;
	private Integer checkFormat;
	private String removeVendorHoldPassword;
	private Boolean deleteUnpostedPrintedDocuments;
	private String exceedMaximumInvoiceAmount;
	private String exceedMaximumWriteoffAmount;
	private String freightVatScheduleId;
	private String miscVatScheduleId;
	private Boolean overrideVoucherNumberTransactionEntry;
	private String purchaseVatScheduleId;
	private Boolean printHistoricalAgedTrialBalance;
	private String userDefine1;
	private String userDefine2;
	private String userDefine3;
	private String messageType;
	private List<DtoPMPeriodSetup> pmPeriodSetupsList;

	public DtoPMSetup() {
		
	}
	public DtoPMSetup(PMSetup pmSetup) {
		this.setAgeingBy(pmSetup.getAgeingBy());
		this.setAgeUnappliedCreditAmounts(pmSetup.isAgeUnappliedCreditAmounts());
		this.setApplyByDefault(pmSetup.getApplyByDefault());
		this.setApTrackingDiscountAvailable(pmSetup.getApTrackingDiscountAvailable());
		this.setCheckBookBankId("");
		if(pmSetup.getCheckbookMaintenance()!=null)
		{
			this.setCheckBookBankId(pmSetup.getCheckbookMaintenance().getCheckBookId());
		}
		this.setCheckFormat(pmSetup.getPmSetupFormatType()!=null?pmSetup.getPmSetupFormatType().getTypeId():null);
		this.setDeleteUnpostedPrintedDocuments(pmSetup.getDeleteUnpostedPrintedDocuments());
		this.setExceedMaximumInvoiceAmount(pmSetup.getExceedMaximumInvoiceAmount());
		this.setExceedMaximumWriteoffAmount(pmSetup.getExceedMaximumWriteoffAmount());
		this.setFreightVatScheduleId("");
		if(pmSetup.getFreightVatScheduleId()!=null){
			this.setFreightVatScheduleId(pmSetup.getFreightVatScheduleId().getVatScheduleId());
		}
		this.setMiscVatScheduleId("");
		if(pmSetup.getMiscVatScheduleId()!=null){
			this.setMiscVatScheduleId(pmSetup.getMiscVatScheduleId().getVatScheduleId());
		}
		
		this.setOverrideVoucherNumberTransactionEntry(pmSetup.getOverrideVoucherNumberTransactionEntry());
		this.setPrintHistoricalAgedTrialBalance(pmSetup.getPrintHistoricalAgedTrialBalance());
		this.setPurchaseVatScheduleId("");
		if(pmSetup.getPurchaseVatScheduleId()!=null){
			this.setPurchaseVatScheduleId(pmSetup.getPurchaseVatScheduleId().getVatScheduleId());
		}
		
		this.setRemoveVendorHoldPassword(pmSetup.getRemoveVendorHoldPassword());
		this.setUserDefine1(pmSetup.getUserDefine1());
		this.setUserDefine2(pmSetup.getUserDefine2());
		this.setUserDefine3(pmSetup.getUserDefine3());
		this.setAllowDuplicateInvoicePerVendor(pmSetup.getAllowDuplicateInvoicePerVendor());
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAgeingBy() {
		return ageingBy;
	}
	public void setAgeingBy(Integer ageingBy) {
		this.ageingBy = ageingBy;
	}
	public boolean getIsAgeUnappliedCreditAmounts() {
		return ageUnappliedCreditAmounts;
	}
	public boolean isAgeUnappliedCreditAmounts() {
		return ageUnappliedCreditAmounts;
	}
	public void setAgeUnappliedCreditAmounts(boolean ageUnappliedCreditAmounts) {
		this.ageUnappliedCreditAmounts = ageUnappliedCreditAmounts;
	}
	public Integer getApplyByDefault() {
		return applyByDefault;
	}
	public void setApplyByDefault(Integer applyByDefault) {
		this.applyByDefault = applyByDefault;
	}
	public Boolean getApTrackingDiscountAvailable() {
		return apTrackingDiscountAvailable;
	}
	public void setApTrackingDiscountAvailable(Boolean apTrackingDiscountAvailable) {
		this.apTrackingDiscountAvailable = apTrackingDiscountAvailable;
	}
	public String getCheckBookBankId() {
		return checkBookBankId;
	}
	public void setCheckBookBankId(String checkBookBankId) {
		this.checkBookBankId = checkBookBankId;
	}
	public Integer getCheckFormat() {
		return checkFormat;
	}
	public void setCheckFormat(Integer checkFormat) {
		this.checkFormat = checkFormat;
	}
	public String getRemoveVendorHoldPassword() {
		return removeVendorHoldPassword;
	}
	public void setRemoveVendorHoldPassword(String removeVendorHoldPassword) {
		this.removeVendorHoldPassword = removeVendorHoldPassword;
	}
	 
	public Boolean getDeleteUnpostedPrintedDocuments() {
		return deleteUnpostedPrintedDocuments;
	}
	public void setDeleteUnpostedPrintedDocuments(Boolean deleteUnpostedPrintedDocuments) {
		this.deleteUnpostedPrintedDocuments = deleteUnpostedPrintedDocuments;
	}
	public String getExceedMaximumInvoiceAmount() {
		return exceedMaximumInvoiceAmount;
	}
	public void setExceedMaximumInvoiceAmount(String exceedMaximumInvoiceAmount) {
		this.exceedMaximumInvoiceAmount = exceedMaximumInvoiceAmount;
	}
	public String getExceedMaximumWriteoffAmount() {
		return exceedMaximumWriteoffAmount;
	}
	public void setExceedMaximumWriteoffAmount(String exceedMaximumWriteoffAmount) {
		this.exceedMaximumWriteoffAmount = exceedMaximumWriteoffAmount;
	}
	public String getFreightVatScheduleId() {
		return freightVatScheduleId;
	}
	public void setFreightVatScheduleId(String freightVatScheduleId) {
		this.freightVatScheduleId = freightVatScheduleId;
	}
	public String getMiscVatScheduleId() {
		return miscVatScheduleId;
	}
	public void setMiscVatScheduleId(String miscVatScheduleId) {
		this.miscVatScheduleId = miscVatScheduleId;
	}
	public Boolean getOverrideVoucherNumberTransactionEntry() {
		return overrideVoucherNumberTransactionEntry;
	}
	public void setOverrideVoucherNumberTransactionEntry(Boolean overrideVoucherNumberTransactionEntry) {
		this.overrideVoucherNumberTransactionEntry = overrideVoucherNumberTransactionEntry;
	}
	public String getPurchaseVatScheduleId() {
		return purchaseVatScheduleId;
	}
	public void setPurchaseVatScheduleId(String purchaseVatScheduleId) {
		this.purchaseVatScheduleId = purchaseVatScheduleId;
	}
	public Boolean getPrintHistoricalAgedTrialBalance() {
		return printHistoricalAgedTrialBalance;
	}
	public void setPrintHistoricalAgedTrialBalance(Boolean printHistoricalAgedTrialBalance) {
		this.printHistoricalAgedTrialBalance = printHistoricalAgedTrialBalance;
	}
	public String getUserDefine1() {
		return userDefine1;
	}
	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}
	public String getUserDefine2() {
		return userDefine2;
	}
	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}
	public String getUserDefine3() {
		return userDefine3;
	}
	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	public List<DtoPMPeriodSetup> getPmPeriodSetupsList() {
		return pmPeriodSetupsList;
	}
	public void setPmPeriodSetupsList(List<DtoPMPeriodSetup> pmPeriodSetupsList) {
		this.pmPeriodSetupsList = pmPeriodSetupsList;
	}
	public Integer getAllowDuplicateInvoicePerVendor() {
		return allowDuplicateInvoicePerVendor;
	}
	public void setAllowDuplicateInvoicePerVendor(Integer allowDuplicateInvoicePerVendor) {
		this.allowDuplicateInvoicePerVendor = allowDuplicateInvoicePerVendor;
	}
	
}
