/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FABookClassSetup;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;

public class DtoFABookClassSetup {
	private int bookIndex;
	private String bookId;
	private Integer amortizationCode;
	private Double initialAllowancePercentage;
	private Double amortizationPercentage;
	private Double amortizationAmount;
	private Integer averagingConvention;
	private int origLifeDays;
	private int origLifeYears;
	private int salvageEstimate;
	private Double salvagePercentage;
	private Integer specialDepreciationAllowance;
	private Double specialDepreciationAllowancePercent;
	private Integer swtch;
	private Integer depreciationMethodId;
	private String bookDescription;
	private String bookDescriptionArabic;
	private String messageType;
	private String classSetupDescription;
	private String classSetupDescriptionArabic;
	private String depreciationMethod;
	private String averagingConventionType;
	
	private String classId;
	public DtoFABookClassSetup() {
		
	}
	public DtoFABookClassSetup(FABookClassSetup faBookClassSetup) {
		this.bookIndex = faBookClassSetup.getBookIndex();
		this.setAmortizationCode(faBookClassSetup.getFaBookMaintenanceAmortizationCodeType()!=null?faBookClassSetup.getFaBookMaintenanceAmortizationCodeType().getTypeId():null);
		this.setAmortizationAmount(UtilRoundDecimal.roundDecimalValue(faBookClassSetup.getAmortizationAmount()));
		  this.setAmortizationPercentage(faBookClassSetup.getAmortizationPercentage());
		 
		  
		  if(faBookClassSetup.getFaDepreciationMethods()!=null){
			  this.setDepreciationMethodId(faBookClassSetup.getFaDepreciationMethods().getDepreciationMethodId());
			  this.setDepreciationMethod("");
			  if(UtilRandomKey.isNotBlank(faBookClassSetup.getFaDepreciationMethods().getDepreciationDescription())){
				  this.setDepreciationMethod(faBookClassSetup.getFaDepreciationMethods().getDepreciationDescription());
			  }
			  
		  }
		  
		  if(faBookClassSetup.getAveragingConventionType()!=null){
			  this.setAveragingConvention(faBookClassSetup.getAveragingConventionType().getTypeId());
			  this.setAveragingConventionType(faBookClassSetup.getAveragingConventionType().getTypePrimary());
		  }
		 
		  this.setInitialAllowancePercentage(UtilRoundDecimal.roundDecimalValue(faBookClassSetup.getInitialAllowancePercentage()));
		  this.setOrigLifeDays(faBookClassSetup.getOrigLifeDays());
		  this.setOrigLifeYears(faBookClassSetup.getOrigLifeYears());
		  this.setSalvageEstimate(faBookClassSetup.getSalvageEstimate());
		  this.setSalvagePercentage(faBookClassSetup.getSalvagePercentage());
		  this.setSpecialDepreciationAllowance(faBookClassSetup.getSpecialDepreciationAllowance());
		  this.setSpecialDepreciationAllowancePercent(UtilRoundDecimal.roundDecimalValue(faBookClassSetup.getSpecialDepreciationAllowancePercent()));
		  this.setSwtch(faBookClassSetup.getSwitchOverType()!=null?faBookClassSetup.getSwitchOverType().getTypeId():null);
		  
		  if(faBookClassSetup.getBookSetup()!=null){
			  this.setBookId(faBookClassSetup.getBookSetup().getBookId());
			  this.setBookDescription(faBookClassSetup.getBookSetup().getBookDescription());
			  this.setBookDescriptionArabic(faBookClassSetup.getBookSetup().getBookDescriptionArabic());
		  }
		  if(faBookClassSetup.getFaClassSetup()!=null){
			  this.setClassId(faBookClassSetup.getFaClassSetup().getClassId());
			  this.setClassSetupDescription(faBookClassSetup.getFaClassSetup().getClassSetupDescription());
			  this.setClassSetupDescriptionArabic(faBookClassSetup.getFaClassSetup().getClassSetupDescriptionArabic());
		  }
	}
	public int getBookIndex() {
		return bookIndex;
	}
	public void setBookIndex(int bookIndex) {
		this.bookIndex = bookIndex;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public Integer getAmortizationCode() {
		return amortizationCode;
	}
	public void setAmortizationCode(Integer amortizationCode) {
		this.amortizationCode = amortizationCode;
	}
	public Double getInitialAllowancePercentage() {
		return initialAllowancePercentage;
	}
	public void setInitialAllowancePercentage(Double initialAllowancePercentage) {
		this.initialAllowancePercentage = UtilRoundDecimal.roundDecimalValue(initialAllowancePercentage);
	}
	public Double getAmortizationPercentage() {
		return amortizationPercentage;
	}
	public void setAmortizationPercentage(Double amortizationPercentage) {
		this.amortizationPercentage = UtilRoundDecimal.roundDecimalValue(amortizationPercentage);
	}
	public Double getAmortizationAmount() {
		return amortizationAmount;
	}
	public void setAmortizationAmount(Double amortizationAmount) {
		this.amortizationAmount = UtilRoundDecimal.roundDecimalValue(amortizationAmount);
	}
	public Integer getAveragingConvention() {
		return averagingConvention;
	}
	public void setAveragingConvention(Integer averagingConvention) {
		this.averagingConvention = averagingConvention;
	}
	public int getOrigLifeDays() {
		return origLifeDays;
	}
	public void setOrigLifeDays(int origLifeDays) {
		this.origLifeDays = origLifeDays;
	}
	public int getOrigLifeYears() {
		return origLifeYears;
	}
	public void setOrigLifeYears(int origLifeYears) {
		this.origLifeYears = origLifeYears;
	}
	public int getSalvageEstimate() {
		return salvageEstimate;
	}
	public void setSalvageEstimate(int salvageEstimate) {
		this.salvageEstimate = salvageEstimate;
	}
	public Double getSalvagePercentage() {
		return salvagePercentage;
	}
	public void setSalvagePercentage(Double salvagePercentage) {
		this.salvagePercentage = UtilRoundDecimal.roundDecimalValue(salvagePercentage);
	}
	public Integer getSpecialDepreciationAllowance() {
		return specialDepreciationAllowance;
	}
	public void setSpecialDepreciationAllowance(Integer specialDepreciationAllowance) {
		this.specialDepreciationAllowance = specialDepreciationAllowance;
	}
	public Double getSpecialDepreciationAllowancePercent() {
		return specialDepreciationAllowancePercent;
	}
	public void setSpecialDepreciationAllowancePercent(Double specialDepreciationAllowancePercent) {
		this.specialDepreciationAllowancePercent = UtilRoundDecimal.roundDecimalValue(specialDepreciationAllowancePercent);
	}
	public Integer getSwtch() {
		return swtch;
	}
	public void setSwtch(Integer swtch) {
		this.swtch = swtch;
	}
	public Integer getDepreciationMethodId() {
		return depreciationMethodId;
	}
	public void setDepreciationMethodId(Integer depreciationMethodId) {
		this.depreciationMethodId = depreciationMethodId;
	}
	public String getBookDescription() {
		return bookDescription;
	}
	public void setBookDescription(String bookDescription) {
		this.bookDescription = bookDescription;
	}
	public String getBookDescriptionArabic() {
		return bookDescriptionArabic;
	}
	public void setBookDescriptionArabic(String bookDescriptionArabic) {
		this.bookDescriptionArabic = bookDescriptionArabic;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getClassSetupDescription() {
		return classSetupDescription;
	}
	public void setClassSetupDescription(String classSetupDescription) {
		this.classSetupDescription = classSetupDescription;
	}
	public String getClassSetupDescriptionArabic() {
		return classSetupDescriptionArabic;
	}
	public void setClassSetupDescriptionArabic(String classSetupDescriptionArabic) {
		this.classSetupDescriptionArabic = classSetupDescriptionArabic;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getDepreciationMethod() {
		return depreciationMethod;
	}
	public void setDepreciationMethod(String depreciationMethod) {
		this.depreciationMethod = depreciationMethod;
	}
	public String getAveragingConventionType() {
		return averagingConventionType;
	}
	public void setAveragingConventionType(String averagingConventionType) {
		this.averagingConventionType = averagingConventionType;
	}

	 
	
}
