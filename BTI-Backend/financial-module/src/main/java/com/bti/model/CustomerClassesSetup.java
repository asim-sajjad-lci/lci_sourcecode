/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

import java.util.List;


/**
 * The persistent class for the ar00200 database table.
 * 
 */
@SuppressWarnings("deprecation")
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar00200")
@NamedQuery(name="CustomerClassesSetup.findAll", query="SELECT a FROM CustomerClassesSetup a")
public class CustomerClassesSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CUSTCLSID")
	private String customerClassId;

	@OneToOne
	@JoinColumn(name="BLNCTYPE")
	private MasterCustomerClassSetupBalanceType masterCustomerClassSetupBalanceType;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CLASDSCR")
	private String customerClassDescription;

	@Column(name="CLASDSCRA")
	private String customeClassDescriptionArabic;

	@OneToOne
	@JoinColumn(name="CRDLIMT")
	private MasterCustomerClassSetupCreditLimit masterCustomerClassSetupCreditLimit;

	@Column(name="CRDLIMTAMT")
	private Double creditLimitAmount;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@ManyToOne
	@JoinColumn(name="CURNCYID")
	private CurrencySetup currencySetup;

	@Column(name="CUSTPRIORITY")
	private int customerPriority;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@OneToOne
	@JoinColumn(name="FNCCHARG")
	private MasterCustomerClassSetupFinanaceCharge masterCustomerClassSetupFinanaceCharge;

	@Column(name="FNCCHARGAMT")
	private Double financeChargeAmount;

	@OneToOne
	@JoinColumn(name="MINCHARG")
	private MasterCustomerClassMinimumCharge masterCustomerClassMinimumCharge;
	
	@Column(name="MINCHARGAMT")
	private Double minimumChargeAmount;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="OPNCLNYER")
	private int openMaintenanceHistoryCalendarYear;

	@Column(name="OPNDISTR")
	private int openMaintenanceHistoryDistribution;

	@Column(name="OPNFSYER")
	private int openMaintenanceHistoryFiscalYear;

	@Column(name="OPNTRXN")
	private int openMaintenanceHistoryTransaction;

	@Column(name="PRCLEVEL")
	private String priceLevelId;

	@Column(name="TRDDISCT")
	private Double tradeDiscountPercent;

	@Column(name="USERDEFN1")
	private String userDefine1;

	@Column(name="USERDEFN2")
	private String userDefine2;

	@Column(name="USERDEFN3")
	private String userDefine3;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ar00101
	@OneToMany(mappedBy="customerClassesSetup")
	private List<CustomerMaintenance> customerMaintenances;

	//bi-directional many-to-one association to Sy03600
	@ManyToOne
	@JoinColumn(name="TAXSCHDID")
	private VATSetup vatSetup;

	//bi-directional many-to-one association to Sy03400
	@ManyToOne
	@JoinColumn(name="SHIPMTHD")
	private ShipmentMethodSetup shipmentMethodSetup;

	//bi-directional many-to-one association to Sy03300
	@ManyToOne
	@JoinColumn(name="PYMTRMID")
	private PaymentTermsSetup paymentTermsSetup;

	//bi-directional many-to-one association to Ar00104
	@ManyToOne
	@JoinColumn(name="SALSPERID")
	private SalesmanMaintenance salesmanMaintenance;

	//bi-directional many-to-one association to Ar00105
	@ManyToOne
	@JoinColumn(name="SALSTERRID")
	private SalesTerritoryMaintenance salesTerritoryMaintenance;

	//bi-directional many-to-one association to Gl00200
	@ManyToOne
	@JoinColumn(name="CHEKBOKID")
	private CheckbookMaintenance checkbookMaintenance;

	//bi-directional many-to-one association to Ar00201
	@ManyToOne
	@JoinColumn(name="ACCTABLID")
	private CustomerClassAccountTableSetup customerClassAccountTableSetup;

	//bi-directional many-to-one association to Ar00201
	@OneToMany(mappedBy="customerClassesSetup")
	private List<CustomerClassAccountTableSetup> customerClassAccountTableSetups;

	public CustomerClassesSetup() {
	}

	public String getCustomerClassId() {
		return customerClassId;
	}

	public void setCustomerClassId(String customerClassId) {
		this.customerClassId = customerClassId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public String getCustomerClassDescription() {
		return customerClassDescription;
	}

	public void setCustomerClassDescription(String customerClassDescription) {
		this.customerClassDescription = customerClassDescription;
	}

	public String getCustomeClassDescriptionArabic() {
		return customeClassDescriptionArabic;
	}

	public void setCustomeClassDescriptionArabic(String customeClassDescriptionArabic) {
		this.customeClassDescriptionArabic = customeClassDescriptionArabic;
	}

	public Double getCreditLimitAmount() {
		return creditLimitAmount;
	}

	public void setCreditLimitAmount(Double creditLimitAmount) {
		this.creditLimitAmount = creditLimitAmount;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public CurrencySetup getCurrencySetup() {
		return currencySetup;
	}

	public void setCurrencySetup(CurrencySetup currencySetup) {
		this.currencySetup = currencySetup;
	}

	public int getCustomerPriority() {
		return customerPriority;
	}

	public void setCustomerPriority(int customerPriority) {
		this.customerPriority = customerPriority;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}


	public Double getFinanceChargeAmount() {
		return financeChargeAmount;
	}

	public void setFinanceChargeAmount(Double financeChargeAmount) {
		this.financeChargeAmount = financeChargeAmount;
	}

	public Double getMinimumChargeAmount() {
		return minimumChargeAmount;
	}

	public void setMinimumChargeAmount(Double minimumChargeAmount) {
		this.minimumChargeAmount = minimumChargeAmount;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getOpenMaintenanceHistoryCalendarYear() {
		return openMaintenanceHistoryCalendarYear;
	}

	public void setOpenMaintenanceHistoryCalendarYear(int openMaintenanceHistoryCalendarYear) {
		this.openMaintenanceHistoryCalendarYear = openMaintenanceHistoryCalendarYear;
	}

	public int getOpenMaintenanceHistoryDistribution() {
		return openMaintenanceHistoryDistribution;
	}

	public void setOpenMaintenanceHistoryDistribution(int openMaintenanceHistoryDistribution) {
		this.openMaintenanceHistoryDistribution = openMaintenanceHistoryDistribution;
	}

	public int getOpenMaintenanceHistoryFiscalYear() {
		return openMaintenanceHistoryFiscalYear;
	}

	public void setOpenMaintenanceHistoryFiscalYear(int openMaintenanceHistoryFiscalYear) {
		this.openMaintenanceHistoryFiscalYear = openMaintenanceHistoryFiscalYear;
	}

	public int getOpenMaintenanceHistoryTransaction() {
		return openMaintenanceHistoryTransaction;
	}

	public void setOpenMaintenanceHistoryTransaction(int openMaintenanceHistoryTransaction) {
		this.openMaintenanceHistoryTransaction = openMaintenanceHistoryTransaction;
	}

	public String getPriceLevelId() {
		return priceLevelId;
	}

	public void setPriceLevelId(String priceLevelId) {
		this.priceLevelId = priceLevelId;
	}

	public Double getTradeDiscountPercent() {
		return tradeDiscountPercent;
	}

	public void setTradeDiscountPercent(Double tradeDiscountPercent) {
		this.tradeDiscountPercent = tradeDiscountPercent;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}


	public CheckbookMaintenance getCheckbookMaintenance() {
		return checkbookMaintenance;
	}

	public void setCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookMaintenance = checkbookMaintenance;
	}

	public List<CustomerMaintenance> getCustomerMaintenances() {
		return customerMaintenances;
	}

	public void setCustomerMaintenances(List<CustomerMaintenance> customerMaintenances) {
		this.customerMaintenances = customerMaintenances;
	}

	public VATSetup getVatSetup() {
		return vatSetup;
	}

	public void setVatSetup(VATSetup vatSetup) {
		this.vatSetup = vatSetup;
	}

	public ShipmentMethodSetup getShipmentMethodSetup() {
		return shipmentMethodSetup;
	}

	public void setShipmentMethodSetup(ShipmentMethodSetup shipmentMethodSetup) {
		this.shipmentMethodSetup = shipmentMethodSetup;
	}

	public PaymentTermsSetup getPaymentTermsSetup() {
		return paymentTermsSetup;
	}

	public void setPaymentTermsSetup(PaymentTermsSetup paymentTermsSetup) {
		this.paymentTermsSetup = paymentTermsSetup;
	}

	public SalesmanMaintenance getSalesmanMaintenance() {
		return salesmanMaintenance;
	}

	public void setSalesmanMaintenance(SalesmanMaintenance salesmanMaintenance) {
		this.salesmanMaintenance = salesmanMaintenance;
	}

	public SalesTerritoryMaintenance getSalesTerritoryMaintenance() {
		return salesTerritoryMaintenance;
	}

	public void setSalesTerritoryMaintenance(SalesTerritoryMaintenance salesTerritoryMaintenance) {
		this.salesTerritoryMaintenance = salesTerritoryMaintenance;
	}

	public CustomerClassAccountTableSetup getCustomerClassAccountTableSetup() {
		return customerClassAccountTableSetup;
	}

	public void setCustomerClassAccountTableSetup(CustomerClassAccountTableSetup customerClassAccountTableSetup) {
		this.customerClassAccountTableSetup = customerClassAccountTableSetup;
	}

	public List<CustomerClassAccountTableSetup> getCustomerClassAccountTableSetups() {
		return customerClassAccountTableSetups;
	}

	public void setCustomerClassAccountTableSetups(List<CustomerClassAccountTableSetup> customerClassAccountTableSetups) {
		this.customerClassAccountTableSetups = customerClassAccountTableSetups;
	}

	public MasterCustomerClassSetupBalanceType getMasterCustomerClassSetupBalanceType() {
		return masterCustomerClassSetupBalanceType;
	}

	public void setMasterCustomerClassSetupBalanceType(
			MasterCustomerClassSetupBalanceType masterCustomerClassSetupBalanceType) {
		this.masterCustomerClassSetupBalanceType = masterCustomerClassSetupBalanceType;
	}

	public MasterCustomerClassSetupCreditLimit getMasterCustomerClassSetupCreditLimit() {
		return masterCustomerClassSetupCreditLimit;
	}

	public void setMasterCustomerClassSetupCreditLimit(
			MasterCustomerClassSetupCreditLimit masterCustomerClassSetupCreditLimit) {
		this.masterCustomerClassSetupCreditLimit = masterCustomerClassSetupCreditLimit;
	}

	public MasterCustomerClassSetupFinanaceCharge getMasterCustomerClassSetupFinanaceCharge() {
		return masterCustomerClassSetupFinanaceCharge;
	}

	public void setMasterCustomerClassSetupFinanaceCharge(
			MasterCustomerClassSetupFinanaceCharge masterCustomerClassSetupFinanaceCharge) {
		this.masterCustomerClassSetupFinanaceCharge = masterCustomerClassSetupFinanaceCharge;
	}

	public MasterCustomerClassMinimumCharge getMasterCustomerClassMinimumCharge() {
		return masterCustomerClassMinimumCharge;
	}

	public void setMasterCustomerClassMinimumCharge(MasterCustomerClassMinimumCharge masterCustomerClassMinimumCharge) {
		this.masterCustomerClassMinimumCharge = masterCustomerClassMinimumCharge;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
}