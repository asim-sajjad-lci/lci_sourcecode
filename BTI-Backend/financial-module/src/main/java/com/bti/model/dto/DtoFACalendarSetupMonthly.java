/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FACalendarSetupMonthly;

/**
 * Description: The DtoFACalendarSetupMonthly class for FACalendarSetup 
 * Name of Project: BTI
 * Created on: May 19, 2017
 * Modified on: May 19, 2017 12:19:38 PM
 * @author seasia
 * Version: 
 */
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoFACalendarSetupMonthly {
	private int periodIndex;
	private String endDate;
	private String startDate;

	public DtoFACalendarSetupMonthly() {
		
	}
	public DtoFACalendarSetupMonthly(FACalendarSetupMonthly faCalendarSetupMonthly) {
		this.periodIndex = faCalendarSetupMonthly.getPeriodIndex();
		this.endDate = faCalendarSetupMonthly.getEndDate();
		this.startDate = faCalendarSetupMonthly.getStartDate();
	}
	public int getPeriodIndex() {
		return periodIndex;
	}
	public void setPeriodIndex(int periodIndex) {
		this.periodIndex = periodIndex;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	 
}
