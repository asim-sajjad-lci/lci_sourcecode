/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.ModulesConfiguration;

/**
 * Description: DTO Module Configuration class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: Aug 04, 2017
 * Modified on: Aug 04, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoModuleConfiguration {

	private int seriesId;
	private String seriesNamePrimary;
	private String seriesNameSecondary;
	private String originNamePrimary;
	private String originNameSecondary;
	private List<Integer> seriesIds;
	
	public DtoModuleConfiguration(){
	}
	
	public DtoModuleConfiguration(ModulesConfiguration modulesConfiguration){
		this.seriesId = modulesConfiguration.getModuleSeriesNumber();
		this.seriesNamePrimary = modulesConfiguration.getModuleDescription();
		this.seriesNameSecondary = modulesConfiguration.getModuleDescriptionArabic();
		this.originNamePrimary = modulesConfiguration.getOriginTransactionDescription();
		this.originNameSecondary = modulesConfiguration.getOriginTransactionDescriptionArabic();
	}
	
	public int getSeriesId() {
		return seriesId;
	}
	public void setSeriesId(int seriesId) {
		this.seriesId = seriesId;
	}
	public String getSeriesNamePrimary() {
		return seriesNamePrimary;
	}
	public void setSeriesNamePrimary(String seriesNamePrimary) {
		this.seriesNamePrimary = seriesNamePrimary;
	}
	public String getSeriesNameSecondary() {
		return seriesNameSecondary;
	}
	public void setSeriesNameSecondary(String seriesNameSecondary) {
		this.seriesNameSecondary = seriesNameSecondary;
	}
	public String getOriginNamePrimary() {
		return originNamePrimary;
	}
	public void setOriginNamePrimary(String originNamePrimary) {
		this.originNamePrimary = originNamePrimary;
	}
	public String getOriginNameSecondary() {
		return originNameSecondary;
	}
	public void setOriginNameSecondary(String originNameSecondary) {
		this.originNameSecondary = originNameSecondary;
	}

	public List<Integer> getSeriesIds() {
		return seriesIds;
	}

	public void setSeriesIds(List<Integer> seriesIds) {
		this.seriesIds = seriesIds;
	}
	
}
