package com.bti.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoGLPaymentVoucherDetail {

	private int id;
	
	private int paymentVoucherHeaderID;
	
	private String accountTableRowIndex;
	
	private String accountNumber;
	
	private String accountDecription;
	
	private int accountType;
	
	private int balanceType;
	
	private String interCompanyIDMultiCompanyTransaction;
	
	private Double exchangeRate;
	
	private Double debitAmount;
	
	private Double originalDebitAmount;
	
	private boolean isDeleted;
	
	private String distributionDescription;
	
	private String companyName;
	
	private Integer auditTrial;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPaymentVoucherHeaderID() {
		return paymentVoucherHeaderID;
	}

	public void setPaymentVoucherHeaderID(int paymentVoucherHeaderID) {
		this.paymentVoucherHeaderID = paymentVoucherHeaderID;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public String getInterCompanyIDMultiCompanyTransaction() {
		return interCompanyIDMultiCompanyTransaction;
	}

	public void setInterCompanyIDMultiCompanyTransaction(String interCompanyIDMultiCompanyTransaction) {
		this.interCompanyIDMultiCompanyTransaction = interCompanyIDMultiCompanyTransaction;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(Double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAccountDecription() {
		return accountDecription;
	}

	public void setAccountDecription(String accountDecription) {
		this.accountDecription = accountDecription;
	}

	public Integer getAuditTrial() {
		return auditTrial;
	}

	public void setAuditTrial(Integer auditTrial) {
		this.auditTrial = auditTrial;
	}
	
	
}
