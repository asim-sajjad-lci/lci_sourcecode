/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl40099 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40099")
@NamedQuery(name="GLSerialsConfigurationForTransactions.findAll", query="SELECT s FROM GLSerialsConfigurationForTransactions s")
public class GLSerialsConfigurationForTransactions implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
 	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="GLSCRNID")
	private int glScreenID;

	@Column(name="GLSCRNDSCR")
	private String glScreenDescription;

	@Column(name="GLSRCCODE")
	private String glSerialCode;

	@Column(name="GLSRCNUMB")
	private int glSerialNumber;
	
	@Column(name="CREATDDT")
	private Date createdDate;
	
	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private int modifyByUserID;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	  
	public GLSerialsConfigurationForTransactions() {
	}

	public int getGlScreenID() {
		return glScreenID;
	}

	public void setGlScreenID(int glScreenID) {
		this.glScreenID = glScreenID;
	}

	public String getGlScreenDescription() {
		return glScreenDescription;
	}

	public void setGlScreenDescription(String glScreenDescription) {
		this.glScreenDescription = glScreenDescription;
	}

	public String getGlSerialCode() {
		return glSerialCode;
	}

	public void setGlSerialCode(String glSerialCode) {
		this.glSerialCode = glSerialCode;
	}

	public int getGlSerialNumber() {
		return glSerialNumber;
	}

	public void setGlSerialNumber(int glSerialNumber) {
		this.glSerialNumber = glSerialNumber;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(int modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	
}