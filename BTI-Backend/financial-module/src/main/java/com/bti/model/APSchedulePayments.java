/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ap10200 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap10200")
@NamedQuery(name="APSchedulePayments.findAll", query="SELECT a FROM APSchedulePayments a")
public class APSchedulePayments implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SCHPNMBR")
	private String schedulePaymentNumber;
	
	@Column(name="DSCRPTN")
	private String scheduleDescription;
	
	@Column(name="DSCRPTNA")
	private String scheduleDescriptionArabic;

	@Column(name="ORGTRXNMR")
	private String apOriginalTransactionNumber;
	
	@Column(name="VENDORID")
	private String vendorID;

	@Column(name="SCHPDT")
	private Date schedulePaymentDate;
	
	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="SCHPAMT")
	private double schedulePaymentAmount;
	
	@Column(name="SCHPINTERAT")
	private double schedulePaymentInterestRate;
	
	@Column(name="SCHPNUM")
	private int numberOfSchedulePayments;
	
	@Column(name="PYMTFREQN")
	private int paymentFrequently;
	
	@Column(name="CALPYMTAMT")
	private double calculatePaymentAmount;
	
	@Column(name="FRSTSCHPDT")
	private Date firstSchedulePaymentDate;
	
	@Column(name="FRSTSCHPDUDT")
	private Date firstSchedulePaymentDueDate;
	
	@Column(name="ACTROWID")
	private int accountTableRowIndexAPAccount;
	
	@Column(name="ACTROWID2")
	private int accountTableRowIndexAPoffset;
	
	@Column(name="ACTROWID3")
	private int accountTableRowIndexInterestIncome;
	
	@Column(name="EXGTBLID")
	private String exchangeTableID;
	
	@Column(name="XCHGRATE")
	private double exchangeTableRate;
	  
	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="MODIFTDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	public String getSchedulePaymentNumber() {
		return schedulePaymentNumber;
	}

	public void setSchedulePaymentNumber(String schedulePaymentNumber) {
		this.schedulePaymentNumber = schedulePaymentNumber;
	}

	public String getScheduleDescription() {
		return scheduleDescription;
	}

	public void setScheduleDescription(String scheduleDescription) {
		this.scheduleDescription = scheduleDescription;
	}

	public String getScheduleDescriptionArabic() {
		return scheduleDescriptionArabic;
	}

	public void setScheduleDescriptionArabic(String scheduleDescriptionArabic) {
		this.scheduleDescriptionArabic = scheduleDescriptionArabic;
	}

	public String getApOriginalTransactionNumber() {
		return apOriginalTransactionNumber;
	}

	public void setApOriginalTransactionNumber(String apOriginalTransactionNumber) {
		this.apOriginalTransactionNumber = apOriginalTransactionNumber;
	}

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public Date getSchedulePaymentDate() {
		return schedulePaymentDate;
	}

	public void setSchedulePaymentDate(Date schedulePaymentDate) {
		this.schedulePaymentDate = schedulePaymentDate;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public double getSchedulePaymentAmount() {
		return schedulePaymentAmount;
	}

	public void setSchedulePaymentAmount(double schedulePaymentAmount) {
		this.schedulePaymentAmount = schedulePaymentAmount;
	}

	public double getSchedulePaymentInterestRate() {
		return schedulePaymentInterestRate;
	}

	public void setSchedulePaymentInterestRate(double schedulePaymentInterestRate) {
		this.schedulePaymentInterestRate = schedulePaymentInterestRate;
	}

	public int getNumberOfSchedulePayments() {
		return numberOfSchedulePayments;
	}

	public void setNumberOfSchedulePayments(int numberOfSchedulePayments) {
		this.numberOfSchedulePayments = numberOfSchedulePayments;
	}

	public int getPaymentFrequently() {
		return paymentFrequently;
	}

	public void setPaymentFrequently(int paymentFrequently) {
		this.paymentFrequently = paymentFrequently;
	}

	public double getCalculatePaymentAmount() {
		return calculatePaymentAmount;
	}

	public void setCalculatePaymentAmount(double calculatePaymentAmount) {
		this.calculatePaymentAmount = calculatePaymentAmount;
	}

	public Date getFirstSchedulePaymentDate() {
		return firstSchedulePaymentDate;
	}

	public void setFirstSchedulePaymentDate(Date firstSchedulePaymentDate) {
		this.firstSchedulePaymentDate = firstSchedulePaymentDate;
	}

	public Date getFirstSchedulePaymentDueDate() {
		return firstSchedulePaymentDueDate;
	}

	public void setFirstSchedulePaymentDueDate(Date firstSchedulePaymentDueDate) {
		this.firstSchedulePaymentDueDate = firstSchedulePaymentDueDate;
	}

	public int getAccountTableRowIndexAPAccount() {
		return accountTableRowIndexAPAccount;
	}

	public void setAccountTableRowIndexAPAccount(int accountTableRowIndexAPAccount) {
		this.accountTableRowIndexAPAccount = accountTableRowIndexAPAccount;
	}

	public int getAccountTableRowIndexAPoffset() {
		return accountTableRowIndexAPoffset;
	}

	public void setAccountTableRowIndexAPoffset(int accountTableRowIndexAPoffset) {
		this.accountTableRowIndexAPoffset = accountTableRowIndexAPoffset;
	}

	public int getAccountTableRowIndexInterestIncome() {
		return accountTableRowIndexInterestIncome;
	}

	public void setAccountTableRowIndexInterestIncome(int accountTableRowIndexInterestIncome) {
		this.accountTableRowIndexInterestIncome = accountTableRowIndexInterestIncome;
	}

	public String getExchangeTableID() {
		return exchangeTableID;
	}

	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}

	public double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}