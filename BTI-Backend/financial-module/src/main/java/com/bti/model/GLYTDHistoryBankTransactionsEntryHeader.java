/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl90500 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl90500")
@NamedQuery(name="GLYTDHistoryBankTransactionsEntryHeader.findAll", query="SELECT a FROM GLYTDHistoryBankTransactionsEntryHeader a")
public class GLYTDHistoryBankTransactionsEntryHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BNKTRXID")
	private String bankTransactionEntryID;
	
	@Column(name="BAKTRXTYPE")
	private int bankTransactionType;

	@Column(name="BNKTRXTYP")
	private int bankTransactionEntryType;

	@Column(name="BNKTRXOPT")
	private int bankTransactionEntryAction;
	
	@Column(name="BNKTRXDT")
	private Date bankTransactionDate;
	
	@Column(name="CHEKBOKID")
	private String checkbookID;
	
	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="EXGTBLID")
	private String exchangeTableID;
	
	@Column(name="CARDID")
	private String creditCardID;
		
	@Column(name="CARDNMN")
	private String creditCardNumber;

	
	
	@Column(name="CARDEXPTY")
	private int creditCardExpireYear;
	
	@Column(name="CARDEXPTM")
	private int creditCardExpireMonth;

	@Column(name="RVRNAME")
	private String receiveName;

	@Column(name="BNKDSCR")
	private String bankTransactionDescription;
	
	@Column(name="BNKAMNT")
	private Double bankTransactionAmount;
	
	@Column(name="PSTDDT")
	private Date postingDate;
	
	@Column(name="PSTURS")
	private String postingbyUserID;
	
	@Column(name="YEAR1")
	private int year;
	
	@Column(name="BATCHID")
	private String batchID;
		
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public String getBankTransactionEntryID() {
		return bankTransactionEntryID;
	}

	public void setBankTransactionEntryID(String bankTransactionEntryID) {
		this.bankTransactionEntryID = bankTransactionEntryID;
	}

	public int getBankTransactionType() {
		return bankTransactionType;
	}

	public void setBankTransactionType(int bankTransactionType) {
		this.bankTransactionType = bankTransactionType;
	}

	public int getBankTransactionEntryAction() {
		return bankTransactionEntryAction;
	}

	public void setBankTransactionEntryAction(int bankTransactionEntryAction) {
		this.bankTransactionEntryAction = bankTransactionEntryAction;
	}

	public Date getBankTransactionDate() {
		return bankTransactionDate;
	}

	public void setBankTransactionDate(Date bankTransactionDate) {
		this.bankTransactionDate = bankTransactionDate;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getExchangeTableID() {
		return exchangeTableID;
	}

	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public String getReceiveName() {
		return receiveName;
	}

	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}

	public String getBankTransactionDescription() {
		return bankTransactionDescription;
	}

	public void setBankTransactionDescription(String bankTransactionDescription) {
		this.bankTransactionDescription = bankTransactionDescription;
	}

	public Double getBankTransactionAmount() {
		return bankTransactionAmount;
	}

	public void setBankTransactionAmount(Double bankTransactionAmount) {
		this.bankTransactionAmount = bankTransactionAmount;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public String getPostingbyUserID() {
		return postingbyUserID;
	}

	public void setPostingbyUserID(String postingbyUserID) {
		this.postingbyUserID = postingbyUserID;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}

	public int getBankTransactionEntryType() {
		return bankTransactionEntryType;
	}

	public void setBankTransactionEntryType(int bankTransactionEntryType) {
		this.bankTransactionEntryType = bankTransactionEntryType;
	}
	
	
}