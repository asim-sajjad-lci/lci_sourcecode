/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;
import java.util.List;

import com.bti.model.GLBatches;
import com.bti.model.GLClearingHeader;
import com.bti.model.GLYTDHistoryTransactions;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.JournalEntryHeader;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DtoAccountCategory class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 10, 2017
 * Modified on: AUG 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoJournalEntryHeader {

	
    private int id;
    private List<Integer> ids;
	private String journalID;
	private String originalJournalID;
	private String glBatchId;
	private int transactionType;
	private String transactionDate;
	private String transactionPostedDate;
	private String transactionReversingDate;
	private int sourceDocumentId;
	private String sourceDocument;
	private String journalDescription;
	private String journalDescriptionArabic;
	private String currencyID;
	private String totalJournalEntry;
	private String totalJournalEntryDebit;
	private String totalJournalEntryCredit;
	private int exchangeTableIndex;
	private int totalNumberTransactionInBatch;
	private String totalAmountTransactionInBatch;
	private Boolean interCompany;
	private String messageType;
	List<DtoJournalEntryDetail> journalEntryDetailsList;
	private boolean save;
	private boolean post;
	private boolean correction;
	private String moduleSeriesNumber;
	private int year;
	private String exchangeRate;
	private String action;
	private int actionType;
	private int balanceType;
	private String transactionDescription;
	private int originId;
	private String moduleName;
	private String amountDifference;
	private String transactionSource;
	
	public String getAmountDifference() {
		return amountDifference;
	}

	public void setAmountDifference(String amountDifference) {
		this.amountDifference = amountDifference;
	}

	public DtoJournalEntryHeader() {
	}
	
	public DtoJournalEntryHeader(JournalEntryHeader journalEntryHeader) {
		this.id=journalEntryHeader.getId();
		this.journalID=journalEntryHeader.getJournalID();
		GLBatches glBatches = journalEntryHeader.getGlBatches();
		this.glBatchId="";
		if(glBatches!=null){
			this.glBatchId=glBatches.getBatchID();
			this.setTotalNumberTransactionInBatch(glBatches.getTotalNumberTransactionInBatch());
			this.setTotalAmountTransactionInBatch("");
			this.setTotalAmountTransactionInBatch(String.valueOf(UtilRoundDecimal.roundDecimalValue(glBatches.getTotalAmountTransactionInBatch())));
		}
		
		this.transactionType=journalEntryHeader.getTransactionType();
		this.transactionDate="";
		if(journalEntryHeader.getTransactionDate()!=null){
			this.transactionDate= UtilDateAndTime.dateToStringddmmyyyy(journalEntryHeader.getTransactionDate());
		}
		
		this.transactionReversingDate="";
		if(journalEntryHeader.getTransactionReversingDate()!=null)
		{
			this.transactionReversingDate=UtilDateAndTime.dateToStringddmmyyyy(journalEntryHeader.getTransactionReversingDate());
		}
		
		this.sourceDocumentId=0;
		this.sourceDocument="";
		this.moduleSeriesNumber="";
		if(journalEntryHeader.getGlConfigurationAuditTrialCodes()!=null){
			this.sourceDocumentId=journalEntryHeader.getGlConfigurationAuditTrialCodes().getSeriesIndex();
			if(journalEntryHeader.getGlConfigurationAuditTrialCodes().getGlConfigurationAuditTrialSeriesType()!=null){
				this.sourceDocument=journalEntryHeader.getGlConfigurationAuditTrialCodes().getSourceDocument();
			    this.moduleSeriesNumber= journalEntryHeader.getGlConfigurationAuditTrialCodes().getGlConfigurationAuditTrialSeriesType().getTypePrimary();
			}
		}
		
		this.journalDescription="";
		if(UtilRandomKey.isNotBlank(journalEntryHeader.getJournalDescription())){
			this.journalDescription=journalEntryHeader.getJournalDescription();
		}
		
		this.journalDescriptionArabic="";
		if(UtilRandomKey.isNotBlank(journalEntryHeader.getJournalDescriptionArabic())){
			this.journalDescriptionArabic=journalEntryHeader.getJournalDescriptionArabic();
		}
		
		this.currencyID="";
		if(journalEntryHeader.getCurrencySetup()!=null){
			this.currencyID=journalEntryHeader.getCurrencySetup().getCurrencyId();
		}
		this.totalJournalEntryCredit=String.valueOf(UtilRoundDecimal.roundDecimalValue(journalEntryHeader.getOriginalTotalJournalEntryCredit()));
		this.totalJournalEntryDebit=String.valueOf(UtilRoundDecimal.roundDecimalValue(journalEntryHeader.getOriginalTotalJournalEntryDebit()));
		this.amountDifference = String.valueOf(UtilRoundDecimal.roundDecimalValue(journalEntryHeader.getOriginalTotalJournalEntryDebit()-journalEntryHeader.getOriginalTotalJournalEntryCredit()));
		this.exchangeTableIndex=0;
		this.exchangeRate=String.valueOf(UtilRoundDecimal.roundDecimalValue(journalEntryHeader.getExchangeTableRate()));
		if(journalEntryHeader.getCurrencyExchangeHeader()!=null){
		  this.exchangeTableIndex=journalEntryHeader.getCurrencyExchangeHeader().getExchangeIndex();
		}
		this.originalJournalID="";
		if(UtilRandomKey.isNotBlank(journalEntryHeader.getOriginalJournalEntryID())){
			this.originalJournalID=journalEntryHeader.getOriginalJournalEntryID();
		}
	}
	
	public DtoJournalEntryHeader(GLYTDOpenTransactions glytdOpenTransactions) 
	{
		this.journalID=glytdOpenTransactions.getJournalEntryID();
		this.glBatchId="";
		this.transactionType=glytdOpenTransactions.getTransactionType();
		this.transactionDate="";
		if(glytdOpenTransactions.getTransactionDate()!=null){
			this.transactionDate= UtilDateAndTime.dateToStringddmmyyyy(glytdOpenTransactions.getTransactionDate());
		}
		
		this.transactionReversingDate="";
		if(glytdOpenTransactions.getTransactionReversingDate()!=null)
		{
			this.transactionReversingDate=UtilDateAndTime.dateToStringddmmyyyy(glytdOpenTransactions.getTransactionReversingDate());
		}
		
		this.sourceDocumentId=0;
		this.sourceDocument="";
		this.moduleSeriesNumber="";
		if(glytdOpenTransactions.getGlConfigurationAuditTrialCodes()!=null){
			this.sourceDocumentId=glytdOpenTransactions.getGlConfigurationAuditTrialCodes().getSeriesIndex();
			if(glytdOpenTransactions.getGlConfigurationAuditTrialCodes().getGlConfigurationAuditTrialSeriesType()!=null){
				this.sourceDocument=glytdOpenTransactions.getGlConfigurationAuditTrialCodes().getSourceDocument();
			    this.moduleSeriesNumber= glytdOpenTransactions.getGlConfigurationAuditTrialCodes().getGlConfigurationAuditTrialSeriesType().getTypePrimary();
			}
		}
		
		this.journalDescription="";
		if(UtilRandomKey.isNotBlank(glytdOpenTransactions.getJournalDescription())){
			this.journalDescription=glytdOpenTransactions.getJournalDescription();
		}
		
		this.journalDescriptionArabic="";
		if(UtilRandomKey.isNotBlank(glytdOpenTransactions.getJournalDescriptionArabic())){
			this.journalDescriptionArabic=glytdOpenTransactions.getJournalDescriptionArabic();
		}
		
		this.currencyID="";
		if(UtilRandomKey.isNotBlank(glytdOpenTransactions.getCurrencyId())){
			this.currencyID=glytdOpenTransactions.getCurrencyId();
		}
		this.totalJournalEntryCredit=String.valueOf(UtilRoundDecimal.roundDecimalValue(glytdOpenTransactions.getOriginalTotalJournalEntryCredit()));
		this.totalJournalEntryDebit=String.valueOf(UtilRoundDecimal.roundDecimalValue(glytdOpenTransactions.getOriginalTotalJournalEntryDebit()));
		this.exchangeTableIndex=glytdOpenTransactions.getExchangeTableIndex();
		this.exchangeRate=String.valueOf(UtilRoundDecimal.roundDecimalValue(glytdOpenTransactions.getExchangeTableRate()));
		this.originalJournalID="";
		if(UtilRandomKey.isNotBlank(glytdOpenTransactions.getOriginalJournalEntryID())){
			this.originalJournalID=glytdOpenTransactions.getOriginalJournalEntryID();
		}
	}
	
	public DtoJournalEntryHeader(GLYTDHistoryTransactions glytdHistoryTransactions) 
	{
		this.journalID=glytdHistoryTransactions.getJournalEntryID();
		this.glBatchId="";
		this.transactionType=glytdHistoryTransactions.getTransactionType();
		this.transactionDate="";
		if(glytdHistoryTransactions.getTransactionDate()!=null){
			this.transactionDate= UtilDateAndTime.dateToStringddmmyyyy(glytdHistoryTransactions.getTransactionDate());
		}
		
		this.transactionReversingDate="";
		if(glytdHistoryTransactions.getTransactionReversingDate()!=null)
		{
			this.transactionReversingDate=UtilDateAndTime.dateToStringddmmyyyy(glytdHistoryTransactions.getTransactionReversingDate());
		}
		
		this.sourceDocumentId=0;
		this.sourceDocument="";
		this.moduleSeriesNumber="";
		if(glytdHistoryTransactions.getGlConfigurationAuditTrialCodes()!=null){
			this.sourceDocumentId=glytdHistoryTransactions.getGlConfigurationAuditTrialCodes().getSeriesIndex();
			if(glytdHistoryTransactions.getGlConfigurationAuditTrialCodes().getGlConfigurationAuditTrialSeriesType()!=null){
				this.sourceDocument=glytdHistoryTransactions.getGlConfigurationAuditTrialCodes().getGlConfigurationAuditTrialSeriesType().getTypePrimary();
			    this.moduleSeriesNumber= glytdHistoryTransactions.getGlConfigurationAuditTrialCodes().getGlConfigurationAuditTrialSeriesType().getTypePrimary();
			}
		}
		
		this.journalDescription="";
		if(UtilRandomKey.isNotBlank(glytdHistoryTransactions.getJournalDescription())){
			this.journalDescription=glytdHistoryTransactions.getJournalDescription();
		}
		
		this.journalDescriptionArabic="";
		if(UtilRandomKey.isNotBlank(glytdHistoryTransactions.getJournalDescriptionArabic())){
			this.journalDescriptionArabic=glytdHistoryTransactions.getJournalDescriptionArabic();
		}
		
		this.currencyID="";
		if(glytdHistoryTransactions.getCurrencySetup()!=null){
			this.currencyID=glytdHistoryTransactions.getCurrencySetup().getCurrencyId();
		}
		
		this.totalJournalEntryCredit="";
		this.totalJournalEntryDebit="";
		if(glytdHistoryTransactions.getOriginalTotalJournalEntryCredit()!=null){
			this.totalJournalEntryCredit=String.valueOf(glytdHistoryTransactions.getOriginalTotalJournalEntryCredit());
		}
		if(glytdHistoryTransactions.getOriginalTotalJournalEntryDebit()!=null){
			this.totalJournalEntryDebit=String.valueOf(glytdHistoryTransactions.getOriginalTotalJournalEntryDebit());
		}
		
		this.exchangeTableIndex=glytdHistoryTransactions.getExchangeTableIndex();
		this.exchangeRate="";
		if(glytdHistoryTransactions.getExchangeTableRate()!=null){
			this.exchangeRate=String.valueOf(glytdHistoryTransactions.getExchangeTableRate());
		}
	}
	
	public DtoJournalEntryHeader(GLClearingHeader glClearingHeader) 
	{
		this.journalID=glClearingHeader.getJournalID();
		this.balanceType=glClearingHeader.getBalanceType();
		this.transactionDate="";
		if(glClearingHeader.getTransactionDate()!=null){
			this.transactionDate=UtilDateAndTime.dateToStringddmmyyyy(glClearingHeader.getTransactionDate());
		}
		GLBatches glBatches = glClearingHeader.getGlBatches();
		this.glBatchId="";
		if(glBatches!=null){
			this.glBatchId=glBatches.getBatchID();
			this.setTotalNumberTransactionInBatch(glBatches.getTotalNumberTransactionInBatch());
			this.setTotalAmountTransactionInBatch("");
			if(glBatches.getTotalAmountTransactionInBatch()!=null){
				this.setTotalAmountTransactionInBatch(String.valueOf(glBatches.getTotalAmountTransactionInBatch()));
			}
		}
		this.transactionDescription="";
		if(UtilRandomKey.isNotBlank(glClearingHeader.getTransactionDescription())){
			this.transactionDescription=glClearingHeader.getTransactionDescription();
		}
		this.sourceDocumentId=0;
		if(glClearingHeader.getGlConfigurationAuditTrialCodes()!=null){
			this.sourceDocumentId=glClearingHeader.getGlConfigurationAuditTrialCodes().getSeriesIndex();
		}
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getJournalID() {
		return journalID;
	}
	
	public void setJournalID(String journalID) {
		this.journalID = journalID;
	}
	
	public String getGlBatchId() {
		return glBatchId;
	}

	public void setGlBatchId(String glBatchId) {
		this.glBatchId = glBatchId;
	}

	public int getTransactionType() {
		return transactionType;
	}
	
	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}
	
	public String getTransactionDate() {
		return transactionDate;
	}
	
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	public String getTransactionReversingDate() {
		return transactionReversingDate;
	}
	
	public void setTransactionReversingDate(String transactionReversingDate) {
		this.transactionReversingDate = transactionReversingDate;
	}
	
	public int getSourceDocumentId() {
		return sourceDocumentId;
	}
	
	public void setSourceDocumentId(int sourceDocumentId) {
		this.sourceDocumentId = sourceDocumentId;
	}
	
	public String getJournalDescription() {
		return journalDescription;
	}
	
	public void setJournalDescription(String journalDescription) {
		this.journalDescription = journalDescription;
	}
	
	public String getJournalDescriptionArabic() {
		return journalDescriptionArabic;
	}
	
	public void setJournalDescriptionArabic(String journalDescriptionArabic) {
		this.journalDescriptionArabic = journalDescriptionArabic;
	}
	
	public String getCurrencyID() {
		return currencyID;
	}
	
	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}
	
	public String getTotalJournalEntry() {
		return totalJournalEntry;
	}
	
	public void setTotalJournalEntry(String totalJournalEntry) {
		this.totalJournalEntry = totalJournalEntry;
	}
	
	public String getTotalJournalEntryDebit() {
		return totalJournalEntryDebit;
	}
	
	public void setTotalJournalEntryDebit(String totalJournalEntryDebit) {
		this.totalJournalEntryDebit = totalJournalEntryDebit;
	}
	
	public String getTotalJournalEntryCredit() {
		return totalJournalEntryCredit;
	}
	
	public void setTotalJournalEntryCredit(String totalJournalEntryCredit) {
		this.totalJournalEntryCredit = totalJournalEntryCredit;
	}
	
	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}
	
	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public String getSourceDocument() {
		return sourceDocument;
	}

	public void setSourceDocument(String sourceDocument) {
		this.sourceDocument = sourceDocument;
	}

	public int getTotalNumberTransactionInBatch() {
		return totalNumberTransactionInBatch;
	}

	public void setTotalNumberTransactionInBatch(int totalNumberTransactionInBatch) {
		this.totalNumberTransactionInBatch = totalNumberTransactionInBatch;
	}

	public String getTotalAmountTransactionInBatch() {
		return totalAmountTransactionInBatch;
	}

	public void setTotalAmountTransactionInBatch(String totalAmountTransactionInBatch) {
		this.totalAmountTransactionInBatch = totalAmountTransactionInBatch;
	}

	public Boolean getInterCompany() {
		return interCompany;
	}

	public void setInterCompany(Boolean interCompany) {
		this.interCompany = interCompany;
	}

	public List<DtoJournalEntryDetail> getJournalEntryDetailsList() {
		return journalEntryDetailsList;
	}

	public void setJournalEntryDetailsList(List<DtoJournalEntryDetail> journalEntryDetailsList) {
		this.journalEntryDetailsList = journalEntryDetailsList;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
    
	

	public boolean isSave() {
		return save;
	}

	public void setSave(boolean save) {
		this.save = save;
	}

	public boolean isPost() {
		return post;
	}

	public void setPost(boolean post) {
		this.post = post;
	}

	public boolean isCorrection() {
		return correction;
	}

	public void setCorrection(boolean correction) {
		this.correction = correction;
	}

	public String getModuleSeriesNumber() {
		return moduleSeriesNumber;
	}

	public void setModuleSeriesNumber(String moduleSeriesNumber) {
		this.moduleSeriesNumber = moduleSeriesNumber;
	}

	public String getOriginalJournalID() {
		return originalJournalID;
	}

	public void setOriginalJournalID(String originalJournalID) {
		this.originalJournalID = originalJournalID;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getActionType() {
		return actionType;
	}

	public void setActionType(int actionType) {
		this.actionType = actionType;
	}

	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public String getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public int getOriginId() {
		return originId;
	}

	public void setOriginId(int originId) {
		this.originId = originId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public String getTransactionSource() {
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource) {
		this.transactionSource = transactionSource;
	}

	public String getTransactionPostedDate() {
		return transactionPostedDate;
	}

	public void setTransactionPostedDate(String transactionPostedDate) {
		this.transactionPostedDate = transactionPostedDate;
	}


	
	
	
	
}
