/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the fa00103 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa00103")
@NamedQuery(name="FABookMaintenance.findAll", query="SELECT f FROM FABookMaintenance f")
public class FABookMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ASSTID")
	private String assetId;

	/*@Column(name="AMORCODE")
	private int amortizationCode;*/
	
	@ManyToOne
	@JoinColumn(name="AMORCODE")
	private FABookMaintenanceAmortizationCodeType amortizationCodeType;


	@Column(name="AMORRAMT")
	private double amortizationAmount;

	/*@Column(name="AVERGCONV")
	private int averagingConvention;*/
	
	@ManyToOne
	@JoinColumn(name="AVERGCONV")
	private FABookMaintenanceAveragingConventionType averagingConventionType;

	@Column(name="BGNYRCOST")
	private double beginYearCost;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

/*	@Column(name="BOKID")
	private String bookId;*/
	
	@OneToOne
	@JoinColumn(name="BOKID")
	private BookSetup bookSetup;

	public BookSetup getBookSetup() {
		return bookSetup;
	}

	public void setBookSetup(BookSetup bookSetup) {
		this.bookSetup = bookSetup;
	}

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="CSTBAISC")
	private double costBasis;

	@Column(name="CURRTDEPC")
	private double currentDepreciation;

	@Column(name="DEPRCTDT")
	private Date depreciatedDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="STATUS")
	private String staus;
	
	
	public String getStaus() {
		return staus;
	}

	public void setStaus(String staus) {
		this.staus = staus;
	}

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="FULLDEPCFLG")
	private String fullDepreciationFlag;

	@Column(name="LTDDECP")
	private double lifeToDateDepreciation;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="NETVALUE")
	private double netFAValue;

	@Column(name="ORIGNLIFDY")
	private int originalLifeDay;

	@Column(name="ORIGNLIFYR")
	private int originalLifeYear;

	@Column(name="PLCINSERVC")
	private Date placedInServiceDate;

	@Column(name="REMLIFDY")
	private int remainingLifeDay;

	@Column(name="REMLIFYR")
	private int remainingLifeYear;

	@Column(name="SALVAAMT")
	private double salvageAmount;

	/*@Column(name="SWITOVER")
	private int switchOver;*/
	
	@ManyToOne
	@JoinColumn(name="SWITOVER")
	private FABookMaintenanceSwitchOverType switchOverType;
	

	@Column(name="YRDEPCRATE")
	private double yearlyDepreciatedRate;

	@Column(name="YTDDECP")
	private double yearToDateDepreciation;

	//bi-directional one-to-one association to Fa00101
	@OneToOne
	@JoinColumn(name="ASSTID")
	private FAGeneralMaintenance faGeneralMaintenance;

	//bi-directional many-to-one association to Fa40055
	@ManyToOne
	@JoinColumn(name="DEPCMETHD")
	private FADepreciationMethods faDepreciationMethods;
	
	
	
	 

	public FABookMaintenance() {
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	
	

	/*public int getAmortizationCode() {
		return amortizationCode;
	}

	public void setAmortizationCode(int amortizationCode) {
		this.amortizationCode = amortizationCode;
	}*/
	
	public FABookMaintenanceAmortizationCodeType getAmortizationCodeType() {
		return amortizationCodeType;
	}

	public void setAmortizationCodeType(FABookMaintenanceAmortizationCodeType amortizationCodeType) {
		this.amortizationCodeType = amortizationCodeType;
	}

	public double getAmortizationAmount() {
		return amortizationAmount;
	}

	public void setAmortizationAmount(double amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	/*public int getAveragingConvention() {
		return averagingConvention;
	}

	public void setAveragingConvention(int averagingConvention) {
		this.averagingConvention = averagingConvention;
	}*/

	public double getBeginYearCost() {
		return beginYearCost;
	}

	public void setBeginYearCost(double beginYearCost) {
		this.beginYearCost = beginYearCost;
	}

/*	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}*/


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public double getCostBasis() {
		return costBasis;
	}

	public void setCostBasis(double costBasis) {
		this.costBasis = costBasis;
	}

	public double getCurrentDepreciation() {
		return currentDepreciation;
	}

	public void setCurrentDepreciation(double currentDepreciation) {
		this.currentDepreciation = currentDepreciation;
	}

	public Date getDepreciatedDate() {
		return depreciatedDate;
	}

	public void setDepreciatedDate(Date depreciatedDate) {
		this.depreciatedDate = depreciatedDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getFullDepreciationFlag() {
		return fullDepreciationFlag;
	}

	public void setFullDepreciationFlag(String fullDepreciationFlag) {
		this.fullDepreciationFlag = fullDepreciationFlag;
	}

	public double getLifeToDateDepreciation() {
		return lifeToDateDepreciation;
	}

	public void setLifeToDateDepreciation(double lifeToDateDepreciation) {
		this.lifeToDateDepreciation = lifeToDateDepreciation;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public double getNetFAValue() {
		return netFAValue;
	}

	public void setNetFAValue(double netFAValue) {
		this.netFAValue = netFAValue;
	}

	public int getOriginalLifeDay() {
		return originalLifeDay;
	}

	public void setOriginalLifeDay(int originalLifeDay) {
		this.originalLifeDay = originalLifeDay;
	}

	public int getOriginalLifeYear() {
		return originalLifeYear;
	}

	public void setOriginalLifeYear(int originalLifeYear) {
		this.originalLifeYear = originalLifeYear;
	}

	public Date getPlacedInServiceDate() {
		return placedInServiceDate;
	}

	public void setPlacedInServiceDate(Date placedInServiceDate) {
		this.placedInServiceDate = placedInServiceDate;
	}

	public int getRemainingLifeDay() {
		return remainingLifeDay;
	}

	public void setRemainingLifeDay(int remainingLifeDay) {
		this.remainingLifeDay = remainingLifeDay;
	}

	public int getRemainingLifeYear() {
		return remainingLifeYear;
	}

	public void setRemainingLifeYear(int remainingLifeYear) {
		this.remainingLifeYear = remainingLifeYear;
	}

	public double getSalvageAmount() {
		return salvageAmount;
	}

	public void setSalvageAmount(double salvageAmount) {
		this.salvageAmount = salvageAmount;
	}

	
	public FABookMaintenanceSwitchOverType getSwitchOverType() {
		return switchOverType;
	}

	public void setSwitchOverType(FABookMaintenanceSwitchOverType switchOverType) {
		this.switchOverType = switchOverType;
	}
/*
	public int getSwitchOver() {
		return switchOver;
	}

	public void setSwitchOver(int switchOver) {
		this.switchOver = switchOver;
	}*/

	public double getYearlyDepreciatedRate() {
		return yearlyDepreciatedRate;
	}

	public void setYearlyDepreciatedRate(double yearlyDepreciatedRate) {
		this.yearlyDepreciatedRate = yearlyDepreciatedRate;
	}

	public double getYearToDateDepreciation() {
		return yearToDateDepreciation;
	}

	public void setYearToDateDepreciation(double yearToDateDepreciation) {
		this.yearToDateDepreciation = yearToDateDepreciation;
	}

	public FAGeneralMaintenance getFaGeneralMaintenance() {
		return faGeneralMaintenance;
	}

	public void setFaGeneralMaintenance(FAGeneralMaintenance faGeneralMaintenance) {
		this.faGeneralMaintenance = faGeneralMaintenance;
	}

	public FADepreciationMethods getFaDepreciationMethods() {
		return faDepreciationMethods;
	}

	public void setFaDepreciationMethods(FADepreciationMethods faDepreciationMethods) {
		this.faDepreciationMethods = faDepreciationMethods;
	}

	public FABookMaintenanceAveragingConventionType getAveragingConventionType() {
		return averagingConventionType;
	}

	public void setAveragingConventionType(FABookMaintenanceAveragingConventionType averagingConventionType) {
		this.averagingConventionType = averagingConventionType;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}