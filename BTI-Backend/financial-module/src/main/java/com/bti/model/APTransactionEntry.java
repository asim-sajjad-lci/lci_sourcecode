/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ap10100 database table.
 * 
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap10100")
@NamedQuery(name="APTransactionEntry.findAll", query="SELECT a FROM APTransactionEntry a")
public class APTransactionEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APTRXNO")
	private String apTransactionNumber;
	
	/*@Column(name="APTRXTYP")
	private int apTransactionType;*/
	
	@ManyToOne
	@JoinColumn(name="APTRXTYP")
	private PMDocumentsTypeSetup transactionType;
	
	@Column(name="DSCRPTN")
	private String apTransactionDescription;

	@ManyToOne
	@JoinColumn(name="BATCHID")
	private APBatches apBatches;


	@Column(name="APTRXDT")
	private Date apTransactionDate;
	
	@Column(name="VENDORID")
	private String vendorID;

	@Column(name="VENDNAME")
	private String vendorName;
	
	@Column(name="VENDNAMEA")
	private String vendorNameArabic;
	
	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="DOCNMBR")
	private String documentNumberOfVendor;
	
	@Column(name="APPURCHAMT")
	private double purchasesAmount;
	
	@Column(name="APTRDAMT")
	private double apTransactionTradeDiscount;
	
	@Column(name="APFRGAMT")
	private double apTransactionFreightAmount;
	
	@Column(name="APMISCAMT")
	private double apTransactionMiscellaneous;
	
	@Column(name="APVATAMT")
	private double apTransactionVATAmount;
	
	@Column(name="APFRCAMT")
	private double apTransactionFinanceChargeAmount;
	
	@Column(name="APCRAMT")
	private double apTransactionCreditMemoAmount;
	
	@Column(name="APTOTAMT")
	private double apTransactionTotalAmount;
	
	@Column(name="APCSHAMT")
	private double apTransactionCashAmount;
	
	@Column(name="APCHKAMT")
	private double apTransactionCheckAmount;
	
	@Column(name="APCRDAMT")
	private double apTransactionCreditCardAmount;
	
	@Column(name="SHIPMTHD")
	private String shippingMethod;
	
	@Column(name="CHEKBOKID")
	private String checkbookID;
	
	@Column(name="CSHRECNO")
	private String cashReceiptNumber;
	
	@Column(name="CHKNUMBR")
	private String checkNumber;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	 
	@Column(name="CARDEXPTY")
	private String creditCardExpireYear;
	
	@Column(name="CARDEXPTM")
	private String creditCardExpireMonth;
 
	@ManyToOne
	@JoinColumn(name="TRXSTAT")
	private MasterAPTransactionStatus apTransactionStatus;
	
	@Column(name="EXGTBLID")
	private String exchangeTableIndex;
	
	@Column(name="XCHGRATE")
	private double exchangeTableRate;
	
	@Column(name="TRXVOID")
	private boolean transactionVoid;
	
	@Column(name="TAXSCHDID")
	private String vatScheduleID;
	
	@Column(name="PYMTRMID")
	private String paymentTermsID;
	 
	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="PAYM_TYP_ID")
	private String paymentTypeId;
	
	@Column(name="PAYM_AMNT")
	private double paymentAmount;
	
	@Column(name="IS_PAYM")
	private boolean payment;
	
	@Column(name="ARSERREPAMT")
	private double apServiceRepairAmount;
	
	@Column(name="ARRETNAMT")
	private double apReturnAmount;
	
	@Column(name="ARWARAMT")
	private double apTransactionWarrantyAmount;
	
	@Column(name="ARDBAMT")
	private double apTransactionDebitMemoAmount;

	public String getApTransactionNumber() {
		return apTransactionNumber;
	}

	public void setApTransactionNumber(String apTransactionNumber) {
		this.apTransactionNumber = apTransactionNumber;
	}

	public PMDocumentsTypeSetup getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(PMDocumentsTypeSetup transactionType) {
		this.transactionType = transactionType;
	}

	public String getApTransactionDescription() {
		return apTransactionDescription;
	}

	public void setApTransactionDescription(String apTransactionDescription) {
		this.apTransactionDescription = apTransactionDescription;
	}

	public APBatches getApBatches() {
		return apBatches;
	}

	public void setApBatches(APBatches apBatches) {
		this.apBatches = apBatches;
	}

	public Date getApTransactionDate() {
		return apTransactionDate;
	}

	public void setApTransactionDate(Date apTransactionDate) {
		this.apTransactionDate = apTransactionDate;
	}

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorNameArabic() {
		return vendorNameArabic;
	}

	public void setVendorNameArabic(String vendorNameArabic) {
		this.vendorNameArabic = vendorNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getDocumentNumberOfVendor() {
		return documentNumberOfVendor;
	}

	public void setDocumentNumberOfVendor(String documentNumberOfVendor) {
		this.documentNumberOfVendor = documentNumberOfVendor;
	}

	public double getPurchasesAmount() {
		return purchasesAmount;
	}

	public void setPurchasesAmount(double purchasesAmount) {
		this.purchasesAmount = purchasesAmount;
	}

	public double getApTransactionTradeDiscount() {
		return apTransactionTradeDiscount;
	}

	public void setApTransactionTradeDiscount(double apTransactionTradeDiscount) {
		this.apTransactionTradeDiscount = apTransactionTradeDiscount;
	}

	public double getApTransactionFreightAmount() {
		return apTransactionFreightAmount;
	}

	public void setApTransactionFreightAmount(double apTransactionFreightAmount) {
		this.apTransactionFreightAmount = apTransactionFreightAmount;
	}

	public double getApTransactionMiscellaneous() {
		return apTransactionMiscellaneous;
	}

	public void setApTransactionMiscellaneous(double apTransactionMiscellaneous) {
		this.apTransactionMiscellaneous = apTransactionMiscellaneous;
	}

	public double getApTransactionVATAmount() {
		return apTransactionVATAmount;
	}

	public void setApTransactionVATAmount(double apTransactionVATAmount) {
		this.apTransactionVATAmount = apTransactionVATAmount;
	}

	public double getApTransactionFinanceChargeAmount() {
		return apTransactionFinanceChargeAmount;
	}

	public void setApTransactionFinanceChargeAmount(double apTransactionFinanceChargeAmount) {
		this.apTransactionFinanceChargeAmount = apTransactionFinanceChargeAmount;
	}

	public double getApTransactionCreditMemoAmount() {
		return apTransactionCreditMemoAmount;
	}

	public void setApTransactionCreditMemoAmount(double apTransactionCreditMemoAmount) {
		this.apTransactionCreditMemoAmount = apTransactionCreditMemoAmount;
	}

	public double getApTransactionTotalAmount() {
		return apTransactionTotalAmount;
	}

	public void setApTransactionTotalAmount(double apTransactionTotalAmount) {
		this.apTransactionTotalAmount = apTransactionTotalAmount;
	}

	public double getApTransactionCashAmount() {
		return apTransactionCashAmount;
	}

	public void setApTransactionCashAmount(double apTransactionCashAmount) {
		this.apTransactionCashAmount = apTransactionCashAmount;
	}

	public double getApTransactionCheckAmount() {
		return apTransactionCheckAmount;
	}

	public void setApTransactionCheckAmount(double apTransactionCheckAmount) {
		this.apTransactionCheckAmount = apTransactionCheckAmount;
	}

	public double getApTransactionCreditCardAmount() {
		return apTransactionCreditCardAmount;
	}

	public void setApTransactionCreditCardAmount(double apTransactionCreditCardAmount) {
		this.apTransactionCreditCardAmount = apTransactionCreditCardAmount;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(String creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(String creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public MasterAPTransactionStatus getApTransactionStatus() {
		return apTransactionStatus;
	}

	public void setApTransactionStatus(MasterAPTransactionStatus apTransactionStatus) {
		this.apTransactionStatus = apTransactionStatus;
	}

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public boolean isTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(boolean transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getVatScheduleID() {
		return vatScheduleID;
	}

	public void setVatScheduleID(String vatScheduleID) {
		this.vatScheduleID = vatScheduleID;
	}

	public String getPaymentTermsID() {
		return paymentTermsID;
	}

	public void setPaymentTermsID(String paymentTermsID) {
		this.paymentTermsID = paymentTermsID;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public boolean isPayment() {
		return payment;
	}

	public void setPayment(boolean payment) {
		this.payment = payment;
	}

	public double getApServiceRepairAmount() {
		return apServiceRepairAmount;
	}

	public void setApServiceRepairAmount(double apServiceRepairAmount) {
		this.apServiceRepairAmount = apServiceRepairAmount;
	}

	public double getApReturnAmount() {
		return apReturnAmount;
	}

	public void setApReturnAmount(double apReturnAmount) {
		this.apReturnAmount = apReturnAmount;
	}

	public double getApTransactionWarrantyAmount() {
		return apTransactionWarrantyAmount;
	}

	public void setApTransactionWarrantyAmount(double apTransactionWarrantyAmount) {
		this.apTransactionWarrantyAmount = apTransactionWarrantyAmount;
	}

	public double getApTransactionDebitMemoAmount() {
		return apTransactionDebitMemoAmount;
	}

	public void setApTransactionDebitMemoAmount(double apTransactionDebitMemoAmount) {
		this.apTransactionDebitMemoAmount = apTransactionDebitMemoAmount;
	}
	
	
	
	 
}