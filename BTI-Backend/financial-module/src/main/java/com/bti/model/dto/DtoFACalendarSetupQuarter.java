/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FACalendarSetupQuarter;

/**
 * Description: The DtoFACalendarSetupMonthly class for FACalendarSetup 
 * Name of Project: BTI
 * Created on: May 19, 2017
 * Modified on: May 19, 2017 12:19:38 PM
 * @author seasia
 * Version: 
 */
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoFACalendarSetupQuarter {
	private int quarterIndex;
	private String endDate;
	private String startDate;
	private String midDate;

	public DtoFACalendarSetupQuarter() {
		
	}
	public DtoFACalendarSetupQuarter(FACalendarSetupQuarter fsCalendarSetupQuarter) {
		this.quarterIndex = fsCalendarSetupQuarter.getQuarterIndex();
		this.endDate = fsCalendarSetupQuarter.getEndDate();
		this.startDate = fsCalendarSetupQuarter.getStartDate();
		this.midDate = fsCalendarSetupQuarter.getMidDate();
	}
 
	
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getMidDate() {
		return midDate;
	}
	public void setMidDate(String midDate) {
		this.midDate = midDate;
	}
	public int getQuarterIndex() {
		return quarterIndex;
	}
	public void setQuarterIndex(int quarterIndex) {
		this.quarterIndex = quarterIndex;
	}

	 
}
