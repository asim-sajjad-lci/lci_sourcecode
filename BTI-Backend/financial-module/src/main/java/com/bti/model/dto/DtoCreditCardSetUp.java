/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.CreditCardType;
import com.bti.model.CreditCardsSetup;


public class DtoCreditCardSetUp {

	private String cardId;
	private Integer cardIndex;
	private String creditCardName;
	private String creditCardNameArabic;
	private String checkBookId;
	private String accountNumber;
	private Integer cardType;
	private String accountTableRowIndex;
	private List<String> cardIds;
 

	public DtoCreditCardSetUp() {
		
	}

	public DtoCreditCardSetUp(CreditCardsSetup creditCardsSetup, CreditCardType creditCardType) {
		this.cardIndex = creditCardsSetup.getCardIndx();
		this.cardId = creditCardsSetup.getCardId();
		this.creditCardName = creditCardsSetup.getCreditCardName();
		this.creditCardNameArabic = creditCardsSetup.getCreditCardNameArabic();
		this.checkBookId = creditCardsSetup.getCheckBookIdBank();
		this.cardType = creditCardType!=null?creditCardType.getTypeId():null;
		this.accountTableRowIndex="";
		if(creditCardsSetup.getGlAccountsTableAccumulation()!=null){
			this.accountTableRowIndex=creditCardsSetup.getGlAccountsTableAccumulation().getAccountTableRowIndex();
		}
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getCreditCardName() {
		return creditCardName;
	}

	public void setCreditCardName(String creditCardName) {
		this.creditCardName = creditCardName;
	}

	public String getCreditCardNameArabic() {
		return creditCardNameArabic;
	}

	public void setCreditCardNameArabic(String creditCardNameArabic) {
		this.creditCardNameArabic = creditCardNameArabic;
	}

	public String getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getCardType() {
		return cardType;
	}

	public void setCardType(Integer cardType) {
		this.cardType = cardType;
	}

	public Integer getCardIndex() {
		return cardIndex;
	}

	public void setCardIndex(Integer cardIndex) {
		this.cardIndex = cardIndex;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public List<String> getCardIds() {
		return cardIds;
	}

	public void setCardIds(List<String> cardIds) {
		this.cardIds = cardIds;
	}

	
}
