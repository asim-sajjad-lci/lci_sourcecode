/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.bti.model.VATBasedOnType;
import com.bti.model.VATSetup;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoVatDetailSetup {
	private String vatScheduleId;
	private String vatDescription;
	private String vatDescriptionArabic;
	private int vatSeriesType;
	private String vatIdNumber;
	private Integer vatBaseOn;
	private Double basperct;
	private Double maximumVATAmount;
	private Double minimumVATAmount;
	private int accountRowId;
	private Double lastYearTotalSalesPurchase;
	private Double lastYearSalesPurchaseTaxes;
	private Double lastYearTaxableSalesPurchase;
	private Double ytdTotalSalesPurchase;
	private Double ytdTotalSalesPurchaseTaxes;
	private Double ytdTotalTaxableSalesPurchase;
	private Double totalApplyAmount;
	private Double totalApplyAmountYtd;
	private String messageType;
	private List<String> vatScheduleIds;
	public DtoVatDetailSetup() {
		
	}
	public DtoVatDetailSetup(VATSetup vatSetup, VATBasedOnType vatBasedOnType) {
		this.setAccountRowId(vatSetup.getAccountRowId());
		this.setBasperct(vatSetup.getBasperct());
		this.setLastYearSalesPurchaseTaxes(vatSetup.getLastYearSalesPurchaseTaxes());
		this.setLastYearTaxableSalesPurchase(vatSetup.getLastYearTaxableSalesPurchase());
		this.setLastYearTotalSalesPurchase(vatSetup.getLastYearTotalSalesPurchase());
		this.setMaximumVATAmount(UtilRoundDecimal.roundDecimalValue(vatSetup.getMaximumVATAmount()));
		this.setMinimumVATAmount(UtilRoundDecimal.roundDecimalValue(vatSetup.getMinimumVATAmount()));
		this.setVatBaseOn(vatBasedOnType!=null?vatBasedOnType.getTypeId():null);
		this.setVatDescription(vatSetup.getVatDescription());
		this.setVatDescriptionArabic(vatSetup.getVatDescriptionArabic());
		this.setVatIdNumber(vatSetup.getVatIdNumber());
		this.setVatScheduleId(vatSetup.getVatScheduleId());
		this.setVatSeriesType(vatSetup.getVatType().getTypeId());
		this.setYtdTotalSalesPurchase(UtilRoundDecimal.roundDecimalValue(vatSetup.getYtdTotalSalesPurchase()));
		this.setYtdTotalSalesPurchaseTaxes(UtilRoundDecimal.roundDecimalValue(vatSetup.getYtdTotalSalesPurchaseTaxes()));
		this.setYtdTotalTaxableSalesPurchase(UtilRoundDecimal.roundDecimalValue(vatSetup.getYtdTotalTaxableSalesPurchase()));
	}
	 
	 
	public Double getBasperct() {
		return basperct;
	}
	public void setBasperct(Double basperct) {
		this.basperct = UtilRoundDecimal.roundDecimalValue(basperct);
	}
	public Double getMaximumVATAmount() {
		return maximumVATAmount;
	}
	public void setMaximumVATAmount(Double maximumVATAmount) {
		this.maximumVATAmount = UtilRoundDecimal.roundDecimalValue(maximumVATAmount);
	}
	public Double getMinimumVATAmount() {
		return minimumVATAmount;
	}
	public void setMinimumVATAmount(Double minimumVATAmount) {
		this.minimumVATAmount = UtilRoundDecimal.roundDecimalValue(minimumVATAmount);
	}
	public int getAccountRowId() {
		return accountRowId;
	}
	public void setAccountRowId(int accountRowId) {
		this.accountRowId = accountRowId;
	}
	
	public Double getLastYearTotalSalesPurchase() {
		return lastYearTotalSalesPurchase;
	}
	public void setLastYearTotalSalesPurchase(Double lastYearTotalSalesPurchase) {
		this.lastYearTotalSalesPurchase = UtilRoundDecimal.roundDecimalValue(lastYearTotalSalesPurchase);
	}
	
	 
	public Double getLastYearSalesPurchaseTaxes() {
		return lastYearSalesPurchaseTaxes;
	}
	public void setLastYearSalesPurchaseTaxes(Double lastYearSalesPurchaseTaxes) {
		this.lastYearSalesPurchaseTaxes = UtilRoundDecimal.roundDecimalValue(lastYearSalesPurchaseTaxes);
	}
	public Double getLastYearTaxableSalesPurchase() {
		return lastYearTaxableSalesPurchase;
	}
	public void setLastYearTaxableSalesPurchase(Double lastYearTaxableSalesPurchase) {
		this.lastYearTaxableSalesPurchase = UtilRoundDecimal.roundDecimalValue(lastYearTaxableSalesPurchase);
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getVatScheduleId() {
		return vatScheduleId;
	}
	public void setVatScheduleId(String vatScheduleId) {
		this.vatScheduleId = vatScheduleId;
	}
	public String getVatDescription() {
		return vatDescription;
	}
	public void setVatDescription(String vatDescription) {
		this.vatDescription = vatDescription;
	}
	public String getVatDescriptionArabic() {
		return vatDescriptionArabic;
	}
	public void setVatDescriptionArabic(String vatDescriptionArabic) {
		this.vatDescriptionArabic = vatDescriptionArabic;
	}
	public int getVatSeriesType() {
		return vatSeriesType;
	}
	public void setVatSeriesType(int vatSeriesType) {
		this.vatSeriesType = vatSeriesType;
	}
	public String getVatIdNumber() {
		return vatIdNumber;
	}
	public void setVatIdNumber(String vatIdNumber) {
		this.vatIdNumber = vatIdNumber;
	}
	public Integer getVatBaseOn() {
		return vatBaseOn;
	}
	public void setVatBaseOn(Integer vatBaseOn) {
		this.vatBaseOn = vatBaseOn;
	}
	public Double getYtdTotalSalesPurchase() {
		return ytdTotalSalesPurchase;
	}
	public void setYtdTotalSalesPurchase(Double ytdTotalSalesPurchase) {
		this.ytdTotalSalesPurchase = UtilRoundDecimal.roundDecimalValue(ytdTotalSalesPurchase);
	}
	public Double getYtdTotalSalesPurchaseTaxes() {
		return ytdTotalSalesPurchaseTaxes;
	}
	public void setYtdTotalSalesPurchaseTaxes(Double ytdTotalSalesPurchaseTaxes) {
		this.ytdTotalSalesPurchaseTaxes = UtilRoundDecimal.roundDecimalValue(ytdTotalSalesPurchaseTaxes);
	}
	public Double getYtdTotalTaxableSalesPurchase() {
		return ytdTotalTaxableSalesPurchase;
	}
	public void setYtdTotalTaxableSalesPurchase(Double ytdTotalTaxableSalesPurchase) {
		this.ytdTotalTaxableSalesPurchase = UtilRoundDecimal.roundDecimalValue(ytdTotalTaxableSalesPurchase);
	}
	public List<String> getVatScheduleIds() {
		return vatScheduleIds;
	}
	public void setVatScheduleIds(List<String> vatScheduleIds) {
		this.vatScheduleIds = vatScheduleIds;
	}
	public Double getTotalApplyAmount() {
		return totalApplyAmount;
	}
	public void setTotalApplyAmount(Double totalApplyAmount) {
		this.totalApplyAmount = UtilRoundDecimal.roundDecimalValue(totalApplyAmount);
	}
	public Double getTotalApplyAmountYtd() {
		return totalApplyAmountYtd;
	}
	public void setTotalApplyAmountYtd(Double totalApplyAmountYtd) {
		this.totalApplyAmountYtd = UtilRoundDecimal.roundDecimalValue(totalApplyAmountYtd);
	}
	
	
	
}
