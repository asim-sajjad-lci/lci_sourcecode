/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the fa40055 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40055")
public class FADepreciationMethods implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DEPCMETHD")
	private int depreciationMethodId;

	@Column(name="DECPDESCRP")
	private String depreciationDescription;

	@Column(name="DECPDESCRPA")
	private String depreciationDescriptionArabic;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public FADepreciationMethods() {
		// TODO Auto-generated constructor stub
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Fa00103
	@OneToMany(mappedBy="faDepreciationMethods")
	private List<FABookMaintenance> faBookMaintenances;

	//bi-directional many-to-one association to Fa40013
	@OneToMany(mappedBy="faDepreciationMethods")
	private List<FABookClassSetup> faBookClassSetups;

	public int getDepreciationMethodId() {
		return depreciationMethodId;
	}

	public void setDepreciationMethodId(int depreciationMethodId) {
		this.depreciationMethodId = depreciationMethodId;
	}

	public String getDepreciationDescription() {
		return depreciationDescription;
	}

	public void setDepreciationDescription(String depreciationDescription) {
		this.depreciationDescription = depreciationDescription;
	}

	public String getDepreciationDescriptionArabic() {
		return depreciationDescriptionArabic;
	}

	public void setDepreciationDescriptionArabic(String depreciationDescriptionArabic) {
		this.depreciationDescriptionArabic = depreciationDescriptionArabic;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public List<FABookMaintenance> getFaBookMaintenances() {
		return faBookMaintenances;
	}

	public void setFaBookMaintenances(List<FABookMaintenance> faBookMaintenances) {
		this.faBookMaintenances = faBookMaintenances;
	}

	public List<FABookClassSetup> getFaBookClassSetups() {
		return faBookClassSetups;
	}

	public void setFaBookClassSetups(List<FABookClassSetup> faBookClassSetups) {
		this.faBookClassSetups = faBookClassSetups;
	}

}