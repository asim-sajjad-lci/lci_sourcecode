/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the gl49999 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl49999")
@NamedQuery(name="GLAccountsTableAccumulation.findAll", query="SELECT g FROM GLAccountsTableAccumulation g")
public class GLAccountsTableAccumulation implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="ACTROWID")
	private String accountTableRowIndex;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="TRXTBLID")
	private int transactionType;
	
	@Column(name="SGMTNUMB")
	private int segmentNumber;
	
	@Column(name="ACCNUMFULLINDEX")
	private String accountNumberFullIndex;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	//bi-directional many-to-one association to Fa40007
	@OneToMany(mappedBy="glAccountsTableAccumulation")
	private List<FAPurchasePostingAccountSetup> faPurchasePostingAccountSetups;
	
	@Column(name="ACTDESC")
	private String  accountDescription;
	
	@Column(name="ACTDESCA")
	private String  accountDescriptionArabic;
	
	@Column(name="ACTNUM")
	private String  accountNumber;
	
	@Column(name="ACCALIS")
	private String accountAliase;
	
	@Column(name="ACCALISA")
	private String accountAliaseArabic;
	

	public GLAccountsTableAccumulation() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSegmentNumber() {
		return segmentNumber;
	}

	public void setSegmentNumber(int segmentNumber) {
		this.segmentNumber = segmentNumber;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public List<FAPurchasePostingAccountSetup> getFaPurchasePostingAccountSetups() {
		return faPurchasePostingAccountSetups;
	}

	public void setFaPurchasePostingAccountSetups(List<FAPurchasePostingAccountSetup> faPurchasePostingAccountSetups) {
		this.faPurchasePostingAccountSetups = faPurchasePostingAccountSetups;
	}
	
	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public String getAccountNumberFullIndex() {
		return accountNumberFullIndex;
	}

	public void setAccountNumberFullIndex(String accountNumberFullIndex) {
		this.accountNumberFullIndex = accountNumberFullIndex;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getAccountDescriptionArabic() {
		return accountDescriptionArabic;
	}

	public void setAccountDescriptionArabic(String accountDescriptionArabic) {
		this.accountDescriptionArabic = accountDescriptionArabic;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getAccountAliaseArabic() {
		return accountAliaseArabic;
	}

	public void setAccountAliaseArabic(String accountAliaseArabic) {
		this.accountAliaseArabic = accountAliaseArabic;
	}

	public String getAccountAliase() {
		return accountAliase;
	}

	public void setAccountAliase(String accountAliase) {
		this.accountAliase = accountAliase;
	}
	
	
	
	
}