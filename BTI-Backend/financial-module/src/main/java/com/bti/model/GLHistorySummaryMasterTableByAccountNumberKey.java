package com.bti.model;

import java.io.Serializable;

public class GLHistorySummaryMasterTableByAccountNumberKey implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int accountTableRowIndex;
	private int year;
	private int periodID;
	
	public int getAccountTableRowIndex() {
		return accountTableRowIndex;
	}
	public void setAccountTableRowIndex(int accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getPeriodID() {
		return periodID;
	}
	public void setPeriodID(int periodID) {
		this.periodID = periodID;
	}
}
