package com.bti.model.dto;

import java.util.HashMap;
import java.util.Map;

import com.bti.model.GLBankTransferHeader;
import com.bti.util.UtilRoundDecimal;

public class DtoGLBankTransferDistribution {
	private int bankTransferNumber;
	private String currencyID;
	private String transferFromBank;
	private String transferToBank;
	private double transferAmount;
	private double originalAmount;
	private String accountTableRowIndexFrom;
	private String accountTableRowIndexTo;
	private Map<String,Object> accountDetailDebit = new HashMap<>();
	private Map<String,Object> accountDetailCredit = new HashMap<>();
	private Map<String,Object> accountDetailCharge = new HashMap<>();
	
	public DtoGLBankTransferDistribution() {
		// to do nothing
	}
	
	public DtoGLBankTransferDistribution(GLBankTransferHeader glBankTransferHeader) {
		this.bankTransferNumber = glBankTransferHeader.getBankTransferNumber();
		this.currencyID = glBankTransferHeader.getCurrencyId();
		this.transferFromBank = glBankTransferHeader.getCheckBookIdFrom();
		this.transferToBank = glBankTransferHeader.getCheckbookIdTo();
		this.transferAmount = UtilRoundDecimal.roundDecimalValue(glBankTransferHeader.getTransferAmountFrom());   //Pending: required calculation amount hare 
		this.originalAmount = UtilRoundDecimal.roundDecimalValue(glBankTransferHeader.getTransferAmountFrom());
	}
	public int getBankTransferNumber() {
		return bankTransferNumber;
	}
	public void setBankTransferNumber(int bankTransferNumber) {
		this.bankTransferNumber = bankTransferNumber;
	}
	public String getCurrencyID() {
		return currencyID;
	}
	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}
	public String getTransferFromBank() {
		return transferFromBank;
	}
	public void setTransferFromBank(String transferFromBank) {
		this.transferFromBank = transferFromBank;
	}
	public String getTransferToBank() {
		return transferToBank;
	}
	public void setTransferToBank(String transferToBank) {
		this.transferToBank = transferToBank;
	}
	public double getTransferAmount() {
		return transferAmount;
	}
	public void setTransferAmount(double transferAmount) {
		this.transferAmount = UtilRoundDecimal.roundDecimalValue(transferAmount);
	}
	public double getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(double originalAmount) {
		this.originalAmount = UtilRoundDecimal.roundDecimalValue(originalAmount);
	}
	public Map<String, Object> getAccountDetailDebit() {
		return accountDetailDebit;
	}
	public void setAccountDetailDebit(Map<String, Object> accountDetailDebit) {
		this.accountDetailDebit = accountDetailDebit;
	}
	public Map<String, Object> getAccountDetailCredit() {
		return accountDetailCredit;
	}
	public void setAccountDetailCredit(Map<String, Object> accountDetailCredit) {
		this.accountDetailCredit = accountDetailCredit;
	}

	public String getAccountTableRowIndexFrom() {
		return accountTableRowIndexFrom;
	}

	public void setAccountTableRowIndexFrom(String accountTableRowIndexFrom) {
		this.accountTableRowIndexFrom = accountTableRowIndexFrom;
	}

	public String getAccountTableRowIndexTo() {
		return accountTableRowIndexTo;
	}

	public void setAccountTableRowIndexTo(String accountTableRowIndexTo) {
		this.accountTableRowIndexTo = accountTableRowIndexTo;
	}

	public Map<String,Object> getAccountDetailCharge() {
		return accountDetailCharge;
	}

	public void setAccountDetailCharge(Map<String,Object> accountDetailCharge) {
		this.accountDetailCharge = accountDetailCharge;
	}
	
	
}
