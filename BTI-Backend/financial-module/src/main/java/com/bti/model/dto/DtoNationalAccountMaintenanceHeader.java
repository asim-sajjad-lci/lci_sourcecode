/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.NationalAccountMaintenanceHeader;

public class DtoNationalAccountMaintenanceHeader {

	private String custNumber;
	private String custNamePrimary;
	private String custNameSecondary;
	private int allowReceiptEntry;
	private int baseCreditCheck;
	private int applyStatus;
	private int baseFinanceCharge;
	
	private List<DtoNationalAccountMaintenanceDetail> accountMaintenanceDetail; 
	
	public DtoNationalAccountMaintenanceHeader(){
		
	}
	
	public DtoNationalAccountMaintenanceHeader(NationalAccountMaintenanceHeader accountMaintenanceHeader){
		this.custNumber ="";
		if(accountMaintenanceHeader.getCustomerMaintenance()!=null){
			custNumber=accountMaintenanceHeader.getCustomerMaintenance().getCustnumber();
		}
		this.custNamePrimary = accountMaintenanceHeader.getCustomerName();
		this.custNameSecondary = accountMaintenanceHeader.getCustomerNameArabic();
		this.allowReceiptEntry = accountMaintenanceHeader.getAllowReceiptsEntrChildrenNationalAccount();
		this.baseCreditCheck = accountMaintenanceHeader.getBaseCreditCheckConsolidatedNationalAccount();
		this.applyStatus = accountMaintenanceHeader.getApplyHoldOrInactiveStatusParentAcrossNationalAccount();
		this.baseFinanceCharge = accountMaintenanceHeader.getBaseFinanceChargConsolidatedNationalAccount();
	}
	
	public String getCustNumber() {
		return custNumber;
	}
	public void setCustNumber(String custNumber) {
		this.custNumber = custNumber;
	}
	public String getCustNamePrimary() {
		return custNamePrimary;
	}
	public void setCustNamePrimary(String custNamePrimary) {
		this.custNamePrimary = custNamePrimary;
	}
	public String getCustNameSecondary() {
		return custNameSecondary;
	}
	public void setCustNameSecondary(String custNameSecondary) {
		this.custNameSecondary = custNameSecondary;
	}
	public int getAllowReceiptEntry() {
		return allowReceiptEntry;
	}
	public void setAllowReceiptEntry(int allowReceiptEntry) {
		this.allowReceiptEntry = allowReceiptEntry;
	}
	public int getBaseCreditCheck() {
		return baseCreditCheck;
	}
	public void setBaseCreditCheck(int baseCreditCheck) {
		this.baseCreditCheck = baseCreditCheck;
	}
	public int getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(int applyStatus) {
		this.applyStatus = applyStatus;
	}
	public int getBaseFinanceCharge() {
		return baseFinanceCharge;
	}
	public void setBaseFinanceCharge(int baseFinanceCharge) {
		this.baseFinanceCharge = baseFinanceCharge;
	}

	public List<DtoNationalAccountMaintenanceDetail> getAccountMaintenanceDetail() {
		return accountMaintenanceDetail;
	}

	public void setAccountMaintenanceDetail(List<DtoNationalAccountMaintenanceDetail> accountMaintenanceDetail) {
		this.accountMaintenanceDetail = accountMaintenanceDetail;
	}
	
	
}
