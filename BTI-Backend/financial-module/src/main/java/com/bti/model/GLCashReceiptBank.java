/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the gl10400 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10400")
@NamedQuery(name="GLCashReceiptBank .findAll", query="SELECT a FROM GLCashReceiptBank  a")
public class GLCashReceiptBank implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BNKRECPNO")
	private int bankCashReceipt;
	
	@Column(name="RECPTNO")
	private String receiptNumber;
	
	@OneToOne
	@JoinColumn(name="SORSDOC")
	private GLConfigurationAuditTrialCodes glConfigurationAuditTrialCOdes;
	
	@ManyToOne
	@JoinColumn(name="CHEKBOKID")
	private CheckbookMaintenance checkbookMaintenance;
	
	@ManyToOne
	@JoinColumn(name="CUSTOMERID")
	private CustomerMaintenance customerMaintenance ;
	
	@ManyToOne
	@JoinColumn(name="VENDORID")
	private VendorMaintenance vendorMaintenance;

	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="EXCHTBLINXD")
	private String exchangeTableIndex;
	
	@Column(name="XCHGRATE")
	private Double exchangeRate;
	
	@Column(name="RECTRXDT")
	private Date receiptDate;
 
	@ManyToOne
	@JoinColumn(name="RECTYPE")
	private ReceiptType receiptType;
	
	@ManyToOne
	@JoinColumn(name="PYMTMETHD")
	private PaymentMethodType paymentMethod;
	
	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;
	
	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWIDDBIT")
	private GLAccountsTableAccumulation glAccountsTableAccumulationDebit;
	
	@Column(name="RECDSCRPTN")
	private String receiptDescription;
	
	@Column(name="RECAMNT")
	private double receiptAmount;

	@Column(name="CHECKNO")
	private String checkNumber;              

	@Column(name="BANKNAM")
	private String bankName;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;

	@Column(name="CARDEXPTY")
	private int creditCardExpireYear;
	
	@Column(name="CARDEXPTM")
	private int creditCardExpireMonth;

	//bi-directional many-to-one association to sy03600
	@ManyToOne
	@JoinColumn(name="TAXSCHDID")
	private VATSetup vatSetup;
	
	@Column(name="TAXAMT")
	private double vatAmount;
	
	@Column(name="DEPOTER")
	private String depositorName;
	
	@ManyToOne
	@JoinColumn(name="BATCHID")
	private GLBatches glBatches;
	
	@Column(name="CREATDDT")
	private Date createDate;

	@Column(name="MODFITDT")
	private Date modifyDate;              
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	public int getBankCashReceipt() {
		return bankCashReceipt;
	}

	public void setBankCashReceipt(int bankCashReceipt) {
		this.bankCashReceipt = bankCashReceipt;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public ReceiptType getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(ReceiptType receiptType) {
		this.receiptType = receiptType;
	}

	public PaymentMethodType getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethodType paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	
	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public String getReceiptDescription() {
		return receiptDescription;
	}

	public void setReceiptDescription(String receiptDescription) {
		this.receiptDescription = receiptDescription;
	}

	public double getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public VATSetup getVatSetup() {
		return vatSetup;
	}

	public void setVatSetup(VATSetup vatSetup) {
		this.vatSetup = vatSetup;
	}

 
	public double getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(double vatAmount) {
		this.vatAmount = vatAmount;
	}

	public String getDepositorName() {
		return depositorName;
	}

	public void setDepositorName(String depositorName) {
		this.depositorName = depositorName;
	}

	public CheckbookMaintenance getCheckbookMaintenance() {
		return checkbookMaintenance;
	}

	public void setCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookMaintenance = checkbookMaintenance;
	}

	public GLBatches getGlBatches() {
		return glBatches;
	}

	public void setGlBatches(GLBatches glBatches) {
		this.glBatches = glBatches;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	public CustomerMaintenance getCustomerMaintenance() {
		return customerMaintenance;
	}

	public void setCustomerMaintenance(CustomerMaintenance customerMaintenance) {
		this.customerMaintenance = customerMaintenance;
	}

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public VendorMaintenance getVendorMaintenance() {
		return vendorMaintenance;
	}

	public void setVendorMaintenance(VendorMaintenance vendorMaintenance) {
		this.vendorMaintenance = vendorMaintenance;
	}

	public GLAccountsTableAccumulation getGlAccountsTableAccumulationDebit() {
		return glAccountsTableAccumulationDebit;
	}

	public void setGlAccountsTableAccumulationDebit(GLAccountsTableAccumulation glAccountsTableAccumulationDebit) {
		this.glAccountsTableAccumulationDebit = glAccountsTableAccumulationDebit;
	}

	public GLConfigurationAuditTrialCodes getGlConfigurationAuditTrialCOdes() {
		return glConfigurationAuditTrialCOdes;
	}

	public void setGlConfigurationAuditTrialCOdes(GLConfigurationAuditTrialCodes glConfigurationAuditTrialCOdes) {
		this.glConfigurationAuditTrialCOdes = glConfigurationAuditTrialCOdes;
	} 
	
	
	
}