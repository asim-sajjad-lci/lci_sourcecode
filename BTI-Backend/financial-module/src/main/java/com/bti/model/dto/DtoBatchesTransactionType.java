/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.BatchTransactionType;

/**
 * Description: DtoAccountCategory class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 10, 2017
 * Modified on: AUG 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

public class DtoBatchesTransactionType {

	private Integer batchTransactionTypeId;
	private Integer typeId;
	private String transactionType;
	private String transactionModule;

	

	public DtoBatchesTransactionType() {

	}

	public DtoBatchesTransactionType(BatchTransactionType batchTransactionType) {
		this.batchTransactionTypeId = batchTransactionType.getBatchTransactionTypeId();
		this.typeId = batchTransactionType.getTypeId();
		this.transactionType=batchTransactionType.getTransactionType();		
		this.transactionModule=batchTransactionType.getTransactionModule();
	}

	public Integer getBatchTransactionTypeId() {
		return batchTransactionTypeId;
	}

	public void setBatchTransactionTypeId(Integer batchTransactionTypeId) {
		this.batchTransactionTypeId = batchTransactionTypeId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionModule() {
		return transactionModule;
	}

	public void setTransactionModule(String transactionModule) {
		this.transactionModule = transactionModule;
	}

	
	
	
}
