/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the master_blncdspl database table.
 * 
 */
@Entity
@Table(name="master_blncdspl")
@NamedQuery(name="MasterBlncdspl.findAll", query="SELECT m FROM MasterBlncdspl m")
public class MasterBlncdspl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private short id;

	@Column(name="TYPE_ID")
	private short typeId;

	@Column(name="TYPE_PRIMARY")
	private String typePrimary;

	/*@Column(name="TYPE_SECONDARY")
	private String typeSecondary;*/
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Gl40000
	@OneToMany(mappedBy="masterBlncdspl")
	private List<GLConfigurationSetup> gl40000s;
	
	@ManyToOne
	@JoinColumn(name="lang_Id")
	private Language language;

	public MasterBlncdspl() {
	}

	public short getId() {
		return this.id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public short getTypeId() {
		return this.typeId;
	}

	public void setTypeId(short typeId) {
		this.typeId = typeId;
	}

	public String getTypePrimary() {
		return this.typePrimary;
	}

	public void setTypePrimary(String typePrimary) {
		this.typePrimary = typePrimary;
	}


	public List<GLConfigurationSetup> getGl40000s() {
		return this.gl40000s;
	}

	public void setGl40000s(List<GLConfigurationSetup> gl40000s) {
		this.gl40000s = gl40000s;
	}

	public GLConfigurationSetup addGl40000(GLConfigurationSetup gl40000) {
		getGl40000s().add(gl40000);
		gl40000.setMasterBlncdspl(this);

		return gl40000;
	}

	public GLConfigurationSetup removeGl40000(GLConfigurationSetup gl40000) {
		getGl40000s().remove(gl40000);
		gl40000.setMasterBlncdspl(null);

		return gl40000;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}
	
}