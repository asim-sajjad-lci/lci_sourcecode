/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FAStructureSetup;

/**
 * Description: The DtoFAStructureSetup class for FAStructureSetup 
 * Name of Project: BTI
 * Created on: May 19, 2017
 * Modified on: May 19, 2017 12:19:38 PM
 * @author seasia
 * Version: 
 */
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoFAStructureSetup {
	private int structureIndex;
	private String structureId;
	private String structureDescription;
	private String structureDescriptionArabic;
	private String messageType;
	
	public DtoFAStructureSetup() {
		
	}
	public DtoFAStructureSetup(FAStructureSetup faStructureSetup) {

		this.structureIndex = faStructureSetup.getStructureIndex();
		this.structureDescription  =faStructureSetup.getStructureDescription();
		this.structureDescriptionArabic = faStructureSetup.getStructureDescriptionArabic();
		this.structureId = faStructureSetup.getStructureId();
		 
	}
	public int getStructureIndex() {
		return structureIndex;
	}
	public void setStructureIndex(int structureIndex) {
		this.structureIndex = structureIndex;
	}
	public String getStructureId() {
		return structureId;
	}
	public void setStructureId(String structureId) {
		this.structureId = structureId;
	}
	public String getStructureDescription() {
		return structureDescription;
	}
	public void setStructureDescription(String structureDescription) {
		this.structureDescription = structureDescription;
	}
	public String getStructureDescriptionArabic() {
		return structureDescriptionArabic;
	}
	public void setStructureDescriptionArabic(String structureDescriptionArabic) {
		this.structureDescriptionArabic = structureDescriptionArabic;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	 

}
