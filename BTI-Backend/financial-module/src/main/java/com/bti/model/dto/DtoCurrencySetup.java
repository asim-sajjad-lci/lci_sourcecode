/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.Date;

import com.bti.model.CurrencySetup;
import com.bti.model.MasterNegativeSymbolSignTypes;
import com.bti.model.MasterNegativeSymbolTypes;
import com.bti.model.MasterSeperatorDecimal;
import com.bti.model.MasterSeperatorThousands;
/**
 * Description: DTO Currency Setup class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
public class DtoCurrencySetup {
	
	private String currencyId;
	private int currencyIndex;
	private String changeBy;
	private int createdBy;
	private String currencyDescription;
	private String currencyDescriptionArabic;
	private String currencySymbol;
	private String currencyUnit;
	private String unitSubunitConnector;
	private String currencySubunit;
	private String currencyUnitArabic;
	private String unitSubunitConnectorArabic;
	private String currencySubunitArabic;
	private int separatorsDecimal;
	private int rowIndexId;
	private Date rowDateIndex;
	private Boolean includeSpaceAfterCurrencySymbol;
	private int negativeSymbol;
	private int displayNegativeSymbolSign;
	private int separatorsThousands;
	private int displayCurrencySymbol;
	private String separatorsDecimalValue;
	private String separatorsThousandValue;
	private String negativeSymbolValue;
	private Boolean defaultCurrency;
	
	/**
	 * 
	 */
	
	
	
	public DtoCurrencySetup() {
		super();
	}
	
	public Boolean getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(Boolean defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	/**
	 * @param currencySetup
	 */
	public DtoCurrencySetup(CurrencySetup currencySetup,MasterSeperatorDecimal masterSeperatorDecimal, MasterNegativeSymbolTypes masterNegativeSymbolTypes,MasterNegativeSymbolSignTypes masterNegativeSymbolSignTypes,MasterSeperatorThousands masterSeperatorThousands) {
		super();
		this.currencyId = currencySetup.getCurrencyId();
		this.currencyDescription = currencySetup.getCurrencyDescription();
		this.currencyDescriptionArabic = currencySetup.getCurrencyDescriptionArabic();
		this.currencySymbol = currencySetup.getCurrencySymbol();
		this.currencyUnit = currencySetup.getCurrencyUnit();
		this.unitSubunitConnector = currencySetup.getUnitSubunitConnector();
		this.currencySubunit = currencySetup.getCurrencySubunit();
		this.currencyUnitArabic = currencySetup.getCurrencyUnitArabic();
		this.unitSubunitConnectorArabic = currencySetup.getUnitSubunitConnectorArabic();
		this.currencySubunitArabic = currencySetup.getCurrencySubunitArabic();
		this.defaultCurrency = currencySetup.isDefault();
		this.separatorsDecimal = 0;
		this.separatorsDecimalValue="";
		if(masterSeperatorDecimal!=null){
			this.separatorsDecimal=masterSeperatorDecimal.getSeperatorDecimalId();
			this.separatorsDecimalValue=masterSeperatorDecimal.getSeperatorDecimalPrimary();
		}
		this.includeSpaceAfterCurrencySymbol = currencySetup.getIncludeSpaceAfterCurrencySymbol();
		this.negativeSymbol = 0;
		this.negativeSymbolValue="";
		if(masterNegativeSymbolTypes!=null){
			this.negativeSymbol=masterNegativeSymbolTypes.getTypeId();
			this.negativeSymbolValue=masterNegativeSymbolTypes.getNegativeSymbolType();
		}
		
		this.displayNegativeSymbolSign = 0;
		if(masterNegativeSymbolSignTypes!=null){
			this.displayNegativeSymbolSign=masterNegativeSymbolSignTypes.getTypeId();
		}
		this.separatorsThousands = 0;
		this.separatorsThousandValue="";
		if(masterSeperatorThousands!=null){
			this.separatorsThousands=masterSeperatorThousands.getSeperatorThousandId();
			this.separatorsThousandValue=masterSeperatorThousands.getSeperatorThousandPrimary();
		}
		this.displayCurrencySymbol=currencySetup.getDisplayCurrencySymbol();
	}

	/**
	 * @return the currencyId
	 */
	public String getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	/**
	 * @return the currencyIndex
	 */
	public int getCurrencyIndex() {
		return currencyIndex;
	}
	/**
	 * @param currencyIndex the currencyIndex to set
	 */
	public void setCurrencyIndex(int currencyIndex) {
		this.currencyIndex = currencyIndex;
	}
	/**
	 * @return the changeBy
	 */
	public String getChangeBy() {
		return changeBy;
	}
	/**
	 * @param changeBy the changeBy to set
	 */
	public void setChangeBy(String changeBy) {
		this.changeBy = changeBy;
	}
	/**
	 * @return the createdBy
	 */
	public int getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the currencyDescription
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}
	/**
	 * @param currencyDescription the currencyDescription to set
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	/**
	 * @return the currencyDescriptionArabic
	 */
	public String getCurrencyDescriptionArabic() {
		return currencyDescriptionArabic;
	}
	/**
	 * @param currencyDescriptionArabic the currencyDescriptionArabic to set
	 */
	public void setCurrencyDescriptionArabic(String currencyDescriptionArabic) {
		this.currencyDescriptionArabic = currencyDescriptionArabic;
	}
	/**
	 * @return the currencySymbol
	 */
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	/**
	 * @param currencySymbol the currencySymbol to set
	 */
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	/**
	 * @return the currencyUnit
	 */
	public String getCurrencyUnit() {
		return currencyUnit;
	}
	/**
	 * @param currencyUnit the currencyUnit to set
	 */
	public void setCurrencyUnit(String currencyUnit) {
		this.currencyUnit = currencyUnit;
	}
	/**
	 * @return the unitSubunitConnector
	 */
	public String getUnitSubunitConnector() {
		return unitSubunitConnector;
	}
	/**
	 * @param unitSubunitConnector the unitSubunitConnector to set
	 */
	public void setUnitSubunitConnector(String unitSubunitConnector) {
		this.unitSubunitConnector = unitSubunitConnector;
	}
	/**
	 * @return the currencySubunit
	 */
	public String getCurrencySubunit() {
		return currencySubunit;
	}
	/**
	 * @param currencySubunit the currencySubunit to set
	 */
	public void setCurrencySubunit(String currencySubunit) {
		this.currencySubunit = currencySubunit;
	}
	/**
	 * @return the currencyUnitArabic
	 */
	public String getCurrencyUnitArabic() {
		return currencyUnitArabic;
	}
	/**
	 * @param currencyUnitArabic the currencyUnitArabic to set
	 */
	public void setCurrencyUnitArabic(String currencyUnitArabic) {
		this.currencyUnitArabic = currencyUnitArabic;
	}
	/**
	 * @return the unitSubunitConnectorArabic
	 */
	public String getUnitSubunitConnectorArabic() {
		return unitSubunitConnectorArabic;
	}
	/**
	 * @param unitSubunitConnectorArabic the unitSubunitConnectorArabic to set
	 */
	public void setUnitSubunitConnectorArabic(String unitSubunitConnectorArabic) {
		this.unitSubunitConnectorArabic = unitSubunitConnectorArabic;
	}
	/**
	 * @return the currencySubunitArabic
	 */
	public String getCurrencySubunitArabic() {
		return currencySubunitArabic;
	}
	/**
	 * @param currencySubunitArabic the currencySubunitArabic to set
	 */
	public void setCurrencySubunitArabic(String currencySubunitArabic) {
		this.currencySubunitArabic = currencySubunitArabic;
	}
	/**
	 * @return the separatorsDecimal
	 */
	public int getSeparatorsDecimal() {
		return separatorsDecimal;
	}
	/**
	 * @param separatorsDecimal the separatorsDecimal to set
	 */
	public void setSeparatorsDecimal(int separatorsDecimal) {
		this.separatorsDecimal = separatorsDecimal;
	}
	/**
	 * @return the rowIndexId
	 */
	public int getRowIndexId() {
		return rowIndexId;
	}
	/**
	 * @param rowIndexId the rowIndexId to set
	 */
	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}
	/**
	 * @return the rowDateIndex
	 */
	public Date getRowDateIndex() {
		return rowDateIndex;
	}
	/**
	 * @param rowDateIndex the rowDateIndex to set
	 */
	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}
	/**
	 * @return the includeSpaceAfterCurrencySymbol
	 */
	public Boolean getIncludeSpaceAfterCurrencySymbol() {
		return includeSpaceAfterCurrencySymbol;
	}
	/**
	 * @param includeSpaceAfterCurrencySymbol the includeSpaceAfterCurrencySymbol to set
	 */
	public void setIncludeSpaceAfterCurrencySymbol(Boolean includeSpaceAfterCurrencySymbol) {
		this.includeSpaceAfterCurrencySymbol = includeSpaceAfterCurrencySymbol;
	}
	/**
	 * @return the negativeSymbol
	 */
	public int getNegativeSymbol() {
		return negativeSymbol;
	}
	/**
	 * @param negativeSymbol the negativeSymbol to set
	 */
	public void setNegativeSymbol(int negativeSymbol) {
		this.negativeSymbol = negativeSymbol;
	}
	/**
	 * @return the displayNegativeSymbolSign
	 */
	public int getDisplayNegativeSymbolSign() {
		return displayNegativeSymbolSign;
	}
	/**
	 * @param displayNegativeSymbolSign the displayNegativeSymbolSign to set
	 */
	public void setDisplayNegativeSymbolSign(int displayNegativeSymbolSign) {
		this.displayNegativeSymbolSign = displayNegativeSymbolSign;
	}
	/**
	 * @return the separatorsThousands
	 */
	public int getSeparatorsThousands() {
		return separatorsThousands;
	}
	/**
	 * @param separatorsThousands the separatorsThousands to set
	 */
	public void setSeparatorsThousands(int separatorsThousands) {
		this.separatorsThousands = separatorsThousands;
	}

	public int getDisplayCurrencySymbol() {
		return displayCurrencySymbol;
	}

	public void setDisplayCurrencySymbol(int displayCurrencySymbol) {
		this.displayCurrencySymbol = displayCurrencySymbol;
	}

	public String getSeparatorsDecimalValue() {
		return separatorsDecimalValue;
	}

	public void setSeparatorsDecimalValue(String separatorsDecimalValue) {
		this.separatorsDecimalValue = separatorsDecimalValue;
	}

	public String getSeparatorsThousandValue() {
		return separatorsThousandValue;
	}

	public void setSeparatorsThousandValue(String separatorsThousandValue) {
		this.separatorsThousandValue = separatorsThousandValue;
	}

	public String getNegativeSymbolValue() {
		return negativeSymbolValue;
	}

	public void setNegativeSymbolValue(String negativeSymbolValue) {
		this.negativeSymbolValue = negativeSymbolValue;
	}
}
