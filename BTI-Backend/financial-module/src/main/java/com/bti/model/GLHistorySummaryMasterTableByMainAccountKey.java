package com.bti.model;
import java.io.Serializable;

public class GLHistorySummaryMasterTableByMainAccountKey  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int mainAccountIndex;
	private int year;
	private int periodID;
	
	public int getMainAccountIndex() {
		return mainAccountIndex;
	}
	public void setMainAccountIndex(int mainAccountIndex) {
		this.mainAccountIndex = mainAccountIndex;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getPeriodID() {
		return periodID;
	}
	public void setPeriodID(int periodID) {
		this.periodID = periodID;
	}
}
