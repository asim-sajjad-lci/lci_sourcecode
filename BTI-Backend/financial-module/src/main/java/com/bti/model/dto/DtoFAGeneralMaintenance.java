/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.model.dto;
import java.util.Date;

import com.bti.model.FAGeneralMaintenance;
import com.bti.model.MasterGeneralMaintenanceAssetStatus;
import com.bti.model.MasterGeneralMaintenanceAssetType;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;


public class DtoFAGeneralMaintenance {

	private String assetId;

	private int assetIdSerial;

	private String faAcquisitionCost;

	private String faAcquisitionDate;

	private String faAddedDate;

	private String assetLabelId;
	
	private String faMasterAssetId;

	private int assetQuantity;
	
	private int assetStatus;

	private int assetType;

	private int changeBy;

	private int createdBy;

	private Date createdDate;

	private String currencyId;

	private String rowIndexId;

	private Date rowDateIndex;

	private String faDescription;

	private String faDescriptionArabic;

	private String employeeId;

	private String assetExtendedDescription;

	private String lastMaintenance;

	private String manufacturerName;

	private String faShortName;

	private String faAccountGroupsSetupId;

	private String faClassSetupId;

	private String faLocationId;

	private Integer faPropertyTypes;

	private String faPhysicalLocationId;

	private String faStructureId;
	
	private String messageType;
	
	private int faAccountGroupIndex;
	
	public DtoFAGeneralMaintenance() {
		
	}
	
	public DtoFAGeneralMaintenance(FAGeneralMaintenance faGeneralMaintenance, MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus, MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType) 
	{
		this.assetId = faGeneralMaintenance.getAssetId();
		this.assetIdSerial=faGeneralMaintenance.getAssetIdSerial();
		this.assetLabelId=faGeneralMaintenance.getAssetLabelId();
		this.assetQuantity=faGeneralMaintenance.getAssetQuantity();
		this.assetStatus=0;
		
		if(masterGeneralMaintenanceAssetStatus!=null){
			this.assetStatus=masterGeneralMaintenanceAssetStatus.getTypeId();
		}
		
		this.assetType=0;
		if(masterGeneralMaintenanceAssetType!=null){
			this.assetType=masterGeneralMaintenanceAssetType.getTypeId();
		}
		
		this.currencyId="";
		if(faGeneralMaintenance.getCurrencySetup()!=null){
			this.currencyId=faGeneralMaintenance.getCurrencySetup().getCurrencyId();
		}
		
		this.faDescription=faGeneralMaintenance.getFADescription();
		this.faDescriptionArabic=faGeneralMaintenance.getFADescriptionArabic();
		this.employeeId=faGeneralMaintenance.getEmployeeId();
		this.faAccountGroupsSetupId="";
		if(faGeneralMaintenance.getFaAccountGroupsSetup()!=null){
			this.faAccountGroupIndex=faGeneralMaintenance.getFaAccountGroupsSetup().getFaAccountGroupIndex();
			this.faAccountGroupsSetupId=faGeneralMaintenance.getFaAccountGroupsSetup().getFaAccountGroupId();
		}
		this.faAcquisitionCost=String.valueOf(UtilRoundDecimal.roundDecimalValue(faGeneralMaintenance.getFAAcquisitionCost()));
		
		this.faAcquisitionDate="";
		if(faGeneralMaintenance.getFAAcquisitionDate()!=null){
			this.faAcquisitionDate=UtilDateAndTime.dateToStringddmmyyyy(faGeneralMaintenance.getFAAcquisitionDate());
		}
		this.faAddedDate="";
		if(faGeneralMaintenance.getFAAddedDate()!=null){
			this.faAddedDate=UtilDateAndTime.dateToStringddmmyyyy(faGeneralMaintenance.getFAAddedDate());
		}
		this.faClassSetupId="";
		if(faGeneralMaintenance.getFaClassSetup()!=null){
			this.faClassSetupId=faGeneralMaintenance.getFaClassSetup().getClassId();
		}
		this.faLocationId="";
		if(faGeneralMaintenance.getFaLocationSetup()!=null){
			this.faLocationId=faGeneralMaintenance.getFaLocationSetup().getLocationId();
		}
		this.faPhysicalLocationId="";
		if(faGeneralMaintenance.getFaPhysicalLocationSetup()!=null){
			this.faPhysicalLocationId=faGeneralMaintenance.getFaPhysicalLocationSetup().getPhysicalLocationId();
		}
		this.faMasterAssetId="";
		if(UtilRandomKey.isNotBlank(faGeneralMaintenance.getFaMasterAssetId())){
			this.faMasterAssetId=faGeneralMaintenance.getFaMasterAssetId();
		}
		this.faPropertyTypes=0;
		if(faGeneralMaintenance.getFaPropertyTypes()!=null){
			this.faPropertyTypes=faGeneralMaintenance.getFaPropertyTypes().getPropertyTypeId();
		}
		this.faShortName="";
		if(UtilRandomKey.isNotBlank(faGeneralMaintenance.getFAShortName())){
			this.faShortName=faGeneralMaintenance.getFAShortName();
		}
		this.faStructureId="";
		if(faGeneralMaintenance.getFaStructureSetup()!=null){
			this.faStructureId=faGeneralMaintenance.getFaStructureSetup().getStructureId();
		}
		this.lastMaintenance="";
		if(faGeneralMaintenance.getLastMaintenance()!=null){
			this.lastMaintenance=UtilDateAndTime.dateToStringddmmyyyy(faGeneralMaintenance.getLastMaintenance());
		}
		this.manufacturerName="";
		if(UtilRandomKey.isNotBlank(faGeneralMaintenance.getManufacturerName())){
			this.manufacturerName=faGeneralMaintenance.getManufacturerName();
		}
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public int getAssetIdSerial() {
		return assetIdSerial;
	}

	public void setAssetIdSerial(int assetIdSerial) {
		this.assetIdSerial = assetIdSerial;
	}

	

	public String getAssetLabelId() {
		return assetLabelId;
	}

	public void setAssetLabelId(String assetLabelId) {
		this.assetLabelId = assetLabelId;
	}

	public String getFaMasterAssetId() {
		return faMasterAssetId;
	}

	public void setFaMasterAssetId(String faMasterAssetId) {
		this.faMasterAssetId = faMasterAssetId;
	}

	public int getAssetQuantity() {
		return assetQuantity;
	}

	public void setAssetQuantity(int assetQuantity) {
		this.assetQuantity = assetQuantity;
	}

	public int getAssetStatus() {
		return assetStatus;
	}

	public void setAssetStatus(int assetStatus) {
		this.assetStatus = assetStatus;
	}

	public int getAssetType() {
		return assetType;
	}

	public void setAssetType(int assetType) {
		this.assetType = assetType;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getAssetExtendedDescription() {
		return assetExtendedDescription;
	}

	public void setAssetExtendedDescription(String assetExtendedDescription) {
		this.assetExtendedDescription = assetExtendedDescription;
	}

	public String getLastMaintenance() {
		return lastMaintenance;
	}

	public void setLastMaintenance(String lastMaintenance) {
		this.lastMaintenance = lastMaintenance;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}


	public String getFaAccountGroupsSetupId() {
		return faAccountGroupsSetupId;
	}

	public void setFaAccountGroupsSetupId(String faAccountGroupsSetupId) {
		this.faAccountGroupsSetupId = faAccountGroupsSetupId;
	}

	public String getFaClassSetupId() {
		return faClassSetupId;
	}

	public void setFaClassSetupId(String faClassSetupId) {
		this.faClassSetupId = faClassSetupId;
	}

	public Integer getFaPropertyTypes() {
		return faPropertyTypes;
	}

	public void setFaPropertyTypes(Integer faPropertyTypes) {
		this.faPropertyTypes = faPropertyTypes;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getFaLocationId() {
		return faLocationId;
	}

	public void setFaLocationId(String faLocationId) {
		this.faLocationId = faLocationId;
	}

	public String getFaPhysicalLocationId() {
		return faPhysicalLocationId;
	}

	public void setFaPhysicalLocationId(String faPhysicalLocationId) {
		this.faPhysicalLocationId = faPhysicalLocationId;
	}

	public String getFaStructureId() {
		return faStructureId;
	}

	public void setFaStructureId(String faStructureId) {
		this.faStructureId = faStructureId;
	}

	public String getFaAcquisitionCost() {
		return faAcquisitionCost;
	}

	public void setFaAcquisitionCost(String faAcquisitionCost) {
		this.faAcquisitionCost = faAcquisitionCost;
	}

	public String getFaAcquisitionDate() {
		return faAcquisitionDate;
	}

	public void setFaAcquisitionDate(String faAcquisitionDate) {
		this.faAcquisitionDate = faAcquisitionDate;
	}

	public String getFaAddedDate() {
		return faAddedDate;
	}

	public void setFaAddedDate(String faAddedDate) {
		this.faAddedDate = faAddedDate;
	}

	public String getFaDescription() {
		return faDescription;
	}

	public void setFaDescription(String faDescription) {
		this.faDescription = faDescription;
	}

	public String getFaDescriptionArabic() {
		return faDescriptionArabic;
	}

	public void setFaDescriptionArabic(String faDescriptionArabic) {
		this.faDescriptionArabic = faDescriptionArabic;
	}

	public String getFaShortName() {
		return faShortName;
	}

	public void setFaShortName(String faShortName) {
		this.faShortName = faShortName;
	}

	public int getFaAccountGroupIndex() {
		return faAccountGroupIndex;
	}

	public void setFaAccountGroupIndex(int faAccountGroupIndex) {
		this.faAccountGroupIndex = faAccountGroupIndex;
	}
	
	
}
