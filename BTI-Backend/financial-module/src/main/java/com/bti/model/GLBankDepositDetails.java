/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10601 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10601")
@NamedQuery(name="GLBankDepositDetails.findAll", query="SELECT a FROM GLBankDepositDetails a")
public class GLBankDepositDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DEPSEQCN")
	private int depositSequence;
	
	@ManyToOne
	@JoinColumn(name="DEPSNO")
	private GLBankDepositHeader glBankDepositHeader;
	
	@Column(name="DEPTYP")
	private int depositType ;

	@Column(name="DEPDATE")
	private Date depositTransactionDateFromSupLedger;
	
	@Column(name="DEPTRXNO")
	private String depositTransactionNumberFromSubLedger;

	@Column(name="DEPAMNT")
	private double depositCheckbookAmount;
	
	@Column(name="ORGDEPAMNT")
	private double originalDepositCheckbookAmount;
	
	@Column(name="DEPTRXTYP")
	private int depositTransactionType;
	
	@Column(name="CREATDDT")
	private Date createDate;
	
	@Column(name="MODIFTDT")
	private Date modifyDate;

	@Column(name="CHANGEBY")
	private String modifyByUserID;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex ;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex ;
	

	public int getDepositSequence() {
		return depositSequence;
	}

	public void setDepositSequence(int depositSequence) {
		this.depositSequence = depositSequence;
	}

	public GLBankDepositHeader getGlBankDepositHeader() {
		return glBankDepositHeader;
	}

	public void setGlBankDepositHeader(GLBankDepositHeader glBankDepositHeader) {
		this.glBankDepositHeader = glBankDepositHeader;
	}

	public int getDepositType() {
		return depositType;
	}

	public void setDepositType(int depositType) {
		this.depositType = depositType;
	}

	public Date getDepositTransactionDateFromSupLedger() {
		return depositTransactionDateFromSupLedger;
	}

	public void setDepositTransactionDateFromSupLedger(Date depositTransactionDateFromSupLedger) {
		this.depositTransactionDateFromSupLedger = depositTransactionDateFromSupLedger;
	}

	public String getDepositTransactionNumberFromSubLedger() {
		return depositTransactionNumberFromSubLedger;
	}

	public void setDepositTransactionNumberFromSubLedger(String depositTransactionNumberFromSubLedger) {
		this.depositTransactionNumberFromSubLedger = depositTransactionNumberFromSubLedger;
	}

	public double getDepositCheckbookAmount() {
		return depositCheckbookAmount;
	}

	public void setDepositCheckbookAmount(double depositCheckbookAmount) {
		this.depositCheckbookAmount = depositCheckbookAmount;
	}

	public double getOriginalDepositCheckbookAmount() {
		return originalDepositCheckbookAmount;
	}

	public void setOriginalDepositCheckbookAmount(double originalDepositCheckbookAmount) {
		this.originalDepositCheckbookAmount = originalDepositCheckbookAmount;
	}

	public int getDepositTransactionType() {
		return depositTransactionType;
	}

	public void setDepositTransactionType(int depositTransactionType) {
		this.depositTransactionType = depositTransactionType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}           

	
	
	
	
	
	

}