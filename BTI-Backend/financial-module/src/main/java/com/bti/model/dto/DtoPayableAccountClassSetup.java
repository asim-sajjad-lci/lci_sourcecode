/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoPayableAccountClassSetup {
	
	private Integer id;
	private Integer accountRowIndex;
	private Integer segmentNumber;
	private Long indexId;
	private Integer transactionType;
	private Integer accountType;
	private String accountTypeValue;
	private String classId;
	private String description;
	private String vendorClassId;
	private String accountDescription;
	private String accountDescriptionArabic;
	private String accountNumber;
	private List<Long> accountNumberIndex;
	private List<DtoPayableAccountClassSetup> accountNumberList;
	private String type;
	private String messageType;
	private List<DtoPayableAccountClassSetup> accNumberIndexValue;
	private String vendorId;
	private String customerId;
	private String accountTableRowIndex;
	private String percentage;
	private String accountValue;
	private String customerClassId;
	private String priority;
	private String mainAccountNumber;
	private Long mainAccountID;
	private String descriptionArabic;
	private String accountGroupId;
	private int accountTypeId;
	private String typicalBalanceType;
	private Boolean isTransactionAllow;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAccountRowIndex() {
		return accountRowIndex;
	}
	public void setAccountRowIndex(Integer accountRowIndex) {
		this.accountRowIndex = accountRowIndex;
	}
	public Integer getSegmentNumber() {
		return segmentNumber;
	}
	public void setSegmentNumber(Integer segmentNumber) {
		this.segmentNumber = segmentNumber;
	}
	public Long getIndexId() {
		return indexId;
	}
	public void setIndexId(Long indexId) {
		this.indexId = indexId;
	}
	public Integer getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Long> getAccountNumberIndex() {
		return accountNumberIndex;
	}
	public void setAccountNumberIndex(List<Long> accountNumberIndex) {
		this.accountNumberIndex = accountNumberIndex;
	}
	public String getVendorClassId() {
		return vendorClassId;
	}
	public void setVendorClassId(String vendorClassId) {
		this.vendorClassId = vendorClassId;
	}
	public String getAccountDescription() {
		return accountDescription;
	}
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	public List<DtoPayableAccountClassSetup> getAccountNumberList() {
		return accountNumberList;
	}
	public void setAccountNumberList(List<DtoPayableAccountClassSetup> accountNumberList) {
		this.accountNumberList = accountNumberList;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getAccountTypeValue() {
		return accountTypeValue;
	}
	public void setAccountTypeValue(String accountTypeValue) {
		this.accountTypeValue = accountTypeValue;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<DtoPayableAccountClassSetup> getAccNumberIndexValue() {
		return accNumberIndexValue;
	}
	public void setAccNumberIndexValue(List<DtoPayableAccountClassSetup> accNumberIndexValue) {
		this.accNumberIndexValue = accNumberIndexValue;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}
	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getAccountValue() {
		return accountValue;
	}
	public void setAccountValue(String accountValue) {
		this.accountValue = accountValue;
	}
	public String getCustomerClassId() {
		return customerClassId;
	}
	public void setCustomerClassId(String customerClassId) {
		this.customerClassId = customerClassId;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getMainAccountNumber() {
		return mainAccountNumber;
	}
	public void setMainAccountNumber(String mainAccountNumber) {
		this.mainAccountNumber = mainAccountNumber;
	}
	public Long getMainAccountID() {
		return mainAccountID;
	}
	public void setMainAccountID(Long mainAccountID) {
		this.mainAccountID = mainAccountID;
	}
	public String getDescriptionArabic() {
		return descriptionArabic;
	}
	public void setDescriptionArabic(String descriptionArabic) {
		this.descriptionArabic = descriptionArabic;
	}
	public String getAccountGroupId() {
		return accountGroupId;
	}
	public void setAccountGroupId(String accountGroupId) {
		this.accountGroupId = accountGroupId;
	}
	public int getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}
	public String getTypicalBalanceType() {
		return typicalBalanceType;
	}
	public void setTypicalBalanceType(String typicalBalanceType) {
		this.typicalBalanceType = typicalBalanceType;
	}
	public Boolean getIsTransactionAllow() {
		return isTransactionAllow;
	}
	public void setIsTransactionAllow(Boolean isTransactionAllow) {
		this.isTransactionAllow = isTransactionAllow;
	}
	public String getAccountDescriptionArabic() {
		return accountDescriptionArabic;
	}
	public void setAccountDescriptionArabic(String accountDescriptionArabic) {
		this.accountDescriptionArabic = accountDescriptionArabic;
	}

	
	
}
