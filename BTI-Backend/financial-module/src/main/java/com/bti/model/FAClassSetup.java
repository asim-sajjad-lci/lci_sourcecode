/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the fa40002 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40002")
@NamedQuery(name="FAClassSetup.findAll", query="SELECT f FROM FAClassSetup f")
public class FAClassSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CLASSID")
	private String classId;
	
	@Column(name="CLASSINDX")
	private int classIndx;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRPTN")
	private String classSetupDescription;

	@Column(name="DSCRPTNA")
	private String classSetupDescriptionArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Fa00101
	@OneToMany(mappedBy="faClassSetup")
	private List<FAGeneralMaintenance> faGeneralMaintenances;
	
	
	//bi-directional many-to-one association to Fa40007
	@OneToMany(mappedBy="faClassSetup")
	private List<FAPurchasePostingAccountSetup> faPurchasePostingAccountSetups;

	//bi-directional many-to-one association to Fa40200
	@ManyToOne
	@JoinColumn(name="INSRANINDX")
	private FAInsuranceClassSetup faInsuranceClassSetup;

	//bi-directional many-to-one association to Fa40100
	@ManyToOne
	@JoinColumn(name="ACCGROPINXD")
	private FAAccountGroupsSetup faAccountGroupsSetup;

	//bi-directional many-to-one association to Fa40013
/*	@OneToMany(mappedBy="faAccountGroupIndex")
	private List<FABookClassSetup> faBookClassSetups;*/

	public FAClassSetup() {
	}

	public int getClassIndx() {
		return classIndx;
	}

	public void setClassIndx(int classIndx) {
		this.classIndx = classIndx;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getClassSetupDescription() {
		return classSetupDescription;
	}

	public void setClassSetupDescription(String classSetupDescription) {
		this.classSetupDescription = classSetupDescription;
	}

	public String getClassSetupDescriptionArabic() {
		return classSetupDescriptionArabic;
	}

	public void setClassSetupDescriptionArabic(String classSetupDescriptionArabic) {
		this.classSetupDescriptionArabic = classSetupDescriptionArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public List<FAGeneralMaintenance> getFaGeneralMaintenances() {
		return faGeneralMaintenances;
	}

	public void setFaGeneralMaintenances(List<FAGeneralMaintenance> faGeneralMaintenances) {
		this.faGeneralMaintenances = faGeneralMaintenances;
	}

	public FAInsuranceClassSetup getFaInsuranceClassSetup() {
		return faInsuranceClassSetup;
	}

	public void setFaInsuranceClassSetup(FAInsuranceClassSetup faInsuranceClassSetup) {
		this.faInsuranceClassSetup = faInsuranceClassSetup;
	}

	public FAAccountGroupsSetup getFaAccountGroupsSetup() {
		return faAccountGroupsSetup;
	}

	public void setFaAccountGroupsSetup(FAAccountGroupsSetup faAccountGroupsSetup) {
		this.faAccountGroupsSetup = faAccountGroupsSetup;
	}

	public List<FAPurchasePostingAccountSetup> getFaPurchasePostingAccountSetups() {
		return faPurchasePostingAccountSetups;
	}

	public void setFaPurchasePostingAccountSetups(List<FAPurchasePostingAccountSetup> faPurchasePostingAccountSetups) {
		this.faPurchasePostingAccountSetups = faPurchasePostingAccountSetups;
	}

	/*public List<FABookClassSetup> getFaBookClassSetups() {
		return faBookClassSetups;
	}

	public void setFaBookClassSetups(List<FABookClassSetup> faBookClassSetups) {
		this.faBookClassSetups = faBookClassSetups;
	}*/

	

	
}