/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the lease_type database table.
 * 
 */
@Entity
@Table(name="lease_type")
@NamedQuery(name="LeaseType.findAll", query="SELECT l FROM LeaseType l")
public class LeaseType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idindex;

	@Column(name="LEASE_TYPE_ID")
	private int leaseTypeId;

	private String value;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	//bi-directional many-to-one association to Fa00105
	/*@OneToMany(mappedBy="leaseType")
	private List<FALeaseMaintenance> faLeaseMaintenances;*/
	
	@ManyToOne
	@JoinColumn(name="lang_Id")
	private Language language;

	public LeaseType() {
	}

	public int getIdindex() {
		return this.idindex;
	}

	public void setIdindex(int idindex) {
		this.idindex = idindex;
	}

	public int getLeaseTypeId() {
		return this.leaseTypeId;
	}

	public void setLeaseTypeId(int leaseTypeId) {
		this.leaseTypeId = leaseTypeId;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	/*public List<FALeaseMaintenance> getFaLeaseMaintenances() {
		return faLeaseMaintenances;
	}

	public void setFaLeaseMaintenances(List<FALeaseMaintenance> faLeaseMaintenances) {
		this.faLeaseMaintenances = faLeaseMaintenances;
	}*/
	
	
	
	

}