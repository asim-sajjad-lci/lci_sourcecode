/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl00108 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl00108")
@NamedQuery(name="GLBudgetMaintenancePeriods.findAll", query="SELECT a FROM GLBudgetMaintenancePeriods a")
public class GLBudgetMaintenancePeriods implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id; 
	
	@ManyToOne
	@JoinColumn(name="BUDGTID")
	private GLBudgetMaintenance glBudgetMaintenance;
	
	@Column(name="PERINDX")
	private int periodIndex;
	
	
	@Column(name="PERNAM")
	private String periodName;

	@Column(name="PERDT")
	private Date periodDate;
	
	@Column(name="PERAMT")
	private double periodAmount;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GLBudgetMaintenance getGlBudgetMaintenance() {
		return glBudgetMaintenance;
	}

	public void setGlBudgetMaintenance(GLBudgetMaintenance glBudgetMaintenance) {
		this.glBudgetMaintenance = glBudgetMaintenance;
	}

	public int getPeriodIndex() {
		return periodIndex;
	}

	public void setPeriodIndex(int periodIndex) {
		this.periodIndex = periodIndex;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public Date getPeriodDate() {
		return periodDate;
	}

	public void setPeriodDate(Date periodDate) {
		this.periodDate = periodDate;
	}

	public double getPeriodAmount() {
		return periodAmount;
	}

	public void setPeriodAmount(double periodAmount) {
		this.periodAmount = periodAmount;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}
		
    
}