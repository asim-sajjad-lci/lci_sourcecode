/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the sy03300 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "sy03300")
@NamedQuery(name="PaymentTermsSetup.findAll", query="SELECT s FROM PaymentTermsSetup s")
public class PaymentTermsSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PYMTRMID")
	private String paymentTermId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DISCAMT")
	private Double discountAmount;

	@Column(name="DISCPER")
	private Double discountPercent;
	
	@ManyToOne
	@JoinColumn(name = "DISCTYPE")
	private MasterDiscountType masterDiscountType;
	
	@ManyToOne
	@JoinColumn(name = "DISPERD")
	private MasterDiscountTypePeriod masterDiscountTypePeriod;

	@Column(name="DISPERDAMT")
	private int discountTypesPeriodValue;

	@Column(name="DSCRIPTN")
	private String paymentDescription;

	@Column(name="DSCRIPTNA")
	private String paymentDescriptionArabic;

	@Column(name="DUEDTDS")
	private int dueDaysDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	@ManyToOne
	@JoinColumn(name = "DUETYPE")
	private MasterDueTypes masterDueTypes;

	@Column(name="MODIFDT")
	private Date modifyDate;

	//bi-directional many-to-one association to Ap00102
	@OneToMany(mappedBy="paymentTermsSetup")
	private List<VendorMaintenanceOptions> vendorMaintenanceOptions;

	//bi-directional many-to-one association to Ap00200
	@OneToMany(mappedBy="paymentTermsSetup")
	private List<VendorClassesSetup> vendorClassesSetups;

	//bi-directional many-to-one association to Ar00102
	@OneToMany(mappedBy="paymentTermsSetup")
	private List<CustomerMaintenanceOptions> customerMaintenanceOptions;

	//bi-directional many-to-one association to Ar00200
	@OneToMany(mappedBy="paymentTermsSetup")
	private List<CustomerClassesSetup> customerClassesSetups;

	public PaymentTermsSetup() {
	}

	public String getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(String paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}

	public int getDiscountTypesPeriodValue() {
		return discountTypesPeriodValue;
	}

	public void setDiscountTypesPeriodValue(int discountTypesPeriodValue) {
		this.discountTypesPeriodValue = discountTypesPeriodValue;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public String getPaymentDescriptionArabic() {
		return paymentDescriptionArabic;
	}

	public void setPaymentDescriptionArabic(String paymentDescriptionArabic) {
		this.paymentDescriptionArabic = paymentDescriptionArabic;
	}

	public int getDueDaysDate() {
		return dueDaysDate;
	}

	public void setDueDaysDate(int dueDaysDate) {
		this.dueDaysDate = dueDaysDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public List<VendorMaintenanceOptions> getVendorMaintenanceOptions() {
		return vendorMaintenanceOptions;
	}

	public void setVendorMaintenanceOptions(List<VendorMaintenanceOptions> vendorMaintenanceOptions) {
		this.vendorMaintenanceOptions = vendorMaintenanceOptions;
	}

	public List<VendorClassesSetup> getVendorClassesSetups() {
		return vendorClassesSetups;
	}

	public void setVendorClassesSetups(List<VendorClassesSetup> vendorClassesSetups) {
		this.vendorClassesSetups = vendorClassesSetups;
	}

	public List<CustomerMaintenanceOptions> getCustomerMaintenanceOptions() {
		return customerMaintenanceOptions;
	}

	public void setCustomerMaintenanceOptions(List<CustomerMaintenanceOptions> customerMaintenanceOptions) {
		this.customerMaintenanceOptions = customerMaintenanceOptions;
	}

	public List<CustomerClassesSetup> getCustomerClassesSetups() {
		return customerClassesSetups;
	}

	public void setCustomerClassesSetups(List<CustomerClassesSetup> customerClassesSetups) {
		this.customerClassesSetups = customerClassesSetups;
	}

	public MasterDiscountType getMasterDiscountType() {
		return masterDiscountType;
	}

	public void setMasterDiscountType(MasterDiscountType masterDiscountType) {
		this.masterDiscountType = masterDiscountType;
	}

	public MasterDiscountTypePeriod getMasterDiscountTypePeriod() {
		return masterDiscountTypePeriod;
	}

	public void setMasterDiscountTypePeriod(MasterDiscountTypePeriod masterDiscountTypePeriod) {
		this.masterDiscountTypePeriod = masterDiscountTypePeriod;
	}

	public MasterDueTypes getMasterDueTypes() {
		return masterDueTypes;
	}

	public void setMasterDueTypes(MasterDueTypes masterDueTypes) {
		this.masterDueTypes = masterDueTypes;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
}