/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.PMDocumentsTypeSetup;

public class DtoPMDocumentsTypeSetup {
	private int apDocumentTypeId;
	private String documentTypeDescription;
	private String documentTypeDescriptionArabic;
	private int documentNumberLastNumber;
	private String documentCode;
	private String documentType;
	private String messageType;
	
	public DtoPMDocumentsTypeSetup() {
		
	}
	public DtoPMDocumentsTypeSetup(PMDocumentsTypeSetup pmDocumentsTypeSetup) {
		this.apDocumentTypeId = pmDocumentsTypeSetup.getApDocumentTypeId();
		this.setDocumentCode(pmDocumentsTypeSetup.getDocumentCode());
		this.setDocumentNumberLastNumber(pmDocumentsTypeSetup.getDocumentNumberLastNumber());
		this.setDocumentType(pmDocumentsTypeSetup.getDocumentType());
		this.setDocumentTypeDescription(pmDocumentsTypeSetup.getDocumentTypeDescription());
		this.setDocumentTypeDescriptionArabic(pmDocumentsTypeSetup.getDocumentTypeDescriptionArabic());
	}
	public int getApDocumentTypeId() {
		return apDocumentTypeId;
	}
	public void setApDocumentTypeId(int apDocumentTypeId) {
		this.apDocumentTypeId = apDocumentTypeId;
	}
	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}
	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}
	public String getDocumentTypeDescriptionArabic() {
		return documentTypeDescriptionArabic;
	}
	public void setDocumentTypeDescriptionArabic(String documentTypeDescriptionArabic) {
		this.documentTypeDescriptionArabic = documentTypeDescriptionArabic;
	}
	public int getDocumentNumberLastNumber() {
		return documentNumberLastNumber;
	}
	public void setDocumentNumberLastNumber(int documentNumberLastNumber) {
		this.documentNumberLastNumber = documentNumberLastNumber;
	}
	public String getDocumentCode() {
		return documentCode;
	}
	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}
	 
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	
}
