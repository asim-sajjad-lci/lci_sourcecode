/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the master seperator thousands database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "master_seperator_thousands")
@NamedQuery(name="MasterSeperatorThousands.findAll", query="SELECT a FROM MasterSeperatorThousands a")
public class MasterSeperatorThousands implements Serializable {
	private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="ID")
		private int id;

		@Column(name="SEPERATOR_THOUSANDS_PRIMARY")
		private String seperatorThousandPrimary;

		/*@Column(name="SEPERATOR_THOUSANDS_SECONDARY")
		private String seperatorThousandSecondary;*/

		@Column(name="SEPERATOR_THOUSANDS_ID")
		private int seperatorThousandId;
		
		@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
		private boolean isDeleted;
		
		@ManyToOne
		@JoinColumn(name="lang_Id")
		private Language language;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getSeperatorThousandPrimary() {
			return seperatorThousandPrimary;
		}

		public void setSeperatorThousandPrimary(String seperatorThousandPrimary) {
			this.seperatorThousandPrimary = seperatorThousandPrimary;
		}

		public int getSeperatorThousandId() {
			return seperatorThousandId;
		}

		public void setSeperatorThousandId(int seperatorThousandId) {
			this.seperatorThousandId = seperatorThousandId;
		}

		public Language getLanguage() {
			return language;
		}

		public void setLanguage(Language language) {
			this.language = language;
		}

		public boolean isDeleted() {
			return isDeleted;
		}

		public void setDeleted(boolean isDeleted) {
			this.isDeleted = isDeleted;
		}
		
		
}