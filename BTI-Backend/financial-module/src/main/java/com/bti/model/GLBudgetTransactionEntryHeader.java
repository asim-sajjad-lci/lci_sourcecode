/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10700 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10700")
@NamedQuery(name="GLBudgetTransactionEntryHeader.findAll", query="SELECT a FROM GLBudgetTransactionEntryHeader a")
public class GLBudgetTransactionEntryHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BUDTRXID")
	private int budgetJournalEntryID;
	
	@Column(name="BUDTRXDDT")
	private Date budgetJournalCreateDate;
	
	@ManyToOne
	@JoinColumn(name="BATCHID")
	private GLBatches glBatches;

	@Column(name="BUDDSCR")
	private String budgetJournalDescription;
	
	@Column(name="ACTINDX")
	private int mainAccountIndex;

	@Column(name="BUDCALMEDH")
	private int budgetCalculationMethod;
	
	@Column(name="BUDINCBB")
	private int includeBeggingBalance;
	
	@Column(name="BUDTYP")
	private int typeProcess;
	
	@ManyToOne
	@JoinColumn(name="BUDGTID")
	private GLBudgetMaintenance glBudgetMaintenance;
	
	@Column(name="YEAR1")
	private int year;

	@Column(name="CREATDDT")
	private Date createDate;

	@Column(name="MODIFTDT")
	private Date modifyDate ;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID ;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex ;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex ;

	
	public int getBudgetJournalEntryID() {
		return budgetJournalEntryID;
	}

	public void setBudgetJournalEntryID(int budgetJournalEntryID) {
		this.budgetJournalEntryID = budgetJournalEntryID;
	}

	public Date getBudgetJournalCreateDate() {
		return budgetJournalCreateDate;
	}

	public void setBudgetJournalCreateDate(Date budgetJournalCreateDate) {
		this.budgetJournalCreateDate = budgetJournalCreateDate;
	}

	public String getBudgetJournalDescription() {
		return budgetJournalDescription;
	}

	public void setBudgetJournalDescription(String budgetJournalDescription) {
		this.budgetJournalDescription = budgetJournalDescription;
	}

	public int getMainAccountIndex() {
		return mainAccountIndex;
	}

	public void setMainAccountIndex(int mainAccountIndex) {
		this.mainAccountIndex = mainAccountIndex;
	}

	public int getBudgetCalculationMethod() {
		return budgetCalculationMethod;
	}

	public void setBudgetCalculationMethod(int budgetCalculationMethod) {
		this.budgetCalculationMethod = budgetCalculationMethod;
	}

	public int getIncludeBeggingBalance() {
		return includeBeggingBalance;
	}

	public void setIncludeBeggingBalance(int includeBeggingBalance) {
		this.includeBeggingBalance = includeBeggingBalance;
	}

	public int getTypeProcess() {
		return typeProcess;
	}

	public void setTypeProcess(int typeProcess) {
		this.typeProcess = typeProcess;
	}
	
	public GLBatches getGlBatches() {
		return glBatches;
	}

	public void setGlBatches(GLBatches glBatches) {
		this.glBatches = glBatches;
	}

	public GLBudgetMaintenance getGlBudgetMaintenance() {
		return glBudgetMaintenance;
	}

	public void setGlBudgetMaintenance(GLBudgetMaintenance glBudgetMaintenance) {
		this.glBudgetMaintenance = glBudgetMaintenance;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}
		
	
	

}