/**
 * BTI - BAAN for Technology And Trade IntegerL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.ARCashReceipt;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoARCashReceiptDistribution {

	private String cashReceiptNumber;
	private String customerId;
	private String customerName;
	private String currencyID;
	private String transactionType;
	private String transactionNumber;
	private Double functionalAmount;
	private Double originalAmount;
	
	List<DtoARDistributionDetail> listDtoARDistributionDetail;
	 

	public DtoARCashReceiptDistribution() {
	}

	public DtoARCashReceiptDistribution(ARCashReceipt arCashReceipt) {
		this.cashReceiptNumber = arCashReceipt.getCashReceiptNumber();
		
 
	}

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Double getFunctionalAmount() {
		return functionalAmount;
	}

	public void setFunctionalAmount(Double functionalAmount) {
		this.functionalAmount = UtilRoundDecimal.roundDecimalValue(functionalAmount);
	}

	public Double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(Double originalAmount) {
		this.originalAmount = UtilRoundDecimal.roundDecimalValue(originalAmount);
	}

	public List<DtoARDistributionDetail> getListDtoARDistributionDetail() {
		return listDtoARDistributionDetail;
	}

	public void setListDtoARDistributionDetail(List<DtoARDistributionDetail> listDtoARDistributionDetail) {
		this.listDtoARDistributionDetail = listDtoARDistributionDetail;
	}
	
	

	 
	 
 
}
