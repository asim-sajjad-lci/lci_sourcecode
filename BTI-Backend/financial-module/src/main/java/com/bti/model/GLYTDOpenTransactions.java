/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl90200 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl90200")
@NamedQuery(name="GLYTDOpenTransactions.findAll", query="SELECT a FROM GLYTDOpenTransactions a")
public class GLYTDOpenTransactions extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDROW")
	private int rowIndex;
	
	@Column(name="YEAR1")
	private int openYear;

	@Column(name="JORNID")
	private String journalEntryID;
	
	@ManyToOne
	@JoinColumn(name="SOURDOC")
	private GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes;
	
	@Column(name="DSCRPTN")
	private String journalDescription;
	
	@Column(name="DSCRPTNA")
	private String journalDescriptionArabic;
	
	@Column(name="TRXDDT")
	private Date transactionDate;

	@Column(name="TRXPDDT")
	private Date transactionPostingDate;
	
	@Column(name="TRXSOURC")
	private String transactionSource;
	
	@Column(name="ACTROWID")
	private String accountTableRowIndex;
	
	@Column(name="USRCREAT")
	private String userCreateTransaction;
	
	@Column(name="USRPOST")
	private String userPostingTransaction;
	
	@ManyToOne
	@JoinColumn(name="SERIES")
	private GLConfigurationAuditTrialCodes moduleSeriesNumber;
	
	@Column(name="ORGTRXNMB")
	private String originalTransactionNumber;
	 
	@Column(name="CURNCYID")
	private String currencyId;
	
	@Column(name="EXGTBLINXD")
	private int exchangeTableIndex;
	  
	@Column(name="XCHGRATE")
	private Double exchangeTableRate;
	
	@Column(name="DEBITAMT")
	private Double debitAmount;
	
	@Column(name="CRDTAMT")
	private Double creditAmount;
	
	@Column(name="ORGDEBTAMT")
	private Double originalDebitAmount;
	
	@Column(name="ORGCRETAMT")
	private Double originalCreditAmount;
	
	@Column(name="ORGCOMID")
	private String originalCompanyID;
	
	@Column(name="ORGJRNENTR")
	private String originalJournalEntryID;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="TRXTYP")
	private int transactionType;
	
	@Column(name="TRXREDT")
	private Date transactionReversingDate;
	
	@Column(name="TOTDBJRN")
	private Double totalJournalEntryDebit;

	@Column(name="TOTCRJRN")
	private Double totalJournalEntryCredit;
	
	@Column(name="OTOTDBJRN")
	private Double originalTotalJournalEntryDebit;

	@Column(name="OTOTCRJRN")
	private Double originalTotalJournalEntryCredit;
	
	@Column(name="TPCLBLNC")
	private int balanceType;
	
	@Column(name="DISDRCPTN")
	private String distributionDescription ;
	
	
	public GLYTDOpenTransactions() {
		this.creditAmount = 0.0;
		this.totalJournalEntryCredit = 0.0;
		this.originalTotalJournalEntryCredit = 0.0;
		
		this.debitAmount = 0.0;
		this.totalJournalEntryDebit = 0.0;
		this.originalDebitAmount = 0.0;
	}


	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getOpenYear() {
		return openYear;
	}

	public void setOpenYear(int openYear) {
		this.openYear = openYear;
	}

	public String getJournalEntryID() {
		return journalEntryID;
	}

	public void setJournalEntryID(String journalEntryID) {
		this.journalEntryID = journalEntryID;
	}
	
	public GLConfigurationAuditTrialCodes getGlConfigurationAuditTrialCodes() {
		return glConfigurationAuditTrialCodes;
	}

	public void setGlConfigurationAuditTrialCodes(GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes) {
		this.glConfigurationAuditTrialCodes = glConfigurationAuditTrialCodes;
	}

	public String getJournalDescription() {
		return journalDescription;
	}

	public void setJournalDescription(String journalDescription) {
		this.journalDescription = journalDescription;
	}

	public String getJournalDescriptionArabic() {
		return journalDescriptionArabic;
	}

	public void setJournalDescriptionArabic(String journalDescriptionArabic) {
		this.journalDescriptionArabic = journalDescriptionArabic;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Date getTransactionPostingDate() {
		return transactionPostingDate;
	}

	public void setTransactionPostingDate(Date transactionPostingDate) {
		this.transactionPostingDate = transactionPostingDate;
	}

	public String getTransactionSource() {
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource) {
		this.transactionSource = transactionSource;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public String getUserCreateTransaction() {
		return userCreateTransaction;
	}

	public void setUserCreateTransaction(String userCreateTransaction) {
		this.userCreateTransaction = userCreateTransaction;
	}

	public String getUserPostingTransaction() {
		return userPostingTransaction;
	}

	public void setUserPostingTransaction(String userPostingTransaction) {
		this.userPostingTransaction = userPostingTransaction;
	}

	public String getOriginalTransactionNumber() {
		return originalTransactionNumber;
	}

	public void setOriginalTransactionNumber(String originalTransactionNumber) {
		this.originalTransactionNumber = originalTransactionNumber;
	}

	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(Double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public Double getOriginalCreditAmount() {
		return originalCreditAmount;
	}

	public void setOriginalCreditAmount(Double originalCreditAmount) {
		this.originalCreditAmount = originalCreditAmount;
	}

	public String getOriginalCompanyID() {
		return originalCompanyID;
	}

	public void setOriginalCompanyID(String originalCompanyID) {
		this.originalCompanyID = originalCompanyID;
	}

	public String getOriginalJournalEntryID() {
		return originalJournalEntryID;
	}

	public void setOriginalJournalEntryID(String originalJournalEntryID) {
		this.originalJournalEntryID = originalJournalEntryID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public GLConfigurationAuditTrialCodes getModuleSeriesNumber() {
		return moduleSeriesNumber;
	}

	public void setModuleSeriesNumber(GLConfigurationAuditTrialCodes moduleSeriesNumber) {
		this.moduleSeriesNumber = moduleSeriesNumber;
	}

	

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public Date getTransactionReversingDate() {
		return transactionReversingDate;
	}

	public void setTransactionReversingDate(Date transactionReversingDate) {
		this.transactionReversingDate = transactionReversingDate;
	}

	public Double getTotalJournalEntryDebit() {
		return totalJournalEntryDebit;
	}

	public void setTotalJournalEntryDebit(Double totalJournalEntryDebit) {
		this.totalJournalEntryDebit = totalJournalEntryDebit;
	}

	public Double getTotalJournalEntryCredit() {
		return totalJournalEntryCredit;
	}

	public void setTotalJournalEntryCredit(Double totalJournalEntryCredit) {
		this.totalJournalEntryCredit = totalJournalEntryCredit;
	}

	public Double getOriginalTotalJournalEntryDebit() {
		return originalTotalJournalEntryDebit;
	}

	public void setOriginalTotalJournalEntryDebit(Double originalTotalJournalEntryDebit) {
		this.originalTotalJournalEntryDebit = originalTotalJournalEntryDebit;
	}

	public Double getOriginalTotalJournalEntryCredit() {
		return originalTotalJournalEntryCredit;
	}

	public void setOriginalTotalJournalEntryCredit(Double originalTotalJournalEntryCredit) {
		this.originalTotalJournalEntryCredit = originalTotalJournalEntryCredit;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}
	
}