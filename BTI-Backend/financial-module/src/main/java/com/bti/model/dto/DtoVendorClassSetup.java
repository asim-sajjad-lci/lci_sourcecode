/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.VendorClassesSetup;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoVendorClassSetup {
	private String vendorClassId;
	private String vendorId;
	private String vendorName;
	private String classDescription;
	private String classDescriptionArabic;
	private int creditLimit;
	private Double creditLimitAmount;
	private String currencyId;
	private int maximumInvoiceAmount;
	private Double maximumInvoiceAmountValue;
	private int minimumCharge;
	private Double minimumChargeAmount;
	private Double minimumOrderAmount;
	private Double tradeDiscountPercent;
	private int openMaintenanceHistoryCalendarYear;
	private int openMaintenanceHistoryDistribution;
	private int openMaintenanceHistoryFiscalYear;
	private int openMaintenanceHistoryTransaction;
	private String userDefine1;
	private String userDefine2;
	private String userDefine3;
	private String messageType;
	private String vatScheduleId;
	private String shipmentMethodId;
	private String checkBookId;
	private String paymentTermId;
	private Integer accTableId;
	
	private Double miscVatPercent;
	private Double freightVatPercent;
	private Double vatPercent;
	private String miscVatScheduleId;
	private String freightVatScheduleId;
	private String  vatId;
	public DtoVendorClassSetup() {}
	public DtoVendorClassSetup(VendorClassesSetup vendorClassesSetup) {
		this.setVendorClassId(vendorClassesSetup.getVendorClassId());
		this.setClassDescription(vendorClassesSetup.getClassDescription());
		this.setClassDescriptionArabic(vendorClassesSetup.getClassDescriptionArabic());
		this.setCreditLimit(vendorClassesSetup.getCreditLimit());
		this.setCreditLimitAmount(UtilRoundDecimal.roundDecimalValue(vendorClassesSetup.getCreditLimitAmount()));
		this.setCurrencyId(vendorClassesSetup.getCurrencySetup()!=null?vendorClassesSetup.getCurrencySetup().getCurrencyId():null);
		this.setMaximumInvoiceAmount(vendorClassesSetup.getMaximumInvoiceAmount());
		this.setMaximumInvoiceAmountValue(UtilRoundDecimal.roundDecimalValue(vendorClassesSetup.getMaximumInvoiceAmountValue()));
		this.setMinimumCharge(vendorClassesSetup.getMinimumCharge());
		this.setMinimumChargeAmount(UtilRoundDecimal.roundDecimalValue(vendorClassesSetup.getMinimumChargeAmount()));
		this.setMinimumOrderAmount(UtilRoundDecimal.roundDecimalValue(vendorClassesSetup.getMinimumOrderAmount()));
		this.setOpenMaintenanceHistoryCalendarYear(vendorClassesSetup.getOpenMaintenanceHistoryCalendarYear());
		this.setOpenMaintenanceHistoryDistribution(vendorClassesSetup.getOpenMaintenanceHistoryDistribution());
		this.setOpenMaintenanceHistoryFiscalYear(vendorClassesSetup.getOpenMaintenanceHistoryFiscalYear());
		this.setOpenMaintenanceHistoryTransaction(vendorClassesSetup.getOpenMaintenanceHistoryTransaction());
		this.setTradeDiscountPercent(UtilRoundDecimal.roundDecimalValue(vendorClassesSetup.getTradeDiscountPercent()));
		this.setUserDefine1(vendorClassesSetup.getUserDefine1());
		this.setUserDefine2(vendorClassesSetup.getUserDefine2());
		this.setUserDefine3(vendorClassesSetup.getUserDefine3());
		this.vatScheduleId = vendorClassesSetup.getVatSetup()!=null?vendorClassesSetup.getVatSetup().getVatScheduleId():null;
		this.shipmentMethodId = vendorClassesSetup.getShipmentMethodSetup()!=null?vendorClassesSetup.getShipmentMethodSetup().getShipmentMethodId():null;
		this.checkBookId = vendorClassesSetup.getCheckbookMaintenance()!=null?vendorClassesSetup.getCheckbookMaintenance().getCheckBookId():null;
		this.paymentTermId = vendorClassesSetup.getPaymentTermsSetup()!=null?vendorClassesSetup.getPaymentTermsSetup().getPaymentTermId():null;
		this.accTableId = vendorClassesSetup.getVendorClassAccountTableSetup()!=null?vendorClassesSetup.getVendorClassAccountTableSetup().getAccTableId():null;
	 
	}

	public String getVendorClassId() {
		return vendorClassId;
	}

	public void setVendorClassId(String vendorClassId) {
		this.vendorClassId = vendorClassId;
	}

	public String getClassDescription() {
		return classDescription;
	}

	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}

	public String getClassDescriptionArabic() {
		return classDescriptionArabic;
	}

	public void setClassDescriptionArabic(String classDescriptionArabic) {
		this.classDescriptionArabic = classDescriptionArabic;
	}

	public int getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Double getCreditLimitAmount() {
		return creditLimitAmount;
	}

	public void setCreditLimitAmount(Double creditLimitAmount) {
		this.creditLimitAmount = UtilRoundDecimal.roundDecimalValue(creditLimitAmount);
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public int getMaximumInvoiceAmount() {
		return maximumInvoiceAmount;
	}

	public void setMaximumInvoiceAmount(int maximumInvoiceAmount) {
		this.maximumInvoiceAmount = maximumInvoiceAmount;
	}

	public int getMinimumCharge() {
		return minimumCharge;
	}

	public void setMinimumCharge(int minimumCharge) {
		this.minimumCharge = minimumCharge;
	}

	public Double getMinimumChargeAmount() {
		return minimumChargeAmount;
	}

	public void setMinimumChargeAmount(Double minimumChargeAmount) {
		this.minimumChargeAmount = UtilRoundDecimal.roundDecimalValue(minimumChargeAmount);
	}

	public Double getMinimumOrderAmount() {
		return minimumOrderAmount;
	}

	public void setMinimumOrderAmount(Double minimumOrderAmount) {
		this.minimumOrderAmount = UtilRoundDecimal.roundDecimalValue(minimumOrderAmount);
	}

	public Double getTradeDiscountPercent() {
		return tradeDiscountPercent;
	}

	public void setTradeDiscountPercent(Double tradeDiscountPercent) {
		this.tradeDiscountPercent = UtilRoundDecimal.roundDecimalValue(tradeDiscountPercent);
	}

	public int getOpenMaintenanceHistoryCalendarYear() {
		return openMaintenanceHistoryCalendarYear;
	}

	public void setOpenMaintenanceHistoryCalendarYear(int openMaintenanceHistoryCalendarYear) {
		this.openMaintenanceHistoryCalendarYear = openMaintenanceHistoryCalendarYear;
	}

	public int getOpenMaintenanceHistoryDistribution() {
		return openMaintenanceHistoryDistribution;
	}

	public void setOpenMaintenanceHistoryDistribution(int openMaintenanceHistoryDistribution) {
		this.openMaintenanceHistoryDistribution = openMaintenanceHistoryDistribution;
	}

	public int getOpenMaintenanceHistoryFiscalYear() {
		return openMaintenanceHistoryFiscalYear;
	}

	public void setOpenMaintenanceHistoryFiscalYear(int openMaintenanceHistoryFiscalYear) {
		this.openMaintenanceHistoryFiscalYear = openMaintenanceHistoryFiscalYear;
	}

	public int getOpenMaintenanceHistoryTransaction() {
		return openMaintenanceHistoryTransaction;
	}

	public void setOpenMaintenanceHistoryTransaction(int openMaintenanceHistoryTransaction) {
		this.openMaintenanceHistoryTransaction = openMaintenanceHistoryTransaction;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getVatScheduleId() {
		return vatScheduleId;
	}

	public void setVatScheduleId(String vatScheduleId) {
		this.vatScheduleId = vatScheduleId;
	}

	public String getShipmentMethodId() {
		return shipmentMethodId;
	}

	public void setShipmentMethodId(String shipmentMethodId) {
		this.shipmentMethodId = shipmentMethodId;
	}

	public String getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(String checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(String paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Integer getAccTableId() {
		return accTableId;
	}

	public void setAccTableId(Integer accTableId) {
		this.accTableId = accTableId;
	}
	public Double getMaximumInvoiceAmountValue() {
		return maximumInvoiceAmountValue;
	}
	public void setMaximumInvoiceAmountValue(Double maximumInvoiceAmountValue) {
		this.maximumInvoiceAmountValue = UtilRoundDecimal.roundDecimalValue(maximumInvoiceAmountValue);
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public Double getMiscVatPercent() {
		return miscVatPercent;
	}
	public void setMiscVatPercent(Double miscVatPercent) {
		this.miscVatPercent = UtilRoundDecimal.roundDecimalValue(miscVatPercent);
	}
	public Double getFreightVatPercent() {
		return freightVatPercent;
	}
	public void setFreightVatPercent(Double freightVatPercent) {
		this.freightVatPercent = UtilRoundDecimal.roundDecimalValue(freightVatPercent);
	}
	public Double getVatPercent() {
		return vatPercent;
	}
	public void setVatPercent(Double vatPercent) {
		this.vatPercent = UtilRoundDecimal.roundDecimalValue(vatPercent);
	}
	public String getMiscVatScheduleId() {
		return miscVatScheduleId;
	}
	public void setMiscVatScheduleId(String miscVatScheduleId) {
		this.miscVatScheduleId = miscVatScheduleId;
	}
	public String getFreightVatScheduleId() {
		return freightVatScheduleId;
	}
	public void setFreightVatScheduleId(String freightVatScheduleId) {
		this.freightVatScheduleId = freightVatScheduleId;
	}
	public String getVatId() {
		return vatId;
	}
	public void setVatId(String vatId) {
		this.vatId = vatId;
	}
	 
	 
	 
	

}
