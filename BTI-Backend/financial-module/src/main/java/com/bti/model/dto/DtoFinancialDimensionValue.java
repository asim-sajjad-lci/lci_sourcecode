/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.COAFinancialDimensionsValues;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoFinancialDimensionValue {
	
	private Long dimInxValue;
	private int dimInxd;
	private String dimensionValue;
	private String dimensionDescription;
	private String dimensionDescriptionArabic;
	private String dimensionName;
	private int segmentNumber;
	private String messageType;
	
	public DtoFinancialDimensionValue() {

	}
	
	public DtoFinancialDimensionValue(COAFinancialDimensionsValues coaFinancialDimensionsValues) {
		this.dimInxValue=coaFinancialDimensionsValues.getDimInxValue();
		this.dimensionValue=coaFinancialDimensionsValues.getDimensionValue();
		this.dimensionDescription=coaFinancialDimensionsValues.getDimensionDescription();
		if(coaFinancialDimensionsValues.getDimensionDescriptionArabic()!=null){
		this.dimensionDescriptionArabic=coaFinancialDimensionsValues.getDimensionDescriptionArabic();
		}
		else
		{
			this.dimensionDescriptionArabic="";
		}
		this.dimInxd=coaFinancialDimensionsValues.getCoaFinancialDimensions().getDimInxd();
		this.dimensionName=coaFinancialDimensionsValues.getCoaFinancialDimensions().getDimensioncolumnname();
	}                                                  
	
	public Long getDimInxValue() {
		return dimInxValue;
	}

	public void setDimInxValue(Long dimInxValue) {
		this.dimInxValue = dimInxValue;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public String getDimensionDescription() {
		return dimensionDescription;
	}

	public void setDimensionDescription(String dimensionDescription) {
		this.dimensionDescription = dimensionDescription;
	}

	public String getDimensionDescriptionArabic() {
		return dimensionDescriptionArabic;
	}

	public void setDimensionDescriptionArabic(String dimensionDescriptionArabic) {
		this.dimensionDescriptionArabic = dimensionDescriptionArabic;
	}

	public int getDimInxd() {
		return dimInxd;
	}

	public void setDimInxd(int dimInxd) {
		this.dimInxd = dimInxd;
	}

	public String getDimensionName() {
		return dimensionName;
	}

	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}

	public int getSegmentNumber() {
		return segmentNumber;
	}

	public void setSegmentNumber(int segmentNumber) {
		this.segmentNumber = segmentNumber;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
}
