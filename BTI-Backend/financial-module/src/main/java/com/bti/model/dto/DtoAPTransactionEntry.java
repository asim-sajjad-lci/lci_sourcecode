package com.bti.model.dto;
import javax.swing.text.Utilities;

import com.bti.constant.MessageLabel;
import com.bti.model.APTransactionEntry;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author BTI
 *
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoAPTransactionEntry {

	private String apTransactionNumber;
	private Integer apTransactionType;
	private String apTransactionDescription;
	private String apTransactionDate;
	private String batchID;
	private String vendorID;
	private String vendorName;
	private String vendorNameArabic;
	private String currencyID;
	private String documentNumberOfVendor;
	private Double purchasesAmount;

	private Double apTransactionTradeDiscount;
	private Double apTransactionFreightAmount;
	private Double apTransactionMiscellaneous;
	private Double apTransactionVATAmount;
	private Double apTransactionFinanceChargeAmount;
	private Double apTransactionCreditMemoAmount;
	private Double apTransactionTotalAmount;
	private Double apTransactionCashAmount;
	private Double apTransactionCheckAmount;
	private Double apTransactionCreditCardAmount;
	private String shippingMethodId;
	private String checkbookID;
	private String cashReceiptNumber;

	private String checkNumber;
	private String creditCardID;
	private String creditCardNumber;
	private String creditCardExpireYear;
	private String creditCardExpireMonth;
	private Integer apTransactionStatus;
	private String exchangeTableIndex;
	private Double exchangeTableRate;
	private boolean transactionVoid;
	private String vatScheduleID;
	private String paymentTermsID;
	private Integer paymentTypeId;
	private String paymentAmount;
	private Boolean isPayment;
	private Double apTransactionDebitMemoAmount;
	private Double apServiceRepairAmount;
	private Double apReturnAmount;
	private Double apTransactionWarrantyAmount;
	private int paymentTransactionTypeId;
	private String messageType;
	private String buttonType;

	public DtoAPTransactionEntry() {
	}

	public DtoAPTransactionEntry(APTransactionEntry apTransactionEntry) {
		this.setApTransactionNumber(apTransactionEntry.getApTransactionNumber());
		this.setApTransactionType(0);
		if(apTransactionEntry.getTransactionType()!=null){
			this.setApTransactionType(apTransactionEntry.getTransactionType().getApDocumentTypeId());
		}
		this.setApTransactionStatus(0);
		if(apTransactionEntry.getApTransactionStatus()!=null){
			this.setApTransactionStatus(apTransactionEntry.getApTransactionStatus().getStatusId());
		}
		
		this.setApTransactionDescription(apTransactionEntry.getApTransactionDescription());
		this.setApTransactionDate(apTransactionEntry.getApTransactionDate() != null
				? UtilDateAndTime.dateToStringddmmyyyy(apTransactionEntry.getApTransactionDate()) : "");
		this.setBatchID(
				apTransactionEntry.getApBatches() != null ? apTransactionEntry.getApBatches().getBatchID() : "");
		this.setVendorID(apTransactionEntry.getVendorID());
		this.setVendorName(apTransactionEntry.getVendorName());
		this.setVendorNameArabic(apTransactionEntry.getVendorNameArabic());
		this.setCurrencyID(apTransactionEntry.getCurrencyID());
		this.setDocumentNumberOfVendor(apTransactionEntry.getDocumentNumberOfVendor());
		this.setPurchasesAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getPurchasesAmount()));

		this.setApTransactionTradeDiscount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionTradeDiscount()));
		this.setApTransactionMiscellaneous(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionMiscellaneous()));
		this.setApTransactionFreightAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionFreightAmount()));
		this.setApTransactionVATAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionVATAmount()));
		this.setApTransactionFinanceChargeAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionFinanceChargeAmount()));
		this.setApTransactionCreditMemoAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionCreditMemoAmount()));
		this.setApTransactionTotalAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionTotalAmount()));
		this.setApTransactionCashAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionCashAmount()));
		this.setApTransactionCheckAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionCheckAmount()));
		this.setApTransactionCreditCardAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionCreditCardAmount()));
        this.setApTransactionDebitMemoAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionDebitMemoAmount()));
		this.setApServiceRepairAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApServiceRepairAmount()));
		this.setApReturnAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApReturnAmount()));
		this.setApTransactionWarrantyAmount(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getApTransactionWarrantyAmount()));
        this.setShippingMethodId(apTransactionEntry.getShippingMethod());
		this.setCheckbookID("");
		this.setCheckNumber("");
		if (UtilRandomKey.isNotBlank(apTransactionEntry.getCheckbookID())) {
			this.setCheckbookID(apTransactionEntry.getCheckbookID());
			this.setCheckNumber(apTransactionEntry.getCheckNumber());
		}

		this.setCreditCardID("");
		this.setCreditCardNumber("");
		this.setCreditCardExpireMonth("");
		this.setCreditCardExpireYear("");
		if (apTransactionEntry.getCreditCardID() != null) {
			this.setCreditCardID(apTransactionEntry.getCreditCardID());
			this.setCreditCardNumber(apTransactionEntry.getCreditCardNumber());
			this.setCreditCardExpireMonth(apTransactionEntry.getCreditCardExpireMonth());
			this.setCreditCardExpireYear(apTransactionEntry.getCreditCardExpireYear());
		}

		this.setApTransactionStatus(apTransactionEntry.getApTransactionStatus() != null
				? apTransactionEntry.getApTransactionStatus().getStatusId() : null);
		this.setCashReceiptNumber(apTransactionEntry.getCashReceiptNumber());
		this.setExchangeTableIndex(apTransactionEntry.getExchangeTableIndex());
		this.setExchangeTableRate(apTransactionEntry.getExchangeTableRate());
		this.setTransactionVoid(apTransactionEntry.isTransactionVoid());
		this.setVatScheduleID("");
		if (UtilRandomKey.isNotBlank(apTransactionEntry.getVatScheduleID())) {
			this.setVatScheduleID(apTransactionEntry.getVatScheduleID());
		}
		this.setPaymentTermsID(apTransactionEntry.getPaymentTermsID());
		this.setPaymentTypeId(0);
		 if(UtilRandomKey.isNotBlank(apTransactionEntry.getPaymentTypeId())){
			 this.setPaymentTypeId(Integer.parseInt(apTransactionEntry.getPaymentTypeId()));
		 }
		 this.setIsPayment(apTransactionEntry.isPayment());
		 this.setPaymentAmount(String.valueOf(UtilRoundDecimal.roundDecimalValue(apTransactionEntry.getPaymentAmount())));
	}

	public String getApTransactionNumber() {
		return apTransactionNumber;
	}

	public void setApTransactionNumber(String apTransactionNumber) {
		this.apTransactionNumber = apTransactionNumber;
	}

	public Integer getApTransactionType() {
		return apTransactionType;
	}

	public void setApTransactionType(Integer apTransactionType) {
		this.apTransactionType = apTransactionType;
	}

	public String getApTransactionDescription() {
		return apTransactionDescription;
	}

	public void setApTransactionDescription(String apTransactionDescription) {
		this.apTransactionDescription = apTransactionDescription;
	}

	public String getApTransactionDate() {
		return apTransactionDate;
	}

	public void setApTransactionDate(String apTransactionDate) {
		this.apTransactionDate = apTransactionDate;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorNameArabic() {
		return vendorNameArabic;
	}

	public void setVendorNameArabic(String vendorNameArabic) {
		this.vendorNameArabic = vendorNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getDocumentNumberOfVendor() {
		return documentNumberOfVendor;
	}

	public void setDocumentNumberOfVendor(String documentNumberOfVendor) {
		this.documentNumberOfVendor = documentNumberOfVendor;
	}

	public Double getPurchasesAmount() {
		return purchasesAmount;
	}

	public void setPurchasesAmount(Double purchasesAmount) {
		this.purchasesAmount = UtilRoundDecimal.roundDecimalValue(purchasesAmount);
	}

	public Double getApTransactionTradeDiscount() {
		return apTransactionTradeDiscount;
	}

	public void setApTransactionTradeDiscount(Double apTransactionTradeDiscount) {
		this.apTransactionTradeDiscount = UtilRoundDecimal.roundDecimalValue(apTransactionTradeDiscount);
	}

	public Double getApTransactionFreightAmount() {
		return apTransactionFreightAmount;
	}

	public void setApTransactionFreightAmount(Double apTransactionFreightAmount) {
		this.apTransactionFreightAmount = UtilRoundDecimal.roundDecimalValue(apTransactionFreightAmount);
	}

	public Double getApTransactionMiscellaneous() {
		return apTransactionMiscellaneous;
	}

	public void setApTransactionMiscellaneous(Double apTransactionMiscellaneous) {
		this.apTransactionMiscellaneous = UtilRoundDecimal.roundDecimalValue(apTransactionMiscellaneous);
	}

	public Double getApTransactionVATAmount() {
		return apTransactionVATAmount;
	}

	public void setApTransactionVATAmount(Double apTransactionVATAmount) {
		this.apTransactionVATAmount = UtilRoundDecimal.roundDecimalValue(apTransactionVATAmount);
	}

	public Double getApTransactionFinanceChargeAmount() {
		return apTransactionFinanceChargeAmount;
	}

	public void setApTransactionFinanceChargeAmount(Double apTransactionFinanceChargeAmount) {
		this.apTransactionFinanceChargeAmount = UtilRoundDecimal.roundDecimalValue(apTransactionFinanceChargeAmount);
	}

	public Double getApTransactionCreditMemoAmount() {
		return apTransactionCreditMemoAmount;
	}

	public void setApTransactionCreditMemoAmount(Double apTransactionCreditMemoAmount) {
		this.apTransactionCreditMemoAmount = UtilRoundDecimal.roundDecimalValue(apTransactionCreditMemoAmount);
	}

	public Double getApTransactionTotalAmount() {
		return apTransactionTotalAmount;
	}

	public void setApTransactionTotalAmount(Double apTransactionTotalAmount) {
		this.apTransactionTotalAmount = UtilRoundDecimal.roundDecimalValue(apTransactionTotalAmount);
	}

	public Double getApTransactionCashAmount() {
		return apTransactionCashAmount;
	}

	public void setApTransactionCashAmount(Double apTransactionCashAmount) {
		this.apTransactionCashAmount = UtilRoundDecimal.roundDecimalValue(apTransactionCashAmount);
	}

	public Double getApTransactionCheckAmount() {
		return apTransactionCheckAmount;
	}

	public void setApTransactionCheckAmount(Double apTransactionCheckAmount) {
		this.apTransactionCheckAmount = UtilRoundDecimal.roundDecimalValue(apTransactionCheckAmount);
	}

	public Double getApTransactionCreditCardAmount() {
		return apTransactionCreditCardAmount;
	}

	public void setApTransactionCreditCardAmount(Double apTransactionCreditCardAmount) {
		this.apTransactionCreditCardAmount = UtilRoundDecimal.roundDecimalValue(apTransactionCreditCardAmount);
	}
	

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(String creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(String creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public Integer getApTransactionStatus() {
		return apTransactionStatus;
	}

	public void setApTransactionStatus(Integer apTransactionStatus) {
		this.apTransactionStatus = apTransactionStatus;
	}
	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = UtilRoundDecimal.roundDecimalValue(exchangeTableRate);
	}


	public boolean isTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(boolean transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getVatScheduleID() {
		return vatScheduleID;
	}

	public void setVatScheduleID(String vatScheduleID) {
		this.vatScheduleID = vatScheduleID;
	}

	public String getPaymentTermsID() {
		return paymentTermsID;
	}

	public void setPaymentTermsID(String paymentTermsID) {
		this.paymentTermsID = paymentTermsID;
	}

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public Integer getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(Integer paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	public String getPaymentAmount() {
		if(paymentAmount==null ||paymentAmount==""){
			return "0.0";
		}
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Boolean getIsPayment() {
		return isPayment;
	}

	public void setIsPayment(Boolean isPayment) {
		this.isPayment = isPayment;
	}

	public String getShippingMethodId() {
		return shippingMethodId;
	}

	public void setShippingMethodId(String shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}

	public Double getApTransactionDebitMemoAmount() {
		return apTransactionDebitMemoAmount;
	}

	public void setApTransactionDebitMemoAmount(Double apTransactionDebitMemoAmount) {
		this.apTransactionDebitMemoAmount = UtilRoundDecimal.roundDecimalValue(apTransactionDebitMemoAmount);
	}

	public Double getApServiceRepairAmount() {
		return apServiceRepairAmount;
	}

	public void setApServiceRepairAmount(Double apServiceRepairAmount) {
		this.apServiceRepairAmount = UtilRoundDecimal.roundDecimalValue(apServiceRepairAmount);
	}

	public Double getApReturnAmount() {
		return apReturnAmount;
	}

	public void setApReturnAmount(Double apReturnAmount) {
		this.apReturnAmount = UtilRoundDecimal.roundDecimalValue(apReturnAmount);
	}

	public Double getApTransactionWarrantyAmount() {
		return apTransactionWarrantyAmount;
	}

	public void setApTransactionWarrantyAmount(Double apTransactionWarrantyAmount) {
		this.apTransactionWarrantyAmount = UtilRoundDecimal.roundDecimalValue(apTransactionWarrantyAmount);
	}

	public int getPaymentTransactionTypeId() {
		return paymentTransactionTypeId;
	}

	public void setPaymentTransactionTypeId(int paymentTransactionTypeId) {
		this.paymentTransactionTypeId = paymentTransactionTypeId;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}
}
