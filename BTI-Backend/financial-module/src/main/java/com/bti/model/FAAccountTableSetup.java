/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the fa00102 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa00102")
@NamedQuery(name="FAAccountTableSetup.findAll", query="SELECT f FROM FAAccountTableSetup f")
public class FAAccountTableSetup implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	/*@Column(name="ACCGROPINXD")
	private int accountGroupIndex;*/
	
	//bi-directional one-to-one association to fa40100
	@OneToOne
	@JoinColumn(name="ACCGROPINXD")
	private FAAccountGroupsSetup faAccountGroupsSetup;

	/*@Column(name="ACCTCTTYPID")
	private int accountType;*/
	
	//bi-directional one-to-one association to fa_account_table_account_type
	@OneToOne
	@JoinColumn(name="ACCTCTTYPID")
	private FAAccountTableAccountType faAccountTableAccountType;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional one-to-one association to Fa00101
	@OneToOne
	@JoinColumn(name="ASSTID")
	private FAGeneralMaintenance faGeneralMaintenance;

	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;

	public FAAccountTableSetup() {
	}

	/*public int getAccountGroupIndex() {
		return accountGroupIndex;
	}

	public void setAccountGroupIndex(int accountGroupIndex) {
		this.accountGroupIndex = accountGroupIndex;
	}*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FAAccountTableAccountType getFaAccountTableAccountType() {
		return faAccountTableAccountType;
	}

	public void setFaAccountTableAccountType(FAAccountTableAccountType faAccountTableAccountType) {
		this.faAccountTableAccountType = faAccountTableAccountType;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public FAGeneralMaintenance getFaGeneralMaintenance() {
		return faGeneralMaintenance;
	}

	public void setFaGeneralMaintenance(FAGeneralMaintenance faGeneralMaintenance) {
		this.faGeneralMaintenance = faGeneralMaintenance;
	}

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public FAAccountGroupsSetup getFaAccountGroupsSetup() {
		return faAccountGroupsSetup;
	}

	public void setFaAccountGroupsSetup(FAAccountGroupsSetup faAccountGroupsSetup) {
		this.faAccountGroupsSetup = faAccountGroupsSetup;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}