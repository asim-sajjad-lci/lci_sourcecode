/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.ShipmentMethodSetup;
import com.bti.model.ShipmentMethodType;
/**
 * Description: DtoShipmentMethodSetup class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 10, 2017
 * Modified on: AUG 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

public class DtoShipmentMethodSetup {
	private String shipmentMethodId;
	private String shipmentDescription;
	private String shipmentDescriptionArabic;
	private String shipmentCarrierAccount;
	private String shipmentCarrier;
	private String shipmentCarrierArabic;
	private String shipmentCarrierContactName;
	private String shipmentPhoneNumber;
	private Integer shipmentMethodType;
	private String messageType;
	private List<String> shipmentMethodIds;
	
	DtoShipmentMethodSetup(){
		
	}
	
	public DtoShipmentMethodSetup(ShipmentMethodSetup shipmentMethodSetup,ShipmentMethodType shipmentMethodType){
	this.shipmentMethodId = shipmentMethodSetup.getShipmentMethodId();
	this.shipmentDescription = shipmentMethodSetup.getShipmentDescription();
	this.shipmentDescriptionArabic = shipmentMethodSetup.getShipmentDescriptionArabic();
	this.shipmentCarrierAccount = shipmentMethodSetup.getShipmentCarrierAccount();
	this.shipmentCarrier = shipmentMethodSetup.getShipmentCarrier();
	this.shipmentCarrierArabic = shipmentMethodSetup.getShipmentCarrierArabic();
	this.shipmentCarrierContactName = shipmentMethodSetup.getShipmentCarrierContactName();
	this.shipmentPhoneNumber = shipmentMethodSetup.getShipmentPhoneNumber();
	this.shipmentMethodType = shipmentMethodType!=null?shipmentMethodType.getTypeId():null;
	
	}
	
	public String getShipmentMethodId() {
		return shipmentMethodId;
	}
	public void setShipmentMethodId(String shipmentMethodId) {
		this.shipmentMethodId = shipmentMethodId;
	}
	public String getShipmentDescription() {
		return shipmentDescription;
	}
	public void setShipmentDescription(String shipmentDescription) {
		this.shipmentDescription = shipmentDescription;
	}
	public String getShipmentCarrierAccount() {
		return shipmentCarrierAccount;
	}
	public void setShipmentCarrierAccount(String shipmentCarrierAccount) {
		this.shipmentCarrierAccount = shipmentCarrierAccount;
	}
	public String getShipmentCarrier() {
		return shipmentCarrier;
	}
	public void setShipmentCarrier(String shipmentCarrier) {
		this.shipmentCarrier = shipmentCarrier;
	}
	public String getShipmentCarrierArabic() {
		return shipmentCarrierArabic;
	}
	public void setShipmentCarrierArabic(String shipmentCarrierArabic) {
		this.shipmentCarrierArabic = shipmentCarrierArabic;
	}
	public String getShipmentCarrierContactName() {
		return shipmentCarrierContactName;
	}
	public void setShipmentCarrierContactName(String shipmentCarrierContactName) {
		this.shipmentCarrierContactName = shipmentCarrierContactName;
	}
	public String getShipmentPhoneNumber() {
		return shipmentPhoneNumber;
	}
	public void setShipmentPhoneNumber(String shipmentPhoneNumber) {
		this.shipmentPhoneNumber = shipmentPhoneNumber;
	}
	public Integer getShipmentMethodType() {
		return shipmentMethodType;
	}
	public void setShipmentMethodType(Integer shipmentMethodType) {
		this.shipmentMethodType = shipmentMethodType;
	}

	public String getShipmentDescriptionArabic() {
		return shipmentDescriptionArabic;
	}

	public void setShipmentDescriptionArabic(String shipmentDescriptionArabic) {
		this.shipmentDescriptionArabic = shipmentDescriptionArabic;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public List<String> getShipmentMethodIds() {
		return shipmentMethodIds;
	}

	public void setShipmentMethodIds(List<String> shipmentMethodIds) {
		this.shipmentMethodIds = shipmentMethodIds;
	}

 
	
	
	
}
