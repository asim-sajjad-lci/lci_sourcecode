/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;
import com.bti.model.JournalEntryDetails;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DtoAccountCategory class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 10, 2017
 * Modified on: AUG 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoGLReportDetail {
	
	private String debitAmount;
	private String creditAmount;
    private int journalSequence;
    private String accountNumber;
    private String accountDescription;
    
	public DtoGLReportDetail() {
		// TO DO Auto-generated constructor stub
	}
	
	public DtoGLReportDetail(JournalEntryDetails journalEntryDetails) 
	{
		this.journalSequence=journalEntryDetails.getJournalSequence();
		this.creditAmount=String.valueOf(UtilRoundDecimal.roundDecimalValue(journalEntryDetails.getOriginalCreditAmount()));
		this.debitAmount=String.valueOf(UtilRoundDecimal.roundDecimalValue(journalEntryDetails.getOriginalDebitAmount()));
	}

	public String getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}

	public String getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}

	public int getJournalSequence() {
		return journalSequence;
	}

	public void setJournalSequence(int journalSequence) {
		this.journalSequence = journalSequence;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	
	 
}
