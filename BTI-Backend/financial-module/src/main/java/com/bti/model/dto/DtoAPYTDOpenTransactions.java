package com.bti.model.dto;

import java.util.List;

import com.bti.constant.ARTransactionTypeConstant;
import com.bti.model.APYTDOpenTransaction;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoAPYTDOpenTransactions {

	private String apTransactionNumber;
	private String vendorID;
	private String apTransactionType;
	private String apTransactionDescription;
	private String batchID;
	private String apTransactionDate;
	private String vendorName;
	private String vendorNameArabic;
	private String currencyID;
	private String documentNumberOfVendor;
	private Double purchasesAmount;
	private Double apTransactionTradeDiscount;
	private Double apTransactionFreightAmount;
	private Double apTransactionMiscellaneous;
	private Double apTransactionVATAmount;
	private Double apTransactionFinanceChargeAmount;
	private Double apTransactionCreditMemoAmount;
	private Double apTransactionTotalAmount;
	private Double apTransactionCashAmount;
	private Double apTransactionCheckAmount;
	private Double apTransactionCreditCardAmount;
	private String shippingMethod;
	private String checkbookID;
	private String cashReceiptNumber;
	private String checkNumber;
	private String creditCardID;
	private String creditCardNumber;
	private String creditCardExpireYear;
	private String creditCardExpireMonth;
	private String exchangeTableID;
	private Double exchangeTableRate;
	private Boolean transactionVoid;
	private String vatScheduleID;
	private String paymentTermsID;
	private String postingDate;
	private String postingByUserID;
	private Double apTransactionDebitMemoAmount;
	private Double apTransactionWarrantyAmount;
	private Boolean completePaymentApplied;
	private String dueDate;
	private Double applyAmount;
	private Double remainingAmount;
	private Double originalAmount;
	private Double functionalAmount;
	private List<DtoAPYTDOpenTransactions> apOpenTransactionList;
	private Integer pageNumber;
	private Integer pageSize;
	private String paymentNumber;
	private int documentType;
	private String applyDate;

	public DtoAPYTDOpenTransactions() {
		// TODO Auto-generated constructor stub
	}
	
	public DtoAPYTDOpenTransactions(APYTDOpenTransaction apYTDOpenTransaction) {
		this.apTransactionNumber = apYTDOpenTransaction.getApTransactionNumber();
		this.vendorID = apYTDOpenTransaction.getVendorID()!=null?apYTDOpenTransaction.getVendorID():"";
		this.vendorName = apYTDOpenTransaction.getVendorName()!=null?apYTDOpenTransaction.getVendorName():"";
		this.vendorNameArabic = apYTDOpenTransaction.getVendorNameArabic();
		this.apTransactionType = "";
		this.apTransactionTotalAmount = UtilRoundDecimal.roundDecimalValue(apYTDOpenTransaction.getApTransactionTotalAmount());
		this.applyAmount=0.0;
		this.remainingAmount= UtilRoundDecimal.roundDecimalValue(apYTDOpenTransaction.getApTransactionTotalAmount());
		this.dueDate="";
	}

	public String getApTransactionNumber() {
		return apTransactionNumber;
	}

	public void setApTransactionNumber(String apTransactionNumber) {
		this.apTransactionNumber = apTransactionNumber;
	}

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public String getApTransactionType() {
		return apTransactionType;
	}

	public void setApTransactionType(String apTransactionType) {
		this.apTransactionType = apTransactionType;
	}

	public String getApTransactionDescription() {
		return apTransactionDescription;
	}

	public void setApTransactionDescription(String apTransactionDescription) {
		this.apTransactionDescription = apTransactionDescription;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getApTransactionDate() {
		return apTransactionDate;
	}

	public void setApTransactionDate(String apTransactionDate) {
		this.apTransactionDate = apTransactionDate;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorNameArabic() {
		return vendorNameArabic;
	}

	public void setVendorNameArabic(String vendorNameArabic) {
		this.vendorNameArabic = vendorNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getDocumentNumberOfVendor() {
		return documentNumberOfVendor;
	}

	public void setDocumentNumberOfVendor(String documentNumberOfVendor) {
		this.documentNumberOfVendor = documentNumberOfVendor;
	}

	public Double getPurchasesAmount() {
		return purchasesAmount;
	}

	public void setPurchasesAmount(Double purchasesAmount) {
		this.purchasesAmount = UtilRoundDecimal.roundDecimalValue(purchasesAmount);
	}

	public Double getApTransactionTradeDiscount() {
		return apTransactionTradeDiscount;
	}

	public void setApTransactionTradeDiscount(Double apTransactionTradeDiscount) {
		this.apTransactionTradeDiscount = UtilRoundDecimal.roundDecimalValue(apTransactionTradeDiscount);
	}

	public Double getApTransactionFreightAmount() {
		return apTransactionFreightAmount;
	}

	public void setApTransactionFreightAmount(Double apTransactionFreightAmount) {
		this.apTransactionFreightAmount = UtilRoundDecimal.roundDecimalValue(apTransactionFreightAmount);
	}

	public Double getApTransactionMiscellaneous() {
		return apTransactionMiscellaneous;
	}

	public void setApTransactionMiscellaneous(Double apTransactionMiscellaneous) {
		this.apTransactionMiscellaneous = UtilRoundDecimal.roundDecimalValue(apTransactionMiscellaneous);
	}

	public Double getApTransactionVATAmount() {
		return apTransactionVATAmount;
	}

	public void setApTransactionVATAmount(Double apTransactionVATAmount) {
		this.apTransactionVATAmount = UtilRoundDecimal.roundDecimalValue(apTransactionVATAmount);
	}

	public Double getApTransactionFinanceChargeAmount() {
		return apTransactionFinanceChargeAmount;
	}

	public void setApTransactionFinanceChargeAmount(Double apTransactionFinanceChargeAmount) {
		this.apTransactionFinanceChargeAmount = UtilRoundDecimal.roundDecimalValue(apTransactionFinanceChargeAmount);
	}

	public Double getApTransactionCreditMemoAmount() {
		return apTransactionCreditMemoAmount;
	}

	public void setApTransactionCreditMemoAmount(Double apTransactionCreditMemoAmount) {
		this.apTransactionCreditMemoAmount = UtilRoundDecimal.roundDecimalValue(apTransactionCreditMemoAmount);
	}

	public Double getApTransactionTotalAmount() {
		return apTransactionTotalAmount;
	}

	public void setApTransactionTotalAmount(Double apTransactionTotalAmount) {
		this.apTransactionTotalAmount = UtilRoundDecimal.roundDecimalValue(apTransactionTotalAmount);
	}

	public Double getApTransactionCashAmount() {
		return apTransactionCashAmount;
	}

	public void setApTransactionCashAmount(Double apTransactionCashAmount) {
		this.apTransactionCashAmount = UtilRoundDecimal.roundDecimalValue(apTransactionCashAmount);
	}

	public Double getApTransactionCheckAmount() {
		return apTransactionCheckAmount;
	}

	public void setApTransactionCheckAmount(Double apTransactionCheckAmount) {
		this.apTransactionCheckAmount = UtilRoundDecimal.roundDecimalValue(apTransactionCheckAmount);
	}

	public Double getApTransactionCreditCardAmount() {
		return apTransactionCreditCardAmount;
	}

	public void setApTransactionCreditCardAmount(Double apTransactionCreditCardAmount) {
		this.apTransactionCreditCardAmount = UtilRoundDecimal.roundDecimalValue(apTransactionCreditCardAmount);
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(String creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(String creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public String getExchangeTableID() {
		return exchangeTableID;
	}

	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = UtilRoundDecimal.roundDecimalValue(exchangeTableRate);
	}

	public Boolean getTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(Boolean transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getVatScheduleID() {
		return vatScheduleID;
	}

	public void setVatScheduleID(String vatScheduleID) {
		this.vatScheduleID = vatScheduleID;
	}

	public String getPaymentTermsID() {
		return paymentTermsID;
	}

	public void setPaymentTermsID(String paymentTermsID) {
		this.paymentTermsID = paymentTermsID;
	}

	public String getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}

	public String getPostingByUserID() {
		return postingByUserID;
	}

	public void setPostingByUserID(String postingByUserID) {
		this.postingByUserID = postingByUserID;
	}

	public Double getApTransactionDebitMemoAmount() {
		return apTransactionDebitMemoAmount;
	}

	public void setApTransactionDebitMemoAmount(Double apTransactionDebitMemoAmount) {
		this.apTransactionDebitMemoAmount = UtilRoundDecimal.roundDecimalValue(apTransactionDebitMemoAmount);
	}

	public Double getApTransactionWarrantyAmount() {
		return apTransactionWarrantyAmount;
	}

	public void setApTransactionWarrantyAmount(Double apTransactionWarrantyAmount) {
		this.apTransactionWarrantyAmount = UtilRoundDecimal.roundDecimalValue(apTransactionWarrantyAmount);
	}

	public Boolean getCompletePaymentApplied() {
		return completePaymentApplied;
	}

	public void setCompletePaymentApplied(Boolean completePaymentApplied) {
		this.completePaymentApplied = completePaymentApplied;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = UtilRoundDecimal.roundDecimalValue(remainingAmount);
	}

	public Double getApplyAmount() {
		return applyAmount;
	}

	public void setApplyAmount(Double applyAmount) {
		this.applyAmount = UtilRoundDecimal.roundDecimalValue(applyAmount);
	}

	public List<DtoAPYTDOpenTransactions> getApOpenTransactionList() {
		return apOpenTransactionList;
	}

	public void setApOpenTransactionList(List<DtoAPYTDOpenTransactions> apOpenTransactionList) {
		this.apOpenTransactionList = apOpenTransactionList;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public Double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(Double originalAmount) {
		this.originalAmount = UtilRoundDecimal.roundDecimalValue(originalAmount);
	}

	public Double getFunctionalAmount() {
		return functionalAmount;
	}

	public void setFunctionalAmount(Double functionalAmount) {
		this.functionalAmount = UtilRoundDecimal.roundDecimalValue(functionalAmount);
	}

	public int getDocumentType() {
		return documentType;
	}

	public void setDocumentType(int documentType) {
		this.documentType = documentType;
	}

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	

}
