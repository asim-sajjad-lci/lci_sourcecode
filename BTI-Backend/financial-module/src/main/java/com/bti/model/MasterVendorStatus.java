/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the master_vendor_status database table.
 * 
 */
@Entity
@Table(name="master_vendor_status")
@NamedQuery(name="MasterVendorStatus.findAll", query="SELECT m FROM MasterVendorStatus m")
public class MasterVendorStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private short id;

	@Column(name="STATUS_ID")
	private short statusId;

	@Column(name="STATUS_PRIMARY")
	private String statusPrimary;

	/*@Column(name="STATUS_SECONDARY")
	private String statusSecondary;*/
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ap00101
	/*@OneToMany(mappedBy="masterVendorStatus")
	private List<VendorMaintenance> ap00101s;*/
	
	@ManyToOne
	@JoinColumn(name="lang_Id")
	private Language language;

	public MasterVendorStatus() {
	}

	public short getId() {
		return this.id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public short getStatusId() {
		return this.statusId;
	}

	public void setStatusId(short statusId) {
		this.statusId = statusId;
	}

	public String getStatusPrimary() {
		return this.statusPrimary;
	}

	public void setStatusPrimary(String statusPrimary) {
		this.statusPrimary = statusPrimary;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/*public List<VendorMaintenance> getAp00101s() {
		return this.ap00101s;
	}

	public void setAp00101s(List<VendorMaintenance> ap00101s) {
		this.ap00101s = ap00101s;
	}*/

	/*public VendorMaintenance addAp00101(VendorMaintenance ap00101) {
		getAp00101s().add(ap00101);
		ap00101.setMasterVendorStatus(this);

		return ap00101;
	}

	public VendorMaintenance removeAp00101(VendorMaintenance ap00101) {
		getAp00101s().remove(ap00101);
		ap00101.setMasterVendorStatus(null);

		return ap00101;
	}*/
	
	

}