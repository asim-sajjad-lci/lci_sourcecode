/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the gl40006 database table.
 * 
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40006")
@NamedQuery(name = "DocumentsTypeSetup.findAll", query = "SELECT a FROM DocumentsTypeSetup a")
public class DocumentsTypeSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "GLDOCTYPE")
	private int glDocumentTypeID;

	@Column(name = "DOCDESCR")
	private String documentTypeDescription;

	@Column(name = "DOCDESCRA")
	private String documentTypeDescriptionArabic;

	@Column(name = "DOCODE")
	private String documentCode;

	@Column(name = "DOCNUMBR")
	private int documentNumber;

	@Column(name = "CHANGEBY")
	private String modifyByUserID;

	@Column(name = "DEX_ROW_ID")
	private int rowIndexId;

	@Column(name = "DEX_ROW_TS")
	private Date rowDateIndex;

	public int getGlDocumentTypeID() {
		return glDocumentTypeID;
	}

	public void setGlDocumentTypeID(int glDocumentTypeID) {
		this.glDocumentTypeID = glDocumentTypeID;
	}

	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}

	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	public String getDocumentTypeDescriptionArabic() {
		return documentTypeDescriptionArabic;
	}

	public void setDocumentTypeDescriptionArabic(String documentTypeDescriptionArabic) {
		this.documentTypeDescriptionArabic = documentTypeDescriptionArabic;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public int getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(int documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}