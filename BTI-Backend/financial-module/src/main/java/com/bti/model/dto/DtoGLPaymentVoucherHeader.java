package com.bti.model.dto;

import java.util.List;

import com.bti.model.GLPaymentVoucherHeader;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoGLPaymentVoucherHeader {
	

	private int id;
	
	private List<Integer> ids;
	
	private int paymentVoucherID;
	
	private String glBatcheId;
	
	private String paymentTypeID;
	
	private String transactionDate;
	
	private String accountTableRowIndex;
	
	private String accountNumber;
	
	private int accountType;
	
	private int  sourceDcoument;
	
	private String voucherDescription;
	
	private String voucherDescriptionArabic;
	
	private String currencyID;
	
	private double totalPaymentVoucher;
	
	private double totalPaymentVoucherDebit;
	
	private double totalPaymentVoucherCredit;

	private double originalTotalPaymentVoucher;
	
	private double originalTotalPaymentVoucherDebit;
	
	private double originalTotalPaymentVoucherCredit;
	
	private int  exchangeTableIndex;
	
	private String exchangeRate;
	
	private String interCompany;
	
	private String messageType;
	
	private List<DtoGLPaymentVoucherDetail> voucherDetailList;
	
	private String moduleSeriesNumber;
	
	private int balanceType;
	
	private String checkbookID;
	
	private int checkNumber;
	
	private Integer auditTrial;
	
	private String creditCardId;
	
	private Integer creditCardExpireYear;
	private Integer creditCardExpireMonth;
	private String creditCardNumber;
	private String bankName;
	

	public DtoGLPaymentVoucherHeader() {
		// TODO Auto-generated constructor stub
		this.accountTableRowIndex="";
		this.exchangeRate = "";
		this.transactionDate = "";
	}
	
	public DtoGLPaymentVoucherHeader(GLPaymentVoucherHeader glPaymentVoucherHeader) {
		this.id = glPaymentVoucherHeader.getId();
		this.paymentVoucherID = glPaymentVoucherHeader.getPaymentVoucherId();
		this.paymentTypeID = glPaymentVoucherHeader.getPaymentMethodType().getTypeId()+"";
		this.accountTableRowIndex="";
		this.accountTableRowIndex = glPaymentVoucherHeader.getAccountTableRowIndex();
		this.balanceType = glPaymentVoucherHeader.getBalanceType();
		this.accountType = glPaymentVoucherHeader.getAccountType();
		this.interCompany = glPaymentVoucherHeader.getIntercompanyIDMultiCompanyTransaction();
		this.exchangeRate = glPaymentVoucherHeader.getExchangeTableRate()+"";
		this.currencyID = glPaymentVoucherHeader.getCurrencySetup().getCurrencyId();
		this.exchangeRate = "";
		this.exchangeTableIndex = glPaymentVoucherHeader.getCurrencyExchangeHeader().getExchangeIndex();
		this.voucherDescription = glPaymentVoucherHeader.getVoucherDescription();
		this.voucherDescriptionArabic = glPaymentVoucherHeader.getVoucherDescriptionArabic();
		this.totalPaymentVoucher = glPaymentVoucherHeader.getTotalVoucherEntry();
		this.totalPaymentVoucherCredit = glPaymentVoucherHeader.getTotalVoucherCredit();
		this.totalPaymentVoucherDebit = glPaymentVoucherHeader.getTotalVoucherDebit();
		this.originalTotalPaymentVoucher = glPaymentVoucherHeader.getOriginalTotalVoucherEntry();
		this.originalTotalPaymentVoucherCredit = glPaymentVoucherHeader.getOriginalTotlalVoucherCredit();
		this.originalTotalPaymentVoucherDebit = glPaymentVoucherHeader.getOriginalTotalVoucherDebit();
		this.transactionDate = "";
		this.transactionDate = glPaymentVoucherHeader.getTransactionDate().toString();
		this.glBatcheId = "";
		this.glBatcheId = glPaymentVoucherHeader.getGlBatches().getBatchID();
		if(glPaymentVoucherHeader.getCheckbookMaintenance() != null ) {
			this.checkbookID = "";
			this.checkbookID = glPaymentVoucherHeader.getCheckbookMaintenance().getCheckBookId();
			this.checkNumber = glPaymentVoucherHeader.getCheckNumber();
		}else if (glPaymentVoucherHeader.getPaymentMethodType().getIdindex() == 3) {
			this.creditCardExpireMonth = glPaymentVoucherHeader.getCreditCardExpireMonth();
			this.creditCardExpireYear = glPaymentVoucherHeader.getCreditCardExpireYear();
			this.creditCardId = glPaymentVoucherHeader.getCreditCardID();
			this.creditCardNumber = glPaymentVoucherHeader.getCreditCardNumber();
			
		}
		this.auditTrial = glPaymentVoucherHeader.getGlConfigurationAuditTrialCodes() != null?
				glPaymentVoucherHeader.getGlConfigurationAuditTrialCodes().getSeriesIndex() : 0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPaymentVoucherID() {
		return paymentVoucherID;
	}

	public void setPaymentVoucherID(int paymentVoucherID) {
		this.paymentVoucherID = paymentVoucherID;
	}

	public String getGlBatcheId() {
		return glBatcheId;
	}

	public void setGlBatcheId(String glBatcheId) {
		this.glBatcheId = glBatcheId;
	}



	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public int getSourceDcoument() {
		return sourceDcoument;
	}

	public void setSourceDcoument(int sourceDcoument) {
		this.sourceDcoument = sourceDcoument;
	}

	public String getVoucherDescription() {
		return voucherDescription;
	}

	public void setVoucherDescription(String voucherDescription) {
		this.voucherDescription = voucherDescription;
	}

	public String getVoucherDescriptionArabic() {
		return voucherDescriptionArabic;
	}

	public void setVoucherDescriptionArabic(String voucherDescriptionArabic) {
		this.voucherDescriptionArabic = voucherDescriptionArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public double getTotalPaymentVoucher() {
		return totalPaymentVoucher;
	}

	public void setTotalPaymentVoucher(double totalPaymentVoucher) {
		this.totalPaymentVoucher = totalPaymentVoucher;
	}

	public double getTotalPaymentVoucherDebit() {
		return totalPaymentVoucherDebit;
	}

	public void setTotalPaymentVoucherDebit(double totalPaymentVoucherDebit) {
		this.totalPaymentVoucherDebit = totalPaymentVoucherDebit;
	}

	public double getTotalPaymentVoucherCredit() {
		return totalPaymentVoucherCredit;
	}

	public void setTotalPaymentVoucherCredit(double totalPaymentVoucherCredit) {
		this.totalPaymentVoucherCredit = totalPaymentVoucherCredit;
	}

	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public String isInterCompany() {
		return interCompany;
	}

	public void setInterCompany(String interCompany) {
		this.interCompany = interCompany;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public List<DtoGLPaymentVoucherDetail> getVoucherDetailList() {
		return voucherDetailList;
	}

	public void setVoucherDetailList(List<DtoGLPaymentVoucherDetail> voucherDetailList) {
		this.voucherDetailList = voucherDetailList;
	}

	public String getModuleSeriesNumber() {
		return moduleSeriesNumber;
	}

	public void setModuleSeriesNumber(String moduleSeriesNumber) {
		this.moduleSeriesNumber = moduleSeriesNumber;
	}

	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public String getPaymentTypeID() {
		return paymentTypeID;
	}

	public void setPaymentTypeID(String paymentTypeID) {
		this.paymentTypeID = paymentTypeID;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public double getOriginalTotalPaymentVoucherDebit() {
		return originalTotalPaymentVoucherDebit;
	}

	public void setOriginalTotalPaymentVoucherDebit(double originalTotalPaymentVoucherDebit) {
		this.originalTotalPaymentVoucherDebit = originalTotalPaymentVoucherDebit;
	}

	public double getOriginalTotalPaymentVoucherCredit() {
		return originalTotalPaymentVoucherCredit;
	}

	public void setOriginalTotalPaymentVoucherCredit(double originalTotalPaymentVoucherCredit) {
		this.originalTotalPaymentVoucherCredit = originalTotalPaymentVoucherCredit;
	}

	public String getExchangeRate() {
		return exchangeRate;
		
	}

	public void setExchangeRate(String exchangeTableRate) {
		this.exchangeRate = exchangeTableRate;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public double getOriginalTotalPaymentVoucher() {
		return originalTotalPaymentVoucher;
	}

	public void setOriginalTotalPaymentVoucher(double originalTotalPaymentVoucher) {
		this.originalTotalPaymentVoucher = originalTotalPaymentVoucher;
	}

	public String getInterCompany() {
		return interCompany;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public int getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(int checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Integer getAuditTrial() {
		return auditTrial;
	}

	public void setAuditTrial(Integer auditTrial) {
		this.auditTrial = auditTrial;
	}

	public String getCreditCardId() {
		return creditCardId;
	}

	public void setCreditCardId(String creditCardId) {
		this.creditCardId = creditCardId;
	}

	public Integer getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(Integer creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public Integer getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(Integer creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	
	
}
