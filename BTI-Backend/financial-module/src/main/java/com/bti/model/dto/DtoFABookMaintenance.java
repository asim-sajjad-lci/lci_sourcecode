/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FABookMaintenance;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRoundDecimal;

public class DtoFABookMaintenance {
	
	private String assetId;
	private Integer amortizationCode;
	private double amortizationAmount;
	private Integer averagingConvention;
	private double beginYearCost;
	private String bookId;
	private double costBasis;
	private double currentDepreciation;
	private String depreciatedDate;
	private String fullDepreciationFlag;
	private double lifeToDateDepreciation;
	private double netFAValue;
	private int originalLifeDay;
	private int originalLifeYear;
	private String placedInServiceDate;
	private int remainingLifeDay;
	private int remainingLifeYear;
	private double salvageAmount;
	private Integer switchOver;
	private double yearlyDepreciatedRate;
	private double yearToDateDepreciation;
	private Integer depreciationMethodId;
	private String messageType;
	private String status;
	
	public DtoFABookMaintenance() {
		
	}
	
	public DtoFABookMaintenance(FABookMaintenance faBookMaintenance) 
	{
		
		this.setAmortizationAmount(UtilRoundDecimal.roundDecimalValue(faBookMaintenance.getAmortizationAmount()));
		this.setAmortizationCode(faBookMaintenance.getAmortizationCodeType()!=null?faBookMaintenance.getAmortizationCodeType().getTypeId():null);
		this.setAssetId(faBookMaintenance.getAssetId());
		this.setAveragingConvention(faBookMaintenance.getAveragingConventionType()!=null?faBookMaintenance.getAveragingConventionType().getTypeId():null);
		this.setBeginYearCost(UtilRoundDecimal.roundDecimalValue(faBookMaintenance.getBeginYearCost()));
		this.setBookId(faBookMaintenance.getBookSetup()!=null?faBookMaintenance.getBookSetup().getBookId():null);
		this.setCostBasis(UtilRoundDecimal.roundDecimalValue(faBookMaintenance.getCostBasis()));
		this.setCurrentDepreciation(UtilRoundDecimal.roundDecimalValue(faBookMaintenance.getCurrentDepreciation()));
		if(faBookMaintenance.getDepreciatedDate()!=null){
			this.setDepreciatedDate(UtilDateAndTime.dateToStringddmmyyyy(faBookMaintenance.getDepreciatedDate()));
		}
		
		this.setFullDepreciationFlag(faBookMaintenance.getFullDepreciationFlag());
		this.setNetFAValue(UtilRoundDecimal.roundDecimalValue(faBookMaintenance.getNetFAValue()));
		this.setOriginalLifeDay(faBookMaintenance.getOriginalLifeDay());
		this.setOriginalLifeYear(faBookMaintenance.getOriginalLifeYear());
		if(faBookMaintenance.getPlacedInServiceDate()!=null){
			this.setPlacedInServiceDate(UtilDateAndTime.dateToStringddmmyyyy(faBookMaintenance.getPlacedInServiceDate()));
		}
		this.setRemainingLifeDay(faBookMaintenance.getRemainingLifeDay());
		this.setRemainingLifeYear(faBookMaintenance.getRemainingLifeYear());
		this.setSalvageAmount(UtilRoundDecimal.roundDecimalValue(faBookMaintenance.getSalvageAmount()));
		this.setSwitchOver(faBookMaintenance.getSwitchOverType()!=null?faBookMaintenance.getSwitchOverType().getTypeId():null);
		this.setYearlyDepreciatedRate(UtilRoundDecimal.roundDecimalValue(faBookMaintenance.getYearlyDepreciatedRate()));
		this.setYearToDateDepreciation(UtilRoundDecimal.roundDecimalValue(faBookMaintenance.getYearToDateDepreciation()));
		this.setStatus(faBookMaintenance.getStaus());
		this.setDepreciationMethodId(faBookMaintenance.getFaDepreciationMethods()!=null?faBookMaintenance.getFaDepreciationMethods().getDepreciationMethodId():null);
	}
	
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public Integer getAmortizationCode() {
		return amortizationCode;
	}
	public void setAmortizationCode(Integer amortizationCode) {
		this.amortizationCode = amortizationCode;
	}
	public double getAmortizationAmount() {
		return amortizationAmount;
	}
	public void setAmortizationAmount(double amortizationAmount) {
		this.amortizationAmount = UtilRoundDecimal.roundDecimalValue(amortizationAmount);
	}
	public Integer getAveragingConvention() {
		return averagingConvention;
	}
	public void setAveragingConvention(Integer averagingConvention) {
		this.averagingConvention = averagingConvention;
	}
	public double getBeginYearCost() {
		return beginYearCost;
	}
	public void setBeginYearCost(double beginYearCost) {
		this.beginYearCost = UtilRoundDecimal.roundDecimalValue(beginYearCost);
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public double getCostBasis() {
		return costBasis;
	}
	public void setCostBasis(double costBasis) {
		this.costBasis = UtilRoundDecimal.roundDecimalValue(costBasis);
	}
	public double getCurrentDepreciation() {
		return currentDepreciation;
	}
	public void setCurrentDepreciation(double currentDepreciation) {
		this.currentDepreciation = UtilRoundDecimal.roundDecimalValue(currentDepreciation);
	}
	 
	public String getDepreciatedDate() {
		return depreciatedDate;
	}
	public void setDepreciatedDate(String depreciatedDate) {
		this.depreciatedDate = depreciatedDate;
	}
	public void setPlacedInServiceDate(String placedInServiceDate) {
		this.placedInServiceDate = placedInServiceDate;
	}
	
	public String getPlacedInServiceDate() {
		return placedInServiceDate;
	}
	public String getFullDepreciationFlag() {
		return fullDepreciationFlag;
	}
	public void setFullDepreciationFlag(String fullDepreciationFlag) {
		this.fullDepreciationFlag = fullDepreciationFlag;
	}
	public double getLifeToDateDepreciation() {
		return lifeToDateDepreciation;
	}
	public void setLifeToDateDepreciation(double lifeToDateDepreciation) {
		this.lifeToDateDepreciation = UtilRoundDecimal.roundDecimalValue(lifeToDateDepreciation);
	}
	public double getNetFAValue() {
		return netFAValue;
	}
	public void setNetFAValue(double netFAValue) {
		this.netFAValue = UtilRoundDecimal.roundDecimalValue(netFAValue);
	}
	public int getOriginalLifeDay() {
		return originalLifeDay;
	}
	public void setOriginalLifeDay(int originalLifeDay) {
		this.originalLifeDay = originalLifeDay;
	}
	public int getOriginalLifeYear() {
		return originalLifeYear;
	}
	public void setOriginalLifeYear(int originalLifeYear) {
		this.originalLifeYear = originalLifeYear;
	}
	 
	public int getRemainingLifeDay() {
		return remainingLifeDay;
	}
	public void setRemainingLifeDay(int remainingLifeDay) {
		this.remainingLifeDay = remainingLifeDay;
	}
	public int getRemainingLifeYear() {
		return remainingLifeYear;
	}
	public void setRemainingLifeYear(int remainingLifeYear) {
		this.remainingLifeYear = remainingLifeYear;
	}
	public double getSalvageAmount() {
		return salvageAmount;
	}
	public void setSalvageAmount(double salvageAmount) {
		this.salvageAmount = UtilRoundDecimal.roundDecimalValue(salvageAmount);
	}
	public Integer getSwitchOver() {
		return switchOver;
	}
	public void setSwitchOver(Integer switchOver) {
		this.switchOver = switchOver;
	}
	public double getYearlyDepreciatedRate() {
		return yearlyDepreciatedRate;
	}
	public void setYearlyDepreciatedRate(double yearlyDepreciatedRate) {
		this.yearlyDepreciatedRate = UtilRoundDecimal.roundDecimalValue(yearlyDepreciatedRate);
	}
	public double getYearToDateDepreciation() {
		return yearToDateDepreciation;
	}
	public void setYearToDateDepreciation(double yearToDateDepreciation) {
		this.yearToDateDepreciation = UtilRoundDecimal.roundDecimalValue(yearToDateDepreciation);
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Integer getDepreciationMethodId() {
		return depreciationMethodId;
	}

	public void setDepreciationMethodId(Integer depreciationMethodId) {
		this.depreciationMethodId = depreciationMethodId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
}
