/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.bti.model.AccountCurrencyAccess;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DtoAccountCurrencyAccess class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoAccountCurrencyAccess {
	
	private List<String> currencyId = new ArrayList<>();
	private String accountDescription;
	private String accountDescriptionArabic;
	private Long accountIndex;
	private String messageType;
	 
	public DtoAccountCurrencyAccess() {
		 
	}

	public DtoAccountCurrencyAccess(AccountCurrencyAccess accountCurrencyAccess, List<String> currenctIds) {
		this.accountIndex = accountCurrencyAccess.getCoaMainAccounts().getActIndx();
		this.accountDescription = accountCurrencyAccess.getCoaMainAccounts().getMainAccountDescription();
		this.accountDescriptionArabic = accountCurrencyAccess.getCoaMainAccounts().getMainAccountDescriptionArabic();
		this.currencyId.addAll(currenctIds);
	}

	public List<String> getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(List<String> currencyId) {
		this.currencyId = currencyId;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public String getAccountDescriptionArabic() {
		return accountDescriptionArabic;
	}

	public void setAccountDescriptionArabic(String accountDescriptionArabic) {
		this.accountDescriptionArabic = accountDescriptionArabic;
	}

	public Long getAccountIndex() {
		return accountIndex;
	}

	public void setAccountIndex(Long accountIndex) {
		this.accountIndex = accountIndex;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	 
}
