/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FAInsuranceMaintenance;
import com.bti.util.UtilRoundDecimal;

/**
 * Description: DtoInsuranceMaintenance
 * Name of Project: BTI
 * Created on: Sep 01, 2017
 * Modified on: Sep 01, 2017 11:08:38 AM
 * @author seasia
 * Version: 
 */
public class DtoInsuranceMaintenance {

	private String insClassId;
	private int insClassIndex;
	private int insuranceYear;
	private double insuranceValue;
	private double replacementCost;
	private double reproductionCost;
	private String assetId;
	
	public DtoInsuranceMaintenance(){
		
	}
	
	public DtoInsuranceMaintenance(FAInsuranceMaintenance faInsuranceMaintenance){
		this.insClassIndex=faInsuranceMaintenance.getInsuranceClassIdIndex();
		this.insClassId="";
		if(faInsuranceMaintenance.getFaInsuranceClassSetup()!=null){
			this.insClassId = faInsuranceMaintenance.getFaInsuranceClassSetup().getInsuranceClassId();
		}
		this.insuranceYear = faInsuranceMaintenance.getInsuranceYears();
		this.insuranceValue = faInsuranceMaintenance.getInsuranceValue();
		this.replacementCost = UtilRoundDecimal.roundDecimalValue(faInsuranceMaintenance.getReplacementCost());
		this.reproductionCost = UtilRoundDecimal.roundDecimalValue(faInsuranceMaintenance.getReproductionCost());
		this.assetId="";
		if(faInsuranceMaintenance.getFaGeneralMaintenance()!=null){
			this.assetId = faInsuranceMaintenance.getFaGeneralMaintenance().getAssetId();
		}
		
	}
	
	public String getInsClassId() {
		return insClassId;
	}

	public void setInsClassId(String insClassId) {
		this.insClassId = insClassId;
	}

	public int getInsuranceYear() {
		return insuranceYear;
	}
	public void setInsuranceYear(int insuranceYear) {
		this.insuranceYear = insuranceYear;
	}
	public double getInsuranceValue() {
		return insuranceValue;
	}
	public void setInsuranceValue(double insuranceValue) {
		this.insuranceValue = UtilRoundDecimal.roundDecimalValue(insuranceValue);
	}
	public double getReplacementCost() {
		return replacementCost;
	}
	public void setReplacementCost(double replacementCost) {
		this.replacementCost = UtilRoundDecimal.roundDecimalValue(replacementCost);
	}
	public double getReproductionCost() {
		return reproductionCost;
	}
	public void setReproductionCost(double reproductionCost) {
		this.reproductionCost = UtilRoundDecimal.roundDecimalValue(reproductionCost);
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public int getInsClassIndex() {
		return insClassIndex;
	}

	public void setInsClassIndex(int insClassIndex) {
		this.insClassIndex = insClassIndex;
	}
}
