/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the fa40200 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40200")
@NamedQuery(name="FAInsuranceClassSetup.findAll", query="SELECT f FROM FAInsuranceClassSetup f")
public class FAInsuranceClassSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="INSRANINDX")
	private int insuranceClassIndex;

	@Column(name="DEPRATE")
	private Double depreciationRate;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;


	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;


	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="DSCRPTN")
	private String insuranceDescription;

	@Column(name="DSCRPTNA")
	private String insuranceDescriptionArabic;

	@Column(name="INFPERCT")
	private Double inflationPercent;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Fa40002
	@OneToMany(mappedBy="faInsuranceClassSetup")
	private List<FAClassSetup> faClassSetups;

	@Column(name="INSRANID")
	private String insuranceClassId;

	public FAInsuranceClassSetup() {
	}

	public int getInsuranceClassIndex() {
		return insuranceClassIndex;
	}

	public void setInsuranceClassIndex(int insuranceClassIndex) {
		this.insuranceClassIndex = insuranceClassIndex;
	}

	public Double getDepreciationRate() {
		return depreciationRate;
	}

	public void setDepreciationRate(Double depreciationRate) {
		this.depreciationRate = depreciationRate;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getInsuranceDescription() {
		return insuranceDescription;
	}

	public void setInsuranceDescription(String insuranceDescription) {
		this.insuranceDescription = insuranceDescription;
	}

	public String getInsuranceDescriptionArabic() {
		return insuranceDescriptionArabic;
	}

	public void setInsuranceDescriptionArabic(String insuranceDescriptionArabic) {
		this.insuranceDescriptionArabic = insuranceDescriptionArabic;
	}

	public Double getInflationPercent() {
		return inflationPercent;
	}

	public void setInflationPercent(Double inflationPercent) {
		this.inflationPercent = inflationPercent;
	}

	public List<FAClassSetup> getFaClassSetups() {
		return faClassSetups;
	}

	public void setFaClassSetups(List<FAClassSetup> faClassSetups) {
		this.faClassSetups = faClassSetups;
	}

	public String getInsuranceClassId() {
		return insuranceClassId;
	}

	public void setInsuranceClassId(String insuranceClassId) {
		this.insuranceClassId = insuranceClassId;
	}
}