/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

public class DtoPeriods {

	private Integer periodNumber;
	private String periodName;
	private String periodNameArabic;
	 
	public DtoPeriods() {
		// To do nothing
	}

	public Integer getPeriodNumber() {
		return periodNumber;
	}

	public void setPeriodNumber(Integer periodNumber) {
		this.periodNumber = periodNumber;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public String getPeriodNameArabic() {
		return periodNameArabic;
	}

	public void setPeriodNameArabic(String periodNameArabic) {
		this.periodNameArabic = periodNameArabic;
	}
	
	 
	
}
