/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10800 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10800")
@NamedQuery(name="GLBankTransactionEntryHeader.findAll", query="SELECT a FROM GLBankTransactionEntryHeader a")
public class GLBankTransactionEntryHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BNKTRXID")
	private String bankTransactionEntryID;
	
	@Column(name="BNKTRXTYP")
	private int bankTransactionEntryType;
	
	@Column(name="BNKTRXOPT")
	private int bankTransactionEntryAction;

	@Column(name="BNKTRXDT")
	private Date bankTransactionDate;
	
	@ManyToOne
	@JoinColumn(name="CHEKBOKID")
	private CheckbookMaintenance checkbookMaintenance;

	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="EXGTBLID")
	private String exchangeTableID;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	
	@Column(name="CARDEXPTY")
	private int creditCardExpireYear;

	@Column(name="CARDEXPTM")
	private int creditCardExpireMonth;

	@Column(name="RVRNAME")
	private String receiveName ;
	
	@Column(name="BNKDSCR")
	private String bankTransactionDescription ;

	@Column(name="BNKAMNT")
	private double bankTransactionAmount ;

	@Column(name="CREATDDT")
	private Date createDate ;

	@Column(name="MODIFDT")
	private Date modifyDate ;

	@Column(name="CHANGEBY")
	private String modifyByUserID ;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex ;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex ;

	public String getBankTransactionEntryID() {
		return bankTransactionEntryID;
	}

	public void setBankTransactionEntryID(String bankTransactionEntryID) {
		this.bankTransactionEntryID = bankTransactionEntryID;
	}

	public int getBankTransactionEntryType() {
		return bankTransactionEntryType;
	}

	public void setBankTransactionEntryType(int bankTransactionEntryType) {
		this.bankTransactionEntryType = bankTransactionEntryType;
	}

	public int getBankTransactionEntryAction() {
		return bankTransactionEntryAction;
	}

	public void setBankTransactionEntryAction(int bankTransactionEntryAction) {
		this.bankTransactionEntryAction = bankTransactionEntryAction;
	}

	public Date getBankTransactionDate() {
		return bankTransactionDate;
	}

	public void setBankTransactionDate(Date bankTransactionDate) {
		this.bankTransactionDate = bankTransactionDate;
	}
     
	public CheckbookMaintenance getCheckbookMaintenance() {
		return checkbookMaintenance;
	}

	public void setCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookMaintenance = checkbookMaintenance;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getExchangeTableID() {
		return exchangeTableID;
	}

	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public String getReceiveName() {
		return receiveName;
	}

	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}

	public String getBankTransactionDescription() {
		return bankTransactionDescription;
	}

	public void setBankTransactionDescription(String bankTransactionDescription) {
		this.bankTransactionDescription = bankTransactionDescription;
	}

	public double getBankTransactionAmount() {
		return bankTransactionAmount;
	}

	public void setBankTransactionAmount(double bankTransactionAmount) {
		this.bankTransactionAmount = bankTransactionAmount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}           
	
		
   
}