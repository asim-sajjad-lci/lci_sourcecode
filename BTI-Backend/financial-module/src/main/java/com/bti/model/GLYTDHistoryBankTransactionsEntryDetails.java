/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl90501 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl90501")
@NamedQuery(name="GLYTDHistoryBankTransactionsEntryDetails.findAll", query="SELECT a FROM GLYTDHistoryBankTransactionsEntryDetails a")
public class GLYTDHistoryBankTransactionsEntryDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BUDTRSQCN")
	private int bankTransactionEntrySequence;
	
	@ManyToOne
	@JoinColumn(name="BNKTRXID")
	private GLYTDOpenBankTransactionsEntryHeader glytdOpenBankTransactionsEntryHeader;
	
	@Column(name="BAKTRXTYPE")
	private int bankTransactionType;
	
	@Column(name="ACTROWID")
	private int accountTableRowIndex;

	@Column(name="DEBITAMT")
	private Double debitAmount;

	@Column(name="CRDTAMT")
	private Double creditAmount;
	
	@Column(name="ORGDEBTAMT")
	private Double originalDebitAmount;
	
	@Column(name="ORGCRETAMT")
	private Double originalCreditAmount;
	
	@Column(name="DSTDSCR")
	private String distributionDescription;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public int getBankTransactionEntrySequence() {
		return bankTransactionEntrySequence;
	}

	public void setBankTransactionEntrySequence(int bankTransactionEntrySequence) {
		this.bankTransactionEntrySequence = bankTransactionEntrySequence;
	}

	public GLYTDOpenBankTransactionsEntryHeader getGlytdOpenBankTransactionsEntryHeader() {
		return glytdOpenBankTransactionsEntryHeader;
	}

	public void setGlytdOpenBankTransactionsEntryHeader(
			GLYTDOpenBankTransactionsEntryHeader glytdOpenBankTransactionsEntryHeader) {
		this.glytdOpenBankTransactionsEntryHeader = glytdOpenBankTransactionsEntryHeader;
	}

	public int getBankTransactionType() {
		return bankTransactionType;
	}

	public void setBankTransactionType(int bankTransactionType) {
		this.bankTransactionType = bankTransactionType;
	}

	public int getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(int accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(Double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public Double getOriginalCreditAmount() {
		return originalCreditAmount;
	}

	public void setOriginalCreditAmount(Double originalCreditAmount) {
		this.originalCreditAmount = originalCreditAmount;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}
	
	
		
}