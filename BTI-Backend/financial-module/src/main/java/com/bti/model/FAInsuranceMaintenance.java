/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the fa00104 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa00104")
@NamedQuery(name="FAInsuranceMaintenance.findAll", query="SELECT f FROM FAInsuranceMaintenance f")
public class FAInsuranceMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="INSRANINDX")
	private int insuranceClassIdIndex;
	
	@ManyToOne
	@JoinColumn(name="INSRANCLASID")
	private FAInsuranceClassSetup faInsuranceClassSetup;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="INCRVAL")
	private double insuranceValue;

	@Column(name="INCRYR")
	private int insuranceYears;

	@Column(name="MODIFDT")
	private Date modiftDate;

	@Column(name="REPCOST")
	private double reproductionCost;

	@Column(name="RPLCCOST")
	private double replacementCost;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	//bi-directional many-to-one association to Fa00101

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Fa00101
	@ManyToOne
	@JoinColumn(name="ASSETID")
	private FAGeneralMaintenance faGeneralMaintenance;
	
	/*@ManyToOne
	@JoinColumn(name="ASSTSERID")
	private FAGeneralMaintenance faGeneralMaintenance2;*/

	//bi-directional many-to-one association to Fa40200
	/*@OneToMany(mappedBy="faInsuranceMaintenance")
	private List<FAInsuranceClassSetup> faInsuranceClassSetups;*/

	public FAInsuranceMaintenance() {
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public double getInsuranceValue() {
		return insuranceValue;
	}

	public void setInsuranceValue(double insuranceValue) {
		this.insuranceValue = insuranceValue;
	}

	public int getInsuranceYears() {
		return insuranceYears;
	}

	public void setInsuranceYears(int insuranceYears) {
		this.insuranceYears = insuranceYears;
	}

	public Date getModiftDate() {
		return modiftDate;
	}

	public void setModiftDate(Date modiftDate) {
		this.modiftDate = modiftDate;
	}

	public double getReproductionCost() {
		return reproductionCost;
	}

	public void setReproductionCost(double reproductionCost) {
		this.reproductionCost = reproductionCost;
	}

	public double getReplacementCost() {
		return replacementCost;
	}

	public void setReplacementCost(double replacementCost) {
		this.replacementCost = replacementCost;
	}

	public FAGeneralMaintenance getFaGeneralMaintenance() {
		return faGeneralMaintenance;
	}

	public void setFaGeneralMaintenance(FAGeneralMaintenance faGeneralMaintenance) {
		this.faGeneralMaintenance = faGeneralMaintenance;
	}

	/*public List<FAInsuranceClassSetup> getFaInsuranceClassSetups() {
		return faInsuranceClassSetups;
	}

	public void setFaInsuranceClassSetups(List<FAInsuranceClassSetup> faInsuranceClassSetups) {
		this.faInsuranceClassSetups = faInsuranceClassSetups;
	}*/

	public FAInsuranceClassSetup getFaInsuranceClassSetup() {
		return faInsuranceClassSetup;
	}

	public void setFaInsuranceClassSetup(FAInsuranceClassSetup faInsuranceClassSetup) {
		this.faInsuranceClassSetup = faInsuranceClassSetup;
	}

	public int getInsuranceClassIdIndex() {
		return insuranceClassIdIndex;
	}

	public void setInsuranceClassIdIndex(int insuranceClassIdIndex) {
		this.insuranceClassIdIndex = insuranceClassIdIndex;
	}
	
}