/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ap00101 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap00101")
@NamedQuery(name="VendorMaintenance.findAll", query="SELECT a FROM VendorMaintenance a")
public class VendorMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VENDORID")
	private String vendorid;
	
	@Column(name="ADDRESS1")
	private String address1;
	
	@Column(name="ADDRESS2")
	private String address2;

	@Column(name="ADDRESS3")
	private String address3;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CITY")
	private int city;

	@Column(name="COUNRTY")
	private int counrty;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="FAX1")
	private String faxNumber;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PHONE1")
	private String phoneNumber1;

	@Column(name="PHONE2")
	private String phoneNumber2;

	@Column(name="PHONE3")
	private String phoneNumber3;

	@Column(name="SHORTNAM")
	private String vendorShortName;

	@Column(name="STATE")
	private int state;

	@Column(name="STATNAME")
	private String vendorStatementName;

	@Column(name="USERDEFN1")
	private String userDefine1;

	@Column(name="USERDEFN2")
	private String userDefine2;

	@Column(name="VENDNAME")
	private String vendorName;

	@Column(name="VENDNAMEA")
	private String vendorNameArabic;
	
	@Column(name="VENPRTY")
	private String vendorPriority;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	//bi-directional many-to-one association to Ap00200
	@ManyToOne
	@JoinColumn(name="VENDCLSID")
	private VendorClassesSetup vendorClassesSetup;

	//bi-directional many-to-one association to MasterVendorStatus
	@ManyToOne
	@JoinColumn(name="VENDSTAT")
	private MasterVendorStatus masterVendorStatus;
	
	//bi-directional many-to-one association to HoldVendorStatus
	@Column(name="VENDHLD")
	private int vendorHoldStatus;
	

	public VendorMaintenance() {
	}

	public String getVendorid() {
		return vendorid;
	}

	public void setVendorid(String vendorid) {
		this.vendorid = vendorid;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}


	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public String getPhoneNumber3() {
		return phoneNumber3;
	}

	public void setPhoneNumber3(String phoneNumber3) {
		this.phoneNumber3 = phoneNumber3;
	}

	public String getVendorShortName() {
		return vendorShortName;
	}

	public void setVendorShortName(String vendorShortName) {
		this.vendorShortName = vendorShortName;
	}

	public String getVendorStatementName() {
		return vendorStatementName;
	}

	public void setVendorStatementName(String vendorStatementName) {
		this.vendorStatementName = vendorStatementName;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorNameArabic() {
		return vendorNameArabic;
	}

	public void setVendorNameArabic(String vendorNameArabic) {
		this.vendorNameArabic = vendorNameArabic;
	}

	public VendorClassesSetup getVendorClassesSetup() {
		return vendorClassesSetup;
	}

	public void setVendorClassesSetup(VendorClassesSetup vendorClassesSetup) {
		this.vendorClassesSetup = vendorClassesSetup;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getCity() {
		return city;
	}

	public void setCity(int city) {
		this.city = city;
	}

	public int getCounrty() {
		return counrty;
	}

	public void setCounrty(int counrty) {
		this.counrty = counrty;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
	public MasterVendorStatus getMasterVendorStatus() {
		return this.masterVendorStatus;
	}

	public void setMasterVendorStatus(MasterVendorStatus masterVendorStatus) {
		this.masterVendorStatus = masterVendorStatus;
	}

	public String getVendorPriority() {
		return vendorPriority;
	}

	public void setVendorPriority(String vendorPriority) {
		this.vendorPriority = vendorPriority;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public int getVendorHoldStatus() {
		return vendorHoldStatus;
	}
	
	public void setVendorHoldStatus(int vendorHoldStatus) {
		this.vendorHoldStatus = vendorHoldStatus;
	}

}