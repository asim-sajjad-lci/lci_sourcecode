/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the gl49998 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl49998")
@NamedQuery(name="GLConfigurationRelationBetweenDimensionsAndMainAccount.findAll", query="SELECT g FROM GLConfigurationRelationBetweenDimensionsAndMainAccount g")
public class GLConfigurationRelationBetweenDimensionsAndMainAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="SGMTNUMB")
	private int segmentNumber;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="ISMAINACCT")
	private int isMainAccount;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to SY40003
	@ManyToOne
	@JoinColumn(name="DIMINXD")
	private COAFinancialStructureDimensions coaFinancialStructureDimensions;

	
	//bi-directional many-to-one association to Gl00101
	@ManyToOne
	@JoinColumn(name="ACTFROMVAL")
	private COAMainAccounts coaMainAccountsFrom;

	//bi-directional many-to-one association to Gl00101
	@ManyToOne
	@JoinColumn(name="ACTTOVAL")
	private COAMainAccounts coaMainAccountsTo;

	//bi-directional many-to-one association to Gl00103
	@ManyToOne
	@JoinColumn(name="SGMFRMVAL")
	private COAFinancialDimensionsValues coaFinancialDimensionsValuesFrom;

	//bi-directional many-to-one association to Gl00103
	@ManyToOne
	@JoinColumn(name="SGMTOVAL")
	private COAFinancialDimensionsValues coaFinancialDimensionsValuesTo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSegmentNumber() {
		return segmentNumber;
	}

	public void setSegmentNumber(int segmentNumber) {
		this.segmentNumber = segmentNumber;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getIsMainAccount() {
		return isMainAccount;
	}

	public void setIsMainAccount(int isMainAccount) {
		this.isMainAccount = isMainAccount;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

//	public COAFinancialDimensions getCoaFinancialDimensions() {
//		return coaFinancialDimensions;
//	}
//
//	public void setCoaFinancialDimensions(COAFinancialDimensions coaFinancialDimensions) {
//		this.coaFinancialDimensions = coaFinancialDimensions;
//	}

	public COAMainAccounts getCoaMainAccountsFrom() {
		return coaMainAccountsFrom;
	}

	public void setCoaMainAccountsFrom(COAMainAccounts coaMainAccountsFrom) {
		this.coaMainAccountsFrom = coaMainAccountsFrom;
	}

	public COAMainAccounts getCoaMainAccountsTo() {
		return coaMainAccountsTo;
	}

	public void setCoaMainAccountsTo(COAMainAccounts coaMainAccountsTo) {
		this.coaMainAccountsTo = coaMainAccountsTo;
	}

	public COAFinancialDimensionsValues getCoaFinancialDimensionsValuesFrom() {
		return coaFinancialDimensionsValuesFrom;
	}

	public void setCoaFinancialDimensionsValuesFrom(COAFinancialDimensionsValues coaFinancialDimensionsValuesFrom) {
		this.coaFinancialDimensionsValuesFrom = coaFinancialDimensionsValuesFrom;
	}

	public COAFinancialDimensionsValues getCoaFinancialDimensionsValuesTo() {
		return coaFinancialDimensionsValuesTo;
	}

	public void setCoaFinancialDimensionsValuesTo(COAFinancialDimensionsValues coaFinancialDimensionsValuesTo) {
		this.coaFinancialDimensionsValuesTo = coaFinancialDimensionsValuesTo;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public COAFinancialStructureDimensions getCoaFinancialStructureDimensions() {
		return coaFinancialStructureDimensions;
	}

	public void setCoaFinancialStructureDimensions(COAFinancialStructureDimensions coaFinancialStructureDimensions) {
		this.coaFinancialStructureDimensions = coaFinancialStructureDimensions;
	}
	
	

}