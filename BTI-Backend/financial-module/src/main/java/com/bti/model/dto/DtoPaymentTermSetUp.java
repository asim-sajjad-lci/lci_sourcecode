/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.MasterDiscountType;
import com.bti.model.MasterDiscountTypePeriod;
import com.bti.model.MasterDueTypes;
import com.bti.model.PaymentTermsSetup;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoPaymentTermSetUp {
	
	String paymentTermId;
	String description;
	String arabicDescription;
	Integer discountPeriod;
	Integer discountType;
	String discountTypeValue;
	Integer discountPeriodDays;
	Integer dueDays;
	Integer dueType;
	private String messageType;
	private String dueTypeValue;
	private List<String> paymentTermIds;
	public DtoPaymentTermSetUp() {
		
	}
	
	public DtoPaymentTermSetUp(PaymentTermsSetup paymentTermsSetup , 
			MasterDiscountTypePeriod masterDiscountTypePeriod , MasterDueTypes masterDueTypes , MasterDiscountType masterDiscountType) {
		this.paymentTermId =paymentTermsSetup.getPaymentTermId();
        	  if( UtilRandomKey.isNotBlank(paymentTermsSetup.getPaymentDescription())){
        		  this.description=paymentTermsSetup.getPaymentDescription();
        	  }
        	  else{
        		  this.description="";
        	  }
            	 if( UtilRandomKey.isNotBlank(paymentTermsSetup.getPaymentDescriptionArabic())){
            		 this.arabicDescription = paymentTermsSetup.getPaymentDescriptionArabic();
            	 }
            	 else
            	 {
            		 this.arabicDescription = "";
            	 }
                 
        this.discountType =0;
        this.discountTypeValue="";
        if(masterDiscountType!=null){
        	this.discountType=masterDiscountType.getTypeId();
        	 if(masterDiscountType.getTypeId()==1){
             	this.discountTypeValue = String.valueOf(paymentTermsSetup.getDiscountPercent());
             }
             else{
             	this.discountTypeValue = String.valueOf(paymentTermsSetup.getDiscountAmount());
             }
        }
       this.dueType=0;
       this.dueTypeValue="";
       if(masterDueTypes!=null)
       {
    	   dueType= masterDueTypes.getTypeId();
    	   dueTypeValue=masterDueTypes.getDueType();
       }
        this.discountPeriod=0;
        if(masterDiscountTypePeriod!=null){
        	this.discountPeriod=masterDiscountTypePeriod.getTypeId();
        }
        this.discountPeriodDays =paymentTermsSetup.getDiscountTypesPeriodValue();
        this.dueDays =paymentTermsSetup.getDueDaysDate();
	}
	
	public String getPaymentTermId() {
		return paymentTermId;
	}
	public void setPaymentTermId(String paymentTermId) {
		this.paymentTermId = paymentTermId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getArabicDescription() {
		return arabicDescription;
	}
	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}
	
	public Integer getDiscountType() {
		return discountType;
	}
	public void setDiscountType(Integer discountType) {
		this.discountType = discountType;
	}
	public Integer getDueDays() {
		return dueDays;
	}
	public void setDueDays(Integer dueDays) {
		this.dueDays = dueDays;
	}
	public Integer getDueType() {
		return dueType;
	}
	public void setDueType(Integer dueType) {
		this.dueType = dueType;
	}
	public Integer getDiscountPeriod() {
		return discountPeriod;
	}
	public void setDiscountPeriod(Integer discountPeriod) {
		this.discountPeriod = discountPeriod;
	}
	public Integer getDiscountPeriodDays() {
		return discountPeriodDays;
	}
	public void setDiscountPeriodDays(Integer discountPeriodDays) {
		this.discountPeriodDays = discountPeriodDays;
	}
	public String getDiscountTypeValue() {
		return discountTypeValue;
	}
	public void setDiscountTypeValue(String discountTypeValue) {
		this.discountTypeValue = discountTypeValue;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getDueTypeValue() {
		return dueTypeValue;
	}

	public void setDueTypeValue(String dueTypeValue) {
		this.dueTypeValue = dueTypeValue;
	}

	public List<String> getPaymentTermIds() {
		return paymentTermIds;
	}

	public void setPaymentTermIds(List<String> paymentTermIds) {
		this.paymentTermIds = paymentTermIds;
	}
}
