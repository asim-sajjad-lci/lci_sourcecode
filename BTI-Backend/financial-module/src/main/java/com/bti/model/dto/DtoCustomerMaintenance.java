/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.CustomerMaintenance;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoCustomerMaintenance {

	private String customerId;
	private String name;
	private String arabicName;
	private String shortName;
	private String classId;
	private String statementName;
	private String phone1;
	private String phone2;
	private String phone3;
	private String address1;
	private String address2;
	private String address3;
	private Integer cityId;
	private Integer stateId;
	private Integer countryId;
	private String fax;
	private String countryName;
	private String stateName;
	private String cityName;
	private String userDefine1;
	private String userDefine2;
	private int activeCustomer;
	private int customerHold;
	private String messageType;
	private String priority;
	
	public DtoCustomerMaintenance() {

	}

	public DtoCustomerMaintenance(CustomerMaintenance customerMaintenance) {
		
		this.customerId = customerMaintenance.getCustnumber();
		this.name = customerMaintenance.getCustomerName();
		this.arabicName = customerMaintenance.getCustomerNameArabic();
		this.shortName = customerMaintenance.getCustomerShortName();
		this.classId = customerMaintenance.getCustomerClassesSetup() != null
				? customerMaintenance.getCustomerClassesSetup().getCustomerClassId() : null;
		this.statementName = customerMaintenance.getCustomerStateName();
		this.phone1 = customerMaintenance.getPhoneNumber1();
		this.phone2 = customerMaintenance.getPhoneNumber2();
		this.phone3 = customerMaintenance.getPhoneNumber3();
		this.address1 = customerMaintenance.getAddress1();
		this.address2 = customerMaintenance.getAddress2();
		this.address3 = customerMaintenance.getAddress3();
		this.fax = customerMaintenance.getFax1();
		this.countryName = customerMaintenance.getCounrty();
		this.stateName = customerMaintenance.getState();
		this.cityName = customerMaintenance.getCity();
		this.userDefine1 = customerMaintenance.getUserDefine1();
		this.userDefine2 = customerMaintenance.getUserDefine2();
		this.activeCustomer = customerMaintenance.getCustomerActiveStatus();
		this.customerHold = customerMaintenance.getCustomerHoldStatus();
		this.priority="1";
		if(UtilRandomKey.isNotBlank(customerMaintenance.getCustomerPriority())){
			this.priority=customerMaintenance.getCustomerPriority();
		}
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArabicName() {
		return arabicName;
	}

	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getStatementName() {
		return statementName;
	}

	public void setStatementName(String statementName) {
		this.statementName = statementName;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public int getActiveCustomer() {
		return activeCustomer;
	}

	public void setActiveCustomer(int activeCustomer) {
		this.activeCustomer = activeCustomer;
	}

	public int getCustomerHold() {
		return customerHold;
	}

	public void setCustomerHold(int customerHold) {
		this.customerHold = customerHold;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

}
