/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10201 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10201")
@NamedQuery(name="GLClearingDetails .findAll", query="SELECT a FROM GLClearingDetails  a")
public class GLClearingDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TRXSEQNC")
	private int transactionRowSequence;

	@Column(name="JORNID")
	private String journalID;
	
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;

	@Column(name="DISTRDSCR")
	private String distributionDescription;
	
	@ManyToOne
	@JoinColumn(name="ACTROWIDO")
	private GLAccountsTableAccumulation glAccountsTableAccumulationOffset;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private Boolean isDeleted;

	public int getTransactionRowSequence() {
		return transactionRowSequence;
	}

	public void setTransactionRowSequence(int transactionRowSequence) {
		this.transactionRowSequence = transactionRowSequence;
	}
	
	public String getJournalID() {
		return journalID;
	}

	public void setJournalID(String journalID) {
		this.journalID = journalID;
	}

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}

	public GLAccountsTableAccumulation getGlAccountsTableAccumulationOffset() {
		return glAccountsTableAccumulationOffset;
	}

	public void setGlAccountsTableAccumulationOffset(GLAccountsTableAccumulation glAccountsTableAccumulationOffset) {
		this.glAccountsTableAccumulationOffset = glAccountsTableAccumulationOffset;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}   
	
}