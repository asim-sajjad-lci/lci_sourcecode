/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.CurrencyExchangeHeader;
import com.bti.model.MasterRateFrequencyTypes;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Exchange Table Setup class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoCurrencyExchangeTableSetupHeader {

	private String exchangeId;
	private String description;
	private String descriptionArabic;
	private String exchangeRateSource;
	private String exchangeRateSourceArabic;
	private int rateFrequency;
	private String rateVariance;
	private String currencyId;
	private String rateCalcMethod;
	private int exchangeTableIndex;
	private boolean isDefault;
	
	public DtoCurrencyExchangeTableSetupHeader() {
		
	}
	
	public DtoCurrencyExchangeTableSetupHeader(CurrencyExchangeHeader currencyExchangeHeader,MasterRateFrequencyTypes masterRateFrequencyTypes) 
	{
		this.exchangeTableIndex=currencyExchangeHeader.getExchangeIndex();
		this.exchangeId=currencyExchangeHeader.getExchangeId();
		this.description=currencyExchangeHeader.getExchangeDescription();
		this.exchangeRateSource=currencyExchangeHeader.getExchangeRateSource();
		this.descriptionArabic=currencyExchangeHeader.getExchangeDescriptionArabic();
		this.exchangeRateSourceArabic=currencyExchangeHeader.getExchangeRateSourceArabic();
		this.isDefault = currencyExchangeHeader.isDefault();
		this.rateFrequency=0;
		if(masterRateFrequencyTypes!=null){
			rateFrequency=masterRateFrequencyTypes.getTypeId();
		}
				
		this.rateVariance=String.valueOf(currencyExchangeHeader.getRateVariance());
		this.currencyId="";
		if(currencyExchangeHeader.getCurrencySetup()!=null){
			this.currencyId=currencyExchangeHeader.getCurrencySetup().getCurrencyId();
         }
		
		this.rateCalcMethod=currencyExchangeHeader.getRateCalcMethod();
	}	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescriptionArabic() {
		return descriptionArabic;
	}
	public void setDescriptionArabic(String descriptionArabic) {
		this.descriptionArabic = descriptionArabic;
	}
	public String getExchangeRateSource() {
		return exchangeRateSource;
	}
	public void setExchangeRateSource(String exchangeRateSource) {
		this.exchangeRateSource = exchangeRateSource;
	}
	public String getExchangeRateSourceArabic() {
		return exchangeRateSourceArabic;
	}
	public void setExchangeRateSourceArabic(String exchangeRateSourceArabic) {
		this.exchangeRateSourceArabic = exchangeRateSourceArabic;
	}
	public int getRateFrequency() {
		return rateFrequency;
	}
	public void setRateFrequency(int rateFrequency) {
		this.rateFrequency = rateFrequency;
	}
	public String getRateVariance() {
		return rateVariance;
	}
	public void setRateVariance(String rateVariance) {
		this.rateVariance = rateVariance;
	}
	public String getExchangeId() {
		return exchangeId;
	}
	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}
	public String getCurrencyId() {
		return currencyId;
	}
	
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getRateCalcMethod() {
		return rateCalcMethod;
	}

	public void setRateCalcMethod(String rateCalcMethod) {
		this.rateCalcMethod = rateCalcMethod;
	}
	
	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}
	
	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	
	
	
}
