/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the ModulesConfiguration database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "sy40002")
@NamedQuery(name="ModulesConfiguration.findAll", query="SELECT s FROM ModulesConfiguration s")
public class ModulesConfiguration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SERIES")
	private int moduleSeriesNumber;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;

	@Column(name="MODLDECR")
	private String moduleDescription;

	@Column(name="MODLDECRA")
	private String moduleDescriptionArabic;

	@Column(name="ORGINID")
	private int originId;

	@Column(name="ORTRXN")
	private String originTransactionDescription;

	@Column(name="ORTRXNA")
	private String originTransactionDescriptionArabic;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	public ModulesConfiguration() {
	}

	public int getModuleSeriesNumber() {
		return moduleSeriesNumber;
	}

	public void setModuleSeriesNumber(int moduleSeriesNumber) {
		this.moduleSeriesNumber = moduleSeriesNumber;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModuleDescription() {
		return moduleDescription;
	}

	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}

	public String getModuleDescriptionArabic() {
		return moduleDescriptionArabic;
	}

	public void setModuleDescriptionArabic(String moduleDescriptionArabic) {
		this.moduleDescriptionArabic = moduleDescriptionArabic;
	}

	public int getOriginId() {
		return originId;
	}

	public void setOriginId(int originId) {
		this.originId = originId;
	}

	public String getOriginTransactionDescription() {
		return originTransactionDescription;
	}

	public void setOriginTransactionDescription(String originTransactionDescription) {
		this.originTransactionDescription = originTransactionDescription;
	}

	public String getOriginTransactionDescriptionArabic() {
		return originTransactionDescriptionArabic;
	}

	public void setOriginTransactionDescriptionArabic(String originTransactionDescriptionArabic) {
		this.originTransactionDescriptionArabic = originTransactionDescriptionArabic;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
}