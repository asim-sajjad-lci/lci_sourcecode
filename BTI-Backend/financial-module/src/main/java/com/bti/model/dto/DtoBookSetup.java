/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;
import com.bti.model.BookSetup;
import com.bti.model.MasterDepreciationPeriodTypes;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoBookSetup {

	private Integer bookIndexId;
	private String bookId;
	private String bookDescription;
	private String bookDescriptionArabic;
	private Integer currentYear;
	private Integer depreciationPeriodId;
	private String depreciationPeriodValue;
	private Integer faCalendarSetupIndexId;
	private String messageType;
	
	public DtoBookSetup() {
		
	}
	
	public DtoBookSetup(BookSetup bookSetup, MasterDepreciationPeriodTypes masterDepreciationPeriodTypes) {
		this.bookIndexId=bookSetup.getBookInxd();
		this.bookId=bookSetup.getBookId();
		this.bookDescription="";
		if(UtilRandomKey.isNotBlank(bookSetup.getBookDescription())){
			this.bookDescription=bookSetup.getBookDescription();
		}
		this.bookDescriptionArabic="";
		if(UtilRandomKey.isNotBlank(bookSetup.getBookDescriptionArabic())){
			this.bookDescriptionArabic=bookSetup.getBookDescriptionArabic();
		}
		this.depreciationPeriodId=0;
		this.depreciationPeriodValue="";
		if(masterDepreciationPeriodTypes!=null){
			this.depreciationPeriodId=masterDepreciationPeriodTypes.getTypeId();
			this.depreciationPeriodValue=masterDepreciationPeriodTypes.getDepreciationPeriodType();
		}
		this.faCalendarSetupIndexId=0;
		if(bookSetup.getFaCalendarSetup()!=null){
			this.faCalendarSetupIndexId=bookSetup.getFaCalendarSetup().getCalendarIndex();
		}
		this.currentYear=bookSetup.getCurrentYear();
	}

	public Integer getBookIndexId() {
		return bookIndexId;
	}

	public void setBookIndexId(Integer bookIndexId) {
		this.bookIndexId = bookIndexId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookDescription() {
		return bookDescription;
	}

	public void setBookDescription(String bookDescription) {
		this.bookDescription = bookDescription;
	}

	public String getBookDescriptionArabic() {
		return bookDescriptionArabic;
	}

	public void setBookDescriptionArabic(String bookDescriptionArabic) {
		this.bookDescriptionArabic = bookDescriptionArabic;
	}

	public Integer getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(Integer currentYear) {
		this.currentYear = currentYear;
	}

	public Integer getDepreciationPeriodId() {
		return depreciationPeriodId;
	}

	public void setDepreciationPeriodId(Integer depreciationPeriodId) {
		this.depreciationPeriodId = depreciationPeriodId;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Integer getFaCalendarSetupIndexId() {
		return faCalendarSetupIndexId;
	}

	public void setFaCalendarSetupIndexId(Integer faCalendarSetupIndexId) {
		this.faCalendarSetupIndexId = faCalendarSetupIndexId;
	}

	public String getDepreciationPeriodValue() {
		return depreciationPeriodValue;
	}

	public void setDepreciationPeriodValue(String depreciationPeriodValue) {
		this.depreciationPeriodValue = depreciationPeriodValue;
	}
	
}
