/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl80000 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl80000")
@NamedQuery(name="GLBatches.findAll", query="SELECT s FROM GLBatches s")
public class GLBatches implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
 	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="BATCHID")
	private String batchID;

	@Column(name="BADSCR")
	private String batchDescription;
	
	@ManyToOne
	@JoinColumn(name="SOURDOC")
	private GLConfigurationAuditTrialCodes gLConfigurationAuditTrialCodes;

	@Column(name="BADSCRA")
	private String batchDescriptionArabic;
	
	@ManyToOne
	@JoinColumn(name="BACHTYPE")
	private BatchTransactionType batchTransactionType;
	
	@Column(name="TOTTRXNO")
	private int totalNumberTransactionInBatch;
	
	@Column(name="TOTTRXAMT")
	private Double totalAmountTransactionInBatch;
	
	@Column(name="APPRVAL")
	private int statusApproval;
	
	@Column(name="CREATDDT")
	private Date createdDate;
	
	@Column(name="MODIFTDT")
	private Date modifiedDate;
	
	@Column(name="CHANGEBY")
	private String changeBy;
	
	@Column(name="APPRDDT")
	private Date approvalDate;
	
	@Column(name="APPRUSR")
	private String approvalByUserID;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="POST_DATE")
	private Date postingDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private Boolean isDeleted;
	
	
	  
	public GLBatches() {
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getBatchDescription() {
		return batchDescription;
	}

	public void setBatchDescription(String batchDescription) {
		this.batchDescription = batchDescription;
	}

	public String getBatchDescriptionArabic() {
		return batchDescriptionArabic;
	}

	public void setBatchDescriptionArabic(String batchDescriptionArabic) {
		this.batchDescriptionArabic = batchDescriptionArabic;
	}

	
	public BatchTransactionType getBatchTransactionType() {
		return batchTransactionType;
	}

	public void setBatchTransactionType(BatchTransactionType batchTransactionType) {
		this.batchTransactionType = batchTransactionType;
	}

	public int getTotalNumberTransactionInBatch() {
		return totalNumberTransactionInBatch;
	}

	public void setTotalNumberTransactionInBatch(int totalNumberTransactionInBatch) {
		this.totalNumberTransactionInBatch = totalNumberTransactionInBatch;
	}

	public Double getTotalAmountTransactionInBatch() {
		return totalAmountTransactionInBatch;
	}

	public void setTotalAmountTransactionInBatch(Double totalAmountTransactionInBatch) {
		this.totalAmountTransactionInBatch = totalAmountTransactionInBatch;
	}

	public int getStatusApproval() {
		return statusApproval;
	}

	public void setStatusApproval(int statusApproval) {
		this.statusApproval = statusApproval;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalByUserID() {
		return approvalByUserID;
	}

	public void setApprovalByUserID(String approvalByUserID) {
		this.approvalByUserID = approvalByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(String changeBy) {
		this.changeBy = changeBy;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GLConfigurationAuditTrialCodes getGlConfigurationAuditTrialCodes() {
		return gLConfigurationAuditTrialCodes;
	}

	public void setGlConfigurationAuditTrialCodes(GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes) {
		this.gLConfigurationAuditTrialCodes = glConfigurationAuditTrialCodes;
	}
	
	
	
}