/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ap90600 database table.
 * 
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap90600")
@NamedQuery(name = "APYTDHistoryApplyDocumentsPayments.findAll", query = "SELECT a FROM APYTDHistoryApplyDocumentsPayments a")
public class APYTDHistoryApplyDocumentsPayments implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	// @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "VENDORID")
	private String vendorNumber;

	@Column(name = "APTRXNMBR")
	private String apTransactionNumber;

	@Column(name = "APTRXDT")
	private Date apTransactionDate;

	@Column(name = "APTRXPODT")
	private Date apTransactionPostingDate;

	@Column(name = "APPLDT")
	private Date applyDate;

	@Column(name = "APPDOCNMR")
	private String applyDocumentNumberPaymentNumber;
	
	@Column(name = "APPLAMNT")
	private double applyAmount;

	@Column(name = "ORAPPAMNT")
	private double originalAmount;

	@Column(name = "REAPPAMNT")
	private double remainAmountAfterApply;
	
	@Column(name = "TRXPOST")
	private int paymentAlreadyPosted;
	
	@Column(name = "YEAR1")
	private int year;
	 
	@Column(name = "DEX_ROW_ID")
	private int rowIndexId;

	@Column(name = "DEX_ROW_TS")
	private Date rowDateIndex;

	public String getVendorNumber() {
		return vendorNumber;
	}

	public void setVendorNumber(String vendorNumber) {
		this.vendorNumber = vendorNumber;
	}

	public String getApTransactionNumber() {
		return apTransactionNumber;
	}

	public void setApTransactionNumber(String apTransactionNumber) {
		this.apTransactionNumber = apTransactionNumber;
	}

	public Date getApTransactionDate() {
		return apTransactionDate;
	}

	public void setApTransactionDate(Date apTransactionDate) {
		this.apTransactionDate = apTransactionDate;
	}

	public Date getApTransactionPostingDate() {
		return apTransactionPostingDate;
	}

	public void setApTransactionPostingDate(Date apTransactionPostingDate) {
		this.apTransactionPostingDate = apTransactionPostingDate;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public String getApplyDocumentNumberPaymentNumber() {
		return applyDocumentNumberPaymentNumber;
	}

	public void setApplyDocumentNumberPaymentNumber(String applyDocumentNumberPaymentNumber) {
		this.applyDocumentNumberPaymentNumber = applyDocumentNumberPaymentNumber;
	}

	public double getApplyAmount() {
		return applyAmount;
	}

	public void setApplyAmount(double applyAmount) {
		this.applyAmount = applyAmount;
	}

	public double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(double originalAmount) {
		this.originalAmount = originalAmount;
	}

	public double getRemainAmountAfterApply() {
		return remainAmountAfterApply;
	}

	public void setRemainAmountAfterApply(double remainAmountAfterApply) {
		this.remainAmountAfterApply = remainAmountAfterApply;
	}

	public int getPaymentAlreadyPosted() {
		return paymentAlreadyPosted;
	}

	public void setPaymentAlreadyPosted(int paymentAlreadyPosted) {
		this.paymentAlreadyPosted = paymentAlreadyPosted;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}