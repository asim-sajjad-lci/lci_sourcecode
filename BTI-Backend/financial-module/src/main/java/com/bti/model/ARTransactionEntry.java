/**
\ * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ar10100 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar10100")
@NamedQuery(name="ARTransactionEntry.findAll", query="SELECT a FROM ARTransactionEntry a")
public class ARTransactionEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ARTRXNO")
	private String arTransactionNumber;
	
	@ManyToOne
	@JoinColumn(name="ARTRXTYP")
	private RMDocumentsTypeSetup transactionType;
	
	@Column(name="DSCRPTN")
	private String arTransactionDescription;

	@ManyToOne
	@JoinColumn(name="BATCHID")
	private ARBatches arBatches;

	@Column(name="ARTRXDT")
	private Date arTransactionDate;
	
	@Column(name="CUSTNMBR")
	private String customerID;
	
	@Column(name="CUSTNAME")
	private String customerName;
	
	@Column(name="CUSTNAMEA")
	private String customerNameArabic;
	
	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="PYMTRMID")
	private String paymentTermsID;
	
	@Column(name="ARCSTAMT")
	private double arTransactionCost;
	
	@Column(name="ARSLSAMT")
	private double arTransactionSalesAmount;
	
	@Column(name="ARTRDAMT")
	private double arTransactionTradeDiscount;
	
	@Column(name="ARFRGAMT")
	private double arTransactionFreightAmount;
	
	@Column(name="ARMISCAMT")
	private double arTransactionMiscellaneous;
	
	@Column(name="ARVATAMT")
	private double arTransactionVATAmount;
	
	@Column(name="ARDBAMT")
	private double arTransactionDebitMemoAmount;
	
	@Column(name="ARFRCAMT")
	private double arTransactionFinanceChargeAmount;
	
	@Column(name="ARWARAMT")
	private double arTransactionWarrantyAmount;
	
	@Column(name="ARCRAMT")
	private double arTransactionCreditMemoAmount;
	
	@Column(name="ARTOTAMT")
	private double arTransactionTotalAmount;
	
	@Column(name="ARCSHAMT")
	private double arTransactionCashAmount;
	
	@Column(name="ARCHKAMT")
	private double arTransactionCheckAmount;
	
	@Column(name="ARCRDAMT")
	private double arTransactionCreditCardAmount;
	
	@Column(name="ARSERREPAMT")
	private double arServiceRepairAmount;
	
	@Column(name="ARRETNAMT")
	private double arReturnAmount;
	
	
	@Column(name="SALSPERID")
	private String salesmanID;
	
	@Column(name="CHEKBOKID")
	private String checkbookID;
	
	@Column(name="CSHRECNO")
	private String cashReceiptNumber;
	
	@Column(name="CHKNUMBR")
	private String checkNumber;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	 
	@Column(name="CARDEXPTY")
	private String creditCardExpireYear;
	
	@Column(name="CARDEXPTM")
	private String creditCardExpireMonth;
	
	@ManyToOne
	@JoinColumn(name="TRXSTAT")
	private MasterARTransactionStatus arTransactionStatus;
	
	@Column(name="EXGTBLINXD")
	private int exchangeTableIndex;
	
	@Column(name="XCHGRATE")
	private double exchangeTableRate;
	
	@Column(name="TRXVOID")
	private boolean transactionVoid;
	
	@Column(name="SHIPMTHD")
	private String shippingMethodID;
	
	@Column(name="TAXSCHDID")
	private String vatScheduleID;
	 
	@Column(name="CREATDDT")
	private Date createDate;
	
	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	
	@Column(name="PAYM_TYP_ID")
	private String paymentTypeId;
	
	@Column(name="SALSTERRID")
	private String salesTerritoryId;
	
	@Column(name="PAYM_AMNT")
	private double paymentAmount;
	
	@Column(name="IS_PAYM")
	private boolean payment;

	public String getArTransactionNumber() {
		return arTransactionNumber;
	}

	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}

	public String getArTransactionDescription() {
		return arTransactionDescription;
	}

	public void setArTransactionDescription(String arTransactionDescription) {
		this.arTransactionDescription = arTransactionDescription;
	}

	public ARBatches getArBatches() {
		return arBatches;
	}

	public void setArBatches(ARBatches arBatches) {
		this.arBatches = arBatches;
	}

	public Date getArTransactionDate() {
		return arTransactionDate;
	}

	public void setArTransactionDate(Date arTransactionDate) {
		this.arTransactionDate = arTransactionDate;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNameArabic() {
		return customerNameArabic;
	}

	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getPaymentTermsID() {
		return paymentTermsID;
	}

	public void setPaymentTermsID(String paymentTermsID) {
		this.paymentTermsID = paymentTermsID;
	}

	public double getArTransactionCost() {
		return arTransactionCost;
	}

	public void setArTransactionCost(double arTransactionCost) {
		this.arTransactionCost = arTransactionCost;
	}

	public double getArTransactionSalesAmount() {
		return arTransactionSalesAmount;
	}

	public void setArTransactionSalesAmount(double arTransactionSalesAmount) {
		this.arTransactionSalesAmount = arTransactionSalesAmount;
	}

	public double getArTransactionTradeDiscount() {
		return arTransactionTradeDiscount;
	}

	public void setArTransactionTradeDiscount(double arTransactionTradeDiscount) {
		this.arTransactionTradeDiscount = arTransactionTradeDiscount;
	}

	public double getArTransactionFreightAmount() {
		return arTransactionFreightAmount;
	}

	public void setArTransactionFreightAmount(double arTransactionFreightAmount) {
		this.arTransactionFreightAmount = arTransactionFreightAmount;
	}

	public double getArTransactionMiscellaneous() {
		return arTransactionMiscellaneous;
	}

	public void setArTransactionMiscellaneous(double arTransactionMiscellaneous) {
		this.arTransactionMiscellaneous = arTransactionMiscellaneous;
	}

	public double getArTransactionVATAmount() {
		return arTransactionVATAmount;
	}

	public void setArTransactionVATAmount(double arTransactionVATAmount) {
		this.arTransactionVATAmount = arTransactionVATAmount;
	}

	public double getArTransactionDebitMemoAmount() {
		return arTransactionDebitMemoAmount;
	}

	public void setArTransactionDebitMemoAmount(double arTransactionDebitMemoAmount) {
		this.arTransactionDebitMemoAmount = arTransactionDebitMemoAmount;
	}

	public double getArTransactionFinanceChargeAmount() {
		return arTransactionFinanceChargeAmount;
	}

	public void setArTransactionFinanceChargeAmount(double arTransactionFinanceChargeAmount) {
		this.arTransactionFinanceChargeAmount = arTransactionFinanceChargeAmount;
	}

	public double getArTransactionWarrantyAmount() {
		return arTransactionWarrantyAmount;
	}

	public void setArTransactionWarrantyAmount(double arTransactionWarrantyAmount) {
		this.arTransactionWarrantyAmount = arTransactionWarrantyAmount;
	}

	public double getArTransactionCreditMemoAmount() {
		return arTransactionCreditMemoAmount;
	}

	public void setArTransactionCreditMemoAmount(double arTransactionCreditMemoAmount) {
		this.arTransactionCreditMemoAmount = arTransactionCreditMemoAmount;
	}

	public double getArTransactionTotalAmount() {
		return arTransactionTotalAmount;
	}

	public void setArTransactionTotalAmount(double arTransactionTotalAmount) {
		this.arTransactionTotalAmount = arTransactionTotalAmount;
	}

	public double getArTransactionCashAmount() {
		return arTransactionCashAmount;
	}

	public void setArTransactionCashAmount(double arTransactionCashAmount) {
		this.arTransactionCashAmount = arTransactionCashAmount;
	}

	public double getArTransactionCheckAmount() {
		return arTransactionCheckAmount;
	}

	public void setArTransactionCheckAmount(double arTransactionCheckAmount) {
		this.arTransactionCheckAmount = arTransactionCheckAmount;
	}

	public double getArTransactionCreditCardAmount() {
		return arTransactionCreditCardAmount;
	}

	public void setArTransactionCreditCardAmount(double arTransactionCreditCardAmount) {
		this.arTransactionCreditCardAmount = arTransactionCreditCardAmount;
	}

	public String getSalesmanID() {
		return salesmanID;
	}

	public void setSalesmanID(String salesmanID) {
		this.salesmanID = salesmanID;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(String creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(String creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public MasterARTransactionStatus getArTransactionStatus() {
		return arTransactionStatus;
	}

	public void setArTransactionStatus(MasterARTransactionStatus arTransactionStatus) {
		this.arTransactionStatus = arTransactionStatus;
	}

	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public boolean isTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(boolean transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getShippingMethodID() {
		return shippingMethodID;
	}

	public void setShippingMethodID(String shippingMethodID) {
		this.shippingMethodID = shippingMethodID;
	}

	public String getVatScheduleID() {
		return vatScheduleID;
	}

	public void setVatScheduleID(String vatScheduleID) {
		this.vatScheduleID = vatScheduleID;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	public RMDocumentsTypeSetup getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(RMDocumentsTypeSetup transactionType) {
		this.transactionType = transactionType;
	}

	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}

	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public boolean isPayment() {
		return payment;
	}

	public void setPayment(boolean payment) {
		this.payment = payment;
	}

	public double getArServiceRepairAmount() {
		return arServiceRepairAmount;
	}

	public void setArServiceRepairAmount(double arServiceRepairAmount) {
		this.arServiceRepairAmount = arServiceRepairAmount;
	}

	public double getArReturnAmount() {
		return arReturnAmount;
	}

	public void setArReturnAmount(double arReturnAmount) {
		this.arReturnAmount = arReturnAmount;
	}
	
	
}