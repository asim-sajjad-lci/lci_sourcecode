/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the ar00104 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar00104")
@NamedQuery(name="SalesmanMaintenance.findAll", query="SELECT a FROM SalesmanMaintenance a")
public class SalesmanMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SALSPERID")
	private String salesmanId;

	@Column(name="ADDRESS1")
	private String address1;

	@Column(name="ADDRESS2")
	private String address2;

	@Column(name="APPLYPER")
	private int applyPercentage;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="COSTSLSAMT")
	private Double costOfSalesYTD;

	@Column(name="COSTSLSAMTL")
	private Double costOfSalesLastYear;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="EMPLOYID")
	private String employeeId;

	@Column(name="FRSTNAME")
	private String salesmanFirstName;

	@Column(name="FRSTNAMEA")
	private String salesmanFirstNameArabic;

	@Column(name="INACTIVE")
	private int inactive;

	@Column(name="LASTNAME")
	private String salesmanLastName;

	@Column(name="LASTNAMEA")
	private String salesmanLastNameArabic;

	@Column(name="MIDNAME")
	private String salesmanMidName;

	@Column(name="MIDNAMEA")
	private String salesmanMidNameArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PERCNTAMT")
	private Double percentageAmount;

	@Column(name="PHONE1")
	private String phone1;

	@Column(name="PHONE2")
	private String phone2;

	@Column(name="SALSTERRID")
	private String salesTerritoryId;

	@Column(name="SLSCOMMAMT")
	private Double salesCommissionsYTD;

	@Column(name="SLSCOMMAMTL")
	private Double salesCommissionsLastYear;

	@Column(name="TOTLCOMMAMT")
	private Double totalCommissionsYTD;

	@Column(name="TOTLCOMMAMTL")
	private Double totalCommissionsLastYear;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ar00102
	@OneToMany(mappedBy="salesmanMaintenance")
	private List<CustomerMaintenanceOptions> customerMaintenanceOptions;

	//bi-directional many-to-one association to Ar00200
	@OneToMany(mappedBy="salesmanMaintenance")
	private List<CustomerClassesSetup> customerClassesSetups;

	public SalesmanMaintenance() {
	}

	public String getSalesmanId() {
		return salesmanId;
	}

	public void setSalesmanId(String salesmanId) {
		this.salesmanId = salesmanId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public int getApplyPercentage() {
		return applyPercentage;
	}

	public void setApplyPercentage(int applyPercentage) {
		this.applyPercentage = applyPercentage;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public Double getCostOfSalesYTD() {
		return costOfSalesYTD;
	}

	public void setCostOfSalesYTD(Double costOfSalesYTD) {
		this.costOfSalesYTD = costOfSalesYTD;
	}

	public Double getCostOfSalesLastYear() {
		return costOfSalesLastYear;
	}

	public void setCostOfSalesLastYear(Double costOfSalesLastYear) {
		this.costOfSalesLastYear = costOfSalesLastYear;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getSalesmanFirstName() {
		return salesmanFirstName;
	}

	public void setSalesmanFirstName(String salesmanFirstName) {
		this.salesmanFirstName = salesmanFirstName;
	}

	public String getSalesmanFirstNameArabic() {
		return salesmanFirstNameArabic;
	}

	public void setSalesmanFirstNameArabic(String salesmanFirstNameArabic) {
		this.salesmanFirstNameArabic = salesmanFirstNameArabic;
	}

	public int getInactive() {
		return inactive;
	}

	public void setInactive(int inactive) {
		this.inactive = inactive;
	}

	public String getSalesmanLastName() {
		return salesmanLastName;
	}

	public void setSalesmanLastName(String salesmanLastName) {
		this.salesmanLastName = salesmanLastName;
	}

	public String getSalesmanLastNameArabic() {
		return salesmanLastNameArabic;
	}

	public void setSalesmanLastNameArabic(String salesmanLastNameArabic) {
		this.salesmanLastNameArabic = salesmanLastNameArabic;
	}

	public String getSalesmanMidName() {
		return salesmanMidName;
	}

	public void setSalesmanMidName(String salesmanMidName) {
		this.salesmanMidName = salesmanMidName;
	}

	public String getSalesmanMidNameArabic() {
		return salesmanMidNameArabic;
	}

	public void setSalesmanMidNameArabic(String salesmanMidNameArabic) {
		this.salesmanMidNameArabic = salesmanMidNameArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Double getPercentageAmount() {
		return percentageAmount;
	}

	public void setPercentageAmount(Double percentageAmount) {
		this.percentageAmount = percentageAmount;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}

	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}

	public Double getSalesCommissionsYTD() {
		return salesCommissionsYTD;
	}

	public void setSalesCommissionsYTD(Double salesCommissionsYTD) {
		this.salesCommissionsYTD = salesCommissionsYTD;
	}

	public Double getSalesCommissionsLastYear() {
		return salesCommissionsLastYear;
	}

	public void setSalesCommissionsLastYear(Double salesCommissionsLastYear) {
		this.salesCommissionsLastYear = salesCommissionsLastYear;
	}

	public Double getTotalCommissionsYTD() {
		return totalCommissionsYTD;
	}

	public void setTotalCommissionsYTD(Double totalCommissionsYTD) {
		this.totalCommissionsYTD = totalCommissionsYTD;
	}

	public Double getTotalCommissionsLastYear() {
		return totalCommissionsLastYear;
	}

	public void setTotalCommissionsLastYear(Double totalCommissionsLastYear) {
		this.totalCommissionsLastYear = totalCommissionsLastYear;
	}

	public List<CustomerMaintenanceOptions> getCustomerMaintenanceOptions() {
		return customerMaintenanceOptions;
	}

	public void setCustomerMaintenanceOptions(List<CustomerMaintenanceOptions> customerMaintenanceOptions) {
		this.customerMaintenanceOptions = customerMaintenanceOptions;
	}

	public List<CustomerClassesSetup> getCustomerClassesSetups() {
		return customerClassesSetups;
	}

	public void setCustomerClassesSetups(List<CustomerClassesSetup> customerClassesSetups) {
		this.customerClassesSetups = customerClassesSetups;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

	
}