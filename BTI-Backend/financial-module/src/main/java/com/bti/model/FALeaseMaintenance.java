/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the fa00105 database table.
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa00105")
@NamedQuery(name="FALeaseMaintenance.findAll", query="SELECT f FROM FALeaseMaintenance f")
public class FALeaseMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="FALEASEINDEX")
	private int faLeaseIndex;
	
	/*@Column(name="ASSTID")
	private String assetId;*/
	
	@Column(name="ASSTSERID")
	private int asstserid;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="COMPID")
	private String leaseCompanyId;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="LECONTCT")
	private String leaseContractNumber;

	@Column(name="LEENDDT")
	private Date leaseEndDate;

	@Column(name="LEINTERT")
	private double interestRatePercent;

	@Column(name="LEPYMNT")
	private double leasePayment;

	/*@Column(name="LETYP")
	private int leaseType;*/

	@Column(name="MODIFDT")
	private Date modifyDate;

	//bi-directional many-to-one association to Fa00101
	/*@ManyToOne
	@JoinColumn(name="ASSTSERID")
	private FAGeneralMaintenance faGeneralMaintenance;*/
	
	
	@Column(name="ASSTID")
	private String assetId;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	//bi-directional one-to-one association to Fa00101
	/*@OneToOne
	@JoinColumn(name="ASSTID")
	private FAGeneralMaintenance faGeneralMaintenances;*/
		
	//bi-directional many-to-one association to Fa40004
/*	@OneToMany(mappedBy="faLeaseMaintenance")
	private List<FALeaseCompanySetup> faLeaseCompanySetups;*/
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Fa40004
	@ManyToOne
	@JoinColumn(name="COMPINDEX")
	private FALeaseCompanySetup faLeaseCompanySetup;
	
	//bi-directional many-to-one association to LeaseType
	@ManyToOne
	@JoinColumn(name="LETYP")
	private LeaseType leaseType;

	public FALeaseMaintenance() {
	}

	/*public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}*/


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public String getLeaseCompanyId() {
		return leaseCompanyId;
	}

	public void setLeaseCompanyId(String leaseCompanyId) {
		this.leaseCompanyId = leaseCompanyId;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getLeaseContractNumber() {
		return leaseContractNumber;
	}

	public void setLeaseContractNumber(String leaseContractNumber) {
		this.leaseContractNumber = leaseContractNumber;
	}

	public Date getLeaseEndDate() {
		return leaseEndDate;
	}

	public void setLeaseEndDate(Date leaseEndDate) {
		this.leaseEndDate = leaseEndDate;
	}

	public double getInterestRatePercent() {
		return interestRatePercent;
	}

	public void setInterestRatePercent(double interestRatePercent) {
		this.interestRatePercent = interestRatePercent;
	}

	public double getLeasePayment() {
		return leasePayment;
	}

	public void setLeasePayment(double leasePayment) {
		this.leasePayment = leasePayment;
	}

	/*public int getLeaseType() {
		return leaseType;
	}

	public void setLeaseType(int leaseType) {
		this.leaseType = leaseType;
	}*/

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

/*	public FAGeneralMaintenance getFaGeneralMaintenance() {
		return faGeneralMaintenance;
	}

	public void setFaGeneralMaintenance(FAGeneralMaintenance faGeneralMaintenance) {
		this.faGeneralMaintenance = faGeneralMaintenance;
	}
*/
	/*public List<FALeaseCompanySetup> getFaLeaseCompanySetups() {
		return faLeaseCompanySetups;
	}

	public void setFaLeaseCompanySetups(List<FALeaseCompanySetup> faLeaseCompanySetups) {
		this.faLeaseCompanySetups = faLeaseCompanySetups;
	}*/

	

	public FALeaseCompanySetup getFaLeaseCompanySetup() {
		return faLeaseCompanySetup;
	}


	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public void setFaLeaseCompanySetup(FALeaseCompanySetup faLeaseCompanySetup) {
		this.faLeaseCompanySetup = faLeaseCompanySetup;
	}

	public LeaseType getLeaseType() {
		return leaseType;
	}

	public void setLeaseType(LeaseType leaseType) {
		this.leaseType = leaseType;
	}

	public int getAsstserid() {
		return asstserid;
	}

	public void setAsstserid(int asstserid) {
		this.asstserid = asstserid;
	}

	public int getFaLeaseIndex() {
		return faLeaseIndex;
	}

	public void setFaLeaseIndex(int faLeaseIndex) {
		this.faLeaseIndex = faLeaseIndex;
	}

	

	
	
}