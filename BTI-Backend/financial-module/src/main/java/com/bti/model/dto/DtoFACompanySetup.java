/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FACompanySetup;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoFACompanySetup {

	private int faCompanySetupId;
	private int bookIndexId;
	private String bookDescription;
	private String bookDescriptionArabic;
	private Integer postDetails;
	private Integer postPayableManagement;
	private Integer postPurchaseOrderProcessing;
	private Integer requireAssetAccount;
	private Integer autoAddBookInformation;
	private Integer defaultAssetLabel;
	private String bookId;
	private String messageType;
	private String updateAndSaveMessage;
	
	public DtoFACompanySetup() {
		
	}
	
	public DtoFACompanySetup(FACompanySetup faCompanySetup) {
		this.faCompanySetupId=faCompanySetup.getFaCompanySetupId();
		this.bookIndexId=0;
		if(faCompanySetup.getBookSetup()!=null){
			this.bookIndexId=faCompanySetup.getBookSetup().getBookInxd();
		}
		this.bookDescription="";
		if(UtilRandomKey.isNotBlank(faCompanySetup.getBookDescription())){
			this.bookDescription=faCompanySetup.getBookDescription();
		}
		this.bookDescriptionArabic="";
		if(UtilRandomKey.isNotBlank(faCompanySetup.getBookDescriptionArabic())){
			this.bookDescriptionArabic=faCompanySetup.getBookDescriptionArabic();
		}
		this.postDetails=faCompanySetup.getPostDetails();
		this.postPayableManagement=faCompanySetup.getPostPayableManagement();
		this.postPurchaseOrderProcessing=faCompanySetup.getPostPurchaseOrderProcessing();
		this.requireAssetAccount=faCompanySetup.getRequireAssetAccount();
		this.autoAddBookInformation=faCompanySetup.getAutoAddBookInformation();
		this.defaultAssetLabel=faCompanySetup.getDefaultAssetLabel();
	}
	public int getBookIndexId() {
		return bookIndexId;
	}
	public void setBookIndexId(int bookIndexId) {
		this.bookIndexId = bookIndexId;
	}
	public String getBookDescription() {
		return bookDescription;
	}
	public void setBookDescription(String bookDescription) {
		this.bookDescription = bookDescription;
	}
	public String getBookDescriptionArabic() {
		return bookDescriptionArabic;
	}
	public void setBookDescriptionArabic(String bookDescriptionArabic) {
		this.bookDescriptionArabic = bookDescriptionArabic;
	}
	public Integer getPostDetails() {
		return postDetails;
	}
	public void setPostDetails(Integer postDetails) {
		this.postDetails = postDetails;
	}
	public Integer getPostPayableManagement() {
		return postPayableManagement;
	}
	public void setPostPayableManagement(Integer postPayableManagement) {
		this.postPayableManagement = postPayableManagement;
	}
	public Integer getPostPurchaseOrderProcessing() {
		return postPurchaseOrderProcessing;
	}
	public void setPostPurchaseOrderProcessing(Integer postPurchaseOrderProcessing) {
		this.postPurchaseOrderProcessing = postPurchaseOrderProcessing;
	}
	public Integer getRequireAssetAccount() {
		return requireAssetAccount;
	}
	public void setRequireAssetAccount(Integer requireAssetAccount) {
		this.requireAssetAccount = requireAssetAccount;
	}
	public Integer getAutoAddBookInformation() {
		return autoAddBookInformation;
	}
	public void setAutoAddBookInformation(Integer autoAddBookInformation) {
		this.autoAddBookInformation = autoAddBookInformation;
	}
	public Integer getDefaultAssetLabel() {
		return defaultAssetLabel;
	}
	public void setDefaultAssetLabel(Integer defaultAssetLabel) {
		this.defaultAssetLabel = defaultAssetLabel;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public int getFaCompanySetupId() {
		return faCompanySetupId;
	}

	public void setFaCompanySetupId(int faCompanySetupId) {
		this.faCompanySetupId = faCompanySetupId;
	}

	public String getUpdateAndSaveMessage() {
		return updateAndSaveMessage;
	}

	public void setUpdateAndSaveMessage(String updateAndSaveMessage) {
		this.updateAndSaveMessage = updateAndSaveMessage;
	}
	
	
}
