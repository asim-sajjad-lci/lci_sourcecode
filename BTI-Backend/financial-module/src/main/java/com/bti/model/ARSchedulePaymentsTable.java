/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar10201 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar10201")
@NamedQuery(name="ARSchedulePaymentsTable.findAll", query="SELECT a FROM ARSchedulePaymentsTable a")
public class ARSchedulePaymentsTable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SCHPSEQC")
	private int schedulePaymentSequence;
	
	@Column(name="SCHPNMBR")
	private String schedulePaymentNumber;
	
	@Column(name="SCHPDUEDT")
	private Date schedulePaymentDueDate;

	@Column(name="SCHPAMNT")
	private Double schedulePaymentAmount;

	@Column(name="INTRAMNT")
	private Double scheduleInterestAmount;
	
	@Column(name="SCHBALNC")
	private Double scheduleBalance;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public int getSchedulePaymentSequence() {
		return schedulePaymentSequence;
	}

	public void setSchedulePaymentSequence(int schedulePaymentSequence) {
		this.schedulePaymentSequence = schedulePaymentSequence;
	}

	public String getSchedulePaymentNumber() {
		return schedulePaymentNumber;
	}

	public void setSchedulePaymentNumber(String schedulePaymentNumber) {
		this.schedulePaymentNumber = schedulePaymentNumber;
	}

	public Date getSchedulePaymentDueDate() {
		return schedulePaymentDueDate;
	}

	public void setSchedulePaymentDueDate(Date schedulePaymentDueDate) {
		this.schedulePaymentDueDate = schedulePaymentDueDate;
	}

	public Double getSchedulePaymentAmount() {
		return schedulePaymentAmount;
	}

	public void setSchedulePaymentAmount(Double schedulePaymentAmount) {
		this.schedulePaymentAmount = schedulePaymentAmount;
	}

	public Double getScheduleInterestAmount() {
		return scheduleInterestAmount;
	}

	public void setScheduleInterestAmount(Double scheduleInterestAmount) {
		this.scheduleInterestAmount = scheduleInterestAmount;
	}

	public Double getScheduleBalance() {
		return scheduleBalance;
	}

	public void setScheduleBalance(Double scheduleBalance) {
		this.scheduleBalance = scheduleBalance;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}
	
	
}