/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar90500 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar90500")
@NamedQuery(name="ARYTDHistoryPayments.findAll", query="SELECT a FROM ARYTDHistoryPayments a")
public class ARYTDHistoryPayments implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DOCNUMBR")
	private String paymentNumber;
	
	@Column(name="CUSTNMBR")
	private String customerNumber;
	
	@Column(name="CHEKBOKID")
	private String checkbookID;

	@Column(name="BATCHID")
	private String batchID;

	@Column(name="ARTRXTYP")
	private int arTransactionType;
	
	@Column(name="ARTRXNO")
	private String arTransactionNumber;
	//
	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="PYMTTYP")
	private int paymentType;
	//
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	
	@Column(name="CSHCHEKT")
	private String checkNumber;
	
	@Column(name="CARDEXPTM")
	private int creditCardExpireMonth;
	
	@Column(name="CARDEXPTY")
	private int creditCardExpireYear;
	
	@Column(name="PYMTDSCR")
	private String paymentDescription;
	
	@Column(name="PYMNTDDT")
	private Date paymentCreateDate;
	
	@Column(name="PYMNTUSR")
	private String paymentCreateByUserID;
	
	@Column(name="PYMNTPST")
	private Date paymentPostingDate;
	
	@Column(name="PYMNTPSTU")
	private String paymentPostingByUserID;
	
	@Column(name="PYMNTAMT")
	private Double paymentAmount;
	
	@Column(name="PYMNTAMTO")
	private Double originalPaymentAmount;
	
	@Column(name="EXGTBLID")
	private String exchangeTableID;
	
	@Column(name="XCHGRATE")
	private Double exchangeTableRate;
	
	@Column(name="YEAR1")
	private int year;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public int getArTransactionType() {
		return arTransactionType;
	}

	public void setArTransactionType(int arTransactionType) {
		this.arTransactionType = arTransactionType;
	}

	public String getArTransactionNumber() {
		return arTransactionNumber;
	}

	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public Date getPaymentCreateDate() {
		return paymentCreateDate;
	}

	public void setPaymentCreateDate(Date paymentCreateDate) {
		this.paymentCreateDate = paymentCreateDate;
	}

	public String getPaymentCreateByUserID() {
		return paymentCreateByUserID;
	}

	public void setPaymentCreateByUserID(String paymentCreateByUserID) {
		this.paymentCreateByUserID = paymentCreateByUserID;
	}

	public Date getPaymentPostingDate() {
		return paymentPostingDate;
	}

	public void setPaymentPostingDate(Date paymentPostingDate) {
		this.paymentPostingDate = paymentPostingDate;
	}

	public String getPaymentPostingByUserID() {
		return paymentPostingByUserID;
	}

	public void setPaymentPostingByUserID(String paymentPostingByUserID) {
		this.paymentPostingByUserID = paymentPostingByUserID;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Double getOriginalPaymentAmount() {
		return originalPaymentAmount;
	}

	public void setOriginalPaymentAmount(Double originalPaymentAmount) {
		this.originalPaymentAmount = originalPaymentAmount;
	}

	public String getExchangeTableID() {
		return exchangeTableID;
	}

	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}
	
	
	
	}