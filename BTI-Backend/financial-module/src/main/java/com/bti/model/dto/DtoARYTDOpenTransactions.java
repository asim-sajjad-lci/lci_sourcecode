package com.bti.model.dto;

import java.util.List;

import com.bti.model.ARYTDOpenTransactions;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoARYTDOpenTransactions {
	private String arTransactionNumber;
	private String customerID;
	private String arTransactionType;
	private String arTransactionDescription;
	private String batchID;
	private String arTransactionDate;
	private String customerName;
	private String customerNameArabic;
	private String currencyID;
	private String paymentTermsID;
	private Double aRTransactionCost;
	private Double arTransactionSalesAmount;
	private Double arTransactionTradeDiscount;
	private Double arTransactionFreightAmount;
	private Double arTransactionMiscellaneous;
	private Double arTransactionVATAmount;
	private Double arTransactionDebitMemoAmount;
	private Double arTransactionFinanceChargeAmount;
	private Double arTransactionWarrantyAmount;
	private Double arTransactionCreditMemoAmount;
	private Double arTransactionTotalAmount;
	private Double arTransactionCashAmount;
	private Double arTransactionCheckAmount;
	private Double arTransactionCreditCardAmount;
	private String salesmanID;
	private String checkbookID;
	private String cashReceiptNumber;
	private String checkNumber;
	private String creditCardID;
	private String creditCardNumber;
	private int creditCardExpireYear;
	private int creditCardExpireMonth;
	private int arTransactionStatus;
	private int exchangeTableIndex;
	private Double exchangeTableRate;
	private Boolean transactionVoid;
	private String shippingMethodID;
	private String vATScheduleID;
	private String postingDate;
	private String postingbyUserID;
	private String salesTerritoryId;
	private Boolean completePaymentApplied;
	private String paymentNumber;
	private int documentType;
	private String dueDate;
	private String applyDate;
	private Double applyAmount;
	private Double remainingAmount;
	private Integer pageNumber;
	private Integer pageSize;
	private Double functionalAmount;
	private Double originalAmount;
	List<DtoARYTDOpenTransactions> arYTDOpenTransactionsList;
	
	public DtoARYTDOpenTransactions() {
		// TODO Auto-generated constructor stub
	}
 
	public DtoARYTDOpenTransactions(ARYTDOpenTransactions arYTDOpenTransactions) 
	{
		 this.arTransactionNumber  = arYTDOpenTransactions.getArTransactionNumber();
		 this.customerID = arYTDOpenTransactions.getCustomerID();
		 this.customerName = arYTDOpenTransactions.getCustomerName();
		 this.customerNameArabic = arYTDOpenTransactions.getCustomerNameArabic();
		 this.arTransactionTotalAmount = UtilRoundDecimal.roundDecimalValue(arYTDOpenTransactions.getArTransactionTotalAmount());
		 this.applyAmount=0.0;
		 this.remainingAmount=UtilRoundDecimal.roundDecimalValue(arYTDOpenTransactions.getArTransactionTotalAmount());
		 this.dueDate="";
	}
	public String getArTransactionNumber() {
		return arTransactionNumber;
	}
	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getArTransactionType() {
		return arTransactionType;
	}
	public void setArTransactionType(String arTransactionType) {
		this.arTransactionType = arTransactionType;
	}
	public String getArTransactionDescription() {
		return arTransactionDescription;
	}
	public void setArTransactionDescription(String arTransactionDescription) {
		this.arTransactionDescription = arTransactionDescription;
	}
	public String getBatchID() {
		return batchID;
	}
	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}
	public String getArTransactionDate() {
		return arTransactionDate;
	}
	public void setArTransactionDate(String arTransactionDate) {
		this.arTransactionDate = arTransactionDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerNameArabic() {
		return customerNameArabic;
	}
	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}
	public String getCurrencyID() {
		return currencyID;
	}
	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}
	public String getPaymentTermsID() {
		return paymentTermsID;
	}
	public void setPaymentTermsID(String paymentTermsID) {
		this.paymentTermsID = paymentTermsID;
	}
	public Double getaRTransactionCost() {
		return aRTransactionCost;
	}
	public void setaRTransactionCost(Double aRTransactionCost) {
		this.aRTransactionCost = UtilRoundDecimal.roundDecimalValue(aRTransactionCost);
	}
	public Double getArTransactionSalesAmount() {
		return arTransactionSalesAmount;
	}
	public void setArTransactionSalesAmount(Double arTransactionSalesAmount) {
		this.arTransactionSalesAmount = UtilRoundDecimal.roundDecimalValue(arTransactionSalesAmount);
	}
	public Double getArTransactionTradeDiscount() {
		return arTransactionTradeDiscount;
	}
	public void setArTransactionTradeDiscount(Double arTransactionTradeDiscount) {
		this.arTransactionTradeDiscount = UtilRoundDecimal.roundDecimalValue(arTransactionTradeDiscount);
	}
	public Double getArTransactionFreightAmount() {
		return arTransactionFreightAmount;
	}
	public void setArTransactionFreightAmount(Double arTransactionFreightAmount) {
		this.arTransactionFreightAmount = UtilRoundDecimal.roundDecimalValue(arTransactionFreightAmount);
	}
	public Double getArTransactionMiscellaneous() {
		return arTransactionMiscellaneous;
	}
	public void setArTransactionMiscellaneous(Double arTransactionMiscellaneous) {
		this.arTransactionMiscellaneous = UtilRoundDecimal.roundDecimalValue(arTransactionMiscellaneous);
	}
	public Double getArTransactionVATAmount() {
		return arTransactionVATAmount;
	}
	public void setArTransactionVATAmount(Double arTransactionVATAmount) {
		this.arTransactionVATAmount = UtilRoundDecimal.roundDecimalValue(arTransactionVATAmount);
	}
	public Double getArTransactionDebitMemoAmount() {
		return arTransactionDebitMemoAmount;
	}
	public void setArTransactionDebitMemoAmount(Double arTransactionDebitMemoAmount) {
		this.arTransactionDebitMemoAmount = UtilRoundDecimal.roundDecimalValue(arTransactionDebitMemoAmount);
	}
	public Double getArTransactionFinanceChargeAmount() {
		return arTransactionFinanceChargeAmount;
	}
	public void setArTransactionFinanceChargeAmount(Double arTransactionFinanceChargeAmount) {
		this.arTransactionFinanceChargeAmount = UtilRoundDecimal.roundDecimalValue(arTransactionFinanceChargeAmount);
	}
	public Double getArTransactionWarrantyAmount() {
		return arTransactionWarrantyAmount;
	}
	public void setArTransactionWarrantyAmount(Double arTransactionWarrantyAmount) {
		this.arTransactionWarrantyAmount = UtilRoundDecimal.roundDecimalValue(arTransactionWarrantyAmount);
	}
	public Double getArTransactionCreditMemoAmount() {
		return arTransactionCreditMemoAmount;
	}
	public void setArTransactionCreditMemoAmount(Double arTransactionCreditMemoAmount) {
		this.arTransactionCreditMemoAmount = UtilRoundDecimal.roundDecimalValue(arTransactionCreditMemoAmount);
	}
	public Double getArTransactionTotalAmount() {
		return arTransactionTotalAmount;
	}
	public void setArTransactionTotalAmount(Double arTransactionTotalAmount) {
		this.arTransactionTotalAmount = UtilRoundDecimal.roundDecimalValue(arTransactionTotalAmount);
	}
	public Double getArTransactionCashAmount() {
		return arTransactionCashAmount;
	}
	public void setArTransactionCashAmount(Double arTransactionCashAmount) {
		this.arTransactionCashAmount = UtilRoundDecimal.roundDecimalValue(arTransactionCashAmount);
	}
	public Double getArTransactionCheckAmount() {
		return arTransactionCheckAmount;
	}
	public void setArTransactionCheckAmount(Double arTransactionCheckAmount) {
		this.arTransactionCheckAmount = UtilRoundDecimal.roundDecimalValue(arTransactionCheckAmount);
	}
	public Double getArTransactionCreditCardAmount() {
		return arTransactionCreditCardAmount;
	}
	public void setArTransactionCreditCardAmount(Double arTransactionCreditCardAmount) {
		this.arTransactionCreditCardAmount = UtilRoundDecimal.roundDecimalValue(arTransactionCreditCardAmount);
	}
	public String getSalesmanID() {
		return salesmanID;
	}
	public void setSalesmanID(String salesmanID) {
		this.salesmanID = salesmanID;
	}
	public String getCheckbookID() {
		return checkbookID;
	}
	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}
	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}
	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}
	public String getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
	public String getCreditCardID() {
		return creditCardID;
	}
	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}
	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}
	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}
	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}
	public int getArTransactionStatus() {
		return arTransactionStatus;
	}
	public void setArTransactionStatus(int arTransactionStatus) {
		this.arTransactionStatus = arTransactionStatus;
	}
	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}
	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}
	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}
	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}
	public Boolean getTransactionVoid() {
		return transactionVoid;
	}
	public void setTransactionVoid(Boolean transactionVoid) {
		this.transactionVoid = transactionVoid;
	}
	public String getShippingMethodID() {
		return shippingMethodID;
	}
	public void setShippingMethodID(String shippingMethodID) {
		this.shippingMethodID = shippingMethodID;
	}
	public String getvATScheduleID() {
		return vATScheduleID;
	}
	public void setvATScheduleID(String vATScheduleID) {
		this.vATScheduleID = vATScheduleID;
	}
	public String getPostingDate() {
		return postingDate;
	}
	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}
	public String getPostingbyUserID() {
		return postingbyUserID;
	}
	public void setPostingbyUserID(String postingbyUserID) {
		this.postingbyUserID = postingbyUserID;
	}
	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}
	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}
	public Boolean getCompletePaymentApplied() {
		return completePaymentApplied;
	}
	public void setCompletePaymentApplied(Boolean completePaymentApplied) {
		this.completePaymentApplied = completePaymentApplied;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public int getDocumentType() {
		return documentType;
	}

	public void setDocumentType(int documentType) {
		this.documentType = documentType;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public Double getApplyAmount() {
		return applyAmount;
	}

	public void setApplyAmount(Double applyAmount) {
		this.applyAmount = UtilRoundDecimal.roundDecimalValue(applyAmount);
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = UtilRoundDecimal.roundDecimalValue(remainingAmount);
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Double getFunctionalAmount() {
		return functionalAmount;
	}

	public void setFunctionalAmount(Double functionalAmount) {
		this.functionalAmount = UtilRoundDecimal.roundDecimalValue(functionalAmount);
	}

	public Double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(Double originalAmount) {
		this.originalAmount = UtilRoundDecimal.roundDecimalValue(originalAmount);
	}

	public List<DtoARYTDOpenTransactions> getArYTDOpenTransactionsList() {
		return arYTDOpenTransactionsList;
	}

	public void setArYTDOpenTransactionsList(List<DtoARYTDOpenTransactions> arYTDOpenTransactionsList) {
		this.arYTDOpenTransactionsList = arYTDOpenTransactionsList;
	}

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	
	
}
