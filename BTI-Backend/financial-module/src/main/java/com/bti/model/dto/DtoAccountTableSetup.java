/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoAccountTableSetup {

	private int accountGroupIndex;
	private String accountDescription;
	private String accountDescriptionArabic;
	private List<Long> accountNumberIndex;
	private String assetId;
	private String assetDescription;
	private String assetDescriptionArabic;
	
	private List<DtoAccountTableSetup> accountNumberList;
	private List<DtoPayableAccountClassSetup> accNumberIndexValue;
	private String messageType;
	private Integer id;
	private Integer segmentNumber;
	private Integer indexId;
	private Integer transactionType;
	private Integer accountType;
	private String accountTypeValue;
	private String classId;
	private String description;
	private String vendorClassId;
	private String accountNumber;
	private String type;
	private String accountTableRowIndex;
	private String accountValue;
	
	
	public int getAccountGroupIndex() {
		return accountGroupIndex;
	}
	public void setAccountGroupIndex(int accountGroupIndex) {
		this.accountGroupIndex = accountGroupIndex;
	}
	public String getAccountDescription() {
		return accountDescription;
	}
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	public List<Long> getAccountNumberIndex() {
		return accountNumberIndex;
	}
	public void setAccountNumberIndex(List<Long> accountNumberIndex) {
		this.accountNumberIndex = accountNumberIndex;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getAssetDescription() {
		return assetDescription;
	}
	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}
	public String getAssetDescriptionArabic() {
		return assetDescriptionArabic;
	}
	public void setAssetDescriptionArabic(String assetDescriptionArabic) {
		this.assetDescriptionArabic = assetDescriptionArabic;
	}
	public List<DtoAccountTableSetup> getAccountNumberList() {
		return accountNumberList;
	}
	public void setAccountNumberList(List<DtoAccountTableSetup> accountNumberList) {
		this.accountNumberList = accountNumberList;
	}
	
	public List<DtoPayableAccountClassSetup> getAccNumberIndexValue() {
		return accNumberIndexValue;
	}
	public void setAccNumberIndexValue(List<DtoPayableAccountClassSetup> accNumberIndexValue) {
		this.accNumberIndexValue = accNumberIndexValue;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSegmentNumber() {
		return segmentNumber;
	}
	public void setSegmentNumber(Integer segmentNumber) {
		this.segmentNumber = segmentNumber;
	}
	public Integer getIndexId() {
		return indexId;
	}
	public void setIndexId(Integer indexId) {
		this.indexId = indexId;
	}
	public Integer getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public String getAccountTypeValue() {
		return accountTypeValue;
	}
	public void setAccountTypeValue(String accountTypeValue) {
		this.accountTypeValue = accountTypeValue;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getVendorClassId() {
		return vendorClassId;
	}
	public void setVendorClassId(String vendorClassId) {
		this.vendorClassId = vendorClassId;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}
	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}
	public String getAccountDescriptionArabic() {
		return accountDescriptionArabic;
	}
	public void setAccountDescriptionArabic(String accountDescriptionArabic) {
		this.accountDescriptionArabic = accountDescriptionArabic;
	}
	public String getAccountValue() {
		return accountValue;
	}
	public void setAccountValue(String accountValue) {
		this.accountValue = accountValue;
	}
	
}
