/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the fa00101 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa00101")
@NamedQuery(name="FAGeneralMaintenance.findAll", query="SELECT f FROM FAGeneralMaintenance f")
public class FAGeneralMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ASSETID")
	private String assetId;
	
	@Column(name="ASSTSERID")
	private int assetIdSerial;
	
	@Column(name="ACQUSTCOT")
	private Double FAAcquisitionCost;

	@Column(name="ACQUSTDT")
	private Date FAAcquisitionDate;

	@Column(name="ASSTADD")
	private Date FAAddedDate;

	@Column(name="ASSTLABEL")
	private String assetLabelId;
	
	@Column(name="ASSTMASTID")
	private String faMasterAssetId;

	@Column(name="ASSTQTY")
	private int assetQuantity;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@ManyToOne
	@JoinColumn(name="ASSTSTUT")
	private MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus;

	@ManyToOne
	@JoinColumn(name="ASSTTYP")
	private MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	
	@ManyToOne
	@JoinColumn(name="CURNCYID")
	private CurrencySetup currencySetup;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRPTN")
	private String FADescription;

	@Column(name="DSCRPTNA")
	private String FADescriptionArabic;

	@Column(name="EMPLOYID")
	private String employeeId;

	@Column(name="EXTDSCRPTN")
	private String assetExtendedDescription;

	@Column(name="LSTMAINTAN")
	private Date lastMaintenance;

	@Column(name="MANFCNAME")
	private String manufacturerName;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="SHRTNAME")
	private String FAShortName;

	//bi-directional many-to-one association to Fa40100
	@ManyToOne
	@JoinColumn(name="ACCGROPID")
	private FAAccountGroupsSetup faAccountGroupsSetup;

	//bi-directional many-to-one association to Fa40002
	@ManyToOne
	@JoinColumn(name="CLASSID")
	private FAClassSetup faClassSetup;

	//bi-directional many-to-one association to Fa40005
	@ManyToOne
	@JoinColumn(name="LOCATNID")
	private FALocationSetup faLocationSetup;

	//bi-directional many-to-one association to Fa40044
	@ManyToOne
	@JoinColumn(name="PROPTTYPID")
	private FAPropertyTypes faPropertyTypes;

	//bi-directional many-to-one association to Fa40006
	@ManyToOne
	@JoinColumn(name="PHYSCALID")
	private FAPhysicalLocationSetup faPhysicalLocationSetup;

	//bi-directional many-to-one association to Fa40009
	@ManyToOne
	@JoinColumn(name="STRCID")
	private FAStructureSetup faStructureSetup;

	//bi-directional one-to-one association to Fa00102
	/*@OneToOne(mappedBy="faGeneralMaintenance")
	private FAAccountTableSetup faAccountTableSetup;*/

	//bi-directional one-to-one association to Fa00103
	@OneToOne(mappedBy="faGeneralMaintenance")
	private FABookMaintenance faBookMaintenance;

	//bi-directional many-to-one association to Fa00104
	@OneToMany(mappedBy="faGeneralMaintenance")
	private List<FAInsuranceMaintenance> faInsuranceMaintenances;

	//bi-directional many-to-one association to Fa00104
	/*@OneToMany(mappedBy="faGeneralMaintenance")
	private List<FAInsuranceMaintenance> faInsuranceMaintenances2;*/

	//bi-directional one-to-one association to Fa00105
	/*@OneToOne(mappedBy="faGeneralMaintenances")
	private FALeaseMaintenance faLeaseMaintenance;*/

	//bi-directional many-to-one association to Fa00105
	/*@OneToMany(mappedBy="faGeneralMaintenance")
	private List<FALeaseMaintenance> faLeaseMaintenances;*/

	public FAGeneralMaintenance() {
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public int getAssetIdSerial() {
		return assetIdSerial;
	}

	public void setAssetIdSerial(int assetIdSerial) {
		this.assetIdSerial = assetIdSerial;
	}

	public Double getFAAcquisitionCost() {
		return FAAcquisitionCost;
	}

	public void setFAAcquisitionCost(Double fAAcquisitionCost) {
		FAAcquisitionCost = fAAcquisitionCost;
	}

	public Date getFAAcquisitionDate() {
		return FAAcquisitionDate;
	}

	public void setFAAcquisitionDate(Date fAAcquisitionDate) {
		FAAcquisitionDate = fAAcquisitionDate;
	}

	public Date getFAAddedDate() {
		return FAAddedDate;
	}

	public void setFAAddedDate(Date fAAddedDate) {
		FAAddedDate = fAAddedDate;
	}

	public String getAssetLabelId() {
		return assetLabelId;
	}

	public void setAssetLabelId(String assetLabelId) {
		this.assetLabelId = assetLabelId;
	}

	public String getFaMasterAssetId() {
		return faMasterAssetId;
	}

	public void setFaMasterAssetId(String faMasterAssetId) {
		this.faMasterAssetId = faMasterAssetId;
	}

	public int getAssetQuantity() {
		return assetQuantity;
	}

	public void setAssetQuantity(int assetQuantity) {
		this.assetQuantity = assetQuantity;
	}

	public MasterGeneralMaintenanceAssetStatus getMasterGeneralMaintenanceAssetStatus() {
		return masterGeneralMaintenanceAssetStatus;
	}

	public void setMasterGeneralMaintenanceAssetStatus(
			MasterGeneralMaintenanceAssetStatus masterGeneralMaintenanceAssetStatus) {
		this.masterGeneralMaintenanceAssetStatus = masterGeneralMaintenanceAssetStatus;
	}

	public MasterGeneralMaintenanceAssetType getMasterGeneralMaintenanceAssetType() {
		return masterGeneralMaintenanceAssetType;
	}

	public void setMasterGeneralMaintenanceAssetType(MasterGeneralMaintenanceAssetType masterGeneralMaintenanceAssetType) {
		this.masterGeneralMaintenanceAssetType = masterGeneralMaintenanceAssetType;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public CurrencySetup getCurrencySetup() {
		return currencySetup;
	}

	public void setCurrencySetup(CurrencySetup currencySetup) {
		this.currencySetup = currencySetup;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getFADescription() {
		return FADescription;
	}

	public void setFADescription(String fADescription) {
		FADescription = fADescription;
	}

	public String getFADescriptionArabic() {
		return FADescriptionArabic;
	}

	public void setFADescriptionArabic(String fADescriptionArabic) {
		FADescriptionArabic = fADescriptionArabic;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getAssetExtendedDescription() {
		return assetExtendedDescription;
	}

	public void setAssetExtendedDescription(String assetExtendedDescription) {
		this.assetExtendedDescription = assetExtendedDescription;
	}

	public Date getLastMaintenance() {
		return lastMaintenance;
	}

	public void setLastMaintenance(Date lastMaintenance) {
		this.lastMaintenance = lastMaintenance;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getFAShortName() {
		return FAShortName;
	}

	public void setFAShortName(String fAShortName) {
		FAShortName = fAShortName;
	}

	public FAAccountGroupsSetup getFaAccountGroupsSetup() {
		return faAccountGroupsSetup;
	}

	public void setFaAccountGroupsSetup(FAAccountGroupsSetup faAccountGroupsSetup) {
		this.faAccountGroupsSetup = faAccountGroupsSetup;
	}

	public FAClassSetup getFaClassSetup() {
		return faClassSetup;
	}

	public void setFaClassSetup(FAClassSetup faClassSetup) {
		this.faClassSetup = faClassSetup;
	}

	public FALocationSetup getFaLocationSetup() {
		return faLocationSetup;
	}

	public void setFaLocationSetup(FALocationSetup faLocationSetup) {
		this.faLocationSetup = faLocationSetup;
	}

	public FAPropertyTypes getFaPropertyTypes() {
		return faPropertyTypes;
	}

	public void setFaPropertyTypes(FAPropertyTypes faPropertyTypes) {
		this.faPropertyTypes = faPropertyTypes;
	}

	public FAPhysicalLocationSetup getFaPhysicalLocationSetup() {
		return faPhysicalLocationSetup;
	}

	public void setFaPhysicalLocationSetup(FAPhysicalLocationSetup faPhysicalLocationSetup) {
		this.faPhysicalLocationSetup = faPhysicalLocationSetup;
	}

	public FAStructureSetup getFaStructureSetup() {
		return faStructureSetup;
	}

	public void setFaStructureSetup(FAStructureSetup faStructureSetup) {
		this.faStructureSetup = faStructureSetup;
	}

/*	public FAAccountTableSetup getFaAccountTableSetup() {
		return faAccountTableSetup;
	}

	public void setFaAccountTableSetup(FAAccountTableSetup faAccountTableSetup) {
		this.faAccountTableSetup = faAccountTableSetup;
	}*/

	public FABookMaintenance getFaBookMaintenance() {
		return faBookMaintenance;
	}

	public void setFaBookMaintenance(FABookMaintenance faBookMaintenance) {
		this.faBookMaintenance = faBookMaintenance;
	}

	public List<FAInsuranceMaintenance> getFaInsuranceMaintenances() {
		return faInsuranceMaintenances;
	}

	public void setFaInsuranceMaintenances(List<FAInsuranceMaintenance> faInsuranceMaintenances) {
		this.faInsuranceMaintenances = faInsuranceMaintenances;
	}

	/*public FALeaseMaintenance getFaLeaseMaintenance() {
		return faLeaseMaintenance;
	}

	public void setFaLeaseMaintenance(FALeaseMaintenance faLeaseMaintenance) {
		this.faLeaseMaintenance = faLeaseMaintenance;
	}*/

	/*public List<FALeaseMaintenance> getFaLeaseMaintenances() {
		return faLeaseMaintenances;
	}

	public void setFaLeaseMaintenances(List<FALeaseMaintenance> faLeaseMaintenances) {
		this.faLeaseMaintenances = faLeaseMaintenances;
	}
*/
	/*public List<FAInsuranceMaintenance> getFaInsuranceMaintenances2() {
		return faInsuranceMaintenances2;
	}

	public void setFaInsuranceMaintenances2(List<FAInsuranceMaintenance> faInsuranceMaintenances2) {
		this.faInsuranceMaintenances2 = faInsuranceMaintenances2;
	}*/

	
	
}