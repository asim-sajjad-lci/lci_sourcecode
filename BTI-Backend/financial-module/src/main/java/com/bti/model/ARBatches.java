/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar80000 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar80000")
@NamedQuery(name="ARBatches.findAll", query="SELECT a FROM ARBatches a")
public class ARBatches implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
 	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="BATCHID")
	private String batchID;
	
	@Column(name="BADSCR")
	private String batchDescription;
	
	@Column(name="BADSCRA")
	private String batchDescriptionArabic;

	@ManyToOne
	@JoinColumn(name="BACHTYPE")
	private BatchTransactionType batchTransactionType;

	@Column(name="TOTTRXNO")
	private int totalNumberTransactionInBatch;
	
	@Column(name="TOTTRXAMT")
	private Double totalAmountTransactionInBatch;
	
	@Column(name="APPRVAL")
	private int statusApprovalForPosting;
	
	@Column(name="APPRDDT")
	private Date approvalDate;
	
	@Column(name="APPRUSR")
	private String approvalByUserID;
	
	@Column(name="CREATDDT")
	private Date createDate;
	
	@Column(name="MODIFTDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifybyUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;
	
	@Column(name="POST_DATE")
	private Date postingDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private Boolean isDeleted;
	  

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getBatchDescription() {
		return batchDescription;
	}

	public void setBatchDescription(String batchDescription) {
		this.batchDescription = batchDescription;
	}

	public String getBatchDescriptionArabic() {
		return batchDescriptionArabic;
	}

	public void setBatchDescriptionArabic(String batchDescriptionArabic) {
		this.batchDescriptionArabic = batchDescriptionArabic;
	}

	public BatchTransactionType getBatchTransactionType() {
		return batchTransactionType;
	}

	public void setBatchTransactionType(BatchTransactionType batchTransactionType) {
		this.batchTransactionType = batchTransactionType;
	}

	public int getTotalNumberTransactionInBatch() {
		return totalNumberTransactionInBatch;
	}

	public void setTotalNumberTransactionInBatch(int totalNumberTransactionInBatch) {
		this.totalNumberTransactionInBatch = totalNumberTransactionInBatch;
	}

	public Double getTotalAmountTransactionInBatch() {
		return totalAmountTransactionInBatch;
	}

	public void setTotalAmountTransactionInBatch(Double totalAmountTransactionInBatch) {
		this.totalAmountTransactionInBatch = totalAmountTransactionInBatch;
	}

	public int getStatusApprovalForPosting() {
		return statusApprovalForPosting;
	}

	public void setStatusApprovalForPosting(int statusApprovalForPosting) {
		this.statusApprovalForPosting = statusApprovalForPosting;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalByUserID() {
		return approvalByUserID;
	}

	public void setApprovalByUserID(String approvalByUserID) {
		this.approvalByUserID = approvalByUserID;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifybyUserID() {
		return modifybyUserID;
	}

	public void setModifybyUserID(String modifybyUserID) {
		this.modifybyUserID = modifybyUserID;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
}