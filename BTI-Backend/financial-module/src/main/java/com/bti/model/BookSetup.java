/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the fa40000 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40000")
@NamedQuery(name="BookSetup.findAll", query="SELECT f FROM BookSetup f")
public class BookSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BOKINXD")
	private int bookInxd;

	@Column(name="BOKID")
	private String bookId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="CURRYEAR")
	private int currentYear;
	
	@ManyToOne
	@JoinColumn(name="DEPPEROD")
	private MasterDepreciationPeriodTypes masterDepreciationPeriodTypes;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRPTN")
	private String bookDescription;

	@Column(name="DSCRPTNA")
	private String bookDescriptionArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Fa40001
	@ManyToOne
	@JoinColumn(name="CALENDINDX")
	private FACalendarSetup faCalendarSetup;

	//bi-directional one-to-one association to Fa40003
	@OneToOne(mappedBy="bookSetup")
	private FACompanySetup faCompanySetup;
	
	//bi-directional many-to-one association to Fa00103
	@OneToMany(mappedBy="bookSetup")
	private List<FABookMaintenance> faBookMaintenances;
	
	//bi-directional many-to-one association to fa40013
		@OneToMany(mappedBy="bookSetup")
		private List<FABookClassSetup> faBookClassSetups;
		
	public List<FABookClassSetup> getFaBookClassSetups() {
			return faBookClassSetups;
		}

		public void setFaBookClassSetups(List<FABookClassSetup> faBookClassSetups) {
			this.faBookClassSetups = faBookClassSetups;
		}

	public List<FABookMaintenance> getFaBookMaintenances() {
			return faBookMaintenances;
		}

		public void setFaBookMaintenances(List<FABookMaintenance> faBookMaintenances) {
			this.faBookMaintenances = faBookMaintenances;
		}

	public int getBookInxd() {
		return bookInxd;
	}

	public void setBookInxd(int bookInxd) {
		this.bookInxd = bookInxd;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getBookDescription() {
		return bookDescription;
	}

	public void setBookDescription(String bookDescription) {
		this.bookDescription = bookDescription;
	}

	public String getBookDescriptionArabic() {
		return bookDescriptionArabic;
	}

	public void setBookDescriptionArabic(String bookDescriptionArabic) {
		this.bookDescriptionArabic = bookDescriptionArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public FACalendarSetup getFaCalendarSetup() {
		return faCalendarSetup;
	}

	public void setFaCalendarSetup(FACalendarSetup faCalendarSetup) {
		this.faCalendarSetup = faCalendarSetup;
	}

	public FACompanySetup getFaCompanySetup() {
		return faCompanySetup;
	}

	public void setFaCompanySetup(FACompanySetup faCompanySetup) {
		this.faCompanySetup = faCompanySetup;
	}

	public MasterDepreciationPeriodTypes getMasterDepreciationPeriodTypes() {
		return masterDepreciationPeriodTypes;
	}

	public void setMasterDepreciationPeriodTypes(MasterDepreciationPeriodTypes masterDepreciationPeriodTypes) {
		this.masterDepreciationPeriodTypes = masterDepreciationPeriodTypes;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
}