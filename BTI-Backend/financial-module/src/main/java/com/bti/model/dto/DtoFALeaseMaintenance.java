/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FALeaseMaintenance;
import com.bti.util.UtilDateAndTime;
import com.bti.util.UtilRoundDecimal;

/**
 * Description: DtoFALeaseMaintenance
 * Name of Project: BTI
 * Created on: Sep 01, 2017
 * Modified on: Sep 01, 2017 11:08:38 AM
 * @author seasia
 * Version: 
 */
public class DtoFALeaseMaintenance {

	private int leaseMaintenanceIndex;
	private String assetId;
	private String leaseCompanyId;
	private int leaseCompanyIndex;
	private int assetSerialId;
	private int leaseTypeIndex;
	private String leaseContactNumber;
	private double leasePayment;
	private double interestRatePercent;
	private String leaseEndDate;
	
	public DtoFALeaseMaintenance(){
		
	}
	public DtoFALeaseMaintenance(FALeaseMaintenance faLeaseMaintenance){
		this.leaseMaintenanceIndex = faLeaseMaintenance.getFaLeaseIndex();
		this.assetId = faLeaseMaintenance.getAssetId();
		if(faLeaseMaintenance.getFaLeaseCompanySetup()!=null){
			this.leaseCompanyIndex = faLeaseMaintenance.getFaLeaseCompanySetup().getLeaseCompanyIndex();
		this.leaseCompanyId = faLeaseMaintenance.getFaLeaseCompanySetup().getCompanyId();
		}
		this.assetSerialId = faLeaseMaintenance.getAsstserid();
		this.leaseTypeIndex = faLeaseMaintenance.getLeaseType().getIdindex();
		this.leaseContactNumber = faLeaseMaintenance.getLeaseContractNumber();
		this.leasePayment = UtilRoundDecimal.roundDecimalValue(faLeaseMaintenance.getLeasePayment());
		this.interestRatePercent = UtilRoundDecimal.roundDecimalValue(faLeaseMaintenance.getInterestRatePercent());
		this.leaseEndDate="";
		if(faLeaseMaintenance.getLeaseEndDate()!=null){
			this.leaseEndDate = UtilDateAndTime.dateToStringddmmyyyy(faLeaseMaintenance.getLeaseEndDate());
		}
	}
	
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	
	public String getLeaseCompanyId() {
		return leaseCompanyId;
	}
	public void setLeaseCompanyId(String leaseCompanyId) {
		this.leaseCompanyId = leaseCompanyId;
	}
	public int getAssetSerialId() {
		return assetSerialId;
	}
	public void setAssetSerialId(int assetSerialId) {
		this.assetSerialId = assetSerialId;
	}
	
	public int getLeaseTypeIndex() {
		return leaseTypeIndex;
	}
	public void setLeaseTypeIndex(int leaseTypeIndex) {
		this.leaseTypeIndex = leaseTypeIndex;
	}
	public String getLeaseContactNumber() {
		return leaseContactNumber;
	}
	public void setLeaseContactNumber(String leaseContactNumber) {
		this.leaseContactNumber = leaseContactNumber;
	}
	public double getLeasePayment() {
		return leasePayment;
	}
	public void setLeasePayment(double leasePayment) {
		this.leasePayment = UtilRoundDecimal.roundDecimalValue(leasePayment);
	}
	public double getInterestRatePercent() {
		return interestRatePercent;
	}
	public void setInterestRatePercent(double interestRatePercent) {
		this.interestRatePercent = UtilRoundDecimal.roundDecimalValue(interestRatePercent);
	}
	public String getLeaseEndDate() {
		return leaseEndDate;
	}
	public void setLeaseEndDate(String leaseEndDate) {
		this.leaseEndDate = leaseEndDate;
	}
	public int getLeaseCompanyIndex() {
		return leaseCompanyIndex;
	}
	public void setLeaseCompanyIndex(int leaseCompanyIndex) {
		this.leaseCompanyIndex = leaseCompanyIndex;
	}
	public int getLeaseMaintenanceIndex() {
		return leaseMaintenanceIndex;
	}
	public void setLeaseMaintenanceIndex(int leaseMaintenanceIndex) {
		this.leaseMaintenanceIndex = leaseMaintenanceIndex;
	}
	
	
	
}
