/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar90600 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar90600")
@NamedQuery(name="ARYTDHistoryApplyDocumentsPayments.findAll", query="SELECT a FROM ARYTDHistoryApplyDocumentsPayments a")
public class ARYTDHistoryApplyDocumentsPayments implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CUSTNMBR")
	private String customerNumber;
	
	@Column(name="ARTRXNMBR")
	private String arTransactionNumber;
	
	@Column(name="ARTRXDT")
	private Date arTransactionDate;

	@Column(name="ARTRXPODT")
	private Date arTransactionPostingDate;

	@Column(name="APPLDT")
	private Date applyDate;
	
	@Column(name="APPDOCNMR")
	private String applyDocumentNumberPaymentNumber;
	//
	@Column(name="APPLAMNT")
	private Double applyAmount;
	
	@Column(name="ORAPPAMNT")
	private Double originalAmount;
	//
	@Column(name="REAPPAMNT")
	private Double remainAmountAfterApply;
	
	@Column(name="TRXPOST")
	private int paymentAlreadyPosted;
	
	@Column(name="YEAR1")
	private Date year;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getArTransactionNumber() {
		return arTransactionNumber;
	}

	public void setArTransactionNumber(String arTransactionNumber) {
		this.arTransactionNumber = arTransactionNumber;
	}

	public Date getArTransactionDate() {
		return arTransactionDate;
	}

	public void setArTransactionDate(Date arTransactionDate) {
		this.arTransactionDate = arTransactionDate;
	}

	public Date getArTransactionPostingDate() {
		return arTransactionPostingDate;
	}

	public void setArTransactionPostingDate(Date arTransactionPostingDate) {
		this.arTransactionPostingDate = arTransactionPostingDate;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public String getApplyDocumentNumberPaymentNumber() {
		return applyDocumentNumberPaymentNumber;
	}

	public void setApplyDocumentNumberPaymentNumber(String applyDocumentNumberPaymentNumber) {
		this.applyDocumentNumberPaymentNumber = applyDocumentNumberPaymentNumber;
	}

	public Double getApplyAmount() {
		return applyAmount;
	}

	public void setApplyAmount(Double applyAmount) {
		this.applyAmount = applyAmount;
	}

	public Double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(Double originalAmount) {
		this.originalAmount = originalAmount;
	}

	public Double getRemainAmountAfterApply() {
		return remainAmountAfterApply;
	}

	public void setRemainAmountAfterApply(Double remainAmountAfterApply) {
		this.remainAmountAfterApply = remainAmountAfterApply;
	}

	public int getPaymentAlreadyPosted() {
		return paymentAlreadyPosted;
	}

	public void setPaymentAlreadyPosted(int paymentAlreadyPosted) {
		this.paymentAlreadyPosted = paymentAlreadyPosted;
	}

	public Date getYear() {
		return year;
	}

	public void setYear(Date year) {
		this.year = year;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}
	
	
	}