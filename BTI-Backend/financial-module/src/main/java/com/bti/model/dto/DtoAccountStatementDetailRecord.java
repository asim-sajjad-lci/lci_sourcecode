/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
* Description: DtoAccountStatementDetailRecord class having getter and setter for fields (POJO) Name
* Name of Project: BTI
* Created on: AUG 10, 2017
* Modified on: AUG 10, 2017 4:19:38 PM
* @author SNS
* Version: 1.0
*/

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoAccountStatementDetailRecord {

	private String journalNo;
	private String journalDate;
	private String distributionReference;
	private double debitAmount;
	private double creditAmount;
	private double balance;
	public String getJournalNo() {
		return journalNo;
	}
	public void setJournalNo(String journalNo) {
		this.journalNo = journalNo;
	}
	public String getJournalDate() {
		return journalDate;
	}
	public void setJournalDate(String journalDate) {
		this.journalDate = journalDate;
	}
	public String getDistributionReference() {
		return distributionReference;
	}
	public void setDistributionReference(String distributionReference) {
		this.distributionReference = distributionReference;
	}
	public double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}
	public double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
	
	
}
