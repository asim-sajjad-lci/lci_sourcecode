/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ap50000 database table.
 * 
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap50000")
@NamedQuery(name = "APSummaryBalances.findAll", query = "SELECT a FROM APSummaryBalances a")
public class APSummaryBalances implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	// @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "VENDORID")
	private String vendorNumber;

	@Column(name = "APTRXTYP")
	private int apTransactionType;

	@Column(name = "TOTAMNT")
	private double totalAmount;

	@Column(name = "LSTUPDDT")
	private Date lastUpdateDate;

	@Column(name = "LSTUPPST")
	private Date lastPostingDateLastTransaction;
	 
	@Column(name = "DEX_ROW_ID")
	private int rowIndexId;

	@Column(name = "DEX_ROW_TS")
	private Date rowDateIndex;

	public String getVendorNumber() {
		return vendorNumber;
	}

	public void setVendorNumber(String vendorNumber) {
		this.vendorNumber = vendorNumber;
	}

	public int getApTransactionType() {
		return apTransactionType;
	}

	public void setApTransactionType(int apTransactionType) {
		this.apTransactionType = apTransactionType;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public Date getLastPostingDateLastTransaction() {
		return lastPostingDateLastTransaction;
	}

	public void setLastPostingDateLastTransaction(Date lastPostingDateLastTransaction) {
		this.lastPostingDateLastTransaction = lastPostingDateLastTransaction;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}