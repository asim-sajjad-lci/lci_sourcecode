/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

public class DtoMassCloseFiscalPeriodSetupRequest {

	private Integer seriesId;
	private Integer year;
	private String origin;
	private Integer originId;
	private boolean allRecord;
	private boolean allOrigin;
	private Integer startPeriodId;
	private Integer endPeriodId;

	public Integer getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(Integer seriesId) {
		this.seriesId = seriesId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public boolean getAllRecord() {
		return allRecord;
	}

	public void setAllRecord(boolean allRecord) {
		this.allRecord = allRecord;
	}

	public Integer getStartPeriodId() {
		return startPeriodId;
	}

	public void setStartPeriodId(Integer startPeriodId) {
		this.startPeriodId = startPeriodId;
	}

	public Integer getEndPeriodId() {
		return endPeriodId;
	}

	public void setEndPeriodId(Integer endPeriodId) {
		this.endPeriodId = endPeriodId;
	}

	public boolean getAllOrigin() {
		return allOrigin;
	}

	public void setAllOrigin(boolean allOrigin) {
		this.allOrigin = allOrigin;
	}

	public Integer getOriginId() {
		return originId;
	}

	public void setOriginId(Integer originId) {
		this.originId = originId;
	}

}
