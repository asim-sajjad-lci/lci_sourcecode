/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the fa40013 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40013")
@NamedQuery(name="FABookClassSetup.findAll", query="SELECT f FROM FABookClassSetup f")
public class FABookClassSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BOKINXD")
	private int bookIndex;

	@ManyToOne
	@JoinColumn(name="AMORCODE")
	private FABookMaintenanceAmortizationCodeType faBookMaintenanceAmortizationCodeType;

	@Column(name="AMORINTPERC")
	private Double initialAllowancePercentage;

	@Column(name="AMORPERC")
	private Double amortizationPercentage;

	@Column(name="AMORRAMT")
	private Double amortizationAmount;

	@ManyToOne
	@JoinColumn(name="AVERGCONV")
	private FABookMaintenanceAveragingConventionType averagingConventionType;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="ORIGLDAYS")
	private int origLifeDays;

	@Column(name="ORIGLYEAR")
	private int origLifeYears;

	@Column(name="SALVEOPNT")
	private int salvageEstimate;

	@Column(name="SALVEPERNT")
	private Double salvagePercentage;

	@Column(name="SPCALLOWDE")
	private int specialDepreciationAllowance;

	@Column(name="SPCALLOWDEPERCT")
	private Double specialDepreciationAllowancePercent;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	/*@Column(name="SWITCH")
	private int swtch;*/
	
	@ManyToOne
	@JoinColumn(name="SWITCH")
	private FABookMaintenanceSwitchOverType switchOverType;

	//bi-directional many-to-one association to Fa40055
	@ManyToOne
	@JoinColumn(name="DEPCMETHD")
	private FADepreciationMethods faDepreciationMethods;

	//bi-directional many-to-one association to Fa40002
	@ManyToOne
	@JoinColumn(name="CLASSID")
	private FAClassSetup faClassSetup;
	
	//bi-directional many-to-one association to Fa40000
	@ManyToOne
	@JoinColumn(name="BOOKID")
	private BookSetup bookSetup;

	public FABookClassSetup() {
	}

	public int getBookIndex() {
		return bookIndex;
	}

	public void setBookIndex(int bookIndex) {
		this.bookIndex = bookIndex;
	}

	public Double getInitialAllowancePercentage() {
		return initialAllowancePercentage;
	}

	public void setInitialAllowancePercentage(Double initialAllowancePercentage) {
		this.initialAllowancePercentage = initialAllowancePercentage;
	}

	public Double getAmortizationPercentage() {
		return amortizationPercentage;
	}

	public void setAmortizationPercentage(Double amortizationPercentage) {
		this.amortizationPercentage = amortizationPercentage;
	}

	public Double getAmortizationAmount() {
		return amortizationAmount;
	}

	public void setAmortizationAmount(Double amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getOrigLifeDays() {
		return origLifeDays;
	}

	public void setOrigLifeDays(int origLifeDays) {
		this.origLifeDays = origLifeDays;
	}

	public int getOrigLifeYears() {
		return origLifeYears;
	}

	public void setOrigLifeYears(int origLifeYears) {
		this.origLifeYears = origLifeYears;
	}

	public int getSalvageEstimate() {
		return salvageEstimate;
	}

	public void setSalvageEstimate(int salvageEstimate) {
		this.salvageEstimate = salvageEstimate;
	}

	public Double getSalvagePercentage() {
		return salvagePercentage;
	}

	public void setSalvagePercentage(Double salvagePercentage) {
		this.salvagePercentage = salvagePercentage;
	}

	public int getSpecialDepreciationAllowance() {
		return specialDepreciationAllowance;
	}

	public void setSpecialDepreciationAllowance(int specialDepreciationAllowance) {
		this.specialDepreciationAllowance = specialDepreciationAllowance;
	}

	public Double getSpecialDepreciationAllowancePercent() {
		return specialDepreciationAllowancePercent;
	}

	public void setSpecialDepreciationAllowancePercent(Double specialDepreciationAllowancePercent) {
		this.specialDepreciationAllowancePercent = specialDepreciationAllowancePercent;
	}

	public FADepreciationMethods getFaDepreciationMethods() {
		return faDepreciationMethods;
	}

	public void setFaDepreciationMethods(FADepreciationMethods faDepreciationMethods) {
		this.faDepreciationMethods = faDepreciationMethods;
	}

	public FAClassSetup getFaClassSetup() {
		return faClassSetup;
	}

	public void setFaClassSetup(FAClassSetup faClassSetup) {
		this.faClassSetup = faClassSetup;
	}

	public FABookMaintenanceAveragingConventionType getAveragingConventionType() {
		return averagingConventionType;
	}

	public void setAveragingConventionType(FABookMaintenanceAveragingConventionType averagingConventionType) {
		this.averagingConventionType = averagingConventionType;
	}

	public FABookMaintenanceSwitchOverType getSwitchOverType() {
		return switchOverType;
	}

	public void setSwitchOverType(FABookMaintenanceSwitchOverType switchOverType) {
		this.switchOverType = switchOverType;
	}

	public BookSetup getBookSetup() {
		return bookSetup;
	}

	public void setBookSetup(BookSetup bookSetup) {
		this.bookSetup = bookSetup;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public FABookMaintenanceAmortizationCodeType getFaBookMaintenanceAmortizationCodeType() {
		return faBookMaintenanceAmortizationCodeType;
	}

	public void setFaBookMaintenanceAmortizationCodeType(
			FABookMaintenanceAmortizationCodeType faBookMaintenanceAmortizationCodeType) {
		this.faBookMaintenanceAmortizationCodeType = faBookMaintenanceAmortizationCodeType;
	}
	

}