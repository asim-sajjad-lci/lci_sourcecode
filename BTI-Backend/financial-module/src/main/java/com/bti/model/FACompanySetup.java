/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the fa40003 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40003")
@NamedQuery(name="FACompanySetup.findAll", query="SELECT f FROM FACompanySetup f")
public class FACompanySetup implements Serializable {
	private static final long serialVersionUID = 1L;

	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="FACOMSETUPID")
	private int faCompanySetupId;
	
	@Column(name="AUTOBOKADD")
	private int autoAddBookInformation;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEFASSTLABL")
	private int defaultAssetLabel;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRPTN")
	private String bookDescription;

	@Column(name="DSCRPTNA")
	private String bookDescriptionArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="POSTDETAL")
	private int postDetails;

	@Column(name="POSTPMFA")
	private int postPayableManagement;

	@Column(name="POSTPOPFA")
	private int postPurchaseOrderProcessing;

	@Column(name="REDFAACC")
	private int requireAssetAccount;

	@Column(name="USERDEFN1")
	private String userDefine1;

	@Column(name="USERDEFN2")
	private String userDefine2;

	@Column(name="USERDEFN3")
	private String userDefine3;

	@Column(name="USERDEFN4")
	private String userDefine4;

	@Column(name="USERDEFN5")
	private String userDefine5;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional one-to-one association to Fa40000
	@OneToOne
	@JoinColumn(name="BOKINXD")
	private BookSetup bookSetup;

	public FACompanySetup() {
	}

	/*public int getBookInxd() {
		return bookInxd;
	}

	public void setBookInxd(int bookInxd) {
		this.bookInxd = bookInxd;
	}*/

	public int getAutoAddBookInformation() {
		return autoAddBookInformation;
	}

	public void setAutoAddBookInformation(int autoAddBookInformation) {
		this.autoAddBookInformation = autoAddBookInformation;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getDefaultAssetLabel() {
		return defaultAssetLabel;
	}

	public void setDefaultAssetLabel(int defaultAssetLabel) {
		this.defaultAssetLabel = defaultAssetLabel;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getBookDescription() {
		return bookDescription;
	}

	public void setBookDescription(String bookDescription) {
		this.bookDescription = bookDescription;
	}

	public String getBookDescriptionArabic() {
		return bookDescriptionArabic;
	}

	public void setBookDescriptionArabic(String bookDescriptionArabic) {
		this.bookDescriptionArabic = bookDescriptionArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getPostDetails() {
		return postDetails;
	}

	public void setPostDetails(int postDetails) {
		this.postDetails = postDetails;
	}

	public int getPostPayableManagement() {
		return postPayableManagement;
	}

	public void setPostPayableManagement(int postPayableManagement) {
		this.postPayableManagement = postPayableManagement;
	}

	public int getPostPurchaseOrderProcessing() {
		return postPurchaseOrderProcessing;
	}

	public void setPostPurchaseOrderProcessing(int postPurchaseOrderProcessing) {
		this.postPurchaseOrderProcessing = postPurchaseOrderProcessing;
	}

	public int getRequireAssetAccount() {
		return requireAssetAccount;
	}

	public void setRequireAssetAccount(int requireAssetAccount) {
		this.requireAssetAccount = requireAssetAccount;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}

	public String getUserDefine4() {
		return userDefine4;
	}

	public void setUserDefine4(String userDefine4) {
		this.userDefine4 = userDefine4;
	}

	public String getUserDefine5() {
		return userDefine5;
	}

	public void setUserDefine5(String userDefine5) {
		this.userDefine5 = userDefine5;
	}

	public BookSetup getBookSetup() {
		return bookSetup;
	}

	public void setBookSetup(BookSetup bookSetup) {
		this.bookSetup = bookSetup;
	}

	public int getFaCompanySetupId() {
		return faCompanySetupId;
	}

	public void setFaCompanySetupId(int faCompanySetupId) {
		this.faCompanySetupId = faCompanySetupId;
	}

	
}