/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar50001 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar50001")
@NamedQuery(name="ARBalancebyPeriods.findAll", query="SELECT a FROM ARBalancebyPeriods a")
public class ARBalancebyPeriods implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CUSTNMBR")
	private String customerNumber;
	
	@Column(name="PRDINDX")
	private int periodIndexing;
	
	@Column(name="PRDDAYS")
	private int periodDays;

	@Column(name="TOTBLNAMT")
	private double totalBalanceAmount;

	@Column(name="TOTBLNPMT")
	private double totalBalancePayments;
	
	@Column(name="LSTAGIDDT")
	private Date lastAgingDate;
	
	@Column(name="LSTAGIUSR")
	private String lastAgingByUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;


	public String getCustomerNumber() {
		return customerNumber;
	}


	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}


	public int getPeriodIndexing() {
		return periodIndexing;
	}


	public void setPeriodIndexing(int periodIndexing) {
		this.periodIndexing = periodIndexing;
	}


	public int getPeriodDays() {
		return periodDays;
	}


	public void setPeriodDays(int periodDays) {
		this.periodDays = periodDays;
	}

	public double getTotalBalanceAmount() {
		return totalBalanceAmount;
	}


	public void setTotalBalanceAmount(double totalBalanceAmount) {
		this.totalBalanceAmount = totalBalanceAmount;
	}

	public double getTotalBalancePayments() {
		return totalBalancePayments;
	}


	public void setTotalBalancePayments(double totalBalancePayments) {
		this.totalBalancePayments = totalBalancePayments;
	}


	public Date getLastAgingDate() {
		return lastAgingDate;
	}


	public void setLastAgingDate(Date lastAgingDate) {
		this.lastAgingDate = lastAgingDate;
	}


	public String getLastAgingByUserID() {
		return lastAgingByUserID;
	}


	public void setLastAgingByUserID(String lastAgingByUserID) {
		this.lastAgingByUserID = lastAgingByUserID;
	}


	public Date getRowDateindex() {
		return rowDateindex;
	}


	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}


	public int getRowIDindex() {
		return rowIDindex;
	}


	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}


	
		
}