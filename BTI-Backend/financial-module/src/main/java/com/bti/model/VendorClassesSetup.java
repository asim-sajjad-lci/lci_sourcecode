/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ap00200 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap00200")  
@NamedQuery(name="VendorClassesSetup.findAll", query="SELECT a FROM VendorClassesSetup a")
public class VendorClassesSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="VENDCLSID")
	private String vendorClassId;

	@Column(name="CHANGEBY")
	private int changeBy;
	
	@Column(name="CLASDSCR")
	private String classDescription;

	@Column(name="CLASDSCRA")
	private String classDescriptionArabic;

	@Column(name="CRDLIMT")
	private int creditLimit;

	@Column(name="CRDLIMTAMT")
	private Double creditLimitAmount;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	/*@Column(name="CURNCYID")
	private String currencyId;*/

	//bi-directional many-to-one association to Mc40200
	@ManyToOne
	@JoinColumn(name="CURNCYID")
	private CurrencySetup currencySetup;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MAXINVAMT")
	private int maximumInvoiceAmount;
	
	@Column(name="MAXINVAMTVAL")
	private Double maximumInvoiceAmountValue;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	public Double getMaximumInvoiceAmountValue() {
		return maximumInvoiceAmountValue;
	}

	public void setMaximumInvoiceAmountValue(Double maximumInvoiceAmountValue) {
		this.maximumInvoiceAmountValue = maximumInvoiceAmountValue;
	}

	@Column(name="MINCHARG")
	private int minimumCharge;

	@Column(name="MINCHARGAMT")
	private Double minimumChargeAmount;

	@Column(name="MINORDER")
	private Double minimumOrderAmount;

	@Column(name="MODFIDT")
	private Date modifyDate;

	@Column(name="TRDDISCT")
	private Double tradeDiscountPercent;
	
	@Column(name="OPNCLNYER")
	private int openMaintenanceHistoryCalendarYear;

	@Column(name="OPNDISTR")
	private int openMaintenanceHistoryDistribution;

	@Column(name="OPNFSYER")
	private int openMaintenanceHistoryFiscalYear;

	@Column(name="OPNTRXN")
	private int openMaintenanceHistoryTransaction;

	@Column(name="USERDEFN1")
	private String userDefine1;

	@Column(name="USERDEFN2")
	private String userDefine2;

	@Column(name="USERDEFN3")
	private String userDefine3;

	//bi-directional many-to-one association to Ap00101
	/*@OneToMany(mappedBy="vendorClassesSetup")
	private List<VendorMaintenance> vendorMaintenances;*/

	//bi-directional many-to-one association to Sy03600
	@ManyToOne
	@JoinColumn(name="TAXSCHDID")
	private VATSetup vatSetup;

	//bi-directional many-to-one association to Sy03400
	@ManyToOne
	@JoinColumn(name="SHIPMTHD")
	private ShipmentMethodSetup shipmentMethodSetup;

	//bi-directional many-to-one association to Sy03300
	@ManyToOne
	@JoinColumn(name="PYMTRMID")
	private PaymentTermsSetup paymentTermsSetup;

	//bi-directional many-to-one association to Ap00201
	@ManyToOne
	@JoinColumn(name="ACCTABLID")
	private VendorClassAccountTableSetup vendorClassAccountTableSetup;

	//bi-directional many-to-one association to Gl00200
	@ManyToOne
	@JoinColumn(name="CHEKBOKID")
	private CheckbookMaintenance checkbookMaintenance;

	//bi-directional many-to-one association to Ap00201
	/*@OneToMany(mappedBy="vendorClassesSetup")
	private List<VendorClassAccountTableSetup> vendorClassAccountTableSetups;*/

	public VendorClassesSetup() {
	}

	public String getVendorClassId() {
		return vendorClassId;
	}

	public void setVendorClassId(String vendorClassId) {
		this.vendorClassId = vendorClassId;
	}

	public String getClassDescription() {
		return classDescription;
	}

	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}

	public String getClassDescriptionArabic() {
		return classDescriptionArabic;
	}

	public void setClassDescriptionArabic(String classDescriptionArabic) {
		this.classDescriptionArabic = classDescriptionArabic;
	}

	public Double getCreditLimitAmount() {
		return creditLimitAmount;
	}

	public void setCreditLimitAmount(Double creditLimitAmount) {
		this.creditLimitAmount = creditLimitAmount;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public CurrencySetup getCurrencySetup() {
		return currencySetup;
	}

	public void setCurrencySetup(CurrencySetup currencySetup) {
		this.currencySetup = currencySetup;
	}

	/*public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}*/

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public String getUserDefine3() {
		return userDefine3;
	}

	public void setUserDefine3(String userDefine3) {
		this.userDefine3 = userDefine3;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getMaximumInvoiceAmount() {
		return maximumInvoiceAmount;
	}

	public void setMaximumInvoiceAmount(int maximumInvoiceAmount) {
		this.maximumInvoiceAmount = maximumInvoiceAmount;
	}

	public int getMinimumCharge() {
		return minimumCharge;
	}

	public void setMinimumCharge(int minimumCharge) {
		this.minimumCharge = minimumCharge;
	}

	public Double getMinimumChargeAmount() {
		return minimumChargeAmount;
	}

	public void setMinimumChargeAmount(Double minimumChargeAmount) {
		this.minimumChargeAmount = minimumChargeAmount;
	}

	public Double getMinimumOrderAmount() {
		return minimumOrderAmount;
	}

	public void setMinimumOrderAmount(Double minimumOrderAmount) {
		this.minimumOrderAmount = minimumOrderAmount;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Double getTradeDiscountPercent() {
		return tradeDiscountPercent;
	}

	public void setTradeDiscountPercent(Double tradeDiscountPercent) {
		this.tradeDiscountPercent = tradeDiscountPercent;
	}

	public int getOpenMaintenanceHistoryCalendarYear() {
		return openMaintenanceHistoryCalendarYear;
	}

	public void setOpenMaintenanceHistoryCalendarYear(int openMaintenanceHistoryCalendarYear) {
		this.openMaintenanceHistoryCalendarYear = openMaintenanceHistoryCalendarYear;
	}

	public int getOpenMaintenanceHistoryDistribution() {
		return openMaintenanceHistoryDistribution;
	}

	public void setOpenMaintenanceHistoryDistribution(int openMaintenanceHistoryDistribution) {
		this.openMaintenanceHistoryDistribution = openMaintenanceHistoryDistribution;
	}

	public int getOpenMaintenanceHistoryFiscalYear() {
		return openMaintenanceHistoryFiscalYear;
	}

	public void setOpenMaintenanceHistoryFiscalYear(int openMaintenanceHistoryFiscalYear) {
		this.openMaintenanceHistoryFiscalYear = openMaintenanceHistoryFiscalYear;
	}

	public int getOpenMaintenanceHistoryTransaction() {
		return openMaintenanceHistoryTransaction;
	}

	public void setOpenMaintenanceHistoryTransaction(int openMaintenanceHistoryTransaction) {
		this.openMaintenanceHistoryTransaction = openMaintenanceHistoryTransaction;
	}
/*
	public List<VendorClassAccountTableSetup> getVendorClassAccountTableSetups() {
		return vendorClassAccountTableSetups;
	}

	public void setVendorClassAccountTableSetups(List<VendorClassAccountTableSetup> vendorClassAccountTableSetups) {
		this.vendorClassAccountTableSetups = vendorClassAccountTableSetups;
	}

	public List<VendorMaintenance> getVendorMaintenances() {
		return vendorMaintenances;
	}

	public void setVendorMaintenances(List<VendorMaintenance> vendorMaintenances) {
		this.vendorMaintenances = vendorMaintenances;
	}*/

	public VATSetup getVatSetup() {
		return vatSetup;
	}

	public void setVatSetup(VATSetup vatSetup) {
		this.vatSetup = vatSetup;
	}

	public ShipmentMethodSetup getShipmentMethodSetup() {
		return shipmentMethodSetup;
	}

	public void setShipmentMethodSetup(ShipmentMethodSetup shipmentMethodSetup) {
		this.shipmentMethodSetup = shipmentMethodSetup;
	}

	public PaymentTermsSetup getPaymentTermsSetup() {
		return paymentTermsSetup;
	}

	public void setPaymentTermsSetup(PaymentTermsSetup paymentTermsSetup) {
		this.paymentTermsSetup = paymentTermsSetup;
	}

	public VendorClassAccountTableSetup getVendorClassAccountTableSetup() {
		return vendorClassAccountTableSetup;
	}

	public void setVendorClassAccountTableSetup(VendorClassAccountTableSetup vendorClassAccountTableSetup) {
		this.vendorClassAccountTableSetup = vendorClassAccountTableSetup;
	}

	public CheckbookMaintenance getCheckbookMaintenance() {
		return checkbookMaintenance;
	}

	public void setCheckbookMaintenance(CheckbookMaintenance checkbookMaintenance) {
		this.checkbookMaintenance = checkbookMaintenance;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

	
}