/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the sy03400 database table.
 * 
 */
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "tpclblnc_type")
@NamedQuery(name = "TPCLBalanceType.findAll", query = "SELECT s FROM TPCLBalanceType s")
public class TPCLBalanceType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "TYPE_PRIMARY")
	private String typePrimary;

	/*@Column(name = "TYPE_SECONDARY")
	private String typeSecondary;*/

	@Column(name = "TYPE_ID")
	private int typeId;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	@OneToMany(mappedBy = "tpclBalanceType")
	private List<COAMainAccounts> coaMainAccounts;
	
	@ManyToOne
	@JoinColumn(name="lang_Id")
	private Language language;

	public TPCLBalanceType() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypePrimary() {
		return typePrimary;
	}

	public void setTypePrimary(String typePrimary) {
		this.typePrimary = typePrimary;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public List<COAMainAccounts> getCoaMainAccounts() {
		return coaMainAccounts;
	}

	public void setCoaMainAccounts(List<COAMainAccounts> coaMainAccounts) {
		this.coaMainAccounts = coaMainAccounts;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

}