package com.bti.model;
import java.io.Serializable;

public class APYTDOpenTransactionKey implements Serializable {

	private static final long serialVersionUID = 1L;
	private String apTransactionNumber;
	private String vendorID;
	
	public String getApTransactionNumber() {
		return apTransactionNumber;
	}
	public void setApTransactionNumber(String apTransactionNumber) {
		this.apTransactionNumber = apTransactionNumber;
	}
	public String getVendorID() {
		return vendorID;
	}
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
}
