/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.FAAccountGroupsSetup;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoFaAccountGroupSetup {
	
	private Integer id;
	private String accountGroupId;
	private Integer accountType;
	private String description;
	private String descriptionArabic;
	private String messageType;
	private String accountDescription;
	private String accountNumber;
	private Integer accountTypeId;
	private String accountTypeValue;
	private Integer accountGroupIndex;
	List<DtoFaAccountGroupSetup> accountNumberList;
	
	
	public DtoFaAccountGroupSetup() {
		// TO DO nothing
	}
	
	public DtoFaAccountGroupSetup(FAAccountGroupsSetup faAccountGroupsSetup) {
		this.accountGroupIndex=faAccountGroupsSetup.getFaAccountGroupIndex();
		this.accountGroupId= faAccountGroupsSetup.getFaAccountGroupId();
		this.description="";
		if(UtilRandomKey.isNotBlank(faAccountGroupsSetup.getFaAccountGroupDescription())){
			this.description=faAccountGroupsSetup.getFaAccountGroupDescription();
		}
		
		this.descriptionArabic="";
		if(UtilRandomKey.isNotBlank(faAccountGroupsSetup.getFaAccountGroupDescriptionArabic())){
			this.descriptionArabic=faAccountGroupsSetup.getFaAccountGroupDescriptionArabic();
		}
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAccountGroupId() {
		return accountGroupId;
	}
	public void setAccountGroupId(String accountGroupId) {
		this.accountGroupId = accountGroupId;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescriptionArabic() {
		return descriptionArabic;
	}
	public void setDescriptionArabic(String descriptionArabic) {
		this.descriptionArabic = descriptionArabic;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getAccountDescription() {
		return accountDescription;
	}
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Integer getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(Integer accountTypeId) {
		this.accountTypeId = accountTypeId;
	}
	public String getAccountTypeValue() {
		return accountTypeValue;
	}
	public void setAccountTypeValue(String accountTypeValue) {
		this.accountTypeValue = accountTypeValue;
	}
	public List<DtoFaAccountGroupSetup> getAccountNumberList() {
		return accountNumberList;
	}
	public void setAccountNumberList(List<DtoFaAccountGroupSetup> accountNumberList) {
		this.accountNumberList = accountNumberList;
	}

	public Integer getAccountGroupIndex() {
		return accountGroupIndex;
	}

	public void setAccountGroupIndex(Integer accountGroupIndex) {
		this.accountGroupIndex = accountGroupIndex;
	}
	
	
}
