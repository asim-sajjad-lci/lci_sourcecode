/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.bti.model.GLConfigurationDefinePeriodsHeader;
import com.bti.util.UtilDateAndTime;

public class DtoFiscalFinancialPeriodSetup {

	private Integer year;
	private String firstDay;
	private String lastDay;
	private boolean historicalYear;
	private Integer numberOfPeriods;
	private List<DtoFiscalFinancialPeriodSetupDetail> dtoFiscalFinancialPeriodSetupDetail = new ArrayList<>();
	public DtoFiscalFinancialPeriodSetup(){
		
	}
	
	public DtoFiscalFinancialPeriodSetup(GLConfigurationDefinePeriodsHeader glConfigurationDefinePeriodsHeader){
		this.year = glConfigurationDefinePeriodsHeader.getYear();
		
		if(glConfigurationDefinePeriodsHeader.getFirstFiscalDate()!=null){
			this.firstDay = UtilDateAndTime.dateToStringddmmyyyy(glConfigurationDefinePeriodsHeader.getFirstFiscalDate());
		}
		
		if(glConfigurationDefinePeriodsHeader.getLastFiscalDate()!=null){
			this.lastDay = UtilDateAndTime.dateToStringddmmyyyy(glConfigurationDefinePeriodsHeader.getLastFiscalDate());
		}
		
		this.historicalYear = glConfigurationDefinePeriodsHeader.getHistoricalYear();
		this.numberOfPeriods = glConfigurationDefinePeriodsHeader.getNumberOfPeriods();
	}
	
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getFirstDay() {
		return firstDay;
	}
	public void setFirstDay(String firstDay) {
		this.firstDay = firstDay;
	}
	public String getLastDay() {
		return lastDay;
	}
	public void setLastDay(String lastDay) {
		this.lastDay = lastDay;
	}
	
	public boolean isHistoricalYear() {
		return historicalYear;
	}

	public void setHistoricalYear(boolean historicalYear) {
		this.historicalYear = historicalYear;
	}

	public Integer getNumberOfPeriods() {
		return numberOfPeriods;
	}
	public void setNumberOfPeriods(Integer numberOfPeriods) {
		this.numberOfPeriods = numberOfPeriods;
	}

	public List<DtoFiscalFinancialPeriodSetupDetail> getDtoFiscalFinancialPeriodSetupDetail() {
		return dtoFiscalFinancialPeriodSetupDetail;
	}

	public void setDtoFiscalFinancialPeriodSetupDetail(
			List<DtoFiscalFinancialPeriodSetupDetail> dtoFiscalFinancialPeriodSetupDetail) {
		this.dtoFiscalFinancialPeriodSetupDetail = dtoFiscalFinancialPeriodSetupDetail;
	}

}
