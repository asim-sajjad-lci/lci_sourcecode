/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the fa40004 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "fa40004")
@NamedQuery(name="FALeaseCompanySetup.findAll", query="SELECT f FROM FALeaseCompanySetup f")
public class FALeaseCompanySetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COMPINDX")
	private int leaseCompanyIndex;

	@Column(name="COMPID")
	private String companyId;
	
	@Column(name="COMPNM")
	private String companyName;
	
	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;

	/*@Column(name="VENDORID")
	private String vendorId;*/

	@Column(name="VENDORNAM")
	private String vendorName;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	//bi-directional many-to-one association to Fa00105
	/*@OneToMany(mappedBy="faLeaseCompanySetup")
	private List<FALeaseMaintenance> faLeaseMaintenances;*/
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	//bi-directional many-to-one association to Ap00101
	@ManyToOne
	@JoinColumn(name="VENDORID")
	private VendorMaintenance vendorMaintenance;

	public FALeaseCompanySetup() {
	}

	public int getLeaseCompanyIndex() {
		return leaseCompanyIndex;
	}

	public void setLeaseCompanyIndex(int leaseCompanyIndex) {
		this.leaseCompanyIndex = leaseCompanyIndex;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}
	
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	/*public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}*/

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	/*public List<FALeaseMaintenance> getFaLeaseMaintenances() {
		return faLeaseMaintenances;
	}

	public void setFaLeaseMaintenances(List<FALeaseMaintenance> faLeaseMaintenances) {
		this.faLeaseMaintenances = faLeaseMaintenances;
	}*/

	public VendorMaintenance getVendorMaintenance() {
		return vendorMaintenance;
	}

	public void setVendorMaintenance(VendorMaintenance vendorMaintenance) {
		this.vendorMaintenance = vendorMaintenance;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	

}