/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar90300 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar90300")
@NamedQuery(name="ARYTDHistoryTransactions.findAll", query="SELECT a FROM ARYTDHistoryTransactions a")
public class ARYTDHistoryTransactions implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ARTRXNO")
	private String aRTransactionNumber;
	
	@Column(name="CUSTNMBR")
	private String customerID;
	
	@Column(name="ARTRXTYP")
	private int aRTransactionType;

	@Column(name="DSCRPTN")
	private String aRTransactionDescription;

	@Column(name="BATCHID")
	private String batchID;
	
	@Column(name="ARTRXDT")
	private Date aRTransactionDate;
	//
	@Column(name="CUSTNAME")
	private String customerName;
	
	@Column(name="CUSTNAMEA")
	private String customerNameArabic;
	//
	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="PYMTRMID")
	private String paymentTermsID;
	
	@Column(name="ARCSTAMT")
	private Double aRTransactionCost;
	
	@Column(name="ARSLSAMT")
	private Double aRTransactionSalesAmount;
	
	@Column(name="ARTRDAMT")
	private Double aRTransactionTradeDiscount;
	
	@Column(name="ARFRGAMT")
	private Double aRTransactionFreightAmount;
	
	@Column(name="ARMISCAMT")
	private Double aRTransactionMiscellaneous;
	
	@Column(name="ARVATAMT")
	private Double aRTransactionVATAmount;
	
	@Column(name="ARDBAMT")
	private Double aRTransactionDebitMemoAmount;
	
	@Column(name="ARFRCAMT")
	private Double aRTransactionFinanceChargeAmount;
	
	@Column(name="ARWARAMT")
	private Double aRTransactionWarrantyAmount;
	
	@Column(name="ARCRAMT")
	private Double aRTransactionCreditMemoAmount;
	
	@Column(name="ARTOTAMT")
	private Double aRTransactionTotalAmount;
	
	@Column(name="ARCSHAMT")
	private Double aRTransactionCashAmount;
	
	@Column(name="ARCHKAMT")
	private Double aRTransactionCheckAmount;
	
	@Column(name="ARCRDAMT")
	private Double aRTransactionCreditCardAmount;
	
	@Column(name="SALSPERID")
	private String salesmanID;
	
	@Column(name="CHEKBOKID")
	private String checkbookID;
	
	@Column(name="CSHRECNO")
	private String cashReceiptNumber;
	
	@Column(name="CHKNUMBR")
	private String checkNumber;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	
	@Column(name="CARDEXPTY")
	private int creditCardExpireYear;
	
	@Column(name="CARDEXPTM")
	private int creditCardExpireMonth;
	
	@Column(name="TRXSTAT")
	private int aRTransactionStatus;
	
	@Column(name="EXGTBLINXD")
	private int exchangeTableIndex;
	
	@Column(name="XCHGRATE")
	private Double exchangeTableRate;
	
	@Column(name="TRXVOID")
	private int transactionVoid;
	
	@Column(name="SHIPMTHD")
	private String shippingMethodID;
	
	@Column(name="TAXSCHDID")
	private String vATScheduleID;
	
	@Column(name="PSTTRXDDT")
	private Date postingDate;
	
	@Column(name="PSTTRXURS")
	private String postingbyUserID;
	
	@Column(name="YEAR1")
	private int year;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateindex;
	
	@Column(name="DEX_ROW_ID")
	private int rowIDindex;

	public String getaRTransactionNumber() {
		return aRTransactionNumber;
	}

	public void setaRTransactionNumber(String aRTransactionNumber) {
		this.aRTransactionNumber = aRTransactionNumber;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public int getaRTransactionType() {
		return aRTransactionType;
	}

	public void setaRTransactionType(int aRTransactionType) {
		this.aRTransactionType = aRTransactionType;
	}

	public String getaRTransactionDescription() {
		return aRTransactionDescription;
	}

	public void setaRTransactionDescription(String aRTransactionDescription) {
		this.aRTransactionDescription = aRTransactionDescription;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public Date getaRTransactionDate() {
		return aRTransactionDate;
	}

	public void setaRTransactionDate(Date aRTransactionDate) {
		this.aRTransactionDate = aRTransactionDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNameArabic() {
		return customerNameArabic;
	}

	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getPaymentTermsID() {
		return paymentTermsID;
	}

	public void setPaymentTermsID(String paymentTermsID) {
		this.paymentTermsID = paymentTermsID;
	}

	public Double getaRTransactionCost() {
		return aRTransactionCost;
	}

	public void setaRTransactionCost(Double aRTransactionCost) {
		this.aRTransactionCost = aRTransactionCost;
	}

	public Double getaRTransactionSalesAmount() {
		return aRTransactionSalesAmount;
	}

	public void setaRTransactionSalesAmount(Double aRTransactionSalesAmount) {
		this.aRTransactionSalesAmount = aRTransactionSalesAmount;
	}

	public Double getaRTransactionTradeDiscount() {
		return aRTransactionTradeDiscount;
	}

	public void setaRTransactionTradeDiscount(Double aRTransactionTradeDiscount) {
		this.aRTransactionTradeDiscount = aRTransactionTradeDiscount;
	}

	public Double getaRTransactionFreightAmount() {
		return aRTransactionFreightAmount;
	}

	public void setaRTransactionFreightAmount(Double aRTransactionFreightAmount) {
		this.aRTransactionFreightAmount = aRTransactionFreightAmount;
	}

	public Double getaRTransactionMiscellaneous() {
		return aRTransactionMiscellaneous;
	}

	public void setaRTransactionMiscellaneous(Double aRTransactionMiscellaneous) {
		this.aRTransactionMiscellaneous = aRTransactionMiscellaneous;
	}

	public Double getaRTransactionVATAmount() {
		return aRTransactionVATAmount;
	}

	public void setaRTransactionVATAmount(Double aRTransactionVATAmount) {
		this.aRTransactionVATAmount = aRTransactionVATAmount;
	}

	public Double getaRTransactionDebitMemoAmount() {
		return aRTransactionDebitMemoAmount;
	}

	public void setaRTransactionDebitMemoAmount(Double aRTransactionDebitMemoAmount) {
		this.aRTransactionDebitMemoAmount = aRTransactionDebitMemoAmount;
	}

	public Double getaRTransactionFinanceChargeAmount() {
		return aRTransactionFinanceChargeAmount;
	}

	public void setaRTransactionFinanceChargeAmount(Double aRTransactionFinanceChargeAmount) {
		this.aRTransactionFinanceChargeAmount = aRTransactionFinanceChargeAmount;
	}

	public Double getaRTransactionWarrantyAmount() {
		return aRTransactionWarrantyAmount;
	}

	public void setaRTransactionWarrantyAmount(Double aRTransactionWarrantyAmount) {
		this.aRTransactionWarrantyAmount = aRTransactionWarrantyAmount;
	}

	public Double getaRTransactionCreditMemoAmount() {
		return aRTransactionCreditMemoAmount;
	}

	public void setaRTransactionCreditMemoAmount(Double aRTransactionCreditMemoAmount) {
		this.aRTransactionCreditMemoAmount = aRTransactionCreditMemoAmount;
	}

	public Double getaRTransactionTotalAmount() {
		return aRTransactionTotalAmount;
	}

	public void setaRTransactionTotalAmount(Double aRTransactionTotalAmount) {
		this.aRTransactionTotalAmount = aRTransactionTotalAmount;
	}

	public Double getaRTransactionCashAmount() {
		return aRTransactionCashAmount;
	}

	public void setaRTransactionCashAmount(Double aRTransactionCashAmount) {
		this.aRTransactionCashAmount = aRTransactionCashAmount;
	}

	public Double getaRTransactionCheckAmount() {
		return aRTransactionCheckAmount;
	}

	public void setaRTransactionCheckAmount(Double aRTransactionCheckAmount) {
		this.aRTransactionCheckAmount = aRTransactionCheckAmount;
	}

	public Double getaRTransactionCreditCardAmount() {
		return aRTransactionCreditCardAmount;
	}

	public void setaRTransactionCreditCardAmount(Double aRTransactionCreditCardAmount) {
		this.aRTransactionCreditCardAmount = aRTransactionCreditCardAmount;
	}

	public String getSalesmanID() {
		return salesmanID;
	}

	public void setSalesmanID(String salesmanID) {
		this.salesmanID = salesmanID;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCashReceiptNumber() {
		return cashReceiptNumber;
	}

	public void setCashReceiptNumber(String cashReceiptNumber) {
		this.cashReceiptNumber = cashReceiptNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public int getaRTransactionStatus() {
		return aRTransactionStatus;
	}

	public void setaRTransactionStatus(int aRTransactionStatus) {
		this.aRTransactionStatus = aRTransactionStatus;
	}

	public int getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(int exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public int getTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(int transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getShippingMethodID() {
		return shippingMethodID;
	}

	public void setShippingMethodID(String shippingMethodID) {
		this.shippingMethodID = shippingMethodID;
	}

	public String getvATScheduleID() {
		return vATScheduleID;
	}

	public void setvATScheduleID(String vATScheduleID) {
		this.vATScheduleID = vATScheduleID;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public String getPostingbyUserID() {
		return postingbyUserID;
	}

	public void setPostingbyUserID(String postingbyUserID) {
		this.postingbyUserID = postingbyUserID;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Date getRowDateindex() {
		return rowDateindex;
	}

	public void setRowDateindex(Date rowDateindex) {
		this.rowDateindex = rowDateindex;
	}

	public int getRowIDindex() {
		return rowIDindex;
	}

	public void setRowIDindex(int rowIDindex) {
		this.rowIDindex = rowIDindex;
	}
	
	
	
	}