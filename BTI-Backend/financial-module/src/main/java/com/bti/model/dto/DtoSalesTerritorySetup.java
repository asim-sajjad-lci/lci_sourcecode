/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.SalesTerritoryMaintenance;
import com.bti.util.UtilRoundDecimal;

/**
 * Description: Dto Sales Territory Setup
 * Name of Project: BTI
 * Created on: Aug 24, 2017
 * Modified on: Aug 24, 2017 10:05:38 AM
 * @author seasia
 * Version: 
 */
public class DtoSalesTerritorySetup {

	private String salesTerritoryId;
	private String descriptionPrimary;
	private String descriptionSecondary;
	private String phone1;
	private String phone2;
	private String managerFirstNamePrimary;
	private String managerMidNamePrimary;
	private String managerLastNamePrimary;
	private String managerFirstNameSecondary;
	private String managerMidNameSecondary;
	private String managerLastNameSecondary;
	private double totalCommissionsYTD;
	private double totalCommissionsLY;
	private double commissionsSalesYTD;
	private double commissionsSalesLY;
	private double costOfSalesYTD;
	private double costOfSalesLY;
	
	public DtoSalesTerritorySetup(){
		
	}
	
	public DtoSalesTerritorySetup(SalesTerritoryMaintenance salesTerritoryMaintenance){
		this.salesTerritoryId = salesTerritoryMaintenance.getSalesTerritoryId();
		this.descriptionPrimary = salesTerritoryMaintenance.getTerritoryDescription();
		this.descriptionSecondary = salesTerritoryMaintenance.getTerritoryDescriptionArabic();
		this.managerFirstNamePrimary = salesTerritoryMaintenance.getManagerFirstName();
		this.managerMidNamePrimary = salesTerritoryMaintenance.getManagerMidName();
		this.managerLastNamePrimary = salesTerritoryMaintenance.getManagerLastName();
		this.managerFirstNameSecondary = salesTerritoryMaintenance.getManagerFirstNameArabic();
		this.managerMidNameSecondary = salesTerritoryMaintenance.getManagerMidNameArabic();
		this.managerLastNameSecondary = salesTerritoryMaintenance.getManagerLastNameArabic();
		this.totalCommissionsYTD = UtilRoundDecimal.roundDecimalValue(salesTerritoryMaintenance.getTotalCommissionsYTD());
		this.totalCommissionsLY = UtilRoundDecimal.roundDecimalValue(salesTerritoryMaintenance.getTotalCommissionsLastYear());
		this.commissionsSalesYTD = UtilRoundDecimal.roundDecimalValue(salesTerritoryMaintenance.getSalesCommissionsYTD());
		this.commissionsSalesLY = UtilRoundDecimal.roundDecimalValue(salesTerritoryMaintenance.getSalesCommissionsLastYear());
		this.costOfSalesYTD = UtilRoundDecimal.roundDecimalValue(salesTerritoryMaintenance.getTotalCostSalesYTD());
		this.costOfSalesLY = UtilRoundDecimal.roundDecimalValue(salesTerritoryMaintenance.getTotalCostSalesLastYear());
		this.setPhone1(salesTerritoryMaintenance.getPhone1());
		this.setPhone2(salesTerritoryMaintenance.getPhone2());
	}
	
	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}
	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}
	public String getDescriptionPrimary() {
		return descriptionPrimary;
	}
	public void setDescriptionPrimary(String descriptionPrimary) {
		this.descriptionPrimary = descriptionPrimary;
	}
	public String getDescriptionSecondary() {
		return descriptionSecondary;
	}
	public void setDescriptionSecondary(String descriptionSecondary) {
		this.descriptionSecondary = descriptionSecondary;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getManagerFirstNamePrimary() {
		return managerFirstNamePrimary;
	}
	public void setManagerFirstNamePrimary(String managerFirstNamePrimary) {
		this.managerFirstNamePrimary = managerFirstNamePrimary;
	}
	public String getManagerMidNamePrimary() {
		return managerMidNamePrimary;
	}
	public void setManagerMidNamePrimary(String managerMidNamePrimary) {
		this.managerMidNamePrimary = managerMidNamePrimary;
	}
	public String getManagerLastNamePrimary() {
		return managerLastNamePrimary;
	}
	public void setManagerLastNamePrimary(String managerLastNamePrimary) {
		this.managerLastNamePrimary = managerLastNamePrimary;
	}
	public String getManagerFirstNameSecondary() {
		return managerFirstNameSecondary;
	}
	public void setManagerFirstNameSecondary(String managerFirstNameSecondary) {
		this.managerFirstNameSecondary = managerFirstNameSecondary;
	}
	public String getManagerMidNameSecondary() {
		return managerMidNameSecondary;
	}
	public void setManagerMidNameSecondary(String managerMidNameSecondary) {
		this.managerMidNameSecondary = managerMidNameSecondary;
	}
	public String getManagerLastNameSecondary() {
		return managerLastNameSecondary;
	}
	public void setManagerLastNameSecondary(String managerLastNameSecondary) {
		this.managerLastNameSecondary = managerLastNameSecondary;
	}
	public double getTotalCommissionsYTD() {
		return totalCommissionsYTD;
	}
	public void setTotalCommissionsYTD(double totalCommissionsYTD) {
		this.totalCommissionsYTD = UtilRoundDecimal.roundDecimalValue(totalCommissionsYTD);
	}
	public double getTotalCommissionsLY() {
		return totalCommissionsLY;
	}
	public void setTotalCommissionsLY(double totalCommissionsLY) {
		this.totalCommissionsLY = UtilRoundDecimal.roundDecimalValue(totalCommissionsLY);
	}
	public double getCommissionsSalesYTD() {
		return commissionsSalesYTD;
	}
	public void setCommissionsSalesYTD(double commissionsSalesYTD) {
		this.commissionsSalesYTD = UtilRoundDecimal.roundDecimalValue(commissionsSalesYTD);
	}
	public double getCommissionsSalesLY() {
		return commissionsSalesLY;
	}
	public void setCommissionsSalesLY(double commissionsSalesLY) {
		this.commissionsSalesLY = UtilRoundDecimal.roundDecimalValue(commissionsSalesLY);
	}
	public double getCostOfSalesYTD() {
		return costOfSalesYTD;
	}
	public void setCostOfSalesYTD(double costOfSalesYTD) {
		this.costOfSalesYTD = UtilRoundDecimal.roundDecimalValue(costOfSalesYTD);
	}
	public double getCostOfSalesLY() {
		return costOfSalesLY;
	}
	public void setCostOfSalesLY(double costOfSalesLY) {
		this.costOfSalesLY = UtilRoundDecimal.roundDecimalValue(costOfSalesLY);
	}
}
