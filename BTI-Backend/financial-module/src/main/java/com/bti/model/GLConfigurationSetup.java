/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the gl40000 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40000")
@NamedQuery(name="GLConfigurationSetup.findAll", query="SELECT g FROM GLConfigurationSetup g")
public class GLConfigurationSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="ACCTNS")
	private Boolean maintainAccountsInHistory;

	/*@Column(name="BLNCDSPL")
	private int displayBalancePeriod;*/

	@Column(name="BUGTRX")
	private Boolean maintainBudgetTransactionsHistory;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DLTSAVTRX")
	private Boolean allowDeletionOfSavedTransactions;

	@Column(name="JRENTRY")
	private int nextJournalEntryVoucher;
	
	@Column(name="SEQTYP")
	private int sequenceType;
	
	@Column(name="INCDATE")
	private Boolean includeDate;
	
	@Column(name="LSTCLOSEDT")
	private int lastCloseYear;
	
	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="NEXTRECNUM")
	private int nextReconciliationNumber;

	@Column(name="NXTBUGTID")
	private int nextBudgetJournalId;

	@Column(name="PSTHISTRY")
	private Boolean allowPostingToHistory;

	
	@ManyToOne
	@JoinColumn(name="RERINDX1")
	private COAMainAccounts retainedEarningsAccountIndex1;

	@ManyToOne
	@JoinColumn(name="RERINDX2")
	private COAMainAccounts retainedEarningsAccountIndex2;

	@Column(name="TRAXACT")
	private Boolean maintainTransactionsInHistory;

	@Column(name="TRERCLSG")
	private Boolean trueRetainedEaringClosing;

	@Column(name="USRDFPR1")
	private String labelUserDefine1;

	@Column(name="USRDFPR2")
	private String labelUserDefine52;

	@Column(name="USRDFPR3")
	private String labelUserDefine3;

	@Column(name="USRDFPR4")
	private String labelUserDefine4;

	@Column(name="USRDFPR5")
	private String labelUserDefine5;

	@Column(name="VODTRX")
	private Boolean allowVoidingCorrectingOfTransactions;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;
	
	//bi-directional many-to-one association to MasterBlncdspl
	@ManyToOne
	@JoinColumn(name="BLNCDSPL")
	private MasterBlncdspl masterBlncdspl;

	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;
	
	@OneToOne
	@JoinColumn(name="CASHAUDTPAY")
	private GLConfigurationAuditTrialCodes cashAuditTrialPayment;
	
	@OneToOne
	@JoinColumn(name="CASHAUDTRCET")
	private GLConfigurationAuditTrialCodes cashAuditTrialReceipt;
	
	@OneToOne
	@JoinColumn(name="CHKAUDTPAY")
	private GLConfigurationAuditTrialCodes checkAuditTrialPayment;
	
	@OneToOne
	@JoinColumn(name="CHKAUDTRCET")
	private GLConfigurationAuditTrialCodes checkAuditTrialReceipt;
	
	@OneToOne
	@JoinColumn(name="TRANSHAUDTPAY")
	private GLConfigurationAuditTrialCodes transferAuditTrialPayment;
	
	@OneToOne
	@JoinColumn(name="TRANSAUDTRCET")
	private GLConfigurationAuditTrialCodes transferAuditTrialReceipt;
	
	@OneToOne
	@JoinColumn(name="CRDTAUDTPAY")
	private GLConfigurationAuditTrialCodes creditAuditTrialPayment;
	
	@OneToOne
	@JoinColumn(name="CRDTAUDTRCET")
	private GLConfigurationAuditTrialCodes creditAuditTrialReceipt;

	public GLConfigurationSetup() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getMaintainAccountsInHistory() {
		return maintainAccountsInHistory;
	}

	public void setMaintainAccountsInHistory(Boolean maintainAccountsInHistory) {
		this.maintainAccountsInHistory = maintainAccountsInHistory;
	}


	/*public int getDisplayBalancePeriod() {
		return displayBalancePeriod;
	}

	public void setDisplayBalancePeriod(int displayBalancePeriod) {
		this.displayBalancePeriod = displayBalancePeriod;
	}*/

	public Boolean getMaintainBudgetTransactionsHistory() {
		return maintainBudgetTransactionsHistory;
	}

	public void setMaintainBudgetTransactionsHistory(Boolean maintainBudgetTransactionsHistory) {
		this.maintainBudgetTransactionsHistory = maintainBudgetTransactionsHistory;
	}


	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Boolean getAllowDeletionOfSavedTransactions() {
		return allowDeletionOfSavedTransactions;
	}

	public void setAllowDeletionOfSavedTransactions(Boolean allowDeletionOfSavedTransactions) {
		this.allowDeletionOfSavedTransactions = allowDeletionOfSavedTransactions;
	}

	public int getNextJournalEntryVoucher() {
		return nextJournalEntryVoucher;
	}

	public void setNextJournalEntryVoucher(int nextJournalEntryVoucher) {
		this.nextJournalEntryVoucher = nextJournalEntryVoucher;
	}

	public int getLastCloseYear() {
		return lastCloseYear;
	}

	public void setLastCloseYear(int lastCloseYear) {
		this.lastCloseYear = lastCloseYear;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getNextReconciliationNumber() {
		return nextReconciliationNumber;
	}

	public void setNextReconciliationNumber(int nextReconciliationNumber) {
		this.nextReconciliationNumber = nextReconciliationNumber;
	}

	public int getNextBudgetJournalId() {
		return nextBudgetJournalId;
	}

	public void setNextBudgetJournalId(int nextBudgetJournalId) {
		this.nextBudgetJournalId = nextBudgetJournalId;
	}

	public Boolean getAllowPostingToHistory() {
		return allowPostingToHistory;
	}

	public void setAllowPostingToHistory(Boolean allowPostingToHistory) {
		this.allowPostingToHistory = allowPostingToHistory;
	}

	public Boolean getMaintainTransactionsInHistory() {
		return maintainTransactionsInHistory;
	}

	public void setMaintainTransactionsInHistory(Boolean maintainTransactionsInHistory) {
		this.maintainTransactionsInHistory = maintainTransactionsInHistory;
	}

	public Boolean getTrueRetainedEaringClosing() {
		return trueRetainedEaringClosing;
	}

	public void setTrueRetainedEaringClosing(Boolean trueRetainedEaringClosing) {
		this.trueRetainedEaringClosing = trueRetainedEaringClosing;
	}

	public String getLabelUserDefine1() {
		return labelUserDefine1;
	}

	public void setLabelUserDefine1(String labelUserDefine1) {
		this.labelUserDefine1 = labelUserDefine1;
	}

	public String getLabelUserDefine52() {
		return labelUserDefine52;
	}

	public void setLabelUserDefine52(String labelUserDefine52) {
		this.labelUserDefine52 = labelUserDefine52;
	}

	public String getLabelUserDefine3() {
		return labelUserDefine3;
	}

	public void setLabelUserDefine3(String labelUserDefine3) {
		this.labelUserDefine3 = labelUserDefine3;
	}

	public String getLabelUserDefine4() {
		return labelUserDefine4;
	}

	public void setLabelUserDefine4(String labelUserDefine4) {
		this.labelUserDefine4 = labelUserDefine4;
	}

	public String getLabelUserDefine5() {
		return labelUserDefine5;
	}

	public void setLabelUserDefine5(String labelUserDefine5) {
		this.labelUserDefine5 = labelUserDefine5;
	}

	public Boolean getAllowVoidingCorrectingOfTransactions() {
		return allowVoidingCorrectingOfTransactions;
	}

	public void setAllowVoidingCorrectingOfTransactions(Boolean allowVoidingCorrectingOfTransactions) {
		this.allowVoidingCorrectingOfTransactions = allowVoidingCorrectingOfTransactions;
	}
	
	public MasterBlncdspl getMasterBlncdspl() {
		return this.masterBlncdspl;
	}

	public void setMasterBlncdspl(MasterBlncdspl masterBlncdspl) {
		this.masterBlncdspl = masterBlncdspl;
	}

	public COAMainAccounts getRetainedEarningsAccountIndex1() {
		return retainedEarningsAccountIndex1;
	}

	public void setRetainedEarningsAccountIndex1(COAMainAccounts retainedEarningsAccountIndex1) {
		this.retainedEarningsAccountIndex1 = retainedEarningsAccountIndex1;
	}

	public COAMainAccounts getRetainedEarningsAccountIndex2() {
		return retainedEarningsAccountIndex2;
	}

	public void setRetainedEarningsAccountIndex2(COAMainAccounts retainedEarningsAccountIndex2) {
		this.retainedEarningsAccountIndex2 = retainedEarningsAccountIndex2;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}
	public int getSequenceType() {
		return sequenceType;
	}

	public void setSequenceType(int sequenceType) {
		this.sequenceType = sequenceType;
	}

	public Boolean getIncludeDate() {
		return includeDate;
	}

	public void setIncludeDate(Boolean includeDate) {
		this.includeDate = includeDate;
	}

	public GLConfigurationAuditTrialCodes getCashAuditTrialPayment() {
		return cashAuditTrialPayment;
	}

	public void setCashAuditTrialPayment(GLConfigurationAuditTrialCodes cashAuditTrialPayment) {
		this.cashAuditTrialPayment = cashAuditTrialPayment;
	}

	public GLConfigurationAuditTrialCodes getCashAuditTrialReceipt() {
		return cashAuditTrialReceipt;
	}

	public void setCashAuditTrialReceipt(GLConfigurationAuditTrialCodes cashAuditTrialReceipt) {
		this.cashAuditTrialReceipt = cashAuditTrialReceipt;
	}

	public GLConfigurationAuditTrialCodes getCheckAuditTrialPayment() {
		return checkAuditTrialPayment;
	}

	public void setCheckAuditTrialPayment(GLConfigurationAuditTrialCodes checkAuditTrialPayment) {
		this.checkAuditTrialPayment = checkAuditTrialPayment;
	}

	public GLConfigurationAuditTrialCodes getcheckAuditTrialReceipt() {
		return checkAuditTrialReceipt;
	}

	public void setcheckAuditTrialReceipt(GLConfigurationAuditTrialCodes cHKAuditTrialReceipt) {
		checkAuditTrialReceipt = cHKAuditTrialReceipt;
	}

	public GLConfigurationAuditTrialCodes getTransferAuditTrialPayment() {
		return transferAuditTrialPayment;
	}

	public void setTransferAuditTrialPayment(GLConfigurationAuditTrialCodes transferAuditTrialPayment) {
		this.transferAuditTrialPayment = transferAuditTrialPayment;
	}

	public GLConfigurationAuditTrialCodes getTransferAuditTrialReceipt() {
		return transferAuditTrialReceipt;
	}

	public void setTransferAuditTrialReceipt(GLConfigurationAuditTrialCodes transferAuditTrialReceipt) {
		this.transferAuditTrialReceipt = transferAuditTrialReceipt;
	}

	public GLConfigurationAuditTrialCodes getCreditAuditTrialPayment() {
		return creditAuditTrialPayment;
	}

	public void setCreditAuditTrialPayment(GLConfigurationAuditTrialCodes creditAuditTrialPayment) {
		this.creditAuditTrialPayment = creditAuditTrialPayment;
	}

	public GLConfigurationAuditTrialCodes getCreditAuditTrialReceipt() {
		return creditAuditTrialReceipt;
	}

	public void setCreditAuditTrialReceipt(GLConfigurationAuditTrialCodes creditAuditTrialReceipt) {
		this.creditAuditTrialReceipt = creditAuditTrialReceipt;
	}
	
}