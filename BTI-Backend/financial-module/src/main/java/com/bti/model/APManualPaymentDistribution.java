/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ap10200 database table.
 * 
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap10301")
@NamedQuery(name = "APManualPaymentDistribution.findAll", query = "SELECT a FROM APManualPaymentDistribution a")
public class APManualPaymentDistribution implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "MNLRXSECQ")
	private int apTransactionSequance;

	@Column(name = "MNLPYMNTID")
	private String manualPaymentNumber;

	@Column(name = "ACTROWID")
	private int accountTableRowIndex;

/*	@Column(name = "TRXTYP")
	private int transactionType;*/
	@ManyToOne
	@JoinColumn(name="TRXTYP")
	private MasterAPManualPaymentDistributionAccountTypes accountTypes;

	@Column(name = "DEBITAMT")
	private double debitAmount;

	@Column(name = "CRDTAMT")
	private double creditAmount;

	@Column(name = "ORGDEBTAMT")
	private double originalDebitAmount;
	
	@Column(name = "ORGCRETAMT")
	private double originalCreditAmount;

	@Column(name = "DISTDSCPTN")
	private String distributionDescription;

	@Column(name = "MODIFTDT")
	private Date modifyDate;

	@Column(name = "CHANGEBY")
	private String modifyByUserID;

	@Column(name = "DEX_ROW_ID")
	private int rowIndexId;

	@Column(name = "DEX_ROW_TS")
	private Date rowDateIndex;

	public int getApTransactionSequance() {
		return apTransactionSequance;
	}

	public void setApTransactionSequance(int apTransactionSequance) {
		this.apTransactionSequance = apTransactionSequance;
	}

	public String getManualPaymentNumber() {
		return manualPaymentNumber;
	}

	public void setManualPaymentNumber(String manualPaymentNumber) {
		this.manualPaymentNumber = manualPaymentNumber;
	}

	public int getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(int accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public MasterAPManualPaymentDistributionAccountTypes getAccountTypes() {
		return accountTypes;
	}

	public void setAccountTypes(MasterAPManualPaymentDistributionAccountTypes accountTypes) {
		this.accountTypes = accountTypes;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public double getOriginalDebitAmount() {
		return originalDebitAmount;
	}

	public void setOriginalDebitAmount(double originalDebitAmount) {
		this.originalDebitAmount = originalDebitAmount;
	}

	public double getOriginalCreditAmount() {
		return originalCreditAmount;
	}

	public void setOriginalCreditAmount(double originalCreditAmount) {
		this.originalCreditAmount = originalCreditAmount;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}