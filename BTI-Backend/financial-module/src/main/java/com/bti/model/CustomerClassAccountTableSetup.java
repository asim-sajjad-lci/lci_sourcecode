/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ar00201 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar00201")
@NamedQuery(name="CustomerClassAccountTableSetup.findAll", query="SELECT a FROM CustomerClassAccountTableSetup a")
public class CustomerClassAccountTableSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACCTABLID")
	private int accountTableArRowIndex;

	@ManyToOne
	@JoinColumn(name="ACCTCTTYPID")
	private MasterArAccountType masterArAccountType;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ar00200
	/*@OneToMany(mappedBy="customerClassAccountTableSetup")
	private List<CustomerClassesSetup> customerClassesSetups;*/

	//bi-directional many-to-one association to Ar00200
	@ManyToOne
	@JoinColumn(name="CUSTCLSID")
	private CustomerClassesSetup customerClassesSetup;

	//bi-directional many-to-one association to Gl49999
	@ManyToOne
	@JoinColumn(name="ACTROWID")
	private GLAccountsTableAccumulation glAccountsTableAccumulation;

	public CustomerClassAccountTableSetup() {
	}

	public int getAccountTableArRowIndex() {
		return accountTableArRowIndex;
	}

	public void setAccountTableArRowIndex(int accountTableArRowIndex) {
		this.accountTableArRowIndex = accountTableArRowIndex;
	}

	public MasterArAccountType getMasterArAccountType() {
		return masterArAccountType;
	}

	public void setMasterArAccountType(MasterArAccountType masterArAccountType) {
		this.masterArAccountType = masterArAccountType;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	

	/*public List<CustomerClassesSetup> getCustomerClassesSetups() {
		return customerClassesSetups;
	}

	public void setCustomerClassesSetups(List<CustomerClassesSetup> customerClassesSetups) {
		this.customerClassesSetups = customerClassesSetups;
	}*/

	public CustomerClassesSetup getCustomerClassesSetup() {
		return customerClassesSetup;
	}

	public void setCustomerClassesSetup(CustomerClassesSetup customerClassesSetup) {
		this.customerClassesSetup = customerClassesSetup;
	}

	public GLAccountsTableAccumulation getGlAccountsTableAccumulation() {
		return glAccountsTableAccumulation;
	}

	public void setGlAccountsTableAccumulation(GLAccountsTableAccumulation glAccountsTableAccumulation) {
		this.glAccountsTableAccumulation = glAccountsTableAccumulation;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}