/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ap10200 database table.
 * 
 */

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap10300")
@NamedQuery(name = "APManualPayment.findAll", query = "SELECT a FROM APManualPayment a")
public class APManualPayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	// @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "MNLPYMNTID")
	private String manualPaymentNumber;

	@ManyToOne
	@JoinColumn(name = "BATCHID")
	private APBatches apBatches;

	@Column(name = "VENDORID")
	private String vendorID;

	@Column(name = "VENDNAME")
	private String vendorName;

	@Column(name = "VENDNAMEA")
	private String vendorNameArabic;

	@Column(name = "CURNCYID")
	private String currencyID;

	@ManyToOne
	@JoinColumn(name = "MNLPYMTTYP")
	private MasterAPManualPaymentType masterAPManualPaymentType;
	
	@Column(name = "MNLPYMNT")
	private double manualPaymentAmount;

	@Column(name = "CHEKBOKID")
	private String checkbookID;

	@Column(name = "CARDID")
	private String creditCardID;

	@Column(name = "CARDNMN")
	private String creditCardNumber;
	
	@Column(name = "CSHCHEKT")
	private String checkNumber;

	@Column(name = "CARDEXPTM")
	private int creditCardExpireMonth;

	@Column(name = "CARDEXPTY")
	private int creditCardExpireYear;

	@Column(name = "MNLDSCRP")
	private String manualPaymentDescription;

	@Column(name = "MNLDDT")
	private Date manualPaymentCreateDate;

	@Column(name = "MNLPST")
	private Date manualPaymentPostingDate;

	@Column(name = "TRXVOID")
	private int transactionVoid;

	@Column(name = "EXGTBLID")
	private String exchangeTableIndex;

	@Column(name = "XCHGRATE")
	private double exchangeTableRate;

	@Column(name = "CREATDDT")
	private Date createdDate;

	@Column(name = "MODIFTDT")
	private Date modifyDate;

	@Column(name = "CHANGEBY")
	private String modifyByUserID;

	@Column(name = "DEX_ROW_ID")
	private int rowIndexId;

	@Column(name = "DEX_ROW_TS")
	private Date rowDateIndex;

	public String getManualPaymentNumber() {
		return manualPaymentNumber;
	}

	public void setManualPaymentNumber(String manualPaymentNumber) {
		this.manualPaymentNumber = manualPaymentNumber;
	}

	public APBatches getApBatches() {
		return apBatches;
	}

	public void setApBatches(APBatches apBatches) {
		this.apBatches = apBatches;
	}

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorNameArabic() {
		return vendorNameArabic;
	}

	public void setVendorNameArabic(String vendorNameArabic) {
		this.vendorNameArabic = vendorNameArabic;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public MasterAPManualPaymentType getMasterAPManualPaymentType() {
		return masterAPManualPaymentType;
	}

	public void setMasterAPManualPaymentType(MasterAPManualPaymentType masterAPManualPaymentType) {
		this.masterAPManualPaymentType = masterAPManualPaymentType;
	}

	public double getManualPaymentAmount() {
		return manualPaymentAmount;
	}

	public void setManualPaymentAmount(double manualPaymentAmount) {
		this.manualPaymentAmount = manualPaymentAmount;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public int getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(int creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public int getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(int creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getManualPaymentDescription() {
		return manualPaymentDescription;
	}

	public void setManualPaymentDescription(String manualPaymentDescription) {
		this.manualPaymentDescription = manualPaymentDescription;
	}

	public Date getManualPaymentCreateDate() {
		return manualPaymentCreateDate;
	}

	public void setManualPaymentCreateDate(Date manualPaymentCreateDate) {
		this.manualPaymentCreateDate = manualPaymentCreateDate;
	}

	public Date getManualPaymentPostingDate() {
		return manualPaymentPostingDate;
	}

	public void setManualPaymentPostingDate(Date manualPaymentPostingDate) {
		this.manualPaymentPostingDate = manualPaymentPostingDate;
	}

	public int getTransactionVoid() {
		return transactionVoid;
	}

	public void setTransactionVoid(int transactionVoid) {
		this.transactionVoid = transactionVoid;
	}

	public String getExchangeTableIndex() {
		return exchangeTableIndex;
	}

	public void setExchangeTableIndex(String exchangeTableIndex) {
		this.exchangeTableIndex = exchangeTableIndex;
	}

	public double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}