/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FALocationSetup;

public class DtoLocationSetup {

	private int locationIndex;
	private String locationId;
	private int stateId;
	private int cityId;
	private int countryId;
	private String state;
	private String city;
	private String country;
	
	public DtoLocationSetup(){
		
	}
	
	public DtoLocationSetup(FALocationSetup faLocationSetup){
		this.locationIndex = faLocationSetup.getLocationIndx();
		this.locationId = faLocationSetup.getLocationId();
		this.stateId = faLocationSetup.getStateMaster().getStateId();
		this.cityId = faLocationSetup.getCityMaster().getCityId();
		this.countryId = faLocationSetup.getCountryMaster().getCountryId();
		this.state = faLocationSetup.getStateMaster().getStateName();
		this.city = faLocationSetup.getCityMaster().getCityName();
		this.country = faLocationSetup.getCountryMaster().getCountryName();
		
	}
	
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public int getLocationIndex() {
		return locationIndex;
	}

	public void setLocationIndex(int locationIndex) {
		this.locationIndex = locationIndex;
	}
	
	
	
}
