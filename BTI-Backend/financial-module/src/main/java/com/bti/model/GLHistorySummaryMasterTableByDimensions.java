/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl90112 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl90212") @IdClass(GLHistorySummaryMasterTableByDimensionsKey.class)
@NamedQuery(name="GLHistorySummaryMasterTableByDimensions.findAll", query="SELECT s FROM GLHistorySummaryMasterTableByDimensions s")
public class GLHistorySummaryMasterTableByDimensions {

	@Id
	@Column(name="ACTINDX")
	private int mainAccountIndex;

	@Id
	@Column(name="YEAR1")
	private int year;

	@Id
	@Column(name="PERIODID")
	private int periodID;
	
	@Column(name="PERBALNC")
	private double periodBalance;
	
	@Column(name="DEBITAMT")
	private double debitAmount;
	
	@Column(name="DIMINXD1")
	private int DimensionIndex1;
	
	@Column(name="DIMINXD2")
	private int DimensionIndex2;
	
	@Column(name="DIMINXD3")
	private int DimensionIndex3;
	
	@Column(name="DIMINXD4")
	private int DimensionIndex4;
	
	@Column(name="DIMINXD5")
	private int DimensionIndex5;
	
	@Column(name="CRDTAMT")
	private double creditAmount;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	  
	public GLHistorySummaryMasterTableByDimensions() {
	}

	public int getMainAccountIndex() {
		return mainAccountIndex;
	}

	public void setMainAccountIndex(int mainAccountIndex) {
		this.mainAccountIndex = mainAccountIndex;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getPeriodID() {
		return periodID;
	}

	public void setPeriodID(int periodID) {
		this.periodID = periodID;
	}

	public double getPeriodBalance() {
		return periodBalance;
	}

	public void setPeriodBalance(double periodBalance) {
		this.periodBalance = periodBalance;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public int getDimensionIndex1() {
		return DimensionIndex1;
	}

	public void setDimensionIndex1(int dimensionIndex1) {
		DimensionIndex1 = dimensionIndex1;
	}

	public int getDimensionIndex2() {
		return DimensionIndex2;
	}

	public void setDimensionIndex2(int dimensionIndex2) {
		DimensionIndex2 = dimensionIndex2;
	}

	public int getDimensionIndex3() {
		return DimensionIndex3;
	}

	public void setDimensionIndex3(int dimensionIndex3) {
		DimensionIndex3 = dimensionIndex3;
	}

	public int getDimensionIndex4() {
		return DimensionIndex4;
	}

	public void setDimensionIndex4(int dimensionIndex4) {
		DimensionIndex4 = dimensionIndex4;
	}

	public int getDimensionIndex5() {
		return DimensionIndex5;
	}

	public void setDimensionIndex5(int dimensionIndex5) {
		DimensionIndex5 = dimensionIndex5;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}