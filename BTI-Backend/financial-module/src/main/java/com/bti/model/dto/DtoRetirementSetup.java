/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright � 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.FARetirementSetup;

/**
 * Description: Dto Retirement Setup
 * Name of Project: BTI
 * Created on: Aug 22, 2017
 * Modified on: Aug 22, 2017 4:25:38 PM
 * @author seasia
 * Version: 
 */
public class DtoRetirementSetup {

	private String faRetirementId;
	private String descriptionPrimary;
	private String descriptionSecondary;
	
	DtoRetirementSetup(){
		
	}
	
	public DtoRetirementSetup(FARetirementSetup faRetirementSetup){
		this.faRetirementId = faRetirementSetup.getRetirementId();
		this.descriptionPrimary = faRetirementSetup.getRetirementDescription();
		this.descriptionSecondary = faRetirementSetup.getRetirementDescriptionArabic();
	}
	
	public String getFaRetirementId() {
		return faRetirementId;
	}
	public void setFaRetirementId(String faRetirementId) {
		this.faRetirementId = faRetirementId;
	}
	public String getDescriptionPrimary() {
		return descriptionPrimary;
	}
	public void setDescriptionPrimary(String descriptionPrimary) {
		this.descriptionPrimary = descriptionPrimary;
	}
	public String getDescriptionSecondary() {
		return descriptionSecondary;
	}
	public void setDescriptionSecondary(String descriptionSecondary) {
		this.descriptionSecondary = descriptionSecondary;
	}
	
	
	
}
