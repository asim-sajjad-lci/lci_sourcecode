/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the master seperator decimal database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "master_seperator_decimal")
@NamedQuery(name="MasterSeperatorDecimal.findAll", query="SELECT a FROM MasterSeperatorDecimal a")
public class MasterSeperatorDecimal implements Serializable {
	private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="ID")
		private int id;

		@Column(name="SEPERATOR_DECIMAL_PRIMARY")
		private String seperatorDecimalPrimary;

		@Column(name="SEPERATOR_DECIMAL_ID")
		private int seperatorDecimalId;
		
		@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
		private boolean isDeleted;
		
		@ManyToOne
		@JoinColumn(name="lang_Id")
		private Language language;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getSeperatorDecimalPrimary() {
			return seperatorDecimalPrimary;
		}

		public void setSeperatorDecimalPrimary(String seperatorDecimalPrimary) {
			this.seperatorDecimalPrimary = seperatorDecimalPrimary;
		}

		public int getSeperatorDecimalId() {
			return seperatorDecimalId;
		}

		public void setSeperatorDecimalId(int seperatorDecimalId) {
			this.seperatorDecimalId = seperatorDecimalId;
		}

		public Language getLanguage() {
			return language;
		}

		public void setLanguage(Language language) {
			this.language = language;
		}

		public boolean isDeleted() {
			return isDeleted;
		}

		public void setDeleted(boolean isDeleted) {
			this.isDeleted = isDeleted;
		}
		
}