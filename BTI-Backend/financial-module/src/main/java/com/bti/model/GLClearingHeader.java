/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl10200 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl10200")
@NamedQuery(name="GLClearingHeader .findAll", query="SELECT a FROM GLClearingHeader  a")
public class GLClearingHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="JORNID")
	private String journalID;
	
	@ManyToOne
	@JoinColumn(name="BATCHID")
	private GLBatches glBatches;
	
	@ManyToOne
	@JoinColumn(name="ORGINID")
	private GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes;

	@Column(name="TRXDT")
	private Date transactionDate;
	
	@Column(name="DSCRPTN")
	private String transactionDescription;

	@Column(name="BLNTYP")
	private int balanceType;
	
	@Column(name="CREATDDT")
	private Date createDate;
	
	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="CHANGEBY")
	private String modifyByUserID;
	
	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DEX_ROW_ID")
	private int rowIDIndex;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private Boolean isDeleted;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJournalID() {
		return journalID;
	}

	public void setJournalID(String journalID) {
		this.journalID = journalID;
	}

	public GLBatches getGlBatches() {
		return glBatches;
	}

	public void setGlBatches(GLBatches glBatches) {
		this.glBatches = glBatches;
	}

	public GLConfigurationAuditTrialCodes getGlConfigurationAuditTrialCodes() {
		return glConfigurationAuditTrialCodes;
	}

	public void setGlConfigurationAuditTrialCodes(GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes) {
		this.glConfigurationAuditTrialCodes = glConfigurationAuditTrialCodes;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyByUserID() {
		return modifyByUserID;
	}

	public void setModifyByUserID(String modifyByUserID) {
		this.modifyByUserID = modifyByUserID;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getRowIDIndex() {
		return rowIDIndex;
	}

	public void setRowIDIndex(int rowIDIndex) {
		this.rowIDIndex = rowIDIndex;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}     
	
}