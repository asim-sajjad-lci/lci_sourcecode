/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the gl00101 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl00101")
@NamedQuery(name="COAMainAccounts.findAll", query="SELECT g FROM COAMainAccounts g")
public class COAMainAccounts implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTINDX")
	private long actIndx;
	
//	
	@Column(name="ACTNUMID")
	private String mainAccountNumber;
	
	@Column(name="ACCTENTR")
	private Boolean allowAccountTransactionEntry;

	@Column(name="ACTALIAS")
	private String aliasAccount;

	@Column(name="ACTALIASA")
	private String aliasAccountArabic;

	@Column(name="ACTDESCR")
	private String mainAccountDescription;

	@Column(name="ACTDESCRA")
	private String mainAccountDescriptionArabic;

	@Column(name="ACTIVE")
	private Boolean active;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="FXALLOCACC")
	private Boolean fixedAllocationAccount;

	@Column(name="MODIFDT")
	private Date modifyDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	@ManyToOne
	@JoinColumn(name="TPCLBLNC")
	private TPCLBalanceType tpclBalanceType;
	

	@Column(name="UNACCOUNT")
	private Boolean unitAccount;

	@Column(name="USERDEF1")
	private String userDef1;

	@Column(name="USERDEF2")
	private String userDef2;

	@Column(name="USERDEF3")
	private String userDef3;

	@Column(name="USERDEF4")
	private String userDef4;

	@Column(name="USERDEF5")
	private String userDef5;

	//bi-directional many-to-one association to Gl40001
	@ManyToOne
	@JoinColumn(name="ACCATNUM")
	private GLConfigurationAccountCategories glConfigurationAccountCategories;
	
	@ManyToOne
	@JoinColumn(name="ACCTTYPE")
	private MainAccountTypes mainAccountTypes;

	//bi-directional many-to-one association to gl40400
	@OneToMany(mappedBy="coaMainAccounts")
	private List<AccountCurrencyAccess> accountCurrencyAccesses;
	
	public List<AccountCurrencyAccess> getAccountCurrencyAccesses() {
		return accountCurrencyAccesses;
	}

	public void setAccountCurrencyAccesses(List<AccountCurrencyAccess> accountCurrencyAccesses) {
		this.accountCurrencyAccesses = accountCurrencyAccesses;
	}

	public COAMainAccounts() {
		// To do nothing
	}

	public String getMainAccountNumber() {
		return mainAccountNumber;
	}

	public void setMainAccountNumber(String mainAccountNumber) {
		this.mainAccountNumber = mainAccountNumber;
	}

	public long getActIndx() {
		return actIndx;
	}

	public void setActIndx(long actIndx) {
		this.actIndx = actIndx;
	}

	public Boolean getAllowAccountTransactionEntry() {
		return allowAccountTransactionEntry;
	}

	public void setAllowAccountTransactionEntry(Boolean allowAccountTransactionEntry) {
		this.allowAccountTransactionEntry = allowAccountTransactionEntry;
	}

	public String getAliasAccount() {
		return aliasAccount;
	}

	public void setAliasAccount(String aliasAccount) {
		this.aliasAccount = aliasAccount;
	}

	public String getAliasAccountArabic() {
		return aliasAccountArabic;
	}

	public void setAliasAccountArabic(String aliasAccountArabic) {
		this.aliasAccountArabic = aliasAccountArabic;
	}

	public String getMainAccountDescription() {
		return mainAccountDescription;
	}

	public void setMainAccountDescription(String mainAccountDescription) {
		this.mainAccountDescription = mainAccountDescription;
	}

	public String getMainAccountDescriptionArabic() {
		return mainAccountDescriptionArabic;
	}

	public void setMainAccountDescriptionArabic(String mainAccountDescriptionArabic) {
		this.mainAccountDescriptionArabic = mainAccountDescriptionArabic;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Boolean getFixedAllocationAccount() {
		return fixedAllocationAccount;
	}

	public void setFixedAllocationAccount(Boolean fixedAllocationAccount) {
		this.fixedAllocationAccount = fixedAllocationAccount;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public TPCLBalanceType getTpclBalanceType() {
		return tpclBalanceType;
	}

	public void setTpclBalanceType(TPCLBalanceType tpclBalanceType) {
		this.tpclBalanceType = tpclBalanceType;
	}

	public Boolean getUnitAccount() {
		return unitAccount;
	}

	public void setUnitAccount(Boolean unitAccount) {
		this.unitAccount = unitAccount;
	}

	public String getUserDef1() {
		return userDef1;
	}

	public void setUserDef1(String userDef1) {
		this.userDef1 = userDef1;
	}

	public String getUserDef2() {
		return userDef2;
	}

	public void setUserDef2(String userDef2) {
		this.userDef2 = userDef2;
	}

	public String getUserDef3() {
		return userDef3;
	}

	public void setUserDef3(String userDef3) {
		this.userDef3 = userDef3;
	}

	public String getUserDef4() {
		return userDef4;
	}

	public void setUserDef4(String userDef4) {
		this.userDef4 = userDef4;
	}

	public String getUserDef5() {
		return userDef5;
	}

	public void setUserDef5(String userDef5) {
		this.userDef5 = userDef5;
	}

	public GLConfigurationAccountCategories getGlConfigurationAccountCategories() {
		return glConfigurationAccountCategories;
	}

	public void setGlConfigurationAccountCategories(GLConfigurationAccountCategories glConfigurationAccountCategories) {
		this.glConfigurationAccountCategories = glConfigurationAccountCategories;
	}

	public MainAccountTypes getMainAccountTypes() {
		return mainAccountTypes;
	}

	public void setMainAccountTypes(MainAccountTypes mainAccountTypes) {
		this.mainAccountTypes = mainAccountTypes;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	
	/*public List<GLConfigurationRelationBetweenDimensionsAndMainAccount> getGlConfigurationRelationBetweenDimensionsAndMainAccounts() {
		return glConfigurationRelationBetweenDimensionsAndMainAccounts;
	}

	public void setGlConfigurationRelationBetweenDimensionsAndMainAccounts(
			List<GLConfigurationRelationBetweenDimensionsAndMainAccount> glConfigurationRelationBetweenDimensionsAndMainAccounts) {
		this.glConfigurationRelationBetweenDimensionsAndMainAccounts = glConfigurationRelationBetweenDimensionsAndMainAccounts;
	}*/

	/*public List<GLAccountsTableAccumulation> getGlAccountsTableAccumulations() {
		return glAccountsTableAccumulations;
	}

	public void setGlAccountsTableAccumulations(List<GLAccountsTableAccumulation> glAccountsTableAccumulations) {
		this.glAccountsTableAccumulations = glAccountsTableAccumulations;
	}*/

	
	

	
}