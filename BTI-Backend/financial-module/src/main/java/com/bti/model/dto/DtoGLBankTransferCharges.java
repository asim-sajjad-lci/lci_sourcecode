package com.bti.model.dto;

import com.bti.model.GLBankTransferEntryCharges;
import com.bti.model.GLBankTransferHeader;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoGLBankTransferCharges {
	private String chargeNumber;
	private Integer bankTransferNumber;
	private String checkbookID;
	private String companyID;
	private String accountNumber;
	private Double chargeAmount;
	private String chargeComments;
	private String tenantId;
	public DtoGLBankTransferCharges() {
		// TO DO nothing
	}
	public DtoGLBankTransferCharges(GLBankTransferEntryCharges bankTransferEntryCharges) {
		 this.chargeAmount =  UtilRoundDecimal.roundDecimalValue(bankTransferEntryCharges.getChargeAmount());
		 this.accountNumber = bankTransferEntryCharges.getAccountTableRowIndex();
		 this.chargeNumber = bankTransferEntryCharges.getChargeNumber();
		 this.bankTransferNumber =bankTransferEntryCharges.getGlBankTransferHeader()!=null?bankTransferEntryCharges.getGlBankTransferHeader().getBankTransferNumber():0;
		 this.checkbookID = bankTransferEntryCharges.getCheckbookId();
		 this.chargeComments = bankTransferEntryCharges.getChargeComments();
		 this.companyID = bankTransferEntryCharges.getGlBankTransferHeader()!=null?bankTransferEntryCharges.getGlBankTransferHeader().getCompanyTransferFrom():"";
	}
	
	public DtoGLBankTransferCharges(GLBankTransferHeader bankTransferHeader) {
		 this.companyID = bankTransferHeader.getCompanyTransferFrom();
		 this.bankTransferNumber = bankTransferHeader.getBankTransferNumber();
		 this.checkbookID = bankTransferHeader.getCheckBookIdFrom();
	}
	
	public String getCheckbookID() {
		return checkbookID;
	}
	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getChargeComments() {
		return chargeComments;
	}
	
	public void setChargeComments(String chargeComments) {
		this.chargeComments = chargeComments;
	}
	
	public String getCompanyID() {
		return companyID;
	}
	
	public void setCompanyID(String companyID) {
		this.companyID = companyID;
	}
	
	public Integer getBankTransferNumber() {
		return bankTransferNumber;
	}
	public void setBankTransferNumber(Integer bankTransferNumber) {
		this.bankTransferNumber = bankTransferNumber;
	}
	public Double getChargeAmount() {
		return chargeAmount;
	}
	public void setChargeAmount(Double chargeAmount) {
		this.chargeAmount = UtilRoundDecimal.roundDecimalValue(chargeAmount);
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getChargeNumber() {
		return chargeNumber;
	}
	public void setChargeNumber(String chargeNumber) {
		this.chargeNumber = chargeNumber;
	}
}
