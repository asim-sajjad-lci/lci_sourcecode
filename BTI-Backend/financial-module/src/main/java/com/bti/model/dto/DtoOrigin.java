/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.constant.ConfigSetting;
import com.bti.model.MasterMassCloseOrigin;
import com.bti.model.ModulesConfiguration;

/**
 * Description: DTO Module Configuration class having getter and setter for
 * fields (POJO) Name Name of Project: BTI Created on: Aug 04, 2017 Modified on:
 * Aug 04, 2017 4:19:38 PM
 * 
 * @author seasia Version:
 */
// @JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoOrigin {

	private int originId;
	private String originName;

	public DtoOrigin() {

	}

	public DtoOrigin(MasterMassCloseOrigin masterMassCloseOrigin) 
	{
		this.originId = masterMassCloseOrigin.getTypeId();
	    this.originName = masterMassCloseOrigin.getOrigin();
	}

	public int getOriginId() {
		return originId;
	}

	public void setOriginId(int originId) {
		this.originId = originId;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

}
