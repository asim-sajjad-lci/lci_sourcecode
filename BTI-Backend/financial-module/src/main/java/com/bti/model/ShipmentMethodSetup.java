/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the sy03400 database table.
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "sy03400")
@NamedQuery(name="ShipmentMethodSetup.findAll", query="SELECT s FROM ShipmentMethodSetup s")
public class ShipmentMethodSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SHIPMTHD")
	private String shipmentMethodId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRIPTN")
	private String shipmentDescription;

	@Column(name="DSCRIPTNA")
	private String shipmentDescriptionArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="SHIPCARACC")
	private String shipmentCarrierAccount;

	@Column(name="SHIPCARR")
	private String shipmentCarrier;

	@Column(name="SHIPCARRA")
	private String shipmentCarrierArabic;

	@Column(name="SHIPCONTAC")
	private String shipmentCarrierContactName;

	@Column(name="SHIPPHON")
	private String shipmentPhoneNumber;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	/*@Column(name="SHIPTYP")
	private int shipmentMethodType;*/
	
	@ManyToOne
	@JoinColumn(name="SHIPTYP")
	private ShipmentMethodType shipmentMethodType;
	
	//bi-directional many-to-one association to Ap00102
	@OneToMany(mappedBy="shipmentMethodSetup")
	private List<VendorMaintenanceOptions> vendorMaintenanceOptions;

	//bi-directional many-to-one association to Ap00200
	@OneToMany(mappedBy="shipmentMethodSetup")
	private List<VendorClassesSetup> vendorClassesSetups;

	//bi-directional many-to-one association to Ar00102
	@OneToMany(mappedBy="shipmentMethodSetup")
	private List<CustomerMaintenanceOptions> customerMaintenanceOptions;

	//bi-directional many-to-one association to Ar00200
	@OneToMany(mappedBy="shipmentMethodSetup")
	private List<CustomerClassesSetup> customerClassesSetups;

	public ShipmentMethodSetup() {
	}

	public String getShipmentMethodId() {
		return shipmentMethodId;
	}

	public void setShipmentMethodId(String shipmentMethodId) {
		this.shipmentMethodId = shipmentMethodId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getShipmentDescription() {
		return shipmentDescription;
	}

	public void setShipmentDescription(String shipmentDescription) {
		this.shipmentDescription = shipmentDescription;
	}

	public String getShipmentDescriptionArabic() {
		return shipmentDescriptionArabic;
	}

	public void setShipmentDescriptionArabic(String shipmentDescriptionArabic) {
		this.shipmentDescriptionArabic = shipmentDescriptionArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getShipmentCarrierAccount() {
		return shipmentCarrierAccount;
	}

	public void setShipmentCarrierAccount(String shipmentCarrierAccount) {
		this.shipmentCarrierAccount = shipmentCarrierAccount;
	}

	public String getShipmentCarrier() {
		return shipmentCarrier;
	}

	public void setShipmentCarrier(String shipmentCarrier) {
		this.shipmentCarrier = shipmentCarrier;
	}

	public String getShipmentCarrierArabic() {
		return shipmentCarrierArabic;
	}

	public void setShipmentCarrierArabic(String shipmentCarrierArabic) {
		this.shipmentCarrierArabic = shipmentCarrierArabic;
	}

	public String getShipmentCarrierContactName() {
		return shipmentCarrierContactName;
	}

	public void setShipmentCarrierContactName(String shipmentCarrierContactName) {
		this.shipmentCarrierContactName = shipmentCarrierContactName;
	}

	public String getShipmentPhoneNumber() {
		return shipmentPhoneNumber;
	}

	public void setShipmentPhoneNumber(String shipmentPhoneNumber) {
		this.shipmentPhoneNumber = shipmentPhoneNumber;
	}

	/*public int getShipmentMethodType() {
		return shipmentMethodType;
	}

	public void setShipmentMethodType(int shipmentMethodType) {
		this.shipmentMethodType = shipmentMethodType;
	}
*/

	public ShipmentMethodType getShipmentMethodType() {
		return shipmentMethodType;
	}

	public void setShipmentMethodType(ShipmentMethodType shipmentMethodType) {
		this.shipmentMethodType = shipmentMethodType;
	}
	
	
	public List<VendorMaintenanceOptions> getVendorMaintenanceOptions() {
		return vendorMaintenanceOptions;
	}

	public void setVendorMaintenanceOptions(List<VendorMaintenanceOptions> vendorMaintenanceOptions) {
		this.vendorMaintenanceOptions = vendorMaintenanceOptions;
	}

	public List<VendorClassesSetup> getVendorClassesSetups() {
		return vendorClassesSetups;
	}

	public void setVendorClassesSetups(List<VendorClassesSetup> vendorClassesSetups) {
		this.vendorClassesSetups = vendorClassesSetups;
	}

	public List<CustomerMaintenanceOptions> getCustomerMaintenanceOptions() {
		return customerMaintenanceOptions;
	}

	public void setCustomerMaintenanceOptions(List<CustomerMaintenanceOptions> customerMaintenanceOptions) {
		this.customerMaintenanceOptions = customerMaintenanceOptions;
	}

	public List<CustomerClassesSetup> getCustomerClassesSetups() {
		return customerClassesSetups;
	}

	public void setCustomerClassesSetups(List<CustomerClassesSetup> customerClassesSetups) {
		this.customerClassesSetups = customerClassesSetups;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
}