/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gl90211 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl90211") @IdClass(GLHistorySummaryMasterTableByAccountNumberKey.class)
@NamedQuery(name="GLHistorySummaryMasterTableByAccountNumber.findAll", query="SELECT s FROM GLHistorySummaryMasterTableByAccountNumber s")
public class GLHistorySummaryMasterTableByAccountNumber  {

	@Id
	@Column(name="ACTROWID")
	private int accountTableRowIndex;

	@Id
	@Column(name="YEAR1")
	private int year;

	@Id
	@Column(name="PERIODID")
	private int periodID;
	
	@Column(name="PERBALNC")
	private double periodBalance;
	
	@Column(name="DEBITAMT")
	private double debitAmount;
	
	@Column(name="CRDTAMT")
	private double creditAmount;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;
	  
	public GLHistorySummaryMasterTableByAccountNumber() {
	}

	public int getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(int accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getPeriodID() {
		return periodID;
	}

	public void setPeriodID(int periodID) {
		this.periodID = periodID;
	}

	public double getPeriodBalance() {
		return periodBalance;
	}

	public void setPeriodBalance(double periodBalance) {
		this.periodBalance = periodBalance;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}
	
}