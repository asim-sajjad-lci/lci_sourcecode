/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ap90500 database table.
 * 
 */

@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ap90500")
@NamedQuery(name="APYTDHistoryPayments.findAll", query="SELECT a FROM APYTDHistoryPayments a")
public class APYTDHistoryPayments implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="VENDORID")
	private String vendorID;
	
	@Column(name="DOCNUMBR")
	private String paymentNumber;
	
	@Column(name="CHEKBOKID")
	private String checkbookID;
	
	@Column(name="BATCHID")
	private String batchID;
	
	@Column(name="APTRXTYP")
	private int apTransactionType;
	
	@Column(name="APTRXNO")
	private String apTransactionNumber;
	
	@Column(name="CURNCYID")
	private String currencyID;
	
	@Column(name="PYMTTYP")
	private int manualPaymentType;
	
	@Column(name="CARDID")
	private String creditCardID;
	
	@Column(name="CARDNMN")
	private String creditCardNumber;
	 
	@Column(name="CHKNUMBR")
	private String checkNumber;
	
	@Column(name="CARDEXPTY")
	private String creditCardExpireYear;
	
	@Column(name="CARDEXPTM")
	private String creditCardExpireMonth;
	
	@Column(name="PYMTDSCR")
	private String paymentDescription;

	@Column(name="PYMNTDDT")
	private Date paymentCreateDate;
	
	@Column(name="PYMNTUSR")
	private Date paymentCreateByUserID;
	
	@Column(name="PYMNTPST")
	private Date paymentPostingDate;
	
	@Column(name="PYMNTPSTU")
	private String paymentPostingByUserID;
	
	@Column(name="PYMNTAMT")
	private double paymentAmount;
	
	@Column(name="PYMNTAMTO")
	private double originalPaymentAmount;
	
	@Column(name="EXGTBLID")
	private String exchangeTableID;
	
	@Column(name="XCHGRATE")
	private double exchangeTableRate;
	
	@Column(name="YEAR1")
	private int Year;
	
	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public String getCheckbookID() {
		return checkbookID;
	}

	public void setCheckbookID(String checkbookID) {
		this.checkbookID = checkbookID;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public int getApTransactionType() {
		return apTransactionType;
	}

	public void setApTransactionType(int apTransactionType) {
		this.apTransactionType = apTransactionType;
	}

	public String getApTransactionNumber() {
		return apTransactionNumber;
	}

	public void setApTransactionNumber(String apTransactionNumber) {
		this.apTransactionNumber = apTransactionNumber;
	}

	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public int getManualPaymentType() {
		return manualPaymentType;
	}

	public void setManualPaymentType(int manualPaymentType) {
		this.manualPaymentType = manualPaymentType;
	}

	public String getCreditCardID() {
		return creditCardID;
	}

	public void setCreditCardID(String creditCardID) {
		this.creditCardID = creditCardID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCreditCardExpireYear() {
		return creditCardExpireYear;
	}

	public void setCreditCardExpireYear(String creditCardExpireYear) {
		this.creditCardExpireYear = creditCardExpireYear;
	}

	public String getCreditCardExpireMonth() {
		return creditCardExpireMonth;
	}

	public void setCreditCardExpireMonth(String creditCardExpireMonth) {
		this.creditCardExpireMonth = creditCardExpireMonth;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public Date getPaymentCreateDate() {
		return paymentCreateDate;
	}

	public void setPaymentCreateDate(Date paymentCreateDate) {
		this.paymentCreateDate = paymentCreateDate;
	}

	public Date getPaymentCreateByUserID() {
		return paymentCreateByUserID;
	}

	public void setPaymentCreateByUserID(Date paymentCreateByUserID) {
		this.paymentCreateByUserID = paymentCreateByUserID;
	}

	public Date getPaymentPostingDate() {
		return paymentPostingDate;
	}

	public void setPaymentPostingDate(Date paymentPostingDate) {
		this.paymentPostingDate = paymentPostingDate;
	}

	public String getPaymentPostingByUserID() {
		return paymentPostingByUserID;
	}

	public void setPaymentPostingByUserID(String paymentPostingByUserID) {
		this.paymentPostingByUserID = paymentPostingByUserID;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public double getOriginalPaymentAmount() {
		return originalPaymentAmount;
	}

	public void setOriginalPaymentAmount(double originalPaymentAmount) {
		this.originalPaymentAmount = originalPaymentAmount;
	}

	public String getExchangeTableID() {
		return exchangeTableID;
	}

	public void setExchangeTableID(String exchangeTableID) {
		this.exchangeTableID = exchangeTableID;
	}

	public double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public int getYear() {
		return Year;
	}

	public void setYear(int year) {
		Year = year;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

}