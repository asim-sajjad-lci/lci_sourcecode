/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the gl40003 database table.
 * 
 */
 
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "gl40003")
@NamedQuery(name="GLConfigurationDefinePeriodsHeader.findAll", query="SELECT g FROM GLConfigurationDefinePeriodsHeader g")
public class GLConfigurationDefinePeriodsHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	/*@GeneratedValue(strategy=GenerationType.IDENTITY)*/
	@Column(name="YEAR1")
	private int year;

	@Column(name="CHANGEBY")
	private int changeBy;
	
	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="FRTFICDT")
	private Date firstFiscalDate;

	@Column(name="HSTYEAR")
	private Boolean historicalYear;

	@Column(name="LSTFICDT")
	private Date lastFiscalDate;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="NMOFPERD")
	private int numberOfPeriods;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Gl40004
	@OneToMany(mappedBy="glConfigurationDefinePeriodsHeader")
	private List<GLConfigurationDefinePeriodsDetails> glConfigurationDefinePeriodsDetails;

	public GLConfigurationDefinePeriodsHeader() {
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public Date getFirstFiscalDate() {
		return firstFiscalDate;
	}

	public void setFirstFiscalDate(Date firstFiscalDate) {
		this.firstFiscalDate = firstFiscalDate;
	}

	public Boolean getHistoricalYear() {
		return historicalYear;
	}

	public void setHistoricalYear(Boolean historicalYear) {
		this.historicalYear = historicalYear;
	}

	public Date getLastFiscalDate() {
		return lastFiscalDate;
	}

	public void setLastFiscalDate(Date lastFiscalDate) {
		this.lastFiscalDate = lastFiscalDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getNumberOfPeriods() {
		return numberOfPeriods;
	}

	public void setNumberOfPeriods(int numberOfPeriods) {
		this.numberOfPeriods = numberOfPeriods;
	}

	public List<GLConfigurationDefinePeriodsDetails> getGlConfigurationDefinePeriodsDetails() {
		return glConfigurationDefinePeriodsDetails;
	}

	public void setGlConfigurationDefinePeriodsDetails(
			List<GLConfigurationDefinePeriodsDetails> glConfigurationDefinePeriodsDetails) {
		this.glConfigurationDefinePeriodsDetails = glConfigurationDefinePeriodsDetails;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
    
	

}