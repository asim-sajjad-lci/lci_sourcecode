/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ar00101 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar00101")
@NamedQuery(name="CustomerMaintenance.findAll", query="SELECT a FROM CustomerMaintenance a")
public class CustomerMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CUSTNMBR")
	private String custnumber;

	@Column(name="ADDRESS1")
	private String address1;

	@Column(name="ADDRESS2")
	private String address2;

	@Column(name="ADDRESS3")
	private String address3;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="CITY")
	private String city;

	@Column(name="COUNRTY")
	private String counrty;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	@Column(name="CUSTACTV")
	private int customerActiveStatus;
	
	@Column(name="CUSTHLD")
	private int customerHoldStatus;

	@Column(name="CUSTNAME")
	private String customerName;

	@Column(name="CUSTNAMEA")
	private String customerNameArabic;

	@Column(name="CUSTSHRT")
	private String customerShortName;

	@Column(name="DEX_ROW_ID")
	private String rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="FAX1")
	private String fax1;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="PHONE1")
	private String phoneNumber1;

	@Column(name="PHONE2")
	private String phoneNumber2;

	@Column(name="PHONE3")
	private String phoneNumber3;

	@Column(name="STATE")
	private String state;

	@Column(name="STATNAME")
	private String customerStateName;

	@Column(name="USERDEFN1")
	private String userDefine1;

	@Column(name="USERDEFN2")
	private String userDefine2;
	
	@Column(name="CUSPRTY")
	private String customerPriority;

	//bi-directional many-to-one association to Ar00200
	@ManyToOne
	@JoinColumn(name="customerClassId")
	private CustomerClassesSetup customerClassesSetup;

	//bi-directional one-to-one association to Ar00102
	@OneToOne(mappedBy="customerMaintenance")
	private CustomerMaintenanceOptions customerMaintenanceOptions;

	//bi-directional many-to-one association to Ar00103
	@OneToMany(mappedBy="customerMaintenance")
	private List<CustomerAccountTableSetup> customerAccountTableSetups;

	public CustomerMaintenance() {
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounrty() {
		return this.counrty;
	}

	public void setCounrty(String counrty) {
		this.counrty = counrty;
	}

	public String getCustnumber() {
		return custnumber;
	}

	public void setCustnumber(String custnumber) {
		this.custnumber = custnumber;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNameArabic() {
		return customerNameArabic;
	}

	public void setCustomerNameArabic(String customerNameArabic) {
		this.customerNameArabic = customerNameArabic;
	}

	public String getCustomerShortName() {
		return customerShortName;
	}

	public void setCustomerShortName(String customerShortName) {
		this.customerShortName = customerShortName;
	}

	public String getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(String rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getFax1() {
		return fax1;
	}

	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public String getPhoneNumber3() {
		return phoneNumber3;
	}

	public void setPhoneNumber3(String phoneNumber3) {
		this.phoneNumber3 = phoneNumber3;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCustomerStateName() {
		return customerStateName;
	}

	public void setCustomerStateName(String customerStateName) {
		this.customerStateName = customerStateName;
	}

	public String getUserDefine1() {
		return userDefine1;
	}

	public void setUserDefine1(String userDefine1) {
		this.userDefine1 = userDefine1;
	}

	public String getUserDefine2() {
		return userDefine2;
	}

	public void setUserDefine2(String userDefine2) {
		this.userDefine2 = userDefine2;
	}

	public CustomerClassesSetup getCustomerClassesSetup() {
		return customerClassesSetup;
	}

	public void setCustomerClassesSetup(CustomerClassesSetup customerClassesSetup) {
		this.customerClassesSetup = customerClassesSetup;
	}

	public CustomerMaintenanceOptions getCustomerMaintenanceOptions() {
		return customerMaintenanceOptions;
	}

	public void setCustomerMaintenanceOptions(CustomerMaintenanceOptions customerMaintenanceOptions) {
		this.customerMaintenanceOptions = customerMaintenanceOptions;
	}

	public List<CustomerAccountTableSetup> getCustomerAccountTableSetups() {
		return customerAccountTableSetups;
	}

	public void setCustomerAccountTableSetups(List<CustomerAccountTableSetup> customerAccountTableSetups) {
		this.customerAccountTableSetups = customerAccountTableSetups;
	}

	public String getCustomerPriority() {
		return customerPriority;
	}

	public void setCustomerPriority(String customerPriority) {
		this.customerPriority = customerPriority;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getCustomerActiveStatus() {
		return customerActiveStatus;
	}

	public void setCustomerActiveStatus(int customerActiveStatus) {
		this.customerActiveStatus = customerActiveStatus;
	}

	public int getCustomerHoldStatus() {
		return customerHoldStatus;
	}

	public void setCustomerHoldStatus(int customerHoldStatus) {
		this.customerHoldStatus = customerHoldStatus;
	}
	
}