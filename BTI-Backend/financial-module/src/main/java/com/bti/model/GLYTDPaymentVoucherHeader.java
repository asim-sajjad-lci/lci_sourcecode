package com.bti.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bti.model.dto.DtoGLPaymentVoucherHeader;

@Entity
@Table(name="gl91100")
@NamedQuery(name="GLYTDPaymentVoucherHeader.findAll",query="select a from GLYTDPaymentVoucherHeader a")
public class GLYTDPaymentVoucherHeader extends BaseEntity {

private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "payvouchid")
	private List<GLYTDPaymentVoucherDetail> glytdPaymentVoucherDetails;
	
	@Column(name="PAYVOCHID")
	private int paymentVoucherId;
	
	
	@ManyToOne
	@JoinColumn(name="BATCHID")
	private GLBatches glBatches;
	
	@Column(name="TRXNUM")
	private Long TransactionNumber;
	
	@ManyToOne
	@JoinColumn(name="GLTRXTYP")
	private PaymentMethodType paymentMethodType;
	
	@OneToOne
	@JoinColumn(name="SORCDOC")
	private GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes;
	
	@Column(name="ACTROWID")
	private String accountTableRowIndex;
	
	@Column(name="TPCLBLNC")
	private int balanceType;
	
	@Column(name="ACTTYP")
	private int accountType;
	
	@Column(name="INTERID" , columnDefinition="tinyint(0) default 1")
	private String intercompanyIDMultiCompanyTransaction;
	
	@Column(name="INTRID")
	private Double exchangeTableRate;
	
	@ManyToOne
	@JoinColumn(name="CURNCYID")
	private CurrencySetup currencySetup;
	
	@ManyToOne
	@JoinColumn(name="EXGTBLINXD")
	private CurrencyExchangeHeader currencyExchangeHeader;
	
	@Column(name="TOTVOUCH")
	private Double totalVoucherEntry;
	
	@Column(name="OTOTVOUCH")
	private Double originalTotalVoucherEntry;
	
	@Column(name="TOTVDBVOUCH")
	private Double totalVoucherDebit;
	
	@Column(name="OTOTDBVOUCH")
	private Double originalTotalVoucherDebit;
	
	@Column(name="TOTCRVOUCH")
	private Double totalVoucherCredit;
	
	@Column(name="OTOTCRVOUCH")
	private Double originalTotlalVoucherCredit;
	
	@Column(name="DISCRPTN")
	private String voucherDescription;
	
	@Column(name="DISCRPTNA")
	private String voucherDescriptionArabic;
	
	@Column(name="DEX_ROW_TS")
	private int rowDateIndex;
	
	@Column(name="XCHGRATE")
	private Double exchangeRate;
	
	@Column(name="TRXDT")
	private Date transactionDate;
	
	
	
	public GLYTDPaymentVoucherHeader() {
		// TODO Auto-generated constructor stub
	}
	
	public GLYTDPaymentVoucherHeader(DtoGLPaymentVoucherHeader dtoGLPaymentVoucherHeader) {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getTransactionNumber() {
		return TransactionNumber;
	}

	public void setTransactionNumber(Long transactionNumber) {
		TransactionNumber = transactionNumber;
	}

	public PaymentMethodType getPaymentMethodType() {
		return paymentMethodType;
	}

	public void setPaymentMethodType(PaymentMethodType paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public String getIntercompanyIDMultiCompanyTransaction() {
		return intercompanyIDMultiCompanyTransaction;
	}

	public void setIntercompanyIDMultiCompanyTransaction(String intercompanyIDMultiCompanyTransaction) {
		this.intercompanyIDMultiCompanyTransaction = intercompanyIDMultiCompanyTransaction;
	}

	public Double getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(Double exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public String getDistributionDescription() {
		return voucherDescription;
	}

	public void setDistributionDescription(String voucherDescription) {
		this.voucherDescription = voucherDescription;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(int rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public int getPaymentVoucherId() {
		return paymentVoucherId;
	}

	public void setPaymentVoucherId(int paymentVoucherID) {
		this.paymentVoucherId = paymentVoucherID;
	}

	public GLBatches getGlBatches() {
		return glBatches;
	}

	public void setGlBatches(GLBatches glBatches) {
		this.glBatches = glBatches;
	}

	public GLConfigurationAuditTrialCodes getGlConfigurationAuditTrialCodes() {
		return glConfigurationAuditTrialCodes;
	}

	public void setGlConfigurationAuditTrialCodes(GLConfigurationAuditTrialCodes glConfigurationAuditTrialCodes) {
		this.glConfigurationAuditTrialCodes = glConfigurationAuditTrialCodes;
	}

	public CurrencySetup getCurrencySetup() {
		return currencySetup;
	}

	public void setCurrencySetup(CurrencySetup currencySetup) {
		this.currencySetup = currencySetup;
	}

	public CurrencyExchangeHeader getCurrencyExchangeHeader() {
		return currencyExchangeHeader;
	}

	public void setCurrencyExchangeHeader(CurrencyExchangeHeader currencyExchangeHeader) {
		this.currencyExchangeHeader = currencyExchangeHeader;
	}

	public Double getTotalVoucherEntry() {
		return totalVoucherEntry;
	}

	public void setTotalVoucherEntry(Double totalVoucherEntry) {
		this.totalVoucherEntry = totalVoucherEntry;
	}

	public Double getTotalVoucherDebit() {
		return totalVoucherDebit;
	}

	public void setTotalVoucherDebit(Double totalVoucherDebit) {
		this.totalVoucherDebit = totalVoucherDebit;
	}

	public Double getTotalVoucherCredit() {
		return totalVoucherCredit;
	}

	public void setTotalVoucherCredit(Double totalVoucherCredit) {
		this.totalVoucherCredit = totalVoucherCredit;
	}

	public Double getOriginalTotalVoucherDebit() {
		return originalTotalVoucherDebit;
	}

	public void setOriginalTotalVoucherDebit(Double originalTotalVoucherDebit) {
		this.originalTotalVoucherDebit = originalTotalVoucherDebit;
	}

	public Double getOriginalTotlalVoucherCredit() {
		return originalTotlalVoucherCredit;
	}

	public void setOriginalTotlalVoucherCredit(Double originalTotlalVoucherCredit) {
		this.originalTotlalVoucherCredit = originalTotlalVoucherCredit;
	}

	public String getVoucherDescription() {
		return voucherDescription;
	}

	public void setVoucherDescription(String voucherDescription) {
		this.voucherDescription = voucherDescription;
	}

	public String getVoucherDescriptionArabic() {
		return voucherDescriptionArabic;
	}

	public void setVoucherDescriptionArabic(String voucherDescriptionArabic) {
		this.voucherDescriptionArabic = voucherDescriptionArabic;
	}


	public Double getExchangeRate() {
		return exchangeRate;
	}


	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public Double getOriginalTotalVoucherEntry() {
		return originalTotalVoucherEntry;
	}


	public void setOriginalTotalVoucherEntry(Double originalTotalVoucherEntry) {
		this.originalTotalVoucherEntry = originalTotalVoucherEntry;
	}


	public Date getTransactionDate() {
		return transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public List<GLYTDPaymentVoucherDetail> getGlytdPaymentVoucherDetails() {
		return glytdPaymentVoucherDetails;
	}

	public void setGlytdPaymentVoucherDetails(List<GLYTDPaymentVoucherDetail> glytdPaymentVoucherDetails) {
		this.glytdPaymentVoucherDetails = glytdPaymentVoucherDetails;
	}



}
