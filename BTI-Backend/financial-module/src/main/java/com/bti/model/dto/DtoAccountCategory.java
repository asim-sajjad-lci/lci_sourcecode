/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.GLConfigurationAccountCategories;

/**
 * Description: DtoAccountCategory class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 10, 2017
 * Modified on: AUG 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

public class DtoAccountCategory {

	private Integer accountCategoryId;
	private String accountCategoryDescription;
	private String accountCategoryDescriptionArabic;
	private Integer preDefined;
	private String categoryName;

	private Double totalTransaction;

	private List<DtoGLYTDOpenTransactions> listOfAccounts;
	
	
	public DtoAccountCategory() {
		this.totalTransaction = 0.0;
		this.accountCategoryId = 0;
	}

	public DtoAccountCategory(GLConfigurationAccountCategories accountCategory) {
		this.accountCategoryId = accountCategory.getAccountCategoryId();
		this.accountCategoryDescription = accountCategory.getAccountCategoryDescription();
		this.accountCategoryDescriptionArabic = accountCategory.getAccountCategoryDescriptionArabic();
		this.preDefined = accountCategory.getPreDefined();
		this.totalTransaction = 0.0;
	}

	public Integer getAccountCategoryId() {
		return accountCategoryId;
	}

	public void setAccountCategoryId(Integer accountCategoryId) {
		this.accountCategoryId = accountCategoryId;
	}

	public String getAccountCategoryDescription() {
		return accountCategoryDescription;
	}

	public void setAccountCategoryDescription(String accountCategoryDescription) {
		this.accountCategoryDescription = accountCategoryDescription;
	}

	public String getAccountCategoryDescriptionArabic() {
		return accountCategoryDescriptionArabic;
	}

	public void setAccountCategoryDescriptionArabic(String accountCategoryDescriptionArabic) {
		this.accountCategoryDescriptionArabic = accountCategoryDescriptionArabic;
	}

	public Integer getPreDefined() {
		return preDefined;
	}

	public void setPreDefined(Integer preDefined) {
		this.preDefined = preDefined;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<DtoGLYTDOpenTransactions> getListOfAccounts() {
		return listOfAccounts;
	}

	public void setListOfAccounts(List<DtoGLYTDOpenTransactions> listOfAccounts) {
		this.listOfAccounts = listOfAccounts;
	}

	public Double getTotalTransaction() {
		return totalTransaction;
	}

	public void setTotalTransaction(Double totalTransaction) {
		this.totalTransaction = totalTransaction;
	}
	
	
}
