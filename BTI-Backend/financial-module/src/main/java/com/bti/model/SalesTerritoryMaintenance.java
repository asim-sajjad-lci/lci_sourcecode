/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the ar00105 database table.
 * 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ar00105")
@NamedQuery(name="SalesTerritoryMaintenance.findAll", query="SELECT a FROM SalesTerritoryMaintenance a")
public class SalesTerritoryMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SALSTERRID")
	private String salesTerritoryId;

	@Column(name="CHANGEBY")
	private int changeBy;

	@Column(name="COSTSLSAMT")
	private Double totalCostSalesYTD;

	@Column(name="COSTSLSAMTL")
	private Double totalCostSalesLastYear;

	@Column(name="COUNTRY")
	private String country;

	@Column(name="CREATDBY")
	private int createdBy;

	@Column(name="CREATDDT")
	private Date createdDate;

	@Column(name="DEX_ROW_ID")
	private int rowIndexId;

	@Column(name="DEX_ROW_TS")
	private Date rowDateIndex;

	@Column(name="DSCRPTN")
	private String territoryDescription;
	
	@Column(name="PHONE1")
	private String phone1;
	@Column(name="PHONE2")
	private String phone2;

	@Column(name="DSCRPTNA")
	private String territoryDescriptionArabic;

	@Column(name="MFRSTNAME")
	private String managerFirstName;

	@Column(name="MLASTNAMEA")
	private String managerLastNameArabic;

	@Column(name="MLASTNAME")
	private String managerLastName;

	@Column(name="MFRSTNAMEA")
	private String managerFirstNameArabic;

	@Column(name="MMIDNAME")
	private String managerMidName;

	@Column(name="MMIDNAMEA")
	private String managerMidNameArabic;

	@Column(name="MODIFDT")
	private Date modifyDate;

	@Column(name="SLSCOMMAMT")
	private Double salesCommissionsYTD;

	@Column(name="SLSCOMMAMTL")
	private Double salesCommissionsLastYear;

	@Column(name="TOTLCOMMAMT")
	private Double totalCommissionsYTD;

	@Column(name="TOTLCOMMAMTL")
	private Double totalCommissionsLastYear;
	
	@Column(name="IS_DELETED" , columnDefinition = "tinyint(0) default 0")
	private boolean isDeleted;

	//bi-directional many-to-one association to Ar00102
	@OneToMany(mappedBy="salesTerritoryMaintenance")
	private List<CustomerMaintenanceOptions> customerMaintenanceOptions;

	//bi-directional many-to-one association to Ar00200
	@OneToMany(mappedBy="salesTerritoryMaintenance")
	private List<CustomerClassesSetup> customerClassesSetups;

	public SalesTerritoryMaintenance() {
	}

	public String getSalesTerritoryId() {
		return salesTerritoryId;
	}

	public void setSalesTerritoryId(String salesTerritoryId) {
		this.salesTerritoryId = salesTerritoryId;
	}

	public int getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(int changeBy) {
		this.changeBy = changeBy;
	}

	public Double getTotalCostSalesYTD() {
		return totalCostSalesYTD;
	}

	public void setTotalCostSalesYTD(Double totalCostSalesYTD) {
		this.totalCostSalesYTD = totalCostSalesYTD;
	}

	public Double getTotalCostSalesLastYear() {
		return totalCostSalesLastYear;
	}

	public void setTotalCostSalesLastYear(Double totalCostSalesLastYear) {
		this.totalCostSalesLastYear = totalCostSalesLastYear;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRowIndexId() {
		return rowIndexId;
	}

	public void setRowIndexId(int rowIndexId) {
		this.rowIndexId = rowIndexId;
	}

	public Date getRowDateIndex() {
		return rowDateIndex;
	}

	public void setRowDateIndex(Date rowDateIndex) {
		this.rowDateIndex = rowDateIndex;
	}

	public String getTerritoryDescription() {
		return territoryDescription;
	}

	public void setTerritoryDescription(String territoryDescription) {
		this.territoryDescription = territoryDescription;
	}

	public String getTerritoryDescriptionArabic() {
		return territoryDescriptionArabic;
	}

	public void setTerritoryDescriptionArabic(String territoryDescriptionArabic) {
		this.territoryDescriptionArabic = territoryDescriptionArabic;
	}

	public String getManagerFirstName() {
		return managerFirstName;
	}

	public void setManagerFirstName(String managerFirstName) {
		this.managerFirstName = managerFirstName;
	}

	public String getManagerLastNameArabic() {
		return managerLastNameArabic;
	}

	public void setManagerLastNameArabic(String managerLastNameArabic) {
		this.managerLastNameArabic = managerLastNameArabic;
	}

	public String getManagerLastName() {
		return managerLastName;
	}

	public void setManagerLastName(String managerLastName) {
		this.managerLastName = managerLastName;
	}

	public String getManagerFirstNameArabic() {
		return managerFirstNameArabic;
	}

	public void setManagerFirstNameArabic(String managerFirstNameArabic) {
		this.managerFirstNameArabic = managerFirstNameArabic;
	}

	public String getManagerMidName() {
		return managerMidName;
	}

	public void setManagerMidName(String managerMidName) {
		this.managerMidName = managerMidName;
	}

	public String getManagerMidNameArabic() {
		return managerMidNameArabic;
	}

	public void setManagerMidNameArabic(String managerMidNameArabic) {
		this.managerMidNameArabic = managerMidNameArabic;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Double getSalesCommissionsYTD() {
		return salesCommissionsYTD;
	}

	public void setSalesCommissionsYTD(Double salesCommissionsYTD) {
		this.salesCommissionsYTD = salesCommissionsYTD;
	}

	public Double getSalesCommissionsLastYear() {
		return salesCommissionsLastYear;
	}

	public void setSalesCommissionsLastYear(Double salesCommissionsLastYear) {
		this.salesCommissionsLastYear = salesCommissionsLastYear;
	}

	public Double getTotalCommissionsYTD() {
		return totalCommissionsYTD;
	}

	public void setTotalCommissionsYTD(Double totalCommissionsYTD) {
		this.totalCommissionsYTD = totalCommissionsYTD;
	}

	public Double getTotalCommissionsLastYear() {
		return totalCommissionsLastYear;
	}

	public void setTotalCommissionsLastYear(Double totalCommissionsLastYear) {
		this.totalCommissionsLastYear = totalCommissionsLastYear;
	}

	public List<CustomerMaintenanceOptions> getCustomerMaintenanceOptions() {
		return customerMaintenanceOptions;
	}

	public void setCustomerMaintenanceOptions(List<CustomerMaintenanceOptions> customerMaintenanceOptions) {
		this.customerMaintenanceOptions = customerMaintenanceOptions;
	}

	public List<CustomerClassesSetup> getCustomerClassesSetups() {
		return customerClassesSetups;
	}

	public void setCustomerClassesSetups(List<CustomerClassesSetup> customerClassesSetups) {
		this.customerClassesSetups = customerClassesSetups;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

	
	
}