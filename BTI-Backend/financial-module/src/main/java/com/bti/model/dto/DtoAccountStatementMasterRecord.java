/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
* Description: DtoAccountStatementMasterRecord class having getter and setter for fields (POJO) Name
* Name of Project: BTI
* Created on: AUG 10, 2017
* Modified on: AUG 10, 2017 4:19:38 PM
* @author SNS
* Version: 1.0
*/

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoAccountStatementMasterRecord {

	private String accountNumber;
	private String description;
	private String accountCategory;
	
	private double beginningBalance;
	private double totalDebitAmount;
	private double totalCreditAmount;
	private double totalBalance;

	private ArrayList<DtoAccountStatementDetailRecord> accountStatmentDetailRecordList;
	
	public DtoAccountStatementMasterRecord() {
	}
	
	public DtoAccountStatementMasterRecord(String accountNumber, String description, String accountCategory) {
		super();
		this.accountNumber = accountNumber;
		this.description = description;
		this.accountCategory = accountCategory;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAccountCategory() {
		return accountCategory;
	}
	public void setAccountCategory(String accountCategory) {
		this.accountCategory = accountCategory;
	}
	public double getBeginningBalance() {
		return beginningBalance;
	}
	public void setBeginningBalance(double beginningBalance) {
		this.beginningBalance = beginningBalance;
	}
	public double getTotalDebitAmount() {
		return totalDebitAmount;
	}
	public void setTotalDebitAmount(double totalDebitAmount) {
		this.totalDebitAmount = totalDebitAmount;
	}
	public double getTotalCreditAmount() {
		return totalCreditAmount;
	}
	public void setTotalCreditAmount(double totalCreditAmount) {
		this.totalCreditAmount = totalCreditAmount;
	}
	public double getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(double totalBalance) {
		this.totalBalance = totalBalance;
	}
	public ArrayList<DtoAccountStatementDetailRecord> getAccountStatmentDetailRecordList() {
		return accountStatmentDetailRecordList;
	}
	public void setAccountStatmentDetailRecordList(
			ArrayList<DtoAccountStatementDetailRecord> accountStatmentDetailRecordList) {
		this.accountStatmentDetailRecordList = accountStatmentDetailRecordList;
	}
	
	
	
	
}
