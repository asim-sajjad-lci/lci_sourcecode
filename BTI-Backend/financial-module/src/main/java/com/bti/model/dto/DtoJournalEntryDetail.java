/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;
import com.bti.model.GLClearingDetails;
import com.bti.model.GLYTDHistoryTransactions;
import com.bti.model.GLYTDOpenTransactions;
import com.bti.model.JournalEntryDetails;
import com.bti.util.UtilRandomKey;
import com.bti.util.UtilRoundDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DtoAccountCategory class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: AUG 10, 2017
 * Modified on: AUG 10, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoJournalEntryDetail {
	
    private int journalSequence;
	private String journalEntryHeaderId;
	private String accountTableRowIndex;
	private String accountTableRowIndexOffset;
	private int accountType;
	private int balanceType;
	private String intercompanyId;
	private String exchangeTableRate;
	private String debitAmount;
	private String creditAmount;
	private String distributionDescription;
	private Boolean isCredit;
	private String accountNumber;
	private String companyName;
	private String accountDescription;
	private String accountDescriptionArabic;
	private int sourceDocumentId;
	private String sourceDocument;

	public DtoJournalEntryDetail() {
		// TO DO Auto-generated constructor stub
	}
	
	public DtoJournalEntryDetail(JournalEntryDetails journalEntryDetails) 
	{
		this.journalSequence=journalEntryDetails.getJournalSequence();
		this.journalEntryHeaderId=journalEntryDetails.getJournalEntryHeader().getJournalID();
		this.accountTableRowIndex=journalEntryDetails.getAccountTableRowIndex();
		isCredit=false;
		if(journalEntryDetails.getBalanceType()==2){
			isCredit=true;
		}
		this.intercompanyId=String.valueOf(journalEntryDetails.getIntercompanyIDMultiCompanyTransaction());
		this.creditAmount=String.valueOf(UtilRoundDecimal.roundDecimalValue(journalEntryDetails.getOriginalCreditAmount()));
		this.debitAmount=String.valueOf(UtilRoundDecimal.roundDecimalValue(journalEntryDetails.getOriginalDebitAmount()));
		this.distributionDescription="";
		if(UtilRandomKey.isNotBlank(journalEntryDetails.getDistributionDescription())){
			this.distributionDescription=journalEntryDetails.getDistributionDescription();
		}
	}
	
	public DtoJournalEntryDetail(GLYTDOpenTransactions glytdOpenTransactions) {

		this.journalEntryHeaderId = glytdOpenTransactions.getJournalEntryID();
		if (UtilRandomKey.isNotBlank(glytdOpenTransactions.getAccountTableRowIndex())) {
			this.accountTableRowIndex = glytdOpenTransactions.getAccountTableRowIndex();
		}
		isCredit = false;
		if (glytdOpenTransactions.getBalanceType() == 2) {
			isCredit = true;
		}

		this.intercompanyId = String.valueOf(glytdOpenTransactions.getOriginalCompanyID());
		this.creditAmount = String.valueOf(UtilRoundDecimal.roundDecimalValue(glytdOpenTransactions.getOriginalCreditAmount()));
		this.debitAmount = String.valueOf(UtilRoundDecimal.roundDecimalValue(glytdOpenTransactions.getOriginalDebitAmount()));
		this.distributionDescription = "";
		if (UtilRandomKey.isNotBlank(glytdOpenTransactions.getDistributionDescription())) {
			this.distributionDescription = glytdOpenTransactions.getDistributionDescription();
		}
	}
	
    public DtoJournalEntryDetail(GLYTDHistoryTransactions glytdHistoryTransactions) {
		
		this.journalEntryHeaderId=glytdHistoryTransactions.getJournalEntryID();
		if(UtilRandomKey.isNotBlank(glytdHistoryTransactions.getAccountTableRowIndex())){
			this.accountTableRowIndex=glytdHistoryTransactions.getAccountTableRowIndex();
		}
		isCredit=false;
		if(glytdHistoryTransactions.getBalanceType()==2){
			isCredit=true;
		}
		this.intercompanyId=String.valueOf(glytdHistoryTransactions.getOriginalCompanyID());
		
		this.creditAmount="";
		if(glytdHistoryTransactions.getOriginalCreditAmount()!=null){
			this.creditAmount=String.valueOf(glytdHistoryTransactions.getOriginalCreditAmount());
		}
		this.debitAmount="";
		if(glytdHistoryTransactions.getOriginalDebitAmount()!=null){
			this.debitAmount=String.valueOf(glytdHistoryTransactions.getOriginalDebitAmount());
		}
		
		this.distributionDescription="";
		if(UtilRandomKey.isNotBlank(glytdHistoryTransactions.getDistributionDescription())){
			this.distributionDescription=glytdHistoryTransactions.getDistributionDescription();
		}
	}
    
    public DtoJournalEntryDetail(GLClearingDetails glClearingDetails) 
    {
		this.journalEntryHeaderId=glClearingDetails.getJournalID();
    	this.accountTableRowIndex="";
    	if(glClearingDetails.getGlAccountsTableAccumulation()!=null){
    		this.accountTableRowIndex=glClearingDetails.getGlAccountsTableAccumulation().getAccountTableRowIndex();
    	}
    	this.accountTableRowIndexOffset="";
    	if(glClearingDetails.getGlAccountsTableAccumulationOffset()!=null){
    		this.accountTableRowIndexOffset=glClearingDetails.getGlAccountsTableAccumulationOffset().getAccountTableRowIndex();
    	}
    	this.distributionDescription="";
    	if(UtilRandomKey.isNotBlank(glClearingDetails.getDistributionDescription())){
    		this.distributionDescription=glClearingDetails.getDistributionDescription();
    	}
    	
    }

	public int getJournalSequence() {
		return journalSequence;
	}

	public void setJournalSequence(int journalSequence) {
		this.journalSequence = journalSequence;
	}

	public String getJournalEntryHeaderId() {
		return journalEntryHeaderId;
	}

	public void setJournalEntryHeaderId(String journalEntryHeaderId) {
		this.journalEntryHeaderId = journalEntryHeaderId;
	}

	public String getAccountTableRowIndex() {
		return accountTableRowIndex;
	}

	public void setAccountTableRowIndex(String accountTableRowIndex) {
		this.accountTableRowIndex = accountTableRowIndex;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public int getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(int balanceType) {
		this.balanceType = balanceType;
	}

	public String getIntercompanyId() {
		return intercompanyId;
	}

	public void setIntercompanyId(String intercompanyId) {
		this.intercompanyId = intercompanyId;
	}

	public String getExchangeTableRate() {
		return exchangeTableRate;
	}

	public void setExchangeTableRate(String exchangeTableRate) {
		this.exchangeTableRate = exchangeTableRate;
	}

	public String getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}

	public String getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getDistributionDescription() {
		return distributionDescription;
	}

	public void setDistributionDescription(String distributionDescription) {
		this.distributionDescription = distributionDescription;
	}

	public Boolean getIsCredit() {
		return isCredit;
	}

	public void setIsCredit(Boolean isCredit) {
		this.isCredit = isCredit;
	}

	public String getAccountTableRowIndexOffset() {
		return accountTableRowIndexOffset;
	}

	public void setAccountTableRowIndexOffset(String accountTableRowIndexOffset) {
		this.accountTableRowIndexOffset = accountTableRowIndexOffset;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public String getAccountDescriptionArabic() {
		return accountDescriptionArabic;
	}

	public void setAccountDescriptionArabic(String accountDescriptionArabic) {
		this.accountDescriptionArabic = accountDescriptionArabic;
	}

	public int getSourceDocumentId() {
		return sourceDocumentId;
	}

	public void setSourceDocumentId(int sourceDocumentId) {
		this.sourceDocumentId = sourceDocumentId;
	}

	public String getSourceDocument() {
		return sourceDocument;
	}

	public void setSourceDocument(String sourceDocument) {
		this.sourceDocument = sourceDocument;
	}

	
	
	

}
