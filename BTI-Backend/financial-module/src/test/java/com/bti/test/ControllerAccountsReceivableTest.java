package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.bti.model.dto.DtoRMDocumentType;
import com.bti.model.dto.DtoRMPeriodSetup;
import com.jayway.restassured.response.Response;
import com.mysql.fabric.xmlrpc.base.Array;

import net.minidev.json.JSONObject;

public class ControllerAccountsReceivableTest {

	/**
	 * Save Customer maintenance data success
	 * @throws Exception
	 */
	@Test
	public void testSaveCustomerMaintenanceDataSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerId", "1");
		obj.put("name", "surjit");
		obj.put("arabicName", "surjit");
		obj.put("shortName", "surjit");
		obj.put("statementName", "maintenance");
		obj.put("phone1", "12345678");
		obj.put("phone2", "12345678");
		obj.put("phone3", "12345678");
		obj.put("address1", "mohali");
		obj.put("address2", "cvxcvxcvx");
		obj.put("address3", "vcxcvcx cxvfdv");
		obj.put("cityId", "1");
		obj.put("stateId", "1");
		obj.put("countryId", "1");
		obj.put("fax", "123456");
		obj.put("countryName", "India");
		obj.put("stateName", "Chandigarh");
		obj.put("cityName", "Chandigarh");
		obj.put("userDefine1", "sdsdfdsf");
		obj.put("userDefine2", "sdsdfdsf");
		obj.put("customerHold", "1");
		obj.put("activeCustomer", "1");
		obj.put("classId", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/maintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Customer maintenance data failure
	 * @throws Exception
	 */
	@Test
	public void testSaveCustomerMaintenanceDataFailure() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerId", "1");
		obj.put("name", "");
		obj.put("arabicName", "");
		obj.put("shortName", "");
		obj.put("statementName", "");
		obj.put("phone1", "");
		obj.put("phone2", "");
		obj.put("phone3", "");
		obj.put("address1", "");
		obj.put("address2", "");
		obj.put("address3", "");
		obj.put("cityId", "1");
		obj.put("stateId", "1");
		obj.put("countryId", "1");
		obj.put("fax", "");
		obj.put("countryName", "");
		obj.put("stateName", "");
		obj.put("cityName", "");
		obj.put("userDefine1", "");
		obj.put("userDefine2", "");
		obj.put("customerHold", "1");
		obj.put("activeCustomer", "1");
		obj.put("classId", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/maintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Customer maintenance data By Id
	 * @throws Exception
	 */
	@Test
	public void testGetCustomerMaintenanceDataSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerId", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/maintenanceGetById").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Update Customer maintenance data Success
	 * @throws Exception
	 */
	@Test
	public void testUpdateMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerId", "1");
		obj.put("name", "surjit");
		obj.put("arabicName", "surjit");
		obj.put("shortName", "surjit");
		obj.put("statementName", "maintenance");
		obj.put("phone1", "12345678");
		obj.put("phone2", "12345678");
		obj.put("phone3", "12345678");
		obj.put("address1", "mohali");
		obj.put("address2", "cvxcvxcvx");
		obj.put("address3", "vcxcvcx cxvfdv");
		obj.put("cityId", "1");
		obj.put("stateId", "1");
		obj.put("countryId", "1");
		obj.put("fax", "123456");
		obj.put("countryName", "India");
		obj.put("stateName", "Chandigarh");
		obj.put("cityName", "Chandigarh");
		obj.put("userDefine1", "sdsdfdsf");
		obj.put("userDefine2", "sdsdfdsf");
		obj.put("customerHold", "1");
		obj.put("activeCustomer", "1");
		obj.put("classId", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/updateMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all Customer maintenance data Success
	 * @throws Exception
	 */
	@Test
	public void testGetAllMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all with pagination Customer maintenance data Success
	 * @throws Exception
	 */
	@Test
	public void testGetAllWithPaginationMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get search results with pagination Customer maintenance data Success
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedWithPaginationMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "1");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/*@Test
	public void testGetMaintenanceActiveTypeStatusSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/getMaintenanceActiveTypeStatus").andReturn();
		assert response.statusCode() == 200;

	}*/
	
	/*@Test
	public void testGetMaintenanceHoldTypeStatusSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/getMaintenanceHoldTypeStatus").andReturn();
		assert response.statusCode() == 200;

	}*/
	
	/**
	 * Delete Customer maintenance data success
	 * @throws Exception
	 */
	@Test
	public void testDeleteMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerId", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/deleteMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Delete Customer maintenance data failure
	 * @throws Exception
	 */
	@Test
	public void testDeleteMaintenanceFailure() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerId", "112311");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/deleteMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save salesperson maintenance data
	 * @throws Exception
	 */
	@Test
	public void testSalesmanMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("salesmanId", "1");
		obj.put("address1", "address1");
		obj.put("address2", "address2");
		obj.put("applyPercentage", "10");
		obj.put("costOfSalesYTD", "10.10");
		obj.put("costOfSalesLastYear", "20.10");
		obj.put("employeeId", "1");
		obj.put("salesmanFirstName", "salesmanFirstName");
		obj.put("salesmanFirstNameArabic", "salesmanFirstNameArabic");
		obj.put("inactive", "1");
		obj.put("salesmanLastName", "salesmanLastName");
		obj.put("salesmanLastNameArabic", "LastNameArabic");
		obj.put("salesmanMidName", "salesmanMidName");
		obj.put("salesmanMidNameArabic", "salesmanMidNameArabic");
		obj.put("percentageAmount", "20.20");
		obj.put("phone1", "1234512345");
		obj.put("phone2", "5432154321");
		obj.put("salesTerritoryId", "1234");
		obj.put("salesCommissionsYTD", "12.12");
		obj.put("salesCommissionsLastYear", "13.13");
		obj.put("totalCommissionsYTD", "14.14");
		obj.put("totalCommissionsLastYear", "15.15");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/salesmanMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get salesperson maintenance data By Id
	 * @throws Exception
	 */
	@Test
	public void testSalesmanMaintenanceGetByIdSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("salesmanId", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/salesmanMaintenanceGetById").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Update salesperson maintenance data
	 * @throws Exception
	 */
	@Test
	public void testUpdateSalesmanMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("salesmanId", "1");
		obj.put("address1", "address1");
		obj.put("address2", "address2");
		obj.put("applyPercentage", "10");
		obj.put("costOfSalesYTD", "10.10");
		obj.put("costOfSalesLastYear", "20.10");
		obj.put("employeeId", "1");
		obj.put("salesmanFirstName", "salesmanFirstName");
		obj.put("salesmanFirstNameArabic", "salesmanFirstNameArabic");
		obj.put("inactive", "1");
		obj.put("salesmanLastName", "salesmanLastName");
		obj.put("salesmanLastNameArabic", "LastNameArabic");
		obj.put("salesmanMidName", "salesmanMidName");
		obj.put("salesmanMidNameArabic", "salesmanMidNameArabic");
		obj.put("percentageAmount", "20.20");
		obj.put("phone1", "1234512345");
		obj.put("phone2", "5432154321");
		obj.put("salesTerritoryId", "1234");
		obj.put("salesCommissionsYTD", "12.12");
		obj.put("salesCommissionsLastYear", "13.13");
		obj.put("totalCommissionsYTD", "14.14");
		obj.put("totalCommissionsLastYear", "15.15");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/updateSalesmanMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all salesperson maintenance data
	 * @throws Exception
	 */
	@Test
	public void testGetAllSalesmanMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchSalesmanMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all salesperson maintenance data with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllWithPaginationSalesmanMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageNumber", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchSalesmanMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get searched salesperson maintenance data with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetResultsWithPaginationSalesmanMaintenanceSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "1");
		obj.put("pageNumber", "1");
		obj.put("pageNumber", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchSalesmanMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Account Receivable Setup success
	 * @throws Exception
	 */
	@Test
	public void testSaveAccountRecievableSetupSuccess() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("ageingBy", "1");
		obj.put("applyByDefault", "1");
		obj.put("checkbookId", "1");
		obj.put("priceLevel", "20");
		obj.put("compoundFinanceCharge", "true");
		obj.put("arTrackingDiscountAvailable", "true");
		obj.put("deleteUnpostedPrintedDocuments", "true");
		obj.put("printHistoricalAgedTrialBalance", "true");
		obj.put("arPayCommissionsInvoicePay", "true");
		obj.put("creditLimitPassword", "123456");
		obj.put("customerHoldPassword", "123457");
		obj.put("waivedFinanceChargePassword", "123478");
		obj.put("writeoffPassword", "1427");
		obj.put("lastDateBalanceForwardAge", "30/12/2017");
		obj.put("deleteUnpostedPrintedDocumentDate", "30/12/2017");
		obj.put("lastDateStatementPrinted", "30/12/2017");
		obj.put("lastFinanceChargeDate", "30/12/2017");
		obj.put("miscVatScheduleId", "1");
		obj.put("salesVatScheduleId", "1");
		obj.put("freightVatScheduleId", "1");
		obj.put("userDefine1", "userDefine1");
		obj.put("userDefine2", "userDefine2");
		obj.put("userDefine3", "userDefine3");
		
		List<DtoRMPeriodSetup> dtoRMPeriodSetupsList = new ArrayList<DtoRMPeriodSetup>();
		DtoRMPeriodSetup dtoRMPeriodSetup = new DtoRMPeriodSetup();
		dtoRMPeriodSetup.setPeriodDescription("0-30 days");
		dtoRMPeriodSetup.setPeriodNoofDays(0);
		dtoRMPeriodSetup.setPeriodEnd(30);
		
		dtoRMPeriodSetupsList.add(dtoRMPeriodSetup);
		
		
		obj.put("dtoRMPeriodSetupsList", dtoRMPeriodSetupsList);
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/saveAccountRecievableSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Receivable Setup success
	 * @throws Exception
	 */
	@Test
	public void testGetAccountRecievableSetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/setup/accounts/getAccountRecievableSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Account Receivable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testSaveAccountRecievableClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerClassId", "233223321");
		obj.put("customerClassDescription", "Abc");
		obj.put("customeClassDescriptionArabic", "Test");
		obj.put("currencyId", "test-id");
		obj.put("customerPriority", "2");
		obj.put("balanceType", "1");
		obj.put("financeCharge", "0");
		obj.put("minimumCharge", "1");
		obj.put("creditLimit", "0");
		obj.put("openMaintenanceHistoryCalendarYear", "0");
		obj.put("openMaintenanceHistoryDistribution", "1");
		obj.put("openMaintenanceHistoryFiscalYear", "0");
		obj.put("openMaintenanceHistoryTransaction", "1");
		obj.put("priceLevelId", "123456");
		obj.put("userDefine1", "userDefine1");
		obj.put("userDefine2", "userDefine2");
		obj.put("userDefine3", "userDefine3");
		obj.put("vatId", "12");
		obj.put("shipmentMethodId", "123");
		obj.put("paymentTermId", "147");
		obj.put("salesmanId", "1478");
		obj.put("salesTerritoryId", "147895");
		obj.put("checkbookId", "114asv");
		obj.put("financeChargeAmount", "10.2");
		obj.put("minimumChargeAmount", "11.2");
		obj.put("tradeDiscountPercent", "10.3");
		obj.put("creditLimitAmount", "9.3");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/saveAccountRecievableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Update Account Receivable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateAccountRecievableClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerClassId", "233223321");
		obj.put("customerClassDescription", "Abc");
		obj.put("customeClassDescriptionArabic", "Test");
		obj.put("currencyId", "test-id");
		obj.put("customerPriority", "2");
		obj.put("balanceType", "1");
		obj.put("financeCharge", "0");
		obj.put("minimumCharge", "1");
		obj.put("creditLimit", "0");
		obj.put("openMaintenanceHistoryCalendarYear", "0");
		obj.put("openMaintenanceHistoryDistribution", "1");
		obj.put("openMaintenanceHistoryFiscalYear", "0");
		obj.put("openMaintenanceHistoryTransaction", "1");
		obj.put("priceLevelId", "123456");
		obj.put("userDefine1", "userDefine1");
		obj.put("userDefine2", "userDefine2");
		obj.put("userDefine3", "userDefine3");
		obj.put("vatId", "12");
		obj.put("shipmentMethodId", "123");
		obj.put("paymentTermId", "147");
		obj.put("salesmanId", "1478");
		obj.put("salesTerritoryId", "147895");
		obj.put("checkbookId", "114asv");
		obj.put("financeChargeAmount", "10.2");
		obj.put("minimumChargeAmount", "11.2");
		obj.put("tradeDiscountPercent", "10.3");
		obj.put("creditLimitAmount", "9.3");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/updateAccountRecievableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Receivable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAccountRecievableClassSetupByClassId() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("customerClassId", "233223321");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/getAccountRecievableClassSetupByClassId").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all Account Receivable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllAccountRecievableClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchAccountRecievableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all with pagination Account Receivable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllWithPaginationAccountRecievableClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchAccountRecievableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get searched results with pagination Account Receivable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedWithPaginationAccountRecievableClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "233");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/searchAccountRecievableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Account Receivable Setup Options
	 * @throws Exception
	 */
	@Test
	public void testSaveAccountRecievableSetupOptions() throws Exception {
		JSONObject obj = new JSONObject();
		
		List<DtoRMDocumentType> rmDocumentTypeList = new ArrayList<>();
		DtoRMDocumentType documentType = new DtoRMDocumentType();
		
		documentType.setDocType("1");
		documentType.setDocumentTypeDescription("Test");
		documentType.setDocumentTypeDescriptionArabic("test");
		documentType.setDocumentNumber(12);
		documentType.setDocumentCode("Te12");
		
		rmDocumentTypeList.add(documentType);
		
		obj.put("rmDocumentTypeList", rmDocumentTypeList);
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/saveAccountRecievableSetupOptions").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Receivable Setup Options
	 * @throws Exception
	 */
	@Test
	public void testGetRecievableSetupOptions() throws Exception {
		JSONObject obj = new JSONObject();
		
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/getRecievableSetupOptions").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Customer maintenance Options
	 * @throws Exception
	 */
	@Test
	public void testSaveCustomerMaintenanceOptions() throws Exception {
		JSONObject obj = new JSONObject();
		
		obj.put("customerNumberId", "123");
		obj.put("currencyId", "test-id");
		obj.put("customerPriority", "2");
		obj.put("balanceType", "1");
		obj.put("financeCharge", "0");
		obj.put("minimumCharge", "1");
		obj.put("creditLimit", "0");
		obj.put("openMaintenanceHistoryCalendarYear", "0");
		obj.put("openMaintenanceHistoryDistribution", "1");
		obj.put("openMaintenanceHistoryFiscalYear", "test-id");
		obj.put("currencyId", "0");
		obj.put("openMaintenanceHistoryTransaction", "1");
		obj.put("priceLevel", "123456");
		obj.put("vatId", "12");
		obj.put("shipmentMethodId", "123");
		obj.put("paymentTermId", "147");
		obj.put("salesmanId", "1478");
		obj.put("salesTerritoryId", "147895");
		obj.put("checkbookId", "114asv");
		obj.put("financeChargeAmount", "10.2");
		obj.put("minimumChargeAmount", "11.2");
		obj.put("tradeDiscountPercent", "10.3");
		obj.put("creditLimitAmount", "9.3");
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/saveCustomerMaintenanceOptions").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Customer maintenance Options
	 * @throws Exception
	 */
	@Test
	public void testGetCustomerMaintenanceOption() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/setup/accounts/getCustomerMaintenanceOption").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save customer account maintenance 
	 * @throws Exception
	 */
	@Test
	public void testSaveCustomerAccountMaintenance() throws Exception {
		JSONObject obj = new JSONObject();
		
		obj.put("customerId", "1");
		
		DtoPayableAccountClassSetup accountClassSetup = new DtoPayableAccountClassSetup();
		List<DtoPayableAccountClassSetup> accountNumberList = new ArrayList<>();
		
		accountClassSetup.setAccountType(1);
		accountClassSetup.setAccountDescription("Test123456");
		List<Long> integers = new ArrayList<>();
		integers.add(1L);
		integers.add(2L);
		integers.add(3L);
		accountClassSetup.setAccountNumberIndex(integers);

		accountNumberList.add(accountClassSetup);
		
		obj.put("accountNumberList", accountNumberList);
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/saveCustomerAccountMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get customer account maintenance 
	 * @throws Exception
	 */
	@Test
	public void testGetCustomerAccountMaintenance() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/setup/accounts/getCustomerAccountMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Account GL Setup
	 * @throws Exception
	 */
	@Test
	public void testAccountRecievableClassGlSetup() throws Exception {
		JSONObject obj = new JSONObject();
		
		obj.put("customerClassId", "1");
		
		DtoPayableAccountClassSetup accountClassSetup = new DtoPayableAccountClassSetup();
		List<DtoPayableAccountClassSetup> accountNumberList = new ArrayList<>();
		
		accountClassSetup.setAccountType(1);
		accountClassSetup.setAccountDescription("Test123456");
		List<Long> integers = new ArrayList<>();
		integers.add(1L);
		integers.add(2L);
		integers.add(3L);
		accountClassSetup.setAccountNumberIndex(integers);
		accountClassSetup.setTransactionType(0);
		
		accountNumberList.add(accountClassSetup);
		
		obj.put("accountNumberList", accountNumberList);
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/setup/accounts/accountRecievableClassGlSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account GL Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAccountRecievableClassGlSetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/setup/accounts/getAccountRecievableClassGlSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
}