package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.bti.model.dto.DtoAccountTableSetup;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import net.minidev.json.JSONObject;

@SpringBootTest
public class ControllerMasterDataFixedAssetsTest {

	/**
	 * 
	 * Add new book maintenance
	 * @throws Exception
	 * 
	 */
	
	public static String baseUrl ="http://localhost:8082/";
	
	@Test
	public void testCase0AddNewBookMaintenance() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "1234567");
		dtoVendor.put("amortizationCode", 1);
		dtoVendor.put("amortizationAmount", 10.10);//Double
		dtoVendor.put("averagingConvention", 2);
		dtoVendor.put("beginYearCost", 22.22);//Double
		dtoVendor.put("bookId", "12345");
		dtoVendor.put("costBasis", 10.12);//Double
		dtoVendor.put("currentDepreciation", 11.22);//Double
		dtoVendor.put("depreciatedDate", "21/01/2017");
		dtoVendor.put("fullDepreciationFlag", "flag");
		dtoVendor.put("lifeToDateDepreciation", 10.00);//Double
		dtoVendor.put("netFAValue", 2.22);//Double
		dtoVendor.put("originalLifeDay", 1);
		dtoVendor.put("originalLifeYear", 2);
		dtoVendor.put("placedInServiceDate", "21/01/2017");
		dtoVendor.put("remainingLifeDay", 2002);
		dtoVendor.put("remainingLifeYear",2003);
		dtoVendor.put("salvageAmount",22.55);//Double
		dtoVendor.put("switchOver", 5);
		dtoVendor.put("yearlyDepreciatedRate",77.88);//Double
		dtoVendor.put("yearToDateDepreciation", 87.55);//Double
		dtoVendor.put("depreciationMethodId", 1);
		dtoVendor.put("status", "ACTIVE");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/add").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * 
	 * Add new book maintenance
	 * @throws Exception
	 * Blank Asset id
	 */
	
	@Test
	public void testCase1AddNewBookMaintenance() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "");
		dtoVendor.put("amortizationCode", 1);
		dtoVendor.put("amortizationAmount", 10.10);//Double
		dtoVendor.put("averagingConvention", 2);
		dtoVendor.put("beginYearCost", 22.22);//Double
		dtoVendor.put("bookId", "12345");
		dtoVendor.put("costBasis", 10.12);//Double
		dtoVendor.put("currentDepreciation", 11.22);//Double
		dtoVendor.put("depreciatedDate", "21/01/2017");
		dtoVendor.put("fullDepreciationFlag", "flag");
		dtoVendor.put("lifeToDateDepreciation", 10.00);//Double
		dtoVendor.put("netFAValue", 2.22);//Double
		dtoVendor.put("originalLifeDay", 1);
		dtoVendor.put("originalLifeYear", 2);
		dtoVendor.put("placedInServiceDate", "21/01/2017");
		dtoVendor.put("remainingLifeDay", 2002);
		dtoVendor.put("remainingLifeYear",2003);
		dtoVendor.put("salvageAmount",22.55);//Double
		dtoVendor.put("switchOver", 5);
		dtoVendor.put("yearlyDepreciatedRate",77.88);//Double
		dtoVendor.put("yearToDateDepreciation", 87.55);//Double
		dtoVendor.put("depreciationMethodId", 1);
		dtoVendor.put("status", "ACTIVE");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/add").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * 
	 * Add new book maintenance
	 * @throws Exception
	 * Blank Depreciated Date
	 * 
	 */
	
	@Test
	public void testCase2AddNewBookMaintenance() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "17478");
		dtoVendor.put("amortizationCode", 1);
		dtoVendor.put("amortizationAmount", 10.10);//Double
		dtoVendor.put("averagingConvention", 2);
		dtoVendor.put("beginYearCost", 22.22);//Double
		dtoVendor.put("bookId", "12345");
		dtoVendor.put("costBasis", 10.12);//Double
		dtoVendor.put("currentDepreciation", 11.22);//Double
		dtoVendor.put("depreciatedDate", "");
		dtoVendor.put("fullDepreciationFlag", "flag");
		dtoVendor.put("lifeToDateDepreciation", 10.00);//Double
		dtoVendor.put("netFAValue", 2.22);//Double
		dtoVendor.put("originalLifeDay", 1);
		dtoVendor.put("originalLifeYear", 2);
		dtoVendor.put("placedInServiceDate", "21/01/2017");
		dtoVendor.put("remainingLifeDay", 2002);
		dtoVendor.put("remainingLifeYear",2003);
		dtoVendor.put("salvageAmount",22.55);//Double
		dtoVendor.put("switchOver", 5);
		dtoVendor.put("yearlyDepreciatedRate",77.88);//Double
		dtoVendor.put("yearToDateDepreciation", 87.55);//Double
		dtoVendor.put("depreciationMethodId", 1);
		dtoVendor.put("status", "ACTIVE");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/add").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * Add new book maintenance
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase3AddNewBookMaintenance() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "17478");
		dtoVendor.put("amortizationCode", 1);
		dtoVendor.put("amortizationAmount", 10.10);//Double
		dtoVendor.put("averagingConvention", 2);
		dtoVendor.put("beginYearCost", 22.22);//Double
		dtoVendor.put("bookId", "12345");
		dtoVendor.put("costBasis", 10.12);//Double
		dtoVendor.put("currentDepreciation", 11.22);//Double
		dtoVendor.put("depreciatedDate", "21/01/2017");
		dtoVendor.put("fullDepreciationFlag", "flag");
		dtoVendor.put("lifeToDateDepreciation", 10.00);//Double
		dtoVendor.put("netFAValue", 2.22);//Double
		dtoVendor.put("originalLifeDay", 1);
		dtoVendor.put("originalLifeYear", 2);
		dtoVendor.put("placedInServiceDate", "");
		dtoVendor.put("remainingLifeDay", 2002);
		dtoVendor.put("remainingLifeYear",2003);
		dtoVendor.put("salvageAmount",22.55);//Double
		dtoVendor.put("switchOver", 5);
		dtoVendor.put("yearlyDepreciatedRate",77.88);//Double
		dtoVendor.put("yearToDateDepreciation", 87.55);//Double
		dtoVendor.put("depreciationMethodId", 1);
		dtoVendor.put("status", "ACTIVE");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/add").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * 
	 * Update Book maintenance
	 * @throws Exception
	 * 
	 */
	
	@Test
	public void testCase0UpdateBookMaintenance() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "1234567");
		dtoVendor.put("amortizationCode", 1);
		dtoVendor.put("amortizationAmount", 10.10);//Double
		dtoVendor.put("averagingConvention", 2);
		dtoVendor.put("beginYearCost", 22.22);//Double
		dtoVendor.put("bookId", "12345");
		dtoVendor.put("costBasis", 10.12);//Double
		dtoVendor.put("currentDepreciation", 11.22);//Double
		dtoVendor.put("depreciatedDate", "21/01/2017");
		dtoVendor.put("fullDepreciationFlag", "flag");
		dtoVendor.put("lifeToDateDepreciation", 10.00);//Double
		dtoVendor.put("netFAValue", 2.22);//Double
		dtoVendor.put("originalLifeDay", 1);
		dtoVendor.put("originalLifeYear", 2);
		dtoVendor.put("placedInServiceDate", "21/01/2017");
		dtoVendor.put("remainingLifeDay", 2002);
		dtoVendor.put("remainingLifeYear",2003);
		dtoVendor.put("salvageAmount",22.55);//Double
		dtoVendor.put("switchOver", 5);
		dtoVendor.put("yearlyDepreciatedRate",77.88);//Double
		dtoVendor.put("yearToDateDepreciation", 87.55);//Double
		dtoVendor.put("depreciationMethodId", 1);
		dtoVendor.put("status", "ACTIVE");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/update").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * 
	 * Add new book maintenance
	 * @throws Exception
	 * Blank Asset id
	 */
	
	@Test
	public void testCase1UpdateBookMaintenance() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "");
		dtoVendor.put("amortizationCode", 1);
		dtoVendor.put("amortizationAmount", 10.10);//Double
		dtoVendor.put("averagingConvention", 2);
		dtoVendor.put("beginYearCost", 22.22);//Double
		dtoVendor.put("bookId", "12345");
		dtoVendor.put("costBasis", 10.12);//Double
		dtoVendor.put("currentDepreciation", 11.22);//Double
		dtoVendor.put("depreciatedDate", "21/01/2017");
		dtoVendor.put("fullDepreciationFlag", "flag");
		dtoVendor.put("lifeToDateDepreciation", 10.00);//Double
		dtoVendor.put("netFAValue", 2.22);//Double
		dtoVendor.put("originalLifeDay", 1);
		dtoVendor.put("originalLifeYear", 2);
		dtoVendor.put("placedInServiceDate", "21/01/2017");
		dtoVendor.put("remainingLifeDay", 2002);
		dtoVendor.put("remainingLifeYear",2003);
		dtoVendor.put("salvageAmount",22.55);//Double
		dtoVendor.put("switchOver", 5);
		dtoVendor.put("yearlyDepreciatedRate",77.88);//Double
		dtoVendor.put("yearToDateDepreciation", 87.55);//Double
		dtoVendor.put("depreciationMethodId", 1);
		dtoVendor.put("status", "ACTIVE");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/update").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * 
	 * Update book maintenance
	 * @throws Exception
	 * Blank Depreciated Date
	 * 
	 */
	
	@Test
	public void testCase2UpdateBookMaintenance() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "17478");
		dtoVendor.put("amortizationCode", 1);
		dtoVendor.put("amortizationAmount", 10.10);//Double
		dtoVendor.put("averagingConvention", 2);
		dtoVendor.put("beginYearCost", 22.22);//Double
		dtoVendor.put("bookId", "12345");
		dtoVendor.put("costBasis", 10.12);//Double
		dtoVendor.put("currentDepreciation", 11.22);//Double
		dtoVendor.put("depreciatedDate", "");
		dtoVendor.put("fullDepreciationFlag", "flag");
		dtoVendor.put("lifeToDateDepreciation", 10.00);//Double
		dtoVendor.put("netFAValue", 2.22);//Double
		dtoVendor.put("originalLifeDay", 1);
		dtoVendor.put("originalLifeYear", 2);
		dtoVendor.put("placedInServiceDate", "21/01/2017");
		dtoVendor.put("remainingLifeDay", 2002);
		dtoVendor.put("remainingLifeYear",2003);
		dtoVendor.put("salvageAmount",22.55);//Double
		dtoVendor.put("switchOver", 5);
		dtoVendor.put("yearlyDepreciatedRate",77.88);//Double
		dtoVendor.put("yearToDateDepreciation", 87.55);//Double
		dtoVendor.put("depreciationMethodId", 1);
		dtoVendor.put("status", "ACTIVE");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/update").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update book maintenance
	 * @throws Exception
	 * Blank Service Date
	 */
	
	@Test
	public void testCase3UpdateBookMaintenance() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "17478");
		dtoVendor.put("amortizationCode", 1);
		dtoVendor.put("amortizationAmount", 10.10);//Double
		dtoVendor.put("averagingConvention", 2);
		dtoVendor.put("beginYearCost", 22.22);//Double
		dtoVendor.put("bookId", "12345");
		dtoVendor.put("costBasis", 10.12);//Double
		dtoVendor.put("currentDepreciation", 11.22);//Double
		dtoVendor.put("depreciatedDate", "21/01/2017");
		dtoVendor.put("fullDepreciationFlag", "flag");
		dtoVendor.put("lifeToDateDepreciation", 10.00);//Double
		dtoVendor.put("netFAValue", 2.22);//Double
		dtoVendor.put("originalLifeDay", 1);
		dtoVendor.put("originalLifeYear", 2);
		dtoVendor.put("placedInServiceDate", "");
		dtoVendor.put("remainingLifeDay", 2002);
		dtoVendor.put("remainingLifeYear",2003);
		dtoVendor.put("salvageAmount",22.55);//Double
		dtoVendor.put("switchOver", 5);
		dtoVendor.put("yearlyDepreciatedRate",77.88);//Double
		dtoVendor.put("yearToDateDepreciation", 87.55);//Double
		dtoVendor.put("depreciationMethodId", 1);
		dtoVendor.put("status", "ACTIVE");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/update").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Book maintenance By Asset Id
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase0GetBookMaintenanceByAssetId() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "17478");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/getById").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Book maintenance By Asset Id
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase1GetBookMaintenanceByAssetId() throws Exception 
	{
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("assetId", "17478");
		
		String jsonString = dtoVendor.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "")
	            .header("userid", "")
				.body(jsonString).when()
				.post(baseUrl+"masterdata/fixed/assets/bookMaintenance/getById").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        
		assert response.statusCode() == 200;
	}
	
	/**
     * Search Book Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0SearchBookMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/bookMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Book Maintenance
     * @throws Exception
     */
    @Test
    public void testCase1SearchBookMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/bookMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Book Maintenance
     * @throws Exception
     */
    @Test
    public void testCase2SearchBookMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/bookMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Book Maintenance
     * @throws Exception
     */
    @Test
    public void testCase3SearchBookMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/bookMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
     
    /**
     * Search Book Maintenance
     * @throws Exception
     */
    @Test
    public void testCase4SearchBookMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/bookMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Book Maintenance
     * @throws Exception
     */
    @Test
    public void testCase5SearchBookMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/bookMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
	 * Get Average Convention Types
	 * @throws Exception
	 * Blank Service Date
	 */
    
	@Test
	public void testCase0GetAverageConventionTypes() throws Exception 
	{
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get(baseUrl+"masterdata/fixed/assets/bookMaintenance/averagingConventionType").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Average Convention Types
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase1GetAverageConventionTypes() throws Exception 
	{
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get(baseUrl+"masterdata/fixed/assets/bookMaintenance/averagingConventionType").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Switch Over Types
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase0GetSwitchOverTypes() throws Exception 
	{
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get(baseUrl+"masterdata/fixed/assets/bookMaintenance/switchOverType").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Switch Over Types
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase1GetSwitchOverTypes() throws Exception 
	{
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get(baseUrl+"masterdata/fixed/assets/bookMaintenance/switchOverType").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * Amortization Code Types
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase0GetAmortizationCodeTypes() throws Exception 
	{
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get(baseUrl+"masterdata/fixed/assets/bookMaintenance/amortizationCodeType").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Amortization Code Types
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase1AmortizationCodeTypes() throws Exception 
	{
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get(baseUrl+"masterdata/fixed/assets/bookMaintenance/amortizationCodeType").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
	 * Depreciation List
	 * @throws Exception
	 * Blank Service Date
	 */
	@Test
	public void testCase0DepreciationList() throws Exception 
	{
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get(baseUrl+"masterdata/fixed/assets/bookMaintenance/depreciationMethod").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        
		assert response.statusCode() == 200;
	}
	
	/**
	 * Depreciation List
	 * @throws Exception
	 * Blank Service Date
	 */
	
	@Test
	public void testCase1DepreciationList() throws Exception 
	{
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get(baseUrl+"masterdata/fixed/assets/bookMaintenance/depreciationMethod").andReturn();
		
		JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
		assert response.statusCode() == 200;
	}
	
	/**
     * Account Table Setup
     * @throws Exception
     */
    @Test
    public void testCase0AccountTableSetup() throws Exception 
    {
    	List<DtoAccountTableSetup> list = new ArrayList<DtoAccountTableSetup>(); 
    	DtoAccountTableSetup dtoAccountTableSetup = new DtoAccountTableSetup();
    	dtoAccountTableSetup.setAccountType(1);
    	dtoAccountTableSetup.setAccountDescription("TestDesc");
    	List<Long> index= new ArrayList<>();
    	index.add(1L);
    	index.add(2L);
    	index.add(3L);
    	dtoAccountTableSetup.setAccountNumberIndex(index);
    	list.add(dtoAccountTableSetup);
    	
    	JSONObject obj = new JSONObject();
    	obj.put("assetId", "123456");
    	obj.put("accountGroupIndex", 6);
    	obj.put("accountNumberList",list);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/accountTableSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Account Table Setup
     * @throws Exception
     * Blank Asset id
     */
    @Test
    public void testCase1AccountTableSetup() throws Exception 
    {
    	List<DtoAccountTableSetup> list = new ArrayList<DtoAccountTableSetup>(); 
    	DtoAccountTableSetup dtoAccountTableSetup = new DtoAccountTableSetup();
    	dtoAccountTableSetup.setAccountType(2);
    	dtoAccountTableSetup.setAccountDescription("TestDesc");
    	List<Long> index= new ArrayList<>();
    	index.add(1L);
    	index.add(2L);
    	index.add(3L);
    	dtoAccountTableSetup.setAccountNumberIndex(index);
    	list.add(dtoAccountTableSetup);
    	
    	JSONObject obj = new JSONObject();
    	obj.put("assetId", "");
    	obj.put("accountGroupIndex", 6);
    	obj.put("accountNumberList",list);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/accountTableSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Account Table Setup
     * @throws Exception
     * Account number list empty
     */
    @Test
    public void testCase2AccountTableSetup() throws Exception 
    {
    	List<DtoAccountTableSetup> list = new ArrayList<DtoAccountTableSetup>(); 
    	DtoAccountTableSetup dtoAccountTableSetup = new DtoAccountTableSetup();
    	dtoAccountTableSetup.setAccountType(2);
    	dtoAccountTableSetup.setAccountDescription("TestDesc");
    	List<Long> index= new ArrayList<>();
    	index.add(1L);
    	index.add(2L);
    	index.add(3L);
    	dtoAccountTableSetup.setAccountNumberIndex(index);
    	//list.add(dtoAccountTableSetup);
    	
    	JSONObject obj = new JSONObject();
    	obj.put("assetId", "123456");
    	obj.put("accountGroupIndex", 6);
    	obj.put("accountNumberList",list);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/accountTableSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get Account Table Setup
     * @throws Exception
     */
    @Test
    public void testCase0GetAccountTableSetup() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .get(baseUrl+"masterdata/fixed/assets/getAccountTableSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get Account Table Setup
     * @throws Exception
     * Blank header parameters
     */
    @Test
    public void testCase1GetAccountTableSetup() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .get(baseUrl+"masterdata/fixed/assets/getAccountTableSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get Master Account Types
     * @throws Exception
     */
    @Test
    public void testCase0GetMasterAccountTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .get(baseUrl+"masterdata/fixed/assets/accountTableSetup/accountType").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get Master Account Types
     * @throws Exception
     * Blank header parameters
     */
    @Test
    public void testCase1GetMasterAccountTypes() throws Exception 
    {
        Response response = given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .get(baseUrl+"masterdata/fixed/assets/accountTableSetup/accountType").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
         assert response.statusCode()==200;
        
    }
    
    /**
     * Create new FA Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0NewFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "1234");
    	obj.put("assetId", "12");
    	obj.put("insuranceYear", 2018); //Integer
    	obj.put("insuranceValue",2.2);//Double Value
    	obj.put("replacementCost", 2.2);//Double Value
    	obj.put("reproductionCost",2.2);//Double Value
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Create new FA Insurance Maintenance
     * @throws Exception
     * Blank Insurance Class id
     */
    @Test
    public void testCase1NewFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "");
    	obj.put("assetId", "12");
    	obj.put("insuranceYear", 2018); //Integer
    	obj.put("insuranceValue",2.2);//Double Value
    	obj.put("replacementCost", 2.2);//Double Value
    	obj.put("reproductionCost",2.2);//Double Value
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Create new FA Insurance Maintenance
     * @throws Exception
     * Blank asset id
     */
    @Test
    public void testCase2NewFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "1234");
    	obj.put("assetId", "");
    	obj.put("insuranceYear", 2018); //Integer
    	obj.put("insuranceValue",2.2);//Double Value
    	obj.put("replacementCost", 2.2);//Double Value
    	obj.put("reproductionCost",2.2);//Double Value
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Update FA Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0UpdateFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "1234");
    	obj.put("assetId", "12");
    	obj.put("insuranceYear", 2018); //Integer
    	obj.put("insuranceValue",2.2);//Double Value
    	obj.put("replacementCost", 2.2);//Double Value
    	obj.put("reproductionCost",2.2);//Double Value
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/update").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Update FA Insurance Maintenance
     * @throws Exception
     * Blank Insurance Class id
     */
    @Test
    public void testCase1UpdateFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "");
    	obj.put("assetId", "12");
    	obj.put("insuranceYear", 2018); //Integer
    	obj.put("insuranceValue",2.2);//Double Value
    	obj.put("replacementCost", 2.2);//Double Value
    	obj.put("reproductionCost",2.2);//Double Value
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/update").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Update FA Insurance Maintenance
     * @throws Exception
     * Blank asset id
     */
    @Test
    public void testCase2UpdateFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "1234");
    	obj.put("assetId", "");
    	obj.put("insuranceYear", 2018); //Integer
    	obj.put("insuranceValue",2.2);//Double Value
    	obj.put("replacementCost", 2.2);//Double Value
    	obj.put("reproductionCost",2.2);//Double Value
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/update").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get FA Insurance Maintenance By Id
     * @throws Exception
     */
    @Test
    public void testCase0GetFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "1234");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/getById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get FA Insurance Maintenance By Id
     * @throws Exception
     * Blank header parameters
     */
    @Test
    public void testCase1GetFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "1234");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/getById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get FA Insurance Maintenance By Id
     * @throws Exception
     * Blank insurance class id
     */
    @Test
    public void testCase2GetFaInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("insClassId", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/getById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0SearchInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase1SearchInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase2SearchInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase3SearchInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
     
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase4SearchInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase5SearchInsuranceMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/insuranceMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
        assert response.statusCode()==200;
    }
    
    /**
     * Create New Lease Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0NewLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("assetId","12");
    	obj.put("leaseCompanyIndex",0);
    	obj.put("assetSerialId",1);
    	obj.put("leaseTypeIndex",1);
    	obj.put("leaseContactNumber", "123456789");
    	obj.put("leasePayment", 1.2);
    	obj.put("interestRatePercent", 0.5);
    	obj.put("leaseEndDate", "12/02/2018");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance").
                andReturn();
        /* JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());*/
        assert response.statusCode()==200;
    }
    
    /**
     * Update Lease Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0UpdateLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("leaseMaintenanceIndex",1);
    	obj.put("assetId","12");
    	obj.put("leaseCompanyIndex",0);
    	obj.put("assetSerialId",1);
    	obj.put("leaseTypeIndex",1);
    	obj.put("leaseContactNumber", "123456789");
    	obj.put("leasePayment", 1.2);
    	obj.put("interestRatePercent", 0.5);
    	obj.put("leaseEndDate", "12/02/2018");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/update").
                andReturn();
        /* JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());*/
        assert response.statusCode()==200;
    }
    
    /**
     * Get Lease Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0GetLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("leaseMaintenanceIndex",1);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/getById").
                andReturn();
        /* JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());*/
        assert response.statusCode()==200;
    }
    
    /**
     * Get Lease Maintenance
     * @throws Exception
     * Blank Header
     */
    @Test
    public void testCase1GetLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("leaseMaintenanceIndex",1);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/getById").
                andReturn();
        /* JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());*/
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0SearchLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase1SearchLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase2SearchLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase3SearchLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
     
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase4SearchLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Insurance Maintenance
     * @throws Exception
     */
    @Test
    public void testCase5SearchLeaseMaintenance() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"masterdata/fixed/assets/leaseMaintenance/search").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get Lease Maintenance
     * @throws Exception
     */
    @Test
    public void testCase0GetLeaseType() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .get(baseUrl+"masterdata/fixed/assets/leaseType").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get Lease Maintenance
     * @throws Exception
     * Blank Header
     */
    @Test
    public void testCase1GetLeaseType() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .get(baseUrl+"masterdata/fixed/assets/leaseType").
                andReturn();
        
        assert response.statusCode()==200;
    }
	
}
