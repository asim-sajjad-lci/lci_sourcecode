package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Test;

import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerCompanySetupTest {

	/**
	 * Configure module in company setup
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterAddVendorRequired() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("seriesNamePrimary", "Financial");
		obj.put("seriesNameSecondary", "Financial in arabic");
		obj.put("originNamePrimary", "General Entry");
		obj.put("originNameSecondary", "General Entry in arabic");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/configure/module").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Payment Term Setup
	 * @throws Exception
	 */
	@Test
	public void testPaymentTermSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("paymentTermId", "123456");
		obj.put("description", "");
		obj.put("arabicDescription", "");
		obj.put("discountPeriod", "1");
		obj.put("discountPeriodDays", "12");
		obj.put("discountType", "1");
		obj.put("discountTypeValue", "10.12");
		obj.put("dueType", "1");
		obj.put("dueDays", "12");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/paymentTermSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Bank Setup
	 * @throws Exception
	 */
	@Test
	public void testBankSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bankId", "Bank1");
		obj.put("bankDescription", "bankDescription");
		obj.put("bankArabicDescription", "bankArabicDescription");
		obj.put("address1", "address1");
		obj.put("address2", "address2");
		obj.put("address3", "address3");
		obj.put("phone1", "phone1");
		obj.put("phone2", "phone2");
		obj.put("phone3", "phone3");
		obj.put("cityId", "1");
		obj.put("stateId", "1");
		obj.put("countryId", "1");
		obj.put("fax", "2423432432");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/bankSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Payment Term Setup Detail By Id
	 * @throws Exception
	 */
	@Test
	public void testPaymentTermSetupGetById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("paymentTermId", "1234567");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/paymentTermSetupGetById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Bank Setup Detail By Id
	 * @throws Exception
	 */
	@Test
	public void testBankSetupGetById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bankId", "Bank1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/bankSetupGetById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Configure module get all modules in company setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllModuleConfiguration() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/get/configure/module").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Configure module get all modules in company setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllWithPaginationModuleConfiguration() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/get/configure/module").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Configure module get searched results modules in company setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testSearchResultsWithPaginationModuleConfiguration() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "finance");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/get/configure/module").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Payment Term Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdatePaymentTermSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("paymentTermId", "123456");
		obj.put("description", "");
		obj.put("arabicDescription", "");
		obj.put("discountPeriod", "1");
		obj.put("discountPeriodDays", "12");
		obj.put("discountType", "1");
		obj.put("discountTypeValue", "10.12");
		obj.put("dueType", "1");
		obj.put("dueDays", "12");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/updatePaymentTermSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Payment Term Setup 
	 * @throws Exception
	 */
	@Test
	public void testGetAllPaymentTermSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchPaymentTermSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Payment Term Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllPaymentTermSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("searchKeyword", "3");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchPaymentTermSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched Payment Term Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedPaymentTermSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "12345");
		obj.put("pageNumber", "0");
		obj.put("searchKeyword", "3");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchPaymentTermSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Bank Setup
	 * @throws Exception
	 */
	@Test
	public void testBankSetupUpdate() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bankId", "Bank1");
		obj.put("bankDescription", "bankDescription");
		obj.put("bankArabicDescription", "bankArabicDescription");
		obj.put("address1", "address1");
		obj.put("address2", "address2");
		obj.put("address3", "address3");
		obj.put("phone1", "phone1");
		obj.put("phone2", "phone2");
		obj.put("phone3", "phone3");
		obj.put("cityId", "1");
		obj.put("stateId", "1");
		obj.put("countryId", "1");
		obj.put("fax", "2423432432");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/bankSetupUpdate").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get configure module by series id
	 * @throws Exception
	 */
	@Test
	public void testGetModuleConfigurationById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("seriesId", "1");

		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/getOne/configure/module").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Bank Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllBankSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchBankSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Bank Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllBankSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("searchKeyword", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchBankSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get search results for Bank Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedBankSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "test-id-2");
		obj.put("pageNumber", "0");
		obj.put("searchKeyword", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchBankSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Credit Card Setup
	 * @throws Exception
	 */
	@Test
	public void testSaveCreditCardSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("checkBookId", "123");
		obj.put("creditCardNameArabic", "creditCardNameArabic");
		obj.put("creditCardName", "creditCardName");
		obj.put("cardType", "1");
		obj.put("accountTableRowIndex", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/saveCreditCardSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Credit Card Setup by Id
	 * @throws Exception
	 */
	@Test
	public void testGetCreditCardSetupById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("cardId", "CC-1011");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/getCreditCardSetupById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Credit Card Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateCreditCardSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("checkBookId", "123");
		obj.put("creditCardNameArabic", "creditCardNameArabic");
		obj.put("creditCardName", "creditCardName");
		obj.put("cardType", "1");
		obj.put("accountTableRowIndex", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/updateCreditCardSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Credit Card Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllCreditCardSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchCreditCardSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Credit Card Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllCreditCardSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchCreditCardSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched Credit Card Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedCreditCardSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "CC-1000");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchCreditCardSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Shipment Method Setup
	 * @throws Exception
	 */
	@Test
	public void testShipmentMethodSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("shipmentMethodId", "1000");
		obj.put("shipmentDescription", "shipmentDescription");
		obj.put("shipmentDescriptionArabic", "shipmentDescriptionArabic");
		obj.put("shipmentCarrierAccount", "shipmentCarrierAccount");
		obj.put("shipmentCarrier", "shipmentCarrier");
		obj.put("shipmentCarrierArabic", "shipmentCarrierArabic");
		obj.put("shipmentCarrierContactName", "shipmentCarrierContactName");
		obj.put("shipmentPhoneNumber", "1234512345");
		obj.put("shipmentMethodType", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/shipmentMethodSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Shipment Method Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateShipmentMethodSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("shipmentMethodId", "1000");
		obj.put("shipmentDescription", "shipmentDescription");
		obj.put("shipmentDescriptionArabic", "shipmentDescriptionArabic");
		obj.put("shipmentCarrierAccount", "shipmentCarrierAccount");
		obj.put("shipmentCarrier", "shipmentCarrier");
		obj.put("shipmentCarrierArabic", "shipmentCarrierArabic");
		obj.put("shipmentCarrierContactName", "shipmentCarrierContactName");
		obj.put("shipmentPhoneNumber", "1234512345");
		obj.put("shipmentMethodType", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/updateShipmentMethodSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Shipment Method Setup Detail by Id
	 * @throws Exception
	 */
	@Test
	public void testGetShipmentMethodSetupById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("shipmentMethodId", "1000");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/getShipmentMethodSetupById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Shipment Method Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllShipmentMethodSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchShipmentMethodSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Shipment Method Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllShipmentMethodSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchShipmentMethodSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched Shipment Method Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedShipmentMethodSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "1000");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchShipmentMethodSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Shipping Method Type
	 * @throws Exception
	 */
	@Test
	public void testGetShipmentMethodTypeStatus() throws Exception {
		JSONObject obj = new JSONObject();
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/getShipmentMethodTypeStatus").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Credit Card Type
	 * @throws Exception
	 */
	@Test
	public void testGetCreditCardTypeStatus() throws Exception {
		JSONObject obj = new JSONObject();
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/getCreditCardTypeStatus").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Vat detail Setup
	 * @throws Exception
	 */
	@Test
	public void testVatDetailSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("vatScheduleId", "4");
		obj.put("vatDescription", "VATDescription");
		obj.put("vatDescriptionArabic", "vatDescriptionArabic");
		obj.put("vatSeriesType", "2");
		obj.put("vatIdNumber", "123");
		obj.put("vatBaseOn", "1");
		obj.put("basperct", "10.10");
		obj.put("maximumVATAmount", "2000.50");
		obj.put("minimumVATAmount", "1000.50");
		obj.put("accountRowId", "123456");
		obj.put("lastYearTotalSalesPurchase", "1234");
		obj.put("lastYearSalesPurchaseTaxes", "23456");
		obj.put("lastYearTaxableSalesPurchase", "1234");
		obj.put("ytdTotalSalesPurchase", "123.12");
		obj.put("ytdTotalSalesPurchaseTaxes", "123.12");
		obj.put("ytdTotalTaxableSalesPurchase", "123.12");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/vatDetailSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Vat detail Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateVatDetailSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("vatScheduleId", "4");
		obj.put("vatDescription", "VATDescription");
		obj.put("vatDescriptionArabic", "vatDescriptionArabic");
		obj.put("vatSeriesType", "2");
		obj.put("vatIdNumber", "123");
		obj.put("vatBaseOn", "1");
		obj.put("basperct", "10.10");
		obj.put("maximumVATAmount", "2000.50");
		obj.put("minimumVATAmount", "1000.50");
		obj.put("accountRowId", "123456");
		obj.put("lastYearTotalSalesPurchase", "1234");
		obj.put("lastYearSalesPurchaseTaxes", "23456");
		obj.put("lastYearTaxableSalesPurchase", "1234");
		obj.put("ytdTotalSalesPurchase", "123.12");
		obj.put("ytdTotalSalesPurchaseTaxes", "123.12");
		obj.put("ytdTotalTaxableSalesPurchase", "123.12");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/updateVatDetailSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Vat Setup Detail by Id
	 * @throws Exception
	 */
	@Test
	public void testGetVatDetailSetupById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("vatScheduleId", "4");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/getVatDetailSetupById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Vat Detail Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllVatDetailSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchVatDetailSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Vat Detail Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllVatDetailSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchVatDetailSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched Vat Detail Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedVatDetailSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "4");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/searchVatDetailSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Vat Series Type
	 * @throws Exception
	 */
	@Test
	public void testGetVatSeriesTypeStatus() throws Exception {
		JSONObject obj = new JSONObject();
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/getVatSeriesTypeStatus").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Vat Based On Type
	 * @throws Exception
	 */
	@Test
	public void testGetVatBasedOnTypeStatus() throws Exception {
		JSONObject obj = new JSONObject();
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/company/getVatBasedOnTypeStatus").andReturn();
		assert response.statusCode() == 200;
	}
}
