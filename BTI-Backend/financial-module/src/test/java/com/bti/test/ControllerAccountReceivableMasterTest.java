package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.bti.model.NationalAccountMaintenanceDetails;
import com.bti.model.dto.DtoNationalAccountMaintenanceDetail;
import com.bti.model.dto.DtoNationalAccountMaintenanceHeader;
import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerAccountReceivableMasterTest {

	/**
	 * Save new sales territory setup
	 * @throws Exception
	 */
	@Test
	public void testSalesTerritorySetup() throws Exception {
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("salesTerritoryId", "TE001");
		dtoSalesTerritorySetup.put("descriptionPrimary", "East Riyadh en");
		dtoSalesTerritorySetup.put("descriptionSecondary", "East Riyadh ar");
		dtoSalesTerritorySetup.put("phone1", "0124741523");
		dtoSalesTerritorySetup.put("phone2", "0021475487");
		dtoSalesTerritorySetup.put("managerFirstNamePrimary", "Omar");
		dtoSalesTerritorySetup.put("managerMidNamePrimary", "Mohammed");
		dtoSalesTerritorySetup.put("managerLastNamePrimary", "Asif");
		dtoSalesTerritorySetup.put("managerFirstNameSecondary", "Omar");
		dtoSalesTerritorySetup.put("managerMidNameSecondary", "Mohammed");
		dtoSalesTerritorySetup.put("managerLastNameSecondary", "Asif");
		dtoSalesTerritorySetup.put("totalCommissionsYTD", "1.2");
		dtoSalesTerritorySetup.put("totalCommissionsLY", "1.3");
		dtoSalesTerritorySetup.put("commissionsSalesYTD", "1.4");
		dtoSalesTerritorySetup.put("commissionsSalesLY", "1.5");
		dtoSalesTerritorySetup.put("costOfSalesYTD", "1.6");
		dtoSalesTerritorySetup.put("costOfSalesLY", "1.7");
		
		String jsonString = dtoSalesTerritorySetup.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/salesTerritorySetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update sales territory setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateSalesTerritorySetup() throws Exception {
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("salesTerritoryId", "TE001");
		dtoSalesTerritorySetup.put("descriptionPrimary", "East Riyadh en");
		dtoSalesTerritorySetup.put("descriptionSecondary", "East Riyadh ar");
		dtoSalesTerritorySetup.put("phone1", "0124741523");
		dtoSalesTerritorySetup.put("phone2", "0021475487");
		dtoSalesTerritorySetup.put("managerFirstNamePrimary", "Omar");
		dtoSalesTerritorySetup.put("managerMidNamePrimary", "Mohammed");
		dtoSalesTerritorySetup.put("managerLastNamePrimary", "Asif");
		dtoSalesTerritorySetup.put("managerFirstNameSecondary", "Omar");
		dtoSalesTerritorySetup.put("managerMidNameSecondary", "Mohammed");
		dtoSalesTerritorySetup.put("managerLastNameSecondary", "Asif");
		dtoSalesTerritorySetup.put("totalCommissionsYTD", "1.2");
		dtoSalesTerritorySetup.put("totalCommissionsLY", "1.3");
		dtoSalesTerritorySetup.put("commissionsSalesYTD", "1.4");
		dtoSalesTerritorySetup.put("commissionsSalesLY", "1.5");
		dtoSalesTerritorySetup.put("costOfSalesYTD", "1.6");
		dtoSalesTerritorySetup.put("costOfSalesLY", "1.7");
		
		String jsonString = dtoSalesTerritorySetup.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/salesTerritorySetup/update").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get sales territory setup
	 * @throws Exception
	 */
	@Test
	public void testGetSalesTerritorySetup() throws Exception {
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("salesTerritoryId", "TE001");

		String jsonString = dtoSalesTerritorySetup.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/salesTerritorySetup/getById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all sales territory setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllSalesTerritorySetup() throws Exception {
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("searchKeyword", "");

		String jsonString = dtoSalesTerritorySetup.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/salesTerritorySetup/search").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all sales territory setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllSalesTerritorySetupWithPagination() throws Exception 
	{
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("searchKeyword", "");
		dtoSalesTerritorySetup.put("pageNumber", "1");
		dtoSalesTerritorySetup.put("pageSize", "1");

		String jsonString = dtoSalesTerritorySetup.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/salesTerritorySetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched sales territory setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedSalesTerritorySetupWithPagination() throws Exception 
	{
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("searchKeyword", "finance");
		dtoSalesTerritorySetup.put("pageNumber", "1");
		dtoSalesTerritorySetup.put("pageSize", "1");

		String jsonString = dtoSalesTerritorySetup.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/salesTerritorySetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Create national account
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceSuccess() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		List<DtoNationalAccountMaintenanceDetail> dtoNationalAccountMaintenanceDetails = new ArrayList<>();
		DtoNationalAccountMaintenanceDetail dtoNationalAccountMaintenanceDetail = new DtoNationalAccountMaintenanceDetail();
		
		dtoAccountMaintenanceHeader.put("custNumber", "P101");
		dtoAccountMaintenanceHeader.put("custNamePrimary", "Nasir en");
		dtoAccountMaintenanceHeader.put("custNameSecondary", "Nasir ar");
		dtoAccountMaintenanceHeader.put("allowReceiptEntry", "1");
		dtoAccountMaintenanceHeader.put("baseCreditCheck", "1");
		dtoAccountMaintenanceHeader.put("applyStatus", "1");
		dtoAccountMaintenanceHeader.put("baseFinanceCharge", "1");
		
		dtoNationalAccountMaintenanceDetail.setChildCustId("CH101");
		dtoNationalAccountMaintenanceDetail.setChildCustNamePrimary("Child1 en");
		dtoNationalAccountMaintenanceDetail.setChilsCustNameSecondary("Child1 ar");
		dtoNationalAccountMaintenanceDetail.setCustChildBalance(1.20);
		
		dtoNationalAccountMaintenanceDetails.add(dtoNationalAccountMaintenanceDetail);
		
		dtoAccountMaintenanceHeader.put("accountMaintenanceDetail", dtoNationalAccountMaintenanceDetails);

		String jsonString = dtoAccountMaintenanceHeader.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Create national account
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceFailure() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		List<DtoNationalAccountMaintenanceDetail> dtoNationalAccountMaintenanceDetails = new ArrayList<>();
		DtoNationalAccountMaintenanceDetail dtoNationalAccountMaintenanceDetail = new DtoNationalAccountMaintenanceDetail();
		
		dtoAccountMaintenanceHeader.put("custNumber", "P101");
		dtoAccountMaintenanceHeader.put("custNamePrimary", "");
		dtoAccountMaintenanceHeader.put("custNameSecondary", "");
		dtoAccountMaintenanceHeader.put("allowReceiptEntry", "");
		dtoAccountMaintenanceHeader.put("baseCreditCheck", "");
		dtoAccountMaintenanceHeader.put("applyStatus", "");
		dtoAccountMaintenanceHeader.put("baseFinanceCharge", "");
		
		dtoNationalAccountMaintenanceDetail.setChildCustId("");
		dtoNationalAccountMaintenanceDetail.setChildCustNamePrimary("");
		dtoNationalAccountMaintenanceDetail.setChilsCustNameSecondary("");
		dtoNationalAccountMaintenanceDetail.setCustChildBalance(0);
		
		dtoNationalAccountMaintenanceDetails.add(dtoNationalAccountMaintenanceDetail);
		
		dtoAccountMaintenanceHeader.put("accountMaintenanceDetail", dtoNationalAccountMaintenanceDetails);

		String jsonString = dtoAccountMaintenanceHeader.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Update national account
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceUpdateSuccess() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		List<DtoNationalAccountMaintenanceDetail> dtoNationalAccountMaintenanceDetails = new ArrayList<>();
		DtoNationalAccountMaintenanceDetail dtoNationalAccountMaintenanceDetail = new DtoNationalAccountMaintenanceDetail();
		
		dtoAccountMaintenanceHeader.put("custNumber", "P101");
		dtoAccountMaintenanceHeader.put("custNamePrimary", "Nasir en");
		dtoAccountMaintenanceHeader.put("custNameSecondary", "Nasir ar");
		dtoAccountMaintenanceHeader.put("allowReceiptEntry", "1");
		dtoAccountMaintenanceHeader.put("baseCreditCheck", "1");
		dtoAccountMaintenanceHeader.put("applyStatus", "1");
		dtoAccountMaintenanceHeader.put("baseFinanceCharge", "1");
		
		dtoNationalAccountMaintenanceDetail.setChildCustId("CH101");
		dtoNationalAccountMaintenanceDetail.setChildCustNamePrimary("Child1 en");
		dtoNationalAccountMaintenanceDetail.setChilsCustNameSecondary("Child1 ar");
		dtoNationalAccountMaintenanceDetail.setCustChildBalance(1.20);
		
		dtoNationalAccountMaintenanceDetails.add(dtoNationalAccountMaintenanceDetail);
		
		dtoAccountMaintenanceHeader.put("accountMaintenanceDetail", dtoNationalAccountMaintenanceDetails);

		String jsonString = dtoAccountMaintenanceHeader.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance/update").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Update national account
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceUpdateFailure() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		List<DtoNationalAccountMaintenanceDetail> dtoNationalAccountMaintenanceDetails = new ArrayList<>();
		DtoNationalAccountMaintenanceDetail dtoNationalAccountMaintenanceDetail = new DtoNationalAccountMaintenanceDetail();
		
		dtoAccountMaintenanceHeader.put("custNumber", "P10111");
		dtoAccountMaintenanceHeader.put("custNamePrimary", "Nasir en");
		dtoAccountMaintenanceHeader.put("custNameSecondary", "Nasir ar");
		dtoAccountMaintenanceHeader.put("allowReceiptEntry", "1");
		dtoAccountMaintenanceHeader.put("baseCreditCheck", "1");
		dtoAccountMaintenanceHeader.put("applyStatus", "1");
		dtoAccountMaintenanceHeader.put("baseFinanceCharge", "1");
		
		dtoNationalAccountMaintenanceDetail.setChildCustId("CH101");
		dtoNationalAccountMaintenanceDetail.setChildCustNamePrimary("Child1 en");
		dtoNationalAccountMaintenanceDetail.setChilsCustNameSecondary("Child1 ar");
		dtoNationalAccountMaintenanceDetail.setCustChildBalance(1.20);
		
		dtoNationalAccountMaintenanceDetails.add(dtoNationalAccountMaintenanceDetail);
		
		dtoAccountMaintenanceHeader.put("accountMaintenanceDetail", dtoNationalAccountMaintenanceDetails);

		String jsonString = dtoAccountMaintenanceHeader.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance/update").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get national account by id
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceGetSuccess() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		dtoAccountMaintenanceHeader.put("custNumber", "P101");

		String jsonString = dtoAccountMaintenanceHeader.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance/getById").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get national account by id
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceGetFailure() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		dtoAccountMaintenanceHeader.put("custNumber", "");

		String jsonString = dtoAccountMaintenanceHeader.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance/getById").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all national account
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceGetAll() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		dtoAccountMaintenanceHeader.put("searchKeyword", "");

		String jsonString = dtoAccountMaintenanceHeader.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance/search").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get all national account with pagination
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceGetAllWithPagination() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		dtoAccountMaintenanceHeader.put("searchKeyword", "");
		dtoAccountMaintenanceHeader.put("pageNumber", "1");
		dtoAccountMaintenanceHeader.put("pageSize", "1");
		String jsonString = dtoAccountMaintenanceHeader.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all national account with pagination and search keyword
	 * @throws Exception
	 */
	@Test
	public void testNationalAccountMaintenanceSearchWithPagination() throws Exception {
		JSONObject dtoAccountMaintenanceHeader = new JSONObject();
		dtoAccountMaintenanceHeader.put("searchKeyword", "finance");
		dtoAccountMaintenanceHeader.put("pageNumber", "1");
		dtoAccountMaintenanceHeader.put("pageSize", "1");
		String jsonString = dtoAccountMaintenanceHeader.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/receivable/nationalAccountMaintenance/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	
}

