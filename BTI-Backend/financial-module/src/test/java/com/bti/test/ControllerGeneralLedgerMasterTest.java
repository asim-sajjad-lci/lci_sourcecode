package com.bti.test;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;


@SpringBootTest
//@RunWith(SpringRunner.class)
public class ControllerGeneralLedgerMasterTest {

	public static String baseUrl ="http://localhost:8082/";
	
	/*********************Test Case for Save Financial Dimension Setup Module**************************/
    /**
     * @Name: Financial Dimension Setup
     * @throws Exception
     */
    @Test
    public void testCase0FinanacialDimensionSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "dime");
    	obj.put("dimensionMask", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetup").
                andReturn();
        assert response.statusCode()==200;
    }
    
    /**
     * @Name: Financial Dimension Setup
     * @throws Exception
     */
    @Test
    public void testCase1FinanacialDimensionSetup() throws Exception 
    {
    	/* Response :
    	 * Code 201 
    	 * Message="FINANCIAL_DIMENSION_SETUP_SUCCESS"
    	 */
    	
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "dime");
    	obj.put("dimensionMask", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetup").
                andReturn();
        assert response.statusCode()==200;
    }
    
    /**
     * @Name: Financial Dimension Setup
     * @throws Exception
     */
    @Test
    public void testCase2FinanacialDimensionSetup() throws Exception 
    {
    	/*Response:  Code=204
    			     Message={messageShort=REQUEST_BODY_IS_EMPTY}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "");
    	obj.put("dimensionMask", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * @Name: Financial Dimension Setup
     * @throws Exception
     */
    @Test
    public void testCase3FinanacialDimensionSetup() throws Exception 
    {
    	/*Response:  Code=201
                     Message={messageShort=FINANCIAL_DIMENSION_SETUP_SUCCESS}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "a");
    	obj.put("dimensionMask", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * @Name: Financial Dimension Setup
     * @throws Exception
     */
    @Test
    public void testCase4FinanacialDimensionSetup() throws Exception 
    {
    	/*Response:  Code=201
                     Message={messageShort=FINANCIAL_DIMENSION_SETUP_SUCCESS}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "");
    	obj.put("dimensionMask", "ass");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test Case for Get Financial Dimension Setup Module By Id**************************/
    
    /**
     * Financial Dimension get By id
     * @throws Exception
     */
    @Test
    public void testCase0ToGetFinaancialDimensionModuleById() throws Exception 
    {
    	/*Response:  Code=302
                     Message={message=Record already exist, messageShort=RECORD_ALREADY_EXIST}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxd", "46");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupGetById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension get By id
     * @throws Exception
     */
    @Test
    public void testCase1ToGetFinaancialDimensionModuleById() throws Exception 
    {
    	/*Response:  Code=404
                     Message={message=Record Not Found, messageShort=RECORD_NOT_FOUND}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxd", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupGetById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension get By id
     * @throws Exception
     */
    @Test
    public void testCase2ToGetFinaancialDimensionModuleById() throws Exception 
    {
    	/*Response:  Code=404
                     Message={message=Record Not Found, messageShort=RECORD_NOT_FOUND}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxd", "1478");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupGetById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension get By id
     * @throws Exception
     */
    @Test
    public void testCase3ToGetFinaancialDimensionModuleById() throws Exception 
    {
    	/*Response:  Code=302
                     Message={messageShort=RECORD_ALREADY_EXIST}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxd", "46");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupGetById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test Case for Update Financial Dimension Setup Module**************************/
    /**
     * Financial Dimension Update
     * @throws Exception
     */
    @Test
    public void testCase0FinanacialDimensionSetupUpdate() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "dime");
    	obj.put("dimensionMask", "abc");
    	obj.put("dimInxd", "46");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupUpdate").
                andReturn();
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Update
     * @throws Exception
     */
    @Test
    public void testCase1FinanacialDimensionSetupUpdate() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "dime");
    	obj.put("dimensionMask", "abc");
    	obj.put("dimInxd", "46");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupUpdate").
                andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
        
    }
    
    /**
     * Financial Dimension Update
     * @throws Exception
     */
    @Test
    public void testCase2FinanacialDimensionSetupUpdate() throws Exception 
    {
    	/*Response:  Code=204
    			     Message={messageShort=REQUEST_BODY_IS_EMPTY}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "");
    	obj.put("dimensionMask", "");
    	obj.put("dimInxd", "46");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupUpdate").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Update
     * @throws Exception
     */
    @Test
    public void testCase3FinanacialDimensionSetupUpdate() throws Exception 
    {
    	
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "a");
    	obj.put("dimensionMask", "");
    	obj.put("dimInxd", "46");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupUpdate").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Update
     * @throws Exception
     */
    @Test
    public void testCase4FinanacialDimensionSetupUpdate() throws Exception 
    {
    	
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "");
    	obj.put("dimensionMask", "ass");
    	obj.put("dimInxd", "46");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupUpdate").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Update
     * @throws Exception
     */
    @Test
    public void testCase5FinanacialDimensionSetupUpdate() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionName", "");
    	obj.put("dimensionMask", "ass");
    	obj.put("dimInxd", "46");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionSetupUpdate").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test Case for Search Financial Dimension Setup Module**************************/
    
    /**
     * Search Finanacial Dimension
     * @throws Exception
     */
    @Test
    public void testCase0SearchFinanacialDimensionSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Financial Dimension
     * @throws Exception
     */
    @Test
    public void testCase1SearchFinanacialDimensionSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Finanacial Dimension
     * @throws Exception
     */
    @Test
    public void testCase2SearchFinanacialDimensionSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Finanacial Dimension
     * @throws Exception
     */
    @Test
    public void testCase3SearchFinanacialDimensionSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
     
    /**
     * Search Finanacial Dimension
     * @throws Exception
     */
    @Test
    public void testCase4SearchFinanacialDimensionSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Financial Dimension
     * @throws Exception
     */
    @Test
    public void testCase5SearchFinanacialDimensionSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test Case for Save Financial Dimension Value Module**************************/
    
    
    /**
     * Save Financial Dimension Value Setup
     * @throws Exception
     */
    @Test
    public void testCase0FinanacialDimensionValueSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionValue", "dime123dd");
    	obj.put("dimInxd", "46");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValue").
                andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * @Name: Save Financial Dimension Value Setup
     * @throws Exception
     */
    @Test
    public void testCase1FinanacialDimensionValueSetup() throws Exception 
    {
    	/* Response :
    	 * Code 201 
    	 * Message="FINANCIAL_DIMENSION_SETUP_SUCCESS"
    	 */
    	
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionValue", "");
    	obj.put("dimInxd", "46");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValue").
                andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * @Name:Save Financial Dimension Value Setup
     * @throws Exception
     */
    @Test
    public void testCase2FinanacialDimensionValueSetup() throws Exception 
    {
    	/*Response:  Code=204
    			     Message={messageShort=REQUEST_BODY_IS_EMPTY}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionValue", "abcaq");
    	obj.put("dimInxd", "");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * @Name: Save Financial Dimension Value Setup
     * @throws Exception
     */
    @Test
    public void testCase3FinanacialDimensionValueSetup() throws Exception 
    {
    	/*Response:  Code=201
                     Message={messageShort=FINANCIAL_DIMENSION_SETUP_SUCCESS}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionValue", "abcawer");
    	obj.put("dimInxd", "47");
    	obj.put("dimensionDescription", "");
    	obj.put("dimensionDescriptionArabic", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * @Name: Save Financial Dimension Value Setup
     * @throws Exception
     */
    @Test
    public void testCase4FinanacialDimensionValueSetup() throws Exception 
    {
    	/*Response:  Code=201
                     Message={messageShort=FINANCIAL_DIMENSION_SETUP_SUCCESS}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimensionValue", "abcczcdxsd");
    	obj.put("dimInxd", "46");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
/*********************Test Case for Get Financial Dimension Value  Module By Id**************************/
    
    /**
     * Financial Dimension Value Get By Id
     * @throws Exception
     */
    @Test
    public void testCase0ToGetFinaancialDimensionValueModuleById() throws Exception 
    {
    	/*Response:  Code=302
                     Message={message=Record already exist, messageShort=RECORD_ALREADY_EXIST}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "1");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueGetById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Value get By id
     * @throws Exception
     */
    @Test
    public void testCase1ToGetFinaancialDimensionValueModuleById() throws Exception 
    {
    	/*Response:  Code=404
                     Message={message=Record Not Found, messageShort=RECORD_NOT_FOUND}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueGetById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Value get By id
     * @throws Exception
     */
    @Test
    public void testCase2ToGetFinaancialDimensionValueModuleById() throws Exception 
    {
    	/*Response:  Code=404
                     Message={message=Record Not Found, messageShort=RECORD_NOT_FOUND}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "1478");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueGetById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Value get By id
     * @throws Exception
     */
    @Test
    public void testCase3ToGetFinaancialDimensionValueModuleById() throws Exception 
    {
    	/*Response:  Code=302
                     Message={messageShort=RECORD_ALREADY_EXIST}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "46");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueGetById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test Case for Update Financial Dimension Value Module**************************/
    /**
     * Financial Dimension Value Update
     * @throws Exception
     */
    @Test
    public void testCase0FinanacialDimensionValueUpdate() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "2");
    	obj.put("dimInxd", "2");
    	obj.put("dimensionValue", "2");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueUpdate").
                andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Value Update
     * @throws Exception
     */
    @Test
    public void testCase1FinanacialDimensionValueUpdate() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "1");
    	obj.put("dimInxd", "");
    	obj.put("dimensionValue", "abcczcdxsd");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueUpdate").
                andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
        
    }
    
    /**
     * Financial Dimension Value Update
     * @throws Exception
     */
    @Test
    public void testCase2FinanacialDimensionValueUpdate() throws Exception 
    {
    	/*Response:  Code=204
    			     Message={messageShort=REQUEST_BODY_IS_EMPTY}*/
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "");
    	obj.put("dimInxd", "46");
    	obj.put("dimensionValue", "abcczcdxsd");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueUpdate").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Value Update
     * @throws Exception
     */
    @Test
    public void testCase3FinanacialDimensionValueUpdate() throws Exception 
    {
    	
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "");
    	obj.put("dimInxd", "");
    	obj.put("dimensionValue", "abcczcdxsd");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueUpdate").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Value Update
     * @throws Exception
     */
    @Test
    public void testCase4FinanacialDimensionValueUpdate() throws Exception 
    {
    	
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "1");
    	obj.put("dimInxd", "46");
    	obj.put("dimensionValue", "");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueUpdate").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Financial Dimension Value Update
     * @throws Exception
     */
    @Test
    public void testCase5FinanacialDimensionValueSetupUpdate() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("dimInxValue", "1");
    	obj.put("dimInxd", "46");
    	obj.put("dimensionValue", "abcczcdxsd");
    	obj.put("dimensionDescription", "abc");
    	obj.put("dimensionDescriptionArabic", "abc");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/financialDimensionValueUpdate").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
/*********************Test Case for Search Financial Dimension Value  Module**************************/
    
    /**
     * Search Finanacial Dimension Value
     * @throws Exception
     */
    @Test
    public void testCase0SearchFinanacialDimensionValue() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Financial Dimension
     * @throws Exception
     */
    @Test
    public void testCase1SearchFinanacialDimensionValue() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Finanacial Dimension Value
     * @throws Exception
     */
    @Test
    public void testCase2SearchFinanacialDimensionValue() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Finanacial Dimension Value
     * @throws Exception
     */
    @Test
    public void testCase3SearchFinanacialDimensionValue() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "");
    	obj.put("pageSize", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
     
    /**
     * Search Finanacial Dimension Value
     * @throws Exception
     */
    @Test
    public void testCase4SearchFinanacialDimensionValue() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "a");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search Financial Dimension Value
     * @throws Exception
     */
    @Test
    public void testCase5SearchFinanacialDimensionValue() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", "0");
    	obj.put("pageSize", "10");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchFinancialDimensionValue").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test case for save account currency access module**************************/
    
    /**
     * Account currency access add
     * @throws Exception
     */
    
    @Test
    public void testCase0AccountCurrencyAccessAdd() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	List<String> currencyIdsList= new ArrayList<>();
    	currencyIdsList.add("test-id");
    	obj.put("accountIndex", "1");
    	obj.put("currencyId", currencyIdsList);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/accountCurrencyAccess/add").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Account currency access add
     * @throws Exception
     */
    
    @Test
    public void testCase1AccountCurrencyAccessAdd() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	List<String> currencyIdsList= new ArrayList<>();
    	obj.put("accountIndex", "1");
    	obj.put("currencyId", currencyIdsList);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/accountCurrencyAccess/add").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Account currency access add
     * @throws Exception
     */
    
    @Test
    public void testCase2AccountCurrencyAccessAdd() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	List<String> currencyIdsList= new ArrayList<>();
    	currencyIdsList.add("3");
    	obj.put("accountIndex",0);
    	obj.put("currencyId", currencyIdsList);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/accountCurrencyAccess/add").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
  /*********************Test case for get account currency access module**************************/
    /**
     * Get account currency access
     * @throws Exception
     */
    
    @Test
    public void testCase0GetAccountCurrencyAccess() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .get(baseUrl+"setup/generalLedgerMaster/getAccountCurrencyAccess").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get account currency access
     * @throws Exception
     */
    
    @Test
    public void testCase1GetAccountCurrencyAccess() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .get(baseUrl+"setup/generalLedgerMaster/getAccountCurrencyAccess").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test case for save fixed allocation account**************************/
    
    /**
     * Save fixed allocation account
     * @throws Exception
     */
    @Test
    public void testCase0SaveFixedAllocationAccount() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	List<DtoPayableAccountClassSetup> accountNumberList= new ArrayList<>();
    	DtoPayableAccountClassSetup dtoPayableAccountClassSetup= new DtoPayableAccountClassSetup();
    	dtoPayableAccountClassSetup.setAccountTableRowIndex("1");
    	dtoPayableAccountClassSetup.setPercentage("20.01");
    	accountNumberList.add(dtoPayableAccountClassSetup);
    	obj.put("accountTableRowIndex", "2");
    	obj.put("accountNumberList", accountNumberList);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/saveFixedAllocationDetail").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save fixed allocation account
     * @throws Exception
     */
    
    @Test
    public void testCase1SaveFixedAllocationAccount() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	List<DtoPayableAccountClassSetup> accountNumberList= new ArrayList<>();
    	DtoPayableAccountClassSetup dtoPayableAccountClassSetup= new DtoPayableAccountClassSetup();
    	dtoPayableAccountClassSetup.setAccountTableRowIndex("1");
    	dtoPayableAccountClassSetup.setPercentage("20.01");
    	accountNumberList.add(dtoPayableAccountClassSetup);
    	obj.put("accountTableRowIndex", "");
    	obj.put("accountNumberList", accountNumberList);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/saveFixedAllocationDetail").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save fixed allocation account
     * @throws Exception
     */
    
    @Test
    public void testCase2SaveFixedAllocationAccount() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	List<DtoPayableAccountClassSetup> accountNumberList= new ArrayList<>();
    	obj.put("accountTableRowIndex", "");
    	obj.put("accountNumberList", accountNumberList);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/saveFixedAllocationDetail").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save fixed allocation account
     * @throws Exception
     */
    
    @Test
    public void testCase3SaveFixedAllocationAccount() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	List<DtoPayableAccountClassSetup> accountNumberList= new ArrayList<>();
    	obj.put("accountTableRowIndex", "2");
    	obj.put("accountNumberList", accountNumberList);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/saveFixedAllocationDetail").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test case for get fixed allocation account by account number**************************/
    
    /**
     * Get fixed allocation account by account number
     * @throws Exception
     */
    @Test
    public void testCase0GetFixedAllocationAccountByAccountNumber() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountTableRowIndex", "2");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/fixedAllocationDetail/getByAccountNumber").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get fixed allocation account by account number
     * @throws Exception
     */
    
    @Test
    public void testCase1GetFixedAllocationAccountByAccountNumber() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountTableRowIndex", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/fixedAllocationDetail/getByAccountNumber").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get fixed allocation account by account number
     * @throws Exception
     */
    @Test
    public void testCase2GetFixedAllocationAccountByAccountNumber() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountTableRowIndex", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/fixedAllocationDetail/getByAccountNumber").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get fixed allocation account by account number
     * @throws Exception
     */
    @Test
    public void testCase3GetFixedAllocationAccountByAccountNumber() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountTableRowIndex", "2");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/fixedAllocationDetail/getByAccountNumber").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test case for get all fixed allocation account number**************************/
    
    /**
     * Get All fixed allocation account numbers
     * @throws Exception
     */
    @Test
    public void testCase0GetAllFixedAllocationAccountNumber() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .get(baseUrl+"setup/generalLedgerMaster/fixedAllocationDetail/getAll").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get All fixed allocation account numbers
     * @throws Exception
     */
    @Test
    public void testCase1GetAllFixedAllocationAccountNumber() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .get(baseUrl+"setup/generalLedgerMaster/fixedAllocationDetail/getAll").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test case for get typical balance types **************************/
    
    
    /**
     * Get typical balance type status
     * @throws Exception
     */
    @Test
    public void testCase0GetTypicalBalaceTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/getTypicalBalanceTypeStatus").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get typical balance type status
     * @throws Exception
     */
    @Test
    public void testCase1GetTypicalBalaceTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/getTypicalBalanceTypeStatus").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
/*********************Test case for Save Main Account Setupr**************************/
    
    /**
     * Save main account setup
     * @throws Exception
     */
    @Test
    public void testCase0SaveMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", 1);
    	obj.put("accountTypeId", 2);
    	obj.put("mainAccountNumber", "20023");
    	obj.put("mainAccountDescription", "Testdes");
    	obj.put("mainAccountDescriptionArabic", "Testdesx");
    	obj.put("tpclblnc", 2);
    	obj.put("aliasAccount", "name");
    	obj.put("aliasAccountArabic", "nameA");
    	obj.put("allowAccountTransactionEntry",true);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/mainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Response: Account category not found
     * Save main account setup
     * @throws Exception
     */
     
    @Test
    public void testCase1SaveMainAccountSetup() throws Exception 
    {
    	
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", "");
    	obj.put("accountTypeId", 0);
    	obj.put("mainAccountNumber", "20024");
    	obj.put("mainAccountDescription", "Testdes");
    	obj.put("mainAccountDescriptionArabic", "Testdesx");
    	obj.put("tpclblnc", 2);
    	obj.put("aliasAccount", "name");
    	obj.put("aliasAccountArabic", "nameA");
    	obj.put("allowAccountTransactionEntry",true);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/mainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Response: Account type not found
     * Save main account setup
     * @throws Exception
     */
     
    @Test
    public void testCase2SaveMainAccountSetup() throws Exception 
    {
    	
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", 1);
    	obj.put("accountTypeId", "");
    	obj.put("mainAccountNumber", "20024");
    	obj.put("mainAccountDescription", "Testdes");
    	obj.put("mainAccountDescriptionArabic", "Testdesx");
    	obj.put("tpclblnc", 2);
    	obj.put("aliasAccount", "name");
    	obj.put("aliasAccountArabic", "nameA");
    	obj.put("allowAccountTransactionEntry",true);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/mainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save main account setup
     * @throws Exception
     * Set tbclblnc 0
     */
    @Test
    public void testCase3SaveMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", "1");
    	obj.put("accountTypeId", "1"); 
    	obj.put("mainAccountNumber", "20024");
    	obj.put("mainAccountDescription", "Testdes");
    	obj.put("mainAccountDescriptionArabic", "Testdesx");
    	obj.put("tpclblnc", 0);   //Integer Value 
    	obj.put("aliasAccount", "name");
    	obj.put("aliasAccountArabic", "nameA");
    	obj.put("allowAccountTransactionEntry",true);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/mainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save main account setup
     * @throws Exception
     * Set value of descrptions empty 
     */
    
    @Test
    public void testCase4SaveMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", "1");
    	obj.put("accountTypeId", "1"); 
    	obj.put("mainAccountNumber", "20025");
    	obj.put("mainAccountDescription", "");
    	obj.put("mainAccountDescriptionArabic", "");
    	obj.put("tpclblnc", 0);   //Integer Value 
    	obj.put("aliasAccount", "");
    	obj.put("aliasAccountArabic", "");
    	obj.put("allowAccountTransactionEntry",false);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/mainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test case for Get Main Account Setup by account number**************************/
    
    /**
     * Get main account setup by account number
     * @throws Exception
     */
    
    @Test
    public void testCase0GeteMainAccountSetupByAccountNumber() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("mainAccountNumber", "20025");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/getMainAccountSetupById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get main account setup by account number
     * @throws Exception
     * Set main account number empty
     */
    @Test
    public void testCase1GeteMainAccountSetupByAccountNumber() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("mainAccountNumber", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/getMainAccountSetupById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Get main account setup by account number
     * @throws Exception
     * Set language and user Id empty in header
     */
    @Test
    public void testCase2GetMainAccountSetupByAccountNumber() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("mainAccountNumber", "20025");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/getMainAccountSetupById").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
/*********************Test case for Update Main Account Setupr**************************/
    
    /**
     * update main account setup
     * @throws Exception
     */
    @Test
    public void testCase0UpdateMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", 1);
    	obj.put("accountTypeId", 2);
    	obj.put("mainAccountNumber", "20023");
    	obj.put("mainAccountDescription", "Testdes");
    	obj.put("mainAccountDescriptionArabic", "Testdesx");
    	obj.put("tpclblnc", 2);
    	obj.put("aliasAccount", "name");
    	obj.put("aliasAccountArabic", "nameA");
    	obj.put("allowAccountTransactionEntry",true);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/updateMainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Response: Account category not found
     * Update main account setup
     * @throws Exception
     */
     
    @Test
    public void testCase1UpdateMainAccountSetup() throws Exception 
    {
    	
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", "");
    	obj.put("accountTypeId", 0);
    	obj.put("mainAccountNumber", "20024");
    	obj.put("mainAccountDescription", "Testdes");
    	obj.put("mainAccountDescriptionArabic", "Testdesx");
    	obj.put("tpclblnc", 2);
    	obj.put("aliasAccount", "name");
    	obj.put("aliasAccountArabic", "nameA");
    	obj.put("allowAccountTransactionEntry",true);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/updateMainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Response: Account type not found
     * Update main account setup
     * @throws Exception
     */
     
    @Test
    public void testCase2UpdateMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", 1);
    	obj.put("accountTypeId", "");
    	obj.put("mainAccountNumber", "20024");
    	obj.put("mainAccountDescription", "Testdes");
    	obj.put("mainAccountDescriptionArabic", "Testdesx");
    	obj.put("tpclblnc", 2);
    	obj.put("aliasAccount", "name");
    	obj.put("aliasAccountArabic", "nameA");
    	obj.put("allowAccountTransactionEntry",true);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/updateMainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Update main account setup
     * @throws Exception
     * Set tbclblnc 0
     */
    @Test
    public void testCase3UpdateMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", "1");
    	obj.put("accountTypeId", "1"); 
    	obj.put("mainAccountNumber", "20024");
    	obj.put("mainAccountDescription", "Testdes");
    	obj.put("mainAccountDescriptionArabic", "Testdesx");
    	obj.put("tpclblnc", 0);   //Integer Value 
    	obj.put("aliasAccount", "name");
    	obj.put("aliasAccountArabic", "nameA");
    	obj.put("allowAccountTransactionEntry",true);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/updateMainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Update main account setup
     * @throws Exception
     * Set value of descrptions empty 
     */
    
    @Test
    public void testCase4UpdateMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("accountCategoryId", "1");
    	obj.put("accountTypeId", "1"); 
    	obj.put("mainAccountNumber", "20025");
    	obj.put("mainAccountDescription", "");
    	obj.put("mainAccountDescriptionArabic", "");
    	obj.put("tpclblnc", 0);   //Integer Value 
    	obj.put("aliasAccount", "");
    	obj.put("aliasAccountArabic", "");
    	obj.put("allowAccountTransactionEntry",false);
    	obj.put("active",true);
    	obj.put("userDef1", "userDef1");
    	obj.put("userDef2", "userDef2");
    	obj.put("userDef3", "userDef3");
    	obj.put("userDef4", "userDef4");
    	obj.put("userDef5", "userDef5");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/updateMainAccountSetUp").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /*********************Test case for Search Main Account Setup**************************/
    
    /**
     * Search main account setup
     * @throws Exception
     */
    @Test
    public void testCase0SearchMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "1");
    	obj.put("pageNumber", 0); 
    	obj.put("pageSize", 10);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchMainAccountSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search main account setup
     * @throws Exception
     * Remove Pagination
     */
    @Test
    public void testCase1SearchMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "1");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchMainAccountSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search main account setup
     * @throws Exception
     * Blank Pagination
     */
    @Test
    public void testCase2SearchMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "1");
    	obj.put("pageNumber", ""); 
    	obj.put("pageSize", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchMainAccountSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search main account setup
     * @throws Exception
     * Blank Pagination
     * Remove Pagination and Set keyword blank (Get All Records)
     */
    @Test
    public void testCase3SearchMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchMainAccountSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search main account setup
     * @throws Exception
     * Blank All Parameters (get All records)
     * 
     */
    @Test
    public void testCase4SearchMainAccountSetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", ""); 
    	obj.put("pageSize", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedgerMaster/searchMainAccountSetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    
}
