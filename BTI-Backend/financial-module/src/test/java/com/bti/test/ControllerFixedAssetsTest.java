package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.bti.model.dto.DtoFACalendarSetupMonthly;
import com.bti.model.dto.DtoFACalendarSetupQuarter;
import com.bti.model.dto.DtoFaAccountGroupSetup;
import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerFixedAssetsTest {

	/**
	 * Create retirement setup
	 * @throws Exception
	 */
	@Test
	public void testCreateRetirementSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("faRetirementId", "FR-1");
		obj.put("descriptionPrimary", "test description in eng");
		obj.put("descriptionSecondary", "test description in ar");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/retirementSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update retirement setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateRetirementSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("faRetirementId", "FR-1");
		obj.put("descriptionPrimary", "test description in eng");
		obj.put("descriptionSecondary", "test description in ar");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/retirementSetup/update").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all retirement setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllRetirementSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/retirementSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all retirement setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllRetirementSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/retirementSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched retirement setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedRetirementSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "finance");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/retirementSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get retirement setup by id
	 * @throws Exception
	 */
	@Test
	public void testGetRetirementSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("faRetirementId", "FR-1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/retirementSetup/getById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Fixed Asset Company Setup
	 * @throws Exception
	 */
	@Test
	public void testSaveFixedAssetCompanySetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bookIndexId", "1");
		obj.put("bookDescription", "abcd");
		obj.put("bookDescriptionArabic", "ass");
		obj.put("postDetails", "1");
		obj.put("postPayableManagement", "0");
		obj.put("postPurchaseOrderProcessing", "1");
		obj.put("autoAddBookInformation", "1");
		obj.put("defaultAssetLabel", "1");
		obj.put("requireAssetAccount", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/saveFixedAssetCompanySetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get FACompany Setup 
	 * @throws Exception
	 */
	@Test
	public void testGetFixedAssetCompanySetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get("http://localhost:8084/fixed/assets/getFixedAssetCompanySetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void testSaveFixedAssetBookSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bookId", "2");
		obj.put("bookDescription", "Tests");
		obj.put("bookDescriptionArabic", "12334");
		obj.put("currentYear", "2017");
		obj.put("depreciationPeriodId", "1");
		obj.put("faCalendarSetupIndexId", "1");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/saveFixedAssetBookSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Book Setup By Index Id
	 * @throws Exception
	 */
	@Test
	public void testGetFixedAssetBookSetupByBookIndexId() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bookIndexId", "2");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/getFixedAssetBookSetupByBookIndexId").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Book Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateFixedAssetBookSetupByBookIndexId() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bookIndexId", "2");
		obj.put("bookId", "2");
		obj.put("bookDescription", "Tests");
		obj.put("bookDescriptionArabic", "12334");
		obj.put("currentYear", "2017");
		obj.put("depreciationPeriodId", "1");
		obj.put("faCalendarSetupIndexId", "1");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/updateFixedAssetBookSetupByBookIndexId").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Book Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllBookSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchBookSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Book Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllBookSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchBookSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched Book Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedBookSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "1");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchBookSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Monthly Calendar Setup
	 * @throws Exception
	 */
	@Test
	public void testCalendarMonthlySetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("calendarId", "123");
		obj.put("calendarDescription", "calendarDescription");
		obj.put("calendarDescriptionArabic", "calendarDescriptionArabic");
		obj.put("year1", "2017");
		
		DtoFACalendarSetupMonthly calendarSetupMonthly = new DtoFACalendarSetupMonthly();
		List<DtoFACalendarSetupMonthly> calendarSetupMonthlys = new ArrayList<DtoFACalendarSetupMonthly>();
		
		calendarSetupMonthly.setPeriodIndex(1);
		calendarSetupMonthly.setEndDate("01/31/2017");
		calendarSetupMonthly.setStartDate("01/01/2017");
		
		calendarSetupMonthlys.add(calendarSetupMonthly);
		
		obj.put("monthlyCalendar", calendarSetupMonthlys);
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/calendarMonthlySetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Monthly Calendar Setup
	 * @throws Exception
	 */
	@Test
	public void testGetCalendarMonthlySetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/fixed/assets/getCalendarMonthlySetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Quarter Calendar Setup
	 * @throws Exception
	 */
	@Test
	public void testCalendarQuarterSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("calendarId", "1234");
		obj.put("calendarDescription", "calendarDescription");
		obj.put("calendarDescriptionArabic", "calendarDescriptionArabic");
		obj.put("year1", "2017");
		
		DtoFACalendarSetupQuarter calendarSetupQuarter = new DtoFACalendarSetupQuarter();
		List<DtoFACalendarSetupQuarter> calendarSetupQuarters = new ArrayList<>();
		
		calendarSetupQuarter.setQuarterIndex(1);
		calendarSetupQuarter.setEndDate("03/31/2017");
		calendarSetupQuarter.setStartDate("01/01/2017");
		calendarSetupQuarter.setMidDate("02/14/2017");
		calendarSetupQuarters.add(calendarSetupQuarter);
		
		obj.put("quarterCalendar", calendarSetupQuarters);
		
		String jsonString = obj.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/calendarQuarterSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Quarter Calendar Setup 
	 * @throws Exception
	 */
	@Test
	public void testGetCalendarQuarterSetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/fixed/assets/getCalendarQuarterSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Structure Setup
	 * @throws Exception
	 */
	@Test
	public void testStructureSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("structureId", "1234");
		obj.put("structureDescription", "structureDescription");
		obj.put("structureDescriptionArabic", "structureDescriptionArabic");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/structureSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Structure Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateStructureSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("structureIndex", "1");
		obj.put("structureId", "1234");
		obj.put("structureDescription", "structureDescription");
		obj.put("structureDescriptionArabic", "structureDescriptionArabic");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/updateStructureSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Structure Setup by Id
	 * @throws Exception
	 */
	@Test
	public void testGetStructureSetupById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("structureIndex", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/getStructureSetupById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Structure Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllStructureSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchStructureSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Structure Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllStructureSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchStructureSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched Structure Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedStructureSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "structureDescription");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchStructureSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save new location setup
	 * @throws Exception
	 */
	@Test
	public void testLocationSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("locationId", "L001");
		obj.put("stateId", "1");
		obj.put("cityId", "1");
		obj.put("countryId", "1");
		obj.put("state", "Andaman and Nicobar Islands");
		obj.put("city", "Bombuflat");
		obj.put("country", "Afghanistan");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/locationSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update location setup
	 * @throws Exception
	 */
	@Test
	public void testLocationSetupUpdate() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("locationId", "L001");
		obj.put("stateId", "1");
		obj.put("cityId", "1");
		obj.put("countryId", "1");
		obj.put("state", "Andaman and Nicobar Islands");
		obj.put("city", "Bombuflat");
		obj.put("country", "Afghanistan");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/locationSetup/update").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get location setup by id
	 * @throws Exception
	 */
	@Test
	public void testLocationSetupGetById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("locationId", "L001");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/locationSetup/getById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all location setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllLocationSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/locationSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all location setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllLocationSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/locationSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched location setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedLocationSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "finance");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/locationSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save new physical location setup
	 * @throws Exception
	 */
	@Test
	public void testPhysicalLocationSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("physicalLocationId", "PL001");
		obj.put("descriptionPrimary", "desc end");
		obj.put("descriptionSecondary", "desc ar");
		obj.put("locationIndex", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/physicalLocationSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update physical location setup
	 * @throws Exception
	 */
	@Test
	public void testUpdatePhysicalLocationSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("physicalLocationId", "PL001");
		obj.put("descriptionPrimary", "desc end");
		obj.put("descriptionSecondary", "desc ar");
		obj.put("locationIndex", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/physicalLocationSetup/update").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get physical location setup by id
	 * @throws Exception
	 */
	@Test
	public void testGetPhysicalLocationSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("physicalLocationId", "PL001");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/physicalLocationSetup/getById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all physical location setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllPhysicalLocationSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/physicalLocationSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all physical location setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllPhysicalLocationSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/physicalLocationSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Search physical location setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedPhysicalLocationSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/physicalLocationSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Maintenance
	 * @throws Exception
	 */
	@Test
	public void testSaveFAGeneralMaintenance() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("assetId", "233");
		obj.put("faAcquisitionCost", "10.2");
		obj.put("faAcquisitionDate", "30/08/2017");
		obj.put("faAddedDate", "30/08/2017");
		obj.put("assetLabelId", "123456789");
		obj.put("faMasterAssetId", "14789");
		obj.put("assetQuantity", "20");
		obj.put("assetStatus", "1");
		obj.put("assetType", "1");
		obj.put("currencyId", "test-id");
		obj.put("faDescription", "Test");
		obj.put("faDescriptionArabic", "Test Arabic");
		obj.put("employeeId", "123456");
		obj.put("assetExtendedDescription", "Test Extend Desc");
		obj.put("lastMaintenance", "30/08/2017");
		obj.put("manufacturerName", "Raman");
		obj.put("faShortName", "Rmn");
		obj.put("faAccountGroupsSetupId", "Test123");
		obj.put("faClassSetupId", "classid");
		obj.put("faLocationId", "locationid");
		obj.put("faPropertyTypes", "1");
		obj.put("faPhysicalLocationId", "locationid");
		obj.put("faStructureId", "structureId");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/saveFAGeneralMaintenance").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Maintenance
	 * @throws Exception
	 */
	@Test
	public void testUpdateFAGeneralMaintenance() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("assetId", "233");
		obj.put("faAcquisitionCost", "10.2");
		obj.put("faAcquisitionDate", "30/08/2017");
		obj.put("faAddedDate", "30/08/2017");
		obj.put("assetLabelId", "123456789");
		obj.put("faMasterAssetId", "14789");
		obj.put("assetQuantity", "20");
		obj.put("assetStatus", "1");
		obj.put("assetType", "1");
		obj.put("currencyId", "test-id");
		obj.put("faDescription", "Test");
		obj.put("faDescriptionArabic", "Test Arabic");
		obj.put("employeeId", "123456");
		obj.put("assetExtendedDescription", "Test Extend Desc");
		obj.put("lastMaintenance", "30/08/2017");
		obj.put("manufacturerName", "Raman");
		obj.put("faShortName", "Rmn");
		obj.put("faAccountGroupsSetupId", "Test123");
		obj.put("faClassSetupId", "classid");
		obj.put("faLocationId", "locationid");
		obj.put("faPropertyTypes", "1");
		obj.put("faPhysicalLocationId", "locationid");
		obj.put("faStructureId", "structureId");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/updateFAGeneralMaintenance").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get  Maintenance By Id
	 * @throws Exception
	 */
	@Test
	public void testGetFAGeneralMaintenanceByAssetId() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("assetId", "233");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/getFAGeneralMaintenanceByAssetId").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all maintenance 
	 * @throws Exception
	 */
	@Test
	public void testGetAllFAGeneralMaintenance() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFAGeneralMaintenance").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all maintenance with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllFAGeneralMaintenanceWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFAGeneralMaintenance").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched maintenance with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedFAGeneralMaintenanceWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "233");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFAGeneralMaintenance").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save or update lease company
	 * @throws Exception
	 */
	@Test
	public void testLeaseCompanySetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("companyId", "COMP-1");
		obj.put("vendorId", "V-2");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/leaseCompanySetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get lease company setup
	 * @throws Exception
	 */
	@Test
	public void testGetLeaseCompanySetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/fixed/assets/leaseCompanySetup/get").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Book class Setup
	 * @throws Exception
	 */
	@Test
	public void testSaveFABookClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bookId", "b1");
		obj.put("classId", "1");
		obj.put("amortizationCode", "1");
		obj.put("initialAllowancePercentage", "10.10");
		obj.put("amortizationPercentage", "10.10");
		obj.put("amortizationAmount", "10.10");
		obj.put("averagingConvention", "1");
		obj.put("origLifeDays", "12");
		obj.put("origLifeYears", "13");
		obj.put("salvageEstimate", "14");
		obj.put("salvagePercentage", "1.2");
		obj.put("specialDepreciationAllowance", "11.3");
		obj.put("specialDepreciationAllowancePercent", "12.2");
		obj.put("swtch", "1");
		obj.put("depreciationMethodId", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/saveFABookClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Book class Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateFABookClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bookIndex", "1");
		obj.put("bookId", "b1");
		obj.put("classId", "1");
		obj.put("amortizationCode", "1");
		obj.put("initialAllowancePercentage", "10.10");
		obj.put("amortizationPercentage", "10.10");
		obj.put("amortizationAmount", "10.10");
		obj.put("averagingConvention", "1");
		obj.put("origLifeDays", "12");
		obj.put("origLifeYears", "13");
		obj.put("salvageEstimate", "14");
		obj.put("salvagePercentage", "1.2");
		obj.put("specialDepreciationAllowance", "11.3");
		obj.put("specialDepreciationAllowancePercent", "12.2");
		obj.put("swtch", "1");
		obj.put("depreciationMethodId", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/updateFABookClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Book class Setup By Index Id
	 * @throws Exception
	 */
	@Test
	public void testGetFABookClassSetupById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("bookIndex", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/getFABookClassSetupById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Book Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllFABookClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFABookClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all Book Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllFABookClassSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFABookClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched Book Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedFABookClassSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "1");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFABookClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Class Setup
	 * @throws Exception
	 */
	@Test
	public void testSaveFixedAssetClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("classId", "12343");
		obj.put("description", "123");
		obj.put("descriptionArabic", "12345");
		obj.put("faInsuranceClassIndexId", "1");
		obj.put("faAccountGroupId", "ab123");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/saveFixedAssetClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update Class Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateFixedAssetClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("classId", "12343");
		obj.put("description", "123");
		obj.put("descriptionArabic", "12345");
		obj.put("faInsuranceClassIndexId", "1");
		obj.put("faAccountGroupId", "ab123");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/updateFixedAssetClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Class Setup
	 * @throws Exception
	 */
	@Test
	public void testGetFixedAssetClassSetupByClassId() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("classId", "12343");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/getFixedAssetClassSetupByClassId").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all FA Class Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllFAClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFAClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all FA Class Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllFAClassSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFAClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched FA Class Setup with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedFAClassSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "1");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "10");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/searchFAClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Create insurance class setup
	 * @throws Exception
	 */
	@Test
	public void testInsuranceClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("insuranceClassId", "INS-1");
		obj.put("insuranceDescriptionPrimary", "desc primary");
		obj.put("insuranceDescriptionSecondary", "desc secondary");
		obj.put("inflationPercent", "1.2");
		obj.put("depriciationRate", "1.1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/insuranceClassSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update insurance class setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateInsuranceClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("insuranceClassIndex", "1");
		obj.put("insuranceClassId", "INS-1");
		obj.put("insuranceDescriptionPrimary", "desc primary");
		obj.put("insuranceDescriptionSecondary", "desc secondary");
		obj.put("inflationPercent", "1.2");
		obj.put("depriciationRate", "1.1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/insuranceClassSetup/update").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get insurance class by id
	 * @throws Exception
	 */
	@Test
	public void testGetByIdInsuranceClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("insuranceClassIndex", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/insuranceClassSetup/getById").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all insurance class
	 * @throws Exception
	 */
	@Test
	public void testGetAllInsuranceClassSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/insuranceClassSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all insurance class with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllInsuranceClassSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/insuranceClassSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched insurance class with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedInsuranceClassSetupWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "finance");
		obj.put("pageNumber", "1");
		obj.put("pageSize", "1");

		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/insuranceClassSetup/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save Account Group
	 * @throws Exception
	 */
	@Test
	public void testAccountGroupSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("accountGroupId", "18");
		obj.put("description", "123456");
		obj.put("descriptionArabic", "Test Arabic");
		
		DtoFaAccountGroupSetup accountGroupSetup = new DtoFaAccountGroupSetup();
		List<DtoFaAccountGroupSetup> accountGroupSetups = new ArrayList<>();
		
		accountGroupSetup.setAccountNumber("12-12-12-12");
		accountGroupSetup.setAccountTypeId(1);
		accountGroupSetup.setAccountDescription("Test");
		
		accountGroupSetups.add(accountGroupSetup);

		obj.put("accountNumberList", accountGroupSetups);
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/accountGroupSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get Account Group
	 * @throws Exception
	 */
	@Test
	public void testGetAccountGroupSetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/fixed/assets/getAccountGroupSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Save or update purchasing posting setup
	 * @throws Exception
	 */
	@Test
	public void testPurchasePostingAccountSetup() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("classId", "1");
		obj.put("accountTableRowIndex", "1");
		
		String jsonString = obj.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/fixed/assets/purchasePostingAccountSetup").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get purchasing posting setup
	 * @throws Exception
	 */
	@Test
	public void testGetPurchasePostingAccountSetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.post("http://localhost:8084/fixed/assets/getPurchasePostingAccountSetup").andReturn();
		assert response.statusCode() == 200;
	}
}
