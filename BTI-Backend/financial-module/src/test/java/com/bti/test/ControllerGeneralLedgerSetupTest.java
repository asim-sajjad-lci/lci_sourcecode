package com.bti.test;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.bti.model.dto.DtoFiscalFinancialPeriodSetup;
import com.bti.model.dto.DtoFiscalFinancialPeriodSetupDetail;
import com.bti.model.dto.DtoGLConfigurationRelationBetweenDimensionsAndMainAccount;
import com.bti.model.dto.DtoMassCloseFiscalPeriodSetup;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;


@SpringBootTest
//@RunWith(SpringRunner.class)
public class ControllerGeneralLedgerSetupTest {

	public static String baseUrl ="http://localhost:8082/";
	
	/*********************Test Case for Save Currency Setup**************************/
	
    /**
     * Save currency  Setup
     * @throws Exception
     */
    
    @Test
    public void testCase0SaveCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "2312");
    	obj.put("currencyDescription", "Test"); 
    	obj.put("currencyDescriptionArabic", "Rabic");
    	obj.put("currencySymbol", "$");
    	obj.put("currencyUnit", "Currency");
    	obj.put("unitSubunitConnector", "subunit connector");   //Integer Value 
    	obj.put("currencySubunit", "name");
    	obj.put("currencyUnitArabic", "nameA");
    	obj.put("unitSubunitConnectorArabic","Arabic");
    	obj.put("currencySubunitArabic","TestArabic");
    	obj.put("separatorsDecimal", 2);
    	obj.put("includeSpaceAfterCurrencySymbol", true);
    	obj.put("negativeSymbol", 2);
    	obj.put("displayNegativeSymbolSign", 2);
    	obj.put("separatorsThousands", 3);
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/currencySetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save currency Setup
     * @throws Exception
     * Integer parameter remove from request
     */
    @Test
    public void testCase1SaveCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "23147");
    	obj.put("currencyDescription", "Test"); 
    	obj.put("currencyDescriptionArabic", "Rabic");
    	obj.put("currencySymbol", "$");
    	obj.put("currencyUnit", "Currency");
    	obj.put("unitSubunitConnector", "subunit connector");   //Integer Value 
    	obj.put("currencySubunit", "name");
    	obj.put("currencyUnitArabic", "nameA");
    	obj.put("unitSubunitConnectorArabic","Arabic");
    	obj.put("currencySubunitArabic","TestArabic");
    	obj.put("includeSpaceAfterCurrencySymbol", true);
    	/*these all mandatory fields default value is null , 
    	if we do not have send any value , than autmatically set is null */
    	
    	/*
    	 
    	obj.put("separatorsDecimal", 2);
    	obj.put("negativeSymbol", 2);
    	obj.put("displayNegativeSymbolSign", 2);
    	obj.put("separatorsThousands", 3);  
    	
    	*/
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/currencySetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save currency  Setup
     * @throws Exception
     * Integer parameter set value 0 
     */
    @Test
    public void testCase2SaveCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "231471");
    	obj.put("currencyDescription", "Test"); 
    	obj.put("currencyDescriptionArabic", "Rabic");
    	obj.put("currencySymbol", "$");
    	obj.put("currencyUnit", "Currency");
    	obj.put("unitSubunitConnector", "subunit connector");   //Integer Value 
    	obj.put("currencySubunit", "name");
    	obj.put("currencyUnitArabic", "nameA");
    	obj.put("unitSubunitConnectorArabic","Arabic");
    	obj.put("currencySubunitArabic","TestArabic");
    	obj.put("includeSpaceAfterCurrencySymbol", true);
    	/* these all mandatory fields default value is null , 
    	if we do not have send any value or 0 , than autmatically set is null  */
    	 
    	obj.put("separatorsDecimal", 0);
    	obj.put("negativeSymbol", 0);
    	obj.put("displayNegativeSymbolSign", 0);
    	obj.put("separatorsThousands", 0);  
    	
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/currencySetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save currency  Setup
     * @throws Exception
     * Set Currency Id Empty 
     */
    
    @Test
    public void testCase3SaveCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "");
    	obj.put("currencyDescription", "Test"); 
    	obj.put("currencyDescriptionArabic", "Rabic");
    	obj.put("currencySymbol", "$");
    	obj.put("currencyUnit", "Currency");
    	obj.put("unitSubunitConnector", "subunit connector");   //Integer Value 
    	obj.put("currencySubunit", "name");
    	obj.put("currencyUnitArabic", "nameA");
    	obj.put("unitSubunitConnectorArabic","Arabic");
    	obj.put("currencySubunitArabic","TestArabic");
    	obj.put("includeSpaceAfterCurrencySymbol", true);
    	/* these all mandatory fields default value is null , 
    	if we do not have send any value or 0 , than autmatically set is null  */
    	 
    	obj.put("separatorsDecimal", 0);
    	obj.put("negativeSymbol", 0);
    	obj.put("displayNegativeSymbolSign", 0);
    	obj.put("separatorsThousands", 0);  
    	
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/currencySetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
   /*********************Test Case for Update Currency Setup**************************/
	
    /**
     * Update currency  Setup
     * @throws Exception
     */
    
    
    @Test
    public void testCase0UpdateCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "2312");
    	obj.put("currencyDescription", "Test"); 
    	obj.put("currencyDescriptionArabic", "Rabic");
    	obj.put("currencySymbol", "$");
    	obj.put("currencyUnit", "Currency");
    	obj.put("unitSubunitConnector", "subunit connector");   //Integer Value 
    	obj.put("currencySubunit", "name");
    	obj.put("currencyUnitArabic", "nameA");
    	obj.put("unitSubunitConnectorArabic","Arabic");
    	obj.put("currencySubunitArabic","TestArabic");
    	obj.put("separatorsDecimal", 2);
    	obj.put("includeSpaceAfterCurrencySymbol", true);
    	obj.put("negativeSymbol", 2);
    	obj.put("displayNegativeSymbolSign", 2);
    	obj.put("separatorsThousands", 3);
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/updateCurrencySetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save currency  Setup
     * @throws Exception
     * Integer parameter remove from request
     */
    @Test
    public void testCase1UpdateCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "23147");
    	obj.put("currencyDescription", "Test"); 
    	obj.put("currencyDescriptionArabic", "Rabic");
    	obj.put("currencySymbol", "$");
    	obj.put("currencyUnit", "Currency");
    	obj.put("unitSubunitConnector", "subunit connector");   //Integer Value 
    	obj.put("currencySubunit", "name");
    	obj.put("currencyUnitArabic", "nameA");
    	obj.put("unitSubunitConnectorArabic","Arabic");
    	obj.put("currencySubunitArabic","TestArabic");
    	obj.put("includeSpaceAfterCurrencySymbol", true);
    	/*these all mandatory fields default value is null , 
    	if we do not have send any value , than autmatically set is null */
    	
    	/*
    	 
    	obj.put("separatorsDecimal", 2);
    	obj.put("negativeSymbol", 2);
    	obj.put("displayNegativeSymbolSign", 2);
    	obj.put("separatorsThousands", 3);  
    	
    	*/
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/updateCurrencySetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save currency  Setup
     * @throws Exception
     * Integer parameter set value 0 
     */
    @Test
    public void testCase2UpdateCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "231471");
    	obj.put("currencyDescription", "Test"); 
    	obj.put("currencyDescriptionArabic", "Rabic");
    	obj.put("currencySymbol", "$");
    	obj.put("currencyUnit", "Currency");
    	obj.put("unitSubunitConnector", "subunit connector");   //Integer Value 
    	obj.put("currencySubunit", "name");
    	obj.put("currencyUnitArabic", "nameA");
    	obj.put("unitSubunitConnectorArabic","Arabic");
    	obj.put("currencySubunitArabic","TestArabic");
    	obj.put("includeSpaceAfterCurrencySymbol", true);
    	/* these all mandatory fields default value is null , 
    	if we do not have send any value or 0 , than autmatically set is null  */
    	 
    	obj.put("separatorsDecimal", 0);
    	obj.put("negativeSymbol", 0);
    	obj.put("displayNegativeSymbolSign", 0);
    	obj.put("separatorsThousands", 0);  
    	
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/updateCurrencySetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Save currency  Setup
     * @throws Exception
     * Set Currency Id Empty 
     */
    @Test
    public void testCase3UpdateCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "");
    	obj.put("currencyDescription", "Test"); 
    	obj.put("currencyDescriptionArabic", "Rabic");
    	obj.put("currencySymbol", "$");
    	obj.put("currencyUnit", "Currency");
    	obj.put("unitSubunitConnector", "subunit connector");   //Integer Value 
    	obj.put("currencySubunit", "name");
    	obj.put("currencyUnitArabic", "nameA");
    	obj.put("unitSubunitConnectorArabic","Arabic");
    	obj.put("currencySubunitArabic","TestArabic");
    	obj.put("includeSpaceAfterCurrencySymbol", true);
    	/* these all mandatory fields default value is null , 
    	if we do not have send any value or 0 , than autmatically set is null  */
    	 
    	obj.put("separatorsDecimal", 0);
    	obj.put("negativeSymbol", 0);
    	obj.put("displayNegativeSymbolSign", 0);
    	obj.put("separatorsThousands", 0);  
    	
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/updateCurrencySetup").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
/*********************Test Case for Get Currency Setup by Currency id**************************/
	
    /**
     * Get currency Setup By Currency Id
     * @throws Exception
     */
    
    
    @Test
    public void testCase0GetCurrencySetupByCurrencyId() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "2312");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/getCurrencySetupDetails").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     *  Get currency Setup By Currency Id
     * @throws Exception
     * Language and UserId empty header
     */
    @Test
    public void testCase1GetCurrencySetupByCurrencyId() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "2312");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/getCurrencySetupDetails").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     *  Get currency Setup By Currency Id
     * @throws Exception
     * Currency id empty
     */
    @Test
    public void testCase2GetCurrencySetupByCurrencyId() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("currencyId", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/getCurrencySetupDetails").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
   /*********************Test Case for Search Currency Setup **************************/
	
    /**
     * Search currency Setup 
     * @throws Exception
     */
    
    @Test
    public void testCase0SearchCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "1");
    	obj.put("pageNumber", 0); 
    	obj.put("pageSize", 10);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/searchCurrencySetups").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search currency Setup 
     * @throws Exception
     * Remove Pagination
     */
    @Test
    public void testCase1SearchCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "1");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/searchCurrencySetups").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search currency Setup 
     * @throws Exception
     * Blank Pagination
     */
    @Test
    public void testCase2SearchCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "1");
    	obj.put("pageNumber", ""); 
    	obj.put("pageSize", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/searchCurrencySetups").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search currency Setup 
     * @throws Exception
     * Blank Pagination
     * Remove Pagination and Set keyword blank (Get All Records)
     */
    @Test
    public void testCase3SearchCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/searchCurrencySetups").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search currency Setup 
     * @throws Exception
     * Blank All Parameters (get All records)
     * 
     */
    @Test
    public void testCase4SearchCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", ""); 
    	obj.put("pageSize", "");
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/searchCurrencySetups").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Search currency Setup 
     * @throws Exception
     * Empty Header parameters
     * 
     */
    @Test
    public void testCase5SearchCurrencySetup() throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	obj.put("searchKeyword", "");
    	obj.put("pageNumber", 0); 
    	obj.put("pageSize", 10);
    	
    	String jsonString = obj.toJSONString();
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "").body(jsonString)
               .when()
               .post(baseUrl+"setup/generalLedger/searchCurrencySetups").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    //*************************test case for get master data for currency setup******************//
    
    /**
     * Master data currency setup (Get Negative Symbols)
     * @throws Exception
     */
    @Test
    public void testCase0GetNegativeSymbolTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .get(baseUrl+"getNegativeSymbolTypes").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Master data currency setup (Get Negative Symbols) 
     * @throws Exception
     * Empty Header parameters
     * 
     */
    @Test
    public void testCase1GetNegativeSymbolTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .get(baseUrl+"getNegativeSymbolTypes").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Master data currency setup (Get Negative Symbols Sign Types)
     * @throws Exception
     */
    @Test
    public void testCase0GetNegativeSymbolSignTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .get(baseUrl+"getNegativeSymbolSignTypes").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Master data currency setup (Get Negative Symbols Sign Types)
     * @throws Exception
     * Empty Header parameters
     * 
     */
    @Test
    public void testCase1GetNegativeSymbolSignTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .get(baseUrl+"getNegativeSymbolSignTypes").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Master data currency setup (Get Seperator Decimal Types)
     * @throws Exception
     */
    @Test
    public void testCase0GetSeperatorDecimalTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "1")
               .header("userid", "1")
               .when()
               .get(baseUrl+"getSeperatorDecimalTypesList").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
     * Master data currency setup (Get Seperator Decimal Types)
     * @throws Exception
     * Empty Header parameters
     * 
     */
    @Test
    public void testCase1GetSeperatorDecimalTypes() throws Exception 
    {
        Response response= given()
               .header("Content-Type","application/json")
               .header("langid", "")
               .header("userid", "")
               .when()
               .get(baseUrl+"getSeperatorDecimalTypesList").
                andReturn();
         JsonPath jsonpath =response.getBody().jsonPath();
         System.out.println("Code="+jsonpath.getString("code").toString());
         System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
    }
    
    /**
    * Master data currency setup (Get Seperator Thousands Types)
    * @throws Exception
    */
   @Test
   public void testCase0GetSeperatorThousandsTypes() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1")
              .when()
              .get(baseUrl+"getSeperatorThousandsTypesList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Master data currency setup (Get Seperator Thousands Types)
    * @throws Exception
    * Empty Header parameters
    * 
    */
   @Test
   public void testCase1GetSeperatorThousandsTypes() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "")
              .when()
              .get(baseUrl+"getSeperatorThousandsTypesList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
 //************************* Test Case For Save Exchange Table Setup Header******************//
   
   /**
    * Currency Exchange Header
    * @throws Exception
    */
   @Test
   public void testCase0SaveCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "85234"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "10.2");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Currency Exchange Header
    * @throws Exception
    * Exchange Id Empty
    */
   @Test
   public void testCase1SaveCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", ""); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "10.2");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Currency Exchange Header
    * @throws Exception
    * Rate variance  Empty
    */
   @Test
   public void testCase2SaveCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "asdef"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Currency Exchange Header
    * @throws Exception
    * Currency Id  Empty
    */
   @Test
   public void testCase3SaveCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "asdefg"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "10.0");
   	   obj.put("currencyId", ""); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Currency Exchange Header
    * @throws Exception
    * Language and user Id empty in header
    */
   @Test
   public void testCase4SaveCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "asdefg"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "10.0");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
  //************************* Test Case For Update Exchange Table Setup Header******************//
   /**
    * Update Currency Exchange Header
    * @throws Exception
    */
   @Test
   public void testCase0UpdateCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "85234"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "10.2");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Header
    * @throws Exception
    * Exchange Id Empty
    */
   @Test
   public void testCase1UpdateCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", ""); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "10.2");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Header
    * @throws Exception
    * Rate variance  Empty
    */
   @Test
   public void testCase2UpdateCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "asdef"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Header
    * @throws Exception
    * Currency Id  Empty
    */
   @Test
   public void testCase3UpdateCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "asdefg"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "10.0");
   	   obj.put("currencyId", ""); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Header
    * @throws Exception
    * Language and user Id empty in header
    */
   @Test
   public void testCase4UpdateCurrencyExchangeHeader() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "asdefg"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRateSource", "Test aa"); 
	   obj.put("exchangeRateSourceArabic", "Test aaa");
	   obj.put("rateFrequency", 1); // Integer Foreign Key if value 0 than set null   
   	   obj.put("rateVariance", "10.0");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   //************************* Test Case For Get Exchange Table Setup Header by exchangeId******************//
   
   /**
    * Get Currency Exchange Header by exchange id
    * @throws Exception
    */
   @Test
   public void testCase0GetCurrencyExchangeHeaderByExchangeId() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "85234"); //create Unique
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Get Currency Exchange Header by exchange id
    * @throws Exception
    * exchange Id empty
    */
   @Test
   public void testCase1GetCurrencyExchangeHeaderByExchangeId() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", ""); //create Unique
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Get Currency Exchange Header by exchange id
    * @throws Exception
    * Language and user id empty in header
    */
   @Test
   public void testCase2GetCurrencyExchangeHeaderByExchangeId() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "85234"); //create Unique
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Search Currency Exchange setup header **************************/
	
   /**
    * Search Currency Exchange setup header
    * @throws Exception
    */
   
   @Test
   public void testCase0CurrencyExchangeSetupHeader() throws Exception 
   {
   	JSONObject obj = new JSONObject();
   	obj.put("searchKeyword", "1");
   	obj.put("pageNumber", 0); 
   	obj.put("pageSize", 10);
   	
   	String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Currency Exchange setup header
    * @throws Exception
    * Remove Pagination
    */
   @Test
   public void testCase1CurrencyExchangeSetupHeader() throws Exception 
   {
   	JSONObject obj = new JSONObject();
   	obj.put("searchKeyword", "1");
   	
   	String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Currency Exchange setup header
    * @throws Exception
    * Blank Pagination
    */
   @Test
   public void testCase2CurrencyExchangeSetupHeader() throws Exception 
   {
   	JSONObject obj = new JSONObject();
   	obj.put("searchKeyword", "1");
   	obj.put("pageNumber", ""); 
   	obj.put("pageSize", "");
   	
   	String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Currency Exchange setup header
    * @throws Exception
    * Blank Pagination
    * Remove Pagination and Set keyword blank (Get All Records)
    */
   @Test
   public void testCase3CurrencyExchangeSetupHeader() throws Exception 
   {
   	JSONObject obj = new JSONObject();
   	obj.put("searchKeyword", "");
   	String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Currency Exchange setup header
    * @throws Exception
    * Blank All Parameters (get All records)
    * 
    */
   @Test
   public void testCase4CurrencyExchangeSetupHeader() throws Exception 
   {
   	JSONObject obj = new JSONObject();
   	obj.put("searchKeyword", "");
   	obj.put("pageNumber", ""); 
   	obj.put("pageSize", "");
   	
   	String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Currency Exchange setup header 
    * @throws Exception
    * Empty Header parameters
    * 
    */
   @Test
   public void testCase5CurrencyExchangeSetupHeader() throws Exception 
   {
   	JSONObject obj = new JSONObject();
   	obj.put("searchKeyword", "");
   	obj.put("pageNumber", 0); 
   	obj.put("pageSize", 10);
   	
   	String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeHeaderSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /*********************Test Case  Get master data for Currency Exchange setup header **************************/
	
   /**
    * Master List for Search Currency Exchange setup header (Rate Frequency)
    * @throws Exception
    */
   
   @Test
   public void testCase0GetRateFrequencyList() throws Exception 
   {
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1")
	              .when()
	              .get(baseUrl+"getRateFrequencyTypes").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Master List for Search Currency Exchange setup header (Rate Frequency)
    * @throws Exception
    * Empty  header language and user Id
    */
   
   @Test
   public void testCase1GetRateFrequencyList() throws Exception 
   {
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "")
	              .when()
	              .get(baseUrl+"getRateFrequencyTypes").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
//************************* Test Case For Save Currency Exchange Detail******************//
   
   /**
    * Save Currency Exchange Detail
    * @throws Exception
    */
   @Test
   public void testCase0SaveCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "85234"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "30/12/217");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Save Currency Exchange Detail
    * @throws Exception
    * Empty exchange Id
    */
   @Test
   public void testCase1SaveCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", ""); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "30/12/217");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Save Currency Exchange Detail
    * @throws Exception
    * Empty exchange Rate
    */
   @Test
   public void testCase2SaveCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "1234789"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", ""); 
	   obj.put("exchangeDate", "30/12/217");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Save Currency Exchange Detail
    * @throws Exception
    * Empty exchange Date
    */
   @Test
   public void testCase3SaveCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "123478d9"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Save Currency Exchange Detail
    * @throws Exception
    * Empty exchange Time
    */
   @Test
   public void testCase4SaveCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "123478d9101"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "30/12/2017");
	   obj.put("exchangeTime", ""); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Save Currency Exchange Detail
    * @throws Exception
    * Empty exchange expiration date
    */
   @Test
   public void testCase5SaveCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "123478d91011"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "30/12/2017");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
//************************* Test Case For Update Currency Exchange Detail******************//
   
   /**
    * Update Currency Exchange Detail
    * @throws Exception
    */
   @Test
   public void testCase0UpdateCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "85234"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "30/12/217");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Detail
    * @throws Exception
    * Empty exchange Id
    */
   @Test
   public void testCase1UpdateCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", ""); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "30/12/217");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Detail
    * @throws Exception
    * Empty exchange Rate
    */
   @Test
   public void testCase2UpdateCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "1234789"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", ""); 
	   obj.put("exchangeDate", "30/12/217");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Detail
    * @throws Exception
    * Empty exchange Date
    */
   @Test
   public void testCase3UpdateCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "123478d9"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Detail
    * @throws Exception
    * Empty exchange Time
    */
   @Test
   public void testCase4UpdateCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "123478d9101"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "30/12/2017");
	   obj.put("exchangeTime", ""); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "30/12/2017");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Update Currency Exchange Detail
    * @throws Exception
    * Empty exchange expiration date
    */
   @Test
   public void testCase5UpdateCurrencyExchangeDetail() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "123478d91011"); //create Unique
   	   obj.put("description", "Test"); 
   	   obj.put("descriptionArabic", "Desc Arb");
   	   obj.put("exchangeRate", "10.2"); 
	   obj.put("exchangeDate", "30/12/2017");
	   obj.put("exchangeTime", "15:00"); // Integer Foreign Key if value 0 than set null   
   	   obj.put("exchangeExpirationDate", "");
   	   obj.put("currencyId", "test-id"); 
	   obj.put("rateCalcMethod", "multiply");
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/updateCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
//************************* Test Case For Get Currency Exchange Detail By exchange id******************//
   
   /**
    * Get Currency Exchange Detail By Exchange id
    * @throws Exception
    */
   @Test
   public void testCase0GetCurrencyExchangeDetailByExchangeId() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "85234"); //create Unique
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Get Currency Exchange Detail By Exchange id
    * @throws Exception
    * Empty Header parameters
    */
   @Test
   public void testCase1GetCurrencyExchangeDetailByExchangeId() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "85234"); //create Unique
   	  
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Get Currency Exchange Detail By Exchange id
    * @throws Exception
    * Empty Exchange Id 
    */
   @Test
   public void testCase2GetCurrencyExchangeDetailByExchangeId() throws Exception 
   {
	   JSONObject obj = new JSONObject();
   	   obj.put("exchangeId", "="); //create Unique
   	  
   	   
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Search Currency Exchange Detail **************************/
	
   /**
    * Search currency Exchange Detail 
    * @throws Exception
    */
   
   @Test
   public void testCase0SearchCurrencyExchangeDetail() throws Exception 
   {
   	JSONObject obj = new JSONObject();
   	obj.put("searchKeyword", "1");
   	obj.put("pageNumber", 0); 
   	obj.put("pageSize", 10);
   	
   	String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeDetail").
               andReturn();
       JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search currency Exchange Detail 
    * @throws Exception
    * Remove Pagination
    */
   @Test
   public void testCase1SearchCurrencyExchangeDetail() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("searchKeyword", "1");
   	
   	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search currency Exchange Detail 
    * @throws Exception
    * Blank Pagination
    */
   @Test
   public void testCase2SearchCurrencyExchangeDetail() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "1");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
	   	
	   	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeDetail").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Search currency Exchange Detail 
    * @throws Exception
    * Blank Pagination
    * Remove Pagination and Set keyword blank (Get All Records)
    */
   @Test
   public void testCase3SearchCurrencyExchangeDetail() throws Exception 
   {
   	 JSONObject obj = new JSONObject();
   	 obj.put("searchKeyword", "");
   	 String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeDetail").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search currency Exchange Detail  
    * @throws Exception
    * Blank All Parameters (get All records)
    * 
    */
   @Test
   public void testCase4SearchCurrencyExchangeDetail() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
	   	
	   	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeDetail").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Search currency Exchange Detail 
    * @throws Exception
    * Empty Header parameters
    * 
    */
   @Test
   public void testCase5SearchCurrencyExchangeDetail() throws Exception 
   {
	   	 JSONObject obj = new JSONObject();
	   	 obj.put("searchKeyword", "");
	   	 obj.put("pageNumber", 0); 
	   	 obj.put("pageSize", 10);
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/searchCurrencyExchangeDetail").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Save Account Type **************************/
	
   /**
    * Save Account Type
    * @throws Exception
    */
   
   @Test
   public void testCase0SaveAccountType() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeName", "Test"); //mandatory
	   	 obj.put("accountTypeNameArabic", "Test Arabic"); 
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAccountType").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Save Account Type
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase1SaveAccountType() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeName", "aa"); //mandatory
	   	 obj.put("accountTypeNameArabic", ""); 
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAccountType").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Save Account Type
    * @throws Exception
    * Empty names
    */
   
   @Test
   public void testCase2SaveAccountType() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeName", "");
	   	 obj.put("accountTypeNameArabic", ""); 
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAccountType").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /*********************Test Case for Update Account Type **************************/
	
   /**
    * Update Account Type
    * @throws Exception
    */
   
   @Test
   public void testCase0UpdateAccountType() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeName", "Test"); //mandatory
	   	 obj.put("accountTypeNameArabic", "Test Arabic"); 
	   	 obj.put("accountTypeId", 1); //Integer Value Pass
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAccountType").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Update Account Type
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase1UpdateAccountType() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeName", "aa"); //mandatory
	   	 obj.put("accountTypeNameArabic", ""); 
	   	 obj.put("accountTypeId", 1); //Integer Value Pass
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAccountType").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Update Account Type
    * @throws Exception
    * Empty names
    */
   
   @Test
   public void testCase2UpdateAccountType() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeName", "");
	   	 obj.put("accountTypeNameArabic", ""); 
	   	 obj.put("accountTypeId", 1); //Integer Value Pass
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAccountType").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /*********************Test Case for Get Account Type By Id **************************/
	
   /**
    * Get Account Type By Id
    * @throws Exception
    */
   
   @Test
   public void testCase0GetAccountTypeById() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeId", 1); //Integer Value Pass
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAccountTypeById").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Get Account Type By Id
    * @throws Exception
    * Account type Id 0
    */
   
   @Test
   public void testCase1GetAccountTypeById() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeId", 0); //Integer Value Pass
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAccountTypeById").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Get Account Type By Id
    * @throws Exception
    * Header parameters empty
    */
   
   @Test
   public void testCase2GetAccountTypeById() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountTypeId", 1); //Integer Value Pass
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAccountTypeById").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /*********************Test Case for Search Account Type **************************/
	
   /**
    * Search account type
    * @throws Exception
    */
   
   @Test
   public void testCase0SearchAccountType() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("searchKeyword", "1");
   	  obj.put("pageNumber", 0); 
   	  obj.put("pageSize", 10);
   	
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/accountTypeList").
               andReturn();
       JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search account type
    * @throws Exception
    * Remove Pagination
    */
   @Test
   public void testCase1SearchAccountType() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("searchKeyword", "1");
   	
   	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/accountTypeList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search account type 
    * @throws Exception
    * Blank Pagination
    */
   @Test
   public void testCase2SearchAccountType() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "1");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
	   	
	   	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/accountTypeList").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Search account type
    * @throws Exception
    * Blank Pagination
    * Remove Pagination and Set keyword blank (Get All Records)
    */
   @Test
   public void testCase3SearchAccountType() throws Exception 
   {
   	 JSONObject obj = new JSONObject();
   	 obj.put("searchKeyword", "");
   	 String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/accountTypeList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search account type  
    * @throws Exception
    * Blank All Parameters (get All records)
    * 
    */
   @Test
   public void testCase4SearchAccountType() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
	   	
	   	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/accountTypeList").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Search account type
    * @throws Exception
    * Empty Header parameters
    * 
    */
   @Test
   public void testCase5SearchAccountType() throws Exception 
   {
	   	 JSONObject obj = new JSONObject();
	   	 obj.put("searchKeyword", "");
	   	 obj.put("pageNumber", 0); 
	   	 obj.put("pageSize", 10);
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/accountTypeList").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Save Account Category **************************/
	
   /**
    * Save Account Category
    * @throws Exception
    */
   
   @Test
   public void testCase0SaveAccountCategory() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryDescription", "Test"); //mandatory
	   	 obj.put("accountCategoryDescriptionArabic", "Test Arabic"); 
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAccountCategory").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Save Account Type
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase1SaveAccountCategory() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryDescription", "aa"); //mandatory
	   	 obj.put("accountCategoryDescriptionArabic", ""); 
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAccountCategory").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Save Account Type
    * @throws Exception
    * Empty Description
    */
   
   @Test
   public void testCase2SaveAccountCategory() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryDescription", "");
	   	 obj.put("accountCategoryDescriptionArabic", ""); 
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAccountCategory").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /*********************Test Case for Update Account Category **************************/
	
   /**
    * Update Account Category
    * @throws Exception
    */
   
   @Test
   public void testCase0UpdateAccountCategory() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryDescription", "Test"); //mandatory
	   	 obj.put("accountCategoryDescriptionArabic", "Test Arabic"); 
	   	 obj.put("accountCategoryId", 1); 
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAccountCategory").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Update Account Type
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase1UpdateAccountCategory() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryDescription", "aa"); //mandatory
	   	 obj.put("accountCategoryDescriptionArabic", ""); 
	   	 obj.put("accountCategoryId", 1); 
	   	
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAccountCategory").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Update Account Type
    * @throws Exception
    * Empty Description
    */
   
   @Test
   public void testCase2UpdateAccountCategory() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryDescription", "");
	   	 obj.put("accountCategoryDescriptionArabic", ""); 
	   	 obj.put("accountCategoryId", 1); 
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAccountCategory").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /*********************Test Case for Get Account Category by Category Id **************************/
	
   /**
    * Get Account Category By Category Id
    * @throws Exception
    */
   
   @Test
   public void testCase0GetAccountCategoryByCategoryId() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryId", 1); //Integer
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAccountCategoryById").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Get Account Category By Category Id
    * @throws Exception
    * Set 0 category id
    */
   
   @Test
   public void testCase1GetAccountCategoryByCategoryId() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryId", 0); //Integer
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAccountCategoryById").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /**
    * Get Account Category By Category Id
    * @throws Exception
    * Empty Header parameters
    */
   
   @Test
   public void testCase2GetAccountCategoryByCategoryId() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("accountCategoryId", 0); //Integer
	     String jsonString = obj.toJSONString();
	     Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAccountCategoryById").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	      System.out.println("Code="+jsonpath.getString("code").toString());
	      System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	      assert response.statusCode()==200;
   }
   
   /*********************Test Case for Search Account Category **************************/
	
   /**
    * Search account Category
    * @throws Exception
    */
   
   @Test
   public void testCase0SearchAccountCategory() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("searchKeyword", "1");
   	  obj.put("pageNumber", 0); 
   	  obj.put("pageSize", 10);
   	
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/accountCategoryList").
               andReturn();
       JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search account Category
    * @throws Exception
    * Remove Pagination
    */
   @Test
   public void testCase1SearchAccountCategory() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("searchKeyword", "1");
   	
   	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/accountCategoryList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search account Category 
    * @throws Exception
    * Blank Pagination
    */
   @Test
   public void testCase2SearchAccountCategory() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "1");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
	   	
	   	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/accountCategoryList").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Search account Category
    * @throws Exception
    * Blank Pagination
    * Remove Pagination and Set keyword blank (Get All Records)
    */
   @Test
   public void testCase3SearchAccountCategory() throws Exception 
   {
   	 JSONObject obj = new JSONObject();
   	 obj.put("searchKeyword", "");
   	 String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/accountCategoryList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search account Category  
    * @throws Exception
    * Blank All Parameters (get All records)
    * 
    */
   @Test
   public void testCase4SearchAccountCategory() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
	   	
	   	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/accountCategoryList").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Search account Category
    * @throws Exception
    * Empty Header parameters
    * 
    */
   @Test
   public void testCase5SearchAccountCategory() throws Exception 
   {
	   	 JSONObject obj = new JSONObject();
	   	 obj.put("searchKeyword", "");
	   	 obj.put("pageNumber", 0); 
	   	 obj.put("pageSize", 10);
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/accountCategoryList").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Save Audit Trial Code **************************/
	
   /**
    * Save Audit trail code
    * @throws Exception
    */
   
   @Test
   public void testCase0SaveAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("sourceCode", "Test");
	   	 obj.put("seriesId", 1); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
	   
   }
   
   /**
    * Save Audit trail code
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase1SaveAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("sourceCode", "Test");
	   	 obj.put("seriesId", 1); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
	   
   }
   
   /**
    * Save Audit trail code
    * @throws Exception
    * Set 0 Series Id
    */
   
   @Test
   public void testCase2SaveAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("sourceCode", "Test");
	   	 obj.put("seriesId", 0); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Save Audit trail code
    * @throws Exception
    * Empty Source Code
    */
   
   @Test
   public void testCase3SaveAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	   	 obj.put("sourceCode", "");
	   	 obj.put("seriesId", 1); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Update Audit Trial Code **************************/
	
   /**
    * Update Audit trail code
    * @throws Exception
    */
   
   @Test
   public void testCase0UpdateAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	     obj.put("seriesIndex", "1");
	   	 obj.put("sourceCode", "Test");
	   	 obj.put("seriesId", 1); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
	   
   }
   
   /**
    * Save Audit trail code
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase1UpdateAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	     obj.put("seriesIndex", "1");
	   	 obj.put("sourceCode", "Test");
	   	 obj.put("seriesId", 1); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
	   
   }
   
   /**
    * Save Audit trail code
    * @throws Exception
    * Set 0 Series Id
    */
   
   @Test
   public void testCase2UpdateAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	     obj.put("seriesIndex", "1");
	   	 obj.put("sourceCode", "Test");
	   	 obj.put("seriesId", 0); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Save Audit trail code
    * @throws Exception
    * Empty Source Code
    */
   
   @Test
   public void testCase3UpdateAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	     obj.put("seriesIndex", "1");
	   	 obj.put("sourceCode", "");
	   	 obj.put("seriesId", 1); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Save Audit trail code
    * @throws Exception
    * Empty Series Index 
    */
   
   @Test
   public void testCase4UpdateAuditTrailCode() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	     obj.put("seriesIndex", "");
	   	 obj.put("sourceCode", "ss");
	   	 obj.put("seriesId", 1); //Integer
	   	 obj.put("sourceDocument", "Doc");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Get Audit Trial Code By Id**************************/
	
   /**
    * Get Audit trail code By Id
    * @throws Exception
    */
   
   @Test
   public void testCase0GetAuditTrailCodeById() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	     obj.put("seriesIndex", "1");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAuditTrialCodeById").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
	   
   }
   
   /**
    * Get Audit trail code By Id
    * @throws Exception
    * Empty series Index
    */
   
   @Test
   public void testCase1GetAuditTrailCodeById() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	     obj.put("seriesIndex", "");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAuditTrialCodeById").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
	   
   }
   
   /**
    * Get Audit trail code By Id
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase2GetAuditTrailCodeById() throws Exception 
   {
	     JSONObject obj = new JSONObject();
	     obj.put("seriesIndex", "1");
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getAuditTrialCodeById").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Search Audit Trail Code  **************************/
	
   /**
    * Search Audit Trail Code
    * @throws Exception
    */
   
   @Test
   public void testCase0SearchAuditTrailCode() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("searchKeyword", "1");
   	  obj.put("pageNumber", 0); 
   	  obj.put("pageSize", 10);
   	
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchAuditTrialCode").
               andReturn();
       JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Audit Trail Code
    * @throws Exception
    * Remove Pagination
    */
   @Test
   public void testCase1SearchAuditTrailCode() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("searchKeyword", "1");
   	
   	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchAuditTrialCode").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Audit Trail Code
    * @throws Exception
    * Blank Pagination
    */
   @Test
   public void testCase2SearchAuditTrailCode() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "1");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
	   	
	   	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/searchAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Search Audit Trail Code
    * @throws Exception
    * Blank Pagination
    * Remove Pagination and Set keyword blank (Get All Records)
    */
   @Test
   public void testCase3SearchAuditTrailCode() throws Exception 
   {
   	 JSONObject obj = new JSONObject();
   	 obj.put("searchKeyword", "");
   	 String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchAuditTrialCode").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Audit Trail Code
    * @throws Exception
    * Blank All Parameters (get All records)
    * 
    */
   @Test
   public void testCase4SearchAuditTrailCode() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
	   	
	   	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/searchAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Search Audit Trail Code
    * @throws Exception
    * Empty Header parameters
    * 
    */
   @Test
   public void testCase5SearchAuditTrailCode() throws Exception 
   {
	   	 JSONObject obj = new JSONObject();
	   	 obj.put("searchKeyword", "");
	   	 obj.put("pageNumber", 0); 
	   	 obj.put("pageSize", 10);
	   	
	    	String jsonString = obj.toJSONString();
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/searchAuditTrialCode").
	               andReturn();
	        JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Save and Update General Ledger Setup  **************************/
	
   /**
    * Save and Update General Ledger Setup
    * @throws Exception
    */
   
   @Test
   public void testCase0SavAndUpdateGeneralLedgerSetup() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
      obj.put("nextJournalEntry", 1);//Integer
 	  obj.put("nextBudgetJournalEntry", 1); //Integer
 	  obj.put("nextReconciliation", 1);//Integer
 	  obj.put("displayNCorPB", 0);//Integer  Display Id (Master data)
	  obj.put("retainedEraningsCheckBox", true); 
	  obj.put("accountSegments", 1);//Integer
  	  obj.put("mainAccount1", "1001");
  	  obj.put("mainAccount2", "1101"); 
  	  obj.put("maintainHistoryAccounts", true);
  	  obj.put("maintainHistoryTransactions", true);
  	  obj.put("maintainHistoryBudgetTransactions", true); 
  	  obj.put("allowPostingToHistory", true);
  	  obj.put("allowDeletionOfSavedTrans",true);
  	  obj.put("allowvoidingCorrectingSubTrans", true); 
  	  obj.put("userDefine1", "");
  	  obj.put("userDefine2", "");
  	  obj.put("userDefine3", ""); 
  	  obj.put("userDefine4", "");
  	  obj.put("userDefine5", "");
   	
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/generalLedgerSetup").
               andReturn();
       JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Save and Update General Ledger Setup
    * @throws Exception
    * Set Display Id 0
    */
   
   @Test
   public void testCase1SavAndUpdateGeneralLedgerSetup() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("nextJournalEntry", 1);//Integer
   	  obj.put("nextBudgetJournalEntry", 1); //Integer
   	  obj.put("nextReconciliation", 1);//Integer
   	  obj.put("displayNCorPB", 0);//Integer  Display Id (Master data)
  	  obj.put("retainedEraningsCheckBox", true); 
  	  obj.put("accountSegments", 1);//Integer
  	  obj.put("mainAccount1", "1001");
  	  obj.put("mainAccount2", "1101"); 
  	  obj.put("maintainHistoryAccounts", true);
  	  obj.put("maintainHistoryTransactions", true);
  	  obj.put("maintainHistoryBudgetTransactions", true); 
  	  obj.put("allowPostingToHistory", true);
  	  obj.put("allowDeletionOfSavedTrans",true);
  	  obj.put("allowvoidingCorrectingSubTrans", true); 
  	  obj.put("userDefine1", "");
  	  obj.put("userDefine2", "");
  	  obj.put("userDefine3", ""); 
  	  obj.put("userDefine4", "");
  	  obj.put("userDefine5", "");
   	
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/generalLedgerSetup").
               andReturn();
       JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Save and Update General Ledger Setup
    * @throws Exception
    * Set Parameters value False
    */
   
   @Test
   public void testCase2SavAndUpdateGeneralLedgerSetup() throws Exception 
   {
   	  JSONObject obj = new JSONObject();
   	  obj.put("nextJournalEntry", 1);//Integer
   	  obj.put("nextBudgetJournalEntry", 2); //Integer
   	  obj.put("nextReconciliation", 2);//Integer
   	  obj.put("displayNCorPB", 1);//Integer  Display Id (Master data)
  	  obj.put("retainedEraningsCheckBox", false); 
  	  obj.put("accountSegments", 1);//Integer
  	  obj.put("mainAccount1", "1001");
  	  obj.put("mainAccount2", "1101"); 
  	  obj.put("maintainHistoryAccounts", false);
  	  obj.put("maintainHistoryTransactions", false);
  	  obj.put("maintainHistoryBudgetTransactions", false); 
  	  obj.put("allowPostingToHistory", false);
  	  obj.put("allowDeletionOfSavedTrans",false);
  	  obj.put("allowvoidingCorrectingSubTrans", false); 
  	  obj.put("userDefine1", "");
  	  obj.put("userDefine2", "");
  	  obj.put("userDefine3", ""); 
  	  obj.put("userDefine4", "");
  	  obj.put("userDefine5", "");
   	
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/generalLedgerSetup").
               andReturn();
       JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Get General Ledger Setup  **************************/
	
   /**
    * Get General Ledger Setup
    * @throws Exception
    */
   
   @Test
   public void testCase0GetGeneralLedgerSetup() throws Exception 
   {
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1")
	              .when()
	              .get(baseUrl+"setup/generalLedger/generalLedgerSetup/get").
	               andReturn();
	       JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
	   
   }
   
   /**
    * Get General Ledger Setup
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase1GetGeneralLedgerSetup() throws Exception 
   {
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "")
	              .when()
	              .get(baseUrl+"setup/generalLedger/generalLedgerSetup/get").
	               andReturn();
	       JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
	   
   }
   
   /*********************Test Case for Get Display Type (master Data general ledger setup) **************************/
	
   /**
    * Get Display type master data (General Ledger Setup)
    * @throws Exception
    */
   
   @Test
   public void testCase0GetDisplayType() throws Exception 
   {
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1")
	              .when()
	              .get(baseUrl+"setup/generalLedger/generalLedgerSetup/displayType").
	               andReturn();
	       JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Get Display type master data (General Ledger Setup)
    * @throws Exception
    * Empty header parameters
    */
   
   @Test
   public void testCase1GetDisplayType() throws Exception 
   {
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "")
	              .when()
	              .get(baseUrl+"setup/generalLedger/generalLedgerSetup/displayType").
	               andReturn();
	       JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Save and Update Fiscal Finance Period setup  **************************/
	
   /**
    *  Save and Update Fiscal Finance Period setup
    * @throws Exception
    */
   
   @Test
   public void testCase0SavAndUpdateFiscalFinanacePeriodSetup() throws Exception 
   {
	  List<DtoFiscalFinancialPeriodSetupDetail> list = new ArrayList<>();
   	  JSONObject obj = new JSONObject();
      obj.put("year", "2018");
 	  obj.put("firstDay", "20/08/2017"); 
 	  obj.put("lastDay", "21/08/2017");
 	  obj.put("isHistoricalYear", true);
	  obj.put("numberOfPeriods", 3); 
	  DtoFiscalFinancialPeriodSetupDetail dtoFiscalFinancialPeriodSetupDetail = new DtoFiscalFinancialPeriodSetupDetail();
	  dtoFiscalFinancialPeriodSetupDetail.setTransactionDocumentDescriptionPrimary("Testa");
	  dtoFiscalFinancialPeriodSetupDetail.setTransactionDocumentDescriptionSecondary("Test rabic");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodId(1);
	  dtoFiscalFinancialPeriodSetupDetail.setStartPeriodDate("10/12/2017");
	  dtoFiscalFinancialPeriodSetupDetail.setEndPeriodDate("11/12/2017");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodNamePrimary("primary Name");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodNameSecondary("Secondary");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesFinancial(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesSales(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesPurchase(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesInventory(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesProject(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesPayroll(true);
	  list.add(dtoFiscalFinancialPeriodSetupDetail);
	  obj.put("dtoFiscalFinancialPeriodSetupDetail", list);
  	 
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/fiscalFinancePeriod/setup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    *  Save and Update Fiscal Finance Period setup
    * @throws Exception
    * Empty FirstDay
    */
   
   @Test
   public void testCase1SavAndUpdateFiscalFinanacePeriodSetup() throws Exception 
   {
	  List<DtoFiscalFinancialPeriodSetupDetail> list = new ArrayList<>();
   	  JSONObject obj = new JSONObject();
      obj.put("year", "2018");
 	  obj.put("firstDay", ""); 
 	  obj.put("lastDay", "21/08/2017");
 	  obj.put("isHistoricalYear", true);
	  obj.put("numberOfPeriods", 3); 
	  DtoFiscalFinancialPeriodSetupDetail dtoFiscalFinancialPeriodSetupDetail = new DtoFiscalFinancialPeriodSetupDetail();
	  dtoFiscalFinancialPeriodSetupDetail.setTransactionDocumentDescriptionPrimary("Testa");
	  dtoFiscalFinancialPeriodSetupDetail.setTransactionDocumentDescriptionSecondary("Test rabic");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodId(1);
	  dtoFiscalFinancialPeriodSetupDetail.setStartPeriodDate("10/12/2017");
	  dtoFiscalFinancialPeriodSetupDetail.setEndPeriodDate("11/12/2017");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodNamePrimary("primary Name");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodNameSecondary("Secondary");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesFinancial(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesSales(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesPurchase(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesInventory(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesProject(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesPayroll(true);
	  list.add(dtoFiscalFinancialPeriodSetupDetail);
	  obj.put("dtoFiscalFinancialPeriodSetupDetail", list);
  	 
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/fiscalFinancePeriod/setup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    *  Save and Update Fiscal Finance Period setup
    * @throws Exception
    * Empty Start period date
    */
   
   @Test
   public void testCase2SavAndUpdateFiscalFinanacePeriodSetup() throws Exception 
   {
	  List<DtoFiscalFinancialPeriodSetupDetail> list = new ArrayList<>();
   	  JSONObject obj = new JSONObject();
      obj.put("year", "2018");
 	  obj.put("firstDay", "21/08/2017"); 
 	  obj.put("lastDay", "21/08/2017");
 	  obj.put("isHistoricalYear", true);
	  obj.put("numberOfPeriods", 3); 
	  DtoFiscalFinancialPeriodSetupDetail dtoFiscalFinancialPeriodSetupDetail = new DtoFiscalFinancialPeriodSetupDetail();
	  dtoFiscalFinancialPeriodSetupDetail.setTransactionDocumentDescriptionPrimary("Testa");
	  dtoFiscalFinancialPeriodSetupDetail.setTransactionDocumentDescriptionSecondary("Test rabic");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodId(1);
	  dtoFiscalFinancialPeriodSetupDetail.setStartPeriodDate("");
	  dtoFiscalFinancialPeriodSetupDetail.setEndPeriodDate("11/12/2017");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodNamePrimary("primary Name");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodNameSecondary("Secondary");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesFinancial(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesSales(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesPurchase(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesInventory(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesProject(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesPayroll(true);
	  list.add(dtoFiscalFinancialPeriodSetupDetail);
	  obj.put("dtoFiscalFinancialPeriodSetupDetail", list);
  	 
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/fiscalFinancePeriod/setup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    *  Save and Update Fiscal Finance Period setup
    * @throws Exception
    * Empty End period date
    */
   
   @Test
   public void testCase3SavAndUpdateFiscalFinanacePeriodSetup() throws Exception 
   {
	  List<DtoFiscalFinancialPeriodSetupDetail> list = new ArrayList<>();
   	  JSONObject obj = new JSONObject();
      obj.put("year", "2018");
 	  obj.put("firstDay", "21/08/2017"); 
 	  obj.put("lastDay", "21/08/2017");
 	  obj.put("isHistoricalYear", true);
	  obj.put("numberOfPeriods", 3); 
	  DtoFiscalFinancialPeriodSetupDetail dtoFiscalFinancialPeriodSetupDetail = new DtoFiscalFinancialPeriodSetupDetail();
	  dtoFiscalFinancialPeriodSetupDetail.setTransactionDocumentDescriptionPrimary("Testa");
	  dtoFiscalFinancialPeriodSetupDetail.setTransactionDocumentDescriptionSecondary("Test rabic");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodId(1);
	  dtoFiscalFinancialPeriodSetupDetail.setStartPeriodDate("11/12/2017");
	  dtoFiscalFinancialPeriodSetupDetail.setEndPeriodDate("");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodNamePrimary("primary Name");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodNameSecondary("Secondary");
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesFinancial(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesSales(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesPurchase(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesInventory(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesProject(true);
	  dtoFiscalFinancialPeriodSetupDetail.setPeriodSeriesPayroll(true);
	  list.add(dtoFiscalFinancialPeriodSetupDetail);
	  obj.put("dtoFiscalFinancialPeriodSetupDetail", list);
  	 
   	  String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/fiscalFinancePeriod/setup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Get Fiscal Finance Period setup  **************************/
	
   /**
    *  Get Fiscal Finance Period setup
    * @throws Exception
    */
   
   @Test
   public void testCase0GetFiscalFinanacePeriodSetup() throws Exception 
   {
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1")
	              .when()
	              .get(baseUrl+"setup/generalLedger/fiscalFinancePeriod/get").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    *  Get Fiscal Finance Period setup
    * @throws Exception
    * Blank header parameters
    */
   
   @Test
   public void testCase1GetFiscalFinanacePeriodSetup() throws Exception 
   {
	       Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "")
	              .when()
	              .get(baseUrl+"setup/generalLedger/fiscalFinancePeriod/get").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Save Checkbook maintenance  **************************/
	
   /**
    * Save checkbook maintenance
    * @throws Exception
    */
   
   @Test
   public void testCase0SaveCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "12345");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "21/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "123456"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveCheckBookMaintenance").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Save checkbook maintenance
    * @throws Exception
    * Empty glAccount Id
    */
   
   @Test
   public void testCase1SaveCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "123456");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", ""); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "21/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "123456"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveCheckBookMaintenance").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Save checkbook maintenance
    * @throws Exception
    * Empty Currency  Id
    */
   
   @Test
   public void testCase2SaveCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "1234565");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", ""); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "21/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "123456"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveCheckBookMaintenance").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Save checkbook maintenance
    * @throws Exception
    * Empty last reconciled date
    */
   
   @Test
   public void testCase3SaveCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "123dd565");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", ""); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "123456"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveCheckBookMaintenance").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Save checkbook maintenance
    * @throws Exception
    * Empty last reconciled Balance
    */
   
   @Test
   public void testCase4SaveCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "123ddd565");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "20/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "20"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveCheckBookMaintenance").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Save checkbook maintenance
    * @throws Exception
    * Empty cash account balance
    */
   
   @Test
   public void testCase5SaveCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "123ddd565");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "20/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", ""); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/saveCheckBookMaintenance").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /********************* Test Case for Update Checkbook maintenance  **************************/
	
   /**
    * Update checkbook maintenance
    * @throws Exception
    */
   
   @Test
   public void testCase0UpdateCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "12345");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "21/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "123456"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateCheckBookMaintenanceByCheckbookId").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Update checkbook maintenance
    * @throws Exception
    * Empty glAccount Id
    */
   
   @Test
   public void testCase1UpdateCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "123456");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", ""); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "21/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "123456"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateCheckBookMaintenanceByCheckbookId").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Update checkbook maintenance
    * @throws Exception
    * Empty Currency  Id
    */
   
   @Test
   public void testCase2UpdateCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "1234565");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", ""); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "21/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "123456"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateCheckBookMaintenanceByCheckbookId").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Update checkbook maintenance
    * @throws Exception
    * Empty last reconciled date
    */
   
   @Test
   public void testCase3UpdateCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "123dd565");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", ""); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "123456"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateCheckBookMaintenanceByCheckbookId").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Update checkbook maintenance
    * @throws Exception
    * Empty last reconciled Balance
    */
   
   @Test
   public void testCase4UpdateCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "123ddd565");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "20/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", "20"); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateCheckBookMaintenanceByCheckbookId").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Update checkbook maintenance
    * @throws Exception
    * Empty cash account balance
    */
   
   @Test
   public void testCase5UpdateCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "123ddd565");
	 	  obj.put("inactive", false); 
	 	  obj.put("checkbookDescription", "test");
	 	  obj.put("checkbookDescriptionArabic", "test Arabic");
		  obj.put("currencyId", "test-id"); 
		  obj.put("glAccountId", "1"); 
	 	  obj.put("nextCheckNumber", 2);
	 	  obj.put("nextDepositNumber", 1);
		  obj.put("lastReconciledDate", "20/08/2017"); 
		  obj.put("lastReconciledBalance", "10.0"); 
	 	  obj.put("officialBankAccountNumber", "123456");
	 	  obj.put("bankId", true);
		  obj.put("userDefine1", 3); 
		  obj.put("userDefine2", 3); 
		  obj.put("currentCheckbookBalance", "20.0"); 
		  obj.put("cashAccountBalance", ""); 
		  obj.put("exceedMaxCheckAmount", 20); 
		  obj.put("passwordOfMaxCheckAmount", "123456"); 
		  obj.put("duplicateCheckNumber", false); 
		  obj.put("overrideCheckNumber", true); 
		  
	   String jsonString= obj.toJSONString();
	   Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/updateCheckBookMaintenanceByCheckbookId").
	               andReturn();
	      JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /********************* Test Case for Get Checkbook maintenance  **************************/
	
   /**
    * Get checkbook maintenance
    * @throws Exception
    */
   
   @Test
   public void testCase0GetCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "12345");
	      
	      String jsonString= obj.toJSONString();
	      Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "1")
	              .header("userid", "1").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getCheckBookMaintenanceByCheckbookId").
	               andReturn();
	       JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /**
    * Get checkbook maintenance
    * @throws Exception
    * Blank header parameters
    */
   
   @Test
   public void testCase1GetCheckbookMaintenance() throws Exception 
   {
	      JSONObject obj = new JSONObject();
	      obj.put("checkBookId", "12345");
	      
	      String jsonString= obj.toJSONString();
	      Response response= given()
	              .header("Content-Type","application/json")
	              .header("langid", "")
	              .header("userid", "").body(jsonString)
	              .when()
	              .post(baseUrl+"setup/generalLedger/getCheckBookMaintenanceByCheckbookId").
	               andReturn();
	       JsonPath jsonpath =response.getBody().jsonPath();
	        System.out.println("Code="+jsonpath.getString("code").toString());
	        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
	       assert response.statusCode()==200;
   }
   
   /*********************Test Case for Search Checkbook maintenance **************************/
	
   /**
    * Search checkbook maintenance 
    * @throws Exception
    */
   
   @Test
   public void testCase0SearchCheckBookMaintenance() throws Exception 
   {
   	 JSONObject obj = new JSONObject();
   	 obj.put("searchKeyword", "1");
   	 obj.put("pageNumber", 0); 
   	 obj.put("pageSize", 10);
   	
   	 String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCheckBookMaintenance").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Checkbook maintenance
    * @throws Exception
    * Remove Pagination
    */
   @Test
   public void testCase1SearchCheckBookMaintenance() throws Exception 
   {
   	 JSONObject obj = new JSONObject();
   	 obj.put("searchKeyword", "1");
   	
   	 String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCheckBookMaintenance").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Checkbook maintenance 
    * @throws Exception
    * Blank Pagination
    */
   @Test
   public void testCase2SearchCheckBookMaintenance() throws Exception 
   {
   	   JSONObject obj = new JSONObject();
   	   obj.put("searchKeyword", "1");
   	   obj.put("pageNumber", ""); 
   	   obj.put("pageSize", "");
   	
   	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCurrencySetups").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Search Checkbook maintenance
    * @throws Exception
    * Blank Pagination
    * Remove Pagination and Set keyword blank (Get All Records)
    */
   @Test
   public void testCase3SearchCheckBookMaintenance() throws Exception 
   {
   	 JSONObject obj = new JSONObject();
   	 obj.put("searchKeyword", "");
   	 String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCheckBookMaintenance").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Checkbook maintenance
    * @throws Exception
    * Blank All Parameters (get All records)
    * 
    */
   @Test
   public void testCase4SearchCheckBookMaintenance() throws Exception 
   {
	   	JSONObject obj = new JSONObject();
	   	obj.put("searchKeyword", "");
	   	obj.put("pageNumber", ""); 
	   	obj.put("pageSize", "");
   	
   	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCheckBookMaintenance").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /**
    * Search Checkbook maintenance
    * @throws Exception
    * Empty Header parameters
    * 
    */
   @Test
   public void testCase5SearchCheckBookMaintenance() throws Exception 
   {
   	 JSONObject obj = new JSONObject();
   	 obj.put("searchKeyword", "");
   	 obj.put("pageNumber", 0); 
   	 obj.put("pageSize", 10);
   	
   	 String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/searchCheckBookMaintenance").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
       assert response.statusCode()==200;
   }
   
   /*********************Test case for master data of account structure setup **************************/
	
   /**
    * Master data get main account list ( Account Structure Setup )
    * @throws Exception
    */
   
   @Test
   public void testCase0GetMainAccountList() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1")
              .when()
              .get(baseUrl+"setup/generalLedger/getMainAccountList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Master data get main account list ( Account Structure Setup )
    * @throws Exception 
    * Empty header parameters
    */
   
   @Test
   public void testCase1GetMainAccountList() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "")
              .when()
              .get(baseUrl+"setup/generalLedger/getMainAccountList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Master Data Get financial dimension list ( Account Structure Setup )
    * @throws Exception 
    */
   @Test
   public void testCase0GetFinancialDimensionList() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1")
              .when()
              .get(baseUrl+"setup/generalLedger/getFinancialDimensionsList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Master Data Get financial dimension list ( Account Structure Setup )
    * @throws Exception 
    * Empty header parameters
    */
   @Test
   public void testCase1GetFinancialDimensionList() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "")
              .when()
              .get(baseUrl+"setup/generalLedger/getFinancialDimensionsList").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Master Data Get financial dimension Value By financial dimension id ( Account Structure Setup )
    * @throws Exception 
    */
   @Test
   public void testCase0GetFinancialDimensionValueByFinancialDimensionId() throws Exception 
   {
	   JSONObject obj = new JSONObject();
	   obj.put("dimInxd", "1");
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getFinancialDimensionsValuesByFinancialDimensionId").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Master Data Get financial dimension Value By financial dimension id ( Account Structure Setup )
    * @throws Exception 
    * Empty header parameters
    */
   @Test
   public void testCase1GetFinancialDimensionValueByFinancialDimensionId() throws Exception 
   {
	   JSONObject obj = new JSONObject();
	   obj.put("dimInxd", "1");
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getFinancialDimensionsValuesByFinancialDimensionId").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /*********************Test case for save account structure setup **************************/
	
   /**
    * Save Account Structure Setup )
    * @throws Exception
    */
   
   @Test
   public void testCase0SaveAccountStructureSetup() throws Exception 
   {
	   DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dtoObj = new DtoGLConfigurationRelationBetweenDimensionsAndMainAccount();
	   dtoObj.setCoaFinancialDimensionsIndexId(38);
	   dtoObj.setCoaFinancialDimensionsFromIndexValue(1L);
	   dtoObj.setCoaFinancialDimensionsToIndexValue(1L);
	   List<DtoGLConfigurationRelationBetweenDimensionsAndMainAccount> list = new ArrayList<>();
	   list.add(dtoObj);
	   JSONObject obj = new JSONObject();
	   obj.put("segmentNumber", "10");
	   obj.put("isMainAccount", "1");
	   obj.put("coaMainAccountsFromActIndexId", "1");
	   obj.put("coaMainAccountsToActIndexId", "1");
	   obj.put("dimensionList",list);
	  
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveAccountStructureSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   
   /**
    * Save Account Structure Setup )
    * @throws Exception
    * Skip Some Parameters (Value set null automatically)
    */
   
   @Test
   public void testCase1SaveAccountStructureSetup() throws Exception 
   {
	   DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dtoObj = new DtoGLConfigurationRelationBetweenDimensionsAndMainAccount();
	   dtoObj.setCoaFinancialDimensionsIndexId(38);
	   dtoObj.setCoaFinancialDimensionsFromIndexValue(1L);
	   dtoObj.setCoaFinancialDimensionsToIndexValue(1L);
	   List<DtoGLConfigurationRelationBetweenDimensionsAndMainAccount> list = new ArrayList<>();
	   list.add(dtoObj);
	   JSONObject obj = new JSONObject();
	   obj.put("segmentNumber", "10");
	  // obj.put("isMainAccount", 1);//Integer 0 or 1
	  // obj.put("coaMainAccountsFromActIndexId", 1);//Integer
	   //obj.put("coaMainAccountsToActIndexId", 1);//Integer
	   obj.put("dimensionList",list);
	  
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveAccountStructureSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Save Account Structure Setup )
    * @throws Exception
    * Set value 0
    */
   
   @Test
   public void testCase2SaveAccountStructureSetup() throws Exception 
   {
	   DtoGLConfigurationRelationBetweenDimensionsAndMainAccount dtoObj = new DtoGLConfigurationRelationBetweenDimensionsAndMainAccount();
	   dtoObj.setCoaFinancialDimensionsIndexId(38);
	   dtoObj.setCoaFinancialDimensionsFromIndexValue(0L);
	   dtoObj.setCoaFinancialDimensionsToIndexValue(0L);
	   List<DtoGLConfigurationRelationBetweenDimensionsAndMainAccount> list = new ArrayList<>();
	   list.add(dtoObj);
	   JSONObject obj = new JSONObject();
	   obj.put("segmentNumber", "10");
	   obj.put("isMainAccount", 0);//Integer 0 or 1
	   obj.put("coaMainAccountsFromActIndexId", 0);//Integer
	   obj.put("coaMainAccountsToActIndexId", 0);//Integer
	   obj.put("dimensionList",list);
	  
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/saveAccountStructureSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /*********************Test case for get account structure setup **************************/
	
   /**
    * get Account Structure Setup 
    * @throws Exception
    */
   
   @Test
   public void testCase0GetAccountStructureSetup() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1")
              .when()
              .get(baseUrl+"setup/generalLedger/getAccountStructureSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * get Account Structure Setup 
    * @throws Exception
    * Blank Header Parameters
    */
   
   @Test
   public void testCase1GetAccountStructureSetup() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "")
              .when()
              .get(baseUrl+"setup/generalLedger/getAccountStructureSetup").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /*********************Test case for get master data for mass close fiscal period setup   **************************/
	
   /**
    * Get Master Data Years 
    * @throws Exception
    */
   
   @Test
   public void testCase0GetMasterDataYears() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1")
              .when()
              .get(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup/getYears").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Get Master Data Years 
    * @throws Exception
    * Blank Header Parameters
    */
   
   @Test
   public void testCase1GetMasterDataYears() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "")
              .when()
              .get(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup/getYears").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }

   /**
    * Get Master Data Series
    * @throws Exception
    */
   
   @Test
   public void testCase0GetMasterDataSeries() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1")
              .when()
              .get(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup/getSeries").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Get Master Data Series 
    * @throws Exception
    * Blank Header Parameters
    */
   
   @Test
   public void testCase1GetMasterDataSeries() throws Exception 
   {
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "")
              .when()
              .get(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup/getSeries").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Get Master Data Max Period
    * @throws Exception
    */
   
   @Test
   public void testCase0GetMasterDataMaxPeriod() throws Exception 
   {
	   JSONObject obj = new JSONObject();
	   obj.put("year", "2016");
	   String jsonString = obj.toJSONString();
	   
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup/getMaxPeriod").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Get Master Data Max Period
    * @throws Exception
    * Blank Header Parameters
    */
   
   @Test
   public void testCase1GetMasterMaxPeriod() throws Exception 
   {
	   JSONObject obj = new JSONObject();
	   obj.put("year", "2016");
	   String jsonString = obj.toJSONString();
	   
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup/getMaxPeriod").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Get Master Data Max Period
    * @throws Exception
    * Set 0 year parameter
    */
   
   @Test
   public void testCase2GetMasterMaxPeriod() throws Exception 
   {
	   JSONObject obj = new JSONObject();
	   obj.put("year", 0);//Integer
	   String jsonString = obj.toJSONString();
	   
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup/getMaxPeriod").
               andReturn();
        JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /*********************Test case for save mass close fiscal setup **************************/
	
   /**
    * Save mass close fiscal setup
    * @throws Exception
    */
   
   @Test
   public void testCase0SaveMassCloseFiscalSetup() throws Exception 
   {
	   List<DtoMassCloseFiscalPeriodSetup> list = new ArrayList<>();
	   DtoMassCloseFiscalPeriodSetup period = new DtoMassCloseFiscalPeriodSetup();
	   period.setPeriodName("primary");
	   period.setIsClose(true);
	   list.add(period);
	  
	   JSONObject obj = new JSONObject();
	   obj.put("year", "2018");
	   obj.put("seriesId", 1);
	   obj.put("records",list);
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Save mass close fiscal setup
    * Set Series Id 0
    */
   
   @Test
   public void testCase1SaveMassCloseFiscalSetup() throws Exception 
   {
	   List<DtoMassCloseFiscalPeriodSetup> list = new ArrayList<>();
	   DtoMassCloseFiscalPeriodSetup period = new DtoMassCloseFiscalPeriodSetup();
	   period.setPeriodName("primary");
	   period.setIsClose(true);
	   list.add(period);
	  
	   JSONObject obj = new JSONObject();
	   obj.put("year", "2018"); // Year mandatory
	   obj.put("seriesId", 0);
	   obj.put("records",list);
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Save mass close fiscal setup
    * Set year blank
    */
   @Test
   public void testCase2SaveMassCloseFiscalSetup() throws Exception 
   {
	   List<DtoMassCloseFiscalPeriodSetup> list = new ArrayList<>();
	   DtoMassCloseFiscalPeriodSetup period = new DtoMassCloseFiscalPeriodSetup();
	   period.setPeriodName("primary");
	   period.setIsClose(true);
	   list.add(period);
	  
	   JSONObject obj = new JSONObject();
	   obj.put("year", "2018"); // Year mandatory
	   obj.put("seriesId", 0);
	   obj.put("records",list);
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * Save mass close fiscal setup
    * Set header parameters blank
    */
   @Test
   public void testCase3SaveMassCloseFiscalSetup() throws Exception 
   {
	   List<DtoMassCloseFiscalPeriodSetup> list = new ArrayList<>();
	   DtoMassCloseFiscalPeriodSetup period = new DtoMassCloseFiscalPeriodSetup();
	   period.setPeriodName("primary");
	   period.setIsClose(true);
	   list.add(period);
	  
	   JSONObject obj = new JSONObject();
	   obj.put("year", "2018"); // Year mandatory
	   obj.put("seriesId", 0);
	   obj.put("records",list);
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/massCloseFiscalPeriodSetup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /*********************Test case for get mass close fiscal setup **************************/
	
   /**
    * get mass close fiscal setup
    * @throws Exception
    */
   
   @Test
   public void testCase0GetMassCloseFiscalSetup() throws Exception 
   {
	  
	   JSONObject obj = new JSONObject();
	   obj.put("year", "2018");
	   obj.put("seriesId", 1);
	   obj.put("allRecord",true);
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getMassCloseFiscalPeriodSetup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * get mass close fiscal setup
    * @throws Exception
    * Set 0 Year
    */
   
   @Test
   public void testCase1GetMassCloseFiscalSetup() throws Exception 
   {
	  
	   JSONObject obj = new JSONObject();
	   obj.put("year", 0);//Integer
	   obj.put("seriesId", 1);
	   obj.put("allRecord",true);
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getMassCloseFiscalPeriodSetup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * get mass close fiscal setup
    * @throws Exception
    * Set all records false
    */
   
   @Test
   public void testCase2GetMassCloseFiscalSetup() throws Exception 
   {
	  
	   JSONObject obj = new JSONObject();
	   obj.put("year", 2018);//Integer
	   obj.put("seriesId", 1);
	   obj.put("allRecord",false);
	   // If All records Set false then required two more parameters in request
	   obj.put("startPeriodId",1);
	   obj.put("endPeriodId",3);
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "1")
              .header("userid", "1").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getMassCloseFiscalPeriodSetup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
   /**
    * get mass close fiscal setup
    * @throws Exception
    * Blank header parameters
    */
   
   @Test
   public void testCase3GetMassCloseFiscalSetup() throws Exception 
   {
	   JSONObject obj = new JSONObject();
	   obj.put("year", 2018);//Integer
	   obj.put("seriesId", 1);
	   obj.put("allRecord",false);
	   // If All records Set false then required two more parameters in request
	   obj.put("startPeriodId",1);
	   obj.put("endPeriodId",3);
	   	
	   String jsonString = obj.toJSONString();
       Response response= given()
              .header("Content-Type","application/json")
              .header("langid", "")
              .header("userid", "").body(jsonString)
              .when()
              .post(baseUrl+"setup/generalLedger/getMassCloseFiscalPeriodSetup").
               andReturn();
      JsonPath jsonpath =response.getBody().jsonPath();
        System.out.println("Code="+jsonpath.getString("code").toString());
        System.out.println("Message="+jsonpath.getString("btiMessage").toString());
        assert response.statusCode()==200;
   }
   
}
