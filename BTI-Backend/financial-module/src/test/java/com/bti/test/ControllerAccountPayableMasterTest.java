package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Test;
import com.jayway.restassured.response.Response;
import net.minidev.json.JSONObject;

public class ControllerAccountPayableMasterTest {

	/**
	 * Create new vendor with required parameters
	 * Required parameters are venderId, venderStatus, classId and vendorHold
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterAddVendorRequired() throws Exception {
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("venderId", "12");
		dtoVendor.put("venderNamePrimary", "");
		dtoVendor.put("venderNameSecondary", "");
		dtoVendor.put("venderStatus", "1");
		dtoVendor.put("shortName", "");
		dtoVendor.put("classId", "1");
		dtoVendor.put("statementName", "");
		dtoVendor.put("address1", "");
		dtoVendor.put("address2", "");
		dtoVendor.put("address3", "");
		dtoVendor.put("country", "");
		dtoVendor.put("state", "");
		dtoVendor.put("city", "");
		dtoVendor.put("phone1", "");
		dtoVendor.put("phone2", "");
		dtoVendor.put("phone3", "");
		dtoVendor.put("fax", "");
		dtoVendor.put("userDefine1", "");
		dtoVendor.put("userDefine2", "");
		dtoVendor.put("vendorHold", "1");
		
		String jsonString = dtoVendor.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/accountPayableMaster/add/vender").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Create new vendor with all parameters
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterAddVendor() throws Exception {
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("venderId", "121");
		dtoVendor.put("venderNamePrimary", "Raman en");
		dtoVendor.put("venderNameSecondary", "Raman ar");
		dtoVendor.put("venderStatus", "1");
		dtoVendor.put("shortName", "Raman");
		dtoVendor.put("classId", "1");
		dtoVendor.put("statementName", "test");
		dtoVendor.put("address1", "seasia");
		dtoVendor.put("address2", "c-136");
		dtoVendor.put("address3", "phase-8");
		dtoVendor.put("country", "1");
		dtoVendor.put("state", "1");
		dtoVendor.put("city", "1");
		dtoVendor.put("phone1", "456456789");
		dtoVendor.put("phone2", "13246579");
		dtoVendor.put("phone3", "013214578");
		dtoVendor.put("fax", "4785");
		dtoVendor.put("userDefine1", "test1");
		dtoVendor.put("userDefine2", "test2");
		dtoVendor.put("vendorHold", "1");
		
		String jsonString = dtoVendor.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/accountPayableMaster/add/vender").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get vendor by vendor id
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterGetVendor() throws Exception {
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("venderId", "121");
		
		String jsonString = dtoVendor.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/accountPayableMaster/getOne/vender").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Search all vendors
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterFindAllVendor() throws Exception {
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("searchKeyword", "");
		
		String jsonString = dtoVendor.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/accountPayableMaster/find/vender").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Search all vendors with pagination
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterFindAllVendorWithPagination() throws Exception {
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("searchKeyword", "");
		dtoVendor.put("pageNumber", "1");
		dtoVendor.put("pageSize", "1");
		
		String jsonString = dtoVendor.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/accountPayableMaster/find/vender").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Search search vendor by keyword with pagination
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterFindVendorWithSearchKeywordAndPagination() throws Exception {
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("searchKeyword", "finance");
		dtoVendor.put("pageNumber", "1");
		dtoVendor.put("pageSize", "1");
		
		String jsonString = dtoVendor.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/accountPayableMaster/find/vender").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Vendor status
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterGetVendorStatus() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get("http://localhost:8084/setup/accountPayableMaster/getVendorStatus").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save vendor maintenance options
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterVendorMaintenanceOptions() throws Exception {
		JSONObject dtoVendor = new JSONObject();
		dtoVendor.put("vendorId", "121");
		dtoVendor.put("creditLimit", "1");
		dtoVendor.put("cardLimitAmount", "10.10");
		dtoVendor.put("currencyId", "1");
		dtoVendor.put("maximumInvoiceAmount", "1");
		dtoVendor.put("maximumInvoiceAmountValue ", "10.22");
		dtoVendor.put("tradeDiscountPercent", "22.22");
		dtoVendor.put("minimumCharge", "1");
		dtoVendor.put("minimumChargeAmount", "10.22");
		dtoVendor.put("minimumOrderAmount", "11.22");
		dtoVendor.put("openMaintenanceHistoryCalendarYear", "1");
		dtoVendor.put("openMaintenanceHistoryDistribution", "0");
		dtoVendor.put("openMaintenanceHistoryFiscalYear", "0");
		dtoVendor.put("openMaintenanceHistoryTransaction", "0");
		dtoVendor.put("userDefine1", "userDefine11");
		dtoVendor.put("userDefine2", "userDefine2");
		dtoVendor.put("userDefine3", "userDefine3");
		dtoVendor.put("vatScheduleId", "1");
		dtoVendor.put("shipmentMethodId", "22");
		dtoVendor.put("checkBookId", "1");
		dtoVendor.put("paymentTermId", "11");
		
		String jsonString = dtoVendor.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/setup/accountPayableMaster/vendorMaintenanceOptions").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get vendor maintenance options
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableMasterGetVendorMaintenanceOptions() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get("http://localhost:8084/setup/accountPayableMaster/getVendorMaintenanceOptions").andReturn();
		assert response.statusCode() == 200;

	}

}
