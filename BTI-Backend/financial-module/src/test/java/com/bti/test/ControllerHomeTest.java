package com.bti.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bti.controller.ControllerHome;
import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

import static com.jayway.restassured.RestAssured.given;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ControllerHomeTest {

	@Autowired
	ControllerHome controller;

	@Test
	public void controllerInitializedCorrectly() {
		assertThat(controller).isNotNull();
	}

	/**
	 * Get Country List
	 * @throws Exception
	 */
	@Test
	public void testGetCountryListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getCountryList").andReturn();
		assert response.statusCode() == 200;

	}

	/**
	 * Get State List By Country Id
	 * @throws Exception
	 */
	@Test
	public void testGetStateListSuccess() throws Exception {

		JSONObject countryId = new JSONObject();
		countryId.put("countryId", "1");
		String jsonString = countryId.toJSONString();

		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/getStateListByCountryId").andReturn();
		assert response.statusCode() == 200;

	}

	/**
	 * Get State List By Country Id
	 * @throws Exception
	 */
	@Test
	public void testGetStateListFailure() throws Exception {

		JSONObject countryId = new JSONObject();
		countryId.put("countryId", "");
		String jsonString = countryId.toJSONString();

		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/getStateListByCountryId").andReturn();
		assert response.statusCode() == 200;

	}

	/**
	 * Get City List By State Id
	 * @throws Exception
	 */
	@Test
	public void testGetCityListSuccess() throws Exception {

		JSONObject stateId = new JSONObject();
		stateId.put("stateId", "1");
		String jsonString = stateId.toJSONString();

		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/getCityListByStateId").andReturn();
		assert response.statusCode() == 200;

	}

	/**
	 * Get City List By State Id
	 * @throws Exception
	 */
	@Test
	public void testGetCityListFailure() throws Exception {

		JSONObject stateId = new JSONObject();
		stateId.put("stateId", "");
		String jsonString = stateId.toJSONString();

		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/getCityListByStateId").andReturn();
		assert response.statusCode() == 200;

	}

	/**
	 * Get Country Code By Country Id
	 * @throws Exception
	 */
	@Test
	public void testGetCountryCodeByCountryIdSuccess() throws Exception {

		JSONObject countryId = new JSONObject();
		countryId.put("countryId", "1");
		String jsonString = countryId.toJSONString();

		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/getCountryCodeByCountryId").andReturn();
		assert response.statusCode() == 200;

	}

	/**
	 * Get Country Code By Country Id
	 * @throws Exception
	 */
	@Test
	public void testGetCountryCodeByCountryIdFailure() throws Exception {

		JSONObject countryId = new JSONObject();
		countryId.put("countryId", "");
		String jsonString = countryId.toJSONString();

		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/getCountryCodeByCountryId").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Gl Account Number List
	 * @throws Exception
	 */
	@Test
	public void testGetGlAccountNumberListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getGlAccountNumberList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Due Types list
	 * @throws Exception
	 */
	@Test
	public void testGetDueTypesListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getDueTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Discount Types List
	 * @throws Exception
	 */
	@Test
	public void testGetDiscountTypesListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getDiscountTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Discount Type Period List
	 * @throws Exception
	 */
	@Test
	public void testGetDiscountPeriodTypesListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getDiscountPeriodTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Negative Symbol Sign Types list
	 * @throws Exception
	 */
	@Test
	public void testGetNegativeSymbolSignTypesSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getNegativeSymbolSignTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Negative Symbol Types list
	 * @throws Exception
	 */
	@Test
	public void testGetNegativeSymbolTypesSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getNegativeSymbolTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master rate frequency list
	 * @throws Exception
	 */
	@Test
	public void testGetRateFrequencyTypesSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getRateFrequencyTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Seperator Thousands list
	 * @throws Exception
	 */
	@Test
	public void testGetSeperatorThousandsTypesListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getSeperatorThousandsTypesList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Depreciation Period Types
	 * @throws Exception
	 */
	@Test
	public void testGetDepreciationPeriodTypesListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getDepreciationPeriodTypesList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Separator Decimal list
	 * @throws Exception
	 */
	@Test
	public void testGetSeperatorDecimalTypesListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getSeperatorDecimalTypesList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Apply by Default List
	 * @throws Exception
	 */
	@Test
	public void testGetRmApplyByDefaultListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getRmApplyByDefaultList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Ageing List
	 * @throws Exception
	 */
	@Test
	public void testGetRmAgeingListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getRmAgeingList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Balance Types
	 * @throws Exception
	 */
	@Test
	public void testGetCustomerClassBalanceTypeListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getCustomerClassBalanceTypeList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Minimun Types
	 * @throws Exception
	 */
	@Test
	public void testGetCustomerClassMinimumChargeListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getCustomerClassMinimumChargeList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Finance Charge
	 * @throws Exception
	 */
	@Test
	public void testGetCustomerClassFinanceChargeListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getCustomerClassFinanceChargeList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Credit Limit
	 * @throws Exception
	 */
	@Test
	public void testGetCustomerClassCreditLimitListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getCustomerClassCreditLimitList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Asset Type
	 * @throws Exception
	 */
	@Test
	public void testGetFAGeneralMaintenanceAssetTypesListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getFAGeneralMaintenanceAssetTypesList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Master Asset Status
	 * @throws Exception
	 */
	@Test
	public void testGetFAGeneralMaintenanceAssetStatusListSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getFAGeneralMaintenanceAssetStatusList").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get FA Property types List
	 * @throws Exception
	 */
	@Test
	public void testGetFAPropertyTypesSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getFAPropertyTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Check Main Account number
	 * @throws Exception
	 */
	@Test
	public void testCheckMainAccountNumberSuccess() throws Exception {
		JSONObject mainAccountNumber = new JSONObject();
		mainAccountNumber.put("mainAccountNumber", "80989898");
		String jsonString = mainAccountNumber.toJSONString();
		
		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/checkMainAccountNumber").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Check Main Account number
	 * @throws Exception
	 */
	@Test
	public void testCheckMainAccountNumberFailure() throws Exception {
		JSONObject mainAccountNumber = new JSONObject();
		mainAccountNumber.put("mainAccountNumber", "");
		String jsonString = mainAccountNumber.toJSONString();
		
		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/checkMainAccountNumber").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Check Dimension value
	 * @throws Exception
	 */
	@Test
	public void testCheckDimensionValuesSuccess() throws Exception {
		JSONObject dimensionValue = new JSONObject();
		dimensionValue.put("dimensionValue", "F3 Value 1");
		String jsonString = dimensionValue.toJSONString();
		
		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/checkDimensionValues").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Check Dimension value
	 * @throws Exception
	 */
	@Test
	public void testCheckDimensionValuesFailure() throws Exception {
		JSONObject dimensionValue = new JSONObject();
		dimensionValue.put("dimensionValue", "");
		String jsonString = dimensionValue.toJSONString();
		
		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/checkDimensionValues").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Types
	 * @throws Exception
	 */
	@Test
	public void testGetAccountTypesSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getAccountTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Types
	 * @throws Exception
	 */
	@Test
	public void testGetArAccountTypesSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getArAccountTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Types
	 * @throws Exception
	 */
	@Test
	public void testGetFAAccountTypesSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json").when()
				.get("http://localhost:8084/getFAAccountTypes").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Gl Account Detail By Account Row index
	 * @throws Exception
	 */
	@Test
	public void testGetGlAccountNumberDetailByIdSuccess() throws Exception {
		JSONObject accountTableRowIndex = new JSONObject();
		accountTableRowIndex.put("accountTableRowIndex", "7123");
		String jsonString = accountTableRowIndex.toJSONString();
		
		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/getGlAccountNumberDetailById").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Gl Account Detail By Account Row index
	 * @throws Exception
	 */
	@Test
	public void testGetGlAccountNumberDetailByIdFailure() throws Exception {
		JSONObject accountTableRowIndex = new JSONObject();
		accountTableRowIndex.put("accountTableRowIndex", "");
		String jsonString = accountTableRowIndex.toJSONString();
		
		Response response = given().header("Content-Type", "application/json").body(jsonString).when()
				.post("http://localhost:8084/getGlAccountNumberDetailById").andReturn();
		assert response.statusCode() == 200;

	}
}
