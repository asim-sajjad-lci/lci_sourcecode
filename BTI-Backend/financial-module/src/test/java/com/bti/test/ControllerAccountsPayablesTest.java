package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.bti.model.dto.DtoPMPeriodSetup;
import com.bti.model.dto.DtoPayableAccountClassSetup;
import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerAccountsPayablesTest {

	/**
	 * Save Account Payable Setup
	 * @throws Exception
	 */
	@Test
	public void testSalesTerritorySetupSuccess() throws Exception {
		JSONObject dtoPMSetup = new JSONObject();
		dtoPMSetup.put("allowDuplicateInvoicePerVendor", "1");
		dtoPMSetup.put("ageingBy", "1");
		dtoPMSetup.put("ageUnappliedCreditAmounts", "true");
		dtoPMSetup.put("applyByDefault", "1");
		dtoPMSetup.put("apTrackingDiscountAvailable", "true");
		dtoPMSetup.put("checkBookBankId", "1");
		dtoPMSetup.put("checkFormat", "1");
		dtoPMSetup.put("removeVendorHoldPassword", "removeVendor");
		dtoPMSetup.put("deleteUnpostedPrintedDocuments", "true");
		dtoPMSetup.put("exceedMaximumInvoiceAmount", "InvoiceAmount");
		dtoPMSetup.put("exceedMaximumWriteoffAmount", "exceed");
		dtoPMSetup.put("freightVatScheduleId", "freightVat");
		dtoPMSetup.put("miscVatScheduleId", "miscVat");
		dtoPMSetup.put("nextPaymentNumber", "123");
		dtoPMSetup.put("nextSchedulePaymentNumber", "456");
		dtoPMSetup.put("nextVoucherNumber", "789");
		dtoPMSetup.put("overrideVoucherNumberTransactionEntry", "true");
		dtoPMSetup.put("purchaseVatScheduleId", "purchaseVat");
		dtoPMSetup.put("printHistoricalAgedTrialBalance", "true");
		dtoPMSetup.put("userDefine1", "userDefine1");
		dtoPMSetup.put("userDefine2", "userDefine2");
		dtoPMSetup.put("userDefine3", "userDefine3");
		
		DtoPMPeriodSetup dtoPMPeriodSetup = new DtoPMPeriodSetup();
		List<DtoPMPeriodSetup> dtoPMPeriodSetups = new ArrayList<DtoPMPeriodSetup>();
		
		dtoPMPeriodSetup.setPeriodDescription("0-30 days");
		dtoPMPeriodSetup.setPeriodNoofDays(0);
		dtoPMPeriodSetup.setPeriodEnd(30);
		
		dtoPMPeriodSetups.add(dtoPMPeriodSetup);
		
		dtoPMSetup.put("pmPeriodSetupsList", dtoPMPeriodSetups);
		
		String jsonString = dtoPMSetup.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/payables/accountPayableSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Account Payable Setup
	 * @throws Exception
	 */
	@Test
	public void testSalesTerritorySetupFailure() throws Exception {
		JSONObject dtoPMSetup = new JSONObject();
		dtoPMSetup.put("allowDuplicateInvoicePerVendor", "");
		dtoPMSetup.put("ageingBy", "");
		dtoPMSetup.put("ageUnappliedCreditAmounts", "");
		dtoPMSetup.put("applyByDefault", "");
		dtoPMSetup.put("apTrackingDiscountAvailable", "");
		dtoPMSetup.put("checkBookBankId", "1");
		dtoPMSetup.put("checkFormat", "1");
		dtoPMSetup.put("removeVendorHoldPassword", "");
		dtoPMSetup.put("deleteUnpostedPrintedDocuments", "");
		dtoPMSetup.put("exceedMaximumInvoiceAmount", "");
		dtoPMSetup.put("exceedMaximumWriteoffAmount", "");
		dtoPMSetup.put("freightVatScheduleId", "");
		dtoPMSetup.put("miscVatScheduleId", "");
		dtoPMSetup.put("nextPaymentNumber", "");
		dtoPMSetup.put("nextSchedulePaymentNumber", "");
		dtoPMSetup.put("nextVoucherNumber", "");
		dtoPMSetup.put("overrideVoucherNumberTransactionEntry", "");
		dtoPMSetup.put("purchaseVatScheduleId", "");
		dtoPMSetup.put("printHistoricalAgedTrialBalance", "");
		dtoPMSetup.put("userDefine1", "");
		dtoPMSetup.put("userDefine2", "");
		dtoPMSetup.put("userDefine3", "");
		
		DtoPMPeriodSetup dtoPMPeriodSetup = new DtoPMPeriodSetup();
		List<DtoPMPeriodSetup> dtoPMPeriodSetups = new ArrayList<DtoPMPeriodSetup>();
		
		dtoPMPeriodSetup.setPeriodDescription("");
		dtoPMPeriodSetup.setPeriodNoofDays(0);
		dtoPMPeriodSetup.setPeriodEnd(0);
		
		dtoPMPeriodSetups.add(dtoPMPeriodSetup);
		
		dtoPMSetup.put("pmPeriodSetupsList", dtoPMPeriodSetups);
		
		String jsonString = dtoPMSetup.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/account/payables/accountPayableSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Payable Setup 
	 * @throws Exception
	 */
	@Test
	public void testGetAccountPayableSetupSuccess() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get("http://localhost:8084/account/payables/getAccountPayableSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Check Format Type
	 * @throws Exception
	 */
	@Test
	public void testGetCheckFormatTypeStatus() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.when()
				.get("http://localhost:8084/account/payables/getCheckFormatTypeStatus").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Account Payable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableClassSetupSuccess() throws Exception {
		JSONObject dtoVendorClassSetup = new JSONObject();
		dtoVendorClassSetup.put("vendorClassId", "123");
		dtoVendorClassSetup.put("classDescription", "classDescription");
		dtoVendorClassSetup.put("classDescriptionArabic", "classDescriptionArabic");
		dtoVendorClassSetup.put("creditLimit", "1");
		dtoVendorClassSetup.put("creditLimitAmount", "10.10");
		dtoVendorClassSetup.put("currencyId", "1");
		dtoVendorClassSetup.put("maximumInvoiceAmount", "1");
		dtoVendorClassSetup.put("maximumInvoiceAmountValue", "10.22");
		dtoVendorClassSetup.put("minimumCharge", "1");
		dtoVendorClassSetup.put("minimumChargeAmount", "10.22");
		dtoVendorClassSetup.put("minimumOrderAmount", "11.22");
		dtoVendorClassSetup.put("tradeDiscountPercent", "22.22");
		dtoVendorClassSetup.put("openMaintenanceHistoryCalendarYear", "0");
		dtoVendorClassSetup.put("openMaintenanceHistoryDistribution", "1");
		dtoVendorClassSetup.put("openMaintenanceHistoryFiscalYear", "3");
		dtoVendorClassSetup.put("openMaintenanceHistoryTransaction", "4");
		dtoVendorClassSetup.put("userDefine1", "userDefine1");
		dtoVendorClassSetup.put("userDefine2", "userDefine2");
		dtoVendorClassSetup.put("userDefine3", "userDefine3");
		dtoVendorClassSetup.put("vatScheduleId", "1");
		dtoVendorClassSetup.put("shipmentMethodId", "2");
		dtoVendorClassSetup.put("checkBookId", "3");
		dtoVendorClassSetup.put("paymentTermId", "4");
		dtoVendorClassSetup.put("accTableId", "5");
		
		String jsonString = dtoVendorClassSetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/accountPayableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Payable Class Setup by Id
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableClassSetupGetByIdSuccess() throws Exception {
		JSONObject dtoVendorClassSetup = new JSONObject();
		dtoVendorClassSetup.put("vendorClassId", "123");
		
		String jsonString = dtoVendorClassSetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/accountPayableClassSetupGetById").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Payable Class Setup by Id
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableClassSetupGetByIdFailure() throws Exception {
		JSONObject dtoVendorClassSetup = new JSONObject();
		dtoVendorClassSetup.put("vendorClassId", "");
		
		String jsonString = dtoVendorClassSetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/accountPayableClassSetupGetById").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Update Account Payable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateAccountPayableClassSetupSuccess() throws Exception {
		JSONObject dtoVendorClassSetup = new JSONObject();
		dtoVendorClassSetup.put("vendorClassId", "123");
		dtoVendorClassSetup.put("classDescription", "classDescription");
		dtoVendorClassSetup.put("classDescriptionArabic", "classDescriptionArabic");
		dtoVendorClassSetup.put("creditLimit", "1");
		dtoVendorClassSetup.put("creditLimitAmount", "10.10");
		dtoVendorClassSetup.put("currencyId", "1");
		dtoVendorClassSetup.put("maximumInvoiceAmount", "1");
		dtoVendorClassSetup.put("maximumInvoiceAmountValue", "10.22");
		dtoVendorClassSetup.put("minimumCharge", "1");
		dtoVendorClassSetup.put("minimumChargeAmount", "10.22");
		dtoVendorClassSetup.put("minimumOrderAmount", "11.22");
		dtoVendorClassSetup.put("tradeDiscountPercent", "22.22");
		dtoVendorClassSetup.put("openMaintenanceHistoryCalendarYear", "0");
		dtoVendorClassSetup.put("openMaintenanceHistoryDistribution", "1");
		dtoVendorClassSetup.put("openMaintenanceHistoryFiscalYear", "3");
		dtoVendorClassSetup.put("openMaintenanceHistoryTransaction", "4");
		dtoVendorClassSetup.put("userDefine1", "userDefine1");
		dtoVendorClassSetup.put("userDefine2", "userDefine2");
		dtoVendorClassSetup.put("userDefine3", "userDefine3");
		dtoVendorClassSetup.put("vatScheduleId", "1");
		dtoVendorClassSetup.put("shipmentMethodId", "2");
		dtoVendorClassSetup.put("checkBookId", "3");
		dtoVendorClassSetup.put("paymentTermId", "4");
		dtoVendorClassSetup.put("accTableId", "5");
		
		String jsonString = dtoVendorClassSetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/updateAccountPayableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Update Account Payable Class Setup
	 * @throws Exception
	 */
	@Test
	public void testUpdateAccountPayableClassSetupFailure() throws Exception {
		JSONObject dtoVendorClassSetup = new JSONObject();
		dtoVendorClassSetup.put("vendorClassId", "");
		dtoVendorClassSetup.put("classDescription", "classDescription");
		dtoVendorClassSetup.put("classDescriptionArabic", "classDescriptionArabic");
		dtoVendorClassSetup.put("creditLimit", "1");
		dtoVendorClassSetup.put("creditLimitAmount", "10.10");
		dtoVendorClassSetup.put("currencyId", "1");
		dtoVendorClassSetup.put("maximumInvoiceAmount", "1");
		dtoVendorClassSetup.put("maximumInvoiceAmountValue", "10.22");
		dtoVendorClassSetup.put("minimumCharge", "1");
		dtoVendorClassSetup.put("minimumChargeAmount", "10.22");
		dtoVendorClassSetup.put("minimumOrderAmount", "11.22");
		dtoVendorClassSetup.put("tradeDiscountPercent", "22.22");
		dtoVendorClassSetup.put("openMaintenanceHistoryCalendarYear", "0");
		dtoVendorClassSetup.put("openMaintenanceHistoryDistribution", "1");
		dtoVendorClassSetup.put("openMaintenanceHistoryFiscalYear", "3");
		dtoVendorClassSetup.put("openMaintenanceHistoryTransaction", "4");
		dtoVendorClassSetup.put("userDefine1", "userDefine1");
		dtoVendorClassSetup.put("userDefine2", "userDefine2");
		dtoVendorClassSetup.put("userDefine3", "userDefine3");
		dtoVendorClassSetup.put("vatScheduleId", "1");
		dtoVendorClassSetup.put("shipmentMethodId", "2");
		dtoVendorClassSetup.put("checkBookId", "3");
		dtoVendorClassSetup.put("paymentTermId", "4");
		dtoVendorClassSetup.put("accTableId", "5");
		
		String jsonString = dtoVendorClassSetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/updateAccountPayableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Search Account payble Class Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllAccountPayableClassSetup() throws Exception {
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("searchKeyword", "");

		String jsonString = dtoSalesTerritorySetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/searchAccountPayableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Search Account payble Class Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAllAccountPayableClassSetupWithPagination() throws Exception {
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("searchKeyword", "");
		dtoSalesTerritorySetup.put("pageNumber", "1");
		dtoSalesTerritorySetup.put("pageSize", "1");

		String jsonString = dtoSalesTerritorySetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/searchAccountPayableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Search Account payble Class Setup
	 * @throws Exception
	 */
	@Test
	public void testSearchAccountPayableClassSetupWithPagination() throws Exception {
		JSONObject dtoSalesTerritorySetup = new JSONObject();
		dtoSalesTerritorySetup.put("searchKeyword", "finance");
		dtoSalesTerritorySetup.put("pageNumber", "1");
		dtoSalesTerritorySetup.put("pageSize", "1");

		String jsonString = dtoSalesTerritorySetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/searchAccountPayableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Account Payable Option Setup
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableOptionSetup() throws Exception {
		JSONObject dtoPMDocumentsTypeSetup = new JSONObject();
		dtoPMDocumentsTypeSetup.put("apDocumentTypeId", "");
		dtoPMDocumentsTypeSetup.put("documentTypeDescription", "scczxczx");
		dtoPMDocumentsTypeSetup.put("documentTypeDescriptionArabic", "cxzcxzc");
		dtoPMDocumentsTypeSetup.put("documentNumberLastNumber", "1");
		dtoPMDocumentsTypeSetup.put("documentCode", "four");
		dtoPMDocumentsTypeSetup.put("documentType", "vcv");
		
		String jsonString = dtoPMDocumentsTypeSetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/accountPayableOptionSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Update Account Payable Option Setup
	 * @throws Exception
	 */
	@Test
	public void testAccountPayableOptionSetupUpdate() throws Exception {
		JSONObject dtoPMDocumentsTypeSetup = new JSONObject();
		dtoPMDocumentsTypeSetup.put("apDocumentTypeId", "1");
		dtoPMDocumentsTypeSetup.put("documentTypeDescription", "scczxczx");
		dtoPMDocumentsTypeSetup.put("documentTypeDescriptionArabic", "cxzcxzc");
		dtoPMDocumentsTypeSetup.put("documentNumberLastNumber", "1");
		dtoPMDocumentsTypeSetup.put("documentCode", "four");
		dtoPMDocumentsTypeSetup.put("documentType", "vcv");
		
		String jsonString = dtoPMDocumentsTypeSetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/accountPayableOptionSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Account Payable Option Setup 
	 * @throws Exception
	 */
	@Test
	public void testGetAccountPayableOptionSetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/account/payables/getAccountPayableOptionSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save Payable Class Account Setup
	 * @throws Exception
	 */
	@Test
	public void testSaveAccountPayableClassSetup() throws Exception {
		JSONObject dtoPMSetup = new JSONObject();
		dtoPMSetup.put("classId", "1");
		
		DtoPayableAccountClassSetup dtoPMPeriodSetup = new DtoPayableAccountClassSetup();
		List<DtoPayableAccountClassSetup> accountClassSetups = new ArrayList<>();
		
		List<Long> accountNumberIndex = new ArrayList<>();
		accountNumberIndex.add(1L);
		accountNumberIndex.add(2L);
		accountNumberIndex.add(3L);
		
		dtoPMPeriodSetup.setAccountType(1);
		dtoPMPeriodSetup.setAccountDescription("Test123456");
		dtoPMPeriodSetup.setAccountNumberIndex(accountNumberIndex);
		
		accountClassSetups.add(dtoPMPeriodSetup);
		
		dtoPMSetup.put("accountNumberList", accountClassSetups);
		
		String jsonString = dtoPMSetup.toJSONString();
		
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/saveAccountPayableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get Payable Class Account Setup
	 * @throws Exception
	 */
	@Test
	public void testGetAccountPayableClassSetup() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/account/payables/getAccountPayableClassSetup").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Save vendor account maintenance 
	 * @throws Exception
	 */
	@Test
	public void testSaveVendorAccountMaintenance() throws Exception {
		JSONObject dtoPayableAccountClassSetup = new JSONObject();
		dtoPayableAccountClassSetup.put("vendorId", "1");
		
		DtoPayableAccountClassSetup dtoPMPeriodSetup = new DtoPayableAccountClassSetup();
		List<DtoPayableAccountClassSetup> accountClassSetups = new ArrayList<>();
		
		List<Long> accountNumberIndex = new ArrayList<>();
		accountNumberIndex.add(1L);
		accountNumberIndex.add(2L);
		accountNumberIndex.add(3L);
		
		dtoPMPeriodSetup.setAccountType(1);
		dtoPMPeriodSetup.setAccountDescription("Test123456");
		dtoPMPeriodSetup.setAccountNumberIndex(accountNumberIndex);
		
		accountClassSetups.add(dtoPMPeriodSetup);
		
		dtoPayableAccountClassSetup.put("accountNumberList", accountClassSetups);
		
		String jsonString = dtoPayableAccountClassSetup.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .body(jsonString).when()
				.post("http://localhost:8084/account/payables/saveVendorAccountMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
	/**
	 * Get vendor account maintenance 
	 * @throws Exception
	 */
	@Test
	public void testGetVendorAccountMaintenance() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
	            .when()
				.get("http://localhost:8084/account/payables/getVendorAccountMaintenance").andReturn();
		assert response.statusCode() == 200;

	}
	
}
