package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Test;

import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerGeneralLedgerTest {

	/**
	 * Save Posting account
	 * @throws Exception
	 */
	@Test
	public void testCreateGeneralLedgerPostingAccount() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("descriptionPrimary", "cash");
		obj.put("descriptionSecondary", "cash in arabic");
		obj.put("accountTableRowIndex", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/general/ledger/posting/account").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Update posting account
	 * @throws Exception
	 */
	@Test
	public void testUpdateGeneralLedgerPostingAccount() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("postingId", "3");
		obj.put("descriptionPrimary", "cash");
		obj.put("descriptionSecondary", "cash in arabic");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/general/ledger/posting/account/update").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all posting account
	 * @throws Exception
	 */
	@Test
	public void testGetAllGeneralLedgerPostingAccount() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/general/ledger/posting/account/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get all posting account with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetAllGeneralLedgerPostingAccountWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/general/ledger/posting/account/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get searched posting account with pagination
	 * @throws Exception
	 */
	@Test
	public void testGetSearchedGeneralLedgerPostingAccountWithPagination() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("searchKeyword", "ca");
		obj.put("pageNumber", "0");
		obj.put("pageSize", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/general/ledger/posting/account/search").andReturn();
		assert response.statusCode() == 200;
	}
	
	/**
	 * Get posting account by id
	 * @throws Exception
	 */
	@Test
	public void testGetGeneralLedgerPostingAccountById() throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("postingId", "1");
		
		String jsonString = obj.toJSONString();

		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "1")
				.body(jsonString).when()
				.post("http://localhost:8084/general/ledger/posting/account/getById").andReturn();
		assert response.statusCode() == 200;
	}
}
