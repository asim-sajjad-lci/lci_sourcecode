package com.bti.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoGridData;
import com.bti.repository.RepositoryGrid;
import com.bti.repository.RepositoryGridData;
import com.bti.service.ServiceGrid;
import com.bti.service.ServiceGridData;
import com.bti.service.ServiceResponse;

/**
 * Description: ControllerField 
 * Name of Project: BTI
 */
@RestController
@RequestMapping("/grids")
public class ControllerGrid {

	/**
	 * @Description LOGGER use for put a logger in Field Controller
	 */
	private static final Logger logger = Logger.getLogger(ControllerField.class);

	@Autowired
	SessionManager sessionManager;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceGrid serviceGrid;

	@Autowired
	ServiceGridData serviceGridData;

	@Autowired
	RepositoryGrid repositoryGrid;

	@Autowired
	RepositoryGridData repositoryGridData;

	@RequestMapping(value = "/changeColumnVisibleStatus", method = RequestMethod.PUT)
	public ResponseMessage changeColumnVisibleStatus(HttpServletRequest request, @RequestBody DtoGridData dtoGridData)
			throws Exception {

		logger.info("Change column visible status method called!!");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);

		if (session != null) {
			DtoGridData dtoSearch = serviceGridData.changeVisible(dtoGridData);
			if (dtoSearch != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("GRID_DATA_UPDATED_SUCCESSFULLY", false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("GRID_DATA_NOT_UPDATED", false), dtoSearch);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		logger.debug("Change visible status:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/hideAllColumns", method = RequestMethod.PUT)
	public ResponseMessage hideAllColumns(HttpServletRequest request, @RequestBody DtoGridData dtoGridData)
			throws Exception {

		logger.info("Hide All Columns method Called!!");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);

		if (session != null) {
			DtoGridData dtoGridDataService = serviceGridData.hideAllColumns(dtoGridData.getGridId());
			if (dtoGridDataService != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("GRID_DATA_UPDATED_SUCCESSFULLY", false),
						dtoGridDataService);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("GRID_DATA_NOT_UPDATED", false),
						dtoGridDataService);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		logger.debug("Hide All Columns:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/showAllColumns", method = RequestMethod.PUT)
	public ResponseMessage showAllColumns(HttpServletRequest request, @RequestBody DtoGridData dtoGridData)
			throws Exception {

		logger.info("Hide All Columns method Called!!");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);

		if (session != null) {
			DtoGridData dtoGridDataService = serviceGridData.showAllColumns(dtoGridData.getGridId());
			if (dtoGridDataService != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("GRID_DATA_UPDATED_SUCCESSFULLY", false),
						dtoGridDataService);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("GRID_DATA_NOT_UPDATED", false),
						dtoGridDataService);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		logger.debug("Hide All Columns:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/resetGrid", method = RequestMethod.PUT)
	public ResponseMessage resetGrid(HttpServletRequest request, @RequestBody DtoGridData dtoGridData)
			throws Exception {

		logger.info("Reset Grid Method Called!!");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);

		if (session != null) {
			DtoGridData dtoGridDataService = serviceGridData.resetGrid(dtoGridData.getGridId());
			if (dtoGridDataService != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("GRID_DATA_UPDATED_SUCCESSFULLY", false),
						dtoGridDataService);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("GRID_DATA_NOT_UPDATED", false),
						dtoGridDataService);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		logger.debug("Reset Grid:" + responseMessage.getMessage());
		return responseMessage;
	}
}
