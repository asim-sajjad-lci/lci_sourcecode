/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.constant.Constant;
import com.bti.constant.MessageLabel;
import com.bti.model.CommonConstant;
import com.bti.model.User;
import com.bti.model.UserCompanyRelation;
import com.bti.model.UserDetail;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoCompany;
import com.bti.model.dto.DtoUser;
import com.bti.model.dto.DtoUserLogin;
import com.bti.repository.RepositoryException;
import com.bti.repository.RepositoryUser;
import com.bti.repository.RepositoryUserCompanyRelation;
import com.bti.repository.RepositoryUserDetail;
import com.bti.repository.RepositoryUserSession;
import com.bti.service.ServiceForgotPassword;
import com.bti.service.ServiceLogin;
import com.bti.service.ServiceLoginOtp;
import com.bti.service.ServiceResponse;
import com.bti.service.ServiceUser;
import com.bti.service.ServiceUserMacAddress;
import com.bti.util.CommonUtils;
import com.bti.util.UtilFindIPAddress;

/**
 * Description: ControllerLogin
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/login")
public class ControllerLogin {

	private static final Logger LOGGER = Logger.getLogger(ControllerLogin.class);

	@Autowired
	RepositoryUser repositoryUser;

	@Autowired
	ServiceUser serviceUser;

	@Autowired
	RepositoryUserSession repositoryUserSession;

	@Autowired
	SessionManager sessionManager;

	@Autowired
	ServiceLoginOtp serviceLoginOtp;

	@Autowired
	RepositoryUserDetail repositoryUserDetail;

	@Autowired
	ServiceUserMacAddress serviceUserIp;

	@Autowired
	ServiceForgotPassword serviceForgotPassword;

	@Autowired
	ServiceLogin serviceLogin;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryException repositoryException;
	
	@Autowired
	RepositoryUserCompanyRelation repositoryUserCompanyRelation;
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	
	/**
	 * @description : Tt checks if ip exist in user ip table
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/checkIpBeforeLogin", method = RequestMethod.GET)
	public ResponseMessage test(HttpServletRequest request) 
	{
		ResponseMessage responseMessage = null;
		LOGGER.info("Application's check Ip Before Login service");
		String userIp = UtilFindIPAddress.getUserIp(request);
		if (userIp != null) 
		{
			LOGGER.info("User ip address " + userIp);
			boolean result = serviceUserIp.allowedIpRequest(userIp);
			Map<String, Boolean> map = new HashMap<>();
			map.put("validIpAddress", result);
			if (result) 
			{
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.VALID_USER_IP, false), map);
			}
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INVALID_USER_IP, false), map);
			}
		}
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_IP_NOT_FOUND, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : It sends otp to user if credentials are valid
	 * @param dtoUser
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/loginUserForOtp", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage loginUser(@RequestBody DtoUser dtoUser, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		User user = repositoryUser.findByusernameAndIsDeleted(dtoUser.getUserName(), false);
		if (user != null && user.isActive()) {
			
			boolean allowedIp=true;
			if(user.getIpChecked()){
            	allowedIp= serviceUserIp.checkAllowedUserIpRequest(request, dtoUser.getUserName());
            }
			
			if (!allowedIp) {
				responseMessage = new ResponseMessage(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INVALID_USER_IP_OR_BLOCKED, false));
			}
			else {
			Boolean session = sessionManager.validateUserSessionExistOrNotByIp(dtoUser);
			if(session){
				DtoUser dtoUserLogin = serviceUser.sendOTPtoUser(dtoUser);
				if (dtoUserLogin != null) {
						dtoUserLogin.setRole(user.getRole().getRoleName());
					if (dtoUserLogin.getMessageType() == null) {
						if (dtoUserLogin.getSmsAuthentication().equalsIgnoreCase("Y")) {
							responseMessage = new ResponseMessage(HttpStatus.ACCEPTED.value(), HttpStatus.ACCEPTED,
									serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_OTP_SENT, false),
									dtoUserLogin);
						} else if (dtoUserLogin.getSmsAuthentication().equalsIgnoreCase("N")) {
							
							//set company count for user
							List<UserCompanyRelation> listUserCompanyRelation=
									repositoryUserCompanyRelation.findByUserUserIdAndIsDeleted(dtoUserLogin.getUserId(), false);
							dtoUserLogin.setCompanyCount(listUserCompanyRelation.size());
							
							responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
									serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LOGGED_IN_SUCCESS, false),
									dtoUserLogin);
						}
					} else {
						responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
								serviceResponse.getMessageByShortAndIsDeleted(dtoUserLogin.getMessageType(), false));
					}

				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_NOT_FOUND, false));
				}		
			}
			else{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_SESSION_ALREADY_EXIST, false));
				  }
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.INVALID_USERNAME_AND_PASSWORD, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Verify user otp for login 
	 * @param user
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/verifyOtpAuthentication", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage verifyOtpAuthentication(@RequestBody DtoUserLogin dtoUserLogin, HttpServletRequest request,
			HttpSession httpSession) {
		String langId = request.getHeader("langId");
		LOGGER.info(" verify Otp Authentication service :::::: ");
		ResponseMessage responseMessage = null;
		DtoUser dtoUser = null;
		User userExist = repositoryUser.findByUserIdAndIsDeleted(dtoUserLogin.getUserId(), false);
		if (userExist != null && userExist.isActive()) {
			
			dtoUserLogin = serviceLoginOtp.validateUserOTP(dtoUserLogin);
			
			if (dtoUserLogin.getMessageType() == null) {
				if (dtoUserLogin.isOtpMatched()!=null && dtoUserLogin.isOtpMatched()) 
				{
					if (userExist.getIsDeleted()) {
						responseMessage = new ResponseMessage(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_NOT_FOUND, false));
					} else {
						String sessionId = sessionManager.getUniqueSessionId(dtoUserLogin.getUserId(),dtoUserLogin.getIpAddress());
						UserDetail userDetail = repositoryUserDetail.findByUserUserId(dtoUserLogin.getUserId());
						dtoUser = new DtoUser(userExist, userDetail, langId);
						dtoUser.setSession(sessionId);
						dtoUser.setRole(userExist.getRole().getRoleName());
						httpSession.setAttribute("loggedInUserId", dtoUserLogin.getUserId());
						
						//set company count for user
						List<UserCompanyRelation> listUserCompanyRelation=
								repositoryUserCompanyRelation.findByUserUserIdAndIsDeleted(dtoUser.getUserId(), false);
						dtoUser.setCompanyCount(listUserCompanyRelation.size());
						
						responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_OTP_VERIFIED, false), dtoUser);
					}
				} 
				else 
				{
					if (dtoUserLogin.isOtpMaxLimitReached() !=null && dtoUserLogin.isOtpMaxLimitReached()) 
					{
						responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_OTP_MAX_LIMIT_REACHED, false),
								dtoUserLogin);
					} 
					else {
						responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_OTP_FAILED, false), dtoUserLogin);
					}
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(dtoUserLogin.getMessageType(), false),
						dtoUserLogin);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_NOT_FOUND, false));

		}
		return responseMessage;
	}
	
	/**
	 * @description : Reset password and send email 
	 * @param user
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
	@ResponseBody
	public ModelMap forgotPassword(@RequestBody DtoUser user, HttpServletRequest request) {
		LOGGER.info(" forgot password service :::::: ");
		ResponseMessage responseMessage = null;
		User userExist = repositoryUser.findByEmailAndIsDeleted(user.getEmail(), false);
		if (userExist != null) {
			boolean result = serviceForgotPassword.resetPasswordAndSendEmail(userExist);
			if (result) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORGOT_PASSWORD_EMAIL_SENT, false));
			} else {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORGOT_PASSWORD_EMAIL_NOT_SENT, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_NOT_FOUND, false));
		}
		return new ModelMap("response", responseMessage);
	}
	
	@RequestMapping(value = "/checkCompanyAccess", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage checkCompanyAccess(@RequestBody DtoUser dtoUser, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		dtoUser = serviceLogin.checkValidCompanyAceess(dtoUser);
		if (dtoUser.getCompanyTenantId()!=null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LOGGED_IN_SUCCESS, false), dtoUser);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.DO_NOT_HAVE_COMPANY_ACCESS, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/checkCompanyAccessForOtherModule", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage checkCompanyAccessForOtherModule(HttpServletRequest request) 
	{
		ResponseMessage responseMessage = null;
		String sessionId = httpServletRequest.getHeader("session");
		int userId=Integer.parseInt(httpServletRequest.getHeader(Constant.USER_ID));
		UserSession session = repositoryUserSession.findByUserUserIdAndSessionAndIsDeleted(userId, sessionId,
				false);
		if (session != null) 
		{
			boolean flag = serviceLogin.checkCompanyAccessForOtherModule();
			if (flag) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LOGGED_IN_SUCCESS, false), flag);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.DO_NOT_HAVE_COMPANY_ACCESS, false), false);
			}
		}
		else
		{
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getCompanyDatabaseCredential", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getCompanyDatabaseCredential(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		DtoCompany dtoCompany = serviceLogin.getCompanyDatabaseCredential();
		if (dtoCompany != null) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoCompany);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.DO_NOT_HAVE_COMPANY_ACCESS, false), false);

		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/validateSession", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage validateSession(HttpServletRequest request) 
	{
		ResponseMessage responseMessage = null;
		String clientId = CommonUtils.removeNull(httpServletRequest.getHeader("clientId"));
		String token = CommonUtils.removeNull(httpServletRequest.getHeader("token"));

		String sessionId = CommonUtils.removeNull(httpServletRequest.getHeader("session"));
		int erpUserId=Integer.parseInt(httpServletRequest.getHeader(Constant.ERP_USER_ID));
		
		if (!serviceLogin.validateClient(clientId, token)) {
			responseMessage = new ResponseMessage(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
			
		} else {
			UserSession session = repositoryUserSession.findByUserUserIdAndSessionAndIsDeleted(erpUserId, sessionId,
					false);
			
			if (session != null) 
			{
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_SESSION_ALREADY_EXIST, false), true);
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SESSION_EXPIRED, false), false);
			}
		}
		return responseMessage;
	}

	@RequestMapping(value = "/validateClient", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage validateClient(HttpServletRequest request) 
	{
		ResponseMessage responseMessage = null;
		String clientId = CommonUtils.removeNull(httpServletRequest.getHeader(Constant.CLIENT_ID));
		String token = CommonUtils.removeNull(httpServletRequest.getHeader(Constant.CLIENT_TOKEN));
		
		if (serviceLogin.validateClient(clientId, token)) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_LOGIN_SUCCESS, false), true);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
		} 
		return responseMessage;
	}


	@RequestMapping(value = "/openWorkflow", method = RequestMethod.GET)
	public ResponseEntity<Object> redirectToExternalUrl(HttpServletRequest request) throws URISyntaxException {
	    URI url = new URI("http://64.188.5.148:8080/bonita/apps/portal");
	    HttpHeaders httpHeaders = new HttpHeaders();
	    httpHeaders.set("client", request.getParameter("client"));
	    httpHeaders.set("userid", request.getParameter("userid"));
	    httpHeaders.set("session", request.getParameter("session"));
	    httpHeaders.set("tenantid", request.getParameter("tenantid"));
	    httpHeaders.setLocation(url);
	    return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
	}}
