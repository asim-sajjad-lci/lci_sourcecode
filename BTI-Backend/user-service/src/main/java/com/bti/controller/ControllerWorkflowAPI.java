/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.constant.Constant;
import com.bti.constant.MessageLabel;
import com.bti.model.CommonConstant;
import com.bti.model.User;
import com.bti.model.UserCompanyRelation;
import com.bti.model.UserDetail;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoCompany;
import com.bti.model.dto.DtoRuleDefinition;
import com.bti.model.dto.DtoUser;
import com.bti.model.dto.DtoUserLogin;
import com.bti.model.dto.DtoWorkflowUserDetails;
import com.bti.repository.RepositoryException;
import com.bti.repository.RepositoryUser;
import com.bti.repository.RepositoryUserCompanyRelation;
import com.bti.repository.RepositoryUserDetail;
import com.bti.repository.RepositoryUserSession;
import com.bti.service.ServiceForgotPassword;
import com.bti.service.ServiceLogin;
import com.bti.service.ServiceLoginOtp;
import com.bti.service.ServiceResponse;
import com.bti.service.ServiceRuleEngine;
import com.bti.service.ServiceUser;
import com.bti.service.ServiceUserMacAddress;
import com.bti.service.ServiceWorkflowApi;
import com.bti.util.CommonUtils;
import com.bti.util.UtilFindIPAddress;

/**
 * Description: ControllerLogin
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 11:39:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/workflowapi")
public class ControllerWorkflowAPI {

	private static final Logger LOGGER = Logger.getLogger(ControllerWorkflowAPI.class);




	@Autowired
	ServiceWorkflowApi serviceWorkflowApi;
	
	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceLogin serviceLogin;



	@Autowired
	RepositoryUserSession repositoryUserSession;



	@Autowired
	ServiceRuleEngine serviceRuleEngine;


	@RequestMapping(value = "/validateAndFetchUserDetails", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage validateAndFetchUserDetails(@RequestBody DtoUser dtoUser, HttpServletRequest request) {
		
		ResponseMessage responseMessage = null;
		String clientId = CommonUtils.removeNull(request.getHeader("clientId"));
		String token = CommonUtils.removeNull(request.getHeader("token"));
		
		String userName = CommonUtils.removeNull(dtoUser.getUserName());
		String password = CommonUtils.removeNull(dtoUser.getPassword());
		
		if (!serviceLogin.validateClient(clientId, token)) {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
			
		} else {
		
			
			DtoWorkflowUserDetails dtoWorkflowUserDetails = new DtoWorkflowUserDetails();
			
			String validationMessage = serviceWorkflowApi.validateAndFetchUserDetails(dtoWorkflowUserDetails, userName, password);
			
			switch(validationMessage) {
				case MessageLabel.WORKFLOW_USER_AUTHENTICATED:
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoWorkflowUserDetails);
					break;

				case MessageLabel.WORKFLOW_INVALID_PASSWORD:
					responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WORKFLOW_INVALID_PASSWORD, false), false);
					break;

				case MessageLabel.WORKFLOW_USER_INACTIVE:
					responseMessage = new ResponseMessage(HttpStatus.CONFLICT.value(), HttpStatus.CONFLICT,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WORKFLOW_USER_INACTIVE, false), false);
					break;
				
				
				case MessageLabel.WORKFLOW_USER_NOT_FOUND:
					responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.WORKFLOW_USER_NOT_FOUND, false), false);
					break;
			}
			
			
		}
		return responseMessage;
	}
	
	
	@RequestMapping(value = "/getWorkflowUserDetailsByUserId", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getWorkflowUserDetailsByUserId(HttpServletRequest request) {
		
		ResponseMessage responseMessage = null;
//TODO Ceneralize Session or Client validation at one place
		
		String clientId = CommonUtils.removeNull(request.getHeader("clientId"));
		String token = CommonUtils.removeNull(request.getHeader("token"));
		
		
		if (!serviceLogin.validateClient(clientId, token)) {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
			
		} else {
		
			String userId = CommonUtils.removeNull(request.getParameter("userId"));
			
			int id = CommonUtils.parseInteger(userId);
			
			DtoWorkflowUserDetails dtoWorkflowUserDetails = serviceWorkflowApi.getUserDetailsById(id);
			
			if (dtoWorkflowUserDetails != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoWorkflowUserDetails);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), false);
	
			}
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getWorkflowUserDetailsByUserName", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getWorkflowUserDetailsByUserName(HttpServletRequest request) {
		
		ResponseMessage responseMessage = null;
		String clientId = CommonUtils.removeNull(request.getHeader("clientId"));
		String token = CommonUtils.removeNull(request.getHeader("token"));
		
		
		if (!serviceLogin.validateClient(clientId, token)) {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
			
		} else {
		
			String userName = CommonUtils.removeNull(request.getParameter("userName"));
			
			DtoWorkflowUserDetails dtoWorkflowUserDetails = new DtoWorkflowUserDetails(); 
			boolean isFetchedSuccessfully = serviceWorkflowApi.fetchUserDetailsByUserName(dtoWorkflowUserDetails, userName);
			
			if (isFetchedSuccessfully) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoWorkflowUserDetails);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), false);
	
			}
		}
		return responseMessage;
	}

	@RequestMapping(value = "/getWorkflowUserDetailsBySession", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getWorkflowUserDetailsBySession(HttpServletRequest request) {
		
		ResponseMessage responseMessage = null;
//TODO Ceneralize Session or Client validation at one place
		
		String clientId = CommonUtils.removeNull(request.getHeader("clientId"));
		String token = CommonUtils.removeNull(request.getHeader("token"));
		
		
		if (!serviceLogin.validateClient(clientId, token)) {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
			
		} else {
		
			String sessionId = CommonUtils.removeNull(request.getHeader("session"));
			int erpUserId=Integer.parseInt(request.getHeader(Constant.ERP_USER_ID));

			UserSession session = repositoryUserSession.findByUserUserIdAndSessionAndIsDeleted(erpUserId, sessionId,
					false);
			
			if (session != null) 
			{
				
				DtoWorkflowUserDetails dtoWorkflowUserDetails = serviceWorkflowApi.getUserDetailsByErpUserId(erpUserId);
				
				if (dtoWorkflowUserDetails != null) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoWorkflowUserDetails);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), false);
		
				}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SESSION_EXPIRED, false), false);
			}

		}
		return responseMessage;
	}

	
	@RequestMapping(value = "/getRules", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getRules(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		String clientId = CommonUtils.removeNull(request.getHeader("clientId"));
		String token = CommonUtils.removeNull(request.getHeader("token"));
		
		
		if (!serviceLogin.validateClient(clientId, token)) {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
			
		} else {
		
			String processCode = request.getParameter(Constant.PROCESS_CODE);
			List rulesList = serviceRuleEngine.getRules(processCode);

			if (rulesList != null && !rulesList.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), rulesList);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), false);
	
			}
			
			
			
		}
		
		
		return responseMessage;
	}
	
	@RequestMapping(value = "/getRulesDefinition", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getRulesDefinition(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		String clientId = CommonUtils.removeNull(request.getHeader("clientId"));
		String token = CommonUtils.removeNull(request.getHeader("token"));
		
		
		if (!serviceLogin.validateClient(clientId, token)) {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false), false);
			
		} else {
		
			String processCode = request.getParameter(Constant.PROCESS_CODE);
			String ruleCode = request.getParameter(Constant.RULE_CODE);
			DtoRuleDefinition dtoRuleDefinition= serviceRuleEngine.getRulesDefination(processCode, ruleCode);

			if (dtoRuleDefinition != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_GET_SUCCESSFULLY, false), dtoRuleDefinition);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.ENTITY_NOT_FOUND.value(), HttpStatus.ENTITY_NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false), false);
	
			}
		}
		
		
		return responseMessage;
	}

}
