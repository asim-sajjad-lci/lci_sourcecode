/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.AccessRole;
import com.bti.model.RoleGroup;
import com.bti.model.UserGroup;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoAccessRole;
import com.bti.model.dto.DtoRoleGroup;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoUserGroup;
import com.bti.repository.RepositoryAccessRole;
import com.bti.repository.RepositoryRoleGroup;
import com.bti.repository.RepositoryUserGroup;
import com.bti.service.ServiceAccessRole;
import com.bti.service.ServiceResponse;
import com.bti.service.ServiceRoleGroup;
import com.bti.service.ServiceUserGroup;

/**
 * Description: ControllerGroup
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/group")
public class ControllerGroup {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ControllerGroup.class);

	@Autowired
	ServiceAccessRole serviceAccessRole;

	@Autowired
	ServiceUserGroup serviceUserGroup;

	@Autowired
	ServiceRoleGroup serviceRoleGroup;

	@Autowired
	SessionManager sessionManager;

	@Autowired
	ServiceResponse serviceResponse;


	@Autowired
	RepositoryAccessRole repositoryAccessRole;
	
	@Autowired
	RepositoryRoleGroup repositoryRoleGroup;
	
	@Autowired
	RepositoryUserGroup repositoryUserGroup;
	
	/**
	 * @description : Save Role
	 * @param dtoAccessRole
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/saveRole", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage saveRole(@RequestBody DtoAccessRole dtoAccessRole, HttpServletRequest request) 
	{
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) 
		{
			AccessRole accessRole=repositoryAccessRole.findByRoleNameAndIsDeleted(dtoAccessRole.getRoleName(), false);
			if(accessRole==null)
			{
				int id = this.serviceAccessRole.saveAccessRole(dtoAccessRole);
				if (id > 0) 
				{
						dtoAccessRole.setAccessRoleId(id);
						responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
								serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_SUCCESS, false), dtoAccessRole);
				}
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCESS_ROLE_NAME_EXIT, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Role Detail
	 * @param dtoAccessRole
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getRoleDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getRoleDetails(@RequestBody DtoAccessRole dtoAccessRole, HttpServletRequest request) {
		ResponseMessage responseMessage = null;

		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoAccessRole = this.serviceAccessRole.getAccessRoleDetails(dtoAccessRole.getId());
			if (dtoAccessRole != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_DETAILS_SUCCESS, false), dtoAccessRole);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Update Role
	 * @param dtoAccessRole
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/updateRole", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage updateRole(@RequestBody DtoAccessRole dtoAccessRole, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			AccessRole accessRole=repositoryAccessRole.findByRoleNameAndAndIdNotEqual(dtoAccessRole.getRoleName(), dtoAccessRole.getId());
			if(accessRole==null)
			{
				int id = this.serviceAccessRole.saveAccessRole(dtoAccessRole);
				if (id > 0) {
					dtoAccessRole.setAccessRoleId(id);
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_UPDATE, false), dtoAccessRole);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_MODIFIED.value(), HttpStatus.NOT_MODIFIED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(),
						HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_NAME_EXIT, false));
			}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Delete Roles
	 * @param dtoAccessRole
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/deleteRoles", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage deleteRoles(@RequestBody DtoAccessRole dtoAccessRole, HttpServletRequest request) {
		ResponseMessage responseMessage = null;

		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			if (dtoAccessRole.getIds() != null && !dtoAccessRole.getIds().isEmpty()) {
				boolean status = this.serviceAccessRole.deleteMultipleRoles(dtoAccessRole.getIds());
				if (status) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_DELETE_SUCCESS, false));
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_MODIFIED.value(), HttpStatus.NOT_MODIFIED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LIST_IS_EMPTY, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Access Role List
	 * @param request
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/getAccessRoleList", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAccessRoleList(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) 
	{
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) 
		{
			DtoSearch dtoSearch2 = this.serviceAccessRole.getAccessRoleList(dtoSearch);
			if (dtoSearch2 != null) 
			{
				@SuppressWarnings("unchecked")
				List<DtoAccessRole> list = (List<DtoAccessRole>) dtoSearch2.getRecords();
				if (!list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_LIST_SUCCESS, false), dtoSearch2);

				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));

				}
			} 
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false), null);
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Access Role List For Drop Down
	 * @param request
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/getAccessRoleListForDropDown", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAccessRoleListForDropDown(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;

		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoSearch dtoSearch2 = this.serviceAccessRole.getAccessRoleListForDropDown(dtoSearch);
			if (dtoSearch2 != null) {
				@SuppressWarnings("unchecked")
				List<DtoAccessRole> list = (List<DtoAccessRole>) dtoSearch2.getRecords();
				if (list!=null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_LIST_SUCCESS, false), dtoSearch2);

				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));

				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Save User Group
	 * @param dtoUserGroup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/saveUserGroup", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage saveUserGroup(@RequestBody DtoUserGroup dtoUserGroup, HttpServletRequest request) 
	{
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) 
		{
			UserGroup userGroup = repositoryUserGroup.findByGroupNameAndIsDeleted(dtoUserGroup.getGroupName(), false);
			if(userGroup==null)
			{
				dtoUserGroup = this.serviceUserGroup.saveUserGroup(dtoUserGroup);
				if (dtoUserGroup.getId() > 0) 
				{
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_GROUP_SUCCESS, false), dtoUserGroup);
				} 
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(),
						HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_GROUP_NAME_EXIT, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get User Group
	 * @param dtoUserGroup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getUserGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getUserGroup(@RequestBody DtoUserGroup dtoUserGroup, HttpServletRequest request) {
		ResponseMessage responseMessage = null;

		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoUserGroup = this.serviceUserGroup.getUserGroupDetailByGroupId(dtoUserGroup.getId());
			if (dtoUserGroup != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_GROUP_FATCHED, false), dtoUserGroup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}

	/**
	 * @description : Update User Group
	 * @param dtoUserGroup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/updateUserGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage updateUserGroup(@RequestBody DtoUserGroup dtoUserGroup, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) 
		{
			UserGroup userGroup = repositoryUserGroup.findByGroupNameAndDeleted(dtoUserGroup.getGroupName(),dtoUserGroup.getId());
			if(userGroup==null)
			{
				boolean status = this.serviceUserGroup.updateUserGroup(dtoUserGroup);
				if (status) 
				{
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_GROUP_UPDATE, false), dtoUserGroup);
				}
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(),
						HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_GROUP_NAME_EXIT, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get All User Group
	 * @param request
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/getAllUserGroup", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllUserGroup(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) 
	{
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) 
		{
			dtoSearch = this.serviceUserGroup.getAllUserGroup(dtoSearch);
			if (dtoSearch != null) 
			{
				@SuppressWarnings("unchecked")
				List<DtoUserGroup> usergroupList = (List<DtoUserGroup>) dtoSearch.getRecords();
				if (usergroupList!=null && !usergroupList.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_GROUP_FATCHED, false), dtoSearch);
				}
				else 
				{
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			}
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false), null);
			}
		}
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
			
	/**
	 * @description : Get All User Group List For Drop Down
	 * @param request
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/getAllUserGroupListForDropDown", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getAllUserGroupListForDropDown(HttpServletRequest request,
			@RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoSearch dtoSearch2 = this.serviceUserGroup.getAllUserGroupForDropDown(dtoSearch);
			if (dtoSearch2 != null) {
				@SuppressWarnings("unchecked")
				List<DtoUserGroup> usergroupList = (List<DtoUserGroup>) dtoSearch2.getRecords();
				if (usergroupList!=null && !usergroupList.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_GROUP_FATCHED, false), dtoSearch2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Delete User Group
	 * @param dtoUserGroup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/deleteUserGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage deleteUserGroup(@RequestBody DtoUserGroup dtoUserGroup, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			if (dtoUserGroup.getDeleteIds() != null && !dtoUserGroup.getDeleteIds().isEmpty()) {
				boolean status = this.serviceUserGroup.deleteUserGroup(dtoUserGroup.getDeleteIds());
				if (status) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.USER_GROUP_DELETE, false));
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_MODIFIED.value(), HttpStatus.NOT_MODIFIED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LIST_IS_EMPTY, false));

			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Save Role Group
	 * @param dtoRoleGroup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/saveRoleGroup", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE +";charset=utf-8", produces = MediaType.APPLICATION_JSON_VALUE +";charset=utf-8", headers = "Accept=application/json ;charset=utf-8")
	public ResponseMessage saveRoleGroup(@RequestBody DtoRoleGroup dtoRoleGroup, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) 
		{
			RoleGroup roleGroup=repositoryRoleGroup.findByRoleGroupNameAndIsDeleted(dtoRoleGroup.getRoleGroupName(), false);
			if(roleGroup==null)
			{
				boolean status = this.serviceRoleGroup.saveRoleGroup(dtoRoleGroup);
				if (status) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_SUCCESS, false), dtoRoleGroup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(),
						HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_NAME_EXIT, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Update Role Group
	 * @param dtoRoleGroup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/updateRoleGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage updateRoleGroup(@RequestBody DtoRoleGroup dtoRoleGroup, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			RoleGroup roleGroup=repositoryRoleGroup.getByRoleGroupNameAndIdNotEqual(dtoRoleGroup.getRoleGroupName(), dtoRoleGroup.getId());
			if(roleGroup==null)
			{
				boolean status = this.serviceRoleGroup.saveRoleGroup(dtoRoleGroup);
				if (status) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_UPDATE, false), dtoRoleGroup);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_MODIFIED.value(), HttpStatus.NOT_MODIFIED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			}
			else
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(),
						HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_NAME_EXIT, false));
			}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Role Group Details
	 * @param dtoRoleGroup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getRoleGroupDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getRoleGroupDetails(@RequestBody DtoRoleGroup dtoRoleGroup, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoRoleGroup = this.serviceRoleGroup.getRoleGroupDetails(dtoRoleGroup.getId());
			if (dtoRoleGroup != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_DETAILS_SUCCESS, false),
						dtoRoleGroup);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Delete Role Groups
	 * @param dtoRoleGroup
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/deleteRoleGroups", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage deleteRoleGroups(@RequestBody DtoRoleGroup dtoRoleGroup, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			if (dtoRoleGroup.getIds() != null && !dtoRoleGroup.getIds().isEmpty()) {
				boolean status = this.serviceRoleGroup.deleteMultipleRoleGroups(dtoRoleGroup.getIds());
				if (status) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_DELETE_SUCCESS, false));
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_MODIFIED.value(), HttpStatus.NOT_MODIFIED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LIST_IS_EMPTY, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Role Group List
	 * @param request
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/getRoleGroupList", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getRoleGroupList(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoSearch = this.serviceRoleGroup.getRoleGroupList(dtoSearch);
			if (dtoSearch != null) {
				@SuppressWarnings("unchecked")
				List<DtoRoleGroup> list = (List<DtoRoleGroup>) dtoSearch.getRecords();
				if (list!=null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_LIST_SUCCESS, false), dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));

				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false), null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Role Group List For Drop Down
	 * @param request
	 * @param dtoSearch
	 * @return
	 */
	@RequestMapping(value = "/getRoleGroupListForDropDown", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getRoleGroupListForDropDown(HttpServletRequest request, @RequestBody DtoSearch dtoSearch) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoSearch dtoSearch2 = this.serviceRoleGroup.getRoleGroupListForDropDown(dtoSearch);
			if (dtoSearch2 != null) {
				@SuppressWarnings("unchecked")
				List<DtoRoleGroup> list = (List<DtoRoleGroup>) dtoSearch2.getRecords();
				if (list!=null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_LIST_SUCCESS, false),
							dtoSearch2);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));

				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Search Access Roles
	 * @param dtoSearch
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/searchAccessRoles", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchRoles(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoSearch = this.serviceAccessRole.searchAcessRole(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_LIST_SUCCESS, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}

		return responseMessage;
	}
	
	/**
	 * @description : Search Role Groups
	 * @param dtoSearch
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/searchRoleGroups", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchRoleGroups(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoSearch = this.serviceRoleGroup.searchRoleGroup(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_LIST_SUCCESS, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}

		return responseMessage;
	}
	
	/**
	 * @description : Search User Groups
	 * @param dtoSearch
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/searchUserGroups", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchUserGroups(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoSearch = this.serviceUserGroup.searchUserGroup(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_GROUP_LIST_SUCCESS, false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}

		return responseMessage;
	}
	
	/**
	 * @description : Save or Update Access Role Screen Access
	 * @param dtoAccessRole
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/saveorUpdateAccessRoleScreenAccess", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage saveorUpdateAccessRoleModuleAccess(@RequestBody DtoAccessRole dtoAccessRole,
			HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			AccessRole accessRole = repositoryAccessRole.findByAccessRoleIdAndIsDeleted(dtoAccessRole.getAccessRoleId(),
					false);
			if (accessRole != null) {
				boolean status = serviceAccessRole.saveorUpdateAcessRoleScreenAccess1(dtoAccessRole.getModulesList(),
						accessRole);
				if (status) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_MODULE_SCREEN_SUCCESS, false),
							dtoAccessRole);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCESS_ROLE_NOT_FOUND, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Save or Update Access Role Transaction Access
	 * @param dtoAccessRole
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/saveorUpdateAccessRoleTransactionAccess", method = RequestMethod.PUT)
	public ResponseMessage saveorUpdateAccessRoleTransactionAccess(@RequestBody DtoAccessRole dtoAccessRole,
			HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			AccessRole accessRole = repositoryAccessRole.findByAccessRoleIdAndIsDeleted(dtoAccessRole.getAccessRoleId(),
					false);
			if (accessRole != null) {
				boolean status = serviceAccessRole
						.saveorUpdateAcessRoleTransactionAccess(dtoAccessRole.getModulesList(), accessRole);
				if (status) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_TRANSACTION_SUCCESS, false),
							dtoAccessRole);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCESS_ROLE_NOT_FOUND, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Save or Update Access Role Report Access
	 * @param dtoAccessRole
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/saveorUpdateAccessRoleReportAccess", method = RequestMethod.PUT)
	public ResponseMessage saveorUpdateAccessRoleReportAccess(@RequestBody DtoAccessRole dtoAccessRole,
			HttpServletRequest request) 
	{
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			AccessRole accessRole = repositoryAccessRole.findByAccessRoleIdAndIsDeleted(dtoAccessRole.getAccessRoleId(),
					false);
			if (accessRole != null) {
				boolean status = serviceAccessRole.saveorUpdateAcessRoleReportAccess(dtoAccessRole.getModulesList(),
						accessRole);
				if (status) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ROLE_REPORT_SUCCESS, false), dtoAccessRole);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							HttpStatus.INTERNAL_SERVER_ERROR,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ACCESS_ROLE_NOT_FOUND, false));
			}
		} 
		else 
		{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
}
