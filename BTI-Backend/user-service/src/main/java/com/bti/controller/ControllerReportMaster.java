package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoCompany;
import com.bti.model.dto.DtoEjbiErpUserRelation;
import com.bti.model.dto.DtoReportMaster;
import com.bti.model.dto.DtoReportSearch;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoUser;
import com.bti.service.ServiceReportMaster;
import com.bti.service.ServiceResponse;

@RestController
@RequestMapping("/reportMaster")
public class ControllerReportMaster {

	private static final Logger logger = Logger.getLogger(ControllerReportMaster.class);

	@Autowired
	SessionManager sessionManager;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceReportMaster serviceReportMaster;

	/**
	 * 
	 * @param request
	 * @param dtoReportMaster
	 * @return @request{ "reportLink":"www.google.com", "reportName":"Sample
	 *         Report", "reportDescription":"Sample Report Description" } @response{
	 *         "code": 201, "status": "CREATED", "result": { "reportLink":
	 *         "www.google.com", "reportName": "Sample Report", "reportDescription":
	 *         "Sample Report Description" }, "btiMessage": { "message": "Report
	 *         Master Created Successfully.", "messageShort":
	 *         "REPORT_MASTER_CREATED_SUCCESSFULLY" } }
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createReportMaster(HttpServletRequest request,
			@RequestBody DtoReportMaster dtoReportMaster) {
		logger.info("In CreateReportMaster Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoReportMaster = serviceReportMaster.saveOrUpdateReportMaster(dtoReportMaster);
			if (dtoReportMaster != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("REPORT_MASTER_CREATED_SUCCESSFULLY", false),
						dtoReportMaster);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("REPORT_MASTER_NOT_CREATED", false),
						dtoReportMaster);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Create Report" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * 
	 * @param request
	 * @param dtoReportMaster
	 * @return @request{ "ids":[1] } @response{ "code": 201, "status": "CREATED",
	 *         "result": { "deleteMessage": "N/A", "associateMessage": "N/A",
	 *         "deleteReportMaster": [ { "id": 1, "reportDescription": "Sample
	 *         Report Description" } ], }, "btiMessage": { "message": "Report Master
	 *         deleted Successfully.", "messageShort":
	 *         "REPORT_MASTER_DELETED_SUCCESSFULLY" } }
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteReportMaster(HttpServletRequest request,
			@RequestBody DtoReportMaster dtoReportMaster) {
		logger.info("Delete ReportMaster Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoReportMaster = serviceReportMaster.deleteReportMaster(dtoReportMaster.getIds());
			if (dtoReportMaster != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("REPORT_MASTER_DELETED_SUCCESSFULLY", false),
						dtoReportMaster);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("REPORT_MASTER_NOT_DELETED", false),
						dtoReportMaster);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Delete ReportMaster Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * 
	 * @param request
	 * @param dtoReportSearch
	 * @return @request{ "searchKeyword":" ", "pageNumber" : 0, "pageSize" :2
	 *         } @response{ "code": 201, "status": "CREATED", "result": {
	 *         "searchKeyword": " ", "pageNumber": 0, "pageSize": 2, "totalCount":
	 *         2, "records": [ { "id": 2, "reportLink": "www.google.com",
	 *         "reportName": "Sample Report1", "reportDescription": "Sample Report
	 *         Description1" }, { "id": 1, "reportLink": "www.yahoo.com",
	 *         "reportName": "Sample Report", "reportDescription": "Sample Report
	 *         Description" } ], "sortOn": "", "sortBy": "", "condition": "id" },
	 *         "btiMessage": { "message": "Report Master list fetched successfully",
	 *         "messageShort": "REPORT_MASTER_GET_ALL" } }
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.POST)
	public ResponseMessage searchReportMaster(HttpServletRequest request,
			@RequestBody DtoReportSearch dtoReportSearch) {
		logger.info("Search ReportMaster Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoReportSearch = serviceReportMaster.searchReportMaster(dtoReportSearch);
			if (dtoReportSearch != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("REPORT_MASTER_GET_ALL", false), dtoReportSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false), dtoReportSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Search ReportMaster Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	/**
	 * 
	 * @param request
	 * @return @response{ "code": 201, "status": "CREATED", "result": [ { "id": 1,
	 *         "reportLink": "www.yahoo.com", "reportName": "Sample Report",
	 *         "reportDescription": "Sample Report Description" }, { "id": 2,
	 *         "reportLink": "www.google.com", "reportName": "Sample Report1",
	 *         "reportDescription": "Sample Report Description1" } ], "btiMessage":
	 *         { "message": "Report Master list fetched successfully",
	 *         "messageShort": "REPORT_MASTER_GET_ALL" } }
	 */
	@RequestMapping(value = "/getAllReportMasterInDropdown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMessage getAllReportMasterInDropdown(HttpServletRequest request) {
		logger.info("Search getAllReportMasterInDropdown Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		try {
			if (session != null) {
				List<DtoReportMaster> reportMasterList = serviceReportMaster.getAllReportMasterDropDownList();
				if (reportMasterList != null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("REPORT_MASTER_GET_ALL", false),
							reportMasterList);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted("LIST_IS_EMPTY", false), reportMasterList);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return responseMessage;
	}

	@RequestMapping(value = "/reportIdCheck", method = RequestMethod.POST)
	public ResponseMessage reportIdcheck(HttpServletRequest request, @RequestBody DtoReportMaster dtoReportMaster) {
		logger.info("reportIdcheck Method called!!");
		ResponseMessage responseMessage = null;
		try {
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoReportMaster = serviceReportMaster.repeatByReportId(dtoReportMaster.getId());
			if (dtoReportMaster != null) {
					responseMessage = new ResponseMessage(
							HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
									.getMessageByShortAndIsDeleted("REPORTS_BY_COMAPNYID_FETCHED_SUCCESSFULLY", false),
							dtoReportMaster);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("REPORTS_BY_COMAPNYID_NOT_FETCHED", false),
						dtoReportMaster);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		} catch (Exception e) {
			logger.error(e);
		}
		return responseMessage;
	}
	
	@RequestMapping(value = "/getReportsByCompanyId", method = RequestMethod.POST)
	public ResponseMessage getReportsByCompanyId(HttpServletRequest request, @RequestBody DtoCompany dtoCompany) {
		logger.info("getReportsByCompanyId Method called!!");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoCompany = serviceReportMaster.getReportsByCompanyId(dtoCompany);
			if (dtoCompany != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted("REPORTS_BY_COMAPNYID_FETCHED_SUCCESSFULLY", false), dtoCompany);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("REPORTS_BY_COMAPNYID_NOT_FETCHED", false),
						dtoCompany);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Reports by Company Id" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getAllCompanyWiseReports", method = RequestMethod.GET)
	public ResponseMessage getAllCompanyWiseReports(HttpServletRequest request) {
		logger.info("getAllCompanyWiseReports Method called!!");
		ResponseMessage responseMessage = null;
		DtoSearch dtoSearch = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoSearch = serviceReportMaster.getAllCompanyWiseReports();
			if (dtoSearch != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted("COMPANY_WISE_REPORTS_GET_ALL_FETCHED_SUCCESSFULLY", false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(
						HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, serviceResponse
								.getMessageByShortAndIsDeleted("COMPANY_WISE_REPORTS_GET_ALL_NOT_FETCHED", false),
						dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Company Wise Reports" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getReportsByUserId", method = RequestMethod.POST)
	public ResponseMessage getReportsByUserId(HttpServletRequest request, @RequestBody DtoUser dtoUser) {
		logger.info("getReportsByUserId Method called!!");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoUser = serviceReportMaster.getReportsByUserId(dtoUser);
			if (dtoUser != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("REPORTS_BY_USERID_FETCHED_SUCCESSFULLY", false),
						dtoUser);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("REPORTS_BY_USERID_NOT_FETCHED", false), dtoUser);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Reports by User Id" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getAllUserWiseReports", method = RequestMethod.GET)
	public ResponseMessage getAllUserWiseReports(HttpServletRequest request) {
		logger.info("getAllUserWiseReports Method called!!");
		ResponseMessage responseMessage = null;
		DtoSearch dtoSearch = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoSearch = serviceReportMaster.getAllUserWiseReports();
			if (dtoSearch != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted("USER_WISE_REPORTS_GET_ALL_FETCHED_SUCCESSFULLY", false),
						dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("USER_WISE_REPORTS_GET_ALL_NOT_FETCHED", false),
						dtoSearch);
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("User Wise Reports" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/assignReportToComapnyUser", method = RequestMethod.POST)
	public ResponseMessage assignReportToComapnyUser(HttpServletRequest request, @RequestBody DtoUser dtoUser) {
		logger.info("assignReportToUser Method called!!");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoUser = serviceReportMaster.assignReportToComapnyUser(dtoUser);
			if (dtoUser != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("REPORT_TO_COMPANY_ASSIGNED_SUCCESSFULLY", false),
						dtoUser);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("REPORT_TO_COMPANY_NOT_ASSIGNED", false),
						dtoUser);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("assign Reports To Comapny & User" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getEJBIUserCredentials", method = RequestMethod.POST)
	public ResponseMessage getEJBIUserCredentials(HttpServletRequest request,
			@RequestBody DtoEjbiErpUserRelation dtoEjbiErpUserRelation) {
		logger.info("getEJBIUserCredentials Controller called!!");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoEjbiErpUserRelation = serviceReportMaster.getEJBIUserCredentials(dtoEjbiErpUserRelation.getReportId());
			if (dtoEjbiErpUserRelation != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("EJBI_USER_CREDENTIALS_GOT_SUCCESSFULLY", false),
						dtoEjbiErpUserRelation);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("EJBI_USER_CREDENTIALS_NOT_GET", false),
						dtoEjbiErpUserRelation);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("EJBI Credentials" + responseMessage.getMessage());
		return responseMessage;
	}
}
