package com.bti.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoFieldAccess;
import com.bti.model.dto.DtoFieldDetail;
import com.bti.model.dto.DtoSearch;
import com.bti.service.ServiceFieldAccess;
import com.bti.service.ServiceLanguage;
import com.bti.service.ServiceResponse;

@RestController
@RequestMapping("/common")
public class ControllerCommon {

	@Autowired
	SessionManager sessionManager;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	ServiceLanguage serviceLanguage;

	@Autowired
	ServiceFieldAccess serviceFieldAccess;

	@RequestMapping(value = "/getAllLanaguage", method = RequestMethod.GET)
	public ResponseMessage getAllLanaguage(HttpServletRequest request) throws Exception {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoSearch dtoSearch = serviceLanguage.getAllLanguage();
			if (dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_LIST_NOT_GETTING", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		return responseMessage;
	}
	
	/*@RequestMapping(value = "/saveFieldsAccess", method = RequestMethod.POST)
	public ResponseMessage saveFieldsAccessTest(HttpServletRequest request, @RequestBody DtoFieldAccess dtoFieldAccess)
			throws Exception {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			if (dtoFieldAccess.getAccessIds() != null && dtoFieldAccess.getAccessIds().size() > 0) {
			dtoFieldAccess = serviceFieldAccess.saveOrUpdateFieldAccess(dtoFieldAccess.getAccessIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_CREATED", false), dtoFieldAccess);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_NOT_CREATED", false), dtoFieldAccess);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		return responseMessage;
	}*/

}
