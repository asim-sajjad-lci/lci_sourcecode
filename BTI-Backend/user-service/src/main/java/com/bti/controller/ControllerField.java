/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.model.AccessRole;
import com.bti.model.FieldAccess;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoFieldAccess;
import com.bti.model.dto.DtoFieldDetail;
import com.bti.model.dto.DtoModule;
import com.bti.model.dto.DtoScreenDetail;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryFieldAccess;
import com.bti.repository.RepositoryFields;
import com.bti.service.ServiceField;
import com.bti.service.ServiceFieldAccess;
import com.bti.service.ServiceResponse;

/**
 * Description: ControllerField 
 * Name of Project: BTI
 */
@RestController
@RequestMapping("/fields")
public class ControllerField {

	/**
	 * @Description LOGGER use for put a logger in Field Controller
	 */
	private static final Logger LOGGER = Logger.getLogger(ControllerField.class);

	@Autowired
	ServiceField serviceField;

	@Autowired
	ServiceFieldAccess serviceFieldAccess;

	@Autowired
	SessionManager sessionManager;

	@Autowired
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryFields repositoryFields;

	@Autowired
	RepositoryFieldAccess repositoryFieldAccess;

	@RequestMapping(value = "/getAll", method = RequestMethod.PUT)
	public ResponseMessage getAllField(HttpServletRequest request, @RequestBody DtoFieldDetail dtoFieldDetail)
			throws Exception {
		LOGGER.info("Get All Field Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoSearch dtoSearch = serviceField.getAllFields(dtoFieldDetail);
			if (dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_LIST_NOT_GETTING", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Get All Fields Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/getAllFieldByCompanyId", method = RequestMethod.PUT)
	public ResponseMessage getAllFieldByCompanyId(HttpServletRequest request,
			@RequestBody DtoFieldAccess dtoFieldAccess) throws Exception {
		LOGGER.info("Get All Field By CompanyId Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoSearch dtoSearch = serviceFieldAccess.getAllFieldsByCompanyId(dtoFieldAccess);
			if (dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_GET_ALL", false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("DEPARTMENT_LIST_NOT_GETTING", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Get All Fields Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/listOfFieldsByCompanyScreenModules", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getListOfScreenModules(@RequestBody DtoScreenDetail dtoScreenDetail,
			HttpServletRequest request) {
		LOGGER.info("Get List of Modules");
		ResponseMessage responseMessage = null;
		List<DtoModule> dtoScreenDetailList = serviceFieldAccess.getAllScreenDetailList(dtoScreenDetail);
		if (dtoScreenDetailList != null) {
			if (dtoScreenDetailList.size() > 0) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("SCREEN_DETAILS_SUCCESS", false),
						dtoScreenDetailList);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted("SCREEN_DETAIL_NOT_FOUND", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
					serviceResponse.getMessageByShortAndIsDeleted("SCREEN_DETAILS_FAIL", false));
		}
		return responseMessage;
	}

	@RequestMapping(value = "/saveFieldsAccess", method = RequestMethod.POST)
	public ResponseMessage saveFieldsAccess(HttpServletRequest request, @RequestBody DtoFieldAccess dtoFieldAccess)
			throws Exception {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			if (dtoFieldAccess.getAccessIds() != null && dtoFieldAccess.getAccessIds().size() > 0) {
				dtoFieldAccess = serviceFieldAccess.saveOrUpdateFieldAccess(dtoFieldAccess.getAccessIds());
				if (dtoFieldAccess != null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("FIELD_ACCESS_CREATED", false), dtoFieldAccess);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted("FIELD_ACCESS_NOT_CREATED", false), dtoFieldAccess);
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("FIELD_ACCESSIDS_NOT_FOUND", false), dtoFieldAccess);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		return responseMessage;
	}

	/**
	 * @description : List Of Screen Modules By Access Role
	 * @param dtoScreenDetail
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/listOfScreenModulesByCompanyId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getListOfScreenModulesByAccessRole(@RequestBody DtoScreenDetail dtoScreenDetail,
			HttpServletRequest request) {
		LOGGER.info("Get List of Modules");
		ResponseMessage responseMessage = null;
		FieldAccess fieldAccess = repositoryFieldAccess
				.findByFieldAccessIdAndIsDeleted(dtoScreenDetail.getFieldAccessId(), false);
		if (fieldAccess != null) {
			List<DtoModule> dtoScreenDetailList = serviceFieldAccess.getAllScreenDetailListByComapnyId(fieldAccess,
					dtoScreenDetail);
			if (dtoScreenDetailList != null) {
				if (dtoScreenDetailList.size() > 0) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted("SCREEN_DETAILS_SUCCESS", false),
							dtoScreenDetailList);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted("SCREEN_DETAIL_NOT_FOUND", false));
				}
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("SCREEN_DETAILS_FAIL", false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
					serviceResponse.getMessageByShortAndIsDeleted("ACCESS_ROLE_NOT_FOUND", false));
		}
		return responseMessage;
	}

	@RequestMapping(value = "/saveOrUpdateFieldForMandatory", method = RequestMethod.PUT)
	public ResponseMessage saveOrUpdateFieldForMandatory(HttpServletRequest request,
			@RequestBody DtoFieldDetail dtoFieldDetail) throws Exception {
		LOGGER.info("Get All Field By CompanyId Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoFieldDetail = serviceField.changeStatus(dtoFieldDetail.getIds());
			if (dtoFieldDetail != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("FIELD_MANDATORY_STATUS_UPDATED", false), dtoFieldDetail);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("FIELD_MANDATORY_STATUS_NOT_UPDATED", false), dtoFieldDetail);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Get All Fields Method:" + responseMessage.getMessage());
		return responseMessage;
	}

	@RequestMapping(value = "/saveOrUpdateFieldLanguage", method = RequestMethod.PUT)
	public ResponseMessage saveOrUpdateFieldLanguage(HttpServletRequest request,
			@RequestBody DtoFieldDetail dtoFieldDetail) throws Exception {
		LOGGER.info("Get All Field By CompanyId Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoFieldDetail = serviceField.changeLanguage(dtoFieldDetail.getLanguageIds());
			if (dtoFieldDetail != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("FIELD_LANGUAGE_UPDATED_SUCCESS", false), dtoFieldDetail);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("FIELD_LANGUAGE_NOT_UPDATED", false), dtoFieldDetail);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted("SESSION_EXPIRED", false));
		}
		LOGGER.debug("Get All Fields Method:" + responseMessage.getMessage());
		return responseMessage;
	}

}