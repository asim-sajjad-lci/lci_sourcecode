package com.bti.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoCompanyReport;
import com.bti.service.ServiceCompanyReport;
import com.bti.service.ServiceResponse;

@RestController
@RequestMapping("/companyreport")
public class ControllerCompanyReport {
	
private static final Logger logger = Logger.getLogger(ControllerCompanyReport.class);
	
	@Autowired
	SessionManager sessionManager;

	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	ServiceCompanyReport serviceCompanyReport;
	
	/**
	 * 
	 * @param request
	 * @param dtoCompanyReport
	 * @return
	 * @request{
			  "description": "sample company",
			  "companyId": 1,
			  "dtoReportMaster": [{
			  	"id":1
			  },{
			  	"id":2
			  }]
			}
	 * @response{
				"code": 201,
				"status": "CREATED",
				"result": {
				"description": "sample company",
				"companyId": 1,
				"dtoReportMaster": [
					  {
						"id": 1
						},
					  {
						"id": 2
						}
					],
				},
					"btiMessage": {
						"message": "N/A",
						"messageShort": "N/A"
						}
				}		
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseMessage createCompayReport(HttpServletRequest request, @RequestBody DtoCompanyReport dtoCompanyReport) {
		logger.info("In CreateCompayReport Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoCompanyReport=serviceCompanyReport.saveOrUpdateCompanyReport(dtoCompanyReport);
			if (dtoCompanyReport != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("COMPANY_REPORT_CREATED_SUCCESSFULLY", false),
						dtoCompanyReport);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("COMPANY_REPORT_NOT_CREATED", false), dtoCompanyReport);
			}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Message" + responseMessage.getMessage());
		return responseMessage;
	}

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateCompayReport(HttpServletRequest request, @RequestBody DtoCompanyReport dtoCompanyReport) {
		logger.info("In UpdateCompayReport Method");
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoCompanyReport=serviceCompanyReport.saveOrUpdateCompanyReport(dtoCompanyReport);
			if (dtoCompanyReport != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted("COMPANY_REPORT_UPDATED_SUCCESSFULLY", false),
						dtoCompanyReport);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted("COMPANY_REPORT_NOT_UPDATED", false), dtoCompanyReport);
			}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		logger.debug("Message" + responseMessage.getMessage());
		return responseMessage;
	}
}
