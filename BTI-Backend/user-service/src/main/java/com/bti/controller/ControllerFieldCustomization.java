/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoFieldCustomization;
import com.bti.service.ServiceFieldCustomization;
import com.bti.service.ServiceResponse;

/**
 * Description: ControllerFieldCustomization Name of Project: BTI Created on:
 * May 17, 2017 Modified on: May 17, 2017 11:19:38 AM
 * 
 * @author Tausif Version:
 */
@RestController
@RequestMapping("/field-customization")
public class ControllerFieldCustomization {

	private static final Logger LOGGER = LoggerFactory.getLogger(ControllerFieldCustomization.class);

	@Autowired
	SessionManager sessionManager;

	@Autowired
	private ServiceFieldCustomization serviceFieldCustomization;

	@Autowired
	private ServiceResponse serviceResponse;

	/**
	 * @description : It will return all the Header and their side bar
	 * @param dtoScreenDetail
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseMessage save(@RequestBody DtoFieldCustomization dtoFieldCustomization, HttpServletRequest request) {

		LOGGER.info("Save Field Type");

		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoFieldCustomization responseDtoFieldCustomization = serviceFieldCustomization
					.saveOrUpdateFieldCustomization(dtoFieldCustomization);
			if (responseDtoFieldCustomization != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FIELD_CUSTOMIZATION_SAVE_SUCCESS, false),
						responseDtoFieldCustomization);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
								.getMessageByShortAndIsDeleted(MessageLabel.FIELD_CUSTOMIZATION_SAVE_ERROR, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}

		return responseMessage;
	}

	/**
	 * @description : Update FieldCustomization
	 * @param request
	 * @param dtoFieldCustomization
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage update(HttpServletRequest request,
			@RequestBody DtoFieldCustomization dtoFieldCustomization) {

		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoFieldCustomization = serviceFieldCustomization.saveOrUpdateFieldCustomization(dtoFieldCustomization);
			if (dtoFieldCustomization != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FIELD_CUSTOMIZATION_UPDATE_SUCCESS, false),
						dtoFieldCustomization);
			} else {
				responseMessage = new ResponseMessage(
						HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR, serviceResponse
								.getMessageByShortAndIsDeleted(MessageLabel.FIELD_CUSTOMIZATION_UPDATE_ERROR, false),
						dtoFieldCustomization);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}

		return responseMessage;
	}

	/**
	 * @description : Delete company
	 * @param request
	 * @param dtoCompany
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteCompany(HttpServletRequest request,
			@RequestBody DtoFieldCustomization dtoFieldCustomization) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			if (dtoFieldCustomization != null && dtoFieldCustomization.getId() > 0) {
				DtoFieldCustomization dtoFieldCustomization2 = serviceFieldCustomization
						.deleteFieldCustomization(dtoFieldCustomization);
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FIELD_CUSTOMIZATION_DELETE_SUCCESS, false),
						dtoFieldCustomization2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FIELD_CUSTOMIZATION_DELETE_ERROR,
								false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}

		return responseMessage;
	}

	/**
	 * get lead by phone
	 * 
	 * @param request
	 * @param phone
	 * @return
	 */
	@RequestMapping(value = "/getByCodeAndUser", method = RequestMethod.GET)
	public ResponseMessage getByCode(HttpServletRequest request, @RequestParam("code") String code,
			@RequestParam("userId") int userId) {

		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoFieldCustomization dtoFieldCustomization = serviceFieldCustomization.getByCodeAndUserId(code, userId);
			if (dtoFieldCustomization != null)
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK, serviceResponse
						.getMessageByShortAndIsDeleted(MessageLabel.FIELD_CUSTOMIZATION_GET_SUCCESS, false),
						dtoFieldCustomization);
			else
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FIELD_CUSTOMIZATION_GET_ERROR,
								false));

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}

		return responseMessage;
	}
}
